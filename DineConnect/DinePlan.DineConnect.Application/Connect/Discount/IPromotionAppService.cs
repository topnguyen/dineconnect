﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Discount
{
    public interface IPromotionAppService : IApplicationService
    {
        Task<PagedResultOutput<PromotionListDto>> GetAll(GetPromotionInput inputDto);
        Task<FileDto> GetAllToExcel(string sorting);
        Task<FileDto> GetExportPromotions(ApiLocationInput input);
        Task<GetPromotionForEditOutput> GetPromotionForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdatePromotion(CreateOrUpdatePromotionInput input);
        Task DeletePromotion(IdInput input);
        Task<ListResultOutput<PromotionListDto>> GetIds();
        Task SaveSortOrder(int[] all);
        Task ActivateItem(IdInput input);
        Task<PagedResultOutput<PromotionListDto>> GetPromotions(GetPromotionInput input);
        Task<ListResultOutput<ComboboxItemDto>> GetPromotionStepDiscountTypes();
        Task<ListResultOutput<ComboboxItemDto>> GetResetType();

        Task<QuotaDetailInfo> GetQuotaDetail(GetQuotaDetailInput input);
        Task<QuotaTicketInfo> GetQuotaTickets(GetQuotaDetailInput input);
        Task<FileDto> GetQuotasExcel(QuotaExportInput input);
        Task<FileDto> GetQuotaTicketsExcel(QuotaExportInput input);
        Task<PagedResultOutput<CombinePromotionDto>> GetPromotionsCombine(GetCombinePromotionInput input);
        Task<ListResultOutput<ComboboxItemDto>> GetPromotionPositions();
        Task<ListResultOutput<ComboboxItemDto>> GetPromotionApplyTypes();

        Task<PromotionQuotaDto> ApiGetPromotionQuota(ApiPromotionDefinitionInput input);
        Task<ApiPromotionOutput> ApiGetPromotions(ApiLocationInput input);
        Task<ApiTimerPromotionOutput> ApiGetTimerPromotions(ApiPromotionDefinitionInput input);
        Task<List<ApiFixedPromotionOutput>> ApiGetFixedPromotion(ApiPromotionDefinitionInput input);
        Task<FreePromotionEditDto> ApiGetFreePromotion(ApiPromotionDefinitionInput input);
        Task<FreeValuePromotionEditDto> ApiGetFreeValuePromotion(ApiPromotionDefinitionInput input);
        Task<DemandPromotionEditDto> ApiGetDemandPromotion(ApiPromotionDefinitionInput input);
        Task<FreeItemPromotionEditDto> ApiGetFreeItemPromotion(ApiPromotionDefinitionInput input);
        Task<TicketDiscountPromotionDto> ApiTicketDiscountPromotion(ApiPromotionDefinitionInput input);

        Task<ApiCardTypeOutput> ApiValidateCard(ApiCardTypeInput input);
        Task<ApiCardTypeOutput> ApiUpdateCard(ApiCardTypeInput input);
        Task<ApiPromotionStatusUpdate> ApiUpdatePromotionStatus(ApiPromotionStatusUpdate input);

        Task<ApiPromotionStatusUpdate> ApiReActivatePromotion(ApiPromotionStatusUpdate input);
        Task<ApiCardTypeOutput> ApiReverseAppliedCard(ApiCardTypeInput input);
        Task ResetQuota();
        Task UpdateQuotaUsage(Transaction.Ticket ticket);
    }
}