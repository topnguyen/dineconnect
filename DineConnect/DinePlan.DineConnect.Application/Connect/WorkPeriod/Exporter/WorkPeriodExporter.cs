﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Connect.WorkPeriod.Implementation;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace DinePlan.DineConnect.Connect.WorkPeriod.Exporter
{
    public class WorkPeriodExporter : FileExporterBase, IWorkPeriodExporter
    {
        private readonly IPaymentTypeAppService _payService;

        public WorkPeriodExporter(IPaymentTypeAppService payService)
        {
            _payService = payService;
        }

        public async Task<FileDto> ExportWorkPeriodSummary(WorkPeriodSummaryInput input, WorkPeriodAppService wpService)
        {
            var output = await wpService.GetSummaryReport(input);
            var builder = new StringBuilder();
            builder.Append(L("Summary"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString("dd/MM/yyyy")
                : DateTime.Now.ToString("dd/MM/yyyy"));

            var file = new FileDto(builder + ".xlsx",                              MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var package = new ExcelPackage())
            {
                if (output?.Periods != null && output.Periods.Any() && output.DashBoard != null)
                {
                    var sheet = package.Workbook.Worksheets.Add(L("Sales"));
                    sheet.OutLineApplyStyle = true;

                    var headers = new List<string>
                    {
                        L("Code").ToUpper(),
                        L("Name").ToUpper(),
                        L("TotalTickets").ToUpper(),
                        L("TotalSales").ToUpper()
                    };
                    var payments = await _payService.GetAllItems();
                    foreach (var pt in payments.Items.Where(a=>!a.Hide))
                    {
                        if (pt.Name.ToUpper().Equals("CASH"))
                        {
                            headers.Add(string.Format(L("Format_2"), pt.Name, L("Actual")).ToUpper());
                            headers.Add(string.Format(L("Format_2"), pt.Name, L("Entered")).ToUpper());
                            headers.Add(string.Format(L("Format_2"), pt.Name, L("Difference")).ToUpper());
                        }
                        else
                        {
                            headers.Add(string.Format(L("Format_2"), pt.Name, L("Sales")).ToUpper());
                        }
                    }

                    AddHeader(
                      sheet,
                      headers.ToArray()
                    );
                    var rowCount = 2;
                    var i = 0;
                   

                    foreach (var wpe in output.Periods)
                    {
                        int colCount = 1;
                        sheet.Cells[i + rowCount, colCount++].Value = wpe.LocationCode;
                        sheet.Cells[i + rowCount, colCount++].Value = wpe.LocationName;
                        sheet.Cells[i + rowCount, colCount++].Value = wpe.TotalTicketCount;
                        sheet.Cells[i + rowCount, colCount++].Value = wpe.Total;
                        var te = JsonConvert.DeserializeObject<List<WorkShiftDto>>(wpe.DayInformation);
                       
                        if (te.Any())
                        {
                            var infos = te.SelectMany(a => a.PaymentInfos);
                            foreach (var pt in payments.Items.Where(a => !a.Hide))
                            {
                                var actual = infos.Where(a => a.PaymentType == pt.Id).Sum(a => a.Actual);
                                var entered = infos.Where(a => a.PaymentType == pt.Id).Sum(a => a.Entered);
                                var difference = entered-actual;

                                if (pt.Name.ToUpper().Equals("CASH"))
                                {
                                    sheet.Cells[i + rowCount, colCount++].Value =
                                        infos.Where(a => a.PaymentType == pt.Id).Sum(a => a.Actual);
                                    sheet.Cells[i + rowCount, colCount++].Value =
                                        infos.Where(a => a.PaymentType == pt.Id).Sum(a => a.Entered);

                                    var colCo = colCount++;
                                    if (difference < 0M)
                                    {
                                        sheet.Cells[i + rowCount, colCo].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        sheet.Cells[i + rowCount, colCo].Style.Fill.BackgroundColor.SetColor(Color.Red);
                                        sheet.Cells[i + rowCount, colCo].Value = difference;
                                    }
                                    else
                                    {
                                        sheet.Cells[i + rowCount, colCo].Value = difference;
                                    }
                                }
                                else
                                {
                                    sheet.Cells[i + rowCount, colCount++].Value = actual;
                                }
                            }
                        }
                        rowCount++;
                    }
                }
                Save(package, file);

            }
            return file;
        }

        public async Task<FileDto> ExportWorkPeriodDetails(WorkPeriodSummaryInput input, WorkPeriodAppService wpService)
        {
            var output = await wpService.GetSummaryReport(input);
            var builder = new StringBuilder();
            builder.Append(L("Detail"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString("dd/MM/yyyy")
                : DateTime.Now.ToString("dd/MM/yyyy"));
            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var package = new ExcelPackage())
            {
                if (output?.Periods != null && output.Periods.Any() && output.DashBoard != null)
                {
                    var sheet = package.Workbook.Worksheets.Add(L("Sales"));
                    sheet.OutLineApplyStyle = true;

                    var headers = new List<string>
                    {
                        L("Code").ToUpper(),
                        L("Name").ToUpper(),
                        L("Date").ToUpper(),
                        L("Shift").ToUpper(),
                    };
                    var payments = await _payService.GetAllItems();
                    var nonHiPT = payments.Items.Where(a => !a.Hide);
                    foreach (var pt in nonHiPT)
                    {
                        if (pt.Name.ToUpper().Equals("CASH"))
                        {
                            headers.Add(string.Format(L("Format_2"), pt.Name, L("Actual")).ToUpper());
                            headers.Add(string.Format(L("Format_2"), pt.Name, L("Entered")).ToUpper());
                            headers.Add(string.Format(L("Format_2"), pt.Name, L("Difference")).ToUpper());
                        }
                        else
                        {
                            headers.Add(string.Format(L("Format_2"), pt.Name, L("Sales")).ToUpper());
                        }
                    }

                    AddHeader(
                      sheet,
                      headers.ToArray()
                    );
                    var rowCount = 2;
                    var i = 0;
                    Dictionary<int, decimal> alltotal = new Dictionary<int, decimal>();
                    Dictionary<int, decimal> allentered = new Dictionary<int, decimal>();
                    Dictionary<int, decimal> alldifference = new Dictionary<int, decimal>();

                    foreach (var wpe in output.Periods)
                    {
                        int colCount = 1;
                        sheet.Cells[i + rowCount, colCount++].Value = wpe.LocationCode;
                        sheet.Cells[i + rowCount, colCount++].Value = wpe.LocationName;
                        var allDayInfos = JsonConvert.DeserializeObject<List<WorkShiftDto>>(wpe.DayInformation);
                        if (allDayInfos.Any())
                        {
                            var myInnerCount = 0;
                            foreach (var aldI in allDayInfos)
                            {
                                myInnerCount = colCount;
                                if (aldI != null)
                                {
                                    sheet.Cells[i + rowCount, myInnerCount++].Value = aldI.StartDate.ToString("yyyy-MM-dd");
                                    sheet.Cells[i + rowCount, myInnerCount++].Value = aldI.StartDate.ToString("HH:mm");
                                    var infos = aldI.PaymentInfos;

                                    foreach (var pt in nonHiPT)
                                    {
                                        var actual = infos.Where(a => a.PaymentType == pt.Id).Sum(a => a.Actual);
                                        var entered = infos.Where(a => a.PaymentType == pt.Id).Sum(a => a.Entered);
                                        var difference = entered - actual;

                                        if (alltotal.ContainsKey(pt.Id.Value))
                                        {
                                            alltotal[pt.Id.Value] += actual;
                                        }
                                        else
                                        {
                                            alltotal[pt.Id.Value] = actual;
                                        }

                                        if (allentered.ContainsKey(pt.Id.Value))
                                        {
                                            allentered[pt.Id.Value] += entered;
                                        }
                                        else
                                        {
                                            allentered[pt.Id.Value] = entered;
                                        }

                                        if (alldifference.ContainsKey(pt.Id.Value))
                                        {
                                            alldifference[pt.Id.Value] += difference;
                                        }
                                        else
                                        {
                                            alldifference[pt.Id.Value] = difference;
                                        }

                                        if (pt.Name.ToUpper().Equals("CASH"))
                                        {
                                            sheet.Cells[i + rowCount, myInnerCount++].Value = actual;
                                            sheet.Cells[i + rowCount, myInnerCount++].Value = entered;

                                            var colCo = myInnerCount++;
                                            if (difference < 0M)
                                            {
                                                sheet.Cells[i + rowCount, colCo].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                sheet.Cells[i + rowCount, colCo].Style.Fill.BackgroundColor.SetColor(Color.Red);
                                                sheet.Cells[i + rowCount, colCo].Value = difference;
                                            }
                                            else
                                            {
                                                sheet.Cells[i + rowCount, colCo].Value = difference;
                                            }
                                        }
                                        else
                                        {
                                            sheet.Cells[i + rowCount, myInnerCount++].Value = actual;
                                        }
                                    }
                                }
                                rowCount++;
                            }
                            myInnerCount = colCount;
                            myInnerCount++;
                            var totC = myInnerCount++;

                            sheet.Cells[i + rowCount, totC].Value = L("Total").ToUpper();

                            var summInfo = allDayInfos.SelectMany(a=>a.PaymentInfos);
                            foreach (var pt in nonHiPT)
                            {
                                var actual = summInfo.Where(a => a.PaymentType == pt.Id).Sum(a => a.Actual);
                                var entered = summInfo.Where(a => a.PaymentType == pt.Id).Sum(a => a.Entered);
                                var difference = entered - actual;

                                if (pt.Name.ToUpper().Equals("CASH"))
                                {
                                    sheet.Cells[i + rowCount, myInnerCount++].Value = actual;
                                    sheet.Cells[i + rowCount, myInnerCount++].Value = entered;

                                   var colCo = myInnerCount++;
                                    if (difference < 0M)
                                    {
                                        sheet.Cells[i + rowCount, colCo].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        sheet.Cells[i + rowCount, colCo].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                                        sheet.Cells[i + rowCount, colCo].Value = difference;
                                    }
                                    else
                                    {
                                        sheet.Cells[i + rowCount, colCo].Value = difference;
                                    }
                                }
                                else
                                {
                                    sheet.Cells[i + rowCount, myInnerCount++].Value = actual;
                                }
                            }

                        }
                        rowCount++;
                    }

                    rowCount++;
                    int mycolCount = 4;
                    sheet.Cells[i + rowCount, mycolCount++].Value = L("GrandTotal").ToUpper();

                    foreach (var pt in nonHiPT)
                    {
                        sheet.Cells[i + rowCount, mycolCount++].Value = alltotal[pt.Id.Value];
                        sheet.Cells[i + rowCount, mycolCount++].Value = allentered[pt.Id.Value];

                        var col = mycolCount++;
                        if (alldifference[pt.Id.Value] < 0M)
                        {
                            sheet.Cells[i + rowCount, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            sheet.Cells[i + rowCount, col].Style.Fill.BackgroundColor.SetColor(Color.Red);
                            sheet.Cells[i + rowCount, col].Value = alldifference[pt.Id.Value];
                        }
                        else
                        {
                            sheet.Cells[i + rowCount, col].Value = alldifference[pt.Id.Value];
                        }
                    }
                }
                Save(package, file);
            }
            return file;
        }

    }
}

