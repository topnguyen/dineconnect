﻿using Abp.Application.Services.Dto;
using Abp.Configuration;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Connect.WorkPeriod.Implementation;
using DinePlan.DineConnect.DataExporting.Base;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Exporter.Util;
using Newtonsoft.Json;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.WorkPeriod.Exporter
{
    public class WorkPeriodOutputExporter : ExporterBase, IWorkPeriodOutputExporter
    {
        private readonly IRepository<Company> _companyService;
        private readonly string _dateFormat = "dd-MM-yyyy";
        private readonly string _dTFormat = "dd-MM-yyyy HH:ss:mm";

        private readonly IPaymentTypeAppService _payService;
        private readonly int _roundDecimals;

        public WorkPeriodOutputExporter(SettingManager settingManager, IRepository<Company> companyService,
            IPaymentTypeAppService payService)
        {
            _companyService = companyService;
            _roundDecimals = 3;
            _dTFormat = settingManager.GetSettingValue(AppSettings.ConnectSettings.DateTimeFormat);

            _payService = payService;
        }

        public async Task<FileDto> ExportSummaryOutput(WorkPeriodSummaryInput input, WorkPeriodAppService appService,
            IConnectReportAppService connReportService)
        {
            var eObject = new ExportInputObject
            {
                ExportType = input.ExportOutputType
            };
            eObject.Fields.Add("Date", new FieldAttributes
            {
                DisplayName = L("Date"),
                Alignment = FieldAlignment.Left,
                FieldType = ExportFiledType.String
            });

            eObject.Fields.Add("Sales", new FieldAttributes
            {
                DisplayName = L("Sales"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });

            eObject.Fields.Add("Tax", new FieldAttributes
            {
                DisplayName = L("Tax"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });

            eObject.Fields.Add("Total", new FieldAttributes
            {
                DisplayName = L("Total"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });

            var output = await appService.GetDetailSummaryByDateByTickets(input);
            var pOutput = output.Cast<ExportDataObject>().ToList();

            if (DynamicQueryable.Any(pOutput))
            {
                var firstPageBuilder = new StringBuilder();
                firstPageBuilder.Append("<H1>");
                firstPageBuilder.Append("TURNOVER CERTIFICATE");
                firstPageBuilder.Append("</H1>");
                firstPageBuilder.Append("{NL}");
                var company = _companyService.FirstOrDefault(a => a.Id > 0);
                if (company != null)
                {
                    firstPageBuilder.Append("Company Name : " + company.Name);
                    firstPageBuilder.Append("{NL}");
                }
                firstPageBuilder.Append(string.Format(L("FromTimeToTime"), input.StartDate.ToString(_dateFormat),
                    input.EndDate.ToString(_dateFormat)));
                firstPageBuilder.Append("{NL}");

                var lPageBuilder = new StringBuilder();
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append(
                    @"I confirm the amount reported above is computed in accordance with the definition of Gross Sales Turnover in the Lease Agreement.");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("------");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("Name & Signature of Authorised Office");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("------");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("Company Stamp");

                eObject.FirstPage = firstPageBuilder.ToString();
                eObject.LastPage = lPageBuilder.ToString();
                eObject.ExportObjects = pOutput;
                eObject.PaperSize = PaperSize.A4;
                eObject.Portrait = false;
                var fileNameBuilder = new StringBuilder();
                fileNameBuilder.Append("TurnOver");
                fileNameBuilder.Append(L("-"));
                fileNameBuilder.Append(!input.StartDate.Equals(DateTime.MinValue)
                    ? input.StartDate.ToString("dd/MM/yyyy")
                    : DateTime.Now.ToString("dd/MM/yyyy"));
                fileNameBuilder.Append(" - ");
                fileNameBuilder.Append(!input.EndDate.Equals(DateTime.MinValue)
                    ? input.EndDate.ToString("dd/MM/yyyy")
                    : DateTime.Now.ToString("dd/MM/yyyy"));

                eObject.FileName = fileNameBuilder.ToString();
                var exporter = new DineConnectDocExporter(eObject);
                return exporter.ExportFile(AppFolders.TempFileDownloadFolder);
            }

            return null;
        }

        public async Task<FileDto> ExportWorkPeriodSummary(WorkPeriodSummaryInput input, WorkPeriodAppService appService)
        {
            var output = await appService.GetSummaryReportOnTickets(input);

            var eObject = new ExportInputObject
            {
                ExportType = input.ExportOutputType
            };
            eObject.Fields.Add("LocationCode", new FieldAttributes
            {
                DisplayName = L("Code"),
                Alignment = FieldAlignment.Left,
                FieldType = ExportFiledType.String
            });
            eObject.Fields.Add("LocationName", new FieldAttributes
            {
                DisplayName = L("Name"),
                Alignment = FieldAlignment.Left,
                FieldType = ExportFiledType.String
            });
            eObject.Fields.Add("Total", new FieldAttributes
            {
                DisplayName = L("Sales"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });
            eObject.Fields.Add("TotalTicketCount", new FieldAttributes
            {
                DisplayName = L("TotalReceipts"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });
            eObject.Fields.Add("Average", new FieldAttributes
            {
                DisplayName = L("Average"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });

            var pOutput = output.Periods;

            if (DynamicQueryable.Any(pOutput))
            {
                var firstPageBuilder = new StringBuilder();
                firstPageBuilder.Append("<H1>");
                firstPageBuilder.Append("DAILY SALES");
                firstPageBuilder.Append("</H1>");
                firstPageBuilder.Append("{NL}");
                var company = _companyService.FirstOrDefault(a => a.Id > 0);
                if (company != null)
                {
                    firstPageBuilder.Append("Company Name : " + company.Name);
                    firstPageBuilder.Append("{NL}");
                }
                firstPageBuilder.Append(string.Format(L("FromTimeToTime"), input.StartDate.ToString(_dateFormat),
                    input.EndDate.ToString(_dateFormat)));
                firstPageBuilder.Append("{NL}");

                var lPageBuilder = new StringBuilder();
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append(L("PrintedOn") + " :" + DateTime.Now.ToString(_dateFormat));

                eObject.FirstPage = firstPageBuilder.ToString();
                eObject.LastPage = lPageBuilder.ToString();
                eObject.ExportObjects = pOutput.Cast<ExportDataObject>().ToList();
                eObject.PaperSize = PaperSize.A4;
                eObject.Portrait = false;
                var fileNameBuilder = new StringBuilder();
                fileNameBuilder.Append("DailySales");
                fileNameBuilder.Append(L("-"));
                fileNameBuilder.Append(!input.StartDate.Equals(DateTime.MinValue)
                    ? input.StartDate.ToString("dd/MM/yyyy")
                    : DateTime.Now.ToString("dd/MM/yyyy"));
                fileNameBuilder.Append(" - ");
                fileNameBuilder.Append(!input.EndDate.Equals(DateTime.MinValue)
                    ? input.EndDate.ToString("dd/MM/yyyy")
                    : DateTime.Now.ToString("dd/MM/yyyy"));

                eObject.FileName = fileNameBuilder.ToString();
                var exporter = new DineConnectDocExporter(eObject);
                return exporter.ExportFile(AppFolders.TempFileDownloadFolder);
            }

            return null;
        }

        public async Task<FileDto> ExportWorkPeriodDetails(WorkPeriodSummaryInput input, WorkPeriodAppService appService)
        {
            var output = await appService.GetWorkPeriods(input);

            var eObject = new ExportInputObject
            {
                ExportType = input.ExportOutputType
            };

            var pOutput = output;

            if (DynamicQueryable.Any(pOutput))
            {
                var firstPageBuilder = new StringBuilder();
                firstPageBuilder.Append("<H1>");
                firstPageBuilder.Append("DAILY SUMMARY");
                firstPageBuilder.Append("</H1>");
                firstPageBuilder.Append("{NL}");
                var company = _companyService.FirstOrDefault(a => a.Id > 0);
                if (company != null)
                {
                    firstPageBuilder.Append("Company Name : " + company.Name);
                    firstPageBuilder.Append("{NL}");
                }
                firstPageBuilder.Append(string.Format(L("FromTimeToTime"), input.StartDate.ToString(_dateFormat),
                    input.EndDate.ToString(_dateFormat)));
                firstPageBuilder.Append("{NL}");

                var lPageBuilder = new StringBuilder();

                eObject.FirstPage = firstPageBuilder.ToString();
                eObject.LastPage = lPageBuilder.ToString();
                eObject.ExportObjects = pOutput.Cast<ExportDataObject>().ToList();
                eObject.PaperSize = PaperSize.A4;
                eObject.Portrait = false;
                var fileNameBuilder = new StringBuilder();
                fileNameBuilder.Append("DailySummary");
                fileNameBuilder.Append(L("-"));
                fileNameBuilder.Append(!input.StartDate.Equals(DateTime.MinValue)
                    ? input.StartDate.ToString("dd/MM/yyyy")
                    : DateTime.Now.ToString("dd/MM/yyyy"));
                fileNameBuilder.Append(" - ");
                fileNameBuilder.Append(!input.EndDate.Equals(DateTime.MinValue)
                    ? input.EndDate.ToString("dd/MM/yyyy")
                    : DateTime.Now.ToString("dd/MM/yyyy"));
                var allPayItems = await _payService.GetAllItems();
                var htmlbody = GenerateHtmlBody(pOutput, allPayItems);
                eObject.FileName = fileNameBuilder.ToString();
                var exporter = new DineConnectDocExporter(eObject, htmlbody);
                return exporter.ExportFile(AppFolders.TempFileDownloadFolder);
            }

            return null;
        }

        public async Task<FileDto> ExportWeekDayOutput(WorkPeriodSummaryInput input, WorkPeriodAppService appService,
            IConnectReportAppService connReportService)
        {
            var eObject = new ExportInputObject
            {
                ExportType = input.ExportOutputType
            };
            eObject.Fields.Add("Date", new FieldAttributes
            {
                DisplayName = L("Date"),
                Alignment = FieldAlignment.Left,
                FieldType = ExportFiledType.String
            });
            eObject.Fields.Add("TotalTickets", new FieldAttributes
            {
                DisplayName = L("Total Tickets"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });
            eObject.Fields.Add("Sales", new FieldAttributes
            {
                DisplayName = L("Total Sales"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });
            eObject.Fields.Add("Average", new FieldAttributes
            {
                DisplayName = L("Average Sales"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });
            eObject.Fields.Add("Tax", new FieldAttributes
            {
                DisplayName = L("Tax"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });

            eObject.Fields.Add("Total", new FieldAttributes
            {
                DisplayName = L("Total"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });
            var output = await appService.GetWeekDaySales(input);
            var pOutput = output.Cast<ExportDataObject>().ToList();

            if (DynamicQueryable.Any(pOutput))
            {
                var firstPageBuilder = new StringBuilder();
                firstPageBuilder.Append("<H1>");
                firstPageBuilder.Append("WEEK DAY SALES ");
                firstPageBuilder.Append("</H1>");
                firstPageBuilder.Append("{NL}");
                var company = _companyService.FirstOrDefault(a => a.Id > 0);
                if (company != null)
                {
                    firstPageBuilder.Append("Company Name : " + company.Name);
                    firstPageBuilder.Append("{NL}");
                }
                firstPageBuilder.Append(string.Format(L("FromTimeToTime"), input.StartDate.ToString(_dateFormat),
                    input.EndDate.ToString(_dateFormat)));
                firstPageBuilder.Append("{NL}");

                var lPageBuilder = new StringBuilder();
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append(
                    @"I confirm the amount reported above is computed in accordance with the definition of Gross Sales Turnover in the Lease Agreement.");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("------");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("Name & Signature of Authorised Office");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("------");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("Company Stamp");

                eObject.FirstPage = firstPageBuilder.ToString();
                eObject.LastPage = lPageBuilder.ToString();
                eObject.ExportObjects = pOutput;
                eObject.PaperSize = PaperSize.A4;
                eObject.Portrait = false;
                var fileNameBuilder = new StringBuilder();
                fileNameBuilder.Append("WeekDay");
                fileNameBuilder.Append(L("-"));
                fileNameBuilder.Append(!input.StartDate.Equals(DateTime.MinValue)
                    ? input.StartDate.ToString("dd/MM/yyyy")
                    : DateTime.Now.ToString("dd/MM/yyyy"));
                fileNameBuilder.Append(" - ");
                fileNameBuilder.Append(!input.EndDate.Equals(DateTime.MinValue)
                    ? input.EndDate.ToString("dd/MM/yyyy")
                    : DateTime.Now.ToString("dd/MM/yyyy"));

                eObject.FileName = fileNameBuilder.ToString();
                var exporter = new DineConnectDocExporter(eObject);
                return exporter.ExportFile(AppFolders.TempFileDownloadFolder);
            }

            return null;
        }

        public async Task<FileDto> ExportWeekEndOutput(WorkPeriodSummaryInput input, WorkPeriodAppService appService,
            IConnectReportAppService connReportService)
        {
            var eObject = new ExportInputObject
            {
                ExportType = input.ExportOutputType
            };
            eObject.Fields.Add("Date", new FieldAttributes
            {
                DisplayName = L("Date"),
                Alignment = FieldAlignment.Left,
                FieldType = ExportFiledType.String
            });

            eObject.Fields.Add("Sales", new FieldAttributes
            {
                DisplayName = L("Sales"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });
            eObject.Fields.Add("TotalTickets", new FieldAttributes
            {
                DisplayName = L("Tickets"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });

            eObject.Fields.Add("Tax", new FieldAttributes
            {
                DisplayName = L("Tax"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });

            eObject.Fields.Add("Total", new FieldAttributes
            {
                DisplayName = L("Total"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });

            var output = await appService.GetWeekEndSales(input);
            var pOutput = output.Cast<ExportDataObject>().ToList();

            if (DynamicQueryable.Any(pOutput))
            {
                var firstPageBuilder = new StringBuilder();
                firstPageBuilder.Append("<H1>");
                firstPageBuilder.Append("WEEK END SALES");
                firstPageBuilder.Append("</H1>");
                firstPageBuilder.Append("{NL}");
                var company = _companyService.FirstOrDefault(a => a.Id > 0);
                if (company != null)
                {
                    firstPageBuilder.Append("Company Name : " + company.Name);
                    firstPageBuilder.Append("{NL}");
                }
                firstPageBuilder.Append(string.Format(L("FromTimeToTime"), input.StartDate.ToString(_dateFormat),
                    input.EndDate.ToString(_dateFormat)));
                firstPageBuilder.Append("{NL}");

                var lPageBuilder = new StringBuilder();
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append(
                    @"I confirm the amount reported above is computed in accordance with the definition of Gross Sales Turnover in the Lease Agreement.");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("------");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("Name & Signature of Authorised Office");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("------");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("Company Stamp");

                eObject.FirstPage = firstPageBuilder.ToString();
                eObject.LastPage = lPageBuilder.ToString();
                eObject.ExportObjects = pOutput;
                eObject.PaperSize = PaperSize.A4;
                eObject.Portrait = false;
                var fileNameBuilder = new StringBuilder();
                fileNameBuilder.Append("WeekEnd");
                fileNameBuilder.Append(L("-"));
                fileNameBuilder.Append(!input.StartDate.Equals(DateTime.MinValue)
                    ? input.StartDate.ToString("dd/MM/yyyy")
                    : DateTime.Now.ToString("dd/MM/yyyy"));
                fileNameBuilder.Append(" - ");
                fileNameBuilder.Append(!input.EndDate.Equals(DateTime.MinValue)
                    ? input.EndDate.ToString("dd/MM/yyyy")
                    : DateTime.Now.ToString("dd/MM/yyyy"));

                eObject.FileName = fileNameBuilder.ToString();
                var exporter = new DineConnectDocExporter(eObject);
                return exporter.ExportFile(AppFolders.TempFileDownloadFolder);
            }

            return null;
        }

        public async Task<FileDto> ExportScheduleSalesOutput(WorkPeriodSummaryInput input, WorkPeriodAppService appService,
         IConnectReportAppService connReportService)
        {
            var eObject = new ExportInputObject
            {
                ExportType = input.ExportOutputType
            };
            eObject.Fields.Add("Date", new FieldAttributes
            {
                DisplayName = L("Date"),
                Alignment = FieldAlignment.Left,
                FieldType = ExportFiledType.String
            });

            eObject.Fields.Add("Sales", new FieldAttributes
            {
                DisplayName = L("Sales"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });
            eObject.Fields.Add("TotalTickets", new FieldAttributes
            {
                DisplayName = L("Tickets"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });

            eObject.Fields.Add("Tax", new FieldAttributes
            {
                DisplayName = L("Tax"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });

            eObject.Fields.Add("Total", new FieldAttributes
            {
                DisplayName = L("Total"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + _roundDecimals,
                TotalRequired = true
            });

            var output = await appService.GetScheduleSales(input);
            var pOutput = output.Cast<ExportDataObject>().ToList();

            if (DynamicQueryable.Any(pOutput))
            {
                var firstPageBuilder = new StringBuilder();
                firstPageBuilder.Append("<H1>");
                firstPageBuilder.Append("SCHEDULE SALES");
                firstPageBuilder.Append("</H1>");
                firstPageBuilder.Append("{NL}");
                var company = _companyService.FirstOrDefault(a => a.Id > 0);
                if (company != null)
                {
                    firstPageBuilder.Append("Company Name : " + company.Name);
                    firstPageBuilder.Append("{NL}");
                }
                firstPageBuilder.Append(string.Format(L("FromTimeToTime"), input.StartDate.ToString(_dateFormat),
                    input.EndDate.ToString(_dateFormat)));
                firstPageBuilder.Append("{NL}");

                var lPageBuilder = new StringBuilder();
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append(
                    @"I confirm the amount reported above is computed in accordance with the definition of Gross Sales Turnover in the Lease Agreement.");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("------");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("Name & Signature of Authorised Office");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("------");
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append("Company Stamp");

                eObject.FirstPage = firstPageBuilder.ToString();
                eObject.LastPage = lPageBuilder.ToString();
                eObject.ExportObjects = pOutput;
                eObject.PaperSize = PaperSize.A4;
                eObject.Portrait = false;
                var fileNameBuilder = new StringBuilder();
                fileNameBuilder.Append("ScheduleSales");
                fileNameBuilder.Append(L("-"));
                fileNameBuilder.Append(!input.StartDate.Equals(DateTime.MinValue)
                    ? input.StartDate.ToString("dd/MM/yyyy")
                    : DateTime.Now.ToString("dd/MM/yyyy"));
                fileNameBuilder.Append(" - ");
                fileNameBuilder.Append(!input.EndDate.Equals(DateTime.MinValue)
                    ? input.EndDate.ToString("dd/MM/yyyy")
                    : DateTime.Now.ToString("dd/MM/yyyy"));

                eObject.FileName = fileNameBuilder.ToString();
                var exporter = new DineConnectDocExporter(eObject);
                return exporter.ExportFile(AppFolders.TempFileDownloadFolder);
            }

            return null;
        }

        private string GenerateHtmlBody(List<WorkPeriodSummaryOutput> output,
            PagedResultOutput<PaymentTypeEditDto> allPayItems)
        {
            var sb = new StringBuilder();
            foreach (var wp in output)
            {
                GenerateLocationTable(wp, sb);
                if (!string.IsNullOrEmpty(wp.DayInformation))
                {
                    var wshiDto = JsonConvert.DeserializeObject<List<WorkShiftDto>>(wp.DayInformation);
                    if (wshiDto.Any())
                    {
                        int Vshift = 1;
                        foreach (var shift in wshiDto)
                        {
                            GenerateShiftInfo(shift, allPayItems.Items, sb, Vshift);
                            Vshift++;
                        }
                    }
                }
                AddPageBreak(sb);
            }

            return sb.ToString();
        }

        private void GenerateShiftInfo(WorkShiftDto wshiDto, IReadOnlyList<PaymentTypeEditDto> allPayItems, StringBuilder sb,
            int shift)
        {
            sb.Append($"<br/><h3 style='text-align:center;'> Shift - {shift} </h3><br/>");

            //using (var table = new Html.Table(sb))
            //{
            //    table.StartBody();
            //    using (var tr = table.AddRow("2"))
            //    {
            //        tr.AddCell(L("StartTime"));
            //        tr.AddCell(wshiDto.StartDate.ToLongTimeString());

            //    }
            //    using (var tr = table.AddRow("2"))
            //    {
            //        tr.AddCell(L("StartUser"));
            //        tr.AddCell(wshiDto.StartUser);
            //    }
            //    using (var tr = table.AddRow("31"))
            //    {
            //        tr.AddCell(L("EndTime"));
            //        tr.AddCell(wshiDto.EndDate.ToLongTimeString());
            //    }
            //    using (var tr = table.AddRow("321"))
            //    {
            //        tr.AddCell(L("EndUser"));
            //        tr.AddCell(wshiDto.StopUser);
            //    }
            //    table.EndBody();
            //}
            sb.Append("<br/>");

            using (var table = new Html.Table(sb))
            {
                table.StartHead();
                using (var thead = table.AddRow())
                {
                    var headers = new List<string>
                            {
                                L("PaymentType").ToUpper(),
                                L("Actual").ToUpper(),
                                L("Entered").ToUpper(),
                                L("Difference").ToUpper()
                            };
                    foreach (var hCell in headers)
                    {
                        thead.AddHeaderCell(hCell);
                    }
                    table.EndHead();
                }
                table.StartBody();

                foreach (var pInfo in wshiDto.PaymentInfos)
                {
                    var mypType = allPayItems.FirstOrDefault(a => a.Id == pInfo.PaymentType);
                    if (mypType != null)
                    {
                        using (var tr = table.AddRow("someattributes"))
                        {
                            tr.AddCell(mypType.Name);
                            tr.AddCell(pInfo.Actual.ToString("N2"));
                            tr.AddCell(pInfo.Entered.ToString("N2"));
                            tr.AddCell(pInfo.Difference.ToString("N2"));
                        }
                    }
                }
                table.EndBody();
                table.EndTable();
            }
            //if (wshiDto.PaymentDenos.Any())
            //{
            //    sb.Append("<br/>");

            //    using (var table = new Html.Table(sb))
            //    {
            //        table.StartHead();
            //        using (var thead = table.AddRow())
            //        {
            //            var headers = new List<string>
            //            {
            //                L("CashType").ToUpper(),
            //                L("CashCount").ToUpper(),
            //                L("Total").ToUpper()
            //            };
            //            foreach (var hCell in headers)
            //            {
            //                thead.AddHeaderCell(hCell);
            //            }
            //            table.EndHead();
            //        }
            //        table.StartBody();

            //        foreach (var pInfo in wshiDto.PaymentDenos)
            //        {
            //            if (!string.IsNullOrEmpty(pInfo.Value))
            //            {
            //                var allDenos = JsonConvert.DeserializeObject<List<WorkTimePaymentDenomationValueDto>>(pInfo.Value);

            //                foreach (var allDeno in allDenos)
            //                {
            //                    using (var tr = table.AddRow("someattributes"))
            //                    {
            //                        tr.AddCell(allDeno.CashType.ToString("N2"), style: "text-align:right;");
            //                        tr.AddCell(allDeno.CashCount.ToString("N2"), style: "text-align:right;");
            //                        tr.AddCell(allDeno.Total.ToString("N2"), style: "text-align:right;");
            //                    }
            //                }

            //            }
            //        }
            //        table.EndBody();
            //        table.EndTable();
            //    }
            //}
        }

        private void AddPageBreak(StringBuilder sb)
        {
            sb.Append("<p style='page-break-after: always;'></p>");
        }

        private void GenerateLocationTable(WorkPeriodSummaryOutput wpe, StringBuilder sb)
        {
            sb.Append("<br/><h3 style='text-align:center;'> Summary </h3><br/>");

            using (var table = new Html.Table(sb))
            {
                table.StartBody();
                using (var tr = table.AddRow("2"))
                {
                    tr.AddCell(L("Code"));
                    tr.AddCell(wpe.LocationCode);
                }
                using (var tr = table.AddRow("2"))
                {
                    tr.AddCell(L("Name"));
                    tr.AddCell(wpe.LocationName);
                }
                using (var tr = table.AddRow("31"))
                {
                    tr.AddCell(L("Date"));
                    tr.AddCell(wpe.ReportStartDay.ToString(_dateFormat));
                }

                using (var tr = table.AddRow("3"))
                {
                    tr.AddCell(L("TicketCount"));
                    tr.AddCell(wpe.TotalTicketCount.ToString("N2"));
                }

                using (var tr = table.AddRow("3"))
                {
                    tr.AddCell(L("Total"));
                    tr.AddCell(wpe.Total.ToString("N2"));
                }
                table.EndBody();
            }
            sb.Append("<br/><br/>");
        }
    }
}