﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;

namespace DinePlan.DineConnect.Connect.WorkPeriod
{
    public interface IWorkPeriodAppService : IApplicationService
    {
        Task<PagedResultOutput<WorkPeriodListDto>> GetAll(GetWorkPeriodInput inputDto);
        Task<CreateOrUpdatePeriodOutput> CreateOrUpdateWorkPeriod(CreateOrUpdateWorkPeriodInput input);
        Task<List<WorkPeriodDetailOutput>> GetDetailSummary(WorkPeriodSummaryInput input);
        Task<FileDto> GetAllToExcel();
        Task<GetWorkPeriodForEditOutput> GetWorkPeriodForEdit(NullableIdInput nullableIdInput);
        Task DeleteWorkPeriod(IdInput input);
        Task<ListResultOutput<WorkPeriodListDto>> GetIds();

        Task<StartWorkPeriodOutput> StartWorkPeriod(StartWorkPeriodInput input);
        Task<EndWorkPeriodOutput> EndWorkPeriod(EndWorkPeriodInput input);

        Task<WorkPeriodOutput> GetSummaryReport(WorkPeriodSummaryInput input);
        Task<List<WorkPeriodSummaryOutput>> GetWorkPeriods(WorkPeriodSummaryInput input);

        Task<PrintOutIn40Cols> GetSummaryPrint(WorkPeriodSummaryInput input);

        Task<FileDto> GetDetailExcel(WorkPeriodSummaryInput input);
        Task<FileDto> GetSummaryExcel(WorkPeriodSummaryInput input);
        Task<FileDto> GetSummaryOutput(WorkPeriodSummaryInput input);
        Task<List<WorkPeriodDetailOutput>> GetDetailSummaryByDate(WorkPeriodSummaryInput input);
        Task<WorkPeriodOutput> GetSummaryReportOnTickets(WorkPeriodSummaryInput input);
        Task<List<WorkDayDetailSummaryOutput>> GetDetailSummaryByDateByTickets(WorkPeriodSummaryInput input);
        Task<List<WorkDayDetailSummaryOutput>> GetWeekDaySales(WorkPeriodSummaryInput input);
        Task<List<WorkDayDetailSummaryOutput>> GetWeekEndSales(WorkPeriodSummaryInput input);
        Task<List<WorkDayDetailSummaryOutput>> GetScheduleSales(WorkPeriodSummaryInput input);

        Task<FileDto> GetWeekDaySalesOutput(WorkPeriodSummaryInput input);
        Task<FileDto> GetWeekEndSalesOutput(WorkPeriodSummaryInput input);
        Task<FileDto> GetScheduleSalesOutput(WorkPeriodSummaryInput input);


        Task<int> SyncWorkPeriod(PostInput postInput);
    }
}