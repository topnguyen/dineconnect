﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Exporter.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using OpenHtmlToPdf;

namespace DinePlan.DineConnect.Connect.WorkPeriod.Dtos
{
    [AutoMapFrom(typeof(Period.WorkPeriod))]
    public class WorkPeriodListDto : FullAuditedEntityDto
    {
        public string StartUser { get; set; }
        public string EndUser { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public virtual int Wid { get; set; }
        public virtual decimal TotalSales { get; set; }
        public virtual int TotalTicketCount { get; set; }

        public virtual int LocationId { get; set; }
        public string LocationName { get; set; }
        public string LocationCode { get; set; }
        public bool AutoClosed { get; set; }
    }

    [AutoMapTo(typeof(Period.WorkPeriod))]
    public class WorkPeriodEditDto
    {
        public int? Id { get; set; }
        public string StartUser { get; set; }
        public string EndUser { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public virtual int Wid { get; set; }
        public virtual decimal TotalSales { get; set; }
        public virtual decimal TotalTaxes { get; set; }
        public virtual int TotalTicketCount { get; set; }

        public virtual int LocationId { get; set; }
        public int TenantId { get; set; }
        public string WorkPeriodInformations { get; set; }
        public string DepartmentTicketInformations { get; set; }

        public bool AutoClosed { get; set; }

    }

    public class CreateOrUpdatePeriodOutput
    {
        public int Wid { get; set; }
    }

    public class GetWorkPeriodInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class WorkPeriodSummaryInput : IInputDto, IFileExport
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<LocationListDto> Locations { get; set; }
        public int LocationId { get; set; }
        public ExportType ExportOutputType { get; set; }
        public bool RunInBackground { get; set; }
        public int? TenantId { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public int UserId { get; set; }
        public DateTime DateReport { get; set; }
        public bool Portrait { get; set; }
        public OpenHtmlToPdf.PaperSize PaperSize { get; set; }
        public string DateFormat { get; set; }
        public string DatetimeFormat { get; set; }

    }

    public class WorkPeriodSummaryInputWithPaging : PagedAndSortedInputDto, IShouldNormalize, IFileExport, IInputDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<LocationListDto> Locations { get; set; }
        public int LocationId { get; set; }
        public ExportType ExportOutputType { get; set; }
        public bool Portrait { get; set; }
        public OpenHtmlToPdf.PaperSize PaperSize { get; set; }
        public bool RunInBackground { get; set; }
        public int? TenantId { get; set; }
        public string ReportDescription { get; set; }
        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "Id";
            }
        }
        public string DateFormat { get; set; }
        public string DatetimeFormat { get; set; }

    }

    public class WorkDayDetailSummaryOutput : ExportDataObject
    {
        public string Date { get; set; }
        public decimal Sales { get; set; }
        public decimal Tax { get; set; }
        public decimal Total { get; set; }
        public decimal Discount { get; set; }
        public decimal TotalTickets { get; set; }

        public decimal Average
        {
            get
            {
                if (Total > 0M && TotalTickets > 0M)
                {
                    return Total / TotalTickets;
                }
                return 0M;
            }
        }
    }

    public class WorkPeriodOutput : IOutputDto
    {
        public DashboardPeriod DashBoard { get; set; }
        public List<WorkPeriodSummaryOutput> Periods { get; set; }
        public string LastLocationClosed { get; set; }
        public DateTime TimeOfClosed { get; set; }
        public Dictionary<string, decimal> OtherPayments { get; set; }

        public WorkPeriodOutput()
        {
            DashBoard = new DashboardPeriod();
            Periods = new List<WorkPeriodSummaryOutput>();
            OtherPayments = new Dictionary<string, decimal>();
        }
    }

    public class DashboardPeriod : IOutputDto
    {
        public decimal TotalSales { get; set; }
        public decimal TotalTickets { get; set; }
        public decimal Average { get; set; }
    }

    public class WorkPeriodDetailOutput : IOutputDto
    {
        public DateTime WorkDay { get; set; }
        public int WorkDayId { get; set; }
        public string TerminalCode { get; set; }
        public string Cashier { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public string LocationCode { get; set; }
        public decimal Collection { get; set; }
        public decimal Ecr { get; set; }
        public decimal GST { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal Difference => Collection - Ecr;
        public string CreateUser { get; set; }
        public DateTime CreateDate { get; set; }
        public string ModifyUser { get; set; }
        public DateTime ModifyDate { get; set; }
        public List<EndPeriodPaymentInformation> PaymentInfo { get; set; }
        public decimal Total { get; set; }
    }

    public class WorkPeriodSummaryOutput : ExportDataObject
    {
        public DateTime ReportStartDay { get; set; }
        public DateTime ReportEndDay { get; set; }
        public string DayStartStr => ReportStartDay.ToString("D");
        public string DayEndStar => ReportEndDay.ToString("D");
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public string LocationCode { get; set; }
        public decimal Total { get; set; }
        public decimal TotalTicketCount { get; set; }

        public decimal Average
        {
            get
            {
                if (Total > 0M && TotalTicketCount > 0M)
                    return Total / TotalTicketCount;
                return 0M;
            }
        }

        public decimal Actual { get; set; }
        public decimal Entered { get; set; }
        public decimal Difference => Entered - Actual;
        public string DayInformation { get; set; }
        public int Wid { get; set; }

        public List<WorkShiftDto> WorkShifts
        {
            get
            {
                if (string.IsNullOrEmpty(DayInformation))
                {
                    return null;
                }

                return JsonConvert.DeserializeObject<List<WorkShiftDto>>(DayInformation);
            }
        }

        public List<WorkShiftDto> WorkShiftsTemp { get; set; }


        public List<string> Terminals
        {
            get
            {
                if (WorkShifts == null)
                {
                    return null;
                }

                return WorkShifts.Select(t => t.TerminalName).Distinct().ToList();
            }
        }
    }

    public class WorkShiftDto
    {
        public WorkShiftDto()
        {
            PaymentInfos = new List<WorkTimePaymentInformationDto>();
            PaymentDenos = new List<WorkTimePaymentDenomationDto>();
        }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string TerminalName { get; set; }

        public string StartUser { get; set; }
        public string StopUser { get; set; }
        public int TillCount { get; set; }
        public List<WorkTimePaymentInformationDto> PaymentInfos { get; set; }
        public List<WorkTimePaymentDenomationDto> PaymentDenos { get; set; }
        public decimal Float { get; set; }

        public string UniqueId
        {
            get
            {
                return $"{StartDate.Ticks}:{EndDate.Ticks}:{TerminalName}";
            }
            set { }
        }
        private string _infoId;
        public string infoId
        {
            get
            {
                return _infoId != null && _infoId.ToUpper() == "ALL" ? _infoId : StartDate.ToShortDateString() + " - " + EndDate.ToShortDateString();
            }
            set
            {
                _infoId = value;
            }
        }
    }

    public class WorkShiftByLocationDto: WorkShiftDto
    {
        public int LocationId { get; set; }
    }

    public class WorkTimePaymentDenomationDto
    {
        public string Value { get; set; }
        public int PaymentType { get; set; }
    }

    public class WorkTimePaymentDenomationValueDto
    {
        public decimal CashCount { get; set; }
        public decimal CashType { get; set; }
        public decimal Total { get; set; }
    }

    public class WorkTimePaymentInformationDto
    {
        public int PaymentType { get; set; }
        public decimal Actual { get; set; }
        public decimal Entered { get; set; }
        public decimal Difference => Entered - Actual;
    }

    public class GetWorkPeriodForEditOutput : IOutputDto
    {
        public WorkPeriodEditDto WorkPeriod { get; set; }
    }

    public class CreateOrUpdateWorkPeriodInput : IInputDto
    {
        [Required]
        public WorkPeriodEditDto WorkPeriod { get; set; }
    }

    public class StartWorkPeriodInput : IInputDto
    {
        public int TenantId { get; set; }
        public long UserId { get; set; }
        public int LocationId { get; set; }
        public decimal Float { get; set; }
        public string UserName { get; set; }
        public DateTime StartTime { get; set; }
        public int Wid { get; set; }
        public decimal TotalTaxs { get; set; }
    }
    public class WorkPeriodDepartmentDto
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int DepartmentSyncId { get; set; }
        public decimal TotalTickets { get; set; }
        public decimal TotalAmount { get; set; }
        public List<WorkPeriodPayment> Payments { get; set; }
    }
    public class WorkPeriodPayment
    {
        public int PaymentTypeId { get; set; }
        public decimal PaymentAmount { get; set; }
        public string PaymentTypeName { get; set; }
        public int PaymentTypeSyncId { get; set; }
    }

    public class WorkPeriodDepartmentPayment
    {
        public int PaymentTypeId { get; set; }
        public decimal PaymentAmount { get; set; }
        public string PaymentTypeName { get; set; }
        public int PaymentTypeSyncId { get; set; }

        public List<WorkPeriodDepartmentDto> Departments { get; set; }

        public WorkPeriodDepartmentPayment()
        {
            Departments = new List<WorkPeriodDepartmentDto>();
        }
    }
    public class StartWorkPeriodOutput : IOutputDto
    {
        public int WorkPeriodId { get; set; }
    }

    public class EndWorkPeriodInput : IInputDto
    {
        public int TenantId { get; set; }

        public int WorkPeriodId { get; set; }
        public DateTime EndTime { get; set; }
        public List<EndPeriodPaymentInformation> Payments { get; set; }
    }

    public class EndPeriodPaymentInformation
    {
        public string Name { get; set; }
        public int PaymentTypeId { get; set; }
        public decimal Actual { get; set; }
        public decimal Entered { get; set; }
    }

    public class EndWorkPeriodOutput : IOutputDto
    {
        public int WorkPeriodId { get; set; }
    }
}