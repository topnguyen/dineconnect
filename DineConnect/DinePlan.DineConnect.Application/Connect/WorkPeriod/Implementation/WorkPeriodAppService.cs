﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using Castle.Core.Logging;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.House.Transaction.Implementation;
using DinePlan.DineConnect.Job.Connect;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Net.Mail;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Job.Mail;

namespace DinePlan.DineConnect.Connect.WorkPeriod.Implementation
{
    public class WorkPeriodAppService : DineConnectAppServiceBase, IWorkPeriodAppService
    {
        private readonly IExcelExporter _exporter;
        private readonly IWorkPeriodManager _workperiodManager;
        private readonly IRepository<Period.WorkPeriod> _workperiodRepo;
        private readonly ILogger _logger;
        private readonly IRepository<Master.Location> _lRepo;
        private readonly IRepository<Company> _cRepo;
        private readonly IWorkPeriodExporter _wpe;
        private readonly IWorkPeriodOutputExporter _wpoe;
        private readonly IPaymentTypeAppService _ptService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IAbpSession _session;
        private readonly IConnectReportAppService _connReportService;
        private readonly ISettingManager _settingManager;
        private readonly IBackgroundJobManager _bgm;
        private readonly IRepository<PaymentType> _pRepo;
        private readonly IReportBackgroundAppService _rbas;
        private readonly ILocationAppService _locService;
        private readonly IEmailSender _emailSender;


        public WorkPeriodAppService(
            ILocationAppService locService,
            IAbpSession session,
            IWorkPeriodManager workperiodManager, IPaymentTypeAppService ptService, IWorkPeriodOutputExporter wpoe,
            IRepository<Period.WorkPeriod> workPeriodRepo, IRepository<Company> cRepo, IRepository<Master.Location> lrepo, IWorkPeriodExporter wpe,
            IExcelExporter exporter, ILogger logger, IBackgroundJobManager bgm, IRepository<PaymentType> pRepo,
            IUnitOfWorkManager unitOfWorkManager, IConnectReportAppService conReportService, ISettingManager settingManager,
            IReportBackgroundAppService rbas, IEmailSender emailSender)
        {
            _session = session;
            _connReportService = conReportService;
            _wpoe = wpoe;
            _workperiodManager = workperiodManager;
            _workperiodRepo = workPeriodRepo;
            _ptService = ptService;
            _exporter = exporter;
            _logger = logger;
            _lRepo = lrepo;
            _cRepo = cRepo;
            _pRepo = pRepo;
            _wpe = wpe;
            _unitOfWorkManager = unitOfWorkManager;
            _settingManager = settingManager;
            _bgm = bgm;
            _rbas = rbas;
            _locService = locService;
            _emailSender = emailSender;
        }

        public async Task<PagedResultOutput<WorkPeriodListDto>> GetAll(GetWorkPeriodInput input)
        {
            var allItems = _workperiodRepo.GetAll();
            if (input.Operation == "SEARCH")
            {
                allItems = _workperiodRepo
                    .GetAll()
                    .WhereIf(
                        !input.Filter.IsNullOrEmpty(),
                        p => p.Id.Equals(input.Filter)
                    );
            }
            else
            {
                allItems = _workperiodRepo
                    .GetAll()
                    .WhereIf(
                        !input.Filter.IsNullOrEmpty(),
                        p => p.StartUser.Contains(input.Filter)
                    );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<WorkPeriodListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<WorkPeriodListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _workperiodRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<WorkPeriodListDto>>();
            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames = new[] { "Id" }
            };
            return _exporter.ExportToFile(baseE, L("WorkPeriod"));
        }

        public async Task<GetWorkPeriodForEditOutput> GetWorkPeriodForEdit(NullableIdInput input)
        {
            WorkPeriodEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _workperiodRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<WorkPeriodEditDto>();
            }
            else
            {
                editDto = new WorkPeriodEditDto();
            }

            return new GetWorkPeriodForEditOutput
            {
                WorkPeriod = editDto
            };
        }

        public async Task<CreateOrUpdatePeriodOutput> CreateOrUpdateWorkPeriod(CreateOrUpdateWorkPeriodInput input)
        {
            var updatePeriod = false;
            CreateOrUpdatePeriodOutput output = new CreateOrUpdatePeriodOutput();

            if (input.WorkPeriod.Id.HasValue && input.WorkPeriod.Id > 0)
            {
                updatePeriod = true;
            }
            if (input.WorkPeriod.Wid > 0)
            {
                var count =
                        await
                            _workperiodRepo.LongCountAsync(
                                a =>
                                    a.Wid == input.WorkPeriod.Wid && a.LocationId == input.WorkPeriod.LocationId);
                updatePeriod = count > 0;
                if (updatePeriod)
                {
                    var period =
                            _workperiodRepo.FirstOrDefault(
                                a =>
                                   a.Wid == input.WorkPeriod.Wid && a.LocationId == input.WorkPeriod.LocationId);

                    if (period != null)
                    {
                        input.WorkPeriod.Id = period.Id;
                    }
                    else
                    {
                        updatePeriod = false;
                    }
                }
            }
            if (updatePeriod)
            {
                await UpdateWorkPeriod(input);
                output.Wid = input.WorkPeriod.Id.Value;
            }
            else
            {
                var pId = await CreateWorkPeriod(input);
                output.Wid = pId;
            }
            if (input.WorkPeriod.TotalSales > 0M)
            {
                await _bgm.EnqueueAsync<ConnectJob, ConnectJobArgs>(new ConnectJobArgs
                {
                    WorkPeriodId = output.Wid,
                    TenantId = input.WorkPeriod.TenantId
                });
            }
            return output;
        }

        public async Task DeleteWorkPeriod(IdInput input)
        {
            await _workperiodRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultOutput<WorkPeriodListDto>> GetIds()
        {
            var lstWorkPeriod = await _workperiodRepo.GetAll().ToListAsync();
            return new ListResultOutput<WorkPeriodListDto>(lstWorkPeriod.MapTo<List<WorkPeriodListDto>>());
        }

        public async Task<StartWorkPeriodOutput> StartWorkPeriod(StartWorkPeriodInput input)
        {
            StartWorkPeriodOutput output = new StartWorkPeriodOutput();

            Period.WorkPeriod period = new Period.WorkPeriod
            {
                StartTime = input.StartTime,
                EndTime = input.StartTime,
                StartUser = input.UserName,
                EndUser = input.UserName,
                TotalSales = 0M,
                TotalTicketCount = 0,
                LocationId = input.LocationId,
                TenantId = input.TenantId,
                Wid = input.Wid
            };
            List<WorkShiftDto> shiftInfo = new List<WorkShiftDto>
            {
                new WorkShiftDto()
                {
                    StartDate = input.StartTime,
                    EndDate = input.StartTime,
                    Float = input.Float,
                    StartUser = input.UserName,
                    StopUser = input.UserName
                }
            };

            period.WorkPeriodInformations = JsonConvert.SerializeObject(shiftInfo);

            var wp = await _workperiodRepo.InsertAndGetIdAsync(period);
            output.WorkPeriodId = wp;
            return output;
        }

        public async Task<EndWorkPeriodOutput> EndWorkPeriod(EndWorkPeriodInput input)
        {
            EndWorkPeriodOutput output = new EndWorkPeriodOutput();
            try
            {
                if (input.WorkPeriodId > 0)
                {
                    var period = await _workperiodRepo.GetAsync(input.WorkPeriodId);
                    if (period != null && period.StartTime == period.EndTime)
                    {
                        period.EndTime = input.EndTime;

                        
                        if (!string.IsNullOrEmpty(period.WorkPeriodInformations))
                        {
                            List<WorkShiftDto> shiftDto =
                                JsonConvert.DeserializeObject<List<WorkShiftDto>>(period.WorkPeriodInformations);
                            if (shiftDto != null && shiftDto.Any())
                            {
                                WorkShiftDto wS = shiftDto.First();
                                wS.EndDate = input.EndTime;
                                wS.PaymentInfos = new List<WorkTimePaymentInformationDto>();
                                var totalAmount = 0M;
                                foreach (var pInfo in input.Payments)
                                {
                                    wS.PaymentInfos.Add(new WorkTimePaymentInformationDto()
                                    {
                                        Actual = pInfo.Actual,
                                        Entered = pInfo.Entered,
                                        PaymentType = pInfo.PaymentTypeId
                                    });

                                    totalAmount += pInfo.Actual;
                                }

                                period.TotalSales = totalAmount;
                            }

                            period.WorkPeriodInformations = JsonConvert.SerializeObject(shiftDto);

                        }
                        await _workperiodRepo.UpdateAsync(period);
                        output.WorkPeriodId = period.Id;

                        bool sendEmail = input.TenantId>0 ? _settingManager.GetSettingValueForTenant<bool>(AppSettings.ConnectSettings.SendEmailOnWorkDayClose, input.TenantId)
                            : _settingManager.GetSettingValue<bool>(AppSettings.ConnectSettings.SendEmailOnWorkDayClose);
                        if (period.LocationId > 0 && sendEmail)
                        {
                            LocationListDto location = await _locService.GetLocationById(new EntityDto()
                            {
                                Id = period.LocationId
                            });

                            if (location != null)
                            {
                               


                                if (!string.IsNullOrEmpty(location.Email))
                                {
                                    var mySubject = "Day End at " + location.Name +" / Total Sales : " + period.TotalSales.ToString("F");
                                    var emailContents = "";
                                    try
                                    {

                                        emailContents = "Start Time: " + period.StartTime.ToString(GetDateTimeFormat()) +
                                                            " <b>To</b> " + "End Time: " +
                                                            period.EndTime.ToString(GetDateTimeFormat()) +
                                                            "<br/> <h4> Total Sales: " + period.TotalSales.ToString("F") +"</h4><br/>"+
                                                            "<span> For Detail Report, Please use DineConnect or DineLive </span>";


                                        await _bgm.EnqueueAsync<SendEmailJob, ReceiptEmailArgs>(new ReceiptEmailArgs
                                        {
                                            Email = location.Email,
                                            EmailType = "WORKPERIOD",
                                            EmailSubject = mySubject,
                                            EmailContents = emailContents
                                        });

                                    }
                                    catch (Exception)
                                    {
                                        await _emailSender.SendAsync(location.Email, mySubject,emailContents);
                                    }
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception exception)
            {
                _logger.Fatal("---Error in End WorkPeriod--");
                _logger.Fatal("Exception ", exception);
                _logger.Fatal("Input " + input.WorkPeriodId);
                _logger.Fatal("---Error in End WorkPeriod--");

            }
            return output;
        }

        public async Task<WorkPeriodOutput> GetSummaryReport(WorkPeriodSummaryInput input)
        {
            WorkPeriodOutput reOutput = new WorkPeriodOutput();
            List<WorkPeriodSummaryOutput> output = new List<WorkPeriodSummaryOutput>();
            var allW = _workperiodRepo.GetAll();

            var locations = new List<int>();

            if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                && !input.LocationGroup.Locations.Any())
            {
                locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                 locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                 locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
            }
            else if (input.LocationGroup == null && input.UserId>0)
            {
                locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
            }


            if (locations.Any())
                allW = allW.Where(a => locations.Contains(a.LocationId));

            var nextDay = input.EndDate.AddDays(1);
            allW = allW.Where(
                                 a =>
                                     DbFunctions.TruncateTime(a.StartTime) >=
                                     DbFunctions.TruncateTime(input.StartDate)
                                     &&
                                     DbFunctions.TruncateTime(a.EndTime) <
                                     DbFunctions.TruncateTime(nextDay));
            decimal total = 0M;
            decimal totalTickets = 0M;
            decimal averVal = 0M;

            foreach (var workPeriod in allW)
            {
                WorkPeriodSummaryOutput sout;
                if (output.Any(a => a.LocationId == workPeriod.LocationId))
                {
                    sout = output.FirstOrDefault(a => a.LocationId == workPeriod.LocationId);
                    if (sout != null)
                    {
                        sout.Total += workPeriod.TotalSales;
                        sout.TotalTicketCount += workPeriod.TotalTicketCount;
                        if (sout.ReportEndDay < workPeriod.EndTime)
                            sout.ReportEndDay = workPeriod.EndTime;

                        if (!string.IsNullOrEmpty(workPeriod.WorkPeriodInformations))
                        {
                            var allShifts =
                                JsonConvert.DeserializeObject<List<WorkShiftDto>>(workPeriod.WorkPeriodInformations);
                            if (!string.IsNullOrEmpty(sout.DayInformation))
                            {
                                var soutShifts = JsonConvert.DeserializeObject<List<WorkShiftDto>>(sout.DayInformation);
                                soutShifts.AddRange(allShifts);
                                sout.DayInformation = JsonConvert.SerializeObject(soutShifts);
                            }

                            if (allShifts.Any())
                            {
                                sout.Actual += allShifts.SelectMany(a => a.PaymentInfos).Sum(a => a.Actual);
                                sout.Entered += allShifts.SelectMany(a => a.PaymentInfos).Sum(a => a.Entered);
                            }
                        }
                    }
                }
                else
                {
                    sout = new WorkPeriodSummaryOutput();
                    Master.Location loc = _lRepo.Get(workPeriod.LocationId);
                    if (loc != null)
                    {
                        sout.ReportStartDay = workPeriod.StartTime;
                        sout.LocationName = loc.Name;
                        sout.Wid = workPeriod.Id;
                        sout.ReportEndDay = workPeriod.EndTime;
                        sout.LocationCode = loc.Code;
                        sout.LocationCode = loc.Code;
                        sout.LocationId = loc.Id;
                        sout.DayInformation = workPeriod.WorkPeriodInformations;
                        sout.Total = workPeriod.TotalSales;
                        sout.TotalTicketCount = workPeriod.TotalTicketCount;

                        var allShifts = JsonConvert.DeserializeObject<List<WorkShiftDto>>(workPeriod.WorkPeriodInformations);
                        if (allShifts.Any())
                        {
                            sout.Actual = allShifts.SelectMany(a => a.PaymentInfos).Sum(a => a.Actual);
                            sout.Entered = allShifts.SelectMany(a => a.PaymentInfos).Sum(a => a.Entered);
                        }

                        output.Add(sout);
                    }
                }
                var allPay = JsonConvert.DeserializeObject<List<WorkShiftDto>>(workPeriod.WorkPeriodInformations);
                var groupPayments = allPay.SelectMany(a => a.PaymentInfos).GroupBy(a => a.PaymentType);

                foreach (var pType in groupPayments)
                {
                    var totalValue = pType.Sum(a => a.Entered);
                    if (totalValue > 0M)
                    {
                        var pt = _pRepo.Get(pType.Key);
                        if (pt != null)
                        {
                            var keyThere = reOutput.OtherPayments.ContainsKey(pt.Name);
                            if (keyThere)
                            {
                                var getObje = reOutput.OtherPayments[pt.Name];
                                getObje += totalValue;
                                reOutput.OtherPayments[pt.Name] = getObje;
                            }
                            else
                            {
                                reOutput.OtherPayments.Add(pt.Name, totalValue);
                            }
                        }
                    }
                }
            }

            totalTickets = output.Sum(a => a.TotalTicketCount);
            total = output.Sum(a => a.Total);

            if (total > 0M && totalTickets > 0M)
                averVal = total / totalTickets;

            if (output.Any())
                output = output.OrderBy(a => a.LocationCode).ToList();

            reOutput.Periods = output;
            reOutput.DashBoard.TotalSales = total;
            reOutput.DashBoard.TotalTickets = totalTickets;
            reOutput.DashBoard.Average = averVal;

            if (reOutput.Periods.Any())
            {
                var myLo = reOutput.Periods.OrderByDescending(a => a.ReportEndDay).First();
                reOutput.LastLocationClosed = myLo.LocationCode;
                reOutput.TimeOfClosed = myLo.ReportEndDay;
            }
            return reOutput;
        }

        public async Task<WorkPeriodOutput> GetSummaryReportOnTickets(WorkPeriodSummaryInput input)
        {
            WorkPeriodOutput output = new WorkPeriodOutput();
            GetTicketInput tInput = new GetTicketInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate
            };
            List<SimpleLocationDto> allLocs = input.Locations.Select(loc => new SimpleLocationDto
            {
                Id = loc.Id,
                Name = loc.Name
            }).ToList();

            tInput.Locations = allLocs;
            var allTickets = _connReportService.GetAllTickets(tInput);

            foreach (var allT in allTickets.GroupBy(a => a.LocationId))
            {
                WorkPeriodSummaryOutput sout = new WorkPeriodSummaryOutput();
                Master.Location loc = _lRepo.Get(allT.Key);
                if (loc != null)
                {
                    sout.LocationName = loc.Name;
                    sout.LocationCode = loc.Code;
                    sout.LocationId = loc.Id;
                    sout.Total = allT.Sum(a => a.TotalAmount);
                    sout.TotalTicketCount = allT.Count();
                    output.Periods.Add(sout);
                }
            }
            return output;
        }

        public async Task<List<WorkPeriodDetailOutput>> GetDetailSummaryByDate(WorkPeriodSummaryInput input)
        {
            var pOutput = await _ptService.ApiGetAll(new ApiPaymentTypeInput());

            List<WorkPeriodDetailOutput> reOutput = new List<WorkPeriodDetailOutput>();
            var allW = _workperiodRepo.GetAll();
            var locations = new List<int>();
            if (input.Locations != null && input.Locations.Any())
            {
                locations = input.Locations.Select(a => a.Id).ToList();
            }
            if (locations.Any())
                allW = allW.Where(a => locations.Contains(a.LocationId));
            var nextDay = input.EndDate.AddDays(1);
            allW = allW.Where(
                                 a =>
                                     DbFunctions.TruncateTime(a.StartTime) >=
                                     DbFunctions.TruncateTime(input.StartDate)
                                     &&
                                     DbFunctions.TruncateTime(a.EndTime) <
                                     DbFunctions.TruncateTime(nextDay));

            decimal totalAmount = allW.Sum(a => a.TotalSales);

            foreach (var workPeriod in allW)
            {
                Master.Location loc = null;
                using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                {
                    loc = _lRepo.Get(workPeriod.LocationId);
                }
                if (loc != null && !string.IsNullOrEmpty(workPeriod.WorkPeriodInformations))
                {
                    WorkPeriodDetailOutput output = reOutput.SingleOrDefault(a => a.LocationId == loc.Id
                    && a.WorkDay == workPeriod.StartTime.Date);

                    if (output == null)
                    {
                        output = new WorkPeriodDetailOutput();
                        reOutput.Add(output);
                    }

                    var shifts = JsonConvert.DeserializeObject<List<WorkShiftDto>>(workPeriod.WorkPeriodInformations);
                    output.LocationId = loc.Id;
                    output.LocationCode = loc.Code;
                    output.LocationName = loc.Name;
                    output.WorkDay = workPeriod.StartTime.Date;
                    output.WorkDayId = workPeriod.Id;
                    output.TerminalCode = loc.Code;
                    output.Cashier = workPeriod.StartUser;
                    output.CreateUser = workPeriod.StartUser;
                    output.CreateDate = workPeriod.StartTime;
                    output.ModifyUser = workPeriod.EndUser;
                    output.ModifyDate = workPeriod.EndTime;
                    output.Ecr += workPeriod.TotalSales;
                    output.Collection += shifts.SelectMany(a => a.PaymentInfos).Sum(a => a.Entered);
                    output.GST = output.Ecr * 7 / 107;
                    output.TotalAmount += shifts.SelectMany(a => a.PaymentInfos).Sum(a => a.Actual);
                    output.Total += workPeriod.TotalSales;

                    output.PaymentInfo = new List<EndPeriodPaymentInformation>();

                    foreach (var allShift in shifts.SelectMany(a => a.PaymentInfos))
                    {
                        var getInfo = output.PaymentInfo.SingleOrDefault(a => a.PaymentTypeId == allShift.PaymentType);
                        if (getInfo == null)
                        {
                            getInfo = new EndPeriodPaymentInformation()
                            {
                                PaymentTypeId = allShift.PaymentType,
                                Actual = allShift.Actual,
                                Entered = allShift.Entered
                            };
                            var apiPaymentTypeOutput = pOutput.Items.SingleOrDefault(a => a.Id == allShift.PaymentType);
                            if (apiPaymentTypeOutput != null)
                                getInfo.Name += apiPaymentTypeOutput.Name;
                            output.PaymentInfo.Add(getInfo);
                        }
                        else
                        {
                            getInfo.Actual += allShift.Actual;
                            getInfo.Entered += allShift.Entered;
                        }
                    }
                }
            }

            decimal rTotalOutput = reOutput.Sum(a => a.Total);

            return reOutput;
        }

        public async Task<List<WorkDayDetailSummaryOutput>> GetDetailSummaryByDateByTickets(WorkPeriodSummaryInput input)
        {
            List<WorkDayDetailSummaryOutput> output = new List<WorkDayDetailSummaryOutput>();
            GetTicketInput tInput = new GetTicketInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate
            };
            List<SimpleLocationDto> allLocs = input.Locations.Select(loc => new SimpleLocationDto
            {
                Id = loc.Id,
                Name = loc.Name
            }).ToList();

            tInput.Locations = allLocs;
            var allTickets = _connReportService.GetAllTickets(tInput);

            var groupTickets =
                allTickets.GroupBy(ra => new { ra.LastPaymentTime.Year, ra.LastPaymentTime.Month, ra.LastPaymentTime.Day });

            foreach (var allT in groupTickets)
            {
                WorkDayDetailSummaryOutput myO = new WorkDayDetailSummaryOutput
                {
                    Date = new DateTime(allT.Key.Year, allT.Key.Month, allT.Key.Day).ToShortDateString(),
                    Total = allT.Sum(a => a.TotalAmount),
                    Tax = allT.Sum(a => a.TotalAmount) * 7 / 107,
                    Discount = 0,
                    TotalTickets = allT.Count()
                };
                myO.Sales = myO.Total - myO.Tax;
                output.Add(myO);
            }
            return output;
        }

        public async Task<List<WorkDayDetailSummaryOutput>> GetWeekDaySales(WorkPeriodSummaryInput input)
        {
            var tenantId = input.TenantId;

            var output = new List<WorkDayDetailSummaryOutput>();
            string weekDay = tenantId != null ? _settingManager.GetSettingValueForTenant(AppSettings.ConnectSettings.WeekDay, tenantId.Value)
                : _settingManager.GetSettingValue(AppSettings.ConnectSettings.WeekDay);
            if (string.IsNullOrEmpty(weekDay))
                return null;

            List<ComboboxItemDto> alLDays = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(weekDay);

            if (alLDays.Any())
            {
                GetTicketInput tInput = new GetTicketInput
                {
                    StartDate = input.StartDate,
                    EndDate = input.EndDate,
                    Locations = input.Locations.Select(loc => new SimpleLocationDto { Id = loc.Id, Name = loc.Name }).ToList()
                };

                var allTickets = _connReportService.GetAllTickets(tInput);

                DateTime firstSunday = new DateTime(1753, 1, 7);
                foreach (var day in alLDays)
                {
                    var myValue = Convert.ToInt32(day.Value);
                    var myTickets = allTickets.Where(a => EntityFunctions.DiffDays(firstSunday, DbFunctions.TruncateTime(a.LastPaymentTime)) % 7 == myValue);
                    myValue = myValue == 7 ? 0 : myValue;
                    var myO = new WorkDayDetailSummaryOutput
                    {
                        Date = day.DisplayText,
                        Discount = 0,
                    };

                    if (myTickets.Any())
                    {
                        myO.Total = myTickets.Sum(a => a.TotalAmount);
                        myO.Tax = myTickets.Sum(a => a.TotalAmount) * 7 / 107;
                        myO.Discount = (await myTickets.Where(t => t.TicketPromotionDetails != null).ToListAsync())
                         .SelectMany(a => JsonConvert.DeserializeObject<List<PromotionDetailValue>>(a.TicketPromotionDetails))
                         .Sum(e => e.PromotionAmount);
                        myO.TotalTickets = myTickets.Count();
                        myO.Sales = myO.Total - myO.Tax;
                    }
                    output.Add(myO);
                }
            }
            return output;
        }

        public async Task<List<WorkDayDetailSummaryOutput>> GetWeekEndSales(WorkPeriodSummaryInput input)
        {
            var tenantId = input.TenantId;

            var output = new List<WorkDayDetailSummaryOutput>();
            string weekEnd = tenantId != null ? _settingManager.GetSettingValueForTenant(AppSettings.ConnectSettings.WeekEnd, tenantId.Value)
                : _settingManager.GetSettingValue(AppSettings.ConnectSettings.WeekEnd);
            if (string.IsNullOrEmpty(weekEnd))
                return null;

            List<ComboboxItemDto> alLDays = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(weekEnd);
            if (alLDays.Any())
            {
                GetTicketInput tInput = new GetTicketInput
                {
                    StartDate = input.StartDate,
                    EndDate = input.EndDate
                };
                List<SimpleLocationDto> allLocs = input.Locations
                    .Select(loc => new SimpleLocationDto
                    {
                        Id = loc.Id,
                        Name = loc.Name
                    }).ToList();

                tInput.Locations = allLocs;

                var allTickets = _connReportService.GetAllTickets(tInput);

                DateTime firstSunday = new DateTime(1753, 1, 7);
                foreach (var day in alLDays)
                {
                    var myValue = Convert.ToInt32(day.Value);

                    if (myValue == 7)
                        myValue = 0;
                    var myTickets = allTickets.Where(a => EntityFunctions.DiffDays(firstSunday, DbFunctions.TruncateTime(a.LastPaymentTime)) % 7 == myValue);

                    var myO = new WorkDayDetailSummaryOutput
                    {
                        Date = day.DisplayText,
                        Discount = 0,
                    };

                    if (myTickets.Any())
                    {
                        myO.Total = myTickets.Sum(a => a.TotalAmount);
                        myO.Tax = myTickets.Sum(a => a.TotalAmount) * 7 / 107;
                        myO.Discount = (await myTickets.Where(t => t.TicketPromotionDetails != null).ToListAsync())
                            .SelectMany(a => JsonConvert.DeserializeObject<List<PromotionDetailValue>>(a.TicketPromotionDetails))
                            .Sum(e => e.PromotionAmount);
                        myO.TotalTickets = myTickets.Count();
                        myO.Sales = myO.Total - myO.Tax;
                    }
                    output.Add(myO);
                }
            }
            return output;
        }

        public async Task<List<WorkDayDetailSummaryOutput>> GetScheduleSales(WorkPeriodSummaryInput input)
        {
            List<WorkDayDetailSummaryOutput> output = new List<WorkDayDetailSummaryOutput>();

            var schedules = _settingManager.GetSettingValue(AppSettings.ConnectSettings.Schedules);
            if (string.IsNullOrEmpty(schedules))
                return null;

            List<ScheduleListDto> alLDays = JsonConvert.DeserializeObject<List<ScheduleListDto>>(schedules);
            if (alLDays.Any())
            {
                GetTicketInput tInput = new GetTicketInput
                {
                    StartDate = input.StartDate,
                    EndDate = input.EndDate
                };
                var allTickets =
                 _connReportService.GetAllTickets(tInput);

                foreach (var allD in alLDays)
                {
                    var endH = allD.EndHour - 1;

                    var myTickets = allTickets.Where(
                        a =>
                            a.LastPaymentTime.Hour >= allD.StartHour &&
                            a.LastPaymentTime.Hour <= endH);

                    if (myTickets.Any())
                    {
                        WorkDayDetailSummaryOutput myO = new WorkDayDetailSummaryOutput
                        {
                            Date = allD.Name,
                            Total = myTickets.Sum(a => a.TotalAmount),
                            Tax = myTickets.Sum(a => a.TotalAmount) * 7 / 107,
                            Discount = 0,
                            TotalTickets = myTickets.Count()
                        };
                        myO.Sales = myO.Total - myO.Tax;
                        output.Add(myO);
                    }
                }
            }
            return output;
        }

        public async Task<List<WorkPeriodSummaryOutput>> GetWorkPeriods(WorkPeriodSummaryInput input)
        {
            List<WorkPeriodSummaryOutput> output = new List<WorkPeriodSummaryOutput>();
            var allW = _workperiodRepo.GetAll();

            var locations = new List<int>();
            if (input.Locations != null && input.Locations.Any())
            {
                locations = input.Locations.Select(a => a.Id).ToList();
            }
            if (locations.Any())
                allW = allW.Where(a => locations.Contains(a.LocationId));
            var nextDay = input.EndDate.AddDays(1);
            allW = allW.Where(
                                 a =>
                                     DbFunctions.TruncateTime(a.StartTime) >=
                                     DbFunctions.TruncateTime(input.StartDate)
                                     &&
                                     DbFunctions.TruncateTime(a.EndTime) <
                                     DbFunctions.TruncateTime(nextDay));

            foreach (var workPeriod in allW)
            {
                WorkPeriodSummaryOutput sout = new WorkPeriodSummaryOutput();
                Master.Location loc = _lRepo.Get(workPeriod.LocationId);
                if (loc != null)
                {
                    sout.ReportStartDay = workPeriod.StartTime;
                    sout.ReportEndDay = workPeriod.EndTime;
                    sout.LocationName = loc.Name;
                    sout.Wid = workPeriod.Id;
                    sout.LocationCode = loc.Code;
                    sout.LocationCode = loc.Code;
                    sout.LocationId = loc.Id;
                    sout.DayInformation = workPeriod.WorkPeriodInformations;
                    sout.Total = workPeriod.TotalSales;
                    sout.TotalTicketCount = workPeriod.TotalTicketCount;

                    var allShifts = JsonConvert.DeserializeObject<List<WorkShiftDto>>(workPeriod.WorkPeriodInformations);
                    if (allShifts.Any())
                    {
                        sout.Actual = allShifts.SelectMany(a => a.PaymentInfos).Sum(a => a.Actual);
                        sout.Entered = allShifts.SelectMany(a => a.PaymentInfos).Sum(a => a.Entered);
                    }
                    output.Add(sout);
                }
            }

            if (output.Any())
                output = output.OrderBy(a => a.LocationId).ToList();

            return output;
        }

        public async Task<PrintOutIn40Cols> GetSummaryPrint(WorkPeriodSummaryInput input)
        {
            var salesSummary = await GetSummaryReport(input);
            PrintOutIn40Cols output = new PrintOutIn40Cols();
            TextFileWriter fp = new TextFileWriter();
            string subPath = "~/Report/";
            bool exists = System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(subPath));

            if (!exists)
                System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(subPath));

            string fileName =
                System.Web.HttpContext.Current.Server.MapPath("~/Report/" + AbpSession.UserId + "_DaySummary.txt");
            int PageLenth = 38;
            var com = await _cRepo.FirstOrDefaultAsync(a => a.Id > 0);
            var myName = com.Name;

            if (input.LocationGroup.Group)
            {
                myName = input.LocationGroup.Groups.First().Name;
            }
            if (salesSummary?.Periods != null && salesSummary.Periods.Any() && com != null)
            {
                var allItems = salesSummary.Periods;
                fp.FileOpen(fileName, "O");
                fp.PrintTextLine(fp.PrintFormatCenter(myName, PageLenth));
                fp.PrintTextLine(fp.PrintFormatCenter(L("DaySummary").ToUpper(), PageLenth));
                fp.PrintTextLine(fp.PrintFormatCenter(input.StartDate.ToString("dd-MM-yyyy") + " - " + input.EndDate.ToString("dd-MM-yyyy"), PageLenth));
                fp.PrintTextLine("");
                fp.PrintTextLine(fp.PrintFormatCenter("Time".ToUpper() + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), PageLenth));
                fp.PrintSepLine('-', PageLenth);
                fp.PrintTextLine(fp.PrintFormatCenter("Last Closed".ToUpper() + " : " + salesSummary.LastLocationClosed, PageLenth));
                fp.PrintTextLine(fp.PrintFormatCenter("Last Time".ToUpper() + " : " + salesSummary.TimeOfClosed.ToString("dd-MM-yyyy HH:mm:ss"), PageLenth));
                fp.PrintSepLine('-', PageLenth);

                fp.PrintText(fp.PrintFormatLeft("LOCATION", 10));
                fp.PrintText(fp.PrintFormatRight("ACTUAL", 9));
                fp.PrintText(fp.PrintFormatRight("ENTERED", 9));
                fp.PrintText(fp.PrintFormatRight("DIFF", 9));
                fp.PrintTextLine("");
                fp.PrintSepLine('-', PageLenth);

                foreach (var dateVa in allItems)
                {
                    fp.PrintText(fp.PrintFormatLeft(dateVa.LocationName.Length>10?dateVa.LocationName.Substring(0, 10):dateVa.LocationName, 11));
                    fp.PrintText(fp.PrintFormatRight(dateVa.Actual.ToString("###0.00"), 9));
                    fp.PrintText(fp.PrintFormatRight(dateVa.Entered.ToString("###0.00"), 9));
                    fp.PrintText(fp.PrintFormatRight(dateVa.Difference.ToString("###0.00"), 9));
                    fp.PrintTextLine("");
                }
                fp.PrintTextLine("");
                fp.PrintSepLine('-', PageLenth);
                fp.PrintText(fp.PrintFormatLeft("TOTAL", 11));
                fp.PrintText(fp.PrintFormatRight(allItems.Sum(a => a.Actual).ToString("###0.00"), 9));
                fp.PrintText(fp.PrintFormatRight(allItems.Sum(a => a.Entered).ToString("###0.00"), 9));
                fp.PrintText(fp.PrintFormatRight(allItems.Sum(a => a.Difference).ToString("###0.00"), 9));
                fp.PrintTextLine("");
                fp.PrintSepLine('-', PageLenth);
                fp.PrintTextLine("");
            }
            if (salesSummary != null && salesSummary.OtherPayments != null && salesSummary.OtherPayments.Keys.Any())
            {
                foreach (var myKey in salesSummary.OtherPayments.Keys)
                {
                    fp.PrintText(fp.PrintFormatLeft(myKey, 7));
                    fp.PrintText(fp.PrintFormatRight(salesSummary.OtherPayments[myKey].ToString("###0.00"), 10));
                    fp.PrintTextLine("");
                }
            }
            fp.PrintSepLine('-', PageLenth);
            fp.PrintTextLine("");
            fp.PrintTextLine("");

            string retText = System.IO.File.ReadAllText(fileName);
            retText = retText.Replace("\r\n", "<br/>");
            retText = retText.Replace(" ", "&nbsp;");

            output.BodyText = retText;
            output.BoldBodyText = "<STRONG>" + retText + "</STRONG>";
            return output;
        }

        public async Task<List<WorkPeriodDetailOutput>> GetDetailSummary(WorkPeriodSummaryInput input)
        {

            List<WorkPeriodDetailOutput> reOutput = new List<WorkPeriodDetailOutput>();
            var allW = _workperiodRepo.GetAll();
            var locations = new List<int>();
            if (input.Locations != null && input.Locations.Any())
            {
                locations = input.Locations.Select(a => a.Id).ToList();
            }
            if (locations.Any())
                allW = allW.Where(a => locations.Contains(a.LocationId));
            var nextDay = input.EndDate.AddDays(1);
            allW = allW.Where(
                                 a =>
                                     DbFunctions.TruncateTime(a.StartTime) >=
                                     DbFunctions.TruncateTime(input.StartDate)
                                     &&
                                     DbFunctions.TruncateTime(a.EndTime) <
                                     DbFunctions.TruncateTime(nextDay));

            foreach (var workPeriod in allW)
            {
                Master.Location loc = null;
                using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                {
                    loc = await _lRepo.GetAsync(workPeriod.LocationId);
                }
                if (loc != null && !string.IsNullOrEmpty(workPeriod.WorkPeriodInformations))
                {
                    WorkPeriodDetailOutput output = reOutput.SingleOrDefault(a => a.LocationId == loc.Id);

                    if (output == null)
                    {
                        output = new WorkPeriodDetailOutput();
                        reOutput.Add(output);
                    }

                    var shifts = JsonConvert.DeserializeObject<List<WorkShiftDto>>(workPeriod.WorkPeriodInformations);
                    output.LocationId = loc.Id;
                    output.LocationCode = loc.Code;
                    output.LocationName = loc.Name;
                    output.WorkDay = workPeriod.StartTime.Date;
                    output.WorkDayId = workPeriod.Id;
                    output.TerminalCode = loc.Code;
                    output.Cashier = workPeriod.StartUser;
                    output.CreateUser = workPeriod.StartUser;
                    output.CreateDate = workPeriod.StartTime;
                    output.ModifyUser = workPeriod.EndUser;
                    output.ModifyDate = workPeriod.EndTime;
                    output.Ecr += workPeriod.TotalSales;
                    output.Collection += shifts.SelectMany(a => a.PaymentInfos).Sum(a => a.Entered);
                    output.GST = output.Ecr * 7 / 107;
                    output.TotalAmount += shifts.SelectMany(a => a.PaymentInfos).Sum(a => a.Actual);

                    output.PaymentInfo = new List<EndPeriodPaymentInformation>();

                    foreach (var allShift in shifts.SelectMany(a => a.PaymentInfos))
                    {
                        var getInfo = output.PaymentInfo.SingleOrDefault(a => a.PaymentTypeId == allShift.PaymentType);
                        if (getInfo == null)
                        {
                            getInfo = new EndPeriodPaymentInformation()
                            {
                                PaymentTypeId = allShift.PaymentType,
                                Actual = allShift.Actual,
                                Entered = allShift.Entered
                            };
                            var apiPaymentTypeOutput = await _pRepo.GetAsync(allShift.PaymentType);
                            if (apiPaymentTypeOutput != null)
                                getInfo.Name += apiPaymentTypeOutput.Name;
                            output.PaymentInfo.Add(getInfo);
                        }
                        else
                        {
                            getInfo.Actual += allShift.Actual;
                            getInfo.Entered += allShift.Entered;
                        }
                    }
                }
            }
            return reOutput;
        }

        public async Task<FileDto> GetSummaryExcel(WorkPeriodSummaryInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new Report.Dtos.CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.DAYSUMMARY,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                    });
                    if (backGroundId > 0)
                    {
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.DAYSUMMARY,
                            WorkPeriodSummaryInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                        });
                    }
                }
                else
                {
                    if (input.ExportOutputType == ExportType.Excel)
                        return await _wpe.ExportWorkPeriodSummary(input, this);
                    else
                        return await _wpoe.ExportWorkPeriodSummary(input, this);
                }
            }
            return null;
        }

        public async Task<FileDto> GetSummaryOutput(WorkPeriodSummaryInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new Report.Dtos.CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.DAYSUMMARYEXPORT,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                    });
                    if (backGroundId > 0)
                    {
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.DAYSUMMARYEXPORT,
                            WorkPeriodSummaryInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                        });
                    }
                }
                else
                {
                    return await _wpoe.ExportSummaryOutput(input, this, _connReportService);
                }
            }
            return null;
        }

        public async Task<FileDto> GetWeekDaySalesOutput(WorkPeriodSummaryInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new Report.Dtos.CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.WEEKDAYSALES,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                    });
                    if (backGroundId > 0)
                    {
                        var workPeriod = new WorkPeriodSummaryInput();
                        workPeriod = input;
                        workPeriod.TenantId = AbpSession.TenantId.Value;
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.WEEKDAYSALES,
                            WorkPeriodSummaryInput = workPeriod,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                        });
                    }
                }
                else
                {
                    return await _wpoe.ExportWeekDayOutput(input, this, _connReportService);
                }
            }
            return null;
        }

        public async Task<FileDto> GetWeekEndSalesOutput(WorkPeriodSummaryInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new Report.Dtos.CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.WEEKENDSALES,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                    });
                    if (backGroundId > 0)
                    {
                        var workPeriod = new WorkPeriodSummaryInput();
                        workPeriod = input;
                        workPeriod.TenantId = AbpSession.TenantId.Value;
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.WEEKENDSALES,
                            WorkPeriodSummaryInput = workPeriod,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                        });
                    }
                }
                else
                {
                    return await _wpoe.ExportWeekEndOutput(input, this, _connReportService);
                }
            }
            return null;
        }

        public async Task<FileDto> GetScheduleSalesOutput(WorkPeriodSummaryInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new Report.Dtos.CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.SCHEDULESALES,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                    });
                    if (backGroundId > 0)
                    {
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.SCHEDULESALES,
                            WorkPeriodSummaryInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                        });
                    }
                }
                else
                {
                    return await _wpoe.ExportScheduleSalesOutput(input, this, _connReportService);
                }
            }
            return null;
        }

        public Task<int> SyncWorkPeriod(PostInput postInput)
        {
            throw new NotImplementedException();
        }

        public async Task<FileDto> GetDetailExcel(WorkPeriodSummaryInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new Report.Dtos.CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.DAYDETAILS,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                    });
                    if (backGroundId > 0)
                    {
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.DAYDETAILS,
                            WorkPeriodSummaryInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                        });
                    }
                }
                else
                {
                    if (input.ExportOutputType == ExportType.Excel)
                        return await _wpe.ExportWorkPeriodDetails(input, this);
                    else
                        return await _wpoe.ExportWorkPeriodDetails(input, this);
                }
            }
            return null;
        }

        protected virtual async Task UpdateWorkPeriod(CreateOrUpdateWorkPeriodInput input)
        {
            var item = await _workperiodRepo.GetAsync(input.WorkPeriod.Id.Value);
            var dto = input.WorkPeriod;

            item.StartUser = dto.StartUser;
            item.EndUser = dto.EndUser;
            
            item.StartTime = dto.StartTime;
            item.EndTime = dto.EndTime;
            item.LocationId = dto.LocationId;
            item.Wid = dto.Wid;
            item.TotalSales = dto.TotalSales;
            item.TotalTicketCount = dto.TotalTicketCount;
            item.TenantId = dto.TenantId;
            item.WorkPeriodInformations = dto.WorkPeriodInformations;
            item.DepartmentTicketInformations = dto.DepartmentTicketInformations;
            item.AutoClosed = dto.AutoClosed;
            item.TotalTaxes = dto.TotalTaxes;
            CheckErrors(await _workperiodManager.CreateSync(item));
        }

        protected virtual async Task<int> CreateWorkPeriod(CreateOrUpdateWorkPeriodInput input)
        {
            try
            {
                var dto = input.WorkPeriod.MapTo<Period.WorkPeriod>();
                var iResult = await _workperiodRepo.InsertAndGetIdAsync(dto);
                return iResult;
            }
            catch (Exception exception)
            {
                _logger.Fatal("---Error in Create WorkPeriod---");
                _logger.Fatal("Exception ", exception);
                _logger.Fatal("Location " + input.WorkPeriod.LocationId);
                _logger.Fatal("---End Error in Create WorkPeriod---");
            }
            return 0;
        }
    }
}