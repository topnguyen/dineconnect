﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Connect.WorkPeriod.Implementation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.WorkPeriod
{
    public interface IWorkPeriodExporter 
    {
        Task<FileDto> ExportWorkPeriodSummary(WorkPeriodSummaryInput input, WorkPeriodAppService wpService);
        Task<FileDto> ExportWorkPeriodDetails(WorkPeriodSummaryInput input, WorkPeriodAppService wpService);

    }
}