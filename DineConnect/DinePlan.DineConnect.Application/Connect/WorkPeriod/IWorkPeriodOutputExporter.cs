﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Connect.WorkPeriod.Implementation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.WorkPeriod
{
    public interface IWorkPeriodOutputExporter
    {
        Task<FileDto> ExportSummaryOutput(WorkPeriodSummaryInput input, WorkPeriodAppService workPeriodAppService, IConnectReportAppService connReportService);
        Task<FileDto> ExportWorkPeriodSummary(WorkPeriodSummaryInput input, WorkPeriodAppService workPeriodAppService);
        Task<FileDto> ExportWorkPeriodDetails(WorkPeriodSummaryInput input, WorkPeriodAppService workPeriodAppService);
        Task<FileDto> ExportWeekDayOutput(WorkPeriodSummaryInput input, WorkPeriodAppService workPeriodAppService, IConnectReportAppService connReportService);
        Task<FileDto> ExportWeekEndOutput(WorkPeriodSummaryInput input, WorkPeriodAppService workPeriodAppService, IConnectReportAppService connReportService);
        Task<FileDto> ExportScheduleSalesOutput(WorkPeriodSummaryInput input, WorkPeriodAppService workPeriodAppService, IConnectReportAppService connReportService);
    }
}