﻿using System;
using System.Collections.Generic;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using Abp.Configuration;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.User.Dtos;
using DinePlan.DineConnect.DataExporting.Base;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter.Util;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;

namespace DinePlan.DineConnect.Connect.Log.Exporter
{
    public class PlanLoggerExporter : FileExporterBase, IPlanLoggerExporter
    {
        private readonly SettingManager _settingManager;
        private int _roundDecimals = 2;
        private readonly string dateFormat = "dd-MM-yyyy HH:ss:mm";
        public PlanLoggerExporter(SettingManager settingManager)
        {
            _settingManager = settingManager;
            _roundDecimals = _settingManager.GetSettingValue<int>(AppSettings.ConnectSettings.Decimals);
            dateFormat = _settingManager.GetSettingValue(AppSettings.ConnectSettings.DateTimeFormat);
        }
        public async Task<FileDto> ExportLogs(IPlanLoggerAppService logService, GetPlanLoggerInput input)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(L("Logs"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString("dd/MM/yyyy")
                : DateTime.Now.ToString("dd/MM/yyyy"));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString("dd/MM/yyyy")
                : DateTime.Now.ToString("dd/MM/yyyy"));
            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var package = new ExcelPackage())
            {
                var sheet = package.Workbook.Worksheets.Add(L("Tickets"));
                sheet.OutLineApplyStyle = true;
                var headers = new List<string>
                {
                    L("EventTime"),
                    L("EventName"),
                    L("UserName"),
                    L("TicketNumber"),
                    L("TicketTotal"),
                    L("Location"),
                    L("EventLog"),
                };
                AddHeader(
                    sheet, true,
                    headers.ToArray()
                );

                var secondTime = false;
                int i = 0;
                var rowCount = 2;

                while (true)
                {
                    input.MaxResultCount = 10000;
                    if (secondTime)
                    {
                        input.NotCorrectDate = true;
                    }
                    var outPut = await logService.GetAll(input);
                    if (!secondTime)
                    {
                        secondTime = true;
                    }
                    if (outPut.Items.Any())
                    {
                        for (i = 0; i < outPut.Items.Count; i++)
                        {
                            var colCount = 1;
                            sheet.Cells[i + rowCount, colCount++].Value = outPut.Items[i].EventTime.ToString(dateFormat);
                            sheet.Cells[i + rowCount, colCount++].Value = outPut.Items[i].EventName;
                            sheet.Cells[i + rowCount, colCount++].Value = outPut.Items[i].UserName;
                            sheet.Cells[i + rowCount, colCount++].Value = outPut.Items[i].TicketNo;
                            sheet.Cells[i + rowCount, colCount++].Value = outPut.Items[i].TicketTotal;
                            sheet.Cells[i + rowCount, colCount++].Value = outPut.Items[i].LocationName;
                            sheet.Cells[i + rowCount, colCount++].Value = outPut.Items[i].EventLog;
                        }

                        input.SkipCount = input.SkipCount + input.MaxResultCount;
                    }
                    else
                    {
                        break;
                    }
                }
                Save(package, file);
            }
            return ProcessFile(input, file);
        }
    }
}