﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Castle.Core.Logging;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.User.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace DinePlan.DineConnect.Connect.Log.Implementation
{
    public class PlanLoggerAppService : DineConnectAppServiceBase, IPlanLoggerAppService
    {
        private readonly ILogger _logger;
        private readonly IRepository<Master.Location> _lService;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ILocationAppService _locService;
        private readonly IPlanLoggerExporter _exporter;

        private readonly IRepository<PlanLogger> _planLogger;

        public PlanLoggerAppService(IRepository<PlanLogger> planLogger,
            ILocationAppService locService, IPlanLoggerExporter exporter,
            ILogger logger, IRepository<Master.Location> lService,
            IReportBackgroundAppService rbas, IBackgroundJobManager bgm,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _planLogger = planLogger;
            _logger = logger;
            _lService = lService;
            _rbas = rbas;
            _bgm = bgm;
            _unitOfWorkManager = unitOfWorkManager;
            _locService = locService;
            _exporter = exporter;
        }

        public async Task<CreateOrUpdatePlanLoggerOutput> CreateOrUpdateLog(CreateOrUpdatePlanLoggerInput input)
        {
            var output = new CreateOrUpdatePlanLoggerOutput { LogId = 1 };
            var logCode = 0;

            if (input.PlanLogger.TicketTotal > 10000000M)
            {
                input.PlanLogger.TicketTotal = 0M;
            }
  
            logCode = input.PlanLogger.Id.HasValue && input.PlanLogger.Id.Value > 0
                ? await UpdateLogger(input.PlanLogger)
                : await CreateLogger(input.PlanLogger);
            output.LogId = logCode;
            return output;
        }

        public async Task<PagedResultOutput<PlanLoggerListDto>> GetAll(GetPlanLoggerInput input)
        {
            var correctDate = CorrectInputDate(input);
            var loggers = _planLogger.GetAll();
            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                if (correctDate)
                {
                    loggers =
                        loggers.Where(
                            a =>
                                a.EventTime >=
                                input.StartDate
                                &&
                                a.EventTime <=
                                input.EndDate);
                }
                else
                {
                    loggers =
                        loggers.Where(
                            a =>
                                DbFunctions.TruncateTime(a.EventTime) >=
                                DbFunctions.TruncateTime(input.StartDate)
                                &&
                                DbFunctions.TruncateTime(a.EventTime) <=
                                DbFunctions.TruncateTime(input.EndDate));
                }
            }
            if (input.Location > 0)
            {
                loggers = loggers.Where(a => a.LocationId == input.Location);
            }
            else if (input.Locations != null && input.Locations.Any())
            {
                var locations = input.Locations.Select(a => a.Id).ToList();
                loggers = loggers.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false,
                    UserId = input.UserId
                });
                loggers = loggers.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false,
                    UserId = input.UserId
                });
                loggers = loggers.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true,
                    UserId = input.UserId
                });
                loggers = loggers.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup == null)
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = new List<Master.Dtos.SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
                loggers = loggers.Where(a => locations.Contains(a.LocationId));
            }

            var ticketNumbers = new List<string>();
            if (input.Unfinished)
            {
                loggers = loggers.Where(a => !string.IsNullOrEmpty(a.TicketNo) && !a.TicketNo.Equals("0"));
                var allGroup = loggers.GroupBy(a => a.TicketNo);

                foreach (var tGroup in allGroup)
                {
                    if (!tGroup.Any(a => a.EventName.Equals("PAYMENT")))
                    {
                        ticketNumbers.Add(tGroup.Key);
                    }
                }
                if (ticketNumbers.Any())
                {
                    loggers = loggers.Where(a => ticketNumbers.Contains(a.TicketNo));
                }
            }
            else if (input.Events != null && input.Events.Any())
            {
                var events = input.Events.Select(a => a.DisplayText).ToList();
                loggers = loggers.Where(a => events.Contains(a.EventName));
            }

            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                {
                    loggers = loggers.BuildQuery(filRule);
                }
            }

            var sortLoggers = await loggers.OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortLoggers.MapTo<List<PlanLoggerListDto>>();

            var allItemCount = loggers.Count();

            return new PagedResultOutput<PlanLoggerListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<TimeLineOutput> GetTimelineLogs(NullableIdInput input)
        {
            List<EventOutput> output = new List<EventOutput>();
            List<NameValueDto> nvDots = new List<NameValueDto>();

            TimeLineOutput returnOutput = new TimeLineOutput
            {
                EventOutputs = output,
                Descriptions = nvDots
            };

            if (input.Id.HasValue && input.Id.Value > 0)
            {
                var indLog = await _planLogger.GetAsync(input.Id.Value);

                if (!string.IsNullOrEmpty(indLog.EventLog))
                {
                    var allString = indLog.EventLog.Split(';');
                    if (allString.Length > 0)
                    {
                        nvDots.AddRange(from allStr in allString
                                        select allStr.Split('=')
                            into valPair
                                        where valPair.Length > 1
                                        select new NameValueDto
                                        {
                                            Name = valPair[0],
                                            Value = valPair[1]
                                        });
                    }
                }
                if (!string.IsNullOrEmpty(indLog?.TicketNo))
                {
                    var allLogs = _planLogger.GetAll().
                        Where(a => a.TicketNo.Equals(indLog.TicketNo) && a.LocationId.Equals(indLog.LocationId));
                    if (allLogs.Any())
                    {
                        output.AddRange(allLogs.OrderBy(a => a.EventTime).Select(pll => new EventOutput()
                        {
                            Content = "Total : " + pll.TicketTotal + "  " + pll.EventLog,
                            Title = pll.EventName,
                            When = pll.EventTime.ToString(),
                            BadgeClass = "info",
                            BadgeIconClass = "glyphicon-check",
                        }));
                    }
                }
            }
            return returnOutput;
        }

        public async Task<FileDto> GetExcel(GetPlanLoggerInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new Report.Dtos.CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.LOGGER,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                    {
                        var planLoggerInput = input;
                        planLoggerInput.UserId = AbpSession.UserId.Value;

                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.LOGGER,
                            PlanLoggerInput = planLoggerInput,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    return await _exporter.ExportLogs(this, input);
                }
            }
            return null;
        }

        private async Task<int> CreateLogger(PlanLoggerEditDto input)
        {

            var logg = input.MapTo<PlanLogger>();
            try
            {
                var inputId = await _planLogger.InsertAndGetIdAsync(logg);
                return inputId;
            }
            catch (Exception exception)
            {
                _logger.Fatal("LOGG SYNC FAILED");
                _logger.Fatal("-------------------");
                _logger.Fatal("LOCATION  : " + input.LocationId);
                _logger.Fatal("EXCEPTION : " + exception);

                _logger.Fatal("Error in Create Log " + exception.Message);
                _logger.Fatal(JsonConvert.SerializeObject(input));
                _logger.Fatal("----End-----");
            }
            return 0;
        }

        private async Task<int> UpdateLogger(PlanLoggerEditDto input)
        {
            if (input.Id == null)
                return 0;

            if (input.Id == 0)
                return 0;

            try
            {
                var logger = _planLogger.Get(input.Id.Value);
                if (logger != null)
                {
                    logger.Terminal = input.Terminal;
                    logger.TicketNo = input.TicketNo;
                    logger.TicketTotal = input.TicketTotal;
                    logger.UserName = input.UserName;
                    logger.EventLog = input.EventLog;
                    logger.EventName = input.EventName;
                    logger.EventTime = input.EventTime;
                    logger.TenantId = input.TenantId;
                    logger.LocationId = input.LocationId;
                    await _planLogger.UpdateAsync(logger);
                }
            }
            catch (Exception exception)
            {
                _logger.Fatal("Error in Update Log " + exception.Message);
                _logger.Fatal(JsonConvert.SerializeObject(input));
                _logger.Fatal("----End-----");


            }
            return input.Id.Value;
        }

        private bool CorrectInputDate(IDateInput input)
        {
            if (input.NotCorrectDate)
            {
                return true;
            }
            var returnStatus = true;

            var operateHours = 0M;

            if (input.Locations != null && input.Locations.Any() && input.Locations.Count() == 1)
            {
                operateHours = _lService.Get(input.Locations.First().Id).ExtendedBusinessHours;
            }

            if (operateHours == 0M && input.Location > 0)
            {
                operateHours = _lService.Get(input.Location).ExtendedBusinessHours;
            }

            if (operateHours == 0M)
            {
                operateHours = SettingManager.GetSettingValue<decimal>(AppSettings.ConnectSettings.OperateHours);
            }

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                input.EndDate = input.EndDate.AddDays(1);
                input.StartDate = input.StartDate.AddHours(Convert.ToDouble(operateHours));
                input.EndDate = input.EndDate.AddHours(Convert.ToDouble(operateHours)).AddMinutes(-1);
            }
            else
            {
                returnStatus = false;
            }
            return returnStatus;
        }
    }
}