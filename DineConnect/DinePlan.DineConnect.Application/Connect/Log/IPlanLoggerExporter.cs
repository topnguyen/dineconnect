﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.User.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Log
{
    public interface IPlanLoggerExporter
    {
        Task<FileDto> ExportLogs(IPlanLoggerAppService logService, GetPlanLoggerInput input);
    }
}