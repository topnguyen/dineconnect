﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.User.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Log
{
    public interface IPlanLoggerAppService : IApplicationService
    {
        Task<CreateOrUpdatePlanLoggerOutput> CreateOrUpdateLog(CreateOrUpdatePlanLoggerInput input);
        Task<PagedResultOutput<PlanLoggerListDto>> GetAll(GetPlanLoggerInput inputDto);
        Task<TimeLineOutput> GetTimelineLogs(NullableIdInput input);
        Task<FileDto> GetExcel(GetPlanLoggerInput inputDto);
    }
}
