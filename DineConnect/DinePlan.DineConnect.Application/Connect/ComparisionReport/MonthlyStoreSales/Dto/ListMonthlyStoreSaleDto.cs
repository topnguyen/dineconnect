﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.Dto
{
	public class ListMonthlyStoreSaleDto
	{
		public DateTime Month { get; set; }

		public string MonthToString
		{
			get
			{
				return Month.ToString("MM/yyyy");
			}
		}

		public List<ListDateByLocation> ListDataByLocation { get; set; }

		public ListMonthlyStoreSaleDto()
		{
			ListDataByLocation = new List<ListDateByLocation>();
		}
	}

	public class ListDateByLocation
	{
		public string LocationCode { get; set; }
		public int LocationId { get; set; }
		public decimal TotalAmountByLocationCurrrentYear
		{
			get
			{
				if(ListDataByDepartment.Count > 0)
				{
					return ListDataByDepartment.Sum(x => x.TotalAmount);
				} else
				{
					return 0;
				}
			}
		}
		public decimal TotalAmountByLocation1YearAgo { get; set; }
		public decimal TotalAmountByLocation2YearAgo { get; set; }

		public decimal PercentWith1YearAgo
		{
			get
			{
				if (TotalAmountByLocation1YearAgo == 0)
				{
					return 100;
				}
				else
				{
					return (TotalAmountByLocationCurrrentYear / TotalAmountByLocation1YearAgo) * 100;
				}
			}
		}

		public decimal PercentWith2YearAgo
		{
			get
			{
				if (TotalAmountByLocation2YearAgo == 0)
				{
					return 100;
				}
				else
				{
					return (TotalAmountByLocationCurrrentYear / TotalAmountByLocation2YearAgo) * 100;
				}
			}
		}


		public List<ListDataByDepartment> ListDataByDepartment { get; set; }

		public ListDateByLocation()
		{
			ListDataByDepartment = new List<ListDataByDepartment>();
		}

	}

	public class ListDataByDepartment
	{
		public DateTime DateTime { get; set; }
		public string LocationCode { get; set; }
		public int LocationId { get; set; }
		public string Department { get; set; }
		public decimal TotalAmount { get; set; }
	}
}
