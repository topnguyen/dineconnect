﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.Dto
{
	public class StartAndEndMonthlyDto
	{
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
	}
}
