﻿using Abp.Domain.Repositories;
using Abp.Extensions;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.CashSummaryReport.Dto;
using DinePlan.DineConnect.Connect.ComparisionReport.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.Exporting
{
	public class MonthlySalesReportExporter : FileExporterBase, IMonthlySalesReportExporter
	{
		private readonly IRepository<Master.Department> _departmentRepository;
		public MonthlySalesReportExporter(IRepository<Master.Department> departmentRepository)
		{
			_departmentRepository = departmentRepository;
		}
		public async Task<FileDto> ExportMonthlyStoreSaleReport(GetMonthlyStoreSaleReportInput input, IMonthlySalesReportAppService appService)
		{
			var builder = new StringBuilder();
			builder.Append(L("MonthlySalesReport"));
			builder.Append(L("-"));
			builder.Append(!input.StartDate.Equals(DateTime.MinValue)
				? input.StartDate.ToString(_simpleDateFormat)
				: DateTime.Now.ToString(_simpleDateFormat));
			builder.Append("_");
			builder.Append(!input.EndDate.Equals(DateTime.MinValue)
				? input.EndDate.ToString(_simpleDateFormat)
				: DateTime.Now.ToString(_simpleDateFormat));

			var file = new FileDto(builder + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
			if (input.ExportOutputType == 0)
			{
				using (var excelPackage = new ExcelPackage())
				{
					await GetMonthlySoteSaleReportExportExcel(excelPackage, input, appService);
					Save(excelPackage, file);
				}
				return ProcessFile(input, file);
			}
			else
			{
				var contentHTML = await ExportDatatableToHtml(input, appService);
				var result = ProcessFileHTML(file, contentHTML);
				return result;
			}
		}
		private async Task<string> ExportDatatableToHtml(GetMonthlyStoreSaleReportInput input, IMonthlySalesReportAppService appService)
		{
			var dtos = await appService.GetMonthlyStoreSalesReports(input);
			var deparments = await _departmentRepository.GetAll().OrderBy(x => x.SortOrder).Select(x => x.Name).ToListAsync();
			StringBuilder strHTMLBuilder = new StringBuilder();
			strHTMLBuilder.Append("<html>");
			strHTMLBuilder.Append("<head>");
			strHTMLBuilder.Append("</head>");

			strHTMLBuilder.Append(@"<style> 
             body  { 
              font:400 12px 'Tahoma';
              font-size: 12px;
              padding:10px;
            }
            table  { 
                margin-top: 10px;
                border-collapse: collapse;
                width: 100%;
                margin-bottom: 10px;
            }

            table td, table th {
                border: 1px solid #ddd;
                padding: 8px;
            }
            table th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                color: white;
            }
            thead {
                display: table-header-group;
            }
            tfoot {
                display: table-row-group;
            }
            tr{
                page-break-inside: avoid;
            }
            blockquote {
              color:white;
              text-align:center;
            }

            h3{
              text-align:center;
              text-transform: capitalize;
              font-size: medium;
            }

			 .thead_title {
				background-color: #deebf7;
				font-weight: bold;
			}

			.thead_content {
				background-color: #fff2cc;
			}

			.footer_item {
				background-color: #d6dce5;
				font-weight: bold;
			}

			.footer {
				background-color: #e2f0d9;
				font-weight: bold;
			}

			.margin_bottom_0 {
				margin-bottom: 0 !important;
			}

			.border_bottom_none {
				border-bottom: none !important;
			}

			.custom_table_date {
				margin-bottom: 0px !important;
			}

			.display_flex {
				display: flex;
			}

			.item {
				max-width: 116px;
			}

			.w-100 {
				width: 100%;
			}


			.float-right {
				float: right;
			}

			.thead_title {
				background-color: #008000;
				font-weight: bold;
				color: white;
			}

			.thead_content {
				background-color: #808080;
				color: white;
			}

			.footer_item {
				background-color: #d6dce5;
				font-weight: bold;
			}

			.footer_str {
				display: flex;
				padding: 10px;
			}

			.footer {
				background-color: #e2f0d9;
				font-weight: bold;
			}

			.color_header {
				background-color: #808080;
				color: white;
			}

			.font_weight_bold {
				font-weight: bold;
			}

			.nodata {
				background: navajowhite;
				color: red;
			}

			.table_Item {
				border: none !important;
				margin: 0px !important;
				padding: 0px !important;
			}

			
            .text-center{
			  text-align: center;           
			}
            </style>");

			strHTMLBuilder.Append("<body>");

			foreach (var dataByMonth in dtos)
			{
				strHTMLBuilder.Append("<div>");
				strHTMLBuilder.Append("<table class='table table-bordered table-condensed'>");
				strHTMLBuilder.Append("<thead> <tr class='thead_title'>");
				strHTMLBuilder.Append($"<th colspan = '22' class='border_bottom_none font_weight_bold'>{dataByMonth.MonthToString}</th>  ");
				strHTMLBuilder.Append($"<tr class='thead_content'>");
				strHTMLBuilder.Append($"<th>{L("Location")}</th>");
				foreach (var department in deparments)
				{
					strHTMLBuilder.Append($"<th class='text-center font_weight_bold'>{department}</th>");
				}
				strHTMLBuilder.Append($"<th class='text-center '>{L("TotalActual")}  { dataByMonth.Month.Year}</th>");
				strHTMLBuilder.Append($"<th class='text-center '>{L("TotalActual")} { dataByMonth.Month.Year -1}</th>");
				strHTMLBuilder.Append($"<th class='text-center '>{L("TotalActual")}  { dataByMonth.Month.Year - 2}</th>");
				strHTMLBuilder.Append($"<th class='text-center '> {L("Actual")}  { dataByMonth.Month.Year}  vs  {L("Actual")}  {dataByMonth.Month.Year - 1} (%)</th>");
				strHTMLBuilder.Append($"<th class='text-center '> {L("Actual")}  { dataByMonth.Month.Year}  vs  {L("Actual")}  {dataByMonth.Month.Year - 2} (%)</th>");
				strHTMLBuilder.Append("</tr>");

				// content
				foreach (var dataByLocation in dataByMonth.ListDataByLocation)
				{
					strHTMLBuilder.Append($"<tr>");
					strHTMLBuilder.Append($"<td class='item border_bottom_none text-center '>{dataByLocation.LocationCode}</td>");
					foreach (var department in deparments)
					{
						strHTMLBuilder.Append($"<td class='item border_bottom_none text-center '>{GetValue(department, dataByLocation.ListDataByDepartment).ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
					}
					strHTMLBuilder.Append($"<td class='item border_bottom_none text-center '>{dataByLocation.TotalAmountByLocationCurrrentYear.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
					strHTMLBuilder.Append($"<td class='item border_bottom_none text-center '>{dataByLocation.TotalAmountByLocation1YearAgo.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
					strHTMLBuilder.Append($"<td class='item border_bottom_none text-center '>{dataByLocation.TotalAmountByLocation2YearAgo.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
					strHTMLBuilder.Append($"<td class='item border_bottom_none text-center '>{dataByLocation.PercentWith1YearAgo} %</td>");
					strHTMLBuilder.Append($"<td class='item border_bottom_none text-center '>{dataByLocation.PercentWith2YearAgo} %</td>");
					strHTMLBuilder.Append("</tr>");
				}
				//Close tags.  
				strHTMLBuilder.Append("</table>");
			}

			strHTMLBuilder.Append("</body>");
			strHTMLBuilder.Append("</html>");

			string Htmltext = strHTMLBuilder.ToString();
			return Htmltext;
		}

		private FileDto ProcessFileHTML(FileDto file, string fileHTML)
		{
			File.WriteAllText(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken), fileHTML);
			file.FileType = MimeTypeNames.ApplicationXhtmlXml;
			file.FileName = Path.GetFileNameWithoutExtension(file.FileName) + ".html";
			return file;
		}

		private async Task GetMonthlySoteSaleReportExportExcel(ExcelPackage package, GetMonthlyStoreSaleReportInput input, IMonthlySalesReportAppService appService)
		{
			var sheet = package.Workbook.Worksheets.Add(L("MonthlySales"));
			sheet.OutLineApplyStyle = true;
			var deparments = await _departmentRepository.GetAll().OrderBy(x => x.SortOrder).Select(x => x.Name).ToListAsync();
			var monthlySoterSales = new List<ListMonthlyStoreSaleDto>();
			if (input.ExportBy == true)
			{
				 monthlySoterSales = await appService.GetMonthlyStoreSalesReports(input);
			}
			else
			{
				 monthlySoterSales = await appService.GetMonthlyStoreSalesReportsByLocationGroup(input);
			}

			if (monthlySoterSales != null)
			{
				int rowCount = 2;
				foreach (var dataByMonth in monthlySoterSales)
				{
					sheet.Cells[rowCount, 1].Value = dataByMonth.MonthToString;
					rowCount++;
					// add header
					var headersEnd = new List<string>
					{
						L("TotalActual") + " " + dataByMonth.Month.Year,
						L("TotalActual") + " " + (dataByMonth.Month.Year - 1),
						L("TotalActual") + " " + (dataByMonth.Month.Year - 2),
						L("Actual") + " " + dataByMonth.Month.Year + " vs " + L("Actual") + " " +  (dataByMonth.Month.Year - 1) + "(%)" ,
						L("Actual") + " " + dataByMonth.Month.Year + " vs " + L("Actual") + " " +(dataByMonth.Month.Year - 2) + "(%)" ,
					};
					var headers = deparments.Concat(headersEnd);
					var index = 2;
					foreach (var item in headers)
					{
						sheet.Cells[rowCount, index].Value = item;
						index++;
					}

					rowCount++;
					// content
					foreach (var dataByLocation in dataByMonth.ListDataByLocation)
					{
						sheet.Cells[rowCount, 1].Value = dataByLocation.LocationCode;
						var cell = 2;
						foreach (var deparment in deparments)
						{
							sheet.Cells[rowCount, cell].Value = GetValue(deparment, dataByLocation.ListDataByDepartment);
							cell++;
						}
						sheet.Cells[rowCount, cell].Value = dataByLocation.TotalAmountByLocationCurrrentYear.ToString(ConnectConsts.ConnectConsts.NumberFormat);
						cell++;
						sheet.Cells[rowCount, cell].Value = dataByLocation.TotalAmountByLocation1YearAgo.ToString(ConnectConsts.ConnectConsts.NumberFormat);
						cell++;
						sheet.Cells[rowCount, cell].Value = dataByLocation.TotalAmountByLocation2YearAgo.ToString(ConnectConsts.ConnectConsts.NumberFormat);
						cell++;
						sheet.Cells[rowCount, cell].Value = dataByLocation.PercentWith1YearAgo + "%";
						cell++;
						sheet.Cells[rowCount, cell].Value = dataByLocation.PercentWith2YearAgo + "%";
						rowCount++;
					}
					rowCount++;
				}
				rowCount++;
				for (var i = 1; i <= 22; i++)
				{
					sheet.Column(i).AutoFit();
				}
			}
		}
		private decimal GetValue(string department, List<ListDataByDepartment> list)
		{
			var result = 0M;
			var index = list.FindIndex(x => x.Department == department);
			if (index >= 0)
			{
				result = list[index].TotalAmount;
			}
			return result;
		}
	}
}