﻿using DinePlan.DineConnect.Connect.ComparisionReport;
using DinePlan.DineConnect.Connect.ComparisionReport.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.Exporting
{
    public interface IMonthlySalesReportExporter
    {
        Task<FileDto> ExportMonthlyStoreSaleReport(GetMonthlyStoreSaleReportInput input, IMonthlySalesReportAppService appService);
    }
}