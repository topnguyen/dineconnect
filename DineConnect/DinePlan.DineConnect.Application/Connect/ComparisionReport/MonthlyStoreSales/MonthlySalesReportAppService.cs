﻿using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Connect.CashSummaryReport.Exporting;
using DinePlan.DineConnect.Connect.ComparisionReport.Dto;
using DinePlan.DineConnect.Connect.ComparisionReport.Exporting;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Setting;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport
{
	public class MonthlySalesReportAppService : DineConnectAppServiceBase, IMonthlySalesReportAppService
	{
		private readonly IRepository<ProgramSettingTemplate> _programSettingTemplateRepository;
		private readonly IRepository<Master.Department> _dRepo;
		private readonly ILocationAppService _locService;
		private readonly IRepository<Master.Location> _lRepo;
		private readonly IRepository<Transaction.Ticket> _ticketManager;
		private readonly IRepository<Transaction.Order> _orderManager;
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private readonly IReportBackgroundAppService _rbas;
		private readonly IBackgroundJobManager _bgm;
		private readonly IMonthlySalesReportExporter _exporter;
		private readonly IRepository<Period.WorkPeriod> _workPeriod;
		private readonly IRepository<PaymentType> _payRe;
		private readonly IConnectReportAppService _connectReportAppService;
		private readonly IRepository<Master.LocationGroup> _locationGroupRepository;

		public MonthlySalesReportAppService(
			IRepository<Transaction.Ticket> ticketManager,
			ILocationAppService locService,
			IRepository<Company> comR,
			IUnitOfWorkManager unitOfWorkManager,
			IRepository<Master.Department> drepo,
			IRepository<Master.Location> lRepo,
			IRepository<Transaction.Order> orderManager,
			IReportBackgroundAppService rbas,
			IBackgroundJobManager bgm,
			IMonthlySalesReportExporter exporter,
			IRepository<Period.WorkPeriod> workPeriod,
			IRepository<PaymentType> payRe,
			IConnectReportAppService connectReportAppService,
			IRepository<ProgramSettingTemplate> programSettingTemplateRepository ,
			IRepository<Master.LocationGroup> locationGroupRepository
			)
		{
			_ticketManager = ticketManager;
			_programSettingTemplateRepository = programSettingTemplateRepository;
			_unitOfWorkManager = unitOfWorkManager;
			_dRepo = drepo;
			_locService = locService;
			_lRepo = lRepo;
			_orderManager = orderManager;
			_rbas = rbas;
			_bgm = bgm;
			_exporter = exporter;
			_workPeriod = workPeriod;
			_payRe = payRe;
			_connectReportAppService = connectReportAppService;
			_locationGroupRepository = locationGroupRepository;
		}
		public async Task<List<ListMonthlyStoreSaleDto>> GetMonthlyStoreSalesReports(GetMonthlyStoreSaleReportInput input)
		{
			var result = new List<ListMonthlyStoreSaleDto>();
			var startDate = input.StartDate.Date;
			while (startDate <= input.EndDate)
			{
				var dataByLocation = new ListMonthlyStoreSaleDto();
				var ticketsByMonth = GetTicketByMonth(input, startDate).Include(x => x.Location);
				var ticketByMonth_GroupLocation_Deparmnt = ticketsByMonth.GroupBy(x => new { x.DepartmentName, x.Location.Code, x.Location.Id }).Select(x => new ListDataByDepartment
				{
					DateTime = x.Select(a => a.LastPaymentTime).FirstOrDefault(),
					Department = x.Key.DepartmentName,
					LocationCode = x.Key.Code,
					LocationId = x.Key.Id,
					TotalAmount = x.Sum(t => t.TotalAmount)
				});

				var ticketByMonth_GroupLocation = ticketByMonth_GroupLocation_Deparmnt.GroupBy(x => new { x.LocationCode, x.LocationId }).Select(x => new ListDateByLocation
				{
					LocationCode = x.Key.LocationCode,
					LocationId = x.Key.LocationId,
					ListDataByDepartment = x.ToList()
				}).ToList();

				foreach (var item in ticketByMonth_GroupLocation)
				{
					item.TotalAmountByLocation1YearAgo = await CaculatorTotalAmountByLocationAndDate(item.LocationId, startDate.AddYears(-1));
					item.TotalAmountByLocation2YearAgo = await CaculatorTotalAmountByLocationAndDate(item.LocationId, startDate.AddYears(-2));
				}
				dataByLocation.Month = startDate;
				dataByLocation.ListDataByLocation = ticketByMonth_GroupLocation.ToList();
				result.Add(dataByLocation);
				startDate = startDate.AddMonths(1);
			}
			return await Task.FromResult(result);
		}

		public async Task<List<ListMonthlyStoreSaleDto>> GetMonthlyStoreSalesReportsByLocationGroup(GetMonthlyStoreSaleReportInput input)
		{
			var result = new List<ListMonthlyStoreSaleDto>();
			var startDate = input.StartDate.Date;
			while (startDate <= input.EndDate)
			{
				var dataByLocation = new ListMonthlyStoreSaleDto();
				var ticketsByMonth = await GetTicketByMonth(input, startDate).Include(x => x.Location.LocationGroups).ToListAsync();
				var ticketsByLocationGroup = ticketsByMonth.SelectMany(t => t.Location.LocationGroups.Select(g => new { LocationGroupId = g.Id, LocationGroupCode = g.Code,  Ticket = t })).ToList();
				var ticketByMonth_GroupLocation_Deparmnt = ticketsByLocationGroup.GroupBy(x => new { x.Ticket.DepartmentName, x.LocationGroupCode, x.LocationGroupId }).Select(x => new ListDataByDepartment
				{
					DateTime = x.Select(a => a.Ticket.CreationTime).FirstOrDefault(),
					Department = x.Key.DepartmentName,
					LocationCode = x.Key.LocationGroupCode,
					LocationId = x.Key.LocationGroupId,
					TotalAmount = x.Sum(t => t.Ticket.TotalAmount)
				});

				var ticketByMonth_GroupLocation = ticketByMonth_GroupLocation_Deparmnt.GroupBy(x => new { x.LocationCode, x.LocationId }).Select(x => new ListDateByLocation
				{
					LocationCode = x.Key.LocationCode,
					LocationId = x.Key.LocationId,
					ListDataByDepartment = x.ToList()
				});

				foreach (var item in ticketByMonth_GroupLocation.ToList())
				{
					item.TotalAmountByLocation1YearAgo = await CaculatorTotalAmountByLocationAndDate(item.LocationId, startDate.AddYears(-1));
					item.TotalAmountByLocation2YearAgo = await CaculatorTotalAmountByLocationAndDate(item.LocationId, startDate.AddYears(-2));
				}
				dataByLocation.Month = startDate;
				dataByLocation.ListDataByLocation = ticketByMonth_GroupLocation.ToList();
				result.Add(dataByLocation);
				startDate = startDate.AddMonths(1);
			}
			return await Task.FromResult(result);
		}

		public IQueryable<Transaction.Ticket> GetTicketByMonth(GetMonthlyStoreSaleReportInput input, DateTime monthInput)
		{

			IQueryable<Transaction.Ticket> output;
			var dataInMonth = GetStartAndEndMonth(monthInput);
			using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
			{
				output = _ticketManager.GetAll().Where(
								a =>
									a.LastPaymentTime >= dataInMonth.StartDate
									&&
									a.LastPaymentTime <= dataInMonth.EndDate);
				if (input.Location > 0)
				{
					output = output.Where(a => a.LocationId == input.Location);
				}
				else if (input.Locations != null && input.Locations.Any())
				{
					var locations = input.Locations.Select(a => a.Id).ToList();
					output = output.Where(a => locations.Contains(a.LocationId));
				}
				else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
						 && !input.LocationGroup.Locations.Any())
				{
					var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
					{
						Locations = input.LocationGroup.Locations,
						Group = false
					});
					if (locations.Any()) output = output.Where(a => locations.Contains(a.LocationId));
				}
				else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
				{
					var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
					{
						Locations = input.LocationGroup.Locations,
						Group = false
					});
					output = output.Where(a => locations.Contains(a.LocationId));
				}
				else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
				{
					List<int> locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
					{
						Groups = input.LocationGroup.Groups,
						Group = true
					});
					output = output.Where(a => locations.Contains(a.LocationId));
				}
				if (input.LocationGroup != null)
				{
					if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
					{
						var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
						output = output.Where(a => !nonlocations.Contains(a.LocationId));
					}
				}

				else if (input.LocationGroup == null)
				{
					var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
					{
						Locations = new List<SimpleLocationDto>(),
						Group = false,
						UserId = input.UserId
					});
					if (input.UserId > 0)
						output = output.Where(a => locations.Contains(a.LocationId));
				}
				output = output.Where(x => !x.PreOrder && !x.Credit);
				return output;
			}
		}

		public async Task<decimal> CaculatorTotalAmountByLocationAndDate(int locationID, DateTime monthInput)
		{
			var dataInMonth = GetStartAndEndMonth(monthInput);
			var output = 0M;
			var tickets = await _ticketManager.GetAll().Where(
								   a => a.LocationId == locationID &&
								   a.LastPaymentTime >= dataInMonth.StartDate &&
								   a.LastPaymentTime <= dataInMonth.EndDate).ToListAsync();
			if (tickets.Count > 0)
			{
				output = tickets.Sum(x => x.TotalAmount);
			}
			return output;
		}


		private StartAndEndMonthlyDto GetStartAndEndMonth(DateTime dateTime)
		{
			var startDate = new DateTime(dateTime.Year, dateTime.Month, 1);
			var endDate = startDate.AddMonths(1).AddDays(-1);

			return new StartAndEndMonthlyDto()
			{
				StartDate = startDate,
				EndDate = endDate
			};
		}

		public async Task<FileDto> GetMonthlyStoreSalesReportToExport(GetMonthlyStoreSaleReportInput input)
		{
			if (AbpSession.UserId != null && AbpSession.TenantId != null)
			{
				input.MaxResultCount = 10000;

				if (input.RunInBackground)
				{
					var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
					{
						ReportName = ReportNames.MONTHLYSTORESALE,
						Completed = false,
						UserId = AbpSession.UserId.Value,
						TenantId = AbpSession.TenantId.Value,
						ReportDescription = input.ReportDescription
					});

					if (backGroundId > 0)
					{
						var tInput = new MonthlyStoreSaleReportInput
						{
							GetMonthlyStoreSaleReportInput = input,
						};
						await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
						{
							BackGroundId = backGroundId,
							ReportName = ReportNames.MONTHLYSTORESALE,
							MonthlyStoreSaleReportInput = tInput,
							UserId = AbpSession.UserId.Value,
							TenantId = AbpSession.TenantId.Value
						});
					}
				}
				else
				{
					var output = await _exporter.ExportMonthlyStoreSaleReport(input, this);
					return output;
				}
			}
			return null;
		}

	}

}