﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.ComparisionReport.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport
{
	public interface IMonthlySalesReportAppService : IApplicationService
	{
		#region Monthly Sales
		Task<List<ListMonthlyStoreSaleDto>> GetMonthlyStoreSalesReports(GetMonthlyStoreSaleReportInput input);
		Task<FileDto> GetMonthlyStoreSalesReportToExport(GetMonthlyStoreSaleReportInput input);
		Task<List<ListMonthlyStoreSaleDto>> GetMonthlyStoreSalesReportsByLocationGroup(GetMonthlyStoreSaleReportInput input);
		#endregion
	}
}
