﻿using DinePlan.DineConnect.Connect.ComparisionReport.DailySalesBreakdown.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.DailySalesBreakdown.Exporting
{
    public interface IDailySalesBreakdownReportExporter
    {
        Task<FileDto> ExportDaylySalesReport(GetTicketInput input, IDailySalesBreakdownReportAppService appService);
    }
}
