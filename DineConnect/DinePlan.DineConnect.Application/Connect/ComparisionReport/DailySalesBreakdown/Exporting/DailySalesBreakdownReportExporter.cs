﻿using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.ComparisionReport.DailySalesBreakdown.Dto;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.DailySalesBreakdown.Exporting
{
    public class DailySalesBreakdownReportExporter : FileExporterBase, IDailySalesBreakdownReportExporter
    {
        private IRepository<Master.Company> _comRepo;
        private IRepository<Master.Location> _locRepo;
        private readonly SettingManager _settingManager;
        public DailySalesBreakdownReportExporter(IRepository<Master.Location> loRepository, IRepository<Company> comRepository, SettingManager settingManager)
        {
            _locRepo = loRepository;
            _comRepo = comRepository;
            _settingManager = settingManager;
            _roundDecimals = _settingManager.GetSettingValue<int>(AppSettings.ConnectSettings.Decimals);
        }
        public async Task<FileDto> ExportDaylySalesReport(GetTicketInput input, IDailySalesBreakdownReportAppService appService)
        {
            var file = new FileDto("DailySalesBreak" + input.StartDate.ToString("yyyy-MM-dd") + "_" + input.EndDate.ToString("yyyy-MM-dd") + ".xlsx", MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Daily"));
                sheet.OutLineApplyStyle = true;
                var output = await appService.GetDailySalesReports(input);
                int rowCount = AddReportHeader(sheet, L("DailySalesBreak"), input);
                var shiftCount = output.ListShift.Count();
                var header = new List<string>{ L("Date"), L("Day") };
                header.AddRange(output.ListShift.Select(s => s.Name));
                header.AddRange(new List<string> { L("TotalSales"), L("Compliment"), L("Promotion"), L("TotalDeduction"), L("NetRest"), L("ServiceCharge"), L("GST"), L("GrossSales"), L("NoOfGuest") });
                //Header
                sheet.Cells[rowCount, 1].Value = L("Restaurant").ToUpper();
                sheet.Cells[rowCount, 1, rowCount, shiftCount + 11].Merge = true;
                sheet.Cells[rowCount, 1].Style.Font.Bold = true;
                sheet.Cells[rowCount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet.Cells[rowCount, 1, rowCount, shiftCount + 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                rowCount++;
                sheet.Cells[rowCount, 1].Value ="";
                sheet.Cells[rowCount, 1, rowCount, 2].Merge = true;
                sheet.Cells[rowCount, 1, rowCount, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                sheet.Cells[rowCount, 3].Value = L("Sales");
                sheet.Cells[rowCount, 3, rowCount, shiftCount + 3].Merge = true;
                sheet.Cells[rowCount, 3].Style.Font.Bold = true;
                sheet.Cells[rowCount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet.Cells[rowCount, 3, rowCount, shiftCount + 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                sheet.Cells[rowCount, shiftCount + 4].Value = L("Deduction");
                sheet.Cells[rowCount, shiftCount + 4, rowCount, shiftCount + 6].Merge = true;
                sheet.Cells[rowCount, shiftCount + 4].Style.Font.Bold = true;
                sheet.Cells[rowCount, shiftCount + 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet.Cells[rowCount, shiftCount + 4, rowCount, shiftCount + 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                sheet.Cells[rowCount, shiftCount + 7, rowCount, shiftCount + 11].Merge = true;
                sheet.Cells[rowCount, shiftCount + 7, rowCount, shiftCount + 11].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                var row = rowCount++;
                row++;
                var col = 1;
                foreach(var head in header)
                {
                    sheet.Cells[row, col].Value = head;
                    sheet.Cells[row, col].Style.Font.Bold = true;
                    sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    col++;
                }
                row++;col = 1;
                int count = 1;
                foreach(var item in output.Items)
                {
                    sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    sheet.Cells[row, col++].Value = count;
                    sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheet.Cells[row, col++].Value = item.Date;
                    sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    foreach (var shift in item.ListShift)
                    {
                        sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                        sheet.Cells[row, col++].Value = Math.Round(shift.ShiftAmount, _roundDecimals, MidpointRounding.AwayFromZero);
                        sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    }
                    sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                    sheet.Cells[row, col++].Value = Math.Round(item.TotalSales, _roundDecimals, MidpointRounding.AwayFromZero);

                    sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                    sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    sheet.Cells[row, col++].Value = Math.Round(item.Compliment, _roundDecimals, MidpointRounding.AwayFromZero);

                    sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                    sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    sheet.Cells[row, col++].Value = Math.Round(item.Promotion, _roundDecimals, MidpointRounding.AwayFromZero);

                    sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                    sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    sheet.Cells[row, col++].Value = Math.Round(item.TotalDeduction, _roundDecimals, MidpointRounding.AwayFromZero);

                    sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                    sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    sheet.Cells[row, col++].Value = Math.Round(item.NetRest, _roundDecimals, MidpointRounding.AwayFromZero);

                    sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                    sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    sheet.Cells[row, col++].Value = Math.Round(item.ServiceCharge, _roundDecimals, MidpointRounding.AwayFromZero);

                    sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                    sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    sheet.Cells[row, col++].Value = Math.Round(item.GST, _roundDecimals, MidpointRounding.AwayFromZero);

                    sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                    sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    sheet.Cells[row, col++].Value = Math.Round(item.GrossSales, _roundDecimals, MidpointRounding.AwayFromZero);

                    sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                    sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    sheet.Cells[row, col++].Value = Math.Round(item.NoOfGuest, _roundDecimals, MidpointRounding.AwayFromZero);
                    row++;count++;
                    col = 1;
                }
                //Footer
                sheet.Cells[row, 1].Value = L("Total");
                sheet.Cells[row, 1, row, 2].Merge = true;
                sheet.Cells[row, 1, row, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                col = 3;
                foreach (var shift in output.TotalListShift)
                {
                    sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                    sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    sheet.Cells[row, col++].Value = Math.Round(shift.ShiftAmount, _roundDecimals, MidpointRounding.AwayFromZero);

                }
                sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                sheet.Cells[row, col++].Value = Math.Round(output.TotalTotalSales, _roundDecimals, MidpointRounding.AwayFromZero);

                sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                sheet.Cells[row, col++].Value = Math.Round(output.TotalCompliment, _roundDecimals, MidpointRounding.AwayFromZero);

                sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                sheet.Cells[row, col++].Value = Math.Round(output.TotalPromotion, _roundDecimals, MidpointRounding.AwayFromZero);

                sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                sheet.Cells[row, col++].Value = Math.Round(output.TotalTotalDeduction, _roundDecimals, MidpointRounding.AwayFromZero);

                sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                sheet.Cells[row, col++].Value = Math.Round(output.TotalNetRest, _roundDecimals, MidpointRounding.AwayFromZero);

                sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                sheet.Cells[row, col++].Value = Math.Round(output.TotalServiceCharge, _roundDecimals, MidpointRounding.AwayFromZero);

                sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                sheet.Cells[row, col++].Value = Math.Round(output.TotalGST, _roundDecimals, MidpointRounding.AwayFromZero);

                sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                sheet.Cells[row, col++].Value = Math.Round(output.TotalGrossSales, _roundDecimals, MidpointRounding.AwayFromZero);

                sheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                sheet.Cells[row, col++].Value = Math.Round(output.TotalNoOfGuest, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[row, 1, row,shiftCount + 11].Style.Font.Bold = true;
                sheet.Cells[row, 1, row, shiftCount + 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                for (int i = 1; i <= shiftCount + 11; i++) sheet.Column(i).AutoFit();
                row++;
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }
        private int AddReportHeader(ExcelWorksheet sheet, string nameOfReport, IDateInput input)
        {
            var brandName = "";
            var locationCode = "";
            var locationName = "";
            var branch = "All";
            var isAllLocation = (input.LocationGroup?.Locations?.Count() + input.LocationGroup?.Groups?.Count() + input.LocationGroup?.LocationTags?.Count()) == _locRepo.GetAll().Count();

            if (isAllLocation)
            {
                branch = "All";
            }
            else
            {
                if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
                {
                    branch = input.LocationGroup.Locations.Select(l => l.Name).JoinAsString(", ");
                }
                if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
                {
                    branch = input.LocationGroup.Groups.Select(l => l.Name).JoinAsString(", ");
                }
                if (input.LocationGroup?.LocationTags != null && input.LocationGroup.LocationTags.Any())
                {
                    branch = input.LocationGroup.LocationTags.Select(l => l.Name).JoinAsString(", ");
                }
            }

            Master.Location myLocation = null;

            if (input.Location > 0)
                myLocation = _locRepo.Get(input.Location);

            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var simpleL = input.LocationGroup.Locations.First();
                if (simpleL != null)
                {
                    myLocation = _locRepo.Get(simpleL.Id);
                }
            }

            if (myLocation != null)
            {
                Master.Company myCompany = _comRepo.Get(myLocation.CompanyRefId);
                if (myCompany != null)
                {
                    brandName = myCompany.Name;
                }
                locationCode = myLocation.Code;
                locationName = myLocation.Name;
            }
            var starRow = 1;
            sheet.Cells[starRow, 1].Value = "Lawry’s The Prime Rib Singapore Pte Ltd";
            sheet.Cells[starRow, 1, starRow, 12].Merge = true;
            sheet.Cells[starRow, 1, starRow, 13].Style.Font.Bold = true;
            sheet.Cells[starRow, 1, starRow, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            starRow++;
            sheet.Cells[starRow, 1].Value = nameOfReport;
            sheet.Cells[starRow, 1, starRow, 12].Merge = true;
            sheet.Cells[starRow, 1, starRow, 13].Style.Font.Bold = true;
            sheet.Cells[starRow, 1, starRow, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            starRow++;

            sheet.Cells[starRow, 1].Value = L("Brand");
            sheet.Cells[starRow, 2].Value = brandName;
            sheet.Cells[starRow, 6].Value = "Business Date:";
            sheet.Cells[starRow, 7].Value = input.StartDate.ToString(input.DateFormat) + " to " + input.EndDate.ToString(input.DateFormat);

            starRow++;
            sheet.Cells[starRow, 1].Value = "Branch Name:";
            sheet.Cells[starRow, 2].Value = branch;
            sheet.Cells[starRow, 6].Value = "Printed On:";
            sheet.Cells[starRow, 7].Value = DateTime.Now.ToString(input.DatetimeFormat);

            sheet.Cells[1, 1, 4, 7].Style.Font.Size = 10;

            return 6;
        }
    }
}
