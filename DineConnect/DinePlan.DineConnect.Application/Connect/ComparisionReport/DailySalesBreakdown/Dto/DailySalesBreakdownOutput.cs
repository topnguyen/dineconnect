﻿using DinePlan.DineConnect.Connect.ComparisionReport.MealTimeSales.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.DailySalesBreakdown.Dto
{
    public class DailySalesBreakdownOutput
    {
        public DailySalesBreakdownOutput()
        {
            TotalListShift = new List<ShiftDataItem>();
            Items = new List<DailySalesItem>();
            ListShift = new List<ShiftDto>();
        }
        public List<ShiftDto> ListShift { get; set; }
        public List<ShiftDataItem> TotalListShift { get; set; }
        public List<DailySalesItem> Items { get; set; }
        public decimal TotalTotalSales => Items.Sum(s => s.TotalSales);
        public decimal TotalCompliment => Items.Sum(s => s.Compliment);
        public decimal TotalPromotion => Items.Sum(s => s.Promotion);
        public decimal TotalTotalDeduction => Items.Sum(s => s.TotalDeduction);
        public decimal TotalNetRest => Items.Sum(s => s.NetRest);
        public decimal TotalServiceCharge => Items.Sum(s => s.ServiceCharge);
        public decimal TotalGST => Items.Sum(s => s.GST);
        public decimal TotalGrossSales => Items.Sum(s => s.GrossSales);
        public decimal TotalNoOfGuest => Items.Sum(s => s.NoOfGuest);
       
    }
    public class DailySalesItem
    {
        public string Date { get; set; }
        public DailySalesItem()
        {
            ListShift = new List<ShiftDataItem>();
        }
        public List<ShiftDataItem> ListShift { get; set; }
        public decimal TotalSales => ListShift.Sum(s => s.ShiftAmount);
        public decimal Compliment { get; set; }
        public decimal Promotion { get; set; }
        public decimal TotalDeduction => Compliment + Promotion;
        public decimal NetRest => TotalSales - TotalDeduction;
        public decimal ServiceCharge { get; set; }
        public decimal GST { get; set; }
        public decimal GrossSales => NetRest + ServiceCharge + GST;
        public decimal NoOfGuest { get; set; }
    }
    public class ShiftDataItem
    {
        public ShiftDataItem()
        {
            Shift = new ShiftDto();
        }
        public ShiftDto Shift { get; set; }
        public decimal ShiftAmount { get; set; }
    }
    public class TicketDate
    {
        public string DateTime => Ticket.LastPaymentTime.ToString("dd/MM/yyyy");
        public Transaction.Ticket Ticket { get; set; }
    }
}