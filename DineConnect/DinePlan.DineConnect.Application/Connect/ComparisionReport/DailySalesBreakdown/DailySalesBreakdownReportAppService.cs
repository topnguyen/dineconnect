﻿using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.ComparisionReport.DailySalesBreakdown.Dto;
using DinePlan.DineConnect.Connect.ComparisionReport.DailySalesBreakdown.Exporting;
using DinePlan.DineConnect.Connect.ComparisionReport.MealTimeSales.Dto;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Ticket.Implementation;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.DailySalesBreakdown
{
    public class DailySalesBreakdownReportAppService : ConnectReportAppServiceBase, IDailySalesBreakdownReportAppService
    {
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly IDailySalesBreakdownReportExporter _exporter;
        private readonly IRepository<Promotion> _promoRepo;
        private readonly List<string> COMPLIMENTS = new List<string>() { "Manager ENT", "Guest Complaint", "Guest Entertainment", "Mr Juan", "Angeline", "Imran", "Musa", "Shiva", "Celina", "Marketing", "Chef Saran", "Chef Kenneth", "Jasmine", "Ethel", "Stephen", "Francesco", "AGM", "Mr Juan VIP", "Staff Birthday Voucher" };
        public DailySalesBreakdownReportAppService(
            IRepository<Transaction.Ticket> ticketManager,
            ILocationAppService locService,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Master.Location> lRepo,
            IReportBackgroundAppService rbas,
            IRepository<Promotion> promoRepo,
            IBackgroundJobManager bgm,
            IDailySalesBreakdownReportExporter exporter)
            : base(ticketManager, locService, unitOfWorkManager, lRepo)
        {
            _rbas = rbas;
            _bgm = bgm;
            _exporter = exporter;
            _promoRepo = promoRepo;
        }
        public async Task<DailySalesBreakdownOutput> GetDailySalesReports(GetTicketInput input)
        {
            input.EndDate = input.EndDate.Date;
            var result = new DailySalesBreakdownOutput();
            var dailyItemList = new List<DailySalesItem>();
            var scheduleFromSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.Schedules);
            var templateSeting = JsonConvert.DeserializeObject<List<LocationSchdule>>(scheduleFromSetting);
            var listShift = templateSeting.MapTo<List<ShiftDto>>();
            var tickets = GetAllTicketsForTicketInput(input);
            var ticketGroup = from d in tickets
                              group d by new { d.LastPaymentTimeTruc }
                              into g
                              select new { Date = g.Key.LastPaymentTimeTruc, Items = g.ToList() };
            foreach (var tic in ticketGroup)
            {
                dailyItemList.Add(GetDailySale(tic.Items, listShift, tic.Date));
            }

            var totalListShift = new List<ShiftDataItem>();
            foreach (var shift in listShift)
            {
                totalListShift.Add(new ShiftDataItem
                {
                    Shift = shift,
                    ShiftAmount = dailyItemList.Any() ? dailyItemList.SelectMany(s => s.ListShift).Where(s => s.Shift == shift).Sum(s => s.ShiftAmount) : 0
                });
            }
            result.ListShift = listShift;
            result.Items = dailyItemList;
            result.TotalListShift = totalListShift;
            return result;
        }

        private DailySalesItem GetDailySale(List<Transaction.Ticket> dayTickets, List<ShiftDto> shifts, DateTime date)
        {
            var promotionList = _promoRepo.GetAll().ToList();
            var listShiftItem = new List<ShiftDataItem>();

            foreach (var shift in shifts)
            {
                var startDate = date.Add(new TimeSpan(shift.StartHour, shift.StartMinute, 0));
                var endDate = date.Add(new TimeSpan(shift.EndHour, shift.EndMinute, 0));
                var ticketShift = dayTickets.Where(s => s.LastPaymentTime >= startDate && s.LastPaymentTime < endDate);
                var sale = ticketShift.SelectMany(t => t.Transactions).Where(t => t.TransactionType.Name == "SALE").Sum(t => t.Amount);

                // shift promotion
                var promotionShift = new List<PromotionDetailValue>();
                foreach (var ticket in ticketShift)
                {
                    promotionShift.AddRange(GetPromotionDetailValues(ticket));
                }

                listShiftItem.Add(new ShiftDataItem
                {
                    Shift = shift,
                    ShiftAmount = sale + promotionShift.Sum(x => x.PromotionAmount)
                });
            }

            // Calculate promotion
            var promotions = new List<PromotionDetailValue>();
            foreach (var ticket in dayTickets)
            {
                promotions.AddRange(GetPromotionDetailValues(ticket));
            }

            // Number of guests
            var numberOfPax = GetNumberOfGuests(dayTickets);

            // compliment amount
            var complimentAmount = promotions.Where(p => promotionList.Any(x => x.Name == p.PromotionName) && COMPLIMENTS.Contains(p.PromotionName)).Sum(p => p.PromotionAmount);
            var promotionAmount = promotions.Where(p => promotionList.Any(x => x.Name == p.PromotionName) && !COMPLIMENTS.Contains(p.PromotionName)).Sum(p => p.PromotionAmount);

            //  service charge            
            var serviceCharge = dayTickets.SelectMany(t => t.Transactions).Where(t => t.TransactionType.Name == "SERVICE CHARGE").Sum(t => t.Amount);

            // Tax
            var gst = dayTickets.SelectMany(t => t.Transactions).Where(t => t.TransactionType.Name == "TAX").Sum(t => t.Amount);

            return new DailySalesItem()
            {
                ListShift = listShiftItem,
                Date = dayTickets.First().LastPaymentTimeTruc.ToString("ddd"),
                Promotion = promotionAmount,
                NoOfGuest = numberOfPax,
                ServiceCharge = serviceCharge,
                Compliment = complimentAmount,
                GST = gst,
            };
        }

        public async Task<FileDto> GetDaiySalesReportsToExport(GetTicketInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                input.MaxResultCount = 10000;

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.DAILYSALEBREAK,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });

                    if (backGroundId > 0)
                    {
                        var tInput = new DailySalesBreakdownReportInput
                        {
                            GetDailySalesBreakdownReportInput = input,
                        };
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.DAILYSALEBREAK,
                            DailySalesBreakdownReportInput = tInput,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    var output = await _exporter.ExportDaylySalesReport(input, this);
                    return output;
                }
            }
            return null;
        }

    }
}
