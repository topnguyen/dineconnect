﻿using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.CashSummaryReport.Exporting;
using DinePlan.DineConnect.Connect.ComparisionReport.Dto;
using DinePlan.DineConnect.Connect.ComparisionReport.Exporting;
using DinePlan.DineConnect.Connect.ComparisionReport.MealTimeSales.Dto;
using DinePlan.DineConnect.Connect.ComparisionReport.MonthlySalesAdvance.Dto;
using DinePlan.DineConnect.Connect.ComparisionReport.MonthlySalesAdvance.Exporting;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Setting;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Ticket.Implementation;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DinePlan.DineConnect.Connect.ComparisionReport.MonthlySalesAdvance
{
    public class MonthlySalesAdvanceReportAppService : ConnectReportAppServiceBase, IMonthlySalesAdvanceReportAppService
    {
        private readonly ILocationAppService _locService;
        private IConnectReportAppService _appService;
        private readonly IRepository<Transaction.Ticket> _ticketManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly IMonthlySalesAdvanceReportExporter _exporter;
        private readonly IRepository<Category> _categoryRepo;
        private readonly IRepository<ProductGroup> _productGroupRepo;
        private readonly IRepository<PaymentType> _payRe;
        public static readonly List<string> FOODs = new List<string>() { "Entrees", "A La Carte", "Dessert" };
        private readonly List<string> BEVERAGEs = new List<string>() { "Beer", "Liquor", "Wine", "Cocktail", "Softdrink", "BEVERAGE" };
        public static readonly List<string> alcoholicTypes = new List<string>() { "Beer", "Liquor", "Wine", "Cocktail" };
        public static readonly List<string> nonAlcohicTypes = new List<string>() { "Softdrink & Others", "BEVERAGE" };
        private readonly List<string> Others = new List<string>() { "OTHER" };
        private readonly List<string> PaymentInsert = new List<string>() { "ROUND-", "TIPS" };
        private readonly List<string> Payment = new List<string>() { "CASH", "AMEX", "MG VOUCHER", "ALI PAY", "DINER", "JCB", "CASH VOUCHER", "VISA", "MASTER", "PAYNOW", "DEPOSIT(M)", "COMP VOUCHER", "CHOPE VOUCHER", "GRABPAY", "CARD", "MEMBER", "DEPOSIT" };
        private readonly string TaxName = "TAX";
        private readonly string ServiceChargeName = "SERVICE CHARGE";
        private readonly List<string> COMPLIMENTS = new List<string>() { "Manager ENT", "Guest Complaint", "Guest Entertainment", "Mr Juan", "Angeline", "Imran", "Musa", "Shiva", "Celina", "Marketing", "Chef Saran", "Chef Kenneth", "Jasmine", "Ethel", "Stephen", "Francesco", "AGM", "Mr Juan VIP", "Staff Birthday Voucher" };
        private readonly IRepository<Promotion> _promoRepo;
        public MonthlySalesAdvanceReportAppService(
            IRepository<Transaction.Ticket> ticketManager,
            ILocationAppService locService,
            IUnitOfWorkManager unitOfWorkManager,
            IConnectReportAppService appService,
            IRepository<Master.Location> lRepo,
            IRepository<Promotion> promoRepo,
            IReportBackgroundAppService rbas,
            IBackgroundJobManager bgm,
            IMonthlySalesAdvanceReportExporter exporter,
            IRepository<PaymentType> payRe,
            IRepository<Category> categoryRepo,
            IRepository<ProductGroup> productGroupRepo)
            : base(ticketManager, locService, unitOfWorkManager, lRepo)
        {
            _ticketManager = ticketManager;
            _unitOfWorkManager = unitOfWorkManager;
            _locService = locService;
            _rbas = rbas;
            _bgm = bgm;
            _exporter = exporter;
            _payRe = payRe;
            _appService = appService;
            _categoryRepo = categoryRepo;
            _productGroupRepo = productGroupRepo;
            _promoRepo = promoRepo;
        }
        public async Task<MonthlySalesAdvanceResultOutput> GetMonthlySalesReports(GetTicketInput input)
        {
            input.EndDate = input.EndDate.Date;
            var allPayment = _payRe.GetAll();
            var scheduleFromSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.Schedules);
            var templateSeting = JsonConvert.DeserializeObject<List<LocationSchdule>>(scheduleFromSetting);
            var listShift = templateSeting.MapTo<List<ShiftDto>>();
            var allTicket = GetAllTicketsForTicketInput(input).ToList();
            var allOrder = allTicket.SelectMany(s => s.Orders).ToList();            
            var taxSerivceTotal = new List<PaymentItem>();

            //  service charge            
            var ttServiceCharge = allTicket.SelectMany(t => t.Transactions).Where(t => t.TransactionType.Name == "SERVICE CHARGE").Sum(t => t.Amount);

            // Tax
            var ttTax = allTicket.SelectMany(t => t.Transactions).Where(t => t.TransactionType.Name == "TAX").Sum(t => t.Amount);

            var result = new MonthlySalesAdvanceResultOutput();
            result.TotalServiceCharge = ttServiceCharge;
            result.TotalGst = ttTax;
            result.FoodList = GetProductList(allOrder, listShift, GetProductGroupIds(FOODs), "Food", ttTax, ttServiceCharge);
            SortCategories(result.FoodList.CategoryItems, FOODs);
            result.BeverageList = GetProductList(allOrder, listShift, GetProductGroupIds(BEVERAGEs), "Beverage", ttTax, ttServiceCharge);
            SortCategories(result.BeverageList.CategoryItems, BEVERAGEs);
            result.Others = GetProductList(allOrder, listShift, GetProductGroupIds(Others), "Others", ttTax, ttServiceCharge);
            result.ListShift = listShift;
            result.Cover = GetCover(listShift, allTicket);
            // Update Total for all
            result.UpdateTotal();

            // Payment
            var paymentList = new List<PaymentItem>();
            var allPaymentTicket = allTicket.SelectMany(s => s.Payments);
            foreach (var pay in allPayment)
            {
                var payAmount = allPaymentTicket.Where(s => s.PaymentTypeId == pay.Id);
                paymentList.Add(new PaymentItem
                {
                    Name = pay.Name,
                    Amount = payAmount.Any() ? payAmount.Sum(s => s.Amount) : 0
                });
            }
            var paymentGroup = from d in paymentList
                               group d by new { d.Name }
                               into g
                               select new { PaymentName = g.Key.Name, Items = g.ToList() };
            var totalPaymentAmount = paymentList.Sum(s => s.Amount);
            var paymentListResult = new List<PaymentItem>();
            foreach (var group in paymentGroup)
            {
                paymentListResult.Add(new PaymentItem
                {
                    Name = group.PaymentName,
                    Amount = group.Items.Sum(s => s.Amount),
                    AmountTotalPercent = totalPaymentAmount
                });
            }
            result.PaymentList = new PaymentOutput() { PaymentItems = paymentListResult };
            SortPayment(result.PaymentList.PaymentItems, Payment);
            var paymentListInsertResult = new List<PaymentItem>();

            //Round ROUNDING	ROUND+	ROUND-
            var listRounds = new List<string>() { "ROUNDING", "ROUND+", "ROUND-" };
            var ttAmount = allTicket.SelectMany(t => t.Transactions).Where(t => listRounds.Contains(t.TransactionType.Name)).Sum(s => s.Amount);
            paymentListInsertResult.Add(new PaymentItem { Amount = ttAmount, Name = "ROUND"});
            // TIPS
            var listTips = new List<string>() { "TIPS" };
            ttAmount = allTicket.SelectMany(t => t.Transactions).Where(t => listTips.Contains(t.TransactionType.Name)).Sum(s => s.Amount);
            paymentListInsertResult.Add(new PaymentItem { Amount = ttAmount, Name = "TIPS" });

            result.PaymentInsertList = new PaymentInsertOutput() { PaymentItems = paymentListInsertResult, PaymentOutput = result.PaymentList };
            return result;
        }
        public async Task<FileDto> GetMonthlySalesReportsToExport(GetTicketInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                input.MaxResultCount = 10000;

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.MONTHLYSALEADVANCE,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });

                    if (backGroundId > 0)
                    {
                        var tInput = new MonthlySaleAdvanceReportInput
                        {
                            GetMonthlySaleAdvanceReportInput = input,
                        };
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.MONTHLYSALEADVANCE,
                            MonthlySaleAdvanceReportInput = tInput,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    var output = await _exporter.ExportMonthlySalesAdvanceReport(input, this);
                    return output;
                }
            }
            return null;
        }

        private Cover GetCover(List<ShiftDto> listShift, List<Transaction.Ticket> tickets)
        {
            var coverShifts = new List<ListShift>();

            foreach (var shift in listShift)
            {
                var ticketShift = tickets.Where(s => (s.LastPaymentTime.Hour * 60 + s.LastPaymentTime.Minute) >= (shift.StartHour * 60 + shift.StartMinute)
                                                                    && (s.LastPaymentTime.Hour * 60 + s.LastPaymentTime.Minute) < (shift.EndHour * 60 + shift.EndMinute));
                coverShifts.Add(new ListShift()
                {
                    Shift = shift,
                    Cover = GetNumberOfGuests(ticketShift.ToList())
                });
            }

            return new Cover(coverShifts);
        }

        private ProductList GetProductList(List<Order> allOrder, List<ShiftDto> listShift, List<int> groupIds, string name, decimal ttTax, decimal ttServiceCharge)
        {
            var promotionList = _promoRepo.GetAll().ToList();
            var result = new ProductList();
            var od = allOrder.Where(s => s.MenuItem != null);
            var groupByProductFood = from d in od
                                     where groupIds.Contains(d.MenuItem.Category.ProductGroupId.GetValueOrDefault())
                                     group d by new { Id = d.MenuItem.Category.ProductGroupId }
                                                                into g
                                     select new { ProductGroupId = g.Key.Id, Items = g.ToList() };


            var listCategoryItem = new List<CategoryItem>();
            var productGroupCache = new Dictionary<int, string>();
            foreach (var food in groupByProductFood)
            {
                var listShiftItem = new List<ListShift>();
                var orderGroupList = food.Items;
                // Promotion
                var promotionDetail = new List<PromotionDetailValue>();
                promotionDetail.AddRange(GetPromotionDetailValues(orderGroupList));

                foreach (var shift in listShift)
                {
                    var orderGroupListShift = orderGroupList.Where(s => (s.Ticket.LastPaymentTime.Hour * 60 + s.Ticket.LastPaymentTime.Minute) >= (shift.StartHour * 60 + shift.StartMinute)
                                                                        && (s.Ticket.LastPaymentTime.Hour * 60 + s.Ticket.LastPaymentTime.Minute) < (shift.EndHour * 60 + shift.EndMinute));
                    var shiftPromotions = GetPromotionDetailValues(orderGroupListShift.ToList());
                    listShiftItem.Add(new ListShift
                    {
                        Shift = shift,
                        ShiftAmount = orderGroupListShift.Sum(s => s.GetTotal()) + shiftPromotions.Sum(p => p.PromotionAmount),
                        //Cover = GetNumberOfGuests(orderGroupListShift.Select(o => o.Ticket).ToList())
                    });
                }

                if (!productGroupCache.ContainsKey(food.ProductGroupId.Value))
                {
                    productGroupCache.Add(food.ProductGroupId.Value, _productGroupRepo.FirstOrDefault(s => s.Id == food.ProductGroupId)?.Name);
                }

                listCategoryItem.Add(new CategoryItem
                {
                    Name = productGroupCache[food.ProductGroupId.Value],
                    ItemListShift = listShiftItem,
                    Compliment = promotionDetail.Where(p => promotionList.Any(x => x.Name == p.PromotionName) && COMPLIMENTS.Contains(p.PromotionName)).Sum(p => p.PromotionAmount),
                    Promotion = promotionDetail.Where(p => promotionList.Any(x => x.Name == p.PromotionName) && !COMPLIMENTS.Contains(p.PromotionName)).Sum(p => p.PromotionAmount),
                    TotalServiceCharge = ttServiceCharge,
                    TotalGst = ttTax,
                });
            }

            result.Name = name.ToUpper();
            result.CategoryItems = listCategoryItem;
            return result;
        }

        private List<int> GetProductGroupIds(List<string> groupProductNames)
        {
            var results = _productGroupRepo.GetAll().Where(p => groupProductNames.Contains(p.Name)).Select(x => x.Id);
            return results.ToList();
        }

        private void SortCategories(List<CategoryItem> input, List<string> sortList)
        {
            input.Sort((x, y) =>
            {

                var index1 = sortList.IndexOf(x.Name);
                var index2 = sortList.IndexOf(y.Name);
                return index1.CompareTo(index2);
            });
        }
        private void SortPayment(List<PaymentItem> input, List<string> sortList)
        {
            input.Sort((x, y) =>
            {

                var index1 = sortList.IndexOf(x.Name);
                var index2 = sortList.IndexOf(y.Name);
                return index1.CompareTo(index2);
            });
        }
    }
}
