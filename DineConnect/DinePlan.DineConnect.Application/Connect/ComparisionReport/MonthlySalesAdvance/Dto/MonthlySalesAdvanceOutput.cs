﻿using DinePlan.DineConnect.Connect.ComparisionReport.MealTimeSales.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.MonthlySalesAdvance.Dto
{
    public class MonthlySalesAdvanceResultOutput
    {
        public MonthlySalesAdvanceResultOutput()
        {
            ListShift = new List<ShiftDto>();

            PaymentList = new PaymentOutput();

            Ledger = new MonthlyRowItem();

            PaymentInsertList = new PaymentInsertOutput();
        }
        public ProductList FoodList { get; set; }
        public ProductList BeverageList { get; set; }
        public MonthlyRowItem TotalFBItem
        {
            get
            {
                var products = new List<ProductList>() { FoodList, BeverageList };
                var results = new List<ListShift>();
                var shiftGroups = products.Select(p => p.SubTotal).SelectMany(c => c.ListShiftTotal).GroupBy(x => x.Shift);
                foreach (var shiftGroup in shiftGroups)
                {
                    results.Add(new ListShift()
                    {
                        Shift = shiftGroup.Key,
                        ShiftAmount = shiftGroup.Sum(s => s.ShiftAmount)
                    });
                }

                return new MonthlyRowItem()
                {
                    Name = "Total F&B",
                    ItemListShift = results,
                    Compliment = products.Select(p => p.SubTotal).Sum(s => s.SubTotalCompliment),
                    Promotion = products.Select(p => p.SubTotal).Sum(s => s.SubTotalPromotion),
                    ServiceCharge = FoodList.SubTotal.SubTotalServiceCharge + BeverageList.SubTotal.SubTotalServiceCharge,
                    GST = FoodList.SubTotal.SubTotalGST + BeverageList.SubTotal.SubTotalGST,
                    TotalPercent = FoodList.SubTotal.SubTotalTotalPercent + BeverageList.SubTotal.SubTotalTotalPercent
                };
            }
        }

        public MonthlyRowItem Other1
        {
            get
            {
                return new MonthlyRowItem()
                {
                    Name = "Others",
                    ItemListShift = Others.CategoryItems.SelectMany(x => x.ItemListShift).ToList(),
                    Compliment = Others.CategoryItems.Sum(x => x.Compliment),
                    Promotion = Others.CategoryItems.Sum(x => x.Promotion),
                    ServiceCharge = GrossSales.ServiceCharge - FoodList.SubTotal.SubTotalServiceCharge - BeverageList.SubTotal.SubTotalServiceCharge,
                    GST = GrossSales.GST - FoodList.SubTotal.SubTotalGST - BeverageList.SubTotal.SubTotalGST,
                    TotalPercent = GrossSales.TotalPercent - FoodList.SubTotal.SubTotalTotalPercent - BeverageList.SubTotal.SubTotalTotalPercent
                };
            }
        }
        public MonthlyRowItem GrossSales
        {
            get
            {
                var products = new List<ProductList>() { FoodList, BeverageList, Others };
                var results = new List<ListShift>();
                var shiftGroups = products.Select(p => p.SubTotal).SelectMany(c => c.ListShiftTotal).GroupBy(x => x.Shift);
                foreach (var shiftGroup in shiftGroups)
                {
                    results.Add(new ListShift()
                    {
                        Shift = shiftGroup.Key,
                        ShiftAmount = shiftGroup.Sum(s => s.ShiftAmount)
                    });
                }
                return new MonthlyRowItem()
                {
                    Name = "Grand Total",
                    ItemListShift = results,
                    Compliment = products.Select(p => p.SubTotal).Sum(s => s.SubTotalCompliment),
                    Promotion = products.Select(p => p.SubTotal).Sum(s => s.SubTotalPromotion),
                    ServiceCharge = TotalServiceCharge,
                    GST = TotalGst,
                    TotalPercent = 100
                };

            }
        }
        //public MonthlyRowItem NetSales
        //{
        //    get
        //    {
        //        var listShift = new List<ListShift>();
        //        if (ListShift != null)
        //        {
        //            for (int i = 0; i < ListShift.Count; i++)
        //            {
        //                listShift.Add(new Dto.ListShift()
        //                {
        //                    Shift = ListShift[i],
        //                    ShiftAmount = GrossSales.ListShiftTotal[i].ShiftAmount
        //                    //- Ledger.ItemListShift[i].ShiftAmount TODO
        //                });
        //            }
        //        }
        //        return new MonthlyRowItem()
        //        {
        //            ItemListShift = listShift,
        //            Compliment = GrossSales.SubTotalCompliment - Ledger.Compliment,
        //            Promotion = GrossSales.SubTotalPromotion - Ledger.Promotion,
        //            TotalAll = GrossSales.SubTotalTotalAll - Ledger.TotalAll,
        //            TotalPercent = 100
        //        };
        //    }
        //}
        public MonthlyRowItem Ledger { get; set; }

        public ProductList Others { get; set; }
        public List<ShiftDto> ListShift { get; set; }

        public PaymentOutput PaymentList { get; set; }
        public PaymentInsertOutput PaymentInsertList { get; set; }
        public Cover Cover
        {
            get; set;
        }
        public decimal PPATotalSales => Cover.TotalCover > 0 ? GrossSales.TotalSales / Cover.TotalCover : 0;
        public decimal PPATotalS => Cover.TotalCover > 0 ? GrossSales.TotalAll / Cover.TotalCover : 0;
        public decimal LyaForMonth = 313821.60M;

        public void UpdateTotal()
        {
            FoodList.CategoryItems.ForEach(x => x.TotalTotalAll = GrossSales.TotalAll);
            BeverageList.CategoryItems.ForEach(x => x.TotalTotalAll = GrossSales.TotalAll);
            Others.CategoryItems.ForEach(x => x.TotalTotalAll = GrossSales.TotalAll);

            FoodList.CategoryItems.ForEach(x => x.TotalTotalSales = GrossSales.TotalSales);
            BeverageList.CategoryItems.ForEach(x => x.TotalTotalSales = GrossSales.TotalSales);
            Others.CategoryItems.ForEach(x => x.TotalTotalSales = GrossSales.TotalSales);

            FoodList.CategoryItems.ForEach(x => x.TotalServiceCharge = TotalServiceCharge);
            BeverageList.CategoryItems.ForEach(x => x.TotalServiceCharge = TotalServiceCharge);
            Others.CategoryItems.ForEach(x => x.TotalServiceCharge = TotalServiceCharge);

            FoodList.CategoryItems.ForEach(x => x.TotalGst = TotalGst);
            BeverageList.CategoryItems.ForEach(x => x.TotalGst = TotalGst);
            Others.CategoryItems.ForEach(x => x.TotalGst = TotalGst);
        }
        public decimal TotalServiceCharge { get; set; }
        public decimal TotalGst { get; set; }
    }
    public class PaymentInsertOutput
    {
        public PaymentInsertOutput()
        {
            PaymentItems = new List<PaymentItem>();
            PaymentOutput = new PaymentOutput();
        }
        public PaymentOutput PaymentOutput { get; set; }
        public List<PaymentItem> PaymentItems { get; set; }
        public decimal TotalAmount => PaymentOutput.TotalAmount - PaymentItems.Sum(s => s.Amount);
    }
    public class PaymentOutput
    {
        public PaymentOutput()
        {
            PaymentItems = new List<PaymentItem>();
        }
        public List<PaymentItem> PaymentItems { get; set; }
        public decimal TotalAmount => PaymentItems.Sum(s => s.Amount);
        public decimal TotalAmountPercent => PaymentItems.Sum(s => s.AmountPercent);
    }
    public class PaymentItem
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountPercent => AmountTotalPercent != 0 ? Amount / AmountTotalPercent * 100 : 0;
        public decimal AmountTotalPercent { get; set; }
    }

    public class SubTotal
    {
        public List<CategoryItem> CategoryItems { get; set; }
        public SubTotal(List<CategoryItem> categoryItems)
        {
            CategoryItems = categoryItems;
        }
        public List<ListShift> ListShiftTotal
        {
            get
            {
                var results = new List<ListShift>();
                var shiftGroups = CategoryItems.SelectMany(c => c.ItemListShift).GroupBy(x => x.Shift);
                foreach (var shiftGroup in shiftGroups)
                {
                    results.Add(new ListShift()
                    {
                        Shift = shiftGroup.Key,
                        ShiftAmount = shiftGroup.Sum(s => s.ShiftAmount)
                    });
                }
                return results;
            }
        }

        public decimal SubTotalCompliment { get { return CategoryItems.Sum(x => x.Compliment); } }
        public decimal SubTotalPromotion { get { return CategoryItems.Sum(x => x.Promotion); } }
        public decimal SubTotalTotalSales { get { return CategoryItems.Sum(x => x.TotalSales); } }
        public decimal SubTotalServiceCharge { get { return CategoryItems.Sum(x => x.ServiceCharge); } }
        public decimal SubTotalGST { get { return CategoryItems.Sum(x => x.GST); } }
        public decimal SubTotalTotalAll { get { return CategoryItems.Sum(x => x.TotalAll); } }
        public decimal SubTotalTotalPercent { get { return CategoryItems.Sum(x => x.TotalPercent); } }
        public decimal TotalAllPercent { get; set; }
        public decimal TotalServiceCharge { get; set; }
        public decimal TotalGst { get; set; }
    }

    public class Cover
    {
        public Cover(List<ListShift> listshift)
        {
            ListShiftTotal = listshift;
        }
        public List<ListShift> ListShiftTotal { get; set; }

        public decimal TotalCover => ListShiftTotal.Sum(x => x.Cover);


    }
    public class ProductList
    {
        public string Name { get; set; }

        private List<CategoryItem> _categoryItems;
        public List<CategoryItem> CategoryItems
        {
            get { return _categoryItems; }
            set
            {
                _categoryItems = value;
                _subTotal = new SubTotal(_categoryItems);
            }
        }

        private SubTotal _subTotal;
        public SubTotal SubTotal
        {
            get
            {
                return _subTotal;
            }
        }

        public decimal Total { get; set; }
    }

    public class MonthlyRowItem
    {
        public MonthlyRowItem()
        {
            ItemListShift = new List<ListShift>();
        }
        public string Name { get; set; }
        public List<ListShift> ItemListShift { get; set; }
        public decimal Compliment { get; set; }
        public decimal Promotion { get; set; }
        public decimal TotalSales => ItemListShift.Sum(s => s.ShiftAmount) - Compliment - Promotion;
        public decimal ServiceCharge { get; set; }
        public decimal GST { get; set; }
        public decimal TotalAll => TotalSales + ServiceCharge + GST;
        public decimal TotalPercent { get; set; }
    }

    public class CategoryItem
    {
        public CategoryItem()
        {
            ItemListShift = new List<ListShift>();
        }
        public string Name { get; set; }
        public string Name2
        {
            get
            {
                if (Name != null && Name.ToUpper() == "WINE")
                    return "Wine";
                if (Name != null && Name.ToUpper() == "SOFTDRINK")
                    return "Softdrink & Others";
                return Name;
            }
        }
        public List<ListShift> ItemListShift { get; set; }
        public decimal Compliment { get; set; }
        public decimal Promotion { get; set; }
        public decimal TotalSales => ItemListShift.Sum(s => s.ShiftAmount) - Compliment - Promotion;
        public decimal ServiceCharge => TotalTotalSales != 0 ? (TotalSales / TotalTotalSales) * TotalServiceCharge : 0;
        public decimal GST => TotalTotalSales + TotalServiceCharge != 0 ? (TotalSales + ServiceCharge) / (TotalTotalSales + TotalServiceCharge) * TotalGst : 0;
        public decimal TotalAll => TotalSales + ServiceCharge + GST;
        public decimal TotalPercent => TotalTotalAll != 0 ? (TotalAll / TotalTotalAll) * 100 : 0;

        public decimal TotalTotalAll { get; set; }
        public decimal TotalServiceCharge { get; set; }
        public decimal TotalGst { get; set; }
        public decimal TotalTotalSales { get; set; }
    }

    public class ListShift
    {
        public ListShift()
        {
            Shift = new ShiftDto();
        }
        public ShiftDto Shift { get; set; }
        public decimal ShiftAmount { get; set; }
        public decimal Cover { get; set; }
    }
}
