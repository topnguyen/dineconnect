﻿using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.ComparisionReport.MonthlySalesAdvance.Dto;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.MonthlySalesAdvance.Exporting
{
    public class MonthlySalesAdvanceReportExporter : FileExporterBase, IMonthlySalesAdvanceReportExporter
    {
        private IRepository<Master.Company> _comRepo;
        private IRepository<Master.Location> _locRepo;
        private readonly SettingManager _settingManager;
        private const string ITEM_SPACE = "   ";
        private List<string> PaymentSum = new List<string>() { "ROUND-", "TIPS" };

        public MonthlySalesAdvanceReportExporter(IRepository<Master.Location> loRepository, IRepository<Company> comRepository, SettingManager settingManager)
        {
            _locRepo = loRepository;
            _comRepo = comRepository;
            _settingManager = settingManager;
            _roundDecimals = _settingManager.GetSettingValue<int>(AppSettings.ConnectSettings.Decimals);
        }
        public async Task<FileDto> ExportMonthlySalesAdvanceReport(GetTicketInput input, IMonthlySalesAdvanceReportAppService appService)
        {

            var file = new FileDto("MonthlySalesAdvance_" + input.StartDate.ToString("yyyy-MM-dd") + "_" + input.EndDate.ToString("yyyy-MM-dd") + ".xlsx", MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Monthly"));
                sheet.OutLineApplyStyle = true;
                int rowCount = AddReportHeader(sheet, L("MonthlySalesAdvance"), input);
                var outPut = await appService.GetMonthlySalesReports(input);
                var foodList = outPut.FoodList;
                var beverageList = outPut.BeverageList;
                var totalFBList = outPut.TotalFBItem;
                var otherList = outPut.Other1;
                var grossSales = outPut.GrossSales;
                //var netSales = outPut.NetSales;
                var cover = outPut.Cover;
                var headers = new List<string> { L("Items") };
                headers.AddRange(outPut.ListShift.Select(s => s.Name));
                headers.AddRange(new List<string> { L("Compliment"), L("Promotion"), L("TotalSales"), L("ServiceCharge"), L("GST"), "Total S($)", "%" });

                AddHeader(
                    sheet, rowCount,
                    headers.ToArray()
                );

                rowCount++;

                //Food
                sheet.Cells[rowCount, 1].Value = foodList.Name;
                sheet.Cells[rowCount, 1].Style.Font.Bold = true;
                sheet.Cells[rowCount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                rowCount++;
                int col = 1;
                foreach (var itemFood in MonthlySalesAdvanceReportAppService.FOODs)
                {
                    var food = foodList.CategoryItems.FirstOrDefault(x => x.Name2 == itemFood);
                    if (food != null)
                    {
                        PrintCategoryItem(sheet, rowCount, col, food);
                        rowCount++;
                        col = 1;
                    }
                }
                PrintSubTotal(outPut, sheet, rowCount, col, foodList.SubTotal);
                rowCount++;
                col = 1;

                //beverageList

                sheet.Cells[rowCount, 1].Value = beverageList.Name;
                sheet.Cells[rowCount, 1].Style.Font.Bold = true;
                sheet.Cells[rowCount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                rowCount++;
                var alcoholics = beverageList.CategoryItems.Where(x => MonthlySalesAdvanceReportAppService.alcoholicTypes.Contains(x.Name2));
                var nonAlcoholics = beverageList.CategoryItems.Where(x => MonthlySalesAdvanceReportAppService.nonAlcohicTypes.Contains(x.Name2));

                sheet.Cells[rowCount, 1].Value = ITEM_SPACE + "Alcoholic Bev.";
                sheet.Cells[rowCount, 1].Style.Font.Bold = true;
                sheet.Cells[rowCount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                rowCount++;
                foreach (var alcohic in MonthlySalesAdvanceReportAppService.alcoholicTypes)
                {
                    var bev = beverageList.CategoryItems.FirstOrDefault(x => x.Name2 == alcohic);
                    if (bev != null)
                    {
                        PrintCategoryItem(sheet, rowCount, col, bev);
                        rowCount++;
                        col = 1;
                    }
                }

                sheet.Cells[rowCount, 1].Value = ITEM_SPACE + "NonAlcoholic Bev.";
                sheet.Cells[rowCount, 1].Style.Font.Bold = true;
                sheet.Cells[rowCount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                rowCount++;

                foreach (var alcohic in MonthlySalesAdvanceReportAppService.nonAlcohicTypes)
                {
                    var bev = beverageList.CategoryItems.FirstOrDefault(x => x.Name2 == alcohic);
                    if (bev != null)
                    {
                        PrintCategoryItem(sheet, rowCount, col, bev);
                        rowCount++;
                        col = 1;
                    }
                }
                PrintSubTotal(outPut, sheet, rowCount, col, beverageList.SubTotal);

                rowCount++;
                col = 1;
                //total FB
                PrintMontlyRow(outPut, sheet, rowCount, col, totalFBList, ITEM_SPACE + "Total F&B", ExcelHorizontalAlignment.Left);
                rowCount++;
                col = 1;
                //Others
                PrintMontlyRow(outPut, sheet, rowCount, col, otherList, ITEM_SPACE + otherList.Name, ExcelHorizontalAlignment.Left);
                //PrintSubTotal(outPut, sheet, rowCount, col, otherList.SubTotal, ITEM_SPACE + otherList.Name, ExcelHorizontalAlignment.Left);
                rowCount++;
                col = 1;
                //Gross Sales
                PrintMontlyRow(outPut, sheet, rowCount, col, grossSales, "Grand Total", ExcelHorizontalAlignment.Left);
                rowCount++;
                col = 1;

                ////Net Sales
                //sheet.Cells[rowCount, col].Style.Font.Bold = true;
                //sheet.Cells[rowCount, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                //sheet.Cells[rowCount, col++].Value = L("NetSales");

                //foreach (var shift in netSales.ItemListShift)
                //{
                //    sheet.Cells[rowCount, col++].Value = Math.Round(shift.ShiftAmount, _roundDecimals, MidpointRounding.AwayFromZero);
                //}
                //sheet.Cells[rowCount, col++].Value = Math.Round(netSales.Compliment, _roundDecimals, MidpointRounding.AwayFromZero);
                //sheet.Cells[rowCount, col++].Value = Math.Round(netSales.Promotion, _roundDecimals, MidpointRounding.AwayFromZero);
                //sheet.Cells[rowCount, col++].Value = Math.Round(netSales.TotalSales, _roundDecimals, MidpointRounding.AwayFromZero);
                //sheet.Cells[rowCount, col++].Value = Math.Round(netSales.ServiceCharge, _roundDecimals, MidpointRounding.AwayFromZero);
                //sheet.Cells[rowCount, col++].Value = Math.Round(netSales.GST, _roundDecimals, MidpointRounding.AwayFromZero);
                //sheet.Cells[rowCount, col++].Value = Math.Round(netSales.TotalAll, _roundDecimals, MidpointRounding.AwayFromZero);
                //sheet.Cells[rowCount, col++].Value = Math.Round(netSales.TotalPercent, _roundDecimals, MidpointRounding.AwayFromZero) + " %";
                //rowCount++;
                //col = 1;
                //Cover 
                sheet.Cells[rowCount, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet.Cells[rowCount, col].Style.Font.Italic = true;
                sheet.Cells[rowCount, col++].Value = L("Cover");
                foreach (var shift in cover.ListShiftTotal)
                {
                    sheet.Cells[rowCount, col].Style.Font.Italic = true;
                    sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
                    sheet.Cells[rowCount, col++].Value = Math.Round(shift.Cover, _roundDecimals, MidpointRounding.AwayFromZero);
                }
                sheet.Cells[rowCount, col++].Value = "";
                sheet.Cells[rowCount, col].Style.Font.Italic = true;
                sheet.Cells[rowCount, col++].Value = L("Cover") + " " + L("Total");
                sheet.Cells[rowCount, col].Style.Font.Italic = true;
                sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[rowCount, col++].Value = Math.Round(cover.TotalCover, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, col++].Value = "";
                sheet.Cells[rowCount, col].Style.Font.Italic = true;
                sheet.Cells[rowCount, col++].Value = "";
                sheet.Cells[rowCount, col].Style.Font.Italic = true;
                sheet.Cells[rowCount, col++].Value = "";
                sheet.Cells[rowCount, col++].Value = "";
                rowCount++;
                col = 1;
                //PPA
                sheet.Cells[rowCount, col++].Value = "";
                foreach (var shift in cover.ListShiftTotal)
                {
                    sheet.Cells[rowCount, col++].Value = "";
                }
                sheet.Cells[rowCount, col++].Value = "";
                sheet.Cells[rowCount, col++].Value = "PPA*";
                sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[rowCount, col++].Value = Math.Round(outPut.PPATotalSales, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, col++].Value = "";
                sheet.Cells[rowCount, col++].Value = "PPA*";
                sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[rowCount, col++].Value = Math.Round(outPut.PPATotalS, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, col++].Value = "";
                rowCount++;
                col = 1;
                //Lya For the Month 
                sheet.Cells[rowCount, col++].Value = "";
                foreach (var shift in cover.ListShiftTotal)
                {
                    sheet.Cells[rowCount, col++].Value = "";
                }
                sheet.Cells[rowCount, col++].Value = "";
                sheet.Cells[rowCount, col++].Value = "";
                sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[rowCount, col++].Value = Math.Round(outPut.LyaForMonth, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, col++].Value = "Lya for the Month";
                sheet.Cells[rowCount, col++].Value = "";
                sheet.Cells[rowCount, col++].Value = "";
                sheet.Cells[rowCount, col++].Value = "";
                rowCount++;
                col = 1;
                //Total Cover 
                sheet.Cells[rowCount, col++].Value = "";
                foreach (var shift in cover.ListShiftTotal)
                {
                    sheet.Cells[rowCount, col++].Value = "";
                }
                sheet.Cells[rowCount, col++].Value = "";
                sheet.Cells[rowCount, col++].Value = "";
                sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[rowCount, col++].Value = Math.Round(cover.TotalCover, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, col++].Value = L("Total") + " " + L("Cover");
                sheet.Cells[rowCount, col++].Value = "";
                sheet.Cells[rowCount, col++].Value = "";
                sheet.Cells[rowCount, col++].Value = "";
                rowCount++;
                col = 1;

                rowCount++; rowCount++;
                col = 1;
                var headerPayment = new List<string>() { L("Payment") + " " + L("Detail"), "S($)", "%" };
                foreach (var head in headerPayment)
                {
                    sheet.Cells[rowCount, col].Value = head;
                    sheet.Cells[rowCount, col].Style.Font.Bold = true;
                    sheet.Cells[rowCount, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    col++;
                }
                col = 1;
                rowCount++;
                foreach (var item in outPut.PaymentList.PaymentItems)
                {
                    sheet.Cells[rowCount, col++].Value = item.Name;
                    sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
                    sheet.Cells[rowCount, col++].Value = Math.Round(item.Amount, _roundDecimals, MidpointRounding.AwayFromZero);
                    sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
                    sheet.Cells[rowCount, col++].Value = Math.Round(item.AmountPercent, _roundDecimals, MidpointRounding.AwayFromZero) + " %";
                    rowCount++; col = 1;
                }
                sheet.Cells[rowCount, col++].Value = L("Payment") + " " + L("Detail");
                sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[rowCount, col++].Value = Math.Round(outPut.PaymentList.TotalAmount, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[rowCount, col++].Value = Math.Round(outPut.PaymentList.TotalAmountPercent, _roundDecimals, MidpointRounding.AwayFromZero) + " %";
                sheet.Cells[rowCount, col].Style.Font.Bold = true;
                col = 1;
                rowCount++;
                foreach (var item in outPut.PaymentInsertList.PaymentItems)
                {
                    sheet.Cells[rowCount, col++].Value = item.Name;
                    sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
                    sheet.Cells[rowCount, col++].Value = Math.Round(item.Amount, _roundDecimals, MidpointRounding.AwayFromZero);
                    rowCount++; col = 1;
                }
                sheet.Cells[rowCount, col++].Value = "";
                sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[rowCount, col++].Value = Math.Round(outPut.PaymentInsertList.TotalAmount, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, col++].Value = "";
                sheet.Cells[rowCount, col].Style.Font.Bold = true;
                for (int i = 1; i <= 21; i++) sheet.Column(i).AutoFit();
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }
        private void PrintCategoryItem(ExcelWorksheet sheet, int rowCount, int col, CategoryItem item)
        {
            sheet.Cells[rowCount, col++].Value = ITEM_SPACE + item.Name2;
            foreach (var shift in item.ItemListShift)
            {
                sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[rowCount, col++].Value = Math.Round(shift.ShiftAmount, _roundDecimals, MidpointRounding.AwayFromZero);
            }
            sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
            sheet.Cells[rowCount, col++].Value = Math.Round(item.Compliment, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
            sheet.Cells[rowCount, col++].Value = Math.Round(item.Promotion, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
            sheet.Cells[rowCount, col++].Value = Math.Round(item.TotalSales, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
            sheet.Cells[rowCount, col++].Value = Math.Round(item.ServiceCharge, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
            sheet.Cells[rowCount, col++].Value = Math.Round(item.GST, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
            sheet.Cells[rowCount, col++].Value = Math.Round(item.TotalAll, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
            sheet.Cells[rowCount, col++].Value = Math.Round(item.TotalPercent, _roundDecimals, MidpointRounding.AwayFromZero) + " %";
        }

        private void PrintMontlyRow(MonthlySalesAdvanceResultOutput outPut, ExcelWorksheet sheet, int rowCount, int col, MonthlyRowItem item, string name = null, ExcelHorizontalAlignment alignment = ExcelHorizontalAlignment.Right)
        {
            sheet.Cells[rowCount, col].Style.Font.Bold = true;
            sheet.Cells[rowCount, col].Style.HorizontalAlignment = alignment;
            sheet.Cells[rowCount, col++].Value = name ?? L("SubTotal");
            foreach (var shift in item.ItemListShift)
            {
                sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
                sheet.Cells[rowCount, col++].Value = Math.Round(shift.ShiftAmount, _roundDecimals, MidpointRounding.AwayFromZero);
            }
            sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
            sheet.Cells[rowCount, col++].Value = Math.Round(item.Compliment, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
            sheet.Cells[rowCount, col++].Value = Math.Round(item.Promotion, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
            sheet.Cells[rowCount, col++].Value = Math.Round(item.TotalSales, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
            sheet.Cells[rowCount, col++].Value = Math.Round(item.ServiceCharge, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
            sheet.Cells[rowCount, col++].Value = Math.Round(item.GST, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
            sheet.Cells[rowCount, col++].Value = Math.Round(item.TotalAll, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, col].Style.Numberformat.Format = "0.00";
            sheet.Cells[rowCount, col++].Value = Math.Round(item.TotalPercent, _roundDecimals, MidpointRounding.AwayFromZero) + " %";
        }


        private void PrintSubTotal(MonthlySalesAdvanceResultOutput outPut, ExcelWorksheet sheet, int rowCount, int col, SubTotal item, string name = null, ExcelHorizontalAlignment alignment = ExcelHorizontalAlignment.Right)
        {
            sheet.Cells[rowCount, col].Style.Font.Bold = true;
            sheet.Cells[rowCount, col].Style.HorizontalAlignment = alignment;
            sheet.Cells[rowCount, col++].Value = name ?? L("SubTotal");
            foreach (var shift in item.ListShiftTotal)
            {
                sheet.Cells[rowCount, col++].Value = Math.Round(shift.ShiftAmount, _roundDecimals, MidpointRounding.AwayFromZero);
            }
            sheet.Cells[rowCount, col++].Value = Math.Round(item.SubTotalCompliment, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, col++].Value = Math.Round(item.SubTotalPromotion, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, col++].Value = Math.Round(item.SubTotalTotalSales, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, col++].Value = Math.Round(item.SubTotalServiceCharge, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, col++].Value = Math.Round(item.SubTotalGST, _roundDecimals, MidpointRounding.AwayFromZero);

            sheet.Cells[rowCount, col++].Value = Math.Round(item.SubTotalTotalAll, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, col++].Value = Math.Round(item.SubTotalTotalPercent, _roundDecimals, MidpointRounding.AwayFromZero) + " %";
        }

        private int AddReportHeader(ExcelWorksheet sheet, string nameOfReport, IDateInput input)
        {
            var brandName = "";
            var locationCode = "";
            var locationName = "";
            var branch = "All";
            var isAllLocation = (input.LocationGroup?.Locations?.Count() + input.LocationGroup?.Groups?.Count() + input.LocationGroup?.LocationTags?.Count()) == _locRepo.GetAll().Count();

            if (isAllLocation)
            {
                branch = "All";
            }
            else
            {
                if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
                {
                    branch = input.LocationGroup.Locations.Select(l => l.Name).JoinAsString(", ");
                }
                if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
                {
                    branch = input.LocationGroup.Groups.Select(l => l.Name).JoinAsString(", ");
                }
                if (input.LocationGroup?.LocationTags != null && input.LocationGroup.LocationTags.Any())
                {
                    branch = input.LocationGroup.LocationTags.Select(l => l.Name).JoinAsString(", ");
                }
            }

            Master.Location myLocation = null;

            if (input.Location > 0)
                myLocation = _locRepo.Get(input.Location);

            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var simpleL = input.LocationGroup.Locations.First();
                if (simpleL != null)
                {
                    myLocation = _locRepo.Get(simpleL.Id);
                }
            }

            if (myLocation != null)
            {
                Master.Company myCompany = _comRepo.Get(myLocation.CompanyRefId);
                if (myCompany != null)
                {
                    brandName = myCompany.Name;
                }
                locationCode = myLocation.Code;
                locationName = myLocation.Name;
            }

            var starRow = 1;
            sheet.Cells[starRow, 1].Value = "Lawry’s The Prime Rib Singapore Pte Ltd";
            sheet.Cells[starRow, 1, starRow, 12].Merge = true;
            sheet.Cells[starRow, 1, starRow, 13].Style.Font.Bold = true;
            sheet.Cells[starRow, 1, starRow, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            starRow++;
            sheet.Cells[starRow, 1].Value = nameOfReport;
            sheet.Cells[starRow, 1, starRow, 12].Merge = true;
            sheet.Cells[starRow, 1, starRow, 13].Style.Font.Bold = true;
            sheet.Cells[starRow, 1, starRow, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            starRow++;
            sheet.Cells[starRow, 1].Value = L("Brand");
            sheet.Cells[starRow, 2].Value = brandName;
            sheet.Cells[starRow, 6].Value = "Business Date:";
            sheet.Cells[starRow, 7].Value = input.StartDate.ToString(input.DateFormat) + " to " + input.EndDate.ToString(input.DateFormat);

            starRow++;
            sheet.Cells[starRow, 1].Value = "Branch Name:";
            sheet.Cells[starRow, 2].Value = branch;
            sheet.Cells[starRow, 6].Value = "Printed On:";
            sheet.Cells[starRow, 7].Value = DateTime.Now.ToString(input.DatetimeFormat);

            sheet.Cells[1, 1, 4, 7].Style.Font.Size = 10;

            return 6;
        }
    }
}
