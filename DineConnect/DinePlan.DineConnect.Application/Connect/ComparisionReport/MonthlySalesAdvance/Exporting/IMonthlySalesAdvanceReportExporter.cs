﻿using DinePlan.DineConnect.Connect.ComparisionReport.MonthlySalesAdvance.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.MonthlySalesAdvance.Exporting
{
    public interface IMonthlySalesAdvanceReportExporter
    {
        Task<FileDto> ExportMonthlySalesAdvanceReport(GetTicketInput input, IMonthlySalesAdvanceReportAppService appService);
    }
}
