﻿using Abp.Domain.Repositories;
using Abp.Extensions;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.CashSummaryReport.Dto;
using DinePlan.DineConnect.Connect.ComparisionReport.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.Exporting
{
	public class MealTimeSalesReportExporter : FileExporterBase, IMealTimeSalesReportExporter
	{
		private readonly IRepository<Master.Department> _departmentRepository;
		public MealTimeSalesReportExporter(IRepository<Master.Department> departmentRepository)
		{
			_departmentRepository = departmentRepository;
		}
		public async Task<FileDto> ExportMealTimeReport(GetMealTimeInput input, IMealTimeSalesReportAppService appService)
		{
			var builder = new StringBuilder();
			builder.Append(L("MealTimeReport"));
			builder.Append(L("-"));
			builder.Append(!input.StartDate.Equals(DateTime.MinValue)
				? input.StartDate.ToString(_simpleDateFormat)
				: DateTime.Now.ToString(_simpleDateFormat));
			builder.Append("_");
			builder.Append(!input.EndDate.Equals(DateTime.MinValue)
				? input.EndDate.ToString(_simpleDateFormat)
				: DateTime.Now.ToString(_simpleDateFormat));

			var file = new FileDto(builder + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
			if (input.ExportOutputType == 0)
			{
				using (var excelPackage = new ExcelPackage())
				{
					await GetMealTimeReportExportExcel(excelPackage, input, appService);
					Save(excelPackage, file);
				}
				return ProcessFile(input, file);
			}
			else
			{
				var contentHTML = await ExportDatatableToHtml(input, appService);
				var result = ProcessFileHTML(file, contentHTML);
				return result;
			}
		}
		private FileDto ProcessFileHTML(FileDto file, string fileHTML)
		{
			File.WriteAllText(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken), fileHTML);
			file.FileType = MimeTypeNames.ApplicationXhtmlXml;
			file.FileName = Path.GetFileNameWithoutExtension(file.FileName) + ".html";
			return file;
		}


		private async Task GetMealTimeReportExportExcel(ExcelPackage package, GetMealTimeInput input, IMealTimeSalesReportAppService appService)
		{
			var sheet = package.Workbook.Worksheets.Add(L("MealTimeReport"));
			sheet.OutLineApplyStyle = true;
			var data = await appService.GetMealTimeSalesReports(input);
			if (data != null)
			{

				int rowCount = 1;
				foreach (var location in data.ListItem)
				{
					var totalCell = 3 + location.ScheduleSettings.Count * 3;
					sheet.Cells[rowCount, 1, rowCount, totalCell].Merge = true;
					sheet.Cells[rowCount, 1, rowCount, totalCell].Value = location.LocationName;
					rowCount++;
					var cellHeaderMealTime = 4;
					foreach (var item in location.ScheduleSettings)
					{
						sheet.Cells[rowCount, cellHeaderMealTime].Value = item.Name + " ( " + item.Start_String + "-" + item.End_String + ")";
						sheet.Cells[rowCount, cellHeaderMealTime, rowCount, cellHeaderMealTime + 2].Merge = true;
						sheet.Cells[rowCount, cellHeaderMealTime, rowCount, cellHeaderMealTime + 2].Style.Font.Bold = true;
						sheet.Cells[rowCount, cellHeaderMealTime, rowCount, cellHeaderMealTime + 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
						cellHeaderMealTime += 3;
					}
					rowCount++;
					sheet.Cells[rowCount, 1].Value = L("Category");
					sheet.Cells[rowCount, 2].Value = L("MenuItem");
					sheet.Cells[rowCount, 3].Value = L("Portion");
					var cellHeader = 4;
					foreach (var item in location.ScheduleSettings)
					{
						sheet.Cells[rowCount, cellHeader].Value = L("Quantity");
						sheet.Cells[rowCount, cellHeader + 1].Value = L("Price");
						sheet.Cells[rowCount, cellHeader + 2].Value = L("Amount");
						cellHeader += 3;
					}
					rowCount++;

					foreach (var item in location.ListData)
					{
						sheet.Cells[rowCount, 1].Value = item.CategoryName;
						sheet.Cells[rowCount, 2].Value = item.MenuItemName;
						sheet.Cells[rowCount, 3].Value = item.MenuItemPortionName;
						var cellItem = 4;
						foreach (var i in item.ListItem)
						{
							sheet.Cells[rowCount, cellItem].Value = i.Quantity.ToString(ConnectConsts.ConnectConsts.NumberFormat);
							sheet.Cells[rowCount, cellItem + 1].Value = i.Price.ToString(ConnectConsts.ConnectConsts.NumberFormat);
							sheet.Cells[rowCount, cellItem + 2].Value = i.Total.ToString(ConnectConsts.ConnectConsts.NumberFormat);
							cellItem += 3;
						}
						rowCount++;
					}
					for (var i = 1; i <= totalCell; i++)
					{
						sheet.Column(i).AutoFit();
					}
					rowCount++;
				}

			}
		}

		private async Task<string> ExportDatatableToHtml(GetMealTimeInput input, IMealTimeSalesReportAppService appService)
		{
			var dtos = await appService.GetMealTimeSalesReports(input);
			var formatDateSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.SimpleDateFormat);
			StringBuilder strHTMLBuilder = new StringBuilder();
			strHTMLBuilder.Append("<html>");
			strHTMLBuilder.Append("<head>");
			strHTMLBuilder.Append("</head>");

			strHTMLBuilder.Append(@"<style> 
             body  { 
              font:400 12px 'Tahoma';
              font-size: 12px;
              padding:10px;
            }
            table  { 
                margin-top: 10px;
                border-collapse: collapse;
                width: 100%;
                margin-bottom: 10px;
            }

            table td, table th {
                border: 1px solid #ddd;
                padding: 8px;
            }
            table th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                color: white;
            }
            thead {
                display: table-header-group;
            }
            tfoot {
                display: table-row-group;
            }
            tr{
                page-break-inside: avoid;
            }
            blockquote {
              color:white;
              text-align:center;
            }

            h3{
              text-align:center;
              text-transform: capitalize;
              font-size: medium;
            }

			 .thead_title {
				background-color: #deebf7;
				font-weight: bold;
			}

			.thead_content {
				background-color: #fff2cc;
			}

			.footer_item {
				background-color: #d6dce5;
				font-weight: bold;
			}

			.footer {
				background-color: #e2f0d9;
				font-weight: bold;
			}

			.margin_bottom_0 {
				margin-bottom: 0 !important;
			}

			.border_bottom_none {
				border-bottom: none !important;
			}

			.custom_table_date {
				margin-bottom: 0px !important;
			}

			.display_flex {
				display: flex;
			}

			.item {
				max-width: 116px;
			}

			.w-100 {
				width: 100%;
			}


			.float-right {
				float: right;
			}

			.thead_title {
				background-color: #008000;
				font-weight: bold;
				color: white;
			}

			.thead_content {
				background-color: #808080;
				color: white;
			}

			.footer_item {
				background-color: #d6dce5;
				font-weight: bold;
			}

			.footer_str {
				display: flex;
				padding: 10px;
			}

			.footer {
				background-color: #e2f0d9;
				font-weight: bold;
			}

			.color_header {
				background-color: #808080;
				color: white;
			}

			.font_weight_bold {
				font-weight: bold;
			}

			.nodata {
				background: navajowhite;
				color: red;
			}

			.table_Item {
				border: none !important;
				margin: 0px !important;
				padding: 0px !important;
			}

			.table_Item_Element {
				border-left: none !important;
				border-bottom: none !important;
			}

			.max_width_50 {
				max-width: 50px;
			}

			.max_width_20 {
				max-width: 20px;
			}
            .text-center{
			  text-align: center;           
			}
            </style>");

			strHTMLBuilder.Append("<body>");

			foreach (var location in dtos.ListItem)
			{
				strHTMLBuilder.Append("<div>");
				strHTMLBuilder.Append("<table class='table table-bordered table-condensed'>");
				strHTMLBuilder.Append("<thead> <tr class='thead_title'>");
				strHTMLBuilder.Append($"<th colspan = '22' class='border_bottom_none font_weight_bold'>{location.LocationName}</th> </thead> ");
			
				strHTMLBuilder.Append("<thead> <tr class='thead_content'>");
				strHTMLBuilder.Append($"<th colspan = '3'></th>");
				foreach (var schedule in location.ScheduleSettings)
				{
					strHTMLBuilder.Append($"<th colspan='3' class='text-center font_weight_bold'>{schedule.Name} <br />  {schedule.Start_String} - {schedule.End_String}  </th>");
				}
				strHTMLBuilder.Append("</tr>");
				strHTMLBuilder.Append("<tr class='thead_content'>");
				strHTMLBuilder.Append($"<th class='text-center max_width_50'>{L("Category")}</th>");
				strHTMLBuilder.Append($"<th class='text-center max_width_50'>{L("MenuItem")}</th>");
				strHTMLBuilder.Append($"<th class='text-center max_width_50'>{L("Portion")}</th>");
				foreach (var schedule in location.ScheduleSettings)
				{
					strHTMLBuilder.Append($"<th colspan='3' style='margin: 0px!important; padding: 0px!important; border: none!important'>");
					strHTMLBuilder.Append($"<table class='table table-bordered table_Item'> <tr> ");
					strHTMLBuilder.Append($"<th class='text-center max_width_20'>{L("Quantity")}</th>");
					strHTMLBuilder.Append($"<th class='text-center max_width_20'>{L("Price")}</th>");
					strHTMLBuilder.Append($"<th class='text-center max_width_20'>{L("Amount")}</th>");
					strHTMLBuilder.Append("</tr> </table>");
				}
				strHTMLBuilder.Append($"</th> </tr> </thead>");

				// content
				foreach (var item in location.ListData)
				{
					strHTMLBuilder.Append($"<tr>");
					strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_50'>{item.CategoryName}</td>");
					strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_50'>{item.MenuItemName}</td>");
					strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_50'>{item.MenuItemPortionName}</td>");
					foreach (var mealTime in item.ListItem)
					{
						strHTMLBuilder.Append($"<td colspan='3' style='margin: 0px!important; padding: 0px!important; border: none!important'>");
						strHTMLBuilder.Append($"<table class='table table-bordered table_Item'> <tr>");
						strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_20'>{mealTime.Quantity.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
						strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_20'>{mealTime.Price.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
						strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_20'>{mealTime.Total.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
						strHTMLBuilder.Append($"</tr> </table> </td>");
					}
					strHTMLBuilder.Append("</tr>");
				}
				//Close tags.  
				strHTMLBuilder.Append("</table>");
			}

			strHTMLBuilder.Append("</body>");
			strHTMLBuilder.Append("</html>");

			string Htmltext = strHTMLBuilder.ToString();
			return Htmltext;
		}

		public async Task<FileDto> ExportMealTimeReportByCategory(GetMealTimeInput input, IMealTimeSalesReportAppService appService)
		{
			var builder = new StringBuilder();
			builder.Append(L("MealTimeReport"));
			builder.Append(L("-"));
			builder.Append(!input.StartDate.Equals(DateTime.MinValue)
				? input.StartDate.ToString(_simpleDateFormat)
				: DateTime.Now.ToString(_simpleDateFormat));
			builder.Append("_");
			builder.Append(!input.EndDate.Equals(DateTime.MinValue)
				? input.EndDate.ToString(_simpleDateFormat)
				: DateTime.Now.ToString(_simpleDateFormat));

			var file = new FileDto(builder + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
			if (input.ExportOutputType == 0)
			{
				using (var excelPackage = new ExcelPackage())
				{
					await GetMealTimeReportByCategoryExportExcel(excelPackage, input, appService);
					Save(excelPackage, file);
				}
				return ProcessFile(input, file);
			}
			else
			{
				var contentHTML = await ExportMealTimeByCategoryToHtml(input, appService);
				var result = ProcessFileHTML(file, contentHTML);
				return result;
			}
		}
		private async Task GetMealTimeReportByCategoryExportExcel(ExcelPackage package, GetMealTimeInput input, IMealTimeSalesReportAppService appService)
		{
			var sheet = package.Workbook.Worksheets.Add(L("MealTimeReport"));
			sheet.OutLineApplyStyle = true;
			var data = await appService.GetMealTimeSalesReports(input);
			if (data != null)
			{

				int rowCount = 1;
				foreach (var location in data.ListItem)
				{
					var totalCell = 3 + location.ScheduleSettings.Count * 3;
					sheet.Cells[rowCount, 1, rowCount, totalCell].Merge = true;
					sheet.Cells[rowCount, 1, rowCount, totalCell].Style.Font.Bold = true;
					sheet.Cells[rowCount, 1, rowCount, totalCell].Value = location.LocationName;
					rowCount++;
					var cellHeaderMealTime = 4;
					foreach (var item in location.ScheduleSettings.OrderBy(x=>x.Start_Number))
					{
						sheet.Cells[rowCount, cellHeaderMealTime].Value = item.Name + " ( " + item.Start_String + "-" + item.End_String + ")";
						sheet.Cells[rowCount, cellHeaderMealTime, rowCount, cellHeaderMealTime + 2].Merge = true;
						sheet.Cells[rowCount, cellHeaderMealTime, rowCount, cellHeaderMealTime + 2].Style.Font.Bold = true;
						sheet.Cells[rowCount, cellHeaderMealTime, rowCount, cellHeaderMealTime + 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
						cellHeaderMealTime += 3;
					}
					rowCount++;
					sheet.Cells[rowCount, 1].Value = L("Category");
					sheet.Cells[rowCount, 2].Value = L("MenuItem");
					sheet.Cells[rowCount, 3].Value = L("Portion");
					var cellHeader = 4;
					foreach (var item in location.ScheduleSettings)
					{
						sheet.Cells[rowCount, cellHeader].Value = L("Quantity");
						sheet.Cells[rowCount, cellHeader + 1].Value = L("Price");
						sheet.Cells[rowCount, cellHeader + 2].Value = L("Amount");
						cellHeader += 3;
					}
					rowCount++;

					// Group data by Category
					var dataByCategory = location.ListData.GroupBy(x => x.CategoryName).Select(x => new
					{
						CategoryName = x.Key,
						ListData = x
					});

					foreach(var category in dataByCategory)
					{
						foreach (var item in category.ListData)
						{
							sheet.Cells[rowCount, 1].Value = item.CategoryName;
							sheet.Cells[rowCount, 2].Value = item.MenuItemName;
							sheet.Cells[rowCount, 3].Value = item.MenuItemPortionName;
							var cellItem = 4;
							foreach (var i in item.ListItem)
							{
								sheet.Cells[rowCount, cellItem].Value = i.Quantity.ToString(ConnectConsts.ConnectConsts.NumberFormat);
								sheet.Cells[rowCount, cellItem + 1].Value = i.Price.ToString(ConnectConsts.ConnectConsts.NumberFormat);
								sheet.Cells[rowCount, cellItem + 2].Value = i.Total.ToString(ConnectConsts.ConnectConsts.NumberFormat);
								cellItem += 3;
							}
							rowCount++;
						}

						// Total by category
						sheet.Cells[rowCount, 1].Value = "Total " + category.CategoryName;
						// filter by shift name 
						var TotalQuantity = category.ListData.Sum(x => x.ListItem.Sum(a => a.Quantity));
						var cellTotal = 4;

						foreach (var shift in location.ScheduleSettings.OrderBy(x => x.Start_Number))
						{
							var dataByShift = category.ListData.SelectMany(x => x.ListItem).Where(x => x.Name == shift.Name);
							sheet.Cells[rowCount, cellTotal].Value = dataByShift.Sum(x=>x.Quantity).ToString(ConnectConsts.ConnectConsts.NumberFormat);
							sheet.Cells[rowCount, cellTotal + 1].Value = dataByShift.Sum(x => x.Price).ToString(ConnectConsts.ConnectConsts.NumberFormat);
							sheet.Cells[rowCount, cellTotal + 2].Value = dataByShift.Sum(x => x.Total).ToString(ConnectConsts.ConnectConsts.NumberFormat);
							cellTotal += 3;
						}

						for(var i = 1;i<= totalCell; i++)
						{
							sheet.Cells[rowCount, i].Style.Font.Bold = true;
						}

						rowCount++;
					}

					for (var i = 1; i <= totalCell; i++)
					{
						sheet.Column(i).AutoFit();
					}
					rowCount++;
				}

			}
		}

		private async Task<string> ExportMealTimeByCategoryToHtml(GetMealTimeInput input, IMealTimeSalesReportAppService appService)
		{
			var dtos = await appService.GetMealTimeSalesReports(input);
			var formatDateSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.SimpleDateFormat);
			StringBuilder strHTMLBuilder = new StringBuilder();
			strHTMLBuilder.Append("<html>");
			strHTMLBuilder.Append("<head>");
			strHTMLBuilder.Append("</head>");

			strHTMLBuilder.Append(@"<style> 
             body  { 
              font:400 12px 'Tahoma';
              font-size: 12px;
              padding:10px;
            }
            table  { 
                margin-top: 10px;
                border-collapse: collapse;
                width: 100%;
                margin-bottom: 10px;
            }

            table td, table th {
                border: 1px solid #ddd;
                padding: 8px;
            }
            table th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                color: white;
            }
            thead {
                display: table-header-group;
            }
            tfoot {
                display: table-row-group;
            }
            tr{
                page-break-inside: avoid;
            }
            blockquote {
              color:white;
              text-align:center;
            }

            h3{
              text-align:center;
              text-transform: capitalize;
              font-size: medium;
            }

			 .thead_title {
				background-color: #deebf7;
				font-weight: bold;
			}

			.thead_content {
				background-color: #fff2cc;
			}

			.footer_item {
				background-color: #d6dce5;
				font-weight: bold;
			}

			.footer {
				background-color: #e2f0d9;
				font-weight: bold;
			}

			.margin_bottom_0 {
				margin-bottom: 0 !important;
			}

			.border_bottom_none {
				border-bottom: none !important;
			}

			.custom_table_date {
				margin-bottom: 0px !important;
			}

			.display_flex {
				display: flex;
			}

			.item {
				max-width: 116px;
			}

			.w-100 {
				width: 100%;
			}


			.float-right {
				float: right;
			}

			.thead_title {
				background-color: #008000;
				font-weight: bold;
				color: white;
			}

			.thead_content {
				background-color: #808080;
				color: white;
			}

			.footer_item {
				background-color: #d6dce5;
				font-weight: bold;
			}

			.footer_str {
				display: flex;
				padding: 10px;
			}

			.footer {
				background-color: #e2f0d9;
				font-weight: bold;
			}

			.color_header {
				background-color: #808080;
				color: white;
			}

			.font_weight_bold {
				font-weight: bold;
			}

			.nodata {
				background: navajowhite;
				color: red;
			}

			.table_Item {
				border: none !important;
				margin: 0px !important;
				padding: 0px !important;
			}

			.table_Item_Element {
				border-left: none !important;
				border-bottom: none !important;
			}

			.max_width_50 {
				max-width: 50px;
			}

			.max_width_20 {
				max-width: 20px;
			}
            .text-center{
			  text-align: center;           
			}
            </style>");

			strHTMLBuilder.Append("<body>");


			foreach (var location in dtos.ListItem)
			{
				strHTMLBuilder.Append("<div>");
				strHTMLBuilder.Append("<table class='table table-bordered table-condensed'>");
				strHTMLBuilder.Append("<thead> <tr class='thead_title'>");
				strHTMLBuilder.Append($"<th colspan = '22' class='border_bottom_none font_weight_bold'>{location.LocationName}</th> </thead> ");

				strHTMLBuilder.Append("<thead> <tr class='thead_content'>");
				strHTMLBuilder.Append($"<th colspan = '3'></th>");
				foreach (var schedule in location.ScheduleSettings)
				{
					strHTMLBuilder.Append($"<th colspan='3' class='text-center font_weight_bold'>{schedule.Name} <br />  {schedule.Start_String} - {schedule.End_String}  </th>");
				}
				strHTMLBuilder.Append("</tr>");
				strHTMLBuilder.Append("<tr class='thead_content'>");
				strHTMLBuilder.Append($"<th class='text-center max_width_50'>{L("Category")}</th>");
				strHTMLBuilder.Append($"<th class='text-center max_width_50'>{L("MenuItem")}</th>");
				strHTMLBuilder.Append($"<th class='text-center max_width_50'>{L("Portion")}</th>");

				foreach (var schedule in location.ScheduleSettings)
				{
					strHTMLBuilder.Append($"<th colspan='3' style='margin: 0px!important; padding: 0px!important; border: none!important'>");
					strHTMLBuilder.Append($"<table class='table table-bordered table_Item'> <tr> ");
					strHTMLBuilder.Append($"<th class='text-center max_width_20'>{L("Quantity")}</th>");
					strHTMLBuilder.Append($"<th class='text-center max_width_20'>{L("Price")}</th>");
					strHTMLBuilder.Append($"<th class='text-center max_width_20'>{L("Amount")}</th>");
					strHTMLBuilder.Append("</tr> </table>");
				}
				strHTMLBuilder.Append($"</th> </tr> </thead>");

				// content

				// Group data by Category
				var dataByCategory = location.ListData.GroupBy(x => x.CategoryName).Select(x => new
				{
					CategoryName = x.Key,
					ListData = x
				});

				foreach (var category in dataByCategory)
				{
					foreach (var item in category.ListData)
					{
						strHTMLBuilder.Append($"<tr>");
						strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_50'>{item.CategoryName}</td>");
						strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_50'>{item.MenuItemName}</td>");
						strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_50'>{item.MenuItemPortionName}</td>");
						foreach (var mealTime in item.ListItem)
						{
							strHTMLBuilder.Append($"<td colspan='3' style='margin: 0px!important; padding: 0px!important; border: none!important'>");
							strHTMLBuilder.Append($"<table class='table table-bordered table_Item'> <tr>");
							strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_20'>{mealTime.Quantity.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
							strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_20'>{mealTime.Price.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
							strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_20'>{mealTime.Total.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
							strHTMLBuilder.Append($"</tr> </table> </td>");
						}
						strHTMLBuilder.Append("</tr>");
					}

					//TODO: Total by category
					strHTMLBuilder.Append($"<tr>");
					strHTMLBuilder.Append($"<td class='item border_bottom_none max_width_50 font_weight_bold' colspan='3'> Total {category.CategoryName}</td>");

					foreach (var shift in location.ScheduleSettings.OrderBy(x => x.Start_Number))
					{
						var dataByShift = category.ListData.SelectMany(x => x.ListItem).Where(x => x.Name == shift.Name);
						strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_20 font_weight_bold'>  {dataByShift.Sum(x => x.Quantity).ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
						strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_20 font_weight_bold'>  {dataByShift.Sum(x => x.Price).ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
						strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_20 font_weight_bold'>  {dataByShift.Sum(x => x.Total).ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
					}
					strHTMLBuilder.Append("</tr>");
				}
				//Close tags.  
				strHTMLBuilder.Append("</table>");
			}

			strHTMLBuilder.Append("</body>");
			strHTMLBuilder.Append("</html>");

			string Htmltext = strHTMLBuilder.ToString();
			return Htmltext;
		}
	}
}