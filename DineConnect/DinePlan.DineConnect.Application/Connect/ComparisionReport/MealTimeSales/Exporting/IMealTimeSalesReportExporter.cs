﻿using DinePlan.DineConnect.Connect.ComparisionReport;
using DinePlan.DineConnect.Connect.ComparisionReport.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.Exporting
{
    public interface IMealTimeSalesReportExporter
    {
        Task<FileDto> ExportMealTimeReport(GetMealTimeInput input, IMealTimeSalesReportAppService appService);
        Task<FileDto> ExportMealTimeReportByCategory(GetMealTimeInput input, IMealTimeSalesReportAppService appService);
    }
}