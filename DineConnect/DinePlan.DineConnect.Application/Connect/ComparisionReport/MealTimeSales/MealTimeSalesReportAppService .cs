﻿using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.CashSummaryReport.Exporting;
using DinePlan.DineConnect.Connect.ComparisionReport.Dto;
using DinePlan.DineConnect.Connect.ComparisionReport.Exporting;
using DinePlan.DineConnect.Connect.ComparisionReport.MealTimeSales.Dto;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Setting;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport
{
	public class MealTimeSalesReportAppService : DineConnectAppServiceBase, IMealTimeSalesReportAppService
	{
		private readonly IRepository<Master.Location> _lRepo;
		private readonly IReportBackgroundAppService _rbas;
		private readonly IBackgroundJobManager _bgm;
		private readonly IMealTimeSalesReportExporter _exporter;
		private readonly IConnectReportAppService _connectReportAppService;
		public MealTimeSalesReportAppService(
			IRepository<Master.Location> lRepo,
			IReportBackgroundAppService rbas,
			IBackgroundJobManager bgm,
			IMealTimeSalesReportExporter exporter,
			IConnectReportAppService connectReportAppService
			)
		{
			_lRepo = lRepo;
			_rbas = rbas;
			_bgm = bgm;
			_exporter = exporter;
			_connectReportAppService = connectReportAppService;
		}

		public async Task<GetMealTimeOutputDto> GetMealTimeSalesReports(GetMealTimeInput input)
		{
			var result = new GetMealTimeOutputDto();
			var data = new List<MealTimeDto>();
			var itemInput = new GetItemInput()
			{
				MaxResultCount = 10000,
				StartDate = input.StartDate,
				EndDate = input.EndDate,
				LocationGroup = input.LocationGroup
			};
			var itemHourlySales = await _connectReportAppService.GetItemHourlySales(itemInput);
			
			var outItem = itemHourlySales.MenuList.Items.GroupBy(c => new
			{
				c.MenuItemName,
				c.MenuItemPortionName,
				c.CategoryName,
				c.LocationName
			}).Select(gcs => new
			{
				gcs.Key.CategoryName,
				gcs.Key.MenuItemName,
				gcs.Key.MenuItemPortionName,
				gcs.Key.LocationName,
				Children = gcs.ToList(),
			});

			var outItemGroupByLocation = outItem.GroupBy(x => x.LocationName).Select(x => new
			{
				Location = x.Key,
				Children = x.ToList()
			});

			var scheduleFromSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.Schedules);
			var templateSeting = (JsonConvert.DeserializeObject<List<LocationSchdule>>(scheduleFromSetting));

			var lstShiftByLocation = new List<MealTimeByLocation>();
			foreach (var location in outItemGroupByLocation)
			{
				var mealTimeByLocation = new MealTimeByLocation();
				mealTimeByLocation.LocationName = location.Location;
				var scheduleSetting = new List<LocationSchdule>();
			    var locationDetail = await _lRepo.GetAll().Include(x => x.Schedules).Where(x => x.Name == location.Location).FirstOrDefaultAsync();
				if(locationDetail != null && locationDetail.Schedules.Count > 0)
				{
					scheduleSetting = locationDetail.Schedules.ToList();
				}	else
				{
					scheduleSetting = templateSeting;
				}
				var listShift = scheduleSetting.MapTo<List<ShiftDto>>();
				mealTimeByLocation.ScheduleSettings = listShift.OrderBy(x => x.Start_Number).ToList();
				var lstMealTime = new List<MealTimeDto>();
				 foreach (var mealTime in location.Children)
				{
					var mealItem = new MealTimeDto();
					mealItem.CategoryName = mealTime.CategoryName;
					mealItem.MenuItemName = mealTime.MenuItemName;
					mealItem.MenuItemPortionName = mealTime.MenuItemPortionName;
					mealItem.LocationName = mealTime.LocationName;

					var lstMealTimeInfo = new List<ShiftInformation>();
					foreach (var shift in listShift.OrderBy(x=>x.StartHour))
					{
						var shiftInfo = new ShiftInformation();
						var da = mealTime.Children.Where(x => x.Hour >= shift.Start_Number && x.Hour <= shift.End_Number);
						shiftInfo.Price = mealTime.Children.FirstOrDefault().Price;
						shiftInfo.Quantity = da.Sum(x => x.Quantity);
						shiftInfo.Name = shift.Name;
						lstMealTimeInfo.Add(shiftInfo);
					}
					mealItem.ListItem = lstMealTimeInfo;
					lstMealTime.Add(mealItem);
				}
				mealTimeByLocation.ListData = lstMealTime;
				lstShiftByLocation.Add(mealTimeByLocation);
			}
			result.ListItem = lstShiftByLocation;
			return await Task.FromResult(result);
		}

		public async Task<FileDto> GetMealTimeSalesReportToExport(GetMealTimeInput input)
		{
			if (AbpSession.UserId != null && AbpSession.TenantId != null)
			{
				input.MaxResultCount = 10000;

				if (input.RunInBackground)
				{
					var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
					{
						ReportName = ReportNames.MEALTIMESALE,
						Completed = false,
						UserId = AbpSession.UserId.Value,
						TenantId = AbpSession.TenantId.Value,
						ReportDescription = input.ReportDescription
					});

					if (backGroundId > 0)
					{
						var tInput = new GetMealTimeReportInput
						{
							GetMealTimeInput = input,
						};
						await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
						{
							BackGroundId = backGroundId,
							ReportName = ReportNames.MEALTIMESALE,
							MealTimeReportInput = tInput,
							UserId = AbpSession.UserId.Value,
							TenantId = AbpSession.TenantId.Value
						});
					}
				}
				else
				{
					var output = await _exporter.ExportMealTimeReport(input, this);
					return output;
				}
			}
			return null;
		}


		public async Task<FileDto> GetMealTimeSalesReportByCategoryToExport(GetMealTimeInput input)
		{
			if (AbpSession.UserId != null && AbpSession.TenantId != null)
			{
				input.MaxResultCount = 10000;

				if (input.RunInBackground)
				{
					var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
					{
						ReportName = ReportNames.MEALTIMESALE,
						Completed = false,
						UserId = AbpSession.UserId.Value,
						TenantId = AbpSession.TenantId.Value,
						ReportDescription = input.ReportDescription
					});

					if (backGroundId > 0)
					{
						var tInput = new GetMealTimeReportInput
						{
							GetMealTimeInput = input,
						};
						await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
						{
							BackGroundId = backGroundId,
							ReportName = ReportNames.MEALTIMESALE,
							MealTimeReportInput = tInput,
							UserId = AbpSession.UserId.Value,
							TenantId = AbpSession.TenantId.Value
						});
					}
				}
				else
				{
					var output = await _exporter.ExportMealTimeReportByCategory(input, this);
					return output;
				}
			}
			return null;
		}

	}
}
