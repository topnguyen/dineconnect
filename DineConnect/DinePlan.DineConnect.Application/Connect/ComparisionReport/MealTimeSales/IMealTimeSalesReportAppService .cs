﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.ComparisionReport.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport
{
	public interface IMealTimeSalesReportAppService : IApplicationService
	{
		Task<GetMealTimeOutputDto> GetMealTimeSalesReports(GetMealTimeInput input);
		Task<FileDto> GetMealTimeSalesReportToExport(GetMealTimeInput input);
		Task<FileDto> GetMealTimeSalesReportByCategoryToExport(GetMealTimeInput input);
	}
}
