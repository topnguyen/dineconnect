﻿using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Master;
using System;

namespace DinePlan.DineConnect.Connect.ComparisionReport.MealTimeSales.Dto
{
	[AutoMap(typeof(LocationSchdule))]
	public class ShiftDto
	{
		public virtual string Name { get; set; }
		public virtual int StartHour { get; set; }
		public virtual int StartMinute { get; set; }
		public virtual int EndHour { get; set; }
		public virtual int EndMinute { get; set; }
		public double Start_Number => Convert.ToDouble(StartHour + StartMinute / 60);
		public double End_Number => Convert.ToDouble(EndHour + EndMinute / 60);
		public string Start_String => StartHour +":"+StartMinute;
		public string End_String => EndHour +":"+EndMinute;
	}
}
