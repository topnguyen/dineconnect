﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.Dto
{
	public class MealTimeSettingDto
	{
		public string Name { get; set; }
		public string From { get; set; }
		public string To { get; set; }

		public TimeSpan From_TimeSpan
		{
			get
			{
				var timeSpan = new TimeSpan();
				var times = From.Split(':');
				timeSpan = TimeSpan.FromHours(double.Parse(times[0]));
				timeSpan += TimeSpan.FromMinutes(double.Parse(times[1]));
				timeSpan += TimeSpan.FromSeconds(double.Parse(times[2]));
				return timeSpan;
			}
		}
		public TimeSpan To_TimeSpan
		{
			get
			{
				var timeSpan = new TimeSpan();
				var times = To.Split(':');
				timeSpan = TimeSpan.FromHours(double.Parse(times[0]));
				timeSpan += TimeSpan.FromMinutes(double.Parse(times[1]));
				timeSpan += TimeSpan.FromSeconds(double.Parse(times[2]));
				return timeSpan;
			}
		}
		public double From_Number
		{
			get
			{
				var times = From.Split(':');
				var result = Convert.ToDouble(double.Parse(times[0]) + (double.Parse(times[1]) / 60));
				return result;
			}
		}
		public double To_Number
		{
			get
			{
				var times = To.Split(':');
				return Convert.ToDouble(double.Parse(times[0]) + (double.Parse(times[1]) / 60));
			}
		}
	}

}
