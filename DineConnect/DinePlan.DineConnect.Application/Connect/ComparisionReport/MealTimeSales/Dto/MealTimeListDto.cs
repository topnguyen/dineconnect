﻿using DinePlan.DineConnect.Connect.ComparisionReport.MealTimeSales.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.Dto
{
	public class GetMealTimeOutputDto
	{
	  public List<MealTimeByLocation> ListItem { get; set; }
		public GetMealTimeOutputDto()
		{
			ListItem = new List<MealTimeByLocation>();
		}
	}


	public class MealTimeByLocation
	{
		public string LocationName { get; set; }
		public List<MealTimeDto> ListData { get; set; }
		public List<ShiftDto> ScheduleSettings { get; set; }   

		public MealTimeByLocation()
		{
			ListData = new List<MealTimeDto>();
			ScheduleSettings = new List<ShiftDto>();
		}
	}
	public class MealTimeDto
	{
		public string LocationName { get; set; }
		public string MenuItemName { get; set; }
		public string CategoryName { get; set; }
		public string MenuItemPortionName { get; set; }
		public List<ShiftInformation> ListItem { get; set; }
		public MealTimeDto()
		{
			ListItem = new List<ShiftInformation>();
		}
	}

	public class ShiftInformation
	{
		public string Name { get; set; }
		[DataType("decimal(16 ,3")]
		public decimal Price { get; set; }

		[DataType("decimal(16 ,3")]
		public decimal Quantity { get; set; }

		[DataType("decimal(16 ,3")]
		public decimal Total => Price * Quantity;
	}
}
