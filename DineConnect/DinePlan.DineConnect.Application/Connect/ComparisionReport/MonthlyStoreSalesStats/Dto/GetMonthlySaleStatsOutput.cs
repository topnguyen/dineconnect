﻿using System.Collections.Generic;
using System.Linq;

namespace DinePlan.DineConnect.Connect.ComparisionReport.MonthlyStoreSalesStats.Dto
{
	public class GetMonthlySaleStatsOutput
	{
		public GetDataByDeparmetGroup DepartmentGroup_1 { get; set; }
		public GetDataByDeparmetGroup DepartmentGroup_2 { get; set; }
		public List<string> Locations { get; set; }
		public string Delivery { get; set; }
		public string Tickets { get; set; }
		public string AOV { get; set; }
	}

	public class GetDataByDeparmetGroup
	{
		public string DepartmentGroup { get; set; }
		public string DepartmentGroupWithLocation { get; set; }
		public List<GetDataByDeparmetWithLocation> ListItem { get; set; }
		public GetDataByDeparmetGroup()
		{
			ListItem = new List<GetDataByDeparmetWithLocation>();
		}
	}
	public class GetDataByDeparmetWithLocation
	{
		public string Department { get; set; }
		public string DepartmentWithLocation { get; set; }
		public string DepartmentGroup { get; set; }
		public List<GetDataByLocation> ListItem { get; set; }
		public decimal TotalAmount
		{
			get
			{
				var result = 0M;
				if (ListItem.Count > 0)
				{
					result = ListItem.Sum(s => s.TotalAmount);
				}
				return result;
			}
		}
	}

	public class GetDataByLocation
	{
		public string Location { get; set; }
		public string Department { get; set; }
		public string DepartmentGroup { get; set; }
		public decimal TotalAmount { get; set; }
		public int Count { get; set; }
		public decimal Percent { get; set; }

	}
	public class GetMonthlySalesByAllDepartment
	{
		public List<string> Locations { get; set; }
		public string TotalTickets { get; set; }
		public string ListDepartment { get; set; }
		public string ListDepartmentPercent { get; set; }

	}

	public class MonthlySale_Department
	{
		public string DepartmentName { get; set; }
	}

}
