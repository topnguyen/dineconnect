﻿using Abp.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;
namespace DinePlan.DineConnect.Connect.ComparisionReport.MonthlyStoreSalesStats.Dto
{
	public class GetMonthlyStoreSalesStatsReportInput: MaxPagedAndSortedInputDto, IShouldNormalize, IDateInput, IFileExport, IBackgroundExport
    {
        public GetMonthlyStoreSalesStatsReportInput()
        {
            Locations = new List<SimpleLocationDto>();
            LocationGroup = new LocationGroupDto();
        }
        public string DepartmentGroupName_1 { get; set; }
        public string DepartmentGroupName_2 { get; set; }
        public string OutputType { get; set; }
        public string ExportType { get; set; }
        public bool ByLocation { get; set; }
        public string Duration { get; set; }
        public int TenantId { get; set; }
        public string ExportOutput { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Location { get; set; }
        public long UserId { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public bool NotCorrectDate { get; set; }
        public bool Credit { get; set; }
        public bool Refund { get; set; }
        public string TicketNo { get; set; }
        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }

        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "Id";
            }
        }

        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }
        public FileDto FileDto { get; set; }
        public bool ReportDisplay { get; set; }
        public string DateFormat { get; set; }
        public string DatetimeFormat { get; set; }
    }
}
