﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.ComparisionReport.MonthlyStoreSalesStats.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.MonthlyStoreSalesStats
{
	public interface IMonthlySalesStatsReportAppService : IApplicationService
	{
		//TODO: comparison 2 department group
		Task<GetMonthlySaleStatsOutput> GetMonthlyStoreSalesReports(GetMonthlyStoreSalesStatsReportInput input);
		Task<FileDto> GetMonthlyStoreSalesReportToExport(GetMonthlyStoreSalesStatsReportInput input);
		// TODO: get all department and calculator percent  
		Task<GetMonthlySalesByAllDepartment> GetMonthlyStoreSalesReportByAllDepartment(GetMonthlyStoreSalesStatsReportInput input);

	}
}
