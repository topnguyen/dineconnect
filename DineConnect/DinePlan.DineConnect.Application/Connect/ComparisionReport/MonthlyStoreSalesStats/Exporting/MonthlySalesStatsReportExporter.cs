﻿using Abp.Domain.Repositories;
using Abp.Extensions;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.CashSummaryReport.Dto;
using DinePlan.DineConnect.Connect.ComparisionReport.Dto;
using DinePlan.DineConnect.Connect.ComparisionReport.MonthlyStoreSalesStats.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.MonthlyStoreSalesStats.Exporting
{
	public class MonthlySalesStatsReportExporter : FileExporterBase, IMonthlySalesStatsReportExporter
	{
		private readonly IRepository<Master.Department> _departmentRepository;
		private readonly IRepository<Master.Location> _locationRepository;
		public MonthlySalesStatsReportExporter(IRepository<Master.Department> departmentRepository,
			IRepository<Master.Location> locationRepository)
		{
			_departmentRepository = departmentRepository;
			_locationRepository = locationRepository;
		}
		public async Task<FileDto> ExportMonthlyStoreSalesStatsReport(GetMonthlyStoreSalesStatsReportInput input, IMonthlySalesStatsReportAppService appService)
		{
			var builder = new StringBuilder();
			builder.Append(L("MonthlyStoreSalesStats"));
			builder.Append(L("-"));
			builder.Append(!input.StartDate.Equals(DateTime.MinValue)
				? input.StartDate.ToString(_simpleDateFormat)
				: DateTime.Now.ToString(_simpleDateFormat));
			builder.Append("_");
			builder.Append(!input.EndDate.Equals(DateTime.MinValue)
				? input.EndDate.ToString(_simpleDateFormat)
				: DateTime.Now.ToString(_simpleDateFormat));

			var file = new FileDto(builder + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
			using (var excelPackage = new ExcelPackage())
			{
				//Case 1: Comparesion 2 location
				if(string.IsNullOrEmpty(input.DepartmentGroupName_1) || string.IsNullOrEmpty(input.DepartmentGroupName_2))
				{
					await GetMonthlySoteSaleReportByAllDepartmentExportExcel(excelPackage, input, appService);
				}	else
				{
					await GetMonthlySoteSaleReportExportExcel(excelPackage, input, appService);
				}
				Save(excelPackage, file);
			}
			return ProcessFile(input, file);
		}

		private async Task GetMonthlySoteSaleReportExportExcel(ExcelPackage package, GetMonthlyStoreSalesStatsReportInput input, IMonthlySalesStatsReportAppService appService)
		{
			var sheet = package.Workbook.Worksheets.Add(L("MonthlySalesStats"));
			sheet.OutLineApplyStyle = true;
			var data = await appService.GetMonthlyStoreSalesReports(input);
			var locations = data.Locations;

			if (data != null)
			{
				int rowCount = 1;
				var headersStart = new List<string>{ L("Sales") };

				var headers = headersStart.Concat(locations);
				var cell_Header = 1;
				var totalCell = headers.Count();
				foreach (var item in headers)
				{
					sheet.Cells[rowCount, cell_Header].Value = item;
					cell_Header++;
				}
				sheet.Cells[rowCount, 1, rowCount, totalCell].Style.Font.Bold = true;
				rowCount++;

				foreach (var department in data.DepartmentGroup_1.ListItem)
				{
					sheet.Cells[rowCount, 1].Value = department.Department;
					var departmentWithLocation = JsonConvert.DeserializeObject<Dictionary<string, decimal>>(department.DepartmentWithLocation);
					var cell_location = 2;
					foreach (var location in locations)
					{
						sheet.Cells[rowCount, cell_location].Value = departmentWithLocation[location];
						cell_location++;
					}
					rowCount++;
				}
				// department Group
				// calculator department group 

				var departmentGroup_1 = JsonConvert.DeserializeObject<Dictionary<string, decimal>>(data.DepartmentGroup_1.DepartmentGroupWithLocation);
				sheet.Cells[rowCount, 1].Value = data.DepartmentGroup_1.DepartmentGroup + " Total";
				var cell_Group = 2;
				foreach (var location in locations)
				{
					sheet.Cells[rowCount, cell_Group].Value = departmentGroup_1[location];
					cell_Group++;
				}
				rowCount++;



				// show data in location Group 2
				foreach (var department in data.DepartmentGroup_2.ListItem)
				{
					sheet.Cells[rowCount, 1].Value = department.Department;
					var departmentWithLocation = JsonConvert.DeserializeObject<Dictionary<string, decimal>>(department.DepartmentWithLocation);
					var cell_location = 2;
					foreach (var location in locations)
					{
						sheet.Cells[rowCount, cell_location].Value = departmentWithLocation[location];
						cell_location++;
					}
					rowCount++;
				}

				// department Group
				// calculator department group 
				var departmentGroup_2 = JsonConvert.DeserializeObject<Dictionary<string, decimal>>(data.DepartmentGroup_2.DepartmentGroupWithLocation);
			
				sheet.Cells[rowCount, 1].Value = data.DepartmentGroup_2.DepartmentGroup + " Total";
				var cell_Group2 = 2;
				foreach (var location in locations)
				{
					sheet.Cells[rowCount, cell_Group2].Value = departmentGroup_2[location];
					cell_Group2++;
				}
				rowCount++;

				// delivery
				sheet.Cells[rowCount, 1].Value = "Delivery %";
				var cell_Delivery = 2;
				var delivery = JsonConvert.DeserializeObject<Dictionary<string, decimal>>(data.Delivery);
				foreach (var location in locations)
				{
					sheet.Cells[rowCount, cell_Delivery].Value = delivery[location].ToString(ConnectConsts.ConnectConsts.NumberFormat) + " %";
					cell_Delivery++;
				}
				rowCount++;

				// tickets
				sheet.Cells[rowCount, 1].Value = "Tickets";
				var tickets = JsonConvert.DeserializeObject<Dictionary<string, decimal>>(data.Tickets);
				var cell_Ticket = 2;
				foreach (var location in locations)
				{
					sheet.Cells[rowCount, cell_Ticket].Value = tickets[location];
					cell_Ticket++;
				}
				rowCount++;

				// AOV
				sheet.Cells[rowCount, 1].Value = "AOV";
				var cell_AOV = 2;
				var aov = JsonConvert.DeserializeObject<Dictionary<string, decimal>>(data.Tickets);
				foreach (var location in locations)
				{
					sheet.Cells[rowCount, cell_AOV].Value = aov[location].ToString(ConnectConsts.ConnectConsts.NumberFormat);
					cell_AOV++;
				}

				for (var i = 1; i <= totalCell; i++)
				{
					sheet.Column(i).AutoFit();
				}
			}
		}

		private async Task GetMonthlySoteSaleReportByAllDepartmentExportExcel(ExcelPackage package, GetMonthlyStoreSalesStatsReportInput input, IMonthlySalesStatsReportAppService appService)
		{
			var sheet = package.Workbook.Worksheets.Add(L("MonthlySalesStats"));
			sheet.OutLineApplyStyle = true;
			var data = await appService.GetMonthlyStoreSalesReportByAllDepartment(input);
			var locations = data.Locations;

			if (data != null)
			{
				int rowCount = 1;
				var headersStart = new List<string> { L("Sales") };

				var headers = headersStart.Concat(locations);
				var cell_Header = 1;
				var totalCell = headers.Count();

				foreach (var item in headers)
				{
					sheet.Cells[rowCount, cell_Header].Value = item;
					cell_Header++;
				}
				sheet.Cells[rowCount, 1, rowCount, totalCell].Style.Font.Bold = true;
				rowCount++;
                var departments = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, object>>>(data.ListDepartment);
                var listDepartmentPercent = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, object>>>(data.ListDepartmentPercent);
				foreach (var department in departments)
				{
					sheet.Cells[rowCount, 1].Value = department.Key;
					var cell_location = 2;
					foreach (var location in locations)
					{
						sheet.Cells[rowCount, cell_location].Value = Convert.ToDecimal(department.Value[location]).ToString(ConnectConsts.ConnectConsts.NumberFormat);
						cell_location++;
					}
					rowCount++;
					cell_location = 2;
					foreach (var location in locations)
					{
						sheet.Cells[rowCount, cell_location].Value = Convert.ToDecimal(listDepartmentPercent[department.Key][location]).ToString(ConnectConsts.ConnectConsts.NumberFormat) + " %";
						cell_location++;
					}
					rowCount++;
				}

				// tickets
				sheet.Cells[rowCount, 1].Value = "Tickets";
				var tickets = JsonConvert.DeserializeObject<Dictionary<string, decimal>>(data.TotalTickets);
				var cell_Ticket = 2;
				foreach (var location in locations)
				{
					sheet.Cells[rowCount, cell_Ticket].Value = Convert.ToInt32(tickets[location]).ToString(ConnectConsts.ConnectConsts.NumberFormat);
					cell_Ticket++;
				}
				rowCount++;
				for (var i = 1; i <= totalCell; i++)
				{
					sheet.Column(i).AutoFit();
				}
			}
		}
	}
}