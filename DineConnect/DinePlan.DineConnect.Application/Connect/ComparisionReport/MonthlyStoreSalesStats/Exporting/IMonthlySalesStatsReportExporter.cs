﻿using DinePlan.DineConnect.Connect.ComparisionReport.MonthlyStoreSalesStats.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.MonthlyStoreSalesStats.Exporting
{
    public interface IMonthlySalesStatsReportExporter
    {
        Task<FileDto> ExportMonthlyStoreSalesStatsReport(GetMonthlyStoreSalesStatsReportInput input, IMonthlySalesStatsReportAppService appService);
    }
}