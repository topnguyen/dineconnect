﻿using Abp.BackgroundJobs;
using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.CashSummaryReport.Exporting;
using DinePlan.DineConnect.Connect.ComparisionReport.Dto;
using DinePlan.DineConnect.Connect.ComparisionReport.Exporting;
using DinePlan.DineConnect.Connect.ComparisionReport.MonthlyStoreSalesStats.Dto;
using DinePlan.DineConnect.Connect.ComparisionReport.MonthlyStoreSalesStats.Exporting;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Setting;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ComparisionReport.MonthlyStoreSalesStats
{
	public class MonthlySalesStatsReportAppService : DineConnectAppServiceBase, IMonthlySalesStatsReportAppService
	{
		private readonly IRepository<ProgramSettingTemplate> _programSettingTemplateRepository;
		private readonly IRepository<Master.Department> _dRepo;
		private readonly ILocationAppService _locService;
		private readonly IRepository<Master.Location> _lRepo;
		private readonly IRepository<Transaction.Ticket> _ticketManager;
		private readonly IRepository<Transaction.Order> _orderManager;
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private readonly IReportBackgroundAppService _rbas;
		private readonly IBackgroundJobManager _bgm;
		private readonly IMonthlySalesStatsReportExporter _exporter;
		private readonly IRepository<Period.WorkPeriod> _workPeriod;
		private readonly IRepository<PaymentType> _payRe;
		private readonly IConnectReportAppService _connectReportAppService;
		public MonthlySalesStatsReportAppService(
			IRepository<Transaction.Ticket> ticketManager,
			ILocationAppService locService,
			IRepository<Company> comR,
			IUnitOfWorkManager unitOfWorkManager,
			IRepository<Master.Department> drepo,
			IRepository<Master.Location> lRepo,
			IRepository<Transaction.Order> orderManager,
			IReportBackgroundAppService rbas,
			IBackgroundJobManager bgm,
			IMonthlySalesStatsReportExporter exporter,
			IRepository<Period.WorkPeriod> workPeriod,
			IRepository<PaymentType> payRe,
			IConnectReportAppService connectReportAppService,
			IRepository<ProgramSettingTemplate> programSettingTemplateRepository
			)
		{
			_ticketManager = ticketManager;
			_programSettingTemplateRepository = programSettingTemplateRepository;
			_unitOfWorkManager = unitOfWorkManager;
			_dRepo = drepo;
			_locService = locService;
			_lRepo = lRepo;
			_orderManager = orderManager;
			_rbas = rbas;
			_bgm = bgm;
			_exporter = exporter;
			_workPeriod = workPeriod;
			_payRe = payRe;
			_connectReportAppService = connectReportAppService;
		}

		

		public async Task<GetMonthlySaleStatsOutput> GetMonthlyStoreSalesReports(GetMonthlyStoreSalesStatsReportInput input)
		{
			var result = new GetMonthlySaleStatsOutput();
			result.DepartmentGroup_1 = await ProcessDataByDepartmentGroup(input, input.DepartmentGroupName_1);
			result.DepartmentGroup_2 = await ProcessDataByDepartmentGroup(input, input.DepartmentGroupName_2);

			//TODO Calculator Delivery
			var delivery = new Dictionary<string, decimal>();
			var locations = await GetLocations(input);
			foreach(var location in locations)
			{
				var departmentGroup_1 = JsonConvert.DeserializeObject<Dictionary<string, decimal>>(result.DepartmentGroup_1.DepartmentGroupWithLocation);
				var departmentGroup_2 = JsonConvert.DeserializeObject<Dictionary<string, decimal>>(result.DepartmentGroup_2.DepartmentGroupWithLocation);
				var percent = 0M;
				if(departmentGroup_2[location] != 0)
				{
					percent = departmentGroup_1[location] / departmentGroup_2[location];
				}
				delivery.Add(location, percent);
			}
			result.Delivery = JsonConvert.SerializeObject(delivery);

			//TODO calculator Tickets
			var tickets = new Dictionary<string, int>();
			foreach (var location in locations)
			{
				var total = 0;
				foreach (var department in result.DepartmentGroup_1.ListItem)
				{
					foreach (var loc in department.ListItem)
					{
						if (loc.Location == location)
							total = total + loc.Count;
					}
				}
				foreach (var department in result.DepartmentGroup_2.ListItem)
				{
					foreach (var loc in department.ListItem)
					{
						if (loc.Location == location)
							total = total + loc.Count;
					}
				}
				tickets.Add(location, total);
			}
			result.Tickets = JsonConvert.SerializeObject(tickets);


			//TODO CalculatorAOV
			var AOV = new Dictionary<string, decimal>();
			foreach (var location in locations)
			{
				var departmentGroup_2 = JsonConvert.DeserializeObject<Dictionary<string, decimal>>(result.DepartmentGroup_2.DepartmentGroupWithLocation);
				var aov_location = 0M;
				if (tickets[location] != 0)
				{
					aov_location = departmentGroup_2[location] / tickets[location];
				}
				AOV.Add(location, aov_location);
			}
			result.AOV = JsonConvert.SerializeObject(AOV);
			result.Locations = locations;
			return await Task.FromResult(result);
		}

		private async Task<GetDataByDeparmetGroup> ProcessDataByDepartmentGroup(GetMonthlyStoreSalesStatsReportInput input, string departmentGroup)
		{
			try
			{
				var result = new GetDataByDeparmetGroup();
				var locations = await GetLocations(input);
				var ticketsByDeparmentGroup = GetAllTicketsByDepartmentGroup(input, departmentGroup).Include(x => x.Location).ToList();

				var tickets_GroupDepartmentGroup_DepartmentName_Location = ticketsByDeparmentGroup.GroupBy(x => new
				{
					DepartmentGroup = x.DepartmentGroup,
					Deparment = x.DepartmentName,
					Location = x.Location.Code,
				}).Select(x => new GetDataByLocation
				{
					DepartmentGroup = x.Key.DepartmentGroup,
					Department = x.Key.Deparment,
					Location = x.Key.Location,
					Count = x.Count(),
					TotalAmount = x.Sum(a => a.TotalAmount)
				});

				var tickets_GroupDepartmentGroup_DepartmentName = tickets_GroupDepartmentGroup_DepartmentName_Location.GroupBy(x => new
				{
					DepartmentGroup = x.DepartmentGroup,
					Deparment = x.Department,
				}).Select(x => new GetDataByDeparmetWithLocation
				{
					DepartmentGroup = x.Key.DepartmentGroup,
					Department = x.Key.Deparment,
					ListItem = x.ToList()
				}).ToList();

				// calculator department with location
				foreach (var item in tickets_GroupDepartmentGroup_DepartmentName)
				{
					var departmentWithLocation = new Dictionary<string, decimal>();
					foreach (var location in locations)
					{
						var value = 0M;
						if (item.ListItem.Any(x => x.Location == location))
						{
							value = item.TotalAmount;
						}
						departmentWithLocation.Add(location, value);
					}
					item.DepartmentWithLocation = JsonConvert.SerializeObject(departmentWithLocation);
				}

				var tickets_GroupDepartmentGroup = tickets_GroupDepartmentGroup_DepartmentName.GroupBy(x => new
				{
					DepartmentGroup = x.DepartmentGroup,
				}).Select(x => new GetDataByDeparmetGroup
				{
					DepartmentGroup = x.Key.DepartmentGroup,
					ListItem = x.ToList()
				});
				if (tickets_GroupDepartmentGroup.Count() >= 1)
				{
					result = tickets_GroupDepartmentGroup.FirstOrDefault();
				}

				result.DepartmentGroup = departmentGroup;

				// calculator departmentGroup with location
				var departmentGroupWithLocation = new Dictionary<string, decimal>();
				foreach (var location in locations)
				{
					var value = 0M;
					foreach (var item in tickets_GroupDepartmentGroup_DepartmentName_Location)
					{
						if (item.Location == location)
							value = value + item.TotalAmount;
					}
					departmentGroupWithLocation.Add(location, value);
				}
				result.DepartmentGroupWithLocation = JsonConvert.SerializeObject(departmentGroupWithLocation);
				return await Task.FromResult(result);
			} catch(Exception e)
			{
				throw e;
			}
		
		}

		private IQueryable<Transaction.Ticket> GetAllTicketsByDepartmentGroup(GetMonthlyStoreSalesStatsReportInput input, string departmentGroup)
		{
			IQueryable<Transaction.Ticket> tickets;
			tickets = GetAllTicketsByFilter(input);
			tickets = tickets.Where(x => x.DepartmentGroup == departmentGroup);
			return tickets;
		}

		private IQueryable<Transaction.Ticket> GetAllTicketsByFilter(GetMonthlyStoreSalesStatsReportInput input)
		{
			IQueryable<Transaction.Ticket> tickets;

			if (!string.IsNullOrEmpty(input.TicketNo))
				using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
				{
					tickets = _ticketManager.GetAll()
						.Where(a => a.TicketNumber.Equals(input.TicketNo) || a.ReferenceNumber.Equals(input.TicketNo));
					return tickets;
				}

			var correctDate = CorrectInputDate(input);
			using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
			{
				tickets = _ticketManager.GetAll();
				if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
				{
					if (correctDate)
					{
						tickets =
							tickets.Where(
								a =>
									a.LastPaymentTime >=
									input.StartDate
									&&
									a.LastPaymentTime <=
									input.EndDate);
					}
					else
					{
						var mySt = input.StartDate.Date;
						var myEn = input.EndDate.Date;

						tickets =
							tickets.Where(
								a =>
									a.LastPaymentTimeTruc >= mySt
									&&
									a.LastPaymentTimeTruc <= myEn);
					}
				}
			}

			if (input.Location > 0)
			{
				tickets = tickets.Where(a => a.LocationId == input.Location);
			}
			else if (input.Locations != null && input.Locations.Any())
			{
				var locations = input.Locations.Select(a => a.Id).ToList();
				tickets = tickets.Where(a => locations.Contains(a.LocationId));
			}
			else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
					 && !input.LocationGroup.Locations.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = input.LocationGroup.Locations,
					Group = false
				});
				if (locations.Any()) tickets = tickets.Where(a => locations.Contains(a.LocationId));
			}
			else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = input.LocationGroup.Locations,
					Group = false
				});
				tickets = tickets.Where(a => locations.Contains(a.LocationId));
			}
			else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Groups = input.LocationGroup.Groups,
					Group = true
				});
				tickets = tickets.Where(a => locations.Contains(a.LocationId));
			}

			if (input.LocationGroup != null)
			{
				if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
				{
					var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
					tickets = tickets.Where(a => !nonlocations.Contains(a.LocationId));
				}
			}

			else if (input.LocationGroup == null)
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = new List<SimpleLocationDto>(),
					Group = false,
					UserId = input.UserId
				});
				if (input.UserId > 0)
					tickets = tickets.Where(a => locations.Contains(a.LocationId));
			}
			tickets = tickets.Where(a => a.Credit == input.Credit);

			return tickets;
		}

		private async Task<List<string>> GetLocations(GetMonthlyStoreSalesStatsReportInput input)
		{
			var locationIds = new List<int>();
			if (input.Location > 0)
			{
				locationIds.Add(input.Location);
			}
			else if (input.Locations != null && input.Locations.Any())
			{
				var locations = input.Locations.Select(a => a.Id).ToList();
				locationIds.AddRange(locations);
			}
			else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
					 && !input.LocationGroup.Locations.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = input.LocationGroup.Locations,
					Group = false
				});
				if (locations.Any()) locationIds.AddRange(locations);
			}
			else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = input.LocationGroup.Locations,
					Group = false
				});
				locationIds.AddRange(locations);
			}
			else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Groups = input.LocationGroup.Groups,
					Group = true
				});
				locationIds.AddRange(locations);
			}
			var locationCodes = await _lRepo.GetAll().Where(x => locationIds.Any(a => a == x.Id)).Select(x => x.Code).ToListAsync();
			return locationCodes;
		}
		public bool CorrectInputDate(IDateInput input)
		{
			if (input.NotCorrectDate) return true;
			var returnStatus = true;

			var operateHours = 0M;

			if (input.Locations != null && input.Locations.Any() && input.Locations.Count() == 1)
				operateHours = _lRepo.Get(input.Locations.First().Id).ExtendedBusinessHours;

			if (operateHours == 0M && input.Location > 0)
				operateHours = _lRepo.Get(input.Location).ExtendedBusinessHours;
			if (operateHours == 0M && input.LocationGroup != null && input.LocationGroup.Locations.Any() &&
				input.LocationGroup.Locations.Count() == 1)
				operateHours = _lRepo.Get(input.LocationGroup.Locations.First().Id).ExtendedBusinessHours;
			if (operateHours == 0M)
				operateHours = SettingManager.GetSettingValue<decimal>(AppSettings.ConnectSettings.OperateHours);

			if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
			{
				if (operateHours == 0M) returnStatus = false;
				input.EndDate = input.EndDate.AddDays(1);
				input.StartDate = input.StartDate.AddHours(Convert.ToDouble(operateHours));
				input.EndDate = input.EndDate.AddHours(Convert.ToDouble(operateHours)).AddMinutes(-1);
			}
			else
			{
				returnStatus = false;
			}

			return returnStatus;
		}

		public async Task<FileDto> GetMonthlyStoreSalesReportToExport(GetMonthlyStoreSalesStatsReportInput input)
		{
			if (AbpSession.UserId != null && AbpSession.TenantId != null)
			{
				input.MaxResultCount = 10000;

				if (input.RunInBackground)
				{
					var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
					{
						ReportName = ReportNames.MONTHLYSTORESALESTATS,
						Completed = false,
						UserId = AbpSession.UserId.Value,
						TenantId = AbpSession.TenantId.Value,
						ReportDescription = input.ReportDescription
					});

					if (backGroundId > 0)
					{
						var tInput = new MonthlyStoreSaleStatsReportInput
						{
							GetMonthlyStoreSalesStatsReportInput = input,
						};
						await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
						{
							BackGroundId = backGroundId,
							ReportName = ReportNames.MONTHLYSTORESALESTATS,
							MonthlyStoreSaleStatsReportInput = tInput,
							UserId = AbpSession.UserId.Value,
							TenantId = AbpSession.TenantId.Value
						});
					}
				}
				else
				{
					var output = await _exporter.ExportMonthlyStoreSalesStatsReport(input, this);
					return output;
				}
			}
			return null;
		}

		public async Task<GetMonthlySalesByAllDepartment> GetMonthlyStoreSalesReportByAllDepartment(GetMonthlyStoreSalesStatsReportInput input)
		{
			var result = new GetMonthlySalesByAllDepartment();
			var tickets = GetAllTicketsByFilter(input);
			var tickets_GroupDepartmentName_Location = tickets.GroupBy(x => new
			{
				Deparment = x.DepartmentName,
				Location = x.Location.Code,
			}).Select(x => new GetDataByLocation
			{
				Department = x.Key.Deparment,
				Location = x.Key.Location,
				Count = x.Count(),
				TotalAmount = x.Sum(a => a.TotalAmount)
			});

			var tickets_DepartmentName = tickets_GroupDepartmentName_Location.GroupBy(x => new
			{
				Deparment = x.Department,
			}).Select(x => new GetDataByDeparmetWithLocation
			{
				Department = x.Key.Deparment,
				ListItem = x.ToList()
			}).ToList();

			// calculator by location
			var locations = await GetLocations(input);

			var D_TotalCount_Ticket = new Dictionary<string, int>();
			foreach (var location in locations)
			{
				var total = 0;
				foreach (var department in tickets_DepartmentName)
				{
					foreach (var loc in department.ListItem)
					{
						if (loc.Location == location)
							total = total + loc.Count;
					}
				}
				D_TotalCount_Ticket.Add(location, total);
			}


			var D_TotalAmount = new Dictionary<string, decimal>();
			foreach (var location in locations)
			{
				var totalAmount = 0M;
				foreach (var department in tickets_DepartmentName)
				{
					foreach (var loc in department.ListItem)
					{
						if (loc.Location == location)
							totalAmount = totalAmount + loc.TotalAmount;
					}
				}
				D_TotalAmount.Add(location, totalAmount);
			}

			var D_Departments_TotalAmount = new Dictionary<string, Dictionary<string, object>>();

			foreach (var department in tickets_DepartmentName)
			{
				var lo = new Dictionary<string, object>();
				foreach (var location in locations)
				{
					foreach (var loc in department.ListItem)
					{
						if (loc.Location == location)
						{
							lo.Add(location, loc.TotalAmount);
							break;
						}
						else
						{
							lo.Add(location, 0M);
							break;
						}
					}
				}
				lo.Add("DepartmentName", department.Department);
				D_Departments_TotalAmount.Add(department.Department, lo);
			}

			var D_Department_Percent = new Dictionary<string, Dictionary<string, object>>();

			foreach (var department in tickets_DepartmentName)
			{
				var lo = new Dictionary<string, object>();
				foreach (var location in locations)
				{
					foreach (var loc in department.ListItem)
					{
						if (loc.Location == location)
						{
							var percent = department.TotalAmount > 0 ? (loc.TotalAmount / D_TotalAmount[location]) * 100 : 0M;
							lo.Add(location, percent);
							break;
						}
						else
						{
							lo.Add(location, 0M);
							break;
						}
					}
				}
				lo.Add("DepartmentName", department.Department);
				D_Department_Percent.Add(department.Department, lo);
			}

			result.Locations = locations;
			result.TotalTickets = JsonConvert.SerializeObject(D_TotalCount_Ticket);
			result.ListDepartment = JsonConvert.SerializeObject(D_Departments_TotalAmount);
			result.ListDepartmentPercent = JsonConvert.SerializeObject(D_Department_Percent);
			return await Task.FromResult(result);
		}
	}

}