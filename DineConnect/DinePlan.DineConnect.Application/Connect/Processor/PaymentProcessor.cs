﻿using System.Collections.Generic;
using System.Linq;
using DinePlan.DineConnect.General;

namespace DinePlan.DineConnect.Connect.Processor
{
    public class PaymentTypeProcessors
    {
        public static readonly Dictionary<string, List<FormlyType>> AllProcessors
            = new Dictionary<string, List<FormlyType>>
            {
                {
                    "Add Limit", new List<FormlyType>
                    {
                        new FormlyType
                        {
                            Key = "MinAmount",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "MinAmount",
                                Placeholder = "0"
                            }
                        },
                        new FormlyType
                        {
                            Key = "MaxAmount",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "MaxAmount",
                                Placeholder = "0"
                            }
                        },
                        new FormlyType
                        {
                            Key = "IncludePastPayments",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "IncludePastPayments"
                            }
                        }
                    }
                },
                {
                    "Add Discount Calculation", new List<FormlyType>
                    {
                        new FormlyType
                        {
                            Key = "CalculationName",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "CalculationName",
                                Placeholder = "Calculation Name"
                            }
                        },
                        new FormlyType
                        {
                            Key = "Rate",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "Rate",
                                Placeholder = "0"
                            }
                        },
                        new FormlyType
                        {
                            Key = "MinAmount",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "MinAmount",
                                Placeholder = "0"
                            }
                        }
                    }
                },
                {
                    "Ask Payment Confirmation", new List<FormlyType>
                    {
                        new FormlyType
                        {
                            Key = "ConfirmationMessage",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "ConfirmationMessage",
                                Placeholder = "ConfirmationMessage"
                            }
                        }
                    }
                },
                {
                    "Ask Payment Description", new List<FormlyType>
                    {
                        new FormlyType
                        {
                            Key = "Description",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Description",
                                Placeholder = "Description"
                            }
                        },
                        new FormlyType
                        {
                            Key = "Mandatory",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Mandatory"
                            }
                        },
                        new FormlyType
                        {
                            Key = "RegEx",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Regular Expression",
                                Placeholder = "RegEx"
                            }
                        }
                    }
                },
                {
                    "Connect Member Processor", new List<FormlyType>()
                },
                {
                    "Connect Voucher Processor", new List<FormlyType>()
                },
                {
                    "Prepaid Card Processor", new List<FormlyType>()
                },
                {
                    "BlueCardRedeem", new List<FormlyType>()
                },
                {
                    "EccCreditLimit", new List<FormlyType>()
                },
                {
                    "Payment Denomination", new List<FormlyType>
                    {
                        new FormlyType
                        {
                            Key = "Denomination",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Denomination",
                                Placeholder = "Denomination"
                            }
                        },
                        new FormlyType
                        {
                            Key = "AskReference",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "AskReference"
                            }
                        },
                        new FormlyType
                        {
                            Key = "RegEx",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Regular Expression",
                                Placeholder = "RegEx"
                            }
                        }
                    }
                },
                {
                    "WithHoldTax", new List<FormlyType>()
                    {
                        new FormlyType
                        {
                            Key = "WithHoldCode",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "text",
                                Label = "WithHoldCode",
                                Placeholder = "0"
                            }
                        },
                        new FormlyType
                        {
                            Key = "Percentage",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "Percentage",
                                Placeholder = "0"
                            }
                        },
                        new FormlyType
                        {
                            Key = "DifferenceWhtAmount",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "DifferenceWhtAmount",
                                Placeholder = "0",
                                Min="0",
                                Max ="0.99"
                            }
                        },
                        new FormlyType
                        {
                            Key = "DifferenceBaseAmount",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "DifferenceBaseAmount",
                                Placeholder = "0"
                            }
                        },
                       

                    }
                },
                 {
                    "ChooseReason", new List<FormlyType>()
                    {
                        new FormlyType
                        {
                            Key = "ReasonGroup",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "text",
                                Label = "ReasonGroup",
                                Placeholder = "0"
                            }
                        }
                    }
                },
                 {
                     "Pax", new List<FormlyType>()
                     {
                         new FormlyType
                         {
                             Key = "Port",
                             Type = "input",
                             TemplateOptions = new FormlyTemplateOption
                             {
                                 Type = "number",
                                 Label = "Port",
                                 Placeholder = "3"
                             }
                         }
                     }
                 },
                 {
                     "Nets", new List<FormlyType>()
                     {
                         new FormlyType
                         {
                             Key = "Port",
                             Type = "input",
                             TemplateOptions = new FormlyTemplateOption
                             {
                                 Type = "text",
                                 Label = "Port",
                                 Placeholder = "COM3"
                             }
                         },
                         new FormlyType
                         {
                             Key = "DefaultTag",
                             Type = "input",
                             TemplateOptions = new FormlyTemplateOption
                             {
                                 Type = "text",
                                 Label = "Port",
                                 Placeholder = "NETS"
                             }
                         }
                     }
                 },
                 {
                     "Nets3Processor", new List<FormlyType>()
                     {
                         new FormlyType
                         {
                             Key = "Port",
                             Type = "input",
                             TemplateOptions = new FormlyTemplateOption
                             {
                                 Type = "text",
                                 Label = "Port",
                                 Placeholder = "COM3"
                             }
                         },
                         new FormlyType
                         {
                             Key = "DefaultTag",
                             Type = "input",
                             TemplateOptions = new FormlyTemplateOption
                             {
                                 Type = "text",
                                 Label = "Port",
                                 Placeholder = "NETS"
                             }
                         }
                     }
                 },
                 {
                     "MSwipe", new List<FormlyType>()
                 },
                 {
                     "Adyen", new List<FormlyType>()
                 }
            };


        public static readonly string[] Processors = AllProcessors.Keys.ToArray();
    }
}