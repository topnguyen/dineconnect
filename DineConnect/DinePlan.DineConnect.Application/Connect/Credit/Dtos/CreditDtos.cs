﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Credit.Dtos
{
    public class ApiCreditInput
    {
        public int TenantId { get; set; }
        public string ReferenceNumber { get; set; }
    }
    public class ApiCreditOutput
    {
        public string Log { get; set; }
        public decimal Value { get; set; }
        public int TicketId { get; set; }


    }
}
