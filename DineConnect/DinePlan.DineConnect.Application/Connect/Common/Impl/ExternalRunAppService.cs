﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Abp.UI;

namespace DinePlan.DineConnect.Connect.Common.Impl
{
    public class ExternalRunAppService : DineConnectAppServiceBase, IExternalRunAppService
    {
        public void RunApp(string appPath)
        {
            try
            {
                var server = HttpContext.Current.Server;
                Process process = Process.Start(server.MapPath(@appPath));
                if (process != null)
                {
                    int id = process.Id;
                    Process tempProc = Process.GetProcessById(id);
                    tempProc.WaitForExit(2000);
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
           
        }
    }
}
