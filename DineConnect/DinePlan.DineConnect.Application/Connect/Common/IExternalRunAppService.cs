﻿using Abp.Application.Services;

namespace DinePlan.DineConnect.Connect.Common
{
    public interface IExternalRunAppService : IApplicationService
    {
         void RunApp(string appPath);
    }
}
