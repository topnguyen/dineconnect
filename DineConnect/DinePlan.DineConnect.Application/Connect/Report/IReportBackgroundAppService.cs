﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Report
{
    public interface IReportBackgroundAppService : IApplicationService
    {
        Task<PagedResultOutput<BackgroundReportOutput>> GetAll(BackgroundReportInput tenant);
        Task<int> CreateOrUpdate(CreateBackgroundReportInput tenant);
        Task<FileDto> GetFileDto(int id);
        Task UpdateFileOutputCompleted(FileDto fileDto, int argsBackGroundId);
        Task SetBackGroundJobCompleted(int argsBackGroundId);

    }
}