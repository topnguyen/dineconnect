﻿using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.CreditSalesReport.Dto;
using DinePlan.DineConnect.Connect.Message.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Message.Exporting
{
	public class TickMessageReportExporter : FileExporterBase, ITickMessageReportExporter
	{
		public async Task<FileDto> ExportTickMessages(GetTickMessageInput input, ITickMessageAppService appService)
		{
			var builder = new StringBuilder();
			builder.Append(L("TickMessageReport"));
			var file = new FileDto(builder + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
			using (var excelPackage = new ExcelPackage())
			{
				await GetTickMessages(excelPackage, input, appService);


				Save(excelPackage, file);
			}

			return ProcessFile(input, file);
		}
		private async Task GetTickMessages(ExcelPackage package, GetTickMessageInput input, ITickMessageAppService appService)
		{
			var simpleDateFormat = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.SimpleDateFormat);
			var sheet = package.Workbook.Worksheets.Add(L("TickMessages"));
			sheet.OutLineApplyStyle = true;

			var headers = new List<string>
			{
				L("MessageHeading"),
				L("Type"),
				L("MessageUntil"),
				L("Acknowledgement"),
				L("OneTimeMessage"),
			};

			int rowCount = 1;

			AddHeader(
				sheet, rowCount, headers.ToArray()
			);
			rowCount++;

			input.SkipCount = 0;
			input.MaxResultCount = 10000;
			var data = await appService.GetAll(input);

			if (data != null)
			{
				foreach (var item in data.Items)
				{
					sheet.Cells[rowCount, 1].Value = item.MessageHeading;
					sheet.Cells[rowCount, 2].Value = item.MessageType;
					sheet.Cells[rowCount, 3].Value = item.ToDate.ToString(simpleDateFormat);
					sheet.Cells[rowCount, 4].Value = item.Acknowledgement;
					sheet.Cells[rowCount, 5].Value = item.OneTimeMessage;
					rowCount++;
				}
			}
			for (int i = 1; i <= 5; i++)
			{
				sheet.Column(i).AutoFit();
			}
		}

	}
}