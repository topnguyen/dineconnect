﻿using DinePlan.DineConnect.Connect.CreditSalesReport.Dto;
using DinePlan.DineConnect.Connect.Message;
using DinePlan.DineConnect.Connect.Message.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Message.Exporting
{
    public interface ITickMessageReportExporter
    {
        Task<FileDto> ExportTickMessages(GetTickMessageInput input, ITickMessageAppService appService);
    }
}