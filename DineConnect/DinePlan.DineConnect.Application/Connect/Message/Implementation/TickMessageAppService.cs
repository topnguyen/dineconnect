﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Message.Dto;
using DinePlan.DineConnect.Connect.Messages;
using DinePlan.DineConnect.Connect.Sync;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Timing;
using Castle.Components.DictionaryAdapter;
using DinePlan.DineConnect.Common;
using Itenso.TimePeriod;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Connect.Message.Exporting;

namespace DinePlan.DineConnect.Connect.Message.Implementation
{
    public class TickMessageAppService : DineConnectAppServiceBase, ITickMessageAppService
    {
        private readonly IRepository<TickMessage> _tickRepo;
        private IRepository<TickMessageReply> _tickReplyRepo;
        private readonly ILocationAppService _locAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private IRepository<Master.Location> _locationRepository;
        private readonly ISyncAppService _syncAppService;
        private ITickMessageReportExporter _exporter;
        public TickMessageAppService(IRepository<TickMessage> tickRepo,
                                     IRepository<TickMessageReply> tickReplyRepo,
                                     ILocationAppService locAppService,
                                     IUnitOfWorkManager unitOfWorkManager,
                                     IRepository<Master.Location> locationRepository,
                                     ISyncAppService syncAppService,
                                     ITickMessageReportExporter exporter
                                    )
        {
            _tickRepo = tickRepo;
            _tickReplyRepo = tickReplyRepo;
            _locAppService = locAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _locationRepository = locationRepository;
            _syncAppService = syncAppService;
            _exporter = exporter;
        }
        public async Task<TickMessageForTimeDto> ApiGetAllTickMessages(ApiLocationInput input)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter(AppConsts.ConnectFilter, "oid", orgId);

            var listDtos = new List<TickMessage>();

            if (input.LocationId <= 0)
            {
                listDtos = await _tickRepo.GetAllListAsync();
            }
            else
            {
                var lstTickMessage = await _tickRepo.GetAllListAsync();

                if (lstTickMessage.Any())
                    foreach (var singleLoc in lstTickMessage)
                    {
                        if (await _locAppService.IsLocationExists(new CheckLocationInput
                        {
                            LocationId = input.LocationId,
                            Locations = singleLoc.Locations,
                            Group = singleLoc.Group,
                            LocationTag = singleLoc.LocationTag,
                            NonLocations = singleLoc.NonLocations
                        }))
                        {
                            listDtos.Add(singleLoc);
                        }
                    }
            }

            var result = listDtos.MapTo<List<TickMessageListDto>>();

            return new TickMessageForTimeDto
            {
                TickMessages = result
            };
        }
        public async Task<TickMessageForTimeDto> ApiGetGetAllTickMessage(ApiLocationInput input)
        {
            return await ApiGetAllTickMessages(input);
        }

        public async Task<PagedResultOutput<TickMessageListDto>> GetAll(GetTickMessageInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _tickRepo.GetAll()
                                        .Where(i => i.IsDeleted == input.IsDeleted)
                                        .WhereIf(!input.Filter.IsNullOrEmpty(), p => p.MessageHeading.Contains(input.Filter));

                var itemsEnum = SearchLocation(allItems, input.LocationGroup).OfType<TickMessage>();

                if (!input.NoFilter)
                {
                    itemsEnum = itemsEnum.Where(x =>
                    {
                        if (!input.FromDate.HasValue || !input.ToDate.HasValue) return true;

                        TimeRange firstRange = new TimeRange(
                            new DateTime(input.FromDate.Value.Year, input.FromDate.Value.Month, input.FromDate.Value.Day),
                            new DateTime(input.ToDate.Value.Year, input.ToDate.Value.Month, input.ToDate.Value.Day)
                            );

                        TimeRange secondRange = new TimeRange(
                            new DateTime(x.FromDate.Year, x.FromDate.Month, x.FromDate.Day),
                            new DateTime(x.ToDate.Year, x.ToDate.Month, x.ToDate.Day)
                        );

                        var relation = firstRange.GetRelation(secondRange);
                        return relation != PeriodRelation.Before &&
                               relation != PeriodRelation.After;



                    });
                }

                var allItemCount = itemsEnum.Count();

                var sortMenuItems = itemsEnum.AsQueryable()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                var allListDtos = sortMenuItems.MapTo<List<TickMessageListDto>>();
                return new PagedResultOutput<TickMessageListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        public async Task<int> CreateOrUpdateMessage(CreateOrTickMessageInput input)
        {
            var output = 0;

            if (input.TickMessage.Id.HasValue)
            {
                await UpdateTickMessage(input);
                output = input.TickMessage.Id.Value;
            }
            else
            {
                output = await CreateTickMessage(input);
            }
            await _syncAppService.UpdateSync(SyncConsts.TICKMESSAGE);
            return output;
        }

        public async Task<GetTickMessageForEdit> GetTicketMessageForEdit(NullableIdInput input)
        {
            var output = new GetTickMessageForEdit();

            var editDto = new TickMessageEditDto();

            if (input.Id.HasValue)
            {
                var message = await _tickRepo.GetAsync(input.Id.Value);
                editDto = message.MapTo<TickMessageEditDto>();
            }
            else
            {
                editDto = new TickMessageEditDto();
            }
            output.TickMessage = editDto;
            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);

            return output;
        }

        public async Task DeleteMessage(NullableIdInput input)
        {
            if (input.Id.HasValue)
                await _tickRepo.DeleteAsync(input.Id.Value);
        }

        private async Task<int> CreateTickMessage(CreateOrTickMessageInput input)
        {
            var tickMessage = input.TickMessage.MapTo<TickMessage>();

            UpdateLocationAndNonLocation(tickMessage, input.LocationGroup);
            var item = await _tickRepo.InsertOrUpdateAndGetIdAsync(tickMessage);
            return item;
        }

        private async Task UpdateTickMessage(CreateOrTickMessageInput input)
        {
            var dto = input.TickMessage;
            var item = await _tickRepo.GetAsync(input.TickMessage.Id.Value);

            dto.MapTo(item);
            //item = dto.MapTo<TickMessage>();

            UpdateLocationAndNonLocation(item, input.LocationGroup);

            await _tickRepo.UpdateAsync(item);
        }

        public async Task<int> ApiUpdateAcknowledgment(ApiTickMessageReplyInput input)
        {
            int newId = 0;

            try
            {
                var ticMessage = await _tickRepo.FirstOrDefaultAsync(t => t.Id == input.TickId);

                if (ticMessage != null)
                {
                    var reply = await _tickReplyRepo.FirstOrDefaultAsync(t => t.TickMessageId == ticMessage.Id && t.LocationId == input.LocationId);

                    if (reply != null)
                    {
                        reply.TickMessageId = ticMessage.Id;
                        reply.Reply = input.Reply;
                        reply.LocationId = input.LocationId;
                        reply.User = input.User;
                        reply.UserId = input.UserId;
                        reply.TerminalId = input.TerminalId;
                        reply.LastModificationTime = Clock.Now;
                    }
                    else
                    {
                        reply = new TickMessageReply
                        {
                            TickMessageId = ticMessage.Id,
                            Reply = input.Reply,
                            LocationId = input.LocationId,
                            User = input.User,
                            UserId = input.UserId,
                            TerminalId = input.TerminalId
                        };
                    }

                    var entity = await _tickReplyRepo.InsertOrUpdateAsync(reply);

                    newId = entity.Id;
                }

                return newId;
            }
            catch (Exception)
            {
                return newId;
            }
        }

        public ListResultDto<ComboboxItemDto> GetTickMessageTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 0;
            foreach (var name in Enum.GetNames(typeof(TickMessageType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultOutput<SimpleLocationDto>> GetUnreadLocations(IdInput input)
        {

            var myMessage = await _tickRepo.GetAsync(input.Id);
            List<SimpleLocationDto> allSimpleLocationDto = new List<SimpleLocationDto>();

            var allReadLocations = await _tickReplyRepo.GetAll().Where(l => l.TickMessageId == input.Id)
                .Select(a => a.LocationId).ToListAsync();

            List<SimpleLocationDto> allSimLos = new List<SimpleLocationDto>();

            if (myMessage != null)
            {
                if (!myMessage.Group && !myMessage.LocationTag && !string.IsNullOrEmpty(myMessage.Locations))
                {
                    allSimLos = JsonConvert.DeserializeObject<List<SimpleLocationDto>>(myMessage.Locations);
                }
                else if (myMessage.Group)
                {
                    var allSimLo = JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(myMessage.Locations);
                    foreach (var simpleLocationGroupDto in allSimLo)
                    {
                        var allLocations =
                            await _locAppService.GetLocationsByLocationGroupName(simpleLocationGroupDto.Name);
                        if (allLocations.Items.Any())
                        {
                            allSimLos.AddRange(allLocations.Items.Select(a => new SimpleLocationDto()
                            {
                                Code = a.DisplayText,
                                Name = a.DisplayText,
                                Id = Convert.ToInt32(a.Value)
                            }));
                        }
                    }
                }
                else if (myMessage.LocationTag)
                {
                    var allSimLo = JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(myMessage.Locations);
                    foreach (var simpleLocationGroupDto in allSimLo)
                    {
                        var allLocations =
                            await _locAppService.GetLocationsByLocationTagName(simpleLocationGroupDto.Name);
                        if (allLocations.Items.Any())
                        {
                            allSimLos.AddRange(allLocations.Items.Select(a => new SimpleLocationDto()
                            {
                                Code = a.DisplayText,
                                Name = a.DisplayText,
                                Id = Convert.ToInt32(a.Value)
                            }));
                        }
                    }
                }
                else if (!myMessage.Group && !myMessage.LocationTag && string.IsNullOrEmpty(myMessage.Locations))
                {
                    var allLocations = _locationRepository.GetAll();
                    var location = new List<Master.Location>();
                    if (allReadLocations.Count != 0 )
                    {
                        foreach (var loc in allReadLocations)
                        {
                            var findLoc = allLocations.Where(s => s.Id == loc);
                            if (findLoc != null)
                            {
                                allLocations = allLocations.Except(findLoc);
                            }
                        }
                        foreach (var locEx in allLocations)
                        {
                            allSimpleLocationDto.Add(new SimpleLocationDto
                            {
                                Id = locEx.Id,
                                Name = locEx.Name,
                                Code = locEx.Code,
                                OrganizationUnitId = locEx.OrganizationUnitId,
                                CompanyRefId = locEx.CompanyRefId
                            });
                        }
                    }
                    else
                    {
                        var myUnReadReply = new SimpleLocationDto()
                        {
                            Id = 0,
                            Name = "ALL",
                            Code = "ALL",
                            OrganizationUnitId = 0,
                            CompanyRefId = 0
                        };
                        return new ListResultOutput<SimpleLocationDto>(new List<SimpleLocationDto> { myUnReadReply });
                    }
                }
                if (allSimLos != null && allSimLos.Any())
                {
                    if (allReadLocations.Any())
                    {
                        allSimpleLocationDto = allSimLos.Where(a => !allReadLocations.Contains(a.Id)).ToList();
                    }
                    else
                    {
                        allSimpleLocationDto = allSimLos;
                    }

                    return new ListResultOutput<SimpleLocationDto>(allSimpleLocationDto);
                }

            }


            return new ListResultOutput<SimpleLocationDto>(allSimpleLocationDto);
        }

        public async Task<ListResultOutput<SimpleLocationDto>> GetMappedLocations(IdInput input)
        {
            var myMessage = await _tickRepo.GetAsync(input.Id);
            if (!myMessage.Group && !myMessage.LocationTag && !string.IsNullOrEmpty(myMessage.Locations))
            {
                var allSimLo = JsonConvert.DeserializeObject<List<SimpleLocationDto>>(myMessage.Locations);
                if (allSimLo != null && allSimLo.Any())
                {
                    return new ListResultOutput<SimpleLocationDto>(allSimLo);
                }
            }
            else if (myMessage.Group)
            {
                var allSimLo = JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(myMessage.Locations);
                if (allSimLo != null && allSimLo.Any())
                {
                    var allLocations = allSimLo
                        .Select(l => new SimpleLocationDto()
                        {
                            Id = l.Id,
                            Name = l.Name,
                            OrganizationUnitId = l.Id,
                            CompanyRefId = 0
                        }).ToList();

                    return new ListResultOutput<SimpleLocationDto>(allLocations);
                }
            }
            else if (myMessage.LocationTag)
            {
                var allSimLo = JsonConvert.DeserializeObject<List<SimpleLocationTagDto>>(myMessage.Locations);
                if (allSimLo != null && allSimLo.Any())
                {
                    var allLocations = allSimLo
                        .Select(l => new SimpleLocationDto()
                        {
                            Id = l.Id,
                            Name = l.Name,
                            OrganizationUnitId = l.Id,
                            CompanyRefId = 0
                        }).ToList();

                    return new ListResultOutput<SimpleLocationDto>(allLocations);
                }
            }
            var myUnReadReply = new SimpleLocationDto()
            {
                Id = 0,
                Name = "ALL",
                Code = "ALL",
                OrganizationUnitId = 0,
                CompanyRefId = 0
            };

            return new ListResultOutput<SimpleLocationDto>(new List<SimpleLocationDto> { myUnReadReply });
        }

        public async Task<ListResultOutput<TickMessageReplyListDto>> GetAllLocations()
        {
            var allLocations = await _locationRepository.GetAll()
                .Select(l => new TickMessageReplyListDto()
                {
                    LocationCode = l.Code,
                    LocationName = l.Name,
                    LocationId = l.Id,
                    OrganizationUnitId = l.OrganizationUnitId,
                    CompanyRefId = l.CompanyRefId,
                })
                .ToListAsync();

            return new ListResultOutput<TickMessageReplyListDto>(allLocations);
        }

        public async Task<ListResultOutput<TickMessageReplyListDto>> GetReplyLocations(IdInput input)
        {
            var replyLocations = await _tickReplyRepo.GetAll()
                .Where(t => t.TickMessageId == input.Id)
                .Where(t => t.Reply != null)
                .Select(t => new TickMessageReplyListDto()
                {
                    LocationCode = t.Location.Code,
                    LocationName = t.Location.Name,
                    LocationId = t.LocationId,
                    OrganizationUnitId = t.Location.OrganizationUnitId,
                    CompanyRefId = t.Location.CompanyRefId,
                    Reply = t.Reply,
                    User = t.User,
                    TickId = t.TickMessageId
                })
                .ToListAsync();

            return new ListResultOutput<TickMessageReplyListDto>(replyLocations);
        }

        public async Task<ListResultOutput<SimpleLocationDto>> GetNonReplyLocations(IdInput input)
        {
            var message = await _tickRepo.GetAsync(input.Id);

            var returnList = new List<SimpleLocationDto>();

            var locations = (await _locationRepository.GetAllListAsync()).MapTo<List<SimpleLocationDto>>();

            if (!message.Locations.IsNullOrEmpty())
            {
                locations = JsonConvert.DeserializeObject<List<SimpleLocationDto>>(message.Locations);
            }
            var replyLocationIds = await _tickReplyRepo.GetAll().Where(t => t.TickMessageId == input.Id).Select(t => t.LocationId).ToListAsync();

            returnList = locations.Where(l => !replyLocationIds.Contains(l.Id)).ToList();

            return new ListResultOutput<SimpleLocationDto>(returnList);
        }

        public async Task<ListResultOutput<SimpleLocationDto>> GetAcknowledgedLocations(IdInput input)
        {
            var replyLocations = await _tickReplyRepo.GetAll()
                .Where(t => t.TickMessageId == input.Id)
                .Where(t => t.Acknowledged)
                .Select(t => new SimpleLocationDto()
                {
                    Code = t.Location.Code,
                    Name = t.Location.Name,
                    Id = t.LocationId,
                    OrganizationUnitId = t.Location.OrganizationUnitId,
                    CompanyRefId = t.Location.CompanyRefId
                })
                .ToListAsync();

            return new ListResultOutput<SimpleLocationDto>(replyLocations);
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _tickRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _tickRepo.UpdateAsync(item);
            }
        }

        public async Task<FileDto> GetTickMessageToExcel(GetTickMessageInput input)
        {
            var output = await _exporter.ExportTickMessages(input, this);
            return output;
        }
    }
}