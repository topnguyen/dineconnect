﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Message.Dto;
using DinePlan.DineConnect.Connect.Reason;
using DinePlan.DineConnect.Connect.Sync;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System;

namespace DinePlan.DineConnect.Connect.Message.Implementation
{
    public class PlanReasonAppService : DineConnectAppServiceBase, IPlanReasonAppService
    {
        private readonly IRepository<PlanReason> _planReasonRepo;
        private readonly ILocationAppService _locationAppService;
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public PlanReasonAppService(IRepository<PlanReason> planReasonRepo
           , ILocationAppService locationAppService
            , ISyncAppService syncAppService
            , IUnitOfWorkManager unitOfWorkManager)
        {
            _planReasonRepo = planReasonRepo;
            _locationAppService = locationAppService;
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultOutput<PlanReasonListDto>> GetAll(GetPlanReasonInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _planReasonRepo
                    .GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted)
                    .WhereIf(
                        !input.Filter.IsNullOrEmpty(),
                        p => p.Reason.Contains(input.Filter));
               
                var allMyEnItems= SearchLocation(allItems, input.LocationGroup).OfType<PlanReason>();

                var allItemCount = allMyEnItems.Count();

                var sortItems = allMyEnItems.AsQueryable()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                var allListDtos = sortItems.MapTo<List<PlanReasonListDto>>();

                return new PagedResultOutput<PlanReasonListDto>(
                    allItemCount,
                    allListDtos
                );
            }
        }

        public async Task<int> CreateOrUpdateMessage(CreateOrPlanReasonInput input)
        {
            var output = 0;

            if (input.PlanReason.Id.HasValue)
            {
                await UpdatePlanReason(input);
                output = input.PlanReason.Id.Value;
            }
            else
            {
                output = await CreatePlanReason(input);
            }
            await _syncAppService.UpdateSync(SyncConsts.REASON);

            return output;
        }

        public async Task<GetPlanReasonForEdit> GetPlanReasonForEdit(NullableIdInput input)
        {
            GetPlanReasonForEdit output = new GetPlanReasonForEdit();

            PlanReasonEditDto editDto = new PlanReasonEditDto();

            if (input.Id.HasValue)
            {
                var message = await _planReasonRepo.GetAsync(input.Id.Value);
                editDto = message.MapTo<PlanReasonEditDto>();
                var recordExists=DineConnectConsts.ReasonGroups.Contains(editDto.ReasonGroup);
                if(recordExists)
                {
                    editDto.ReasonGroupCheckBox = false;
                }
                else
                {
                    editDto.ReasonGroupCheckBox = true;
                }
            }
            else
            {
                editDto = new PlanReasonEditDto();
            }
            output.PlanReason = editDto;
            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);
            return output;
        }

        public async Task DeletePlanReason(IdInput input)
        {
            await _planReasonRepo.DeleteAsync(input.Id);
        }

        private async Task<int> CreatePlanReason(CreateOrPlanReasonInput input)
        {
            var planReason = new PlanReason
            {
                Reason = input.PlanReason.Reason,
                AliasReason = input.PlanReason.AliasReason,
                ReasonGroup = input.PlanReason.ReasonGroup
            };
            UpdateLocationAndNonLocation(planReason, input.LocationGroup);
            var item = await _planReasonRepo.InsertOrUpdateAndGetIdAsync(planReason);
            return item;
        }

        private async Task UpdatePlanReason(CreateOrPlanReasonInput input)
        {
            var dto = input.PlanReason;
            var item = await _planReasonRepo.GetAsync(input.PlanReason.Id.Value);

            item.Reason = dto.Reason;
            item.AliasReason = dto.AliasReason;
            item.ReasonGroup = dto.ReasonGroup;

            UpdateLocationAndNonLocation(item, input.LocationGroup);
            await _planReasonRepo.UpdateAsync(item);
        }

        public async Task<List<ApiPlanReasonOutput>> ApiGetPlanReasons(ApiLocationInput locationInput)
        {
            var localRepoTypes = await _planReasonRepo.GetAllListAsync(a => a.TenantId == locationInput.TenantId);

            List<ApiPlanReasonOutput> allPlanReasonOutputs = new List<ApiPlanReasonOutput>();
            if (!string.IsNullOrEmpty(locationInput.Tag))
            {
                var allLocations = await _locationAppService.GetLocationsByLocationGroupCode(locationInput.Tag);
                if (allLocations != null && allLocations.Items.Any())
                    foreach (var sortL in localRepoTypes)
                        if (!sortL.Group && !string.IsNullOrEmpty(sortL.Locations) &&
                            !sortL.LocationTag)
                        {
                            var scLocations =
                                JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(sortL.Locations);
                            if (scLocations.Any())
                                foreach (var i in scLocations.Select(a => a.Id))
                                    if (allLocations.Items.Select(a => a.Value).Contains(i.ToString()))
                                        allPlanReasonOutputs.Add(new ApiPlanReasonOutput()
                                        {
                                            Reason = sortL.Reason,
                                            AliasReason = sortL.AliasReason,
                                            ReasonGroup = sortL.ReasonGroup,
                                            Id = sortL.Id
                                        });
                        }
                        else
                        {
                            allPlanReasonOutputs.Add(new ApiPlanReasonOutput()
                            {
                                Reason = sortL.Reason,
                                AliasReason = sortL.AliasReason,
                                ReasonGroup = sortL.ReasonGroup,
                                Id = sortL.Id
                            });
                        }
            }

            else if (locationInput.LocationId > 0)
            {
                foreach (var sortL in localRepoTypes)
                {
                    if (await _locationAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = locationInput.LocationId,
                        Locations = sortL.Locations,
                        Group = sortL.Group,
                        NonLocations = sortL.NonLocations,
                        LocationTag = sortL.LocationTag
                    }))
                    {
                        allPlanReasonOutputs.Add(new ApiPlanReasonOutput()
                        {
                            Reason = sortL.Reason,
                            AliasReason = sortL.AliasReason,
                            ReasonGroup = sortL.ReasonGroup,
                            Id = sortL.Id
                        });
                    }
                }
            }

            return new List<ApiPlanReasonOutput>(allPlanReasonOutputs);
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _planReasonRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _planReasonRepo.UpdateAsync(item);
            }
        }
    }
}