﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using Abp.Timing;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Messages;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Filter;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Message.Dto
{
    public class GetTickMessageInput : PagedAndSortedInputDto, IShouldNormalize, IFileExport
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

        public string Location { get; set; }

        public DateTime? ServerTime { get; set; }
        public bool IsDeleted { get; set; }
        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
        public GetTickMessageInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public LocationGroupDto LocationGroup { get; set; }
        public bool NoFilter { get; set; }
    }

    public class TickMessageForTimeDto
    {
        public TickMessageForTimeDto()
        {
            TickMessages = new List<TickMessageListDto>();
            RequestTime = Clock.Now;
        }

        public List<TickMessageListDto> TickMessages { get; set; }
        public DateTime RequestTime { get; set; }
    }

    [AutoMapFrom(typeof(TickMessage))]
    public class TickMessageListDto : ConnectFullMultiTenantAuditEntityDto
    {
        public string MessageHeading { get; set; }
        public string MessageContent { get; set; }
        public TickMessageType MessageType { get; set; }
        public bool Acknowledgement { get; set; }
        public bool OneTimeMessage { get; set; }
        public bool Reply { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public bool InstantMessage { get; set; }
        public string TimeIntervals { get; set; }

    }

    [AutoMapTo(typeof(TickMessage))]
    public class TickMessageEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public string MessageHeading { get; set; }
        public string MessageContent { get; set; }
        public bool Acknowledgement { get; set; }
        public TickMessageType MessageType { get; set; }
        public bool OneTimeMessage { get; set; }
        public bool Reply { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public bool InstantMessage { get; set; }
        public string TimeIntervals { get; set; }

    }

    public class GetTickMessageForEdit : IOutputDto
    {
        public GetTickMessageForEdit()
        {
            TickMessage = new TickMessageEditDto();
            LocationGroup = new LocationGroupDto();
        }

        [Required]
        public TickMessageEditDto TickMessage { get; set; }

        public LocationGroupDto LocationGroup { get; set; }
    }

    public class CreateOrTickMessageInput : IInputDto
    {
        [Required]
        public TickMessageEditDto TickMessage { get; set; }

        public LocationGroupDto LocationGroup { get; set; }
    }

    public class ApiTickMessageOutput
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public bool Acknowledgement { get; set; }
    }

    [AutoMapTo(typeof(TickMessageReply))]
    public class ApiTickMessageReplyInput
    {
        public int TickId { get; set; }

        public int LocationId { get; set; }

        public string User { get; set; }
        public string UserId { get; set; }

        public string Reply { get; set; }

        public int TerminalId { get; set; }
    }

    public class TickMessageReplyListDto
    {
        public int TickId { get; set; }

        public int LocationId { get; set; }

        public string User { get; set; }

        public string Reply { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public long OrganizationUnitId { get; set; }
        public int CompanyRefId { get; set; }
    }
}