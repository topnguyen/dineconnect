﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Reason;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Filter;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Message.Dto
{
    public class GetPlanReasonInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
        public LocationGroupDto LocationGroup { get; set; }

    }

    [AutoMapFrom(typeof(PlanReason))]
    public class PlanReasonListDto : ConnectFullMultiTenantAuditEntityDto
    {
        public string Reason { get; set; }
        public string AliasReason { get; set; }
        public string ReasonGroup { get; set; }
    }

    [AutoMapFrom(typeof(PlanReason))]
    public class PlanReasonEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public string Reason { get; set; }
        public string AliasReason { get; set; }
        public string ReasonGroup { get; set; }
        public bool ReasonGroupCheckBox { get; set; }
    }

    public class GetPlanReasonForEdit : IOutputDto
    {
        public GetPlanReasonForEdit()
        {
            PlanReason = new PlanReasonEditDto();
            LocationGroup = new LocationGroupDto();
        }

        [Required]
        public PlanReasonEditDto PlanReason { get; set; }

        public LocationGroupDto LocationGroup { get; set; }
    }

    public class CreateOrPlanReasonInput : IInputDto
    {
        [Required]
        public PlanReasonEditDto PlanReason { get; set; }

        public LocationGroupDto LocationGroup { get; set; }
    }

    public class ApiPlanReasonOutput
    {
        public int Id { get; set; }
        public string Reason { get; set; }
        public string AliasReason { get; set; }
        public string ReasonGroup { get; set; }
    }
}