﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Message.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Message
{
    public interface ITickMessageAppService : IApplicationService
    {
        Task<PagedResultOutput<TickMessageListDto>> GetAll(GetTickMessageInput input);
        Task<FileDto> GetTickMessageToExcel(GetTickMessageInput input);

        Task<TickMessageForTimeDto> ApiGetGetAllTickMessage(ApiLocationInput input);
        Task<TickMessageForTimeDto> ApiGetAllTickMessages(ApiLocationInput input);

        Task<int> CreateOrUpdateMessage(CreateOrTickMessageInput input);

        Task<GetTickMessageForEdit> GetTicketMessageForEdit(NullableIdInput nullableIdInput);

        Task<int> ApiUpdateAcknowledgment(ApiTickMessageReplyInput input);


        ListResultDto<ComboboxItemDto> GetTickMessageTypes();

        Task DeleteMessage(NullableIdInput input);

        Task<ListResultOutput<TickMessageReplyListDto>> GetReplyLocations(IdInput input);

        Task<ListResultOutput<SimpleLocationDto>> GetNonReplyLocations(IdInput input);

        Task ActivateItem(IdInput input);
        Task<ListResultOutput<SimpleLocationDto>> GetAcknowledgedLocations(IdInput input);
        Task<ListResultOutput<SimpleLocationDto>> GetUnreadLocations(IdInput input);
        Task<ListResultOutput<SimpleLocationDto>> GetMappedLocations(IdInput input);

        Task<ListResultOutput<TickMessageReplyListDto>> GetAllLocations();

    }
}