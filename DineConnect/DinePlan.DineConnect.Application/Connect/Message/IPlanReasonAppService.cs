﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Message.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Message
{
    public interface IPlanReasonAppService : IApplicationService
    {
        Task<PagedResultOutput<PlanReasonListDto>> GetAll(GetPlanReasonInput input);

        Task<int> CreateOrUpdateMessage(CreateOrPlanReasonInput input);

        Task<GetPlanReasonForEdit> GetPlanReasonForEdit(NullableIdInput nullableIdInput);

        Task DeletePlanReason(IdInput input);

        Task<List<ApiPlanReasonOutput>> ApiGetPlanReasons(ApiLocationInput locationInput);

        Task ActivateItem(IdInput input);
    }
}