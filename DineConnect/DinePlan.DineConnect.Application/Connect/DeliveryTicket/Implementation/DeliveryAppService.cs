﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Logging;
using Abp.UI;
using Castle.Core.Logging;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Connect.Delivery;
using DinePlan.DineConnect.Connect.DeliveryTicket.Dto;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Engage.Member;
using DinePlan.DineConnect.Engage.Member.Dtos.DinePlan.DineConnect.Engage.Dtos;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Connect.Audit;

namespace DinePlan.DineConnect.Connect.DeliveryTicket.Implementation
{
    public class DeliveryAppService : DineConnectAppServiceBase, IDeliveryAppService
    {
        private readonly ILogger _logger;
        private readonly IRepository<Delivery.DeliveryTicket> _ticketManager;
        private readonly IMemberAppService _memberService;
        private readonly IRepository<MenuItemPortion> _mpManager;
        private readonly IRepository<MenuItem> _mManager;
        private readonly ILocationAppService _locationAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<ExternalLog> _logRepository;


        public DeliveryAppService(IRepository<Delivery.DeliveryTicket> ticketManager, IUnitOfWorkManager unitOfWorkManager,
            ILogger logger, IMemberAppService memberService, IRepository<MenuItemPortion> menuPortion, 
            IRepository<MenuItem> mManager, ILocationAppService locationAppService, IRepository<ExternalLog> logRepo)
        {
            _ticketManager = ticketManager;
            _logger = logger;
            _memberService = memberService;
            _mpManager = menuPortion;
            _mManager = mManager;
            _locationAppService = locationAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _logRepository = logRepo;
        }

        public async Task<CreateOrUpdateDeliveryTicketOutput> CreateOrUpdateTicket(
            CreateOrUpdateDeliveryTicketInput input)
        {
            try
            {
                
                var validator = new ApiCreateTicketInputValidator();
                var result = await validator.ValidateAsync(input);
                if (!result.IsValid)
                {
                    return new CreateOrUpdateDeliveryTicketOutput
                    {
                        Error = true,
                        Status = FluentValidatoryExtension.ValidationResult(result.Errors)
                    };
                }
                var updateTicket = false;
                var list = 0;
                if (input.Ticket.Id>0)
                {
                    updateTicket = true;
                }

                var count =
                    await
                        _ticketManager.LongCountAsync(
                            a =>
                                a.TicketNumber.Equals(input.Ticket.TicketNumber) &&
                                a.LocationId.Equals(input.Ticket.LocationId));

                updateTicket = count > 0;

                if (updateTicket)
                {
                    var ticket =
                        await _ticketManager.FirstOrDefaultAsync(
                            a =>
                                a.TicketNumber.Equals(input.Ticket.TicketNumber) &&
                                a.LocationId.Equals(input.Ticket.LocationId));

                    if (ticket != null)
                    {
                        input.Ticket.Id = ticket.Id;
                    }
                    else
                    {
                        updateTicket = false;
                    }
                }


                if (updateTicket)
                {
                    list = input.Ticket.Id.Value;
                }
                else
                {
                    list = await CreateTicketAsync(input.Ticket);
                }

                return new CreateOrUpdateDeliveryTicketOutput
                {
                    Ticket = list
                };
            }
            catch (Exception exception)
            {
                _logger.Log(LogSeverity.Error, "DeliveryTicketCreation", exception);
                throw new UserFriendlyException(exception.Message);
            }
        }

        public async Task<int> CreateTicket(CreateDeliveryTicketInput input)
        {
            var ticket = new Delivery.DeliveryTicket
            {
                LocationId = input.LocationId,
                ConnectMemberId = input.MemberId,
                DeliveryStatus = DeliveryTicketStatus.NotAssigned,
                DeliveryTicketType = (DeliveryTicketType) input.DeliveryTicketType,
                Note = input.Note,
                TotalAmount = input.Total,
                Address = input.Address,
                Name = input.Name,
                Locality = input.Locality,
                TicketNumber = GenerateTicketNumber(input.MemberId, input.LocationId),
                Orders = new List<DeliveryOrder>(),
                CreationTime = DateTime.Now,
                CreatorUserId = AbpSession.UserId,
                LastModificationTime = DateTime.Now,
                LastModifierUserId = AbpSession.UserId,
                LastUpdateTime = DateTime.Now,
                Paid = !string.IsNullOrEmpty(input.paymentInformation),
                TableNumber = input.tableNumber
            };
            foreach (var order in input.Orders.Select(doi => new DeliveryOrder
            {
                MenuItemPortionId = doi.MenuItemPortionId,
                Price = doi.Price,
                Tax = doi.Tax,
                Quantity = doi.Quantity,
                Note = doi.Note,
                CreationTime = DateTime.Now,
                CreatorUserId = AbpSession.UserId
            }))
            {
                ticket.Orders.Add(order);
            }

            await _memberService.UpdateMemberLastLocation
                (new Engage.Member.Dtos.DinePlan.DineConnect.Engage.Dtos.MemberLocationInput
                {
                    LocationId = input.LocationId,
                    MemberId = input.MemberId
                });

            return await _ticketManager.InsertAndGetIdAsync(ticket);
        }

        public async Task<DeliveryTicketOutput> ApiGetTickets(ApiLocationInput locationInput)
        {
            if (locationInput.LocationId > 0)
            {
                var orgId = await _locationAppService.GetOrgnizationIdForLocationId(locationInput.LocationId);
                if (orgId > 0)
                {
                    CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
                }
            }
            var output = new DeliveryTicketOutput();
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                try
                {
                    var allRes = new List<DeliveryTicketDto>();
                    var allTickets = _ticketManager.GetAll().Where(a =>
                            a.LocationId == locationInput.LocationId && a.SyncId == 0 &&
                            a.TenantId == locationInput.TenantId)
                        .OrderByDescending(a => a.CreationTime).PageBy(0, 20);
                    foreach (var dTicket in allTickets)
                    {
                        var dTick = dTicket.MapTo<DeliveryTicketDto>();
                        if (dTicket?.ConnectMember != null)
                        {
                            dTick.MemberCode = dTicket.ConnectMember.MemberCode;
                            dTick.Name = dTicket.ConnectMember.Name;
                            allRes.Add(dTick);
                        }
                        else if (dTicket?.ConnectMemberId > 0)
                        {
                            var memberInfo = await _memberService.GetMemberForEdit(new NullableIdInput
                            {
                                Id = dTicket.ConnectMemberId
                            });
                            if (memberInfo?.Member != null)
                            {
                                dTick.MemberCode = memberInfo.Member.MemberCode;
                                dTick.Name = memberInfo.Member.Name;
                                allRes.Add(dTick);
                            }
                        }

                        if (dTick?.Orders != null)
                        {
                            foreach (var deliveryOrder in dTick?.Orders)
                            {
                                var portion = await _mpManager.GetAsync(deliveryOrder.MenuItemPortionId);
                                if (portion != null)
                                {
                                    MenuItem mItem = null;
                                    mItem = portion.MenuItem == null ? 
                                        _mManager.GetAll().Where(a=>a.Id == portion.MenuItemId && a.TenantId == locationInput.TenantId).ToList().LastOrDefault() : portion.MenuItem;

                                    if (mItem != null)
                                    {
                                        deliveryOrder.Name = mItem.Name;
                                        deliveryOrder.MenuItemId = mItem.Id.ToString();
                                        deliveryOrder.MenuItemPortionId = portion.Id;
                                        deliveryOrder.PortionName = portion.Name;

                                    }
                                }
                            }
                        }
                    }

                    output.Tickets = allRes;
                    await ClearUpTickets(locationInput);
                    return output;
                }
                catch (Exception exception)
                {
                    await _logRepository.InsertAsync(LogException(exception, ExternalLogType.DineCall));
                }
            }

            return output;
        }

        private async Task ClearUpTickets(ApiLocationInput locationInput)
        {
            DateTime myTime = DateTime.Now.AddDays(-2);
            var allTickets = await _ticketManager.GetAll().Where(a =>
                a.LocationId == locationInput.LocationId &&   a.TenantId == locationInput.TenantId &&
                a.CreationTime < myTime).ToListAsync();

            if (allTickets.Any())
            {
                foreach (var allTicket in allTickets.Select(a=>a.Id).ToArray())
                {
                    await _ticketManager.DeleteAsync(allTicket);
                }
            }
        }

        public async Task<int> ApiUpdateTicket(UpdateCallInput input)
        {
            if (input != null && input.TicketId > 0)
            {
                try
                {
                    using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MustHaveTenant))
                    {
                        var allTickets = await _ticketManager.GetAll().Where(a=>a.Id == input.TicketId).ToListAsync();
                        if (allTickets.Any())
                        {
                            var ticket = allTickets.LastOrDefault();
                            if (ticket != null)
                            {
                                ticket.SyncId = input.LocalTicketId;
                                ticket.DeliveryStatus = (DeliveryTicketStatus)input.DeliveryStatus;
                                await _ticketManager.UpdateAsync(ticket);
                                return ticket.Id;
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    _logger.Fatal("No Delivery Ticket ", exception);
                }
                return 0;
            }

            return 0;
        }

        public async Task<CallStatusOutput> GetDeliveryStatus(CallStatusInput input)
        {
            var reOutput = new CallStatusOutput();
            var allTickets = _ticketManager.GetAll().Where(a => a.DeliveryStatus != (DeliveryTicketStatus.Paid));
            if (input.Location > 0)
            {
                allTickets = allTickets.Where(a => a.LocationId == input.Location);
            }

            if (input.Member > 0)
            {
                allTickets = allTickets.Where(a => a.ConnectMemberId == input.Member);
            }
            if (input.TicketId > 0)
            {
                allTickets = allTickets.Where(a => a.Id == input.TicketId);
            }
            var allMyTickets = allTickets.GroupBy(a => new {a.DeliveryStatus, a.LocationId});

            foreach (var allSt in allMyTickets)
            {
                if (allSt.Key.DeliveryStatus == DeliveryTicketStatus.NotAssigned)
                {
                    reOutput.NotAssigned = allSt.Count();
                }

                if (allSt.Key.DeliveryStatus == DeliveryTicketStatus.Delivering)
                {
                    reOutput.Delivering = allSt.Count();
                }

                if (allSt.Key.DeliveryStatus == DeliveryTicketStatus.Delivered)
                {
                    reOutput.Delivered = allSt.Count();
                }

                if (allSt.Key.DeliveryStatus == DeliveryTicketStatus.Preparing)
                {
                    reOutput.Preparing = allSt.Count();
                }
            }

            return reOutput;
        }


        public async Task<PagedResultOutput<DeliveryTicketList>> GetDeliveryTickets(CallStatusInput input)
        {
            var allTickets = _ticketManager.GetAll().Where(a => a.DeliveryStatus != (DeliveryTicketStatus.Paid));
            if (input.Location > 0)
            {
                allTickets = allTickets.Where(a => a.LocationId == input.Location);
            }

            if (input.Status >= 0)
            {
                allTickets = allTickets.Where(a => a.DeliveryStatus == (DeliveryTicketStatus)input.Status);
            }

            var sortTickets = await allTickets.OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();
            var outputDtos = sortTickets.MapTo<List<DeliveryTicketList>>();

            var menuItemCount = outputDtos.Count();
            var output = new PagedResultOutput<DeliveryTicketList>(
                menuItemCount,
                outputDtos
                );
            return output;
        }

        public async Task<DeliveryTicketDto> GetDeliveryTicketById(IdInput input)
        {
            DeliveryTicketDto output = new DeliveryTicketDto();

            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                if (input.Id > 0)
                {
                    var dTicket = await _ticketManager.GetAsync(input.Id);
                    if (dTicket.LocationId > 0)
                    {
                        var orgId = await _locationAppService.GetOrgnizationIdForLocationId(dTicket.LocationId);
                        if (orgId > 0)
                        {
                            CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
                        }

                        output = dTicket.MapTo<DeliveryTicketDto>();
                        foreach (var myOrder in output.Orders)
                        {
                            var portion = _mpManager.Get(myOrder.MenuItemPortionId);
                            var itemName = "";
                            if (portion != null && portion.MenuItem == null)
                            {
                                var mItem = _mManager.Get(portion.MenuItemId);
                                itemName = mItem.Name + "(" + portion.Name + ")";

                            }
                            else
                            {
                                if (portion != null)
                                    itemName = portion.MenuItem.Name + "(" + portion.Name + ")";
                            }

                            myOrder.Name = itemName;
                        }

                        if (dTicket?.ConnectMember != null)
                        {
                            output.MemberCode = dTicket.ConnectMember.MemberCode;
                            output.Name = dTicket.ConnectMember.Name;
                        }
                    }
                }
            }

            return output;
        }

        public async Task ChangeLocation(UpdateCallInput input)
        {
            if (input != null)
            {
                var delTicket = await _ticketManager.GetAsync(input.TicketId);
                if (delTicket != null && delTicket.DeliveryStatus == DeliveryTicketStatus.NotAssigned)
                {
                    delTicket.LocationId = input.LocationId;
                }
            }
        }

        private async Task<int> CreateTicketAsync(DeliveryTicketDto input)
        {
            MessageOutputDto memberOutput = await GetMemberId(input);
            if (memberOutput != null && memberOutput.Id>0)
            {
                Delivery.DeliveryTicket ticket = new Delivery.DeliveryTicket
                {
                    TicketNumber = input.TicketNumber,
                    LocationId = input.LocationId,
                    ConnectMemberId = memberOutput.Id,
                    DeliveryStatus = DeliveryTicketStatus.NotAssigned,
                    DeliveryTicketType = (DeliveryTicketType) input.DeliveryTicketType,
                    Note = input.Note,
                    TotalAmount = input.TotalAmount,
                    Address = input.Address,
                    Name = input.Name,
                    Locality = input.Locality,
                    Orders = new List<DeliveryOrder>(),
                    CreationTime = DateTime.Now,
                    CreatorUserId = AbpSession.UserId,
                    LastModificationTime = DateTime.Now,
                    LastModifierUserId = AbpSession.UserId,
                    LastUpdateTime = DateTime.Now,
                    TenantId = input.TenantId,
                    TableNumber = input.TableNumber,
                    Paid = !string.IsNullOrEmpty(input.PaymentInformation)
                };

                foreach (var order in input.Orders.Select(doi => new DeliveryOrder
                {
                    MenuItemPortionId = doi.MenuItemPortionId,
                    Price = doi.Price,
                    Tax = doi.Tax,
                    Quantity = doi.Quantity,
                    Note = doi.Note,
                    CreationTime = DateTime.Now,
                    CreatorUserId = AbpSession.UserId,
                    OrderTags = doi.OrderTags
                }))
                {
                    ticket.Orders.Add(order);
                }
                return await _ticketManager.InsertAndGetIdAsync(ticket);
            }

            throw new UserFriendlyException(memberOutput?.ErrorMessage);
        }

        private async Task<MessageOutputDto> GetMemberId(DeliveryTicketDto input)
        {
            MemberEditDto member = new MemberEditDto
            {
                MemberCode = input.MemberCode,
                Name = input.Name,
                Locality = input.Locality,
                Address = input.Address,
                LocationRefId = input.LocationId,
                PostalCode = input.PostalCode,
                City = input.City,
                State = input.State,
                TenantId = input.TenantId
            };
            var memId = await _memberService.CreateOrUpdateMember(new Engage.Member.Dtos.DinePlan.DineConnect.Engage.Dtos.
                CreateOrUpdateMemberInput
            {
                Member = member,
            });

            return memId;
        }

       
       
        private string GenerateTicketNumber(int memberId, int locationId)
        {
            var builder = new StringBuilder();
            builder.Append(DateTime.Now.ToString("ddmmyyhhmmss"));
            builder.Append(memberId.ToString("00000"));
            builder.Append(locationId.ToString("000"));
            return builder.ToString();
        }
    }

    public class ApiCreateTicketInputValidator : AbstractValidator<CreateOrUpdateDeliveryTicketInput>
    {
        public ApiCreateTicketInputValidator()
        {
            RuleFor(x => x).NotNull();
            RuleFor(x => x.Ticket).NotNull();
            RuleFor(x => x.Ticket.TicketNumber).NotNull().NotEmpty();
            RuleFor(x => x.Ticket.LocationId).NotNull().NotEmpty().NotEqual(0);
            RuleForEach(x => x.Ticket.Orders).SetValidator(new ApiCreateTicketOrderValidator());


        }
    }

    public class ApiCreateTicketOrderValidator: AbstractValidator<DeliveryOrderDto>
    {
        public ApiCreateTicketOrderValidator()
        {
            RuleFor(x => x).NotNull();
            RuleFor(x => x.MenuItemPortionId).NotNull().NotEqual(0);
            RuleFor(x => x.Name).NotNull().NotEmpty();
            RuleFor(x => x.Quantity).NotNull().NotEqual(0);
            RuleFor(x => x.Price).NotEmpty();



        }
    }
}