﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.DeliveryTicket.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.Connect.DeliveryTicket
{
    public interface IDeliveryAppService : IApplicationService
    {
        Task<CreateOrUpdateDeliveryTicketOutput> CreateOrUpdateTicket(CreateOrUpdateDeliveryTicketInput input);
        Task<int> CreateTicket(CreateDeliveryTicketInput input);
        Task<DeliveryTicketOutput> ApiGetTickets(ApiLocationInput locationInput);
        Task<int> ApiUpdateTicket(UpdateCallInput input);

        Task<CallStatusOutput> GetDeliveryStatus(CallStatusInput input);

        Task<PagedResultOutput<DeliveryTicketList>> GetDeliveryTickets(CallStatusInput input);
        Task<DeliveryTicketDto> GetDeliveryTicketById(IdInput input);
        Task ChangeLocation(UpdateCallInput input);

    }
}
