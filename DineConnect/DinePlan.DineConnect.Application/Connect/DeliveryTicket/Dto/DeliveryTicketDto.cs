﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Delivery;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Engage;

namespace DinePlan.DineConnect.Connect.DeliveryTicket.Dto
{
    [AutoMapFrom(typeof (Delivery.DeliveryTicket))]
    public class DeliveryTicketDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public string TicketNumber { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public DateTime LastUpdateTime { get; set; }
        public  decimal TotalAmount { get; set; }
        public string Address { get; set; }
        public string Note { get; set; }
        public  int SyncId { get; set; }
        public List<DeliveryOrderDto> Orders { get; set; }
        public DeliveryTicketStatus DeliveryStatus { get; set; }
        public DeliveryTicketType DeliveryTicketType { get; set; }
        public string MemberCode { get; set; }
        public string Name { get; set; }
        public string Locality { get; set; }
        public string City { get; set; }
        public string State{ get; set; }
        public string PostalCode { get; set; }
        public bool Paid { get; set; }
        public string PaymentInformation { get; set; }

        public string TableNumber { get; set; }
        public int TenantId { get; set; }

        public string Deliverer { get; set; }

        public DeliveryTicketDto()
        {
            Orders = new List<DeliveryOrderDto>();
        }
    }

   

    [AutoMapFrom(typeof(Delivery.DeliveryOrder))]
    public class DeliveryOrderDto : CreationAuditedEntityDto
    {
        public string Name { get; set; }
        public string PortionName { get; set; }
        public string MenuItemId { get; set; }


        public  int MenuItemPortionId { get; set; }
        public  decimal Price { get; set; }
        public  decimal Tax { get; set; }
        public  decimal Quantity { get; set; }
        public  string Note { get; set; }
        public string OrderTags { get; set; } 

    }

    public class CreateOrUpdateDeliveryTicketInput : IInputDto
    {
        [Required]
        public DeliveryTicketDto Ticket { get; set; }
    }

    public class CreateOrUpdateDeliveryTicketOutput : IOutputDto
    {
        public int Ticket { get; set; }
        public string Status { get; set; }
        public bool Error{ get; set; }
    }
    public class DeliveryTicketOutput : IOutputDto
    {
        public List<DeliveryTicketDto> Tickets { get; set; }
    }
    public class CreateDeliveryTicketInput : IInputDto
    {
        public string paymentInformation;
        public string tableNumber;
        public int LocationId { get; set; }
        public int DeliveryTicketType { get; set; }

        public int MemberId { get; set; }
        public decimal Total { get; set; }
        public List<DeliveryOrderInput> Orders { get; set; }
        public string Address { get; set; }
        public string Note { get; set; }
        public string Name { get; set; }
        public string Locality { get; set; }

    }

    public class DeliveryOrderInput
    {
        public int MenuItemPortionId { get; set; }
        public decimal Quantity { get; set; }
        public string Note{ get; set; }
        public decimal Tax{ get; set; }
        public decimal Price { get; set; }

    }
}