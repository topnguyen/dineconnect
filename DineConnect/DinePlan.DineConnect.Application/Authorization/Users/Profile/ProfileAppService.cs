using System.Threading.Tasks;
using Abp.Auditing;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Authorization.Users.Profile.Dto;

namespace DinePlan.DineConnect.Authorization.Users.Profile
{
    [AbpAuthorize]
    public class ProfileAppService : DineConnectAppServiceBase, IProfileAppService
    {
        private readonly IRepository<UserPasswordArchive> _userPasswordArchiveRepo;

        public ProfileAppService(
            IRepository<UserPasswordArchive> userPasswordArchiveRepo)
        {
            _userPasswordArchiveRepo = userPasswordArchiveRepo;
        }

        public async Task<CurrentUserProfileEditDto> GetCurrentUserProfileForEdit()
        {
            var user = await GetCurrentUserAsync();
            return user.MapTo<CurrentUserProfileEditDto>();
        }

        public async Task UpdateCurrentUserProfile(CurrentUserProfileEditDto input)
        {
            var user = await GetCurrentUserAsync();
            input.MapTo(user);
            CheckErrors(await UserManager.UpdateAsync(user));
        }

        [DisableAuditing]
        public async Task ChangePassword(ChangePasswordInput input)
        {
            string passWord = input.NewPassword;
            User user;
            if(input.UserId>0)
            {
                user= await UserManager.GetUserByIdAsync(input.UserId);
                await UserManager.RemovePasswordAsync(input.UserId);
                await UserManager.AddPasswordAsync(input.UserId, input.NewPassword);
            }
            else
            {
                user = await GetCurrentUserAsync();
                CheckErrors(await UserManager.ChangePasswordAsync(user.Id, input.CurrentPassword, input.NewPassword));
            }
            //Update User Password Archive
            UserPasswordArchive userPasswordArchive = new UserPasswordArchive
            {
                UserId = (int)user.Id,
                Password = input.NewPassword
            };
            await _userPasswordArchiveRepo.InsertAndGetIdAsync(userPasswordArchive);
        } 
    }
}