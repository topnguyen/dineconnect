﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Notifications;
using Abp.Runtime.Session;
using Abp.UI;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Authorization.Dto;
using DinePlan.DineConnect.Authorization.Roles;
using DinePlan.DineConnect.Authorization.Users.Dto;
using DinePlan.DineConnect.Authorization.Users.Exporting;
using DinePlan.DineConnect.Connect.Departments.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Notifications;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Configuration.Tenants.Dto;
using System.Text;
using DinePlan.DineConnect.MultiTenancy;
using Abp.Configuration;
using DinePlan.DineConnect.Configuration;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Abp.BackgroundJobs;
using DinePlan.DineConnect.Job;
using DinePlan.DineConnect.Template;
using DinePlan.DineConnect.Email.Implementation;
using DinePlan.DineConnect.Email;

namespace DinePlan.DineConnect.Authorization.Users
{
    public class UserAppService : DineConnectAppServiceBase, IUserAppService
    {
        private readonly RoleManager _roleManager;
        private readonly IUserEmailer _userEmailer;
        private readonly IUserListExcelExporter _userListExcelExporter;
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        private readonly IAppNotifier _appNotifier;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<UserDefaultOrganization> _userDefaultOrganizationRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<Company> _companyRepo;
        private readonly Random _random = new Random();
        private readonly SettingManager _settingManager;
        private readonly IRepository<UserPasswordArchive> _userPasswordArchiveRepo;
        private readonly IBackgroundJobManager _bgm;
        private readonly IRepository<EmailTemplate> _emailTemplate;
        private readonly IEmailTemplateAppService _emailTemplateAppService;

        public UserAppService(
            RoleManager roleManager,
            IUserEmailer userEmailer,
            IUserListExcelExporter userListExcelExporter,
            INotificationSubscriptionManager notificationSubscriptionManager, IUnitOfWorkManager uommanager,
            IAppNotifier appNotifier,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IRepository<UserDefaultOrganization> userDefaultOrganizationRepo,
            IRepository<Location> locationRepo,
            IRepository<Company> companyRepo,
            SettingManager settingManager,
            IRepository<UserPasswordArchive> userPasswordArchiveRepo,
            IBackgroundJobManager backgroundJobManager,
            IRepository<EmailTemplate> emailTemplate,
            IEmailTemplateAppService emailTemplateAppService)
        {
            _roleManager = roleManager;
            _userEmailer = userEmailer;
            _userListExcelExporter = userListExcelExporter;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _appNotifier = appNotifier;
            _unitOfWorkManager = uommanager;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _userDefaultOrganizationRepo = userDefaultOrganizationRepo;
            _locationRepo = locationRepo;
            _companyRepo = companyRepo;
            _settingManager = settingManager;
            _userPasswordArchiveRepo = userPasswordArchiveRepo;
            _bgm = backgroundJobManager;
            _emailTemplate = emailTemplate;
            _emailTemplateAppService = emailTemplateAppService;
        }

        public async Task<PagedResultOutput<UserListDto>> GetUsers(GetUsersInput input)
        {
            var query = UserManager.Users
                .Include(u => u.Roles)
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    u =>
                        u.Name.Contains(input.Filter) ||
                        u.Surname.Contains(input.Filter) ||
                        u.UserName.Contains(input.Filter) ||
                        u.EmailAddress.Contains(input.Filter)
                );

            var userCount = await query.CountAsync();
            var users = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var userListDtos = users.MapTo<List<UserListDto>>();
            foreach (var lst in userListDtos)
            {
                lst.UserJsonDataObject = new UserJsonMaster();
                if (!lst.UserJsonData.IsNullOrEmpty())
                {
                    UserJsonMaster userJsonMaster = JsonConvert.DeserializeObject<UserJsonMaster>(lst.UserJsonData);
                    lst.UserJsonDataObject = userJsonMaster;
                    if (lst.UserJsonDataObject.SFL_User == 1)
                    {
                        lst.UserJsonDataObject.IsLocked = true;
                    }
                    else
                    {
                        lst.UserJsonDataObject.IsLocked = false;
                    }
                }
                else
                {
                    lst.UserJsonDataObject = new UserJsonMaster();
                }

            }
            await FillRoleNames(userListDtos);

            return new PagedResultOutput<UserListDto>(
                userCount,
                userListDtos
                );
        }


        public async Task<UserListDto> GetUserInfoBasedOnUserId(IdInput input)
        {
            var query = UserManager.Users
                .Include(u => u.Roles)
               .Where(t => t.Id == input.Id);

            var users = await query
                .ToListAsync();

            var userListDtos = users.MapTo<UserListDto>();

            return userListDtos;
        }

        public async Task<FileDto> GetUsersToExcel()
        {
            var users = await UserManager.Users.Include(u => u.Roles).ToListAsync();
            var userListDtos = users.MapTo<List<UserListDto>>();
            await FillRoleNames(userListDtos);

            return _userListExcelExporter.ExportToFile(userListDtos);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Users_Create, AppPermissions.Pages_Administration_Users_Edit)]
        public async Task<GetUserForEditOutput> GetUserForEdit(GetUserInfoInputDto input)
        {
            //Getting all available roles
            var userRoleDtos = (await _roleManager.Roles
                .OrderBy(r => r.DisplayName)
                .Select(r => new UserRoleDto
                {
                    RoleId = r.Id,
                    RoleName = r.Name,
                    RoleDisplayName = r.DisplayName
                })
                .WhereIf(!string.IsNullOrEmpty(input.FilterText), p => p.RoleDisplayName.ToUpper().Contains(input.FilterText.ToUpper()))
                .ToArrayAsync());

            var output = new GetUserForEditOutput
            {
                Roles = userRoleDtos
            };

            if (!input.Id.HasValue)
            {
                //Creating a new user
                output.User = new UserEditDto { IsActive = true, ShouldChangePasswordOnNextLogin = true };

                foreach (var defaultRole in await _roleManager.Roles.Where(r => r.IsDefault).ToListAsync())
                {
                    var defaultUserRole = userRoleDtos.FirstOrDefault(ur => ur.RoleName == defaultRole.Name);
                    if (defaultUserRole != null)
                    {
                        defaultUserRole.IsAssigned = true;
                    }
                }
            }
            else
            {
                //Editing an existing user
                var user = await UserManager.GetUserByIdAsync(input.Id.Value);
                output.User = user.MapTo<UserEditDto>();
                if (!output.User.UserJsonData.IsNullOrEmpty())
                {
                    UserJsonMaster userJsonMaster = JsonConvert.DeserializeObject<UserJsonMaster>(output.User.UserJsonData);
                    output.User.UserJsonDataObject = userJsonMaster;
                }
                else
                {
                    output.User.UserJsonDataObject = new UserJsonMaster();
                }
                output.ProfilePictureId = user.ProfilePictureId;

                foreach (var userRoleDto in userRoleDtos)
                {
                    userRoleDto.IsAssigned = await UserManager.IsInRoleAsync(input.Id.Value, userRoleDto.RoleName);
                }
            }

            if (input.Id.HasValue)
            {
                var ret = await _userOrganizationUnitRepository.FirstOrDefaultAsync(t => t.UserId == input.Id.Value);
                if (ret != null)
                {
                    output.DefaultLocationRefId = int.Parse(ret.OrganizationUnitId.ToString());
                }

                IQueryable<Location> allItems;
                allItems = from loc in _locationRepo.GetAll().Where(a => !a.IsDeleted)
                           join ou in _userOrganizationUnitRepository.GetAll().Where(a => a.UserId == input.Id.Value)
                               on loc.Id equals ou.OrganizationUnitId
                           select loc;

                var sortMenuItems = await allItems.ToListAsync();

                var allListDtos = sortMenuItems.MapTo<List<LocationListDto>>();

                output.Locations = allListDtos;

                if (allListDtos.Count > 0)
                {
                    int[] arrCompanyRefIds = allListDtos.Select(t => t.CompanyRefId).ToArray();

                    var companyList = await _companyRepo.GetAllListAsync(t => arrCompanyRefIds.Contains(t.Id));

                    if (companyList.Count > 0)
                    {
                        output.CompanyList = new List<ComboboxItemDto>(
                        companyList.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
                    }
                }


            }


            return output;

        }

        public async Task<ApiUserDto> ApiGetUser(ApiLocationInput input)
        {
            ApiUserDto output = new ApiUserDto();
            _unitOfWorkManager.Current.EnableFilter(AbpDataFilters.MayHaveTenant);
            _unitOfWorkManager.Current.SetFilterParameter(AbpDataFilters.MayHaveTenant, AbpDataFilters.Parameters.TenantId, input.TenantId);

            var user = await UserManager.GetUserByIdAsync(input.UserId);
            output.UserName = user.UserName;
            output.Password = user.Password;
            output.IsActive = user.IsActive;
            output.Name = user.Name;
            return output;
        }


        [AbpAuthorize(AppPermissions.Pages_Administration_Users_ChangePermissions)]
        public async Task<GetUserPermissionsForEditOutput> GetUserPermissionsForEdit(IdInput<long> input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            var permissions = PermissionManager.GetAllPermissions();
            var grantedPermissions = await UserManager.GetGrantedPermissionsAsync(user);

            return new GetUserPermissionsForEditOutput
            {
                Permissions = permissions.MapTo<List<FlatPermissionDto>>().OrderBy(p => p.DisplayName).ToList(),
                GrantedPermissionNames = grantedPermissions.Select(p => p.Name).ToList()
            };
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Users_ChangePermissions)]
        public async Task ResetUserSpecificPermissions(IdInput<long> input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            await UserManager.ResetAllPermissionsAsync(user);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Users_ChangePermissions)]
        public async Task UpdateUserPermissions(UpdateUserPermissionsInput input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            var grantedPermissions = PermissionManager.GetPermissionsFromNamesByValidating(input.GrantedPermissionNames);
            await UserManager.SetGrantedPermissionsAsync(user, grantedPermissions);
        }

        public async Task<IdInput> CreateOrUpdateUser(CreateOrUpdateUserInput input)
        {
            if (input.User.Id.HasValue)
            {
                return await UpdateUserAsync(input);
            }
            else
            {
                return await CreateUserAsync(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Users_Delete)]
        public async Task DeleteUser(IdInput<long> input)
        {
            if (input.Id == AbpSession.GetUserId())
            {
                throw new UserFriendlyException(L("YouCanNotDeleteOwnAccount"));
            }

            var user = await UserManager.GetUserByIdAsync(input.Id);
            CheckErrors(await UserManager.DeleteAsync(user));
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Users_Edit)]
        protected virtual async Task<IdInput> UpdateUserAsync(CreateOrUpdateUserInput input)
        {
            Debug.Assert(input.User.Id != null, "input.User.Id should be set.");

            var user = await UserManager.FindByIdAsync(input.User.Id.Value);

            //Update user properties
            input.User.MapTo(user); //Passwords is not mapped (see mapping configuration)

            //if (!input.User.Password.IsNullOrEmpty())
            //{
            //	CheckErrors(await UserManager.ChangePasswordAsync(user, input.User.Password));
            //}

            CheckErrors(await UserManager.UpdateAsync(user));

            //Update roles
            CheckErrors(await UserManager.SetRoles(user, input.AssignedRoleNames));

            if (input.SendActivationEmail)
            {
                user.SetNewEmailConfirmationCode();
                await _userEmailer.SendEmailActivationLinkAsync(user, input.User.Password);
            }

            //Update User Password Archive
            UserPasswordArchive userPasswordArchive = new UserPasswordArchive
            {
                UserId = (int)user.Id,
                Password = user.Password
            };
            await _userPasswordArchiveRepo.InsertAndGetIdAsync(userPasswordArchive);

            return new IdInput { Id = int.Parse(user.Id.ToString()) };
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Users_Create)]
        protected virtual async Task<IdInput> CreateUserAsync(CreateOrUpdateUserInput input)
        {

            //var user = input.User.MapTo<User>(); //Passwords is not mapped (see mapping configuration)
            User user = new User();

            user.Name = input.User.Name;
            user.Surname = input.User.Surname;
            user.UserName = input.User.UserName;
            user.EmailAddress = input.User.EmailAddress;
            user.IsActive = input.User.IsActive;
            user.Password = input.User.Password;
            user.UserJsonData = input.User.UserJsonData;

            user.TenantId = AbpSession.TenantId;

            var generatePassword = input.User.Password.IsNullOrEmpty();
            //Set password
            if (!input.User.Password.IsNullOrEmpty())
            {
                CheckErrors(await UserManager.PasswordValidator.ValidateAsync(input.User.Password));
            }
            else
            {
                //var passWordPolicy = await GetPasswordPolicy();
                //if (passWordPolicy == null)
                //    input.User.Password = User.CreateRandomPassword();
                //else
                //    input.User.Password = passWordPolicy.SamplePassword;
                var passWordPolicy = await GenerateSamplePasswordBasedOnPasswordPolicy();
                {
                    input.User.Password = passWordPolicy.SamplePassword;
                }
            }

            //Update User Password Archive
            UserPasswordArchive userPasswordArchive = new UserPasswordArchive
            {
                UserId = (int)user.Id,
                Password = user.Password
            };

            await _userPasswordArchiveRepo.InsertAndGetIdAsync(userPasswordArchive);
            user.Password = new PasswordHasher().HashPassword(input.User.Password);
            user.ShouldChangePasswordOnNextLogin = input.User.ShouldChangePasswordOnNextLogin;

            //Assign roles
            user.Roles = new Collection<UserRole>();
            foreach (var roleName in input.AssignedRoleNames)
            {
                var role = await _roleManager.GetRoleByNameAsync(roleName);
                user.Roles.Add(new UserRole { RoleId = role.Id });
            }

            CheckErrors(await UserManager.CreateAsync(user));
            await CurrentUnitOfWork.SaveChangesAsync(); //To get new user's Id.

            //Notifications
            await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(user.TenantId, user.Id);
            await _appNotifier.WelcomeToTheApplicationAsync(user);

            //Send activation email
            if (input.SendActivationEmail)
            {
                user.SetNewEmailConfirmationCode();
                await _userEmailer.SendEmailActivationLinkAsync(user, input.User.Password);
            }
            else if (generatePassword)
            {
                await _bgm.EnqueueAsync<RandomPasswordJob, ResetPasswordJobArgs>(new ResetPasswordJobArgs
                {
                    UserName = input.User.UserName,
                    NewPassword = input.User.Password,
                    Sender = L("DinePlan"),
                    EmailAddress = input.User.EmailAddress,
                    TenantId = user.TenantId.GetValueOrDefault()
                });
            }

            return new IdInput { Id = int.Parse(user.Id.ToString()) };
        }

        private async Task FillRoleNames(List<UserListDto> userListDtos)
        {
            /* This method is optimized to fill role names to given list. */

            var distinctRoleIds = (
                from userListDto in userListDtos
                from userListRoleDto in userListDto.Roles
                select userListRoleDto.RoleId
                ).Distinct();

            var roleNames = new Dictionary<int, string>();
            foreach (var roleId in distinctRoleIds)
            {
                roleNames[roleId] = (await _roleManager.GetRoleByIdAsync(roleId)).DisplayName;
            }

            foreach (var userListDto in userListDtos)
            {
                foreach (var userListRoleDto in userListDto.Roles)
                {
                    userListRoleDto.RoleName = roleNames[userListRoleDto.RoleId];
                }

                userListDto.Roles = userListDto.Roles.OrderBy(r => r.RoleName).ToList();
            }
        }

        public async Task<bool> CheckMemberRoleExists(long userId, int tenantId)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var allRoles = await UserManager.GetRolesAsync(userId);
                if (allRoles.Any())
                {
                    return allRoles.Contains(StaticRoleNames.Tenants.Member);
                }
            }
            return false;
        }

        //public async Task<PasswordPolicyOutputDto> GetPasswordPolicy()
        //{
        //    var min_Length = await _settingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_Length);
        //    var max_Length = await _settingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Max_Length);
        //    var min_LCL = await _settingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_LCL);
        //    var min_UCL = await _settingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_UCL);
        //    var min_SC = await _settingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_SC);
        //    var min_AC = await _settingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_AC);
        //    if (min_Length==0 && max_Length == 0)
        //    {
        //        return null;
        //    }
        //    PasswordPolicySettingEditDto passwordPolicySettingEditDto = new PasswordPolicySettingEditDto
        //    {
        //        Min_Length = min_Length,
        //        Max_Length = max_Length,
        //        Min_UCL = min_UCL,
        //        Min_LCL = min_LCL,
        //        Min_SC = min_SC,
        //        Min_AC = min_AC
        //    };
        //    GetPassWordPolicyInputDto passedSettingInput = new GetPassWordPolicyInputDto { PasswordPolicy = passwordPolicySettingEditDto };
        //    var output = await GetPasswordPolicyForSetting(passedSettingInput);
        //    return output;
        //}

        //public async Task<PasswordPolicyOutputDto> GetPasswordPolicyForSetting(GetPassWordPolicyInputDto input)
        //{
        //    if (input.PasswordPolicy.Min_Length == 0 && input.PasswordPolicy.Max_Length == 0)
        //    {
        //        return null;
        //    }

        //    var passwordBuilder = new StringBuilder();
        //    string output = "<b>";
        //    if (input.PasswordPolicy.Min_Length > 0)
        //        output += L("PasswordPolicyMinLength", input.PasswordPolicy.Min_Length) + " <BR>";
        //    if (input.PasswordPolicy.Max_Length > 0)
        //        output += L("PasswordPolicyMaxLength", input.PasswordPolicy.Max_Length) + " <BR>";
        //    if (input.PasswordPolicy.Min_LCL > 0)
        //    {
        //        output += L("PasswordPolicyLowerCaseLetters", input.PasswordPolicy.Min_LCL) + " <BR>";
        //        passwordBuilder.Append(RandomString(input.PasswordPolicy.Min_LCL, true));
        //    }
        //    if (input.PasswordPolicy.Min_UCL > 0)
        //    {
        //        output += L("PasswordPolicyUpperCaseLetters", input.PasswordPolicy.Min_UCL) + " <BR>";
        //        passwordBuilder.Append(RandomString(input.PasswordPolicy.Min_UCL, false));
        //    }
        //    if (input.PasswordPolicy.Min_SC > 0)
        //    {
        //        output += L("PasswordPolicySpecialCharacters", input.PasswordPolicy.Min_SC) + " <BR>";
        //        passwordBuilder.Append(RandomSplCharacterString(input.PasswordPolicy.Min_SC));
        //    }
        //    if (input.PasswordPolicy.Min_AC > 0)
        //        output += L("PasswordPolicyAlphaNumericCharacters", input.PasswordPolicy.Min_AC) + " <BR>";

        //    if (passwordBuilder.ToString().Length< input.PasswordPolicy.Min_Length)
        //    {
        //        int shortageLength = input.PasswordPolicy.Min_Length - passwordBuilder.ToString().Length;
        //        if (shortageLength > 0)
        //        {
        //            if (shortageLength <= 3)
        //            {
        //                passwordBuilder.Append(RandomString(shortageLength, true));
        //            }
        //            else
        //            {
        //                shortageLength = shortageLength / 2;
        //                passwordBuilder.Append(RandomNumericString(shortageLength));
        //                shortageLength = input.PasswordPolicy.Min_Length - passwordBuilder.ToString().Length;
        //                if (shortageLength > 0)
        //                    passwordBuilder.Append(RandomString(shortageLength, true));
        //            }
        //        }
        //    }
        //    if (input.PasswordPolicy.Max_Length > 0)
        //    {
        //        int shortageLength = input.PasswordPolicy.Max_Length - passwordBuilder.ToString().Length;
        //        if (shortageLength > 0)
        //        {
        //            if (shortageLength <= 3)
        //            {
        //                passwordBuilder.Append(RandomString(shortageLength, true));
        //            }
        //            else
        //            {
        //                shortageLength = shortageLength / 2;
        //                passwordBuilder.Append(RandomNumericString(shortageLength));
        //                shortageLength = input.PasswordPolicy.Max_Length - passwordBuilder.ToString().Length;
        //                if (shortageLength > 0)
        //                    passwordBuilder.Append(RandomString(shortageLength, true));
        //            }
        //        }
        //    }
        //    string samplePassword = passwordBuilder.ToString();
        //    string randomPassword = new string(samplePassword.ToCharArray().OrderBy(s => (_random.Next(2) % 2) == 0).ToArray());
        //    if (randomPassword.Length>0)
        //        output += L("SamplePassword", randomPassword) + " <BR>";
        //    output += "</b>";
        //    return  new PasswordPolicyOutputDto { Description = output, SamplePassword = passwordBuilder.ToString()};
        //}


        public async Task<PasswordPolicyOutputDto> GetPasswordPolicy()
        {
            var passwordPolicySettingEditDto = await GetPassPolicySettingDtos();
            GetPassWordPolicyInputDto passedSettingInput = new GetPassWordPolicyInputDto { PasswordPolicy = passwordPolicySettingEditDto };
            var output = await GetPasswordPolicyForSetting(passedSettingInput);
            return output;
        }

        public async Task<PasswordPolicyOutputDto> CheckPasswordPolicyWithTheGivenPassword(PasswordPolicyInputDto inputPasswordDto)
        {
            string argPassword = inputPasswordDto.Password == null ? "" : inputPasswordDto.Password;
            string argUserName = inputPasswordDto.LoginUserName == null ? "" : inputPasswordDto.LoginUserName;
            string output = "";
            if (inputPasswordDto.UserId == 0)
            {
                output = L("UserIdCanNotBeZero") + " <BR>";
            }
            PasswordPolicyOutputDto outputDto = new PasswordPolicyOutputDto();
            PasswordPolicySettingEditDto passwordPolicySettingEditDto = await GetPassPolicySettingDtos();
            GetPassWordPolicyInputDto input = new GetPassWordPolicyInputDto { PasswordPolicy = passwordPolicySettingEditDto };

            if (passwordPolicySettingEditDto.Enforce == false)
            {
                if (argPassword.Length == 0)
                {
                    output = L("PasswordCanNotBeEmpty");
                }
            }
            else
            {
                #region Min / Max Length
                if (passwordPolicySettingEditDto.Min_Length > 0)
                {
                    if (argPassword.Length < passwordPolicySettingEditDto.Min_Length)
                        output += L("PasswordPolicyMinLength", input.PasswordPolicy.Min_Length) + " <BR>";
                }
                if (passwordPolicySettingEditDto.Max_Length > 0)
                {
                    if (argPassword.Length > passwordPolicySettingEditDto.Max_Length)
                        output += L("PasswordPolicyMaxLength", input.PasswordPolicy.Max_Length) + " <BR>";
                }
                #endregion

                if (input.PasswordPolicy.Min_LCL > 0)
                {
                    Regex x = new Regex("[a-z]{1}", RegexOptions.Compiled | RegexOptions.CultureInvariant);
                    int lowerletterCount = x.Matches(argPassword).Count;
                    if (input.PasswordPolicy.Min_LCL > lowerletterCount)
                        output += L("PasswordPolicyLowerCaseLetters", input.PasswordPolicy.Min_LCL) + " <BR>";
                }

                if (input.PasswordPolicy.Min_UCL > 0)
                {
                    Regex x = new Regex("[A-Z]{1}", RegexOptions.Compiled | RegexOptions.CultureInvariant);
                    int upperletterCount = x.Matches(argPassword).Count;
                    if (input.PasswordPolicy.Min_UCL > upperletterCount)
                        output += L("PasswordPolicyUpperCaseLetters", input.PasswordPolicy.Min_UCL) + " <BR>";
                }

                if (input.PasswordPolicy.Min_SC > 0)
                {
                    Regex x = new Regex("[^0-9a-zA-Z]", RegexOptions.Compiled | RegexOptions.CultureInvariant);
                    int splLetterCount = x.Matches(argPassword).Count;
                    if (input.PasswordPolicy.Min_SC > splLetterCount)
                        output += L("PasswordPolicySpecialCharacters", input.PasswordPolicy.Min_SC) + " <BR>";
                }

                if (input.PasswordPolicy.Min_AC > 0)
                {
                    Regex x = new Regex("[0-9a-zA-Z]", RegexOptions.Compiled | RegexOptions.CultureInvariant);
                    int alphaNumericLetterCount = x.Matches(argPassword).Count;
                    if (input.PasswordPolicy.Min_AC > alphaNumericLetterCount)
                        output += L("PasswordPolicyAlphaNumericCharacters", input.PasswordPolicy.Min_AC) + " <BR>";
                }

                if (input.PasswordPolicy.Impermissible_Passwords.Length > 0)
                {
                    var impermissibleList = input.PasswordPolicy.Impermissible_Passwords.ToLower().Split(",");
                    foreach (var impermissibleString in impermissibleList)
                    {
                        var lsttoCheck = impermissibleString.Trim().ToLower();
                        if (lsttoCheck.Length == 0)
                            continue;
                        lsttoCheck = lsttoCheck.Replace("*", "");

                        Regex x = new Regex(lsttoCheck.ToLower(), RegexOptions.Compiled | RegexOptions.CultureInvariant);
                        int impermissibleCount = x.Matches(argPassword.ToLower()).Count;
                        if (impermissibleCount > 0)
                            output += L("PasswordPolicyImpermissible_Passwords", lsttoCheck) + " <BR>";
                    }

                }

                if (input.PasswordPolicy.Allow_LIDPP == false)
                {
                    Regex x = new Regex(argUserName, RegexOptions.Compiled | RegexOptions.CultureInvariant);
                    int loginIdAsPartOfPasswordCount = x.Matches(argPassword).Count;
                    if (loginIdAsPartOfPasswordCount > 0)
                        output += L("PasswordPolicyAllow_LIDPP") + " <BR>";
                }

                if (input.PasswordPolicy.Allow_OPPP == false)
                {
                    var userOldPasswordArchieve = await _userPasswordArchiveRepo.GetAll().Where(t => t.UserId == inputPasswordDto.UserId).OrderByDescending(t => t.CreationTime).ToListAsync();
                    if (userOldPasswordArchieve.Count > 0)
                    {
                        var oldPasswordListCannotbeNewPasswordPart = userOldPasswordArchieve.Take(input.PasswordPolicy.Size).Select(t => t.Password).ToList();
                        foreach (var oldPw in oldPasswordListCannotbeNewPasswordPart)
                        {
                            Regex x = new Regex(oldPw, RegexOptions.Compiled | RegexOptions.CultureInvariant);
                            int oldPwAsPartOfNewPasswordCount = x.Matches(argPassword).Count;
                            var alreadyPartIdx = argPassword.IndexOf(oldPw);
                            if (oldPwAsPartOfNewPasswordCount > 0 || oldPw == argPassword || alreadyPartIdx >= 0)
                            {
                                output += L("OldPassWordCanNotBePartNewPassword", input.PasswordPolicy.Size) + " <BR>";
                                break;
                            }
                        }
                    }
                }

                if (input.PasswordPolicy.Suggest_Message != null && input.PasswordPolicy.Suggest_Message.Length > 0)
                {
                    output += L("Suggest_Message", input.PasswordPolicy.Suggest_Message) + " <BR>";
                }
            }
            if (output.Length > 0)
            {
                output = "<B>" + output + "</B>";
                outputDto.ErrorFlag = true;
            }
            outputDto.Description = output;
            return outputDto;
        }

        public async Task<PasswordPolicyOutputDto> CheckPasswordWhenCreateUser(PasswordPolicyInputDto inputPasswordDto)
        {
            string argPassword = inputPasswordDto.Password == null ? "" : inputPasswordDto.Password;
            string argUserName = inputPasswordDto.LoginUserName == null ? "" : inputPasswordDto.LoginUserName;
            string output = "";
            PasswordPolicyOutputDto outputDto = new PasswordPolicyOutputDto();
            PasswordPolicySettingEditDto passwordPolicySettingEditDto = await GetPassPolicySettingDtos();
            GetPassWordPolicyInputDto input = new GetPassWordPolicyInputDto { PasswordPolicy = passwordPolicySettingEditDto };

            if (passwordPolicySettingEditDto.Enforce == false)
            {
                if (argPassword.Length == 0)
                {
                    output = L("PasswordCanNotBeEmpty");
                }
            }
            else
            {
                #region Min / Max Length
                if (passwordPolicySettingEditDto.Min_Length > 0)
                {
                    if (argPassword.Length < passwordPolicySettingEditDto.Min_Length)
                        output += L("PasswordPolicyMinLength", input.PasswordPolicy.Min_Length) + " <BR>";
                }
                if (passwordPolicySettingEditDto.Max_Length > 0)
                {
                    if (argPassword.Length > passwordPolicySettingEditDto.Max_Length)
                        output += L("PasswordPolicyMaxLength", input.PasswordPolicy.Max_Length) + " <BR>";
                }
                #endregion

                if (input.PasswordPolicy.Min_LCL > 0)
                {
                    Regex x = new Regex("[a-z]{1}", RegexOptions.Compiled | RegexOptions.CultureInvariant);
                    int lowerletterCount = x.Matches(argPassword).Count;
                    if (input.PasswordPolicy.Min_LCL > lowerletterCount)
                        output += L("PasswordPolicyLowerCaseLetters", input.PasswordPolicy.Min_LCL) + " <BR>";
                }

                if (input.PasswordPolicy.Min_UCL > 0)
                {
                    Regex x = new Regex("[A-Z]{1}", RegexOptions.Compiled | RegexOptions.CultureInvariant);
                    int upperletterCount = x.Matches(argPassword).Count;
                    if (input.PasswordPolicy.Min_UCL > upperletterCount)
                        output += L("PasswordPolicyUpperCaseLetters", input.PasswordPolicy.Min_UCL) + " <BR>";
                }

                if (input.PasswordPolicy.Min_SC > 0)
                {
                    Regex x = new Regex("[^0-9a-zA-Z]", RegexOptions.Compiled | RegexOptions.CultureInvariant);
                    int splLetterCount = x.Matches(argPassword).Count;
                    if (input.PasswordPolicy.Min_SC > splLetterCount)
                        output += L("PasswordPolicySpecialCharacters", input.PasswordPolicy.Min_SC) + " <BR>";
                }

                if (input.PasswordPolicy.Min_AC > 0)
                {
                    Regex x = new Regex("[0-9a-zA-Z]", RegexOptions.Compiled | RegexOptions.CultureInvariant);
                    int alphaNumericLetterCount = x.Matches(argPassword).Count;
                    if (input.PasswordPolicy.Min_AC > alphaNumericLetterCount)
                        output += L("PasswordPolicyAlphaNumericCharacters", input.PasswordPolicy.Min_AC) + " <BR>";
                }

                if (input.PasswordPolicy.Impermissible_Passwords.Length > 0)
                {
                    var impermissibleList = input.PasswordPolicy.Impermissible_Passwords.ToLower().Split(",");
                    foreach (var impermissibleString in impermissibleList)
                    {
                        var lsttoCheck = impermissibleString.Trim().ToLower();
                        if (lsttoCheck.Length == 0)
                            continue;
                        lsttoCheck = lsttoCheck.Replace("*", "");

                        Regex x = new Regex(lsttoCheck.ToLower(), RegexOptions.Compiled | RegexOptions.CultureInvariant);
                        int impermissibleCount = x.Matches(argPassword.ToLower()).Count;
                        if (impermissibleCount > 0)
                            output += L("PasswordPolicyImpermissible_Passwords", lsttoCheck) + " <BR>";
                    }

                }

                if (input.PasswordPolicy.Allow_LIDPP == false)
                {
                    Regex x = new Regex(argUserName, RegexOptions.Compiled | RegexOptions.CultureInvariant);
                    int loginIdAsPartOfPasswordCount = x.Matches(argPassword).Count;
                    if (loginIdAsPartOfPasswordCount > 0)
                        output += L("PasswordPolicyAllow_LIDPP") + " <BR>";
                }
                if (input.PasswordPolicy.Suggest_Message != null && input.PasswordPolicy.Suggest_Message.Length > 0)
                {
                    output += L("Suggest_Message", input.PasswordPolicy.Suggest_Message) + " <BR>";
                }
            }
            if (output.Length > 0)
            {
                output = "<B>" + output + "</B>";
                outputDto.ErrorFlag = true;
            }
            outputDto.Description = output;
            return outputDto;
        }


        public async Task<PasswordPolicySettingEditDto> GetPassPolicySettingDtos()
        {
            var min_Length = await _settingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_Length);
            var max_Length = await _settingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Max_Length);
            var min_LCL = await _settingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_LCL);
            var min_UCL = await _settingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_UCL);
            var min_SC = await _settingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_SC);
            var min_AC = await _settingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_AC);
            var impermissible_Passwords = await _settingManager.GetSettingValueAsync(AppSettings.PasswordPolicySettings.Impermissible_Passwords);
            var allow_LIDPP = await _settingManager.GetSettingValueAsync<bool>(AppSettings.PasswordPolicySettings.Allow_LIDPP);
            var allow_OPPP = await _settingManager.GetSettingValueAsync<bool>(AppSettings.PasswordPolicySettings.Allow_OPPP);
            var allow_UCOP = await _settingManager.GetSettingValueAsync<bool>(AppSettings.PasswordPolicySettings.Allow_UCOP);
            var oldPassword_Size = await _settingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Size);
            var enforce = await _settingManager.GetSettingValueAsync<bool>(AppSettings.PasswordPolicySettings.Enforce);

            PasswordPolicySettingEditDto passwordPolicySettingEditDto = new PasswordPolicySettingEditDto
            {
                Min_Length = min_Length,
                Max_Length = max_Length,
                Min_UCL = min_UCL,
                Min_LCL = min_LCL,
                Min_SC = min_SC,
                Min_AC = min_AC,
                Impermissible_Passwords = impermissible_Passwords,
                Allow_LIDPP = allow_LIDPP,
                Allow_OPPP = allow_OPPP,
                Allow_UCOP = allow_UCOP,
                Size = oldPassword_Size,
                Enforce = enforce
            };
            return passwordPolicySettingEditDto;
        }

        public async Task<PasswordPolicyOutputDto> GetPasswordPolicyForSetting(GetPassWordPolicyInputDto input)
        {
            if (input.PasswordPolicy.Min_Length == 0 && input.PasswordPolicy.Max_Length == 0)
            {
                return null;
            }

            var passwordBuilder = new StringBuilder();
            string output = "<B>";
            //if (input.PasswordPolicy.Allow_UCOP == true)
            //{
            //	output += L("PasswordPolicyAllow_UCOP") + " <BR>";
            //}
            //else
            //{
            if (input.PasswordPolicy.Min_Length > 0)
                output += L("PasswordPolicyMinLength", input.PasswordPolicy.Min_Length) + " <BR>";
            if (input.PasswordPolicy.Max_Length > 0)
                output += L("PasswordPolicyMaxLength", input.PasswordPolicy.Max_Length) + " <BR>";
            if (input.PasswordPolicy.Min_LCL > 0)
            {
                output += L("PasswordPolicyLowerCaseLetters", input.PasswordPolicy.Min_LCL) + " <BR>";
                passwordBuilder.Append(RandomString(input.PasswordPolicy.Min_LCL, true));
            }
            if (input.PasswordPolicy.Min_UCL > 0)
            {
                output += L("PasswordPolicyUpperCaseLetters", input.PasswordPolicy.Min_UCL) + " <BR>";
                passwordBuilder.Append(RandomString(input.PasswordPolicy.Min_UCL, false));
            }
            if (input.PasswordPolicy.Min_SC > 0)
            {
                output += L("PasswordPolicySpecialCharacters", input.PasswordPolicy.Min_SC) + " <BR>";
                passwordBuilder.Append(RandomSplCharacterString(input.PasswordPolicy.Min_SC));
            }
            if (input.PasswordPolicy.Min_AC > 0)
                output += L("PasswordPolicyAlphaNumericCharacters", input.PasswordPolicy.Min_AC) + " <BR>";

            if (passwordBuilder.ToString().Length < input.PasswordPolicy.Min_Length)
            {
                int shortageLength = input.PasswordPolicy.Min_Length - passwordBuilder.ToString().Length;
                if (shortageLength > 0)
                {
                    if (shortageLength <= 3)
                    {
                        passwordBuilder.Append(RandomString(shortageLength, true));
                    }
                    else
                    {
                        shortageLength = shortageLength / 2;
                        passwordBuilder.Append(RandomNumericString(shortageLength));
                        shortageLength = input.PasswordPolicy.Min_Length - passwordBuilder.ToString().Length;
                        if (shortageLength > 0)
                            passwordBuilder.Append(RandomString(shortageLength, true));
                    }
                }
            }
            if (input.PasswordPolicy.Max_Length > 0)
            {
                int shortageLength = input.PasswordPolicy.Max_Length - passwordBuilder.ToString().Length;
                if (shortageLength > 0)
                {
                    if (shortageLength <= 3)
                    {
                        passwordBuilder.Append(RandomString(shortageLength, true));
                    }
                    else
                    {
                        shortageLength = shortageLength / 2;
                        passwordBuilder.Append(RandomNumericString(shortageLength));
                        shortageLength = input.PasswordPolicy.Max_Length - passwordBuilder.ToString().Length;
                        if (shortageLength > 0)
                            passwordBuilder.Append(RandomString(shortageLength, true));
                    }
                }
            }
            if (input.PasswordPolicy.Impermissible_Passwords != null && input.PasswordPolicy.Impermissible_Passwords.Length > 0)
            {
                output += L("PasswordPolicyImpermissible_Passwords", input.PasswordPolicy.Impermissible_Passwords) + " <BR>";
            }

            if (input.PasswordPolicy.Allow_LIDPP == false)
            {
                output += L("PasswordPolicyAllow_LIDPP", input.PasswordPolicy.Impermissible_Passwords) + " <BR>";
            }

            if (input.PasswordPolicy.Allow_OPPP == false)
            {
                if (input.PasswordPolicy.Size > 0)
                    output += L("OldPassWordCanNotBePartNewPassword", input.PasswordPolicy.Size) + " <BR>";
            }

            if (input.PasswordPolicy.Suggest_Message != null && input.PasswordPolicy.Suggest_Message.Length > 0)
            {
                output += L("Suggest_Message", input.PasswordPolicy.Suggest_Message) + " <BR>";
            }
            #region PasswordPolicy Validation
            if (input.PasswordPolicy != null)
            {
                if (input.PasswordPolicy.Min_Length > 0 && input.PasswordPolicy.Max_Length > 0)
                {
                    int passwordLength = 0;
                    if (input.PasswordPolicy.Min_LCL > 0)
                    {
                        passwordLength = passwordLength + input.PasswordPolicy.Min_LCL;
                    }
                    if (input.PasswordPolicy.Min_UCL > 0)
                    {
                        passwordLength = passwordLength + input.PasswordPolicy.Min_UCL;
                    }
                    if (input.PasswordPolicy.Min_SC > 0)
                    {
                        passwordLength = passwordLength + input.PasswordPolicy.Min_SC;
                    }

                    if (passwordLength > input.PasswordPolicy.Max_Length)
                    {
                        throw new UserFriendlyException(L("LCL_UCL_SC_LengthError", passwordLength, input.PasswordPolicy.Max_Length));
                    }
                }
            }
            #endregion
            string samplePassword = passwordBuilder.ToString();
            string randomPassword = new string(samplePassword.ToCharArray().OrderBy(s => (_random.Next(2) % 2) == 0).ToArray());
            if (randomPassword.Length > 0)
                output += L("SamplePassword", randomPassword) + " <BR>";
            samplePassword = randomPassword;
            //}
            output += "</B>";
            return new PasswordPolicyOutputDto { Description = output, SamplePassword = passwordBuilder.ToString() };
        }


        public string RandomString(int size, bool lowerCase = false)
        {
            var builder = new StringBuilder(size);

            // Unicode/ASCII Letters are divided into two blocks
            // (Letters 65–90 / 97–122):   
            // The first group containing the uppercase letters and
            // the second group containing the lowercase.  

            // char is a single Unicode character  
            char offset = lowerCase ? 'a' : 'A';
            const int lettersOffset = 26; // A...Z or a..z: length = 26  

            for (var i = 0; i < size; i++)
            {
                var @char = (char)_random.Next(offset, offset + lettersOffset);
                builder.Append(@char);
            }

            return lowerCase ? builder.ToString().ToLower() : builder.ToString();
        }

        public string RandomNumericString(int size)
        {
            var builder = new StringBuilder(size);

            // Unicode/ASCII Letters are divided into two blocks
            // (Letters 65–90 / 97–122):   
            // The first group containing the uppercase letters and
            // the second group containing the lowercase.  

            // char is a single Unicode character  
            char offset = '0';
            const int lettersOffset = 10; // A...Z or a..z: length = 26  

            for (var i = 0; i < size; i++)
            {
                var @char = (char)_random.Next(offset, offset + lettersOffset);
                builder.Append(@char);
            }

            return builder.ToString();
        }

        public string RandomSplCharacterString(int size)
        {
            var builder = new StringBuilder(size);

            // Unicode/ASCII Letters are divided into two blocks
            // (Letters 65–90 / 97–122):   
            // The first group containing the uppercase letters and
            // the second group containing the lowercase.  

            // char is a single Unicode character  
            string specialCharactorSet = "!@#$%^&*()+<>?~";
            int lettersOffset = specialCharactorSet.Length; // A...Z or a..z: length = 26  

            for (var i = 0; i < size; i++)
            {
                var @char = (char)specialCharactorSet[_random.Next(0, lettersOffset - 1)];
                builder.Append(@char);
            }

            return builder.ToString();
        }


        public async Task UpdateSFL_UserInfo(UserInput input)
        {
            var dto = input.User;
            var user = await UserManager.FindByIdAsync(input.User.Id.Value);
            user.UserJsonData = JsonConvert.SerializeObject(dto.UserJsonDataObject);
            await UserManager.UpdateAsync(user);
        }
        public async Task<PasswordPolicyOutputDto> GenerateSamplePasswordBasedOnPasswordPolicy()
        {
            var passwordPolicySettingEditDto = await GetPassPolicySettingDtos();
            if (passwordPolicySettingEditDto.Min_Length == 0 && passwordPolicySettingEditDto.Max_Length == 0)
            {
                passwordPolicySettingEditDto = new PasswordPolicySettingEditDto
                {
                    Min_Length = 5,
                    Max_Length = 7,
                    Min_UCL = 2,
                    Min_LCL = 2,
                    Min_SC = 2,
                    Min_AC = 0,
                    Impermissible_Passwords = "",
                    Allow_LIDPP = false,
                    Allow_OPPP = false,
                    Allow_UCOP = false,
                    Size = 6,
                    Suggest_Message = ""
                };
            }

            GetPassWordPolicyInputDto passedSettingInput = new GetPassWordPolicyInputDto { PasswordPolicy = passwordPolicySettingEditDto };
            var output = await GetPasswordPolicyForSetting(passedSettingInput);
            return output;
        }
        public async Task GenerateNewPassword(UserInput input)
        {
            var dto = input.User;
            var user = await UserManager.FindByIdAsync(input.User.Id.Value);
            user.UserJsonData = JsonConvert.SerializeObject(dto.UserJsonDataObject);

            var passWordPolicy = await GenerateSamplePasswordBasedOnPasswordPolicy();
            {
                input.User.Password = passWordPolicy.SamplePassword;
            }
            if (AbpSession.UserId.HasValue)
            {
                input.User.CurrentUserId = AbpSession.UserId == null ? 0 : AbpSession.UserId.Value;
                var currentUserInfo = await UserManager.FindByIdAsync(input.User.CurrentUserId);
                if (!string.IsNullOrEmpty(currentUserInfo.Name))
                {
                    input.User.CurrentUserName = currentUserInfo.Name;
                }
                else
                {
                    input.User.CurrentUserName = L("DinePlan");
                }
            }
            else
            {
                input.User.CurrentUserName = L("DinePlan");
            }
            ResetPasswordInput resetPasswordInput = new ResetPasswordInput
            {
                UserName = user.Name,
                NewPassword = input.User.Password,
                EmailAddress = user.EmailAddress,
                Sender = input.User.CurrentUserName,
                TenantId = user.TenantId.Value
            };
            input.User.MapTo(user);
            CheckErrors(await UserManager.ChangePasswordAsync(user, input.User.Password));
            //CheckErrors(await UserManager.UpdateAsync(user));
            await EmailResetPassword(resetPasswordInput);
        }

        public async Task EmailResetPassword(ResetPasswordInput input)
        {
            var template = await _emailTemplate.FirstOrDefaultAsync(a => a.TenantId == input.TenantId && a.Name.Equals("RESET PASSWORD"));
            if (template == null)
            {
                await _emailTemplateAppService.CreateEmailTemplateForResetPassword(input.TenantId);
                template = await _emailTemplate.FirstOrDefaultAsync(a => a.TenantId == input.TenantId && a.Name.Equals("RESET PASSWORD"));
            }

            await _bgm.EnqueueAsync<ResetPasswordJob, ResetPasswordJobArgs>(new ResetPasswordJobArgs
            {
                UserName = input.UserName,
                NewPassword = input.NewPassword,
                Sender = input.Sender,
                EmailAddress = input.EmailAddress,
                EmailTemplate = template
            });
        }

        public async Task EmailRandomPassword(ResetPasswordInput input)
        {

            await _bgm.EnqueueAsync<RandomPasswordJob, ResetPasswordJobArgs>(new ResetPasswordJobArgs
            {
                UserName = input.UserName,
                NewPassword = input.NewPassword,
                Sender = input.Sender,
                EmailAddress = input.EmailAddress,
                TenantId = input.TenantId
            });
        }
        public virtual async Task<IdInput> CreateMemberAsync(CreateOrUpdateUserInput input)
        {
            User user = new User();
            user.Name = input.User.Name;
            user.Surname = input.User.Surname;
            user.UserName = input.User.UserName;
            user.EmailAddress = input.User.EmailAddress;
            user.IsActive = input.User.IsActive;
            user.Password = input.User.Password;
            user.UserJsonData = input.User.UserJsonData;

            user.TenantId = AbpSession.TenantId;

            //Set password

            //var passWordPolicy = await GetPasswordPolicy();
            //if (passWordPolicy == null)
            //    input.User.Password = User.CreateRandomPassword();
            //else
            //    input.User.Password = passWordPolicy.SamplePassword;
            var passWordPolicy = await GenerateSamplePasswordBasedOnPasswordPolicy();
            {
                input.User.Password = passWordPolicy.SamplePassword;
            }

            //Update User Password Archive
            UserPasswordArchive userPasswordArchive = new UserPasswordArchive
            {
                UserId = (int)user.Id,
                Password = user.Password
            };

            await _userPasswordArchiveRepo.InsertAndGetIdAsync(userPasswordArchive);
            user.Password = new PasswordHasher().HashPassword(input.User.Password);
            user.ShouldChangePasswordOnNextLogin = input.User.ShouldChangePasswordOnNextLogin;

            //Assign roles
            user.Roles = new Collection<UserRole>();
            foreach (var roleName in input.AssignedRoleNames)
            {
                var role = await _roleManager.GetRoleByNameAsync(roleName);
                user.Roles.Add(new UserRole { RoleId = role.Id });
            }

            CheckErrors(await UserManager.CreateAsync(user));
            await CurrentUnitOfWork.SaveChangesAsync(); //To get new user's Id.

            //Notifications
            await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(user.TenantId, user.Id);
            await _appNotifier.WelcomeToTheApplicationAsync(user);

            //Send activation email
            if (input.SendActivationEmail)
            {
                user.SetNewEmailConfirmationCode();
                await _userEmailer.SendEmailActivationLinkAsync(user, input.User.Password);
            }

            return new IdInput { Id = int.Parse(user.Id.ToString()) };
        }

        [AbpAuthorize]
        public async Task<LockUserOutput> ApiLockUser(LockUserInput input)
        {
            LockUserOutput returnOutput = new LockUserOutput();
            if (string.IsNullOrEmpty(input.UserName))
            {
                returnOutput.Status = false;
                returnOutput.ErrorDescription = "Input Username is empty";
                return returnOutput;
            }

            var user = await UserManager.FindByNameAsync(input.UserName);

            if (user == null)
            {
                returnOutput.Status = false;
                returnOutput.ErrorDescription = "User is not available : " + input.UserName;
                return returnOutput;
            }

            //user.IsActive = false;
            returnOutput.Status = true;
            return returnOutput;

        }

        [AbpAuthorize]
        public async Task<LockUserOutput> ApiUnLockUser(LockUserInput input)
        {
            LockUserOutput returnOutput = new LockUserOutput();
            if (string.IsNullOrEmpty(input.UserName))
            {
                returnOutput.Status = false;
                returnOutput.ErrorDescription = "Input Username is empty";
                return returnOutput;
            }

            var user = await UserManager.FindByNameAsync(input.UserName);

            if (user == null)
            {
                returnOutput.Status = false;
                returnOutput.ErrorDescription = "User is not available : " + input.UserName;
                return returnOutput;
            }

            user.IsActive = true;
            returnOutput.Status = true;
            return returnOutput;
        }

        #region UserManagement

        public async Task<PagedResultOutput<UserListDto>> ApiSearchUsers(ApiLocationInput input)
        {
            _unitOfWorkManager.Current.EnableFilter(AbpDataFilters.MayHaveTenant);
            _unitOfWorkManager.Current.SetFilterParameter(AbpDataFilters.MayHaveTenant, AbpDataFilters.Parameters.TenantId, input.TenantId);

            var query = UserManager.Users
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    u =>
                        u.Name.Contains(input.Filter) ||
                        u.Surname.Contains(input.Filter) ||
                        u.UserName.Contains(input.Filter) ||
                        u.EmailAddress.Contains(input.Filter)
                );

            var users = await query.ToListAsync();
            var dtos = users.OrderBy(x => x.Name).MapTo<List<UserListDto>>();

            return new PagedResultOutput<UserListDto>(users.Count, dtos);
        }

        public virtual async Task<IdInput> ApiCreateUser(CreateOrUpdateUserInput input)
        {
            User user = new User
            {
                Name = input.User.Name,
                Surname = input.User.Surname,
                UserName = input.User.UserName,
                EmailAddress = input.User.EmailAddress,
                IsActive = input.User.IsActive,
                Password = input.User.Password,
                UserJsonData = input.User.UserJsonData,
                TenantId = input.TenantId
            };

            //Set password
            if (!input.User.Password.IsNullOrEmpty())
            {
                CheckErrors(await UserManager.PasswordValidator.ValidateAsync(input.User.Password));
            }
            else
            {
                var passWordPolicy = await GenerateSamplePasswordBasedOnPasswordPolicy();
                {
                    input.User.Password = passWordPolicy.SamplePassword;
                }
            }

            //Update User Password Archive
            UserPasswordArchive userPasswordArchive = new UserPasswordArchive
            {
                UserId = (int)user.Id,
                Password = user.Password
            };

            await _userPasswordArchiveRepo.InsertAndGetIdAsync(userPasswordArchive);
            user.Password = new PasswordHasher().HashPassword(input.User.Password);
            user.ShouldChangePasswordOnNextLogin = input.User.ShouldChangePasswordOnNextLogin;

            //Assign roles
            user.Roles = new Collection<UserRole>();
            foreach (var roleName in input.AssignedRoleNames)
            {
                var role = await _roleManager.GetRoleByNameAsync(roleName);
                user.Roles.Add(new UserRole { RoleId = role.Id });
            }

            CheckErrors(await UserManager.CreateAsync(user));
            await CurrentUnitOfWork.SaveChangesAsync(); //To get new user's Id.

            //Notifications
            await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(user.TenantId, user.Id);
            await _appNotifier.WelcomeToTheApplicationAsync(user);

            //Send activation email
            if (input.SendActivationEmail)
            {
                user.SetNewEmailConfirmationCode();
                await _userEmailer.SendEmailActivationLinkAsync(user, input.User.Password);
            }

            return new IdInput { Id = int.Parse(user.Id.ToString()) };
        }

        public async Task ApiDeleteUser(ApiDeleteUserInput input)
        {
            _unitOfWorkManager.Current.EnableFilter(AbpDataFilters.MayHaveTenant);
            _unitOfWorkManager.Current.SetFilterParameter(AbpDataFilters.MayHaveTenant, AbpDataFilters.Parameters.TenantId, input.TenantId);


            var user = await UserManager.GetUserByIdAsync(input.Id);
            CheckErrors(await UserManager.DeleteAsync(user));
        }

        public virtual async Task<IdInput> ApiUpdateUser(CreateOrUpdateUserInput input)
        {
            _unitOfWorkManager.Current.EnableFilter(AbpDataFilters.MayHaveTenant);
            _unitOfWorkManager.Current.SetFilterParameter(AbpDataFilters.MayHaveTenant, AbpDataFilters.Parameters.TenantId, input.TenantId);

            var user = await UserManager.FindByIdAsync(input.User.Id.Value);
            //Update user properties
            input.User.MapTo(user); //Passwords is not mapped (see mapping configuration)

            CheckErrors(await UserManager.UpdateAsync(user));

            //Update roles
            CheckErrors(await UserManager.SetRoles(user, input.AssignedRoleNames));

            if (input.SendActivationEmail)
            {
                user.SetNewEmailConfirmationCode();
                await _userEmailer.SendEmailActivationLinkAsync(user, input.User.Password);
            }

            return new IdInput { Id = int.Parse(user.Id.ToString()) };
        }

        #endregion
    }
}
