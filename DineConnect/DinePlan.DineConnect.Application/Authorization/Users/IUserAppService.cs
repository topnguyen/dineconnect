﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Authorization.Users.Dto;
using DinePlan.DineConnect.Configuration.Tenants.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Authorization.Users
{
    public interface IUserAppService : IApplicationService
    {
        Task<PagedResultOutput<UserListDto>> GetUsers(GetUsersInput input);

        Task<FileDto> GetUsersToExcel();

        Task<GetUserForEditOutput> GetUserForEdit(GetUserInfoInputDto input);
        Task<ApiUserDto> ApiGetUser(ApiLocationInput input);


        Task<GetUserPermissionsForEditOutput> GetUserPermissionsForEdit(IdInput<long> input);

        Task ResetUserSpecificPermissions(IdInput<long> input);

        Task UpdateUserPermissions(UpdateUserPermissionsInput input);

        Task<IdInput> CreateOrUpdateUser(CreateOrUpdateUserInput input);
        Task DeleteUser(IdInput<long> input);
        Task<UserListDto> GetUserInfoBasedOnUserId(IdInput input);
        Task<bool> CheckMemberRoleExists(long id, int value);
        Task<PasswordPolicyOutputDto> GetPasswordPolicyForSetting(GetPassWordPolicyInputDto input);
        Task<PasswordPolicyOutputDto> GetPasswordPolicy();
        Task<PasswordPolicyOutputDto> CheckPasswordPolicyWithTheGivenPassword(PasswordPolicyInputDto inputPasswordDto);
        Task<PasswordPolicyOutputDto> CheckPasswordWhenCreateUser(PasswordPolicyInputDto inputPasswordDto);
        Task UpdateSFL_UserInfo(UserInput input);
        Task GenerateNewPassword(UserInput input);
        Task<IdInput> CreateMemberAsync(CreateOrUpdateUserInput input);

        Task<LockUserOutput> ApiLockUser(LockUserInput input);
        Task<LockUserOutput> ApiUnLockUser(LockUserInput input);

        Task<PagedResultOutput<UserListDto>> ApiSearchUsers(ApiLocationInput input);
        Task<IdInput> ApiCreateUser(CreateOrUpdateUserInput input);
        Task ApiDeleteUser(ApiDeleteUserInput input);
        Task<IdInput> ApiUpdateUser(CreateOrUpdateUserInput input);
    }
}