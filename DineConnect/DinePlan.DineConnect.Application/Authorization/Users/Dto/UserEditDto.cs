﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using Abp.Runtime.Validation;

namespace DinePlan.DineConnect.Authorization.Users.Dto
{
    //Mapped to/from User in CustomDtoMapper
    [AutoMapTo(typeof(User))]
    public class UserEditDto : IValidate, IPassivable
    {
        /// <summary>
        /// Set null to create a new user. Set user's Id to update a user
        /// </summary>
        public UserEditDto()
        {
            UserJsonDataObject = new UserJsonMaster();
        }
        public long? Id { get; set; }

        [Required]
        [StringLength(User.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(User.MaxSurnameLength)]
        public string Surname { get; set; }

        [Required]
        [StringLength(User.MaxUserNameLength)]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(User.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        // Not used "Required" attribute since empty value is used to 'not change password'

        public string Password { get; set; }

        public bool IsActive { get; set; }

        public bool ShouldChangePasswordOnNextLogin { get; set; }
        public DateTime CreationTime { get; set; }
        public string UserJsonData { get; set; }
        public UserJsonMaster UserJsonDataObject { get; set; }
        public string NewPassword { get; set; }
        public long CurrentUserId { get; set; }
        public string CurrentUserName { get; set; }
    }
    public class GetUserInfoInputDto
    {
        public int? Id { get; set; }
        public string FilterText { get; set; }
    }
    public class UserJsonMaster
    {
        public virtual int PasswordPolicy { get; set; }
        public virtual DateTime? GLTGV { get; set; } //User Valid From
        public virtual DateTime? GLTGB { get; set; } //User Valid To
        public virtual string Language { get; set; }
        public virtual string Tel1_Number { get; set; }
        public virtual string Tel1_Ext { get; set; }
        public virtual string FaxNumber { get; set; }
        public virtual string Mobile { get; set; }
        public virtual string Department { get; set; }
        public virtual string Role_Name { get; set; }
        public virtual int NFLA { get; set; } //Number of Failed Logon Attempts
        public virtual DateTime? NFLA_Date { get; set; } //Number of Failed Logon Attempts Time
        public virtual int SFL_User { get; set; } //Status Failed Logon of user
        public virtual string UserActivationCode { get; set; }
        public bool IsLocked { get; set; }
    }
}