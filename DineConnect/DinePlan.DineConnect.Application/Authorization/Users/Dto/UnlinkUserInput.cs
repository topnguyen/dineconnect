using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Authorization.Users.Dto
{
    public class UnlinkUserInput : IInputDto
    {
        public long UserId { get; set; }
    }
}