﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Authorization.Users.Dto
{
    public class CreateOrUpdateUserInput : IInputDto
    {
        [Required]
        public UserEditDto User { get; set; }

        [Required]
        public string[] AssignedRoleNames { get; set; }

        public bool SendActivationEmail { get; set; }
        public int TenantId { get; set; }
    }
    public class UserInput : IInputDto
    {
        public UserEditDto User { get; set; }
    }
    public class ResetPasswordInput
    {
        public string UserName { get; set; }
        public string NewPassword { get; set; }
        public string Sender { get;set; }
        public string EmailAddress { get; set; }
        public int TenantId { get; set; }
    }

    public class LockUserInput
    {
        public string UserName { get; set; }
    }

    public class LockUserOutput
    {
        public bool Status { get; set; }
        public string ErrorDescription { get; set; }

    }
    public class ApiDeleteUserInput
    {
        public long Id { get; set; }
        public int TenantId { get; set; }

    }
}