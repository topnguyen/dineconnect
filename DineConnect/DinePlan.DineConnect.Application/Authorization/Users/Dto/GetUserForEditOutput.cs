﻿using System;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Authorization.Users.Dto
{
    public class GetUserForEditOutput : IOutputDto
    {
        public Guid? ProfilePictureId { get; set; }

        public UserEditDto User { get; set; }

        public UserRoleDto[] Roles { get; set; }

		public List<ComboboxItemDto> CompanyList { get; set; }

		public List<LocationListDto> Locations { get; set; }

		public int? DefaultLocationRefId { get; set; }
    }


    public class ApiUserDto : IOutputDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
    }
}