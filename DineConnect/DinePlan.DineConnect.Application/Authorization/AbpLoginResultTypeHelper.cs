﻿using System;
using Abp.Authorization.Users;
using Abp.Dependency;
using Abp.UI;

namespace DinePlan.DineConnect.Authorization
{
    public class AbpLoginResultTypeHelper : DineConnectServiceBase, ITransientDependency
    {
        public Exception CreateExceptionForFailedLoginAttempt(AbpLoginResultType result, string usernameOrEmailAddress,
            string tenancyName)
        {
            switch (result)
            {
                case AbpLoginResultType.Success:
                    return new ApplicationException("Don't call this method with a success result!");
                case AbpLoginResultType.InvalidUserNameOrEmailAddress:
                case AbpLoginResultType.InvalidPassword:
                    return new UserFriendlyException(L("InvalidUserNameOrPassword"), L("LoginFailed"));
                case AbpLoginResultType.InvalidTenancyName:
                    return new UserFriendlyException(L("ThereIsNoTenantDefinedWithName{0}", tenancyName),
                        L("LoginFailed"));
                case AbpLoginResultType.TenantIsNotActive:
                    return new UserFriendlyException( L("TenantIsNotActive", tenancyName),L("LoginFailed"));
                case AbpLoginResultType.UserIsNotActive:
                    return new UserFriendlyException(
                        L("UserIsNotActiveAndCanNotLogin", usernameOrEmailAddress),L("LoginFailed"));
                case AbpLoginResultType.UserEmailIsNotConfirmed:
                    return new UserFriendlyException(L("UserEmailIsNotConfirmedAndCanNotLogin"),L("LoginFailed"));
                default:
                    Logger.Warn("Unhandled login fail reason: " + result);
                    return new UserFriendlyException(L("LoginFailed"));
            }
        }
    }
}