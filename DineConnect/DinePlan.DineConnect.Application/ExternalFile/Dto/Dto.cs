﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.ExternalFile.Dto
{
    public class ExternalFileOutput : IOutputDto
    {
    }

    public class ExternalFileInput : IInputDto
    {
    }
}
