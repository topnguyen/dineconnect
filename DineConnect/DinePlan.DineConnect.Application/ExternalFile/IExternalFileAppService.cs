﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.ExternalFile.Dto;

namespace DinePlan.DineConnect.ExternalFile
{
    public interface IExternalFileAppService : IApplicationService
    {
        Task<ExternalFileOutput> GetFilePath(ExternalFileInput input);
    }
}