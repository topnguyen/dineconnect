﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master
{
    public interface IDelAggLocationGroupAppService : IApplicationService
    {
        Task<PagedResultOutput<DelAggLocationGroupListDto>> GetAll(GetDelAggLocationGroupInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggLocationGroupInput input);
        Task<GetDelAggLocationGroupForEditOutput> GetDelAggLocationGroupForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateDelAggLocationGroup(CreateOrUpdateDelAggLocationGroupInput input);
        Task DeleteDelAggLocationGroup(IdInput input);
        Task<ListResultOutput<DelAggLocationGroupListDto>> GetNames();
    }
}