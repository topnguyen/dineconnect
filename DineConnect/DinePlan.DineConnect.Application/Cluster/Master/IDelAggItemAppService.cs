﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Member.Dtos.DinePlan.DineConnect.Engage.Dtos;

namespace DinePlan.DineConnect.Cluster.Master
{
    public interface IDelAggItemAppService : IApplicationService
    {
        Task<PagedResultOutput<DelAggItemListDto>> GetAll(GetDelAggItemInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggItemInput input);
        Task<GetDelAggItemForEditOutput> GetDelAggItemForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateDelAggItem(CreateOrUpdateDelAggItemInput input);
        Task DeleteDelAggItem(IdInput input);
        Task<ListResultOutput<DelAggItemListDto>> GetNames();
        Task<List<DelAggItemEditDto>> GetImportDelAggItemDetail(GetDelAggItemInput input);
        Task<FileDto> BulkDelAggItemImport(BulkDelAggItemImportDtos bulkList);
        Task<MessageOutputDto> ConvertAsMenuItemAsDelAggItem(TenantUserIdInput input);
        Task<FileDto> CloneItems(CloneDelAggItemDtos input);
        Task InitiateBackGroundClone();


    }
}