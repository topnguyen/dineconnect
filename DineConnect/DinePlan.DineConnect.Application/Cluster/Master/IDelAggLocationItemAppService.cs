﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Cluster.Dtos;

namespace DinePlan.DineConnect.Cluster.Master.Implementation
{
    public interface IDelAggLocationItemAppService : IApplicationService
    {
        Task<PagedResultOutput<DelAggLocationItemListDto>> GetAll(GetDelAggLocationItemInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggLocationItemInput input);
        Task<GetDelAggLocationItemForEditOutput> GetDelAggLocationItemForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateDelAggLocationItem(CreateOrUpdateDelAggLocationItemInput input);
        Task DeleteDelAggLocationItem(IdInput input);
    }
}