﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master
{
    public interface IDelAggItemGroupAppService : IApplicationService
    {
        Task<PagedResultOutput<DelAggItemGroupListDto>> GetAll(GetDelAggItemGroupInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggItemGroupInput input);
        Task<GetDelAggItemGroupForEditOutput> GetDelAggItemGroupForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateDelAggItemGroup(CreateOrUpdateDelAggItemGroupInput input);
        Task DeleteDelAggItemGroup(IdInput input);
        Task<ListResultOutput<DelAggItemGroupListDto>> GetNames();
    }
}