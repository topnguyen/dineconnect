﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master
{
    public interface IDelAggChargeAppService : IApplicationService
    {
        Task<PagedResultOutput<DelAggChargeListDto>> GetAll(GetDelAggChargeInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggChargeInput input);
        Task<GetDelAggChargeForEditOutput> GetDelAggChargeForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateDelAggCharge(CreateOrUpdateDelAggChargeInput input);
        Task DeleteDelAggCharge(IdInput input);
    }
}