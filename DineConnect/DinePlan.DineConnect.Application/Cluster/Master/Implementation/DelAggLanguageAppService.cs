﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Cluster.Dtos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;


namespace DinePlan.DineConnect.Cluster.Implementation
{
    public class DelAggLanguageAppService : DineConnectAppServiceBase, IDelAggLanguageAppService
    {
        private readonly IRepository<DelAggLanguage> _delAggLanguageRepo;
        private readonly IDelAggLanguageManager _delAggLanguageManager;

        public DelAggLanguageAppService(
            IRepository<DelAggLanguage> delAggLanguageRepo,
            IDelAggLanguageManager delAggLanguageManager)
        {
            _delAggLanguageRepo = delAggLanguageRepo;
            _delAggLanguageManager = delAggLanguageManager;
        }

        public async Task<PagedResultOutput<DelAggLanguageListDto>> GetAll(GetDelAggLanguageInput input)
        {
            var allItems = _delAggLanguageRepo.GetAll().Where(t => t.ReferenceId == input.Id && t.LanguageDescriptionType == input.LanguageDescriptionType).WhereIf(
                                !input.Filter.IsNullOrEmpty(),
                                p => p.Language.Contains(input.Filter));

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DelAggLanguageListDto>>();
            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<DelAggLanguageListDto>(
                allItemCount,
                allListDtos
                );
        }



        public async Task<GetDelAggLanguageForEditOutput> GetDelAggLanguageForEdit(NullableIdInput input)
        {
            DelAggLanguageEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _delAggLanguageRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DelAggLanguageEditDto>();
            }
            else
            {
                editDto = new DelAggLanguageEditDto();
            }

            return new GetDelAggLanguageForEditOutput
            {
                DelAggLanguage = editDto
            };
        }

        public async Task<IdInput> CreateOrUpdateDelAggLanguage(CreateOrUpdateDelAggLanguageInput input)
        {
            input.DelAggLanguage.Language = input.DelAggLanguage.Language.Trim().ToUpper();
            bool updateDesc = input.DelAggLanguage.Id.HasValue;

            var myDesc =
            await _delAggLanguageRepo.FirstOrDefaultAsync(t => t.Language.Equals(input.DelAggLanguage.Language) && t.ReferenceId == input.DelAggLanguage.ReferenceId && t.LanguageDescriptionType == input.DelAggLanguage.LanguageDescriptionType);
            if (myDesc != null)
            {
                input.DelAggLanguage.Id = myDesc.Id;
                updateDesc = true;
            }

            if (updateDesc)
                return await UpdateDelAggLanguage(input);
            return await CreateDelAggLanguage(input);

        }

        public async Task DeleteDelAggLanguage(IdInput input)
        {
            await _delAggLanguageRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateDelAggLanguage(CreateOrUpdateDelAggLanguageInput input)
        {
            var item = await _delAggLanguageRepo.GetAsync(input.DelAggLanguage.Id.Value);
            var dto = input.DelAggLanguage;
            item.DelAggTypeRefId = dto.DelAggTypeRefId;
            item.Language = dto.Language;
            item.LanguageValue = dto.LanguageValue;
            item.LanguageDescriptionType = dto.LanguageDescriptionType;
            item.ReferenceId = dto.ReferenceId;
            return new IdInput { Id = item.Id };
        }

        protected virtual async Task<IdInput> CreateDelAggLanguage(CreateOrUpdateDelAggLanguageInput input)
        {
            var dto = input.DelAggLanguage.MapTo<DelAggLanguage>();

            CheckErrors(await _delAggLanguageManager.CreateOrUpdateSync(dto));
            return new IdInput { Id = dto.Id };
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetLanguageDescriptionTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 1;
            foreach (var name in Enum.GetNames(typeof(DelLanguageDescriptionType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);

        }
    }
}