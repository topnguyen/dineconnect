﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Logging;
using Castle.Core.Logging;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Dto;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Cluster.Master.Implementation
{
    public class DelAggApiAppService : DineConnectAppServiceBase, IDelAggApiAppService
    {
        private readonly IRepository<DelAggCategory> _delaggcategoryRepo;
        private readonly IRepository<DelAggImage> _delaggimageRepo;
        private readonly IRepository<DelAggItemGroup> _delaggItemGroupRepo;
        private readonly IRepository<DelAggItem> _delaggitemRepo;
        private readonly IRepository<DelAggLocation> _delAggLocation;
        private readonly IRepository<DelAggLocationGroup> _delagglocationGroupRepo;
        private readonly IRepository<DelAggLocationItem> _delAggLocationItemRepo;
        private readonly IRepository<DelAggLocMapping> _delAggLocMappingRepo;
        private readonly IRepository<DelAggModifierGroupItem> _delAggModiferGroupItemRepo;
        private readonly IRepository<DelAggModifierGroup> _delAggModifierGroupRepo;
        private readonly IRepository<DelAggModifier> _delAggModifierRepo;
        private readonly IRepository<DelAggVariantGroup> _delAggVariantGroupRepo;
        private readonly IRepository<DelAggVariant> _delAggVariantItemRepo;
        private readonly IRepository<DelTimingDetail> _delTimingDetailRepo;
        private readonly IRepository<DelTimingGroup> _delTimingGroupRepo;
        

        private readonly IRepository<MenuItem> _menuItemRepo;
        private readonly IRepository<ProductComboGroup> _productComboGroupRepo;
        private readonly IRepository<ProductCombo> _productComboRepo;

        private readonly ITenantSettingsAppService _tenantSettingsService;
        private readonly ILocationAppService _locationAppService;

        private readonly ILogger _logger;


        public DelAggApiAppService(
            IRepository<DelAggCategory> delaggcategoryRepo,
            IRepository<DelAggImage> delaggimageRepo,
            IRepository<DelAggItemGroup> delaggItemGroupRepo,
            IRepository<DelAggItem> delaggitemRepo,
            IRepository<DelAggLocationGroup> delagglocationGroupRepo,
            IRepository<DelAggLocationItem> delAggLocationItemRepo,
            IRepository<MenuItem> menuItemRepo,
            IRepository<DelAggModifier> delAggModifierRepo,
            IRepository<DelAggModifierGroup> delAggModifierGroupRepo,
            IRepository<DelAggModifierGroupItem> delAggModiferGroupItemRepo,
            IRepository<DelAggLocation> delAggLocation,
            ITenantSettingsAppService tenantSettingsService,
            IRepository<DelAggLocMapping> delAggLocMappingRepo,
            IRepository<DelTimingGroup> delTimingGroupRepo,
            IRepository<DelTimingDetail> delTimingDetailRepo,
            IRepository<DelAggVariantGroup> delAggVariantGroupRepo,
            IRepository<DelAggVariant> delAggVariantItemRepo,
            IRepository<ProductCombo> productComboRepo,
            IRepository<ProductComboGroup> productComboGroupRepo, ILocationAppService locationAppService,
            ILogger logger)
        {
            _menuItemRepo = menuItemRepo;
            _delAggModifierRepo = delAggModifierRepo;
            _delAggModifierGroupRepo = delAggModifierGroupRepo;
            _delAggModiferGroupItemRepo = delAggModiferGroupItemRepo;
            _delAggLocation = delAggLocation;
            _delAggLocationItemRepo = delAggLocationItemRepo;
            _delagglocationGroupRepo = delagglocationGroupRepo;
            _delaggitemRepo = delaggitemRepo;
            _delaggItemGroupRepo = delaggItemGroupRepo;
            _delaggimageRepo = delaggimageRepo;
            _delaggcategoryRepo = delaggcategoryRepo;
            _tenantSettingsService = tenantSettingsService;
            _delAggLocMappingRepo = delAggLocMappingRepo;
            _delTimingGroupRepo = delTimingGroupRepo;
            _delTimingDetailRepo = delTimingDetailRepo;
            _delAggVariantGroupRepo = delAggVariantGroupRepo;
            _delAggVariantItemRepo = delAggVariantItemRepo;
            _productComboRepo = productComboRepo;
            _productComboGroupRepo = productComboGroupRepo;
            _logger = logger;
            _locationAppService = locationAppService;
        }

        public async Task<GrabOutputApi> ApiForGrab(ApiDelAggRequest delAggRequest)
        {
            var orgId = await _locationAppService.GetOrgnizationIdForLocationId(delAggRequest.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);

            CurrentUnitOfWork.EnableFilter(AbpDataFilters.MayHaveTenant);
            CurrentUnitOfWork.SetFilterParameter(AbpDataFilters.MayHaveTenant, AbpDataFilters.Parameters.TenantId, delAggRequest.TenantId);
           
            var mylocationMappings = _delAggLocMappingRepo.GetAll().Where(a =>
                 a.DelAggLocation.LocationId == delAggRequest.LocationId && a.DelAggTypeRefId == DelAggType.Grab).ToList();

            if (mylocationMappings.Any())
            {
                var myFirst = mylocationMappings.FirstOrDefault();
                if (myFirst != null && !string.IsNullOrEmpty(myFirst.MenuContents))
                {
                    var outputResult = JsonConvert.DeserializeObject<GrabOutputApi>(myFirst.MenuContents);
                    return outputResult;
                }
            }


            var aggLocation = await GetAggLocationItemsDto(new DelAggInput
            { LocationId = delAggRequest.LocationId, Type = DelAggType.Grab });

            if (aggLocation == null || !aggLocation.Any())
            {
                _logger.Log(LogSeverity.Fatal, "Mapping for AggLocation is not Available " + delAggRequest.LocationId);
                return null;
            }

            var allDelAggModifierGroup = _delAggModifierGroupRepo.GetAll();
            var listSectionsApi = new List<GrabSection>();
            var allItemGroupByLoc = from d in aggLocation
                                    group d by new { d.DelTimingGroupId, d.DelTimingGroupName }
                into g
                                    select new { g.Key.DelTimingGroupId, g.Key.DelTimingGroupName, Items = g };

            var sectionSort = 1;

            foreach (var itemLoc in allItemGroupByLoc)
            {
                var groupItemByCategory = from d in itemLoc.Items
                                      group d by new { d.CategoryId, d.CategoryName }
                    into g
                                      select new { g.Key.CategoryId, g.Key.CategoryName, Items = g };

                var listCategory = new List<GrabCategory>();

                foreach (var itemCategory in groupItemByCategory)
                {
                    var listItem = new List<GrabItem>();
                    var itemGroups = itemCategory.Items.GroupBy(x => x.DelAggItem);
                    var itemSort = 1;
                    foreach (var item in itemGroups.OrderBy(a=>a.Key.SortOrder))
                    {

                        var myId = item.Key.MenuItem.AliasCode ?? item.Key.Name.Replace(" ", "-");
                        var listModifierGroup = new List<GrabModifierGroup>();

                        if (item.Any() && item.Count() > 1)
                        {
                            List<GrabModifier> mySizeModifiers = new List<GrabModifier>();

                            var myModGroup = new GrabModifierGroup
                            {
                                Id = "Size-" + myId,
                                Name = "Size-" + myId,
                                SelectionRangeMax = 1,
                                SelectionRangeMin = 0,
                                Sequence = 1,
                                Modifiers = mySizeModifiers
                            };

                            bool firstItem = true;
                            var price = 0M;
                            foreach (var myItem in item.OrderBy(a=>a.Price))
                            {
                                if (firstItem)
                                {
                                    firstItem = false;
                                    price = myItem.Price;
                                    continue;
                                }

                                var differPrice = myItem.Price - price;

                                mySizeModifiers.Add(new GrabModifier
                                {
                                    Id = "Size-" + myItem.DelAggVariant.Name,
                                    Name = myItem.DelAggVariant.Name,
                                    Price = Convert.ToInt32(differPrice),
                                });
                            }
                            listModifierGroup.Add(myModGroup);
                        }

                        var findAllModifierGroupItem = item.Select(s => s.DelAggModifier);
                        var listModifier = new List<GrabModifier>();
                        foreach (var modifierItem in findAllModifierGroupItem)
                        {
                            if (modifierItem == null)
                                continue;
                            listModifier.Add(new GrabModifier
                            {
                                Id = modifierItem.Name.ToString(),
                                Name = modifierItem.Name,
                                Price = Convert.ToInt32(modifierItem.Price)*100,
                                GroupId = modifierItem.DelAggModifierGroupId,
                            });
                        }

                        var modifierGroupIds = item.Where(a => a.DelAggModifier != null && a.DelAggModifier.DelAggModifierGroupId != null)
                            .Select(s => s.DelAggModifier.DelAggModifierGroupId).Distinct();
                        foreach (var modifierGroupId in modifierGroupIds)
                        {
                            var findModifierGroup = allDelAggModifierGroup.FirstOrDefault(s => s.Id == modifierGroupId);
                            if (!listModifierGroup.Any(x => findModifierGroup != null && x.Id == findModifierGroup.Id.ToString()))
                                if (findModifierGroup != null)
                                    listModifierGroup.Add(new GrabModifierGroup
                                    {
                                        Id = findModifierGroup.Id.ToString(),
                                        Name = findModifierGroup.Name,
                                        SelectionRangeMax = findModifierGroup.Max,
                                        SelectionRangeMin = findModifierGroup.Min,
                                        Modifiers = listModifier.Where(x => x.GroupId == findModifierGroup.Id).ToList(),
                                        Sequence = 1
                                    });
                        }

                        int delType = (int)DelAggType.Grab;

                        var allImages = await _delaggimageRepo.GetAll().Where(a =>
                            a.ReferenceId == item.Key.Id && a.DelAggImageTypeRefId == 1 && a.TenantId == delAggRequest.TenantId &&
                            (a.DelAggTypeRefId == null || a.DelAggTypeRefId == delType)).ToListAsync();
                        
                        var images = allImages
                            .Select(x =>
                                    {
                                        var fileInfo = JsonConvert.DeserializeObject<FileDto>(x.ImagePath);
                                        return fileInfo.FileSystemName;
                                    }).Distinct();

                        var allItemForPrice = item.OrderBy(a => a.Price);
                        listItem.Add(new GrabItem
                        {
                            Id = myId,
                            Description = item.Key.Description,
                            ModifierGroups = listModifierGroup,
                            Name = item.Key.Name,
                            Price = Convert.ToInt32(allItemForPrice.First().Price),
                            SpecialType = item.Key.FoodType,
                            Photos = images.ToList(),
                            Sequence = itemSort++
                        });
                    }

                    var myNewCategory = new GrabCategory
                    {
                        Id = itemCategory.CategoryId.ToString(),
                        Name = itemCategory.CategoryName,
                        Items = listItem
                    };

                    if (itemCategory.Items.Any())
                    {
                        myNewCategory.Sequence = itemCategory.Items.First().CategorySortOrder;
                    }
                    listCategory.Add(myNewCategory );
                }

                listSectionsApi.Add(new GrabSection
                {
                    Id = itemLoc.DelTimingGroupId.ToString(),
                    Name = itemLoc.DelTimingGroupName,
                    ServiceHours = new GrabServiceHours
                    {
                        Monday = new GrabDayFormat
                        {
                            OpenPeriodType = "OpenPeriod",
                            Periods = GetPeriods(DayOfWeek.Monday, itemLoc.DelTimingGroupId, delAggRequest.TenantId)
                        },
                        Tuesday = new GrabDayFormat
                        {
                            OpenPeriodType = "OpenPeriod",
                            Periods = GetPeriods(DayOfWeek.Tuesday, itemLoc.DelTimingGroupId,delAggRequest.TenantId)
                        },
                        Wednesday = new GrabDayFormat
                        {
                            OpenPeriodType = "OpenPeriod",
                            Periods = GetPeriods(DayOfWeek.Wednesday, itemLoc.DelTimingGroupId,delAggRequest.TenantId)
                        },
                        Thursday = new GrabDayFormat
                        {
                            OpenPeriodType = "OpenPeriod",
                            Periods = GetPeriods(DayOfWeek.Thursday, itemLoc.DelTimingGroupId,delAggRequest.TenantId)
                        },
                        Friday = new GrabDayFormat
                        {
                            OpenPeriodType = "OpenPeriod",
                            Periods = GetPeriods(DayOfWeek.Friday, itemLoc.DelTimingGroupId,delAggRequest.TenantId)
                        },
                        Saturday = new GrabDayFormat
                        {
                            OpenPeriodType = "OpenPeriod",
                            Periods = GetPeriods(DayOfWeek.Saturday, itemLoc.DelTimingGroupId,delAggRequest.TenantId)
                        },
                        Sunday = new GrabDayFormat
                        {
                            OpenPeriodType = "OpenPeriod",
                            Periods = GetPeriods(DayOfWeek.Sunday, itemLoc.DelTimingGroupId,delAggRequest.TenantId)
                        }
                    },
                    Categories = listCategory,
                    Sequence = sectionSort++
                });
            }

            var result = new GrabOutputApi
            {
                Currency = new GrabCurrency
                {
                    Code = string.IsNullOrEmpty(delAggRequest.CurrencyCode) ? "SGD" : delAggRequest.CurrencyCode,
                    Exponent = 2,
                    Symbol = string.IsNullOrEmpty(delAggRequest.CurrencySymbol) ? "S$"
                    : delAggRequest.CurrencySymbol
                },
                MerchantID = delAggRequest.PartnerMerchantCode,
                Sections = listSectionsApi,
                PartnerMerchantID = delAggRequest.MerchantCode
            };
            return result;
        }
        public async Task<DeliveroApiOutput> ApiForDeliveroo(ApiDelAggRequest delAggRequest)
        {
            var allItemsDto = await GetAggLocationItemsDto(new DelAggInput
            { LocationId = delAggRequest.LocationId, Type = DelAggType.Deliveroo });

            var setting = await _tenantSettingsService.GetAllSettings();
            var allDelAggModifierGroupItem = _delAggModiferGroupItemRepo.GetAll();
            var allDelAggModifierGroup = _delAggModifierGroupRepo.GetAll();
            var allDellAggModifier = _delAggModifierRepo.GetAll();
            var allDelTimingDetail = _delTimingDetailRepo.GetAll();
            var allCategories = _delaggcategoryRepo.GetAll();
            var allDelAggItems = _delaggitemRepo.GetAll();
            var allDelAggLoctionItem = _delAggLocationItemRepo.GetAll();

            var groupByTime = from d in allItemsDto
                              group d by new { d.DelTimingGroupId, d.DelTimingGroupName }
                into g
                              select new { g.Key.DelTimingGroupId, g.Key.DelTimingGroupName, Items = g };
            var listMealTime = new List<Mealtime>();
            var listCategory = new List<CategoryDeliveroo>();
            var listItem = new List<ItemDeliveroo>();
            var listModifier = new List<ModifierDeliveroo>();
            foreach (var time in groupByTime)
            {
                var findAllCate = time.Items.GroupBy(s => s.CategoryId).Select(t => new
                {
                    Id = t.Key,
                    Description = t.First().CategoryDescription,
                    Name = t.First().CategoryName,
                    Items = t.ToList()
                });
                foreach (var cate in findAllCate)
                {
                    var findAllItem = cate.Items.GroupBy(x => x.DelAggItem);
                    listCategory.Add(new CategoryDeliveroo
                    {
                        Description = new Description { En = cate.Description },
                        Id = cate.Id.ToString(),
                        ItemIds = findAllItem.Select(s => s.Key.Id.ToString()).ToList(),
                        Name = new Name { En = cate.Name }
                    });
                    foreach (var item in findAllItem)
                    {
                        var findAllModifierItems = item.Select(s => s.DelAggModifier);
                        var listModifierIdReferItem = new List<string>();
                        foreach (var modifierItem in findAllModifierItems)
                        {
                            var listItemIdModifierReferAggItem = new List<string>();
                            var findModifierGroup =
                                allDelAggModifierGroup.FirstOrDefault(s => s.Id == modifierItem.DelAggModifierGroupId);
                            listModifierIdReferItem.Add(modifierItem.Id.ToString());
                            listModifier.Add(new ModifierDeliveroo
                            {
                                Description = new Description { En = "" },
                                Id = modifierItem.Id.ToString(),
                                MaxSelection = findModifierGroup.Max,
                                MinSelection = findModifierGroup.Min,
                                Name = new Name { En = modifierItem.Name },
                                Repeatable = false,
                                ItemIds = listItemIdModifierReferAggItem
                            });
                        }

                        listItem.Add(new ItemDeliveroo
                        {
                            Id = item.Key.Id.ToString(),
                            //ContainsAlcohol = true,
                            Allergies = new List<string>(),
                            Description = new Description { En = item.First().DelAggItemDescription },
                            //Ian = "Ian",
                            Image = new Image { Url = "" },
                            ModifierIds = listModifierIdReferItem,
                            Name = new Name { En = item.First().DelAggItemName },
                            //NutritionalInfo = new NutritionalInfo { EnergyKcal = new EnergyKcal { High = 100, Low = 50 } },
                            //OperationalName = "aaaa",
                            Plu = $"{item.First().DelAggItemName}-{item.First().DelAggItemId}",
                            PriceInfo = new PriceInfo { Price = item.First().Price, Overrides = new List<Override>() },
                            TaxRate = item.Key.DelAggTax?.TaxPercentage ?? 0
                        });
                    }
                }

                listMealTime.Add(new Mealtime
                {
                    CategoryIds = findAllCate.Select(t => t.Id.ToString()).ToList(),
                    Description = new Description { En = time.Items.FirstOrDefault()?.DelTimingGroup.Description },
                    Id = time.DelTimingGroupId.ToString(),
                    Name = new Name { En = time.DelTimingGroupName },
                    Image = new Image { Url = "" },
                    SeoDescription = new SeoDescription { En = "" }
                });
            }

            var location = _delAggLocation.GetAll().FirstOrDefault(x => x.LocationId == delAggRequest.LocationId);
            var result = new DeliveroApiOutput
            {
                Menu = new Menu
                { Categories = listCategory, Items = listItem, Mealtimes = listMealTime, Modifiers = listModifier },
                Name = location?.Name,
                SiteIds = new List<string> { location?.Id.ToString() }
            };
            return result;
        }
        public async Task<DeliveryHeroApiOutput> ApiForDeliveryHero(ApiDelAggRequest delAggRequest)
        {
            var allItemsDto = await GetAggLocationItemsDto(new DelAggInput
            { LocationId = delAggRequest.LocationId, Type = DelAggType.DeliveryHero });

            var setting = await _tenantSettingsService.GetAllSettings();
            var allDelAggModifierGroupItem = _delAggModiferGroupItemRepo.GetAll().Include(x => x.DelAggModifierGroup);
            var allDelAggVariants = _delAggVariantItemRepo.GetAll();
            var allDelAggModifierGroup = _delAggModifierGroupRepo.GetAll();
            var allDellAggModifier = _delAggModifierRepo.GetAll();
            var allDelTimingDetail = _delTimingDetailRepo.GetAll();
            var allDelAggLoctionItem = _delAggLocationItemRepo.GetAll();

            var groupByTime = from d in allItemsDto
                              group d by new { d.DelTimingGroupId, d.DelTimingGroupName, d.DelTimingGroup.Description }
                into g
                              select new { g.Key.DelTimingGroupId, g.Key.DelTimingGroupName, g.Key.Description, Items = g };
            var listMenuTime = new List<MenuDelivery>();
            var listMenuProduct = new List<MenuProduct>();
            var listMenuCategory = new List<MenuCategory>();
            var listToppingTemplate = new List<ToppingTemplate>();
            var listToppingTemplateProduct = new List<ToppingTemplateProduct>();
            var listProductVariationToppingTemplate = new List<ProductVariationToppingTemplate>();
            foreach (var timeGroup in groupByTime)
            {
                var findAllTimeDetail = allDelTimingDetail.Where(s => s.DelTimingGroupId == timeGroup.DelTimingGroupId)
                    .Select(s => new MenuDelivery
                    {
                        Description = timeGroup.Description,
                        EndHour = s.EndHour.ToString(),
                        StartHour = s.StartHour.ToString(),
                        Id = s.Id.ToString(),
                        MenuType = timeGroup.DelTimingGroupName,
                        Title = timeGroup.DelTimingGroupName
                    });
                if (!findAllTimeDetail.Any()) continue;

                listMenuTime.AddRange(findAllTimeDetail);

                var groupbycate = from d in timeGroup.Items
                                  group d by new { d.CategoryId, d.CategoryName }
                    into g
                                  select new { g.Key.CategoryId, g.Key.CategoryName, Items = g };
                foreach (var cate in groupbycate)
                {
                    var itemGroups = cate.Items.GroupBy(x => x.DelAggItem);
                    var listProduct = new List<Product>();
                    foreach (var item in itemGroups)
                    {
                        var variantItems = item.Select(s => s.DelAggVariant);
                        var listVariant = new List<ProductVariationsItem>();
                        foreach (var variant in variantItems)
                            listVariant.Add(new ProductVariationsItem
                            {
                                Id = variant.Id.ToString(),
                                SalesPrice = variant.SalesPrice.ToString(),
                                MarkupPrice = variant.MarkupPrice.ToString(),
                                Title = variant.Name,
                                ContainerPrice = "0"
                            });

                        listProduct.Add(new Product
                        {
                            Description = item.First().DelAggItem.Description,
                            Id = item.First().DelAggItem.Id.ToString(),
                            Image = "",
                            Title = item.Key.Name,
                            ProductVariations = new ProductVariations { ProductVariation = listVariant }
                        });

                        var findModifierItems = item.Select(s => s.DelAggModifier);
                        foreach (var findModifierItem in findModifierItems)
                        {
                            listToppingTemplateProduct.Add(new ToppingTemplateProduct
                            {
                                Price = findModifierItem.Price.ToString(),
                                ProductId = item.Key.Id.ToString(),
                                ToppingTemplateId = findModifierItem.Id.ToString()
                            });
                            if (!listToppingTemplate.Any(x => x.Id == findModifierItem.Id.ToString()))
                            {
                                var findModifierGroup =
                                    allDelAggModifierGroup.FirstOrDefault(x =>
                                        x.Id == findModifierItem.DelAggModifierGroupId);
                                listToppingTemplate.Add(new ToppingTemplate
                                {
                                    Id = findModifierItem.Id.ToString(),
                                    IsHalfHalf = "0",
                                    QuantityMax = findModifierGroup?.Max.ToString(),
                                    QuantityMin = findModifierGroup?.Min.ToString(),
                                    Title = findModifierItem.Name
                                });
                            }
                        }

                        item.ToList().ForEach(i => listProductVariationToppingTemplate.Add(
                            new ProductVariationToppingTemplate
                            {
                                ProductVariationId = i.DelAggVariant.Id.ToString(),
                                ToppingTemplateId = i.DelAggModifier.Id.ToString()
                            }));
                    }

                    if (cate != null)
                        listMenuCategory.Add(new MenuCategory
                        {
                            Description = cate.Items.FirstOrDefault()?.DelAggCategory.Description,
                            Id = cate.Items.FirstOrDefault()?.DelAggCategory.Id.ToString(),
                            Title = cate.Items.FirstOrDefault()?.DelAggCategory.Name,
                            Products = new Products { Product = listProduct }
                        });
                }
            }

            var result = new DeliveryHeroApiOutputVendor
            {
                Id = "1",
                Menus = new Menus { Menu = listMenuTime },
                MenuCategories = new MenuCategories { MenuCategory = listMenuCategory },
                MenuProducts = new MenuProducts { MenuProduct = listMenuProduct },
                ToppingTemplates = new ToppingTemplates { ToppingTemplate = listToppingTemplate },
                ToppingTemplateProducts = new ToppingTemplateProducts
                { ToppingTemplateProduct = listToppingTemplateProduct },
                ProductVariationToppingTemplates = new ProductVariationToppingTemplates
                { ProductVariationToppingTemplate = listProductVariationToppingTemplate },
                Translations = new Translations { Language = new List<Language>() }
            };
            return new DeliveryHeroApiOutput { Vendor = result };
        }
        public async Task<ZomatoAggApiOutput> ApiForZomato(ApiDelAggRequest delAggRequest)
        {
            var allItemsDto = await GetAggLocationItemsDto(new DelAggInput
            { LocationId = delAggRequest.LocationId, Type = DelAggType.Zomato });

            var setting = await _tenantSettingsService.GetAllSettings();
            var allDelAggModifierGroupItem = _delAggModiferGroupItemRepo.GetAll().Include(x => x.DelAggModifierGroup);
            var allDelAggModifierGroup = _delAggModifierGroupRepo.GetAll();
            var allDellAggModifier = _delAggModifierRepo.GetAll();
            var allDelTimingDetail = _delTimingDetailRepo.GetAll();
            var allDelAggLoctionItem = _delAggLocationItemRepo.GetAll();
            var alldelAggVariantGroup = _delAggVariantGroupRepo.GetAll();
            var alldelAggVariantItem = _delAggVariantItemRepo.GetAll().Include(x => x.MenuItemPortion);
            var menuZomato = new MenuZomato();
            var listCategoriZo = new List<CategoriesZomato>();
            var listCatalogues = new List<CataloguesZomato>();
            var listModifierGroup = new List<ModifierGroupsZomato>();
            var groundwater = from d in allItemsDto
                              where !d.IsCombo
                              group d by new { d.CategoryId, d.CategoryName }
                into g
                              select new { g.Key.CategoryId, g.Key.CategoryName, Items = g };

            foreach (var cate in groundwater)
            {
                var findTimingGroup = cate.Items.FirstOrDefault()?.DelTimingGroup;

                // Group by item
                var itemGroups = cate.Items.GroupBy(x => x.DelAggItem);

                foreach (var item in itemGroups)
                {
                    var listVariant = new List<VariantsCatalogues>();
                    var findVariantGroupItem = item.GroupBy(s => s.DelAggVariant);
                    var properties = new List<PropertiesCatalogues>
                    {
                        new PropertiesCatalogues()
                        {
                            Name = item.First().DelAggItemName
                        }
                    };

                    foreach (var variantGroupItem in findVariantGroupItem)
                    {
                        var variant = variantGroupItem.Key;

                        properties[0].PropertyValues.Add(new PropertyValuesZomato
                        {
                            VendorEntityId = variant.MenuItemPortion.Id.ToString(),
                            Value = variant.MenuItemPortion.Name
                        });
                        var priceVariants = new List<PriceVariants>
                        {
                            new PriceVariants()
                            {
                                Service = variant.Name,
                                Price = variant.SalesPrice
                            }
                        };

                        listVariant.Add(new VariantsCatalogues
                        {
                            Prices = priceVariants,
                            ModifierGroups = variantGroupItem.Select(x => new ModifierGroupsVariant
                            { VendorEntityId = x.DelAggModifier.Id.ToString() }).ToList(),
                            PropertyValues = new List<PropertyVariants>
                            {
                                new PropertyVariants()
                                {
                                    VendorEntityId = variant.MenuItemPortionId.ToString()
                                }
                            },
                            VendorEntityId = variant.Id.ToString()
                        });
                    }

                    var findItem = item.Key;
                    listCatalogues.Add(new CataloguesZomato
                    {
                        VendorEntityId = findItem.Id.ToString(),
                        Charges = new List<ChargeCatalogues>(),
                        Description = findItem.Description,
                        ImageUrl = "",
                        InStock = true,
                        Kind = "",
                        MeatTypes = new List<string> { findItem.FoodType },
                        Name = findItem.Name,
                        NutritionInfo = new List<NutritionInfoCatalogues>(),
                        PortionSize = new List<PortionSizeCatalogues>(),
                        PreparationTime = "",
                        Properties = properties,
                        Tags = new List<string>(),
                        TaxGroups = new List<TaxGroupsCatalogues>(),
                        Variants = listVariant
                    });
                }

                var findAllTimingDetail = allDelTimingDetail.Where(s => s.DelTimingGroupId == findTimingGroup.Id);
                var groupTimeDetaiByDay = from d in findAllTimingDetail
                                          group d by new { d.Days }
                    into g
                                          select new { g.Key.Days, Items = g.ToList() };
                var listTiming = new List<TimingZomato>();
                foreach (var time in groupTimeDetaiByDay)
                {
                    listTiming.Add(new TimingZomato
                    {
                        EndDate = "",
                        StartDate = "",
                        Time1From = GetTime(time.Items, 0),
                        Time2From = GetTime(time.Items, 1),
                        Time3From = GetTime(time.Items, 2),
                        Time1To = GetTime(time.Items, 0, false),
                        Time2To = GetTime(time.Items, 1, false),
                        Time3To = GetTime(time.Items, 2, false),
                        Monday = time.Days == ((int)DayOfWeek.Monday).ToString(),
                        Tuesday = time.Days == ((int)DayOfWeek.Tuesday).ToString(),
                        Wednesday = time.Days == ((int)DayOfWeek.Wednesday).ToString(),
                        Thursday = time.Days == ((int)DayOfWeek.Thursday).ToString(),
                        Friday = time.Days == ((int)DayOfWeek.Friday).ToString(),
                        Saturday = time.Days == ((int)DayOfWeek.Saturday).ToString(),
                        Sunday = time.Days == ((int)DayOfWeek.Sunday).ToString()
                    });
                }

                listCategoriZo.Add(new CategoriesZomato
                {
                    Name = cate.CategoryName,
                    VendorEntityId = cate.CategoryId.ToString(),
                    SubCategories = new List<SubCategories>(),
                    Timings = listTiming
                });
            }

            menuZomato.Catalogues = listCatalogues;
            menuZomato.Categories = listCategoriZo;

            //Combo
            var listCombos = new List<CombosZomato>();
            menuZomato.Combos = listCombos;
            var comboItems = allItemsDto.Where(x => x.IsCombo).ToList();
            foreach (var comboItem in comboItems)
            {
                var productCombo = _productComboRepo.GetAll()
                    .FirstOrDefault(x => x.MenuItemId == comboItem.DelAggItem.MenuItemId);
                if (productCombo == null) continue;

                var productComboGroups = _productComboGroupRepo.GetAll().Include(x => x.ComboItems)
                    .Where(x => x.ProductComboId == productCombo.Id).ToList();

                listCombos.Add(new CombosZomato
                {
                    Name = productCombo?.Name,
                    VendorEntityId = productCombo?.Id.ToString(),
                    Selections = productComboGroups.Select(x => new SelectionCombo
                    {
                        VendorEntityId = x.Id.ToString(),
                        Title = x.Name,
                        MinSelection = x.Minimum,
                        MaxSelection = x.Maximum,
                        SelectionEntities = x.ComboItems
                            .Where(y => allItemsDto.Any(a =>
                                a.DelAggItem.MenuItemId == y.MenuItemId &&
                                a.DelAggVariant.MenuItemPortionId == y.MenuItemPortionId))
                            .Select(y => new SelectionEntity
                            {
                                VariantVendorEntityId = allItemsDto
                                    .First(a => a.DelAggVariant.MenuItemPortionId == y.MenuItemPortionId).DelAggVariant
                                    .Id.ToString(),
                                CatalogueVendorEntityId = allItemsDto
                                    .First(a => a.DelAggItem.MenuItemId == y.MenuItemId).DelAggItemId.ToString()
                            }).ToList()
                    }).ToList()
                });
            }

            // Modifier
            var listVariantModifierGroup = new List<VariantModifierGroup>();
            var findAllModiferGroup = allItemsDto.GroupBy(x => x.DelAggModifier);
            foreach (var modifier in findAllModiferGroup)
            {
                var listModifierGroupsVariant = modifier.Select(s => new ModifierGroupsVariant
                { VendorEntityId = s.DelAggVariant.Id.ToString() }).ToList();
                var modifierGr = allDelAggModifierGroup.FirstOrDefault(x => x.Id == modifier.Key.DelAggModifierGroupId);
                listModifierGroup.Add(new ModifierGroupsZomato
                {
                    DisplayName = modifier.Key.Name,
                    Kind = "",
                    Max = modifierGr?.Max,
                    Min = modifierGr?.Min,
                    Name = modifier.Key.Name,
                    VendorEntityId = modifier.Key.Id.ToString(),
                    Variants = modifier.Select(x => new VariantModifierGroup
                    { VendorEntityId = x.DelAggVariant.Id.ToString() }).ToList()
                });
            }

            menuZomato.ModifierGroups = listModifierGroup;
            var result = new ZomatoAggApiOutput
            {
                OutletId = "1",
                Menu = menuZomato,
                Charges = new List<ChargesZomato>()
            };
            return result;
        }


        #region PrivateMethods

        private List<GrabPeriod> GetPeriods(DayOfWeek dayOfWeek, int timingGroupId, int tenantId )
        {
            List<GrabPeriod> periods = new List<GrabPeriod>();
            bool add = true;
            var allDelTimingDetail = _delTimingDetailRepo.GetAll().Where(a=>a.TenantId == tenantId);

            if (allDelTimingDetail.Any())
            {
                add = false;
                var findTimingDetail = allDelTimingDetail.Where(s => s.DelTimingGroupId == timingGroupId);
                if (findTimingDetail.Any())
                {
                    periods = findTimingDetail.Where(s => string.IsNullOrEmpty(s.Days) || s.Days == ((int)dayOfWeek).ToString())
                        .Select(s => new GrabPeriod
                        {
                            StartTime = (s.StartHour == 0 ?"00":s.StartHour.ToString()) + ":" + (s.StartMinute == 0 ?"00":s.StartMinute.ToString()),
                            EndTime = (s.EndHour == 0 ?"00":s.EndHour.ToString()) + ":" + (s.EndHour == 0 ?"00":s.EndHour.ToString()),
                        }).ToList();
                }
            }
            if (add)
            {
                periods.Add(new GrabPeriod());
            }
            return periods;

        }

        private string GetTime(List<DelTimingDetail> timings, int index, bool isStart = true)
        {
            if (timings.Count < index + 1) return null;
            if (isStart)
                return timings[0]?.StartHour.ToString("D2") + ":" + timings[0]?.StartMinute.ToString("D2");
            return timings[0]?.EndHour.ToString("D2") + ":" + timings[0]?.EndMinute.ToString("D2");
        }

        private async Task<List<AggLocationItemsDto>> GetAggLocationItemsDto(DelAggInput input)
        {
            var orgId = await _locationAppService.GetOrgnizationIdForLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);

            var locMapLit = _delAggLocMappingRepo.GetAll().Where(s => s.DelAggTypeRefId == input.Type
                                                                      && s.DelAggLocationId == input.LocationId).ToList();

            if (!locMapLit.Any())
                return null;

            var locMap = locMapLit.LastOrDefault();

            var locationItem = _delAggLocationItemRepo.GetAll().Where(s => s.DelAggLocMappingId == locMap.Id);

            var allDelAggItems = _delaggitemRepo.GetAll().Include(x => x.MenuItem);
            var locItems = from i in allDelAggItems
                           join l in locationItem on i.Id equals l.DelAggItemId
                           into locationItems
                           from loc in locationItems.DefaultIfEmpty()
                           join cat in _delaggcategoryRepo.GetAll() on i.DelAggCatId equals cat.Id
                           join tim in _delTimingGroupRepo.GetAll() on cat.DelTimingGroupId equals tim.Id
                           join var in _delAggVariantItemRepo.GetAll() on i.DelAggVariantGroupId equals var.DelAggVariantGroupId
                           join modGrpItem in _delAggModiferGroupItemRepo.GetAll() on i.Id equals modGrpItem.DelAggItemId into modGrpItems
                           from modGrpItemDefault in modGrpItems.DefaultIfEmpty()
                           join modItem in _delAggModifierRepo.GetAll() on modGrpItemDefault.DelAggModifierGroupId equals modItem.DelAggModifierGroupId into modItems
                           from modItemDefault in modItems.DefaultIfEmpty()
                           join delAggImage in _delaggimageRepo.GetAll().Where(x => x.DelAggTypeRefId == (int)input.Type) on i.Id equals delAggImage.ReferenceId into dimgs
                           from delImages in dimgs.DefaultIfEmpty()
                           where loc == null || !loc.InActive
                           select new { item = i, category = cat, timing = tim, variant = var, modifier = modItemDefault, image = delImages };
            var allItemsDto = new List<AggLocationItemsDto>();
            foreach (var locItem in locItems)
            {
                var menuItem = locItem.item.MenuItem;

                if(menuItem.ProductType == 2)
                    continue;

                if (locMap != null)
                {
                    allItemsDto.Add(new AggLocationItemsDto
                    {
                        DelAggItemId = locItem.item.Id,
                        DelAggItemName = locItem.item.Name,
                        DelAggItemDescription = locItem.item.Description,
                        CategoryId = locItem.category.Id,
                        LocationId = locMap.DelAggLocation.LocationId,
                        LocationName = locMap.DelAggLocation.Name,
                        Price = input.Type == DelAggType.Grab
                            ? locItem.variant.SalesPrice * 100
                            : locItem.variant.SalesPrice,
                        CategoryName = locItem.category.Name,
                        CategoryDescription = locItem.category.Description,
                        DelTimingGroup = locItem.timing,
                        DelTimingGroupId = locItem.timing.Id,
                        DelTimingGroupName = locItem.timing.Name,
                        DelAggItem = locItem.item,
                        DelAggVariant = locItem.variant,
                        DelAggCategory = locItem.category,
                        DelAggModifier = locItem.modifier,
                        DelAggImage = locItem.image,
                        IsCombo = menuItem?.ProductType == 2,
                        CategorySortOrder = locItem.category.SortOrder
                    });
                }
            }

            return allItemsDto;
        }

        #endregion
    }
}