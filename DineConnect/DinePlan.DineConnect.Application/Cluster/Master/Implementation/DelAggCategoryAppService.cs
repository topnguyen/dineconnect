﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Impl;
using System.Linq;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using System;
using DinePlan.DineConnect.Cluster.Master;

namespace DinePlan.DineConnect.Cluster.Implementation
{
    public class DelAggCategoryAppService : DineConnectAppServiceBase, IDelAggCategoryAppService
    {
        private readonly IDelAggCategoryListExcelExporter _delaggcategoryExporter;
        private readonly IRepository<DelAggCategory> _delaggcategoryRepo;
        private readonly IRepository<DelAggItem> _delaggitemRepo;
        private readonly IDelAggCategoryManager _delaggcategoryManager;
        private readonly IRepository<DelTimingGroup> _deltiminggroupRepo;

        public DelAggCategoryAppService(IRepository<DelAggCategory> delaggcategoryRepo,
            IRepository<DelAggItem> delaggitemRepo,
            IDelAggCategoryManager delaggcategoryManager,
            IDelAggCategoryListExcelExporter delaggcategoryExporter, IRepository<DelTimingGroup> deltiminggroupRepo)
        {
            _delaggcategoryRepo = delaggcategoryRepo;
            _delaggitemRepo = delaggitemRepo;
            _delaggcategoryManager = delaggcategoryManager;
            _delaggcategoryExporter = delaggcategoryExporter;
            _deltiminggroupRepo = deltiminggroupRepo;
        }

        public async Task<PagedResultOutput<DelAggCategoryListDto>> GetAll(GetDelAggCategoryInput input)
        {
            var rscategory = _delaggcategoryRepo.GetAll();
            var rstiminggroup = _deltiminggroupRepo.GetAll();
            if (input.DelTimingGroupId.HasValue)
            {
                rscategory = rscategory.Where(t => t.DelTimingGroupId == input.DelTimingGroupId);
            }

            var allItems = (from category in rscategory
                            join tg in rstiminggroup
                            on category.DelTimingGroupId equals tg.Id
                            select new DelAggCategoryListDto()
                            {
                                Id = category.Id,
                                Name = category.Name,
                                Code = category.Code,
                                Description = category.Description,
                                DelTimingGroupId = category.DelTimingGroupId,
                                CreationTime = category.CreationTime,
                                SortOrder = category.SortOrder
                            }).WhereIf(
                             !input.Filter.IsNullOrEmpty(),
                             p => p.Name.Contains(input.Filter) || p.Code.Contains(input.Filter)
                         );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DelAggCategoryListDto>>();

            var allItemCount = await allItems.CountAsync();
            foreach (var lst in allListDtos)
            {
                if (lst.DelTimingGroupId != null)
                {
                    var lstDelAggGrp = rstiminggroup.FirstOrDefault(t => t.Id == lst.DelTimingGroupId);
                    if (lstDelAggGrp != null)
                        lst.DelTimingGroupName = lstDelAggGrp.Name;
                }
            }
            return new PagedResultOutput<DelAggCategoryListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetDelAggCategoryInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DelAggCategoryListDto>>();
            return _delaggcategoryExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDelAggCategoryForEditOutput> GetDelAggCategoryForEdit(NullableIdInput input)
        {
            DelAggCategoryEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _delaggcategoryRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DelAggCategoryEditDto>();
            }
            else
            {
                editDto = new DelAggCategoryEditDto();
            }

            return new GetDelAggCategoryForEditOutput
            {
                DelAggCategory = editDto
            };
        }

        public async Task CreateOrUpdateDelAggCategory(CreateOrUpdateDelAggCategoryInput input)
        {
            if (input.DelAggCategory.Id.HasValue)
            {
                await UpdateDelAggCategory(input);
            }
            else
            {
                await CreateDelAggCategory(input);
            }
        }

        public async Task DeleteDelAggCategory(IdInput input)
        {
            await _delaggcategoryRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateDelAggCategory(CreateOrUpdateDelAggCategoryInput input)
        {
            var item = await _delaggcategoryRepo.GetAsync(input.DelAggCategory.Id.Value);
            var dto = input.DelAggCategory;
            //TODO: SERVICE DelAggCategory Update Individually
            item.Name = dto.Name;
            item.Code = dto.Code;
            item.Description = dto.Description;
            item.SortOrder = dto.SortOrder;
            item.LocalRefCode = dto.LocalRefCode;
            item.DelAggCatParentId = dto.DelAggCatParentId;
            item.DelTimingGroupId = dto.DelTimingGroupId;
            CheckErrors(await _delaggcategoryManager.CreateSync(item));
        }

        protected virtual async Task CreateDelAggCategory(CreateOrUpdateDelAggCategoryInput input)
        {
            var dto = input.DelAggCategory.MapTo<DelAggCategory>();
            CheckErrors(await _delaggcategoryManager.CreateSync(dto));
        }
        public async Task<ListResultOutput<DelAggCategoryListDto>> GetNames()
        {
            var lstTax = await _delaggcategoryRepo.GetAll().ToListAsync();
            return new ListResultOutput<DelAggCategoryListDto>(lstTax.MapTo<List<DelAggCategoryListDto>>());
        }
        //private string getRandColor()
        //{
        //    Random rnd = new Random();
        //    string hexOutput = String.Format("{0:X}", rnd.Next(0, 0xFFFFFF));
        //    while (hexOutput.Length < 6)
        //        hexOutput = "0" + hexOutput;
        //    return "#" + hexOutput;
        //}
        public async Task<PagedResultOutput<DelAggCategoryListDto>> GetDelAggCategories(GetCategoryInput input)
        {
            string[] hexValue = { "#28B463", "#F63B13", "#FFC300", "#3498DB", "#D4AC0D", "#5D6D7E", "#8E44AD", "#DC7633", "#DC7633", "#EC7063" };
            var allItems = _delaggcategoryRepo
            .GetAll();

            if (!string.IsNullOrEmpty(input.Filter))
            {
                allItems = allItems.WhereIf(!string.IsNullOrEmpty(input.Filter), p => p.Name.Contains(input.Filter));
            }
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("All"))
            {
                allItems = allItems.OrderBy(input.Sorting);
            }
            else
            {
                allItems = allItems
                    .OrderBy(input.Sorting);

                if (input.MaxResultCount > 1)
                {
                    allItems = allItems
                    .PageBy(input);
                }
            }

            var cats = await allItems.ToListAsync();


            var allListDtos = cats.MapTo<List<DelAggCategoryListDto>>();
            for (int i = 0; i < allListDtos.Count; i++)// var category in allListDtos)
            {
                var j = i % hexValue.Length;
                allListDtos[i].MainButtonColor = hexValue[j];
            }
            var allItemCount = await allItems.CountAsync();
            return new PagedResultOutput<DelAggCategoryListDto>(
                allItemCount,
                allListDtos
                );
        }
        public async Task SaveSortDelAggCategories(int[] categories)
        {
            var i = 1;
            foreach (var category in categories)
            {
                var cate = await _delaggcategoryRepo.GetAsync(category);
                cate.SortOrder = i++;
                await _delaggcategoryRepo.UpdateAsync(cate);
            }
        }

        public async Task<GetItemByCategoryOuput> GetItemForVisualScreen(GetCategoryInput input)
        {
            string[] colorList = { "#ff7f50", "#87cefa", "#da70d6", "#32cd32", "#6495ed", "#ff69b4", "#ba55d3", "#cd5c5c", "#ffa500", "#40e0d0" };
            var allItems = await _delaggitemRepo.GetAll()
                                                .WhereIf(input.DelAggCategoryId > 0, x => x.DelAggCatId == input.DelAggCategoryId)
                                                .ToListAsync();
            var allListDtos = allItems.MapTo<List<DelAggItemListDto>>();
            for(int i = 0; i < allListDtos.Count; i++)
            {
                var j = i % colorList.Length;
                allListDtos[i].ButtonColor = colorList[j];
            }

            return new GetItemByCategoryOuput { DelAggItems = allListDtos };
        }
    }
}
