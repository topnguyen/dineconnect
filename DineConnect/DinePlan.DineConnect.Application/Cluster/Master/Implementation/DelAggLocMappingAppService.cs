﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Cluster.Master.Exporter;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master.Implementation
{
    public class DelAggLocMappingAppService : DineConnectAppServiceBase, IDelAggLocMappingAppService
    {
        private readonly IRepository<DelAggLocation> _delagglocationRepo;
        private readonly IDelAggLocMappingListExcelExporter _delagglocmappingExporter;
        private readonly IDelAggLocMappingManager _delAggLocMappingManager;
        private readonly IRepository<DelAggLocMapping> _delagglocmappingRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public DelAggLocMappingAppService(IRepository<DelAggLocMapping> delagglocmappingManager,
            IDelAggLocMappingListExcelExporter delagglocmappingExporter,
            IRepository<DelAggLocation> delagglocationRepo,
            IDelAggLocMappingManager delAggLocMappingManager,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _delagglocmappingRepo = delagglocmappingManager;
            _delagglocmappingExporter = delagglocmappingExporter;
            _delagglocationRepo = delagglocationRepo;
            _delAggLocMappingManager = delAggLocMappingManager;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultOutput<DelAggLocMappingListDto>> GetAll(GetDelAggLocMappingInput input)
        {
            var delagglocmap = _delagglocmappingRepo.GetAll();
            var rsLocation = _delagglocationRepo.GetAll();
            if (input.DelAggTypeRefId.HasValue)
                delagglocmap = delagglocmap.Where(t => (int)t.DelAggTypeRefId == input.DelAggTypeRefId);
            if (input.DelAggLocationId.HasValue)
                delagglocmap = delagglocmap.Where(t => t.DelAggLocationId == input.DelAggLocationId.Value);


            var allItems = (from locmap in delagglocmap
                join
                    loc in rsLocation
                    on locmap.DelAggLocationId equals loc.Id
                select new DelAggLocMappingListDto
                {
                    Id = locmap.Id,
                    DelAggLocationId = locmap.DelAggLocationId,
                    DelAggLocationName = loc.Location.Name,
                    DelAggTypeRefId = (int)locmap.DelAggTypeRefId,
                    GatewayCode = locmap.GatewayCode,
                    RemoteCode = locmap.RemoteCode,
                    DeliveryUrl = locmap.DeliveryUrl,
                    Active = locmap.Active,
                    Name = locmap.Name,
                    AddOns = locmap.AddOns,
                    CreationTime = locmap.CreationTime
                }).WhereIf(
                !input.Filter.IsNullOrEmpty(),
                p => p.Name.Contains(input.Filter)
            );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DelAggLocMappingListDto>>();

            var allItemCount = await allItems.CountAsync();

            var delAggTypeList = await GetDelAggTypeForCombobox();
            var delAggTypeDtos = delAggTypeList.Items.MapTo<List<ComboboxItemDto>>();
            foreach (var gp in allListDtos.GroupBy(t => t.DelAggTypeRefId))
            {
                var dat = delAggTypeDtos.FirstOrDefault(t => t.Value == gp.Key.ToString());
                if (dat != null)
                    foreach (var lst in gp.ToList())
                        lst.DelAggTypeRefName = dat.DisplayText;
            }

            return new PagedResultOutput<DelAggLocMappingListDto>(
                allItemCount,
                allListDtos
            );
        }

        public async Task<FileDto> GetAllToExcel(GetDelAggLocMappingInput input)
        {
            input.MaxResultCount = 1000;
            input.SkipCount = 0;

            var allList = await GetAll(input);

            //var allList = await _delagglocmappingRepo.GetAll().ToListAsync();
            var allListDtos = allList.Items.MapTo<List<DelAggLocMappingListDto>>();
            return _delagglocmappingExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDelAggLocMappingForEditOutput> GetDelAggLocMappingForEdit(NullableIdInput input)
        {
            DelAggLocMappingEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _delagglocmappingRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DelAggLocMappingEditDto>();
            }
            else
            {
                editDto = new DelAggLocMappingEditDto();
            }

            return new GetDelAggLocMappingForEditOutput
            {
                DelAggLocMapping = editDto
            };
        }

        public async Task CreateOrUpdateDelAggLocMapping(CreateOrUpdateDelAggLocMappingInput input)
        {
            var result = await ExistAllServerLevelBusinessRules(input);
            if (result)
            {
                if (input.DelAggLocMapping.Id.HasValue)
                    await UpdateDelAggLocMapping(input);
                else
                    await CreateDelAggLocMapping(input);
            }
        }

        public async Task DeleteDelAggLocMapping(IdInput input)
        {
            await _delagglocmappingRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetDelAggTypeForCombobox()
        {
            var retList = new List<ComboboxItemDto>();

            string enumstring;
            var EnumValues = Enum.GetValues(typeof(DelAggType));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(DelAggType), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                new ListResultOutput<ComboboxItemDto>(
                    retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetDelAggImageTypeForCombobox()
        {
            var retList = new List<ComboboxItemDto>();

            string enumstring;
            var EnumValues = Enum.GetValues(typeof(DelAggImageType));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(DelAggImageType), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                new ListResultOutput<ComboboxItemDto>(
                    retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetDelPriceTypeForCombobox()
        {
            var retList = new List<ComboboxItemDto>();

            string enumstring;
            var EnumValues = Enum.GetValues(typeof(DelPriceType));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(DelPriceType), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                new ListResultOutput<ComboboxItemDto>(
                    retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<ListResultOutput<DelAggLocationListDto>> GetAggLocationNames()
        {
            var lstDelAggLocation = await _delagglocationRepo.GetAll().ToListAsync();
            return new ListResultOutput<DelAggLocationListDto>(lstDelAggLocation.MapTo<List<DelAggLocationListDto>>());
        }

        public async Task<ListResultOutput<DelAggLocMappingListDto>> GetAggLocationMapNames()
        {
            var lstDelAggLocationMap = await _delagglocmappingRepo.GetAll().ToListAsync();
            return new ListResultOutput<DelAggLocMappingListDto>(lstDelAggLocationMap
                .MapTo<List<DelAggLocMappingListDto>>());
        }

        public async Task<bool> ExistAllServerLevelBusinessRules(CreateOrUpdateDelAggLocMappingInput input)
        {
            var locationId = input.DelAggLocMapping.DelAggLocationId;
            var delAggTypeId = input.DelAggLocMapping.DelAggTypeRefId;

            if (locationId == 0) throw new UserFriendlyException(L("LocationErr"));

            var locationExists = await _delagglocationRepo.FirstOrDefaultAsync(t => t.Id == locationId);

            var temp = await GetDelAggTypeForCombobox();
            var delAggTypeDtos = temp.Items.MapTo<List<ComboboxItemDto>>();

            string retList = null;

            foreach (var gp in delAggTypeDtos)
                if (gp.Value.Contains(delAggTypeId.ToString()))
                    retList = gp.DisplayText;

            if (input.DelAggLocMapping.Id.HasValue)
            {
                var recordExists = await _delagglocmappingRepo.FirstOrDefaultAsync(t =>
                    t.DelAggLocationId.Equals(locationId) && t.DelAggTypeRefId == input.DelAggLocMapping.DelAggTypeRefId);
                if (recordExists != null)
                    if (recordExists.Id != input.DelAggLocMapping.Id)
                        throw new UserFriendlyException(L("SameDataAlreadyExists") + " " +
                                                        locationExists.Location.Name + " " + retList);
            }
            else
            {
                var recordExists = await _delagglocmappingRepo.FirstOrDefaultAsync(t =>
                    t.DelAggLocationId.Equals(locationId) && t.DelAggTypeRefId == input.DelAggLocMapping.DelAggTypeRefId);
                if (recordExists != null)
                    throw new UserFriendlyException(L("SameDataAlreadyExists") + " " + locationExists.Location.Name +
                                                    " " + retList);
            }

            return true;
        }

        protected virtual async Task CreateDelAggLocMapping(CreateOrUpdateDelAggLocMappingInput input)
        {
            var dto = input.DelAggLocMapping.MapTo<DelAggLocMapping>();
            CheckErrors(await _delAggLocMappingManager.CreateSync(dto));
        }

        protected virtual async Task UpdateDelAggLocMapping(CreateOrUpdateDelAggLocMappingInput input)
        {
            var item = await _delagglocmappingRepo.GetAsync(input.DelAggLocMapping.Id.Value);
            var dto = input.DelAggLocMapping;
            //TODO: SERVICE DelAggLocMapping Update Individually
            item.DelAggLocationId = dto.DelAggLocationId;
            item.DelAggTypeRefId = dto.DelAggTypeRefId;
            item.Active = dto.Active;
            item.AddOns = dto.AddOns;
            item.DeliveryUrl = dto.DeliveryUrl;
            item.GatewayCode = dto.GatewayCode;
            item.Name = dto.Name;
            item.RemoteCode = dto.RemoteCode;
            CheckErrors(await _delAggLocMappingManager.CreateSync(item));
        }
    }
}