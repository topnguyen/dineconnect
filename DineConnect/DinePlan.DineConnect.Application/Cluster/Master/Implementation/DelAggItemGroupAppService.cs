﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Impl;

namespace DinePlan.DineConnect.Cluster.Master.Implementation
{
    public class DelAggItemGroupAppService : DineConnectAppServiceBase, IDelAggItemGroupAppService
    {
        private readonly IDelAggItemGroupListExcelExporter _delaggitemgroupExporter;
        private readonly IRepository<DelAggItemGroup> _delaggitemgroupRepo;
        private readonly IDelAggItemGroupManager _delAggItemGroupManager;
        private readonly IRepository<DelAggItem> _delaggitemRepo;
        public DelAggItemGroupAppService(IRepository<DelAggItemGroup> delaggitemgroupRepo,
            IDelAggItemGroupListExcelExporter delaggitemgroupExporter,
            IDelAggItemGroupManager delAggItemGroupManager,
            IRepository<DelAggItem> delaggitemRepo)
        {
            _delaggitemgroupRepo = delaggitemgroupRepo;
            _delaggitemgroupExporter = delaggitemgroupExporter;
            _delAggItemGroupManager = delAggItemGroupManager;
            _delaggitemRepo = delaggitemRepo;
        }

        public async Task<PagedResultOutput<DelAggItemGroupListDto>> GetAll(GetDelAggItemGroupInput input)
        {
            var allItems = _delaggitemgroupRepo
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DelAggItemGroupListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<DelAggItemGroupListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetDelAggItemGroupInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DelAggItemGroupListDto>>();
            return _delaggitemgroupExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDelAggItemGroupForEditOutput> GetDelAggItemGroupForEdit(NullableIdInput input)
        {
            DelAggItemGroupEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _delaggitemgroupRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DelAggItemGroupEditDto>();
            }
            else
            {
                editDto = new DelAggItemGroupEditDto();
            }

            return new GetDelAggItemGroupForEditOutput
            {
                DelAggItemGroup = editDto
            };
        }

        public async Task CreateOrUpdateDelAggItemGroup(CreateOrUpdateDelAggItemGroupInput input)
        {
            if (input.DelAggItemGroup.Id.HasValue)
            {
                //  In edit mode, the updated Name should not be Equal to existing Name
                var alreadyExist = _delaggitemgroupRepo.FirstOrDefault(p => p.Name == input.DelAggItemGroup.Name && p.Id != input.DelAggItemGroup.Id);
                if (alreadyExist != null)
                {
                    throw new UserFriendlyException(L("NameAlreadyExists"));
                }
                await UpdateDelAggItemGroup(input);
            }
            else
            {
                var alreadyExist = _delaggitemgroupRepo.FirstOrDefault(p => p.Name == input.DelAggItemGroup.Name);
                if (alreadyExist != null)
                {
                    throw new UserFriendlyException(L("NameAlreadyExists"));
                }
                await CreateDelAggItemGroup(input);
            }
        }

        public async Task DeleteDelAggItemGroup(IdInput input)
        {
            var locRefCount = await _delaggitemRepo.CountAsync(t => t.DelAggItemGroupId.Equals(input.Id));
            if (locRefCount == 0)
            {
                await _delaggitemgroupRepo.DeleteAsync(input.Id);
            }
            else
            {
                throw new UserFriendlyException(L("ReferenceExists", L("DelAggItemGroup"), L("DelAggItem")));
            }

        }

        protected virtual async Task UpdateDelAggItemGroup(CreateOrUpdateDelAggItemGroupInput input)
        {
            var item = await _delaggitemgroupRepo.GetAsync(input.DelAggItemGroup.Id.Value);
            var dto = input.DelAggItemGroup;
            //TODO: SERVICE DelAggItemGroup Update Individually
            item.Name = dto.Name;
            item.Code = dto.Code;
            CheckErrors(await _delAggItemGroupManager.CreateSync(item));
        }

        protected virtual async Task CreateDelAggItemGroup(CreateOrUpdateDelAggItemGroupInput input)
        {
            var dto = input.DelAggItemGroup.MapTo<DelAggItemGroup>();
            CheckErrors(await _delAggItemGroupManager.CreateSync(dto));
        }
        public async Task<ListResultOutput<DelAggItemGroupListDto>> GetNames()
        {
            var lstDelAggItemGroup = await _delaggitemgroupRepo.GetAll().ToListAsync();
            return new ListResultOutput<DelAggItemGroupListDto>(lstDelAggItemGroup.MapTo<List<DelAggItemGroupListDto>>());
        }
    }
}

