﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using System.Linq;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Cluster.Master;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Impl;

namespace DinePlan.DineConnect.Cluster.Implementation
{
    public class DelAggChargeAppService : DineConnectAppServiceBase, IDelAggChargeAppService
    {
        private readonly IDelAggChargeListExcelExporter _delaggchargeExporter;
        private readonly IRepository<DelAggCharge> _delaggchargeRepo;
        private readonly IDelAggChargeManager _delaggchargeManager;
        private readonly IRepository<DelAggChargeMapping> _delaggchargemappingRepo;

        public DelAggChargeAppService(IRepository<DelAggCharge> delaggchargeRepo, IDelAggChargeManager delaggchargeManager, IRepository<DelAggChargeMapping> delaggchargemappingRepo,
            IDelAggChargeListExcelExporter delaggchargeExporter)
        {
            _delaggchargeRepo = delaggchargeRepo;
            _delaggchargeManager = delaggchargeManager;
            _delaggchargemappingRepo = delaggchargemappingRepo;
            _delaggchargeExporter = delaggchargeExporter;
        }

        public async Task<PagedResultOutput<DelAggChargeListDto>> GetAll(GetDelAggChargeInput input)
        {
            var allItems = _delaggchargeRepo
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DelAggChargeListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<DelAggChargeListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetDelAggChargeInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DelAggChargeListDto>>();
            return _delaggchargeExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDelAggChargeForEditOutput> GetDelAggChargeForEdit(NullableIdInput input)
        {
            DelAggChargeEditDto editDto;
            List<DelAggChargeMappingListDto> editDetailDto;

            if (input.Id.HasValue)
            {
                var hDto = await _delaggchargeRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DelAggChargeEditDto>();

                editDetailDto = await (from tt in _delaggchargemappingRepo.GetAll().Where(a => a.DelAggChargeId == input.Id.Value)
                                       select new DelAggChargeMappingListDto
                                       {
                                           Id = tt.Id,
                                           DelAggChargeId = tt.DelAggChargeId,
                                           DelAggLocationGroupId = tt.DelAggLocationGroupId,
                                           DelItemGroupId = tt.DelItemGroupId
                                       }).ToListAsync();
            }
            else
            {
                editDto = new DelAggChargeEditDto();
                editDetailDto = new List<DelAggChargeMappingListDto>();
            }

            return new GetDelAggChargeForEditOutput
            {
                DelAggCharge = editDto,
                DelAggChargeMapping = editDetailDto
            };
        }

        public async Task CreateOrUpdateDelAggCharge(CreateOrUpdateDelAggChargeInput input)
        {
            int chargeId = 0;
            if (input.DelAggCharge.Id.HasValue)
            {
                chargeId = input.DelAggCharge.Id.Value;
                await UpdateDelAggCharge(input);
            }
            else
            {
                chargeId = await CreateDelAggCharge(input);
            }
        }

        public async Task DeleteDelAggCharge(IdInput input)
        {
            var mapdetail = await _delaggchargemappingRepo.GetAll().Where(t => t.DelAggChargeId == input.Id).ToListAsync();

            foreach (var map in mapdetail)
            {
                await _delaggchargemappingRepo.DeleteAsync(map.Id);
            }

            await _delaggchargeRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateDelAggCharge(CreateOrUpdateDelAggChargeInput input)
        {
            var item = await _delaggchargeRepo.GetAsync(input.DelAggCharge.Id.Value);
            var dto = input.DelAggCharge;
            //TODO: SERVICE DelAggCharge Update Individually
            item.Name = dto.Name;
            item.Code = dto.Code;
            item.Amount = dto.Amount;
            item.ApplicableOn = dto.ApplicableOn;
            item.ExcludeDeliveryTypes = dto.ExcludeDeliveryTypes;
            item.FullFilmentTypes = dto.FullFilmentTypes;
            item.LocalRefCode = dto.LocalRefCode;

            List<int> recordsToBeRetained = new List<int>();
            if (input.DelAggChargeMapping != null && input.DelAggChargeMapping.Count > 0)
            {
                foreach (var items in input.DelAggChargeMapping)
                {
                    List<DelAggChargeMapping> existingIssueDetail;
                    if (items.Id != 0)
                    {
                        var recId = (int)items.Id;
                        recordsToBeRetained.Add(recId);
                        existingIssueDetail = _delaggchargemappingRepo.GetAllList(tt => tt.DelAggChargeId == dto.Id && tt.Id == recId);
                    }
                    else
                    {
                        existingIssueDetail = _delaggchargemappingRepo.GetAllList(tt => tt.DelAggChargeId == dto.Id && tt.DelAggLocationGroupId == items.DelAggLocationGroupId && tt.DelItemGroupId == items.DelItemGroupId);
                    }

                    if (existingIssueDetail.Count == 0)  //Add new record
                    {
                        DelAggChargeMapping tm = new DelAggChargeMapping();
                        tm.DelAggChargeId = (int)dto.Id;
                        tm.DelAggLocationGroupId = items.DelAggLocationGroupId;
                        tm.DelItemGroupId = items.DelItemGroupId;

                        var ret2 = await _delaggchargemappingRepo.InsertOrUpdateAndGetIdAsync(tm);
                        recordsToBeRetained.Add(ret2);
                    }
                    else
                    {
                        var editDetailDto = await _delaggchargemappingRepo.GetAsync(existingIssueDetail[0].Id);

                        editDetailDto.DelAggChargeId = (int)dto.Id;
                        editDetailDto.DelAggLocationGroupId = items.DelAggLocationGroupId;
                        editDetailDto.DelItemGroupId = items.DelItemGroupId;
                        var ret3 = await _delaggchargemappingRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);
                        recordsToBeRetained.Add(ret3);
                    }
                }
            }

            var delTTList = _delaggchargemappingRepo.GetAll().Where(a => a.DelAggChargeId == input.DelAggCharge.Id.Value && !recordsToBeRetained.Contains((int)a.Id)).ToList();
            foreach (var a in delTTList)
            {
                _delaggchargemappingRepo.Delete(a.Id);
            }

            CheckErrors(await _delaggchargeManager.CreateSync(item));
        }

        protected virtual async Task<int> CreateDelAggCharge(CreateOrUpdateDelAggChargeInput input)
        {
            var dto = input.DelAggCharge.MapTo<DelAggCharge>();
            if (input.DelAggChargeMapping == null || input.DelAggChargeMapping.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }

            int retId = await _delaggchargeRepo.InsertAndGetIdAsync(dto);

            foreach (DelAggChargeMapping items in input.DelAggChargeMapping.ToList())
            {
                DelAggChargeMapping tm = new DelAggChargeMapping();
                tm.DelAggChargeId = (int)retId;
                tm.DelAggLocationGroupId = items.DelAggLocationGroupId;
                tm.DelItemGroupId = items.DelItemGroupId;

                var ret2 = await _delaggchargemappingRepo.InsertAndGetIdAsync(tm);
            }

            CheckErrors(await _delaggchargeManager.CreateSync(dto));

            return dto.Id;

            CheckErrors(await _delaggchargeManager.CreateSync(dto));
        }
    }
}