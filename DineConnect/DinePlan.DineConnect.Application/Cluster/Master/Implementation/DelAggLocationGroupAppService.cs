﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;

namespace DinePlan.DineConnect.Cluster.Master.Implementation
{
    public class DelAggLocationGroupAppService : DineConnectAppServiceBase, IDelAggLocationGroupAppService
    {
        private readonly IDelAggLocationGroupListExcelExporter _delagglocationgroupExporter;
        private readonly IRepository<DelAggLocationGroup> _delagglocationgroupRepo;
        private readonly IDelAggLocationGroupManager _delAggLocationGroupManager;
        private readonly IRepository<DelAggLocation> _delagglocationRepo;
        public DelAggLocationGroupAppService(IRepository<DelAggLocationGroup> delagglocationgroupRepo,
            IDelAggLocationGroupListExcelExporter delagglocationgroupExporter,
            IDelAggLocationGroupManager delAggLocationGroupManager,
            IRepository<DelAggLocation> delagglocationRepo)
        {
            _delagglocationgroupRepo = delagglocationgroupRepo;
            _delagglocationgroupExporter = delagglocationgroupExporter;
            _delAggLocationGroupManager = delAggLocationGroupManager;
            _delagglocationRepo = delagglocationRepo;
        }

        public async Task<PagedResultOutput<DelAggLocationGroupListDto>> GetAll(GetDelAggLocationGroupInput input)
        {
            var allItems = _delagglocationgroupRepo
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DelAggLocationGroupListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<DelAggLocationGroupListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetDelAggLocationGroupInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DelAggLocationGroupListDto>>();
            return _delagglocationgroupExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDelAggLocationGroupForEditOutput> GetDelAggLocationGroupForEdit(NullableIdInput input)
        {
            DelAggLocationGroupEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _delagglocationgroupRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DelAggLocationGroupEditDto>();
            }
            else
            {
                editDto = new DelAggLocationGroupEditDto();
            }

            return new GetDelAggLocationGroupForEditOutput
            {
                DelAggLocationGroup = editDto
            };
        }

        public async Task CreateOrUpdateDelAggLocationGroup(CreateOrUpdateDelAggLocationGroupInput input)
        {
            if (input.DelAggLocationGroup.Id.HasValue)
            {
                //  In edit mode, the updated Name should not be Equal to existing Name
                var alreadyExist = _delagglocationgroupRepo.FirstOrDefault(p => p.Name == input.DelAggLocationGroup.Name && p.Id != input.DelAggLocationGroup.Id);
                if (alreadyExist != null)
                {
                    throw new UserFriendlyException(L("NameAlreadyExists"));
                }
                await UpdateDelAggLocationGroup(input);
            }
            else
            {
                var alreadyExist = _delagglocationgroupRepo.FirstOrDefault(p => p.Name == input.DelAggLocationGroup.Name);
                if (alreadyExist != null)
                {
                    throw new UserFriendlyException(L("NameAlreadyExists"));
                }
                await CreateDelAggLocationGroup(input);
            }
        }

        public async Task DeleteDelAggLocationGroup(IdInput input)
        {
            var locRefCount = await _delagglocationRepo.CountAsync(t => t.DelAggLocationGroupId.Equals(input.Id));
            if( locRefCount == 0)
            {
                await _delagglocationgroupRepo.DeleteAsync(input.Id);
            }
            else
            {
                throw new UserFriendlyException( L("ReferenceExists", L("DelAggLocationGroup") ,L("DelAggLocation")));
            }
            
        }

        protected virtual async Task UpdateDelAggLocationGroup(CreateOrUpdateDelAggLocationGroupInput input)
        {
            var item = await _delagglocationgroupRepo.GetAsync(input.DelAggLocationGroup.Id.Value);
            var dto = input.DelAggLocationGroup;
            //TODO: SERVICE DelAggLocationGroup Update Individually
            item.Name = dto.Name;
        }

        protected virtual async Task CreateDelAggLocationGroup(CreateOrUpdateDelAggLocationGroupInput input)
        {
            var dto = input.DelAggLocationGroup.MapTo<DelAggLocationGroup>();
            CheckErrors(await _delAggLocationGroupManager.CreateSync(dto));
        }
        public async Task<ListResultOutput<DelAggLocationGroupListDto>> GetNames()
        {
            var lstDelAggLocationGroup = await _delagglocationgroupRepo.GetAll().ToListAsync();
            return new ListResultOutput<DelAggLocationGroupListDto>(lstDelAggLocationGroup.MapTo<List<DelAggLocationGroupListDto>>());
        }
    }
}

