﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Connect.OrderTags;
using DinePlan.DineConnect.Dto;
using System.Linq;
using System;
using Castle.Core.Logging;
using DinePlan.DineConnect.Cluster.Dtos;

namespace DinePlan.DineConnect.Cluster.Master.Implementation
{
    public class DelAggModifierGroupAppService : DineConnectAppServiceBase, IDelAggModifierGroupAppService
    {
        private readonly IDelAggModifierGroupListExcelExporter _delaggmodifiergroupExporter;
        private readonly IRepository<DelAggModifierGroup> _delaggmodifiergroupRepo;
        private readonly IRepository<DelAggModifier> _delaggmodifierRepo;
        private readonly IDelAggModifierGroupManager _delAggModifierGroupManager;
        private readonly IRepository<OrderTagGroup> _ordertaggroupRepo;
        private readonly IRepository<DelAggModifierGroupItem> _delaggmodifiergroupitemRepo;
        private readonly ILogger _logger;
        private readonly IRepository<DelAggImage> _delaggimageRepo;

        public DelAggModifierGroupAppService(IRepository<DelAggModifierGroup> delaggmodifiergroupRepo, IRepository<DelAggModifier> delaggmodifierRepo,
            IDelAggModifierGroupListExcelExporter delaggmodifiergroupExporter,
            IDelAggModifierGroupManager delAggModifierGroupManager,
            IRepository<OrderTagGroup> ordertaggroupRepo, IRepository<DelAggModifierGroupItem> delaggmodifiergroupitemRepo, ILogger logger, IRepository<DelAggImage> delaggimageRepo)
        {
            _delaggmodifiergroupRepo = delaggmodifiergroupRepo;
            _delaggmodifierRepo = delaggmodifierRepo;
            _delaggmodifiergroupExporter = delaggmodifiergroupExporter;
            _delAggModifierGroupManager = delAggModifierGroupManager;
            _ordertaggroupRepo = ordertaggroupRepo;
            _delaggmodifiergroupitemRepo = delaggmodifiergroupitemRepo;
            _logger = logger;
            _delaggimageRepo = delaggimageRepo;
        }

        public async Task<PagedResultOutput<DelAggModifierGroupListDto>> GetAll(GetDelAggModifierGroupInput input)
        {
            var rsdelaggmodifiergroup = _delaggmodifiergroupRepo.GetAll();

            if (input.OrderTagGroupId.HasValue)
            {
                rsdelaggmodifiergroup = rsdelaggmodifiergroup.Where(t => t.OrderTagGroupId == input.OrderTagGroupId.Value);
            }

            var allItems = (from mg in rsdelaggmodifiergroup
                            join otg in _ordertaggroupRepo.GetAll()
                            on mg.OrderTagGroupId equals otg.Id
                            select new DelAggModifierGroupListDto()
                            {
                                Id = mg.Id,
                                OrderTagGroupId = mg.OrderTagGroupId,
                                Name = mg.Name,
                                Min = mg.Min,
                                Max = mg.Max,
                                CreationTime = mg.CreationTime
                            }).WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DelAggModifierGroupListDto>>();

            var allItemCount = await allItems.CountAsync();

            foreach (var lst in allListDtos)
            {
                if (lst.OrderTagGroupId != null)
                {
                    var lstOrderTagGrp = _ordertaggroupRepo.FirstOrDefault(t => t.Id == lst.OrderTagGroupId);
                    if (lstOrderTagGrp != null)
                        lst.OrderTagGroupName = lstOrderTagGrp.Name;
                }
            }

            return new PagedResultOutput<DelAggModifierGroupListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetDelAggModifierGroupInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DelAggModifierGroupListDto>>();
            return _delaggmodifiergroupExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDelAggModifierGroupForEditOutput> GetDelAggModifierGroupForEdit(NullableIdInput input)
        {
            DelAggModifierGroupEditDto editDto;
            List<DelAggImageEditDto> editImageDto = new List<DelAggImageEditDto>();
            List<DelAggModifierGroupItemEditDto> editDetailDto;

            if (input.Id.HasValue)
            {
                var hDto = await _delaggmodifiergroupRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DelAggModifierGroupEditDto>();

                #region DelAggModifierGroupItems
                var editModifierGroupDto = await _delaggmodifiergroupitemRepo.GetAllListAsync(t => t.DelAggModifierGroupId == editDto.Id);

                if (editModifierGroupDto.Count > 0)
                    editDetailDto = editModifierGroupDto.MapTo<List<DelAggModifierGroupItemEditDto>>();
                else
                    editDetailDto = new List<DelAggModifierGroupItemEditDto>();

                #endregion

                #region Image Edit

                var imageItem = await _delaggimageRepo.GetAllListAsync(t => t.ReferenceId == editDto.Id && t.DelAggImageTypeRefId == (int)DelAggImageType.ModifierGroup);

                if (imageItem != null && imageItem.Count > 0)
                    editImageDto = imageItem.MapTo<List<DelAggImageEditDto>>();
                else
                    editImageDto = new List<DelAggImageEditDto>();

                #endregion

            }
            else
            {
                editDto = new DelAggModifierGroupEditDto();
                editImageDto = new List<DelAggImageEditDto>();
                editDetailDto = new List<DelAggModifierGroupItemEditDto>();
            }

            return new GetDelAggModifierGroupForEditOutput
            {
                DelAggModifierGroup = editDto,
                DelAggImage = editImageDto,
                DelAggModifierGroupItem = editDetailDto
            };
        }

        public async Task<int> CreateOrUpdateDelAggModifierGroup(CreateOrUpdateDelAggModifierGroupInput input)
        {
            var result = await ExistAllServerLevelBusinessRules(input);
            if (result == true)
            {
                if (input.DelAggModifierGroup.Id.HasValue)
                {
                    return await UpdateDelAggModifierGroup(input);
                }
                else
                {
                    return await CreateDelAggModifierGroup(input);
                }
            }
            return 0;
        }
        public async Task<bool> ExistAllServerLevelBusinessRules(CreateOrUpdateDelAggModifierGroupInput input)
        {
            var orderTagGroupExists = await _ordertaggroupRepo.FirstOrDefaultAsync(t => t.Id == (input.DelAggModifierGroup.OrderTagGroupId));

            if (input.DelAggModifierGroup.Id.HasValue)
            {
                var recordExists = await _delaggmodifiergroupRepo.FirstOrDefaultAsync(t => t.Name.Equals(input.DelAggModifierGroup.Name) && t.OrderTagGroupId == input.DelAggModifierGroup.OrderTagGroupId);
                if (recordExists != null)
                {
                    if (recordExists.Id != input.DelAggModifierGroup.Id)
                    {
                        throw new UserFriendlyException(L("SameDataAlreadyExists") + " " + orderTagGroupExists.Name);
                    }
                    else
                    {
                    }
                }
                else
                {

                }
            }
            else
            {
                var recordExists = await _delaggmodifiergroupRepo.FirstOrDefaultAsync(t => t.Name.Equals(input.DelAggModifierGroup.Name) && t.OrderTagGroupId == input.DelAggModifierGroup.OrderTagGroupId);
                if (recordExists != null)
                {
                    throw new UserFriendlyException(L("SameDataAlreadyExists") + " " + orderTagGroupExists.Name);
                }
                else
                {

                }
            }
            return true;
        }

        public async Task DeleteDelAggModifierGroup(IdInput input)
        {
            var locRefCount = await _delaggmodifierRepo.CountAsync(t => t.DelAggModifierGroupId.Value == input.Id);
            if (locRefCount == 0)
            {
                var mapdetail = await _delaggmodifiergroupitemRepo.GetAll().Where(t => t.DelAggModifierGroupId == input.Id).ToListAsync();

                foreach (var map in mapdetail)
                {
                    await _delaggmodifiergroupitemRepo.DeleteAsync(map.Id);
                }

                await _delaggmodifiergroupRepo.DeleteAsync(input.Id);
            }
            else
            {
                throw new UserFriendlyException(L("ReferenceExists", L("DelAggModifierGroup"), L("DelAggModifier")));
            }

        }

        protected virtual async Task<int> UpdateDelAggModifierGroup(CreateOrUpdateDelAggModifierGroupInput input)
        {
            var item = await _delaggmodifiergroupRepo.GetAsync(input.DelAggModifierGroup.Id.Value);
            var dto = input.DelAggModifierGroup;
            //TODO: SERVICE DelAggModifierGroup Update Individually
            item.Name = dto.Name;
            item.Code = dto.Code;
            item.SortOrder = dto.SortOrder;
            item.LocalRefCode = dto.LocalRefCode;
            item.Min = dto.Min;
            item.Max = dto.Max;
            item.OrderTagGroupId = dto.OrderTagGroupId;

            #region DelAggModifierGroup Items
            List<int> toBeRetainedDetailsIds = new List<int>();
            foreach (DelAggModifierGroupItemEditDto items in input.DelAggModifierGroupItems)
            {
                var alreadyExist = await _delaggmodifiergroupitemRepo.FirstOrDefaultAsync(t => t.DelAggModifierGroupId == dto.Id && t.DelAggItemId == items.DelAggItemId);
                if (alreadyExist == null)
                {
                    DelAggModifierGroupItem tm = new DelAggModifierGroupItem();
                    tm.DelAggModifierGroupId = (int)dto.Id;
                    tm.DelAggItemId = items.DelAggItemId;
                    var ret2 = await _delaggmodifiergroupitemRepo.InsertAsync(tm);
                    toBeRetainedDetailsIds.Add(tm.Id);
                }
                else
                {
                    toBeRetainedDetailsIds.Add(alreadyExist.Id);
                }
            }
            var detailsToBeDeleted = await _delaggmodifiergroupitemRepo.GetAll().Where(t => t.DelAggModifierGroupId == dto.Id && !toBeRetainedDetailsIds.Contains(t.Id)).ToListAsync();
            foreach(var delLst in detailsToBeDeleted)
            {
                await _delaggmodifiergroupitemRepo.DeleteAsync(delLst.Id);
            }
            #endregion

            #region DelAggImage Creation

            var toBeRetainedImageItems = new List<int>();
            if (input.DelAggImage != null && input.DelAggImage.Count > 0)
            {
                foreach (var lst in input.DelAggImage)
                {
                    var existsImage = await _delaggimageRepo.FirstOrDefaultAsync(t =>
                            t.DelAggImageTypeRefId == (int)DelAggImageType.ModifierGroup &&
                            t.DelAggTypeRefId == lst.DelAggTypeRefId && t.Id == lst.Id);
                    if (existsImage == null)
                    {
                        var delAggImageDto = new DelAggImage
                        {
                            DelAggTypeRefId = lst.DelAggTypeRefId,
                            DelAggImageTypeRefId = (int)DelAggImageType.ModifierGroup,
                            ImagePath = lst.ImagePath,
                            ReferenceId = dto.Id

                        };
                        await _delaggimageRepo.InsertOrUpdateAndGetIdAsync(delAggImageDto);
                        toBeRetainedImageItems.Add(delAggImageDto.Id);
                    }
                    else
                        toBeRetainedImageItems.Add(existsImage.Id);
                }

            }

            var delImagesToBe = await _delaggimageRepo.GetAll().Where(t => t.ReferenceId == item.Id && !toBeRetainedImageItems.Contains(t.Id)).ToListAsync();
            foreach (var imgToBeDelete in delImagesToBe)
            {
                await _delaggimageRepo.DeleteAsync(imgToBeDelete.Id);
            }

            #endregion

            CheckErrors(await _delAggModifierGroupManager.CreateSync(item));

            return item.Id;
        }

        protected virtual async Task<int> CreateDelAggModifierGroup(CreateOrUpdateDelAggModifierGroupInput input)
        {
            try
            {
                var dto = input.DelAggModifierGroup.MapTo<DelAggModifierGroup>();


                int retId = await _delaggmodifiergroupRepo.InsertAndGetIdAsync(dto);

                foreach (DelAggModifierGroupItemEditDto items in input.DelAggModifierGroupItems)
                {
                    DelAggModifierGroupItem tm = new DelAggModifierGroupItem();
                    tm.DelAggModifierGroupId = (int)retId;
                    tm.DelAggItemId = items.DelAggItemId;

                    var ret2 = await _delaggmodifiergroupitemRepo.InsertAndGetIdAsync(tm);
                }


                CheckErrors(await _delAggModifierGroupManager.CreateSync(dto));

                return dto.Id;

            }
            catch (Exception exception)
            {
                _logger.Error("CreateModifierGroup", exception);
                throw new UserFriendlyException(exception.Message);
            }
        }
        public async Task<ListResultOutput<DelAggModifierGroupListDto>> GetNames()
        {
            var lstDelAggModifierGroup = await _delaggmodifiergroupRepo.GetAll().ToListAsync();
            return new ListResultOutput<DelAggModifierGroupListDto>(lstDelAggModifierGroup.MapTo<List<DelAggModifierGroupListDto>>());
        }
    }
}

