﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Cluster.Master.Exporter;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.OrderTags;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Member.Dtos.DinePlan.DineConnect.Engage.Dtos;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Job.Cluster;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Cluster.Master.Implementation
{
    public class DelAggItemAppService : DineConnectAppServiceBase, IDelAggItemAppService
    {
        private readonly IBackgroundJobManager _bgm;
        private readonly IDelAggCategoryAppService _categoryAppService;
        private readonly IRepository<DelAggCategory> _delaggcategoryRepo;
        private readonly IRepository<DelAggImage> _delaggimageRepo;
        private readonly IDelAggItemListExcelExporter _delaggitemExporter;
        private readonly IDelAggItemGroupAppService _delAggItemGroupAppService;
        private readonly IRepository<DelAggItemGroup> _delaggitemgroupRepo;
        private readonly IDelAggItemManager _delAggItemManager;
        private readonly IRepository<DelAggItem> _delaggitemRepo;
        private readonly IRepository<DelAggTax> _delaggtaxRepo;
        private readonly IRepository<DelAggVariantGroup> _delaggvariantgroupRepo;
        private readonly IRepository<DelAggVariant> _delaggvariantRepo;
        private readonly IDelTimingGroupAppService _delTimingGroupAppService;
        private readonly IDelAggVariantGroupAppService _delAggVariantGroupAppService;
        private readonly IRepository<DelTimingGroup> _deltiminggroupRepo;
        private readonly IMenuItemAppService _menuItemAppService;
        private readonly IRepository<Category> _categoryRepo;
        private readonly IRepository<MenuItemPortion> _menuitemportionRepo;
        private readonly IRepository<MenuItem> _menuitemRepo;
        private readonly IRepository<OrderTagGroup> _orderTagGroupRepo;
        private readonly IDelAggModifierGroupAppService _delAggModifierGroupAppService;
        private readonly IDelAggModifierAppService _delAggModifierAppService;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IRepository<DelAggModifierGroup> _delAggModifierGroupRepo;
        private readonly IRepository<DelAggModifierGroupItem> _delAggModifierGroupItemRepo;
        private readonly IRepository<DelAggModifier> _delAggModifierRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public DelAggItemAppService(IRepository<DelAggItem> delaggitemRepo,
            IDelAggItemListExcelExporter delaggitemExporter,
            IDelAggItemManager delAggItemManager, IRepository<MenuItem> menuitemRepo,
            IRepository<MenuItemPortion> menuitemportionRepo, IRepository<DelAggCategory> delaggcategoryRepo,
            IRepository<DelAggTax> delaggtaxRepo, IRepository<DelAggItemGroup> delaggitemgroupRepo,
            IRepository<DelAggVariantGroup> delaggvariantgroupRepo,
            IRepository<DelAggVariant> delaggvariantRepo,
            IRepository<DelAggImage> delaggimageRepo,
            IMenuItemAppService menuItemAppService,
            IDelAggCategoryAppService categoryAppService,
            IDelAggItemGroupAppService delAggItemGroupAppService,
            IDelAggVariantGroupAppService delAggVariantGroupAppService,
            IRepository<DelTimingGroup> deltiminggroupRepo,
            IDelTimingGroupAppService delTimingGroupAppService,
            IDelAggModifierGroupAppService delAggModifierGroupAppService,
            IDelAggModifierAppService delAggModifierAppService,
            IReportBackgroundAppService rbas,
            IBackgroundJobManager bgm,
            IRepository<OrderTagGroup> orderTagGroupRepo,
            IRepository<DelAggModifierGroup> delAggModifierGroupRepo,
            IRepository<DelAggModifierGroupItem> delAggModifierGroupItemRepo,
            IRepository<DelAggModifier> delAggModifierRepo,
            IRepository<Category> categoryRepo,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _delaggitemRepo = delaggitemRepo;
            _delaggitemExporter = delaggitemExporter;
            _delAggItemManager = delAggItemManager;
            _menuitemRepo = menuitemRepo;
            _menuitemportionRepo = menuitemportionRepo;
            _delaggcategoryRepo = delaggcategoryRepo;
            _delaggtaxRepo = delaggtaxRepo;
            _delaggitemgroupRepo = delaggitemgroupRepo;
            _delaggvariantgroupRepo = delaggvariantgroupRepo;
            _delaggvariantRepo = delaggvariantRepo;
            _delaggimageRepo = delaggimageRepo;
            _menuItemAppService = menuItemAppService;
            _categoryAppService = categoryAppService;
            _delAggItemGroupAppService = delAggItemGroupAppService;
            _deltiminggroupRepo = deltiminggroupRepo;
            _delTimingGroupAppService = delTimingGroupAppService;
            _delAggVariantGroupAppService = delAggVariantGroupAppService;
            _rbas = rbas;
            _bgm = bgm;
            _unitOfWorkManager = unitOfWorkManager;
            _orderTagGroupRepo = orderTagGroupRepo;
            _delAggModifierGroupAppService = delAggModifierGroupAppService;
            _delAggModifierAppService = delAggModifierAppService;
            _delAggModifierGroupRepo = delAggModifierGroupRepo;
            _delAggModifierGroupItemRepo = delAggModifierGroupItemRepo;
            _delAggModifierRepo = delAggModifierRepo;
            _categoryRepo = categoryRepo;
        }

        public async Task<PagedResultOutput<DelAggItemListDto>> GetAll(GetDelAggItemInput input)
        {
            var rsDeAggItem = _delaggitemRepo.GetAll();

            var rsMenuItemPortion = _menuitemportionRepo.GetAll();
            var rsMenuItem = _menuitemRepo.GetAll();
            var rsCategory = _delaggcategoryRepo.GetAll();
            var rsTax = _delaggtaxRepo.GetAll();
            var rsItemGroup = _delaggitemgroupRepo.GetAll();

            if (input.MenuItemId.HasValue && input.MenuItemId.Value > 0)
                rsDeAggItem = rsDeAggItem.Where(t => t.MenuItemId == input.MenuItemId.Value);
            if (input.DelAggCatId.HasValue)
                rsDeAggItem = rsDeAggItem.Where(t => t.DelAggCatId == input.DelAggCatId.Value);
            if (input.DelAggTaxId.HasValue)
                rsDeAggItem = rsDeAggItem.Where(t => t.DelAggTaxId == input.DelAggTaxId.Value);
            if (input.DelAggItemGroupId.HasValue)
                rsDeAggItem = rsDeAggItem.Where(t => t.DelAggItemGroupId == input.DelAggItemGroupId.Value);

            var allItems = (from i in rsDeAggItem
                            join ig in rsItemGroup
                                on i.DelAggItemGroupId equals ig.Id
                            join mi in rsMenuItem
                                on i.MenuItemId equals mi.Id
                            join c in rsCategory
                                on i.DelAggCatId equals c.Id
                            select new DelAggItemListDto
                            {
                                Id = i.Id,
                                Name = i.Name,
                                Code = i.Code,
                                DelAggItemGroupId = i.DelAggItemGroupId,
                                MenuItemId = i.MenuItemId,
                                DelAggVariantGroupId = i.DelAggVariantGroupId,
                                DelAggTaxId = i.DelAggTaxId.Value,
                                DelAggCatId = i.DelAggCatId,
                                FoodType = i.FoodType,
                                SortOrder = i.SortOrder,
                                LocalRefCode = i.LocalRefCode
                            }).WhereIf(
                !input.Filter.IsNullOrEmpty(),
                p => p.Name.Contains(input.Filter)
            );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DelAggItemListDto>>();

            var allItemCount = await allItems.CountAsync();
            foreach (var lst in allListDtos)
            {
                {
                    var lstDelAggGrp = rsItemGroup.FirstOrDefault(t => t.Id == lst.DelAggItemGroupId);
                    if (lstDelAggGrp != null)
                        lst.DelAggItemGroupName = lstDelAggGrp.Name;
                }

                if (lst.DelAggTaxId != null)
                {
                    var lstDelAggTax = rsTax.FirstOrDefault(t => t.Id == lst.DelAggTaxId);
                    if (lstDelAggTax != null)
                        lst.DelAggTaxName = lstDelAggTax.Name;
                }

                {
                    var lstDelAggCat = rsCategory.FirstOrDefault(t => t.Id == lst.DelAggCatId);
                    if (lstDelAggCat != null)
                        lst.DelAggCatName = lstDelAggCat.Name;
                }

                {
                    var lstMenuItem = rsMenuItem.FirstOrDefault(t => t.Id == lst.MenuItemId);
                    if (lstMenuItem != null)
                        lst.MenuItemName = lst.MenuItemId + " - " + lstMenuItem.Name;
                }
            }

            return new PagedResultOutput<DelAggItemListDto>(
                allItemCount,
                allListDtos
            );
        }

        public async Task<FileDto> GetAllToExcel(GetDelAggItemInput input)
        {
            var allList = await _delaggitemRepo.GetAll().ToListAsync();
            var allListDots = allList.MapTo<List<DelAggItemListDto>>();
            return _delaggitemExporter.ExportToFile(allListDots.OrderBy(a => a.DelAggCatName)
                .ToList());
        }

        public async Task<GetDelAggItemForEditOutput> GetDelAggItemForEdit(NullableIdInput input)
        {
            DelAggItemEditDto editDto;
            var editDelAggVariantGroupDto = new DelAggVariantGroupEditDto();
            List<DelAggVariantEditDto> editDelAggVariantDto;
            List<DelAggImageEditDto> editImageDto = new List<DelAggImageEditDto>();
            List<DelAggModifierGroupItemEditDto> editDetailDto;

            if (input.Id.HasValue)
            {
                var hDto = await _delaggitemRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DelAggItemEditDto>();

                var rsMenuItem = _menuitemRepo.GetAll();
                var lstMenuItem = rsMenuItem.FirstOrDefault(t => t.Id == hDto.MenuItemId);
                var rsMenuItemPortion = _menuitemportionRepo.GetAll();

                var varGrp =
                    await _delaggvariantgroupRepo.FirstOrDefaultAsync(
                        t => t.Id == hDto.DelAggVariantGroupId);
                if (varGrp != null)
                    editDelAggVariantGroupDto = varGrp.MapTo<DelAggVariantGroupEditDto>();

                if (lstMenuItem != null)
                    editDto.MenuItemName = lstMenuItem.Name;

                var lstVariants =
                   await _delaggvariantRepo.GetAllListAsync(t => t.DelAggVariantGroupId == hDto.DelAggVariantGroupId);
                editDelAggVariantDto = lstVariants.MapTo<List<DelAggVariantEditDto>>();

                if (editDelAggVariantDto.Count > 0)
                    foreach (var lst in editDelAggVariantDto)
                    {
                        var lstMenuItemPortion =
                            rsMenuItemPortion.FirstOrDefault(t => t.Id == lst.MenuItemPortionId);
                        if (lstMenuItemPortion != null)
                            lst.MenuItemPortionName =
                                lstMenuItemPortion.MenuItem.Name + "(" + lstMenuItemPortion.Name + ")";
                    }

                #region Image Edit

                var imageItem = await _delaggimageRepo.GetAllListAsync(t => t.ReferenceId == editDto.Id && t.DelAggImageTypeRefId == (int)DelAggImageType.Item);

                if (imageItem != null && imageItem.Count > 0)
                    editImageDto = imageItem.MapTo<List<DelAggImageEditDto>>();
                else
                    editImageDto = new List<DelAggImageEditDto>();

                #endregion

                #region DelAggModifierGroupItems

                editDetailDto = await (from es in _delAggModifierGroupItemRepo.GetAll().Where(t => t.DelAggItemId == editDto.Id)
                                   join modGrp in _delAggModifierGroupRepo.GetAll() on
                                   es.DelAggModifierGroupId equals modGrp.Id
                                   select new DelAggModifierGroupItemEditDto
                                   {
                                       DelAggItemId = es.DelAggItemId,
                                       DelAggModifierGroupId = es.DelAggModifierGroupId,
                                       Name = modGrp.Name
                                   }).ToListAsync();

                //var editModifierGroupDto = await _delAggModifierGroupItemRepo.GetAllListAsync(t => t.DelAggItemId == editDto.Id);

                //if (editModifierGroupDto.Count > 0)
                //    editDetailDto = editModifierGroupDto.MapTo<List<DelAggModifierGroupItemEditDto>>();
                //else
                //    editDetailDto = new List<DelAggModifierGroupItemEditDto>();

                #endregion
            }
            else
            {
                editDto = new DelAggItemEditDto();
                editDelAggVariantGroupDto = new DelAggVariantGroupEditDto();
                editImageDto = new List<DelAggImageEditDto>();
                editDelAggVariantDto = new List<DelAggVariantEditDto>();
                editDetailDto = new List<DelAggModifierGroupItemEditDto>();
            }

            return new GetDelAggItemForEditOutput
            {
                DelAggItem = editDto,
                DelAggVariantGroup = editDelAggVariantGroupDto,
                DelAggVariant = editDelAggVariantDto,
                DelAggImage = editImageDto,
                DelAggModifierGroupItem = editDetailDto
            };
        }

        public async Task<IdInput> CreateOrUpdateDelAggItem(CreateOrUpdateDelAggItemInput input)
        {
            if (input.DelAggItem.Id.HasValue)
                return await UpdateDelAggItem(input);
            return await CreateDelAggItem(input);
        }

        public async Task DeleteDelAggItem(IdInput input)
        {
            await _delaggitemRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultOutput<DelAggItemListDto>> GetNames()
        {
            var lstDelAggItem = await _delaggitemRepo.GetAll().ToListAsync();
            return new ListResultOutput<DelAggItemListDto>(lstDelAggItem.MapTo<List<DelAggItemListDto>>());
        }

        public async Task<List<DelAggItemEditDto>> GetImportDelAggItemDetail(GetDelAggItemInput input)
        {
            var allItems = await _delaggitemRepo.GetAllListAsync();
            var sortMenuItems = allItems
                .OrderBy(input.Sorting);
            var allListDtos = sortMenuItems.MapTo<List<DelAggItemEditDto>>();
            foreach (var lst in allListDtos)
            {
                var remarks = " ";
                if (lst.OutputJsonDto != null)
                {
                    var jsonOutput = JsonConvert.DeserializeObject<ExportDelAggItemImportStatusDto>(lst.OutputJsonDto);
                    if (jsonOutput != null && !jsonOutput.IsFailed)
                    {
                        remarks = remarks + "Inserted Cards : " + jsonOutput.ImportedCardsList.Count + " , " +
                                  "Duplicate Exists in Excel : " + jsonOutput.DuplicateExistInExcel.Count + " , " +
                                  "Duplicate Exists in DB : " + jsonOutput.DuplicateExistInDB.Count + " , Error : " +
                                  jsonOutput.ErrorList.Count;
                        lst.IsFailed = false;
                    }
                    else
                    {
                        remarks = L("Can’tImport");
                        lst.IsFailed = true;
                    }
                }
                else
                {
                    remarks = remarks + "In Process";
                    lst.IsFailed = true;
                }

                lst.Remarks = remarks;
            }

            return allListDtos;
        }


        public async Task<FileDto> BulkDelAggItemImport(BulkDelAggItemImportDtos input)
        {
            foreach (var lst in input.CreateOrUpdateDelAggItemInputs)
            {
                lst.DelAggItem.TenantId = input.TenantId;
                lst.DelAggItem.CreatorUserId = input.UserId;
                await CreateOrUpdateDelAggItem(lst);
            }

            return null;
        }

        public async Task InitiateBackGroundClone()
        {
            if (AbpSession.UserId != null)
                if (AbpSession.TenantId != null)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = "CloneClusterItems",
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = "CloneClusterItems"
                    });

                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<DelAggItemBackgroundClone, DelAggItemBackgroundCloneIdJobArgs>(
                            new DelAggItemBackgroundCloneIdJobArgs
                            {
                                BackGroundId = backGroundId,
                                UserId = AbpSession.UserId.Value,
                                TenantId = AbpSession.TenantId.Value
                            });
                }
        }

        public async Task<FileDto> CloneItems(CloneDelAggItemDtos input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MustHaveTenant, AppConsts.ConnectFilter))
            {
                var alreadyLinkedMenuIds = await _delaggitemRepo.GetAll().Where(t => t.TenantId == input.TenantId)
                    .Select(t => t.MenuItemId).ToListAsync();
                var menuItems = await _menuitemRepo.GetAll().Where(t => t.TenantId == input.TenantId
                                                                        && !alreadyLinkedMenuIds.Contains(t.Id) &&
                                                                        t.CreatorUserId == input.UserId)
                    .Select(t => new { t.Id, t.Name, t.AliasCode }).ToListAsync();

                foreach (var lst in menuItems)
                    await ConvertAsMenuItemAsDelAggItem(new TenantUserIdInput
                    { TenantId = input.TenantId, UserId = input.UserId, Id = lst.Id });

                #region Modifier
                var orderTagGroups = await _orderTagGroupRepo.GetAll().Include(x => x.Maps).Include(x => x.Tags).ToListAsync();
                foreach (var orderTagGroup in orderTagGroups)
                {
                    // Modifier Group
                    var delModGroup = await _delAggModifierGroupRepo.FirstOrDefaultAsync(x => x.Name == orderTagGroup.Name);
                    if (delModGroup == null)
                    {
                        var groupItems = new List<DelAggModifierGroupItemEditDto>();
                        // Add item
                        foreach (var map in orderTagGroup.Maps)
                        {
                            var delIds = new List<int>();
                            if (map.MenuItemId != null)
                            {
                                // Find del item
                                var delId = _delaggitemRepo.FirstOrDefault(x => x.MenuItemId == map.MenuItemId)?.Id;
                                if (delId != null) { delIds.Add(delId.Value); }

                            }
                            else if (map.CategoryId != null)
                            {
                                delIds = await (from delItem in _delaggitemRepo.GetAll()
                                                join mnItem in _menuitemRepo.GetAll() on delItem.MenuItemId equals mnItem.Id
                                                join cat in _categoryRepo.GetAll() on mnItem.CategoryId equals cat.Id
                                                where cat.Id == map.CategoryId.Value
                                                select delItem.Id).ToListAsync();
                            }
                            delIds.ForEach(x => groupItems.Add(new DelAggModifierGroupItemEditDto() { DelAggItemId = x }));

                        }
                        var groupId = await _delAggModifierGroupAppService.CreateOrUpdateDelAggModifierGroup(new CreateOrUpdateDelAggModifierGroupInput()
                        {
                            DelAggModifierGroup = new DelAggModifierGroupEditDto()
                            {
                                Code = orderTagGroup.Name,
                                Name = orderTagGroup.Name,
                                Min = orderTagGroup.MinSelectedItems,
                                Max = orderTagGroup.MaxSelectedItems,
                                OrderTagGroupId = orderTagGroup.Id,
                                SortOrder = orderTagGroup.SortOrder
                            },
                            DelAggModifierGroupItems = groupItems
                        });

                        foreach (var tag in orderTagGroup.Tags)
                        {
                            var delTag = await _delAggModifierRepo.FirstOrDefaultAsync(x => x.Name == tag.Name);
                            if (delTag == null)
                            {
                                await _delAggModifierAppService.CreateOrUpdateDelAggModifier(new CreateOrUpdateDelAggModifierInput()
                                {
                                    DelAggModifier = new DelAggModifierEditDto()
                                    {
                                        DelAggModifierGroupId = groupId,
                                        OrderTagId = tag.Id,
                                        OrderTagName = tag.Name,
                                        Name = tag.Name,
                                        Price = tag.Price
                                    }
                                });
                            }
                        }

                    }
                }
                #endregion
                return new FileDto();
            }
        }

        public async Task<MessageOutputDto> ConvertAsMenuItemAsDelAggItem(TenantUserIdInput input)
        {
            if (input.TenantId == 0)
                if (AbpSession.TenantId != null)
                    input.TenantId = AbpSession.TenantId.Value;
            if (input.UserId == 0)
                if (AbpSession.UserId != null)
                    input.UserId = AbpSession.UserId.Value;

            var outputDto = new MessageOutputDto();
            // Check Whether the menu item id already linked with DelAggMenuItem
            var alreadyLinked = await _delaggitemRepo.FirstOrDefaultAsync(t => t.MenuItemId == input.Id);
            if (alreadyLinked != null)
            {
                outputDto.ErrorMessage = L("MenuAlreadyLinkedWithDelAggItem",
                    alreadyLinked.Code + " - " + alreadyLinked.Name);
                outputDto.SuccessFlag = false;
                return outputDto;
            }

            //  Get Data From Menu
            var menuDto = await _menuItemAppService.GetMenuItemForEdit(new NullableIdInput { Id = input.Id });

            // Call ForEdit for DelAggMenuItem - Call as null for getting new records to add
            var delAggItemNewDto = await GetDelAggItemForEdit(new NullableIdInput { Id = null });

            // Fill the DelAggMenuItem
            delAggItemNewDto.DelAggItem.Code = menuDto.MenuItem.AliasCode;
            delAggItemNewDto.DelAggItem.Name = menuDto.MenuItem.Name;
            var delAggVarGroupDto = new DelAggVariantGroupEditDto();
            if (menuDto.MenuItem.Id != null)
            {
                delAggItemNewDto.DelAggItem.MenuItemId = menuDto.MenuItem.Id.Value;
                delAggItemNewDto.DelAggItem.MenuItemName = menuDto.MenuItem.Name;

                #region Item Group Setup

                var groupCode = L("Food");
                var groupName = L("Food");
                if (menuDto.MenuItem.Category != null && menuDto.MenuItem.Category.ProductGroup != null)
                {
                    groupCode = menuDto.MenuItem.Category.ProductGroup.Code;
                    groupName = menuDto.MenuItem.Category.ProductGroup.Name;
                }

                var delAggItemGroupDto = await GetOrCreateDelAggItemGroupId(new DelAggItemGroupEditDto
                { Name = groupName, TenantId = input.TenantId, Code = groupCode });
                if (delAggItemGroupDto.Id != null)
                {
                    delAggItemNewDto.DelAggItem.DelAggItemGroupId = delAggItemGroupDto.Id.Value;
                    delAggItemNewDto.DelAggItem.DelAggItemGroupName = delAggItemGroupDto.Name;
                }

                #endregion

                #region Item Category Setup

                var categoryCode = L("Food");
                var categoryName = L("Food");
                if (menuDto.MenuItem.CategoryId.HasValue)
                {
                    categoryCode = menuDto.MenuItem.CategoryCode;
                    categoryName = menuDto.MenuItem.CategoryName;
                }

                var delAggCategoryDto = await GetOrCreateDelAggCategoryId(new DelAggCategoryEditDto
                { Code = categoryCode, Name = categoryName, TenantId = input.TenantId });
                if (delAggCategoryDto.Id != null)
                {
                    delAggItemNewDto.DelAggItem.DelAggCatId = delAggCategoryDto.Id.Value;
                    delAggItemNewDto.DelAggItem.DelAggCatName = delAggCategoryDto.Name;
                }

                #endregion

                #region Tax Setup

                // Tax is Wrong

                ////var delAggTaxList = await _delaggtaxRepo.GetAllListAsync(t => t.TenantId == input.TenantId);
                ////var tax = delAggTaxList.FirstOrDefault();
                ////if (tax != null)
                ////{
                ////    delAggItemNewDto.DelAggItem.DelAggTaxId = tax.Id;
                ////    delAggItemNewDto.DelAggItem.DelAggTaxName = tax.Name;
                ////}

                #endregion

                #region DelAggVariantGroup - Setup

                delAggItemNewDto.DelAggVariantGroup.Name = menuDto.MenuItem.Id + "-" + menuDto.MenuItem.Name;
                delAggItemNewDto.DelAggVariantGroup.Code = menuDto.MenuItem.Id + "-" + menuDto.MenuItem.Name;
                delAggItemNewDto.DelAggVariantGroup.TenantId = input.TenantId;

                delAggVarGroupDto = await GetOrCreateDelAggVarientGroupId(new DelAggVariantGroupEditDto
                { Code = delAggItemNewDto.DelAggVariantGroup.Code, Name = delAggItemNewDto.DelAggVariantGroup.Name, TenantId = input.TenantId });
                if (delAggVarGroupDto.Id != null)
                {
                    delAggItemNewDto.DelAggItem.DelAggVariantGroupId = delAggVarGroupDto.Id.Value;
                    delAggItemNewDto.DelAggItem.DelAggVariantGroupName = delAggVarGroupDto.Name;
                }
                #endregion
            }


            #region DelAggVariant - Portions Create

            var delAggVariants = new List<DelAggVariantEditDto>();
            var sortOrder = 1;
            foreach (var lst in menuDto.MenuItem.Portions)
                if (lst.Id != null)
                {
                    var newDetailDto = new DelAggVariantEditDto
                    {
                        DelAggVariantGroupName = delAggItemNewDto.DelAggVariantGroup.Name,
                        MenuItemPortionId = lst.Id.Value,
                        Name = lst.Id.Value + "-" + lst.Name,
                        Code = lst.Id.Value + "-" + lst.Name,
                        SalesPrice = lst.Price,
                        MarkupPrice = lst.Price,
                        SortOrder = sortOrder++,
                        TenantId = input.TenantId
                    };
                    delAggVariants.Add(newDetailDto);
                }

            delAggItemNewDto.DelAggVariant = delAggVariants;
            delAggItemNewDto.DelAggItem.TenantId = input.TenantId;

            #endregion

            // Call CreateOrUpdate
            var newDto = new CreateOrUpdateDelAggItemInput
            {
                DelAggItem = delAggItemNewDto.DelAggItem,
                DelAggVariantGroup = delAggVarGroupDto,
                DelAggVariant = delAggVariants,
                DelAggImage = delAggItemNewDto.DelAggImage
            };

            var retId = await CreateOrUpdateDelAggItem(newDto);
            return new MessageOutputDto { Id = retId.Id, SuccessFlag = true };
        }

        protected virtual async Task<IdInput> UpdateDelAggItem(CreateOrUpdateDelAggItemInput input)
        {
            var tenantId = input.DelAggItem.TenantId;

            var existMenuItem = await _delaggitemRepo.FirstOrDefaultAsync(t => t.Id == input.DelAggItem.MenuItemId);
            if (existMenuItem != null && existMenuItem.Id != input.DelAggItem.Id)
                throw new UserFriendlyException("MenuItemAlreadyExists");

            #region DelAggItem Update
            var item = await _delaggitemRepo.GetAsync(input.DelAggItem.Id.Value);
            var dto = input.DelAggItem;
            item.Name = dto.Name;
            item.Code = dto.Code;
            item.LocalRefCode = dto.LocalRefCode;
            item.SortOrder = dto.SortOrder;
            item.Serves = dto.Serves;
            item.FoodType = dto.FoodType;
            item.DelAggCatId = dto.DelAggCatId;
            item.Weight = dto.Weight;
            item.DelAggItemGroupId = dto.DelAggItemGroupId;
            item.DelAggTaxId = dto.DelAggTaxId;
            item.Description = dto.Description;
            item.IsRecommended = dto.IsRecommended;
            CheckErrors(await _delAggItemManager.CreateSync(item));
            #endregion

            #region Update VariantGroupName

            var returnVarGrpId = 0;
            var existsVarGrp =
                    await _delaggvariantgroupRepo.FirstOrDefaultAsync(t => t.Id == input.DelAggVariantGroup.Id);
            if (existsVarGrp != null)
            {
                var varGrpNameExists = await _delaggvariantgroupRepo.FirstOrDefaultAsync(t =>
                        t.Name.Equals(input.DelAggVariantGroup.Name) && t.Id != input.DelAggVariantGroup.Id);
                if (varGrpNameExists == null)
                {
                    returnVarGrpId = existsVarGrp.Id;
                    existsVarGrp.Name = input.DelAggVariantGroup.Name;
                    await _delaggvariantgroupRepo.UpdateAsync(existsVarGrp);
                }
                else
                    throw new UserFriendlyException("VariantGroupNameAlreadyExists");
            }
            else
            {
                var variantGrpdto = input.DelAggVariantGroup.MapTo<DelAggVariantGroup>();
                returnVarGrpId = await _delaggvariantgroupRepo.InsertAndGetIdAsync(variantGrpdto);
            }

            #endregion
            #region Update Variants

            var toBeRetainedVariantItems = new List<int>();

            if (input.DelAggVariant != null && input.DelAggVariant.Count > 0)
                foreach (var lst in input.DelAggVariant)
                    if (lst.Id.HasValue && lst.Id.Value > 0)
                    {
                        var existsVariant = await _delaggvariantRepo.FirstOrDefaultAsync(t => t.Id == lst.Id.Value);
                        if (existsVariant != null)
                        {
                            existsVariant.Name = lst.Name;
                            existsVariant.DelAggVariantGroupId = returnVarGrpId;
                            existsVariant.MenuItemPortionId = lst.MenuItemPortionId;
                            existsVariant.SalesPrice = lst.SalesPrice;
                            existsVariant.MarkupPrice = lst.MarkupPrice;
                            await _delaggvariantRepo.UpdateAsync(existsVariant);
                            toBeRetainedVariantItems.Add(existsVariant.Id);
                        }
                    }
                    else
                    {
                        var varNameExists = _delaggvariantRepo.FirstOrDefaultAsync(t => t.Name.Equals(lst.Name));
                        if (varNameExists != null) throw new UserFriendlyException("VariantNameAlreadyExists");

                        var delAggVariantDto = new DelAggVariant
                        {
                            Name = lst.Name,
                            Code = lst.Name,
                            DelAggVariantGroupId = returnVarGrpId,
                            MenuItemPortionId = lst.MenuItemPortionId,
                            SalesPrice = lst.SalesPrice,
                            MarkupPrice = lst.MarkupPrice
                        };
                        await _delaggvariantRepo.InsertAsync(delAggVariantDto);
                        toBeRetainedVariantItems.Add(delAggVariantDto.Id);
                    }

            var toBeDeleted = await _delaggvariantRepo.GetAll()
                    .Where(
                        a => a.DelAggVariantGroupId == returnVarGrpId && !toBeRetainedVariantItems.Contains(a.Id))
                    .ToListAsync();

            foreach (var a in toBeDeleted) await _delaggvariantRepo.DeleteAsync(a.Id);

            #endregion
            #region DelAggImage Creation

            var toBeRetainedImageItems = new List<int>();
            if (input.DelAggImage != null && input.DelAggImage.Count > 0)
            {
                foreach (var lst in input.DelAggImage)
                {
                    var existsImage = await _delaggimageRepo.FirstOrDefaultAsync(t =>
                            t.DelAggImageTypeRefId == (int)DelAggImageType.Item &&
                            t.DelAggTypeRefId == lst.DelAggTypeRefId && t.Id == lst.Id);
                    if (existsImage == null)
                    {
                        var delAggImageDto = new DelAggImage
                        {
                            DelAggTypeRefId = lst.DelAggTypeRefId,
                            DelAggImageTypeRefId = (int)DelAggImageType.Item,
                            ImagePath = lst.ImagePath,
                            ReferenceId = dto.Id,
                            TenantId = tenantId
                        };
                        await _delaggimageRepo.InsertOrUpdateAndGetIdAsync(delAggImageDto);
                        toBeRetainedImageItems.Add(delAggImageDto.Id);
                    }
                    else
                        toBeRetainedImageItems.Add(existsImage.Id);
                }
            }
            var delImagesToBe = await _delaggimageRepo.GetAll().Where(t => t.ReferenceId == item.Id && !toBeRetainedImageItems.Contains(t.Id)).ToListAsync();
            foreach (var imgToBeDelete in delImagesToBe)
            {
                await _delaggimageRepo.DeleteAsync(imgToBeDelete.Id);
            }
            #endregion
            #region DelAggModifierGroup Items
            List<int> toBeRetainedDetailsIds = new List<int>();
            foreach (DelAggModifierGroupItemEditDto items in input.DelAggModifierGroupItem)
            {

                var alreadyExist = await _delAggModifierGroupItemRepo.FirstOrDefaultAsync(t => t.DelAggModifierGroupId == items.DelAggModifierGroupId && t.DelAggItemId == dto.Id);
                if (alreadyExist == null)
                {
                    DelAggModifierGroupItem tm = new DelAggModifierGroupItem();
                    tm.DelAggItemId = (int)dto.Id;
                    tm.DelAggModifierGroupId = items.Id.Value;
                    var ret2 = await _delAggModifierGroupItemRepo.InsertAsync(tm);
                    toBeRetainedDetailsIds.Add(tm.Id);
                }
                else
                {
                    toBeRetainedDetailsIds.Add(alreadyExist.Id);
                }
            }
            var detailsToBeDeleted = await _delAggModifierGroupItemRepo.GetAll().Where(t => t.DelAggItemId == item.Id && !toBeRetainedDetailsIds.Contains(t.Id)).ToListAsync();
            foreach (var delLst in detailsToBeDeleted)
            {
                await _delAggModifierGroupItemRepo.DeleteAsync(delLst.Id);
            }
            #endregion
            return new IdInput { Id = item.Id };

        }


        protected virtual async Task<IdInput> CreateDelAggItem(CreateOrUpdateDelAggItemInput input)
        {
            int tenantId = input.DelAggItem.TenantId;

            var dto = input.DelAggItem.MapTo<DelAggItem>();
            DelAggVariantGroup variantGroupDto;
            DelAggModifierGroupItem modifierGroupItemDto;

            var existMenuItem = await _delaggitemRepo.FirstOrDefaultAsync(t => t.Id == dto.MenuItemId);
            if (existMenuItem != null)
                throw new UserFriendlyException("MenuItemAlreadyExists");

            #region VariantGroup Create

            if (input.DelAggVariantGroup.Id == null)
            {
                var alreadyExistsVarGrp =
                        await _delaggvariantgroupRepo.FirstOrDefaultAsync(t =>
                            t.Name.Equals(input.DelAggVariantGroup.Name));
                if (alreadyExistsVarGrp != null) throw new UserFriendlyException("VariantGroupNameAlreadyExists");

                var tm = new DelAggVariantGroupEditDto
                {
                    Name = input.DelAggVariantGroup.Name,
                    Code = input.DelAggVariantGroup.Name,
                    SortOrder = input.DelAggVariantGroup.SortOrder,
                    LocalRefCode = input.DelAggVariantGroup.LocalRefCode
                };

                variantGroupDto = tm.MapTo<DelAggVariantGroup>();
                variantGroupDto.TenantId = tenantId;
                var retId = await _delaggvariantgroupRepo.InsertAndGetIdAsync(variantGroupDto);
                dto.DelAggVariantGroupId = retId;
            }

            #endregion

            #region Variants Create
            if (input.DelAggVariant != null)
            {
                foreach (var lst in input.DelAggVariant)
                {
                    if (lst.Id == 0)
                    {
                        var alreadyExistsVar =
                                await _delaggvariantRepo.FirstOrDefaultAsync(t => t.Name.Equals(lst.Name));
                        if (alreadyExistsVar != null) throw new UserFriendlyException("VariantNameAlreadyExists");
                    }
                    lst.DelAggVariantGroupId = dto.DelAggVariantGroupId;
                    lst.Code = lst.Code;
                    lst.Name = lst.Name;
                    var variantDto = lst.MapTo<DelAggVariant>();
                    await _delaggvariantRepo.InsertAndGetIdAsync(variantDto);
                }
            }

            //DelAggItem Create
            dto.TenantId = tenantId;
            var retItemId = await _delaggitemRepo.InsertAndGetIdAsync(dto);

            #endregion

            #region ModifierGroupItemCreate
            if (input.DelAggModifierGroupItem != null && input.DelAggModifierGroupItem.Count > 0)
            {
                foreach (DelAggModifierGroupItemEditDto items in input.DelAggModifierGroupItem)
                {
                    if (items.Id != null)
                    {
                        var alreadyExistsModGrpItem =
                                await _delAggModifierGroupItemRepo.FirstOrDefaultAsync(t =>
                                    t.DelAggModifierGroupId == items.Id && t.DelAggItemId == dto.Id);
                        if (alreadyExistsModGrpItem != null) throw new UserFriendlyException("MappingAlreadyExists");

                        var tm = new DelAggModifierGroupItemEditDto
                        {
                            DelAggItemId = dto.Id,
                            DelAggModifierGroupId = items.Id.Value
                        };

                        modifierGroupItemDto = tm.MapTo<DelAggModifierGroupItem>();
                        var retId = await _delAggModifierGroupItemRepo.InsertAndGetIdAsync(modifierGroupItemDto);
                    }
                }
            }
            #endregion
            return new IdInput { Id = dto.Id };
        }
        public async Task<DelAggVariantGroupEditDto> GetOrCreateDelAggVarientGroupId(DelAggVariantGroupEditDto input)
        {
            var existRecord =
                await _delaggvariantgroupRepo.FirstOrDefaultAsync(t => t.Name.ToUpper().Equals(input.Name.ToUpper()));
            if (existRecord == null)
            {
                var createDelAggVariantGroupDto = new CreateOrUpdateDelAggVariantGroupInput();

                createDelAggVariantGroupDto.DelAggVariantGroup = new DelAggVariantGroupEditDto();
                createDelAggVariantGroupDto.DelAggVariantGroup = input;
                await _delAggVariantGroupAppService.CreateOrUpdateDelAggVariantGroup(createDelAggVariantGroupDto);
                existRecord =
                    await _delaggvariantgroupRepo.FirstOrDefaultAsync(t => t.Name.ToUpper().Equals(input.Name.ToUpper()));
            }

            return existRecord.MapTo<DelAggVariantGroupEditDto>();
        }

        public async Task<DelAggItemGroupEditDto> GetOrCreateDelAggItemGroupId(DelAggItemGroupEditDto input)
        {
            var existRecord =
                await _delaggitemgroupRepo.FirstOrDefaultAsync(t => t.Name.ToUpper().Equals(input.Name.ToUpper()));
            if (existRecord == null)
            {
                var createDelAggItemGroupDto = new CreateOrUpdateDelAggItemGroupInput
                {
                    DelAggItemGroup = new DelAggItemGroupEditDto()
                };

                createDelAggItemGroupDto.DelAggItemGroup = input;
                await _delAggItemGroupAppService.CreateOrUpdateDelAggItemGroup(createDelAggItemGroupDto);
                existRecord =
                    await _delaggitemgroupRepo.FirstOrDefaultAsync(t => t.Name.ToUpper().Equals(input.Name.ToUpper()));
            }

            return existRecord.MapTo<DelAggItemGroupEditDto>();
        }

        public async Task<DelAggCategoryEditDto> GetOrCreateDelAggCategoryId(DelAggCategoryEditDto input)
        {
            var timingGroupName = L("General");
            var existCategroy =
                await _delaggcategoryRepo.FirstOrDefaultAsync(t => t.Code.ToUpper().Equals(input.Code.ToUpper()));
            if (existCategroy == null)
            {
                var delTimingGroupDto = await GetOrCreateDelAggTimingGroupId(new DelTimingGroupEditDto
                { Name = timingGroupName, TenantId = input.TenantId });

                var createDelAggCategoryDto = new CreateOrUpdateDelAggCategoryInput();
                createDelAggCategoryDto.DelAggCategory = new DelAggCategoryEditDto();
                input.DelTimingGroupId = delTimingGroupDto.Id;
                createDelAggCategoryDto.DelAggCategory = input;
                await _categoryAppService.CreateOrUpdateDelAggCategory(createDelAggCategoryDto);
                existCategroy =
                    await _delaggcategoryRepo.FirstOrDefaultAsync(t => t.Code.ToUpper().Equals(input.Code.ToUpper()));
            }

            return existCategroy.MapTo<DelAggCategoryEditDto>();
        }

        public async Task<IdInput> GetOrCreateDelAggTimingGroupId(DelTimingGroupEditDto input)
        {
            var existRecord =
                await _deltiminggroupRepo.FirstOrDefaultAsync(t => t.Name.ToUpper().Equals(input.Name.ToUpper()));
            if (existRecord == null)
            {
                var createDelTimingGroupDto = new CreateOrUpdateDelTimingGroupInput();

                createDelTimingGroupDto.DelTimingGroup = new DelTimingGroupEditDto();
                createDelTimingGroupDto.DelTimingGroup = input;
                await _delTimingGroupAppService.CreateOrUpdateDelTimingGroup(createDelTimingGroupDto);
                existRecord =
                    await _deltiminggroupRepo.FirstOrDefaultAsync(t => t.Name.ToUpper().Equals(input.Name.ToUpper()));
            }

            return new IdInput { Id = existRecord.Id };
        }
    }
}