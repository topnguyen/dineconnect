﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Connect.OrderTags;
using DinePlan.DineConnect.Dto;
using System.Linq;
using DinePlan.DineConnect.Cluster.Dtos;

namespace DinePlan.DineConnect.Cluster.Master.Implementation
{
    public class DelAggModifierAppService : DineConnectAppServiceBase, IDelAggModifierAppService
    {
        private readonly IDelAggModifierListExcelExporter _delaggmodifierExporter;
        private readonly IRepository<DelAggModifier> _delaggmodifierRepo;
        private readonly IRepository<DelAggModifierGroup> _delaggmodifiergroupRepo;
        private readonly IDelAggModifierManager _delAggModifierManager;
        private readonly IRepository<OrderTag> _ordertagRepo;
        private readonly IRepository<DelAggImage> _delaggimageRepo;
        public DelAggModifierAppService(IRepository<DelAggModifier> delaggmodifierRepo, IRepository<DelAggModifierGroup> delaggmodifiergroupRepo,
            IDelAggModifierListExcelExporter delaggmodifierExporter,
            IDelAggModifierManager delAggModifierManager,
            IRepository<OrderTag> ordertagRepo, IRepository<DelAggImage> delaggimageRepo)
        {
            _delaggmodifierRepo = delaggmodifierRepo;
            _delaggmodifiergroupRepo = delaggmodifiergroupRepo;
            _delaggmodifierExporter = delaggmodifierExporter;
            _delAggModifierManager = delAggModifierManager;
            _ordertagRepo = ordertagRepo;
            _delaggimageRepo = delaggimageRepo;
        }

        public async Task<PagedResultOutput<DelAggModifierListDto>> GetAll(GetDelAggModifierInput input)
        {
            var rsdelaggmodifier = _delaggmodifierRepo.GetAll();
            var rsdelaggmodifiergroup = _delaggmodifiergroupRepo.GetAll();
            var rsordertag = _ordertagRepo.GetAll();

            if (input.OrderTagId.HasValue)
            {
                rsdelaggmodifier = rsdelaggmodifier.Where(t => t.OrderTagId == input.OrderTagId.Value);
            }
            if (input.DelAggModifierGroupId.HasValue)
            {
                rsdelaggmodifier = rsdelaggmodifier.Where(t => t.DelAggModifierGroupId == input.DelAggModifierGroupId.Value);
            }
            var allItems = (from mg in rsdelaggmodifier
                            join otg in rsordertag
                            on mg.OrderTagId equals otg.Id
                            join dmg in rsdelaggmodifiergroup
                            on mg.DelAggModifierGroupId equals dmg.Id
                            select new DelAggModifierListDto()
                            {
                                Id = mg.Id,
                                OrderTagId = mg.OrderTagId,
                                DelAggModifierGroupId=mg.DelAggModifierGroupId,
                                Name = mg.Name,
                                Code=mg.Code,
                                Price=mg.Price,
                                CreationTime = mg.CreationTime
                            }).WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DelAggModifierListDto>>();

            var allItemCount = await allItems.CountAsync();

            foreach (var lst in allListDtos)
            {
                if (lst.OrderTagId != null)
                {
                    var lstOrderTag = _ordertagRepo.FirstOrDefault(t => t.Id == lst.OrderTagId);
                    if (lstOrderTag != null)
                        lst.OrderTagName = lstOrderTag.Name;
                }
                if (lst.DelAggModifierGroupId != null)
                {
                    var lstModiGrp = _delaggmodifiergroupRepo.FirstOrDefault(t => t.Id == lst.DelAggModifierGroupId);
                    if (lstModiGrp != null)
                        lst.DelAggModifierGroupName = lstModiGrp.Name;
                }
            }

            return new PagedResultOutput<DelAggModifierListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetDelAggModifierInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DelAggModifierListDto>>();
            return _delaggmodifierExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDelAggModifierForEditOutput> GetDelAggModifierForEdit(NullableIdInput input)
        {
            DelAggModifierEditDto editDto;
            List<DelAggImageEditDto> editImageDto = new List<DelAggImageEditDto>();

            if (input.Id.HasValue)
            {
                var hDto = await _delaggmodifierRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DelAggModifierEditDto>();

                #region Image Edit

                var imageItem = await _delaggimageRepo.GetAllListAsync(t => t.ReferenceId == editDto.Id && t.DelAggImageTypeRefId == (int)DelAggImageType.Modifier);

                if (imageItem != null && imageItem.Count > 0)
                    editImageDto = imageItem.MapTo<List<DelAggImageEditDto>>();
                else
                    editImageDto = new List<DelAggImageEditDto>();

                #endregion
            }
            else
            {
                editDto = new DelAggModifierEditDto();
                editImageDto = new List<DelAggImageEditDto>();
            }

            return new GetDelAggModifierForEditOutput
            {
                DelAggModifier = editDto,
                DelAggImage = editImageDto
            };
        }

        public async Task CreateOrUpdateDelAggModifier(CreateOrUpdateDelAggModifierInput input)
        {
            if (input.DelAggModifier.Id.HasValue)
            {
                //  In edit mode, the updated Name should not be Equal to existing Name
                var alreadyExist = _delaggmodifierRepo.FirstOrDefault(p => p.Name == input.DelAggModifier.Name && p.Id != input.DelAggModifier.Id);
                if (alreadyExist != null)
                {
                    throw new UserFriendlyException(L("NameAlreadyExists"));
                }
                await UpdateDelAggModifier(input);
            }
            else
            {
                var alreadyExist = _delaggmodifierRepo.FirstOrDefault(p => p.Name == input.DelAggModifier.Name);
                if (alreadyExist != null)
                {
                    throw new UserFriendlyException(L("NameAlreadyExists"));
                }
                await CreateDelAggModifier(input);
            }
        }

        public async Task DeleteDelAggModifier(IdInput input)
        {
                await _delaggmodifierRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateDelAggModifier(CreateOrUpdateDelAggModifierInput input)
        {
            var item = await _delaggmodifierRepo.GetAsync(input.DelAggModifier.Id.Value);
            var dto = input.DelAggModifier;
            //TODO: SERVICE DelAggModifier Update Individually
            item.Name = dto.Name;
            item.Code = dto.Code;
            item.SortOrder = dto.SortOrder;
            item.LocalRefCode = dto.LocalRefCode;
            item.DelAggModifierGroupId = dto.DelAggModifierGroupId;
            item.OrderTagId = dto.OrderTagId;
            item.NestedModGroupId = dto.NestedModGroupId;
            item.Price = dto.Price;


            #region DelAggImage Creation

            var toBeRetainedImageItems = new List<int>();
            if (input.DelAggImage != null && input.DelAggImage.Count > 0)
            {
                foreach (var lst in input.DelAggImage)
                {
                    var existsImage = await _delaggimageRepo.FirstOrDefaultAsync(t =>
                            t.DelAggImageTypeRefId == (int)DelAggImageType.Modifier &&
                            t.DelAggTypeRefId == lst.DelAggTypeRefId && t.Id == lst.Id);
                    if (existsImage == null)
                    {
                        var delAggImageDto = new DelAggImage
                        {
                            DelAggTypeRefId = lst.DelAggTypeRefId,
                            DelAggImageTypeRefId = (int)DelAggImageType.Modifier,
                            ImagePath = lst.ImagePath,
                            ReferenceId = dto.Id

                        };
                        await _delaggimageRepo.InsertOrUpdateAndGetIdAsync(delAggImageDto);
                        toBeRetainedImageItems.Add(delAggImageDto.Id);
                    }
                    else
                        toBeRetainedImageItems.Add(existsImage.Id);
                }

            }

            var delImagesToBe = await _delaggimageRepo.GetAll().Where(t => t.ReferenceId == item.Id && !toBeRetainedImageItems.Contains(t.Id)).ToListAsync();
            foreach (var imgToBeDelete in delImagesToBe)
            {
                await _delaggimageRepo.DeleteAsync(imgToBeDelete.Id);
            }

            #endregion
            CheckErrors(await _delAggModifierManager.CreateSync(item));
        }

        protected virtual async Task CreateDelAggModifier(CreateOrUpdateDelAggModifierInput input)
        {
            var dto = input.DelAggModifier.MapTo<DelAggModifier>();
            CheckErrors(await _delAggModifierManager.CreateSync(dto));
        }
        public async Task<ListResultOutput<DelAggModifierListDto>> GetNames()
        {
            var lstDelAggModifier = await _delaggmodifierRepo.GetAll().ToListAsync();
            return new ListResultOutput<DelAggModifierListDto>(lstDelAggModifier.MapTo<List<DelAggModifierListDto>>());
        }
    }
}

