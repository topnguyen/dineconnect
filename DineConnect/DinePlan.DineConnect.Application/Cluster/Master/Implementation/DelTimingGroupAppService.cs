﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Impl;
using System;
using Abp.UI;
using Castle.Core.Logging;
using AutoMapper;

namespace DinePlan.DineConnect.Cluster.Implementation
{
    public class DelTimingGroupAppService : DineConnectAppServiceBase, IDelTimingGroupAppService
    {
        private readonly IDelTimingGroupListExcelExporter _deltiminggroupExporter;
        private readonly IRepository<DelTimingGroup> _deltiminggroupRepo;
        private readonly IRepository<DelTimingDetail> _deltimingdetailRepo;
        private readonly IDelTimingGroupManager _deltiminggroupManager;
        private readonly ILogger _logger;

        public DelTimingGroupAppService(IRepository<DelTimingGroup> deltiminggroupRepo,
            IRepository<DelTimingDetail> deltimingdetailRepo,
            IDelTimingGroupManager deltiminggroupManager,
            IDelTimingGroupListExcelExporter deltiminggroupExporter,
            ILogger logger)
        {
            _deltiminggroupRepo = deltiminggroupRepo;
            _deltimingdetailRepo = deltimingdetailRepo;
            _deltiminggroupManager = deltiminggroupManager;
            _deltiminggroupExporter = deltiminggroupExporter;
            _logger = logger;
        }

        public async Task<PagedResultOutput<DelTimingGroupListDto>> GetAll(GetDelTimingGroupInput input)
        {
            var allItems = _deltiminggroupRepo
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DelTimingGroupListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<DelTimingGroupListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetDelTimingGroupInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DelTimingGroupListDto>>();
            return _deltiminggroupExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDelTimingGroupForEditOutput> GetDelTimingGroupForEdit(NullableIdInput input)
        {
            var output = new GetDelTimingGroupForEditOutput();

            var editDto = new DelTimingGroupEditDto();

            if (input.Id.HasValue)
            {
                var timingGroup = await _deltiminggroupRepo.GetAll()
                   .Include(e => e.DelTimingDetails)
                   .FirstOrDefaultAsync(e => e.Id == input.Id.Value);
                editDto = timingGroup.MapTo<DelTimingGroupEditDto>();
            }

            foreach (var edi in editDto.DelTimingDetails)
            {
                edi.AllDays = new List<ComboboxItemDto>();

                if (!string.IsNullOrEmpty(edi.Days))
                    foreach (var day in edi.Days.Split(","))
                    {
                        var myday = (DayOfWeek)Convert.ToInt32(day);
                        edi.AllDays.Add(new ComboboxItemDto
                        {
                            DisplayText = myday.ToString(),
                            Value = day
                        });
                    }
            }

            output.DelTimingGroup = editDto;
            return output;
        }

        public async Task CreateOrUpdateDelTimingGroup(CreateOrUpdateDelTimingGroupInput input)
        {
            if (input.DelTimingGroup.Id.HasValue)
            {
                await UpdateDelTimingGroup(input);
            }
            else
            {
                await CreateDelTimingGroup(input);
            }
        }

        public async Task DeleteDelTimingGroup(IdInput input)
        {
            await _deltimingdetailRepo.DeleteAsync(t => t.DelTimingGroupId == input.Id);
            await _deltiminggroupRepo.DeleteAsync(input.Id);
            
        }

        protected virtual async Task UpdateDelTimingGroup(CreateOrUpdateDelTimingGroupInput input)
        {
            var item = await _deltiminggroupRepo.GetAsync(input.DelTimingGroup.Id.Value);
            var dto = input.DelTimingGroup;
            //TODO: SERVICE DelTimingGroup Update Individually
            item.Name = dto.Name;
            item.Description = dto.Description;

            var upSchduleItems = new List<int>();
            if (dto?.DelTimingDetails != null && dto.DelTimingDetails.Any())
                foreach (var otdto in dto.DelTimingDetails)
                    if (otdto.Id != null && otdto.Id > 0)
                    {
                        upSchduleItems.Add(otdto.Id.Value);
                        var fromUpdation = item.DelTimingDetails.SingleOrDefault(a => a.Id.Equals(otdto.Id));
                        if (fromUpdation != null)
                        {
                            fromUpdation.DelTimingGroupId = item.Id;
                            UpdateDelTimingDetails(fromUpdation, otdto);
                        }
                    }
                    else
                    {
                        var obj = Mapper.Map < DelTimingDetailEditDto, DelTimingDetail>(otdto);
                        item.DelTimingDetails.Add(obj);
                    }

            var toRemoveSchdules =
                item.DelTimingDetails.Where(a => !a.Id.Equals(0) && !upSchduleItems.Contains(a.Id)).ToList();
            foreach (var schdule in toRemoveSchdules) await _deltimingdetailRepo.DeleteAsync(schdule.Id);

            CheckErrors(await _deltiminggroupManager.CreateSync(item));
        }

        protected virtual async Task<int> CreateDelTimingGroup(CreateOrUpdateDelTimingGroupInput input)
        {
            try
            {
                var timingGroup = input.DelTimingGroup.MapTo<DelTimingGroup>();
                var dto = input.DelTimingDetail;

                CheckErrors(await _deltiminggroupManager.CreateSync(timingGroup));
                var item = await _deltiminggroupRepo.InsertOrUpdateAndGetIdAsync(timingGroup);
                return item;
            }
            catch (Exception exception)
            {
                _logger.Error("CreateTimingGroup", exception);
                throw new UserFriendlyException(exception.Message);
            }
        }
        public async Task<ListResultOutput<DelTimingGroupListDto>> GetTimingGroupNames()
        {
            var lstTax = await _deltiminggroupRepo.GetAll().ToListAsync();
            return new ListResultOutput<DelTimingGroupListDto>(lstTax.MapTo<List<DelTimingGroupListDto>>());
        }

        private void UpdateDelTimingDetails(DelTimingDetail fromUpdation, DelTimingDetailEditDto otdto)
        {
            fromUpdation.StartMinute = otdto.StartMinute;
            fromUpdation.StartHour = otdto.StartHour;
            fromUpdation.EndMinute = otdto.EndMinute;
            fromUpdation.EndHour = otdto.EndHour;
            fromUpdation.Days = otdto.Days;
        }
    }
}
