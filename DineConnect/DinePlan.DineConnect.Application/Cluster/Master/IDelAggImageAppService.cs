﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master
{
    public interface IDelAggImageAppService : IApplicationService
    {
        Task<PagedResultOutput<DelAggImageListDto>> GetAll(GetDelAggImageInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggImageInput input);
        Task<GetDelAggImageForEditOutput> GetDelAggImageForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateDelAggImage(CreateOrUpdateDelAggImageInput input);
        Task DeleteDelAggImage(IdInput input);
    }
}