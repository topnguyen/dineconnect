﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggTaxAppService : IApplicationService
    {
        Task<PagedResultOutput<DelAggTaxListDto>> GetAll(GetDelAggTaxInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggTaxInput input);
        Task<GetDelAggTaxForEditOutput> GetDelAggTaxForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateDelAggTax(CreateOrUpdateDelAggTaxInput input);
        Task DeleteDelAggTax(IdInput input);
        Task<ListResultOutput<ComboboxItemDto>> GetDelAggTaxForCombobox();
        Task<ListResultOutput<ComboboxItemDto>> GetDelAggTaxTypeForCombobox();
    }
}
