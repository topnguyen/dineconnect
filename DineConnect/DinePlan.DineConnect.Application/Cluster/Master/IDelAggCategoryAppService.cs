﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master
{
    public interface IDelAggCategoryAppService : IApplicationService
    {
        Task<PagedResultOutput<DelAggCategoryListDto>> GetAll(GetDelAggCategoryInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggCategoryInput input);
        Task<GetDelAggCategoryForEditOutput> GetDelAggCategoryForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateDelAggCategory(CreateOrUpdateDelAggCategoryInput input);
        Task DeleteDelAggCategory(IdInput input);
        Task<ListResultOutput<DelAggCategoryListDto>> GetNames();
        Task<PagedResultOutput<DelAggCategoryListDto>> GetDelAggCategories(GetCategoryInput input);
        Task SaveSortDelAggCategories(int[] categories);
        Task<GetItemByCategoryOuput> GetItemForVisualScreen(GetCategoryInput input);
    }
}