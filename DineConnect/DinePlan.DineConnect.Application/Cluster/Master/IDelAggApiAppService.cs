﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Cluster.Master.Dtos;

namespace DinePlan.DineConnect.Cluster.Master
{
    public interface IDelAggApiAppService : IApplicationService
    {
        Task<GrabOutputApi> ApiForGrab(ApiDelAggRequest delAggRequest);

        Task<DeliveroApiOutput> ApiForDeliveroo(ApiDelAggRequest delAggRequest);

        Task<DeliveryHeroApiOutput> ApiForDeliveryHero(ApiDelAggRequest delAggRequest);

        Task<ZomatoAggApiOutput> ApiForZomato(ApiDelAggRequest delAggRequest);
    }
}
