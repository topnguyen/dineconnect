﻿using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master.Exporter
{
    public interface IDelAggItemListExcelExporter
    {
        FileDto ExportToFile(List<DelAggItemListDto> dtos);
    }
}
