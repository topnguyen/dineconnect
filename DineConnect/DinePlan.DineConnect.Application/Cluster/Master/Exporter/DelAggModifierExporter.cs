﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master.Exporter
{
    public class DelAggModifierListExcelExporter : FileExporterBase, IDelAggModifierListExcelExporter
    {
        public FileDto ExportToFile(List<DelAggModifierListDto> dtos)
        {
            return CreateExcelPackage(
                "DelAggModifierList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DelAggModifier"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name"),
                        L("Code"),
                        L("DelAggModifierGroup"),
                        L("OrderTag"),
                        L("LocalRefCode"),
                        L("Price")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Name,
                        _=>_.Code,
                        _=>_.DelAggModifierGroupName,
                        _=>_.OrderTagName,
                        _=>_.LocalRefCode,
                        _=>_.Price

                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}

