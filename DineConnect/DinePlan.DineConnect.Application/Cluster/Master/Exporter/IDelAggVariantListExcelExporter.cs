﻿using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master
{
    public interface IDelAggVariantListExcelExporter
    {
        FileDto ExportToFile(List<DelAggVariantListDto> dtos);
    }
}