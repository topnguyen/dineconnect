﻿using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master
{
    public interface IDelAggLocationGroupListExcelExporter
    {
        FileDto ExportToFile(List<DelAggLocationGroupListDto> dtos);
    }
}