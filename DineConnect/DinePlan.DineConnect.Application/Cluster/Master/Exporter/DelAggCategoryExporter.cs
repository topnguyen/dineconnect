﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Exporter
{
    public class DelAggCategoryListExcelExporter : FileExporterBase, IDelAggCategoryListExcelExporter
    {
        public FileDto ExportToFile(List<DelAggCategoryListDto> dtos)
        {
            return CreateExcelPackage(
                "DelAggCategoryList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DelAggCategory"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name"),
                        L("Code"),
                        L("DelTimingGroup")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _=>_.Name,
                        _=>_.Code,
                        _=>_.DelTimingGroupName

                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}

