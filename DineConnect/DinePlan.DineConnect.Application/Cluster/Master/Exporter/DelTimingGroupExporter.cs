﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Exporter
{
    public class DelTimingGroupListExcelExporter : FileExporterBase, IDelTimingGroupListExcelExporter
    {
        public FileDto ExportToFile(List<DelTimingGroupListDto> dtos)
        {
            return CreateExcelPackage(
                "DelTimingGroupList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DelTimingGroup"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Name
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
