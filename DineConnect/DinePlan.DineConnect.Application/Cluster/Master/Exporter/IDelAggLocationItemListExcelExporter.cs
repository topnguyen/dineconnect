﻿using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master.Exporter
{
    public interface IDelAggLocationItemListExcelExporter
    {
        FileDto ExportToFile(List<DelAggLocationItemListDto> dtos);
    }
}
