﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master.Exporter
{
    public class DelAggItemListExcelExporter : FileExporterBase, IDelAggItemListExcelExporter
    {
        public FileDto ExportToFile(List<DelAggItemListDto> dtos)
        {
            return CreateExcelPackage(
                "DelAggItemList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DelAggItem"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("GroupId"),
                        L("GroupName"),
                        L("CategoryId"),
                        L("CategoryName"),
                        L("ItemCode"),
                        L("ItemName")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.DelAggItemGroupId,
                        _ => _.DelAggItemGroupName,
                        _ => _.DelAggCatId,
                        _ => _.DelAggCatName,
                        _ => _.Code,
                        _ => _.Name
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
