﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Localization;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Localization.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Cluster.Dtos;

namespace DinePlan.DineConnect.Cluster.Implementation
{
    public interface IDelAggLanguageAppService : IApplicationService
    {
        Task<PagedResultOutput<DelAggLanguageListDto>> GetAll(GetDelAggLanguageInput inputDto);
        Task<GetDelAggLanguageForEditOutput> GetDelAggLanguageForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateDelAggLanguage(CreateOrUpdateDelAggLanguageInput input);
        Task DeleteDelAggLanguage(IdInput input);

        Task<ListResultDto<ComboboxItemDto>> GetLanguageDescriptionTypes();



    }
}