﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master
{
    public interface IDelAggVariantAppService : IApplicationService
    {
        Task<PagedResultOutput<DelAggVariantListDto>> GetAll(GetDelAggVariantInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggVariantInput input);
        Task<GetDelAggVariantForEditOutput> GetDelAggVariantForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateDelAggVariant(CreateOrUpdateDelAggVariantInput input);
        Task DeleteDelAggVariant(IdInput input);
        Task<ListResultOutput<DelAggVariantListDto>> GetNames();
    }
}