﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master
{
    public interface IDelAggModifierGroupAppService : IApplicationService
    {
        Task<PagedResultOutput<DelAggModifierGroupListDto>> GetAll(GetDelAggModifierGroupInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggModifierGroupInput input);
        Task<GetDelAggModifierGroupForEditOutput> GetDelAggModifierGroupForEdit(NullableIdInput nullableIdInput);
        Task<int> CreateOrUpdateDelAggModifierGroup(CreateOrUpdateDelAggModifierGroupInput input);
        Task DeleteDelAggModifierGroup(IdInput input);
        Task<ListResultOutput<DelAggModifierGroupListDto>> GetNames();
    }
}