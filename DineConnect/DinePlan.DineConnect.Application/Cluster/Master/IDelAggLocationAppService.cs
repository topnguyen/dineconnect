﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master
{
    public interface IDelAggLocationAppService : IApplicationService
    {
        Task<PagedResultOutput<DelAggLocationListDto>> GetAll(GetDelAggLocationInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetDelAggLocationForEditOutput> GetDelAggLocationForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateDelAggLocation(CreateOrUpdateDelAggLocationInput input);
        Task DeleteDelAggLocation(IdInput input);
        Task<ListResultOutput<DelAggLocationListDto>> GetNames();
    }
}