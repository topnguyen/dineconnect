﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master.Dtos
{
    [AutoMapFrom(typeof(DelAggLocMapping))]
    public class DelAggLocMappingListDto : FullAuditedEntityDto
    {
        //TODO: DTO DelAggLocMapping Properties Missing
        public int Id { get; set; }
        public virtual int DelAggLocationId { get; set; }
        public virtual int DelAggTypeRefId { get; set; }
        public virtual string DelAggLocationName { get; set; }
        public virtual string DelAggTypeRefName { get; set; }
        public string GatewayCode { get; set; }
        public string RemoteCode { get; set; }
        public string DeliveryUrl { get; set; }
        public bool Active { get; set; }
        public string AddOns { get; set; }
        public string Name { get; set; }
        public virtual int TenantId { get; set; }
    }
    [AutoMapTo(typeof(DelAggLocMapping))]
    public class DelAggLocMappingEditDto
    {
        public DelAggLocMappingEditDto()
        {
            Active = true;
        }
        public int? Id { get; set; }
        //TODO: DTO DelAggLocMapping Properties Missing
        public virtual int DelAggLocationId { get; set; }
        public virtual DelAggType DelAggTypeRefId { get; set; }
        public virtual string DelAggLocationName { get; set; }
        public virtual string DelAggTypeRefName { get; set; }
        public string GatewayCode { get; set; }
        public string RemoteCode { get; set; }
        public string DeliveryUrl { get; set; }
        public bool Active { get; set; }
        public string AddOns { get; set; }
        public string Name { get; set; }
        public virtual int TenantId { get; set; }
    }

    public class GetDelAggLocMappingInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public virtual int? DelAggTypeRefId { get; set; }
        public virtual int? DelAggLocationId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class GetDelAggLocMappingForEditOutput : IOutputDto
    {
        public DelAggLocMappingEditDto DelAggLocMapping { get; set; }
    }
    public class CreateOrUpdateDelAggLocMappingInput : IInputDto
    {
        [Required]
        public DelAggLocMappingEditDto DelAggLocMapping { get; set; }
    }
}
