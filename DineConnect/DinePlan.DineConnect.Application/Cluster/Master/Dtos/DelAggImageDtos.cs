﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
namespace DinePlan.DineConnect.Cluster.Dtos
{
    [AutoMapFrom(typeof(DelAggImage))]
    public class DelAggImageListDto : FullAuditedEntityDto
    {
        //TODO: DTO DelAggImage Properties Missing
        public int Id { get; set; }
        public virtual int DelAggImageTypeRefId { get; set; }
        public virtual string DelAggImageTypeRefName { get; set; }
        public virtual string DelAggTypeRefName { get; set; }
        public virtual int? DelAggTypeRefId { get; set; }
        public int? ReferenceId { get; set; }
        public string ImagePath { get; set; }
        public string AddOns { get; set; }
    }
    [AutoMapTo(typeof(DelAggImage))]
    public class DelAggImageEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO DelAggImage Properties Missing
        public virtual int DelAggImageTypeRefId { get; set; }
        public virtual string DelAggImageTypeRefName { get; set; }
        public virtual string DelAggTypeRefName { get; set; }
        public virtual int? DelAggTypeRefId { get; set; }
        public int? ReferenceId { get; set; }
        public string ImagePath { get; set; }
        public string AddOns { get; set; }
    }

    public class GetDelAggImageInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public virtual int? DelAggTypeRefId { get; set; }
        public virtual int? DelAggImageTypeRefId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetDelAggImageForEditOutput : IOutputDto
    {
        public DelAggImageEditDto DelAggImage { get; set; }
    }
    public class CreateOrUpdateDelAggImageInput : IInputDto
    {
        [Required]
        public DelAggImageEditDto DelAggImage { get; set; }
    }
}

