﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;
namespace DinePlan.DineConnect.Cluster.Master.Dtos
{
    [AutoMapFrom(typeof(DelAggVariantGroup))]
    public class DelAggVariantGroupListDto : FullAuditedEntityDto
    {
        //TODO: DTO DelAggVariantGroup Properties Missing
        public int Id { get; set; }
        public  string Name { get; set; }
        public virtual string Code { get; set; }
        public int SortOrder { get; set; }
        public string LocalRefCode { get; set; }
    }
    [AutoMapTo(typeof(DelAggVariantGroup))]
    public class DelAggVariantGroupEditDto
    {
        public virtual string Code { get; set; }
        public int? Id { get; set; }
        //TODO: DTO DelAggVariantGroup Properties Missing
        public virtual string Name { get; set; }
        public int SortOrder { get; set; }
        public string LocalRefCode { get; set; }
        public int TenantId { get; set; }
    }

    public class GetDelAggVariantGroupInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class GetDelAggVariantGroupForEditOutput : IOutputDto
    {
        public DelAggVariantGroupEditDto DelAggVariantGroup { get; set; }
        public List<DelAggImageEditDto> DelAggImage { get; set; }
    }
    public class CreateOrUpdateDelAggVariantGroupInput : IInputDto
    {
        [Required]
        public DelAggVariantGroupEditDto DelAggVariantGroup { get; set; }
        public List<DelAggImageEditDto> DelAggImage { get; set; }
    }
}