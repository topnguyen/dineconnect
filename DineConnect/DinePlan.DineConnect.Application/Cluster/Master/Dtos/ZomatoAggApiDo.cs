﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Cluster.Master.Dtos
{
    public class ZomatoAggApiOutput
    {
        [JsonProperty("outletId")]
        public string OutletId { get; set; }
        [JsonProperty("charges")]
        public List<ChargesZomato> Charges { get; set; }
        [JsonProperty("menu")]
        public MenuZomato Menu { get; set; }
    }
    public class ChargesZomato
    {
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("vendorEntityId")]
        public string VendorEntityId { get; set; }
        [JsonProperty("multiItem")]
        public bool MultiItem { get; set; }
        [JsonProperty("chargeValue")]
        public decimal ChargeValue { get; set; }
        [JsonProperty("applicableOnItem")]
        public bool ApplicableOnItem { get; set; }
        [JsonProperty("taxGroups")]
        public List<TaxGroupsZomato> TaxGroups { get; set; }
    }
    public class TaxGroupsZomato
    {
        [JsonProperty("slug")]
        public string Slug { get; set; }
    }
    public class MenuZomato
    {
        [JsonProperty("categories")]
        public List<CategoriesZomato> Categories { get; set; }
        [JsonProperty("catalogues")]
        public List<CataloguesZomato> Catalogues { get; set; }
        [JsonProperty("combos")]
        public List<CombosZomato> Combos { get; set; }
        [JsonProperty("modifierGroups")]
        public List<ModifierGroupsZomato> ModifierGroups { get; set; }
    }
    public class ModifierGroupsZomato
    {
        [JsonProperty("vendorEntityId")]
        public string VendorEntityId { get; set; }
        [JsonProperty("displayName")]
        public string DisplayName { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("kind")]
        public string Kind { get; set; }
        [JsonProperty("max")]
        public int? Max { get; set; }
        [JsonProperty("min")]
        public int? Min { get; set; }
        [JsonProperty("variants")]
        public List<VariantModifierGroup> Variants { get; set; }

    }
    public class VariantModifierGroup
    {
        [JsonProperty("vendorEntityId")]
        public string VendorEntityId { get; set; }
    }
    public class CategoriesZomato
    {
        [JsonProperty("vendorEntityId")]
        public string VendorEntityId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("timings")]
        public List<TimingZomato> Timings { get; set; }
        [JsonProperty("subCategories")]
        public List<SubCategories> SubCategories { get; set; }
    }
    public class SubCategories
    {
        [JsonProperty("vendorEntityId")]
        public string VendorEntityId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("entities")]
        public List<EntityZomato> Entities { get; set; }
    }
    public class EntityZomato
    {
        [JsonProperty("entityType")]
        public string EntityType { get; set; }
        [JsonProperty("vendorEntityId")]
        public string VendorEntityId { get; set; }
    }
    public class TimingZomato
    {
        [JsonProperty("service")]
        public string Service { get; set; }
        [JsonProperty("time1From")]
        public string Time1From { get; set; }
        [JsonProperty("time1To")]
        public string Time1To { get; set; }
        [JsonProperty("time2From")]
        public string Time2From { get; set; }
        [JsonProperty("time2To")]
        public string Time2To { get; set; }
        [JsonProperty("time3From")]
        public string Time3From { get; set; }
        [JsonProperty("time3To")]
        public string Time3To { get; set; }
        [JsonProperty("dayMonday")]
        public bool Monday { get; set; }
        [JsonProperty("dayTuesday")]
        public bool Tuesday { get; set; }
        [JsonProperty("dayWednesday")]
        public bool Wednesday { get; set; }
        [JsonProperty("dayThursday")]
        public bool Thursday { get; set; }
        [JsonProperty("vendorFriday")]
        public bool Friday { get; set; }
        [JsonProperty("daySaturday")]
        public bool Saturday { get; set; }
        [JsonProperty("daySunday")]
        public bool Sunday { get; set; }
        [JsonProperty("startDate")]
        public string StartDate { get; set; }
        [JsonProperty("endDate")]
        public string EndDate { get; set; }
    }
    public class CataloguesZomato
    {
        [JsonProperty("vendorEntityId")]
        public string VendorEntityId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get;set; }
        [JsonProperty("kind")]
        public string Kind { get; set; }
        [JsonProperty("inStock")]
        public bool InStock { get; set; } = true;
        [JsonProperty("nutritionInfo")]
        public List<NutritionInfoCatalogues> NutritionInfo { get; set; }
        [JsonProperty("preparationTime")]
        public string PreparationTime { get; set; }
        [JsonProperty("portionSize")]
        public List<PortionSizeCatalogues> PortionSize { get; set; }
        [JsonProperty("properties")]
        public List<PropertiesCatalogues> Properties { get; set; }
        [JsonProperty("variants")]
        public List<VariantsCatalogues> Variants { get; set; }
        [JsonProperty("tags")]
        public List<string> Tags { get; set; }
        [JsonProperty("meatTypes")]
        public List<string> MeatTypes { get; set; }
        [JsonProperty("imageUrl")]
        public string ImageUrl { get; set; }
        [JsonProperty("taxGroups")]
        public List<TaxGroupsCatalogues> TaxGroups { get; set; }
        [JsonProperty("charges")]
        public List<ChargeCatalogues> Charges { get; set; }

    }
  
    public class ChargeCatalogues
    {
        [JsonProperty("vendorEntityId")]
        public string VendorEntityId { get; set; }
    }
    public class TaxGroupsCatalogues
    {
        [JsonProperty("slug")]
        public string Slug { get; set; }
    }
    public class VariantsCatalogues
    {
        public VariantsCatalogues()
        {
            ModifierGroups = new List<ModifierGroupsVariant>();
        }
        [JsonProperty("vendorEntityId")]
        public string VendorEntityId { get; set; }
        [JsonProperty("inStock")]
        public bool InStock { get; set; } = true;
        [JsonProperty("propertyValues")]
        public List<PropertyVariants> PropertyValues { get; set; }
        [JsonProperty("prices")]
        public List<PriceVariants> Prices { get; set; }
        [JsonProperty("modifierGroups")]
        public List<ModifierGroupsVariant> ModifierGroups{ get; set; }
    }
    public class ModifierGroupsVariant
    {
        [JsonProperty("vendorEntityId")]
        public string VendorEntityId { get; set; }
    }
    public class PriceVariants
    {
        [JsonProperty("service")]
        public string Service { get; set; }
        [JsonProperty("price")]
        public decimal Price { get; set; }
    }
    public class PropertyVariants
    {
        [JsonProperty("vendorEntityId")]
        public string VendorEntityId { get; set; }
    }
    public class PropertiesCatalogues
    {
        public PropertiesCatalogues()
        {
            PropertyValues = new List<PropertyValuesZomato>();
        }
        [JsonProperty("vendorEntityId")]
        public string VendorEntityId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("kind")]
        public string Kind { get; set; }
        [JsonProperty("propertyValues")]
        public List<PropertyValuesZomato> PropertyValues { get; set; }

    }
    public class PropertyValuesZomato
    {
        [JsonProperty("vendorEntityId")]
        public string VendorEntityId { get; set; }
        [JsonProperty("kind")]
        public string Kind { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
        [JsonProperty("unit")]
        public string Unit { get; set; }
    }
    public class PortionSizeCatalogues
    {
        [JsonProperty("value")]
        public string Value { get; set; }
        [JsonProperty("unit")]
        public string Unit { get; set; }
    }
    public class NutritionInfoCatalogues
    {
        [JsonProperty("calorieCount")]
        public decimal CalorieCount { get; set; }
    }
    public class CombosZomato
    {
        [JsonProperty("vendorEntityId")]
        public string VendorEntityId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("reducedPrice")]
        public decimal ReducePrice { get; set; }
        [JsonProperty("taxGroups")]
        public List<TaxGroupCombo> TaxGroups { get; set; }
        [JsonProperty("charges")]
        public List<ChargeCombo> Charges { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("subtitle")]
        public string Subtitle { get; set; }
        [JsonProperty("inStock")]
        public bool InStock { get; set; }
        [JsonProperty("media")]
        public List<MediaCombos> Media { get; set; }
        [JsonProperty("selections")]
        public List<SelectionCombo> Selections { get; set; }
        [JsonProperty("services")]
        public ServiceCombo Services { get; set; }
    }
    public class ServiceCombo
    {
        [JsonProperty("service")]
        public List<string> Service { get; set; }
    }
    public class SelectionCombo
    {
        [JsonProperty("vendorEntityId")]
        public string VendorEntityId { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("minSelections")]
        public int  MinSelection  { get; set; }
        [JsonProperty("maxSelections")]
        public int MaxSelection { get; set; }
        [JsonProperty("maxSelectionsPerItem")]
        public int MaxSelectionsPerItem { get; set; }
        [JsonProperty("discountValue")]
        public decimal DiscountValue { get; set; }
        [JsonProperty("selectionEntities")]
        public List<SelectionEntity> SelectionEntities { get; set; }

    }
    public class SelectionEntity
    {
        [JsonProperty("variantVendorEntityId")]
        public string VariantVendorEntityId { get; set; }
        [JsonProperty("catalogueVendorEntityId")]
        public string CatalogueVendorEntityId { get; set; }
    }
    public class MediaCombos
    {
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("usageType")]
        public string UsageType { get; set; }
    }
    public class ChargeCombo
    {
        [JsonProperty("vendorEntityId")]
        public string VendorEntityId { get; set; }
    }
    public class TaxGroupCombo
    {
        [JsonProperty("slug")]
        public string Slug { get; set; }
    }
    public class ListTimingDto
    {
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public string DayOfWeek { get; set; }
    }
}
