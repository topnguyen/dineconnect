﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.MultiLingual;
using DinePlan.DineConnect.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Cluster.Dtos
{
    [AutoMapFrom(typeof(DelAggLanguage))]
    public class DelAggLanguageListDto : FullAuditedEntityDto
    {
        public virtual int DelAggTypeRefId { get; set; }
        public virtual string DelAggTypeRefName { get; set; }
        public string Language { get; set; }
        public string LanguageValue { get; set; }
        public int? ReferenceId { get; set; }
        public string AddOns { get; set; }
        public DelLanguageDescriptionType LanguageDescriptionType { get; set; }
    }
    [AutoMapTo(typeof(DelAggLanguage))]
    public class DelAggLanguageEditDto
    {
        public int? Id { get; set; }
        public virtual int DelAggTypeRefId { get; set; }
        public virtual string DelAggTypeRefName { get; set; }
        public string Language { get; set; }
        public string LanguageValue { get; set; }
        public int? ReferenceId { get; set; }
        public string AddOns { get; set; }
        public DelLanguageDescriptionType LanguageDescriptionType { get; set; }
    }

    public class GetDelAggLanguageInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public int Id { get; set; }
        public string Operation { get; set; }
        public DelLanguageDescriptionType LanguageDescriptionType { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id Desc";
            }
        }
    }
    public class GetDelAggLanguageForEditOutput : IOutputDto
    {
        public DelAggLanguageEditDto DelAggLanguage { get; set; }
    }
    public class CreateOrUpdateDelAggLanguageInput : IInputDto
    {
        [Required]
        public DelAggLanguageEditDto DelAggLanguage { get; set; }
    }
}