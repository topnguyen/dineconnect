﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;
namespace DinePlan.DineConnect.Cluster.Master.Dtos
{
    [AutoMapFrom(typeof(DelAggModifierGroup))]
    public class DelAggModifierGroupListDto : FullAuditedEntityDto
    {
        //TODO: DTO DelAggModifierGroup Properties Missing
        public int Id { get; set; }
        public  string Name { get; set; }
        public  string Code { get; set; }
        public int SortOrder { get; set; }
        public string LocalRefCode { get; set; }
        public int? Min { get; set; }
        public int? Max { get; set; }
        public int? OrderTagGroupId { get; set; }
        public string OrderTagGroupName { get; set; }
    }
    [AutoMapTo(typeof(DelAggModifierGroup))]
    public class DelAggModifierGroupEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO DelAggModifierGroup Properties Missing
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public int SortOrder { get; set; }
        public string LocalRefCode { get; set; }
        public int? Min { get; set; }
        public int? Max { get; set; }
        public int? OrderTagGroupId { get; set; }
        public string OrderTagGroupName { get; set; }
    }


    [AutoMapTo(typeof(DelAggModifierGroupItem))]
    public class DelAggModifierGroupItemEditDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public int DelAggModifierGroupId { get; set; }
        public string DelAggModifierGroupName { get; set; }
        public string Name { get; set; }
        public int DelAggItemId { get; set; }
        public string DelAggItemName { get; set; }
    }

    public class GetDelAggModifierGroupInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public int? OrderTagGroupId { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class GetDelAggModifierGroupForEditOutput : IOutputDto
    {
        public DelAggModifierGroupEditDto DelAggModifierGroup { get; set; }
        public List<DelAggModifierGroupItemEditDto> DelAggModifierGroupItem { get; set; }
        public List<DelAggImageEditDto> DelAggImage { get; set; }
    }
    public class CreateOrUpdateDelAggModifierGroupInput : IInputDto
    {
        public CreateOrUpdateDelAggModifierGroupInput()
        {
            DelAggModifierGroupItems = new List<DelAggModifierGroupItemEditDto>();
        }
        [Required]
        public DelAggModifierGroupEditDto DelAggModifierGroup { get; set; }
        public List<DelAggImageEditDto> DelAggImage { get; set; }

        public List<DelAggModifierGroupItemEditDto> DelAggModifierGroupItems { get; set; }
    }
}