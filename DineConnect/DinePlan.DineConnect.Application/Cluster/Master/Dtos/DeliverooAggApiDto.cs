﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Cluster.Master.Dtos
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Name
    {
        [JsonProperty("en")]
        public string En { get; set; }
    }

    public class Description
    {
        [JsonProperty("en")]
        public string En { get; set; }
    }

    public class SeoDescription
    {
        [JsonProperty("en")]
        public string En { get; set; }
    }

    public class Image
    {
        [JsonProperty("url")]
        public string Url { get; set; }
    }

    public class Mealtime
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public Name Name { get; set; }

        [JsonProperty("description")]
        public Description Description { get; set; }

        [JsonProperty("seo_description")]
        public SeoDescription SeoDescription { get; set; }

        [JsonProperty("image")]
        public Image Image { get; set; }

        [JsonProperty("category_ids")]
        public List<string> CategoryIds { get; set; }
    }

    public class CategoryDeliveroo
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public Name Name { get; set; }

        [JsonProperty("description")]
        public Description Description { get; set; }

        [JsonProperty("item_ids")]
        public List<string> ItemIds { get; set; }
    }

    public class Override
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("price")]
        public int Price { get; set; }
    }

    public class PriceInfo
    {
        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("overrides")]
        public List<Override> Overrides { get; set; }
    }

    public class EnergyKcal
    {
        [JsonProperty("low")]
        public int Low { get; set; }

        [JsonProperty("high")]
        public int High { get; set; }
    }

    public class NutritionalInfo
    {
        [JsonProperty("energy_kcal")]
        public EnergyKcal EnergyKcal { get; set; }
    }

    public class ItemDeliveroo
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public Name Name { get; set; }

        [JsonProperty("description")]
        public Description Description { get; set; }

        [JsonProperty("price_info")]
        public PriceInfo PriceInfo { get; set; }

        [JsonProperty("plu")]
        public string Plu { get; set; }

        [JsonProperty("ian")]
        public string Ian { get; set; }

        [JsonProperty("image")]
        public Image Image { get; set; }

        [JsonProperty("tax_rate")]
        public decimal TaxRate { get; set; }

        [JsonProperty("modifier_ids")]
        public List<string> ModifierIds { get; set; }

        [JsonProperty("allergies")]
        public List<string> Allergies { get; set; }

        [JsonProperty("nutritional_info")]
        public NutritionalInfo NutritionalInfo { get; set; }

        [JsonProperty("contains_alcohol")]
        public bool ContainsAlcohol { get; set; }

        [JsonProperty("operational_name")]
        public string OperationalName { get; set; }
    }

    public class ModifierDeliveroo
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public Name Name { get; set; }

        [JsonProperty("description")]
        public Description Description { get; set; }

        [JsonProperty("min_selection")]
        public int? MinSelection { get; set; }

        [JsonProperty("max_selection")]
        public int? MaxSelection { get; set; }

        [JsonProperty("repeatable")]
        public bool Repeatable { get; set; }

        [JsonProperty("item_ids")]
        public List<string> ItemIds { get; set; }
    }

    public class Menu
    {
        [JsonProperty("mealtimes")]
        public List<Mealtime> Mealtimes { get; set; }

        [JsonProperty("categories")]
        public List<CategoryDeliveroo> Categories { get; set; }

        [JsonProperty("items")]
        public List<ItemDeliveroo> Items { get; set; }

        [JsonProperty("modifiers")]
        public List<ModifierDeliveroo> Modifiers { get; set; }
    }

    public class DeliveroApiOutput
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("menu")]
        public Menu Menu { get; set; }

        [JsonProperty("site_ids")]
        public List<string> SiteIds { get; set; }
    }


}
