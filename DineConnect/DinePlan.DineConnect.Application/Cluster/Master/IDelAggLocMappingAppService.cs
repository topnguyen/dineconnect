﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master
{
    public interface IDelAggLocMappingAppService : IApplicationService
    {
        Task<PagedResultOutput<DelAggLocMappingListDto>> GetAll(GetDelAggLocMappingInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggLocMappingInput input);
        Task<GetDelAggLocMappingForEditOutput> GetDelAggLocMappingForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateDelAggLocMapping(CreateOrUpdateDelAggLocMappingInput input);
        Task DeleteDelAggLocMapping(IdInput input);
        Task<ListResultOutput<ComboboxItemDto>> GetDelAggTypeForCombobox();
        Task<ListResultOutput<ComboboxItemDto>> GetDelAggImageTypeForCombobox();
        Task<ListResultOutput<ComboboxItemDto>> GetDelPriceTypeForCombobox();
        Task<ListResultOutput<DelAggLocationListDto>> GetAggLocationNames();
        Task<ListResultOutput<DelAggLocMappingListDto>> GetAggLocationMapNames();
    }
}
