﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master
{
    public interface IDelAggVariantGroupAppService : IApplicationService
    {
        Task<PagedResultOutput<DelAggVariantGroupListDto>> GetAll(GetDelAggVariantGroupInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetDelAggVariantGroupForEditOutput> GetDelAggVariantGroupForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateDelAggVariantGroup(CreateOrUpdateDelAggVariantGroupInput input);
        Task DeleteDelAggVariantGroup(IdInput input);
        Task<ListResultOutput<DelAggVariantGroupListDto>> GetNames();
    }
}