﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster.Master
{
    public interface IDelAggModifierAppService : IApplicationService
    {
        Task<PagedResultOutput<DelAggModifierListDto>> GetAll(GetDelAggModifierInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelAggModifierInput input);
        Task<GetDelAggModifierForEditOutput> GetDelAggModifierForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateDelAggModifier(CreateOrUpdateDelAggModifierInput input);
        Task DeleteDelAggModifier(IdInput input);
        Task<ListResultOutput<DelAggModifierListDto>> GetNames();
    }
}