﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cluster.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelTimingGroupAppService : IApplicationService
    {
        Task<PagedResultOutput<DelTimingGroupListDto>> GetAll(GetDelTimingGroupInput inputDto);
        Task<FileDto> GetAllToExcel(GetDelTimingGroupInput input);
        Task<GetDelTimingGroupForEditOutput> GetDelTimingGroupForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateDelTimingGroup(CreateOrUpdateDelTimingGroupInput input);
        Task DeleteDelTimingGroup(IdInput input);
        Task<ListResultOutput<DelTimingGroupListDto>> GetTimingGroupNames();
    }
}