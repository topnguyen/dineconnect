using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Localization.Dto
{
    public class LanguageTextListDto : FullAuditedEntityDto
    {
        public string Key { get; set; }

        public string BaseValue { get; set; }

        public string TargetValue { get; set; }
    }
}