﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Localization;
using DinePlan.DineConnect.Connect.Language;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Localization.Dto
{
    [AutoMapTo(typeof(DinePlanLanguageText), typeof(ApplicationLanguageText))]
    public class UpdateLanguageTextInput : EntityDto<long?>, IInputDto
    {
        [Required]
        [StringLength(ApplicationLanguage.MaxNameLength)]
        public string LanguageName { get; set; }

        [Required]
        [StringLength(ApplicationLanguageText.MaxSourceNameLength)]
        public string SourceName { get; set; }

        [Required]
        [StringLength(ApplicationLanguageText.MaxKeyLength)]
        public string Key { get; set; }

        [Required(AllowEmptyStrings = true)]
        [StringLength(ApplicationLanguageText.MaxValueLength)]
        public string Value { get; set; }
    }
}