﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Language;

namespace DinePlan.DineConnect.Localization.Dto
{
    [AutoMapFrom(typeof(DinePlanLanguageText))]
    public class DinePlanLanguageTextListDto : FullAuditedEntityDto
    {
        public long? Id { get; set; }

        public string LanguageName { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }
    }
}