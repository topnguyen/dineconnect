﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Localization;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Localization.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.Localization
{
    public interface ILanguageAppService : IApplicationService
    {
        Task<GetLanguagesOutput> GetLanguages();

        Task<GetLanguageForEditOutput> GetLanguageForEdit(NullableIdInput input);

        Task CreateOrUpdateLanguage(CreateOrUpdateLanguageInput input);

        Task DeleteLanguage(IdInput input);

        Task SetDefaultLanguage(SetDefaultLanguageInput input);

        Task<PagedResultOutput<LanguageTextListDto>> GetLanguageTexts(GetLanguageTextsInput input);

        Task<FileDto> GetDineConnectLanguageTextsForExcel(GetLanguageTextsInput input);

        Task UpdateLanguageText(UpdateLanguageTextInput input);

        Task<PagedResultOutput<DinePlanLanguageTextListDto>> GetDinePlanLanguageTexts(GetLanguageTextsInput input);

        Task<UpdateLanguageTextInput> GetDinePlanLanguageTextForEdit(NullableIdInput input);

        Task UpdateDinePlanLanguageText(UpdateLanguageTextInput input);

        Task<FileDto> GetDinePlanLanguageTextsForExcel(GetLanguageTextsInput input);

        Task ImportDineConnectLanguage(List<ApplicationLanguageText> languageTexts);

        Task<GetLanguagesOutput> ApiGetLanguages(ApiLocationInput input);
        Task<PagedResultOutput<DinePlanLanguageTextListDto>> ApiGetLanguageTexts(GetLanguageTextsInput input);




    }
}