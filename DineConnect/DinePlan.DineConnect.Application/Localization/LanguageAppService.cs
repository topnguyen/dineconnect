﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Localization;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Language;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Localization.Dto;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Sync;

namespace DinePlan.DineConnect.Localization
{
    [AbpAuthorize(AppPermissions.Pages_Administration_Languages)]
    public class LanguageAppService : DineConnectAppServiceBase, ILanguageAppService
    {
        private readonly IApplicationLanguageManager _applicationLanguageManager;
        private readonly IApplicationLanguageTextManager _applicationLanguageTextManager;
        private readonly IRepository<ApplicationLanguage> _languageRepository;
        private readonly IRepository<DinePlanLanguageText, long> _dinePlanLanguageTextRepository;
        private readonly IExcelExporter _exporter;
        private readonly IRepository<ApplicationLanguageText, long> _languageTextRepository;
        private readonly ISyncAppService _syncAppService;

        public LanguageAppService(
            IApplicationLanguageManager applicationLanguageManager,
            IApplicationLanguageTextManager applicationLanguageTextManager,
            IRepository<ApplicationLanguage> languageRepository,
            IRepository<DinePlanLanguageText, long> dinePlanLanguageTextRepository,
            IExcelExporter exporter,ISyncAppService syncAppService,
            IRepository<ApplicationLanguageText, long> languageTextRepository)
        {
            _applicationLanguageManager = applicationLanguageManager;
            _languageRepository = languageRepository;
            _dinePlanLanguageTextRepository = dinePlanLanguageTextRepository;
            _exporter = exporter;
            _languageTextRepository = languageTextRepository;
            _applicationLanguageTextManager = applicationLanguageTextManager;
            _syncAppService = syncAppService;
        }

        public async Task<GetLanguagesOutput> GetLanguages()
        {
            var languages = (await _applicationLanguageManager.GetLanguagesAsync(AbpSession.TenantId)).OrderBy(l => l.DisplayName);
            var defaultLanguage = await _applicationLanguageManager.GetDefaultLanguageOrNullAsync(AbpSession.TenantId);

            return new GetLanguagesOutput(
                languages.MapTo<List<ApplicationLanguageListDto>>(),
                defaultLanguage == null ? null : defaultLanguage.Name
                );
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Languages_Create, AppPermissions.Pages_Administration_Languages_Edit)]
        public async Task<GetLanguageForEditOutput> GetLanguageForEdit(NullableIdInput input)
        {
            ApplicationLanguage language = null;
            if (input.Id.HasValue)
            {
                language = await _languageRepository.GetAsync(input.Id.Value);
            }

            var output = new GetLanguageForEditOutput();

            //Language
            output.Language = language != null
                ? language.MapTo<ApplicationLanguageEditDto>()
                : new ApplicationLanguageEditDto();

            //Language names
            output.LanguageNames = CultureInfo
                .GetCultures(CultureTypes.AllCultures)
                .OrderBy(c => c.DisplayName)
                .Select(c => new ComboboxItemDto(c.Name, c.DisplayName + " (" + c.Name + ")") { IsSelected = output.Language.Name == c.Name })
                .ToList();

            //Flags
            output.Flags = FamFamFamFlagsHelper
                .FlagClassNames
                .OrderBy(f => f)
                .Select(f => new ComboboxItemDto(f, FamFamFamFlagsHelper.GetCountryCode(f)) { IsSelected = output.Language.Icon == f })
                .ToList();

            return output;
        }

        public async Task CreateOrUpdateLanguage(CreateOrUpdateLanguageInput input)
        {
            if (input.Language.Id.HasValue)
            {
                await UpdateLanguageAsync(input);
            }
            else
            {
                await CreateLanguageAsync(input);
            }
        }

        public async Task DeleteLanguage(IdInput input)
        {
            var language = await _languageRepository.GetAsync(input.Id);
            await _applicationLanguageManager.RemoveAsync(AbpSession.TenantId, language.Name);
        }

        public async Task SetDefaultLanguage(SetDefaultLanguageInput input)
        {
            await _applicationLanguageManager.SetDefaultLanguageAsync(
                AbpSession.TenantId,
                GetCultureInfoByChecking(input.Name).Name
                );
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Languages_ChangeTexts)]
        public async Task<PagedResultOutput<LanguageTextListDto>> GetLanguageTexts(GetLanguageTextsInput input)
        {
            /* Note: This method is used by SPA without paging, MPA with paging.
             * So, it can both usable with paging or not */

            //Normalize base language name
            if (input.BaseLanguageName.IsNullOrEmpty())
            {
                var defaultLanguage = await _applicationLanguageManager.GetDefaultLanguageOrNullAsync(AbpSession.TenantId);
                if (defaultLanguage == null)
                {
                    defaultLanguage = (await _applicationLanguageManager.GetLanguagesAsync(AbpSession.TenantId)).FirstOrDefault();
                    if (defaultLanguage == null)
                    {
                        throw new ApplicationException("No language found in the application!");
                    }
                }

                input.BaseLanguageName = defaultLanguage.Name;
            }

            var source = LocalizationManager.GetSource(input.SourceName);
            var baseCulture = CultureInfo.GetCultureInfo(input.BaseLanguageName);
            var targetCulture = CultureInfo.GetCultureInfo(input.TargetLanguageName);

            var languageTexts = source
                .GetAllStrings()
                .Select(localizedString => new LanguageTextListDto
                {
                    Key = localizedString.Name,
                    BaseValue = _applicationLanguageTextManager.GetStringOrNull(AbpSession.TenantId, source.Name, baseCulture, localizedString.Name),
                    TargetValue = _applicationLanguageTextManager.GetStringOrNull(AbpSession.TenantId, source.Name, targetCulture, localizedString.Name, false)
                })
                .AsQueryable();

            //Filters
            if (input.TargetValueFilter == "EMPTY")
            {
                languageTexts = languageTexts.Where(s => s.TargetValue.IsNullOrEmpty());
            }

            if (!input.FilterText.IsNullOrEmpty())
            {
                languageTexts = languageTexts.Where(
                    l => (l.Key != null && l.Key.IndexOf(input.FilterText, StringComparison.CurrentCultureIgnoreCase) >= 0) ||
                         (l.BaseValue != null && l.BaseValue.IndexOf(input.FilterText, StringComparison.CurrentCultureIgnoreCase) >= 0) ||
                         (l.TargetValue != null && l.TargetValue.IndexOf(input.FilterText, StringComparison.CurrentCultureIgnoreCase) >= 0)
                    );
            }

            var totalCount = languageTexts.Count();

            //Ordering
            if (!input.Sorting.IsNullOrEmpty())
            {
                languageTexts = languageTexts.OrderBy(input.Sorting);
            }

            //Paging
            if (input.SkipCount > 0)
            {
                languageTexts = languageTexts.Skip(input.SkipCount);
            }

            if (input.MaxResultCount > 0)
            {
                languageTexts = languageTexts.Take(input.MaxResultCount);
            }

            return new PagedResultOutput<LanguageTextListDto>(
                totalCount,
                languageTexts.ToList()
                );
        }

        public async Task<FileDto> GetDineConnectLanguageTextsForExcel(GetLanguageTextsInput input)
        {
            var languageTexts = await GetLanguageTexts(input);

            var baseE = new BaseExportObject
            {
                ExportObject = languageTexts.Items,
                ColumnNames = new[] { "Key", "BaseValue", "TargetValue" }
            };
            return _exporter.ExportToFile(baseE, L("DineConnectLanguageTexts"));
        }

        public async Task UpdateLanguageText(UpdateLanguageTextInput input)
        {
            var culture = GetCultureInfoByChecking(input.LanguageName);
            var source = LocalizationManager.GetSource(input.SourceName);
            await _applicationLanguageTextManager.UpdateStringAsync(AbpSession.TenantId, source.Name, culture, input.Key, input.Value);
        }

        public async Task<PagedResultOutput<DinePlanLanguageTextListDto>> GetDinePlanLanguageTexts(GetLanguageTextsInput input)
        {
            var query = _dinePlanLanguageTextRepository.GetAll()
                .WhereIf(!input.FilterText.IsNullOrEmpty(), l => l.Key.Contains(input.FilterText) || l.Value.Contains(input.FilterText))
                .Where(l => l.LanguageName == input.TargetLanguageName);

            var totalCount = await query.CountAsync();

            var languageTexts = await query
                .OrderBy(input.Sorting ?? "Key")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultOutput<DinePlanLanguageTextListDto>(
                totalCount,
                languageTexts.MapTo<List<DinePlanLanguageTextListDto>>()
                );
        }

        public async Task<UpdateLanguageTextInput> GetDinePlanLanguageTextForEdit(NullableIdInput input)
        {
            var dto = new UpdateLanguageTextInput();

            if (input.Id.HasValue)
            {
                var languageText = await _dinePlanLanguageTextRepository.FirstOrDefaultAsync(l => l.Id == input.Id);

                languageText.MapTo(dto);
            }
            return dto;
        }

        public async Task<FileDto> GetDinePlanLanguageTextsForExcel(GetLanguageTextsInput input)
        {
            var languageTexts = await GetLanguageTexts(input);

            var baseE = new BaseExportObject
            {
                ExportObject = languageTexts.Items,
                ColumnNames = new[] { "Key", "TargetValue" }
            };
            return _exporter.ExportToFile(baseE, L("DineConnectLanguageTemplate"));
        }

        public async Task UpdateDinePlanLanguageText(UpdateLanguageTextInput input)
        {
            var exists = _dinePlanLanguageTextRepository.GetAll()
                .Where(l => l.LanguageName == input.LanguageName)
                .Where(l => l.Key == input.Key)
                .WhereIf(input.Id.HasValue, l => l.Id != input.Id);

            if (exists.Any())
            {
                throw new UserFriendlyException(L("ThisLanguageTextAlreadyExists"));
            }

            var languageText = new DinePlanLanguageText();
            if (input.Id.HasValue)
            {
                languageText = await _dinePlanLanguageTextRepository.FirstOrDefaultAsync(l => l.Id == input.Id);
            }
            input.MapTo(languageText);

            languageText.TenantId = AbpSession.TenantId;

            await _dinePlanLanguageTextRepository.InsertOrUpdateAsync(languageText);
            await _syncAppService.UpdateSync(SyncConsts.LOCALIZATION);
        }

        public async Task ImportDineConnectLanguage(List<ApplicationLanguageText> languageTexts)
        {
            foreach (var text in languageTexts)
            {
                text.TenantId = AbpSession.TenantId;
                await _languageTextRepository.InsertOrUpdateAsync(text);
            }
        }

        public async Task<GetLanguagesOutput> ApiGetLanguages(ApiLocationInput input)
        {
            var languages = (await _applicationLanguageManager.GetLanguagesAsync(input.TenantId)).OrderBy(l => l.DisplayName);
            var defaultLanguage = await _applicationLanguageManager.GetDefaultLanguageOrNullAsync(input.TenantId);

            return new GetLanguagesOutput(
                languages.MapTo<List<ApplicationLanguageListDto>>(),
                defaultLanguage == null ? null : defaultLanguage.Name
            );
        }

       

        public async  Task<PagedResultOutput<DinePlanLanguageTextListDto>> ApiGetLanguageTexts(GetLanguageTextsInput input)
        {
            return await GetDinePlanLanguageTexts(input);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Languages_Create)]
        protected virtual async Task CreateLanguageAsync(CreateOrUpdateLanguageInput input)
        {
            var culture = GetCultureInfoByChecking(input.Language.Name);

            await CheckLanguageIfAlreadyExists(culture.Name);

            await _applicationLanguageManager.AddAsync(
                new ApplicationLanguage(
                    AbpSession.TenantId,
                    culture.Name,
                    culture.DisplayName,
                    input.Language.Icon
                    )
                );
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Languages_Edit)]
        protected virtual async Task UpdateLanguageAsync(CreateOrUpdateLanguageInput input)
        {
            Debug.Assert(input.Language.Id != null, "input.Language.Id != null");

            var culture = GetCultureInfoByChecking(input.Language.Name);

            await CheckLanguageIfAlreadyExists(culture.Name, input.Language.Id.Value);

            var language = await _languageRepository.GetAsync(input.Language.Id.Value);

            language.Name = culture.Name;
            language.DisplayName = culture.DisplayName;
            language.Icon = input.Language.Icon;

            await _applicationLanguageManager.UpdateAsync(AbpSession.TenantId, language);
        }

        private CultureInfo GetCultureInfoByChecking(string name)
        {
            try
            {
                return CultureInfo.GetCultureInfo(name);
            }
            catch (CultureNotFoundException ex)
            {
                Logger.Warn(ex.ToString(), ex);
                throw new UserFriendlyException(L("InvlalidLanguageCode"));
            }
        }

        private async Task CheckLanguageIfAlreadyExists(string languageName, int? expectedId = null)
        {
            var existingLanguage = (await _applicationLanguageManager.GetLanguagesAsync(AbpSession.TenantId))
                .FirstOrDefault(l => l.Name == languageName);

            if (existingLanguage == null)
            {
                return;
            }

            if (expectedId != null && existingLanguage.Id == expectedId.Value)
            {
                return;
            }

            throw new UserFriendlyException(L("ThisLanguageAlreadyExists"));
        }
    }
}