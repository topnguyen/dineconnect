﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Wheel.Dto;

namespace DinePlan.DineConnect.Wheel
{
    public interface ICallService
    {
         Task ReceiveCall(CallInput message);
    }
}
