﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.RealTime;
using DinePlan.DineConnect.Wheel.Dto;

namespace DinePlan.DineConnect.Wheel.Impl
{
    public class CallService : DineConnectAppServiceBase, ICallService
    {
        private IOnlineClientManager _onlineManager;

        public CallService(IOnlineClientManager onlineClientManager)
        {
            _onlineManager = onlineClientManager;
        }
        public async Task ReceiveCall(CallInput message)
        {
            var user = _onlineManager.GetByUserIdOrNull(message.Caller);
            if (user != null)
            {
                var t = "Test";
            }
            return;
        }
    }
}
