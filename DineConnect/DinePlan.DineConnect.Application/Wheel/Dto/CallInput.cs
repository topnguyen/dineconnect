﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Wheel.Dto
{
    public class CallInput : IInputDto
    {
        public long Caller { get; set; }
        public string PhoneNumber { get; set; }
    }
}
