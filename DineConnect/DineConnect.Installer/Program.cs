﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.Web.Administration;

namespace DineConnect.Installer
{
    internal class Program
    {
        private static readonly string ParentSitePath = ConfigurationManager.AppSettings["ParentSitePath"];
        private static readonly string DownloadPath = ConfigurationManager.AppSettings["DownloadPath"];
        private static readonly bool Migrate = Convert.ToBoolean(ConfigurationManager.AppSettings["Migrate"]);
        private static readonly string NullSitesC = ConfigurationManager.AppSettings["NullSites"];
        private static readonly string MigrateSitesC = ConfigurationManager.AppSettings["MigrateSites"];

        private static  string[] NullSites = null;
        private static  string[] MigrateSites = null;


        private static bool GotConfig;

        public static List<string> allSites = new List<string>();

        private static void Main(string[] args)
        {
            Console.WriteLine("************ Starting DineConnect Installer **********");
            if (!string.IsNullOrEmpty(NullSitesC))
            {
                NullSites = NullSitesC.Split(',');
            }

            if (!string.IsNullOrEmpty(MigrateSitesC))
            {
                MigrateSites = MigrateSitesC.Split(',');
            }
            while (true)
            {
                Console.WriteLine("****** Steps *****");
                Console.WriteLine("1. Deploy : (D)");
                Console.WriteLine("2. Create Site : (C)");
                Console.WriteLine("3. Exit : (X)");
                Console.Write("Your Choice : ");

                var readK = Console.ReadLine();

                if (string.IsNullOrEmpty(readK))
                {
                    Console.WriteLine("************ Exiting **********");
                    break;
                }

                if (readK.Trim().ToUpper().Equals("D")) Deploy();

                if (readK.Trim().ToUpper().Equals("C")) CreateSite();

                if (readK.Trim().ToUpper().Equals("X"))
                {
                    Console.WriteLine("************ Exiting **********");
                    break;
                }
            }
        }

        private static void CreateSite()
        {
            Console.WriteLine("************ Creating Site **********");
            Console.Write("Site Name : ");
            var readK = Console.ReadLine();

            if (string.IsNullOrEmpty(readK))
                return;
            
            ServerManager server = new ServerManager();
            ApplicationPool myApplicationPool = null;
            if (!string.IsNullOrEmpty(readK))
            {
                if (server.ApplicationPools != null && server.ApplicationPools.Count > 0)
                {
                    var myAppPool = server.ApplicationPools.LastOrDefault(a => a.Name.Equals(readK));
                    if (myAppPool == null)
                    {
                        myApplicationPool = server.ApplicationPools.Add(readK);
                    }
                }
 
                if (myApplicationPool != null)
                {
                    myApplicationPool.ProcessModel.IdentityType = ProcessModelIdentityType.NetworkService;
                    myApplicationPool.ManagedRuntimeVersion = "v4.0";
                    server.CommitChanges();
                }
            }

          
            Console.WriteLine("Application Pool Created");

            var finalPath = Path.Combine(ParentSitePath, readK);

            if (myApplicationPool!=null && server.Sites != null && server.Sites.Count > 0)
            {

                var mySite = server.Sites.FirstOrDefault(a => a.Name.Equals(readK));
                if(mySite==null)
                {
                    string ip = "*";
                    string port = "80";
                    string hostName = readK.ToLower()+".dineconnect.net";
 
                    string bindingInfo = $@"{ip}:{port}:{hostName}";
 
                    Site site = server.Sites.Add(readK, "http", bindingInfo, finalPath);
                    site.ApplicationDefaults.ApplicationPoolName = myApplicationPool.Name;
                    server.CommitChanges();
                }
            }
            Console.WriteLine("Application Created");
            if (!Directory.Exists(finalPath))
            {
                Directory.CreateDirectory(finalPath);
            }
            if(Directory.Exists(finalPath))
                JustCopySite(finalPath);

        }

        private static void Deploy()
        {
            var server = new ServerManager();
            var sites = server.Sites;
            foreach (var site in sites)
            {
                var applications = site.Applications;
                foreach (var application in applications)
                {
                    if (NullSites != null && NullSites.Any())
                    {
                        if (NullSites.Contains(application.ApplicationPoolName))
                        {
                            continue;
                        }
                    }

                    if (MigrateSites != null && MigrateSites.Any())
                    {
                        if (!MigrateSites.Contains(application.ApplicationPoolName))
                        {
                            continue;
                        }
                    }
                    
                    var directories = application.VirtualDirectories;
                    foreach (var directory in directories)
                    {
                        var physicalPath = directory.PhysicalPath;
                        allSites.Add(physicalPath);
                    }
                }
            }

            if (allSites.Any())
            {
                var failedSites = new List<string>();
                var efailedSites = new List<string>();

                foreach (var site in allSites)
                {
                    GotConfig = false;

                    var exe = ExecuteSite(site);
                    if (!string.IsNullOrEmpty(exe)) failedSites.Add(site);
                }

                if (failedSites.Any())
                    foreach (var site in failedSites)
                    {
                        var exe = ExecuteSite(site);
                        if (!string.IsNullOrEmpty(exe)) efailedSites.Add(site);
                    }

                if (efailedSites.Any())
                {
                    Console.WriteLine("*********FAILED SITE *************");

                    foreach (var site in efailedSites) Console.WriteLine(site);
                    Console.WriteLine("*********END FAILED SITE *************");
                }
            }
        }
        private static string JustCopySite(string site)
        {
            try
            {
                var dPath = site;
                Console.WriteLine();
                Console.WriteLine("----------------");
                Console.WriteLine("Starting on  : " + site);
                Console.WriteLine("Deleting : " + site);
                DeleteFilesFromDirectory(site, "Web_Deploy.config");
                Console.WriteLine("Copying : " + site);
                DirectoryCopy(DownloadPath, dPath, true);
                Console.WriteLine("Ending   : " + site);
                Console.WriteLine("--------------------------");
                Console.WriteLine();


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return site;
            }

            return "";
        }

        private static string ExecuteSite(string site)
        {
            try
            {
                var dPath = site;
                Console.WriteLine();
                Console.WriteLine("----------------");
                Console.WriteLine("Starting on  : " + site);
                Console.WriteLine("Deleting : " + site);
                DeleteFilesFromDirectory(site, "Web_Deploy.config");
                Console.WriteLine("Copying : " + site);
                DirectoryCopy(DownloadPath, dPath, true);
                if (Migrate)
                {
                    Console.WriteLine("Executing Migrate : " + site);
                    Console.WriteLine();
                    ExecuteCommand(dPath, "Execute.bat");
                }

                Console.WriteLine("Ending   : " + site);
                Console.WriteLine("--------------------------");
                Console.WriteLine();


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return site;
            }

            return "";
        }

        private static void DeleteFilesFromDirectory(string directoryPath, string exceptionFile)
        {
            var d = new DirectoryInfo(directoryPath);
            foreach (var fi in d.GetFiles())
            {
                var name = fi.Name;
                if (GotConfig)
                    fi.Delete();
                else if (name.ToUpper().Equals(exceptionFile.ToUpper()))
                    GotConfig = true;
                else
                    fi.Delete();
            }

            foreach (var di in d.GetDirectories())
                if (!di.Name.Equals("_Tickets"))
                {
                    DeleteFilesFromDirectory(di.FullName, exceptionFile);
                    di.Delete();
                }
        }

        public static void DirectoryCopy(string sourceDirPath, string destDirName, bool isCopySubDirs)
        {
            // Get the subdirectories for the specified directory.
            var directoryInfo = new DirectoryInfo(sourceDirPath);
            var directories = directoryInfo.GetDirectories();
            if (!directoryInfo.Exists)
                throw new DirectoryNotFoundException("Source directory does not exist or could not be found: "
                                                     + sourceDirPath);
            var parentDirectory = Directory.GetParent(directoryInfo.FullName);
            destDirName = Path.Combine(parentDirectory.FullName, destDirName);

            // If the destination directory doesn't exist, create it. 
            if (!Directory.Exists(destDirName)) Directory.CreateDirectory(destDirName);
            // Get the files in the directory and copy them to the new location.
            var files = directoryInfo.GetFiles();

            foreach (var file in files)
            {
                var tempPath = Path.Combine(destDirName, file.Name);

                if (File.Exists(tempPath)) File.Delete(tempPath);

                file.CopyTo(tempPath, false);
            }

            // If copying subdirectories, copy them and their contents to new location using recursive  function. 
            if (isCopySubDirs)
                foreach (var item in directories)
                {
                    var tempPath = Path.Combine(destDirName, item.Name);
                    DirectoryCopy(item.FullName, tempPath, isCopySubDirs);
                }
        }

        public static void ExecuteCommand(string directory, string command)
        {
            var startInfo = new ProcessStartInfo
            {
                WorkingDirectory = directory,
                WindowStyle = ProcessWindowStyle.Normal,
                FileName = "cmd.exe",
                Arguments = "/c " + command,
                RedirectStandardInput = true,
                UseShellExecute = false
            };
            var process = Process.Start(startInfo);
            process?.WaitForExit();
            process?.Close();
        }
    }
}