﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFBIFoodItConsole.Process.Executor
{
   public class OutboundConsts
    {
        public const string DateFormat = "yyyy-MM-dd";
        public const string DateTimeFormat = "yyyy-MM-dd hh:mm:ss";
        public const string DateTimeFileFormat = "yyyyMMddhhmmss";
       
        public const string DecimalFormat = "{0:0.00}";

        public const string Currency = "THB";



        public const string Refund = "Refund";
        public const string Return = "Return";

        public const string BlueCardNo = "Blue Card No";

        public const string EmployeeId = "PttEmployeeID";
        public const string EmployeeName = "PttEmployeeName";
        public const string EmployeeDepartment = "PttEmployeeDept";
        public const string PttEmployeeCostCenter = "PttEmployeeCostCenter";

        
        public const string CustomerCode = "CustomerCode";
        public const string S4HANASaleOrderCode = "S4HANASaleOrderCode";
        public const string S4HanaOrder = "S4HanaOrder";




        


    }
}
