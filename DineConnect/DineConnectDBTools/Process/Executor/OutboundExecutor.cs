﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using Newtonsoft.Json;
using PttDatabaseCommon.Data;
using SFBIFoodItConsole;
using SFBIFoodItConsole.Process.Model;
using SFBIFoodItConsole.Process.Utility;

namespace DineConnectDBTools.Process.Executor
{
    public class OutboundExecutor
    {
        public static string Run(AppConfig config, IUnitOfWork uow, PttInboundDbContext dbContext)
        {
            try
            {
                var myBuilderReason = new StringBuilder();

                var morPromListBuilder = new StringBuilder();

                var allTickets = uow.Tickets
                    .FindQuery(a => a.TenantId == config.TenantId
                                    && a.TotalAmount == 0 && a.TicketPromotionDetails.Length> 5)
                    .OrderByDescending(a => a.Id);

                foreach (var allTicket in allTickets)
                {
                    if (allTicket != null && allTicket.GetTicketPromotionList().Any() && allTicket.GetTicketPromotionList().Count > 1)
                    {
                        var myTicket = allTicket.Id;
                        morPromListBuilder.Append(allTicket.Id+","+allTicket.LastPaymentTimeTruc);
                        morPromListBuilder.Append(Environment.NewLine);
                    }

                    if (allTicket != null && allTicket.Transactions!=null && allTicket.Transactions.Any())
                    {
                        var allFourTrans = allTicket.Transactions.Where(a => a.TransactionTypeId == 4);

                        var allTransTypes4 = allFourTrans
                            .Sum(a => a.Amount);

                        var myAmount = allTicket.GetTicketPromotionList().Sum(a => a.PromotionAmount);
                       
                        if (allTransTypes4!=myAmount)
                        {
                            var myInsertStatement =
                                $"update TicketTransactions set Amount = {myAmount} where TicketId = {allTicket.Id} and TransactionTypeId = 4 ;";

                            myBuilderReason.Append(myInsertStatement);
                            myBuilderReason.Append(Environment.NewLine);
                        }

                        if (allFourTrans.Any())
                        {

                            var myInsertStatement =
                                $"update TicketTransactions set Amount = {myAmount} where TicketId = {allTicket.Id} and TransactionTypeId = 4 ;";

                            myBuilderReason.Append(myInsertStatement);
                            myBuilderReason.Append(Environment.NewLine);
                        }
                    }
                }

                File.WriteAllText(config.BackupFolder+"\\Files.txt",myBuilderReason.ToString());
                File.WriteAllText(config.BackupFolder+"\\FilesExtra.txt",morPromListBuilder.ToString());

            }
            catch (Exception exception)
            {
                return exception.Message;
            }


            return "";
        }
    }
}