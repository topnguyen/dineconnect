﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFBIFoodItConsole.Process.Model
{
    public class TicketOutputDto
    {
        public int Number { get; set; }
        public string Name { get; set; }

        public decimal AmountOfMoney { get; set; }

        public int Points { get; set; }

        

        public int NoOfTimes { get; set; }

        public int NoOfSheets { get; set; }

        public int NoOfSets { get; set; }

        public decimal NoOfGuests { get; set; }

        private string amount;
        public string AmountOfMoneyStr
        {
            get
            {
                return amount;
            }
            set { amount = value; }
        }

        public string PointStr { get; set; }
    }

    public class DepartmentGroupDto
    {
        public string DepartmentName { get; set; }

        public decimal AmountOfMoneyExcludedTax { get; set; }

        public decimal AmountOfMoneyIncludedTax { get; set; }

        public int NoOfSets { get; set; }

        public decimal NoOfGuests { get; set; }

        public decimal Discount { get; set; }
        public decimal Round { get; set; }
        public decimal PlainSum { get; set; }
        public decimal TaxTotal { get; set; }
        public decimal SubTotal { get; set; }
        public decimal ActualTax { get; set; }
        public decimal NetSales { get; set; }
        public decimal NetTax { get; set; }
        public decimal NetDiscount { get; set; }
        public decimal NetRound { get; set; }
    }

    public class PromotionDto
    {
        public string Name { get; set; }

        public decimal Amount { get; set; }

        public int TicketId { get; set; }
    }

    public class VoucherDto
    {
        public string Name { get; set; }

        public decimal Amount { get; set; }

        public int TicketId { get; set; }
    }

    public class PaymentTypeDto
    {
        public string Name { get; set; }

        public decimal Amount { get; set; }

        public int TicketId { get; set; }
    }
}
