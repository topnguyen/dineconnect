namespace SFBIFoodItConsole.Process.Utility
{

    public class SettingsModel
    {
        public ConnectorCategory CategoryData { get; set; }
        public ConnectorItem ItemData { get; set; }
        public ConnectorLocation LocationData{ get; set; }

        public class ConnectorCategory
        {
            public string BaseElementPath { get; set; }
            public NodeDefinition Organization { get; set; }
            public NodeDefinition Code { get; set; }
            public NodeDefinition Name { get; set; }
            public NodeDefinition Hierarchy { get; set; }

        }
        public class ConnectorItem
        {
            public string BaseElementPath { get; set; }
            public NodeDefinition Organization { get; set; }
            public NodeDefinition Category { get; set; }
            public NodeDefinition Code { get; set; }
            public NodeDefinition Name { get; set; }
            public NodeDefinition AliasName { get; set; }

            public NodeDefinition Description { get; set; }
            public NodeDefinition IsWeightProduct { get; set; }
            public NodeDefinition IsDeleted { get; set; }
            public NodeDefinition NoTax { get; set; }
            public NodeDefinition ValidFrom { get; set; }
            public NodeDefinition ValidTo { get; set; }
            public NodeDefinition Price { get; set; }
            public NodeDefinition Uom { get; set; }
            public NodeDefinition IncludePrice { get; set; }
        }

        public class ConnectorLocation
        {
            public string BaseElementPath { get; set; }
            public NodeDefinition StoreCode { get; set; }
            public NodeDefinition Name1 { get; set; }
            public NodeDefinition Name2 { get; set; }
            public NodeDefinition Name3 { get; set; }
            public NodeDefinition Name4 { get; set; }
            public NodeDefinition Name5 { get; set; }
            public NodeDefinition Street1 { get; set; }
            public NodeDefinition Street2 { get; set; }
            public NodeDefinition Street3 { get; set; }
            public NodeDefinition Street4 { get; set; }
            public NodeDefinition Street5 { get; set; }
            public NodeDefinition District { get; set; }
            public NodeDefinition City { get; set; }
            public NodeDefinition Country { get; set; }
            public NodeDefinition PostalCode { get; set; }
            public NodeDefinition Email { get; set; }
            public NodeDefinition Telephone { get; set; }
            public NodeDefinition BranchStoreCode {get;set;}
            public NodeDefinition BranchName1 {get;set;}
            public NodeDefinition BranchName2 {get;set;}
            public NodeDefinition BranchName3 {get;set;}
            public NodeDefinition BranchName4 {get;set;}
            public NodeDefinition BranchName5 {get;set;}
            public NodeDefinition BranchStreet1 {get;set;}
            public NodeDefinition BranchStreet2 {get;set;}
            public NodeDefinition BranchStreet3 {get;set;}
            public NodeDefinition BranchStreet4 {get;set;}
            public NodeDefinition BranchStreet5 {get;set;}
            public NodeDefinition BranchDistrict {get;set;}
            public NodeDefinition BranchCity {get;set;}
            public NodeDefinition BranchCountry {get;set;}
            public NodeDefinition BranchPostalCode {get;set;}
            public NodeDefinition BranchEmail {get;set;}
            public NodeDefinition BranchTelephone {get;set;}
        }

    }

    public enum NodeType
    {
        Text, Attribute
    }
    public enum FieldType
    {
        Text, Date, Character
    }
    public class NodeDefinition
    {
        public string Path { get; set; }
        public string Format { get; set; }
        public NodeType NodeType { get; set; }
        public FieldType FieldType { get; set; }
        public int TotalChars{ get; set; }
        public string DefaultValue { get; set; }

        public NodeDefinition()
        {
            FieldType = FieldType.Text;
            TotalChars = 1;
        }
    }

}