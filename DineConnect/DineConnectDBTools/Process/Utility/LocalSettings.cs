﻿using System;
using System.IO;

namespace SFBIFoodItConsole.Process.Utility
{
    public static class LocalSettings
    {
        public static SettingsModel Settings { get; private set; }

        static LocalSettings()
        {
            if (!Directory.Exists(DocumentPath)) Directory.CreateDirectory(DocumentPath);
            if (!Directory.Exists(DataPath)) Directory.CreateDirectory(DataPath);
            Settings = ObjectSerializer.LoadSettings(SettingsFileName);
        }

        public static string DocumentPath => Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + AppName;

        private static string DataPath
            => Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\" + AppName;
        public static string CommonSettingsFileName => DataPath + "\\Config.json";

        public static string SettingsFileName => CommonSettingsFileName;
        public static string AppName => "SFBIFoodItConsole";
        public static string DatabaseName => AppName;

        public static void Save(SettingsModel settingsModel)
        {
            ObjectSerializer.SaveSettings(SettingsFileName, settingsModel);
            lock (Settings)
            {
                Settings = settingsModel;
            }
        }

    }

}