﻿namespace SFBIFoodItConsole.Process.Utility
{
    public static class SettingModelDefaultBuilder
    {
        public static SettingsModel SetDefault(this SettingsModel source)
        {
            source.CategoryData = new SettingsModel.ConnectorCategory().SetDefault();
            source.ItemData = new SettingsModel.ConnectorItem().SetDefault();
            source.LocationData = new SettingsModel.ConnectorLocation().SetDefault();


            return source;
        }

        public static SettingsModel.ConnectorCategory SetDefault(this SettingsModel.ConnectorCategory source)
        {
            source.BaseElementPath = "//WPDWGR01/E1WPW01";
            source.Organization = new NodeDefinition
            {
                Path = "FILIALE",
                NodeType = NodeType.Text
            };

            source.Code = new NodeDefinition
            {
                Path = "WARENGR",
                NodeType = NodeType.Text
            };

            source.Name = new NodeDefinition
            {
                Path = "E1WPW02/BEZEICH",
                NodeType = NodeType.Text
            };

            source.Hierarchy = new NodeDefinition
            {
                Path = "E1WPW02/HIERARCHIE",
                NodeType = NodeType.Text
            };
            return source;
        }

        public static SettingsModel.ConnectorLocation SetDefault(this SettingsModel.ConnectorLocation source)
        {
            source.BaseElementPath = "//PLANTDOWNLOAD/PLANT";
            source.StoreCode = new NodeDefinition
            {
                Path = "STORECODE",
                NodeType = NodeType.Text
            };
            source.Name1 = new NodeDefinition
            {
                Path = "NAME1",
                NodeType = NodeType.Text
            };
            source.Name2 = new NodeDefinition
            {
                Path = "NAME2",
                NodeType = NodeType.Text
            };
            source.Name3 = new NodeDefinition
            {
                Path = "NAME3",
                NodeType = NodeType.Text
            };
            source.Name4 = new NodeDefinition
            {
                Path = "NAME4",
                NodeType = NodeType.Text
            };
            source.Name5 = new NodeDefinition
            {
                Path = "NAME5",
                NodeType = NodeType.Text
            };

            source.Street1 = new NodeDefinition
            {
                Path = "STREET1",
                NodeType = NodeType.Text
            };

            source.Street2 = new NodeDefinition
            {
                Path = "STREET2",
                NodeType = NodeType.Text
            };
            source.Street3 = new NodeDefinition
            {
                Path = "STREET3",
                NodeType = NodeType.Text
            };
            source.Street4 = new NodeDefinition
            {
                Path = "STREET4",
                NodeType = NodeType.Text
            };
            source.Street5 = new NodeDefinition
            {
                Path = "STREET5",
                NodeType = NodeType.Text
            };
            source.District = new NodeDefinition
            {
                Path = "DISTRICT",
                NodeType = NodeType.Text
            };

            source.City = new NodeDefinition
            {
                Path = "CITY",
                NodeType = NodeType.Text
            };
            source.Country = new NodeDefinition
            {
                Path = "COUNTRY",
                NodeType = NodeType.Text
            };
            source.PostalCode = new NodeDefinition
            {
                Path = "POSTALCODE",
                NodeType = NodeType.Text
            };
            source.Email = new NodeDefinition
            {
                Path = "EMAIL",
                NodeType = NodeType.Text
            };
            source.Telephone = new NodeDefinition
            {
                Path = "TELEPHONE",
                NodeType = NodeType.Text
            };
            source.BranchStoreCode = new NodeDefinition
            {
                Path = "BRANCH/BRANCHCODE",
                NodeType = NodeType.Text
            };

            source.BranchName1 = new NodeDefinition
            {
                Path = "BRANCH/ADDR_BR/NAME1",
                NodeType = NodeType.Text
            };

            source.BranchName2 = new NodeDefinition
            {
                Path = "BRANCH/ADDR_BR/NAME2",
                NodeType = NodeType.Text
            };

            source.BranchName3 = new NodeDefinition
            {
                Path = "BRANCH/ADDR_BR/NAME3",
                NodeType = NodeType.Text
            };

            source.BranchName4 = new NodeDefinition
            {
                Path = "BRANCH/ADDR_BR/NAME4",
                NodeType = NodeType.Text
            };

            source.BranchName5 = new NodeDefinition
            {
                Path = "BRANCH/ADDR_BR/NAME5",
                NodeType = NodeType.Text
            };

            source.BranchStreet1 = new NodeDefinition
            {
                Path = "BRANCH/ADDR_BR/STREET1",
                NodeType = NodeType.Text
            };

            source.BranchStreet2 = new NodeDefinition
            {
                Path = "BRANCH/ADDR_BR/STREET2",
                NodeType = NodeType.Text
            };

            source.BranchStreet3 = new NodeDefinition
            {
                Path = "BRANCH/ADDR_BR/STREET3",
                NodeType = NodeType.Text
            };

            source.BranchStreet4 = new NodeDefinition
            {
                Path = "BRANCH/ADDR_BR/STREET4",
                NodeType = NodeType.Text
            };

            source.BranchStreet5 = new NodeDefinition
            {
                Path = "BRANCH/ADDR_BR/STREET5",
                NodeType = NodeType.Text
            };

            source.BranchDistrict = new NodeDefinition
            {
                Path = "BRANCH/ADDR_BR/DISTRICT",
                NodeType = NodeType.Text
            };

            source.BranchCity = new NodeDefinition
            {
                Path = "BRANCH/ADDR_BR/CITY",
                NodeType = NodeType.Text
            };

            source.BranchCountry = new NodeDefinition
            {
                Path = "BRANCH/ADDR_BR/COUNTRY",
                NodeType = NodeType.Text
            };

            source.BranchPostalCode = new NodeDefinition
            {
                Path = "BRANCH/ADDR_BR/POST_CODE1",
                NodeType = NodeType.Text
            };

            source.BranchEmail = new NodeDefinition
            {
                Path = "EMAIL",
                NodeType = NodeType.Text
            };

            source.BranchTelephone = new NodeDefinition
            {
                Path = "TELEPHONE",
                NodeType = NodeType.Text
            };

            return source;
        }

        public static SettingsModel.ConnectorItem SetDefault(this SettingsModel.ConnectorItem source)
        {
            source.BaseElementPath = "//WBBDLD07/E1WBB01";
            source.Organization = new NodeDefinition
            {
                Path = "LOCNR",
                NodeType = NodeType.Text
            };
            source.Code = new NodeDefinition
            {
                Path = "MATNR",
                NodeType = NodeType.Text
            };
            source.Uom = new NodeDefinition
            {
                Path = "E1WBB03/MEINH",
                NodeType = NodeType.Text
            };
            source.Category = new NodeDefinition
            {
                Path = "E1WBB02/MATKL",
                NodeType = NodeType.Text
            };
            source.Description = new NodeDefinition
            {
                Path = "E1WBB10/MAKTM",
                NodeType = NodeType.Text
            };
            source.IsWeightProduct = new NodeDefinition
            {
                Path = "E1WBB09/SCAGR",
                NodeType = NodeType.Text
            };
            source.IsDeleted = new NodeDefinition
            {
                Path = "AENKZ",
                NodeType = NodeType.Text,
                DefaultValue = "DELE"
            };
            source.NoTax = new NodeDefinition
            {
                Path = "Z1WBB01/TAXTYPE",
                NodeType = NodeType.Text,
                DefaultValue = "1"
            };
            source.Name = new NodeDefinition
            {
                Path = "E1WBB10/MAKTM",
                NodeType = NodeType.Text
            };
            source.AliasName = new NodeDefinition
            {
                Path = "E1WBB03/E1WBB20/MAKTM_ME",
                NodeType = NodeType.Text
            };
            source.ValidFrom = new NodeDefinition
            {
                Path = "E1WBB03/E1WBB07/DATAB",
                NodeType = NodeType.Text,
                FieldType = FieldType.Date,
                Format = "yyyyMMdd"
            };
            source.ValidTo = new NodeDefinition
            {
                Path = "E1WBB03/E1WBB07/DATBI",
                NodeType = NodeType.Text,
                FieldType = FieldType.Date,
                Format = "yyyyMMdd"
            };
            source.Price = new NodeDefinition
            {
                Path = "E1WBB03/E1WBB07/Z1WBB07/PRICE_EXT",
                NodeType = NodeType.Text
            };

            source.IncludePrice = new NodeDefinition
            {
                Path = "E1WBB03/E1WBB07/E1WBB08/KWERT",
                NodeType = NodeType.Text
            };
            return source;
        }
    }
}