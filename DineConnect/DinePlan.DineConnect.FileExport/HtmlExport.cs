﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.FileExport
{
    public class HtmlExport
    {
        public static void Export(string fileName, string folder)
        {
            try
            {
                FileInfo newFile = new FileInfo(Path.Combine(folder, fileName));
                var htmlContent = new ExcelToHtml.ToHtml(newFile);
                var allHtml = htmlContent.GetHtml();
                File.WriteAllText(Path.Combine(folder, Path.GetFileNameWithoutExtension(fileName), ".html"), allHtml);
            }
            catch (Exception exception)
            {
                var messa = exception.Message;
                // ignored
            }
        }

       
    }
}
