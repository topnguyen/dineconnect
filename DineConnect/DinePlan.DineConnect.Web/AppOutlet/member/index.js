﻿(function () {
    appOutletModule.controller('common.views.outlet.member', [
       '$scope', '$state', '$stateParams', 'abp.services.app.member',
       function ($scope, $state, $stateParams, memberService) {
           var vm = this;

           vm.memberCode = $stateParams.id;

           $scope.$on('$viewContentLoaded', function () {
               App.initAjax();
           });
           vm.pullDetails = function () {
               vm.loading = true;
               memberService.getPointsDetailsByName(vm.memberCode).success(function (result) {
                   vm.result = result;
                   console.log(vm.result);
               }).finally(function () {
                   vm.loading = false;
               });
           }

           vm.pullDetails();

       }]);
})();