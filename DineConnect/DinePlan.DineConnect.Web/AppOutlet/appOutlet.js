﻿/* 'app' MODULE DEFINITION */
var appOutletModule = angular.module("appOutlet", [
    "ui.router",
    "ui.bootstrap",
    'ui.utils',
    "ui.jq",
    'ui.grid',
    'ui.grid.pagination',
    "oc.lazyLoad",
    "ngSanitize",
    'angularFileUpload',
    'daterangepicker',
    'angularMoment',
    'frapontillo.bootstrap-switch',
    'abp',
    'ng.deviceDetector',
    "ui.select",    
    "multipleDatePicker",
    "angularFileUpload",
    "daterangepicker",
    "angularMoment",
    "ngTable",
    "angular-sortable-view",
    "cfp.hotkeys",
    "angucomplete-alt",
]);

/* LAZY LOAD CONFIG */

/* This application does not define any lazy-load yet but you can use $ocLazyLoad to define and lazy-load js/css files.
 * This code configures $ocLazyLoad plug-in for this application.
 * See it's documents for more information: https://github.com/ocombe/ocLazyLoad
 */
appOutletModule.config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        cssFilesInsertBefore: 'ng_load_plugins_before', // load the css files before a LINK element with this ID.
        debug: false,
        events: true,
        modules: []
    });
}]);

/* THEME SETTINGS */
App.setAssetsPath(abp.appPath + 'metronic/assets/');
appOutletModule.factory('settings', ['$rootScope', function ($rootScope) {
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        layoutImgPath: App.getAssetsPath() + 'admin/layout4/img/',
        layoutCssPath: App.getAssetsPath() + 'admin/layout4/css/',
        assetsPath: abp.appPath + 'metronic/assets',
        globalPath: abp.appPath + 'metronic/assets/global',
        layoutPath: abp.appPath + 'metronic/assets/layouts/layout4'
    };

    $rootScope.settings = settings;

    return settings;
}]);

/* ROUTE DEFINITIONS */

appOutletModule.config([
    '$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {

        // Default route (overrided below if user has permission)
        $urlRouterProvider.otherwise("/welcome");

        //Welcome page
        $stateProvider.state('dashboard', {
            url: '/dashboard/:id',
            templateUrl: '~/AppOutlet/dashboard/index.cshtml'
        });

        $stateProvider.state('member', {
            url: '/member/:id',
            templateUrl: '~/AppOutlet/member/index.cshtml'
        });

        $stateProvider.state('stock', {
            url: '/stock/:id',
            templateUrl: '~/AppOutlet/stock/mobileclosingstock.cshtml'
        });

        $stateProvider.state('po', {
            url: '/po/:id',
            templateUrl: '~/AppOutlet/po/index.cshtml'
        });

        $stateProvider.state('invoice', {
            url: '/invoice/:id',
            templateUrl: '~/AppOutlet/invoice/mobileinvoiceDetail.cshtml'
        });

				$stateProvider.state('cardbalance', {
					url: '/cardbalance',
					templateUrl: '~/AppOutlet/swipecardBalance/swipecardBalance.cshtml'
				});
    }
]);

appOutletModule.run(["$rootScope", "settings", "$state", 'i18nService', '$uibModalStack', function ($rootScope, settings, $state, i18nService, $uibModalStack) {
    $rootScope.$state = $state;
    $rootScope.$settings = settings;

    $rootScope.$on('$stateChangeSuccess', function () {
        $uibModalStack.dismissAll();
    });

    //Set Ui-Grid language
    if (i18nService.get(abp.localization.currentCulture.name)) {
        i18nService.setCurrentLang(abp.localization.currentCulture.name);
    } else {
        i18nService.setCurrentLang("en");
    }

    $rootScope.safeApply = function (fn) {
        var phase = this.$root.$$phase;
        if (phase == '$apply' || phase == '$digest') {
            if (fn && (typeof (fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };
}]);