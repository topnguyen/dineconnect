﻿(function () {
    appOutletModule.controller('common.views.outlet.stock', [
        '$scope', '$state', '$stateParams', 'abp.services.app.closingStock', 'abp.services.app.location',
        //'appSession',
        'abp.services.app.material', 'abp.services.app.unitConversion', 'abp.services.app.template', 'deviceDetector',
        function ($scope, $state, $stateParams, closingstockService, locationService,
            //appSession, 
            materialService, unitconversionService, templateService, deviceDetector)
        {
            var vm = this;
            vm.locationCode = $stateParams.id;

            $scope.$on('$viewContentLoaded', function() {
                App.initAjax();
            });

            vm.gotoHome = function () {
                $state.go("dashboard", {
                    id: vm.locationCode
                });
            }

            $scope.minDate = moment().add(0, 'days');
            // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            $('input[name="closingDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });

            vm.keyup = function(e,option)
            {
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) {
                    var tabables = $("*[tabindex != '-1']:visible,:text,");
                    var index = tabables.index(this);
                    tabables.eq(index + 1).focus();
                }
            }

            vm.LocalNumberWithPointPassingText = function(inputtext, e, objfocus, argboolean) {
                var keynum;
                var keychar;
                var charcheck;

                if (window.event) // IE
                {
                    keynum = e.keyCode;
                    if (keynum == 13) {
                        e.keyCode = 9;
                        return true;
                    }
                    
                }
                else if (e.which) // Netscape/Firefox/Opera
                {
                    keynum = e.which;
                    if (keynum == 13)
                        e.which = 9;
                }
                if (keynum == 11)
                    return false;
                if (keynum == 13) {
                    
                    //if (objfocus != null)
                    //{
                    //    var a = $('input,select,button').index(this) + 1;
                    //    $('input,select,button')[$('input,select,button').index(this) + 1].focus();
                    //}
                        //document.all[objfocus].focus();
                    return false;
                }

                if (argboolean == false)
                    inputtext = inputtext + ".";

                var patt1 = /\./;
                var result = inputtext.search(patt1);
                if (result >= 0 && keynum == 46)
                    keynum = 95;

                keychar = String.fromCharCode(keynum);
                charcheck = /[0123456789.]/;
                return charcheck.test(keychar);
            }


            vm.pullDetails = function () {
                vm.loading = true;
                locationService.getLocationById({ Id: vm.locationCode }).success(function (result) {
                    if (result == null) {
                        abp.notify.warn(app.localize('Location') + ' ' + app.localize('NotExist'));
                        return;
                    }
                    vm.location = result;
                    $scope.minDate = moment(result.houseTransactionDate);

                    $('input[name="closingDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: moment(),
                        minDate: $scope.minDate,
                        maxDate: moment()
                    });

                }).finally(function () {
                    vm.loading = false;
                });
            }
            vm.pullDetails();

            vm.permissions = {
                barcodeUsage: abp.auth.hasPermission('Pages.Tenant.House.Master.Material.Barcode'),
            };


            vm.saving = false;
            vm.closingstock = null;
            $scope.existall = true;
            vm.closingstockId = null; // $stateParams.id;
            vm.sno = 0;
            vm.closingstockPortions = [];

            vm.closingStockDetail = [];

            vm.defaultLocationId = 1; //appSession.user.locationRefId;
            vm.mutlipleUomAllowed = true; // appSession.user.multipleUomEntryAllowed;

            vm.uilimit = null;
            vm.templatesave = null;

          


            vm.defaulttemplatetype = 6;

            vm.refTemplateDetail = [];
            vm.reftemplate = [];

            function fillDropDownTemplate() {
                vm.loading = true;
                templateService.getTemplateDetail({
                    locationRefId: vm.defaultLocationId,
                    templateType: vm.defaulttemplatetype
                }).success(function (result) {
                    vm.refTemplateDetail = result.items;
                    angular.forEach(vm.refTemplateDetail, function (value, key) {
                        vm.reftemplate.push({
                            'value': value.id,
                            'displayText': value.name,
                        });
                    });
                    vm.loading = false;
                });
            }

            fillDropDownTemplate();

            vm.fillDataFromTemplate = function () {
                vm.selectedTemplate = null;
                vm.refTemplateDetail.some(function (value, key) {
                    if (value.id == vm.template) {
                        vm.selectedTemplate = value.templateData;
                        return true;
                    }
                });

                if (vm.selectedTemplate == null)
                    return;

                vm.loading = true;

                closingstockService.getTemplateObjectForEdit({
                    objectString: vm.selectedTemplate
                }).success(function (result) {

                    vm.closingstock = result.closingStock;

                    vm.closingStockDetail = result.closingStockDetail;

                    vm.closingstock.stockDate = moment().format($scope.format);
                    vm.closingstock.id = null;
                    vm.closingstock.locationRefId = vm.defaultLocationId;

                    $('input[name="closingDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.closingstock.stockDate,
                        minDate: $scope.minDate,
                        maxDate: moment().format($scope.format)
                    });

                    vm.closingstockPortions = [];
                    vm.sno = 1;
                    angular.forEach(vm.closingStockDetail, function (value, key) {
                        vm.closingstockPortions.push({
                            'sno': vm.sno,
                            'barcode': value.barcode,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'defaultUnit': value.unitRefName,
                            'defaultUnitId': value.unitRefId,
                            'onHand': null,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'changeunitflag': false,
                        });
                        vm.sno = vm.sno + 1;
                    });
                    vm.sno = vm.closingstockPortions.length + 1;

                    vm.loading = false;
                });

            }

            vm.templateSave = function()
            {
                vm.closingstockRecipeDetail = [];
                vm.errorflag = false;

                if (vm.templatesave.templatename.length == 0) {
                    abp.message.warn(app.localize("TemplateNameRequired"));
                    return;
                }

                vm.saving = true;
                vm.closingStockDetail = [];

                vm.errorflag = false;
                angular.forEach(vm.closingstockPortions, function (value, key) {
                    if (value.unitRefId == null || value.unitRefId == 0) {
                        abp.message.warn(app.localize("UnitErr", value.materialRefName));
                        vm.errorflag = true;
                        return;
                    }

                    vm.closingStockDetail.push({
                        'sno': value.sno,
                        'barcode' : value.barcode,
                        'materialRefId': value.materialRefId,
                        'materialRefName': value.materialRefName,
                        'onHand': value.onHand,
                        'unitRefId': value.unitRefId,
                        'unitRefName': value.unitRefName,
                        'changeunitflag': false,
                    });

                });

                if (vm.errorflag == true) {
                    vm.saving = false;
                    return;
                }


                if (vm.templatesave.templatename != null)
                    vm.templatesave.templatename = vm.templatesave.templatename.toUpperCase();

                closingstockService.saveTemplateClosingStock({
                    closingstock: vm.closingstock,
                    closingStockDetail: vm.closingStockDetail,
                    templateSave: vm.templatesave,
                }).success(function (result) {
                    abp.notify.info(app.localize('Template') + ' ' + app.localize('SavedSuccessfully'));
                    vm.cancel(0);
                }).finally(function () {
                    vm.saving = false;
                });
            }

            vm.save = function (argSaveOption)
            {
                if (vm.existall == false)
                    return;

                var convFactor = 0;
                var convExistFlag = false;



                vm.closingstockRecipeDetail = [];
                vm.errorflag = false;

                if (vm.templatesave != null) {
                    if (vm.templatesave.templateflag == true && vm.templatesave.templatename.length == 0) {
                        abp.message.warn(app.localize("TemplateNameRequired"));
                        return;
                    }
                    if (vm.templatesave.templatename != null)
                        vm.templatesave.templatename = vm.templatesave.templatename.toUpperCase();

                }

                vm.saving = true;
                vm.closingStockDetail = [];

                vm.errorflag = false;
                angular.forEach(vm.closingstockPortions, function (value, key) {
                    if (value.onHand == 0 || value.onHand==null)
                    {
                        abp.message.info(app.localize("ClosingOnHandShouldNotBeZero", value.materialRefName));
                    }
                    if (value.unitRefId == null || value.unitRefId == 0)
                    {
                        abp.message.warn(app.localize("UnitErr", value.materialRefName));
                        vm.errorflag = true;
                        return;
                    }
                   
                    vm.closingStockDetail.push({
                        'sno': value.sno,
                        'materialRefId': value.materialRefId,
                        'materialRefName': value.materialRefName,
                        'onHand': value.onHand,
                        'unitRefId': value.unitRefId,
                        'unitRefName': value.unitRefName,
                        'changeunitflag': false,
                    });
                   
                });


                if (vm.errorflag == true) {
                    vm.saving = false;
                    return;
                }

                closingstockService.createOrUpdateClosingStock({
                    closingstock: vm.closingstock,
                    closingStockDetail: vm.closingStockDetail,
                    templateSave: vm.templatesave,
                }).success(function (result) {
                    abp.notify.info(app.localize('ClosingStock')+ ' ' + app.localize('SavedSuccessfully'));
                    vm.cancel(0);
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {
                $scope.errmessage = "";
                if (moment(vm.closingstock.stockDate) > moment())
                {
                    $scope.errmessage = app.localize('FutureDateNotAllowed');
                }
                if ($scope.errmessage != "") {
                    abp.notify.warn($scope.errmessage, 'Required');
                    return false;
                }
                else {
                    return true;
                }
            };

            vm.addPortion = function (data)
            {
                if (data != null) {
                    if (data.sno < vm.sno -1 ) {
                        if (data.materialRefId == null || data.materialRefId == 0 || data.materialRefName == '') {
                            abp.notify.warn(app.localize('MaterialErr'));
                        }

                        if (data.unitRefId == 0 || data.unitRefId == null) {
                            abp.notify.warn(app.localize('UnitErr', data.materialRefName));
                            return;
                        }

                        if (data.onHand == 0 || data.onHand == null) {
                            abp.notify.info(app.localize('StockAsZeroAlert', data.materialRefName));
                        }

                        if (errorFlag) {
                            vm.saving = false;
                            vm.loading = false;
                            $scope.existall = false;
                        }

                        if (data.sno < vm.sno) {
                            $(this).next().focus();
                            return;
                        }
                    }
                    else {

                    }
                }

                if (vm.sno > 0) {
                    if (vm.existall() == false)
                        return;

                    var errorFlag = false;

                    if (vm.closingstockPortions.length <= 0) {
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }
                    else {
                        if (vm.isUndefinedOrNull(vm.closingstockPortions[0].materialRefId) || vm.closingstockPortions[0].materialRefId == 0) {
                            abp.notify.info(app.localize('MinimumOneDetail'));
                            return;
                        }
                    }

                    var lastelement = vm.closingstockPortions[vm.closingstockPortions.length - 1];

                    if (lastelement.materialRefId == null || lastelement.materialRefId == 0 || lastelement.materialRefName == '') {
                        abp.notify.warn(app.localize('MaterialErr'));
                    }

                    if (lastelement.unitRefId == 0 || lastelement.unitRefId == null) {
                        abp.notify.warn(app.localize('UnitErr', lastelement.materialRefName));
                        return;
                    }

                    if (lastelement.onHand == 0 || lastelement.onHand == null) {
                        abp.notify.info(app.localize('StockAsZeroAlert', lastelement.materialRefName));
                    }

                    if (errorFlag) {
                        vm.saving = false;
                        vm.loading = false;
                        $scope.existall = false;
                    }

                }


                vm.sno = vm.sno + 1;
                vm.closingstockPortions.push({
                    'sno': vm.sno, 'barcode' : '', 'materialRefId': 0, 'materialRefName': '',  'onHand': null,
                    'defaultUnit': "", 'unitRefId': '', 'unitRefName': '', 'changeunitflag': false,
                });

            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex)
            {
                if(vm.closingstockPortions.length>1)
                {
                    vm.closingstockPortions.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                }
                else
                {
                    vm.closingstockPortions.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                    vm.addPortion();
                    return;
                }
            }

            $scope.func = function (data, val) {
                var forLoopFlag = true;

                angular.forEach(vm.closingstockPortions, function (value, key)
                {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag)
                {
                    data.materialRefId = 0;
                    data.barcode = '';
                    data.materialRefName = '';
                    return;
                }
                data.materialRefId = val.id;
                data.materialRefName = val.materialName;
                data.defaultUnitId = val.unitRefId;
                data.defaultUnit = val.defaultUnitName;
                data.unitRefName = val.defaultUnitName;
                data.unitRefId = val.unitRefId;
                data.barcode = val.barcode;
                if (data.barcode == null)
                    data.barcode = data.materialRefName;
            };

            vm.refconversionunit = [];
            vm.fillUnitConversion = function () {
                unitconversionService.getAll({
                }).success(function (result) {
                    vm.refconversionunit = result.items;
                });
            }

            vm.fillUnitConversion();


            vm.refmaterial = [];
            vm.tempRefMaterial = [];

            function fillDropDownRecipe() {
                materialService.getView({
                    //skipCount: requestParams.skipCount,
                    //maxResultCount: 1,
                    //sorting: requestParams.sorting,
                    //filter: vm.filterText,
                    operation: 'FillAll',
                }).success(function (result) {
                    vm.refmaterial = result.items;
                });
            }

            fillDropDownRecipe();
           

            vm.cancel = function (argOption) {

                if (argOption == 1) {
                    abp.message.confirm(
                       app.localize('CancelEntry?'),
                       function (isConfirmed) {
                           if (isConfirmed) {
                               $state.go("dashboard", {
                                   id: vm.locationCode
                               });
                           }
                       }
                   );
                }
                else
                {
                    $state.go("dashboard", {
                        id: vm.locationCode
                    });
                }
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.reflocation = [];

            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;
                
                });
            }

            vm.refrequest = [];
         
            vm.isUndefinedOrNull =function(val)
            {
                return angular.isUndefined(val) || val == null;
            };

       

            function init() {
                vm.loading = true;

                fillDropDownLocation();

                closingstockService.getClosingStockForEdit({
                    Id: vm.closingstockId
                }).success(function (result)
                {
                    vm.closingstock = result.closingStock;                    
                    vm.closingstock.locationRefId = vm.defaultLocationId;

                    if (result.closingStock.id != null) {
                        vm.uilimit = null;
                        vm.closingStockDetail = result.closingStockDetail;

                        vm.closingstock.stockDate = moment(result.closingStock.stockDate).format($scope.format);
                        $scope.minDate = vm.closingstock.stockDate;

                        angular.forEach(result.closingStockDetail, function (value, key) {
                            vm.closingstockPortions.push({
                                'sno': value.sno,
                                'barcode' : value.barcode,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'defaultUnit': value.defaultUnitName,
                                'defaultUnitId':value.defaultUnitId,
                                'onHand': value.onHand,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'changeunitflag': false,
                            })
                        });
                        vm.sno = vm.closingstockPortions.length;
                    }
                    else
                    {
                        vm.closingstock.stockDate = moment().format($scope.format);
                        vm.closingstock.locationRefId = vm.locationCode;
                        vm.addPortion();
                        vm.uilimit = 20;
                    }
                    vm.loading = false;
                });
            }

            init();

            vm.removeUnfilled = function () {
                var listtoremoved = [];
                tempportions = [];

                angular.forEach(vm.closingstockPortions, function (value, key) {
                    if (value.onHand == 0 || value.onHand == null || value.onHand == "") {
                        listtoremoved.push(key)
                    }
                    else {
                        tempportions.push(value);
                    }
                });

                vm.closingstockPortions = tempportions;

                vm.sno = vm.closingstockPortions.length;

                if (vm.closingstockPortions.length == 0)
                    vm.addPortion();
            }

            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

            vm.unitReference = function (selected, data) {
                data.unitRefId = selected.value;
                data.unitRefName = selected.displayText;
                data.changeunitflag = false;
            }

            vm.selectMaterialBasedOnBarcode = function (data) {
                if (data.barcode == '' || data.barcode == null)
                    return;

                var forLoopFlag = true;
                angular.forEach(vm.closingstockPortions, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(data.barcode, value.barcode) && !angular.equals(data.sno, value.sno)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });
                if (forLoopFlag == false)
                    return;

                vm.uilimit = null;
                var selectedMaterial = null;
                vm.refmaterial.some(function (refdata, refkey) {
                    if (refdata.barcode == data.barcode) {
                        selectedMaterial = refdata;
                        return true;
                    }
                });

                if (selectedMaterial != null) {
                    $scope.func(data, selectedMaterial);
                }
                else {
                    abp.notify.warn(app.localize('Barcode') + ' ' + app.localize('NotExist'));
                }
            }
       }]);
})();