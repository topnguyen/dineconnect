﻿(function () {
    appOutletModule.controller('common.views.outlet.mobilestock', [
        '$scope', '$state', "$window", '$stateParams', 'abp.services.app.closingStock', 'abp.services.app.location',
        'abp.services.app.material', 'abp.services.app.unitConversion', 'abp.services.app.template', 'deviceDetector', "hotkeys",
        function ($scope, $state, $window, $stateParams, closingstockService, locationService,
            materialService, unitconversionService, templateService, deviceDetector, hotKeys) {
            var vm = this;
            vm.locationCode = $stateParams.id;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            $scope.setMyFocus = function (str) {
                var element = $window.document.getElementById(str);
                element.focus();
            };


            vm.acceptDuplicateFlag = false;
            vm.selectedItem = null;
            vm.selectedQuantity = "";
            vm.currentMaterial = null;
						vm.loadingMaterial = false;
						vm.materialRefId = null;
						vm.materialRefName = null;
            vm.loading = true;


            vm.noofProducts = 0;
            vm.noofQuantities = 0;

            vm.gotoHome = function () {
                $state.go("dashboard", {
                    id: vm.locationCode
                });
            }

            $scope.minDate = moment().add(0, 'days');
            // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            //$('input[name="closingDate"]').daterangepicker({
            //    locale: {
            //        format: $scope.format
            //    },
            //    singleDatePicker: true,
            //    showDropdowns: true,
            //    startDate: moment(),
            //    minDate: $scope.minDate,
            //    maxDate: moment()
            //});

            vm.keyup = function (e, option) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) {
                    var tabables = $("*[tabindex != '-1']:visible,:text,");
                    var index = tabables.index(this);
                    tabables.eq(index + 1).focus();
                }
            }



            vm.saving = false;
            vm.closingstock = null;
            $scope.existall = true;
            vm.closingstockId = null; // $stateParams.id;
            vm.sno = 0;
            vm.closingstockPortions = [];

            vm.closingStockDetail = [];

            vm.defaultLocationId = vm.locationCode; //appSession.user.locationRefId;
            vm.mutlipleUomAllowed = true; // appSession.user.multipleUomEntryAllowed;

            vm.uilimit = null;

            vm.save = function (argSaveOption) {
                if (vm.existall == false)
                    return;

                var convFactor = 0;
                var convExistFlag = false;



                vm.closingstockRecipeDetail = [];
                vm.errorflag = false;

                if (vm.templatesave != null) {
                    if (vm.templatesave.templateflag == true && vm.templatesave.templatename.length == 0) {
                        abp.message.warn(app.localize("TemplateNameRequired"));
                        return;
                    }
                    if (vm.templatesave.templatename != null)
                        vm.templatesave.templatename = vm.templatesave.templatename.toUpperCase();

                }

                vm.saving = true;
                vm.closingStockDetail = [];

                vm.errorflag = false;
                angular.forEach(vm.closingstockPortions, function (value, key) {
                    if (value.onHand == 0 || value.onHand == null) {
                        abp.message.info(app.localize("ClosingOnHandShouldNotBeZero", value.materialRefName));
                    }
                    if (value.unitRefId == null || value.unitRefId == 0) {
                        abp.message.warn(app.localize("UnitErr", value.materialRefName));
                        vm.errorflag = true;
                        return;
                    }

                    vm.closingStockDetail.push({
                        'sno': value.sno,
                        'materialRefId': value.materialRefId,
                        'materialRefName': value.materialRefName,
                        'onHand': value.onHand,
                        'unitRefId': value.unitRefId,
                        'unitRefName': value.unitRefName,
                        'changeunitflag': false,
                    });

                });


                if (vm.errorflag == true) {
                    vm.saving = false;
                    return;
                }

                closingstockService.createOrUpdateClosingStock({
                    closingstock: vm.closingstock,
                    closingStockDetail: vm.closingStockDetail,
                    templateSave: vm.templatesave,
                }).success(function (result) {
                    abp.notify.info(app.localize('ClosingStock') + ' ' + app.localize('SavedSuccessfully'));
                    vm.cancel(0);
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {
                $scope.errmessage = "";
                if (moment(vm.closingstock.stockDate) > moment()) {
                    $scope.errmessage = app.localize('FutureDateNotAllowed');
                }
                if ($scope.errmessage != "") {
                    abp.notify.warn($scope.errmessage, 'Required');
                    return false;
                }
                else {
                    return true;
                }
            };

            vm.addPortion = function (data) {
                if (data != null) {
                    if (data.sno < vm.sno - 1) {
                        if (data.materialRefId == null || data.materialRefId == 0 || data.materialRefName == '') {
                            abp.notify.warn(app.localize('MaterialErr'));
                        }

                        if (data.unitRefId == 0 || data.unitRefId == null) {
                            abp.notify.warn(app.localize('UnitErr', data.materialRefName));
                            return;
                        }

                        if (data.onHand == 0 || data.onHand == null) {
                            abp.notify.info(app.localize('StockAsZeroAlert', data.materialRefName));
                        }

                        if (errorFlag) {
                            vm.saving = false;
                            
                            $scope.existall = false;
                        }

                        if (data.sno < vm.sno) {
                            $(this).next().focus();
                            return;
                        }
                    }
                    else {

                    }
                }

                if (vm.sno > 0) {
                    if (vm.existall() == false)
                        return;

                    var errorFlag = false;

                    if (vm.closingstockPortions.length <= 0) {
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }
                    else {
                        if (vm.isUndefinedOrNull(vm.closingstockPortions[0].materialRefId) || vm.closingstockPortions[0].materialRefId == 0) {
                            abp.notify.info(app.localize('MinimumOneDetail'));
                            return;
                        }
                    }

                    var lastelement = vm.closingstockPortions[vm.closingstockPortions.length - 1];

                    if (lastelement.materialRefId == null || lastelement.materialRefId == 0 || lastelement.materialRefName == '') {
                        abp.notify.warn(app.localize('MaterialErr'));
                    }

                    if (lastelement.unitRefId == 0 || lastelement.unitRefId == null) {
                        abp.notify.warn(app.localize('UnitErr', lastelement.materialRefName));
                        return;
                    }

                    if (lastelement.onHand == 0 || lastelement.onHand == null) {
                        abp.notify.info(app.localize('StockAsZeroAlert', lastelement.materialRefName));
                    }

                    if (errorFlag) {
                        vm.saving = false;
                        
                        $scope.existall = false;
                    }

                }


                vm.sno = vm.sno + 1;
                vm.closingstockPortions.push({
                    'sno': vm.sno, 'barcode': '', 'materialRefId': 0, 'materialRefName': '', 'onHand': null,
                    'defaultUnit': "", 'unitRefId': '', 'unitRefName': '', 'changeunitflag': false,
                });

            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex) {
                if (vm.closingstockPortions.length > 1) {
                    vm.closingstockPortions.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                }
                else {
                    vm.closingstockPortions.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                    vm.addPortion();
                    return;
                }
            }

            $scope.func = function (data, val) {
                var forLoopFlag = true;

                angular.forEach(vm.closingstockPortions, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.materialRefId = 0;
                    data.barcode = '';
                    data.materialRefName = '';
                    return;
                }
                data.materialRefId = val.id;
                data.materialRefName = val.materialName;
                data.defaultUnitId = val.unitRefId;
                data.defaultUnit = val.defaultUnitName;
                data.unitRefName = val.defaultUnitName;
                data.unitRefId = val.unitRefId;
                data.barcode = val.barcode;
                if (data.barcode == null)
                    data.barcode = data.materialRefName;
            };

            vm.refconversionunit = [];
            vm.fillUnitConversion = function () {
                unitconversionService.getAll({
                }).success(function (result) {
                    vm.refconversionunit = result.items;
                });
            }

            vm.fillUnitConversion();


            vm.refmaterial = [];
            vm.tempRefMaterial = [];
            vm.menuForLocation = [];
            vm.refbarcodelist = [];

            function fillDropDownMaterial() {
                materialService.getMaterialWithBarcodeListSeparate({
                    operation: 'FillAll',
                }).success(function (result) {
                    vm.refmaterial = result.materialViewList;
                    vm.menuForLocation = vm.refmaterial;
                    vm.refbarcodelist = result.barcodeList;
                }).finally(function () {
                });
            }

            fillDropDownMaterial();


            vm.cancel = function (argOption) {

                if (argOption == 1) {
                    abp.message.confirm(
                       app.localize('CancelEntry?'),
                       function (isConfirmed) {
                           if (isConfirmed) {
                               $state.go("dashboard", {
                                   id: vm.locationCode
                               });
                           }
                       }
                   );
                }
                else {
                    $state.go("dashboard", {
                        id: vm.locationCode
                    });
                }
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.refrequest = [];

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };



            function init() {
                vm.loading = true;
                closingstockService.getClosingStockForEdit({
                    Id: vm.closingstockId
                }).success(function (result) {
                    vm.closingstock = result.closingStock;
                    vm.closingstock.locationRefId = vm.defaultLocationId;

                    if (result.closingStock.id != null) {
                        vm.uilimit = null;
                        vm.closingStockDetail = result.closingStockDetail;
                        vm.closingstock.stockDate = moment(result.closingStock.stockDate).format($scope.format);
                        $scope.minDate = vm.closingstock.stockDate;

                        angular.forEach(result.closingStockDetail, function (value, key) {
                            vm.closingstockPortions.push({
                                'sno': value.sno,
                                'barcode': value.barcode,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'defaultUnit': value.defaultUnitName,
                                'defaultUnitId': value.defaultUnitId,
                                'onHand': value.onHand,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'changeunitflag': false,
                            })
                        });
                        vm.sno = vm.closingstockPortions.length;
                    }
                    else {
                        var hour = moment().format('HH');

                        $scope.maxDate = moment();

                        if (hour < 13) {
                            vm.closingstock.stockDate = moment(moment().add(-1, 'days')).format($scope.format);
                            //$scope.maxDate = moment(moment().add(-1, 'days')).format($scope.format);
                        }
                        else {
                            vm.closingstock.stockDate = moment().format($scope.format);
                        }

                        $('input[name="closingDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: vm.closingstock.stockDate,
                            minDate: $scope.minDate,
                            maxDate: $scope.maxDate
                        });

                        vm.closingstock.locationRefId = vm.locationCode;
                        vm.uilimit = 20;
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            }

            

            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loadingMaterial = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }
                    vm.loadingMaterial = false;
                    
                }).finally(function () {
                    vm.loadingMaterial = false;
                });;
            }

            vm.unitReference = function (selected, data) {
                data.unitRefId = selected.value;
                data.unitRefName = selected.displayText;
                data.changeunitflag = false;
            }

            vm.barcodeKeyPress = function ($event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    //alert(objId);
                    $("#searQuantity").focus();
                }
            };

            vm.searquantityKeyPress = function (objId, $event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    //alert(objId);
                    vm.addItemToOrder();
                }
            };

						vm.selectMaterialBasedOnBarcode = function (argOption, argSelectedMaterial) {
							var selectedMaterial = null;
							vm.uilimit = null;
							if (argOption == 1) {

								if (vm.barcode == '' || vm.barcode == null) {
									vm.currentMaterial = null;
									vm.selectedItem = null;
									vm.selectedQuantity = null;
									//$("#barcode").focus();
									return;
								}
								vm.refmaterial.some(function (refdata, refkey) {
									if (refdata.barcode == vm.barcode) {
										selectedMaterial = refdata;
										return true;
									}
								});

								vm.refbarcodelist.some(function (bcdata, bckey) {
									if (bcdata.barcode == vm.barcode) {
										vm.refmaterial.some(function (matdata, matkey) {
											if (bcdata.materialRefId == matdata.id) {
												selectedMaterial = matdata;
												selectedMaterial.barcode = vm.barcode;
												vm.materialRefId = matdata.id;
												vm.materialRefName = matdata.materialName;
												return true;
											}
										});
										return true;
									}
								});
							}
							else {
								if (argSelectedMaterial == null)
									return;
								selectedMaterial = argSelectedMaterial;
								if (selectedMaterial.barcode != null && selectedMaterial.barcode != '') {
									vm.barcode = selectedMaterial.barcode;
								}
								else {
									var existFlag = false;
									vm.refbarcodelist.some(function (bcdata, bckey) {
										if (bcdata.materialRefId == selectedMaterial.id) {
											vm.barcode = bcdata.barcode;
											existFlag = true;
											return true;
										}
									});
								}
							}
						

                if (selectedMaterial != null) {
										vm.currentMaterial = selectedMaterial;
										vm.materialRefId = selectedMaterial.id;
										vm.materialRefName = selectedMaterial.materialName;
                    vm.selectedItem = {
                        originalObject: selectedMaterial
                    };
                }
                else {
                    abp.notify.warn(app.localize('Barcode') + ' ' + vm.barcode + ' ' + app.localize('NotExist'));
                    vm.selectedItem = null;
                    vm.currentMaterial = null;
                    vm.barcode = null;
                    //$("#barcode").focus();
                    $("#barcode").focus();
                    return;
                }

                var forLoopFlag = true;
                angular.forEach(vm.closingstockPortions, function (value, key) {
                    if (angular.equals(selectedMaterial.id, value.materialRefId)) {
                        if (!vm.acceptDuplicateFlag)
                            abp.notify.warn(app.localize('SameMaterialAlreadyExists',vm.currentMaterial.materialName,value.onHand));
                        forLoopFlag = false;
                    }
                });

                if (forLoopFlag == false && vm.acceptDuplicateFlag == false) {
                    vm.barcode = null;
                    vm.currentMaterial = null;
                    vm.selectedItem = null;
										selectedMaterial = null;
										vm.materialRefId = null;
										vm.materialRefName = null;
										vm.uilimit = 50;
                    $("#barcode").focus();
                    return;
                }

            };

            vm.addItemToOrder = function () {
							if (vm.currentMaterial == null) {
								if (vm.barcode == '' || vm.barcode == null || vm.materialRefId == null)
                    {
											abp.notify.warn(app.localize('Barcode') + ' ' + vm.barcode + ' ' + app.localize('NotExist'));
											abp.notify.warn(app.localize('Material') + ' ' + vm.materialRefName + ' ' + app.localize('NotExist'));
											vm.barcode = null;
											vm.selectedItem = {
												originalObject: null
											};
											vm.selectedItem = null;
											vm.selectedQuantity = "";
											vm.currentMaterial = null;
											vm.materialRefId = null;
											vm.materialRefName = null;
											vm.uilimit = 50;
											$scope.clearInput();
											vm.notefocushappened = false;
											$("#barcode").focus();
											vm.selectedItem = null;
                      return;
                    }

                    var selectedMaterial = null;
                    vm.refmaterial.some(function (refdata, refkey) {
                        if (refdata.barcode == vm.barcode) {
                            selectedMaterial = refdata;
                            return true;
                        }
                    });

                    if (selectedMaterial != null) {
                        vm.currentMaterial = selectedMaterial;
                        vm.selectedItem = {
                            originalObject: selectedMaterial
                        };
                    }
                    else {
                        $scope.setMyFocus("barcode");
                        abp.notify.warn(app.localize('Barcode') + ' ' + vm.barcode + ' ' + app.localize('NotExist'));
                        //abp.notify.warn(app.localize('Barcode') + ' ' + app.localize('NotExist'));
                        vm.selectedItem = null;
                        vm.currentMaterial = null;
                        return;
                    }
                }

                //  Check whether the Product Already Exists or not 
                angular.forEach(vm.closingstockPortions, function (value, key) {
                    vm.closingstockPortions[key].alreadyExists = false;
                    if (value.materialRefName == vm.currentMaterial.materialName) {
                        vm.closingstockPortions[key].alreadyExists = true;
                    }
                });

								if (vm.selectedQuantity == 0 || vm.selectedQuantity == null) {
                    abp.notify.warn(app.localize("NoQuantity"));
                    $scope.setMyFocus("searQuantity");
                    return;
                }

             
                
                var mat = vm.currentMaterial;

                vm.closingstockPortions.push({
                    'sno': vm.sno,
                    'barcode': mat.barcode,
                    'materialRefId': mat.id,
                    'materialRefName': mat.materialName,
                    //'defaultUnit': value.unitRefName,
                    //'defaultUnitId': value.unitRefId,
                    'onHand': vm.selectedQuantity,
                    'unitRefId': mat.unitRefId,
                    'unitRefName': mat.defaultUnitName,
                    'changeunitflag': false,
                    'alreadyExists': false
                });
                vm.sno = vm.sno + 1;

                vm.barcode = null;
                vm.selectedItem = null;
                vm.selectedQuantity = "";
								vm.currentMaterial = null;
								vm.materialRefId = null;
								vm.materialRefName = null;
								vm.uilimit = 50;
                $scope.clearInput();
                vm.notefocushappened = false;
                $("#barcode").focus();
            };

            $scope.clearInput = function (id) {
                if (id) {
                    $scope.$broadcast("angucomplete-alt:clearInput", id);
                } else {
                    $scope.$broadcast("angucomplete-alt:clearInput");
                }
            };

            vm.menuSearch = function (str) {
                var matches = [];
                vm.menuForLocation.forEach(function (menu) {
                    if ((menu.materialName.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
                    (menu.barcode != null && menu.barcode.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)) {
                        matches.push(menu);
                    }
                });
                return matches;
            };

            vm.pullDetails = function () {
                locationService.getLocationById({ Id: vm.locationCode }).success(function (result) {
                    if (result == null) {
                        abp.notify.warn(app.localize('Location') + ' ' + app.localize('NotExist'));
                        return;
                    }

                    vm.location = result;
                    $scope.minDate = moment(result.houseTransactionDate);

                    $('input[name="closingDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: moment(),
                        minDate: $scope.minDate,
                        maxDate: moment()
                    });

                }).finally(function () {
                    init();
                });
            }

            vm.pullDetails();

        }]);
})();