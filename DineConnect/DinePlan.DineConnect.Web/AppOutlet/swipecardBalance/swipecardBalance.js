﻿(function () {
	appOutletModule.controller('host.views.swipe.transaction.membercard.swipecardBalance', [
		'$scope', '$state', '$stateParams', 'abp.services.app.memberCard',
		function ($scope, $state, $stateParams, membercardService) {

            var vm = this;
            vm.memberDetails = null;
						vm.detailData = [];
						vm.cardRefId = null;
						vm.lastNRecords = 10;
						vm.membercard = null;

						vm.isUndefinedOrNull = function (val) {
							return angular.isUndefined(val) || val == null;
						};


						$scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
						$scope.format = $scope.formats[0];

						var todayAsString = moment().format('YYYY-MM-DD');
						$('input[name="reportDate"]').daterangepicker({
							locale: {
								format: $scope.format
							},
							showDropdowns: true,
							startDate: todayAsString,
							endDate: todayAsString
						});


						vm.dateRangeOptions = app.createDateRangePickerOptions();

						vm.dateRangeModel = {
							startDate: todayAsString,
							endDate: todayAsString
						};

            vm.fillTransaction = function () {
               
                vm.memberDetails = null;
								vm.cardDetail = null;
								vm.memberCardList = [];
								vm.detailData = [];

								if (vm.cardNumber == '' || vm.cardNumber == null) {
                    abp.notify.error(app.localize('CardNumber') + " ?");
                    return;
								}

								var StartDate = null;
								var EndDate = null;
								if (vm.datefilterFlag) {
									StartDate = moment(vm.dateRangeModel.startDate).format($scope.format);
									EndDate = moment(vm.dateRangeModel.endDate).format($scope.format);
								}

								if (vm.isUndefinedOrNull(vm.lastNRecords)) {
									vm.lastNRecords = 10;
								}
								
                vm.loading = true;
								
                membercardService.getCardTransactionList(
									{
										cardNumber : vm.cardNumber,
										startDate: StartDate,
										endDate: EndDate,
										lastNRecord : vm.lastNRecords
                }).success(function (result) {
									if (result != null) {
										vm.memberDetails = result;
										vm.memberDetails.cardDetails.some(function (refdata, refkey) {
											if (refdata.cardRefId == vm.cardRefId) {
												vm.cardDetail = refdata;
												vm.memberCardList = vm.cardDetail.memberCards;
												return true;
											}


											refdata.memberCards.some(function (memdata, memkey) {
												if (memdata.cardRefId == vm.cardRefId) {
													vm.cardDetail = refdata;
													vm.memberCardList = vm.cardDetail.memberCards;
													vm.detailData = vm.cardDetail.cardTransactionDetails;
													return true;
												}
											});

											if (vm.detailData != null)
												return true;
										});

									}
                }).finally(function () {
                    vm.loading = false;
                });
            }


						vm.verifyCard = function () {
							if (vm.cardNumber == '' || vm.cardNumber == null) {
								vm.cardRefId = null;
								return;
							}
							if (vm.cardnumberblurneeded == false)
								return;

							vm.memberCardList = [];
							vm.uilimit = null;
							vm.selectedCard = null;
							vm.refCards.some(function (refdata, refkey) {
								if (refdata.cardNumber == vm.membercard.cardNumber) {
									vm.selectedCard = refdata;
									return true;
								}
							});

							vm.cardnumberblurneeded = false;
							if (vm.selectedCard == null)       //  No More Records Found In Card , Means New Card Issue
							{
								return;
							}

							vm.membercard.cardRefId = vm.selectedCard.id;
							vm.membercard.cardTypeRefId = vm.selectedCard.cardTypeRefId;
							vm.membercard.canIssueWithOutMember = vm.selectedCard.canIssueWithOutMember;
							vm.membercard.isEmployeeCard = vm.selectedCard.isEmployeeCard;

							if (vm.membercard.canIssueWithOutMember == true) {
								vm.membercard.memberRefId = null;
								vm.membercard.memberCode = null;
								vm.membercard.id = null;
							}
							//vm.getCardBalance();
							vm.fillTransaction();
							return;

							//vm.uilimit = null;
							//var selectedMemberCard = null;

							//vm.refMemberCards.some(function (refdata, refkey) {
							//	if (refdata.cardNumber == vm.membercard.cardNumber) {
							//		selectedMemberCard = refdata;
							//		return true;
							//	}
							//});

							//if (selectedMemberCard != null) {
							//	vm.depositAmountToBeCollected = 0;
							//	vm.depositNeedToCollectFlag = false;
							//	vm.membercard.memberRefId = selectedMemberCard.memberRefId;
							//	vm.membercard.isPrimaryCard = selectedMemberCard.isPrimaryCard;
							//	vm.membercard.primaryCardRefId = selectedMemberCard.primaryCardRefId;
							//	vm.membercard.id = selectedMemberCard.id;
							//	vm.primaryCardRefId = selectedMemberCard.primaryCardRefId;
							//	vm.membercard.depositAmount = selectedMemberCard.depositAmount;
							//	vm.membercard.employeeRefId = selectedMemberCard.employeeRefId;
							//	vm.membercard.employeeRefName = '';

							//	if (selectedMemberCard.employeeRefId != null) {
							//		var selectedEmployee;
							//		vm.refEmployees.some(function (refdata, refkey) {
							//			if (refdata.id == vm.membercard.employeeRefId) {
							//				vm.membercard.employeeRefName = refdata.employeeName;
							//				selectedEmployee = refdata;
							//				return true;
							//			}
							//		});
							//		if (selectedEmployee != null) {
							//			$scope.employeefunc(selectedEmployee);
							//		}
							//	}
							//	else {
							//		var selectedMember = null;
							//		vm.refSwipeMembers.some(function (refdata, refkey) {
							//			if (refdata.id == vm.membercard.memberRefId) {
							//				selectedMember = refdata;
							//				return true;
							//			}
							//		});

							//		if (selectedMember != null) {
							//			$scope.memberfunc(selectedMember);
							//		}
							//	}

							//	$('#topup').focus();
							//}
							//else {
							//	vm.membercard.memberRefId = null;
							//	vm.membercard.id = null;
							//	vm.membercard.isPrimaryCard = true;
							//	vm.membercard.primaryCardRefId = null;
							//	vm.membercard.depositAmount = 0;
							//	vm.membercard.employeeRefId = null;
							//	vm.membercard.employeeRefName = '';

							//	//About Collecting Deposit
							//	if (vm.selectedCard.depositAmount > 0) {
							//		abp.notify.info(app.localize('DepositNeedToCollect', vm.selectedCard.depositAmount));
							//		vm.depositAmountToBeCollected = vm.selectedCard.depositAmount;
							//		vm.depositNeedToCollectFlag = true;
							//	}
							//	else {
							//		vm.depositAmountToBeCollected = 0;
							//		vm.depositNeedToCollectFlag = false;
							//	}
							//	$scope.$broadcast('UiSelectDemo1');
							//}
						}
        }
    ]);
})();