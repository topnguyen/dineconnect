﻿(function () {
    appOutletModule.controller('common.views.noAuthLayout', [
        '$scope',
        function ($scope) {
            $scope.$on('$viewContentLoaded', function () {
                App.initComponents(); // init core components
            });
        }
    ]);
})();