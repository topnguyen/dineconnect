﻿(function () {
    appOutletModule.controller('common.views.outlet.dashboard', [
        '$scope', '$state', '$stateParams', 'abp.services.app.location',
        function($scope, $state, $stateParams, locationService) {
            var vm = this;
            vm.locationCode = $stateParams.id;

            $scope.$on('$viewContentLoaded', function() {
                App.initAjax();
            });

            vm.gotoStock = function () {
                $state.go("stock", {
                    id: vm.locationCode
                });
            }

            vm.gotoPurchaseOrder = function () {
                $state.go("invoice", {
                    id: vm.locationCode
                });
            }

            vm.pullDetails = function () {
                vm.loading = true;
                locationService.getLocationById({ Id: vm.locationCode }).success(function (result) {
                    vm.location = result;
                }).finally(function () {
                    vm.loading = false;
                });
            }
            vm.pullDetails();


       }]);
})();