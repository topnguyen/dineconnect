﻿(function () {
    appOutletModule.controller('common.views.outlet.po', [
        '$scope', '$state', '$stateParams', 'abp.services.app.location',
        function($scope, $state, $stateParams, locationService) {
            var vm = this;
            vm.locationCode = $stateParams.id;

            $scope.$on('$viewContentLoaded', function() {
                App.initAjax();
            });

            vm.gotoHome = function () {
                $state.go("dashboard", {
                    id: vm.locationCode
                });
            }


            vm.pullDetails = function () {
                vm.loading = true;
                locationService.getLocationById({ Id: vm.locationCode }).success(function (result) {
                    vm.location = result;
                }).finally(function () {
                    vm.loading = false;
                });
            }
            vm.pullDetails();


       }]);
})();