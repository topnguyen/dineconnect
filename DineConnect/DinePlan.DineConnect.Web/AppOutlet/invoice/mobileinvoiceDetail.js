﻿(function () {
    appOutletModule.controller('common.views.outlet.invoice.mobileinvoiceDetail', [
        '$scope', '$filter', '$state', '$stateParams', 'abp.services.app.invoice',
        'abp.services.app.supplierMaterial', 'abp.services.app.location', 'abp.services.app.material',
        'abp.services.app.unitConversion', "$window", 'abp.services.app.dayClose',
        function ($scope, $filter, $state, $stateParams, invoiceService, suppliermaterialService, locationService, materialService, unitconversionService, $window, daycloseService) {
            var vm = this;
            vm.detailloading = false;
            /* eslint-disable */

            vm.saving = false;
            vm.invoice = null;
            $scope.existall = true;
            vm.invoiceId = null;// $stateParams.id;
            vm.invoiceType = $stateParams.invoiceType;
            vm.loadingcount = 0;

            $scope.setMyFocus = function (str) {
                var element = $window.document.getElementById(str);
                element.focus();
            };


            vm.selectedItem = null;
            vm.currentMaterial = null;


            vm.focAlertMessageFlag = false;
            vm.reftax = [];
            vm.uilimit = null;
            vm.allTimeDcs = true;
            vm.refdctranid = [];
            vm.invoicemode = [];
            vm.invoicePortions = [];
            vm.refunit = [];
            vm.sno = 0;
            vm.invoicedetail = [];
            vm.availableQty = 0;
            vm.checkFinished = false;
            vm.totalTaxAmount = 0;
            vm.invoiceTotalAmount = 0;
            vm.locationCode = $stateParams.id;

            //vm.ratechangeallowedflag = abp.setting.getBoolean("App.House.InvoiceSupplierRateChangeFlag");
            //vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            //vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.defaultLocationId = vm.locationCode; //appSession.user.locationRefId;
            vm.mutlipleUomAllowed = true; //appSession.user.multipleUomEntryAllowed;

            //if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
            //    abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
            //    return;
            //}

            vm.temptaxinfolist = [];

            $scope.minDate = moment().add(-90, 'days');  // -30 or -60 Sysadmin Table 
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];



            $('input[name="invoiceDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });

            vm.tempinvoicedetail = [];

            vm.invoicedirectcreditlink = [];
            vm.materialTotAmt = 0;

            vm.invoicemode.push(
                { 'modeno': 1, 'invmode': 'CREDIT' }, { 'modeno': 2, 'invmode': 'CASH' });

            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();

            //vm.AccountdateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.material = {
                'sno': vm.sno,
                'dcTranid': 0,
                'barcode': '',
                'materialRefId': 0,
                'materialRefName': '',
                'totalQty': '',
                'price': '',
                'discountFlag': false,
                'discountPercentage': 0,
                'discountAmount': 0,
                'totalAmount': 0,
                'taxAmount': 0,
                'netAmount': 0,
                'remarks': '',
                'taxForMaterial': null,
                'uom': '',
                'unitRefId': 0,
                'changeunitflag': false,
                'applicableTaxes': []
            };


            vm.pullDetails = function () {
                vm.loading = true;
                locationService.getLocationById({ Id: vm.locationCode }).success(function (result) {
                    if (result == null) {
                        abp.notify.warn(app.localize('Location') + ' ' + app.localize('NotExist'));
                        return;
                    }

                    vm.location = result;
                    $scope.minDate = moment(result.houseTransactionDate);

                    $('input[name="closingDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: moment(),
                        minDate: $scope.minDate,
                        maxDate: moment()
                    });

                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.pullDetails();

            //////---------------- Direct Invoice

            $scope.func = function (data, val) {
                var forLoopFlag = true;

                angular.forEach(vm.invoicePortions, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialRefName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.materialRefId = null;
                    data.barcode = '';
                    data.materialRefName = '';
                    return;
                }

                data.materialRefId = val.materialRefId;
                data.materialRefName = val.materialRefName;
                data.uom = val.uom;
                data.defaultUnitId = val.defaultUnitId;
                data.price = parseFloat(val.materialPrice);
                data.initialprice = parseFloat(val.materialPrice);
                data.unitRefName = val.uom;
                data.unitRefId = val.unitRefId;
                data.barcode = val.barcode;

                if (data.price == 0 || data.price == null)
                    data.rateeditflag = true;
                else
                    data.rateeditflag = vm.ratechangeallowedflag;

                vm.temptaxinfolist = [];

                angular.forEach(val.taxForMaterial, function (value, key) {
                    vm.temptaxinfolist.push({
                        'materialRefId': value.materialRefId,
                        'taxRefId': value.taxRefId,
                        'taxName': value.taxName,
                        'taxRate': value.taxRate,
                        'taxValue': value.taxValue,
                        'rounding': value.rounding,
                        'sortOrder': value.sortOrder,
                        'taxCalculationMethod': value.taxCalculationMethod
                    });
                });
                data.taxForMaterial = vm.temptaxinfolist;
                data.applicableTaxes = val.applicableTaxes;
                data.unitRefId = val.unitRefId;
                data.barcode = val.barcode;
                //fillDropDownUnitBasedOnMaterial(data.materialRefId, data);
            };

            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

            vm.unitReference = function (selected, data) {
                data.unitRefId = selected.value;
                data.unitRefName = selected.displayText;
                data.changeunitflag = false;

                if (data.defaultUnitId == data.unitRefId) {
                    data.price = data.initialprice;
                }
                else {
                    var convFactor = 0;
                    var convExistFlag = false;
                    vm.refconversionunit.some(function (refdata, refkey) {
                        if (refdata.baseUnitId == data.defaultUnitId && refdata.refUnitId == data.unitRefId) {
                            convFactor = refdata.conversion;
                            convExistFlag = true;
                            return true;
                        }
                    });


                    if (convExistFlag == false) {
                        abp.notify.info(app.localize('ConversionFactorNotExist', data.uom, data.unitRefName));
                        return;
                    }

                    var converstionUnitPrice = data.initialprice * 1 / convFactor;
                    data.price = converstionUnitPrice.toFixed(2);
                }
                vm.amtCalc(data, data.totalQty, data.price);
                vm.calculateTotalVlaue(vm.invoicePortions);
            }

            $scope.taxCalc = function (data, totalQty, price) {
                if (vm.isUndefinedOrNull(totalQty))
                    totalQty = 0;
                if (vm.isUndefinedOrNull(price) || price == '')
                    price = 0;

                data.totalAmount = parseFloat(price) * parseFloat(totalQty);
                data.totalAmount = parseFloat(data.totalAmount.toFixed(2));

                data.taxAmount = 0;
                data.taxForMaterial = [];

                taxneedstoremove = [];

                if (data.applicableTaxes.length > 0)    //  Calculate Tax 
                {
                    angular.forEach(data.applicableTaxes, function (value, key) {
                        var taxCalculationOnValue = 0.0;

                        if (value.taxCalculationMethod == 'GT' || value.taxCalculationMethod == 'ST') {
                            taxCalculationOnValue = data.totalAmount;
                        }
                        else {
                            var taxrefvalueflag = false;
                            data.taxForMaterial.some(function (taxdata, taxkey) {
                                if (taxdata.taxName == value.taxCalculationMethod) {
                                    taxCalculationOnValue = taxdata.taxValue;
                                    taxrefvalueflag = true;
                                    return true;
                                }
                            });

                            if (taxrefvalueflag == false)
                                taxneedstoremove.push({ 'taxName': value.taxName });
                        }

                        if (taxCalculationOnValue > 0) {
                            var taxamt = parseFloat(taxCalculationOnValue * value.taxRate / 100);
                            taxamt = parseFloat(taxamt.toFixed(2));
                            data.taxAmount = parseFloat(data.taxAmount) + parseFloat(taxamt);
                            data.taxAmount = parseFloat(data.taxAmount.toFixed(2));

                            if (taxamt > 0) {
                                data.taxForMaterial.push
                                    ({
                                        'materialRefId': data.materialRefId,
                                        'taxRefId': value.taxRefId,
                                        'taxName': value.taxName,
                                        'taxRate': value.taxRate,
                                        'taxValue': taxamt,
                                        'rounding': value.rounding,
                                        'sortOrder': value.sortOrder,
                                        'taxCalculationMethod': value.taxCalculationMethod
                                    });
                            }
                        }
                    });
                }


                angular.forEach(taxneedstoremove, function (value, key) {
                    angular.forEach(data.applicableTaxes, function (detvalue, detkey) {
                        if (value.taxName == detvalue.taxName)
                            data.applicableTaxes.splice(detkey, 1);
                    });
                });

                data.netAmount = parseFloat(parseFloat(data.totalAmount).toFixed(2)) + parseFloat(parseFloat(data.taxAmount).toFixed(2));
                vm.calculateTotalVlaue(vm.invoicePortions);
            }

            vm.amtCalc = function (data, totalQty, price) {

                $scope.taxCalc(data, totalQty, price);
                return;
            }

            vm.refconversionunit = [];
            vm.fillUnitConversion = function () {
                unitconversionService.getAll({
                }).success(function (result) {
                    vm.refconversionunit = result.items;
                });
            }

            vm.fillUnitConversion();

            vm.refmaterial = [];
            vm.fillDropDownMaterial = function () {
                vm.detailloading = true;
                vm.loading = true;
                vm.loadingcount++;
                suppliermaterialService.getMaterialBasedOnSupplierWithPrice({
                    locationRefId: vm.invoice.locationRefId,
                    supplierRefId: vm.invoice.supplierRefId
                }).success(function (result) {
                    vm.refmaterial = result.items;
                }).finally(function () {
                    //if (vm.invoice.isDirectInvoice && vm.invoicePortions.length==0 )
                    //    vm.addPortion();
                    vm.detailloading = false;
                    vm.loading = false;
                    vm.loadingcount--;
                });
            }

            vm.refbarcodelist = [];
            function fillDropDownBarCodeList() {
                vm.loading = true;
                vm.loadingcount++;
                vm.refbarcodelist = [];
                materialService.getBarcodesForMaterial().success(function (result) {
                    vm.refbarcodelist = result.barcodeList;
                    vm.loading = false;
                    vm.loadingcount--;
                });
            }
            fillDropDownBarCodeList();




            vm.reftax = [];
            vm.fillDropDownTaxes = function () {
                suppliermaterialService.getTaxForMaterial()
                    .success(function (result) {
                        vm.reftax = result;
                    }).finally(function () {

                    });
            }


            vm.getMaterialQuotePrice = function (data, supmaterial) {

                if (data.materialRefId > 0) {
                    if (vm.invoice.id == null) {
                        data.price = parseFloat(supmaterial.materialPrice);

                        if (data.price == 0)
                            data.rateeditflag = true;
                        else
                            data.rateeditflag = vm.ratechangeallowedflag;
                    }
                    data.uom = supmaterial.uom;
                }
                return parseInt(supmaterial.materialRefId);
            };

            vm.addPortion = function () {

                var errorFlag = false;
                var value = vm.invoicePortions[vm.invoicePortions.length - 1];

                if (vm.sno > 0) {
                    var value = vm.invoicePortions[vm.invoicePortions.length - 1];
                    //value = value[0];

                    if (vm.existAllElements() == false)
                        return;

                    if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MaterialRequired'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.materialRefName)) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MaterialRequired'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.totalQty) || value.totalQty == 0) {
                        errorFlag = true;
                        abp.notify.warn(app.localize('InvalidOrderQty'));
                        return;
                    }

                    if ((vm.isUndefinedOrNull(value.price) || value.price == 0) && vm.invoice.isFoc == false) {
                        errorFlag = true;
                        abp.notify.warn(app.localize('InvalidMaterialPrice') + " / " + app.localize('FOC') + " ?");
                        return;
                    }

                    if ((vm.isUndefinedOrNull(value.totalAmount) || value.totalAmount == 0) && vm.invoice.isFoc == false) {
                        errorFlag = true;
                        abp.notify.warn(app.localize('Amount'));
                        return;
                    }
                    if ((vm.isUndefinedOrNull(value.netAmount) || value.netAmount == 0) && vm.invoice.isFoc == false) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialTotalAmt'));
                        return;
                    }

                }

                vm.refunit = [];

                vm.sno = vm.sno + 1;

                vm.invoicePortions.push({
                    'sno': vm.sno,
                    'dcTranid': 0,
                    'barcode': '',
                    'materialRefId': 0,
                    'materialRefName': '',
                    'totalQty': '',
                    'price': '',
                    'discountFlag': false,
                    'discountPercentage': 0,
                    'discountAmount': 0,
                    'totalAmount': 0,
                    'taxAmount': 0,
                    'netAmount': 0,
                    'remarks': '',
                    'taxForMaterial': null,
                    'uom': '',
                    'unitRefId': 0,
                    'changeunitflag': false,
                    'applicableTaxes': []
                });

                vm.calculateTotalVlaue(vm.invoicePortions);
                //$('input')[$('input').index(this) + 1].focus();
            }
            /////------------------------------------------

            vm.existAllElements = function () {
                $scope.existall = true;
                $scope.errmessage = "";

                if (vm.invoice.supplierRefId == null || vm.invoice.supplierRefId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('SupplierErr');
                    $('#selsupplierRefId').focus();
                }

                if (vm.invoice.invoiceDate == null) {
                    $scope.errmessage = $scope.errmessage + app.localize('InvoiceDateErr');
                    $('#invoicedate').focus();
                }

                if (vm.invoice.accountDate == null) {
                    $scope.errmessage = $scope.errmessage + app.localize('AccountDateErr');
                    $('#invoicedate').focus();
                }

                if (vm.invoice.invoiceNumber == null || vm.invoice.invoiceNumber.length == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('InvoiceNumberErr');
                    $('#invoiceno').focus();
                }

                if (vm.invoice.locationRefId == null || vm.invoice.locationRefId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('LocationErr');
                    $('#defaultLocation').focus();
                }

                if (vm.invoice.dcFromDate == null || vm.invoice.dcToDate == null) {
                    vm.invoice.dcFromDate = vm.invoice.accountDate;
                    vm.invoice.dcToDate = vm.invoice.accountDate;
                }

                if ($scope.errmessage != "") {
                    $scope.existall = false;
                    abp.notify.warn($scope.errmessage, 'Required');
                    return false;
                }
                else
                    return true;

            };

            vm.getDetailMaterial = function () {
                vm.checkFinished = false;
                vm.uilimit = null;
                if (vm.existAllElements() == false)
                    return;

                invoiceService.getMaterialsFromDcId({
                    dcRefId: vm.tempinvoicedetail.dcTranid,
                    supplierRefId: vm.invoice.supplierRefId,
                    locationRefId: vm.defaultLocationId
                }).success(function (result) {

                    angular.forEach(result.dcDetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.invoicePortions.push({
                            'sno': vm.sno,
                            'dcTranid': value.dcTranId,
                            'barcode': value.barcode,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'totalQty': value.totalQty,
                            'price': value.price,
                            'initialprice': value.price,
                            'discountFlag': value.discountFlag,
                            'discountPercentage': value.discountPercentage,
                            'discountAmount': parseFloat(vm.isUndefinedOrNull(value.discountAmount) ? 0 : value.discountAmount),
                            'totalAmount': parseFloat(parseFloat(value.totalAmount).toFixed(2)),
                            'taxAmount': parseFloat(parseFloat(value.taxAmount).toFixed(2)),
                            'netAmount': parseFloat(parseFloat(value.netAmount).toFixed(2)),
                            'remarks': value.remarks,
                            'taxForMaterial': value.taxForMaterial,
                            'uom': value.uom,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'changeunitflag': false,
                            'applicableTaxes': value.applicableTaxes,
                            'rateeditflag': vm.ratechangeallowedflag,
                            'defaultUnitId': value.defaultUnitId,
                            'defaultUnitName': value.defaultUnitName
                        });


                        vm.calculateTotalVlaue(vm.invoicePortions);

                        vm.refdctranid.some(function (value, key) {
                            if (parseInt(value.value) == parseInt(vm.tempinvoicedetail.dcTranid)) {
                                vm.refdctranid.splice(key, 1);
                                return true;
                            }
                        });

                        vm.tempinvoicedetail = [];

                    });
                });
            }


            vm.next = function () {
                vm.checkFinished = false;
                if (vm.existAllElements() == false)
                    return;

                var value = vm.tempinvoicedetail;

                var errorFlag = false;

                if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
                    errorFlag = true;
                    abp.notify.info(app.localize('MinimumOneDetail'));
                    return;
                }

                if (vm.isUndefinedOrNull(value.materialRefName) || value.materialRefName == 0) {
                    errorFlag = true;
                    abp.notify.info(app.localize('MaterialRequired'));
                    return;
                }

                if (vm.isUndefinedOrNull(value.totalQty) || value.totalQty == 0) {
                    errorFlag = true;
                    abp.notify.info(app.localize('InvalidInvoiceQty'));
                    return;
                }
                if ((vm.isUndefinedOrNull(value.price) || value.price == 0) && vm.invoice.isFoc == false) {
                    errorFlag = true;
                    abp.notify.info(app.localize('InvalidMaterialPrice'));
                    return;
                }
                if ((vm.isUndefinedOrNull(value.totalAmount) || value.totalAmount == 0) && vm.invoice.isFoc == false) {
                    errorFlag = true;
                    abp.notify.info(app.localize('InvalidMaterialTotalAmt'));
                    return;
                }

                if (vm.isUndefinedOrNull(value.dcTranid)) {
                    errorFlag = true;
                    abp.notify.info(app.localize('PleaseSelectDCTranId'));
                    return;
                }

                if ($("#discFlag").is(":checked")) {
                    if (vm.isUndefinedOrNull(value.discountAmount) || value.discountAmount == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidDiscountAmt'));
                        return;
                    }
                }

                if (errorFlag) {
                    vm.saving = false;
                    vm.loading = false;
                    $scope.existall = false;
                }

                vm.sno = vm.sno + 1;
                var netamt = parseFloat(vm.isUndefinedOrNull(value.totalAmount) ? 0 : value.totalAmount) - parseFloat(vm.isUndefinedOrNull(value.discountAmount) ? 0 : value.discountAmount);

                vm.invoicePortions.push({
                    'sno': vm.sno,
                    'dcTranid': value.dcTranid,
                    'barcode': value.barcode,
                    'materialRefId': value.materialRefId,
                    'materialRefName': value.materialRefName,
                    'totalQty': value.totalQty,
                    'price': value.price,
                    'discountFlag': value.discountFlag,
                    'discountPercentage': value.discountPercentage,
                    'discountAmount': parseFloat(vm.isUndefinedOrNull(value.discountAmount) ? 0 : value.discountAmount),
                    'totalAmount': parseFloat(parseFloat(value.totalAmount).toFixed(2)),
                    'taxAmount': parseFloat(parseFloat(value.taxAmount).toFixed(2)),
                    'netAmount': parseFloat(parseFloat(netAmount).toFixed(2)),
                    'remarks': value.remarks,
                    'taxForMaterial': value.taxForMaterial,
                    'uom': value.uom,
                    'unitRefId': value.unitRefId,
                    'changeunitflag': false,
                    'applicableTaxes': value.applicableTaxes
                });

                vm.tempinvoicedetail = [];
                vm.calculateTotalVlaue(vm.invoicePortions);
            };

            $scope.amtCalc = function (totalQty, price) {
                if (vm.isUndefinedOrNull(totalQty))
                    totalQty = 0;
                if (vm.isUndefinedOrNull(price))
                    price = 0;
                vm.tempinvoicedetail.totalAmount = price * totalQty;
            }

            $scope.convert = function (thedate) {
                var tempstr = thedate.toString();
                var newstr = tempstr.toString().replace(/GMT.*/g, "")
                newstr = newstr + " UTC";
                return new Date(newstr);
            };

            vm.invoiceAlreadyExists = function () {
                if (vm.invoice.invoiceNumber) {
                    var invoiceDate = $scope.convert(moment(vm.invoice.invoiceDate, $scope.format));

                    var resDate = $scope.convert(vm.invoice.invoiceDate);
                    invoiceService.invoiceAlreadyExists(
                        {
                            supplierRefId: vm.invoice.supplierRefId,
                            locationRefId: vm.defaultLocationId,
                            invoiceNumber: vm.invoice.invoiceNumber,
                            startDate: invoiceDate,
                            endDate: invoiceDate
                        }).success(function (result) {
                            vm.alreadyExistInvoice = result.items;
                            if (result.totalCount > 0) {
                                abp.notify.warn(app.localize('InvoiceAlreadyExists', result.items[0].invoiceNumber));
                                vm.invoice.invoiceNumber = null;
                            }
                        });
                }
            }

            $scope.filterDcDate = function (val) {
                if (vm.invoice.supplierRefId > 0) {


                    if (moment(val).isValid()) {
                        if (vm.allDcShow) {
                            vm.invoice.dcFromDate = null;
                            vm.invoice.dcToDate = null;
                        }
                        else {
                            vm.invoice.dcFromDate = moment(val.startDate).format('YYYY-MM-DD');
                            vm.invoice.dcToDate = moment(val.endDate).format('YYYY-MM-DD');
                        }

                        vm.dateRangeModel.startDate = vm.invoice.dcFromDate;
                        vm.dateRangeModel.endDate = vm.invoice.dcToDate;

                        fillDcLists(vm.dateRangeModel);
                    }
                }
            }

            $scope.funcMat = function (val) {
                var forLoopFlag = true;


                angular.forEach(vm.invoicePortions, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    vm.tempinvoicedetail.materialRefId = 0;
                    vm.tempinvoicedetail.barcode = '';
                    vm.tempinvoicedetail.materialRefName = '';
                    return;
                }
                vm.tempinvoicedetail.materialRefName = val;
            };

            vm.refdctranid = [];

            function fillDcLists(daterange) {
                vm.uilimit = null;
                invoiceService.setDcCompleteStatus({
                    id: vm.invoice.supplierRefId
                }).success(function (result) {
                    //abp.notify.info('Completed');
                });


                invoiceService.getDcTranId($.extend({
                    allTimeDcs: vm.allTimeDcs,
                    supplierRefId: vm.invoice.supplierRefId,
                    locationRefId: vm.defaultLocationId
                }, daterange)).success(function (result) {
                    vm.refdctranid = result.items;
                    // vm.reflocation.unshift({ value: "0", displayText: app.localize('NotAssigned') });
                });
            }

            $scope.checkPendingQty = function (enteredQty) {
                if (parseFloat(enteredQty) == parseFloat(vm.availableQty))
                    return;
                else if (parseFloat(enteredQty) > parseFloat(vm.availableQty)) {
                    abp.message.warn(app.localize("QtyErr", enteredQty, vm.availableQty), "Error");
                    vm.tempinvoicedetail.totalQty = 0;
                    $("#totqty").focus();
                }
            }

            vm.getDcQty = function (dcitem) {
                vm.availableQty = parseFloat(dcitem.pendingQty);
                return parseInt(dcitem.materialRefId);
            };

            vm.refdcmaterial = [];

            vm.fillMaterialBasedOnInwardDC = function (selValue) {
                invoiceService.getMaterialForInwardDc({ Id: selValue }).success(function (result) {
                    //vm.refdcmaterial = result.items;
                    vm.refdcmaterial = result;
                });
            }

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };




            vm.calculateTotalVlaue = function (data) {
                var tempTotDiscAmt = 0;
                var tempTotInvAmt = 0;
                var tempTotTaxAmt = 0;
                var tempTotalAmount = 0;

                angular.forEach(data, function (val, key) {
                    data[key].changeunitflag = false;
                    tempTotDiscAmt = parseFloat(tempTotDiscAmt) + parseFloat(vm.isUndefinedOrNull(val.discountAmount) ? 0 : val.discountAmount);
                    tempTotTaxAmt = parseFloat(tempTotTaxAmt) + parseFloat(parseFloat(vm.isUndefinedOrNull(val.taxAmount) ? 0 : val.taxAmount).toFixed(2));
                    tempTotInvAmt = parseFloat(tempTotInvAmt) + parseFloat(parseFloat(vm.isUndefinedOrNull(val.netAmount) ? 0 : val.netAmount).toFixed(2));
                    tempTotalAmount = parseFloat(tempTotalAmount) + parseFloat(parseFloat(vm.isUndefinedOrNull(val.totalAmount) ? 0 : val.totalAmount).toFixed(2));
                });
                vm.invoiceTotalAmount = parseFloat(tempTotalAmount.toFixed(2));;
                vm.totalTaxAmount = parseFloat(tempTotTaxAmt.toFixed(2));

                if (vm.invoice.totalDiscountAmount == null)
                    vm.invoice.totalDiscountAmount = 0;

                if (vm.invoice.totalShipmentCharges == null)
                    vm.invoice.totalShipmentCharges = 0;

                if (vm.invoice.roundedAmount == null)
                    vm.invoice.roundedAmount = 0;

                if (vm.invoice.totalDiscountAmount > tempTotInvAmt) {
                    abp.notify.warn("DiscountInvoiceErr");
                    vm.invoice.totalDiscountAmount = 0;
                }

                if (vm.invoice.roundedAmount > tempTotInvAmt) {
                    abp.notify.warn("RoundedAmountInvoiceErr");
                    vm.invoice.roundedAmount = 0;
                }

                tempTotInvAmt = (tempTotInvAmt - parseFloat(vm.invoice.totalDiscountAmount) + parseFloat(vm.invoice.totalShipmentCharges) + parseFloat(vm.invoice.roundedAmount));

                vm.invoice.invoiceAmount = parseFloat(tempTotInvAmt.toFixed(2));
                //vm.invoice.totalDiscountAmount = parseFloat(tempTotDiscAmt.toFixed(2));
                //vm.invoice.totalShipmentCharges = parseFloat(vm.invoice.totalShipmentCharges.toFixed(2));
            }

            $scope.calcDiscountAmt = function (discperct, discamt) {
                if ($("#discFlag").is(":checked")) {
                    if (vm.isUndefinedOrNull(discperct))
                        discperct = 0;
                    if (vm.isUndefinedOrNull(discamt))
                        discamt = 0;

                    vm.tempinvoicedetail.discountAmount = parseFloat($filter('number')(vm.tempinvoicedetail.totalAmount * (discperct / 100), 2));
                }

            }

            vm.discFlagCheckBox = function () {
                if (!$("#discFlag").is(":checked")) {
                    vm.tempinvoicedetail.discountAmount = 0;
                    vm.tempinvoicedetail.discountPercentage = 0;
                }
            }

            $scope.calcDiscountPerc = function (discperct, discamt) {
                if ($("#discFlag").is(":checked")) {
                    if (vm.isUndefinedOrNull(discperct))
                        discperct = 0;
                    if (vm.isUndefinedOrNull(discamt))
                        discamt = 0;

                    vm.tempinvoicedetail.discountPercentage = parseFloat($filter('number')((discamt / vm.tempinvoicedetail.totalAmount) * 100, 2));
                }
            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex) {

                vm.invoicePortions.splice(productIndex, 1);
                vm.sno = vm.sno - 1;
                vm.calculateTotalVlaue(vm.invoicePortions);
                //if (vm.invoicePortions.length == 0)
                //    vm.addPortion();

            }

            vm.inwardDcTranIdRef = function (selDcId) {
                vm.checkFinished = false;
                if (selDcId > 0) {
                    vm.fillMaterialBasedOnInwardDC(selDcId);
                    //vm.getDetailMaterial();
                }
            }

            vm.save = function () {
                if ($scope.existall == false)
                    return;

                if (vm.existAllElements() == false)
                    return;

                if (parseFloat(vm.invoice.invoiceAmount) == 0 && vm.invoice.isFoc == false) {
                    abp.message.warn(app.localize("EmptyDetail"));
                    return;
                }

                if (parseFloat(vm.invoice.invoiceamoutenteredbyuser) != parseFloat(vm.invoice.invoiceAmount)) {
                    abp.message.warn(app.localize("InvoiceAmtNotMatchedWithAutomaticCalculation"));
                    return;
                }

                vm.saving = true;
                vm.invoicedetail = [];
                vm.discFlag = 0;

                errorFlag = false;

                angular.forEach(vm.invoicePortions, function (value, key) {
                    if (value.discountFlag == true || value.discountFlag == 1)
                        vm.discFlag = 1;
                    else
                        vm.discFlag = 0;

                    if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.materialRefName)) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MaterialRequired'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.totalQty) || value.totalQty == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidOrderQty'));
                        return;
                    }

                    if ((vm.isUndefinedOrNull(value.price) || value.price == 0) && vm.invoice.isFoc == false) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialPrice') + ' - ' + value.materialRefName);
                        return;
                    }

                    if ((vm.isUndefinedOrNull(value.price) || value.price == 0) && vm.focAlertMessageFlag == false) {
                        vm.focAlertMessageFlag = true;
                        errorFlag = true;
                        return;
                    }



                    if ((vm.isUndefinedOrNull(value.totalAmount) || value.totalAmount == 0) && vm.invoice.isFoc == false) {
                        errorFlag = true;
                        abp.notify.warn(app.localize('Amount'));
                        return;
                    }
                    if ((vm.isUndefinedOrNull(value.netAmount) || value.netAmount == 0) && vm.invoice.isFoc == false) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialTotalAmt'));
                        return;
                    }


                    vm.invoicedetail.push({
                        'sno': value.sno,
                        'materialRefId': value.materialRefId,
                        'barcode': value.barcode,
                        'totalQty': value.totalQty,
                        'price': value.price,
                        'discountFlag': vm.discFlag,
                        'discountPercentage': value.discountPercentage,
                        'discountAmount': value.discountAmount,
                        'totalAmount': value.totalAmount,
                        'taxAmount': value.taxAmount,
                        'netAmount': value.netAmount,
                        'remarks': value.remarks,
                        'taxForMaterial': value.taxForMaterial,
                        'applicableTaxes': value.applicableTaxes,
                        'uom': value.uom,
                        'unitRefId': value.unitRefId,
                        'changeunitflag': false,
                    });
                });

                if (errorFlag == true) {
                    vm.saving = false;
                    return;
                }


                //invoicedirectcreditlink
                vm.invoicedirectcreditlink = [];
                if (vm.invoice.isDirectInvoice == false) {
                    angular.forEach(vm.invoicePortions, function (value, key) {
                        vm.invoicedirectcreditlink.push({
                            'invoiceRefId': 0,
                            'sno': value.sno,
                            'materialRefId': value.materialRefId,
                            'inwardDirectCreditRefId': value.dcTranid,
                            'materialConvertedQty': value.totalQty,
                        });
                    });
                }

                ////vm.invoice.accountDate = vm.invoice.invoiceDate;
                vm.invoice.invoiceMode = "CREDIT";

                invoiceService.createOrUpdateInvoiceDraft({
                    invoice: vm.invoice,
                    invoicedetail: vm.invoicedetail,
                    invoiceDirectCreditLink: vm.invoicedirectcreditlink
                }).success(function (result) {

                    abp.notify.info(app.localize('Draft') + ' ' + app.localize('Invoice') + ' ' + app.localize('SavedSuccessfully'));

                    abp.message.success(app.localize('InvoiceDraftSuccessMessage', result.invoiceDraftReference));

                    //resetField();
                    vm.cancel(0);

                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.printInvoice = function (objId) {
                //$rootScope.settings.layout.pageSidebarClosed = false;
                $state.go('tenant.invoiceprintdetail', {
                    printid: objId
                });
            };

            function resetField() {
                vm.invoicePortions = [];
                vm.invoicedetail = [];
                vm.tempinvoicedetail = [];
                vm.invoiceDate = moment().format($scope.format);
                vm.invoice.invoiceMode = '';
                vm.AccountDate = moment().format($scope.format)
                vm.invoice.supplierRefId = 0;
                vm.invoice.invoiceNumber = '';
                vm.DcFromDate = moment().format($scope.format)
                vm.DcToDate = moment().format($scope.format)
                vm.invoice.totalDiscountAmount = 0;
                vm.invoice.totalShipmentCharges = 0;
                vm.invoice.roundedAmount = 0;
                vm.invoice.invoiceAmount = 0;
                vm.dcTranid = '';
                vm.invoice.invoiceamoutenteredbyuser = 0;
                vm.sno = 0;
            }

            vm.checkAmountWithAuto = function () {
                vm.calculateTotalVlaue(vm.invoicePortions);

                if (parseFloat(vm.invoice.invoiceAmount) == 0 && vm.invoice.isFoc == false) {
                    abp.notify.warn(app.localize('EmptyDetail'))
                    vm.checkFinished = false;
                    return;
                }

                if (parseFloat(vm.invoice.invoiceAmount) == parseFloat(vm.invoice.invoiceamoutenteredbyuser)) {
                    vm.invoice.checkingStatus = app.localize("MatchingCorrect")
                }
                else {
                    vm.invoice.checkingStatus = app.localize("MatchingFailed")
                    abp.notify.error(app.localize('MatchingFailed'));
                }
                vm.checkFinished = true;
            }

            vm.existall = function () {

                if (vm.invoice.id == null) {
                    vm.existall = false;
                    return;
                }

                invoiceService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.invoice.id,
                    operation: 'SEARCH',
                    locationRefId: vm.defaultLocationId
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.invoice.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.invoice.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function (argOption) {
                if (argOption == 1) {
                    abp.message.confirm(
                        app.localize('CancelEntry?'),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                $state.go("dashboard", {
                                    id: vm.locationCode
                                });
                            }
                        }
                    );
                }
                else {
                    $state.go("dashboard", {
                        id: vm.locationCode
                    });
                }
            };


            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.reflocation = [];

            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;

                });
            }

            vm.refsupplier = [];

            function fillDropDownSupplier() {
                vm.loading = true;
                vm.loadingcount++;
                suppliermaterialService.getSupplierForCombobox({}).success(function (result) {
                    vm.refsupplier = result.items;
                    vm.loading = false;
                    vm.loadingcount--;
                });
            }

            vm.refpurchasecategory = [];
            function fillDropDownPurchaseCategory() {
                invoiceService.getPurchaseCategoryForCombobox({}).success(function (result) {
                    vm.refpurchasecategory = result.items;
                });
            }

            fillDropDownPurchaseCategory();


            vm.checkCloseDay = function () {
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        abp.notify.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                    vm.invoice.accountDate = moment(result.accountDate).format($scope.format);

                    $('input[name="accountDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.invoice.accountDate,
                        maxDate: vm.invoice.accountDate,
                    });

                    $('input[name="invoiceDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.invoice.accountDate,
                        minDate: $scope.minDate,
                        maxDate: moment()
                    });



                }).finally(function (result) {

                });
            }

            function init() {
                fillDropDownLocation();
                fillDropDownSupplier();
                vm.fillDropDownTaxes();
                vm.detailloading = true;
                vm.errorFlag = true;



                invoiceService.getInvoiceForEdit({
                    Id: vm.invoiceId
                }).success(function (result) {
                    vm.errorFlag = false;
                    vm.invoice = result.invoice;
                    vm.invoice.locationRefId = vm.defaultLocationId;
                    vm.invoice.invoiceamoutenteredbyuser = 0;

                    var todayAsString = moment().format('YYYY-MM-DD');

                    vm.dateRangeModel = {
                        startDate: todayAsString,
                        endDate: todayAsString
                    };

                    $scope.filterDcDate(vm.dateRangeModel);

                    if (result.invoice.id != null) {
                        vm.uilimit = null;
                        if (vm.invoice.isDirectInvoice == true) {
                            vm.invoiceType = 'Direct';
                        }

                        vm.invoice.accountDate = moment(result.invoice.accountDate).format($scope.format);
                        vm.invoice.invoiceDate = moment(result.invoice.invoiceDate).format($scope.format);
                        $scope.minDate = vm.invoice.invoiceDate;

                        vm.fillDropDownMaterial();

                        $('input[name="invoiceDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: moment(),
                            maxDate: moment()
                        });

                        vm.invoicedetail = result.invoiceDetail;
                        vm.invoicedirectcreditlink = result.invoiceDirectCreditLink;
                        vm.refdctranid = result.invoiceDirectCreditLink;

                        angular.forEach(vm.invoicedetail, function (value, key) {
                            vm.invoicePortions.push({
                                'sno': value.sno,
                                'dcTranid': value.dcTranId,
                                'barcode': value.barcode,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'totalQty': value.totalQty,
                                'price': value.price,
                                'discountFlag': value.discountFlag,
                                'discountPercentage': value.discountPercentage,
                                'discountAmount': parseFloat(vm.isUndefinedOrNull(value.discountAmount) ? 0 : value.discountAmount),
                                'totalAmount': parseFloat(parseFloat(value.totalAmount).toFixed(2)),
                                'taxAmount': parseFloat(parseFloat(value.taxAmount).toFixed(2)),
                                'netAmount': parseFloat(parseFloat(value.netAmount).toFixed(2)),
                                'remarks': value.remarks,
                                'taxForMaterial': value.taxForMaterial,
                                'uom': value.uom,
                                'unitRefId': value.unitRefId,
                                'changeunitflag': false,
                                'applicableTaxes': value.applicableTaxes
                            });
                        });
                        abp.ui.clearBusy('#invoiceForm');
                        vm.calculateTotalVlaue(vm.invoicePortions);
                        vm.invoice.invoiceamoutenteredbyuser = parseFloat(vm.invoice.invoiceAmount).toFixed(2);
                        vm.checkAmountWithAuto();
                    }
                    else {
                        vm.invoice.invoiceDate = moment().format($scope.format);
                        //if (vm.invoiceType == null || vm.invoiceType == "")
                        //    vm.invoice.isDirectInvoice = false;
                        //else
                        //    vm.invoice.isDirectInvoice = true;

                        vm.invoice.isDirectInvoice = true;
                        vm.invoice.isFoc = false;

                        vm.invoice.totalDiscountAmount = 0;
                        vm.invoice.totalShipmentCharges = 0;
                        vm.invoice.roundedAmount = 0;
                        vm.checkCloseDay();
                        vm.uilimit = 20;
                    }
                    vm.detailloading = false;
                }).finally(function () {
                    if (vm.errorFlag == true)
                        vm.cancel(0);
                });


            }

            init();

            vm.selectMaterialBasedOnBarcode = function (argOption, data, givenmaterial) {
                if (argOption == 1) {	//	BarCode Selection
                    if (vm.material.barcode == '' || vm.material.barcode == null) {
                        //abp.notify.warn(app.localize('Barcode') + ' ' + vm.material.barcode + ' ' + app.localize('NotExist'));
                        vm.currentMaterial = null;
                        vm.selectedItem = null;
                        vm.material.totalQty = null;
                        //$("#barcode").focus();
                        return;
                    }


                    vm.uilimit = null;
                    var selectedMaterial = null;
                    vm.refmaterial.some(function (refdata, refkey) {
                        if (refdata.barcode == vm.material.barcode) {
                            selectedMaterial = refdata;
                            return true;
                        }
                    });

                    vm.refbarcodelist.some(function (bcdata, bckey) {
                        if (bcdata.barcode == vm.material.barcode) {
                            vm.refmaterial.some(function (matdata, matkey) {
                                if (bcdata.materialRefId == matdata.materialRefId) {
                                    selectedMaterial = matdata;
                                    selectedMaterial.barcode = vm.material.barcode;
                                    return true;
                                }
                            });
                            return true;
                        }
                    });
                }
                else {
                    if (givenmaterial == null)
                        return;
                    selectedMaterial = givenmaterial;
                    if (selectedMaterial.barcode != null && selectedMaterial.barcode != '') {
                        vm.material.barcode = selectedMaterial.barcode;
                    }
                    else {
                        var existFlag = false;
                        vm.refbarcodelist.some(function (bcdata, bckey) {
                            if (bcdata.materialRefId == selectedMaterial.id) {
                                vm.material.barcode = bcdata.barcode;
                                existFlag = true;
                                return true;
                            }
                        });
                    }
                }

                if (selectedMaterial != null) {
                    vm.currentMaterial = selectedMaterial;
                    vm.selectedItem = {
                        originalObject: selectedMaterial
                    };
                }
                else {
                    abp.notify.warn(app.localize('Barcode') + ' ' + vm.material.barcode + ' ' + app.localize('NotExist'));
                    vm.selectedItem = null;
                    vm.currentMaterial = null;
                    vm.material.barcode = null;
                    //$("#barcode").focus();
                    return;
                }

                var forLoopFlag = true;
                angular.forEach(vm.invoicePortions, function (value, key) {
                    if (angular.equals(vm.currentMaterial.materialRefName, value.materialRefName)) {
                        abp.notify.warn(app.localize('SameMaterialAlreadyExists', vm.currentMaterial.materialRefName, value.totalQty));
                        forLoopFlag = false;
                    }
                });

                if (forLoopFlag == false) {
                    vm.material.barcode = null;
                    vm.currentMaterial = null;
                    vm.selectedItem = null;
                    $("#barcode").focus();
                    return;
                }

                val = vm.currentMaterial;

                data.materialRefId = val.materialRefId;
                data.materialRefName = val.materialRefName;
                vm.material.materialRefName = val.materialRefName;
                data.uom = val.uom;
                data.defaultUnitId = val.defaultUnitId;
                data.price = parseFloat(val.materialPrice);
                data.initialprice = parseFloat(val.materialPrice);
                data.unitRefName = val.uom;
                data.unitRefId = val.unitRefId;
                data.barcode = val.barcode;

                if (data.price == 0 || data.price == null) {
                    data.rateeditflag = true;
                    data.price = null;
                }
                else
                    data.rateeditflag = vm.ratechangeallowedflag;

                vm.temptaxinfolist = [];

                angular.forEach(val.taxForMaterial, function (value, key) {
                    vm.temptaxinfolist.push({
                        'materialRefId': value.materialRefId,
                        'taxRefId': value.taxRefId,
                        'taxName': value.taxName,
                        'taxRate': value.taxRate,
                        'taxValue': value.taxValue,
                        'rounding': value.rounding,
                        'sortOrder': value.sortOrder,
                        'taxCalculationMethod': value.taxCalculationMethod
                    });
                });
                data.taxForMaterial = vm.temptaxinfolist;
                data.applicableTaxes = val.applicableTaxes;
            };

            vm.addItemToOrder = function () {
                if (vm.currentMaterial == null) {
                    if (vm.material.barcode == '' || vm.material.barcode == null) {
                        abp.notify.warn(app.localize('Barcode') + ' ' + vm.material.barcode + ' ' + app.localize('NotExist'));
                        vm.currentMaterial = null;
                        vm.selectedItem = null;
                        vm.material.totalQty = null;
                        $("#barcode").focus();
                        return;
                    }

                    var selectedMaterial = null;
                    vm.refmaterial.some(function (refdata, refkey) {
                        if (refdata.barcode == vm.material.barcode) {
                            selectedMaterial = refdata;
                            return true;
                        }
                    });

                    if (selectedMaterial != null) {
                        vm.currentMaterial = selectedMaterial;
                        vm.selectedItem = {
                            originalObject: selectedMaterial
                        };
                    }
                    else {
                        $scope.setMyFocus("barcode");
                        abp.notify.warn(app.localize('Barcode') + ' ' + vm.material.barcode + ' ' + app.localize('NotExist'));
                        //abp.notify.warn(app.localize('Barcode') + ' ' + app.localize('NotExist'));
                        vm.selectedItem = null;
                        vm.currentMaterial = null;
                        return;
                    }
                }

                if (vm.material.price == null || vm.material.price == 0) {
                    abp.notify.warn(app.localize('Price') + " ?");
                    $scope.setMyFocus("txtprice");
                    return;
                }

                if (vm.material.totalQty == null || vm.material.totalQty == 0) {
                    abp.notify.warn(app.localize('Quantity') + " ?");
                    $scope.setMyFocus("searQuantity");
                    return;
                }
                //  Check whether the Product Already Exists or not 
                angular.forEach(vm.invoicePortions, function (value, key) {
                    vm.invoicePortions[key].alreadyExists = false;
                    if (value.materialRefName == vm.currentMaterial.materialName) {
                        vm.invoicePortions[key].alreadyExists = true;
                    }
                });

                if (vm.material.totalQty == 0) {
                    abp.notify.warn(app.localize("NoQuantity"));
                    $scope.setMyFocus("searQuantity");
                    return;
                }



                var mat = vm.currentMaterial;

                //vm.invoicePortions.push({
                //    'sno': vm.sno,
                //    'barcode': mat.barcode,
                //    'materialRefId': mat.materialRefId,
                //    'materialRefName': mat.materialRefName,
                //    //'defaultUnit': value.unitRefName,
                //    //'defaultUnitId': value.unitRefId,
                //    'onHand': vm.material.totalQty,
                //    'unitRefId': mat.unitRefId,
                //    'unitRefName': mat.unitRefName,
                //    'changeunitflag': false,
                //    'alreadyExists': false
                //});


                vm.amtCalc(vm.material, vm.material.totalQty, vm.material.price);
                vm.invoicePortions.push(vm.material);

                vm.sno = vm.sno + 1;

                //vm.material.barcode = null;
                //vm.selectedItem = null;
                //vm.material.totalQty = null;
                //vm.material.price = null;

                vm.material = {
                    'sno': vm.sno,
                    'dcTranid': 0,
                    'barcode': '',
                    'materialRefId': 0,
                    'materialRefName': '',
                    'totalQty': '',
                    'price': '',
                    'discountFlag': false,
                    'discountPercentage': 0,
                    'discountAmount': 0,
                    'totalAmount': 0,
                    'taxAmount': 0,
                    'netAmount': 0,
                    'remarks': '',
                    'taxForMaterial': null,
                    'uom': '',
                    'unitRefId': 0,
                    'changeunitflag': false,
                    'applicableTaxes': []
                };

                vm.calculateTotalVlaue(vm.invoicePortions);

                vm.currentMaterial = null;

                //$scope.clearInput();

                $("#barcode").focus();
            };

            vm.supplierKeyPress = function (objId, $event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    //alert(objId);
                    $("#invoiceDate").focus();
                }
            };

            vm.barcodeKeyPress = function (objId, $event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    //alert(objId);
                    $("#searQuantity").focus();
                }
            };

            vm.invoiceNumberKeyPress = function (objId, $event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    $("#barcode").focus();
                }
            };

            vm.searquantityKeyPress = function (objId, $event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    //alert(objId);
                    //vm.addItemToOrder();
                    $("#txtprice").focus();
                }
            };

            vm.priceKeyPress = function (objId, $event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    //alert(objId);
                    vm.addItemToOrder();
                }
            };


        }
    ]);
})();

