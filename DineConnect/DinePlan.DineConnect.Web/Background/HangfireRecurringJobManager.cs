﻿using Abp.BackgroundJobs;
using Abp.Hangfire.Configuration;
using Abp.Threading.BackgroundWorkers;
using Hangfire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Background
{
    public class HangfireRecurringJobManager : BackgroundWorkerBase, IRecurringJobManager
    {

        public HangfireRecurringJobManager()
        {
        }

        //public override void Start()
        //{
        //    base.Start();

        //    if (hangfireConfiguration.Server == null && backgroundJobConfiguration.IsJobExecutionEnabled)
        //    {
        //        hangfireConfiguration.Server = new BackgroundJobServer();
        //    }
        //}

        //public override void WaitToStop()
        //{
        //    if (hangfireConfiguration.Server != null)
        //    {
        //        try
        //        {
        //            hangfireConfiguration.Server.Dispose();
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.Warn(ex.ToString(), ex);
        //        }
        //    }

        //    base.WaitToStop();
        //}

        public Task AddOrUpdateAsync<TJob, TArgs>(TArgs args, string cronExpressions, BackgroundJobPriority priority = BackgroundJobPriority.Normal)
            where TJob : IBackgroundJob<TArgs>
        {
            RecurringJob.AddOrUpdate<TJob>(job => job.Execute(args), cronExpressions);
            return Task.FromResult(0);
        }
    }
}
