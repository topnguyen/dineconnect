﻿using Abp.Web.Mvc.Views;

namespace DinePlan.DineConnect.Web.Views
{
    public abstract class DineConnectWebViewPageBase : DineConnectWebViewPageBase<dynamic>
    {

    }

    public abstract class DineConnectWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected DineConnectWebViewPageBase()
        {
            LocalizationSourceName = DineConnectConsts.LocalizationSourceName;
        }
    }
}