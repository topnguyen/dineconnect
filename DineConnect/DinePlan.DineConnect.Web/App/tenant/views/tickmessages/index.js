﻿(function () {
    appModule.controller('tenant.views.tickmessages.index', ['$rootScope', '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.tickMessage',
        function ($rootScope, $scope, $state, $modal, uiGridConstants, tickMessageService) {
            var vm = this;
            $rootScope.settings.layout.pageSidebarClosed = false;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.isDeleted = false;
            vm.noFilter = true;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.TickMessage.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.TickMessage.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.TickMessage.Delete')
            };

            $scope.formats = ['YYYY-MM-DD', 'DD-MM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.dateRangeOptions = app.createFlexibleDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: moment().format("YYYY-MM-DD"),
                endDate: moment().format("YYYY-MM-DD")
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.edit" ng-click="grid.appScope.editMessage(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.delete" ng-click="grid.appScope.deleteMessage(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.isDeleted && grid.appScope.permissions.edit" ng-click="grid.appScope.activateItem(row.entity)">' + app.localize('Revert') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('MessageHeading'),
                        field: 'messageHeading'
                    },
                    {
                        name: app.localize('Type'),
                        field: 'messageType',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.messageType == 0" >' + app.localize('Text') + '</span>' +
                            '  <span ng-show="row.entity.messageType == 1" >' + app.localize('Image') + '</span>' +
                            '  <span ng-show="row.entity.messageType == 2" >' + app.localize('Video') + '</span>' +
                            '  <span ng-show="row.entity.messageType == 3" >' + app.localize('PDF') + '</span>' +
                            '  <span ng-show="row.entity.messageType == 4" >' + app.localize('Word') + '</span>' +
                            '  <span ng-show="row.entity.messageType == 5" >' + app.localize('Excel') + '</span>' +
                            '</div>'
                    },
                    {
                        name: app.localize('MessageUntil'),
                        field: 'toDate',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD\'',
                        minWidth: 100
                    },
                    {
                        name: app.localize('Acknowledgement'),
                        field: 'acknowledgement'
                    },
                    {
                        name: app.localize('OneTimeMessage'),
                        field: 'oneTimeMessage'
                    },
                    {
                        name: app.localize('Acknowledged'),
                        enableSorting: false,
                        cellTemplate:
                            '<div ng-if="row.entity.acknowledgement" class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.openAcknowledgedModel(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Acknowledged') + '"><i class="fa fa-dashcube"></i></button>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Replies'),
                        enableSorting: false,
                        cellTemplate:
                            '<div ng-if="row.entity.reply" class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.openRepliesModel(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Replies') + '"><i class="fa fa-dashcube"></i></button>' +
                            '</div>'
                    },
                    {
                        name: app.localize('NonReplies'),
                        enableSorting: false,
                        cellTemplate:
                            '<div ng-if="row.entity.reply" class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.openNonRepliesModel(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('NonReplies') + '"><i class="fa fa-dashcube"></i></button>' +
                            '</div>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            function openDetailCategory(objId) {
                $state.go("tenant.detailtickmessage", {
                    id: objId
                });
            }

            vm.editMessage = function (myObj) {
                openDetailCategory(myObj.id);
            };

            vm.createMessage = function () {
                openDetailCategory(null);
            };

            vm.getAll = function () {
                vm.loading = true;
                tickMessageService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    location: vm.currentLocation ? vm.currentLocation.name : null,
                    fromDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    toDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    isDeleted: vm.isDeleted,
                    locationGroup: vm.locationGroup,
                    noFilter: vm.noFilter
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.deleteMessage = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteTickMessageWarning', myObject.message),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            tickMessageService.deleteMessage({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.exportToExcel = function () {
                console.log('exportToExcel');
                tickMessageService.getTickMessageToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    location: vm.currentLocation ? vm.currentLocation.name : null,
                    fromDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    toDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    isDeleted: vm.isDeleted,
                    locationGroup: vm.locationGroup,
                    noFilter: vm.noFilter
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.currentLocation = null;
            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForCreate();
            };
            vm.intiailizeOpenLocation();
            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.currentLocation = result.locations[0];
                        vm.location = result.locations[0].id;
                    } else {
                        vm.location = 0;
                        vm.currentLocation = null;
                    }
                });
            };
            vm.clear = function () {
                vm.currentLocation = null;
                vm.filterText = null;
                vm.intiailizeOpenLocation();
                vm.getAll();
            };
            vm.openAcknowledgedModel = function (myObj) {
                vm.openLocationModel(myObj.id, false, true);
            };

            vm.openRepliesModel = function (myObj) {
                vm.openLocationModel(myObj.id, true);
            };

            vm.openNonRepliesModel = function (myObj) {
                vm.openLocationModel(myObj.id, false, false, true);
            };

            vm.openLocationModel = function (tickId, isReply, acknowledged, isNonReply) {
                $modal.open({
                    templateUrl: "~/App/tenant/views/tickmessages/reply.cshtml",
                    controller: "tenant.views.tickmessages.reply as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        isUnread: function () {
                            return false;
                        },
                        isAllLocations: function () {
                            return false;
                        },
                        isReply: function () {
                            return isReply;
                        },
                        acknowledged: function () {
                            return acknowledged;
                        },
                        tickMessageId: function () {
                            return tickId;
                        },
                        isNonReply: function () {
                            return isNonReply;
                        }
                    }
                });
            };

            vm.activateItem = function (myObject) {
                tickMessageService.activateItem({
                    id: myObject.id
                }).success(function (result) {
                    abp.notify.success(app.localize("Successfully"));
                    vm.getAll();
                });
            };

            vm.getAll();

            vm.preventNonNumericalInput = function (e) {
                e = e || window.event;
                var charCode = (typeof e.which === "undefined") ? e.keyCode : e.which;
                var charStr = String.fromCharCode(charCode);

                if (!charStr.match(/^[0-9-]+$/))
                    e.preventDefault();
            };

        }]);
})();