﻿(function () {
    appModule.controller("tenant.views.tickmessages.reply",
        ["$scope", "$uibModalInstance", "uiGridConstants", "abp.services.app.tickMessage", "tickMessageId", "isReply", "acknowledged", "isNonReply", "isUnread","isAllLocations",
            function ($scope, $modalInstance, uiGridConstants, tickMessageService, tickMessageId, isReply, acknowledged, isNonReply, isUnread, isAllLocations) {
                //console.log(location);
                /* eslint-disable */
                var vm = this;
                vm.tickMessageId = tickMessageId;
                vm.isReply = isReply;
                vm.acknowledged = acknowledged;
                vm.isNonReply = isNonReply;
                vm.isUnread = isUnread;
                vm.isAllLocations = isAllLocations;

                if (vm.acknowledged || vm.isNonReply || vm.isAllLocations || vm.isUnread) {
                    vm.locationOptions = {
                        enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                        enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                        paginationPageSizes: app.consts.grid.defaultPageSizes,
                        paginationPageSize: app.consts.grid.defaultPageSize,
                        useExternalPagination: true,
                        useExternalSorting: true,
                        appScopeProvider: vm,
                        rowTemplate:
                            "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                        columnDefs: [
                            {
                                name: app.localize("Id"),
                                field: "id"
                            },
                            {
                                name: app.localize("Code"),
                                field: "code"
                            },
                            {
                                name: app.localize("Name"),
                                field: "name"
                            }
                        ],
                        onRegisterApi: function (gridApi) {
                        },
                        data: []
                    };
                } else {
                    vm.locationOptions = {
                        enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                        enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                        paginationPageSizes: app.consts.grid.defaultPageSizes,
                        paginationPageSize: app.consts.grid.defaultPageSize,
                        useExternalPagination: true,
                        useExternalSorting: true,
                        appScopeProvider: vm,
                        rowTemplate:
                            "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                        columnDefs: [
                            {
                                name: app.localize("Id"),
                                field: "locationId"
                            },
                            {
                                name: app.localize("Code"),
                                field: "locationCode"
                            },
                            {
                                name: app.localize("Name"),
                                field: "locationName"
                            },
                            {
                                name: app.localize("Reply"),
                                field: "reply"
                            }
                        ],
                        onRegisterApi: function (gridApi) {
                        },
                        data: []
                    };
                }

                vm.getLocations = function () {
                    vm.loading = true;
                    if (vm.isUnread) {
                        tickMessageService.getUnreadLocations({ id: vm.tickMessageId })
                            .success(function (result) {
                                vm.locationOptions.totalItems = result.items.length;
                                vm.locationOptions.data = result.items;
                            }).finally(function () {
                                vm.loading = false;
                            });
                        return;
                    }

                    if (vm.acknowledged) {
                        tickMessageService.getAcknowledgedLocations({ id: vm.tickMessageId })
                            .success(function (result) {
                                vm.locationOptions.totalItems = result.items.length;
                                vm.locationOptions.data = result.items;
                            }).finally(function () {
                                vm.loading = false;
                            });
                        return;
                    }

                    if (isNonReply) {
                        tickMessageService.getNonReplyLocations({ id: vm.tickMessageId })
                            .success(function (result) {
                                vm.locationOptions.totalItems = result.items.length;
                                vm.locationOptions.data = result.items;
                            }).finally(function () {
                                vm.loading = false;
                            });
                        return;
                    }
                    if (isReply) {
                        tickMessageService.getReplyLocations({ id: vm.tickMessageId })
                            .success(function (result) {
                                vm.locationOptions.totalItems = result.items.length;
                                vm.locationOptions.data = result.items;
                            }).finally(function () {
                                vm.loading = false;
                            });
                        return;
                    }

                    if (isAllLocations) {
                        tickMessageService.getMappedLocations({ id: vm.tickMessageId })
                            .success(function (result) {
                                vm.locationOptions.totalItems = result.items.length;
                                vm.locationOptions.data = result.items;
                            }).finally(function () {
                                vm.loading = false;
                            });
                        return;
                    }
                };

                vm.cancel = function () {
                    $modalInstance.dismiss();
                };

                vm.getLocations();
            }
        ]);
})();