﻿(function () {
    appModule.controller('tenant.views.tickmessages.detail', [
        '$scope', "$uibModal", '$state', '$stateParams', 'abp.services.app.tickMessage', 'FileUploader',
        function ($scope, $modal, $state, $stateParams, tickService, fileUploader) {
            var vm = this;
            vm.tickMessageId = $stateParams.id;
            vm.messageTypes = [];
            vm.replyFiles = [];

            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createDateRangeFuturePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.save = function () {
                
                vm.tickMessage.fromDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                vm.tickMessage.toDate = moment(vm.dateRangeModel.endDate).format($scope.format);

                if (vm.tickMessage.messageType !== 0) {
                    if (vm.replyFiles.length==0) {
                        abp.notify.error(app.localize("UploadFileErr"));
                        return;
                    }
                    else
                    {
                        vm.tickMessage.messageContent = angular.toJson(vm.replyFiles);
                        vm.uploader.uploadAll();
                    }
                    
                }
                if (!vm.tickMessage.acknowledgement) {
                    vm.tickMessage.reply = null;
                }
                vm.saving = true;
                tickService.createOrUpdateMessage({
                    tickMessage: vm.tickMessage,
                    locationGroup: vm.locationGroup
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $state.go('tenant.tickmessage');
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.tickmessage');
            };

            function init() {
                tickService.getTicketMessageForEdit({
                    id: vm.tickMessageId
                }).success(function (result) {
                    vm.tickMessage = result.tickMessage;
                    vm.locationGroup = result.locationGroup;
                    vm.dateRangeModel = {
                        startDate: result.tickMessage.fromDate || todayAsString,
                        endDate: result.tickMessage.toDate || todayAsString
                    };

                    if (vm.tickMessage.messageType !== 0) {
                        vm.replyFiles = vm.tickMessage.messageContent ? angular.fromJson(vm.tickMessage.messageContent) : [];
                    }
                });

                tickService.getTickMessageTypes()
                    .success(function (result) {
                        vm.messageTypes = result.items;
                    });
            }

            init();
            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForCreate();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };

            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadFile'
            });

            vm.uploader.filters.push({
                name: 'imageFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    return true;
                }
            });

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (vm.replyFiles == null) {
                    vm.replyFiles = [];
                }
                if (response != null) {
                    if (response.result != null) {
                        if (response.result.fileName != null) {
                            vm.replyFiles.push(response.result);
                            vm.refreshImage = true;
                        }
                    }
                }
            };

            vm.uploader.onAfterAddingFile = function(item) {
                console.log(item);

                if (item.file.size > 6000000) {
                    abp.notify.error(app.localize('Size is Big'));
                    vm.uploader.clearQueue();
                    return;
                }
                if (vm.tickMessage.messageType == 1 && !item.file.type.startsWith('image')) {
                    abp.notify.error(app.localize('WrongFile'));
                    vm.uploader.clearQueue();
                    return;

                }
                if (vm.tickMessage.messageType == 2 && !item.file.type.startsWith('video')) {
                    abp.notify.error(app.localize('WrongFile'));
                    vm.uploader.clearQueue();
                    return;

                }
                if (vm.tickMessage.messageType == 3 && item.file.type != 'application/pdf') {
                    abp.notify.error(app.localize('WrongFile'));
                    vm.uploader.clearQueue();
                    return;

                }

                if (vm.tickMessage.messageType == 4 && item.file.type != 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                    abp.notify.error(app.localize('WrongFile'));
                    vm.uploader.clearQueue();
                    return;
                }

                if (vm.tickMessage.messageType == 5 && item.file.type != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                    abp.notify.error(app.localize('WrongFile'));
                    vm.uploader.clearQueue();
                    return;
                }

                item.upload();
            };

            vm.downloader = function (file) {
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + file.fileName+'&url='+file.fileSystemName;

            };

            vm.removeFile = function (findex) {
                vm.replyFiles.splice(findex, 1);
                vm.refreshImage = true;
            };

            vm.onChangedType = function () {
                vm.replyFiles = [];
                vm.uploader.queue = [];
            };

            vm.preventNonNumericalInput = function (e) {
                e = e || window.event;
                var charCode = (typeof e.which === "undefined") ? e.keyCode : e.which;
                var charStr = String.fromCharCode(charCode);

                if (!charStr.match(/^[0-9-]+$/))
                    e.preventDefault();
            };

            vm.chooseTimeInterval = function () {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/tickmessages/timeInterval.cshtml',
                    controller: 'tickmessages.timeInterval as vm',
                    backdrop: 'static',
                    resolve: {
                        input: function () {
                            return vm.tickMessage.timeIntervals;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.tickMessage.timeIntervals = result;
                });
            };
        }
    ]);
})();