﻿(function () {
    appModule.controller('tickmessages.timeInterval', [
        '$scope', '$uibModalInstance', 'input',
        function ($scope, $modalInstance, input) {
            var vm = this;
            vm.intervals = [];
            vm.currentTime = '';

            vm.ok = function () {
                var result = vm.intervals.join(', ');
                $modalInstance.close(result);
            };

            vm.close = function () {
                $modalInstance.dismiss();
            };
            vm.timeChanged = function () {
                if (vm.currentTime) {
                    var time = moment(new Date(vm.currentTime)).format('HH:mm:ss')
                    if (vm.intervals.indexOf(time) === -1) {
                        vm.intervals.push(time);

                        //Sort
                        vm.intervals = vm.intervals.sort();
                    }
                }
            };

            vm.removeRow = function (index) {
                vm.intervals.splice(index, 1);
            };
            
            vm.init = function () {
                if (input) {
                    var tags = input.split(', ');

                    angular.forEach(tags, function (item) {
                        if (item) {
                            vm.intervals.push(item);
                        }
                    });
                }
            };

            vm.init();
        }
    ]);
})();