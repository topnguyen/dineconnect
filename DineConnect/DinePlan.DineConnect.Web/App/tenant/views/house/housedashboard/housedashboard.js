﻿(function () {
    appModule.controller('tenant.views.house.housedashboard.housedashboard', [
        '$scope', 'appSession', 'abp.services.app.location', 'abp.services.app.supplier', 'abp.services.app.houseReport', 'NgTableParams',
        function ($scope, appSession, locationService, supplierService, housereportService, ngTableParams) {

            var vm = this;
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];
            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };
            vm.requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.alllocationflag = true;
            vm.allsupplierflag = true;
            vm.selectedSuppliers = [];
            vm.selectedLocations = [];
            vm.drillDownLevel = 2;

            vm.supplierTop = 10;
            vm.supplierChartType = 'BAR';

            vm.materialTop = 10;
            vm.materialChartType = 'PIE';

            vm.currentData = [];

            vm.currentLocation = appSession.location!=null?appSession.location.name:"";
            vm.currentLocationId = appSession.location!=null?appSession.location.id:0;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            $scope.getLValue = function (column) {
                return app.localize(column).toUpperCase();
            };

            vm.locations = [];
            vm.alllocations = [];
            vm.currentUserId = abp.session.userId;

            vm.getLocations = function () {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                    //if (vm.defaultLocationId != null && vm.defaultLocationId > 0) {
                    //    vm.locationRefId = vm.defaultLocationId;
                    //}
                }).finally(function (result) {

                });

                //locationService.getLocations({
                //}).success(function (result) {
                //    vm.alllocations = $.parseJSON(JSON.stringify(result.items));
                //}).finally(function (result) {
                //    //vm.dashBoardReport();
                //});

            };
            vm.getLocations();

            vm.suppliers = [];
            vm.getSuppliers = function () {
                supplierService.getAll({
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting
                }).success(function (result) {
                    vm.suppliers = $.parseJSON(JSON.stringify(result.items));

                }).finally(function (result) {
                });
            };
            vm.getSuppliers();

            vm.dashBoardReport = function () {
                vm.locationlist = vm.selectedLocations;
                if (vm.selectedLocations.length == 0)
                    vm.locationlist = vm.locations;
              
                if (vm.locationlist == null || vm.locationlist.length == 0) {
                    abp.notify.warn(app.localize('LocationErr'));
                    return;
                }

                vm.supplierlist = vm.selectedSuppliers;
                if (vm.selectedSuppliers.length == 0)
                    vm.supplierlist = vm.suppliers;

                if (vm.supplierlist == null || vm.supplierlist.length == 0) {
                    abp.notify.warn(app.localize('SupplierErr'));
                    return;
                }

                vm.loading = true;
                housereportService.getHouseDashboardReport({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    allLocationFlag : vm.alllocationflag,
                    locations: vm.locationlist,
                    allSupplierFlag : vm.allsupplierflag,
                    suppliers: vm.supplierlist,
                    drillDownLevel : vm.drillDownLevel
                }).success(function (result) {
                    vm.output = result;
                    vm.tableParams = new ngTableParams({}, { dataset: vm.output.salesAndExpenses, counts: [] });
                    console.log(vm.tableParams);
                    if (vm.output.salesAndExpenses.length > 0)
                        vm.graphDraw(vm.output.salesAndExpenses[0]);
                }).finally(function () {
                    vm.loading = false;
                });

              

            };

            vm.setJobDates = function () {

                var sd = moment().subtract(1, 'day');
                var td = moment().subtract(1, 'day');

                vm.dateRangeModel = {
                    startDate: sd,
                    endDate: td
                };

                vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
                vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;
            };
            vm.setJobDates();

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.dashBoardReportAsync = function () {
                vm.output = {
                    dateRemarks: '',
                    salesAndExpenses: [],
                };

                vm.materialChartData = [];
                vm.chartmaterialList = [];
                vm.qtyList = [];
                vm.chartpurchaseList = [];
                vm.supplierChartData = [];
                vm.chartsupplierList = [];
                vm.chartpurchaseList = [];

                vm.finalResultManipulation();
                vm.processStart = moment();


                vm.loading = true;
                vm.locationlist = vm.selectedLocations;
                if (vm.selectedLocations.length == 0)
                    vm.locationlist = vm.locations;

                if (vm.locationlist == null || vm.locationlist.length == 0) {
                    abp.notify.warn(app.localize('LocationErr'));
                    vm.loading = false;
                    return;
                }

                vm.supplierlist = vm.selectedSuppliers;
                if (vm.selectedSuppliers.length == 0)
                    vm.supplierlist = vm.suppliers;

                if (vm.supplierlist == null || vm.supplierlist.length == 0) {
                    abp.notify.warn(app.localize('SupplierErr'));
                    vm.loading = false;
                    return;
                }

                if (vm.drillDownLevel<=1)
                    vm.drillDownLevel = 2;

                if (vm.isUndefinedOrNull(vm.dateRangeModel.startDate))
                    vm.setJobDates();

                var startDate = angular.copy(vm.dateRangeModel.startDate);
                var endDate = angular.copy(vm.dateRangeModel.endDate);

                var diffInDays = endDate.diff(startDate, 'days') + 1;


                var DateRangeRemarks = "";
                if (diffInDays == 7)
                    DateRangeRemarks = app.localize("Weekly");
                else if (startDate.date()==1)
                {

                    var tempDate = angular.copy(vm.dateRangeModel.startDate);
                    tempDate = tempDate.add(1, 'months').subtract(1, 'days');
                    if (tempDate.format($scope.format) == endDate.format($scope.format))
                    {
                        DateRangeRemarks = app.localize("Monthly");
                    }
                    else if (endDate.format($scope.format) == moment().format($scope.format))
                    {
                        DateRangeRemarks = app.localize("ThisMonth");
                    }
                    else
                        DateRangeRemarks = diffInDays + app.localize("Days");
                }
                else
                    DateRangeRemarks = diffInDays + app.localize("Days");

                vm.output.dateRemarks = DateRangeRemarks;

                var fromDate = startDate;
                var toDate = endDate;

                vm.loading = true;

                for (var i = 1; i <= vm.drillDownLevel; i++)
                {
                    
                    housereportService.getPurchaseVsSalesReport({
                        startDate: moment(fromDate).format($scope.format),
                        endDate: moment(toDate).format($scope.format),
                        allLocationFlag: vm.alllocationflag,
                        locations: vm.locationlist,
                        allSupplierFlag: vm.allsupplierflag,
                        suppliers: vm.supplierlist,
                        drillDownLevel: vm.drillDownLevel,
                        currentLevel : i
                    }).success(function (result) {
                        vm.individualresult = result;
                        vm.output.salesAndExpenses.push(vm.individualresult);
                        
                    }).finally(function () {
                        if (vm.drillDownLevel == vm.output.salesAndExpenses.length) {
                            vm.processEnd = moment();
                            vm.finalResultManipulation();
                            vm.loading = false;
                        }
                    });

                  

                if (DateRangeRemarks==app.localize("Monthly"))
                {
                    var tempDate = fromDate.subtract(1, 'months');
                    month = tempDate.month();
                    year = tempDate.year();
                    fromDate = moment({ years: year, months: month, days: 1 });
                    toDate = angular.copy(fromDate);
                    toDate = toDate.add(1, 'months').subtract(1, 'days');
                }
                else if (DateRangeRemarks==app.localize("ThisMonth"))
                {
                    var tempDate = fromDate.subtract(1, 'months');
                    month = tempDate.month();
                    year = tempDate.year();
                    day = endDate.date(); 
                    fromDate = moment({ years: year, months: month, days: 1 });
                    toDate = moment({ years: year, months: month, days: day });
                }
                else
                {
                    fromDate = fromDate.subtract(diffInDays,'days');
                    toDate = toDate.subtract(diffInDays,'days');
                }
                  

            }
            };


            vm.finalResultManipulation = function()
            {
                if (vm.drillDownLevel == vm.output.salesAndExpenses.length) {

                    vm.output.salesAndExpenses = vm.output.salesAndExpenses.sort(function (a, b) {
                        if (a.currentLevel > b.currentLevel) {
                            return 1;
                        }
                        if (a.currentLevel < b.currentLevel) {
                            return -1;
                        }
                        // a must be equal to b
                        return 0;
                    });


                    var recCount = vm.output.salesAndExpenses.length - 1;
                    for (var i = recCount; i >= 0; i--) {
                        if (i == recCount)
                            continue;

                        var salesDifference = vm.output.salesAndExpenses[i].sales - vm.output.salesAndExpenses[i + 1].sales;
                        var salesDiffPercentage = 0;
                        if (vm.output.salesAndExpenses[i + 1].sales > 0)
                            salesDiffPercentage = salesDifference / vm.output.salesAndExpenses[i + 1].sales * 100;


                        var purchaseDifference = vm.output.salesAndExpenses[i].purchases - vm.output.salesAndExpenses[i + 1].purchases;
                        var purchaseDiffPercentage = 0;
                        if (vm.output.salesAndExpenses[i + 1].purchases > 0)
                            purchaseDiffPercentage = purchaseDifference / vm.output.salesAndExpenses[i + 1].purchases * 100;

                        vm.output.salesAndExpenses[i].salesDifferent = salesDifference;
                        vm.output.salesAndExpenses[i].diffSalesPercentage = salesDiffPercentage;

                        vm.output.salesAndExpenses[i].purchasesDifferent = purchaseDifference;
                        vm.output.salesAndExpenses[i].diffPurchasesPercentage = purchaseDiffPercentage;
                    }

                    if (vm.drillDownLevel == vm.output.salesAndExpenses.length) {
                        vm.tableParams = new ngTableParams({}, { dataset: vm.output.salesAndExpenses, counts: [] });
                        console.log(vm.tableParams);
                        if (vm.output.salesAndExpenses.length > 0)
                            vm.graphDraw(vm.output.salesAndExpenses[0]);
                    }
                    else {
                        vm.tableParams = new ngTableParams({}, { dataset: [], counts: [] });
                        console.log(vm.tableParams);

                    }
                }
            }

            vm.supplierTopModify = function (argOperation) {
                if (argOperation == '-')
                    vm.supplierTop--;
                else
                    vm.supplierTop++;

                if (vm.supplierTop > 15) {
                    vm.supplierTop = 15;
                    abp.notify.warn(app.localize('MaximumAllowed', 15));
                }
                if (vm.supplierTop <= 1) {
                    vm.supplierTop = 2;
                    abp.notify.warn(app.localize('MinimumAllowed', 15));
                }
                vm.supplierChart(vm.currentData);

            }

            vm.supplierChartTypeChange = function (argChartType) {
                vm.supplierChartType = argChartType;
                vm.supplierChart(vm.currentData);
            }


            vm.supplierChart = function (data) {

                vm.supplierChartData = [];
                vm.chartsupplierList = [];
                vm.chartpurchaseList = [];


                angular.forEach(data.supplierWisePurchases, function (value, key) {
                    if (key < vm.supplierTop) {
                        vm.supplierChartData.push({ 'name': value.supplierRefName, 'y': value.purchase });
                        vm.chartsupplierList.push(value.supplierRefName);
                        vm.chartpurchaseList.push(value.purchase);
                    }
                    else
                        return;
                });

                if (vm.supplierChartType == 'PIE') {
                    Highcharts.chart('supplier_stats', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{point.y} {point.percentage:.1f} %'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.y} <br/>{point.percentage:.1f} %',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            name: app.localize('Purchase'),
                            colorByPoint: true,
                            data: vm.supplierChartData
                        }]
                    });
                }
                else if (vm.supplierChartType == "LINE") {
                    $('#supplier_stats').highcharts({
                        title: {
                            text: app.localize('SupplierWise'),
                            x: -20 //center
                        },
                        xAxis: {
                            categories: vm.chartsupplierList,
                        },
                        yAxis: {

                            plotLines: [{
                                value: 0,
                                width: 2,
                                color: '#808080'
                            }]
                        },

                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle',
                            borderWidth: 0
                        },
                        plotOptions: {
                            line: {
                                dataLabels: {
                                    enabled: true
                                },
                                enableMouseTracking: false
                            }
                        },
                        series: [
                           {
                               name: app.localize('Purchases'),
                               data: vm.chartpurchaseList
                           },
                            //{
                            //    name: app.localize('PurchaseAmount'),
                            //    data: vm.purchaseamountlist
                            //},
                        ]
                    });
                }
                else {
                    $('#supplier_stats').highcharts({
                        chart: {
                            type: 'bar'
                        },
                        title: {
                            text: app.localize('SupplierWise'),
                            x: -20 //center
                        },
                        xAxis: {
                            categories: vm.chartsupplierList,
                        },
                        yAxis: {

                            plotLines: [{
                                value: 0,
                                width: 2,
                                color: '#808080'
                            }]
                        },

                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle',
                            borderWidth: 0
                        },
                        plotOptions: {
                            bar: {
                                dataLabels: {
                                    enabled: true
                                }
                            }
                        },
                        series: [
                           {
                               name: app.localize('Purchases'),
                               data: vm.chartpurchaseList
                           },
                            //{
                            //    name: app.localize('PurchaseAmount'),
                            //    data: vm.purchaseamountlist
                            //},
                        ]
                    });
                }
            };


            vm.materialTopModify = function (argOperation) {
                if (argOperation == '-')
                    vm.materialTop--;
                else
                    vm.materialTop++;

                if (vm.materialTop > 15) {
                    vm.materialTop = 15;
                    abp.notify.warn(app.localize('MaximumAllowed', 15));
                }
                if (vm.materialTop <= 1) {
                    vm.materialTop = 2;
                    abp.notify.warn(app.localize('MinimumAllowed', 15));
                }
                vm.materialChart(vm.currentData);

            }

            vm.materialChartTypeChange = function (argChartType) {
                vm.materialChartType = argChartType;
                vm.materialChart(vm.currentData);
            }


            vm.materialChart = function (data) {
                vm.materialChartData = [];
                vm.chartmaterialList = [];
                vm.qtyList = [];
                vm.chartpurchaseList = [];


                angular.forEach(data.materialWisePurchases, function (value, key) {
                    if (key < vm.materialTop) {
                        vm.materialChartData.push({ 'name': value.materialRefName, 'y': value.purchase });
                        vm.chartmaterialList.push(value.materialRefName);
                        vm.chartpurchaseList.push(value.purchase);
                        vm.qtyList.push(value.quantity);
                    }
                    else
                        return;
                });

                if (vm.materialChartType == 'PIE') {
                    Highcharts.chart('material_stats', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{point.y} {point.percentage:.1f} %'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.y} <br/>{point.percentage:.1f} %',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            name: app.localize('Material'),
                            colorByPoint: true,
                            data: vm.materialChartData
                        }]
                    });
                }
                else if (vm.materialChartType == "LINE") {
                    $('#material_stats').highcharts({
                        title: {
                            text: app.localize('Material'),
                            x: -20 //center
                        },
                        xAxis: {
                            categories: vm.chartmaterialList,
                        },
                        yAxis: {

                            plotLines: [{
                                value: 0,
                                width: 2,
                                color: '#808080'
                            }]
                        },

                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle',
                            borderWidth: 0
                        },
                        plotOptions: {
                            line: {
                                dataLabels: {
                                    enabled: true
                                },
                                enableMouseTracking: false
                            }
                        },
                        series: [
                           {
                               name: app.localize('Purchases'),
                               data: vm.chartpurchaseList
                           },
                            //{
                            //    name: app.localize('PurchaseAmount'),
                            //    data: vm.purchaseamountlist
                            //},
                        ]
                    });
                }
                else {
                    $('#material_stats').highcharts({
                        chart: {
                            type: 'bar'
                        },
                        title: {
                            text: app.localize('MaterialWise'),
                            x: -20 //center
                        },
                        xAxis: {
                            categories: vm.chartmaterialList,
                        },
                        yAxis: {

                            plotLines: [{
                                value: 0,
                                width: 2,
                                color: '#808080'
                            }]
                        },

                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle',
                            borderWidth: 0
                        },
                        plotOptions: {
                            bar: {
                                dataLabels: {
                                    enabled: true
                                }
                            }
                        },
                        series: [
                           {
                               name: app.localize('Quantity'),
                               data: vm.qtyList
                           },
                            {
                                name: app.localize('Purchases'),
                                data: vm.chartpurchaseList
                            },
                            //{
                            //    name: app.localize('PurchaseAmount'),
                            //    data: vm.purchaseamountlist
                            //},
                        ]
                    });
                }
            };

            vm.graphDraw = function (data) {
                vm.currentData = data;
                vm.supplierChart(data);
                vm.materialChart(data);
            }

           
        }
    ]);
})();