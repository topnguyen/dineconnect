﻿(function () {
    appModule.controller('tenant.views.house.notificationDashboard', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.interTransfer', 'appSession', 'abp.services.app.location', 'abp.services.app.dayClose', 'abp.services.app.purchaseOrder',
        function ($scope, $state, $modal, uiGridConstants, intertransferService, appSession, locationService, daycloseService, purchaseOrderService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.permissions = {
                approveRequest: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransfer.Approve'),
                approvePurchase: abp.auth.hasPermission('Pages.Tenant.House.Transaction.PurchaseOrder.Approve')
            };

            vm.defaultLocationId = appSession.user.locationRefId;

            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.approvalPendingDtos = [];
            vm.transferPendingDtos = [];
            vm.receivePendingDtos = []
            vm.pendingPoListDtos = [];

            vm.approvalPendingCount = 0;
            vm.transferPendingCount = 0;
            vm.receivePendingCount = 0;
            vm.poPendingCount = 0;


            vm.showPendingRequestApprove = function (data) {
                if (data.locationRefId != vm.defaultLocationId) {
                    abp.notify.error(app.localize('ApprovalLocationError'));
                    return;
                }

                $state.go("tenant.intertransferrequestdetail", {
                    id: data.id,
                    directTransfer: false
                });
            }
            vm.showPendingTransferApprove = function (data) {
                if (data.requestLocationRefId != vm.defaultLocationId) {
                    abp.notify.error(app.localize('ApprovalLocationError'));
                    return;
                }
                $state.go("tenant.intertransferapprovaldetail", {
                    id: data.id, requestLocationRefName: data.requestLocationRefName,
                    locationRefName: data.locationRefName
                });

            }


            vm.showPendingPoApprove = function (data) {
                if (data.locationRefId != vm.defaultLocationId) {
                    abp.notify.error(app.localize('ApprovalLocationError'));
                    return;
                }

                $state.go("tenant.purchaseorderdetail", {
                    id: data.id,
                    autoPoFlag: false

                });
            }

            vm.showPendingReceive


            vm.showPendingReceive = function (data) {
                if (data.locationRefId != vm.defaultLocationId) {
                    abp.notify.error(app.localize('ApprovalLocationError'));
                    return;
                }

                $state.go("tenant.purchaseorderdetail", {
                    id: data.id,
                    autoPoFlag: false

                });
            }

            vm.getPendingApproval = function () {
                vm.loading = true;
                intertransferService.getApprovalAndReceivePendingStaus(vm.currentUserId
                ).success(function (result) {
                    vm.approvalPendingDtos = result.approvalPendingDtos;
                    vm.transferPendingDtos = result.transferPendingDtos;
                    vm.receivePendingDtos = result.receivePendingDtos;
                    vm.approvalPendingCount = result.approvalPendingCount;
                    vm.transferPendingCount = result.transferPendingCount;
                    vm.receivePendingCount = result.receivePendingCount;
                }).finally(function () {
                    vm.loading = false;
                });
            }


            vm.getPendingPo = function () {
                vm.loading = true;
                purchaseOrderService.getPoPendingStatus(
                    vm.currentUserId,
                    vm.defaultLocationId
                ).success(function (result) {
                    vm.pendingPoListDtos = result.pendingPoListDtos;
                    vm.poPendingCount = result.poPendingCount;
                }).finally(function () {
                    vm.loading = false;
                });
            }



            vm.refreshAll = function () {
                vm.approvalPendingDtos = [];
                vm.transferPendingDtos = [];
                vm.receivePendingDtos = [];
                vm.pendingPoListDtos = [];
                vm.approvalPendingCount = 0;
                vm.transferPendingCount = 0;
                vm.receivePendingCount = 0;

                vm.poPendingCount = 0;
                if (vm.permissions.approveRequest) {
                    vm.getPendingApproval();
                }
                if (vm.permissions.approvePurchase) {
                    vm.getPendingPo();
                }


            }

            vm.refreshAll();


        }]);
})();

