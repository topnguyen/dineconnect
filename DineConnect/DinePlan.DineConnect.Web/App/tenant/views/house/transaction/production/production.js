﻿(function () {
    appModule.controller('tenant.views.house.transaction.production.production', [
        '$scope', '$filter', '$timeout', '$state', '$stateParams', 'abp.services.app.production',
			'appSession', 'abp.services.app.template', 'abp.services.app.material', 'abp.services.app.issue', 'abp.services.app.unitConversion', 'abp.services.app.dayClose', 
        function ($scope, $filter, $timeout, $state, $stateParams, productionService,  appSession,
					templateService, materialService, issueService, unitconversionService, daycloseService) {

            var vm = this;
            
            vm.saving = false;
            vm.production = null;
            vm.productionDetail = [];
            vm.refunit = [];

            vm.productionDetailPortion = [];
            vm.sno = 0;
            vm.uilimit = null;
            vm.productionUnitExistsFlag = appSession.location.isProductionUnitAllowed;
            vm.materialGroupCategoryRefId = null;

            vm.refproductionunit = [];
            if (vm.productionUnitExistsFlag == true) {
                vm.refproductionunit = appSession.location.productionUnitList;
            }

            vm.cancel = function () {
                $state.go('tenant.production');
            };

            vm.defaultLocationId = appSession.user.locationRefId;
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.isProductionAllowed = appSession.location.isProductionAllowed;
            if (vm.isProductionAllowed == false) {
                abp.notify.warn(app.localize('ProductionAllowedErr'));
                vm.cancel();
                return;
            }

            vm.defaulttemplatetype = 3;

            vm.productionId = $stateParams.id;
            vm.prodmethod = $stateParams.prodmethod;

            if (vm.prodmethod == 'REGULAR')
                vm.withoutissueflag = false;
            else
                vm.withoutissueflag = true;


            vm.template = null;
            vm.selectedTemplate = null;

            vm.templatesave = null;

            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['llll', 'YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.dateOptions = [];
            vm.dateOptions.push({
                'singleDatePicker': true,
                'showDropdowns': true,
                'startDate': moment(),
                'minDate': $scope.minDate,
                'maxDate': moment()
            });

            //date start

            $('input[name="productionTime"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });

            vm.existAllElements = function () {
                $scope.errmessage = "";

                if (vm.production.productionTime == null) {
                    $scope.errmessage = $scope.errmessage + app.localize('productionTimeErr');
                    $('#productionTime').focus();
                }

                if (vm.production.locationRefId == null || vm.production.locationRefId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('LocationErr');
                }

                if (vm.production.productionSlipNumber == null || vm.production.productionSlipNumber == "") {
                    $scope.errmessage = $scope.errmessage + app.localize('ReferenceErr');
                }

                if ($scope.errmessage != "") {
                    abp.notify.warn($scope.errmessage, 'Required');
                    return false;
                }
                else
                    return true;

            };

			vm.addPortion = function () {
			    var errorFlag = false;

			    if (vm.sno > 0) {

			        if (vm.productionUnitExistsFlag == true && vm.isUndefinedOrNull(vm.production.productionUnitRefId) == true) {
			            abp.notify.warn(app.localize('ProductionUnitNotSelectErr'));
			            return;
			        }

			        if (vm.existAllElements() == false)
			            return;
			        
			        var value = vm.productionDetailPortion[vm.productionDetailPortion.length - 1];

			        if (vm.withoutissueflag == false)
			        {
			            if (vm.isUndefinedOrNull(value.issueRefId) || value.issueRefId == 0) {
			                errorFlag = true;
			                abp.notify.info(app.localize('IssueIdRequired'));
			                return;
			            }
			        }
			        if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
			            errorFlag = true;
			            abp.notify.info(app.localize('MinimumOneDetail'));
			            return;
			        }

			        if (vm.isUndefinedOrNull(value.materialRefName)) {
			            errorFlag = true;
			            abp.notify.info(app.localize('MaterialRequired'));
			            return;
			        }

			        if (vm.isUndefinedOrNull(value.productionQty) || value.productionQty == 0) {
			            errorFlag = true;
			            abp.notify.info(app.localize('InvalidQty'));
			            return;
			        }
			        if (vm.isUndefinedOrNull(value.unitRefId) || value.unitRefId == 0) {
			            errorFlag = true;
			            abp.notify.info(app.localize('UnitRequired'));
			            return;
			        }

			    }
			    vm.sno = vm.sno + 1;

			    vm.productionDetailPortion.push({
			        'sno': vm.sno,
			        'materialRefId': 0,
			        'materialRefName': '',
			        'productionQty': '',
			        'unitRefId': '',
                    'unitRefName' : '',
			        'uom': '',
			        'currentInHand': '',
			        'allowedMinQty': 0,
			        'allowedMaxQty': 0,
			        'multipleBatchProductionAllowed': false,
			        'particularRecipeFlag': false,
			        'changeunitflag' : false
			    });

			}

			vm.portionLength = function () {
			    return true;
			}

			vm.removeRow = function (productIndex) {
			    if (vm.productionDetailPortion.length > 1) {
			        vm.productionDetailPortion.splice(productIndex, 1);
			        vm.sno = vm.sno - 1;
			    }
			    else {
			        vm.productionDetailPortion.splice(productIndex, 1);
			        vm.sno = 0;
			        vm.addPortion();
			        return;
			    }
			}

			vm.allowedMinQty = 0;
			vm.allowedMaxQty = 0;
			
			$scope.issuefunc = function (data, val, objList) {

			    if (val.issueRecipeDetail != null && val.issueRecipeDetail.length>0) {
			        data.particularRecipeFlag = true;

			        vm.refRecipe = [];

			        angular.forEach(vm.temprefRecipe, function (value, key) {
			            val.issueRecipeDetail.some(function (recdata, reckey) {
			                if (recdata.recipeRefId == value.id) {
			                    vm.refRecipe.push(value);
			                    return true;
			                }
			            });
			        });
			    }
			    else
			    {
			        vm.refRecipe = vm.temprefRecipe;
			        data.particularRecipeFlag = false;
			    }

			    data.issueRefId = val.id;
			    data.materialRefId = 0;
			    data.materialRefName = '';
			    data.productionQty = '';
			 
			};


			$scope.func = function (data, val, objList) {

			    var forLoopFlag = true;
			    vm.allowedMinQty = 0;
			    vm.allowedMaxQty = 0;

			    var recdetail;

			    if (data.particularRecipeFlag) {
			        vm.refissues.some(function (value, key) {
			            if (value.id == data.issueRefId ) 
			            {
			                value.issueRecipeDetail.some(function (recdata, reckey) {
			                    if (recdata.recipeRefId == val.id) {
			                        recdetail = recdata;

			                        return true;
			                    }
			                });
			                return true;
			            }
			        });
			        if(recdetail.completionFlag)
			        {
			            abp.message.warn(app.localize('AlreadyProductionFinished'));
			            return;
			        }
			        if (recdetail.recipeProductionQty < 0)
			            recdetail.recipeProductionQty = 0;
			    
			    vm.allowedMinQty = 0;

			    if (!vm.isUndefinedOrNull(recdetail.multipleBatchProductionAllowed) && recdetail.multipleBatchProductionAllowed == false) {
			        if (recdetail.multipleBatchProductionAllowed == false) {
			            vm.allowedMinQty = val.recipeProductionQty * 0.9;
			        }
			        angular.forEach(objList, function (value, key) {
			            if (forLoopFlag) {
			                if (angular.equals(data.issueRefId, value.issueRefId) && angular.equals(data.materialRefId,value.materialRefId) && data.sno != value.sno) {
			                    abp.message.warn(app.localize('AlreadyEntered'));


			                    forLoopFlag = false;
			                }
			            }
			        });
			    }
			    else
			    {
			        
			        vm.allowedMaxQty = recdetail.recipeProductionQty * 1.1;
			    }

			    if (!forLoopFlag) {
			        data.issueRefId = 0;
			        data.materialRefId = 0;
			        data.materialRefName = '';
			        data.productionQty = '';
			        return;
			    }
			}

			    if (data.particularRecipeFlag == false) {
			        forLoopFlag = true;

			        angular.forEach(objList, function (value, key) {
			            if (forLoopFlag) {
			                if (angular.equals(val.materialName, value.materialRefName)) {
			                    abp.message.warn(app.localize('DuplicateMaterialExists'));
			                    forLoopFlag = false;
			                }
			            }
			        });

			        if (!forLoopFlag) {
			            data.materialRefId = 0;
			            data.materialRefName = '';
			            return;
			        }
			    }

			    data.materialRefName = val.materialName;
			    data.currentInHand = val.onHand;
			    data.unitRefId = val.issueUnitId;
			    data.unitRefName = val.issueUnitName;
			    data.defaultUnitId = val.unitRefId;
			    data.uom = val.defaultUnitName;
			    data.allowedMinQty = vm.allowedMinQty;
			    data.allowedMaxQty = vm.allowedMaxQty;
			    data.multipleBatchProductionAllowed = recdetail.multipleBatchProductionAllowed;
			};


			vm.checkQty = function (enteredQty, data) {
			    if (data.particularRecipeFlag == false || parseFloat(enteredQty)==0 || enteredQty == null || enteredQty == "" )
			    {
			        return;
			    }

			    var revisedQty = enteredQty;

			    if (data.defaultUnitId == data.unitRefId) {
			        revisedQty = enteredQty;
			    }
			    else {
			        var convFactor = 0;
			        var convExistFlag = false;
			        vm.refconversionunit.some(function (refdata, refkey) {
			            if (refdata.baseUnitId == data.defaultUnitId && refdata.refUnitId == data.unitRefId) {
			                convFactor = refdata.conversion;
			                convExistFlag = true;
			                return true;
			            }
			        });


			        if (convExistFlag == false) {
			            data.productionQty = null;
			            abp.notify.info(app.localize('ConversionFactorNotExist', data.uom, data.unitRefName));
			            return;
			        }

			        revisedQty = enteredQty * 1 / convFactor;
			    }

			    

			    if ((parseFloat(revisedQty) >= parseFloat(data.allowedMinQty)) && (parseFloat(revisedQty) <= parseFloat(data.allowedMaxQty)))
			        return;
			    else 
                    {
			        abp.message.warn(app.localize("ProdQtyErr", revisedQty, '0.001', data.allowedMaxQty.toFixed(2), data.materialRefName), "Error");
			        data.productionQty = '';
			    }
			}

			vm.isUndefinedOrNull = function (val) {
			    return angular.isUndefined(val) || val == null;
			};
			
            vm.save = function () {
			   if (vm.existAllElements() == false)
			       return;
			   if (vm.templatesave.templateflag == true && vm.templatesave.templatename.length == 0) {
			       abp.message.warn(app.localize("TemplateNameRequired"));
			       return;
			   }

			   if (vm.productionUnitExistsFlag == true && vm.isUndefinedOrNull(vm.production.productionUnitRefId) == true) {
			       abp.notify.warn(app.localize('ProductionUnitNotSelectErr'));
			       return;
			   }


			   vm.saving = true;

			   vm.productionDetail = [];

			   vm.sno = 0;
			   vm.errorFlag = false;
			   angular.forEach(vm.productionDetailPortion, function (value, key) {
			       if (vm.withoutissueflag == false) {
			           if (vm.isUndefinedOrNull(value.issueRefId) || value.issueRefId == 0) {
			               vm.errorFlag = true;
			               abp.notify.info(app.localize('IssueIdRequired'));
			               return;
			           }
			       }
			       if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
			           vm.errorFlag = true;
			           abp.notify.info(app.localize('MaterialRequired'));
			           return;
			       }

			       if (vm.isUndefinedOrNull(value.materialRefName)) {
			           vm.errorFlag = true;
			           abp.notify.info(app.localize('MaterialRequired'));
			           return;
			       }

			       if (vm.isUndefinedOrNull(value.productionQty) || value.productionQty == 0) {
			           vm.errorFlag = true;
			           abp.notify.info(app.localize('InvalidQty'));
			           return;
			       }
			       if (vm.isUndefinedOrNull(value.unitRefId) || value.unitRefId == 0) {
			           vm.errorFlag = true;
			           abp.notify.info(app.localize('UnitRequired'));
			           return;
			       }

			       
			       if (value.materialRefId != 0 && value.materialRefId != null && value.uom != '' && value.productionQty != 0 && value.productionQty != '') {
			           vm.sno++;
			           vm.productionDetail.push({
			               'sno': vm.sno,
			               'MaterialRefId': value.materialRefId,
			               'materialRefName': value.materialRefName,
			               'productionQty': value.productionQty,
			               'uom': value.uom,
			               'unitRefId': value.unitRefId,
                           'issueRefId' : value.issueRefId,
			               'allowedMinQty': value.allowedMinQty,
			               'allowedMaxQty': value.allowedMaxQty,
			               'multipleBatchProductionAllowed': value.multipleBatchProductionAllowed,
			               'particularRecipeFlag': value.particularRecipeFlag,
			               'changeunitflag': false
			           });
			       }
			   });
                
			   if (vm.errorFlag)
			   {
			       vm.saving = false;
			       return;
			   }
			   if (vm.productionDetail.length == 0)
			   {
			       abp.message.warn(app.localize("NoDetailsFoundToSave"));
			       vm.saving = false;
			       return;
			   }
				
			   vm.production.productionSlipNumber = vm.production.productionSlipNumber.toUpperCase();

                productionService.createOrUpdateProduction({
                    production: vm.production,
                    ProductionDetail: vm.productionDetail
                }).success(function (result) {
                    issueService.setCompletedIssues({
                        locationRefId: vm.defaultLocationId,
                        productionUnitRefId : vm.production.productionUnitRefId
                    }).success(function () {
                   
                    });
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.printProduction(result.id)
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.printProduction = function (objId) {
                $state.go('tenant.productionprint', {
                    printid: objId
                });
            };


       

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
			 };

			 vm.getComboValue = function (item) {
			     return parseInt(item.value);
			 };
			
			 vm.refRecipe = [];
			 vm.temprefRecipe = [];
			 function fillDropDownRecipe() {
			     vm.loadingflag = true;
			     materialService.getRecipeViewWithOnHand({
			         id: vm.defaultLocationId
			     }).success(function (result) {
			         vm.refRecipe = result.items;
			         vm.temprefRecipe = result.items;
			         vm.loadingflag = false;
			     }).finally(function () {
			         vm.loadingflag == false;
			     });
			 }

			 vm.refmaterial = [];
			 vm.fillDropDownMaterial = function () {
			     vm.loadingflag == true;
			     materialService.getViewWithOnHand({
			         id: vm.defaultLocationId
			     }).success(function (result) {
			         vm.refmaterial = result.items;
			         vm.loadingflag = false;
			     }).finally(function () {
			         vm.loadingflag = false;
			     });
			 }

			 vm.refTemplateDetail = [];
			 vm.reftemplate = [];
			 function fillDropDownTemplate() {
			     templateService.getTemplateDetail({
			         locationRefId: vm.defaultLocationId,
			         templateType: vm.defaulttemplatetype
			     }).success(function (result) {
			         vm.refTemplateDetail = result.items;

			         angular.forEach(vm.refTemplateDetail, function (value, key) {
			             vm.reftemplate.push({
			                 'value': value.id,
			                 'displayText': value.name,
			             });
			         });

			     });
			 }

			 vm.refissues = [];
			 vm.fillIssues = function () {
			     vm.loadingflag = true;
			     issueService.getPendingIssues({
			         locationRefId: vm.defaultLocationId,
                     productionUnitRefId : vm.production.productionUnitRefId
			     }).success(function (result) {
			         vm.refissues = result;
			     }).finally(function () {
			         vm.loadingflag == false;
			     });
			 }

			 vm.refmaterialgroupcategory = [];
			 function fillDropDownMaterialGroupCategory() {
			     materialService.getMaterialGroupCategoryForCombobox({}).success(function (result) {
			         vm.refmaterialgroupcategory = result.items;
			     });
			 }


			 function init() {
			     fillDropDownMaterialGroupCategory();
			     fillDropDownRecipe();
			     vm.fillDropDownMaterial();
			     fillDropDownTemplate();

                productionService.getProductionForEdit({
                    Id: vm.productionId
                }).success(function (result) {
                    vm.production = result.production;
                    vm.productionDetail = result.productionDetail;
                    vm.templatesave = result.templatesave;

                    vm.production.locationRefId = vm.defaultLocationId;

                    vm.templatesave.templateflag = false;
                    vm.templatesave.templatename = null;
                    vm.templatesave.templatescope = false;

                    vm.productionDetailPortion = [];

                    if (vm.productionUnitExistsFlag == false)
                        vm.fillIssues();

                    $scope.dt = moment().format($scope.format);
                    if (result.production.id != null) {
                        vm.uilimit = null;
                        //vm.production.productionTime = moment(result.production.productionTime).format($scope.format);
                        vm.production.productionTime = moment(result.production.productionTime).format($scope.format);
                        $scope.minDate = vm.production.productionTime;

                        $('input[name="productionTime"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: vm.production.productionTime,
                            minDate: $scope.minDate,
                            maxDate: moment().format($scope.format)
                        });

                        vm.productionDetailPortion = [];

                        angular.forEach(vm.productionDetail, function (value, key) {
                            vm.productionDetailPortion.push({
                                'sno': value.sno,
                                'materialRefId': value.outputMaterialRefId,
                                'materialRefName': value.materialRefName,
                                'productionQty': value.productionQty,
                                'issueRefId': value.issueRefId,
                                'unitRefId' : value.unitRefId,
                                'uom': value.uom,
                                'currentInHand': value.currentInHand,
                                'allowedMinQty': 0,
                                'allowedMaxQty': 0,
                                'multipleBatchProductionAllowed': false,
                                'particularRecipeFlag': false,
                                'changeunitflag': false
                            });
                        });

                        if (vm.productionDetailPortion.length == 0)
                            vm.addPortion();

                        vm.fillDropDownMaterial();
                    }
                    else {
                        vm.production.productionTime = moment().format($scope.format);
                        vm.checkCloseDay();
                        vm.addPortion();
                        vm.uilimit = 20;
                    }
                });
			 }

			 vm.checkCloseDay = function () {
				 daycloseService.getDayCloseStatus({
			         id: vm.defaultLocationId
			     }).success(function (result) {
			         if (result.id == 0) {

			             if (vm.softmessagenotificationflag)
			                 abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format('YYYY-MMM-DD')));

			             if (vm.messagenotificationflag)
			                 abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format('YYYY-MMM-DD')));

			         }

			         if (moment(result.accountDate).format("YYYY-MMM-DD") == moment().format("YYYY-MMM-DD"))
			             return;
                     
    			         vm.production.productionTime = moment(result.accountDate).format($scope.format);

			         $scope.minDate = vm.production.productionTime;

			         $('input[name="productionTime"]').daterangepicker({
			             locale: {
			                 format: $scope.format
			             },
			             singleDatePicker: true,
			             showDropdowns: true,
			             startDate: vm.production.productionTime,
			             minDate: $scope.minDate,
			             maxDate: vm.production.productionTime
			         });

			     }).finally(function (result) {

			     });
			 }

			 //vm.checkCloseDay();
			 init();

			 vm.showErrorProductionUnitNotSelected = function () {
			     if (vm.productionUnitExistsFlag == true && vm.isUndefinedOrNull(vm.production.productionUnitRefId) == true) {
			         abp.notify.warn(app.localize('ProductionUnitNotSelectErr'));
			         return false;
			     }
			     return true;
			 }

			 vm.refconversionunit = [];
			 vm.fillUnitConversion = function () {
			     unitconversionService.getAll({
			     }).success(function (result) {
			         vm.refconversionunit = result.items;
			     });
			 }

			 vm.fillUnitConversion();



			 vm.changeUnit = function (data) {
			     if (!vm.mutlipleUomAllowed) {
			         abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
			         return;
			     }

			     data.changeunitflag = true;
			     fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
			 }

			 function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
			     vm.loading = true;
			     vm.refunit = [];
			     materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
			         vm.refunit = result.items;
			         if (vm.refunit.length == 1) {
			             data.unitRefId = vm.refunit[0].value;
			             data.unitRefName = vm.refunit[0].displayText;
			             data.changeunitflag = false;
			         }

			         vm.loading = false;
			     });
			 }

			 vm.unitReference = function (selected, data) {
			     data.unitRefId = selected.value;
			     data.unitRefName = selected.displayText;
			     data.changeunitflag = false;
			 }



			 vm.removeUnfilled = function () {
			     var listtoremoved = [];
			     tempportions = [];
			     vm.loading = true;
			     vm.sno = 0;
			     angular.forEach(vm.productionDetailPortion, function (value, key) {
			         if (value.productionQty == 0 || value.productionQty == null || value.productionQty == "") {
			             listtoremoved.push(key)
			         }
			         else {
			             vm.sno++;
			             value.sno = vm.sno;
			             tempportions.push(value);
			         }
			     });

			     vm.productionDetailPortion = tempportions;

                 vm.sno = vm.productionDetailPortion.length;

			     if (vm.productionDetailPortion.length == 0)
			         vm.addPortion();

			     vm.loading = false;
			 }

			 vm.getDetailMaterial = function () {
			     vm.uilimit = null;
			     if (vm.materialGroupCategoryRefId == null || vm.materialGroupCategoryRefId == 0) {
			         abp.message.warn(app.localize('CategorySelectErr'));
			         return;
			     }
			     if (vm.productionDetailPortion != null) {
			         var element = vm.productionDetailPortion[vm.productionDetailPortion.length - 1];
			         if (element.materialRefId == 0 || element.materialRefId == null) {
			             productIndex = vm.productionDetailPortion.length - 1;
			             vm.productionDetailPortion.splice(productIndex, 1);
			             vm.sno = vm.sno - 1;
			         }
			     }

			     vm.loading = true;
			     materialService.getIssueForCategory({
			         materialGroupCategoryRefId: vm.materialGroupCategoryRefId,
			         locationRefId: vm.defaultLocationId,
			         semiFinishedOnly : true
			     }).success(function (result) {

			         angular.forEach(result.issueDetail, function (value, key) {
			             var alreadyExist = false;
			             vm.productionDetailPortion.some(function (existvalue, existkey) {
			                 if (existvalue.materialRefId == value.materialRefId) {
			                     alreadyExist = true;
			                     return true;
			                 }
			             });

			             if (alreadyExist == false) {
			                 vm.sno = vm.sno + 1;
			                 vm.productionDetailPortion.push({
			                     'sno': vm.sno,
			                     'materialRefId': value.materialRefId,
			                     'materialRefName': value.materialRefName,
			                     'productionQty': '',
			                     'unitRefId': value.unitRefId,
			                     'unitRefName': value.unitRefName,
			                     'uom': value.defaultUnitName,
			                     'currentInHand': value.currentInHand,
			                     'allowedMinQty': 0,
			                     'allowedMaxQty': 0,
			                     'multipleBatchProductionAllowed': false,
			                     'particularRecipeFlag': false,
			                     'changeunitflag': false
			                 })
			             }
			         });
			     }).finally(function (result) {
			         vm.loading = false;
			     });
			 }


			
        }
    ]);
})();

