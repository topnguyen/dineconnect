﻿(function () {
    appModule.controller('tenant.views.house.transaction.production.productionPrint', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.production', 'appSession',
        'abp.services.app.supplier', 'abp.services.app.location',
        function ($scope, $filter, $state, $stateParams, $modal, uiGridConstants, productionService, appSession,
            supplierService, locationService) {

            var vm = this;
            vm.issuerefprintflag = false;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.connectdecimals = appSession.connectdecimals;
            vm.housedecimals = appSession.housedecimals;

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.productionId = $stateParams.printid;

            vm.headerline = "~~~~~~~~~~~~~~~~~~~~";

            vm.getData = function () {
                productionService.getProductionForEdit({
                    id: vm.productionId
                }).success(function (result) {
                    vm.production = result.production;
                    vm.productionPortions = [];

                    vm.sno = 0;


                    vm.productiondetail = result.productionDetail;


                    angular.forEach(vm.productiondetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        if (value.issueRefId != null)
                            vm.issuerefprintflag = true;

                        vm.productionPortions.push({
                            'sno': value.sno,
                            'materialRefId': value.outputMaterialRefId,
                            'materialRefName': value.materialRefName,
                            'productionQty': value.productionQty,
                            'issueRefId': value.issueRefId,
                            'unitRefId': value.unitRefId,
                            'uom': value.uom,
                            'unitRefName': value.unitRefName,
                            'currentInHand': value.currentInHand,
                            'allowedMinQty': 0,
                            'allowedMaxQty': 0,
                            'multipleBatchProductionAllowed': false,
                            'particularRecipeFlag': false,
                            'price': value.price,
                            'lineTotal' : value.lineTotal
                        });
                    });
                });
            }

            vm.getData();

        }]);
})();

