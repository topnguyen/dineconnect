﻿(function () {
    appModule.controller('tenant.views.house.transaction.closingstock.mobileClosingstockMainForm', [
			'$scope', '$state', '$stateParams', 'abp.services.app.closingStock', 'abp.services.app.commonLookup', 'abp.services.app.location', 'appSession', 'abp.services.app.material', 'abp.services.app.unitConversion', 'abp.services.app.template', 'deviceDetector', 'abp.services.app.dayClose', 
				function ($scope, $state, $stateParams, closingstockService, commonLookupService, locationService, appSession, materialService, unitconversionService, templateService, deviceDetector, daycloseService) {

            var vm = this;

            vm.saving = false;
            vm.closingstock = null;
            $scope.existall = true;
            vm.closingstockId = $stateParams.id;
            vm.sno = 0;
            vm.closingstockPortions = [];

            vm.closingStockDetail = [];

            vm.defaultLocationId = appSession.user.locationRefId;
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.uilimit = null;
            vm.templatesave = null;

            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            $('input[name="closingDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                //minDate: $scope.minDate,
                //maxDate: moment()
            });


            vm.defaulttemplatetype = 6;

            vm.refTemplateDetail = [];
            vm.reftemplate = [];

            function fillDropDownTemplate() {
                vm.loading = true;
                templateService.getTemplateDetail({
                    locationRefId: vm.defaultLocationId,
                    templateType: vm.defaulttemplatetype
                }).success(function (result) {
                    vm.refTemplateDetail = result.items;
                    angular.forEach(vm.refTemplateDetail, function (value, key) {
                        vm.reftemplate.push({
                            'value': value.id,
                            'displayText': value.name,
                        });
                    });
                    vm.loading = false;
                });
            }

            fillDropDownTemplate();

            vm.fillDataFromTemplate = function () {
                vm.selectedTemplate = null;
                vm.refTemplateDetail.some(function (value, key) {
                    if (value.id == vm.template) {
                        vm.selectedTemplate = value.templateData;
                        return true;
                    }
                });

                if (vm.selectedTemplate == null)
                    return;

                vm.loading = true;

                closingstockService.getTemplateObjectForEdit({
                    objectString: vm.selectedTemplate
                }).success(function (result) {

                    vm.closingstock = result.closingStock;

                    vm.closingStockDetail = result.closingStockDetail;

                    vm.closingstock.stockDate = moment().format($scope.format);
                    vm.closingstock.id = null;
                    vm.closingstock.locationRefId = vm.defaultLocationId;

                    $('input[name="closingDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.closingstock.stockDate,
                        minDate: $scope.minDate,
                        maxDate: moment().format($scope.format)
                    });

                    vm.closingstockPortions = [];
                    vm.sno = 0;
                    angular.forEach(vm.closingStockDetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.closingstockPortions.push({
                            'sno': vm.sno,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'defaultUnit': value.unitRefName,
                            'defaultUnitId': value.unitRefId,
                            'onHand': null,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'changeunitflag': false,
                        });
                        
                    });
                    vm.sno = vm.closingstockPortions.length + 1;

                    vm.loading = false;
                });

            }

            vm.templateSave = function()
            {
                vm.closingstockRecipeDetail = [];
                vm.errorflag = false;

                if (vm.templatesave.templatename.length == 0) {
                    abp.message.warn(app.localize("TemplateNameRequired"));
                    return;
                }

                vm.saving = true;
                vm.closingStockDetail = [];

                vm.errorflag = false;
                angular.forEach(vm.closingstockPortions, function (value, key) {
                    if (value.unitRefId == null || value.unitRefId == 0) {
                        abp.message.warn(app.localize("UnitErr", value.materialRefName));
                        vm.errorflag = true;
                        return;
                    }

                    vm.closingStockDetail.push({
                        'sno': value.sno,
                        'materialRefId': value.materialRefId,
                        'materialRefName': value.materialRefId,
                        'onHand': value.onHand,
                        'unitRefId': value.unitRefId,
                        'unitRefName': value.unitRefName,
                        'changeunitflag': false,
                    });

                });

                if (vm.errorflag == true) {
                    vm.saving = false;
                    return;
                }


                if (vm.templatesave.templatename != null)
                    vm.templatesave.templatename = vm.templatesave.templatename.toUpperCase();

                closingstockService.saveTemplateClosingStock({
                    closingstock: vm.closingstock,
                    closingStockDetail: vm.closingStockDetail,
                    templateSave: vm.templatesave,
                }).success(function (result) {
                    abp.notify.info(app.localize('Template') + ' ' + app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            }

            vm.save = function (argSaveOption)
            {
                if (vm.existall == false)
                    return;

                var convFactor = 0;
                var convExistFlag = false;

                vm.closingstockRecipeDetail = [];
                vm.errorflag = false;

                if (vm.templatesave != null) {
                    if (vm.templatesave.templateflag == true && vm.templatesave.templatename.length == 0) {
                        abp.message.warn(app.localize("TemplateNameRequired"));
                        return;
                    }
                    if (vm.templatesave.templatename != null)
                        vm.templatesave.templatename = vm.templatesave.templatename.toUpperCase();

                }

              
            
                vm.saving = true;
                vm.closingStockDetail = [];

                vm.errorflag = false;
                angular.forEach(vm.closingstockPortions, function (value, key) {
                    if (value.onHand == 0 || value.onHand==null)
                    {
                        abp.message.info(app.localize("ClosingOnHandShouldNotBeZero", value.materialRefName));
                    }
                    if (value.unitRefId == null || value.unitRefId == 0)
                    {
                        abp.message.warn(app.localize("UnitErr", value.materialRefName));
                        vm.errorflag = true;
                        return;
                    }
                   
                    vm.closingStockDetail.push({
                        'sno': value.sno,
                        'materialRefId': value.materialRefId,
                        'materialRefName': value.materialRefId,
                        'onHand': value.onHand,
                        'unitRefId': value.unitRefId,
                        'unitRefName': value.unitRefName,
                        'changeunitflag': false,
                    });
                   
                });


                if (vm.errorflag == true) {
                    vm.saving = false;
                    return;
                }


                closingstockService.createOrUpdateClosingStock({
                    closingstock: vm.closingstock,
                    closingStockDetail: vm.closingStockDetail,
                    templateSave: vm.templatesave,
                }).success(function (result) {
                    abp.notify.info(app.localize('ClosingStock')+ ' ' + app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {
                $scope.errmessage = "";
                if (moment(vm.closingstock.stockDate) > moment())
                {
                    $scope.errmessage = app.localize('FutureDateNotAllowed');
                }
                if ($scope.errmessage != "") {
                    abp.notify.warn($scope.errmessage, 'Required');
                    return false;
                }
                else {
                    return true;
                }
            };

            vm.addPortion = function (data)
            {
                if (data != null) {
                    if (data.sno < vm.sno -1 ) {
                        if (data.materialRefId == null || data.materialRefId == 0 || data.materialRefName == '') {
                            abp.notify.warn(app.localize('MaterialErr'));
                        }

                        if (data.unitRefId == 0 || data.unitRefId == null) {
                            abp.notify.warn(app.localize('UnitErr', data.materialRefName));
                            return;
                        }

                        if (data.onHand == 0 || data.onHand == null) {
                            abp.notify.info(app.localize('StockAsZeroAlert', data.materialRefName));
                        }

                        if (errorFlag) {
                            vm.saving = false;
                            vm.loading = false;
                            $scope.existall = false;
                        }

                        if (data.sno < vm.sno) {
                            $(this).next().focus();
                            return;
                        }
                    }
                    else {

                    }
                }

                if (vm.sno > 0) {
                    if (vm.existall() == false)
                        return;

                    var errorFlag = false;

                    if (vm.closingstockPortions.length <= 0) {
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }
                    else {
                        if (vm.isUndefinedOrNull(vm.closingstockPortions[0].materialRefId) || vm.closingstockPortions[0].materialRefId == 0) {
                            abp.notify.info(app.localize('MinimumOneDetail'));
                            return;
                        }
                    }

                    var lastelement = vm.closingstockPortions[vm.closingstockPortions.length - 1];

                    if (lastelement.materialRefId == null || lastelement.materialRefId == 0 || lastelement.materialRefName == '') {
                        abp.notify.warn(app.localize('MaterialErr'));
                    }

                    if (lastelement.unitRefId == 0 || lastelement.unitRefId == null) {
                        abp.notify.warn(app.localize('UnitErr', lastelement.materialRefName));
                        return;
                    }

                    if (lastelement.onHand == 0 || lastelement.onHand == null) {
                        abp.notify.info(app.localize('StockAsZeroAlert', lastelement.materialRefName));
                    }

                    if (errorFlag) {
                        vm.saving = false;
                        vm.loading = false;
                        $scope.existall = false;
                    }

                }


                vm.sno = vm.sno + 1;
                vm.closingstockPortions.push({
                    'sno': vm.sno, 'materialRefId': 0, 'materialRefName': '',  'onHand': null,
                    'defaultUnit': "", 'unitRefId': '', 'unitRefName': '', 'changeunitflag': false,
                });
            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex)
            {
                if(vm.closingstockPortions.length>1)
                {
                    vm.closingstockPortions.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                }
                else
                {
                    vm.closingstockPortions.splice(productIndex, 1);
                    vm.sno = 0;
                    vm.addPortion();
                    return;
                }
            }

            $scope.func = function (data, val,obj) {
                var forLoopFlag = true;

                angular.forEach(vm.closingstockPortions, function (value, key)
                {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag)
                {
                    data.materialRefId = 0;
                    data.materialRefName = '';
                    return;
                }
                data.materialRefName = val.materialName;
                data.defaultUnitId = val.unitRefId;
                data.defaultUnit = val.defaultUnitName;
                data.unitRefName = val.defaultUnitName;
                data.unitRefId = val.unitRefId;
            };

            vm.refconversionunit = [];
            vm.fillUnitConversion = function () {
                unitconversionService.getAll({
                }).success(function (result) {
                    vm.refconversionunit = result.items;
                });
            }

            vm.fillUnitConversion();


            vm.refmaterial = [];
            vm.tempRefMaterial = [];

            function fillDropDownRecipe() {
                materialService.getView({
                    //skipCount: requestParams.skipCount,
                    //maxResultCount: 1,
                    //sorting: requestParams.sorting,
                    //filter: vm.filterText,
                    operation: 'FillAll',
                }).success(function (result) {
                    vm.refmaterial = result.items;
                });
            }

            fillDropDownRecipe();
           

            vm.cancel = function () {
                $state.go("tenant.closingstock");
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.reflocation = [];

            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;
                
                });
            }

            vm.refrequest = [];
         
            vm.isUndefinedOrNull =function(val)
            {
                return angular.isUndefined(val) || val == null;
            };

            vm.checkCloseDay = function () {
							daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format('YYYY-MMM-DD')));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format('YYYY-MMM-DD')));

                    }
                    if (moment(result.accountDate).format("YYYY-MMM-DD") == moment().format("YYYY-MMM-DD"))
                        return;

                    $scope.minDate = moment(result.accountDate).format($scope.format);
                    $scope.maxDate = moment().format($scope.format);
                    vm.closingstock.stockDate = moment().format($scope.format);

                    $('input[name="closingDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: false,
                        startDate: vm.closingstock.stockDate,
                        minDate: $scope.minDate,
                        maxDate : $scope.maxDate
                    });

                }).finally(function (result) {

                });
            }


            function init() {
                vm.loading = true;

                fillDropDownLocation();

                closingstockService.getClosingStockForEdit({
                    Id: vm.closingstockId
                }).success(function (result)
                {
                    vm.closingstock = result.closingStock;                    
                    vm.closingstock.locationRefId = vm.defaultLocationId;

                    if (result.closingStock.id != null) {
                        vm.uilimit = null;
                        vm.closingStockDetail = result.closingStockDetail;

                        vm.closingstock.stockDate = moment(result.closingStock.stockDate).format($scope.format);
                        $scope.minDate = vm.closingstock.stockDate;

                        angular.forEach(result.closingStockDetail, function (value, key) {
                            vm.closingstockPortions.push({
                                'sno': value.sno,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'defaultUnit': value.defaultUnitName,
                                'defaultUnitId':value.defaultUnitId,
                                'onHand': value.onHand,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'changeunitflag': false,
                            })
                        });
                        vm.sno = vm.closingstockPortions.length;
                    }
                    else
                    {
                        vm.closingstock.stockDate = moment().format($scope.format);
                        vm.addPortion();
                        vm.checkCloseDay();
                        vm.uilimit = 20;
                    }
                    vm.loading = false;
                });
            }

            init();

            vm.removeUnfilled = function () {
                var listtoremoved = [];
                tempportions = [];

                angular.forEach(vm.closingstockPortions, function (value, key) {
                    if (value.onHand == 0 || value.onHand == null || value.onHand == "") {
                        listtoremoved.push(key)
                    }
                    else {
                        tempportions.push(value);
                    }
                });

                vm.closingstockPortions = tempportions;

                vm.sno = vm.closingstockPortions.length;

                if (vm.closingstockPortions.length == 0)
                    vm.addPortion();
            }

            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

            vm.unitReference = function (selected, data) {
                data.unitRefId = selected.value;
                data.unitRefName = selected.displayText;
                data.changeunitflag = false;
            }
        }
    ]);
})();

