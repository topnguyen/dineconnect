﻿(function () {
    appModule.controller('tenant.views.house.transaction.closingstock.closingstockMainForm', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.closingStock', 'abp.services.app.location', 'appSession', 'abp.services.app.material',
        'abp.services.app.unitConversion', 'abp.services.app.template', 'deviceDetector', 'abp.services.app.dayClose', 'abp.services.app.houseReport',
        function ($scope, $state, $stateParams, $modal, closingstockService, locationService, appSession, materialService, unitconversionService, templateService, deviceDetector, daycloseService, housereportService) {
            /* eslint-disable */

            var vm = this;

            vm.locationCode = $stateParams.id;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.gotoHome = function () {
                $state.go("dashboard", {
                    id: vm.locationCode
                });
            };

            vm.pullDetails = function () {
                vm.loading = true;
                locationService.getLocationById({ Id: vm.locationCode }).success(function (result) {
                    vm.location = result;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.pullDetails();

            vm.refmaterialgroupcategory = [];

            function fillDropDownMaterialGroupCategory() {
                materialService.getMaterialGroupCategoryForCombobox({}).success(function (result) {
                    vm.refmaterialgroupcategory = result.items;
                });
            }
            fillDropDownMaterialGroupCategory();

            vm.saving = false;
            vm.closingstock = null;
            $scope.existall = true;
            vm.includeMaterialNotMappedWithStockCycle = false;
            vm.isHighValueItemOnly = false;
            vm.enterOnHandFilter = -1;
            vm.closingstockId = $stateParams.id;
            vm.sno = 0;
            vm.closingstockPortions = [];
            vm.closingStockDetail = [];

            vm.defaultLocationId = appSession.user.locationRefId;
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.uilimit = null;
            vm.templatesave = null;

            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            $('input[name="closingDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                //minDate: $scope.minDate,
                //maxDate: moment()
            });


            vm.defaulttemplatetype = 6;

            vm.refTemplateDetail = [];
            vm.reftemplate = [];

            function fillDropDownTemplate() {
                vm.loading = true;
                templateService.getTemplateDetail({
                    locationRefId: vm.defaultLocationId,
                    templateType: vm.defaulttemplatetype
                }).success(function (result) {
                    vm.refTemplateDetail = result.items;
                    angular.forEach(vm.refTemplateDetail, function (value, key) {
                        vm.reftemplate.push({
                            'value': value.id,
                            'displayText': value.name,
                        });
                    });
                    vm.loading = false;
                });
            }

            fillDropDownTemplate();

            vm.fillDataFromTemplate = function () {
                vm.selectedTemplate = null;
                vm.refTemplateDetail.some(function (value, key) {
                    if (value.id == vm.template) {
                        vm.selectedTemplate = value.templateData;
                        return true;
                    }
                });

                if (vm.selectedTemplate == null)
                    return;

                vm.loading = true;

                closingstockService.getTemplateObjectForEdit({
                    objectString: vm.selectedTemplate
                }).success(function (result) {

                    vm.closingstock = result.closingStock;

                    vm.closingStockDetail = result.closingStockDetail;

                    vm.closingstock.stockDate = moment().format($scope.format);
                    vm.closingstock.id = null;
                    vm.closingstock.locationRefId = vm.defaultLocationId;

                    $('input[name="closingDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.closingstock.stockDate,
                        minDate: $scope.minDate,
                        maxDate: moment().format($scope.format)
                    });

                    vm.closingstockPortions = [];
                    vm.sno = 1;
                    angular.forEach(vm.closingStockDetail, function (value, key) {
                        vm.closingstockPortions.push({
                            'sno': vm.sno,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'materialPetName': value.materialPetName,
                            'materialGroupCategoryRefId': value.materialGroupCategoryRefId,
                            'materialGroupCategoryRefName': value.materialGroupCategoryRefName,
                            //'defaultUnit': value.unitRefName,
                            //'defaultUnitId': value.unitRefId,
                            'onHand': 0,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'changeunitflag': false,
                        });
                        vm.sno = vm.sno + 1;
                    });
                    vm.sno = vm.closingstockPortions.length + 1;

                    vm.loading = false;
                });
            }

            vm.fillScheduled = function () {
                vm.loading = true;
                housereportService.getStockTakenScheduledMaterialList({
                    locationRefId: vm.defaultLocationId,
                    stockTakenDate: vm.closingstock.stockDate,
                    isHighValueItemOnly: vm.isHighValueItemOnly,
                    multipleUomAllowed: true,
                    excludeMaterialWhichStockCycleNotAssigned: !vm.includeMaterialNotMappedWithStockCycle
                }).success(function (result) {
                    vm.closingstockPortions = [];
                    vm.sno = 1;
                    angular.forEach(result.materialStockList, function (value, key) {
                        vm.closingstockPortions.push({
                            'sno': vm.sno++,
                            'materialRefId': value.id,
                            'materialRefName': value.materialName,
                            'materialPetName': value.materialPetName,
                            'materialGroupCategoryRefId': value.materialGroupCategoryRefId,
                            'materialGroupCategoryRefName': value.materialGroupCategoryName,
                            'onHand': value.onHand,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.uom,
                            'changeunitflag': false,
                            'unitWiseDetails': value.unitWiseDetails,
                            'editFlag': false
                        })
                    });
                    vm.sno = vm.closingstockPortions.length;
                    vm.uilimit = null;
                    if (vm.closingstockPortions.length == 0) {
                        abp.notify.error('No More Materials Scheduled for Stock Taken');
                    }
                    else {
                        abp.notify.error(vm.closingstockPortions.length + ' Materials Scheduled for Stock Taken');
                    }
                }).finally(function () {
                    vm.loading = false;
                });
              
            }

            vm.templateSave = function () {
                vm.closingstockRecipeDetail = [];
                vm.errorflag = false;

                if (vm.templatesave.templatename.length == 0) {
                    abp.message.warn(app.localize("TemplateNameRequired"));
                    return;
                }

                vm.saving = true;
                vm.closingStockDetail = [];

                vm.errorflag = false;
                angular.forEach(vm.closingstockPortions, function (value, key) {
                    if (value.unitRefId == null || value.unitRefId == 0) {
                        abp.message.warn(app.localize("UnitErr", value.materialRefName));
                        vm.errorflag = true;
                        return;
                    }

                    vm.closingStockDetail.push({
                        'sno': value.sno,
                        'materialRefId': value.materialRefId,
                        'materialRefName': value.materialRefId,
                        'onHand': value.onHand,
                        'unitRefId': value.unitRefId,
                        'unitRefName': value.unitRefName,
                        'changeunitflag': false,
                    });

                });

                if (vm.errorflag == true) {
                    vm.saving = false;
                    return;
                }


                if (vm.templatesave.templatename != null)
                    vm.templatesave.templatename = vm.templatesave.templatename.toUpperCase();

                closingstockService.saveTemplateClosingStock({
                    closingstock: vm.closingstock,
                    closingStockDetail: vm.closingStockDetail,
                    templateSave: vm.templatesave,
                }).success(function (result) {
                    abp.notify.info(app.localize('Template') + ' ' + app.localize('SavedSuccessfully'));
                    vm.cancel(0);
                }).finally(function () {
                    vm.saving = false;
                });
            }

            vm.save = function (argSaveOption) {
                if (vm.existall == false)
                    return;

                var convFactor = 0;
                var convExistFlag = false;

                vm.closingstockRecipeDetail = [];
                vm.errorflag = false;

                if (vm.templatesave != null) {
                    if (vm.templatesave.templateflag == true && vm.templatesave.templatename.length == 0) {
                        abp.message.warn(app.localize("TemplateNameRequired"));
                        return;
                    }
                    if (vm.templatesave.templatename != null)
                        vm.templatesave.templatename = vm.templatesave.templatename.toUpperCase();

                }

                vm.saving = true;
                vm.closingStockDetail = [];

                vm.errorflag = false;
                angular.forEach(vm.closingstockPortions, function (value, key) {
                    if (value.onHand < 0) {
                        abp.message.info(value.materialRefName + ' ' + app.localize("ClosingOnHandShouldNotBeZero", value.materialRefName));
                        vm.errorflag = true;
                        return;
                    }
                    if (value.unitRefId == null || value.unitRefId == 0) {
                        abp.message.warn(app.localize("UnitErr", value.materialRefName));
                        vm.errorflag = true;
                        return;
                    }
                    if (value.onHand > 0) {
                        vm.closingStockDetail.push({
                            'sno': value.sno,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefId,
                            'onHand': value.onHand,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'changeunitflag': false,
                            'unitWiseDetails': value.unitWiseDetails
                        });
                    }
                });


                if (vm.errorflag == true) {
                    vm.saving = false;
                    return;
                }
                vm.loading = true;

                closingstockService.createOrUpdateClosingStock({
                    closingstock: vm.closingstock,
                    closingStockDetail: vm.closingStockDetail,
                    templateSave: vm.templatesave,
                }).success(function (result) {
                    abp.notify.info(app.localize('ClosingStock') + ' ' + app.localize('SavedSuccessfully'));
                    vm.cancel(0);
                }).finally(function () {
                    vm.saving = false;
                    vm.loading = false;
                });
            };

            vm.existall = function () {
                $scope.errmessage = "";
                if (moment(vm.closingstock.stockDate) > moment()) {
                    $scope.errmessage = app.localize('FutureDateNotAllowed');
                }
                if ($scope.errmessage != "") {
                    abp.notify.warn($scope.errmessage, 'Required');
                    return false;
                }
                else {
                    return true;
                }
            };

            vm.addPortion = function (data) {
                if (data != null) {
                    if (data.sno < vm.sno - 1) {
                        if (data.materialRefId == null || data.materialRefId == 0 || data.materialRefName == '') {
                            abp.notify.warn(app.localize('MaterialErr'));
                        }

                        if (data.unitRefId == 0 || data.unitRefId == null) {
                            abp.notify.warn(app.localize('UnitErr', data.materialRefName));
                            return;
                        }

                        if (data.onHand == 0 || data.onHand == null) {
                            abp.notify.info(app.localize('StockAsZeroAlert', data.materialRefName));
                        }
                        r
                        if (errorFlag) {
                            vm.saving = false;
                            vm.loading = false;
                            $scope.existall = false;
                        }

                        if (data.sno < vm.sno) {
                            $(this).next().focus();
                            return;
                        }
                    }
                    else {

                    }
                }

                if (vm.sno > 0) {
                    if (vm.existall() == false)
                        return;

                    var errorFlag = false;

                    if (vm.closingstockPortions.length <= 0) {
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }
                    else {
                        if (vm.isUndefinedOrNull(vm.closingstockPortions[0].materialRefId) || vm.closingstockPortions[0].materialRefId == 0) {
                            abp.notify.info(app.localize('MinimumOneDetail'));
                            return;
                        }
                    }

                    var lastelement = vm.closingstockPortions[vm.closingstockPortions.length - 1];

                    if (lastelement.materialRefId == null || lastelement.materialRefId == 0 || lastelement.materialRefName == '') {
                        abp.notify.warn(app.localize('MaterialErr'));
                    }

                    if (lastelement.unitRefId == 0 || lastelement.unitRefId == null) {
                        abp.notify.warn(app.localize('UnitErr', lastelement.materialRefName));
                        return;
                    }

                    if (lastelement.onHand == 0 || lastelement.onHand == null) {
                        abp.notify.info(app.localize('StockAsZeroAlert', lastelement.materialRefName));
                    }

                    if (errorFlag) {
                        vm.saving = false;
                        vm.loading = false;
                        $scope.existall = false;
                    }

                }

                angular.forEach(vm.closingstockPortions, function (value, key) {
                    value.editFlag = false;
                });

                vm.sno = vm.sno + 1;
                vm.closingstockPortions.push({
                    'sno': vm.sno, 'materialRefId': 0, 'materialRefName': '', 'onHand': 0,
                    'defaultUnit': "", 'unitRefId': '', 'unitRefName': '', 'changeunitflag': false,
                    'editFlag': true, 'unitWiseDetails': [], 'materialPetName': ''
                });
                vm.uilimit = 30;
            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex) {
                if (vm.closingstockPortions.length > 1) {
                    vm.closingstockPortions.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                }
                else {
                    vm.closingstockPortions.splice(productIndex, 1);
                    vm.sno = 0;
                    return;
                }
            }

            $scope.func = function (data, val, obj) {
                var forLoopFlag = true;

                angular.forEach(vm.closingstockPortions, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialName, value.materialRefName)) {
                            abp.notify.error(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.materialRefId = 0;
                    data.materialRefName = '';

                    return;
                }
                data.materialRefName = val.materialName;
                data.materialPetName = val.materialPetName;
                data.unitRefName = val.defaultUnitName;
                data.unitRefId = val.unitRefId;
                data.materialGroupCategoryRefId = val.materialGroupCategoryRefId;
                data.unitWiseDetails = val.unitWiseDetails;
                angular.forEach(data.unitWiseDetails, function (value, key) {
                    value.onHand = '';
                });
            };

            vm.refmaterial = [];

            function fillDropDownRecipe() {
                vm.loadingCount++;
                materialService.getMaterialViewWithUnitList({
                    operation: 'FillAll',
                }).success(function (result) {
                    vm.refmaterial = result.items;
                    vm.loadingCount--;
                });
            }

            fillDropDownRecipe();


            vm.setEditFlag = function (data, index) {
                vm.loading = true;
                angular.forEach(vm.closingstockPortions, function (value, key) {
                    value.editFlag = false;
                });
                data.editFlag = true;
                vm.loading = false;
                vm.uilimit = null;
            }

            vm.refconversionunit = [];
            vm.fillUnitConversion = function () {
                unitconversionService.getAllUnitConversionWithBaseIds({
                }).success(function (result) {
                    vm.refconversionunit = result;
                });
            }

            vm.fillUnitConversion();

            vm.cancel = function (argOption) {
                if (argOption == 1) {
                    abp.message.confirm(
                        app.localize('CancelEntry?'),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                $state.go("tenant.closingstockentry");
                                //$rootScope.settings.layout.pageSidebarClosed = false;
                            }
                        }
                    );
                }
                else {
                    $state.go("tenant.closingstockentry");
                    //$rootScope.settings.layout.pageSidebarClosed = false;
                }
            };



            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.reflocation = [];

            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;

                });
            }

            vm.refrequest = [];

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.checkCloseDay = function () {
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format('YYYY-MMM-DD')));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format('YYYY-MMM-DD')));

                    }
                    if (moment(result.accountDate).format("YYYY-MMM-DD") == moment().format("YYYY-MMM-DD"))
                        return;

                    $scope.minDate = moment(result.accountDate).add(-30, 'days').format($scope.format);
                    $scope.maxDate = moment().format($scope.format);
                    vm.closingstock.stockDate = moment().format($scope.format);

                    $('input[name="closingDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: false,
                        startDate: vm.closingstock.stockDate,
                        minDate: $scope.minDate,
                        maxDate: $scope.maxDate
                    });

                }).finally(function (result) {

                });
            }


            function init() {
                vm.loading = true;

                fillDropDownLocation();

                closingstockService.getClosingStockForEdit({
                    Id: vm.closingstockId
                }).success(function (result) {
                    vm.closingstock = result.closingStock;
                    vm.closingstock.locationRefId = vm.defaultLocationId;

                    if (result.closingStock.id != null) {
                        vm.uilimit = null;
                        vm.closingStockDetail = result.closingStockDetail;

                        vm.closingstock.stockDate = moment(result.closingStock.stockDate).format($scope.format);
                        $scope.minDate = vm.closingstock.stockDate;

                        vm.sno = 1;
                        angular.forEach(result.closingStockDetail, function (value, key) {
                            vm.closingstockPortions.push({
                                'sno': vm.sno++,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'materialPetName': value.materialPetName,
                                'materialGroupCategoryRefId': value.materialGroupCategoryRefId,
                                'materialGroupCategoryRefName': value.materialGroupCategoryRefName,
                                'onHand': value.onHand,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'changeunitflag': false,
                                'unitWiseDetails': value.unitWiseDetails,
                                'editFlag': false
                            })
                        });
                        vm.sno = vm.closingstockPortions.length;
                    }
                    else {
                        vm.closingstock.stockDate = moment().format($scope.format);
                        vm.checkCloseDay();
                        vm.uilimit = 30;
                    }
                    vm.loading = false;
                });
            }

            init();

            vm.removeUnfilled = function () {
                var listtoremoved = [];
                tempportions = [];

                angular.forEach(vm.closingstockPortions, function (value, key) {
                    if (value.onHand == 0 || value.onHand == null || value.onHand == "") {
                        listtoremoved.push(key);
                    }
                    else {
                        tempportions.push(value);
                    }
                });

                vm.closingstockPortions = tempportions;

                vm.sno = vm.closingstockPortions.length;

                if (vm.closingstockPortions.length == 0)
                    vm.addPortion();
            }

            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            };

            vm.unitReference = function (selected, data) {
                data.unitRefId = selected.value;
                data.unitRefName = selected.displayText;
                data.changeunitflag = false;
            };

            vm.checkAndImport = function (argOption) {
                if (vm.closingstock.stockDate == null) {
                    abp.notify.warn(app.localize("SelectStockTakenDateErr"));
                    return;
                }
                vm.loading = true;
                if (argOption == 2 && vm.closingstock.id != null) {
                    vm.save(argOption);
                    return;
                }
                closingstockService.isLocationClosingStockExists({
                    locationRefId: vm.defaultLocationId,
                    stockTakenDate: vm.closingstock.stockDate
                }).success(function (result) {
                    if (result.count > 0) {
                        abp.notify.error(app.localize('ClosingStock') + ' ' + app.localize('AlreadyExists'));
                        abp.message.warn(app.localize('ClosingStock') + ' ' + app.localize('AlreadyExists'));
                    }
                    else {
                        if (argOption == 0)
                            vm.getDataForStockAdjustmentExcel();
                        else
                            vm.save(argOption);
                    }
                    vm.loading = false;
                }).finally(function () {
                    vm.saving = false;
                    vm.loading = false;
                });
            }



            vm.calculateDefaultOnHand = function (mat) {
                var onHand = 0.0;

                angular.forEach(mat.unitWiseDetails, function (lst, key) {
                    if (vm.isUndefinedOrNull(lst.onHand) == true || lst.onHand=='') {
                        //do nothing
                    }
                    else {
                        if (mat.unitRefId == lst.unitRefId) {
                            lst.conversion = 1;
                            lst.convertedQuantity = lst.onHand;
                            onHand = parseFloat(onHand) + parseFloat(lst.onHand);
                        }
                        else {
                            var unitConversion = null;
                            vm.refconversionunit.some(function (ucvalue, uckey) {
                                if (ucvalue.baseUnitId == lst.unitRefId && ucvalue.refUnitId == mat.unitRefId) {
                                    unitConversion = ucvalue;
                                    return true;
                                }
                            });

                            if (unitConversion == null) {
                                abp.notify.error('Unit Conversion Error For Material ' + mat.materialRefName + ' From Unit : ' + mat.unitRefName + ' To Unit ' + lst.unitRefName);
                                mat.onHand = 0;
                                return;
                            }
                            //var unitConversion =
                            //    rsUc.FirstOrDefault(
                            //        t => t.BaseUnitId == lst.UnitRefId && t.RefUnitId == mat.DefaultUnitId);
                            //if (unitConversion == null)
                            //    throw new UserFriendlyException(L("UnitConversion") + " " + L("NotExist") + " - " +
                            //        lst.UnitRefName);
                            lst.conversion = unitConversion.conversion;
                            lst.convertedQuantity = parseFloat(unitConversion.conversion * lst.onHand).toFixed(unitConversion.decimalPlaceRounding); //, MidpointRounding.AwayFromZero);
                            onHand = parseFloat(onHand) + parseFloat(lst.convertedQuantity); //, MidpointRounding.AwayFromZero);
                        }

                        //if (value.unitRefId == null || value.unitRefId == 0) {
                        //    abp.message.warn(app.localize("UnitErr", value.materialRefName));
                        //    vm.errorflag = true;
                        //    return;
                        //}
                    }
                });

                mat.onHand = onHand;

            }

            vm.getDataForStockAdjustmentExcel = function () {

                if (vm.closingstock.stockDate == null) {
                    abp.notify.warn(app.localize("SelectStockTakenDateErr"));
                    return;
                }

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/adjustment/importStockTakenModal.cshtml',
                    controller: 'tenant.views.house.transaction.adjustment.importStockTakenModal as vm',
                    resolve: {
                        locationRefId: function () {
                            return vm.defaultLocationId;
                        },
                        allItems: function () {
                            return null;
                        },
                        stockTakenDate: function () {
                            return vm.closingstock.stockDate;
                        },
                        isHighValueItemOnly: function () {
                            return false;
                        },
                        forceAdjustmentFlag: function () {
                            return false;
                        },
                        stockEntryOnlyFlag: function () {
                            return true;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    if (result != null) {
                        vm.uilimit = null;
                        vm.closingstockPortions = [];
                        vm.sno = 1;
                        angular.forEach(result, function (value, key) {
                            vm.closingstockPortions.push({
                                'sno': vm.sno,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialName,
                                //'defaultUnit': value.unitRefName,
                                //'defaultUnitId': value.unitRefId,
                                'onHand': value.currentStock,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'changeunitflag': false,
                                'unitWiseDetails': value.unitWiseQuantityList,
                                'editFlag': false
                            });
                            vm.sno = vm.sno + 1;
                        });
                        vm.sno = vm.closingstockPortions.length;
                        vm.readfromexcelstatus = false;
                        vm.autolock = true;
                        vm.excelautopost = true;
                        if (vm.forceAdjustmentFlag && vm.forceAdjustmentWithDayCloseflag) {
                            vm.adjustment.adjustmentDate = vm.closingTakenDate;
                            abp.notify.warn(app.localize('ForceCloseWarning', moment(vm.closingTakenDate).format($scope.format)));
                        }
                    }
                });
            };

        }
    ]);
})();