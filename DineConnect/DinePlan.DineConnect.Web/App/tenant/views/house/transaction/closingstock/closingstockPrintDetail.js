﻿(function () {
    appModule.controller('tenant.views.house.transaction.issue.issuePrintDetail', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.closingstock', 'appSession',  'abp.services.app.material',
        
        function ($scope, $filter, $state, $stateParams, $modal, uiGridConstants, closingstockService, appSession,
           materialService) {

					var vm = this;

					vm.connectdecimals = appSession.connectdecimals;
					vm.housedecimals = appSession.housedecimals;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            //vm.defaultLocationId = appSession.user.locationRefId;

            
            vm.recipePortions = [];
            vm.closingstockId = $stateParams.printid;

            vm.headerline = "~~~~~~~~~~~~~~~~~~~~";

            vm.getData = function () {
                vm.loading = true;
             closingstockService.getClosingStockForEdit({
                    Id: vm.closingstockId
            }).success(function (result)
            {
                vm.closingstock = result.closingstock;
              

                    vm.closingstockPortions = [];

                    vm.sno = 0;
         
                    vm.recipePortions = [];
                    if (result.closingstockRecipeDetail != null && result.closingstockRecipeDetail.length > 0) {
                        vm.recipeflag = true;
                    }
                    else {
                        vm.recipeflag = false;
                        vm.recipename = app.localize("General");
                    }

                    angular.forEach(result.closingstockRecipeDetail, function (value, key) {
                        vm.recipePortions.push({
                            'sno': value.sno,
                            'recipeRefId': value.recipeRefId,
                            'recipeRefName': value.recipeRefName,
                            'recipeProductionQty': value.recipeProductionQty,
                            'unitRefId': value.unitRefId,
                            'defaultUnit': value.uom,
                            'multipleBatchProductionAllowed': value.multipleBatchProductionAllowed,
                            'completedQty': value.completedQty,
                            'remarks': value.remarks,
                            'completionFlag': value.completionFlag,
                            'currentInHand': value.currentInHand
                        })
                    });


                    vm.closingstockdetail = result.closingstockDetail;

                    angular.forEach(vm.closingstockdetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.closingstockPortions.push({
                            'sno': value.sno,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'closingstockQty': value.closingstockQty,
                            'defaultUnit': value.defaultUnit,
                            'unitRefName' : value.unitRefName
                        });
                    });
                    vm.loading = false;
                });
            }

            vm.getData();

        }]);
})();

