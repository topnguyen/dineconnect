﻿
(function () {
    appModule.controller('tenant.views.house.transaction.closingstock.mobileIndex', [
			'$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.closingStock', 'appSession', 'abp.services.app.location', 'abp.services.app.dayClose', 
			function ($scope, $state, $modal, uiGridConstants, closingstockService, appSession, locationService, daycloseService) {

            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.defaultLocationId = appSession.user.locationRefId;
            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Transaction.ClosingStock.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Transaction.ClosingStock.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Transaction.ClosingStock.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.advancedFiltersAreShown = false;
            vm.dateFilterApplied = false;
            vm.outputData = [];

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };


            vm.userGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
					{
						name: app.localize('Actions'),
						enableSorting: false,
						width: 120,

						cellTemplate:
						   "<div class=\"ui-grid-cell-contents\">" +
							    "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
							   "    <ul uib-dropdown-menu>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editClosingStock(row.entity)\">" + app.localize("Edit") + "</a></li>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteClosingStock(row.entity)\">" + app.localize("Delete") + "</a></li>" +
							   "    </ul>" +
							   "  </div>" +
							   "</div>"
					},
                    {
                        name: app.localize('Id'),
                        field: 'id'
                    },
                    {
                        name: app.localize('StockDate'),
                        field: 'stockDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                        minWidth: 100
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.printClosingStock = function (myObject) {
                $state.go('tenant.closingstockprint', {
                    printid: myObject.id
                });
            };

            vm.clear = function () {
                vm.filterText = null;
                vm.dateRangeModel = null;
                vm.dateFilterApplied = false;
                vm.getAll();

            }

            vm.getAll = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                //var requestSlipNumber = null;

                if (vm.advancedFiltersAreShown) {
                    if (vm.dateFilterApplied) {
                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                    //requestSlipNumber = vm.requestSlipNumber;
                }
                vm.outputData = [];
                closingstockService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                    vm.outputData = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editClosingStock = function (myObj) {
                if (myObj.approvedUserId !=null)
                {
                    abp.notify.warn(app.localize('AlreadyApproved'));
                    return;
                }
                openCreateOrEditModal(myObj.id);
            };

            vm.createClosingStock = function () {
                openCreateOrEditModal(null);
            };

            vm.deleteClosingStock = function (myObject) {
                   if (myObject.approvedUserId != null) {
                    abp.notify.warn(app.localize('AlreadyApproved'));
                    return;
                }
                abp.message.confirm(
                    app.localize('DeleteClosingStockWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            closingstockService.deleteClosingStock({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.mobileClosingstockdetail", {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                //var requestSlipNumber = null;

                if (vm.advancedFiltersAreShown) {
                    if (vm.dateFilterApplied) {
                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                    //requestSlipNumber = vm.requestSlipNumber;
                }
                closingstockService.getAllToExcel({
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting,
                filter: vm.filterText,
                defaultLocationRefId: vm.defaultLocationId,
                startDate: startDate,
                endDate: endDate,
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            };

            vm.getAll();
               vm.checkCloseDay = function () {
								 daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {

                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                    }
                  

                }).finally(function () {
                });
            }

            vm.checkCloseDay();
        }]);
})();

