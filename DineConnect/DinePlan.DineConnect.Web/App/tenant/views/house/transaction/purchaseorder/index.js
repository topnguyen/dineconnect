﻿
(function () {
    appModule.controller('tenant.views.house.transaction.purchaseorder.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.purchaseOrder', 'appSession', 'abp.services.app.setup', 'abp.services.app.location', 'abp.services.app.material', 'abp.services.app.dayClose', 
        function ($scope, $state, $modal, uiGridConstants, purchaseorderService, appSession, setupService, locationService, materialService, daycloseService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");
            console.log("PO Index.js Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.totalPending = 0;
            vm.totalApproved = 0;
            vm.totalDeclined = 0;
            vm.totalYetToReceived = 0;
            vm.poReferenceCode = null;
            vm.id = null;

            vm.defaultLocationId = appSession.user.locationRefId;

            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.isPurchaseAllowed = appSession.location.isPurchaseAllowed;

            vm.advancedFiltersAreShown = false;
            vm.dateFilterApplied = false;

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            if (vm.isPurchaseAllowed == true) {
                vm.permissions = {
                    create: abp.auth.hasPermission('Pages.Tenant.House.Transaction.PurchaseOrder.Create'),
                    edit: abp.auth.hasPermission('Pages.Tenant.House.Transaction.PurchaseOrder.Edit'),
                    importData: abp.auth.hasPermission('Pages.Tenant.Setup.ImportData'),
                    'delete': abp.auth.hasPermission('Pages.Tenant.House.Transaction.PurchaseOrder.Delete'),
                    approve: abp.auth.hasPermission('Pages.Tenant.House.Transaction.PurchaseOrder.Approve'),
                    createInvoice: abp.auth.hasPermission('Pages.Tenant.House.Transaction.Invoice.Create'),
                    canEditApprovePurchaseOrderIfNotLinked: abp.auth.hasPermission('Pages.Tenant.House.Transaction.PurchaseOrder.CanEditApprovePurchaseOrderIfNotLinked'),
                    canViewMultipleLocationPurchaseOrder: abp.auth.hasPermission('Pages.Tenant.House.Transaction.PurchaseOrder.CanViewMultipleLocationPurchaseOrder'),
                };
            }
            else {
                vm.permissions = {
                    create: false,
                    edit: false,
                    importData: false,
                    'delete': false,
                    approve: false,
                    createInvoice: false,
                    canEditApprovePurchaseOrderIfNotLinked: false,
                    canViewMultipleLocationPurchaseOrder : false
                };
            }

            vm.requestParams = {
                userName: '',
                serviceName: '',
                methodName: '',
                browserInfo: '',
                hasException: '',
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.editApprovePurchaseOrderIfNotLinked = function (argPo) {
                purchaseorderService.checkCanEditThePurchaseOrder({
                    id: argPo.id
                }).success(function (result) {
                    if (result.successFlag == true) {
                        openCreateOrEditModal(argPo.id, false);
                    }
                    else {
                        abp.notify.error(result.errorMessage);
                        abp.message.warn(result.errorMessage);
                        vm.viewPoDetail(argPo);
                    }
                });
            }

            vm.createInvoiceFromPo = function (argPo) {
                $state.go("tenant.invoicedetailfromPo", {
                    id: null,
                    invoiceType: null,
                    poRefId : argPo.id
                });
            }



            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editPurchaseOrder(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.approve\" ng-click=\"grid.appScope.approvePurchaseOrder(row.entity)\">" + app.localize("Approve") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deletePurchaseOrder(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.create\" ng-click=\"grid.appScope.viewPoDetail(row.entity)\">" + app.localize("ViewDetail") + '/' + app.localize('SetPoAsCompletion') + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.printPurchaseOrder(row.entity)\">" + app.localize("Print") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.createInvoice && row.entity.poCurrentStatus=='Approved'\" ng-click=\"grid.appScope.createInvoiceFromPo(row.entity)\">" + app.localize("ConvertAsInvoice") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.canEditApprovePurchaseOrderIfNotLinked && row.entity.poCurrentStatus=='Approved'\" ng-click=\"grid.appScope.editApprovePurchaseOrderIfNotLinked(row.entity)\">" + app.localize("Edit") + ' ' + app.localize("Approved") + "</a></li>" +
                            
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Id'),
                        field: 'id',
                        maxWidth: 80
                    },
                    {
                        name: app.localize('Date'),
                        field: 'poDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                        width: 100
                    },
                    {
                        name: app.localize('Reference'),
                        field: 'poReferenceCode',
                        minWidth: 180
                    },
                    {
                        name: app.localize('SupplierName'),
                        field: 'supplierName',
                        minWidth: 250
                    },
                    {
                        name: app.localize('Location'),
                        field: 'locationRefName',
                        maxWidth: 80
                    },
                    {
                        name: app.localize('ShippingLocation'),
                        field: 'shippingLocationName'
                    },
                    {
                        name: app.localize('Status'),
                        field: 'poCurrentStatus',
                        minWidth: 80,
                        maxWidth: 130
                    },
                    {
                        name: app.localize('LinkedWith'),
                        field: 'poCurrentStatus',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.poBasedOnInterTransferRequest==true" class="label label-success">' + app.localize('Transfer') + '</span>' +
                            '  <span ng-show="row.entity.poBasedOnCater==true" class="label label-danger">' + app.localize('Cater') + '</span>' +
                            '  <span ng-show="row.entity.poBasedOnCater==true"><br/></span>' +
                            '  <span ng-show="row.entity.poBasedOnCater==true" ng-repeat="item in row.entity.caterQuoteDetails" style="color:blue;" >{{item.caterCustomerCustomerName}} - {{item.eventDateTimeString}} - {{item.id}}<br/> </span>' +
                            '</div>',
                        minWidth: 80
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };


            vm.printPurchaseOrder = function (myObject) {
                var currentStatus = myObject.poCurrentStatus;
                if (currentStatus == app.localize('Approved') || currentStatus == app.localize('Completed') || currentStatus == app.localize('Partial')) {

                    //purchaseorderService.getPurchaseOrderReportInNotePad({
                    //    purchaseOrderId: myObject.id
                    //}).success(function (result) {
                    //    vm.potext = result;
                    //});
                    $state.go('tenant.purchaseorderprint', {
                        printid: myObject.id
                    });
                }
                else {
                    abp.message.error(app.localize('CouldNotPrintPurchaseOrder'), app.localize(currentStatus));
                    return;
                }
            };

            vm.downloadDocument = function (employeerow) {
                fileDownload(employeerow.id);
            };

            function fileDownload(objId) {

                purchaseorderService.getPurchaseOrderForEdit({
                    Id: objId
                }).success(function (result) {
                    vm.purchaseorder = result.purchaseOrder;

                    var savePerson = function (person) {
                        return abp.ajax({
                            url: '/DocumentUpload/DownloadPoUpload',
                            data: JSON.stringify(person)
                        });
                    };

                    //Create a new person
                    var newPerson = {
                        locationRefId: vm.purchaseorder.locationRefId,
                        supplierRefId: vm.purchaseorder.supplierRefId
                    };

                    savePerson(newPerson);
                    //    .done(function (data) {
                    //    abp.notify.success('created new person with id = ' + data.personId);
                    //});

                });

            }

            vm.upLoadPurchaseOrder = function (myObject) {
                openUpload(myObject.id);
            };

            function openUpload(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/purchaseorder/poUpload.cshtml',
                    controller: 'tenant.views.house.transaction.purchaseorder.poUpload as vm',
                    backdrop: 'static',
                    resolve: {
                        poId: function () {
                            return objId;
                        },
                    }
                });

                //$state.go('tenant.purchaseorderupload', {
                //    id: myObject.id
                //})
            };


            vm.viewPoDetail = function (myObject) {
                openView(myObject.id);
            };

            function openView(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/purchaseorder/podetailViewInGrid.cshtml',
                    controller: 'tenant.views.house.transaction.purchaseorder.podetailViewInGrid as vm',
                    backdrop: 'static',
                    resolve: {
                        poId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                }).finally(function (result) {
                    vm.getAll();
                });
            }

            vm.clear = function () {
                vm.filterText = null;
                vm.poReferenceCode = null;
                vm.dateRangeModel = null;
                vm.dateFilterApplied = false;
                vm.status = null;
                vm.materialRefId = null;
                vm.id = null;
                vm.getAll();
            }

            vm.getAll = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var poReferenceCode = null;
                var status = null;
                var materialRefId = null;

                if (vm.dateFilterApplied) {
                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }
                poReferenceCode = vm.poReferenceCode;
                status = vm.status;
                materialRefId = vm.materialRefId
                console.log("PO Index.js getAllWithRefName Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                purchaseorderService.getAllWithRefName({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    id: vm.id,
                    filter: vm.filterText,
                    defaultLocationRefId: 0, // vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    poReferenceCode: poReferenceCode,
                    poStatus: status,
                    materialRefId: materialRefId,
                    locationGroup: vm.locationGroup,
                    //userId: vm.currentUserId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                    console.log("PO Index.js getAllWithRefName() End Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));

                });
            };

            vm.editPurchaseOrder = function (myObj) {
                var currentStatus = myObj.poCurrentStatus;
                if (currentStatus == app.localize('Pending'))
                    openCreateOrEditModal(myObj.id, false);
                else {
                    abp.message.error(app.localize('EditNotAllowedApproved'), app.localize('CouldNotEdit'));
                }
            };

            vm.approvePurchaseOrder = function (myObj) {
                var currentStatus = myObj.poCurrentStatus;
                if (currentStatus == app.localize('Pending'))
                    openCreateOrEditModal(myObj.id, false);
                else if (currentStatus == app.localize('Declined')) {
                    abp.message.error(app.localize('AlreadyDeclined'), app.localize('Declined'));
                }
                else {
                    abp.message.error(app.localize('AlreadyApproved'), app.localize('Approved'));
                }
            };

            vm.createPurchaseOrder = function () {
                openCreateOrEditModal(null, false);
            };

            vm.deletePurchaseOrder = function (myObject) {
                var currentStatus = myObject.poCurrentStatus;
                if (currentStatus == app.localize('Approved') || currentStatus == app.localize('Completed') || currentStatus == app.localize('Declined') || currentStatus == app.localize('Partial')) {
                    abp.message.error(app.localize('CouldNotDeletePurchaseOrder'), app.localize(currentStatus));
                    return;
                }

                abp.message.confirm(
                    app.localize('DeletePurchaseOrderWarning', myObject.id + " " + myObject.poReferenceCode),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            purchaseorderService.deletePurchaseOrder({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId, autoPoFlag) {
                $state.go('tenant.purchaseorderdetail', {
                    id: objId,
                    autoPoFlag: autoPoFlag
                });
            }

            vm.exportToExcel = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var poReferenceCode = null;

                if (vm.advancedFiltersAreShown) {
                    if (vm.dateFilterApplied) {
                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                    poReferenceCode = vm.poReferenceCode;
                }

                purchaseorderService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    poReferenceCode: poReferenceCode
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            };

            vm.getDashBoard = function () {
                vm.loading = true;
                console.log("PO Index.js getDashBoardPurchaseOrder() Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                purchaseorderService.getDashBoardPurchaseOrder({
                    Id: vm.defaultLocationId
                }).success(function (result) {
                    vm.totalPending = result.totalPending;
                    vm.totalApproved = result.totalApproved;
                    vm.totalYetToReceived = result.totalYetToReceived;
                    vm.totalDeclined = result.totalDeclined;
                }).finally(function () {
                    vm.loading = false;
                    console.log("PO Index.js getDashBoardPurchaseOrder() End Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));

                });
            };

            vm.getDashBoard();

            //  Location Link Start
            vm.setDefaultLocationAsSelectedLocation = function () {
                vm.loading = true;
                locationService.getSimpleAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    locationRefId: vm.defaultLocationId,
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.locationGroup.locations = result.items;
                }).finally(function () {
                    vm.loading = false;
                    vm.getAll();
                });
            }


            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForSearch();
                //vm.locationGroup.single = true;
                vm.setDefaultLocationAsSelectedLocation();
            };

            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                    //vm.getAll();
                });
            };

            //  Location Link End

            vm.refmaterial = [];


            function fillDropDownMaterial() {
                materialService.getMaterialForCombobox({}).success(function (result) {
                    vm.refmaterial = result.items;
                });
            }

            fillDropDownMaterial();

            vm.autoPo = function () {

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/purchaseorder/AutoPoSetting.cshtml',
                    controller: 'tenant.views.house.transaction.purchaseorder.autoPoSetting as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        locationId: function () {
                            return vm.defaultLocationId;
                        },
                        autoMode: function () {
                            return 'PO';
                        },
                    }
                });

                modalInstance.result.then(function (result) {

                });

            }

            vm.checkCloseDay = function () {
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        //if (vm.messagenotificationflag)
                        //    abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                }).finally(function () {

                });
            }


            vm.checkCloseDay();
        }]);
})();

