﻿    (function () {
        appModule.controller('tenant.views.house.transaction.purchaseorder.fileDownload', [
     '$scope', '$uibModalInstance', 'uiGridConstants', 'abp.services.app.purchaseOrder', 'poId', 'appSession','$http',
        function ($scope, $modalInstance, uiGridConstants, purchaseorderService, poId, appSession,http) {

            var vm = this;
            
            vm.saving = false;
            vm.purchaseorder = null;
			$scope.existall = true;

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 function init() {
				    
                    purchaseorderService.getPurchaseOrderForEdit({
                        Id: poId
                    }).success(function (result) {
                          vm.purchaseorder = result.purchaseOrder;

                        //if(vm.purchaseorder.id != null){
                        //    if (vm.purchaseorder.fileName == null) {
                        //        vm.cancel();
                        //        alert("No File Is Uploaded To Download");
                                
                        //    }
                        //    else {

                         // $http({
                         //     method: 'POST',
                         //     url: '/DocumentUpload/DownloadPoUpload',
                         //     data: {
                         //         locationRefId: vm.purchaseorder.locationRefId,
                         //         supplierRefId: vm.purchaseorder.supplierRefId
                         //     }
                         // }).success(function (result) {

                         // }).error(function (error) {
                         //  //Showing error message 
                         //  $scope.status = 'Unable to connect' + error.message;
                         //});


                          var savePerson = function (person) {
                              return abp.ajax({
                                  url: '/DocumentUpload/DownloadPoUpload',
                                  data: JSON.stringify(person)
                              });
                          };

                        //Create a new person
                          var newPerson = {
                              locationRefId: vm.purchaseorder.locationRefId,
                              supplierRefId: vm.purchaseorder.supplierRefId
                          };

                          savePerson(newPerson).done(function (data) {
                              abp.notify.success('created new person with id = ' + data.personId);
                          });
                                
                          vm.importpath = abp.appPath + 'DocumentUpload/DownloadPoUpload?locationRefId=' + vm.purchaseorder.locationRefId
                                                                            + "&supplierRefId=" + vm.purchaseorder.supplierRefId;

                        //    } 
                        //}
                                          
                    });
			     }

			     init();

			  
        }
    ]);
})();

