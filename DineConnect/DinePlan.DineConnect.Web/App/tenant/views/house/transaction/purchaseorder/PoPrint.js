﻿(function () {
    appModule.controller('tenant.views.house.transaction.purchaseorder.PoPrint', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.purchaseOrder', 'appSession', 'abp.services.app.company',
        'abp.services.app.supplier', 'abp.services.app.location', '$rootScope', '$document',
        function ($scope, $filter, $state, $stateParams, $modal, uiGridConstants, purchaseorderService, appSession, companyService,
            supplierService, locationService, $rootScope, $document) {

            var vm = this;
            vm.printmailflag = true;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            
            vm.company = null;
            vm.loading = true;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.connectdecimals = appSession.connectdecimals;
            vm.housedecimals = appSession.housedecimals;

            vm.purchaseorderId = $stateParams.printid;

            vm.headerline = "~~~~~~~~~~~~~~~~~~~~";

            vm.gotoIndex = function () {
                $state.go('tenant.purchaseorder');
                //$rootScope.settings.layout.pageSidebarClosed = false;
            }

            vm.sendMail = function () {
                vm.potext = '';
                vm.loading = true;
                purchaseorderService.sendPurchaseOrderEmail(
                    {
                        purchaseOrderId: vm.purchaseorderId,
                        purchaseOrderHTML: vm.potext
                    }).success(function (result) {

                        abp.message.success(app.localize('MailedSuccessfully'));
                        abp.notify.success(app.localize('MailedSuccessfully'));
                    }).finally(function () {
                        vm.loading = false;
                    });
            };

            vm.getData = function () {
                vm.caterQuoteDetails = [];
                purchaseorderService.getPurchaseOrderForEdit({
                    Id: vm.purchaseorderId
                }).success(function (result) {
                    vm.purchaseOrder = result.purchaseOrder;
                    vm.purchaseOrderDetail = result.purchaseOrderDetail;
                    vm.caterQuoteDetails = result.caterQuoteDetails;

                    

                    vm.purchaseDetailPortion = [];

                    vm.sno = 0;
                    vm.subtotalamount = 0;
                    vm.subtaxamount = 0;
                    vm.subnetamount = 0;

                    vm.deliverydate = $filter('date')(new Date(vm.purchaseOrder.deliveryDateExpected), 'fullDate');

                    vm.termdelivery = app.localize('TermDelivery', vm.deliverydate);

                    angular.forEach(vm.purchaseOrderDetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.subtotalamount = vm.subtotalamount + value.totalAmount;
                        vm.subtaxamount = vm.subtaxamount + value.taxAmount;
                        vm.subnetamount = vm.subnetamount + value.netAmount;

                        vm.discFlag = null;
                        if (value.discountOption == 'F')
                            vm.discFlag = false;
                        else if (value.discountOption == 'P')
                            vm.discFlag = true;
                        vm.purchaseDetailPortion.push({
                            'sno': vm.sno,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'supplierMaterialAliasName': value.supplierMaterialAliasName,
                            'qtyOrdered': value.qtyOrdered,
                            'uom': value.uom,
                            'unitRefName': value.unitRefName,
                            'unitRefId': value.unitRefId,
                            'price': value.price,
                            'discountOption': vm.discFlag,
                            'discountValue': value.discountValue,
                            'totalAmount': value.totalAmount,
                            'taxAmount': value.taxAmount,
                            'netAmount': value.netAmount,
                            'remarks': value.remarks,
                            'currentInHand': value.currentInHand,
                            'alreadyOrderedQuantity': value.alreadyOrderedQuantity,
                            'taxlist': value.taxForMaterial
                        });
                    });

                    //userService.getUserForEdit({
                    //    Id: vm.purchaseOrder.creatorUserId
                    //}).success(function (result) {
                    //    vm.user = result.user;
                    //});

                    locationService.getUserInfoBasedOnUserId({
                        id: vm.purchaseOrder.creatorUserId
                    }).success(function (result) {
                        vm.user = result;
                    });

                    supplierService.getSupplierForEdit({
                        Id: vm.purchaseOrder.supplierRefId
                    }).success(function (result) {
                        vm.supplier = result.supplier;
                        var combinedName = vm.supplier.supplierName + " " + moment(vm.purchaseOrder.poDate).format('DDMMMYYYY') + " " + vm.purchaseOrder.poReferenceCode;
                        combinedName = combinedName.replaceAll(".", "");
                        combinedName = combinedName.replaceAll("*", "");
                        combinedName = combinedName.replaceAll("'", "");
                        combinedName = combinedName.replaceAll("|", "");
                        combinedName = combinedName.replaceAll("=", "");
                        combinedName = combinedName.replaceAll("{", "");
                        combinedName = combinedName.replaceAll("}", "");
                        combinedName = combinedName.replaceAll("/", "_");
                        $scope.printTitle = combinedName.replaceAll(" ", "_");
                    });

                    locationService.getLocationForEdit({
                        Id: vm.purchaseOrder.locationRefId
                    }).success(function (result) {
                        vm.billinglocation = result.location;
                    });

                    locationService.getLocationForEdit({
                        Id: vm.purchaseOrder.shippingLocationId
                    }).success(function (result) {
                        vm.shippinglocation = result.location;
                    });

                    locationService.getLocationForEdit({
                        Id: vm.purchaseOrder.locationRefId
                    }).success(function (result) {
                        vm.locations = result.location;
                        if (vm.locations != null) {
                            companyService.getCompanyForEdit({
                                Id: vm.locations.companyRefId
                            }).success(function (result) {
                                vm.company = result.company;
                                vm.companyProfilePictureId = result.company.companyProfilePictureId;
                            });
                        }
                    });

                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.getData();

            $scope.printTitle = "Print Title";

        }]);
})();

