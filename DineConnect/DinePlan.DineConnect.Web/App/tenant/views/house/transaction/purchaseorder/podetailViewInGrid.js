﻿
(function () {
    appModule.controller('tenant.views.house.transaction.purchaseorder.podetailViewInGrid', [
        '$scope', '$uibModalInstance', 'uiGridConstants', 'abp.services.app.purchaseOrder', 'poId', 'appSession',
        function ($scope, $modalInstance, uiGridConstants, purchaseorderService, poId, appSession) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;

            vm.porefnumber = null;
            vm.podate = null;
            vm.manualCompleteAllowed = false;


            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('MaterialName'),
                        field: 'materialRefName',
                        width : 250
                    },
                    {
                        name: app.localize('Qty Ord'),
                        field: 'qtyOrdered',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Qty Rcd'),
                        field: 'qtyReceived',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Pending Qty'),
                        field: 'qtyPending',
                        cellClass : 'ui-ralign'
                    },
                   {
                       name: app.localize('UOM'),
                       field: 'unitRefName'
                   },
                    {
                        name: app.localize('Cost'),
                        field: 'price',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Total'),
                        field: 'totalAmount',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Tax'),
                        field: 'taxAmount',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('LineTotal'),
                        field: 'netAmount',
                        cellClass: 'ui-ralign'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.poreport = null;

            vm.printpo = function () {
                //vm.loading = true;
                //purchaseorderService.getPurchaseOrderReportInNotePad({
                //    skipCount: requestParams.skipCount,
                //    maxResultCount: requestParams.maxResultCount,
                //    sorting: requestParams.sorting,
                //    filter: vm.filterText,
                //    defaultLocationRefId: vm.defaultLocationId,
                //    purchaseOrderId: poId
                //}).success(function (result) {
                //    vm.poreport = result;
                //    //$('#regReport').text(result);
                //}).finally(function () {
                //    vm.loading = false;
                //});

            };


            vm.setPoAsCompletion = function (data) 
            {
                vm.loading = true;
                purchaseorderService.setCompletePurchaseOrder({
                    poId: poId,
                    poRemarks : 'Manual Completion'
                }).success(function (result) {
                    vm.cancel();
                }).finally(function () {
                    vm.loading = false;
                });
            };
            
            vm.caterQuoteDetails = [];
            vm.getAll = function() {
                vm.loading = true;
                purchaseorderService.getPurchaseOrderForEdit({
                    Id: poId
                }).success(function (result) {
                    vm.caterQuoteDetails = result.caterQuoteDetails;
                    vm.userGridOptions.totalItems = result.purchaseOrderDetail.length;
                    vm.userGridOptions.data = result.purchaseOrderDetail;
                    if (result.purchaseOrderDetail.length > 0) {
                        vm.porefnumber = result.purchaseOrder.poReferenceCode;
                        vm.podate = result.purchaseOrder.poDate;
                        vm.locationRefName = result.purchaseOrder.locationRefName;
                        vm.shippingLocationRefName = result.purchaseOrder.shippingLocationRefName;
                        vm.supplierRefName = result.purchaseOrder.supplierRefName;
                        vm.poNetAmount = result.purchaseOrder.poNetAmount;
                        if (result.purchaseOrder.poCurrentStatus == 'H') {
                            vm.manualCompleteAllowed = true;
                        }

                    }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.hide = function() {
                vm.poreport = "";
            };

            vm.getAll();
        }]);
})();