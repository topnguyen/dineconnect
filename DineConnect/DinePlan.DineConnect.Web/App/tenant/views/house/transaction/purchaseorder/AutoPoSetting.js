﻿(function () {
    appModule.controller('tenant.views.house.transaction.purchaseorder.autoPoSetting', [
        '$scope', '$state', '$uibModalInstance', 'appSession', 'autoMode',
        function ($scope, $state, $modalInstance, appSession, autoMode) {

            /* eslint-disable */
            var vm = this;
            vm.autoMode = autoMode;

            vm.saving = false;

            vm.autoPoSetting = {
                rolBased: true,
                avgConsumptionBased: false,
                requestBased : false
            };

            $scope.existall = true;

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.save = function () {
                openCreateOrEditModal(null, true);
            };

            function init() {

            }

            init();

            function openCreateOrEditModal(objId, autoFlag) {
                if (vm.autoPoSetting.rolBased) {
                    $state.go('tenant.purchaseorderdetail', {
                        id: objId,
                        autoPoFlag: autoFlag
                    });
                }
                else if (vm.autoPoSetting.requestBased) {
                    //$state.go('tenant.intertransferrequestdetail', {
                    //    id: objId,
                    //    autoRequestFlag: autoFlag,
                    //    directTransfer: false
                    //});
                    //vm.requestToAutoPO = function () {
                        $state.go("tenant.requestintoAutoPo", {

                        });
                    //}

                }

                $modalInstance.close();
            }

        }
    ]);
})();