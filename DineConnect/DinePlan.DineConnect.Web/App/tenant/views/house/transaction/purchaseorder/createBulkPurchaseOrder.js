﻿(function () {
    appModule.controller('tenant.views.house.transaction.purchaseorder.createBulkPurchaseOrder', [
        '$scope', '$state', '$window', "$uibModal", 'appSession', 'abp.services.app.supplierMaterial',
        'abp.services.app.supplier', '$rootScope', 'abp.services.app.purchaseOrder',
        function ($scope, $state, $window, $modal, appSession, suppliermaterialService, supplierService, $rootScope, purchaseorderService) {
            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            $rootScope.settings.layout.pageSidebarClosed = true;
            vm.loading = false;
            vm.loadingCount = 0;

            vm.currentUserId = abp.session.userId;
            vm.selectedLocationName = "";
            vm.uilimit = null;
            vm.locationList = [];

            vm.setShippingName = function (loc) {
                vm.selectedLocationName = loc.code;
                return parseInt(loc.id);
            }

            vm.permissions = {
                approve: abp.auth.hasPermission('Pages.Tenant.M3Manage.Inventory.Transaction.PurchaseOrder.Approve')
            };

            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            $scope.dt = moment().format($scope.format);
            vm.deliveryDateExpected = moment().format($scope.format);

            $('input[name="dueDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: vm.deliveryDateExpected,
                minDate: $scope.minDate,
            });

            $scope.existall = true;

            vm.sno = 0;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.defaultOrganisationId = appSession.user.organisationRefId;

            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.resetField = function () {
                vm.supplierRefId = null;
                vm.locationList = [];
                vm.bulkPodata = [];
            }

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.refsupplier = [];

            function fillDropDownSupplier() {
                vm.loading = true;
                vm.loadingCount++;
                supplierService.getSupplierForCombobox({}).success(function (result) {
                    vm.refsupplier = result.items;

                }).finally(function () {
                    vm.loading = false;
                    vm.loadingCount--;
                });
            }
            vm.rows = null;
            vm.columns = [];

            vm.refmaterial = [];
            vm.fillDropDownMaterial = function () {
                vm.loading = true;
                vm.loadingCount++;
                suppliermaterialService.getMaterialBasedOnSupplierWithPrice({
                    locationRefId: vm.defaultLocationId,
                    supplierRefId: vm.supplierRefId,
                    LoadOnlyLinkedMaterials:true
                }).success(function (result) {
                    vm.refmaterial = result.items;
                    vm.rows = vm.refmaterial.length;
                    if (vm.rows == 0) {
                        abp.notify.error(app.localize('NoMoreRecordsFound'));
                        abp.message.error(app.localize('NoMoreRecordsFound'));
                    }
                }).finally(function () {
                    vm.loading = false;
                    vm.loadingCount--;
                    vm.generateData();
                });
            }

            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationList = result.locations;
                    vm.refShippingLocation = $.parseJSON(JSON.stringify(result.locations));

                }).finally(function () {
                    vm.loading = true;
                    vm.generateData();
                });
            };

            fillDropDownSupplier();

            $scope.showSelectionBox = true;

            vm.addintoTotalQty = function (data) {
                var totalQty = 0.0;
                angular.forEach(data.locationWiseRequestQty, function (value, key) {
                    var qtyConv = parseFloat(value);
                    if (qtyConv > 0) {
                        totalQty = totalQty + qtyConv;
                    }
                });
                data.totalRequestQty = totalQty.toFixed(2);
            }

            vm.generateData = function () {
                vm.loading = true;
                vm.loadingCount++;
                vm.columnslength = vm.locationList.length;
                var i, c;
                var newData = [];
                var newColumns = [];

                for (c = 0; c < vm.columnslength; ++c)
                    newColumns.push({
                        title: vm.locationList[c].code,
                        field: 'c' + c
                    });


                for (i = 0; i < vm.rows; ++i) {
                    var bulkMat = {
                        materialRefId: vm.refmaterial[i].materialRefId,
                        materialPetName: vm.refmaterial[i].materialPetName,
                        materialName: vm.refmaterial[i].materialRefName,
                        uom: vm.refmaterial[i].uom,
                        material: vm.refmaterial[i],
                        totalRequestQty: 0.0,
                        locationWiseRequestQty: []
                    };

                    for (c = 0; c < vm.columnslength; ++c)
                        bulkMat.locationWiseRequestQty.push('');

                    newData.push(bulkMat);
                }


                vm.bulkPodata = newData;
                //$scope.data = newData;
                vm.columns = newColumns;
                vm.loading = false;
                vm.loadingCount--;
            };

            vm.defaulttemplatetype = 0;
            vm.template = null;
            vm.selectedTemplate = null;

            vm.templatesave = null;

            vm.addedPoList = [];

            vm.generateBulkPo = function () {
                vm.saving = true;
                if (vm.errorflag == true) {
                    vm.saving = false;
                    return;
                }

                if (vm.templatesave != null) {
                    if (vm.templatesave.templatename != null)
                        vm.templatesave.templatename = vm.templatesave.templatename.toUpperCase();
                }

                bulkDataToBeUploaded = [];
                angular.forEach(vm.bulkPodata, function (data, matkey) {
                    if (data.totalRequestQty > 0) {
                        angular.forEach(data.locationWiseRequestQty, function (value, key) {
                            var qtyConv = parseFloat(value);
                            if (qtyConv > 0) {
                                value = qtyConv;
                            }
                            else {
                                value = 0;
                            }
                        });
                        bulkDataToBeUploaded.push(data);
                    }
                });

                vm.loadingCount++;
                vm.addedPoList = [];
                purchaseorderService.createBulkPurchaseOrder({
                    supplierRefId: vm.supplierRefId,
                    locations: vm.locationList,
                    BulkPoMaterialListDtos: bulkDataToBeUploaded,
                    templateSave: vm.templatesave,
                    deliveryDateExpected : vm.deliveryDateExpected
                }).success(function (result) {
                    if (result.successFlag) {
                        abp.notify.info(app.localize('Bulk') + ' ' + app.localize('PurchaseOrder') + ' ' + app.localize('SavedSuccessfully'));
                        vm.addedPoList = result.purchaseOrderList;
                    } else {
                        abp.notify.info(app.localize(result.errorMessage));
                    }
                }).finally(function () {
                    vm.saving = false;
                    vm.loadingCount--;
                });
            }

            vm.printPo = function (myObject) {
                var url = $state.href('tenant.purchaseorderprint', {
                    printid: myObject.id
                });
                $window.open(url, '_blank');
            }

            vm.viewPoDetail = function (myObject) {
                openView(myObject.id);
            };

            function openView(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/purchaseorder/podetailViewInGrid.cshtml',
                    controller: 'tenant.views.house.transaction.purchaseorder.podetailViewInGrid as vm',
                    backdrop: 'static',
                    resolve: {
                        poId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {

                }).finally(function (result) {

                });
            }

        }
    ]);
})();

