﻿(function () {
    appModule.controller('tenant.views.house.transaction.purchaseorder.previousPriceModal', [
        '$scope', '$uibModalInstance', 'materialRefId', 'materialRefName', 'uom', 'abp.services.app.invoice',
        function ($scope, $modalInstance, materialRefId, materialRefName, uom, invoiceService) {
            var vm = this;

            vm.loading = false;

            vm.previousPriceList = null;
            vm.materialRefId = materialRefId;
            vm.materialRefName = materialRefName;
            vm.uom = uom;
            vm.norecordsfound = false;

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            function init() {
                vm.loading = true;

                invoiceService.getMaterialPurchase({
                    effectiveDate: moment(),
                    materialRefId: vm.materialRefId,
                    numberOfLastPurchaseDetail : 5
                }).success(function (result) {
                    vm.previousPriceList = result;
                    if (result.length == 0)
                        vm.norecordsfound = true;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            init();

        }
    ]);
})();