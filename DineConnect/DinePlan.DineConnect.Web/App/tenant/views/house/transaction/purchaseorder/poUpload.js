﻿
(function () {
    appModule.controller('tenant.views.house.transaction.purchaseorder.poUpload', [
        '$scope', '$uibModalInstance', 'uiGridConstants', 'abp.services.app.purchaseOrder', 'poId', 'appSession', 'FileUploader',
        function ($scope, $modalInstance, uiGridConstants, purchaseorderService, poId, appSession, fileUploader) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;

            vm.porefnumber = null;
            vm.podate = null;

            vm.purchaseorder = [];

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };


            vm.uploader = new fileUploader({
                url: abp.appPath + 'DocumentUpload/UploadPoDocumentInfo',
                formData: [{ id: 0 }],
                queueLimit: 1,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        //File type check
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        if ('|jpg|jpeg|pdf|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ProfilePicture_Warn_FileType'));
                            return false;
                        }
                        //File size check
                        if (item.size > 3072000) //3000KB
                        {
                            abp.message.warn(app.localize('ProfilePicture_Warn_SizeLimit'));
                            return false;
                        }
                        return true;
                    }
                }]
            });

            vm.uploader.onBeforeUploadItem = function (fileitem) {
                fileitem.formData.push({ locationRefId: vm.purchaseorder.locationRefId });
                fileitem.formData.push({ supplierRefId: vm.purchaseorder.supplierRefId })
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                vm.loading = true;
                vm.loading = false;
                vm.cancel();
            };

            vm.save = function () {

                purchaseorderService.getPurchaseOrderForEdit({
                    Id: poId
                }).success(function (result) {
                    vm.purchaseorder = result.purchaseOrder;
                    vm.purchaseorderid = vm.purchaseorder.id;
                    vm.uploader.uploadAll();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {

                if (vm.purchaseorder.id == null) {
                    vm.existall = false;
                    return;
                }

                purchaseorderService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.purchaseorder.id,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.purchaseorder.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.purchaseorder.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.getAll = function () {
                vm.loading = true;
                purchaseorderService.getPurchaseOrderForEdit({
                    Id: poId
                }).success(function (result) {
                    //vm.userGridOptions.totalItems = result.purchaseOrderDetail.length;
                    vm.purchaseorderdetail = result.purchaseOrderDetail;
                    if (result.purchaseOrderDetail.length > 0) {
                        vm.porefnumber = result.purchaseOrder.poReferenceCode;
                        vm.podate = result.purchaseOrder.poDate;
                        vm.locationRefName = result.purchaseOrder.locationRefName;
                        vm.shippingLocationRefName = result.purchaseOrder.shippingLocationRefName;
                        vm.supplierRefName = result.purchaseOrder.supplierRefName;
                        vm.poNetAmount = result.purchaseOrder.poNetAmount;
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.hide = function () {
                vm.poreport = "";
            };

            vm.getAll();
        }]);
})();