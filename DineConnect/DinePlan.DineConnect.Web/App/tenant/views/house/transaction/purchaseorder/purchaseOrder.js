﻿(function () {
    appModule.controller('tenant.views.house.transaction.purchaseorder.purchaseOrder', [
        '$scope', '$window', '$uibModal', '$filter', '$timeout', '$state', '$stateParams', 'abp.services.app.purchaseOrder',
        'appSession', 'abp.services.app.supplierMaterial', 'abp.services.app.template',
        'abp.services.app.location', 'abp.services.app.supplier', 'abp.services.app.material', 'abp.services.app.unitConversion', '$rootScope', 'abp.services.app.dayClose', 'abp.services.app.purchaseOrderExtended',
        function ($scope, $window, $modal, $filter, $timeout, $state, $stateParams, purchaseorderService, appSession, suppliermaterialService, templateService, locationService, supplierService, materialService, unitconversionService, $rootScope, daycloseService, purchaseOrderExtendedService) {
            var vm = this;
            /* eslint-disable */
            vm.reftax = [];
            vm.saving = false;
            vm.loadingflag = true;
            $rootScope.settings.layout.pageSidebarClosed = true;
            vm.loading = true;
            vm.autoPoloading = false;
            console.log("PO.js Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
            vm.poshowflag = true;
            vm.loadingCount = 0;
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");
            vm.supplierTaxRemarks = '';

            vm.purchaseorder = null;
            vm.purchaseOrderDetail = [];
            vm.currentUserId = abp.session.userId;
            vm.purchaseOrderTaxDetail = [];
            vm.purchaseDetailPortion = [];
            vm.selectedLocationName = "";
            vm.lastpriceshow = true;
            vm.uilimit = null;
            vm.refunit = [];
            vm.autoPoDetails = [];
            vm.loadOnlyLinkedMaterials = true;

            vm.defaulttemplatetype = 0;
            vm.template = null;
            vm.selectedTemplate = null;

            vm.templatesave = null;

            vm.setShippingName = function (loc) {
                vm.selectedLocationName = loc.code;
                return parseInt(loc.id);
            }

            vm.permissions = {
                approve: abp.auth.hasPermission('Pages.Tenant.House.Transaction.PurchaseOrder.Approve')
            };

            $scope.existall = true;
            vm.purchaseorderId = $stateParams.id;
            vm.autoPoFlag = $stateParams.autoPoFlag;
            vm.auotPoStatus = '';
            if (vm.autoPoFlag == "true" || vm.autoPoFlag == true) {
                vm.autoPoFlag = true;
                vm.loadOnlyLinkedMaterials = false;
            }
            else {
                vm.autoPoFlag = false;
                vm.loadOnlyLinkedMaterials = true;
            }

            vm.cancel = function (argOption) {
                if (argOption == 1) {
                    abp.message.confirm(
                        app.localize('CancelEntry?'),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                $state.go('tenant.purchaseorder');
                                $rootScope.settings.layout.pageSidebarClosed = false;
                            }
                        }
                    );
                }
                else {
                    $state.go('tenant.purchaseorder');
                    $rootScope.settings.layout.pageSidebarClosed = false;
                }
            };

            vm.isPurchaseAllowed = appSession.location.isPurchaseAllowed;

            if (vm.isPurchaseAllowed == false) {
                abp.notify.warn(app.localize('PurchaseAllowedErr'));
                vm.cancel(0);
                return;
            }

            vm.sno = 0;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.defaultOrganisationId = appSession.user.organisationRefId;

            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;

            $scope.locitems = [];

            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.dateOptions = [];
            vm.dateOptions.push({
                'singleDatePicker': true,
                'showDropdowns': true,
                'startDate': moment(),
                'minDate': $scope.minDate,
                'maxDate': moment()
            });

            //date start

            $scope.openDt = function ($event, index) {
                $('input[name=delDate' + index + ']').daterangepicker({
                    locale: {
                        format: $scope.format
                    },
                    singleDatePicker: true,
                    showDropdowns: true,
                    startDate: moment(),
                    minDate: $scope.minDate,
                });
            };

            $('input[name="poDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });

            $('input[name="dueDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
            });

            vm.existAllElements = function () {
                $scope.existall = true;
                $scope.errmessage = "";

                if (vm.purchaseorder.supplierRefId == null || vm.purchaseorder.supplierRefId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('SupplierErr');
                    $('#selsupplierRefId').focus();
                }

                if (vm.purchaseorder.poDate == null) {
                    $scope.errmessage = $scope.errmessage + app.localize('PoDateErr');
                    $('#poDate').focus();
                }

                if (vm.purchaseorder.locationRefId == null || vm.purchaseorder.locationRefId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('LocationErr');
                    $('#selLocationRefid').focus();
                }
                if (vm.purchaseorder.shippingLocationId == null || vm.purchaseorder.shippingLocationId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('ShippingLocationErr');
                    $('#selShipLocationRefid').focus();
                }

                if ($scope.errmessage != "") {
                    $scope.existall = false;
                    abp.notify.warn($scope.errmessage, 'Required');
                    return false;
                }
                else
                    return true;

            };

            vm.addPortion = function () {
                var errorFlag = false;

                var value = vm.purchaseDetailPortion[vm.purchaseDetailPortion.length - 1];

                if (vm.sno > 0) {
                    var value = vm.purchaseDetailPortion[vm.purchaseDetailPortion.length - 1];
                    //value = value[0];


                    if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
                        errorFlag = true;
                        if (vm.purchaseorder.supplierRefId == 0 || vm.purchaseorder.supplierRefId == null) {
                            var aaa = 5;
                        }
                        else {
                            abp.notify.info(app.localize('MinimumOneDetail'));
                        }
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.materialRefName)) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MaterialRequired'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.qtyOrdered) || value.qtyOrdered == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidOrderQty'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.price) || value.price == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialPrice'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.totalAmount) || value.totalAmount == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialTotalAmt'));
                        return;
                    }

                    if (vm.existAllElements() == false)
                        return;


                    var lastelement = value;

                    if (lastelement.unitRefIdForMinOrder == lastelement.unitRefId) {
                        if (lastelement.qtyOrdered < lastelement.minimumOrderQuantity && lastelement.minimumOrderQuantity > 0) {
                            errorFlag = true;
                            abp.notify.warn(app.localize('SupplierMOQ', lastelement.qtyOrdered, lastelement.minimumOrderQuantity, lastelement.materialRefName, lastelement.unitRefNameForMinOrder));
                            return;
                        }
                        if (lastelement.qtyOrdered < lastelement.maximumOrderQuantity && lastelement.maximumOrderQuantity > 0) {
                            errorFlag = true;
                            abp.notify.warn(app.localize('SupplierMaxOrderQty', lastelement.qtyOrdered, lastelement.maximumOrderQuantity, lastelement.materialRefName, lastelement.unitRefNameForMinOrder));
                            return;
                        }
                    }
                    else {
                        var convFactor = 0;
                        var convExistFlag = false;
                        vm.refconversionunit.some(function (data, key) {
                            if (data.baseUnitId == lastelement.unitRefIdForMinOrder && data.refUnitId == lastelement.unitRefId) {
                                convFactor = data.conversion;
                                convExistFlag = true;
                                return true;
                            }
                        });


                        if (convExistFlag == false) {
                            abp.notify.info(app.localize('ConversionFactorNotExist', lastelement.defaultUnitName, lastelement.unitRefNameForMinOrder));
                            return;
                        }

                        var curMoqConversion = lastelement.minimumOrderQuantity * convFactor;
                        if (lastelement.qtyOrdered < curMoqConversion && curMoqConversion > 0) {
                            errorFlag = true;
                            abp.notify.warn(app.localize('SupplierMOQ', lastelement.qtyOrdered, curMoqConversion, lastelement.materialRefName, lastelement.unitRefName));
                            return;
                        }

                        var curMaxOrderQtyConversion = lastelement.maximumOrderQuantity * convFactor;
                        if (lastelement.qtyOrdered > curMaxOrderQtyConversion && curMaxOrderQtyConversion > 0) {
                            errorFlag = true;
                            abp.notify.warn(app.localize('SupplierMaxOrderQty', lastelement.qtyOrdered, curMaxOrderQtyConversion, lastelement.materialRefName, lastelement.unitRefName));
                            return;
                        }

                    }


                }
                vm.sno = vm.sno + 1;
                vm.temptaxinfolist = [];

                vm.temptaxinfolist.push({
                    'materialRefId': 0,
                    'taxRefId': 0,
                    'taxName': "",
                    'taxRate': 0,
                    'taxValue': 0,
                });

                vm.refunit = [];
                vm.purchaseDetailPortion.push({
                    'sno': vm.sno,
                    'materialRefId': 0,
                    'materialRefName': '',
                    'qtyOrdered': '',
                    'defaultUnitId': 0,
                    'uom': '',
                    'unitRefId': '',
                    'unitRefName': '',
                    'price': '',
                    'initialprice': '',
                    'discountOption': '',
                    'discountValue': 0,
                    'taxAmount': 0,
                    'totalAmount': 0,
                    'netAmount': 0,
                    'remarks': '',
                    'currentInHand': 0,
                    'alreadyOrderedQuantity': 0,
                    'taxlist': vm.temptaxinfolist,
                    'applicableTaxes': [],
                    'changeunitflag': false,
                    'unitRefIdForMinOrder': '',
                    'unitRefNameForMinOrder': ''
                });

                vm.calculateTotalVlaue(vm.purchaseDetailPortion);
            }

            vm.calculateTotalVlaue = function (data) {
                var tempTotDiscAmt = 0;
                var tempTotPoNetAmt = 0;
                var tempTaxAmt = 0;
                var tempTotalAmt = 0;

                if (data.length == 0 || vm.isUndefinedOrNull(data))
                    return;

                angular.forEach(data, function (value, key) {
                    data[key].changeunitflag = false;
                    tempTotalAmt = parseFloat(tempTotalAmt) + parseFloat(vm.isUndefinedOrNull(value.totalAmount) ? 0 : parseFloat(value.totalAmount.toFixed(2)));
                    tempTaxAmt = parseFloat(tempTaxAmt) + parseFloat(vm.isUndefinedOrNull(value.taxAmount) ? 0 : parseFloat(value.taxAmount.toFixed(2)));
                    tempTotDiscAmt = parseFloat(tempTotDiscAmt) + parseFloat(vm.isUndefinedOrNull(value.discountValue) ? 0 : parseFloat(value.discountValue.toFixed(2)));
                    tempTotPoNetAmt = parseFloat(tempTotPoNetAmt) + parseFloat(vm.isUndefinedOrNull(value.netAmount) ? 0 : parseFloat(value.netAmount.toFixed(2)));

                });

                vm.poTotalAmount = parseFloat(tempTotalAmt.toFixed(2));
                vm.poTaxAmount = parseFloat(tempTaxAmt.toFixed(2));
                vm.purchaseorder.poNetAmount = parseFloat(tempTotPoNetAmt.toFixed(2));
                vm.supplierTaxRemarks = '';
                if (vm.poTotalAmount > 0) {
                    if (vm.poTaxAmount > 0 && vm.purchaseorder.supplierTaxApplicableByDefault == false) {
                        vm.supplierTaxRemarks = app.localize('SupplierTaxAddedExtraAlert');
                    }
                    else if (vm.poTaxAmount == 0 && vm.purchaseorder.supplierTaxApplicableByDefault == true) {
                        vm.supplierTaxRemarks = app.localize('SupplierTaxMissingAlert');
                    }
                    else {
                        vm.supplierTaxRemarks = '';
                    }
                }
                else {
                    vm.supplierTaxRemarks = '';
                }
            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex) {
                if (vm.purchaseDetailPortion.length > 1) {
                    vm.purchaseDetailPortion.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                }
                else {
                    vm.purchaseDetailPortion.splice(productIndex, 1);
                    vm.sno = 0;
                    vm.addPortion();
                    return;
                }
            }

            $scope.func = function (data, val, obj) {
                var forLoopFlag = true;

                angular.forEach(vm.purchaseDetailPortion, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialRefName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.materialRefId = 0;
                    data.materialRefName = '';
                    return;
                }

                data.materialRefName = val.materialRefName;
                data.alreadyOrderedQuantity = val.alreadyOrderedQuantity;
                data.currentInHand = val.currentInHand;
                data.uom = val.uom;
                data.defaultUnitId = val.defaultUnitId;
                data.unitRefName = val.unitRefName;
                data.unitRefId = val.unitRefId;
                data.minimumOrderQuantity = val.minimumOrderQuantity;
                data.unitRefIdForMinOrder = val.unitRefIdForMinOrder;
                data.unitRefNameForMinOrder = val.unitRefNameForMinOrder;
                data.price = parseFloat(val.materialPrice);
                data.initialprice = val.materialPrice;

                if (data.price == 0)
                    data.price = null;

                vm.temptaxinfolist = [];

                angular.forEach(val.taxForMaterial, function (value, key) {
                    vm.temptaxinfolist.push({
                        'materialRefId': value.materialRefId,
                        'taxRefId': value.taxRefId,
                        'taxName': value.taxName,
                        'taxRate': value.taxRate,
                        'taxValue': value.taxValue,
                        'rounding': value.rounding,
                        'sortOrder': value.sortOrder,
                        'taxCalculationMethod': value.taxCalculationMethod
                    });
                });

                data.taxlist = vm.temptaxinfolist;
                if (vm.purchaseorder.supplierTaxApplicableByDefault) {
                    data.applicableTaxes = val.applicableTaxes;
                }
            };

            $scope.taxCalc = function (data, totalQty, price) {
                if (vm.isUndefinedOrNull(totalQty))
                    totalQty = 0;
                if (vm.isUndefinedOrNull(price))
                    price = 0;

                data.totalAmount = parseFloat(price) * parseFloat(totalQty);
                data.totalAmount = parseFloat(data.totalAmount.toFixed(2));

                data.taxAmount = 0;
                data.taxlist = [];

                taxneedstoremove = [];

                if (data.applicableTaxes.length > 0)    //  Calculate Tax 
                {
                    angular.forEach(data.applicableTaxes, function (value, key) {
                        var taxCalculationOnValue = 0.0;

                        if (value.taxCalculationMethod == 'GT' || value.taxCalculationMethod == 'ST') {
                            taxCalculationOnValue = data.totalAmount;
                        }
                        else {
                            var taxrefvalueflag = false;
                            data.taxlist.some(function (taxdata, taxkey) {
                                if (taxdata.taxName == value.taxCalculationMethod) {
                                    taxCalculationOnValue = taxdata.taxValue;
                                    taxrefvalueflag = true;
                                    return true;
                                }
                            });

                            if (taxrefvalueflag == false)
                                taxneedstoremove.push({ 'taxName': value.taxName });
                        }

                        if (taxCalculationOnValue > 0) {
                            var taxamt = parseFloat(taxCalculationOnValue * value.taxRate / 100);
                            taxamt = parseFloat(taxamt.toFixed(2));
                            data.taxAmount = parseFloat(data.taxAmount) + parseFloat(taxamt);
                            data.taxAmount = parseFloat(data.taxAmount.toFixed(2));

                            if (taxamt > 0) {
                                data.taxlist.push
                                    ({
                                        'materialRefId': data.materialRefId,
                                        'taxRefId': value.taxRefId,
                                        'taxName': value.taxName,
                                        'taxRate': value.taxRate,
                                        'taxValue': taxamt,
                                        'rounding': value.rounding,
                                        'sortOrder': value.sortOrder,
                                        'taxCalculationMethod': value.taxCalculationMethod
                                    });
                            }
                        }
                    });
                }


                angular.forEach(taxneedstoremove, function (value, key) {
                    angular.forEach(data.applicableTaxes, function (detvalue, detkey) {
                        if (value.taxName == detvalue.taxName)
                            data.applicableTaxes.splice(detkey, 1);
                    });
                });

                data.netAmount = parseFloat(parseFloat(data.totalAmount).toFixed(2)) + parseFloat(parseFloat(data.taxAmount).toFixed(2));
                vm.calculateTotalVlaue(vm.purchaseDetailPortion);
            }


            $scope.amtCalc = function (data, totalQty, price) {
                if (vm.isUndefinedOrNull(totalQty))
                    totalQty = 0;
                if (vm.isUndefinedOrNull(price))
                    price = 0;

                data.totalAmount = parseFloat(price) * parseFloat(totalQty);
                data.totalAmount = parseFloat(data.totalAmount.toFixed(2));

                data.taxAmount = 0;

                if (data.taxlist.length > 0)    //  Calculate Tax 
                {
                    angular.forEach(data.taxlist, function (value, key) {
                        var taxCalculationOnValue = 0.0;

                        if (value.taxCalculationMethod == 'GT' || value.taxCalculationMethod == 'ST') {
                            taxCalculationOnValue = data.totalAmount;
                        }
                        else {
                            data.taxlist.some(function (taxdata, taxkey) {
                                if (taxdata.taxName == value.taxCalculationMethod) {
                                    taxCalculationOnValue = taxdata.taxValue;
                                    return true;
                                }
                            });
                        }
                        var taxamt = parseFloat(taxCalculationOnValue * value.taxRate / 100);
                        taxamt = parseFloat(taxamt.toFixed(2));
                        data.taxAmount = parseFloat(data.taxAmount) + parseFloat(taxamt);
                        data.taxAmount = parseFloat(data.taxAmount.toFixed(2));
                        data.taxlist[key].taxValue = taxamt;
                        data.taxlist[key].materialRefId = data.materialRefId;
                    });
                }

                data.netAmount = parseFloat(parseFloat(data.totalAmount).toFixed(2)) + parseFloat(parseFloat(data.taxAmount).toFixed(2));
                vm.calculateTotalVlaue(vm.purchaseDetailPortion);
            }

            $scope.discOptionCheck = function (data) {
                data.discountValue = 0;
                $scope.calcDiscountPerc(data);
            }

            $scope.calcDiscountPerc = function (data) {
                var discamt = data.discountValue;
                if (parseFloat(discamt) == 0) {
                    $scope.amtCalc(data, data.totalQty, data.price);
                    return;
                }

                var totalQty = 0;
                var price = 0;
                if (vm.isUndefinedOrNull(data.qtyOrdered))
                    totalQty = 0;
                else
                    totalQty = parseFloat(data.qtyOrdered);
                if (vm.isUndefinedOrNull(data.price))
                    price = 0;
                else
                    price = parseFloat(data.price);

                if (totalQty == 0 || price == 0) {
                    abp.message.warn(app.localize("QtyOrderedOrPriceShouldNotBeZero"));
                    return;
                }

                var totAmt = price * totalQty;
                if (data.discountOption) {
                    if (vm.isUndefinedOrNull(discamt))
                        discamt = 0;
                    discamt = parseFloat($filter('number')(discamt));

                    if (discamt > 100) {
                        abp.message.warn(app.localize("DiscountPercentageWrong"));
                        return;
                    }

                    var disccal = totAmt - (totAmt * (discamt / 100));
                    data.netAmount = parseFloat(data.totalAmount) - parseFloat((disccal));
                }
                else {
                    if (vm.isUndefinedOrNull(discamt))
                        discamt = 0;

                    discamt = parseFloat($filter('number')(discamt));

                    var disccal = totAmt - discamt;
                    data.netAmount = parseFloat(data.totalAmount) - parseFloat((disccal));

                }

                vm.calculateTotalVlaue(vm.purchaseDetailPortion);
            }

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };


            vm.save = function (argSaveOption) {

                if (vm.existAllElements() == false)
                    return;

                if ($scope.existall == false)
                    return;

                if (vm.purchaseorder.poNetAmount == 0) {
                    abp.message.warn(app.localize("EmptyDetail"));
                    return;
                }

                if (vm.templatesave.templateflag == true && vm.templatesave.templatename.length == 0) {
                    abp.message.warn(app.localize("TemplateNameRequired"));
                    return;
                }

                vm.saving = true;

                vm.purchaseOrderDetail = [];
                vm.discFlag = 0;

                vm.errorflag = false;
                angular.forEach(vm.purchaseDetailPortion, function (value, key) {
                    if (value.discountOption == true || value.discountOption == 1)
                        vm.discFlag = "P";
                    else
                        vm.discFlag = "F";

                    if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
                        vm.errorflag = true;
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.materialRefName)) {
                        vm.errorflag = true;
                        abp.notify.info(app.localize('MaterialRequired'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.qtyOrdered) || value.qtyOrdered == 0) {
                        vm.errorflag = true;
                        abp.notify.info(app.localize('InvalidOrderQty'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.price) || value.price == 0) {
                        vm.errorflag = true;
                        abp.notify.info(app.localize('InvalidMaterialPrice'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.totalAmount) || value.totalAmount == 0) {
                        vm.errorflag = true;
                        abp.notify.info(app.localize('InvalidMaterialTotalAmt'));
                        return;
                    }

                    var lastelement = value;

                    if (lastelement.unitRefIdForMinOrder == lastelement.unitRefId) {
                        if (lastelement.qtyOrdered < lastelement.minimumOrderQuantity && lastelement.minimumOrderQuantity > 0) {
                            vm.errorflag = true;
                            abp.notify.warn(app.localize('SupplierMOQ', lastelement.qtyOrdered, lastelement.minimumOrderQuantity, lastelement.materialRefName, lastelement.unitRefNameForMinOrder));
                            return;
                        }
                        if (lastelement.qtyOrdered < lastelement.maximumOrderQuantity && lastelement.maximumOrderQuantity > 0) {
                            errorFlag = true;
                            abp.notify.warn(app.localize('SupplierMaxOrderQty', lastelement.qtyOrdered, lastelement.maximumOrderQuantity, lastelement.materialRefName, lastelement.unitRefNameForMinOrder));
                            return;
                        }
                    }
                    else {
                        var convFactor = 0;
                        var convExistFlag = false;
                        vm.refconversionunit.some(function (data, key) {
                            if (data.baseUnitId == lastelement.unitRefIdForMinOrder && data.refUnitId == lastelement.unitRefId) {
                                convFactor = data.conversion;
                                convExistFlag = true;
                                return true;
                            }
                        });

                        if (convExistFlag == false) {
                            abp.notify.info(app.localize('ConversionFactorNotExist', lastelement.unitRefNameForMinOrder, lastelement.unitRefName));
                            vm.errorflag = true;
                            return;
                        }

                        var curMoqConversion = lastelement.minimumOrderQuantity * convFactor;
                        if (lastelement.qtyOrdered < curMoqConversion && curMoqConversion > 0) {
                            vm.errorflag = true;
                            abp.notify.warn(app.localize('SupplierMOQ', lastelement.qtyOrdered, curMoqConversion, lastelement.materialRefName, lastelement.unitRefName));
                            return;
                        }

                        var curMaxOrderQtyConversion = lastelement.maximumOrderQuantity * convFactor;
                        if (lastelement.qtyOrdered > curMaxOrderQtyConversion && curMaxOrderQtyConversion > 0) {
                            errorFlag = true;
                            abp.notify.warn(app.localize('SupplierMaxOrderQty', lastelement.qtyOrdered, curMaxOrderQtyConversion, lastelement.materialRefName, lastelement.unitRefName));
                            return;
                        }
                    }

                    vm.purchaseOrderDetail.push({
                        'sno': value.sno,
                        'materialRefId': value.materialRefId,
                        'materialRefName': value.materialRefName,
                        'qtyOrdered': value.qtyOrdered,
                        'defaultUnitId': value.defaultUnitId,
                        'uom': value.uom,
                        'unitRefId': value.unitRefId,
                        'unitRefName': value.unitRefName,
                        'price': value.price,
                        'discountOption': vm.discFlag,
                        'discountValue': value.discountValue,
                        'totalAmount': value.totalAmount,
                        'taxAmount': value.taxAmount,
                        'netAmount': value.netAmount,
                        'remarks': value.remarks,
                        'taxForMaterial': value.taxlist,
                        'interTransferDetailWithPORefIds': value.interTransferDetailWithPORefIds,
                        'caterHeaderRefIds': value.caterHeaderRefIds,
                    });
                });

                if (vm.errorflag == true) {
                    vm.saving = false;
                    return;
                }

                if (vm.templatesave.templatename != null)
                    vm.templatesave.templatename = vm.templatesave.templatename.toUpperCase();

                vm.loadingCount++;

                purchaseorderService.createOrUpdatePurchaseOrder({
                    purchaseOrder: vm.purchaseorder,
                    purchaseOrderDetail: vm.purchaseOrderDetail,
                    templateSave: vm.templatesave,
                    statusToBeAssigned: argSaveOption
                }).success(function (result) {
                    abp.notify.info(app.localize('PurchaseOrder') + ' ' + app.localize('SavedSuccessfully'));
                    abp.message.success(app.localize('POSuccessMessage', result.poReferenceCode));

                    if ($stateParams.autoPoStatus == 'TRQ') {
                        var pendingDraftExists = false;
                        angular.forEach(vm.autoPoDetails, function (value, key) {
                            if (value.supplierRefId == vm.purchaseorder.supplierRefId) {
                                value.alreadyConverted = true;
                                value.poReferenceCode = result.poReferenceCode;
                                value.purchaseOrderId = result.id;
                            }
                            if (value.alreadyConverted == false)
                                pendingDraftExists = true;
                        });
                        resetField();
                        if (pendingDraftExists == true) {
                            abp.notify.info('Please Generate Next PO');
                            vm.showAllAutoPos(vm.autoPoDetails);
                        }
                        else {
                            vm.cancel(0);
                        }
                    }
                    else if ($stateParams.autoPoStatus == 'CATER') {
                        var pendingDraftExists = false;
                        angular.forEach(vm.autoPoDetails, function (value, key) {
                            if (value.supplierRefId == vm.purchaseorder.supplierRefId) {
                                value.alreadyConverted = true;
                                value.poReferenceCode = result.poReferenceCode;
                                value.purchaseOrderId = result.id;
                            }
                            if (value.alreadyConverted == false)
                                pendingDraftExists = true;
                        });
                        resetField();
                        if (pendingDraftExists == true) {
                            abp.notify.info('Please Generate Next PO');
                            vm.showAllAutoPos(vm.autoPoDetails);
                        }
                        else {
                            vm.cancel(0);
                        }
                    }
                    else {
                        resetField();
                        if (argSaveOption == 'A') {
                            vm.printPurchaseOrder(result.id);
                            vm.cancel(0);
                        }
                        else {
                            vm.cancel(0);
                        }
                    }

                }).finally(function () {
                    vm.saving = false;
                    vm.loadingCount--;
                });
            };

            vm.printPurchaseOrder = function (poid) {
                //$rootScope.settings.layout.pageSidebarClosed = false;
                //$state.go('tenant.purchaseorderprint', {
                //    printid: poid
                //});

                var url = $state.href('tenant.purchaseorderprint', {
                    printid: poid
                });
                $window.open(url, '_blank');
            };

            vm.draft = function () {
                if ($scope.existall == false)
                    return;
                vm.saving = true;

                vm.templatesave.templateflag = true;
                vm.templatesave.templatename = app.localize("LastPoDraft");

                vm.purchaseOrderDetail = [];
                vm.discFlag = 0;

                angular.forEach(vm.purchaseDetailPortion, function (value, key) {
                    if (value.discountOption == true || value.discountOption == 1)
                        vm.discFlag = "P";
                    else
                        vm.discFlag = "F";

                    vm.purchaseOrderDetail.push({
                        'sno': value.sno,
                        'materialRefId': value.materialRefId,
                        'qtyOrdered': value.qtyOrdered,
                        'defaultUnitId': value.defaultUnitId,
                        'uom': value.uom,
                        'unitRefId': value.unitRefId,
                        'unitRefName': value.unitRefName,
                        'price': value.price,
                        'discountOption': vm.discFlag,
                        'discountValue': value.discountValue,
                        'totalAmount': value.totalAmount,
                        'taxAmount': value.taxAmount,
                        'netAmount': value.netAmount,
                        'remarks': value.remarks,
                    });
                });

                if (vm.templatesave.templatename != null)
                    vm.templatesave.templatename = vm.templatesave.templatename.toUpperCase();

                vm.purchaseorder.poReferenceCode = vm.selectedLocationName + '/1';

                purchaseorderService.createDraft({
                    purchaseOrder: vm.purchaseorder,
                    purchaseOrderDetail: vm.purchaseOrderDetail,
                    templateSave: vm.templatesave
                }).success(function () {
                    abp.notify.info('\' PurchaseOrder \'' + app.localize('DraftedSuccessfully'));
                    resetField();
                    vm.cancel(0);
                }).finally(function () {
                    vm.saving = false;
                });
            };


            function resetField() {
                vm.sno = 0;
                vm.purchaseorder = [];
                vm.purchaseOrderDetail = [];
                vm.purchaseOrderTaxDetail = [];
                vm.purchaseDetailPortion = [];
                vm.purchaseorder.locationRefId = vm.defaultLocationId;
                init();
                $('input[name="poDate"]').daterangepicker({
                    locale: {
                        format: $scope.format
                    },
                    singleDatePicker: true,
                    showDropdowns: true,
                    startDate: moment(),
                    minDate: $scope.minDate,
                    maxDate: moment()
                });

                $('input[name="dueDate"]').daterangepicker({
                    locale: {
                        format: $scope.format
                    },
                    singleDatePicker: true,
                    showDropdowns: true,
                    startDate: moment(),
                    minDate: $scope.minDate,
                });

            }


            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };


            vm.getSupplierValue = function (data) {
                vm.purchaseorder.supplierRefId = data.id;
                vm.purchaseorder.supplierTaxApplicableByDefault = data.taxApplicable;
                //return parseInt(item.value);
            };

            vm.reflocations = [];

            function fillDropDownLocation() {
                vm.loading = true;
                console.log("PO.js fillDropDownLocation() Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                locationService.getLocationBasedOnUser({}).success(function (result) {
                    vm.reflocation = result.items;
                }).finally(function () {
                    //vm.loading = false;
                    console.log("PO.js fillDropDownLocation() End Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                    init();
                });

            }

            vm.refsupplier = [];

            function fillDropDownSupplier() {
                vm.loading = true;
                console.log("PO.js getSimpleSupplierListDtos() Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                supplierService.getSimpleSupplierListDtos({}).success(function (result) {
                    vm.refsupplier = result.items;
                    //vm.loading = false;
                }).finally(function () {
                    console.log("PO.js getSimpleSupplierListDtos() End Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));

                    fillDropDownLocation();
                });
            }


            vm.refconversionunit = [];
            vm.fillUnitConversion = function () {
                unitconversionService.getAll({
                }).success(function (result) {
                    vm.refconversionunit = result.items;
                });
            }

            vm.fillUnitConversion();

            vm.refmaterial = [];
            vm.fillDropDownMaterial = function () {
                vm.loading = true;
                suppliermaterialService.getMaterialBasedOnSupplierWithPrice({
                    locationRefId: vm.defaultLocationId,
                    supplierRefId: vm.purchaseorder.supplierRefId,
                    poRefId: vm.purchaseorder.id,
                    loadOnlyLinkedMaterials: vm.loadOnlyLinkedMaterials
                }).success(function (result) {
                    vm.refmaterial = result.items;

                }).finally(function () {
                    vm.templist = [];

                    angular.forEach(vm.purchaseDetailPortion, function (value, key) {
                        vm.discFlag = null;
                        if (value.discountOption == 'F')
                            vm.discFlag = false;
                        else if (value.discountOption == 'P')
                            vm.discFlag = true;

                        vm.temponhand = 0;
                        vm.temponorder = 0;
                        vm.matname = "";

                        vm.refmaterial.some(function (matvalue, matkey) {
                            if (matvalue.materialRefId == value.materialRefId) {
                                vm.temponhand = matvalue.currentInHand;
                                vm.temponorder = matvalue.alreadyOrderedQuantity;
                                vm.matname = matvalue.materialRefName;
                                return true;
                            }
                        });

                        vm.templist.push({
                            'sno': value.sno,
                            'materialRefId': value.materialRefId,
                            'materialRefName': vm.matname,
                            'qtyOrdered': value.qtyOrdered,
                            'defaultUnitId': value.defaultUnitId,
                            'uom': value.uom,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'price': value.price,
                            'initialprice': value.price,
                            'discountOption': vm.discFlag,
                            'discountValue': value.discountValue,
                            'totalAmount': value.totalAmount,
                            'taxAmount': value.taxAmount,
                            'netAmount': value.netAmount,
                            'remarks': value.remarks,
                            'currentInHand': vm.temponhand,
                            'alreadyOrderedQuantity': vm.temponorder,
                            'taxlist': value.taxlist,
                            'applicableTaxes': value.applicableTaxes,
                            'unitRefIdForMinOrder': value.unitRefIdForMinOrder,
                            'unitRefNameForMinOrder': value.unitRefNameForMinOrder,
                            'minimumOrderQuantity': value.minimumOrderQuantity,
                            'interTransferDetailWithPORefIds': value.interTransferDetailWithPORefIds,
                            'caterHeaderRefIds': value.caterHeaderRefIds,
                        });
                    });

                    vm.purchaseDetailPortion = vm.templist;
                    vm.loadingflag == false;
                    vm.loading = false;
                });
            }

            vm.reftax = [];
            vm.fillDropDownTaxes = function () {
                suppliermaterialService.getTaxForMaterial()
                    .success(function (result) {
                        vm.reftax = result;
                    }).finally(function () {

                    });
            }


            vm.getMaterialQuotePrice = function (data, supmaterial) {
                if (data.materialRefId > 0) {
                    data.price = parseFloat(supmaterial.materialPrice);
                    data.initialprice = data.price;
                    data.uom = supmaterial.uom;
                    data.unitRefName = supmaterial.uom,
                        data.unitRefId = supmaterial.unitRefId
                }
                return parseInt(supmaterial.materialRefId);

                //if (vm.isUndefinedOrNull(supmaterial.materialPrice))
                //{
                //    return data.materialRefId;
                //}
                //if (data.materialRefId > 0)  {
                //    data.price = parseFloat(supmaterial.materialPrice);
                //    data.initialprice = data.price;
                //    data.uom = supmaterial.uom;
                //}
                //else {
                //    return data.materialRefId;
                //}
                //return parseInt(supmaterial.materialRefId);
            };

            vm.refShippingLocation = [];
            function fillDropDownShippingLocation() {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.refShippingLocation = $.parseJSON(JSON.stringify(result.items));
                    if (vm.refShippingLocation.length == 1) {
                        vm.purchaseorder.shippingLocationId = vm.refShippingLocation[0].id;
                        vm.selectedLocationName = vm.refShippingLocation[0].code;
                    }
                }).finally(function (result) {
                });
            }

            vm.refTemplateDetail = [];
            vm.reftemplate = [];

            function fillDropDownTemplate() {
                vm.loading = true;
                templateService.getTemplateDetail({
                    locationRefId: vm.defaultLocationId,
                    templateType: vm.defaulttemplatetype
                }).success(function (result) {
                    vm.refTemplateDetail = result.items;

                    angular.forEach(vm.refTemplateDetail, function (value, key) {
                        vm.reftemplate.push({
                            'value': value.id,
                            'displayText': value.name,
                        });
                    });
                    vm.loading = false;
                });
            }

            vm.fillDataFromTemplate = function () {
                vm.autoPoFlag = false;
                vm.autoPoDetails = [];
                if (vm.template == null) {
                    abp.notify.warn(app.localize('Template') + " ?")
                    return;
                }

                vm.refTemplateDetail.some(function (value, key) {
                    if (value.id == vm.template) {
                        vm.selectedTemplate = value.templateData;
                        return true;
                    }
                });

                vm.loading = true;

                purchaseorderService.getTemplateObjectForEdit({
                    objectString: vm.selectedTemplate
                }).success(function (result) {

                    vm.purchaseorder = result.purchaseOrder;
                    vm.purchaseOrderDetail = result.purchaseOrderDetail;

                    $scope.dt = moment().format($scope.format);
                    vm.purchaseorder.poDate = moment().format($scope.format);
                    vm.purchaseorder.deliveryDateExpected = moment().format($scope.format);
                    vm.purchaseorder.locationRefId = vm.defaultLocationId;

                    $('input[name="poDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.purchaseorder.poDate,
                        minDate: $scope.minDate,
                        maxDate: moment().format($scope.format)
                    });

                    $('input[name="dueDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.purchaseorder.deliveryDateExpected,
                        minDate: $scope.minDate,
                    });

                    vm.purchaseDetailPortion = [];

                    angular.forEach(vm.purchaseOrderDetail, function (value, key) {
                        vm.discFlag = null;
                        if (value.discountOption == 'F')
                            vm.discFlag = false;
                        else if (value.discountOption == 'P')
                            vm.discFlag = true;

                        vm.temponhand = 0;
                        vm.temponorder = 0;

                        vm.purchaseDetailPortion.push({
                            'sno': value.sno,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'qtyOrdered': value.qtyOrdered,
                            'defaultUnitId': value.defaultUnitId,
                            'uom': value.uom,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'price': value.price,
                            'initialprice': value.price,
                            'discountOption': vm.discFlag,
                            'discountValue': value.discountValue,
                            'totalAmount': value.totalAmount,
                            'taxAmount': value.taxAmount,
                            'netAmount': value.netAmount,
                            'remarks': value.remarks,
                            'currentInHand': vm.temponhand,
                            'alreadyOrderedQuantity': vm.temponorder,
                            'taxlist': value.taxForMaterial,
                            'applicableTaxes': value.taxForMaterial,
                            'changeunitflag': false,
                            'unitRefIdForMinOrder': value.unitRefIdForMinOrder,
                            'unitRefNameForMinOrder': value.unitRefNameForMinOrder,
                            'minimumOrderQuantity': value.minimumOrderQuantity,
                            'interTransferDetailWithPORefIds': value.interTransferDetailWithPORefIds,
                            'caterHeaderRefIds': value.caterHeaderRefIds,
                        });
                    });
                    vm.loading = false;
                    vm.fillDropDownMaterial();

                    vm.calculateTotalVlaue(vm.purchaseDetailPortion);
                });
            }

            vm.checkCloseDay = function () {
                vm.loading = true;
                vm.loadingflag = true;
                vm.loadingCount++;
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {

                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        //if (vm.messagenotificationflag)
                        //    abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                    vm.purchaseorder.poDate = moment(result.accountDate).format($scope.format);
                    vm.purchaseorder.deliveryDateExpected = moment(result.accountDate).format($scope.format);
                    $scope.minDate = result.accountDate;

                    $('input[name="poDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.purchaseorder.poDate,
                        minDate: $scope.minDate,
                        maxDate: vm.purchaseorder.poDate
                    });

                    $('input[name="dueDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.purchaseorder.deliveryDateExpected,
                        minDate: $scope.minDate,
                    });

                }).finally(function (result) {
                    vm.loading = false;
                    vm.loadingflag = false;
                    vm.loadingCount--;
                });
            }

            function init() {

                if (vm.autoPoFlag == true) {
                    vm.fillAutoPoDetails();
                    vm.poshowflag = false;
                }
                else {
                    vm.poshowflag = true;
                }
                vm.loading = true;

                vm.loadingCount++;
                console.log("PO.js getPurchaseOrderForEdit() Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                purchaseorderService.getPurchaseOrderForEdit({
                    Id: vm.purchaseorderId
                }).success(function (result) {
                    vm.purchaseorder = result.purchaseOrder;
                    vm.purchaseOrderDetail = result.purchaseOrderDetail;

                    vm.purchaseorder.locationRefId = vm.defaultLocationId;

                    vm.templatesave = result.templatesave;

                    vm.templatesave.templateflag = false;
                    vm.templatesave.templatename = null;
                    vm.templatesave.templatescope = false;

                    fillDropDownShippingLocation();

                    $scope.dt = moment().format($scope.format);
                    if (result.purchaseOrder.id != null) {
                        vm.uilimit = null;
                        vm.defaultLocationId = vm.purchaseorder.locationRefId;
                        vm.purchaseorder.poDate = moment(result.purchaseOrder.poDate).format($scope.format);
                        vm.purchaseorder.deliveryDateExpected = moment(result.purchaseOrder.deliveryDateExpected).format($scope.format);
                        $scope.minDate = vm.purchaseorder.poDate;

                        $('input[name="poDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: vm.purchaseorder.poDate,
                            minDate: $scope.minDate,
                            maxDate: moment().format($scope.format)
                        });

                        $('input[name="dueDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: vm.purchaseorder.deliveryDateExpected,
                            minDate: $scope.minDate,
                        });

                        vm.purchaseDetailPortion = [];

                        angular.forEach(vm.purchaseOrderDetail, function (value, key) {
                            vm.discFlag = null;
                            if (value.discountOption == 'F')
                                vm.discFlag = false;
                            else if (value.discountOption == 'P')
                                vm.discFlag = true;
                            vm.purchaseDetailPortion.push({
                                'sno': value.sno,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'qtyOrdered': value.qtyOrdered,
                                'defaultUnitId': value.defaultUnitId,
                                'uom': value.uom,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'price': value.price,
                                'initialprice': value.price,
                                'discountOption': vm.discFlag,
                                'discountValue': value.discountValue,
                                'totalAmount': value.totalAmount,
                                'taxAmount': value.taxAmount,
                                'netAmount': value.netAmount,
                                'remarks': value.remarks,
                                'currentInHand': value.currentInHand,
                                'alreadyOrderedQuantity': value.alreadyOrderedQuantity,
                                'taxlist': value.taxForMaterial,
                                'applicableTaxes': value.applicableTaxes,
                                'changeunitflag': false,
                                'unitRefIdForMinOrder': value.unitRefIdForMinOrder,
                                'unitRefNameForMinOrder': value.unitRefNameForMinOrder,
                                'minimumOrderQuantity': value.minimumOrderQuantity,
                                'interTransferDetailWithPORefIds': value.interTransferDetailWithPORefIds,
                                'caterHeaderRefIds': value.caterHeaderRefIds,
                            });
                        });
                        vm.fillDropDownMaterial();
                        vm.calculateTotalVlaue(vm.purchaseDetailPortion);
                    }
                    else {
                        vm.purchaseorder.poDate = moment().format($scope.format);
                        vm.purchaseorder.deliveryDateExpected = moment().format($scope.format);
                        vm.purchaseorder.shippingLocationId = vm.defaultLocationId;
                        vm.purchaseorder.locationRefId = vm.defaultLocationId;
                        vm.checkCloseDay();
                        vm.addPortion();
                        vm.uilimit = 50;
                    }

                    
                }).finally(function () {
                    vm.loadingCount--;
                    vm.loading = false;
                    console.log("PO.js getPurchaseOrderForEdit() End Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                });

            }




            vm.qtyKeyPress = function (objId, $event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    //alert(objId);
                    $("#price-" + objId).focus();
                }
            };

            vm.priceKeyPress = function (objId, $event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    //alert(objId);
                    $("#btnadd-" + objId).focus();
                }
            };

            vm.ratediffshow = function (data) {
                if (vm.lastpriceshow == false || data.price == data.initialprice || data.initialprice == 0 || data.initialprice == "")
                    return;
                vm.loading = true;
                vm.previousPriceOpen(data);

            }


            vm.previousPriceOpen = function (data) {
                if (data.materialRefId == 0 || data.materialRefId == null)
                    return;

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/purchaseorder/previousPriceModal.cshtml',
                    controller: 'tenant.views.house.transaction.purchaseorder.previousPriceModal as vm',
                    backdrop: 'static',
                    resolve: {
                        materialRefId: function () {
                            return data.materialRefId;
                        },
                        materialRefName: function () {
                            return data.materialRefName;
                        },
                        uom: function () {
                            return data.uom;
                        }
                    }
                });

                modalInstance.result.then(function (result) {

                });
                vm.loading = false;
            }

            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

            vm.unitReference = function (selected, data) {
                data.unitRefId = selected.value;
                data.unitRefName = selected.displayText;
                data.changeunitflag = false;

                if (data.unitRefIdForMinOrder == data.unitRefId) {
                    data.price = data.initialprice;
                    $scope.amtCalc(data, data.qtyOrdered, data.price);
                    vm.calculateTotalVlaue(vm.purchaseDetailPortion);
                }
                else {
                    vm.loadingCount++;
                    materialService.getConversion({
                        baseUnitId: data.unitRefIdForMinOrder,
                        refUnitId: data.unitRefId,
                        materialRefId: data.materialRefId,
                        supplierRefId: vm.purchaseorder.supplierRefId
                    })
                        .success(function (result) {
                            var conversion = result;

                            if (conversion.id == 0 || conversion.id == null) {
                                abp.notify.info(app.localize('ConversionFactorNotExist', data.unitRefNameForMinOrder, data.unitRefName));
                                data.price = 0;
                                $scope.amtCalc(data, data.qtyOrdered, data.price);
                                vm.calculateTotalVlaue(vm.purchaseDetailPortion);
                            }
                            else {
                                var convFactor = conversion.conversion;
                                var converstionUnitPrice = data.initialprice * 1 / convFactor;
                                data.price = parseFloat(converstionUnitPrice.toFixed(2));
                                $scope.amtCalc(data, data.qtyOrdered, data.price);
                                vm.calculateTotalVlaue(vm.purchaseDetailPortion);
                            }
                        }).finally(function () {
                            vm.loadingCount--;
                        });

                       //var convFactor = 0;
                            //var convExistFlag = false;

                            //vm.refconversionunit.some(function (refdata, refkey) {
                            //    if (refdata.baseUnitId == data.unitRefIdForMinOrder && refdata.refUnitId == data.unitRefId) {
                            //        convFactor = refdata.conversion;
                            //        convExistFlag = true;
                            //        return true;
                            //    }
                            //});

                            //if (convExistFlag == false) {
                            //    abp.notify.info(app.localize('ConversionFactorNotExist', data.unitRefNameForMinOrder, data.unitRefName));
                            //    data.price = 0;
                            //    $scope.amtCalc(data, data.qtyOrdered, data.price);
                            //    vm.calculateTotalVlaue(vm.purchaseDetailPortion);
                            //    return;
                            //}

                }


            }

            vm.autoPoDetails = [];
            vm.fillAutoPoDetails = function () {
                vm.autoPoloading = true;
                vm.autoPoDetails = [];
                vm.loading = true;
                if ($stateParams.autoPoStatus == 'TRQ') {
                    vm.existdata = $stateParams.autoTransferPoDetails;
                    if (vm.existdata !== "") {
                        vm.autoPoDetails = JSON.parse(vm.existdata);
                    }
                    if (vm.autoPoDetails.length == 0) {
                        vm.autoPoFlag = false;
                        vm.poshowflag = true;
                        abp.notify.warn(app.localize('AutoPoMaterialNotExist'));
                        abp.message.warn(app.localize('AutoPoMaterialNotExist'));
                    }
                    else {
                        angular.forEach(vm.autoPoDetails, function (value, key) {
                            value.visibleFlag = true;
                            value.alreadyConverted = false;
                            value.poReferenceCode = '';
                            value.purchaseOrderId = null;
                        });
                    }
                    vm.autoPoloading = false;
                    vm.loading = false;
                }
                else if ($stateParams.autoPoStatus == 'CATER') {
                    vm.existdata = $stateParams.autoCaterPoDetails;
                    if (vm.existdata !== "") {
                        vm.autoPoDetails = JSON.parse(vm.existdata);
                    }
                    if (vm.autoPoDetails.length == 0) {
                        vm.autoPoFlag = false;
                        vm.poshowflag = true;
                        abp.notify.warn(app.localize('AutoPoMaterialNotExist'));
                        abp.message.warn(app.localize('AutoPoMaterialNotExist'));
                    }
                    else {
                        angular.forEach(vm.autoPoDetails, function (value, key) {
                            value.visibleFlag = true;
                            value.alreadyConverted = false;
                            value.poReferenceCode = '';
                            value.purchaseOrderId = null;
                        });
                    }
                    vm.autoPoloading = false;
                    vm.loading = false;
                }
                else {
                    purchaseOrderExtendedService.getAutoPoDetails({
                        locationRefId: vm.defaultLocationId
                    }).success(function (result) {
                        vm.autoPoDetails = result;
                        if (vm.autoPoDetails.length == 0) {
                            vm.autoPoFlag = false;
                            vm.poshowflag = true;
                            abp.notify.warn(app.localize('AutoPoMaterialNotExist'));
                            abp.message.warn(app.localize('AutoPoMaterialNotExist'));
                        }
                        else {
                            angular.forEach(vm.autoPoDetails, function (value, key) {
                                value.visibleFlag = true;
                                value.alreadyConverted = false;
                                value.poReferenceCode = '';
                            });
                        }
                    }).finally(function () {
                        vm.autoPoloading = false;
                        vm.loading = false;
                    });
                }
            }

            //fillDropDownLocation();
            fillDropDownSupplier();
            vm.fillDropDownTaxes();
            fillDropDownTemplate();

            vm.loadingflag = false;

            vm.makePoBasedOnAutoPo = function (data) {

                vm.loading = true;
                vm.autoPoloading = true;
                vm.hideAllAutoPos(data);
                result = angular.copy(data.autoPoDto);
                vm.poshowflag = true;
                vm.purchaseorder = result.purchaseOrder;
                vm.purchaseOrderDetail = result.purchaseOrderDetail;

                //vm.purchaseorder.locationRefId = vm.defaultLocationId;

                //fillDropDownShippingLocation();

                $scope.dt = moment().format($scope.format);
                vm.uilimit = null;

                var poDate = $scope.convert(result.purchaseOrder.poDate);

                //vm.purchaseorder.locationRefId = vm.purchaseOrder.locationRefId;
                vm.purchaseorder.poDate = moment(result.purchaseOrder.poDate).format($scope.format);
                vm.purchaseorder.deliveryDateExpected = moment(result.purchaseOrder.deliveryDateExpected).format($scope.format);
                $scope.minDate = vm.purchaseorder.poDate;

                $('input[name="poDate"]').daterangepicker({
                    locale: {
                        format: $scope.format
                    },
                    singleDatePicker: true,
                    showDropdowns: true,
                    startDate: vm.purchaseorder.poDate,
                    minDate: $scope.minDate,
                    maxDate: moment().format($scope.format)
                });

                $('input[name="dueDate"]').daterangepicker({
                    locale: {
                        format: $scope.format
                    },
                    singleDatePicker: true,
                    showDropdowns: true,
                    startDate: vm.purchaseorder.deliveryDateExpected,
                    minDate: $scope.minDate,
                });

                vm.purchaseDetailPortion = [];
                vm.sno = 0;
                vm.loading = true;
                angular.forEach(vm.purchaseOrderDetail, function (value, key) {
                    vm.discFlag = null;
                    if (value.discountOption == 'F')
                        vm.discFlag = false;
                    else if (value.discountOption == 'P')
                        vm.discFlag = true;
                    vm.sno = vm.sno + 1;
                    $scope.taxCalc(value, value.qtyOrdered, value.price);
                    vm.purchaseDetailPortion.push({
                        'sno': vm.sno,
                        'materialRefId': value.materialRefId,
                        'materialRefName': value.materialRefName,
                        'qtyOrdered': value.qtyOrdered,
                        'defaultUnitId': value.defaultUnitId,
                        'uom': value.uom,
                        'unitRefId': value.unitRefId,
                        'unitRefName': value.unitRefName,
                        'price': value.price,
                        'initialprice': value.price,
                        'discountOption': vm.discFlag,
                        'discountValue': value.discountValue,
                        'totalAmount': value.totalAmount,
                        'taxAmount': value.taxAmount,
                        'netAmount': value.netAmount,
                        'remarks': value.remarks,
                        'currentInHand': value.currentInHand,
                        'alreadyOrderedQuantity': value.alreadyOrderedQuantity,
                        'taxlist': value.taxForMaterial,
                        'applicableTaxes': value.applicableTaxes,
                        'changeunitflag': false,
                        'unitRefIdForMinOrder': value.unitRefIdForMinOrder,
                        'unitRefNameForMinOrder': value.unitRefNameForMinOrder,
                        'minimumOrderQuantity': value.minimumOrderQuantity,
                        'interTransferDetailWithPORefIds': value.interTransferDetailWithPORefIds,
                        'caterHeaderRefIds': value.caterHeaderRefIds,
                    });
                });
                vm.fillDropDownMaterial();
                vm.calculateTotalVlaue(vm.purchaseDetailPortion);

                vm.loading = false;
                vm.autoPoloading = false;
            }

            $scope.convert = function (thedate) {
                var tempstr = thedate.toString();
                var newstr = tempstr.toString().replace(/GMT.*/g, "")
                newstr = newstr + " UTC";
                return new Date(newstr);
            };

            vm.showAllAutoPos = function (data) {
                angular.forEach(data, function (value, key) {
                    value.visibleFlag = true;
                });
            }

            vm.hideAllAutoPos = function (data) {
                angular.forEach(vm.autoPoDetails, function (value, key) {
                    value.visibleFlag = false;
                });
                data.visibleFlag = true;
            }


            vm.viewPoDetail = function (purchaseOrderId) {
                if (purchaseOrderId != null)
                    openView(purchaseOrderId);
            };

            function openView(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/purchaseorder/podetailViewInGrid.cshtml',
                    controller: 'tenant.views.house.transaction.purchaseorder.podetailViewInGrid as vm',
                    backdrop: 'static',
                    resolve: {
                        poId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {

                }).finally(function (result) {
                });
            }

        }
    ]);
})();

