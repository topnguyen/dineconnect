﻿
(function () {
    appModule.controller('tenant.views.house.transaction.menuitemwastage.menuitemwastageMainForm', [
        '$scope', '$state', '$stateParams', '$uibModal',  'abp.services.app.commonLookup', 'abp.services.app.menuItemWastage', 'abp.services.app.location', 'appSession', 'abp.services.app.material', 'abp.services.app.unitConversion', 'abp.services.app.template', 'deviceDetector', 'abp.services.app.dayClose',
        function ($scope, $state, $stateParams, $modal, commonLookupService, menuitemwastageService, locationService, appSession, materialService, unitconversionService, templateService, deviceDetector, daycloseService) {
            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            vm.menuitemwastage = null;
            $scope.existall = true;
            vm.menuitemwastageId = $stateParams.id;
            vm.sno = 0;
            vm.menuitemwastagePortions = [];
            vm.loadingCount = 0;

            vm.menuItemWastageDetail = [];

            vm.defaultLocationId = appSession.user.locationRefId;
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.uilimit = 50;
            vm.templatesave = null;

            $scope.minDate = moment(appSession.location.houseTransactionDate).add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            $('input[name="closingDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });


            vm.defaulttemplatetype = 6;

            vm.refTemplateDetail = [];
            vm.reftemplate = [];

            function fillDropDownTemplate() {
                vm.loading = true;
                vm.loadingCount++;
                templateService.getTemplateDetail({
                    locationRefId: vm.defaultLocationId,
                    templateType: vm.defaulttemplatetype
                }).success(function (result) {
                    vm.refTemplateDetail = result.items;
                    angular.forEach(vm.refTemplateDetail, function (value, key) {
                        vm.reftemplate.push({
                            'value': value.id,
                            'displayText': value.name,
                        });
                    });
                    vm.loading = false;
                    vm.loadingCount--;
                });
            }

            vm.refmenuitem = [];

            function fillDropDownMenuItem() {
                vm.loading = true;
                vm.loadingCount++;

                commonLookupService.getMenuPortionForCombobox({}).success(function (result) {
                    vm.refmenuitem = result.items;
                    vm.loading = false;
                    vm.loadingCount--;
                });
            }


            fillDropDownTemplate();

            vm.fillDataFromTemplate = function () {
                vm.selectedTemplate = null;
                vm.refTemplateDetail.some(function (value, key) {
                    if (value.id == vm.template) {
                        vm.selectedTemplate = value.templateData;
                        return true;
                    }
                });

                if (vm.selectedTemplate == null)
                    return;

                vm.loading = true;

                menuitemwastageService.getTemplateObjectForEdit({
                    objectString: vm.selectedTemplate
                }).success(function (result) {

                    vm.menuitemwastage = result.menuItemWastage;

                    vm.menuItemWastageDetail = result.menuItemWastageDetail;

                    vm.menuitemwastage.salesDate = moment().format($scope.format);
                    vm.menuitemwastage.id = null;
                    vm.menuitemwastage.locationRefId = vm.defaultLocationId;

                    $('input[name="closingDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.menuitemwastage.salesDate,
                        minDate: $scope.minDate,
                        maxDate: moment().format($scope.format)
                    });

                    vm.menuitemwastagePortions = [];
                    vm.sno = 1;
                    angular.forEach(vm.menuItemWastageDetail, function (value, key) {
                        vm.menuitemwastagePortions.push({
                            'sno': vm.sno,
                            'posMenuPortionRefId': value.posMenuPortionRefId,
                            'posMenuPortionRefName': value.posMenuPortionRefName,
                            'wastageQty': value.wastageQty,
                            'wastageRemarks': value.wastageRemarks,
                        });
                        vm.sno = vm.sno + 1;
                    });
                    vm.sno = vm.menuitemwastagePortions.length + 1;

                    vm.loading = false;
                });

            }

            vm.templateSave = function () {
                vm.menuitemwastageRecipeDetail = [];
                vm.errorflag = false;

                if (vm.templatesave.templatename.length == 0) {
                    abp.message.warn(app.localize("TemplateNameRequired"));
                    return;
                }

                vm.saving = true;
                vm.menuItemWastageDetail = [];

                vm.errorflag = false;
                angular.forEach(vm.menuitemwastagePortions, function (value, key) {
                    if (value.unitRefId == null || value.unitRefId == 0) {
                        abp.message.warn(app.localize("UnitErr", value.materialRefName));
                        vm.errorflag = true;
                        return;
                    }

                    vm.menuItemWastageDetail.push({
                        'sno': vm.sno,
                        'posMenuPortionRefId': value.posMenuPortionRefId,
                        'posMenuPortionRefName': value.posMenuPortionRefName,
                        'wastageQty': value.wastageQty,
                        'wastageRemarks': value.wastageRemarks,
                    });

                });

                if (vm.errorflag == true) {
                    vm.saving = false;
                    return;
                }


                if (vm.templatesave.templatename != null)
                    vm.templatesave.templatename = vm.templatesave.templatename.toUpperCase();

                menuitemwastageService.saveTemplateMenuItemWastage({
                    menuitemwastage: vm.menuitemwastage,
                    menuItemWastageDetail: vm.menuItemWastageDetail,
                    templateSave: vm.templatesave,
                }).success(function (result) {
                    abp.notify.info(app.localize('Template') + ' ' + app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            }

            vm.save = function (argSaveOption) {
                if (vm.existall == false)
                    return;

                vm.menuitemwastageRecipeDetail = [];
                vm.errorflag = false;

                vm.saving = true;
                vm.menuItemWastageDetail = [];

                vm.errorflag = false;
                angular.forEach(vm.menuitemwastagePortions, function (value, key) {
                    if (value.wastageQty == 0 || value.wastageQty == null) {
                        abp.notify.warn(app.localize("WastageShouldNotBeZero", value.materialRefName));
                        vm.errorflag = true;
                    }

                    vm.menuItemWastageDetail.push({
                        'sno': value.sno,
                        'posMenuPortionRefId': value.posMenuPortionRefId,
                        'posMenuPortionRefName': value.posMenuPortionRefName,
                        'wastageQty': value.wastageQty,
                        'wastageRemarks': value.wastageRemarks,
                    });

                });


                if (vm.errorflag == true) {
                    vm.saving = false;
                    return;
                }


                menuitemwastageService.createOrUpdateMenuItemWastage({
                    menuitemwastage: vm.menuitemwastage,
                    menuItemWastageDetail: vm.menuItemWastageDetail,
                }).success(function (result) {
                    abp.notify.info(app.localize('MenuItemWastage') + ' ' + app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {
                $scope.errmessage = "";
                if (moment(vm.menuitemwastage.salesDate) > moment()) {
                    $scope.errmessage = app.localize('FutureDateNotAllowed');
                }

                if (moment(vm.menuitemwastage.salesDate) < appSession.location.houseTransactionDate) {
                    $scope.errmessage = app.localize('DateShouldBeGreaterThanHouseTransactionDate');
                }

                if ($scope.errmessage != "") {
                    abp.notify.warn($scope.errmessage, 'Required');
                    return false;
                }
                else {
                    return true;
                }
            };

            vm.addPortion = function (data) {
                if (data != null) {
                    if (data.sno < vm.sno - 1) {
                        if (data.posMenuPortionRefId == null || data.posMenuPortionRefId == 0 || data.posMenuPortionRefName == '') {
                            abp.notify.warn(app.localize('MenuItemErr'));
                        }

                        if (data.wastageQty == 0 || data.wastageQty == null) {
                            abp.notify.info(app.localize('QtyAsZeroAlert', data.posMenuPortionRefName));
                        }

                        if (errorFlag) {
                            vm.saving = false;
                            vm.loading = false;
                            $scope.existall = false;
                        }

                        if (data.sno < vm.sno) {
                            $(this).next().focus();
                            return;
                        }
                    }
                    else {

                    }
                }

                if (vm.sno > 0) {
                    if (vm.existall() == false)
                        return;

                    var errorFlag = false;

                    if (vm.menuitemwastagePortions.length <= 0) {
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }
                    else {
                        if (vm.isUndefinedOrNull(vm.menuitemwastagePortions[0].posMenuPortionRefId) || vm.menuitemwastagePortions[0].posMenuPortionRefId == 0) {
                            abp.notify.info(app.localize('MinimumOneDetail'));
                            return;
                        }
                    }

                    var lastelement = vm.menuitemwastagePortions[vm.menuitemwastagePortions.length - 1];

                    if (lastelement.posMenuPortionRefId == null || lastelement.posMenuPortionRefId == 0 || lastelement.posMenuPortionRefName == '') {
                        abp.notify.warn(app.localize('MenuItemErr'));
                    }

                    if (lastelement.wastageQty == 0 || lastelement.wastageQty == null) {
                        abp.notify.info(app.localize('QtyAsZeroAlert', lastelement.posMenuPortionRefName));
                    }

                    if (errorFlag) {
                        vm.saving = false;
                        vm.loading = false;
                        $scope.existall = false;
                    }

                }

                vm.sno = vm.sno + 1;
                vm.menuitemwastagePortions.push({
                    'sno': vm.sno, 'posMenuPortionRefId': 0, 'posMenuPortionRefName': '', 'wastageQty': null, 'wastageRemarks': "",
                });
            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex) {
                if (vm.menuitemwastagePortions.length > 1) {
                    vm.menuitemwastagePortions.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                }
                else {
                    vm.menuitemwastagePortions.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                    vm.addPortion();
                    return;
                }
            }

            $scope.func = function (data, val, obj) {
                var forLoopFlag = true;

                angular.forEach(vm.menuitemwastagePortions, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.displayText, value.posMenuPortionRefName)) {
                            abp.notify.info(app.localize('DuplicateMenuItemExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.posMenuPortionRefId = 0;
                    data.posMenuPortionRefName = '';
                    return;
                }
                data.posMenuPortionRefId = val.value;
                data.posMenuPortionRefName = val.displayText;
            };

            //vm.refmaterial = [];
            //vm.tempRefMaterial = [];

            //function fillDropDownRecipe() {
            //    materialService.getView({
            //        //skipCount: requestParams.skipCount,
            //        //maxResultCount: 1,
            //        //sorting: requestParams.sorting,
            //        //filter: vm.filterText,
            //        operation: 'FillAll',
            //    }).success(function (result) {
            //        vm.refmaterial = result.items;
            //    });
            //}

            //fillDropDownRecipe();


            vm.cancel = function () {
                $state.go("tenant.menuitemwastage");
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.reflocation = [];

            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;
                });
            }

            vm.refrequest = [];

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.checkCloseDay = function () {
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format('YYYY-MMM-DD')));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format('YYYY-MMM-DD')));

                    }
                    if (moment(result.accountDate).format("YYYY-MMM-DD") == moment().format("YYYY-MMM-DD"))
                        return;

                    $scope.minDate = moment(result.accountDate).format($scope.format);
                    $scope.maxDate = moment().format($scope.format);
                    vm.menuitemwastage.salesDate = moment().format($scope.format);

                    $('input[name="closingDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: false,
                        startDate: vm.menuitemwastage.salesDate,
                        minDate: $scope.minDate,
                        maxDate: $scope.maxDate
                    });

                }).finally(function (result) {

                });
            }


            function init() {
                fillDropDownLocation();
                fillDropDownMenuItem();

                menuitemwastageService.getMenuItemWastageForEdit({
                    Id: vm.menuitemwastageId
                }).success(function (result) {
                    vm.menuitemwastage = result.menuItemWastage;
                    vm.menuitemwastage.locationRefId = vm.defaultLocationId;

                    if (result.menuItemWastage.id != null) {
                        vm.uilimit = null;
                        vm.menuItemWastageDetail = result.menuItemWastageDetail;

                        vm.menuitemwastage.salesDate = moment(result.menuItemWastage.salesDate).format($scope.format);
                        $scope.minDate = vm.menuitemwastage.salesDate;

                        angular.forEach(result.menuItemWastageDetail, function (value, key) {
                            vm.menuitemwastagePortions.push({
                                'sno': value.sno,
                                'posMenuPortionRefId': value.posMenuPortionRefId,
                                'posMenuPortionRefName': value.posMenuPortionRefName,
                                'wastageQty': value.wastageQty,
                                'wastageRemarks': value.wastageRemarks,

                            })
                        });
                        vm.sno = vm.menuitemwastagePortions.length;
                    }
                    else {
                        vm.menuitemwastage.tokenRefNumber = '';
                        vm.menuitemwastage.salesDate = moment().format($scope.format);
                        vm.addPortion();
                        vm.checkCloseDay();
                        vm.uilimit = 50;
                    }
                    vm.loading = false;
                });
            }

            init();

            vm.removeUnfilled = function () {
                var listtoremoved = [];
                tempportions = [];

                angular.forEach(vm.menuitemwastagePortions, function (value, key) {
                    if (value.wastageQty == 0 || value.wastageQty == null || value.wastageQty == "") {
                        listtoremoved.push(key)
                    }
                    else {
                        tempportions.push(value);
                    }
                });

                vm.menuitemwastagePortions = tempportions;

                vm.sno = vm.menuitemwastagePortions.length;

                if (vm.menuitemwastagePortions.length == 0)
                    vm.addPortion();
            }

            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

            vm.unitReference = function (selected, data) {
                data.unitRefId = selected.value;
                data.unitRefName = selected.displayText;
                data.changeunitflag = false;
            };



            //  Manual Reason Modal Start
            vm.openManualReason = function (data, argColumnName) {
                vm.loading = true;
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/manualreason/manualReason.cshtml',
                    controller: 'tenant.views.house.master.manualreason.manualReason as vm',
                    backdrop: 'static',
                    resolve: {
                        manualreasonId: function () {
                            return null;
                        },
                        selectOnly: function () {
                            return true;
                        },
                        generalIncluded: function () {
                            return true;
                        },
                        defaultManualReasonCategoryRefId: function () {
                            return 6;
                        }
                        // General = 99,
                        //PurchaseOrder = 1,
                        //Receipt = 2,
                        //Invoice = 3,
                        //PurchaseReturn = 4,
                        //Adjustment = 5,
                        //MenuWastage = 6
                    }
                });

                modalInstance.result.then(function (result) {
                    if (!vm.isUndefinedOrNull(result)) {
                        if (argColumnName == 'remarks')
                            data.remarks = result;
                        else if (argColumnName == 'wastageRemarks') {
                            data.wastageRemarks = result;
                        }
                        else if (argColumnName == 'adjustmentRemarks') {
                            data.adjustmentRemarks = result;
                        }
                        else if (argColumnName == 'adjustmentApprovedRemarks') {
                            data.adjustmentApprovedRemarks = result;
                        }
                    }
                }, function (result) {
                    vm.loading = false;
                });


                modalInstance.closed.then(function (result) {

                }, function (result) {
                    vm.loading = false;
                });
                vm.loading = false;
            }
            //  Manual Reason Modal End
        }
    ]);


})();

