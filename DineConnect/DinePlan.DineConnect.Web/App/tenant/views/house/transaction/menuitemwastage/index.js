﻿
(function () {
    appModule.controller('tenant.views.house.transaction.menuitemwastage.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.menuItemWastage', 'appSession',
        function ($scope, $state, $modal, uiGridConstants, menuitemwastageService, appSession) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.advancedFiltersAreShown = false;
            vm.dateFilterApplied = false;
            vm.defaultLocationId = appSession.user.locationRefId;

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.clear = function () {
                vm.filterText = null;
                vm.dateRangeModel = null;
                vm.dateFilterApplied = false;
                vm.getAll();
            }


            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Transaction.MenuItemWastage.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Transaction.MenuItemWastage.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Transaction.MenuItemWastage.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editMenuItemWastage(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteMenuItemWastage(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('SalesDate'),
                        field: 'salesDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                        minWidth: 100
                    },
                    {
                        name: app.localize('Ref#'),
                        field: 'tokenRefNumber'
                    },
                    {
                        name: app.localize('Remarks'),
                        field: 'remarks'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };



            vm.getAll = function () {
                vm.loading = true;
                var startDate = null;
                var endDate = null;
                //var tokenRefNumber = null;

                if (vm.dateFilterApplied) {

                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }
                //tokenRefNumber = vm.tokenRefNumber;

                menuitemwastageService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    startDate: startDate,
                    endDate: endDate,
                    locationRefId: vm.defaultLocationId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editMenuItemWastage = function (myObj) {
                //if (myObj.accountDate != null || myObj.adjustmentRefId != null) {
                //    abp.notify.warn(app.localize('WastageLinkedWithLedgerErr'));
                //    return;
                //}

                openCreateOrEditModal(myObj.id);
            };

            vm.createMenuItemWastage = function () {
                openCreateOrEditModal(null);
            };

            vm.deleteMenuItemWastage = function (myObject) {
                if (myObject.accountDate != null || myObject.adjustmentRefId != null) {
                    abp.notify.warn(app.localize('WastageLinkedWithLedgerErr'));
                    return;
                }

                abp.message.confirm(
                    app.localize('DeleteMenuItemWastageWarning', myObject.tokenRefNumber),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            menuitemwastageService.deleteMenuItemWastage({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.menuitemwastagedetail", {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                //        menuitemwastageService.getAllToExcel({})
                //            .success(function (result) {
                //                app.downloadTempFile(result);
                //});

                vm.loading = true;

                var startDate = null;
                var endDate = null;
                //var tokenRefNumber = null;

                if (vm.dateFilterApplied) {

                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }

                menuitemwastageService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: 20,
                    sorting: requestParams.sorting,
                    locationRefId: vm.defaultLocationId,
                    filter: vm.filterText,
                    startDate: startDate,
                    endDate: endDate,
                    locationRefId: vm.defaultLocationId
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });

            };


            vm.getAll();
        }]);
})();

