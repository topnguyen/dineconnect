﻿(function () {
    appModule.controller('tenant.views.house.transaction.dayclose.daycloseindex', [
        '$scope', '$state', '$window', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.material', 'appSession', '$timeout', '$interval', 'abp.services.app.dayClose',
        function ($scope, $state, $window, $stateParams, $modal, uiGridConstants, materialService, appSession, $timeout, $interval, daycloseService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.doesRunInBackGround = false;
            vm.backgroundFinished = null;
            $scope.startTime = new Date().toLocaleDateString() + ' ' + +  new Date().toLocaleTimeString();

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.backgroundTransactionDate = null;
            vm.displayDate = '';
            if ($stateParams.doesRunInBackGround == true || $stateParams.doesRunInBackGround == 'true') {
                vm.doesRunInBackGround = true;
                vm.backgroundTransactionDate = $stateParams.backgroundTransactionDate;
                vm.displayDate = moment(vm.backgroundTransactionDate).format('YYYY-MMM-DD ddd');
            }

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Transaction.DayClose.Create'),
                missingSalesIntoDayClose: abp.auth.hasPermission('Pages.Tenant.House.Transaction.DayClose.MissingSalesIntoDayClose'),
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: 10, // app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('AccountDate'),
                        field: 'closedDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'LLL\'',
                    }

                ],
                onRegisterApi: function (gridApi) {

                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.defaultLocationId = appSession.user.locationRefId;

            vm.getAll = function () {
                vm.loading = true;
                materialService.getAllDayClose({
                    locationRefId: vm.defaultLocationId,
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                }).then(function (result) {
                    vm.userGridOptions.totalItems = result.data.totalCount;
                    vm.userGridOptions.data = result.data.items;
                }).finally(function () {

                    vm.loading = false;
                });
            };

            vm.createDetailItem = function (myObj) {
                openDetailMenuItem(null, false);
            };


            function openDetailMenuItem(objId, changeUnitFlag) {
                $state.go("tenant.dayclose", {
                });
            };

            vm.missingSalesModule = function () {
                $state.go("tenant.includemissingsales", {
                });
            }

            vm.getAll();

            var executeDayCloseInInterval = null;

            vm.backgroundCheckCloseDay = function () {
                vm.loadingCount++;
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.doesDayCloseRunInBackGround == true) {
                        vm.backgroundTransactionDate = moment(result.accountDate).format($scope.format);
                        vm.displayDate = moment(vm.backgroundTransactionDate, 'YYYY-MMM-DD').format('YYYY-MMM-DD ddd');
                        abp.notify.warn(app.localize("StillBackGroundProcessing", moment(result.accountDate).format($scope.format)));
                        if (result.backGroundDayCloseStartTime != null) {
                            var a = result.backGroundDayCloseStartTime;
                            $scope.startTime = new Date(result.backGroundDayCloseStartTime).toLocaleDateString() + ' ' + new Date(result.backGroundDayCloseStartTime).toLocaleTimeString();

                        }
                        vm.noofInterval++;
                        if (vm.noofInterval > 40)
                            vm.addedText = "Its Almost Done ....";
                        else if (vm.noofInterval > 30)
                            vm.addedText = "Processing in Mid Stage ...";
                        else if (vm.noofInterval > 10)
                            vm.addedText = "In Progress ...";
                        else if (vm.noofInterval > 0)
                            vm.addedText = "Initiated ...";

                        vm.backgroundStatus = app.localize('DayCloseBackGroundRunning', vm.displayDate, vm.addedText);
                    }
                    else {
                        abp.notify.info(app.localize("BackGroundProcessFinished"));
                        abp.notify.info(app.localize("BackGroundProcessFinished"));
                        abp.message.info(app.localize("BackGroundProcessFinished"));
                        vm.doesRunInBackGround = false;
                        vm.backgroundFinished = true;
                        $interval.cancel(executeDayCloseInInterval);
                        vm.remarksAfterFinished = app.localize("BackGroundProcessFinished");
                        vm.getAll()
                        // Pending Auto Audjustment

                    }
                }).finally(function (result) {
                    vm.loadingCount--;
                });
            }

            $scope.theTime = new Date().toLocaleTimeString();
            vm.noofInterval = 0;
            vm.backgroundStarted = true;
            vm.checkBackGround = function () {
                if (vm.doesRunInBackGround) {
                    executeDayCloseInInterval =
                        $interval(function () {
                            vm.backgroundStarted = true;
                            vm.backgroundCheckCloseDay();
                            $scope.theTime = new Date().toLocaleTimeString();
                        }, 10000);
                }
            }
            vm.checkBackGround();

            vm.openHangFire = function () {
                var url = "";
                url = "http://" + window.location.host + "/hangfire";
                //$window.open(url, '_blank');
                //url = "http://localhost:7301/hangfire";
                window.open(url);
            }

            vm.checkCloseDay = function () {
                if (vm.doesRunInBackGround)
                    return;
                vm.loadingCount++;
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.doesDayCloseRunInBackGround == true && vm.doesRunInBackGround == false) {
                        vm.doesRunInBackGround = result.doesDayCloseRunInBackGround;
                        vm.backgroundTransactionDate = moment(result.accountDate).format($scope.format);
                        vm.displayDate = moment(vm.backgroundTransactionDate, 'YYYY-MMM-DD').format('YYYY-MMM-DD ddd');
                        abp.notify.warn(app.localize("StillBackGroundProcessing", moment(result.accountDate).format($scope.format)));
                        vm.noofInterval++;
                        if (vm.noofInterval > 40)
                            vm.addedText = "Its Almost Done ....";
                        else if (vm.noofInterval > 30)
                            vm.addedText = "Processing in Mid Stage ...";
                        else if (vm.noofInterval > 10)
                            vm.addedText = "In Progress ...";
                        else if (vm.noofInterval > 0)
                            vm.addedText = "Initiated ...";

                        vm.backgroundStatus = app.localize('DayCloseBackGroundRunning', vm.displayDate, vm.addedText);
                        vm.checkBackGround();
                    }
                }).finally(function (result) {
                    vm.loadingCount--;
                });
            }

            vm.checkCloseDay();

            //$interval(function () {
            //    $scope.theTime = new Date().toLocaleTimeString();
            //}, 20000);

            //$interval(function () {
            //    vm.checkCloseDay();
            //}, 5000);

        }]);
})();

