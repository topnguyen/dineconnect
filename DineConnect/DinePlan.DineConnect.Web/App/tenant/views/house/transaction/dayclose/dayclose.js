﻿(function () {
    appModule.controller('tenant.views.house.transaction.dayclose.dayclose', [
        '$scope', '$state', '$stateParams', 'abp.services.app.location', 'appSession', '$uibModal', 'abp.services.app.material', 'abp.services.app.closingStock', '$rootScope', 'abp.services.app.dayClose',
        function ($scope, $state, $stateParams, locationService, appSession, $modal, materialService, closingstockService, $rootScope, daycloseService) {
            /* eslint-disable */
            var vm = this;
            vm.saving = false;
            vm.loading = false;
            $scope.existall = true;
            vm.sno = 0;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.wipeoutstockdetails = [];
            vm.verifymode = false;
            vm.autopostrequired = false;
            vm.locationRefId = null;
            vm.wipeoutMaterialpendinglist = [];
            vm.carryoverMateriallist = [];
            vm.carryoverMateriallistAllStockSold = [];
            vm.carryoverMateriallistReceivedOrProductionEntryMissing = [];
            vm.carryoverMateriallistNegativeStockBecomesZero = [];
            vm.carryoverMateriallistCarryOverToNextDay = [];
            vm.compWastageMaterialList = [];
            vm.wastageMenuMaterialList = [];
            vm.carryoverMaterialOthers = [];
            vm.resetFlag = true;
            vm.firstFlag = true;
            vm.status = '';
            vm.acceptNegativeStockFlag = false;
            vm.skipTransactionDays = false;

            $rootScope.settings.layout.pageSidebarClosed = true;

            vm.backgroundSettingExists = abp.setting.getBoolean("App.House.BackGroundDayCloseCanBeAllowed");

            vm.permissions = {
                negativeStockAllowedAuthorization: abp.auth.hasPermission('Pages.Tenant.House.Master.MaterialLedger.NegativeStock'),
                hideSales: abp.auth.hasPermission('Pages.Tenant.House.Transaction.DayClose.HideSalesOnDayClose'),
                hideCarryOverStockInDayClose: abp.auth.hasPermission('Pages.Tenant.House.Transaction.DayClose.HideCarryOverStockInDayClose'),
                sameDayClose: abp.auth.hasPermission('Pages.Tenant.House.Transaction.DayClose.SameDayClose')
            };



            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            $('input[name="trnDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });

            vm.runInBackGround = false;

            vm.backgroundsave = function (argOption) {
                vm.runInBackGround = true;
                vm.save(argOption);
            }

            vm.checkNegativeStockPermission = function () {
                if (vm.permissions.negativeStockAllowedAuthorization == false) {
                    abp.notify.error("You do not have the rights for Accept Negative Stock");
                    return false;
                }
                else {
                    return true;
                }
            }

            vm.save = function (argOption) {
                console.log(argOption + "Coming Here");
                if (vm.acceptNegativeStockFlag == true) {
                    if (vm.checkNegativeStockPermission == false) {
                        abp.notify.error("Day Close denied due the Negative Stock");
                        return;
                    }
                }
                vm.loading = true;
                vm.saving = true;

                if (argOption == 2) {
                    vm.affectstock = true;
                    vm.status = "Saving Process...";
                }
                else {
                    vm.affectstock = false;
                    vm.status = "Verifying........";
                }

                vm.daycloseSuccessFlag = false;

                materialService.verifyWipeOutStockForCloseDay(
                    {
                        locationRefId: vm.locationRefId,
                        transactionDate: vm.housetransactiondate,
                        ledgerAffect: vm.affectstock,
                        resetFlag: vm.resetFlag,
                        runInBackGround: vm.runInBackGround
                    }).then(function (result) {
                        if (result.data.doesRunInBackGround) {
                            $state.go("tenant.daycloseindex", {
                                doesRunInBackGround: true,
                                backgroundTransactionDate: vm.housetransactiondate,
                                remarks: ''
                            });
                            //$state.go('tenant.dashboard');
                            return;
                        }

                        getClosingStockDetails();

                        vm.daycloseSuccessFlag = result.data.dayCloseSuccessFlag;
                        vm.salesMessage = result.data.salesMessage;
                        vm.salesExistFlag = result.data.salesExistFlag;
                        vm.dayCloseSyncErrorList = result.data.dayCloseSyncErrorList;
                        vm.lastDayCloseErrorFlag = result.data.lastDayCloseErrorFlag;

                        vm.dayclosestock = result.data.salesData;
                        vm.compWastageMaterialList = result.data.compData;
                        vm.wastageMenuMaterialList = result.data.menuWastageData;
                        vm.menuWastageConsolidated = result.data.menuWastageConsolidated;

                        vm.wipeoutMaterialpendinglist = [];
                        vm.carryoverMateriallist = [];
                        vm.carryoverMateriallistAllStockSold = [];
                        vm.carryoverMateriallistReceivedOrProductionEntryMissing = [];
                        vm.carryoverMateriallistNegativeStockBecomesZero = [];
                        vm.carryoverMateriallistCarryOverToNextDay = [];
                        vm.carryoverMaterialOthers = [];

                        var noissueFlag = true;
                        var negativeStockAnyExist = false;

                        angular.forEach(vm.dayclosestock, function (value, key) {
                            if (value.wipeOutStockOnClosingDay) {
                                vm.wipeoutMaterialpendinglist.push(value);
                            }
                            else {
                                //if (sortMenuItems[loopIndex].Difference == 0) {
                                //    sortMenuItems[loopIndex].Remarks = L("AllStockSold");
                                //}
                                //else if (sortMenuItems[loopIndex].Difference < 0) {
                                //    sortMenuItems[loopIndex].Remarks = L("ReceivedOrProductionEntryMissing");
                                //}
                                //else {
                                //    sortMenuItems[loopIndex].Remarks = L("CarryOverToNextDay");
                                //}
                                vm.carryoverMateriallist.push(value);


                                if (value.remarks == app.localize('AllStockSold')) {
                                    vm.carryoverMateriallistAllStockSold.push(value);
                                }
                                else if (value.remarks == app.localize('ReceivedOrProductionEntryMissing')) {
                                    vm.carryoverMateriallistReceivedOrProductionEntryMissing.push(value);
                                }
                                else if (value.remarks == app.localize('NegativeStockBecomesZero')) {
                                    vm.carryoverMateriallistNegativeStockBecomesZero.push(value);
                                }
                                else if (value.remarks == app.localize('CarryOverToNextDay')) {
                                    vm.carryoverMateriallistCarryOverToNextDay.push(value);
                                }
                                else {
                                    vm.carryoverMaterialOthers.push(value);
                                }


                                if (value.difference < 0) {
                                    if (vm.permissions.negativeStockAllowedAuthorization == false || vm.acceptNegativeStockFlag == false) {
                                        negativeStockAnyExist = true;
                                    }
                                    abp.notify.warn(app.localize('NegativeStockErrWithName', value.difference, value.materialRefName));
                                }
                            }
                        });

                        if (negativeStockAnyExist == true)
                            noissueFlag = false;

                        vm.negativeAdjustmentFlag = false;

                        angular.forEach(vm.wipeoutMaterialpendinglist, function (value, key) {
                            if (noissueFlag) {
                                if (value.difference != 0) {
                                    abp.notify.info(app.localize('SomeMaterialsNotTallied'));
                                    noissueFlag = false;
                                }
                            }
                            if (value.difference < 0) {
                                vm.negativeAdjustmentFlag = true;
                            }
                        });

                        if (noissueFlag) {
                            vm.verifymode = true;
                        }
                        else {
                            vm.verifymode = false;
                        }

                        //  If Sales not happened , then Save after verify will only close the day
                        if (vm.salesExistFlag == false && vm.resetFlag == true) {
                            vm.verifymode = false;
                        }

                        vm.resetFlag = false;

                    }).finally(function () {

                        vm.loading = false;
                        vm.saving = false;
                        vm.status = '';

                        if (vm.daycloseSuccessFlag == false)
                            return;

                        if (argOption == 1) {
                            if (vm.wipeoutMaterialpendinglist.length > 0 && vm.verifymode == false)
                                vm.autopostrequired = true;
                            else
                                vm.autopostrequired = false;
                            //return;

                            if (vm.verifymode == true) {
                                if (vm.firstFlag == false
                                    && appSession.location.isDayCloseRecursiveUptoDateAllowed == true) {
                                    vm.save(2);
                                    return;
                                }
                                //else 
                                //    vm.save(2);
                                vm.firstFlag = false;
                                return;
                            }
                        }

                        if (vm.verifymode == false)
                            return;

                        vm.loading = true;
                        vm.saving = true;
                        daycloseService.setCloseDay(
                            {
                                locationRefId: vm.locationRefId,
                                transactionDate: vm.housetransactiondate
                            }).then(function (result) {
                                vm.status = '';
                                abp.notify.info(app.localize("DayCloseSuccess", vm.housetransactiondate));
                                appSession.location.houseTransactionDate = result.data.currentDate;
                                if (vm.closingStockCount > 0) {
                                    $state.go("tenant.adjustmentdetail", {
                                        id: null,
                                        autodetail: null,
                                        forceAdjustmentFlag: false,
                                        closingstockAdjustmentFlag: true,
                                        dayCloseCallFlag: true
                                    });
                                    return;
                                }
                                if ((moment(result.data.currentDate).format("YYYY-MM-DD") == moment().format("YYYY-MM-DD")) || vm.sameDayCloseAlertFlag)
                                    $state.go('tenant.dashboard');
                                else
                                    init();
                            }).finally(function () {
                                vm.loading = false;
                                vm.saving = false;
                                vm.status = '';
                            });
                    });
            }

            vm.wipeoutMaterialpendinglist = [];


            vm.reflocation = [];

            function fillDropDownLocation() {
                vm.loading = true;
                locationService.getLocationForCombobox({}).then(function (result) {
                    vm.reflocation = result.data.items;
                    if (vm.defaultLocationId != null && vm.defaultLocationId > 0) {
                        vm.locationRefId = vm.defaultLocationId;
                    }
                }).finally(function () {
                    fillTransactionDay();
                });
            }

            vm.closingStockCount = 0;

            function getClosingStockDetails() {
                vm.closingStockCount = 0;
                closingstockService.getClosingExists({
                    dt: vm.housetransactiondate,
                    locationRefId: vm.defaultLocationId
                }).then(function (result) {
                    vm.closingStockCount = result.data.count;
                }).finally(function () {
                });
            }


            vm.sameDayCloseAlertFlag = false;
            vm.sameDayCloseMessage = "";

            function fillTransactionDay() {
                vm.loading = true;
                daycloseService.getDayClose(
                    {
                        id: vm.defaultLocationId
                    }).then(function (result) {
                        vm.housetransactiondate = moment(result.data.transactionDate).format($scope.format);
                        var a = vm.housetransactiondate.toUpperCase();
                        var b = moment().format("YYYY-MMM-DD").toUpperCase();

                        //if (moment(vm.housetransactiondate).format("YYYY-MM-DD").toUpperCase() == moment().format("YYYY-MM-DD").toUpperCase()) {
                        if (a == b) {
                            if (!vm.permissions.sameDayClose) {
                                $state.go('tenant.dashboard');
                                abp.notify.error(app.localize("YouCanNotCloseDateForTodayAsOfNow"));
                                abp.message.warn(app.localize("YouCanNotCloseDateForTodayAsOfNow"));
                                return;
                            }
                            vm.sameDayCloseAlertFlag = true;
                            vm.sameDayCloseMessage = app.localize('SameDayCloseMessage', b);
                        }
                        else {
                            vm.sameDayCloseAlertFlag = false;
                            vm.sameDayCloseMessage = "";
                        }
                        vm.skipTransactionDays = result.data.skipTransactionDays;
                        vm.remarks = '';
                        if (result.data.remarks != '') {
                            vm.remarks = result.data.remarks;
                            vm.skipTransactionDays = result.data.skipTransactionDays;
                            getClosingStockDetails();
                        }

                        $('input[name="trnDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: vm.housetransactiondate,
                            minDate: vm.housetransactiondate,
                            maxDate: vm.housetransactiondate
                        });

                    }).finally(function () {
                        vm.loading = false;
                        if (vm.locationRefId != null) {
                            if (vm.remarks == '')
                                vm.save(1);
                        }

                    });
            }

            vm.autoadjustmentpost = function () {
                if (moment(vm.housetransactiondate).format("YYYY-MM-DD") == moment().format("YYYY-MM-DD")) {
                    if (vm.permissions.sameDayClose == false) {
                        abp.message.warn(app.localize("YouCanNotCloseDateForTodayAsOfNow"));
                        return;
                    }
                }

                vm.adjustmentDetail = [];
                vm.saving = true;
                vm.sno = 0;

                angular.forEach(vm.wipeoutMaterialpendinglist, function (value, key) {
                    vm.sno = vm.sno + 1;
                    if (value.difference > 0) {
                        vm.adjustmentDetail.push({
                            'sno': vm.sno,
                            'adjustmentRefIf': value.adjustmentRefIf,
                            'materialRefId': value.materialRefId,
                            'materialName': value.materialRefName,
                            'adjustmentQty': value.difference,
                            'adjustmentMode': app.localize('Wastage'),
                            'adjustmentApprovedRemarks': app.localize('DayClose'),
                            'unitRefId': value.defaultUnitId,
                            'unitRefName': value.defaultUnitName,
                            'defaultUnitId': value.defaultUnitId,
                            'defaultUnitName': value.defaultUnitName
                        });
                    }

                    if (value.difference < 0) {
                        vm.adjustmentDetail.push({
                            'sno': vm.sno,
                            'adjustmentRefIf': value.adjustmentRefIf,
                            'materialRefId': value.materialRefId,
                            'materialName': value.materialRefName,
                            'adjustmentQty': Math.abs(value.difference),
                            'adjustmentMode': app.localize('Excess'),
                            'adjustmentApprovedRemarks': app.localize('DayClose'),
                            'unitRefId': value.defaultUnitId,
                            'unitRefName': value.defaultUnitName,
                            'defaultUnitId': value.defaultUnitId,
                            'defaultUnitName': value.defaultUnitName
                        });
                    }

                });


                if (vm.adjustmentDetail.length > 0) {
                    vm.passvalue = JSON.stringify(vm.adjustmentDetail);

                    $state.go("tenant.adjustmentdetail", {
                        id: null,
                        autoadjustmentdetail: vm.passvalue,
                    });
                }
                else {
                    abp.notify.warn(app.localize('EnterManualAdjustmentForCarryOverMaterials'));
                }
                vm.saving = false;
                vm.status = '';
            }



            function init() {
                vm.resetFlag = true;
                vm.compWastageMaterialList = [];
                vm.wastageMenuMaterialList = [];
                vm.menuWastageConsolidated = [];

                vm.wipeoutMaterialpendinglist = [];
                vm.carryoverMateriallist = [];
                vm.carryoverMateriallistAllStockSold = [];
                vm.carryoverMateriallistReceivedOrProductionEntryMissing = [];
                vm.carryoverMateriallistNegativeStockBecomesZero = [];
                vm.carryoverMateriallistCarryOverToNextDay = [];
                vm.carryoverMaterialOthers = [];
                vm.salesMessage = '';
                vm.closingStockCount = 0;


                fillDropDownLocation();

                if (vm.permissions.hideCarryOverStockInDayClose == true) {
                    abp.notify.error(app.localize('HideStockMessage'));
                }
            }

            init();

            vm.detailDataShown = function (data) {
                var argOption = 'Sales';

                var ledgerDetails = {
                    locationRefId: vm.defaultLocationId,
                    ledgerDate: vm.housetransactiondate,
                    materialRefId: data.materialRefId,
                    materialRefName: data.materialRefName,
                    uom: data.uom,
                    sales: data.sales
                };

                var modalInstance1 = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/materialledger/drillDownLevel.cshtml',
                    controller: 'tenant.views.house.master.materialledger.drillDownLevel as vm',
                    backdrop: 'static',
                    resolve: {
                        ledgerDetails: function () {
                            return ledgerDetails;
                        },
                        searchFor: function () {
                            return argOption;
                        },
                        startDate: function () {
                            return vm.housetransactiondate;
                        },
                        endDate: function () {
                            return vm.housetransactiondate;
                        },
                    }
                });

                modalInstance1.result.then(function (result) {
                    //vm.getAll();
                });
            }

            vm.checkCloseDay = function () {
                vm.loadingCount++;
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.doesDayCloseRunInBackGround == true && vm.backgroundStarted == false) {
                        vm.doesRunInBackGround = result.doesDayCloseRunInBackGround;
                        vm.backgroundTransactionDate = moment(result.accountDate).format($scope.format);
                        vm.displayDate = moment(vm.backgroundTransactionDate).format('YYYY-MMM-DD ddd');
                        abp.notify.warn(app.localize("StillBackGroundProcessing", moment(result.accountDate).format($scope.format)));
                    }
                }).finally(function (result) {
                    vm.loadingCount--;
                });
            }

            vm.checkCloseDay();



            vm.backgroundTest = function () {
                materialService.helloWorldTest({
                    id: 15
                })
                    .success(function (result) {
                        abp.notify.info('Background Started');
                    });
            }

            vm.skipTransactionDayProcess = function () {
                vm.loading = true;
                vm.saving = true;
                daycloseService.setCloseDay(
                    {
                        locationRefId: vm.locationRefId,
                        transactionDate: vm.housetransactiondate
                    }).then(function (result) {
                        vm.status = '';
                        abp.notify.info(app.localize("DayCloseSuccess", vm.housetransactiondate));
                        appSession.location.houseTransactionDate = result.data.currentDate;
                        if (vm.closingStockCount > 0) {
                            $state.go("tenant.adjustmentdetail", {
                                id: null,
                                autodetail: null,
                                forceAdjustmentFlag: false,
                                closingstockAdjustmentFlag: true,
                                dayCloseCallFlag: true
                            });
                            return;
                        }
                        if (moment(result.data.currentDate).format("YYYY-MM-DD") == moment().format("YYYY-MM-DD"))
                            $state.go('tenant.dashboard');
                        else
                            init();
                    }).finally(function () {
                        vm.loading = false;
                        vm.saving = false;
                        vm.status = '';
                    });
            }


        }
    ]);
})();