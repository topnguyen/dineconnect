﻿(function () {
    appModule.controller('tenant.views.house.transaction.customersales.salesinvoice.salesinvoiceDetail', [
        '$scope', '$filter', '$state', '$stateParams', 'abp.services.app.salesInvoice',
        'abp.services.app.commonLookup', 'appSession', 'abp.services.app.customer', 'abp.services.app.supplierMaterial', 'abp.services.app.location', 'abp.services.app.material',
        'abp.services.app.unitConversion', 'abp.services.app.dayClose',
        function ($scope, $filter, $state, $stateParams, salesinvoiceService, commonLookupService, appSession, customerService, suppliermaterialService, locationService, materialService, unitconversionService, daycloseService) {
            var vm = this;
            vm.detailloading = false;

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");


            vm.saving = false;
            vm.salesinvoice = null;
            $scope.existall = true;
            vm.salesinvoiceId = $stateParams.id;
            vm.salesinvoiceType = $stateParams.salesinvoiceType;
            vm.reftax = [];
            vm.uilimit = null;
            vm.allTimeDcs = true;
            vm.refdctranid = [];
            vm.salesinvoicemode = [];
            vm.salesinvoicePortions = [];
            vm.refunit = [];
            vm.sno = 0;
            vm.salesinvoicedetail = [];
            vm.availableQty = 0;
            vm.totalTaxAmount = 0;
            vm.salesinvoiceTotalAmount = 0;

            vm.ratechangeallowedflag = true;// abp.setting.getBoolean("App.House.SalesInvoiceCustomerRateChangeFlag");
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.defaultLocationId = appSession.user.locationRefId;
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;

            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.temptaxinfolist = [];

            $scope.minDate = moment().add(-90, 'days');  // -30 or -60 Sysadmin Table 
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];


            $('input[name="invoiceDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });

            vm.tempsalesinvoicedetail = [];

            vm.salesinvoicedeliveryorderlink = [];
            vm.materialTotAmt = 0;

            vm.salesinvoicemode.push(
                { 'modeno': 1, 'invmode': 'CREDIT' }, { 'modeno': 2, 'invmode': 'CASH' });

            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();

            //vm.AccountdateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            //////---------------- Direct SalesInvoice

            $scope.func = function (data, val, obj) {
                var forLoopFlag = true;

                angular.forEach(vm.salesinvoicePortions, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialRefName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.materialRefId = 0;
                    data.materialRefName = '';
                    return;
                }
                data.materialRefName = val.materialRefName;
                data.uom = val.uom;
                data.defaultUnitId = val.defaultUnitId;
                data.price = parseFloat(val.materialPrice);
                data.initialprice = parseFloat(val.materialPrice);
                data.unitRefName = val.uom;
                data.unitRefId = val.unitRefId;

                if (data.price == 0)
                    data.rateeditflag = true;
                else
                    data.rateeditflag = vm.ratechangeallowedflag;

                vm.temptaxinfolist = [];

                angular.forEach(val.taxForMaterial, function (value, key) {
                    vm.temptaxinfolist.push({
                        'materialRefId': value.materialRefId,
                        'taxRefId': value.taxRefId,
                        'taxName': value.taxName,
                        'taxRate': value.taxRate,
                        'taxValue': value.taxValue,
                        'rounding': value.rounding,
                        'sortOrder': value.sortOrder,
                        'taxCalculationMethod': value.taxCalculationMethod
                    });
                });
                data.taxForMaterial = vm.temptaxinfolist;
                data.applicableTaxes = val.applicableTaxes;
                data.unitRefId = val.unitRefId;
                //fillDropDownUnitBasedOnMaterial(data.materialRefId, data);
            };

            vm.changeUnit = function (data) {
                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

            vm.unitReference = function (selected, data) {
                data.unitRefId = selected.value;
                data.unitRefName = selected.displayText;
                data.changeunitflag = false;

                if (data.defaultUnitId == data.unitRefId) {
                    data.price = data.initialprice;
                }
                else {
                    var convFactor = 0;
                    var convExistFlag = false;
                    vm.refconversionunit.some(function (refdata, refkey) {
                        if (refdata.baseUnitId == data.defaultUnitId && refdata.refUnitId == data.unitRefId) {
                            convFactor = refdata.conversion;
                            convExistFlag = true;
                            return true;
                        }
                    });


                    if (convExistFlag == false) {
                        abp.notify.info(app.localize('ConversionFactorNotExist', data.uom, data.unitRefName));
                        return;
                    }

                    var converstionUnitPrice = data.initialprice * 1 / convFactor;
                    data.price = converstionUnitPrice.toFixed(2);
                }
                vm.amtCalc(data, data.totalQty, data.price);
                vm.calculateTotalVlaue(vm.salesinvoicePortions);
            }

            $scope.taxCalc = function (data, totalQty, price) {
                if (vm.isUndefinedOrNull(totalQty))
                    totalQty = 0;
                if (vm.isUndefinedOrNull(price))
                    price = 0;

                data.totalAmount = parseFloat(price) * parseFloat(totalQty);
                data.totalAmount = parseFloat(data.totalAmount.toFixed(2));

                data.taxAmount = 0;
                data.taxForMaterial = [];

                taxneedstoremove = [];


                if (data.applicableTaxes != null && data.applicableTaxes.length > 0)    //  Calculate Tax 
                {
                    angular.forEach(data.applicableTaxes, function (value, key) {
                        var taxCalculationOnValue = 0.0;

                        if (value.taxCalculationMethod == 'GT' || value.taxCalculationMethod == 'ST') {
                            taxCalculationOnValue = data.totalAmount;
                        }
                        else {
                            var taxrefvalueflag = false;
                            data.taxForMaterial.some(function (taxdata, taxkey) {
                                if (taxdata.taxName == value.taxCalculationMethod) {
                                    taxCalculationOnValue = taxdata.taxValue;
                                    taxrefvalueflag = true;
                                    return true;
                                }
                            });

                            if (taxrefvalueflag == false)
                                taxneedstoremove.push({ 'taxName': value.taxName });
                        }

                        if (taxCalculationOnValue > 0) {
                            var taxamt = parseFloat(taxCalculationOnValue * value.taxRate / 100);
                            taxamt = parseFloat(taxamt.toFixed(2));
                            data.taxAmount = parseFloat(data.taxAmount) + parseFloat(taxamt);
                            data.taxAmount = parseFloat(data.taxAmount.toFixed(2));

                            if (taxamt > 0) {
                                data.taxForMaterial.push
                                    ({
                                        'materialRefId': data.materialRefId,
                                        'taxRefId': value.taxRefId,
                                        'taxName': value.taxName,
                                        'taxRate': value.taxRate,
                                        'taxValue': taxamt,
                                        'rounding': value.rounding,
                                        'sortOrder': value.sortOrder,
                                        'taxCalculationMethod': value.taxCalculationMethod
                                    });
                            }
                        }
                    });
                }


                angular.forEach(taxneedstoremove, function (value, key) {
                    angular.forEach(data.applicableTaxes, function (detvalue, detkey) {
                        if (value.taxName == detvalue.taxName)
                            data.applicableTaxes.splice(detkey, 1);
                    });
                });

                data.netAmount = parseFloat(parseFloat(data.totalAmount).toFixed(2)) + parseFloat(parseFloat(data.taxAmount).toFixed(2));
                vm.calculateTotalVlaue(vm.salesinvoicePortions);
            }



            vm.amtCalc = function (data, totalQty, price) {

                $scope.taxCalc(data, totalQty, price);
                return;
            }

            vm.refconversionunit = [];
            vm.fillUnitConversion = function () {
                unitconversionService.getAll({
                }).success(function (result) {
                    vm.refconversionunit = result.items;
                });
            }

            vm.fillUnitConversion();

            vm.refmaterial = [];
            vm.fillDropDownMaterial = function () {
                if (vm.salesinvoice.locationRefId == 0 || vm.salesinvoice.locationRefId == null) {
                    abp.notify.error(app.localize('Wait'));
                    return;
                }
                vm.detailloading = true;
                vm.loading = true;
                suppliermaterialService.getMaterialBasedOnCustomerWithPrice({
                    locationRefId: vm.salesinvoice.locationRefId,
                    customerRefId: vm.salesinvoice.customerRefId
                }).success(function (result) {
                    vm.refmaterial = result.items;
                }).finally(function () {
                    if (vm.salesinvoice.isDirectSalesInvoice && vm.salesinvoicePortions.length == 0)
                        vm.addPortion();
                    vm.detailloading = false;
                    vm.loading = false;
                });
            }

            vm.reftax = [];
            vm.fillDropDownTaxes = function () {
                suppliermaterialService.getSaleTaxForMaterial()
                    .success(function (result) {
                        vm.reftax = result;
                    }).finally(function () {

                    });
            }


            vm.getMaterialQuotePrice = function (data, supmaterial) {

                if (data.materialRefId > 0) {
                    if (vm.salesinvoice.id == null) {
                        data.price = parseFloat(supmaterial.materialPrice);

                        if (data.price == 0)
                            data.rateeditflag = true;
                        else
                            data.rateeditflag = vm.ratechangeallowedflag;
                    }
                    data.uom = supmaterial.uom;
                }
                return parseInt(supmaterial.materialRefId);
            };

            vm.addPortion = function () {

                var errorFlag = false;
                var value = vm.salesinvoicePortions[vm.salesinvoicePortions.length - 1];

                if (vm.sno > 0) {
                    var value = vm.salesinvoicePortions[vm.salesinvoicePortions.length - 1];
                    //value = value[0];

                    if (vm.existAllElements() == false)
                        return;

                    if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.materialRefName)) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MaterialRequired'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.totalQty) || value.totalQty == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidOrderQty'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.price) || value.price == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialPrice'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.totalAmount) || value.totalAmount == 0) {
                        errorFlag = true;
                        abp.notify.warn(app.localize('Amount'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.netAmount) || value.netAmount == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialTotalAmt'));
                        return;
                    }

                }

                vm.refunit = [];

                vm.sno = vm.sno + 1;

                vm.salesinvoicePortions.push({
                    'sno': vm.sno,
                    'dcTranid': 0,
                    'materialRefId': 0,
                    'materialRefName': '',
                    'totalQty': '',
                    'price': '',
                    'discountFlag': false,
                    'discountPercentage': 0,
                    'discountAmount': 0,
                    'totalAmount': 0,
                    'taxAmount': 0,
                    'netAmount': 0,
                    'remarks': '',
                    'taxForMaterial': null,
                    'uom': '',
                    'unitRefId': 0,
                    'changeunitflag': false,
                    'applicableTaxes': []
                });

                vm.calculateTotalVlaue(vm.salesinvoicePortions);
            }
            /////------------------------------------------

            vm.existAllElements = function () {
                $scope.existall = true;
                $scope.errmessage = "";

                if (vm.salesinvoice.customerRefId == null || vm.salesinvoice.customerRefId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('CustomerErr');
                    $('#selcustomerRefId').focus();
                }

                //if (vm.salesinvoice.salesinvoiceMode == null || vm.salesinvoice.salesinvoiceMode == 0) {
                //    $scope.errmessage = $scope.errmessage + app.localize('SalesInvoiceModeErr');
                //    $('#selInvMode').focus();
                //}

                if (vm.salesinvoice.invoiceDate == null) {
                    $scope.errmessage = $scope.errmessage + app.localize('InvoiceDateErr');
                    $('#invoicedate').focus();
                }

                if (vm.salesinvoice.accountDate == null) {
                    $scope.errmessage = $scope.errmessage + app.localize('AccountDateErr');
                    $('#invoicedate').focus();
                }

                //@@Pending -   Shall we put in App Setting for this
                //if (vm.invoice.accountDate < vm.invoice.invoiceDate) {
                //    $scope.errmessage = $scope.errmessage + app.localize('SalesInvoiceDateShouldNotGreaterThanAccountDate');
                //    $('#invoicedate').focus();
                //}

                if (vm.salesinvoice.invoiceNumber == null || vm.salesinvoice.invoiceNumber.length == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('InvoiceNumberErr');
                    $('#invoiceno').focus();
                }



                if (vm.salesinvoice.locationRefId == null || vm.salesinvoice.locationRefId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('LocationErr');
                    $('#defaultLocation').focus();
                }



                if ($scope.errmessage != "") {
                    $scope.existall = false;
                    abp.notify.warn($scope.errmessage, 'Required');
                    return false;
                }
                else
                    return true;

            };



            vm.getDetailMaterial = function () {
                vm.uilimit = null;
                if (vm.existAllElements() == false)
                    return;

                //vm.salesinvoicePortions = [];

                salesinvoiceService.getMaterialsFromDcId({
                    deliveryRefId: vm.tempsalesinvoicedetail.dcTranid,
                    customerRefId: vm.salesinvoice.customerRefId,
                    locationRefId: vm.defaultLocationId
                }).success(function (result) {

                    if (result.salesDcDetail.length > 0 && vm.salesinvoicePortions.length > 0) {
                        var lastelement = vm.salesinvoicePortions[vm.salesinvoicePortions.length - 1];
                        if (!(lastelement.materialRefId > 0 && lastelement.totalQty > 0)) {
                            vm.salesinvoicePortions.splice(vm.salesinvoicePortions - 1, 1);
                            vm.sno = vm.sno - 1;
                            if (vm.sno == 0)
                                vm.sno = 1;

                            vm.calculateTotalVlaue(vm.salesinvoicePortions);
                        }
                    }

                    angular.forEach(result.salesDcDetail, function (value, key) {

                        vm.salesinvoicePortions.push({
                            'sno': vm.sno,
                            'dcTranid': value.dcTranId,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'totalQty': value.totalQty,
                            'price': value.price,
                            'initialprice': value.price,
                            'discountFlag': value.discountFlag,
                            'discountPercentage': value.discountPercentage,
                            'discountAmount': parseFloat(vm.isUndefinedOrNull(value.discountAmount) ? 0 : value.discountAmount),
                            'totalAmount': parseFloat(parseFloat(value.totalAmount).toFixed(2)),
                            'taxAmount': parseFloat(parseFloat(value.salesTaxAmount).toFixed(2)),
                            'netAmount': parseFloat(parseFloat(value.netAmount).toFixed(2)),
                            'remarks': value.remarks,
                            'taxForMaterial': value.taxForMaterial,
                            'uom': value.uom,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'changeunitflag': false,
                            'applicableTaxes': value.applicableTaxes,
                            'rateeditflag': vm.ratechangeallowedflag,
                            'defaultUnitId': value.defaultUnitId,
                            'defaultUnitName': value.defaultUnitName
                        });

                        vm.sno = vm.sno + 1;

                        vm.calculateTotalVlaue(vm.salesinvoicePortions);

                        vm.refdctranid.some(function (value, key) {
                            if (parseInt(value.value) == parseInt(vm.tempsalesinvoicedetail.dcTranid)) {
                                vm.refdctranid.splice(key, 1);
                                return true;
                            }
                        });

                        vm.tempsalesinvoicedetail = [];

                    });
                });
            }


            vm.next = function () {
                if (vm.existAllElements() == false)
                    return;

                var value = vm.tempsalesinvoicedetail;

                var errorFlag = false;

                if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
                    errorFlag = true;
                    abp.notify.info(app.localize('MinimumOneDetail'));
                    return;
                }

                if (vm.isUndefinedOrNull(value.materialRefName) || value.materialRefName == 0) {
                    errorFlag = true;
                    abp.notify.info(app.localize('MaterialRequired'));
                    return;
                }

                if (vm.isUndefinedOrNull(value.totalQty) || value.totalQty == 0) {
                    errorFlag = true;
                    abp.notify.info(app.localize('InvalidSalesInvoiceQty'));
                    return;
                }
                if (vm.isUndefinedOrNull(value.price) || value.price == 0) {
                    errorFlag = true;
                    abp.notify.info(app.localize('InvalidMaterialPrice'));
                    return;
                }
                if (vm.isUndefinedOrNull(value.totalAmount) || value.totalAmount == 0) {
                    errorFlag = true;
                    abp.notify.info(app.localize('InvalidMaterialTotalAmt'));
                    return;
                }

                if (vm.isUndefinedOrNull(value.dcTranid)) {
                    errorFlag = true;
                    abp.notify.info(app.localize('PleaseSelectDCTranId'));
                    return;
                }

                if ($("#discFlag").is(":checked")) {
                    if (vm.isUndefinedOrNull(value.discountAmount) || value.discountAmount == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidDiscountAmt'));
                        return;
                    }
                }

                if (errorFlag) {
                    vm.saving = false;
                    vm.loading = false;
                    $scope.existall = false;
                }

                vm.sno = vm.sno + 1;
                var netamt = parseFloat(vm.isUndefinedOrNull(value.totalAmount) ? 0 : value.totalAmount) - parseFloat(vm.isUndefinedOrNull(value.discountAmount) ? 0 : value.discountAmount);

                vm.salesinvoicePortions.push({
                    'sno': vm.sno,
                    'dcTranid': value.dcTranid,
                    'materialRefId': value.materialRefId,
                    'materialRefName': value.materialRefName,
                    'totalQty': value.totalQty,
                    'price': value.price,
                    'discountFlag': value.discountFlag,
                    'discountPercentage': value.discountPercentage,
                    'discountAmount': parseFloat(vm.isUndefinedOrNull(value.discountAmount) ? 0 : value.discountAmount),
                    'totalAmount': parseFloat(parseFloat(value.totalAmount).toFixed(2)),
                    'taxAmount': parseFloat(parseFloat(value.taxAmount).toFixed(2)),
                    'netAmount': parseFloat(parseFloat(netAmount).toFixed(2)),
                    'remarks': value.remarks,
                    'taxForMaterial': value.taxForMaterial,
                    'uom': value.uom,
                    'unitRefId': value.unitRefId,
                    'changeunitflag': false,
                    'applicableTaxes': value.applicableTaxes
                });

                vm.tempsalesinvoicedetail = [];
                vm.calculateTotalVlaue(vm.salesinvoicePortions);
            };




            $scope.amtCalc = function (totalQty, price) {
                if (vm.isUndefinedOrNull(totalQty))
                    totalQty = 0;
                if (vm.isUndefinedOrNull(price))
                    price = 0;
                vm.tempsalesinvoicedetail.totalAmount = price * totalQty;
            }

            $scope.filterDcDate = function (val) {
                if (vm.salesinvoice.customerRefId > 0) {
                    if (moment(val).isValid()) {
                        if (vm.allDcShow) {
                            vm.salesinvoice.dcFromDate = null;
                            vm.salesinvoice.dcToDate = null;
                        }
                        else {
                            vm.salesinvoice.dcFromDate = moment(val.startDate).format('YYYY-MM-DD');
                            vm.salesinvoice.dcToDate = moment(val.endDate).format('YYYY-MM-DD');
                        }

                        vm.dateRangeModel.startDate = vm.salesinvoice.dcFromDate;
                        vm.dateRangeModel.endDate = vm.salesinvoice.dcToDate;

                        fillDcLists(vm.dateRangeModel);
                    }
                }
            }

            $scope.funcMat = function (val) {
                var forLoopFlag = true;


                angular.forEach(vm.salesinvoicePortions, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    vm.tempsalesinvoicedetail.materialRefId = 0;
                    vm.tempsalesinvoicedetail.materialRefName = '';
                    return;
                }
                vm.tempsalesinvoicedetail.materialRefName = val;
            };

            vm.refdctranid = [];

            function fillDcLists(daterange) {
                vm.uilimit = null;
                salesinvoiceService.setDcCompleteStatus({
                    id: vm.salesinvoice.customerRefId
                }).success(function (result) {
                    //abp.notify.info('Completed');
                });


                salesinvoiceService.getDcTranId($.extend({
                    allTimeDcs: vm.allTimeDcs,
                    customerRefId: vm.salesinvoice.customerRefId,
                    locationRefId: vm.defaultLocationId
                }, daterange)).success(function (result) {
                    vm.refdctranid = result.items;
                    // vm.reflocation.unshift({ value: "0", displayText: app.localize('NotAssigned') });
                });
            }

            $scope.checkPendingQty = function (enteredQty) {
                if (parseFloat(enteredQty) == parseFloat(vm.availableQty))
                    return;
                else if (parseFloat(enteredQty) > parseFloat(vm.availableQty)) {
                    abp.message.warn(app.localize("QtyErr", enteredQty, vm.availableQty), "Error");
                    vm.tempsalesinvoicedetail.totalQty = 0;
                    $("#totqty").focus();
                }
            }

            vm.getDcQty = function (dcitem) {
                vm.availableQty = parseFloat(dcitem.pendingQty);
                return parseInt(dcitem.materialRefId);
            };

            vm.refdcmaterial = [];

            vm.fillMaterialBasedOnInwardDC = function (selValue) {
                salesinvoiceService.getMaterialForInwardDc({ Id: selValue }).success(function (result) {
                    //vm.refdcmaterial = result.items;
                    vm.refdcmaterial = result;
                });
            }

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };




            vm.calculateTotalVlaue = function (data) {
                var tempTotDiscAmt = 0;
                var tempTotInvAmt = 0;
                var tempTotTaxAmt = 0;
                var tempTotalAmount = 0;

                angular.forEach(data, function (val, key) {
                    data[key].changeunitflag = false;
                    tempTotDiscAmt = parseFloat(tempTotDiscAmt) + parseFloat(vm.isUndefinedOrNull(val.discountAmount) ? 0 : val.discountAmount);
                    tempTotTaxAmt = parseFloat(tempTotTaxAmt) + parseFloat(parseFloat(vm.isUndefinedOrNull(val.taxAmount) ? 0 : val.taxAmount).toFixed(2));
                    tempTotInvAmt = parseFloat(tempTotInvAmt) + parseFloat(parseFloat(vm.isUndefinedOrNull(val.netAmount) ? 0 : val.netAmount).toFixed(2));
                    tempTotalAmount = parseFloat(tempTotalAmount) + parseFloat(parseFloat(vm.isUndefinedOrNull(val.totalAmount) ? 0 : val.totalAmount).toFixed(2));
                });
                vm.salesinvoiceTotalAmount = parseFloat(tempTotalAmount.toFixed(2));;
                vm.totalTaxAmount = parseFloat(tempTotTaxAmt.toFixed(2));

                if (vm.salesinvoice.totalDiscountAmount == null)
                    vm.salesinvoice.totalDiscountAmount = 0;

                if (vm.salesinvoice.totalShipmentCharges == null)
                    vm.salesinvoice.totalShipmentCharges = 0;

                if (vm.salesinvoice.roundedAmount == null)
                    vm.salesinvoice.roundedAmount = 0;

                if (vm.salesinvoice.totalDiscountAmount > tempTotInvAmt) {
                    abp.notify.warn("DiscountSalesInvoiceErr");
                    vm.salesinvoice.totalDiscountAmount = 0;
                }

                if (vm.salesinvoice.roundedAmount > tempTotInvAmt) {
                    abp.notify.warn("RoundedAmountSalesInvoiceErr");
                    vm.salesinvoice.roundedAmount = 0;
                }

                tempTotInvAmt = (tempTotInvAmt - parseFloat(vm.salesinvoice.totalDiscountAmount) + parseFloat(vm.salesinvoice.totalShipmentCharges) + parseFloat(vm.salesinvoice.roundedAmount));

                vm.salesinvoice.salesinvoiceAmount = parseFloat(tempTotInvAmt.toFixed(2));
                //vm.salesinvoice.totalDiscountAmount = parseFloat(tempTotDiscAmt.toFixed(2));
                //vm.salesinvoice.totalShipmentCharges = parseFloat(vm.salesinvoice.totalShipmentCharges.toFixed(2));
            }

            $scope.calcDiscountAmt = function (discperct, discamt) {
                if ($("#discFlag").is(":checked")) {
                    if (vm.isUndefinedOrNull(discperct))
                        discperct = 0;
                    if (vm.isUndefinedOrNull(discamt))
                        discamt = 0;

                    vm.tempsalesinvoicedetail.discountAmount = parseFloat($filter('number')(vm.tempsalesinvoicedetail.totalAmount * (discperct / 100), 2));
                }

            }

            vm.discFlagCheckBox = function () {
                if (!$("#discFlag").is(":checked")) {
                    vm.tempsalesinvoicedetail.discountAmount = 0;
                    vm.tempsalesinvoicedetail.discountPercentage = 0;
                }
            }

            $scope.calcDiscountPerc = function (discperct, discamt) {
                if ($("#discFlag").is(":checked")) {
                    if (vm.isUndefinedOrNull(discperct))
                        discperct = 0;
                    if (vm.isUndefinedOrNull(discamt))
                        discamt = 0;

                    vm.tempsalesinvoicedetail.discountPercentage = parseFloat($filter('number')((discamt / vm.tempsalesinvoicedetail.totalAmount) * 100, 2));
                }
            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex) {

                vm.salesinvoicePortions.splice(productIndex, 1);
                vm.sno = vm.sno - 1;
                vm.calculateTotalVlaue(vm.salesinvoicePortions);
                if (vm.salesinvoicePortions.length == 0)
                    vm.addPortion();

            }

            vm.inwardDcTranIdRef = function (selDcId) {
                if (selDcId > 0) {
                    vm.fillMaterialBasedOnInwardDC(selDcId);
                    //vm.getDetailMaterial();
                }
            }

            vm.save = function () {
                if ($scope.existall == false)
                    return;

                vm.salesinvoice.invoiceAmount = vm.salesinvoice.salesinvoiceAmount;

                if (vm.existAllElements() == false)
                    return;



                if (parseFloat(vm.salesinvoice.salesinvoiceAmount) == 0) {
                    abp.message.warn(app.localize("EmptyDetail"));
                    return;
                }

                //if (vm.checkAmountWithAuto() == false) {
                //    return false;
                //}

                //if (parseFloat(vm.salesinvoice.salesinvoiceamoutenteredbyuser) != parseFloat(vm.salesinvoice.salesinvoiceAmount)) {
                //    abp.message.warn(app.localize("SalesInvoiceAmtNotMatchedWithAutomaticCalculation"));
                //    return;
                //}

                vm.saving = true;
                vm.salesinvoicedetail = [];
                vm.discFlag = 0;

                errorFlag = false;
                var serialno = 1;
                angular.forEach(vm.salesinvoicePortions, function (value, key) {
                    if (value.discountFlag == true || value.discountFlag == 1)
                        vm.discFlag = 1;
                    else
                        vm.discFlag = 0;

                    if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.materialRefName)) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MaterialRequired'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.totalQty) || value.totalQty == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidOrderQty'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.price) || value.price == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialPrice'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.totalAmount) || value.totalAmount == 0) {
                        errorFlag = true;
                        abp.notify.warn(app.localize('Amount'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.netAmount) || value.netAmount == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialTotalAmt'));
                        return;
                    }


                    vm.salesinvoicedetail.push({
                        'sno': serialno++,
                        'materialRefId': value.materialRefId,
                        'totalQty': value.totalQty,
                        'price': value.price,
                        'discountFlag': vm.discFlag,
                        'discountPercentage': value.discountPercentage,
                        'discountAmount': value.discountAmount,
                        'totalAmount': value.totalAmount,
                        'salesTaxAmount': value.taxAmount,
                        'netAmount': value.netAmount,
                        'remarks': value.remarks,
                        'taxForMaterial': value.taxForMaterial,
                        'applicableTaxes': value.applicableTaxes,
                        'uom': value.uom,
                        'unitRefId': value.unitRefId,
                        'changeunitflag': false,
                    });
                });

                if (errorFlag == true) {
                    vm.saving = false;
                    return;
                }


                //salesinvoicedeliveryorderlink
                vm.salesinvoicedeliveryorderlink = [];
                if (vm.salesinvoice.isDirectSalesInvoice == false) {
                    angular.forEach(vm.salesinvoicePortions, function (value, key) {
                        vm.salesinvoicedeliveryorderlink.push({
                            'salesinvoiceRefId': 0,
                            'sno': value.sno,
                            'materialRefId': value.materialRefId,
                            'deliveryOrderRefId': value.dcTranid,
                            'materialConvertedQty': value.totalQty,
                        });
                    });
                }

                ////vm.salesinvoice.accountDate = vm.salesinvoice.salesinvoiceDate;
                vm.salesinvoice.invoiceMode = "CREDIT";


                salesinvoiceService.createOrUpdateSalesInvoice({
                    salesinvoice: vm.salesinvoice,
                    salesinvoicedetail: vm.salesinvoicedetail,
                    salesinvoiceDeliveryOrderLink: vm.salesinvoicedeliveryorderlink
                }).success(function (result) {
                    abp.notify.info('\' SalesInvoice \'' + app.localize('SavedSuccessfully'));
                    abp.message.success(app.localize('SalesInvoiceSuccessMessage', result.id));

                    salesinvoiceService.setDcCompleteStatus({
                        id: vm.salesinvoice.customerRefId
                    }).success(function (result) { });

                    //resetField();
                    vm.printSalesInvoice(result.id);
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.printSalesInvoice = function (objId) {
                $state.go('tenant.salesinvoiceprintdetail', {
                    printid: objId
                });
            };

            function resetField() {
                vm.salesinvoicePortions = [];
                vm.salesinvoicedetail = [];
                vm.tempsalesinvoicedetail = [];
                vm.invoiceDate = moment().format($scope.format);
                vm.salesinvoice.invoiceMode = '';
                vm.AccountDate = moment().format($scope.format)
                vm.salesinvoice.customerRefId = 0;
                vm.salesinvoice.invoiceNumber = '';
                vm.DcFromDate = moment().format($scope.format)
                vm.DcToDate = moment().format($scope.format)
                vm.salesinvoice.totalDiscountAmount = 0;
                vm.salesinvoice.totalShipmentCharges = 0;
                vm.salesinvoice.roundedAmount = 0;
                vm.salesinvoice.salesinvoiceAmount = 0;
                vm.dcTranid = '';
                vm.salesinvoice.salesinvoiceamoutenteredbyuser = "";
                vm.sno = 0;
            }

            //vm.checkAmountWithAuto = function () {
            //    vm.checkFinished = false;
            //    vm.calculateTotalVlaue(vm.salesinvoicePortions);

            //    if (parseFloat(vm.salesinvoice.salesinvoiceAmount) == 0) {
            //        abp.notify.warn(app.localize('EmptyDetail'))
            //        vm.checkFinished = false;
            //        return false;
            //    }

            //    if (parseFloat(vm.salesinvoice.salesinvoiceAmount) == parseFloat(vm.salesinvoice.salesinvoiceamoutenteredbyuser)) {
            //        vm.salesinvoice.checkingStatus = app.localize("MatchingCorrect");
            //        abp.notify.info(app.localize('MatchingCorrect'));
            //        vm.checkFinished = true;
            //        return true;
            //    }
            //    else {
            //        vm.salesinvoice.checkingStatus = app.localize("MatchingFailed");
            //        abp.notify.error(app.localize('MatchingFailed'));
            //        vm.checkFinished = false;
            //        return false;
            //    }
            //}

            vm.existall = function () {

                if (vm.salesinvoice.id == null) {
                    vm.existall = false;
                    return;
                }

                salesinvoiceService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.salesinvoice.id,
                    operation: 'SEARCH',
                    locationRefId: vm.defaultLocationId
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.salesinvoice.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.salesinvoice.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $state.go('tenant.salesinvoice');
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.reflocation = [];

            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;

                });
            }

            vm.refcustomer = [];

            function fillDropDownCustomer() {
                vm.loading = true;
                customerService.getCustomerForCombobox({}).success(function (result) {
                    vm.refcustomer = result.items;
                    vm.loading = false;
                });
            }

            vm.checkCloseDay = function () {
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                    vm.salesinvoice.accountDate = moment(result.accountDate).format($scope.format);

                    $('input[name="accountDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.salesinvoice.accountDate,
                        maxDate: vm.salesinvoice.accountDate,
                    });

                    $('input[name="invoiceDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.salesinvoice.accountDate,
                        minDate: $scope.minDate,
                        maxDate: moment()
                    });



                }).finally(function (result) {

                });
            }

            vm.loadingCount = 0;

            function init() {
                fillDropDownLocation();
                fillDropDownCustomer();
                vm.fillDropDownTaxes();
                //	vm.fillDropDownMaterial();
                vm.detailloading = true;
                vm.errorFlag = true;
                vm.loading = true;
                vm.loadingCount++;
                salesinvoiceService.getSalesInvoiceForEdit({
                    Id: vm.salesinvoiceId
                }).success(function (result) {
                    vm.errorFlag = false;
                    vm.salesinvoice = result.salesInvoice;
                    vm.salesinvoice.locationRefId = vm.defaultLocationId;
                    

                    var todayAsString = moment().format('YYYY-MM-DD');

                    vm.dateRangeModel = {
                        startDate: todayAsString,
                        endDate: todayAsString
                    };

                    $scope.filterDcDate(vm.dateRangeModel);

                    if (result.salesInvoice.id != null) {
                        vm.uilimit = null;
                        if (vm.salesinvoice.isDirectSalesInvoice == true) {
                            vm.salesinvoiceType = 'Direct';
                        }

                        vm.salesinvoice.accountDate = moment(result.salesInvoice.accountDate).format($scope.format);
                        vm.salesinvoice.invoiceDate = moment(result.salesInvoice.invoiceDate).format($scope.format);
                        $scope.minDate = vm.salesinvoice.invoiceDate;

                        vm.fillDropDownMaterial();

                        $('input[name="invoiceDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: moment(),
                            maxDate: moment()
                        });

                        vm.salesinvoicedetail = result.salesInvoiceDetail;
                        vm.salesinvoicedeliveryorderlink = result.salesInvoiceDeliveryOrderLink;
                        vm.refdctranid = result.salesInvoiceDeliveryOrderLink;

                        angular.forEach(vm.salesinvoicedetail, function (value, key) {
                            vm.salesinvoicePortions.push({
                                'sno': value.sno,
                                'dcTranid': value.dcTranId,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'totalQty': value.totalQty,
                                'price': value.price,
                                'discountFlag': value.discountFlag,
                                'discountPercentage': value.discountPercentage,
                                'discountAmount': parseFloat(vm.isUndefinedOrNull(value.discountAmount) ? 0 : value.discountAmount),
                                'totalAmount': parseFloat(parseFloat(value.totalAmount).toFixed(2)),
                                'taxAmount': parseFloat(parseFloat(value.taxAmount).toFixed(2)),
                                'netAmount': parseFloat(parseFloat(value.netAmount).toFixed(2)),
                                'remarks': value.remarks,
                                'taxForMaterial': value.taxForMaterial,
                                'uom': value.uom,
                                'unitRefId': value.unitRefId,
                                'changeunitflag': false,
                                'applicableTaxes': value.applicableTaxes
                            });
                        });
                        abp.ui.clearBusy('#salesinvoiceForm');
                        vm.calculateTotalVlaue(vm.salesinvoicePortions);
                        vm.salesinvoice.salesinvoiceamoutenteredbyuser = parseFloat(vm.salesinvoice.invoiceAmount).toFixed(2);
                        vm.checkAmountWithAuto();
                    }
                    else {
                        vm.salesinvoice.invoiceDate = moment().format($scope.format);
                        if (vm.salesinvoiceType == null || vm.salesinvoiceType == "")
                            vm.salesinvoice.isDirectSalesInvoice = false;
                        else
                            vm.salesinvoice.isDirectSalesInvoice = true;
                        vm.addPortion();
                        vm.salesinvoice.totalDiscountAmount = 0;
                        vm.salesinvoice.totalShipmentCharges = 0;
                        vm.salesinvoice.roundedAmount = 0;
                        vm.checkCloseDay();
                        vm.uilimit = 20;
                    }
                    vm.detailloading = false;
                }).finally(function () {
                    vm.loadingCount--;
                    if (vm.errorFlag == true)
                        vm.cancel();
                });


            }

            init();

        }
    ]);
})();

