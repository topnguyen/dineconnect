﻿(function () {
    appModule.controller('tenant.views.house.transaction.customersales.salesinvoice.invoiceSummary', [
        '$scope','$state', '$uibModal', 'uiGridConstants', 'abp.services.app.salesInvoice', 'abp.services.app.setup', 'appSession', 'abp.services.app.location', 'abp.services.app.customer',
        'abp.services.app.supplierMaterial',
        function ($scope,$state, $modal, uiGridConstants, salesinvoiceService, setupService, appSession, locationService, customerService, suppliermaterialService) {
            var vm = this;


            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.outputDtoList = null;


            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.defaultLocationId = appSession.user.locationRefId;

            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.isSalesAllowed = true;// appSession.location.isSalesAllowed;

            vm.requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize('Id'),
                        field: 'id'
                    },
                     {
                         name: app.localize('Customer'),
                         field: 'supplierRefName'
                     },
                    {
                        name: app.localize('InvoiceDate'),
                        field: 'invoiceDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD \''
                    },
                    {
                        name: app.localize('Number'),
                        field: 'invoiceNumber'
                    },
                    {
                        name: app.localize('Amount'),
                        field: 'invoiceAmount',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('DirectSalesInvoice'),
                        field: 'isDirectSalesInvoice',
                        cellTemplate:
                         '<div class=\"ui-grid-cell-contents\">' +
                         '  <span ng-show="row.entity.isDirectInvoice" class="label label-success">' + app.localize('Yes') + '</span>' +
                         '  <span ng-show="!row.entity.isDirectInvoice" class="label label-danger">' + app.localize('No') + '</span>' +
                         '</div>'
                    },
                    {
                        name: app.localize('AccountDate'),
                        field: 'accountDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD \''
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.customers = [];
            vm.getCustomers = function () {
                customerService.getAll({
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting
                }).success(function (result) {
                    vm.customers = $.parseJSON(JSON.stringify(result.items));

                }).finally(function (result) {
                });
            };

            vm.printInvoice = function (myObject) {
                $state.go('tenant.salesinvoiceprintdetail', {
                    printid: myObject.id
                });
            };

            vm.importSampleInvoice = function () {
                abp.message.confirm(
                    app.localize('ImportInwardWarning'),
                    function (isConfirmed) {
                        vm.loading = true;
                        if (isConfirmed) {
                            setupService.seedSampleInvoice()
                            .success(function () {
                                abp.notify.success(app.localize('SuccessfullyImported'));
                                vm.getAll();
                            }).finally(function () {
                                vm.loading = false;
                            });
                        }
                    });
            };

            vm.getAll = function () {
                vm.loading = true;
                salesinvoiceService.getSalesInvoiceSearch({
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    customerRefId:vm.customerRefId,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format)
                }).success(function(result){
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                    vm.outputDtoList = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.refcustomer = [];

            function fillDropDownCustomer() {
                vm.loading = true;
                customerService.getCustomerForCombobox({}).success(function (result) {
                    vm.refcustomer = result.items;
                    vm.loading = false;
                });
            }

      
            //@@Pending to Make Excel
            vm.exportToExcel = function () {
                salesinvoiceService.getSalesInvoiceSummaryAllToExcel({
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    customerRefId: vm.customerRefId,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format)
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            fillDropDownCustomer();
            
            vm.getCustomers();

        }
    ]);
})();