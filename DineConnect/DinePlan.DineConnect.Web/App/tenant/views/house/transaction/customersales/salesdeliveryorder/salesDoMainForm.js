﻿(function () {
    appModule.controller('tenant.views.house.transaction.customersales.salesdeliveryorder.salesDoMainForm', [
        '$scope', '$state', '$stateParams', 'abp.services.app.salesDeliveryOrder', 
			'abp.services.app.location', 'appSession', 'abp.services.app.customer', 'abp.services.app.supplierMaterial', 'abp.services.app.salesOrder', 'abp.services.app.material', 'abp.services.app.dayClose', 
        function ($scope, $state, $stateParams, salesdeliveryorderService, locationService, appSession,
					customerService, suppliermaterialService, salesorderService, materialService, daycloseService) {
            var vm = this;
            vm.saving = false;
            vm.salesdeliveryorder = null;
            vm.salesdeliveryorderdetail = null;
            $scope.existall = true;
            vm.sno = 0;
            vm.salesdeliveryorderId = $stateParams.id;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.uilimit = null;

            vm.checkCloseDay = function () {
							daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                    }

                    vm.salesdeliveryorder.deliveryDate = moment(result.accountDate).format($scope.format);
                    $scope.minDate = result.accountDate;

                    $('input[name="deliveryDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.salesdeliveryorder.deliveryDate,
                        minDate: $scope.minDate,
                        maxDate: vm.salesdeliveryorder.deliveryDate
                    });
                }).finally(function (result) {

                });
            }

            var requestParams = {
                skipCount: 0,
                maxResultCount: 5, // app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.salesdeliveryorderdetaildata = [];
            vm.tempdetaildata = [];

            vm.save = function (argoption) {
                if ($scope.existall == false)
                    return;
                vm.saving = true;
                
                vm.salesdeliveryorderdetaildata = [];
                angular.forEach(vm.tempdetaildata, function (value, key) {
                    vm.salesdeliveryorderdetaildata.push({
                        "deliveryRefId": 0,
                        "materialRefId": value.materialRefId,
                        "materialRefName": value.materialRefName,
                        "deliveryQty": value.deliveryQty,
                        "unitRefId": value.unitRefId,
                        "unitRefName" : value.unitRefName,
                        "materialReceivedStatus": 0,
                       // "isFractional": value.isFractional
                    });
                });

                salesdeliveryorderService.createOrUpdateSalesDeliveryOrder({
                    salesDeliveryOrder: vm.salesdeliveryorder,
                    salesDeliveryOrderDetail: vm.salesdeliveryorderdetaildata
                }).success(function (result) {
                    vm.transactionId = result.id;

                    abp.notify.info(app.localize('SalesDeliveryOrder') + app.localize('SavedSuccessfully'));
                    abp.message.success(app.localize('SalesCompleted', vm.transactionId))

                    if (argoption == 2)
                    {   
                        vm.cancel();
                        vm.saving = false;
                        return;
                    }
                    vm.salesdeliveryorderId = null;
                    vm.tempdetaildata = [];
                    vm.sno = 0;
                    init();
                    $("#selcustomerRefId").focus();

                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.removeRow = function (productIndex)
            {
                var element = vm.tempdetaildata[productIndex];

                if (vm.tempdetaildata.length > 1)  {
                    vm.tempdetaildata.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;

                }
                    
                else  {
                    abp.notify.info(app.localize('MinimumOneDetail'));
                    return;
                }

                vm.materialRefresh();
            }

            vm.addPortion = function () {
                if (vm.sno > 0) {

                    if (vm.existAllElements() == false)
                        return;

                    var lastelement = vm.tempdetaildata[vm.tempdetaildata.length - 1];

                    if (lastelement.materialRefName == null || lastelement.materialRefName == "") {
                        abp.notify.info(app.localize('MatRequired'));
                        $("#selMaterial1").focus();
                        return;
                    }

                    if (lastelement.deliveryQty == 0 || lastelement.deliveryQty == null || lastelement.deliveryQty == "") {
                        abp.notify.info(app.localize('QtyRequired'));
                        $("#rcdqty").focus();
                        return;
                    }

                    if (lastelement.unitRefId == 0 || lastelement.unitRefId == null) {
                        abp.notify.info(app.localize('UnitRequired'));
                        $("#selUnit").focus();
                        return;
                    }

                    if (!vm.extraMaterialCanBeReceived)
                    {
                        if(vm.tempRefMaterial.length == vm.tempdetaildata.length)
                        {
                            abp.notify.info(app.localize('NoMoreMaterial'));
                            return;
                        }
                    }
                    vm.removeMaterial(lastelement);
                }

                vm.sno = vm.sno + 1;
                vm.tempdetaildata.push({ 'deliveryRefId': 0, 'materialRefId': '', 'materialRefName': '', 'deliveryQty': '', 'unitRefId': 0, 'unitRefName': '', 'materialReceivedStatus': 0, 'id': 0, "isFractional": false });

                $("#selMaterial1").focus();
            }
            vm.portionLength = function () {
                return true;
            }

            vm.removeMaterial = function (objRemove) {
                var indexPosition = -1;
               angular.forEach(vm.tempRefMaterial, function (value, key) {
                   if (value.materialRefName == objRemove.materialRefName)
                       indexPosition = key;
               });

                if (indexPosition>=0) 
                    vm.tempRefMaterial.splice(indexPosition, 1);
                    
            }

            vm.materialRefresh = function (objRemove) {
                var indexPosition = -1;

                if (!vm.extraMaterialCanBeReceived)
                {
                    return;
                }
                {
                    vm.tempRefMaterial = vm.refmaterial;
                }

                angular.forEach(vm.tempdetaildata, function (valueinArray, keyinArray) {
                    indexPosition = -1;
                    var materialtoremove = valueinArray.materialRefName;
                    angular.forEach(vm.tempRefMaterial, function (value, key) {
                        if (value.materialRefName == valueinArray.materialRefName)
                            indexPosition = key;
                    });
                    if (indexPosition >= 0)
                        vm.tempRefMaterial.splice(indexPosition, 1);
                });

            }

            $scope.func = function (data, val) {          

                if (val.isQuoteNeededForPurchase == true)
                {
                    if(vm.salesdeliveryorder.soReferenceCode==null)
                    {
                        abp.message.warn(app.localize("MaterialCanBeReceiptOnlyThroughSO"));
                        data.materialRefId = 0;
                        data.materialRefName = "";
                        return;
                    }
                }

                var forLoopFlag = true;
                angular.forEach(vm.tempdetaildata, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialRefName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.materialRefId = 0;
                    data.materialRefName = '';
                    return;
                }

                data.materialRefName = val.materialRefName;
                data.isFractional = val.isFractional;
                data.unitRefName = val.uom;
                data.unitRefId = val.unitRefId;
                fillDropDownUnitBasedOnMaterial(data.materialRefId,data);
            };

            function fillDropDownUnitBasedOnMaterial(selectmaterialId,data) {
                vm.refunit = [];
                vm.tempRefUnit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    vm.tempRefUnit = vm.refunit;
                    if (vm.refunit.length == 1)
                        data.unitRefId = vm.refunit[0].value;
                });
            }

          
            vm.existAllElements = function () {
                $scope.existall = true;
                $scope.errmessage = "";

                if (vm.salesdeliveryorder.dcNumberGivenByCustomer == null || vm.salesdeliveryorder.dcNumberGivenByCustomer.length == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('SalesDoNumberErr');
                    $('#dcNumber').focus();
                }

                if (vm.salesdeliveryorder.customerRefId == null || vm.salesdeliveryorder.customerRefId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('CustomerErr');
                    $('#selcustomerRefId').focus();
                }

                if (vm.salesdeliveryorder.deliveryDate == null || vm.salesdeliveryorder.deliveryDate == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('DcDateErr');
                    $('#deliveryDate').focus();
                }

                if (vm.salesdeliveryorder.locationRefId == null || vm.salesdeliveryorder.locationRefId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('LocationErr');
                    $('#defaultLocation').focus();
                }

                if (vm.salesdeliveryorder.locationRefId == null || vm.salesdeliveryorder.locationRefId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('LocationErr');
                    $('#defaultLocation').focus();
                }

                
                if ($scope.errmessage != "") {
                    $scope.existall = false;
                    abp.notify.warn($scope.errmessage, 'Required');
                    return false;
                }
                else
                    return true;
               
            };

            vm.cancel = function () {
                $state.go("tenant.salesdeliveryorder");
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.dsetFocus = function (obj) {
                //console.log(obj);
            }

            vm.refcustomer = [];

            function fillDropDownCustomer()
            {
                vm.loading = true;
                customerService.getCustomerForCombobox({}).success(function (result) {
                    vm.refcustomer = result.items;
                    vm.loading = false;
                });
            }

            vm.refmaterial = [];
            vm.tempRefMaterial = [];

            vm.fillDropDownMaterial = function () {
                if (vm.salesdeliveryorder.customerRefId == null) {
                    vm.refmaterial = [];
                    vm.tempRefMaterial = [];
                }
                else {
									vm.loading = true;
									vm.uilimit = null;
                    suppliermaterialService.getMaterialBasedOnCustomerWithPrice({
												locationRefId: vm.defaultLocationId,
                        customerRefId: vm.salesdeliveryorder.customerRefId
										}).success(function (result) {
											if (result != null) {
												vm.refmaterial = result.items;
												vm.tempRefMaterial = result.items;
											}
											else {
												vm.refmaterial = [];
												vm.tempRefMaterial = [];
											}
                        vm.loading = false;
                    });
                }
            }

            vm.refunit = [];
            vm.tempRefUnit = [];

            function fillDropDownUnit() {
                materialService.getUnitForCombobox({}).success(function (result) {
                    vm.refunit = result.items;
                    vm.tempRefUnit = vm.refunit;
                });
            }


            vm.requestParams = {
                locations: '',
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.reflocation = [];
            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;
                 
                });
            }

            vm.refsoreferences = [];
            vm.fillDropDownSoReference = function () {
                vm.salesdeliveryorder.soReferenceCode = null;

                vm.loading = true;
                salesorderService.getSalesReferenceNumber({
                    customerRefId: vm.salesdeliveryorder.customerRefId,
                    locationRefId: vm.defaultLocationId
                }).success(function (result) {
                    vm.refsoreferences = result.items;
                    vm.loading = false;
                    if (vm.refsoreferences.length==1) {
                      
                    }
                });
            }

            vm.getpoid = function (item) {
                //vm.availableQty = parseFloat(dcitem.pendingQty);
                vm.salesdeliveryorder.soRefId = parseInt(item.id);
                return parseInt(item.id);
            };


            vm.extraMaterialCanBeReceived = abp.setting.getBoolean("App.House.ExtraMaterialReceivedInDc");

            vm.fillDataFromPo = function () {
                vm.tempdetaildata = [];
                vm.sno = 0;

                vm.loading = true;
                vm.uilimit = null;

                salesorderService.getSoDetailBasedOnSoReferenceCode({ Id: vm.salesdeliveryorder.soRefId }).success(function (result) {
                    
                    angular.forEach(result.salesOrderDetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.tempdetaildata.push({
                            "deliveryRefId": 0,
                            "materialRefId": value.materialRefId,
                            "materialRefName": value.materialRefName,
                            "deliveryQty": parseFloat(value.qtyOrdered) - parseFloat(value.qtyReceived),
                            "unitRefId": value.unitRefId,
                            "unitRefName": value.unitRefName,
                            "materialReceivedStatus": 0,
                            "isFractional" : value.isFractional
                        });
                    });


                    if (!vm.extraMaterialCanBeReceived)
                    {
                        var tempmaterial = [];

                        angular.forEach(vm.tempdetaildata, function (value, key) {
                                vm.refmaterial.some(function (matdata, matkey) {
                                    if (matdata.materialRefId == value.materialRefId) {
                                        tempmaterial.push(matdata);
                                        return true;
                                    }
                                });
                        });

                        vm.tempRefMaterial = tempmaterial;
                    }

                    if (vm.tempdetaildata.length==0)
                    {
                        vm.addPortion();
                    }
                    vm.loading = false;
                });
            }

            vm.tempdetaildata = [];


            function init() {
                abp.ui.setBusy('#sdoDetailForm');
                fillDropDownLocation();
                
                fillDropDownUnit();
                vm.errorFlag = true;

                vm.loading = true;
                salesdeliveryorderService.getSalesDeliveryOrderForEdit({
                    Id: vm.salesdeliveryorderId
                }).success(function (result) {
                    vm.errorFlag = false;
                    vm.salesdeliveryorder = result.salesDeliveryOrder;

                    vm.salesdeliveryorder.locationRefId = vm.defaultLocationId;
										fillDropDownCustomer();
                   
                    if (result.salesDeliveryOrder.id != null)
                    {
                        vm.uilimit = null;
                        vm.tempdetaildata = [];
                        vm.salesdeliveryorderdetail = result.salesDeliveryOrderDetail;
                        vm.ssno = 0;
                        angular.forEach(result.salesDeliveryOrderDetail, function (value, key) {
                            vm.tempdetaildata.push({
                                'deliveryRefId': value.deliveryRefId,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'deliveryQty': value.deliveryQty, 
                                'unitRefId' : value.unitRefId, 
                                'unitRefName' : value.unitRefName, 
                                'materialReceivedStatus': value.materialReceivedStatus,
                                'id': value.id,
                                "isFractional": value.isFractional
                            });
                        });

                        vm.salesdeliveryorder.deliveryDate = moment(result.salesDeliveryOrder.deliveryDate).format($scope.format)

                        $scope.minDate = moment(vm.salesdeliveryorder.deliveryDate).format($scope.format);

                        $('input[name="deliveryDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: vm.salesdeliveryorder.deliveryDate,
                            minDate: $scope.minDate,
                            maxDate: moment().format($scope.format)
                        });
                    }
                    else
                    {

                        vm.salesdeliveryorder.deliveryDate = moment().format($scope.format);
                        vm.checkCloseDay();
                        vm.addPortion();
                        vm.uilimit = 20;
                    }
                    vm.fillDropDownMaterial();
                }).finally(function () {
                    if (vm.errorFlag == true)
                        vm.cancel();
                    vm.loading = false;
                });
                abp.ui.clearBusy('#sdoDetailForm');
            }

            init();
        }
    ]);
})();   