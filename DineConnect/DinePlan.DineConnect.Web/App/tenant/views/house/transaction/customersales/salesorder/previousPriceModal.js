﻿(function () {
    appModule.controller('tenant.views.house.transaction.customersales.salesorder.previousPriceModal', [
        '$scope', '$uibModalInstance', 'materialRefId', 'materialRefName', 'uom', 'abp.services.app.salesInvoice',
        function ($scope, $modalInstance, materialRefId, materialRefName, uom, salesinvoiceService) {
            var vm = this;

            vm.loading = false;

            vm.previousPriceList = null;
            vm.materialRefId = materialRefId;
            vm.materialRefName = materialRefName;
            vm.uom = uom;
            vm.norecordsfound = false;

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            function init() {
                vm.loading = true;

                salesinvoiceService.getMaterialSales({
                    effectiveDate: moment(),
                    materialRefId: vm.materialRefId,
                    numberOfLastSalesDetail : 5
                }).success(function (result) {
                    vm.previousPriceList = result;
                    if (result.length == 0)
                        vm.norecordsfound = true;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            init();

        }
    ]);
})();