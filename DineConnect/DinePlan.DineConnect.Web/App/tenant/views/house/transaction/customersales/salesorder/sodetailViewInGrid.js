﻿
(function () {
    appModule.controller('tenant.views.house.transaction.customersales.salesorder.sodetailViewInGrid', [
        '$scope', '$uibModalInstance', 'uiGridConstants', 'abp.services.app.salesOrder', 'soId', 'appSession',
        function ($scope, $modalInstance, uiGridConstants, salesorderService, soId, appSession) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;

            vm.sorefnumber = null;
            vm.sodate = null;
            vm.manualCompleteAllowed = false;


            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('MaterialName'),
                        field: 'materialRefName',
                        width : 250
                    },
                    {
                        name: app.localize('Qty Ord'),
                        field: 'qtyOrdered',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Qty Rcd'),
                        field: 'qtyReceived',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Pending Qty'),
                        field: 'qtyPending',
                        cellClass : 'ui-ralign'
                    },
                   {
                       name: app.localize('UOM'),
                       field: 'unitRefName'
                   },
                    {
                        name: app.localize('Cost'),
                        field: 'price',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Total'),
                        field: 'totalAmount',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Tax'),
                        field: 'taxAmount',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('LineTotal'),
                        field: 'netAmount',
                        cellClass: 'ui-ralign'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.soreport = null;

            //vm.printso = function () {
            //    vm.loading = true;
            //    salesorderService.getSalesOrderReportInNotePad({
            //        skipCount: requestParams.skipCount,
            //        maxResultCount: requestParams.maxResultCount,
            //        sorting: requestParams.sorting,
            //        filter: vm.filterText,
            //        defaultLocationRefId: vm.defaultLocationId,
            //        salesOrderId: soId
            //    }).success(function (result) {
            //        vm.soreport = result;
            //        //$('#regReport').text(result);
            //    }).finally(function () {
            //        vm.loading = false;
            //    });

            //};


            vm.setPoAsCompletion = function (data) 
            {
                vm.loading = true;
                salesorderService.setCompleteSalesOrder({
                    soId: soId,
                    soRemarks : 'Manual Completion'
                }).success(function (result) {
                    vm.cancel();
                }).finally(function () {
                    vm.loading = false;
                });
            };
            

            vm.getAll = function() {
                vm.loading = true;
                salesorderService.getSalesOrderForEdit({
                    Id: soId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.salesOrderDetail.length;
                    vm.userGridOptions.data = result.salesOrderDetail;
                    if (result.salesOrderDetail.length > 0) {
                        vm.sorefnumber = result.salesOrder.soReferenceCode;
                        vm.sodate = result.salesOrder.soDate;
                        vm.locationRefName = result.salesOrder.locationRefName;
                        vm.shippingLocationRefName = result.salesOrder.shippingLocationRefName;
                        vm.supplierRefName = result.salesOrder.supplierRefName;
                        vm.soNetAmount = result.salesOrder.soNetAmount;
                        if (result.salesOrder.soCurrentStatus == 'H') {
                            vm.manualCompleteAllowed = true;
                        }

                    }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.hide = function() {
                vm.soreport = "";
            };

            vm.getAll();
        }]);
})();