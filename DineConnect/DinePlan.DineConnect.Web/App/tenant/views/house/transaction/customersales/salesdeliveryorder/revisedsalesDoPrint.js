﻿(function () {
    appModule.controller('tenant.views.house.transaction.customersales.salesdeliveryorder.revisedsalesDoPrint', [
       '$scope', '$state', '$stateParams', 'abp.services.app.salesDeliveryOrder',
        'abp.services.app.location', 'appSession', 'abp.services.app.supplierMaterial', 'abp.services.app.salesOrder', 'abp.services.app.material', 'abp.services.app.supplier',
        function ($scope, $state, $stateParams, salesdeliveryorderService, locationService, appSession,
            suppliermaterialService,salesorderService, materialService, supplierService) {

            var vm = this;
            vm.printmailflag = true;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;

            

            vm.salesdeliveryorderId = $stateParams.printid;

            vm.headerline = "~~~~~~~~~~~~~~~~~~~~";

            vm.sendMail = function () {
                vm.potext = '';
                vm.loading = true;
                salesdeliveryorderService.sendSalesOrderEmail(
                {
                    salesdeliveryorderId: vm.salesdeliveryorderId,
                    salesDeliveryOrderHTML: vm.potext
                }).success(function (result) {
                  
                    abp.message.success(app.localize('MailedSuccessfully'));
                    abp.notify.success(app.localize('MailedSuccessfully'));
                }).finally(function () {
                    vm.loading = false;
                    });
            }


            function myFunction() {
                document.getElementById("divprintemail").style.visibility = "hidden";
                myPrint();
            }

            function myPrint() {
                window.print();
                $state.go('tenant.salesdoprint', {
                    printid: vm.salesdeliveryorderId
                });
            }


            vm.getData = function () {

                salesdeliveryorderService.getPrintData({
                    Id: vm.salesdeliveryorderId
                }).success(function (result) {
                    vm.printresult = result;
                    
                    $("#printidtextHeader").html(result.headerText);
                    $("#printidtextBody").html(result.bodyText);
                    
                    myFunction();
                    });


            }

            vm.getData();


            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

        }]);
})();

