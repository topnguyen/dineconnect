﻿    (function () {
        appModule.controller('tenant.views.house.transaction.customersales.salesorder.fileDownload', [
     '$scope', '$uibModalInstance', 'uiGridConstants', 'abp.services.app.salesOrder', 'soId', 'appSession','$http',
        function ($scope, $modalInstance, uiGridConstants, salesorderService, soId, appSession,http) {

            var vm = this;
            
            vm.saving = false;
            vm.salesorder = null;
			$scope.existall = true;

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 function init() {
				    
                    salesorderService.getSalesOrderForEdit({
                        Id: soId
                    }).success(function (result) {
                          vm.salesorder = result.salesOrder;

                        //if(vm.salesorder.id != null){
                        //    if (vm.salesorder.fileName == null) {
                        //        vm.cancel();
                        //        alert("No File Is Uploaded To Download");
                                
                        //    }
                        //    else {

                         // $http({
                         //     method: 'POST',
                         //     url: '/DocumentUpload/DownloadPoUpload',
                         //     data: {
                         //         locationRefId: vm.salesorder.locationRefId,
                         //         supplierRefId: vm.salesorder.supplierRefId
                         //     }
                         // }).success(function (result) {

                         // }).error(function (error) {
                         //  //Showing error message 
                         //  $scope.status = 'Unable to connect' + error.message;
                         //});


                          var savePerson = function (person) {
                              return abp.ajax({
                                  url: '/DocumentUpload/DownloadSoUpload',
                                  data: JSON.stringify(person)
                              });
                          };

                        //Create a new person
                          var newPerson = {
                              locationRefId: vm.salesorder.locationRefId,
                              supplierRefId: vm.salesorder.supplierRefId
                          };

                          savePerson(newPerson).done(function (data) {
                              abp.notify.success('created new person with id = ' + data.personId);
                          });
                                
                          vm.importpath = abp.appPath + 'DocumentUpload/DownloadPoUpload?locationRefId=' + vm.salesorder.locationRefId
                                                                            + "&supplierRefId=" + vm.salesorder.supplierRefId;

                        //    } 
                        //}
                                          
                    });
			     }

			     init();

			  
        }
    ]);
})();

