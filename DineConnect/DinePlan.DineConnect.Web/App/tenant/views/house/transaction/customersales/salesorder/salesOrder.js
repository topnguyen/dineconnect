﻿(function () {
    appModule.controller('tenant.views.house.transaction.customersales.salesorder.salesOrder', [
        '$scope', '$uibModal', '$filter', '$timeout', '$state', '$stateParams', 'abp.services.app.salesOrder',
        'abp.services.app.commonLookup', 'appSession', 'abp.services.app.supplierMaterial', 'abp.services.app.template',
			'abp.services.app.location', 'abp.services.app.customer', 'abp.services.app.material', 'abp.services.app.unitConversion', 'abp.services.app.dayClose', 
        function ($scope, $modal, $filter, $timeout, $state, $stateParams, salesorderService, commonLookupService,
					appSession, suppliermaterialService, templateService, locationService, customerService, materialService, unitconversionService, daycloseService) {
            var vm = this;
            vm.reftax = [];
            vm.saving = false;
            vm.loadingflag = true;

            vm.loading = false;
            vm.auotSoloading = false;
            vm.soshowflag = true;

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");


            vm.salesorder = null;
            vm.salesOrderDetail = [];
            vm.currentUserId = abp.session.userId;
            vm.salesOrderTaxDetail = [];
            vm.salesDetailPortion = [];
            vm.selectedLocationName = "";
            vm.lastpriceshow = true;
            vm.uilimit = null;
            vm.refunit = [];

            vm.defaulttemplatetype = 6;
            vm.template = null;
            vm.selectedTemplate = null;

            vm.templatesave = null;

            vm.setShippingName = function (loc) {
                vm.selectedLocationName = loc.code;
                return parseInt(loc.id);
            }

            vm.permissions = {
                approve: abp.auth.hasPermission('Pages.Tenant.House.Transaction.SalesOrder.Approve')
            };

            $scope.existall = true;
            vm.salesorderId = $stateParams.id;
            vm.autoSoFlag = $stateParams.autoSoFlag;

            vm.sno = 0;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;

            $scope.locitems = [];

            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.dateOptions = [];
            vm.dateOptions.push({
                'singleDatePicker': true,
                'showDropdowns': true,
                'startDate': moment(),
                'minDate': $scope.minDate,
                'maxDate': moment()
            });

            //date start

            $scope.openDt = function ($event, index) {
                $('input[name=delDate' + index + ']').daterangepicker({
                    locale: {
                        format: $scope.format
                    },
                    singleDatePicker: true,
                    showDropdowns: true,
                    startDate: moment(),
                    minDate: $scope.minDate,
                });
            };

            $('input[name="soDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });
           
            $('input[name="dueDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
            });

            vm.existAllElements = function () {
                $scope.existall = true;
                $scope.errmessage = "";

                if (vm.salesorder.customerRefId == null || vm.salesorder.customerRefId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('CustomerErr');
                    $('#selcustomerRefId').focus();
                }

                if (vm.salesorder.soDate == null) {
                    $scope.errmessage = $scope.errmessage + app.localize('SoDateErr');
                    $('#soDate').focus();
                }

                if (vm.salesorder.locationRefId == null || vm.salesorder.locationRefId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('LocationErr');
                    $('#selLocationRefid').focus();
                }
                if (vm.salesorder.shippingLocationId == null || vm.salesorder.shippingLocationId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('ShippingLocationErr');
                    $('#selShipLocationRefid').focus();
                }

                if ($scope.errmessage != "") {
                    $scope.existall = false;
                    abp.notify.warn($scope.errmessage, 'Required');
                    return false;
                }
                else
                    return true;

            };

            vm.addPortion= function()
            {

                var errorFlag = false;

                

                var value = vm.salesDetailPortion[vm.salesDetailPortion.length-1];

                if (vm.sno > 0)
                {
                    var value = vm.salesDetailPortion[vm.salesDetailPortion.length - 1];
                    //value = value[0];

                    if (vm.existAllElements() == false)
                        return;

                    if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.materialRefName)) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MaterialRequired'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.qtyOrdered) || value.qtyOrdered == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidOrderQty'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.price) || value.price == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialPrice'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.totalAmount) || value.totalAmount == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialTotalAmt'));
                        return;
                    }
                    
                    var lastelement = value;

                    if (lastelement.defaultUnitId == lastelement.unitRefId) {
                        if (lastelement.qtyOrdered < lastelement.minimumOrderQuantity && lastelement.minimumOrderQuantity > 0) {
                            errorFlag = true;
                            abp.notify.warn(app.localize('CustomerMOQ', lastelement.qtyOrdered, lastelement.minimumOrderQuantity, lastelement.materialRefName, lastelement.unitRefName));
                            return;
                        }
                    }
                    else {
                        var convFactor = 0;
                        var convExistFlag = false;
                        vm.refconversionunit.some(function (data, key) {
                            if (data.baseUnitId == lastelement.defaultUnitId && data.refUnitId == lastelement.unitRefId) {
                                convFactor = data.conversion;
                                convExistFlag = true;
                                return true;
                            }
                        });


                        if (convExistFlag == false) {
                            abp.notify.info(app.localize('ConversionFactorNotExist', lastelement.defaultUnitName, lastelement.unitRefName));
                            return;
                        }

                        var curMoqConversion = lastelement.minimumOrderQuantity * convFactor;
                        if (lastelement.qtyOrdered < curMoqConversion && curMoqConversion > 0) {
                            errorFlag = true;
                            abp.notify.warn(app.localize('CustomerMOQ', lastelement.qtyOrdered, curMoqConversion, lastelement.materialRefName, lastelement.unitRefName));
                            return;
                        }
                    }

                  
                }
                vm.sno = vm.sno + 1;
                vm.temptaxinfolist = [];

                vm.temptaxinfolist.push({
                    'materialRefId': 0,
                    'taxRefId': 0,
                    'taxName': "",
                    'taxRate': 0,
                    'taxValue' : 0,
                });

                vm.refunit = [];
                vm.salesDetailPortion.push({
                    'sno': vm.sno,
                    'materialRefId': 0,
                    'materialRefName': '',
                    'qtyOrdered': '',
                    'defaultUnitId': 0,
                    'uom': '',
                    'unitRefId': '',
                    'unitRefName' : '',
                    'price': '',
                    'initialprice' : '',
                    'discountOption': '',
                    'discountValue': 0,
                    'taxAmount' : 0,
                    'totalAmount':0,
                    'netAmount': 0,
                    'remarks': '',
                    'currentInHand': 0,
                    'alreadyOrderedQuantity': 0,
                    'taxlist': vm.temptaxinfolist,
                    'applicableTaxes': [],
                    'changeunitflag': false,
                });

                vm.calculateTotalVlaue(vm.salesDetailPortion);
            }

            vm.calculateTotalVlaue = function (data) {
                var tempTotDiscAmt = 0;
                var tempTotSoNetAmt = 0;
                var tempTaxAmt = 0;
                var tempTotalAmt = 0;

                if (data.length == 0 || vm.isUndefinedOrNull(data))
                    return;

                angular.forEach(data, function (value,key)
                {
                    data[key].changeunitflag = false;
                    tempTotalAmt = parseFloat(tempTotalAmt) + parseFloat(vm.isUndefinedOrNull(value.totalAmount) ? 0 : parseFloat(value.totalAmount.toFixed(2)));
                    tempTaxAmt = parseFloat(tempTaxAmt) + parseFloat(vm.isUndefinedOrNull(value.taxAmount) ? 0 : parseFloat(value.taxAmount.toFixed(2)));
                    tempTotDiscAmt = parseFloat(tempTotDiscAmt) + parseFloat(vm.isUndefinedOrNull(value.discountValue) ? 0 : parseFloat( value.discountValue.toFixed(2)));
                    tempTotSoNetAmt = parseFloat(tempTotSoNetAmt) + parseFloat(vm.isUndefinedOrNull(value.netAmount) ? 0 : parseFloat(value.netAmount.toFixed(2)));
                    
                });
               
                vm.poTotalAmount = parseFloat(tempTotalAmt.toFixed(2));
                vm.poTaxAmount = parseFloat(tempTaxAmt.toFixed(2));
                vm.salesorder.poNetAmount = parseFloat(tempTotSoNetAmt.toFixed(2));
            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex) {
                if (vm.salesDetailPortion.length > 1) {
                    vm.salesDetailPortion.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                }
                else {
                    vm.salesDetailPortion.splice(productIndex, 1);
                    vm.sno = 0;
                    vm.addPortion();
                    return;
                }
            }

            $scope.func = function (data, val, obj) {
                var forLoopFlag = true;

                angular.forEach(vm.salesDetailPortion, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialRefName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag)
                {
                    data.materialRefId = 0;
                    data.materialRefName = '';
                    return;
                }
                data.materialRefName = val.materialRefName;
                data.alreadyOrderedQuantity = val.alreadyOrderedQuantity;
                data.currentInHand = val.currentInHand;
                data.uom = val.uom;
                data.defaultUnitId = val.defaultUnitId;
                data.unitRefName = val.uom;
                data.unitRefId = val.unitRefId;
                data.minimumOrderQuantity = val.minimumOrderQuantity;
                data.price = parseFloat(val.materialPrice);
                data.initialprice = val.materialPrice;

                vm.temptaxinfolist = [];

                angular.forEach(val.taxForMaterial, function (value, key) {
                    vm.temptaxinfolist.push({
                        'materialRefId': value.materialRefId,
                        'taxRefId': value.taxRefId,
                        'taxName' : value.taxName,
                        'taxRate': value.taxRate,
                        'taxValue': value.taxValue,
                        'rounding': value.rounding,
                        'sortOrder': value.sortOrder,
                        'taxCalculationMethod' : value.taxCalculationMethod
                    });
                });
                data.taxlist = vm.temptaxinfolist;
                data.applicableTaxes = val.applicableTaxes;


            };

            $scope.taxCalc = function (data, totalQty, price) {
                if (vm.isUndefinedOrNull(totalQty))
                    totalQty = 0;
                if (vm.isUndefinedOrNull(price))
                    price = 0;

                data.totalAmount = parseFloat(price) * parseFloat(totalQty);
                data.totalAmount = parseFloat(data.totalAmount.toFixed(2));

                data.taxAmount = 0;
                data.taxlist = [];

                taxneedstoremove = [];

                if (data.applicableTaxes.length > 0)    //  Calculate Tax 
                {
                    angular.forEach(data.applicableTaxes, function (value, key) {
                        var taxCalculationOnValue = 0.0;

                        if (value.taxCalculationMethod == 'GT' || value.taxCalculationMethod == 'ST') {
                            taxCalculationOnValue = data.totalAmount;
                        }
                        else {
                            var taxrefvalueflag = false;
                            data.taxlist.some(function (taxdata, taxkey) {
                                if (taxdata.taxName == value.taxCalculationMethod) {
                                    taxCalculationOnValue = taxdata.taxValue;
                                    taxrefvalueflag = true;
                                    return true;
                                }
                            });

                            if (taxrefvalueflag == false)
                                taxneedstoremove.push({ 'taxName': value.taxName });
                        }

                        if (taxCalculationOnValue > 0) {
                            var taxamt = parseFloat(taxCalculationOnValue * value.taxRate / 100);
                            taxamt = parseFloat(taxamt.toFixed(2));
                            data.taxAmount = parseFloat(data.taxAmount) + parseFloat(taxamt);
                            data.taxAmount = parseFloat(data.taxAmount.toFixed(2));

                            if (taxamt > 0) {
                                data.taxlist.push
                                ({
                                    'materialRefId': data.materialRefId,
                                    'taxRefId': value.taxRefId,
                                    'taxName': value.taxName,
                                    'taxRate': value.taxRate,
                                    'taxValue': taxamt,
                                    'rounding': value.rounding,
                                    'sortOrder': value.sortOrder,
                                    'taxCalculationMethod': value.taxCalculationMethod
                                });
                            }
                        }
                    });
                }


                angular.forEach(taxneedstoremove, function (value, key) {
                    angular.forEach(data.applicableTaxes, function (detvalue, detkey) {
                        if (value.taxName == detvalue.taxName)
                            data.applicableTaxes.splice(detkey, 1);
                    });
                });

                data.netAmount = parseFloat(parseFloat(data.totalAmount).toFixed(2)) + parseFloat(parseFloat(data.taxAmount).toFixed(2));
                vm.calculateTotalVlaue(vm.salesDetailPortion);
            }


            $scope.amtCalc = function (data,totalQty, price) {
                if (vm.isUndefinedOrNull(totalQty))
                    totalQty = 0;
                if (vm.isUndefinedOrNull(price))
                    price = 0;

                data.totalAmount = parseFloat(price) * parseFloat(totalQty);
                data.totalAmount = parseFloat(data.totalAmount.toFixed(2));
          
                data.taxAmount = 0;

                if (data.taxlist.length > 0)    //  Calculate Tax 
                {
                    angular.forEach(data.taxlist, function (value, key) {
                        var taxCalculationOnValue = 0.0;

                        if(value.taxCalculationMethod == 'GT' || value.taxCalculationMethod == 'ST')
                        {
                            taxCalculationOnValue= data.totalAmount;
                        }
                        else
                        {
                            data.taxlist.some(function (taxdata, taxkey) {
                                if (taxdata.taxName == value.taxCalculationMethod) {
                                    taxCalculationOnValue = taxdata.taxValue;
                                    return true;
                                }
                            });
                        }
                        var taxamt = parseFloat(taxCalculationOnValue * value.taxRate / 100);
                        taxamt = parseFloat(taxamt.toFixed(2));
                        data.taxAmount = parseFloat(data.taxAmount) + parseFloat(taxamt);
                        data.taxAmount = parseFloat(data.taxAmount.toFixed(2));
                        data.taxlist[key].taxValue = taxamt;
                        data.taxlist[key].materialRefId = data.materialRefId;
                    });
                }

                data.netAmount = parseFloat(parseFloat(data.totalAmount).toFixed(2)) + parseFloat(parseFloat(data.taxAmount).toFixed(2));
                vm.calculateTotalVlaue(vm.salesDetailPortion);
            }

            $scope.discOptionCheck=function(data)
            {
                data.discountValue = 0;
                $scope.calcDiscountPerc(data);
            }

            $scope.calcDiscountPerc = function (data)
            {
                var discamt = data.discountValue;
                if (parseFloat(discamt) == 0) {
                    $scope.amtCalc(data, data.totalQty, data.price);
                    return;
                }

                var totalQty = 0;
                var price = 0;
                if (vm.isUndefinedOrNull(data.qtyOrdered))
                    totalQty = 0;
                else
                    totalQty = parseFloat(data.qtyOrdered);
                if (vm.isUndefinedOrNull(data.price))
                    price = 0;
                else
                    price = parseFloat( data.price);

                if (totalQty == 0 || price == 0) {
                    abp.message.warn(app.localize("QtyOrderedOrPriceShouldNotBeZero"));
                    return;
                }

                var totAmt =  price * totalQty;
                if (data.discountOption)
                {
                    if (vm.isUndefinedOrNull(discamt))
                        discamt = 0;
                    discamt = parseFloat($filter('number')(discamt));

                    if (discamt > 100)
                    {
                        abp.message.warn(app.localize("DiscountPercentageWrong"));
                        return;
                    }

                    var disccal = totAmt - (totAmt * (discamt/100));
                    data.netAmount = parseFloat(data.totalAmount) -  parseFloat((disccal));
                }
                else
                {
                    if (vm.isUndefinedOrNull(discamt))
                        discamt = 0;

                    discamt = parseFloat($filter('number')(discamt));

                    var disccal = totAmt - discamt;
                    data.netAmount = parseFloat(data.totalAmount) - parseFloat((disccal));
                  
                }

                vm.calculateTotalVlaue(vm.salesDetailPortion);
            }

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };


            vm.save = function (argSaveOption) {
                if ($scope.existall == false)
                    return;
                

                if (vm.existAllElements() == false)
                    return;

                if (vm.salesorder.poNetAmount == 0)
                {
                    abp.message.warn(app.localize("EmptyDetail"));
                    return;
                }

                if (vm.templatesave.templateflag == true && vm.templatesave.templatename.length == 0) {
                    abp.message.warn(app.localize("TemplateNameRequired"));
                    return;
                }

                vm.saving = true;

                vm.salesOrderDetail = [];
                vm.discFlag = 0;

                vm.errorflag = false;
                angular.forEach(vm.salesDetailPortion, function (value, key)
                {
                    if (value.discountOption == true || value.discountOption == 1)
                        vm.discFlag = "P";
                    else
                        vm.discFlag = "F";

                    if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
                        vm.errorflag = true;
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.materialRefName)) {
                        vm.errorflag = true;
                        abp.notify.info(app.localize('MaterialRequired'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.qtyOrdered) || value.qtyOrdered == 0) {
                        vm.errorflag = true;
                        abp.notify.info(app.localize('InvalidOrderQty'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.price) || value.price == 0) {
                        vm.errorflag = true;
                        abp.notify.info(app.localize('InvalidMaterialPrice'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.totalAmount) || value.totalAmount == 0) {
                        vm.errorflag = true;
                        abp.notify.info(app.localize('InvalidMaterialTotalAmt'));
                        return;
                    }

                    var lastelement = value;

                    if (lastelement.defaultUnitId == lastelement.unitRefId) {
                        if (lastelement.qtyOrdered < lastelement.minimumOrderQuantity && lastelement.minimumOrderQuantity > 0) {
                            vm.errorFlag = true;
                            abp.notify.warn(app.localize('CustomerMOQ', lastelement.qtyOrdered, lastelement.minimumOrderQuantity, lastelement.materialRefName, lastelement.unitRefName));
                            return;
                        }
                    }
                    else {
                        var convFactor = 0;
                        var convExistFlag = false;
                        vm.refconversionunit.some(function (data, key) {
                            if (data.baseUnitId == lastelement.defaultUnitId && data.refUnitId == lastelement.unitRefId) {
                                convFactor = data.conversion;
                                convExistFlag = true;
                                return true;
                            }
                        });


                        if (convExistFlag == false) {
                            abp.notify.info(app.localize('ConversionFactorNotExist', lastelement.defaultUnitName, lastelement.unitRefName));
                            vm.errorflag = true;
                            return;
                        }

                        var curMoqConversion = lastelement.minimumOrderQuantity * convFactor;
                        if (lastelement.qtyOrdered < curMoqConversion && curMoqConversion > 0) {
                            vm.errorflag = true;
                            abp.notify.warn(app.localize('CustomerMOQ', lastelement.qtyOrdered, curMoqConversion, lastelement.materialRefName, lastelement.unitRefName));
                            return;
                        }
                    }

                    vm.salesOrderDetail.push({
                        'sno': value.sno,
                        'materialRefId': value.materialRefId,
                        'materialRefName': value.materialRefName,   
                        'qtyOrdered': value.qtyOrdered,
                        'defaultUnitId': value.defaultUnitId,
                        'uom': value.uom,
                        'unitRefId' : value.unitRefId,
                        'unitRefName': value.unitRefName,
                        'price': value.price,
                        'discountOption': vm.discFlag,
                        'discountValue': value.discountValue,
                        'totalAmount': value.totalAmount,
                        'taxAmount': value.taxAmount,
                        'netAmount': value.netAmount,
                        'remarks': value.remarks,
                        'taxForMaterial' : value.taxlist
                    });
                });

                if (vm.errorflag == true)
                {
                    vm.saving = false;
                    return;
                }

                if (vm.templatesave.templatename != null)
                    vm.templatesave.templatename = vm.templatesave.templatename.toUpperCase();


                salesorderService.createOrUpdateSalesOrder({
                    salesOrder: vm.salesorder,
                    salesOrderDetail: vm.salesOrderDetail,
                    templateSave: vm.templatesave,
                    statusToBeAssigned: argSaveOption
                }).success(function (result) {
                    abp.notify.info('\' SalesOrder \'' + app.localize('SavedSuccessfully'));
                    abp.message.success(app.localize('SOSuccessMessage', result.soReferenceCode));
                    resetField();
                    if (argSaveOption == 'A') {
                        vm.printSalesOrder(result.id);
                    }
                    else {
                        vm.cancel();
                    }
                  
                    //vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.printSalesOrder = function (poid) {
                $state.go('tenant.salesorderprint', {
                    printid: poid
                });
            };

            vm.draft = function () {
                if ($scope.existall == false)
                    return;
                vm.saving = true;

                vm.templatesave.templateflag = true;
                vm.templatesave.templatename = app.localize("LastSoDraft");

                vm.salesOrderDetail = [];
                vm.discFlag = 0;

                angular.forEach(vm.salesDetailPortion, function (value, key) {
                    if (value.discountOption == true || value.discountOption == 1)
                        vm.discFlag = "P";
                    else
                        vm.discFlag = "F";

                    vm.salesOrderDetail.push({
                        'sno': value.sno,
                        'materialRefId': value.materialRefId,
                        'qtyOrdered': value.qtyOrdered,
                        'defaultUnitId': value.defaultUnitId,
                        'uom': value.uom,
                        'unitRefId': value.unitRefId,
                        'unitRefName' : value.unitRefName,
                        'price': value.price,
                        'discountOption': vm.discFlag,
                        'discountValue': value.discountValue,
                        'totalAmount': value.totalAmount,
                        'taxAmount': value.taxAmount,
                        'netAmount': value.netAmount,
                        'remarks': value.remarks
                    });
                });

                if (vm.templatesave.templatename != null)
                    vm.templatesave.templatename = vm.templatesave.templatename.toUpperCase();

                vm.salesorder.soReferenceCode = vm.selectedLocationName + '/1';

                salesorderService.createDraft({
                    salesOrder: vm.salesorder,
                    salesOrderDetail: vm.salesOrderDetail,
                    templateSave: vm.templatesave
                }).success(function () {
                    abp.notify.info('\' SalesOrder \'' + app.localize('DraftedSuccessfully'));
                    resetField();
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };


            function resetField() {
                vm.sno = 0;
                vm.salesorder = [];
                vm.salesOrderDetail = [];
                vm.salesOrderTaxDetail = [];
                vm.salesDetailPortion = [];
                vm.salesorder.locationRefId = vm.defaultLocationId;
            }

            vm.cancel = function () {
                //$modalInstance.dismiss();
                $state.go('tenant.salesorder');
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.reflocations = [];

            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;
                 
                });
            }

            vm.refcustomer = [];

            function fillDropDownCustomer() {
                vm.loading = true;
                customerService.getCustomerForCombobox({}).success(function (result) {
                    vm.refcustomer = result.items;
                    vm.loading = false;
                });
            }

            vm.refconversionunit = [];
            vm.fillUnitConversion = function () {
                unitconversionService.getAll({
                }).success(function (result) {
                    vm.refconversionunit = result.items;
                });
            }

            vm.fillUnitConversion();


            vm.refmaterial = [];
            vm.fillDropDownMaterial = function () {
                vm.loading = true;
                suppliermaterialService.getMaterialBasedOnCustomerWithPrice({
                    locationRefId: vm.defaultLocationId,
                    customerRefId: vm.salesorder.customerRefId,
                    soRefId : vm.salesorder.id
                }).success(function (result) {
                    vm.refmaterial = result.items;
                    
                }).finally(function () {
                    vm.templist = [];

                    angular.forEach(vm.salesDetailPortion, function (value, key) {
                        vm.discFlag = null;
                        if (value.discountOption == 'F')
                            vm.discFlag = false;
                        else if (value.discountOption == 'P')
                            vm.discFlag = true;

                        vm.temponhand = 0;
                        vm.temponorder = 0;
                        vm.matname = "";

                        vm.refmaterial.some(function (matvalue, matkey) {
                            if (matvalue.materialRefId == value.materialRefId) {
                                vm.temponhand = matvalue.currentInHand;
                                vm.temponorder = matvalue.alreadyOrderedQuantity;
                                vm.matname = matvalue.materialRefName;
                                return true;
                            }
                        });

                        vm.templist.push({
                            'sno': value.sno,
                            'materialRefId': value.materialRefId,
                            'materialRefName': vm.matname,
                            'qtyOrdered': value.qtyOrdered,
                            'defaultUnitId': value.defaultUnitId,
                            'uom': value.uom,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'price': value.price,
                            'initialprice' : value.price,
                            'discountOption': vm.discFlag,
                            'discountValue': value.discountValue,
                            'totalAmount' : value.totalAmount,
                            'taxAmount': value.taxAmount,
                            'netAmount': value.netAmount,
                            'remarks': value.remarks,
                            'currentInHand': vm.temponhand,
                            'alreadyOrderedQuantity': vm.temponorder,
                            'taxlist': value.taxlist,
                            'applicableTaxes' :value.applicableTaxes
                        });
                    });

                    vm.salesDetailPortion = vm.templist;
                    vm.loadingflag == false;
                    vm.loading = false;
                });
            }

            vm.reftax = [];
            vm.fillDropDownTaxes = function () {
                suppliermaterialService.getTaxForMaterial()
                .success(function(result) {
                    vm.reftax = result;
                }).finally(function() {
                
                });
            }


            vm.getMaterialQuotePrice = function (data, supmaterial) {
                if (data.materialRefId > 0) {
                    data.price = parseFloat(supmaterial.materialPrice);
                    data.initialprice = data.price;
                    data.uom = supmaterial.uom;
                    data.unitRefName = supmaterial.uom,
                    data.unitRefId = supmaterial.unitRefId
                }
                return parseInt(supmaterial.materialRefId);

                //if (vm.isUndefinedOrNull(supmaterial.materialPrice))
                //{
                //    return data.materialRefId;
                //}
                //if (data.materialRefId > 0)  {
                //    data.price = parseFloat(supmaterial.materialPrice);
                //    data.initialprice = data.price;
                //    data.uom = supmaterial.uom;
                //}
                //else {
                //    return data.materialRefId;
                //}
                //return parseInt(supmaterial.materialRefId);
            };

            vm.refShippingLocation = [];
            function fillDropDownShippingLocation()
            {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.refShippingLocation = $.parseJSON(JSON.stringify(result.items));
                    if (vm.refShippingLocation.length==1)
                    {
                        vm.salesorder.shippingLocationId = vm.refShippingLocation[0].id;
                        vm.selectedLocationName = vm.refShippingLocation[0].code;
                    }
                }).finally(function (result) {
                });
            }

            vm.refTemplateDetail = [];
            vm.reftemplate = [];

            function fillDropDownTemplate() {
                vm.loading = true;
                templateService.getTemplateDetail({
                    locationRefId: vm.defaultLocationId,
                    templateType : vm.defaulttemplatetype
                }).success(function (result) {
                    vm.refTemplateDetail = result.items;

                    angular.forEach(vm.refTemplateDetail, function (value, key) {
                        vm.reftemplate.push({
                            'value': value.id,
                            'displayText': value.name,
                        });
                    });
                    vm.loading = false;
                });
            }

            vm.fillDataFromTemplate = function () {
                vm.autoSoFlag = false;
                vm.autoSoDetails = [];

                vm.refTemplateDetail.some(function (value, key) {
                    if (value.id == vm.template) {
                        vm.selectedTemplate = value.templateData;
                        return true;
                    }
                });

                vm.loading = true;

                salesorderService.getTemplateObjectForEdit({
                    objectString : vm.selectedTemplate
                }).success(function (result) {

                vm.salesorder = result.salesOrder;
                vm.salesOrderDetail = result.salesOrderDetail;

                $scope.dt = moment().format($scope.format);
                vm.salesorder.soDate = moment().format($scope.format);
                vm.salesorder.deliveryDateExpected = moment().format($scope.format);
                vm.salesorder.locationRefId = vm.defaultLocationId;

                $('input[name="soDate"]').daterangepicker({
                    locale: {
                        format: $scope.format
                    },
                    singleDatePicker: true,
                    showDropdowns: true,
                    startDate: vm.salesorder.soDate,
                    minDate: $scope.minDate,
                    maxDate: moment().format($scope.format)
                });

                $('input[name="dueDate"]').daterangepicker({
                    locale: {
                        format: $scope.format
                    },
                    singleDatePicker: true,
                    showDropdowns: true,
                    startDate: vm.salesorder.deliveryDateExpected,
                    minDate: $scope.minDate,
                });

                vm.salesDetailPortion = [];

                angular.forEach(result.salesOrderDetail, function (value, key)
                {
                    vm.discFlag = null;
                    if (value.discountOption == 'F')
                        vm.discFlag = false;
                    else if (value.discountOption == 'P')
                        vm.discFlag = true;

                    vm.temponhand = 0;
                    vm.temponorder = 0;

                    vm.salesDetailPortion.push({
                        'sno': value.sno,
                        'materialRefId': value.materialRefId,
                        'materialRefName': value.materialRefName,
                        'qtyOrdered': value.qtyOrdered,
                        'defaultUnitId' : value.defaultUnitId,
                        'uom': value.uom,
                        'unitRefId': value.unitRefId,
                        'unitRefName' : value.unitRefName,
                        'price': value.price,
                        'initialprice' : value.price,
                        'discountOption': vm.discFlag,
                        'discountValue': value.discountValue,
                        'totalAmount': value.totalAmount,
                        'taxAmount': value.taxAmount,
                        'netAmount': value.netAmount,
                        'remarks': value.remarks,
                        'currentInHand': vm.temponhand,
                        'alreadyOrderedQuantity': vm.temponorder,
                        'taxlist': value.taxForMaterial,
                        'applicableTaxes': value.applicableTaxes,
                        'changeunitflag': false,
                    });
                });
                vm.loading = false;
                vm.fillDropDownMaterial();

                vm.calculateTotalVlaue(vm.salesDetailPortion);
        });
               
            }

            vm.checkCloseDay = function () {
							daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {

                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                    vm.salesorder.soDate = moment(result.accountDate).format($scope.format);
                    vm.salesorder.deliveryDateExpected = moment(result.accountDate).format($scope.format);
                    $scope.minDate = result.accountDate;

                    $('input[name="soDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.salesorder.soDate,
                        minDate: $scope.minDate,
                        maxDate: vm.salesorder.soDate
                    });

                    $('input[name="dueDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.salesorder.deliveryDateExpected,
                        minDate: $scope.minDate,
                    });

                }).finally(function (result) {

                });
            }

            function init() {

                fillDropDownLocation();
                fillDropDownCustomer();
                vm.fillDropDownTaxes();
                fillDropDownTemplate();

                if (vm.autoSoFlag)
                {
                    //vm.fillAutoSoDetails();
                    vm.soshowflag = false;
                }
                else
                {
                    vm.soshowflag = true;
                }

                salesorderService.getSalesOrderForEdit({
                    Id: vm.salesorderId
                }).success(function (result) {
                    vm.salesorder = result.salesOrder;
                    vm.salesOrderDetail = result.salesOrderDetail;

                    vm.salesorder.locationRefId = vm.defaultLocationId;

                    vm.templatesave = result.templatesave;

                    vm.templatesave.templateflag = false;
                    vm.templatesave.templatename = null;
                    vm.templatesave.templatescope = false;

                    fillDropDownShippingLocation();

                    $scope.dt = moment().format($scope.format);
                    if (result.salesOrder.id != null)
                    {
                        vm.uilimit = null;
                        vm.salesorder.soDate = moment(result.salesOrder.soDate).format($scope.format);
                        vm.salesorder.deliveryDateExpected = moment(result.salesOrder.deliveryDateExpected).format($scope.format);
                        $scope.minDate = vm.salesorder.soDate;

                        $('input[name="soDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: vm.salesorder.soDate,
                            minDate: $scope.minDate,
                            maxDate: moment().format($scope.format)
                        });

                        $('input[name="dueDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: vm.salesorder.deliveryDateExpected,
                            minDate: $scope.minDate,
                        });

                        vm.salesDetailPortion = [];

                        angular.forEach(vm.salesOrderDetail, function (value, key)
                        {
                            vm.discFlag = null;
                            if (value.discountOption == 'F')
                                vm.discFlag = false;
                            else if (value.discountOption == 'P')
                                vm.discFlag = true;
                            vm.salesDetailPortion.push({
                                'sno': value.sno,
                                'materialRefId': value.materialRefId,
                                'materialRefName' : value.materialRefName,
                                'qtyOrdered': value.qtyOrdered,
                                'defaultUnitId': value.defaultUnitId,
                                'uom': value.uom,
                                'unitRefId': value.unitRefId,
                                'unitRefName' : value.unitRefName,
                                'price': value.price,
                                'initialprice' : value.price,
                                'discountOption': vm.discFlag,
                                'discountValue': value.discountValue,
                                'totalAmount': value.totalAmount,
                                'taxAmount' : value.taxAmount,
                                'netAmount': value.netAmount,
                                'remarks': value.remarks,
                                'currentInHand': value.currentInHand,
                                'alreadyOrderedQuantity': value.alreadyOrderedQuantity,
                                'taxlist': value.taxForMaterial,
                                'applicableTaxes': value.applicableTaxes,
                                'changeunitflag': false,
                            });
                        });
                        vm.fillDropDownMaterial();
                        vm.calculateTotalVlaue(vm.salesDetailPortion);
                    }
                    else
                    {
                        vm.salesorder.soDate = moment().format($scope.format);
                        vm.salesorder.deliveryDateExpected = moment().format($scope.format);
                        vm.salesorder.shippingLocationId = vm.defaultLocationId;
                        vm.checkCloseDay();
                        vm.addPortion();
                        vm.uilimit = 20;
                    }
                });
            }
      



            vm.ratediffshow = function (data) {
                if (vm.lastpriceshow == false || data.price == data.initialprice || data.initialprice == 0 || data.initialprice == "" )
                    return;
                vm.loading = true;
                vm.previousPriceOpen(data);

            }


            vm.previousPriceOpen = function (data) {
                if (data.materialRefId == 0 || data.materialRefId == null)
                    return;

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/salesorder/previousPriceModal.cshtml',
                    controller: 'tenant.views.house.transaction.salesorder.previousPriceModal as vm',
                    backdrop: 'static',
                    resolve: {
                        materialRefId: function () {
                            return data.materialRefId;
                        },
                        materialRefName: function () {
                            return data.materialRefName;
                        },
                        uom: function () {
                            return data.uom;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    
                });
                vm.loading = false;
            }

            vm.changeUnit = function (data) {
                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

            vm.unitReference = function (selected, data) {
                data.unitRefId = selected.value;
                data.unitRefName = selected.displayText;
                data.changeunitflag = false;

                if (data.defaultUnitId == data.unitRefId) {
                    data.price = data.initialprice;
                }
                else {
                    var convFactor = 0;
                    var convExistFlag = false;
                    vm.refconversionunit.some(function (refdata, refkey) {
                        if (refdata.baseUnitId == data.defaultUnitId && refdata.refUnitId == data.unitRefId) {
                            convFactor = refdata.conversion;
                            convExistFlag = true;
                            return true;
                        }
                    });


                    if (convExistFlag == false) {
                        abp.notify.info(app.localize('ConversionFactorNotExist', data.uom, data.unitRefName));
                        return;
                    }

                    var converstionUnitPrice = data.initialprice * 1/ convFactor;
                    data.price = converstionUnitPrice;
                }
                $scope.amtCalc(data, data.qtyOrdered, data.price);
                vm.calculateTotalVlaue(vm.salesDetailPortion);

            }

            vm.autoSoDetails = [];

            //vm.fillAutoSoDetails = function () {
            //    vm.auotSoloading = true;
            //    vm.loading = true;

            //    salesorderService.getAutoSoDetails({
            //        locationRefId : vm.defaultLocationId
            //    }).success(function (result) {
            //        vm.autoSoDetails = result;
            //        if (vm.autoSoDetails.length == 0) {
            //            vm.autoSoFlag = false;
            //            vm.soshowflag = true;
            //        }
            //        else
            //        {
            //            angular.forEach(vm.autoSoDetails, function (value, key) {
            //                value.visibleFlag = true;
            //            });
            //        }
            //    }).finally(function () {
            //        vm.auotSoloading = false;
            //        vm.loading = false;
            //    });
            //}

            init();
            vm.loadingflag = false;

            vm.makeSoBasedOnAutoSo = function (data) {

                vm.loading = true;
                vm.auotSoloading = true;
                result = angular.copy(data.autoSoDto);
                vm.salesorder = result.salesOrder;
                vm.salesOrderDetail = result.salesOrderDetail;

                vm.salesorder.locationRefId = vm.defaultLocationId;

                fillDropDownShippingLocation();

                $scope.dt = moment().format($scope.format);
                vm.uilimit = null;

                var soDate = $scope.convert(result.salesOrder.soDate);

                vm.salesorder.soDate = moment(result.salesOrder.soDate).format($scope.format);
                vm.salesorder.deliveryDateExpected = moment(result.salesOrder.deliveryDateExpected).format($scope.format);
                $scope.minDate = vm.salesorder.soDate;

                $('input[name="soDate"]').daterangepicker({
                    locale: {
                        format: $scope.format
                    },
                    singleDatePicker: true,
                    showDropdowns: true,
                    startDate: vm.salesorder.soDate,
                    minDate: $scope.minDate,
                    maxDate: moment().format($scope.format)
                });

                $('input[name="dueDate"]').daterangepicker({
                    locale: {
                        format: $scope.format
                    },
                    singleDatePicker: true,
                    showDropdowns: true,
                    startDate: vm.salesorder.deliveryDateExpected,
                    minDate: $scope.minDate,
                });

                vm.salesDetailPortion = [];
                vm.sno = 0;
                angular.forEach(vm.salesOrderDetail, function (value, key) {
                    vm.discFlag = null;
                    if (value.discountOption == 'F')
                        vm.discFlag = false;
                    else if (value.discountOption == 'P')
                        vm.discFlag = true;
                    vm.sno = vm.sno + 1;
                    $scope.taxCalc(value, value.qtyOrdered, value.price);
                    vm.salesDetailPortion.push({
                        'sno': vm.sno,
                        'materialRefId': value.materialRefId,
                        'materialRefName': value.materialRefName,
                        'qtyOrdered': value.qtyOrdered,
                        'defaultUnitId': value.defaultUnitId,
                        'uom': value.uom,
                        'unitRefId': value.unitRefId,
                        'unitRefName': value.unitRefName,
                        'price': value.price,
                        'initialprice': value.price,
                        'discountOption': vm.discFlag,
                        'discountValue': value.discountValue,
                        'totalAmount': value.totalAmount,
                        'taxAmount': value.taxAmount,
                        'netAmount': value.netAmount,
                        'remarks': value.remarks,
                        'currentInHand': value.currentInHand,
                        'alreadyOrderedQuantity': value.alreadyOrderedQuantity,
                        'taxlist': value.taxForMaterial,
                        'applicableTaxes': value.applicableTaxes,
                        'changeunitflag': false,
                    });
                });
                vm.fillDropDownMaterial();
                vm.calculateTotalVlaue(vm.salesDetailPortion);
                vm.soshowflag = true;
                vm.loading = false;
                vm.auotSoloading = false;
                vm.hideAllAutoSos(data);
            }

            $scope.convert = function (thedate) {
                var tempstr = thedate.toString();
                var newstr = tempstr.toString().replace(/GMT.*/g, "")
                newstr = newstr + " UTC";
                return new Date(newstr);
            };

            vm.showAllAutoSos = function(data)
            {
                angular.forEach(data, function (value, key) {
                    value.visibleFlag = true;
                });

            }

            vm.hideAllAutoSos = function(data)
            {
                angular.forEach(vm.autoSoDetails, function (value, key) {
                    value.visibleFlag = false;
                });
                data.visibleFlag = true;
            }

        }
    ]);
})();

