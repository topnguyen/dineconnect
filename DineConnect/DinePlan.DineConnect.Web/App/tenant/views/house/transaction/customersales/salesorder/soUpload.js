﻿
(function () {
    appModule.controller('tenant.views.house.transaction..customersales.salesorder.soUpload', [
        '$scope', '$uibModalInstance', 'uiGridConstants', 'abp.services.app.salesOrder', 'soId', 'appSession', 'FileUploader',
        function ($scope, $modalInstance, uiGridConstants, salesorderService, soId, appSession, fileUploader) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;

            vm.sorefnumber = null;
            vm.sodate = null;

            vm.salesorder = [];

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };


            vm.uploader = new fileUploader({
                url: abp.appPath + 'DocumentUpload/UploadSoDocumentInfo',
                formData: [{ id: 0 }],
                queueLimit: 1,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        //File type check
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        if ('|jpg|jpeg|pdf|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ProfilePicture_Warn_FileType'));
                            return false;
                        }
                        //File size check
                        if (item.size > 3072000) //3000KB
                        {
                            abp.message.warn(app.localize('ProfilePicture_Warn_SizeLimit'));
                            return false;
                        }
                        return true;
                    }
                }]
            });

            vm.uploader.onBeforeUploadItem = function (fileitem) {
                fileitem.formData.push({ locationRefId: vm.salesorder.locationRefId });
                fileitem.formData.push({ customerRefId: vm.salesorder.customerRefId })
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                vm.loading = true;
                vm.loading = false;
                vm.cancel();
            };

            vm.save = function () {

                salesorderService.getSalesOrderForEdit({
                    Id: soId
                }).success(function (result) {
                    vm.salesorder = result.salesOrder;
                    vm.salesorder.id = vm.salesorder.id;
                    vm.uploader.uploadAll();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {

                if (vm.salesorder.id == null) {
                    vm.existall = false;
                    return;
                }

                salesorderService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.salesorder.id,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.salesorder.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.salesorder.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.getAll = function () {
                vm.loading = true;
                salesorderService.getSalesOrderForEdit({
                    Id: soId
                }).success(function (result) {
                    //vm.userGridOptions.totalItems = result.salesOrderDetail.length;
                    vm.salesorderdetail = result.salesOrderDetail;
                    if (result.salesOrderDetail.length > 0) {
                        vm.sorefnumber = result.salesOrder.poReferenceCode;
                        vm.sodate = result.salesOrder.poDate;
                        vm.locationRefName = result.salesOrder.locationRefName;
                        vm.shippingLocationRefName = result.salesOrder.shippingLocationRefName;
                        vm.customerRefName = result.salesOrder.customerRefName;
                        vm.soNetAmount = result.salesOrder.soNetAmount;
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.hide = function () {
                vm.soreport = "";
            };

            vm.getAll();
        }]);
})();