﻿(function () {
    appModule.controller('tenant.views.house.transaction.customersales.salesdeliveryorder.detailViewInGrid', [
        '$scope', '$uibModalInstance', 'uiGridConstants', 'abp.services.app.inwardDirectCredit', 'salesdeliveryorderId', 'appSession',
        function ($scope, $modalInstance, uiGridConstants, salesdeliveryorderService, salesdeliveryorderId, appSession) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;

            vm.refreceiptnumber = null;
            vm.refreciptdate = null;


            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [                  
                    {
                        name: app.localize('MaterialName'),
                        field: 'materialName'
                    },
                    {
                        name: app.localize('DeliveredQty'),
                        field: 'deliveryQty'
                    },
                    {
                        name: app.localize('UOM'),
                        field: 'unitRefName'
                    },
                    

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };



            vm.getAll = function () {
                vm.loading = true;
                salesdeliveryorderService.getAllView({
                    skipCount: requestParams.skipCount,
                    defaultLocationRefId:vm.defaultLocationId,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    selectedId: salesdeliveryorderId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;

                    if (result.items.length > 0) {
                        vm.refreceiptnumber = result.items[0].dcNumberGivenByCustomer;
                        vm.refreciptdate = result.items[0].deliveryDate;
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };


            vm.getAll();
        }]);
})();