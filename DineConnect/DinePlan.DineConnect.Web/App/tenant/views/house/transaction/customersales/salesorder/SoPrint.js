﻿(function () {
    appModule.controller('tenant.views.house.transaction.customersales.salesorder.SoPrint', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.salesOrder', 'appSession', 'abp.services.app.company',
        'abp.services.app.customer', 'abp.services.app.location', 
        function ($scope, $filter, $state, $stateParams, $modal, uiGridConstants, salesorderService, appSession, companyService,
            customerService, locationService) {

            var vm = this;
            vm.printmailflag = true;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

						vm.connectdecimals = appSession.connectdecimals;
						vm.housedecimals = appSession.housedecimals;

						vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
						vm.defaultLocationId = appSession.user.locationRefId;

						vm.connectdecimals = appSession.connectdecimals;
						vm.housedecimals = appSession.housedecimals;

            

            vm.salesorderId = $stateParams.printid;

            vm.headerline = "~~~~~~~~~~~~~~~~~~~~";

            vm.sendMail = function () {
                vm.potext = '';
                vm.loading = true;
                salesorderService.sendPruchaseOrderEmail(
                {
                    salesOrderId: vm.salesorderId,
                    salesOrderHTML: vm.potext
                }).success(function (result) {
                  
                    abp.message.success(app.localize('MailedSuccessfully'));
                    abp.notify.success(app.localize('MailedSuccessfully'));
                }).finally(function () {
                    vm.loading = false;
                    });
            }

            vm.getData = function () {
                salesorderService.getSalesOrderForEdit({
                    Id: vm.salesorderId
                }).success(function (result) {
                    vm.salesOrder = result.salesOrder;
                    vm.salesOrderDetail = result.salesOrderDetail;

                    if (vm.salesOrder.locationRefId != vm.defaultLocationId) {
                        $state.go("tenant.salesorder");
                        return;
                    }

                    vm.salesDetailPortion = [];

                    vm.sno = 0;
                    vm.subtotalamount = 0;
                    vm.subtaxamount = 0;
                    vm.subnetamount = 0;

                    vm.deliverydate = $filter('date')(new Date(vm.salesOrder.deliveryDateExpected), 'fullDate');

                    vm.termSalesOrder = app.localize('TermSalesOrder', vm.deliverydate);

                    angular.forEach(vm.salesOrderDetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.subtotalamount = vm.subtotalamount + value.totalAmount;
                        vm.subtaxamount = vm.subtaxamount + value.taxAmount;
                        vm.subnetamount = vm.subnetamount + value.netAmount;

                        vm.discFlag = null;
                        if (value.discountOption == 'F')
                            vm.discFlag = false;
                        else if (value.discountOption == 'P')
                            vm.discFlag = true;
                        vm.salesDetailPortion.push({
                            'sno': vm.sno,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'qtyOrdered': value.qtyOrdered,
                            'uom': value.uom,
                            'unitRefName': value.unitRefName,
                            'unitRefId' : value.unitRefId,
                            'price': value.price,
                            'discountOption': vm.discFlag,
                            'discountValue': value.discountValue,
                            'totalAmount': value.totalAmount,
                            'taxAmount': value.taxAmount,
                            'netAmount': value.netAmount,
                            'remarks': value.remarks,
                            'currentInHand': value.currentInHand,
                            'alreadyOrderedQuantity': value.alreadyOrderedQuantity,
                            'taxlist': value.taxForMaterial
                        });
                    });

                    //userService.getUserForEdit({
                    //    Id: vm.salesOrder.creatorUserId
                    //}).success(function (result) {
                    //    vm.user = result.user;
                    //});

                    locationService.getUserInfoBasedOnUserId({
                        id: vm.salesOrder.creatorUserId
                    }).success(function (result) {
                        vm.user = result;
                    });

                    customerService.getCustomerForEdit({
                        Id: vm.salesOrder.customerRefId
                    }).success(function (result) {
                        vm.customer = result.customer;
                    });

                    locationService.getLocationForEdit({
                        Id: vm.salesOrder.locationRefId
                    }).success(function (result) {
                        vm.billinglocation = result.location;
                    });

                    locationService.getLocationForEdit({
                        Id: vm.salesOrder.shippingLocationId
                    }).success(function (result) {
                        vm.shippinglocation = result.location;
                    });

                    locationService.getLocationForEdit({
                        Id: vm.salesOrder.locationRefId
                    }).success(function (result) {
                        vm.locations = result.location;
                            if(vm.locations != null )
                            {
                                    companyService.getCompanyForEdit({
                                        Id: vm.locations.companyRefId
                                    }).success(function (result) {
                                        vm.companyProfilePictureId = result.company.companyProfilePictureId;
                                    });
                            }
                    });

                });
            }

            vm.getData();

        }]);
})();

