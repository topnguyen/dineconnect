﻿(function () {
    appModule.controller('tenant.views.house.transaction.customersales.salesdeliveryorder.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.salesDeliveryOrder', 'appSession',
			'abp.services.app.location', 'abp.services.app.dayClose', 
				function ($scope, $state, $modal, uiGridConstants, salesdeliveryorderService, appSession, locationService, daycloseService){   // setupService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.defaultLocationId = appSession.user.locationRefId;

            vm.advancedFiltersAreShown = false;
            vm.dateFilterApplied = false;

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };


            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.isPurchaseAllowed = true;//appSession.location.isPurchaseAllowed;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Transaction.SalesDeliveryOrder.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Transaction.SalesDeliveryOrder.Edit'),
                importData: abp.auth.hasPermission('Pages.Tenant.Setup.ImportData'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Transaction.SalesDeliveryOrder.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

         

            vm.userGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
					{
						name: app.localize('Actions'),
						enableSorting: false,
						width: 120,

						cellTemplate:
						   "<div class=\"ui-grid-cell-contents\">" +
							    "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
							   "    <ul uib-dropdown-menu>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editSalesDeliveryOrder(row.entity)\">" + app.localize("Edit") + "</a></li>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteSalesDeliveryOrder(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                 "      <li><a ng-click=\"grid.appScope.salesDoPrint(row.entity)\">" + app.localize("Print") + "</a></li>" +
							   "    </ul>" +
							   "  </div>" +
							   "</div>"
					},
                    {
                        name: app.localize('Date'),
                        field: 'deliveryDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                        width : 120
                    },
                    {
                        name: app.localize('Customer'),
                        field: 'customerName'
                    },
                    {
                        name: app.localize('DONumberGivenByCustomer'),
                        field: 'dcNumberGivenByCustomer',
                    
                    },                  
   				    {
                        name: app.localize('BillConversion'),
                        field: 'isDcCompleted',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.isDcCompleted" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.isDcCompleted" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                        minWidth: 80,
                        width: 180
                   },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.viewSalesDoDetail = function (myObject) {
                openView(myObject.id);
            };

            function openView(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/customersales/salesdeliveryorder/detailViewInGrid.cshtml',
                    controller: 'tenant.views.house.transaction.customersales.salesdeliveryorder.detailViewInGrid as vm',
                    backdrop: 'static',
                    resolve: {
                        salesdeliveryorderId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.salesDoPrint = function (myObject) {
                $state.go('tenant.salesdoprint', {
                        printid: myObject.id
                    });
            };

            vm.clear = function () {
                vm.filterText = null;
                vm.dcNumberGivenByCustomer = null;
                vm.dateRangeModel = null;
                vm.dateFilterApplied = false;
                vm.getAll();
            }

            vm.getAll = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var dcNumberGivenByCustomer = null;

                    if (vm.dateFilterApplied) {
                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                    dcNumberGivenByCustomer = vm.dcNumberGivenByCustomer;

                salesdeliveryorderService.getAllWithCustomerNames({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    customerName : vm.filterText,
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    dcNumberGivenByCustomer: dcNumberGivenByCustomer
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editSalesDeliveryOrder = function (myObj) {
                if (myObj.isDcCompleted==false)
                    openCreateOrEditModal(myObj.id);
                else
                {
                    abp.notify.info(app.localize('AlreadyCompletedDc'));
                }
            };

            vm.createSalesDeliveryOrder = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteSalesDeliveryOrder = function (myObject) {
                if (myObject.isDcCompleted == true) {
                    abp.notify.info(app.localize('SelectedReceiptsBillConvertedErr'));
                    return;
                }

                abp.message.confirm(
                    app.localize('DeleteSalesDeliveryOrderWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            salesdeliveryorderService.deleteSalesDeliveryOrder({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };



            function openCreateOrEditModal(objId)
            {
                //var modalInstance = $modal.open({
                //    templateUrl: '~/App/tenant/views/house/transaction/salesdeliveryorder/createOrEditModal.cshtml',
                //    controller: 'tenant.views.house.transaction.salesdeliveryorder.createOrEditModal as vm',
                //    backdrop: 'static',
				//	keyboard: false,
                //    resolve: {
                //        salesdeliveryorderId: function () {
                //            return objId;
                //        }
                //    }
                //});

                //modalInstance.result.then(function (result) {
                //    vm.getAll();
                //});
                //debugger;
                $state.go("tenant.salesdeliveryordermainform", {
                    id:objId
                });

            }

            vm.exportToExcel = function () {

                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var dcNumberGivenByCustomer = null;

                if (vm.advancedFiltersAreShown) {
                    if (vm.dateFilterApplied) {
                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                    dcNumberGivenByCustomer = vm.dcNumberGivenByCustomer;
                }

                salesdeliveryorderService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    customerName : vm.filterText,
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    dcNumberGivenByCustomer: dcNumberGivenByCustomer
                }).success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            };


            vm.getAll();

            vm.checkCloseDay = function () {
							daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                  
                    }
                  

                }).finally(function () {
                });
            }

            vm.checkCloseDay();

        }]);
})();

