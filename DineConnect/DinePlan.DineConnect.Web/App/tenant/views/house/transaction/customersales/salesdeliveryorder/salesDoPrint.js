﻿(function () {
    appModule.controller('tenant.views.house.transaction.customersales.salesdeliveryorder.salesDoPrint', [
       '$scope', '$state', '$stateParams', 'abp.services.app.salesDeliveryOrder',
        'abp.services.app.location', 'appSession', 'abp.services.app.salesOrder',
        'abp.services.app.customer','abp.services.app.user',
        function ($scope, $state, $stateParams, salesdeliveryorderService, locationService, appSession,
             salesorderService,  customerService, userService) {

            var vm = this;
            vm.printmailflag = true;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
						vm.defaultLocationId = appSession.user.locationRefId;
						vm.connectdecimals = appSession.connectdecimals;
						vm.housedecimals = appSession.housedecimals;

            vm.smartprintourexists = abp.setting.getBoolean("App.House.SmartPrintOutExists");
            

            vm.salesdeliveryorderId = $stateParams.printid;

            vm.headerline = "......................";

            vm.sendMail = function () {
                vm.potext = '';
                vm.loading = true;
                salesdeliveryorderService.sendSalesOrderEmail(
                {
                    salesdeliveryorderId: vm.salesdeliveryorderId,
                    salesDeliveryOrderHTML: vm.potext
                }).success(function (result) {
                  
                    abp.message.success(app.localize('MailedSuccessfully'));
                    abp.notify.success(app.localize('MailedSuccessfully'));
                }).finally(function () {
                    vm.loading = false;
                    });
            }

            vm.getData = function () {
                salesdeliveryorderService.getSalesDeliveryOrderForEdit({
                    Id: vm.salesdeliveryorderId
                }).success(function (result) {
                    vm.salesdeliveryorder = result.salesDeliveryOrder;
                    vm.salesDoPortion = [];

                    vm.sno = 0;
                    vm.subtotalamount = 0;
                    vm.subtaxamount = 0;
                    vm.subnetamount = 0;

                    vm.salesdeliveryorderdetail = result.salesDeliveryOrderDetail;

                    angular.forEach(vm.salesdeliveryorderdetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.salesDoPortion.push({
                                            'sno': vm.sno,
                                            "deliveryRefId": value.deliveryRefId,
                                            "materialRefId": value.materialRefId,
                                            "materialRefName": value.materialRefName,
                                            "deliveryQty": value.deliveryQty,
                                            "unitRefId": value.unitRefId,
                                            "unitRefName": value.unitRefName,
                                            "materialReceivedStatus":value.materialReceivedStatus,
                                            "isFractional": value.isFractional
                                        });
                    });

                    //userService.getUserForEdit({
                    //    Id: vm.salesdeliveryorder.creatorUserId
                    //}).success(function (result) {
                    //    vm.user = result.user;
                    //});

                    locationService.getUserInfoBasedOnUserId({
                        id: vm.salesdeliveryorder.creatorUserId
                    }).success(function (result) {
                        vm.user = result;
                    });

                    customerService.getCustomerForEdit({
                        Id: vm.salesdeliveryorder.customerRefId
                    }).success(function (result) {
                        vm.customer = result.customer;
                    });

                    locationService.getLocationForEdit({
                        Id: vm.salesdeliveryorder.locationRefId
                    }).success(function (result) {
                        vm.billinglocation = result.location;
                        vm.shippinglocation = result.location;
                    });

                });

            }

            vm.getData();

            var person = {
                firstName: "Christophe",
                lastName: "Coenraets",
                blogURL: "http://coenraets.org"
            };

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            var template = "<h1>{{firstName}} {{lastName}}</h1>Blog: {{blogURL}}";

            vm.printIn40Col = function () {
                vm.printmailflag = false;
                $state.go('tenant.inwarddcprint40col', {
                    printid: vm.salesdeliveryorderId
                });
            }

        }]);
})();

