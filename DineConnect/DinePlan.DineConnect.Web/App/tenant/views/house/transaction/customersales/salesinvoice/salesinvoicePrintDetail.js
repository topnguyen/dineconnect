﻿(function () {
    appModule.controller('tenant.views.house.transaction.customersales.salesinvoice.invoicePrintDetail', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.salesInvoice', 'appSession',
        'abp.services.app.customer', 'abp.services.app.location',
        function ($scope, $filter, $state, $stateParams, $modal, uiGridConstants, salesinvoiceService, appSession,
            customerService, locationService) {

            var vm = this;
						vm.connectdecimals = appSession.connectdecimals;
						vm.housedecimals = appSession.housedecimals;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            //vm.defaultLocationId = appSession.user.locationRefId;
            vm.smartprintourexists = abp.setting.getBoolean("App.House.SmartPrintOutExists");
            

            vm.salesinvoiceId = $stateParams.printid;

            vm.headerline = "~~~~~~~~~~~~~~~~~~~~";

            vm.getData = function () {
             salesinvoiceService.getSalesInvoiceForEdit({
                    Id: vm.salesinvoiceId
            }).success(function (result)
            {
                vm.salesinvoice = result.salesInvoice;
                    //vm.salesinvoicedetail = result.salesinvoiceDetail;

                    vm.salesinvoicePortions = [];

                    vm.sno = 0;
                    vm.subtotalamount = 0;
                    vm.subtaxamount = 0;
                    vm.subnetamount = 0;

                    vm.salesinvoicedetail = result.salesInvoiceDetail;
                    vm.salesinvoicedeliveryorderlink = result.salesInvoiceDeliveryOrderLink;
                    vm.refdctranid = result.salesInvoiceDeliveryOrderLink;

                    angular.forEach(vm.salesinvoicedetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.subtotalamount = vm.subtotalamount + value.totalAmount;
                        vm.subtaxamount = vm.subtaxamount + value.salesTaxAmount;
                        vm.subnetamount = vm.subnetamount + value.netAmount;
                        vm.salesinvoicePortions.push({
                            'sno': value.sno,
                            'dcTranid': value.dcTranId,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'totalQty': value.totalQty,
                            'price': value.price,
                            'discountFlag': value.discountFlag,
                            'discountPercentage': value.discountPercentage,
                            'discountAmount': parseFloat(vm.isUndefinedOrNull(value.discountAmount) ? 0 : value.discountAmount),
                            'totalAmount': parseFloat(parseFloat(value.totalAmount).toFixed(2)),
                            'taxAmount': parseFloat(parseFloat(value.salesTaxAmount).toFixed(2)),
                            'netAmount': parseFloat(parseFloat(value.netAmount).toFixed(2)),
                            'remarks': value.remarks,
                            'taxForMaterial': value.taxForMaterial,
                            'uom': value.uom,
                            'unitRefId': value.unitRefId,
                            'unitRefName' : value.unitRefName
                        });
                    });

                    customerService.getCustomerForEdit({
                        Id: vm.salesinvoice.customerRefId
                    }).success(function (result) {
                        vm.customer = result.customer;
                    });

                    locationService.getLocationForEdit(
                        {
                            id : vm.salesinvoice.locationRefId
                        }).success(function (result) {
                            vm.location = result.location;
                        });
                });
            }

            vm.getData();

            var person = {
                firstName: "Christophe",
                lastName: "Coenraets",
                blogURL: "http://coenraets.org"
            };

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.printIn40Col = function () {
                vm.printmailflag = false;
                $state.go('tenant.salesinvoiceprintdetail40col', {
                    printid: vm.salesinvoiceId
                });
            }


            var template = "<h1>{{firstName}} {{lastName}}</h1>Blog: {{blogURL}}";
            //var html = Mustache.to_html(template, person);
            //$('#sampleArea').html(html);

        
        }]);
})();

