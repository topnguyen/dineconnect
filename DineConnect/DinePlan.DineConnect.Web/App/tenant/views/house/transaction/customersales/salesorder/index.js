﻿(function () {
    appModule.controller('tenant.views.house.transaction.customersales.salesorder.index', [
			'$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.salesOrder', 'appSession', 'abp.services.app.location', 'abp.services.app.material', 'abp.services.app.dayClose', 
			function ($scope, $state, $modal, uiGridConstants, salesorderService, appSession, locationService, materialService, daycloseService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.totalPending = 0;
            vm.totalApproved = 0;
            vm.totalDeclined = 0;
            vm.totalYetToReceived = 0;
            vm.soReferenceCode = null;

            vm.defaultLocationId = appSession.user.locationRefId;

            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }
//  vm.isSalesAllowed = appSession.location.isSalesAllowed;

            vm.advancedFiltersAreShown = false;
            vm.dateFilterApplied = false;

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };
           

            //if (vm.isSalesAllowed == true) {
                vm.permissions = {
                    create: abp.auth.hasPermission('Pages.Tenant.House.Transaction.SalesOrder.Create'),
                    edit: abp.auth.hasPermission('Pages.Tenant.House.Transaction.SalesOrder.Edit'),
                    importData: abp.auth.hasPermission('Pages.Tenant.Setup.ImportData'),
                    'delete': abp.auth.hasPermission('Pages.Tenant.House.Transaction.SalesOrder.Delete'),
                    approve: abp.auth.hasPermission('Pages.Tenant.House.Transaction.SalesOrder.Approve')
                };
            //}
            //else {
            //    vm.permissions = {
            //        create: false,
            //        edit: false,
            //        importData: false,
            //        'delete': false,
            //        approve: false
            //    };
            //}

            vm.requestParams = {
                userName: '',
                serviceName: '',
                methodName: '',
                browserInfo: '',
                hasException: '',
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
					{
						name: app.localize('Actions'),
						enableSorting: false,
						width: 120,

						cellTemplate:
						   "<div class=\"ui-grid-cell-contents\">" +
							   "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
							   "    <ul uib-dropdown-menu>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editSalesOrder(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                                "      <li><a ng-if=\"grid.appScope.permissions.approve\" ng-click=\"grid.appScope.approveSalesOrder(row.entity)\">" + app.localize("Approve") + "</a></li>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteSalesOrder(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                               "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.printSalesOrder(row.entity)\">" + app.localize("Print") + "</a></li>" +
							   "    </ul>" +
							   "  </div>" +
							   "</div>"
					},
                    {
                        name: app.localize('Date'),
                        field: 'soDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                        width:100
                    },
                    {
                        name: app.localize('Reference'),
                        field: 'soReferenceCode',
                        width:180
                    },
                    {
                        name: app.localize('CustomerName'),
                        field: 'customerName',
                        width: 250
                    },
                    {
                        name: app.localize('ShippingLocation'),
                        field: 'shippingLocationName'
                    },
                    {
                        name: app.localize('Status'),
                        field: 'soCurrentStatus',
                        //cellTemplate:
                        //    '<div class=\"ui-grid-cell-contents\">' +
                        //    '  <span ng-show="row.entity.soCurrentStatus==\"Approved\"" class="label label-success">' + app.localize('Yes') + '</span>' +
                        //    '  <span ng-show="!row.entity.soStatus" class="label label-default">' + app.localize('No') + '</span>' +
                        //    '</div>',
                        minWidth: 80
                    },
			//	    {
            //            name: app.localize('View'),
            //            enableSorting: false,
            //            width: 75,
            //            cellTemplate:
            //            '<div class=\"ui-grid-cell-contents text-center\">' +
            //            //' <a href="{{ vm.importpath }}"><button class="btn btn-default btn-xs" ><i class="fa fa-download"></i></button></a>' +
            //                ' <a href="{{ vm.importpath }}"> <button class="btn btn-default btn-xs" ng-click="grid.appScope.downloadDocument(row.entity)"><i class="fa fa-download"></i></button><a>' +
            //                '</div>'
            //},
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.viewPoDetail = function (myObject) {
                openView(myObject.id);
            };

            vm.printSalesOrder = function (myObject) {
                var currentStatus = myObject.soCurrentStatus;
                if (currentStatus == app.localize('Approved') || currentStatus == app.localize('Completed') || currentStatus == app.localize('Partial')) {
                    $state.go('tenant.salesorderprint', {
                        printid: myObject.id
                    });
                }
                else
                {
                    abp.message.error(app.localize('CouldNotPrintSalesOrder'), app.localize(currentStatus));
                    return;
                }
            };

            vm.downloadDocument = function (employeerow) {
                fileDownload(employeerow.id);
            }

            function fileDownload(objId) {
                 
                salesorderService.getSalesOrderForEdit({
                    Id: objId
                }).success(function (result) {
                    vm.salesorder = result.salesOrder;

                    var savePerson = function (person) {
                        return abp.ajax({
                            url: '/DocumentUpload/DownloadPoUpload',
                            data: JSON.stringify(person)
                        });
                    };

                    //Create a new person
                    var newPerson = {
                        locationRefId: vm.salesorder.locationRefId,
                        customerRefId: vm.salesorder.customerRefId
                    };

                    savePerson(newPerson);
                    //    .done(function (data) {
                    //    abp.notify.success('created new person with id = ' + data.personId);
                    //});

                });
           
            }

            vm.upLoadSalesOrder = function (myObject) {
                openUpload(myObject.id);
            };

            function openUpload (objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/customersales/salesorder/soUpload.cshtml',
                    controller: 'tenant.views.house.transaction.customersales.salesorder.soUpload as vm',
                    backdrop: 'static',
                    resolve: {
                        poId: function () {
                            return objId;
                        },
                    }
                });

                //$state.go('tenant.salesorderupload', {
                //    id: myObject.id
                //})
            };

            function openView(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/customersales/salesorder/sodetailViewInGrid.cshtml',
                    controller: 'tenant.views.house.transaction.customersales.salesorder.sodetailViewInGrid as vm',
                    backdrop: 'static',
                    resolve: {
                        poId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                }).finally(function (result) {
                    vm.getAll();
                });


            }

            vm.clear = function () {
                vm.filterText = null;
                vm.soReferenceCode = null;
                vm.dateRangeModel = null;
                vm.dateFilterApplied = false;
                vm.status = null;
                vm.materialRefId = null;
                vm.getAll();
            }

            vm.getAll = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var soReferenceCode = null;
                var status = null;
                var materialRefId = null;

                if (vm.dateFilterApplied) {
                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }
                soReferenceCode = vm.soReferenceCode;
                status = vm.status;
                materialRefId = vm.materialRefId

                salesorderService.getAllWithRefName({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    soReferenceCode: soReferenceCode,
                    soStatus: status,
                    materialRefId : materialRefId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editSalesOrder = function (myObj) {
                var currentStatus = myObj.soCurrentStatus;
                if (currentStatus == app.localize('Pending'))
                    openCreateOrEditModal(myObj.id,false);
                else {
                    abp.message.error(app.localize('EditNotAllowedApproved'), app.localize('CouldNotEdit'));
                }
            };

            vm.approveSalesOrder = function (myObj) {
                var currentStatus = myObj.soCurrentStatus;
                if (currentStatus == app.localize('Pending'))
                    openCreateOrEditModal(myObj.id,false);
                else if (currentStatus == app.localize('Declined'))
                {
                    abp.message.error(app.localize('AlreadyDeclined'), app.localize('Declined'));
                }
                else {
                    abp.message.error(app.localize('AlreadyApproved'), app.localize('Approved'));
                }
            };

            vm.createSalesOrder = function () {
                openCreateOrEditModal(null,false);
            };

            vm.deleteSalesOrder = function (myObject) {
                var currentStatus = myObject.soCurrentStatus;
                if (currentStatus == app.localize('Approved') || currentStatus == app.localize('Completed') || currentStatus == app.localize('Declined') || currentStatus == app.localize('Partial')) {
                    abp.message.error(app.localize('CouldNotDeleteSalesOrder'), app.localize(currentStatus));
                    return;
                }

                abp.message.confirm(
                    app.localize('DeleteSalesOrderWarning', myObject.id + " " + myObject.soReferenceCode),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            salesorderService.deleteSalesOrder({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId,autoSoFlag) {
                $state.go('tenant.salesorderdetail', {
                    id: objId,
                    autoSoFlag: autoSoFlag
                });
            }

            vm.exportToExcel = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var soReferenceCode = null;

                if (vm.advancedFiltersAreShown) {
                    if (vm.dateFilterApplied) {
                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                    soReferenceCode = vm.soReferenceCode;
                }

                salesorderService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    soReferenceCode: soReferenceCode
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            };

            vm.getDashBoard = function () {
                vm.loading = true;
                salesorderService.getDashBoardSalesOrder({
                   Id : vm.defaultLocationId
                }).success(function (result) {
                    vm.totalPending = result.totalPending;
                    vm.totalApproved = result.totalApproved;
                    vm.totalYetToReceived = result.totalYetToReceived;
                    vm.totalDeclined = result.totalDeclined;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.getDashBoard();
            vm.getAll();

            vm.refmaterial = [];


            function fillDropDownMaterial() {
                materialService.getMaterialForCombobox({}).success(function (result) {
                    vm.refmaterial = result.items;
                });
            }

            fillDropDownMaterial();

            vm.autoPo = function () 
            {
                openCreateOrEditModal(null, true);
            }

            vm.checkCloseDay = function () {
							daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        if(vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                }).finally(function () {
                });
            }

            
            vm.checkCloseDay();
        }]);
})();

