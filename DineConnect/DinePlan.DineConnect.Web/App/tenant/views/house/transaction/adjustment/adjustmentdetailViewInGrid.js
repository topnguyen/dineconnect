﻿
(function () {
    appModule.controller('tenant.views.house.transaction.adjustment.adjustmentdetailViewInGrid', [
        '$scope', '$uibModalInstance', 'uiGridConstants', 'abp.services.app.adjustment', 'adjustmentId', 'appSession',
        function ($scope, $modalInstance, uiGridConstants, adjustmentService, adjustmentId, appSession) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;

            vm.tokennumber = null;
            vm.adjustmentdate = null;


            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                {
                    name: app.localize('MaterialName'),
                    field: 'materialRefName',
                    width : 250
                },
                {
                    name: app.localize('AdjustmentQty'),
                    field: 'adjustmentQty',
                    cellClass: 'ui-ralign',
                    cellFilter: 'number: 2'
                },
                {
                    name: app.localize('UOM'),
                    field: 'unitRefName'
                },
                {
                    name: app.localize('AdjustmentMode'),
                    field: 'adjustmentMode'
                },
                {
                    name: app.localize('ApprovedRemarks'),
                    field: 'adjustmentApprovedRemarks',
                    cellClass: 'ui-ralign'
                },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.adjustmentreport = null;

            vm.getAll = function() {
                vm.loading = true;
                adjustmentService.getAdjustmentForEdit({
                    Id: adjustmentId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.adjustmentDetail.length;
                    vm.userGridOptions.data = result.adjustmentDetail;
                    if (result.adjustmentDetail.length > 0) {
                        vm.tokennumber = result.adjustment.tokenRefNumber;
                        vm.adjustmentdate = result.adjustment.adjustmentDate;
                        vm.materialRefName = result.adjustmentDetail.materialRefName;
                        vm.adjustmentQty = result.adjustmentDetail.adjustmentQty;
                        vm.adjustmentMode = result.adjustmentDetail.adjustmentMode;
                        vm.adjustmentApprovedRemarks = result.adjustmentDetail.adjustmentApprovedRemarks;
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.hide = function() {
                vm.adjustmentreport = "";
            };

            vm.getAll();
        }]);
})();