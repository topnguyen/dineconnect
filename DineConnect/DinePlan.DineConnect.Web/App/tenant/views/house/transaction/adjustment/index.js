﻿
(function () {
    appModule.controller('tenant.views.house.transaction.adjustment.index', [
			'$scope', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.adjustment', 'appSession', 'abp.services.app.location', 'abp.services.app.dayClose', 
			function ($scope, $state, $stateParams, $modal, uiGridConstants, adjustmentService, appSession, locationService, daycloseService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.advancedFiltersAreShown = false;
            vm.dateFilterApplied = false;
            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.reportHeaderInfo = app.localize('AdjustmentHeaderInfo');

            vm.reportFlag = $stateParams.physicalReportFlag;

            if (vm.reportFlag) {
                vm.advancedFiltersAreShown = true;
                vm.filterText = $stateParams.filtervalue;
                if (vm.filterText=='999')
                {
                    vm.reportHeaderInfo = app.localize('DayCloseAdjustmentReport');
                }
                else if (vm.filterText=='888')
                {
                    vm.reportHeaderInfo = app.localize('PhysicalStockReport');
                }
            }

            vm.closingstockAdjustmentFlag = $stateParams.closingstockAdjustmentFlag;
            if (vm.closingstockAdjustmentFlag) {
                vm.reportHeaderInfo = app.localize('ClosingStock') + ' ' + app.localize('Adjustment');
                vm.filterText = '888';
            }

            vm.defaultLocationId = appSession.user.locationRefId;

            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Transaction.Adjustment.Create'),
                //edit: abp.auth.hasPermission('Pages.Tenant.House.Transaction.Adjustment.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Transaction.Adjustment.Delete'),
                forceadjustment : abp.auth.hasPermission('Pages.Tenant.House.Transaction.Adjustment.ForceAdjustment')
            };

            if (vm.reportFlag)
            {
                vm.permissions.create = false;
                vm.permissions.delete = false;
                vm.permissions.forceadjustment = false;
            }
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

        

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.userGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
					{
						name: app.localize('Actions'),
						enableSorting: false,
						width: 120,

						cellTemplate:
						   "<div class=\"ui-grid-cell-contents\">" +
							    "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
							   "    <ul uib-dropdown-menu>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editAdjustment(row.entity)\">" + app.localize("Edit") + "</a></li>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteAdjustment(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                               "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.viewAdjustmentDetail(row.entity)\">" + app.localize("ViewDetail") + "</a></li>" +
                                "      <li><a ng-click=\"grid.appScope.printAdjustment(row.entity)\">" + app.localize("Print") + "</a></li>" +
							   "    </ul>" +
							   "  </div>" +
							   "</div>"
					},
                    {
                        name: app.localize('LocationName'),
                        field: 'locationRefName'
                    },
                    {
                        name: app.localize('AdjustmentDate'),
                        field: 'adjustmentDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\''
                    },
                    {
                        name: app.localize('TokenNumber'),
                        field: 'tokenRefNumber',
                    },
                    {
                        name: app.localize('ForceAdjustment'),
                        field: 'closingStockDate',
                        width: 100,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.closingStockDate==null  && row.entity.tokenRefNumber==888" class="label label-success">' + app.localize('ForceAdj') + '</span>' +
                            '</div>',
                    },
                    {
                        name: app.localize('Remarks'),
                        field: 'adjustmentRemarks'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm:ss\''
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.clear = function () {
                vm.filterText = null;
                vm.dateRangeModel = null;
                vm.dateFilterApplied = false;
                vm.getAll();
            }
            vm.printAdjustment = function (myObject) {
                //var currentStatus = myObject.poCurrentStatus;
                //if (currentStatus == app.localize('Approved') || currentStatus == app.localize('Completed') || currentStatus == app.localize('Partial')) {

                    //adjustmentService.getAdjustmentReportInNotePad({
                    //    adjustmentId: myObject.id
                    //}).success(function (result) {
                    //    vm.potext = result;
                    //});
                    $state.go('tenant.adjustmentprint', {
                        printid: myObject.id
                    });
                //}
                //else {
                //    abp.message.error(app.localize('CouldNotPrintAdjustment'), app.localize(currentStatus));
                //    return;
                //}
            };

            vm.getAll = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                 //var tokenRefNumber = null;

              
                    if (vm.dateFilterApplied) {

                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                    //tokenRefNumber = vm.tokenRefNumber;
                

                adjustmentService.getView({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    startDate: startDate,
                    endDate: endDate,
                    //tokenRefNumber: tokenRefNumber,
										locationRefId: vm.defaultLocationId,
										
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editAdjustment = function (myObj) {
                openCreateOrEditModal(myObj.id,false,false);
            };

            
            vm.fixTheStockDifference = function (argclosingstockAdjustmentFlag) {
                if (argclosingstockAdjustmentFlag) {
                    abp.message.confirm(
                        app.localize('ClosingStockAdjustmentWarning'),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                openCreateOrEditModal(null, false,true);
                            }
                        }
                    );
                }
            };

            vm.createAdjustment = function (argForceAdjustmentFlag) {
                if (argForceAdjustmentFlag) {
                    abp.message.confirm(
                        app.localize('ForceAdjustmentWarning'),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                openCreateOrEditModal(null, argForceAdjustmentFlag,false);
                            }
                        }
                    );
                }
                else {
                    openCreateOrEditModal(null, argForceAdjustmentFlag,false);
                }
            };

            vm.viewAdjustmentDetail = function (myObject) {
                openView(myObject.id);
            };

            function openView(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/adjustment/adjustmentdetailViewInGrid.cshtml',
                    controller: 'tenant.views.house.transaction.adjustment.adjustmentdetailViewInGrid as vm',
                    backdrop: 'static',
                    resolve: {
                        adjustmentId: function () {
                            return objId;
                        }
                    }
                });
            }

            vm.deleteAdjustment = function (myObject) {

                abp.message.confirm(
                    app.localize('DeleteAdjustmentWarning', myObject.id + ', ' +  myObject.tokenRefNumber + ' , ' + myObject.adjustmentRemarks),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            adjustmentService.deleteAdjustment({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId, argForceAdjustmentFlag, argclosingstockAdjustmentFlag) {
                $state.go("tenant.adjustmentdetail", {
                    id: objId,
                    autodetail: null,
                    forceAdjustmentFlag: argForceAdjustmentFlag,
                    closingstockAdjustmentFlag: argclosingstockAdjustmentFlag
                });

            }


            vm.generateStockReport = function () 
            {
                $state.go("tenant.housereportclosingstock", {
                    reportKey: 'CLOSESTOCK',
                });

                
            }

            vm.exportToExcel = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                 //var tokenRefNumber = null;

                if (vm.advancedFiltersAreShown) {
                    if (vm.dateFilterApplied) {

                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                    //tokenRefNumber = vm.tokenRefNumber;
                }

                adjustmentService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    locationRefId:vm.defaultLocationId,
                    filter: vm.filterText,
                    startDate: startDate,
                    endDate: endDate,
                    //tokenRefNumber: tokenRefNumber,
                    locationRefId : vm.defaultLocationId
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            };


            vm.getAll();

            vm.checkCloseDay = function () {
							daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                }).finally(function () {
                });
            }

            vm.checkCloseDay();

            if (vm.reportFlag) {
                vm.columnprice =
                            {
                                name: app.localize('Print'),
                                cellTemplate:
                                    '<div class=\"ui-grid-cell-contents text-center\">' +
                                    '  <button ng-click="grid.appScope.printAdjustment(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Print') + '"><i class="fa fa-print"></i></button>' +
                                    '</div>',
                                width: 80
                            };

                //vm.userGridOptions.columnDefs.splice(1, 0, vm.columnprice);
                vm.userGridOptions.columnDefs.splice(0, 1, vm.columnprice);
            }


        }]);
})();

