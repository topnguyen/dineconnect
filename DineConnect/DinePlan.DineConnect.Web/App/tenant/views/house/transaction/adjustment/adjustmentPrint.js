﻿(function () {
    appModule.controller('tenant.views.house.transaction.adjustment.adjustmentPrint', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.adjustment', 'appSession', 'abp.services.app.company',
        'abp.services.app.location', '$rootScope',
        function ($scope, $filter, $state, $stateParams, $modal, uiGridConstants, adjustmentService, appSession, companyService,
            locationService, $rootScope) {
            /* eslint-disable */
            var vm = this;
            vm.printmailflag = true;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.connectdecimals =  appSession.connectdecimals;
            vm.housedecimals = appSession.housedecimals;
            $rootScope.settings.layout.pageSidebarClosed = true;

            vm.loading = true;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.adjustment = null;

            vm.closingStockBasedFlag = false;

            vm.adjustmentId = $stateParams.printid;

            vm.headerline = "~~~~~~~~~~~~~~~~~~~~";

            vm.getData = function () {
                vm.loading = true;
                adjustmentService.getAdjustmentForEdit({
                    Id: vm.adjustmentId
                }).success(function (result) {
                    vm.loading = false;
                    vm.adjustment = result.adjustment;
                    vm.adjustmentDetail = result.adjustmentDetail;

                    if (vm.adjustment.locationRefId != vm.defaultLocationId) {
                        $state.go("tenant.adjustment");
                        return;
                    }

                    vm.adjustmentDetailPortion = [];

                    vm.sno = 0;

                    angular.forEach(vm.adjustmentDetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        var remarks = '';
                        if (value.closingStock != null) {
                            vm.closingStockBasedFlag = true;
                        }

                        if (value.adjustmentApprovedRemarks != null && value.adjustmentApprovedRemarks.length > 0) {
                            if (remarks.length > 0)
                                remarks = remarks + ' , ' + value.adjustmentApprovedRemarks
                            else
                                remarks = value.adjustmentApprovedRemarks;
                        }

                        vm.adjustmentDetailPortion.push({
                            'sno': vm.sno,
                            'adjustmentRefIf': value.adjustmentRefIf,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'materialGroupCategoryName': value.materialGroupCategoryName,
                            'adjustmentQty': value.adjustmentQty,
                            'defaultUnitId': value.defaultUnitId,
                            'defaultUnitName': value.defaultUnitName,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'adjustmentMode': value.adjustmentMode,
                            'adjustmentApprovedRemarks': remarks,
                            'closingStock': value.closingStock,
                            'closingStockValue': value.closingStockValue,
                            'stockEntry': value.stockEntry,
                            'stockEntryValue': value.stockEntryValue,
                            'price': value.price,
                            'adjustmentCost': value.adjustmentCost
                        });
                    });

                    locationService.getUserInfoBasedOnUserId({
                        id: vm.adjustment.creatorUserId
                    }).success(function (result) {
                        vm.user = result;
                    });

                    locationService.getLocationForEdit({
                        Id: vm.adjustment.locationRefId
                    }).success(function (result) {
                        vm.locations = result.location;
                        if (vm.locations != null) {
                            companyService.getCompanyForEdit({
                                Id: vm.locations.companyRefId
                            }).success(function (result) {
                                vm.companyProfilePictureId = result.company.companyProfilePictureId;
                            });
                        }
                    });

                }).finally(function () {
                    vm.loading = false;
                    vm.readfromexcelstatus = true;
                    vm.uilimit = null;
                });

            }

            vm.getData();


            vm.exportToExcel = function () {
                vm.loading = true;
                adjustmentService.getManualStockAdjustmentDetailReportToExcel(
                    {
                        adjustment: vm.adjustment,
                        adjustmentDetail: vm.adjustmentDetailPortion
                    }
                )
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            }

            vm.myFunction = function () {
                if (vm.isUndefinedOrNull(vm.adjustment) || vm.adjustmentDetail.length == 0) {
                    abp.notify.warn(app.localize('NoDataFound'));
                    vm.printmailflag = true;
                    return;
                }

                document.getElementById("divprintemail").style.visibility = "hidden";
                myPrint();
            };

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            function myPrint() {
                vm.printmailflag = false;
                window.print();
                document.getElementById("divprintemail").style.visibility = "visible";
                vm.printmailflag = true;
            }



        }]);
})();

