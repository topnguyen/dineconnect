﻿(function () {
    appModule.controller('tenant.views.house.transaction.adjustment.drillDownLevel', [
        '$scope', '$uibModalInstance', 'materialDetail' ,
        function ($scope, $modalInstance, materialDetail) {

            var vm = this;
            vm.materialDetail = materialDetail;
            vm.detailData = materialDetail.unitWiseQuantityList;

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

        }
    ]);
})();