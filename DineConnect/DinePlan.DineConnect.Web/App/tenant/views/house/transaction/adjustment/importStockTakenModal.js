﻿
(function () {
    appModule.controller('tenant.views.house.transaction.adjustment.importStockTakenModal', [
        '$scope', 'appSession', '$uibModalInstance', 'FileUploader', 'stockTakenDate', 'allItems',
        'locationRefId', 'abp.services.app.houseReport', 'isHighValueItemOnly', 'forceAdjustmentFlag', 'stockEntryOnlyFlag', '$window', '$state',
        function ($scope, appSession, $uibModalInstance, fileUploader, stockTakenDate, allItems, locationRefId,
            housereportService, isHighValueItemOnly, forceAdjustmentFlag, stockEntryOnlyFlag, $window, $state) {
            var vm = this;
            vm.loading = false;
            /* eslint-disable */
            vm.allItems = allItems;
            vm.locationRefId = locationRefId;
            vm.stockTakenDate = stockTakenDate;
            vm.isHighValueItemOnly = isHighValueItemOnly;
            vm.forceAdjustmentFlag = forceAdjustmentFlag;
            vm.stockEntryOnlyFlag = stockEntryOnlyFlag;
            vm.selectedUnitTypes = [];
            vm.downLoadMultipleUOMMultipleSupplierMaterialOnly = false;


            vm.refUnitTypes = [];

            vm.init = function () {
                vm.allItems = [];
                vm.errorList = [];
                vm.initUploader();
            }

            function fillDropDownUnits() {
                //vm.loading = true;
                vm.selectedUnitTypes = [];
                housereportService.getUnitTypeForCombobox({

                }).success(function (result) {
                    vm.refUnitTypes = result.items;
                   // vm.loading = false;
                    angular.forEach(vm.refUnitTypes, function (value, key) {
                        if (value.value == "2" || value.value == 2)
                            vm.selectedUnitTypes.push(value);
                    });
                });
            }
            fillDropDownUnits();

            vm.initUploader = function () {
                vm.uploader = new fileUploader({
                    url: abp.appPath + 'Import/ImportStockAdjustmentBasedOnStockTakenMultipleSupplierAliasNameReport',
                    formData: [
                        {
                            "closingStockDate": vm.stockTakenDate
                        },
                        {
                            "locationRefId": vm.locationRefId
                        },
                        {
                            "forceAdjustmentFlag": vm.forceAdjustmentFlag
                        },
                        {
                            "stockEntryOnlyFlag": vm.stockEntryOnlyFlag
                        }
                    ],
                    queueLimit: 1,
                    filters: [{
                        name: 'imageFilter',
                        fn: function (item, options) {
                            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                            if ('|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|'.indexOf(type) === -1) {
                                abp.message.warn(app.localize('ImportTemplate_Warn_FileType'));
                                return false;
                            }
                            return true;
                        }
                    }]
                });
            }
            vm.init();
            //vm.importpath = abp.appPath + 'Import/ImportStockTakenTemplate';

            vm.getTemplate = function () {
                vm.loading = true;
                housereportService.getStockTakenReportExcel({
                    locationRefId: vm.locationRefId,
                    stockTakenDate: vm.stockTakenDate,
                    isHighValueItemOnly: vm.isHighValueItemOnly,
                    multipleUomAllowed: true,
                    uomTypeFilters: vm.selectedUnitTypes,
                    DownLoadMultipleUOMMultipleSupplierMaterialOnly: vm.downLoadMultipleUOMMultipleSupplierMaterialOnly
                }).success(function (result) {
                    app.downloadTempFile(result);
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.save = function () {
                if (vm.stockTakenDate == null) {
                    abp.notify.warn(app.localize("SelectStockTakenDateErr"));
                    return;
                }
                if (vm.uploader.queue.length == 0) {
                    abp.notify.warn(app.localize('YouAreNotSelectAFile'));
                    return;
                }
                vm.loading = true;
                vm.uploader.uploadAll();
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.processPartial = function () {
                $uibModalInstance.close(vm.allItems);
                abp.notify.error(vm.errorList.length + ' ' + app.localize('Records') + ' ' + app.localize('Omitted'));
                abp.notify.info(vm.allItems.length + ' ' + app.localize('Records') + ' ' + app.localize('Processed'));
            }

            vm.datafromexcel = [];

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.error != null) {
                    abp.message.warn(response.error.message);
                    $uibModalInstance.dismiss();
                    vm.loading = false;
                    return;
                }
                vm.allItems = [];
                vm.importResult = response.result;
                vm.datafromexcel = vm.importResult.materialStockList;
                vm.errorList = vm.importResult.errorList;

                if (vm.datafromexcel == null) {
                    if (response.error != null) {
                        abp.message.warn(response.error.message);
                        vm.readfromexcelstatus = false;
                        return;
                    }
                }

                if (vm.datafromexcel != null) {
                    vm.sno = 0;
                    vm.allItems = [];
                    angular.forEach(vm.datafromexcel, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.allItems.push({
                            'sno': vm.sno,
                            'materialRefId': value.materialRefId,
                            'materialName': value.materialName,
                            'adjustmentQty': value.differenceQuantity,
                            'adjustmentMode': value.adjustmentStatus,
                            'clBalance': value.clBalance,
                            'currentStock': value.currentStock,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'defaultUnitId': value.defaultUnitId,
                            'defaultUnitName': value.defaultUnitName,
                            'unitWiseQuantityList': value.unitWiseQuantityList
                        });
                    });
                }
                else {
                    vm.allItems = [];
                }
                vm.readfromexcelstatus = false;

                abp.notify.info(app.localize("FINISHED"));
                vm.loading = false;
                if (vm.errorList.length == 0) {
                    $uibModalInstance.close(vm.allItems);
                    abp.notify.info(vm.allItems.length + ' ' + app.localize('Records') + ' ' + app.localize('Processed'));
                }

            };

            vm.materialEdit = function (argId) {
                if (vm.permissions.create) {
                    var url = $state.href('tenant.materialdetail', {
                        id: argId,
                        changeUnitFlag: false
                    });
                    $window.open(url, '_blank');
                }
            }

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.Material.Create'),
            }
        }

    ]);
})();
