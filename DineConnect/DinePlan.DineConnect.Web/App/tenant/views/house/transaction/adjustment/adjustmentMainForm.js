﻿(function () {
    appModule.controller('tenant.views.house.transaction.adjustment.adjustmentMainForm', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.adjustment', 'abp.services.app.commonLookup',
        'appSession', 'abp.services.app.houseReport', 'abp.services.app.material', 'FileUploader',
        'abp.services.app.unitConversion', 'abp.services.app.issue', 'abp.services.app.materialMenuMapping', 'abp.services.app.location', '$rootScope', 'abp.services.app.dayClose',
        function ($scope, $state, $stateParams, $modal, adjustmentService, commonLookupService,
            appSession, housereportService, materialService, fileUploader, unitconversionService, issueService, materialmenumappingService, locationService, $rootScope, daycloseService) {
            /* eslint-disable */

            var vm = this;
            vm.readfromexcelstatus = false;
            vm.excelautopost = false;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.multipleUomAllowed = appSession.user.multipleUomEntryAllowed;

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");
            vm.forceAdjustmentWithDayCloseflag = false;
            $rootScope.settings.layout.pageSidebarClosed = true;
            vm.allEqualFlag = false;

            vm.permissions = {
                hideStock: abp.auth.hasPermission('Pages.Tenant.House.Transaction.DayClose.HideStockInAdjustment')
            };

            vm.adjustment = null;
            $scope.existall = true;
            vm.adjustmentId = $stateParams.id;

            if ($stateParams.dayCloseCallFlag == 'true' || $stateParams.dayCloseCallFlag == true) {
                vm.dayCloseCallFlag = true;
            }
            else {
                vm.dayCloseCallFlag = false;
            }

            if ($stateParams.forceAdjustmentFlag == 'true' || $stateParams.forceAdjustmentFlag == true) {
                vm.forceAdjustmentFlag = true;
                vm.forceAdjustmentWithDayCloseflag = true;
            }
            else {
                vm.forceAdjustmentFlag = false;
            }

            vm.closingstockAdjustmentFlag = false;
            if ($stateParams.closingstockAdjustmentFlag == 'true' || $stateParams.closingstockAdjustmentFlag == true) {
                vm.closingstockAdjustmentFlag = true;
            }
            else {
                vm.closingstockAdjustmentFlag = false;
            }

            vm.uilimit = null;
            vm.isHighValueItemOnly = false;

            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.existdata = $stateParams.autoadjustmentdetail;
            if (vm.existdata !== "") {
                vm.autopostdetail = JSON.parse(vm.existdata);
                vm.autopostflag = true;
                vm.loading = true;
            }
            else {
                vm.autopostdetail = null;
                vm.autopostflag = false;
            }

            vm.sno = 0;
            vm.autolock = false;


            vm.adjustmentPortions = [];
            vm.adjustmentDetail = [];

            if (appSession.location.houseTransactionDate == null) {
                $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
                vm.closingTakenDate = moment(moment().add(-1, 'days')).format($scope.format);
                $scope.maxDate = $scope.minDate;
                $scope.forceminDate = vm.closingTakenDate;
                $scope.forcemaxDate = vm.closingTakenDate;
            }
            else {
                if (vm.autopostflag)
                    $scope.minDate = appSession.location.houseTransactionDate;
                else
                    $scope.minDate = moment(appSession.location.houseTransactionDate); // appSession.location.houseTransactionDate; //  -1 or -2 based on Sysadmin Table

                if (vm.forceAdjustmentFlag == true && vm.forceAdjustmentWithDayCloseflag == true) {
                    if (moment(appSession.location.houseTransactionDate) < moment(moment().add(-1, 'days'))) {
                        vm.closingTakenDate = moment(moment().add(-1, 'days')).format($scope.format);
                        $scope.forceminDate = moment(appSession.location.houseTransactionDate).format($scope.format);
                    }
                    else {
                        vm.closingTakenDate = moment(appSession.location.houseTransactionDate).format($scope.format);
                        $scope.forceminDate = moment(appSession.location.houseTransactionDate).format($scope.format);
                    }


                    $scope.forcemaxDate = moment().format($scope.format);
                }
                else {

                    $scope.maxDate = $scope.minDate;
                    if (vm.closingstockAdjustmentFlag)
                        $scope.forceminDate = moment(moment(appSession.location.houseTransactionDate).add(-1, 'days')).format($scope.format);
                    else
                        $scope.forceminDate = moment(moment(appSession.location.houseTransactionDate).add(-30, 'days')).format($scope.format);
                    $scope.forcemaxDate = moment(moment(appSession.location.houseTransactionDate).add(-1, 'days')).format($scope.format);
                    vm.closingTakenDate = moment($scope.forcemaxDate).format($scope.format);
                }
            }



            $('input[name="adjDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: $scope.minDate,
                minDate: $scope.minDate,
                maxDate: $scope.minDate
            });


            $('input[name="closingTakenDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: $scope.forceminDate,
                minDate: moment($scope.forceminDate),
                maxDate: $scope.forcemaxDate
            });

            vm.datafromexcel = [];
            vm.lastadjustmentslist = [];

            vm.importFromExcel = function () {
                vm.loading = true;
                vm.lastadjustmentslist = [];
                adjustmentService.getLastAutoAdjustments({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    vm.lastadjustmentslist = result;
                }).finally(function () {
                    vm.loading = false;
                    vm.readfromexcelstatus = true;
                    vm.uilimit = null;
                });
            };

            vm.showDetails = function (row) {
                vm.viewAdjustmentDetail(row);
            };

            vm.viewAdjustmentDetail = function (adjustmentObj) {
                openView(adjustmentObj);
            };

            function openView(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/adjustment/adjustmentdetailViewInGrid.cshtml',
                    controller: 'tenant.views.house.transaction.adjustment.adjustmentdetailViewInGrid as vm',
                    backdrop: 'static',
                    resolve: {
                        adjustmentId: function () {
                            return objId;
                        }
                    }
                });
            }


            vm.cancelreaddatafromexcel = function () {

                if (vm.forceAdjustmentFlag == true || vm.closingstockAdjustmentFlag == true) {
                    vm.cancel();
                    return;
                }
                vm.readfromexcelstatus = false;
            }

            vm.generateStockTakenTemplate = function () {
                vm.loading = true;
                housereportService.getStockTakenReportExcel({
                    locationRefId: vm.defaultLocationId,
                    stockTakenDate: vm.closingTakenDate,
                    isHighValueItemOnly: vm.isHighValueItemOnly,
                    multipleUomAllowed: true
                }).success(function (result) {
                    app.downloadTempFile(result);
                }).finally(function () {
                    vm.loading = false;
                    vm.cancelreaddatafromexcel();
                });
            };

            vm.generateClosingStockTakenTemplate = function (argExcel, argData) {
                vm.loadinfordayclose = true;
                vm.loading = true;
                housereportService.getClosingStockTakenReportExcel({
                    locationRefId: vm.defaultLocationId,
                    stockTakenDate: vm.closingTakenDate,
                    isHighValueItemOnly: vm.isHighValueItemOnly
                }).success(function (result) {
                    if (argExcel == true)
                        app.downloadTempFile(result.excelFile);

                    if (result.errorMessage != null && result.errorMessage != '' && argData == true) {
                        abp.message.error(result.errorMessage);
                    }
                    else if (result.closingAdjustmentDtos !== null && argData == true) {
                        vm.sno = 0;
                        vm.adjustmentPortions = [];
                        angular.forEach(result.closingAdjustmentDtos, function (value, key) {
                            vm.sno = vm.sno + 1;
                            vm.adjustmentPortions.push({
                                'sno': vm.sno,
                                'materialRefId': value.materialRefId,
                                'materialName': value.materialName,
                                'adjustmentQty': value.differenceQuantity,
                                'adjustmentMode': value.adjustmentStatus,
                                'clBalance': value.clBalance,
                                'currentStock': value.currentStock,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'defaultUnitId': value.defaultUnitId,
                                'defaultUnitName': value.defaultUnitName
                            })
                        });

                        vm.uilimit = null;
                        //vm.adjustmentPortions = result.closingAdjustmentDtos;
                        vm.adjustment.tokenRefNumber = 888;
                        vm.adjustment.adjustmentRemarks = vm.closingTakenDate + ' - ' + app.localize("StockTakenDifference");
                        vm.readfromexcelstatus = false;
                        vm.autolock = true;
                        vm.excelautopost = true;
                        if (vm.forceAdjustmentFlag && vm.forceAdjustmentWithDayCloseflag) {
                            vm.adjustment.adjustmentDate = vm.closingTakenDate;
                            abp.notify.warn(app.localize('ForceCloseWarning', moment(vm.closingTakenDate).format($scope.format)));
                        }
                    }
                }).finally(function () {
                    vm.loading = false;
                    vm.loadinfordayclose = false;
                });
            };


            vm.uploader = new fileUploader({
                url: abp.appPath + 'Import/ImportStockAdjustmentBasedOnStockTakenMultipleSupplierAliasNameReport',
                formData: [
                    {
                        "closingStockDate": vm.closingTakenDate
                    },
                    {
                        "locationRefId": vm.defaultLocationId
                    },
                    {
                        "forceAdjustmentFlag": vm.forceAdjustmentFlag
                    },
                    {
                        "stockEntryOnlyFlag": false
                    }
                ],
                queueLimit: 1,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        if ('|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ImportTemplate_Warn_FileType'));
                            return false;
                        }
                        return true;
                    }
                }]
            });


            vm.uploader.onErrorItem = function (fileItem, response, status, headers) {
                if (response.success) {
                    // Do nothing
                } else {
                    abp.message.error(response);
                    abp.notify.error(response);
                    vm.loading = false;
                    vm.saving = false;
                    return;
                }
            };

            vm.readdatafromexcel = function () {
                if (vm.uploader.queue.length == 0) {
                    abp.notify.warn(app.localize('YouAreNotSelectAFile'));
                    return;
                }
                vm.uploader.uploadAll();
            };

            vm.uploadfunction = function () {
                vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {

                    vm.importResult = response.result;
                    vm.datafromexcel = vm.importResult.materialStockList;
                    vm.errorList = vm.importResult.errorList;
                    if (vm.datafromexcel == null) {
                        if (response.error !== null) {
                            abp.message.warn(response.error.message);
                            vm.readfromexcelstatus = false;
                            vm.cancel();
                            return;
                        }
                    }

                    if (vm.datafromexcel !== null) {
                        vm.sno = 0;
                        vm.adjustmentPortions = [];
                        vm.autolock = true;
                        vm.excelautopost = true;
                        angular.forEach(vm.datafromexcel, function (value, key) {
                            vm.sno = vm.sno + 1;
                            vm.adjustmentPortions.push({
                                'sno': vm.sno,
                                'materialRefId': value.materialRefId,
                                'materialName': value.materialName,
                                'adjustmentQty': value.differenceQuantity,
                                'adjustmentMode': value.adjustmentStatus,
                                'clBalance': value.clBalance,
                                'currentStock': value.currentStock,
                                'changeunitflag': false,
                            })
                        });
                        vm.adjustment.tokenRefNumber = 777;
                        vm.adjustment.adjustmentRemarks = app.localize("AutoPostFromExcel");
                    }
                    else {
                        vm.excelautopost = false;
                    }
                    vm.readfromexcelstatus = false;
                };
            }

            vm.adjustmentPortions = [];
            vm.getDataForStockAdjustmentExcel = function () {

                if (vm.closingTakenDate == null) {
                    abp.notify.warn(app.localize("SelectStockTakenDateErr"));
                    return;
                }

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/adjustment/importStockTakenModal.cshtml',
                    controller: 'tenant.views.house.transaction.adjustment.importStockTakenModal as vm',
                    resolve: {
                        locationRefId: function () {
                            return vm.defaultLocationId;
                        },
                        allItems: function () {
                            return vm.adjustmentPortions;
                        },
                        stockTakenDate: function () {
                            return vm.closingTakenDate;
                        },
                        isHighValueItemOnly: function () {
                            return vm.isHighValueItemOnly;
                        },
                        forceAdjustmentFlag: function () {
                            return vm.forceAdjustmentFlag;
                        },
                        stockEntryOnlyFlag: function () {
                            return false;
                        }

                    }
                });

                modalInstance.result.then(function (result) {
                    if (result != null) {
                        vm.uilimit = null;
                        vm.adjustmentPortions = result;
                        vm.adjustment.tokenRefNumber = 888;
                        vm.adjustment.adjustmentRemarks = vm.closingTakenDate + ' - ' + app.localize("StockTakenDifference");
                        vm.readfromexcelstatus = false;
                        vm.autolock = true;
                        vm.excelautopost = true;
                        if (vm.forceAdjustmentFlag && vm.forceAdjustmentWithDayCloseflag) {
                            vm.adjustment.adjustmentDate = vm.closingTakenDate;
                            abp.notify.warn(app.localize('ForceCloseWarning', moment(vm.closingTakenDate).format($scope.format)));
                        }
                    }
                });


            };


            vm.saving = false;

            vm.checkTokenRefEntry = function () {
                if (vm.autolock == false) {
                    //222   -   NegativeStock As Zero
                    //333   -   Purchase Yield Adjustment References
                    //444   -   
                    //999   -   Day Close Wastages
                    //222 - Negative Stock As Zero, 333 - PurchaseYield Difference, 444 - Menu Wastage, 555 - CompWastage, 666 - OnHandInitialSetup, 777 - AutoPostFromExcel, 888 - StockTakenDifference, 999 - TransferReceivedDifference
                    if (parseFloat(vm.adjustment.tokenRefNumber) == 222 || parseFloat(vm.adjustment.tokenRefNumber) == 333 || parseFloat(vm.adjustment.tokenRefNumber) == 444 || parseFloat(vm.adjustment.tokenRefNumber) == 555 || parseFloat(vm.adjustment.tokenRefNumber) == 777 || parseFloat(vm.adjustment.tokenRefNumber) == 888 || parseFloat(vm.adjustment.tokenRefNumber) === 666 || parseFloat(vm.adjustment.tokenRefNumber) == 999) {
                        abp.message.warn(app.localize('AdjRefNumberErr'));
                        vm.adjustment.tokenRefNumber = null;
                        $("#TokenRefNumber").focus();
                        return;
                    }
                }
            }

            vm.save = function (argSaveOption) {
                vm.checkTokenRefEntry();
                vm.existallElements();
                if ($scope.existall == false)
                    return;
                vm.saving = true;

                if (vm.excelautopost == true) {
                    vm.adjustment.closingStockDate = vm.closingTakenDate;
                }
                vm.adjustmentDetail = [];

                var errorFlag = false;

                var errMessage = "";

                angular.forEach(vm.adjustmentPortions, function (value, key) {
                    if (vm.isUndefinedOrNull(value.adjustmentQty)) {
                        //do nothing
                    }
                    else {
                        var closingStock = null;
                        var stockEntry = null;
                        if (vm.excelautopost) {
                            stockEntry = value.currentStock;
                            closingStock = value.clBalance;
                        }

                        vm.adjustmentDetail.push({
                            'sno': value.sno,
                            'adjustmentRefIf': value.adjustmentRefIf,
                            'materialRefId': value.materialRefId,
                            'adjustmentQty': value.adjustmentQty,
                            'adjustmentMode': value.adjustmentMode,
                            'adjustmentApprovedRemarks': value.adjustmentApprovedRemarks,
                            'unitRefId': value.unitRefId,
                            'changeunitflag': false,
                            'stockEntry': value.currentStock,
                            'closingStock': value.clBalance,
                        });
                    }

                    if (value.adjustmentQty < 0) {
                        errorFlag = true;
                        abp.notify.warn(app.localize('NegativeValueNotAllowed'));
                        errMessage = app.localize('NegativeValueNotAllowed');
                    }
                    if (value.materialRefId == null || value.materialRefId == 0) {
                        errorFlag = true;
                        abp.notify.warn(app.localize('Material'));
                        errMessage = app.localize('Material');
                    }

                    var lastelement = value;

                    if (lastelement.unitRefId == 0 || lastelement.unitRefId == null) {
                        errorFlag = true;
                        abp.notify.info(app.localize('UnitErr', lastelement.materialName));
                    }

                    if (vm.isUndefinedOrNull(lastelement.materialRefId) || lastelement.materialRefId == 0) {
                        abp.notify.info(app.localize('Material') + ' ' + lastelement.materialName);
                        errorFlag = true;
                    }

                    if (vm.isUndefinedOrNull(lastelement.adjustmentQty) || lastelement.adjustmentQty < 0) {
                        abp.notify.info(app.localize('InvalidAdjustmentQty', lastelement.materialName));
                        errorFlag = true;
                    }

                    if (vm.isUndefinedOrNull(lastelement.materialName) || lastelement.materialName == '' || lastelement.materialName == null) {
                        abp.notify.info(app.localize('Material'));
                        errorFlag = true;
                    }

                    if (!(vm.forceAdjustmentFlag && vm.forceAdjustmentWithDayCloseflag && !vm.closingstockAdjustmentFlag)) {
                        if (vm.adjustment.tokenRefNumber != 999 && vm.adjustment.tokenRefNumber != 888) {
                            if (lastelement.adjustmentMode != app.localize('Excess') && lastelement.adjustmentMode != app.localize('Wastage')) {
                                vm.refmenuMappedMaterials.some(function (refdata, refkey) {
                                    if (refdata.materialRefId == lastelement.materialRefId) {
                                        var d1 = moment().format('YYYY-MMM-DD');
                                        var d2 = moment(vm.adjustment.adjustmentDate).format('YYYY-MMM-DD')
                                        if (d1 != d2) {
                                            errorFlag = true;
                                            abp.notify.warn(app.localize('AutoSalesAdjustmentError', refdata.posRefName, refdata.materialRefName));
                                            errMessage = app.localize('AutoSalesAdjustmentError', refdata.posRefName, refdata.materialRefName)
                                        }
                                        else {
                                            abp.notify.info(app.localize('AutoSalesAdjustmentInfo', refdata.posRefName, refdata.materialRefName));
                                        }
                                        return true;
                                    }
                                });
                            }
                        }
                    }
                });

                if (errorFlag == true) {
                    abp.message.info(errMessage);
                    vm.saving = false;
                    return
                }

                if (vm.adjustmentDetail.length == 0) {
                    abp.message.info(app.localize('NoAdjustmentDetailFound'));
                    vm.saving = false;
                    return
                }

                var successFlag = false;
                adjustmentService.createOrUpdateAdjustment({
                    adjustment: vm.adjustment,
                    adjustmentDetail: vm.adjustmentDetail,
                    forceAdjustmentFlag: vm.forceAdjustmentFlag,
                    closingStockFlag: vm.closingstockAdjustmentFlag,
                    closingStockDate: vm.closingTakenDate
                }).success(function (result) {
                    if (result == null) {
                        successFlag = false;
                        return;
                    }
                    successFlag = true;
                    abp.notify.info(app.localize('Adjustment') + app.localize('SavedSuccessfully'));
                    abp.message.success(app.localize('AdjustmentSuccessMessage'));

                    if (vm.autopostflag || vm.dayCloseCallFlag) {
                        $rootScope.settings.layout.pageSidebarClosed = false;
                        if (moment(appSession.location.houseTransactionDate).format("YYYY-MM-DD") == moment().format("YYYY-MM-DD"))
                            $state.go('tenant.dashboard');
                        else
                            $state.go("tenant.dayclose");
                        vm.saving = false;
                        vm.loading = false;
                        return;
                    }

                    if (argSaveOption == 2) {
                        vm.cancel();
                        return;
                    }
                }).finally(function (result) {

                    if (successFlag == false) {
                        vm.saving = false;
                        return;
                    }

                    vm.saving = false;

                    if (argSaveOption == 3) {
                        vm.loading = true;
                        vm.saving = true;
                        daycloseService.setCloseDay(
                            {
                                locationRefId: vm.defaultLocationId,
                                transactionDate: moment(vm.closingTakenDate).format($scope.format)
                            }).success(function (result) {
                                abp.notify.info(app.localize("DayCloseSuccess"));
                                appSession.location.houseTransactionDate = result.currentDate;
                                if (moment(result.currentDate).format("YYYY-MM-DD") == moment().format("YYYY-MM-DD")) {
                                    $rootScope.settings.layout.pageSidebarClosed = false;
                                    $state.go('tenant.dashboard');
                                }
                                else {
                                    init();
                                }
                            }).finally(function () {
                                vm.loading = false;
                                vm.saving = false;
                                vm.cancel();
                            });
                    }
                    else {
                        vm.cancel();
                    }
                });
            };

            function resetField() {
                vm.adjustmentPortions = [];
                vm.adjustmentDetail = [];
                vm.adjustment.tokenRefNumber = '';
                vm.adjustment.adjustmentRemarks = '';
                vm.adjustment.adjustmentDate = '';
                vm.adjustment.locationRefId = '';
                vm.adjustmentDetail.materialRefId = '';
                vm.adjustmentDetail.adjustmentMode = '';
                vm.adjustmentDetail.adjustmentQty = '';
                vm.sno = 0;
                $scope.checked = false;
                vm.addPortion();
            }


            vm.addPortion = function () {

                if (vm.autolock == true) {
                    abp.notify.info(app.localize('AutomaticAdjustmentError'));
                    return;
                }

                if (vm.sno > 0) {

                    if (vm.existallElements() == false)
                        return;

                    var errorFlag = false;

                    if (vm.adjustmentPortions.length <= 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }
                    else {
                        var lastelement = vm.adjustmentPortions[vm.adjustmentPortions.length - 1];

                        if (vm.isUndefinedOrNull(lastelement.materialRefId) || lastelement.materialRefId == 0) {
                            abp.notify.warn(app.localize('Material'));
                            return;
                        }

                        if (vm.isUndefinedOrNull(lastelement.materialName) || lastelement.materialName == '' || lastelement.materialName == null) {
                            abp.notify.warn(app.localize('Material'));
                            return;
                        }

                        if (lastelement.unitRefId == 0 || lastelement.unitRefId == null) {
                            abp.notify.warn(app.localize('UnitErr', lastelement.materialName));
                            return;
                        }

                        if (vm.isUndefinedOrNull(lastelement.adjustmentQty) || lastelement.adjustmentQty == 0) {
                            abp.notify.warn(app.localize('InvalidAdjustmentQty', lastelement.materialName));
                            return;
                        }

                        if (vm.isUndefinedOrNull(lastelement.adjustmentMode) || lastelement.adjustmentMode == '' || lastelement.adjustmentMode == null) {
                            abp.notify.warn(app.localize('AdjustmentMode'));
                            return;
                        }

                        if (lastelement.adjustmentMode != app.localize('Excess') && lastelement.adjustmentMode != app.localize('Wastage') ) {
                            vm.refmenuMappedMaterials.some(function (refdata, refkey) {
                                if (refdata.materialRefId == lastelement.materialRefId) {
                                    if (moment().format('YYYY-MMM-DD') != moment(vm.adjustment.adjustmentDate).format('YYYY-MMM-DD')) {
                                        errorFlag = true;
                                        abp.notify.warn(app.localize('AutoSalesAdjustmentError', refdata.posRefName, refdata.materialRefName));
                                    }
                                    else {
                                        abp.notify.info(app.localize('AutoSalesAdjustmentInfo', refdata.posRefName, refdata.materialRefName));
                                    }
                                    return true;
                                }
                            });
                        }
                    }

                    if (vm.refmaterial.length == vm.adjustmentPortions.length) {
                        errorFlag = true;
                        abp.notify.warn(app.localize('AllTheMaterialAdded'));
                        return;
                    }


                    if (errorFlag) {
                        vm.saving = false;
                        vm.loading = false;
                        $scope.existall = false;
                        return;
                    }

                }


                vm.sno = vm.sno + 1;

                vm.adjustmentPortions.push({
                    'sno': vm.sno, 'materialRefId': 0, 'materialName': '', 'adjustmentQty': '', 'adjustmentApprovedRemarks': '', 'unitRefId': 0, 'unitRefName': '',
                    'defaultUnitId': '', 'defaultUnitName': '', 'changeunitflag': false,
                });
            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex) {
                if (vm.autolock == true) {
                    abp.notify.info(app.localize('AutomaticAdjustmentError'));
                    return;
                }
                if (vm.adjustmentPortions.length > 1) {
                    vm.adjustmentPortions.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                }
                else {
                    abp.notify.info(app.localize('MinimumOneDetail'));
                    return;
                }
            }

            $scope.funcAdjustmentDate = function (selDt) {

                if (!vm.isUndefinedOrNull(selDt))
                    vm.setMaxTokenNumber(selDt);
            }

            vm.setMaxTokenNumber = function (val) {
                adjustmentService.getMaxTokenNumber(
                    moment(val).format("DD-MMM-YYYY")
                ).success(function (result) {
                    vm.adjustment.tokenRefNumber = result;
                });
            };

            $scope.func = function (data, val, obj) {
                var forLoopFlag = true;

                angular.forEach(vm.adjustmentPortions, function (value, key) {

                    if (forLoopFlag) {
                        if (angular.equals(val.materialRefName, value.materialName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });



                if (!forLoopFlag) {
                    data.materialRefId = 0;
                    return;
                }
                data.materialName = val.materialRefName;
                //data.unitRefId = val.issueUnitId;
                //data.unitRefName = val.issueUnitName;
                data.unitRefId = val.stockAdjustmentUnitId;
                data.unitRefName = val.stockAdjustentUnitRefName;
                data.defaultUnitId = val.defaultUnitId;
                data.defaultUnitName = val.defaultUnitName;
            };


            vm.existallElements = function () {
                var errMessage = '';

                if (vm.adjustment.tokenRefNumber == null) {
                    errMessage = errMessage + app.localize("AdjustmentTokenRefErr");
                }
                if (vm.adjustment.adjustmentRemarks == null || vm.adjustment.adjustmentRemarks == "") {
                    errMessage = errMessage + " " + app.localize("AdjustmentRemarksErr");
                }

                if (errMessage == '') {
                    $scope.existall = true;
                    return true;
                }
                else {
                    $scope.existall = false;
                    abp.message.warn(errMessage);
                    return false;
                }

            };

            vm.cancel = function () {
                $rootScope.settings.layout.pageSidebarClosed = false;

                if (vm.forceAdjustmentFlag == true || vm.closingstockAdjustmentFlag == true) {
                    if (vm.dayCloseCallFlag == true) {
                        $state.go("tenant.dayclose");
                    }
                    else {
                        $state.go("tenant.closingstockadjustment");
                    }
                }
                else {
                    $state.go("tenant.adjustment");
                }

            };


            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.setMaxTokenNumber = function (val) {
                adjustmentService.getMaxTokenNumber(
                    moment(val).format("DD-MMM-YYYY")
                ).success(function (result) {
                    vm.adjustment.tokenRefNumber = result;
                });
            };

            vm.reflocation = [];

            function fillDropDownLocation() {
                commonLookupService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;


                });
            }

            vm.refmaterial = [];
            vm.tempRefMaterial = [];

            vm.fillMaterialBasedOnRecipt = function (selValue) {
                issueService.getMaterialForGivenRecipeCombobox({
                    locationRefId: vm.defaultLocationId,
                    recipeRefId: selValue
                }).success(function (result) {
                    vm.refmaterial = result;
                    vm.tempRefMaterial = result;
                });
            }




            vm.importFromAuto = function () {
                vm.loading = true;

                if (vm.existdata != '' && vm.existdata != null) {
                    vm.autopostdetail = JSON.parse(vm.existdata);
                    vm.autolock = true;
                }
                else {
                    vm.autopostdetail = null;
                }

                if (vm.autopostdetail.length > 0) {
                    vm.sno = 0;
                    vm.autolock = true;
                    angular.forEach(vm.autopostdetail, function (value, key) {
                        vm.sno = vm.sno + 1;

                        vm.adjustmentPortions.push({
                            'sno': vm.sno,
                            'materialRefId': value.materialRefId,
                            'materialName': value.materialName,
                            'adjustmentQty': value.adjustmentQty,
                            'adjustmentMode': value.adjustmentMode,
                            'adjustmentApprovedRemarks': value.adjustmentApprovedRemarks,
                            'changeunitflag': false,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'defaultUnitId': value.defaultUnitId,
                            'defaultUnitName': value.defaultUnitName

                        })
                    });

                }

                vm.loading = false;
            }



            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            function init() {

                abp.ui.setBusy('#adjustmentForm');
                vm.loading = true;
                vm.fillMaterialBasedOnRecipt(null);
                fillDropDownLocation();

                adjustmentService.getAdjustmentForEdit({
                    Id: vm.adjustmentId
                }).success(function (result) {
                    vm.adjustment = result.adjustment;
                    vm.adjustment.locationRefId = vm.defaultLocationId;

                    if (vm.adjustment.id === null)
                        vm.adjustment.tokenRefNumber = '';


                    if (result.adjustment.id != null) {
                        vm.uilimit = null;
                        vm.adjustmentDetail = result.adjustmentDetail;

                        vm.adjustment.adjustmentDate = moment(result.adjustment.adjustmentDate).format($scope.format);

                        if (appSession.location.houseTransactionDate == null) {
                            vm.adjustment.adjustmentDate = moment().add(-1, 'days');
                        }
                        else {
                            vm.adjustment.adjustmentDate = moment(appSession.location.houseTransactionDate).format($scope.format); //moment().add(-1, 'days');

                            //$scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
                            //vm.adjustment.adjustmentDate = $scope.minDate;


                        }

                        $scope.minDate = vm.adjustment.adjustmentDate;



                        $('input[name="adjDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: vm.adjustment.adjustmentDate,
                            minDate: $scope.minDate,
                            maxDate: vm.adjustment.adjustmentDate// moment().format($scope.format)
                        });

                        angular.forEach(result.adjustmentDetail, function (value, key) {

                            vm.adjustmentPortions.push({
                                'sno': value.sno,
                                'materialRefId': value.materialRefId,
                                'materialName': value.materialName,
                                'adjustmentQty': value.adjustmentQty,
                                'adjustmentMode': value.adjustmentMode,
                                'changeunitflag': false,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'defaultUnitId': value.defaultUnitId,
                                'defaultUnitName': value.defaultUnitName
                            })
                        });
                    }


                    else {
                        vm.checkCloseDay();


                        if (vm.autopostdetail == null) {
                            vm.addPortion();
                            vm.uilimit = 50;
                        }
                        else {
                            if (vm.autopostdetail.length > 0) {
                                vm.loading = true;
                                vm.uilimit = null;
                                vm.sno = 0;
                                vm.adjustment.tokenRefNumber = 999;
                                vm.adjustment.adjustmentRemarks = app.localize("DayCloseWastage");
                                vm.autolock = true;

                                angular.forEach(vm.autopostdetail, function (value, key) {
                                    vm.sno = vm.sno + 1;

                                    vm.adjustmentPortions.push({
                                        'sno': vm.sno,
                                        'materialRefId': value.materialRefId,
                                        'materialName': value.materialName,
                                        'adjustmentQty': value.adjustmentQty,
                                        'adjustmentMode': value.adjustmentMode,
                                        'adjustmentApprovedRemarks': value.adjustmentApprovedRemarks,
                                        'changeunitflag': false,
                                        'unitRefId': value.unitRefId,
                                        'unitRefName': value.unitRefName,
                                        'defaultUnitId': value.defaultUnitId,
                                        'defaultUnitName': value.defaultUnitName
                                    })
                                });

                                vm.loading = false;
                                //vm.addPortion();

                                //vm.adjustmentPortions = vm.autopostdetail;
                            }
                            else {
                                vm.addPortion();
                                vm.uilimit = 50;
                            }
                        }
                    }
                }).finally(function () {
                    vm.loading = false;
                    if (vm.dayCloseCallFlag == true) {
                        vm.generateClosingStockTakenTemplate(false, true)
                    }
                });
                abp.ui.clearBusy('#adjustmentForm');


                if (vm.permissions.hideStock == true) {
                    abp.notify.error(app.localize('HideStockMessage'));
                }
            }

            vm.refmenuMappedMaterials = [];
            vm.menuMappingList = function () {
                vm.refmenuMappedMaterials = [];
                materialmenumappingService.getMappedMenuList({
                }).success(function (result) {
                    vm.refmenuMappedMaterials = result;
                });

            }

            vm.menuMappingList();

            init();

            vm.checkCloseDay = function () {
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id === 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                    vm.adjustment.adjustmentDate = moment(result.accountDate).format($scope.format);
                    $scope.minDate = result.accountDate;

                    $('input[name="adjDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: $scope.minDate,
                        minDate: $scope.minDate,
                        maxDate: $scope.minDate//moment()
                    });



                }).finally(function (result) {

                });
            }

            vm.changeUnit = function (data) {
                if (!vm.multipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

            vm.unitReference = function (selected, data) {
                data.unitRefId = selected.value;
                data.unitRefName = selected.displayText;
                data.changeunitflag = false;
            };

            vm.detailDataShown = function (data) {
                var modalInstance1 = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/adjustment/drillDownLevel.cshtml',
                    controller: 'tenant.views.house.transaction.adjustment.drillDownLevel as vm',
                    backdrop: 'static',
                    resolve: {
                        materialDetail: function () {
                            return data;
                        },
                    }
                });

                modalInstance1.result.then(function (result) {
                    //vm.getAll();
                });
            };

            if (vm.forceAdjustmentFlag || vm.closingstockAdjustmentFlag)
                vm.importFromExcel();


            //  Manual Reason Modal Start
            vm.openManualReason = function (data, argColumnName) {
                vm.loading = true;
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/manualreason/manualReason.cshtml',
                    controller: 'tenant.views.house.master.manualreason.manualReason as vm',
                    backdrop: 'static',
                    resolve: {
                        manualreasonId: function () {
                            return null;
                        },
                        selectOnly: function () {
                            return true;
                        },
                        generalIncluded: function () {
                            return true;
                        },
                        defaultManualReasonCategoryRefId: function () {
                            return 5;
                        }
                        // General = 99,
                        //PurchaseOrder = 1,
                        //Receipt = 2,
                        //Invoice = 3,
                        //PurchaseReturn = 4,
                        //Adjustment = 5,
                        //MenuWastage = 6
                    }
                });

                modalInstance.result.then(function (result) {
                    if (!vm.isUndefinedOrNull(result)) {
                        if (argColumnName == 'remarks')
                            data.remarks = result;
                        else if (argColumnName == 'adjustmentRemarks') {
                            data.adjustmentRemarks = result;
                        }
                        else if (argColumnName == 'adjustmentApprovedRemarks') {
                            data.adjustmentApprovedRemarks = result;
                        }
                    }
                }, function (result) {
                    vm.loading = false;
                });


                modalInstance.closed.then(function (result) {
                  
                }, function (result) {
                    vm.loading = false;
                });
                vm.loading = false;
            }
            //  Manual Reason Modal End

        }
    ]);
})();