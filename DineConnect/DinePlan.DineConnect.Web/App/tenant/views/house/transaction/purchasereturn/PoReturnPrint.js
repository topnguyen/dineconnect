﻿(function () {
    appModule.controller('tenant.views.house.transaction.purchasereturn.PoReturnPrint', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.purchaseReturn', 'appSession', 'abp.services.app.company',
        'abp.services.app.supplier', 'abp.services.app.location', 
        function ($scope, $filter, $state, $stateParams, $modal, uiGridConstants,
            purchasereturnService, appSession, companyService, supplierService, locationService) {

            var vm = this;
            vm.printmailflag = true;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
						vm.defaultLocationId = appSession.user.locationRefId;

						vm.connectdecimals = appSession.connectdecimals;
						vm.housedecimals = appSession.housedecimals;

						vm.purchasereturnId = $stateParams.printid;

            vm.headerline = "~~~~~~~~~~~~~~~~~~~~";

            vm.sendMail = function () {
                vm.potext = '';
                vm.loading = true;
                purchasereturnService.sendPruchaseOrderEmail(
                {
                    purchaseReturnId: vm.purchasereturnId,
                    purchaseReturnHTML: vm.potext
                }).success(function (result) {
                  
                    abp.message.success(app.localize('MailedSuccessfully'));
                    abp.notify.success(app.localize('MailedSuccessfully'));
                }).finally(function () {
                    vm.loading = false;
                    });
            }

            vm.getData = function () {
                purchasereturnService.getPurchaseReturnForEdit({
                    Id: vm.purchasereturnId
                }).success(function (result) {
                    vm.purchaseReturn = result.purchaseReturn;

                    vm.purchaseReturnDetail = result.purchaseReturnDetail;
                    if (vm.purchaseReturn.locationRefId != vm.defaultLocationId) {
                        $state.go("tenant.purchasereturn");
                        return;
                    }

                    vm.purchaseDetailPortion = [];

                    vm.sno = 0;
                    vm.subtotalamount = 0;
                    vm.subtaxamount = 0;
                    vm.subnetamount = 0;

                    angular.forEach(vm.purchaseReturnDetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.subtotalamount = vm.subtotalamount + value.totalAmount;
                        vm.subtaxamount = vm.subtaxamount + value.taxAmount;
                        vm.subnetamount = vm.subnetamount + value.netAmount;

                        vm.discFlag = null;
                        if (value.discountOption == 'F')
                            vm.discFlag = false;
                        else if (value.discountOption == 'P')
                            vm.discFlag = true;
                        vm.purchaseDetailPortion.push({
                            'sno': vm.sno,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'quantity': value.quantity,
                            'uom': value.uom,
                            'unitRefName': value.unitRefName,
                            'unitRefId' : value.unitRefId,
                            'price': value.price,
                            'discountOption': vm.discFlag,
                            'discountValue': value.discountValue,
                            'totalAmount': value.totalAmount,
                            'taxAmount': value.taxAmount,
                            'netAmount': value.netAmount,
                            'remarks': value.remarks,
                            'currentInHand': value.currentInHand,
                            'alreadyOrderedQuantity': value.alreadyOrderedQuantity,
                            'taxlist': value.taxForMaterial,
                            'supplierMaterialAliasName': value.supplierMaterialAliasName
                        });
                    });

                    //userService.getUserForEdit({
                    //    Id: vm.purchaseReturn.creatorUserId
                    //}).success(function (result) {
                    //    vm.user = result.user;
                    //});

                    locationService.getUserInfoBasedOnUserId({
                        id: vm.purchaseReturnDetail[0].creatorUserId
                    }).success(function (result) {
                        vm.user = result;
                    });

                    supplierService.getSupplierForEdit({
                        Id: vm.purchaseReturn.supplierRefId
                    }).success(function (result) {
                        vm.supplier = result.supplier;
                    });

                    locationService.getLocationForEdit({
                        Id: vm.purchaseReturn.locationRefId
                    }).success(function (result) {
                        vm.billinglocation = result.location;
                    });

                    //locationService.getLocationForEdit({
                    //    Id: vm.purchaseReturn.shippingLocationId
                    //}).success(function (result) {
                    //    vm.shippinglocation = result.location;
                    //});

                    locationService.getLocationForEdit({
                        Id: vm.purchaseReturn.locationRefId
                    }).success(function (result) {
                        vm.locations = result.location;
                            if(vm.locations != null )
                            {
                                    companyService.getCompanyForEdit({
                                        Id: vm.locations.companyRefId
                                    }).success(function (result) {
                                        vm.companyProfilePictureId = result.company.companyProfilePictureId;
                                    });
                            }
                    });

                });
            }

            vm.getData();

        }]);
})();

