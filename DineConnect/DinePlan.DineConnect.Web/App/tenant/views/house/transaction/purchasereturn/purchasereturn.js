﻿
(function () {
    appModule.controller('tenant.views.house.transaction.purchasereturn.purchasereturn', [
        '$scope', '$filter', '$uibModal','$state', '$stateParams', 'abp.services.app.purchaseReturn', 'appSession', 'abp.services.app.supplierMaterial', 'abp.services.app.location', 'abp.services.app.material', 'abp.services.app.unitConversion', 'abp.services.app.invoice', 'abp.services.app.dayClose',
        function ($scope, $filter, $modal, $state, $stateParams, purchasereturnService, appSession, suppliermaterialService, locationService, materialService, unitconversionService, invoiceService, daycloseService) {
            var vm = this;
            vm.detailloading = false;
            /* eslint-disable */
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.saving = false;
            vm.purchasereturn = null;
            $scope.existall = true;
            vm.purchasereturnId = $stateParams.id;
            vm.reftax = [];
            vm.uilimit = null;

            vm.refdctranid = [];
            vm.returnPortions = [];
            vm.refunit = [];
            vm.sno = 0;
            vm.returndetail = [];
            vm.checkFinished = false;
            vm.totalTaxAmount = 0;
            vm.purchasereturnTotalAmount = 0;

            vm.permissions = {
                editPrice: abp.auth.hasPermission('Pages.Tenant.House.Transaction.PurchaseReturn.EditPrice'),
            };

            vm.ratechangeallowedflag = abp.setting.getBoolean("App.House.InvoiceSupplierRateChangeFlag");

            vm.defaultLocationId = appSession.user.locationRefId;
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;

            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.temptaxinfolist = [];

            $scope.minDate = moment().add(0, 'days');  // -30 or -60 Sysadmin Table 
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];


            $('input[name="returnDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });

            vm.temppurchasereturndetail = [];

            vm.invoicedirectcreditlink = [];
            vm.materialTotAmt = 0;

            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            $scope.func = function (data, val, obj) {
                var forLoopFlag = true;

                angular.forEach(vm.returnPortions, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialRefName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.materialRefId = 0;
                    data.materialRefName = '';
                    return;
                }
                data.materialRefName = val.materialRefName;
                data.uom = val.uom;
                data.defaultUnitId = val.defaultUnitId;
                data.unitRefName = val.uom;
                data.unitRefId = val.unitRefId;
                data.changeunitflag = false;
                data.unitRefIdForMinOrder = val.unitRefIdForMinOrder;
                data.unitRefNameForMinOrder = val.unitRefNameForMinOrder;
                data.price = parseFloat(val.materialPrice);
                data.initialprice = parseFloat(val.materialPrice);

                if (vm.permissions.editPrice) {
                    data.rateeditflag = true;
                }

                if (data.price == 0) {
                    data.rateeditflag = true;
                    data.price = null;
                }
                else {
                    data.rateeditflag = vm.ratechangeallowedflag;
                }

                vm.temptaxinfolist = [];

                angular.forEach(val.taxForMaterial, function (value, key) {
                    vm.temptaxinfolist.push({
                        'materialRefId': value.materialRefId,
                        'taxRefId': value.taxRefId,
                        'taxName': value.taxName,
                        'taxRate': value.taxRate,
                        'taxValue': value.taxValue,
                        'rounding': value.rounding,
                        'sortOrder': value.sortOrder,
                        'taxCalculationMethod': value.taxCalculationMethod,

                    });
                });
                
                data.taxForMaterial = vm.temptaxinfolist;
                data.applicableTaxes = val.applicableTaxes;
                data.unitRefId = val.unitRefId;
                data.purchasedQty = val.maximumOrderQuantity;
                data.alreadyReturnedQuantity = val.alreadyReturnedQuantity;
                data.availableQuantity = data.purchasedQty - data.alreadyReturnedQuantity;
            };

            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

            vm.unitReference = function (selected, data) {
                data.unitRefId = selected.value;
                data.uom = selected.displayText;
                data.changeunitflag = false;

                if (data.unitRefIdForMinOrder == data.unitRefId) {
                    data.price = data.initialprice;
                    vm.amtCalc(data, data.quantity, data.price);
                }
                else {
                    vm.loadingCount++;
                    materialService.getConversion({
                        baseUnitId: data.unitRefIdForMinOrder,
                        refUnitId: data.unitRefId,
                        materialRefId: data.materialRefId,
                        supplierRefId: vm.purchasereturn.supplierRefId
                    })
                        .success(function (result) {
                            var conversion = result;

                            if (conversion.id == 0 || conversion.id == null) {
                                abp.notify.info(app.localize('ConversionFactorNotExist', data.unitRefNameForMinOrder, data.unitRefName));
                                data.price = 0;
                                vm.amtCalc(data, data.quantity, data.price);
                                vm.calculateTotalVlaue(vm.returnPortions);
                            }
                            else {
                                var convFactor = conversion.conversion;
                                var converstionUnitPrice = data.initialprice * 1 / convFactor;
                                data.price = parseFloat(converstionUnitPrice.toFixed(2));
                                vm.amtCalc(data, data.quantity, data.price);
                                vm.calculateTotalVlaue(vm.returnPortions);
                            }
                        }).finally(function () {
                            vm.loadingCount--;
                        });


                    //var convFactor = 0;
                    //var convExistFlag = false;
                    //vm.refconversionunit.some(function (refdata, refkey) {
                    //    if (refdata.baseUnitId == data.unitRefIdForMinOrder && refdata.refUnitId == data.unitRefId) {
                    //        convFactor = refdata.conversion;
                    //        convExistFlag = true;
                    //        return true;
                    //    }
                    //});


                    //if (convExistFlag == false) {
                    //    abp.notify.error(app.localize('ConversionFactorNotExist', data.unitRefNameForMinOrder, data.unitRefName));
                    //    data.price = 0;
                    //    vm.amtCalc(data, data.quantity, data.price);
                    //    vm.calculateTotalVlaue(vm.invoicePortions);
                    //    return;
                    //}

                    //var converstionUnitPrice = data.initialprice * 1 / convFactor;
                    //data.price = converstionUnitPrice.toFixed(6);
                }
              
            }

            $scope.taxCalc = function (data, quantity, price) {
                if (vm.isUndefinedOrNull(quantity))
                    quantity = 0;
                if (vm.isUndefinedOrNull(price))
                    price = 0;

                data.totalAmount = parseFloat(price) * parseFloat(quantity);
                data.totalAmount = parseFloat(data.totalAmount.toFixed(2));

                data.taxAmount = 0;
                data.taxForMaterial = [];

                taxneedstoremove = [];

                if (data.applicableTaxes.length > 0)    //  Calculate Tax 
                {
                    angular.forEach(data.applicableTaxes, function (value, key) {
                        var taxCalculationOnValue = 0.0;

                        if (value.taxCalculationMethod == 'GT' || value.taxCalculationMethod == 'ST') {
                            taxCalculationOnValue = data.totalAmount;
                        }
                        else {
                            var taxrefvalueflag = false;
                            data.taxForMaterial.some(function (taxdata, taxkey) {
                                if (taxdata.taxName == value.taxCalculationMethod) {
                                    taxCalculationOnValue = taxdata.taxValue;
                                    taxrefvalueflag = true;
                                    return true;
                                }
                            });

                            if (taxrefvalueflag == false)
                                taxneedstoremove.push({ 'taxName': value.taxName });
                        }

                        if (taxCalculationOnValue > 0) {
                            var taxamt = parseFloat(taxCalculationOnValue * value.taxRate / 100);
                            taxamt = parseFloat(taxamt.toFixed(2));
                            data.taxAmount = parseFloat(data.taxAmount) + parseFloat(taxamt);
                            data.taxAmount = parseFloat(data.taxAmount.toFixed(2));

                            if (taxamt > 0) {
                                data.taxForMaterial.push
                                    ({
                                        'materialRefId': data.materialRefId,
                                        'taxRefId': value.taxRefId,
                                        'taxName': value.taxName,
                                        'taxRate': value.taxRate,
                                        'taxValue': taxamt,
                                        'rounding': value.rounding,
                                        'sortOrder': value.sortOrder,
                                        'taxCalculationMethod': value.taxCalculationMethod
                                    });
                            }
                        }
                    });
                }


                angular.forEach(taxneedstoremove, function (value, key) {
                    angular.forEach(data.applicableTaxes, function (detvalue, detkey) {
                        if (value.taxName == detvalue.taxName)
                            data.applicableTaxes.splice(detkey, 1);
                    });
                });

                data.netAmount = parseFloat(parseFloat(data.totalAmount).toFixed(2)) + parseFloat(parseFloat(data.taxAmount).toFixed(2));
                vm.calculateTotalVlaue(vm.returnPortions);
            }



            vm.amtCalc = function (data, quantity, price) {

                $scope.taxCalc(data, quantity, price);
                return;

                //if (vm.isUndefinedOrNull(quantity))
                //    quantity = 0;
                //if (vm.isUndefinedOrNull(price))
                //    price = 0;

                //data.totalAmount = parseFloat(parseFloat(parseFloat(price) * parseFloat(quantity)).toFixed(2));

                //if (data.taxForMaterial.length > 0)    //  Calculate Tax 
                //{
                //    data.taxAmount = 0;
                //    angular.forEach(data.taxForMaterial, function (value, key) {
                //        var taxCalculationOnValue = 0.0;

                //        if (value.taxCalculationMethod == 'GT' || value.taxCalculationMethod == 'ST') {
                //            taxCalculationOnValue = data.totalAmount;
                //        }
                //        else {
                //            data.taxForMaterial.some(function (taxdata, taxkey) {
                //                if (taxdata.taxName == value.taxCalculationMethod) {
                //                    taxCalculationOnValue = taxdata.taxValue;
                //                    return true;
                //                }
                //            });
                //        }

                //        var taxamt = parseFloat(taxCalculationOnValue * value.taxRate / 100);
                //        taxamt = parseFloat(taxamt.toFixed(2));
                //        data.taxAmount = parseFloat(data.taxAmount) + parseFloat(taxamt);
                //        data.taxAmount = parseFloat(data.taxAmount.toFixed(2));
                //        data.taxForMaterial[key].taxValue = taxamt;
                //    });
                //}

                //data.netAmount = parseFloat((parseFloat(data.totalAmount) + parseFloat(data.taxAmount)).toFixed(2));

                //vm.calculateTotalVlaue(vm.returnPortions);
            }

            vm.refmaterial = [];
            vm.fillDropDownMaterial = function () {
                vm.refmaterial = [];
                if (vm.invoiceRefFlag) {
                    vm.filterInvoiceList();
                    return;
                }
                vm.detailloading = true;
                vm.loading = true;
                suppliermaterialService.getMaterialBasedOnSupplierWithPrice({
                    locationRefId: vm.purchasereturn.locationRefId,
                    supplierRefId: vm.purchasereturn.supplierRefId,
                    purchaseReturnFlag: true
                }).success(function (result) {
                    vm.refmaterial = result.items;
                }).finally(function () {
                    if (vm.returnPortions.length == 0)
                        vm.addPortion();
                    vm.detailloading = false;
                    vm.loading = false;
                });
            }

            vm.reftax = [];
            vm.fillDropDownTaxes = function () {
                suppliermaterialService.getTaxForMaterial()
                    .success(function (result) {
                        vm.reftax = result;
                    }).finally(function () {

                    });
            }


            vm.getMaterialQuotePrice = function (data, supmaterial) {

                //if (data.materialRefId > 0) {
                //    if (vm.purchasereturn.id == null) {
                //        data.price = parseFloat(supmaterial.materialPrice);

                //        if (data.price == 0)
                //            data.rateeditflag = true;
                //        else
                //            data.rateeditflag = vm.ratechangeallowedflag;

                //        if (vm.permissions.editPrice) {
                //            data.rateeditflag = true;
                //        }
                //    }
                //    data.uom = supmaterial.uom;
                //}
                return parseInt(supmaterial.materialRefId);
            };

            vm.addPortion = function () {

                var errorFlag = false;
                var value = vm.returnPortions[vm.returnPortions.length - 1];

                if (vm.sno > 0) {
                    var value = vm.returnPortions[vm.returnPortions.length - 1];
                    //value = value[0];

                    if (vm.existAllElements() == false)
                        return;

                    if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.materialRefName)) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MaterialRequired'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.quantity) || value.quantity == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidOrderQty'));
                        return;
                    }

                    if ((vm.isUndefinedOrNull(value.quantity) || value.quantity > value.availableQuantity) && vm.invoiceRefFlag) {
                        errorFlag = true;

                        abp.notify.warn(app.localize('Available') + ' ' + app.localize('Quantity') + ' : ' + value.availableQuantity + ' ' + app.localize('Returned') + ' ' + app.localize('Quantity') + ' : ' + value.quantity + ' ??? ');

                        abp.notify.warn(app.localize('Already') + ' ' + app.localize('Returned') + ' ' + app.localize('Quantity') + ' : ' + value.alreadyReturnedQuantity);
                        abp.notify.warn(app.localize('Purchase') + ' ' + app.localize('Quantity') + ' : ' + value.purchasedQty);
                        return;
                    }



                    if (vm.isUndefinedOrNull(value.price) || value.price == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialPrice'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.totalAmount) || value.totalAmount == 0) {
                        errorFlag = true;
                        abp.notify.warn(app.localize('Amount'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.netAmount) || value.netAmount == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialTotalAmt'));
                        return;
                    }

                }

                vm.refunit = [];

                vm.sno = vm.sno + 1;

                vm.returnPortions.push({
                    'sno': vm.sno,
                    'dcTranid': 0,
                    'materialRefId': 0,
                    'materialRefName': '',
                    'quantity': '',
                    'price': '',
                    'totalAmount': 0,
                    'taxAmount': 0,
                    'netAmount': 0,
                    'remarks': '',
                    'taxForMaterial': null,
                    'uom': '',
                    'unitRefId': 0,
                    'unitRefName': '',
                    'defaultUnitId': 0,
                    'defaultUnitName': '',
                    'changeunitflag': false,
                    'applicableTaxes': [],
                    'materialSupplierAliasName': ''
                });

                vm.calculateTotalVlaue(vm.returnPortions);
            }
            /////------------------------------------------

            vm.existAllElements = function () {
                $scope.existall = true;
                $scope.errmessage = "";

                if (vm.purchasereturn.supplierRefId == null || vm.purchasereturn.supplierRefId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('SupplierErr');
                    $('#selsupplierRefId').focus();
                }

                if (vm.purchasereturn.returnDate == null) {
                    $scope.errmessage = $scope.errmessage + app.localize('returnDateErr');
                    $('#returnDate').focus();
                }

                if (vm.purchasereturn.referenceNumber == null || vm.purchasereturn.referenceNumber.length == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('Ref#') + app.localize('Error');
                }

                if (vm.purchasereturn.locationRefId == null || vm.purchasereturn.locationRefId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('LocationErr');
                }



                if ($scope.errmessage != "") {
                    $scope.existall = false;
                    abp.notify.warn($scope.errmessage, 'Required');
                    return false;
                }
                else
                    return true;

            };



            vm.next = function () {
                vm.checkFinished = false;
                if (vm.existAllElements() == false)
                    return;

                var value = vm.temppurchasereturndetail;

                var errorFlag = false;

                if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
                    errorFlag = true;
                    abp.notify.info(app.localize('MinimumOneDetail'));
                    return;
                }

                if (vm.isUndefinedOrNull(value.materialRefName) || value.materialRefName == 0) {
                    errorFlag = true;
                    abp.notify.info(app.localize('MaterialRequired'));
                    return;
                }

                if (vm.isUndefinedOrNull(value.quantity) || value.quantity == 0) {
                    errorFlag = true;
                    abp.notify.info(app.localize('Qty'));
                    return;
                }
                if (vm.isUndefinedOrNull(value.price) || value.price == 0) {
                    errorFlag = true;
                    abp.notify.info(app.localize('InvalidMaterialPrice'));
                    return;
                }
                if (vm.isUndefinedOrNull(value.totalAmount) || value.totalAmount == 0) {
                    errorFlag = true;
                    abp.notify.info(app.localize('InvalidMaterialTotalAmt'));
                    return;
                }

                if (vm.isUndefinedOrNull(value.dcTranid)) {
                    errorFlag = true;
                    abp.notify.info(app.localize('PleaseSelectDCTranId'));
                    return;
                }



                if (errorFlag) {
                    vm.saving = false;
                    vm.loading = false;
                    $scope.existall = false;
                }

                vm.sno = vm.sno + 1;
                var netamt = parseFloat(vm.isUndefinedOrNull(value.totalAmount) ? 0 : value.totalAmount)

                vm.returnPortions.push({
                    'sno': vm.sno,
                    'dcTranid': value.dcTranid,
                    'materialRefId': value.materialRefId,
                    'materialRefName': value.materialRefName,
                    'quantity': value.quantity,
                    'price': value.price,
                    'totalAmount': parseFloat(parseFloat(value.totalAmount).toFixed(2)),
                    'taxAmount': parseFloat(parseFloat(value.taxAmount).toFixed(2)),
                    'netAmount': parseFloat(parseFloat(netAmount).toFixed(2)),
                    'remarks': value.remarks,
                    'taxForMaterial': value.taxForMaterial,
                    'uom': value.uom,
                    'unitRefId': value.unitRefId,
                    'changeunitflag': false,
                    'applicableTaxes': value.applicableTaxes,
                    'materialSupplierAliasName': value.materialSupplierAliasName
                });

                vm.temppurchasereturndetail = [];
                vm.calculateTotalVlaue(vm.returnPortions);
            };


            //$scope.amtCalc = function (quantity, price) {
            //    if (vm.isUndefinedOrNull(quantity))
            //        quantity = 0;
            //    if (vm.isUndefinedOrNull(price))
            //        price = 0;
            //    vm.temppurchasereturndetail.totalAmount = price * quantity;
            //}

            $scope.funcMat = function (val) {
                var forLoopFlag = true;


                angular.forEach(vm.returnPortions, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    vm.temppurchasereturndetail.materialRefId = 0;
                    vm.temppurchasereturndetail.materialRefName = '';
                    return;
                }
                vm.temppurchasereturndetail.materialRefName = val;
            };

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };




            vm.calculateTotalVlaue = function (data) {
                var tempTotDiscAmt = 0;
                var tempTotInvAmt = 0;
                var tempTotTaxAmt = 0;
                var tempTotalAmount = 0;

                angular.forEach(data, function (val, key) {
                    data[key].changeunitflag = false;
                    tempTotTaxAmt = parseFloat(tempTotTaxAmt) + parseFloat(parseFloat(vm.isUndefinedOrNull(val.taxAmount) ? 0 : val.taxAmount).toFixed(2));
                    tempTotInvAmt = parseFloat(tempTotInvAmt) + parseFloat(parseFloat(vm.isUndefinedOrNull(val.netAmount) ? 0 : val.netAmount).toFixed(2));
                    tempTotalAmount = parseFloat(tempTotalAmount) + parseFloat(parseFloat(vm.isUndefinedOrNull(val.totalAmount) ? 0 : val.totalAmount).toFixed(2));
                });
                vm.purchasereturnTotalAmount = parseFloat(tempTotalAmount.toFixed(2));;
                vm.totalTaxAmount = parseFloat(tempTotTaxAmt.toFixed(2));


                vm.purchasereturn.purchaseReturnAmount = parseFloat(tempTotInvAmt.toFixed(2));
            }


            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex) {

                vm.returnPortions.splice(productIndex, 1);
                vm.sno = vm.sno - 1;
                vm.calculateTotalVlaue(vm.returnPortions);
            }


            vm.save = function () {

                if (!vm.invoiceRefFlag)
                    vm.purchasereturn.invoiceRefId = null;

                vm.calculateTotalVlaue(vm.returnPortions);

                if ($scope.existall == false)
                    return;

                if (vm.existAllElements() == false)
                    return;

                if (parseFloat(vm.purchasereturn.purchaseReturnAmount) == 0) {
                    abp.message.warn(app.localize("EmptyDetail"));
                    return;
                }

                vm.saving = true;
                vm.purchasereturndetail = [];
                vm.discFlag = 0;
                errorFlag = false;

                angular.forEach(vm.returnPortions, function (value, key) {

                    if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MaterialRequired'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.materialRefName)) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MaterialRequired'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.quantity) || value.quantity == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('Quantity'));
                        return;
                    }

                    if ((vm.isUndefinedOrNull(value.quantity) || value.quantity > value.availableQuantity) && vm.invoiceRefFlag) {
                        errorFlag = true;

                        abp.notify.warn(app.localize('Available') + ' ' + app.localize('Quantity') + ' : ' + value.availableQuantity + ' ' + app.localize('Returned') + ' ' + app.localize('Quantity') + ' : ' + value.quantity + ' ??? ');

                        abp.notify.warn(app.localize('Already') + ' ' + app.localize('Returned') + ' ' + app.localize('Quantity') + ' : ' + value.alreadyReturnedQuantity);
                        abp.notify.warn(app.localize('Purchase') + ' ' + app.localize('Quantity') + ' : ' + value.purchasedQty);
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.price) || value.price == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialPrice'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.totalAmount) || value.totalAmount == 0) {
                        errorFlag = true;
                        abp.notify.warn(app.localize('Amount'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.netAmount) || value.netAmount == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialTotalAmt'));
                        return;
                    }

                    vm.purchasereturndetail.push({
                        'sno': value.sno,
                        'materialRefId': value.materialRefId,
                        'quantity': value.quantity,
                        'price': value.price,
                        'totalAmount': value.totalAmount,
                        'taxAmount': value.taxAmount,
                        'netAmount': value.netAmount,
                        'remarks': value.remarks,
                        'taxForMaterial': value.taxForMaterial,
                        'applicableTaxes': value.applicableTaxes,
                        'uom': value.uom,
                        'unitRefId': value.unitRefId,
                        'changeunitflag': false,
                    });
                });

                if (errorFlag) {
                    vm.saving = false;
                    return;
                }

                vm.saving = true;

                purchasereturnService.createOrUpdatePurchaseReturn({
                    purchaseReturn: vm.purchasereturn,
                    purchaseReturnDetail: vm.purchasereturndetail
                }).success(function () {
                    abp.notify.info(app.localize('PurchaseReturn') + ' ' + app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {

                if (vm.purchasereturn.code == null) {
                    vm.existall = false;
                    return;
                }

                purchasereturnService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'code',
                    filter: vm.purchasereturn.code,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.purchasereturn.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.purchasereturn.code + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $state.go('tenant.purchasereturn');
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.reflocations = [];

            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocations = result.items;
                });
            }

            vm.refsupplier = [];

            function fillDropDownSupplier() {
                vm.loading = true;
                suppliermaterialService.getSupplierForCombobox({}).success(function (result) {
                    vm.refsupplier = result.items;
                    vm.loading = false;
                });
            }

            vm.checkCloseDay = function () {
                vm.loadingCount++;
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.error(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                    vm.purchasereturn.returnDate = moment(result.accountDate).format($scope.format);

                    $('input[name="returnDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.purchasereturn.returnDate,
                        minDate: vm.purchasereturn.returnDate,
                        maxDate: vm.purchasereturn.returnDate
                    });



                }).finally(function (result) {
                    vm.loadingCount--;
                });
            }

            function init() {
                fillDropDownLocation();
                fillDropDownSupplier();
                vm.fillDropDownTaxes();
                vm.detailloading = true;
                vm.errorFlag = true;

                purchasereturnService.getPurchaseReturnForEdit({
                    Id: vm.purchasereturnId
                }).success(function (result) {
                    vm.purchasereturn = result.purchaseReturn;
                    vm.errorFlag = false;
                    vm.purchasereturn.locationRefId = vm.defaultLocationId;


                    if (result.purchaseReturn.id != null) {
                        vm.uilimit = null;

                        vm.purchasereturn.returnDate = moment(result.purchaseReturn.returnDate).format($scope.format);
                        $scope.minDate = vm.purchasereturn.returnDate;

                        vm.fillDropDownMaterial();

                        $('input[name="returnDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: moment(),
                            maxDate: moment()
                        });

                        vm.purchasereturndetail = result.purchaseReturnDetail;

                        angular.forEach(vm.purchasereturndetail, function (value, key) {
                            vm.returnPortions.push({
                                'sno': value.sno,
                                'dcTranid': value.dcTranId,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'quantity': value.quantity,
                                'price': value.price,
                                'totalAmount': parseFloat(parseFloat(value.totalAmount).toFixed(2)),
                                'taxAmount': parseFloat(parseFloat(value.taxAmount).toFixed(2)),
                                'netAmount': parseFloat(parseFloat(value.netAmount).toFixed(2)),
                                'remarks': value.remarks,
                                'taxForMaterial': value.taxForMaterial,
                                'uom': value.uom,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'defaultUnitId': value.defaultUnitId,
                                'defaultUnitName': value.defaultUnitName,
                                'changeunitflag': false,
                                'applicableTaxes': value.applicableTaxes
                            });
                        });
                        vm.calculateTotalVlaue(vm.returnPortions);
                    }
                    else {
                        vm.purchasereturn.returnDate = moment().format($scope.format);
                        vm.checkCloseDay();
                        vm.uilimit = 50;
                    }
                    vm.detailloading = false;
                }).finally(function () {
                    if (vm.errorFlag == true)
                        vm.cancel();
                });

            }

            init();

            vm.refconversionunit = [];
            vm.fillUnitConversion = function () {
                unitconversionService.getAll({
                }).success(function (result) {
                    vm.refconversionunit = result.items;
                });
            }

            vm.fillUnitConversion();



            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

          

            vm.refinvoices = [];

            vm.filterInvoiceList = function () {
                vm.refinvoices = [];
                vm.refmaterial = [];

                if (!vm.invoiceRefFlag) {
                    vm.fillDropDownMaterial();
                    vm.purchasereturn.invoiceRefId = null;
                    vm.returnPortions = [];
                    vm.sno = 0;
                    vm.addPortion();
                    return;
                }

                vm.returnPortions = [];
                vm.sno = 0;

                vm.loading = true;

                var startDate = moment().add(-180, 'days');  // -30 or -60 Sysadmin Table 
                var endDate = moment();

                invoiceService.getInvoiceSearch({
                    locationRefId: vm.purchasereturn.locationRefId,
                    supplierRefId: vm.purchasereturn.supplierRefId,
                    startDate: startDate.format($scope.format),
                    endDate: endDate.format($scope.format)
                }).success(function (result) {
                    vm.purchasereturn.invoiceRefId = null;
                    vm.refinvoices = result.items;
                    vm.addPortion();
                }).finally(function () {
                    vm.loading = false;
                });

            }

            vm.fillMaterialBasedOnInvoiceSelection = function () {
                vm.refmaterial = [];
                vm.loading = true;
                invoiceService.getMaterialBasedOnSupplierAndInvoice({
                    locationRefId: vm.purchasereturn.locationRefId,
                    supplierRefId: vm.purchasereturn.supplierRefId,
                    invoiceRefId: vm.purchasereturn.invoiceRefId
                }).success(function (result) {
                    vm.refmaterial = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.openManualReason = function (data, argColumnName) {
                vm.loading = true;
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/manualreason/manualReason.cshtml',
                    controller: 'tenant.views.house.master.manualreason.manualReason as vm',
                    backdrop: 'static',
                    resolve: {
                        manualreasonId: function () {
                            return null;
                        },
                        selectOnly: function () {
                            return true;
                        },
                        generalIncluded: function () {
                            return true;
                        },
                        defaultManualReasonCategoryRefId: function () {
                            return 4;
                        }
                        // General = 99,
                        //PurchaseOrder = 1,
                        //Receipt = 2,
                        //Invoice = 3,
                        //PurchaseReturn = 4,
                        //Adjustment = 5,
                        //MenuWastage = 6
                    }
                });

                modalInstance.result.then(function (result) {
                    if (!vm.isUndefinedOrNull(result)) {
                        if (argColumnName == 'remarks')
                            data.remarks = result;
                        else if (argColumnName == 'reason') {
                            data.reason = result;
                        }
                    }
                }, function (result) {
                    vm.loading = false;
                    });


                modalInstance.closed.then(function (result) {
                    if (!vm.isUndefinedOrNull(result)) {
                        if (argColumnName == 'remarks')
                            data.remarks = result;
                        else if (argColumnName == 'reason') {
                            data.reason = result;
                        }
                    }
                }, function (result) {
                    vm.loading = false;
                });
                vm.loading = false;
            }


        }
    ]);
})();

