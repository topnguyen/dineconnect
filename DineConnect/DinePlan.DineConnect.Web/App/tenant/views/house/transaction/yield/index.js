﻿(function () {
    appModule.controller('tenant.views.house.transaction.yield.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.yield', 'appSession',
        function ($scope, $state, $modal, uiGridConstants, yieldService, appSession) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.defaultLocationId = appSession.user.locationRefId;
            vm.currentTransactinDate = appSession.location.houseTransactionDate;

            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }
            vm.isProductionAllowed = appSession.location.isProductionAllowed;
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");


            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Transaction.Yield.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Transaction.Yield.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Transaction.Yield.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.advancedFiltersAreShown = false;
            vm.dateFilterApplied = false;

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.userGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
					{
						name: app.localize('Actions'),
						enableSorting: false,
						cellTemplate:
						   "<div class=\"ui-grid-cell-contents\">" +
							    "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
							   "    <ul uib-dropdown-menu>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editYield(row.entity)\">" + app.localize("Edit") + "</a></li>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteYield(row.entity)\">" + app.localize("Delete") + "</a></li>" +
						"      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.outputYieldFeeding(row.entity)\">" + app.localize("YieldOuputEntry") + "</a></li>" +						
						"      <li><a ng-if=\"grid.appScope.permissions.create\" ng-click=\"grid.appScope.printYield(row.entity)\">" + app.localize("Print") + "</a></li>" +
							   "    </ul>" +
							   "  </div>" +
							   "</div>"
					},
                    {
                        name: app.localize('YieldReferece'),
                        field: 'requestSlipNumber'
                    },
                    {
                        name: app.localize('IssueTime'),
                        field: 'issueTime',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm\'',
                    },
                    {
                        name: app.localize('Status'),
                        field: 'status'
                    },
				    {
                        name: app.localize('CompletedTime'),
                        field: 'completedTime',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm\''
				    }

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.clear = function () {
                vm.filterText = null;
                vm.dateRangeModel = null;
                vm.dateFilterApplied = false;
                vm.requestSlipNumber = false;
                vm.getAll();

						}

						vm.printYield = function (myObject) {
							$state.go('tenant.yieldprint', {
								printid: myObject.id
							});
						};


            vm.getAll = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var requestSlipNumber = null;

                if (vm.advancedFiltersAreShown) {
                    if (vm.dateFilterApplied) {

                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                     requestSlipNumber = vm.requestSlipNumber;
                }

                yieldService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    requestSlipNumber: requestSlipNumber,
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editYield = function (myObj) {
                var issueDate = moment(myObj.issueTime).format('DD-MMM-YYYY');
                var transactionDate = moment(vm.currentTransactinDate).format('DD-MMM-YYYY');
                if (issueDate != transactionDate)
                {
                    abp.notify.warn(app.localize('IssueDateNotMatchedWithHouseTransactionDate', issueDate, transactionDate));
                    abp.notify.warn(app.localize('EditCanNotAllowed'));
                    return;
                }

                openCreateOrEditModal(myObj.id,'editinput');
            };

            vm.outputYieldFeeding = function (myObj) {
                openCreateOrEditModal(myObj.id,'editoutput');
            }

            vm.createYield = function () {
                openCreateOrEditModal(null,'createnew');
            };
            
            vm.createYieldinputandoutput = function () {
                openCreateOrEditModal(null, 'createboth');
            };

            function openCreateOrEditModal(objId,editMode) {
                $state.go('tenant.yielddetail', {
                    id: objId,
                    editMode: editMode
                });
            }

            vm.deleteYield = function (myObject) {
                var issueDate = moment(myObject.issueTime).format('DD-MMM-YYYY');
                var transactionDate = moment(vm.currentTransactinDate).format('DD-MMM-YYYY');
                if (issueDate != transactionDate) {
                    abp.notify.warn(app.localize('IssueDateNotMatchedWithHouseTransactionDate', issueDate, transactionDate));
                    abp.notify.warn('DeleteCanNotAllowed');
                    return;
                }
                abp.message.confirm(
                    app.localize('DeleteYieldWarning', myObject.requestSlipNumber),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            yieldService.deleteYield({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };


            vm.exportToExcel = function () {
                console.log("TTT");
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                 var requestSlipNumber = null;

                if (vm.advancedFiltersAreShown) {
                    if (vm.dateFilterApplied) {

                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                    requestSlipNumber = vm.requestSlipNumber;
                }

                yieldService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                     requestSlipNumber: requestSlipNumber,
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            };


            vm.getAll();

            function init() {
                vm.productionUnitExistsFlag = appSession.location.isProductionUnitAllowed;
                if (vm.productionUnitExistsFlag == true) {
                    vm.columnPrdUnitRefName =
                        {
                            name: app.localize('ProductionUnit'),
                            field: 'productionUnitRefName'
                        };
                    vm.userGridOptions.columnDefs.splice(2, 0, vm.columnPrdUnitRefName);
                }
            };

            init();

        }]);
})();

