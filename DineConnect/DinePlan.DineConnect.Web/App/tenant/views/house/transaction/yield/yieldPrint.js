﻿(function () {
    appModule.controller('tenant.views.house.transaction.yield.yieldPrint', [
			'$scope', '$filter', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.yield', 'appSession',  'abp.services.app.material',
        
			function ($scope, $filter, $state, $stateParams, $modal, uiGridConstants, yieldService, appSession,
           materialService) {

            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;
            //vm.defaultLocationId = appSession.user.locationRefId;

						vm.connectdecimals = appSession.connectdecimals;
						vm.housedecimals = appSession.housedecimals;

						vm.yieldId = $stateParams.printid;

            vm.headerline = "~~~~~~~~~~~~~~~~~~~~";

            vm.getData = function () {
							vm.loading = true;
							yieldService.getYieldForEdit({
								Id: vm.yieldId
							}).success(function (result) {
								vm.yield = result.yield;
								vm.yieldInput = result.yieldInputDetail;
								vm.yieldOutput = result.yieldOutputDetail;

								if (vm.yield.recipeRefId != null) {
									vm.recipeflag = true;
									vm.recipename = vm.yield.recipename;
								}
								else {
									vm.recipeflag = false;
									vm.recipename = app.localize("General");
								}

								$scope.dt = moment().format($scope.format);
								if (result.yield.id != null) {
									vm.yield.issueTime = moment(result.yield.issueTime).format($scope.format);

									vm.yieldInputDetailPortion = [];
									vm.yieldOutputDetailPortion = [];

									vm.sno = 0;
									angular.forEach(vm.yieldInput, function (value, key) {
										vm.sno = vm.sno + 1;
										vm.yieldInputDetailPortion.push({
											'sno': vm.sno,
											'materialRefId': value.inputMaterialRefId,
											'materialRefName': value.materialRefName,
											'inputQty': value.inputQty,
											'uom': value.uom,
											'currentInHand': value.currentInHand,
											'changeunitflag': false,
											'unitRefId': value.unitRefId,
											'unitRefName': value.unitRefName,
											'defaultUnitId': value.defaultUnitId,
											'defaultUnitName': value.defaultUnitName
										});
									});

									vm.outSno = 0;
									angular.forEach(vm.yieldOutput, function (value, key) {
										vm.outSno = vm.outSno + 1;
										vm.yieldOutputDetailPortion.push({
											'sno': vm.outSno,
											'materialRefId': value.outputMaterialRefId,
											'materialRefName': value.materialRefName,
											'outputQty': value.outputQty,
											'uom': value.uom,
											'currentInHand': value.currentInHand,
											'changeunitflag': false,
											'unitRefId': value.unitRefId,
											'unitRefName': value.unitRefName,
											'defaultUnitId': value.defaultUnitId,
											'defaultUnitName': value.defaultUnitName
										});
									});
								}
								});
            }

            vm.getData();

        }]);
})();

