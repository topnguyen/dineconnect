﻿(function () {
    appModule.controller('tenant.views.house.transaction.yield.yield', [
        '$scope', '$filter', '$timeout', '$state', '$stateParams', 'abp.services.app.yield', 'abp.services.app.commonLookup', 'appSession', 'abp.services.app.template', 'abp.services.app.material', 'abp.services.app.issue', 'abp.services.app.unitConversion',
        'abp.services.app.location', 'abp.services.app.dayClose',
        function ($scope, $filter, $timeout, $state, $stateParams, yieldService, commonLookupService,
            appSession, templateService, materialService, issueService, unitconversionService, locationService, daycloseService) {
            /* eslint-disable */
            var vm = this;
            vm.saving = false;
            vm.loadingflag = true;

            vm.yield = null;
            vm.yieldInput = [];
            vm.yieldOutput = [];
            vm.yieldStatus = null;
            vm.uilimit = null;

            vm.loadingCount = 0;

            vm.productionUnitExistsFlag = appSession.location.isProductionUnitAllowed;

            vm.refproductionunit = [];
            if (vm.productionUnitExistsFlag == true) {
                vm.refproductionunit = appSession.location.productionUnitList;
            }

            vm.yieldInputDetailPortion = [];
            vm.yieldOutputDetailPortion = [];

            vm.defaulttemplatetype = 2;

            vm.template = null;
            vm.selectedTemplate = null;

            vm.templatesave = null;

            vm.yieldId = $stateParams.id;
            vm.editMode = $stateParams.editMode;
            vm.outputeditallowed = null;
            vm.inputeditallowed = null;

            if (vm.editMode == 'createnew' || vm.editMode == 'editinput') {
                vm.outputeditallowed = false;
                vm.inputeditallowed = true;
            }
            else if (vm.editMode == 'editoutput') {
                vm.inputeditallowed = false;
                vm.outputeditallowed = true;
            }
            else {
                vm.inputeditallowed = true;
                vm.outputeditallowed = true;
            }

            vm.sno = 0;
            vm.outSno = 0;

            vm.defaultLocationId = appSession.user.locationRefId;
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");


            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['llll', 'YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.dateOptions = [];
            vm.dateOptions.push({
                'singleDatePicker': true,
                'showDropdowns': true,
                'startDate': moment(),
                'minDate': $scope.minDate,
                'maxDate': moment()
            });

            //date start

            $('input[name="issueTime"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });

            $('input[name="completedTime"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
            });


            vm.existAllElements = function () {
                $scope.errmessage = "";

                if ($('#recipeFlag').is(":checked")) {
                    if (vm.yield.recipeRefId == null || vm.yield.recipeRefId == "") {
                        $scope.errmessage = $scope.errmessage + app.localize('RecipeSelectErr');
                    }
                }
                if (vm.yield.issueTime == null) {
                    $scope.errmessage = $scope.errmessage + app.localize('issueTimeErr');
                    $('#issueTime').focus();
                }

                if (vm.yield.locationRefId == null || vm.yield.locationRefId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('LocationErr');
                }

                if (vm.yield.requestSlipNumber == null || vm.yield.requestSlipNumber == "") {
                    $scope.errmessage = $scope.errmessage + app.localize('ReferenceErr');
                }

                if ($scope.errmessage != "") {
                    abp.notify.warn($scope.errmessage, 'Required');
                    return false;
                }
                else
                    return true;

            };


            vm.addPortion = function () {

                var errorFlag = false;


                if (vm.sno > 0) {

                    if (vm.productionUnitExistsFlag == true && vm.isUndefinedOrNull(vm.yield.productionUnitRefId) == true) {
                        abp.notify.warn(app.localize('ProductionUnitNotSelectErr'));
                        return;
                    }

                    if (vm.existAllElements() == false)
                        return;

                    var lastelement = vm.yieldInputDetailPortion[vm.yieldInputDetailPortion.length - 1];

                    if (lastelement.defaultUnitId == lastelement.unitRefId) {
                        if (parseFloat(lastelement.inputQty) > parseFloat(lastelement.currentInHand)) {
                            abp.notify.info(app.localize('QuantityShortage', lastelement.materialRefName));
                            return;
                        }
                    }
                    else {
                        var convFactor = 0;
                        var convExistFlag = false;
                        vm.refconversionunit.some(function (data, key) {
                            if (data.baseUnitId == lastelement.unitRefId && data.refUnitId == lastelement.defaultUnitId) {
                                convFactor = data.conversion;
                                convExistFlag = true;
                                return true;
                            }
                        });


                        if (convExistFlag == false) {
                            abp.notify.info(app.localize('ConversionFactorNotExist', lastelement.defaultUnitName, lastelement.unitRefName));
                            return;
                        }

                        var curHandConversion = lastelement.currentInHand * 1 / convFactor;
                        if (parseFloat(lastelement.inputQty) > parseFloat(curHandConversion)) {
                            abp.notify.info(app.localize('QuantityShortage', lastelement.materialRefName));
                            return;
                        }

                    }



                    if (vm.isUndefinedOrNull(lastelement.materialRefId) || lastelement.materialRefId == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(lastelement.materialRefName)) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MaterialRequired'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(lastelement.inputQty) || lastelement.inputQty == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidQty'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(lastelement.unitRefId) || lastelement.unitRefId == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('UnitRequired'));
                        return;
                    }

                }
                vm.sno = vm.sno + 1;

                vm.yieldInputDetailPortion.push({
                    'sno': vm.sno,
                    'materialRefId': 0,
                    'materialRefName': '',
                    'inputQty': '',
                    'unitRefId': '',
                    'uom': '',
                    'currentInHand': '',
                    'changeunitflag': false,
                });

            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex) {
                if (vm.yieldInputDetailPortion.length > 1) {
                    vm.yieldInputDetailPortion.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                }
                else {
                    vm.yieldInputDetailPortion.splice(productIndex, 1);
                    vm.sno = 0;
                    vm.addPortion();
                    return;
                }
            }

            vm.outputaddPortion = function () {
                var errorFlag = false;
                var value = vm.yieldOutputDetailPortion[vm.yieldOutputDetailPortion.length - 1];

                if (vm.outSno > 0) {
                    var value = vm.yieldOutputDetailPortion[vm.yieldOutputDetailPortion.length - 1];
                    //value = value[0];

                    if (vm.existAllElements() == false)
                        return;

                    if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.materialRefName)) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MaterialRequired'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.outputQty) || value.outputQty == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidQty'));
                        return;
                    }
                    if (vm.isUndefinedOrNull(value.unitRefId) || value.unitRefId == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('UnitRequired'));
                        return;
                    }

                }
                vm.outSno = vm.outSno + 1;

                vm.yieldOutputDetailPortion.push({
                    'sno': vm.outSno,
                    'materialRefId': 0,
                    'materialRefName': '',
                    'outputQty': '',
                    'unitRefId': '',
                    'uom': '',
                    'currentInHand': '',
                    'changeunitflag': false,
                });

            }

            vm.outremoveRow = function (productIndex) {
                if (vm.yieldOutputDetailPortion.length > 1) {
                    vm.yieldOutputDetailPortion.splice(productIndex, 1);
                    vm.outSno = vm.outSno - 1;
                }
                else {
                    vm.yieldOutputDetailPortion.splice(productIndex, 1);
                    vm.outSno = 0;
                    vm.outputaddPortion();
                    return;
                }
            }

            $scope.func = function (data, val, objList) {

                if (vm.showErrorProductionUnitNotSelected() == false) {
                    data.materialRefId = 0;
                    data.materialRefName = '';
                    return;
                }

                var forLoopFlag = true;

                angular.forEach(objList, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.materialRefId = 0;
                    data.materialRefName = '';
                    return;
                }
                data.materialRefName = val.materialName;
                data.currentInHand = val.onHand;
                //data.unitRefId = val.unitRefId;
                data.uom = val.defaultUnitName;
                data.defaultUnitId = val.unitRefId;
                data.defaultUnitName = val.defaultUnitName;
                data.unitRefName = val.issueUnitName;
                data.unitRefId = val.issueUnitId;

            };

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };


            vm.save = function () {
                if (vm.existAllElements() == false)
                    return;

                if (vm.templatesave.templateflag == true && vm.templatesave.templatename.length == 0) {
                    abp.message.warn(app.localize("TemplateNameRequired"));
                    return;
                }

                if (vm.productionUnitExistsFlag == true && vm.isUndefinedOrNull(vm.yield.productionUnitRefId) == true) {
                    abp.notify.warn(app.localize('ProductionUnitNotSelectErr'));
                    return;
                }

                var lastelement = vm.yieldInputDetailPortion[vm.yieldInputDetailPortion.length - 1];


                vm.saving = true;

                vm.yieldInput = [];

                vm.errorFlag = false;

                angular.forEach(vm.yieldInputDetailPortion, function (value, key) {

                    var lastelement = value;
                    if (vm.inputeditallowed == true) {

                        if (lastelement.defaultUnitId == lastelement.unitRefId) {
                            if (parseFloat(lastelement.inputQty) > parseFloat(lastelement.currentInHand)) {
                                abp.notify.info(app.localize('QuantityShortage', lastelement.materialRefName));
                                vm.errorFlag = true;
                                return;
                            }
                        }
                        else {
                            var convFactor = 0;
                            var convExistFlag = false;
                            vm.refconversionunit.some(function (data, key) {
                                if (data.baseUnitId == lastelement.unitRefId && data.refUnitId == lastelement.defaultUnitId) {
                                    convFactor = data.conversion;
                                    convExistFlag = true;
                                    return true;
                                }
                            });


                            if (convExistFlag == false) {
                                abp.notify.info(app.localize('ConversionFactorNotExist', lastelement.defaultUnitName, lastelement.unitRefName) + ' - ' + value.materialRefName);
                                vm.errorFlag = true;
                                return;
                            }

                            var curHandConversion = lastelement.currentInHand * 1 / convFactor;
                            if (parseFloat(lastelement.inputQty) > parseFloat(curHandConversion)) {
                                abp.notify.info(app.localize('QuantityShortage', lastelement.materialRefName));
                                vm.errorFlag = true;

                                return;
                            }

                        }
                    }

                    if (value.inputQty == 0) {
                        abp.notify.warn(app.localize('Quantity') + ' ' + app.localize('Required') + ' ' + value.materialRefName);
                        vm.errorFlag = true;
                    }

                    if (value.materialRefId != 0 && value.materialRefId != null && value.uom != '' && value.inputQty != 0 && value.inputQty != '') {
                        vm.yieldInput.push({
                            'sno': value.sno,
                            'inputMaterialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'inputQty': value.inputQty,
                            'unitRefId': value.unitRefId,
                            'uom': value.uom,
                            'changeunitflag': false,
                        });
                    }
                });

                if (vm.errorFlag) {
                    vm.saving = false;
                    vm.loadingflag = false;
                    return;
                }


                vm.yieldOutput = [];
                if (vm.outputeditallowed == true) {
                    angular.forEach(vm.yieldOutputDetailPortion, function (value, key) {
                        if (value.outputQty == 0) {
                            abp.notify.warn(app.localize('YieldOutput') + ' ' + app.localize('Quantity') + ' ' + app.localize('Required') + ' ' + value.materialRefName);
                            vm.errorFlag = true;
                        }

                        if (value.materialRefId != 0 && value.materialRefId != null && value.uom != '' && value.outputQty != 0 && value.outputQty != '') {
                            vm.yieldOutput.push({
                                'sno': value.sno,
                                'outputMaterialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'outputQty': value.outputQty,
                                'uom': value.uom,
                                'unitRefId': value.unitRefId,
                                'changeunitflag': false,
                            });
                        }
                    });
                }
                if (vm.errorFlag) {
                    vm.saving = false;
                    vm.loadingflag = false;
                    return;
                }

                if (vm.yieldOutput.length == 0) {
                    vm.yieldStatus = app.localize("Pending");
                    vm.yield.completedTime = null;
                }
                else {
                    vm.yieldStatus = app.localize("Completed");
                    //vm.yield.completedTime = moment().format($scope.format);
                }

                if (vm.templatesave.templatename != null)
                    vm.templatesave.templatename = vm.templatesave.templatename.toUpperCase();


                yieldService.createOrUpdateYield({
                    'yield': vm.yield,
                    yieldInputDetail: vm.yieldInput,
                    yieldOutputDetail: vm.yieldOutput,
                    templatesave: vm.templatesave,
                    statusToBeAssigned: vm.yieldStatus
                }).success(function () {
                    abp.notify.info(app.localize('Yield') + app.localize('SavedSuccessfully'));
                    vm.cancel();

                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.yield');
            };

            vm.filterMaterial = function () {
                if ($('#recipeFlag').is(":checked")) {
                    vm.refmaterialoutput = [];
                    angular.forEach(vm.refmaterial, function (value, key) {
                        if (value.materialTypeName != app.localize('RAW')) {
                            vm.refmaterialoutput.push(value);
                        }
                    });
                    vm.fillMaterialBasedOnRecipe(vm.yield.recipeRefId);
                }
                else {
                    vm.fillDropDownMaterial();
                    vm.yield.recipeRefId = null;
                }
            }

            $scope.RecipeFunc = function (data) {
                var selValue = data.value;
                vm.fillMaterialBasedOnRecipe(selValue);

            }

            vm.refRecipe = [];
            function fillDropDownRecipe() {
                materialService.getMaterialRecipeTypesForCombobox({}).success(function (result) {
                    vm.refRecipe = result.items;

                });
            }

            vm.refmaterial = [];
            vm.tempRefMaterial = [];

            vm.fillMaterialBasedOnRecipe = function (selValue) {
                vm.loadingflag = true;


                materialService.getMaterialViewWithOnHandForRecipe({
                    locationRefId: vm.defaultLocationId,
                    recipeRefId: selValue
                }).success(function (result) {
                    vm.refmaterial = result.items;
                    vm.tempRefMaterial = result.items;
                }).finally(function () {
                    vm.loadingflag = false;
                });
            }

            vm.refmaterial = [];
            vm.refmaterialoutput = [];

            vm.fillDropDownMaterial = function () {
                vm.loadingCount++;
                materialService.getViewWithOnHand({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    vm.refmaterial = result.items;
                    vm.refmaterialoutput = result.items;
                }).finally(function () {
                    vm.loadingCount--;
                });
            }

            vm.getDetailMaterial = function () {
                if (vm.yield.recipeProductionQty == 0 || vm.yield.recipeProductionQty == null) {
                    abp.notify.info("EnterProductionQuantity");
                    return;
                }

                vm.loadingflag = true;
                vm.loading = true;
                issueService.getIssueForRecipe({
                    recipeRefId: vm.yield.recipeRefId,
                    requestQtyforProduction: vm.yield.recipeProductionQty,
                    locationRefId: vm.defaultLocationId
                }).success(function (result) {
                    vm.yieldInputDetailPortion = [];
                    vm.sno = 0;
                    angular.forEach(result.issueDetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.yieldInputDetailPortion.push({
                            'sno': value.sno,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'inputQty': value.issueQty,
                            'uom': value.defaultUnitName,
                            'currentInHand': value.currentInHand,
                            'changeunitflag': false,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'defaultUnitId': value.defaultUnitId,
                            'defaultUnitName': value.defaultUnitName
                        });
                    });
                }).finally(function () {
                    vm.loadingflag = false;
                    vm.loading = false;
                });
            }


            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.refTemplateDetail = [];
            vm.reftemplate = [];
            function fillDropDownTemplate() {
                templateService.getTemplateDetail({
                    locationRefId: vm.defaultLocationId,
                    templateType: vm.defaulttemplatetype
                }).success(function (result) {
                    vm.refTemplateDetail = result.items;

                    angular.forEach(vm.refTemplateDetail, function (value, key) {
                        vm.reftemplate.push({
                            'value': value.id,
                            'displayText': value.name,
                        });
                    });

                });
            }


            vm.fillDataFromTemplate = function () {
                vm.refTemplateDetail.some(function (value, key) {
                    if (value.id == vm.template) {
                        vm.selectedTemplate = value.templateData;
                        return true;
                    }
                });
                vm.uilimit = null;
                vm.loading = true;
                vm.loadingflag = true;
                vm.loadingCount++;
                yieldService.getTemplateObjectForEdit({
                    objectString: vm.selectedTemplate
                }).success(function (result) {
                    vm.uilimit = null;
                    var tempyieldTime = vm.yield.issueTime;
                    var tempcompletedTime = vm.yield.completedTime

                    vm.yield = result.yield;

                    vm.yield.issueTime = moment(tempyieldTime).format($scope.format);

                    if (vm.outputeditallowed == true)
                        vm.yield.completedTime = moment(tempcompletedTime).format($scope.format);
                    else
                        vm.yield.completedTime = null;

                    vm.yieldInput = result.yieldInputDetail;
                    vm.yieldOutput = result.yieldOutputDetail;

                    $scope.dt = moment().format($scope.format);
                    //vm.yield.issueTime = moment().format($scope.format);
                    //vm.yield.completedTime = moment().format($scope.format);
                    vm.yield.locationRefId = vm.defaultLocationId;

                    if (vm.isUndefinedOrNull(vm.yield.recipeRefId)) {
                        $scope.checked = false;
                    }
                    else {
                        $scope.checked = true;
                    }

                    vm.yieldInputDetailPortion = [];
                    vm.sno = 0;
                    angular.forEach(vm.yieldInput, function (value, key) {
                        vm.temponhand = 0;
                        vm.temponorder = 0;
                        vm.sno = vm.sno + 1;
                        vm.yieldInputDetailPortion.push({
                            'sno': vm.sno,
                            'materialRefId': value.inputMaterialRefId,
                            'materialRefName': value.materialRefName,
                            'inputQty': value.inputQty,
                            'uom': value.uom,
                            'currentInHand': value.currentInHand,
                            'changeunitflag': false,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'defaultUnitId': value.defaultUnitId,
                            'defaultUnitName': value.defaultUnitName
                        });
                    });

                    if (vm.yieldOutput.length > 0) {
                        vm.yieldOutputDetailPortion = [];
                        vm.outSno = 0;
                        angular.forEach(vm.yieldOutput, function (value, key) {
                            vm.outSno = vm.outSno + 1;
                            vm.yieldOutputDetailPortion.push({
                                'sno': vm.outSno,
                                'materialRefId': value.outputMaterialRefId,
                                'materialRefName': value.materialRefName,
                                'outputQty': value.outputQty,
                                'uom': value.uom,
                                'currentInHand': value.currentInHand,
                                'changeunitflag': false,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'defaultUnitId': value.defaultUnitId,
                                'defaultUnitName': value.defaultUnitName
                            });
                        });
                    }
                    vm.fillDropDownMaterial();

                }).finally(function (result) {
                    vm.loading = false;
                    vm.loadingflag = false;
                    vm.loadingCount--;
                });

            }


            function init() {
                fillDropDownRecipe();
                vm.fillDropDownMaterial();
                fillDropDownTemplate();

                yieldService.getYieldForEdit({
                    Id: vm.yieldId
                }).success(function (result) {
                    vm.yield = result.yield;
                    vm.yieldInput = result.yieldInputDetail;
                    vm.yieldOutput = result.yieldOutputDetail;
                    vm.templatesave = result.templatesave;

                    vm.yield.locationRefId = vm.defaultLocationId;

                    vm.templatesave.templateflag = false;
                    vm.templatesave.templatename = null;
                    vm.templatesave.templatescope = false;

                    $scope.dt = moment().format($scope.format);
                    if (result.yield.id != null) {
                        vm.uilimit = null;
                        vm.yield.issueTime = moment(result.yield.issueTime).format($scope.format);
                        $scope.minDate = vm.yield.issueTime;

                        if (vm.isUndefinedOrNull(vm.yield.recipeRefId)) {
                            $scope.checked = false;
                        }
                        else {
                            $scope.checked = true;
                        }

                        $('input[name="completedTime"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: vm.yield.completedTime,
                            minDate: $scope.minDate,
                        });

                        vm.yieldInputDetailPortion = [];
                        vm.yieldOutputDetailPortion = [];

                        vm.sno = 0;
                        angular.forEach(vm.yieldInput, function (value, key) {
                            vm.sno = vm.sno + 1;
                            vm.yieldInputDetailPortion.push({
                                'sno': vm.sno,
                                'materialRefId': value.inputMaterialRefId,
                                'materialRefName': value.materialRefName,
                                'inputQty': value.inputQty,
                                'uom': value.uom,
                                'currentInHand': value.currentInHand,
                                'changeunitflag': false,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'defaultUnitId': value.defaultUnitId,
                                'defaultUnitName': value.defaultUnitName
                            });
                        });

                        vm.outSno = 0;
                        angular.forEach(vm.yieldOutput, function (value, key) {
                            vm.outSno = vm.outSno + 1;
                            vm.yieldOutputDetailPortion.push({
                                'sno': vm.outSno,
                                'materialRefId': value.outputMaterialRefId,
                                'materialRefName': value.materialRefName,
                                'outputQty': value.outputQty,
                                'uom': value.uom,
                                'currentInHand': value.currentInHand,
                                'changeunitflag': false,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'defaultUnitId': value.defaultUnitId,
                                'defaultUnitName': value.defaultUnitName
                            });
                        });

                        vm.checkCloseDay();

                        if (vm.yieldOutput.length == 0)
                            vm.outputaddPortion();

                        vm.fillDropDownMaterial();
                    }
                    else {
                        //vm.yield.issueTime = moment().format($scope.format);
                        vm.yield.issueTime = null;
                        vm.yield.completedTime = null;
                        vm.checkCloseDay();
                        vm.addPortion();
                        vm.outputaddPortion();
                        vm.uilimit = 20;

                    }

                });
            }

            init();

            vm.showErrorProductionUnitNotSelected = function () {
                if (vm.productionUnitExistsFlag == true && vm.isUndefinedOrNull(vm.yield.productionUnitRefId) == true) {
                    abp.notify.warn(app.localize('ProductionUnitNotSelectErr'));
                    return false;
                }
                return true;
            }

            vm.checkCloseDay = function () {
                vm.loading = true;
                vm.loadingCount++;
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {

                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format('YYYY-MMM-DD')));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format('YYYY-MMM-DD')));

                    }
                    //if (moment(result.accountDate).format("YYYY-MMM-DD") == moment().format("YYYY-MMM-DD"))
                    //    return;

                    if (vm.yield.issueTime == null && vm.inputeditallowed == true)
                        vm.yield.issueTime = moment(result.accountDate).format($scope.format);

                    if (vm.outputeditallowed == true)
                        vm.yield.completedTime = moment(result.accountDate).format($scope.format);

                    $('input[name="issueTime"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.yield.issueTime,
                        minDate: vm.yield.issueTime,
                        maxDate: vm.yield.issueTime
                    });

                    $('input[name="completedTime"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.yield.completedTime,
                        minDate: vm.yield.completedTime,
                    });

                }).finally(function (result) {
                    vm.loading = false;
                    vm.loadingCount--;
                });
            }


            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

            vm.unitReference = function (selected, data) {
                data.unitRefId = selected.value;
                data.unitRefName = selected.displayText;
                data.changeunitflag = false;
            }

            vm.refconversionunit = [];
            vm.fillUnitConversion = function () {
                unitconversionService.getAll({
                }).success(function (result) {
                    vm.refconversionunit = result.items;
                });
            }

            vm.fillUnitConversion();
        }
    ]);
})();

