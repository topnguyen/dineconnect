﻿(function () {
    appModule.controller('tenant.views.house.transaction.intertransfer.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.interTransfer', 'appSession', 'abp.services.app.location', 'abp.services.app.dayClose', 'abp.services.app.material', 'abp.services.app.interTransferRequest',
        function ($scope, $state, $modal, uiGridConstants, intertransferService, appSession, locationService, daycloseService, materialService, interTransferRequestService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            console.log("Request Index.js Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.id = null;
            vm.defaultLocationId = appSession.user.locationRefId;

            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Transaction.InterTransfer.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Transaction.InterTransfer.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Transaction.InterTransfer.Delete'),
                approveRequest: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransfer.Approve'),
            };

            vm.refmaterialgroup = [];
            function fillDropDownMaterialGroup() {
                materialService.getMaterialGroupForCombobox({}).success(function (result) {
                    vm.refmaterialgroup = result.items;
                });
            }
            fillDropDownMaterialGroup();

            vm.refmaterialgroupcategory = [];
            vm.fillDropDownMaterialGroupCategory = function () {
                materialService.getMaterialGroupCategoryForCombobox({
                    id: vm.materialGroupRefId
                }).success(function (result) {
                    vm.refmaterialgroupcategory = result.items;
                    if (result.items.length == 1) {
                        vm.materialGroupCategoryRefId = result.items[0].value;
                    }
                    else {
                        vm.materialGroupCategoryRefId = null;
                    }
                });
            }

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.advancedFiltersAreShown = false;
            vm.dateFilterApplied = false;

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.firstFlag = true;

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editInterTransfer(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.approveRequest\" ng-click=\"grid.appScope.approveRequestPending(row.entity)\">" + app.localize("Approve") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteInterTransfer(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.printInterTransfer(row.entity)\">" + app.localize("Print") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.printInterTransferReceived(row.entity)\">" + app.localize("Received") + " " + app.localize("Print") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.create\" ng-click=\"grid.appScope.viewInterTransferDetail(row.entity)\">" + app.localize("ViewDetail") /*+ '/' + app.localize('SetPoAsCompletion') */ + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Id'),
                        field: 'id',
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <a ng-click=\"grid.appScope.viewInterTransferDetail(row.entity)\" class=\"bold\"> {{row.entity.id}} </a>" +
                            "  <br ng-if=\"row.entity.parentRequestRefId!=null\">" +
                            "  <br ng-if=\"row.entity.parentRequestRefId!=null\">" +
                            "  <a ng-click=\"grid.appScope.viewInterTransferDetailParent(row.entity)\" ng-if=\"row.entity.parentRequestRefId!=null\"> " + app.localize("Parent") + ' ' + app.localize("Id") + " : {{row.entity.parentRequestRefId}} </a>" +
                            "</div>",
                        maxWidth: 100
                    },
                    {
                        name: app.localize('Location'),
                        field: 'locationRefName'
                    },
                    {
                        name: app.localize('RequestLocation'),
                        field: 'requestLocationRefName'
                    },
                    {
                        name: app.localize('Material') + ' ' + app.localize('Count'),
                        field: 'noOfMaterials',
                        maxWidth: 100
                    },
                    {
                        name: app.localize('CurrentStatus'),
                        field: 'currentStatus',
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <span class=\"bold\"> {{row.entity.currentStatus}} <span>" +
                            "  <br ng-if=\"row.entity.poStatusRemarks.length>0\">" +
                            "  <a ng-if=\"row.entity.poStatusRemarks.length>0\"> " + app.localize("PO") + ' ' + app.localize("Status") + " : {{row.entity.poStatusRemarks}} <a>" +
                            "</div>",
                    },
                    {
                        name: app.localize('Request') + ' ' + app.localize('AccountDate'),
                        field: 'requestDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\''
                    },
                    {
                        name: app.localize('DeliveryDateRequested'),
                        field: 'deliveryDateRequested',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm\'',
                        cellClass:
                            function (grid, row) {
                                if (row.entity.deliverDateAlertFlag)
                                    return 'ui-pending';
                            },

                    },
                    {
                        name: app.localize('CompletedDate'),
                        field: 'completedTime',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm\'',
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm\'',
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.clear = function () {
                vm.filterText = null;
                vm.dateRangeModel = null;
                vm.dateFilterApplied = false;
                vm.status = null;
                vm.id = null;
                vm.getAll();
            }

            vm.approveRequestPending = function (myObj) {
                var currentStatus = myObj.currentStatus;
                if (currentStatus == app.localize('RequestApprovalPending'))
                    openCreateOrEditModal(myObj.id);
                else if (currentStatus == app.localize('Declined')) {
                    abp.message.error(app.localize('AlreadyDeclined'), app.localize('Declined'));
                }
                else {
                    abp.message.error(app.localize('AlreadyApproved'), app.localize('Approved'));
                }
            };

            vm.getAll = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var currentStatus = null;


                if (vm.dateFilterApplied) {

                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }
                currentStatus = vm.status;
                console.log("Request Index.js getViewInterTransfer() Call Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                interTransferRequestService.getViewInterTransfer({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    requestStatus: '',
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    currentStatus: currentStatus,                    
                    materialGroupRefId: vm.materialGroupRefId,
                    materialGroupCategoryRefId: vm.materialGroupCategoryRefId,
                    id: vm.id
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                    console.log("Request Index.js getViewInterTransfer() Call End Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));

                });
            };

            vm.editInterTransfer = function (myObj) {
                var currentStatus = myObj.currentStatus;
                if (currentStatus == app.localize('Pending') || currentStatus == app.localize('RequestApprovalPending'))
                    openCreateOrEditModal(myObj.id);
                else {
                    abp.message.error(app.localize('EditNotAllowedApproved'), app.localize('CouldNotEdit'));
                }
            };

            vm.createInterTransfer = function () {
                openCreateOrEditModal(null);
            };

            vm.deleteInterTransfer = function (myObject) {
                var currentStatus = myObject.currentStatus;
                if (currentStatus != app.localize('Pending')) {
                    abp.message.error(app.localize('DeleteNotAllowedApproved'), app.localize('CouldNotDelete'));
                    return;
                }
                abp.message.confirm(
                    app.localize('DeleteInterTransferRequestWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            interTransferRequestService.deleteInterTransfer({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.intertransferrequestdetail", {
                    id: objId,
                    directTransfer: false
                });
            }

            vm.exportToExcel = function () {
                vm.loading = true;
                var startDate = null;
                var endDate = null;
                var currentStatus = null;

                if (vm.advancedFiltersAreShown) {
                    if (vm.dateFilterApplied) {

                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                    currentStatus = vm.status;
                }
                interTransferRequestService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    requestStatus: '',
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    currentStatus: currentStatus
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            };

            vm.getDashBoard = function () {
                vm.loadingDashboard = true;
                console.log("Dashboard Call Time :" + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                interTransferRequestService.getDashBoardTransfer({
                    Id: vm.defaultLocationId
                }).success(function (result) {
                    vm.totalPending = result.totalPending;
                    vm.totalApproved = result.totalApproved;
                    vm.totalReceived = result.totalReceived;
                    vm.totalRejected = result.totalRejected;
                }).finally(function () {
                    vm.loadingDashboard = false;
                    vm.firstFlag = false;
                });
            };

            if (vm.firstFlag)
                vm.getDashBoard();

            vm.getAll();

            vm.checkCloseDay = function () {
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (result.currentUserId != vm.currentUserId) {
                            vm.currentLanguage = abp.localization.currentLanguage;
                            location.href = abp.appPath + 'Localization/ChangeCulture?cultureName=' + vm.currentLanguage.name + '&returnUrl=' + window.location.href;
                            var errorMessage = app.localize('UserIdChangedErrorAlert', vm.currentUserId, result.currentUserId)
                            abp.notify.error(errorMessage);
                            abp.message.error(errorMessage);
                        }

                        if (vm.softmessagenotificationflag)
                            abp.notify.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }
                }).finally(function () {

                });
            }

            vm.viewInterTransferDetailParent = function (myObject) {
                vm.loading = true;
                openView(myObject.parentRequestRefId);
            }

            vm.viewInterTransferDetail = function (myObject) {
                vm.loading = true;
                openView(myObject.id);
            };

            function openView(objId) {
                vm.loading = true;
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/intertransfer/request/requestViewInGrid.cshtml',
                    controller: 'tenant.views.house.transaction.intertransfer.requestViewInGrid as vm',
                    backdrop: 'static',
                    resolve: {
                        transferId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    // vm.getAll();
                }).finally(function (result) {
                    //  vm.getAll();
                });
                vm.loading = false;
            }

            vm.checkCloseDay();

            vm.printInterTransfer = function (myObject) {
                $state.go('tenant.intertransferrequestprint', {
                    printid: myObject.id
                });
            };

            vm.printInterTransferReceived = function (myObject) {
                var currentStatus = myObject.currentStatus;
                if (currentStatus == app.localize('Received') || currentStatus == app.localize('PartiallyReceived')) {
                    $state.go('tenant.intertransferreceivedprint', {
                        printid: myObject.id
                    });
                }
                else {
                    abp.notify.warn(app.localize('Request') + ' ' + app.localize('Not') + ' ' + app.localize('Received'));
                }
            };

        }]);
})();

