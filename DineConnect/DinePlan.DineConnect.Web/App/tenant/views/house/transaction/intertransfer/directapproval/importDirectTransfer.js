﻿(function () {
    appModule.controller('tenant.views.house.transaction.intertransfer.directapproval.importDirectTransfer', [
        '$scope', 'appSession', '$uibModalInstance', 'FileUploader', 'requestLocationRefId', 'abp.services.app.location', 'abp.services.app.interTransfer', 'appSession',
        function ($scope, appSession, $uibModalInstance, fileUploader, requestLocationRefId, locationService, intertransferService) {
            var vm = this;
            vm.loading = false;

            vm.requestLocationRefId = requestLocationRefId;
            vm.stockTakenDate = moment();
            vm.allItems = [];
            vm.defaultLocationId = appSession.user.locationRefId;

            vm.locationRefId = null;

            vm.reflocation = [];
            vm.filteredreflocations = [];

            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;
                    angular.forEach(vm.reflocation, function (value, key) {
                        if (value.value != vm.defaultLocationId)
                            vm.filteredreflocations.push(value);
                    });

                }).finally(function () {
                });
            }

            fillDropDownLocation();

            vm.uploader = new fileUploader({
                url: abp.appPath + 'Import/ImportDirectTransfer',
                formData: [
                    {
                        "closingStockDate": vm.stockTakenDate
                    },
                    {
                        "requestLocationRefId": vm.requestLocationRefId
                    },
                ],
                queueLimit: 1,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        if ('|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ImportTemplate_Warn_FileType'));
                            return false;
                        }
                        return true;
                    }
                }]
            });

            vm.importpath = abp.appPath + 'Import/ImportDirectTransferTemplate?locationRefId=';

            vm.getTransferTemplate = function () {
                vm.saving = true;
                intertransferService.importDirectTransferTemplate({
                    locationRefId: vm.defaultLocationId,
                    requestLocationRefId : vm.locationRefId
                }).success(function (result) {
                    app.downloadTempFile(result);
                    vm.saving = false;
                });
            };

            vm.consolidatedExcel = function () {
                vm.saving = true;
                intertransferService.getConsolidatedToExcel({
                    requestList: vm.userGridOptions.data
                }).success(function (result) {
                    app.downloadTempFile(result);
                    vm.saving = false;
                });
            };

            vm.save = function () {
                if (vm.stockTakenDate == null) {
                    abp.notify.warn(app.localize("SelectStockTakenDateErr"));
                    return;
                }
                if (vm.uploader.queue.length == 0) {
                    abp.notify.warn(app.localize('YouAreNotSelectAFile'));
                    return;
                }
                vm.loading = true;
                vm.uploader.uploadAll();
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.datafromexcel = [];

            vm.uploader.onErrorItem = function (fileItem, response, status, headers) {
                if (response.success) {
                   // Do nothing
                } else {
                    abp.message.error(response);
                    abp.notify.error(response);
                    vm.loading = false;
                    vm.saving = false;
                    return;
                }
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.error != null) {
                    abp.message.warn(response.error.message);
                    $uibModalInstance.dismiss();
                    vm.loading = false;
                    return;
                }
                vm.allItems = [];
                vm.datafromexcel = response.result;
                if (vm.datafromexcel == null) {
                    if (response.error != null) {
                        abp.message.warn(response.error.message);
                        vm.readfromexcelstatus = false;
                        vm.cancel();
                        return;
                    }
                }

                if (vm.datafromexcel != null) {
                    vm.sno = 0;
                    vm.allItems = [];
                    angular.forEach(vm.datafromexcel, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.allItems.push({
                            'interTransferRefId': 0,
                            'sno': value.sno,
                            'requestLocationRefId': value.requestLocationRefId,
                            'locationRefId': value.locationRefId,
                            'locationRefName': value.locationRefName,
                            'barcode' : value.barcode,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'requestQty': value.requestQty,
                            'issueQty': 0,
                            'currentStatus': 'A',
                            'requestRemarks': value.requestRemarks,
                            'approvedRemarks': value.approvedRemarks,
                            'id': 0,
                            'materialTypeName': value.materialTypeName,
                            'uom': value.uom,
                            'onHand': 0,
                            'usagePatternList': null,
                            'currenteditstatus': false,
                            'unitRefId' : value.unitRefId
                        })
                    });
                }
                else {
                    vm.allItems = [];
                }

                abp.notify.info(app.localize("FINISHED"));
                vm.loading = false;
                $uibModalInstance.close(vm.allItems);
                
            };
        }

    ]);
})();
