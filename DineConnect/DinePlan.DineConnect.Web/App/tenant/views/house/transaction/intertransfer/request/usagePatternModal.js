﻿(function () {
    appModule.controller('tenant.views.house.transaction.intertransfer.request.usagePatternModal', [
        '$scope', '$uibModalInstance', 'usagePatternList','materialRefName', 'uom',
        function ($scope, $modalInstance, usagePatternList, materialRefName,uom) {
            var vm = this;

            vm.usagePatternList = usagePatternList;
            vm.materialRefName = materialRefName;
            vm.uom = uom;

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

        }
    ]);
})();