﻿(function () {
    appModule.controller('tenant.views.house.transaction.intertransfer.approval.changeDefaultSupplier', [
        '$scope', '$uibModalInstance', 'data', 'materialRefId', 'materialRefName', 'uom',
        function ($scope, $modalInstance, data, materialRefId, materialRefName, uom) {
            var vm = this;
            /* eslint-disable */
            vm.loading = false;
            vm.materialRefId = materialRefId;
            vm.materialRefName = materialRefName;
            vm.uom = uom;

            vm.data = data;
            vm.norecordsfound = false;
            if (data.defaultSupplier.supplierRefName == null || data.supplierMaterialViewDtos == null || data.supplierMaterialViewDtos.length == 0) {
                vm.norecordsfound = true;
            }
            else {
                vm.defaultSupplier = data.defaultSupplier;
            }

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.save = function () {
                $modalInstance.close(vm.defaultSupplier);
            };

            vm.changeDefaultSupplier = function (argData) {
                vm.defaultSupplier = argData;
                vm.data.defaultSupplier = argData;
            };

        }
    ]);
})();