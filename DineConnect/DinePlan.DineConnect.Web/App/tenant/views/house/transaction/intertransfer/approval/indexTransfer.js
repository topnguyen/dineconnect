﻿
(function () {
    appModule.controller('tenant.views.house.transaction.intertransfer.approval.indexTransfer', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.interTransfer', 'appSession',
        'abp.services.app.dayClose', 'abp.services.app.material', 'abp.services.app.interTransferSupplementary', 'abp.services.app.interTransferCrud',
        function ($scope, $state, $modal, uiGridConstants, intertransferService, appSession, daycloseService, materialService, interTransferSupplementaryService, interTransferCrudService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.id = null;
            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.transfervalueshown = abp.setting.getBoolean("App.House.InterTransferValueShown");
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");
            vm.showOnlyDeliveryDateExpectedAsOnDate = false;
            vm.firstTimeFlag = true;
            vm.saving = false;

            vm.defaultLocationId = appSession.user.locationRefId;

            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Transaction.InterTransferApproval.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Transaction.InterTransferApproval.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Transaction.InterTransferApproval.Delete'),
                approve: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransferApproval.Approve'),
                directapprove: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransferApproval.DirectApprove'),
                directapproveandreceive: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransferApproval.DirectApproveAndReceive'),
            };


            vm.refmaterialgroup = [];
            function fillDropDownMaterialGroup() {
                materialService.getMaterialGroupForCombobox({}).success(function (result) {
                    vm.refmaterialgroup = result.items;
                });
            }
            fillDropDownMaterialGroup();

            vm.refmaterialgroupcategory = [];
            vm.fillDropDownMaterialGroupCategory = function () {
                materialService.getMaterialGroupCategoryForCombobox({
                    id: vm.materialGroupRefId
                }).success(function (result) {
                    vm.refmaterialgroupcategory = result.items;
                    if (result.items.length == 1) {
                        vm.materialGroupCategoryRefId = result.items[0].value;
                    }
                    else {
                        vm.materialGroupCategoryRefId = null;
                    }
                });
            }

            var requestParams = {
                skipCount: 0,
                maxResultCount: 10, // app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.advancedFiltersAreShown = false;
            vm.dateFilterApplied = false;

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };


            $('input[name="deliveryDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment().add(90, 'days').format('YYYY-MM-DD'),
            });

            vm.deliveryDateRangeOptions = app.createDateRangePickerOptions();
            vm.deliveryDateRangeOptions.ranges[app.localize('Tomorrow')] = [moment().add(1, 'days').startOf('day'), moment().add(1, 'days').endOf('day')];
            vm.deliveryDateRangeOptions.ranges[app.localize('Next7Days')] = [moment().startOf('day'), moment().add(6, 'days').endOf('day')];
            vm.deliveryDateRangeOptions.ranges[app.localize('Next30Days')] = [moment().startOf('day'), moment().add(29, 'days').endOf('day')];

            vm.deliveryDateRangeOptions.max = moment().add(90, 'days').format('YYYY-MM-DD');
            vm.deliveryDateRangeOptions.maxDate = moment().add(90, 'days').format('YYYY-MM-DD');

            vm.deliveryDateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.selectedRecords = [];

            vm.pushSelectedRow = function (myObj) {
                if (myObj != null) {
                    var myIndex = -1;
                    if (vm.selectedRecords != null) {
                        vm.selectedRecords.some(function (value, key) {
                            if (value.id == myObj.id) {
                                myIndex = value;
                                return true;
                            }
                        });
                    }

                    if (myIndex == -1) {
                        vm.selectedRecords.push(myObj);
                    } else {
                        if (vm.autoSelection == false)
                            abp.notify.error(myObj.employeeName + '  ' + app.localize('Available'));
                    }
                }
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: 10, // app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                enableRowSelection: true,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.approve\" ng-click=\"grid.appScope.approveTransfer(row.entity)\">" + app.localize("Approve") + "</a></li>" + "      <li><a ng-if=\"grid.appScope.permissions.approve\" ng-click=\"grid.appScope.autoApproval(row.entity)\">" + app.localize("Auto") + ' ' + app.localize("Approval") + "</a></li>"+
                            "      <li><a ng-if=\"grid.appScope.permissions.approve\" ng-click=\"grid.appScope.printApproval(row.entity)\">" + app.localize("Print") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.create\" ng-click=\"grid.appScope.viewInterTransferDetail(row.entity)\">" + app.localize("ViewDetail") /*+ '/' + app.localize('SetPoAsCompletion') */ + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>",
                        maxWidth: 120
                    },
                    {
                        name: app.localize('Id'),
                        field: 'id',
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <a ng-click=\"grid.appScope.viewInterTransferDetail(row.entity)\" class=\"bold\"> {{row.entity.id}} </a>" +
                            "  <br ng-if=\"row.entity.parentRequestRefId!=null\">" +
                            "  <br ng-if=\"row.entity.parentRequestRefId!=null\">" +
                            "  <a ng-click=\"grid.appScope.viewInterTransferDetailParent(row.entity)\" ng-if=\"row.entity.parentRequestRefId!=null\"> " + app.localize("Parent") + ' ' + app.localize("Id") + " : {{row.entity.parentRequestRefId}} </a>" +
                            "</div>",
                        maxWidth: 80
                    },
                    {
                        name: app.localize('RequestLocation'),
                        field: 'locationRefName',
                        minWidth: 150,
                        cellClass:
                            function (grid, row) {
                                if (row.entity.currentStatus == app.localize('Pending') || row.entity.currentStatus == app.localize('Drafted'))
                                    return 'ui-pending';
                                else if (row.entity.currentStatus == app.localize('Approved') || row.entity.currentStatus == app.localize('Excess'))
                                    return 'ui-approved';
                                else if (row.entity.currentStatus == app.localize('Rejected'))
                                    return 'ui-rejected';
                                else if (row.entity.currentStatus == app.localize('Received'))
                                    return 'ui-received';
                            },
                    },
                    {
                        name: app.localize('Location'),
                        field: 'requestLocationRefName',
                        minWidth: 150,
                    },
                    {
                        name: app.localize('Material') + ' ' + app.localize('Count'),
                        field: 'noOfMaterials',
                        cellClass: 'ui-ralign',
                        maxWidth: 80
                    },
                    {
                        name: app.localize('CurrentStatus'),
                        field: 'currentStatus', //  poStatusRemarks
                        maxWidth : 110,
                        cellTemplate:
                            "<div ng-click=\"grid.appScope.viewInterTransferDetail(row.entity)\" class=\"ui-grid-cell-contents\">" +
                            "  <span class=\"bold\"> {{row.entity.currentStatus}} <span>" +
                            "  <br ng-if=\"row.entity.poStatusRemarks.length>0\">" +
                            "  <a ng-if=\"row.entity.poStatusRemarks.length>0\"> " + app.localize("PO") + ' ' + app.localize("Status") + " : {{row.entity.poStatusRemarks}} <a>" +
                            "  <br ng-if=\"row.entity.doesSplitNeeded\">" +
                            "  <a ng-if=\"row.entity.noOfPoDependent>0\"> " + app.localize("PO") + ' ' + app.localize("Dependent") + " : {{row.entity.noOfPoDependent}} <a>" +
                            "  <br ng-if=\"row.entity.doesSplitNeeded\">" +
                            "  <a ng-if=\"row.entity.noOfStockDependent>0\"> " + app.localize("Stock") + ' ' + app.localize("Dependent") + " : {{row.entity.noOfStockDependent}} <a>" +
                            "</div>",
                        cellClass:
                            function (grid, row) {
                                if (row.entity.currentStatus == app.localize('Pending') || row.entity.currentStatus == app.localize('Drafted'))
                                    return 'ui-pending';
                                else if (row.entity.currentStatus == app.localize('Approved') || row.entity.currentStatus == app.localize('Excess'))
                                    return 'ui-approved';
                                else if (row.entity.currentStatus == app.localize('Rejected'))
                                    return 'ui-rejected';
                                else if (row.entity.currentStatus == app.localize('Received'))
                                    return 'ui-received';
                            },
                    },
                    {
                        name: app.localize('Request') + ' ' + app.localize('AccountDate'),
                        field: 'requestDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                        maxWidth:120
                    },
                    {
                        name: app.localize('DeliveryDateRequested'),
                        field: 'deliveryDateRequested',
                        maxWidth: 150,
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm\'',
                        cellClass:
                            function (grid, row) {
                                if (row.entity.deliverDateAlertFlag)
                                    return 'ui-pending';
                            },
                    },
                    {
                        name: app.localize('Split'),
                        enableSorting: true,
                        field: 'doesSplitNeeded',
                        maxWidth: 80,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-if="row.entity.doesSplitNeeded" ng-click="grid.appScope.spiltRequest(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-columns"></i></button>' +
                            '  <br>' +
                            '</div>',
                    },
                    {
                        name: app.localize('Request') + ' ' +  app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm\'',
                        maxWidth : 150
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });

                    gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                        if (row.isSelected) {
                            vm.pushSelectedRow(row.entity);
                        }
                        else {
                            for (var i = 0; i < vm.selectedRecords.length; i++) {
                                var obj = vm.selectedRecords[i];

                                if (obj.id == row.entity.id) {
                                    vm.selectedRecords.splice(i, 1);
                                }
                            }
                        }
                    });

                    gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
                        //var msg = 'rows changed ' + rows.length;
                        console.log('rows changed ' + rows.length);
                        angular.forEach(rows, function (row, key) {
                            if (row.isSelected)
                                vm.pushSelectedRow(row.entity);
                            else {
                                for (var i = 0; i < vm.selectedRecords.length; i++) {
                                    var obj = vm.selectedRecords[i];

                                    if (obj.id == row.entity.id) {
                                        vm.selectedRecords.splice(i, 1);
                                    }
                                }
                            }
                        });
                    });
                },
                data: []
            };

            vm.spiltRequest = function (row) {
                vm.loading = true;
                intertransferService.splitRequestAsPOBasedDependentAndExistingStockDependent({
                    id : row.id
                }).success(function (result) {
                    vm.getAll();
                    var message = app.localize('SplitMessage1', result.id);
                    abp.notify.info(message);
                    abp.message.info(message);
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.clear = function () {
                vm.filterText = null;
                vm.dateRangeModel = null;
                vm.dateFilterApplied = false;
                vm.deliveryDateFilterApplied = false;
                vm.deliveryDateRangeModel = null;
                vm.status = null;
                vm.id = null;
                vm.getAll();

            }

            vm.getAll = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var currentStatus = null;

                if (vm.dateFilterApplied) {
                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }

                var deliveryStartDate = null;
                var deliveryEndDate = null;

                if (vm.deliveryDateFilterApplied) {
                    deliveryStartDate = moment(vm.deliveryDateRangeModel.startDate).format($scope.format);
                    deliveryEndDate = moment(vm.deliveryDateRangeModel.endDate).format($scope.format);
                }
                currentStatus = vm.status;
                vm.selectedRecords = [];
                console.log("Approval Index.js GetViewForApproved() Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                interTransferSupplementaryService.getViewForApproved({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    requestStatus: '',
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    currentStatus: currentStatus,
                    showOnlyDeliveryDateExpectedAsOnDate: vm.showOnlyDeliveryDateExpectedAsOnDate,
                    locationGroup: vm.locationGroup,
                    deliveryStartDate: deliveryStartDate,
                    deliveryEndDate: deliveryEndDate,
                    materialGroupRefId: vm.materialGroupRefId,
                    materialGroupCategoryRefId: vm.materialGroupCategoryRefId,
                    id : vm.id
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                    if (vm.firstTimeFlag)
                        vm.getDashBoard();

                    console.log("Approval Index.js GetViewForApproved() End Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                });
            };

            vm.editInterTransfer = function (myObj) {
                var currentStatus = myObj.currentStatus;
                if (currentStatus == app.localize('Pending'))
                    openCreateOrEditModal(myObj.id);
                else
                    abp.notify.info(app.localize('EditNotAllowedApproved'));
            };

            vm.deleteInterTransfer = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteInterTransferWarning', myObject.Id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            intertransferService.deleteInterTransfer({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.consolidatedExcel = function () {
                vm.saving = true;
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var currentStatus = null;

                var deliveryStartDate = null;
                var deliveryEndDate = null;

                if (vm.dateFilterApplied) {
                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }

                if (vm.deliveryDateFilterApplied) {
                    deliveryStartDate = moment(vm.deliveryDateRangeModel.startDate).format($scope.format);
                    deliveryEndDate = moment(vm.deliveryDateRangeModel.endDate).format($scope.format);
                }
                currentStatus = vm.status;
                console.log("Approval Index.js getConsolidatedToExcel() Call Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                intertransferService.getConsolidatedToExcel({
                    sorting: requestParams.sorting,
                    requestStatus: '',
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    currentStatus: currentStatus,
                    showOnlyDeliveryDateExpectedAsOnDate: vm.showOnlyDeliveryDateExpectedAsOnDate,
                    locationGroup: vm.locationGroup,
                    deliveryStartDate: deliveryStartDate,
                    deliveryEndDate: deliveryEndDate,
                    materialGroupRefId: vm.materialGroupRefId,
                    materialGroupCategoryRefId: vm.materialGroupCategoryRefId,
                    DoesPendingRequest : true
                }).success(function (result) {
                    app.downloadTempFile(result);
                    vm.saving = false;
                    vm.loading = false;
                });
            };

            vm.approveTransfer = function (myObj) {
                var currentStatus = myObj.currentStatus;

                if (currentStatus == app.localize('Received')) {
                    abp.message.error(app.localize('ApproveNotAllowedForReceived'));
                    return;
                }

                if (currentStatus == app.localize('PartiallyReceived')) {
                    abp.message.error(app.localize('ApproveNotAllowedForPartialReceived'));
                    return;
                }

                if (currentStatus == app.localize('Completed')) {
                    abp.message.error(app.localize('ApproveNotAllowedForCompleted'));
                    return;
                }

                if (currentStatus == app.localize('Approved')) {
                    abp.message.warn(app.localize('AlreadyApproved'));
                    return;
                }

                if (currentStatus == app.localize('Cancelled')) {
                    abp.message.error(app.localize('ApproveNotAllowedForCancel'));
                    return;
                }
                else
                    openCreateOrEditModal(myObj);

            }

            vm.createApproval = function () {
                openCreateOrEditModal(null);
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.intertransferapprovaldetail", {
                    id: objId.id, requestLocationRefName: objId.requestLocationRefName,
                    locationRefName: objId.locationRefName
                });
            }

            vm.bulkAutoApproval = function () {
                //vm.selectedRecords
                var listOfSelectedIds = [];
                vm.loadingCount++;
                angular.forEach(vm.selectedRecords, function (myObj, key) {
                    var errorFlag = false;
                    var currentStatus = myObj.currentStatus;

                    if (currentStatus == app.localize('Received')) {
                        abp.notify.error(app.localize('ApproveNotAllowedForReceived') + ', Id :' + myObj.id);
                        errorFlag = true;
                    }

                    if (currentStatus == app.localize('PartiallyReceived')) {
                        abp.notify.error(app.localize('ApproveNotAllowedForPartialReceived') + ', Id :' + myObj.id);
                        errorFlag = true;
                    }

                    if (currentStatus == app.localize('Completed')) {
                        abp.notify.error(app.localize('ApproveNotAllowedForCompleted') + ', Id :' + myObj.id);
                        errorFlag = true;
                    }

                    if (currentStatus == app.localize('Approved')) {
                        abp.notify.warn(app.localize('AlreadyApproved') + ', Id :' + myObj.id);
                        errorFlag = true;
                    }

                    if (currentStatus == app.localize('Cancelled')) {
                        abp.notify.error(app.localize('ApproveNotAllowedForCancel') + ', Id :' + myObj.id);
                        errorFlag = true;
                    }
                    if (errorFlag==false)
                        listOfSelectedIds.push({ id: myObj.id });
                });

                if (listOfSelectedIds.length > 0) {
                    vm.loadingCount++;
                    interTransferCrudService.bulkAutoApproval({
                        BulkApprovalInputs: listOfSelectedIds
                    }).success(function (result) {
                        vm.getAll();
                        abp.notify.info(app.localize('Bulk') + ' ' + app.localize('Approval') + ' ' + app.localize('Done'));
                        abp.message.info(app.localize('Bulk') + ' ' + app.localize('Approval') + ' ' + app.localize('Done'));
                        vm.selectedRecords = [];
                    }).finally(function () {
                        vm.loadingCount--;
                    });
                }
                vm.loadingCount--;
            }

            vm.autoApproval = function (myObj) {
                var currentStatus = myObj.currentStatus;

                if (currentStatus == app.localize('Received')) {
                    abp.message.error(app.localize('ApproveNotAllowedForReceived') + ", Id :" + myObj.id );
                    return;
                }

                if (currentStatus == app.localize('PartiallyReceived')) {
                    abp.message.error(app.localize('ApproveNotAllowedForPartialReceived') + ", Id :" + myObj.id);
                    return;
                }

                if (currentStatus == app.localize('Completed')) {
                    abp.message.error(app.localize('ApproveNotAllowedForCompleted') + ", Id :" + myObj.id);
                    return;
                }

                if (currentStatus == app.localize('Approved')) {
                    abp.message.warn(app.localize('AlreadyApproved') + ", Id :" + myObj.id);
                    return;
                }

                if (currentStatus == app.localize('Cancelled')) {
                    abp.message.error(app.localize('ApproveNotAllowedForCancel') + ", Id :" + myObj.id);
                    return;
                }

                vm.loading = true;
                interTransferCrudService.triggerAutoApprovalForGivenTransferRequestId({
                    Id: myObj.id
                }).success(function (result) {
                    vm.getAll();
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.requestToAutoPO = function () {
                $state.go("tenant.requestintoAutoPo", {

                });
            }


            vm.getDashBoard = function () {
                vm.loadingDashboard = true;
                interTransferSupplementaryService.getDashBoardTransferApproval({
                    Id: vm.defaultLocationId
                }).success(function (result) {
                    vm.totalPending = result.totalPending;
                    vm.totalApproved = result.totalApproved;
                    vm.totalReceived = result.totalReceived;
                    vm.totalRejected = result.totalRejected;
                }).finally(function () {
                    vm.loadingDashboard = false;
                    vm.firstTimeFlag = false;
                });
            };


            vm.printApproval = function (myObject) {
                var currentStatus = myObject.currentStatus;
                if (currentStatus == app.localize('Approved') || currentStatus == app.localize('Completed')
                    || currentStatus == app.localize('Received') || currentStatus == app.localize('PartiallyReceived')) {


                    $state.go('tenant.intertransferapprovalprint', {
                        printid: myObject.id
                    });
                }
                else {
                    abp.message.error(app.localize('CouldNotPrintApproval'), app.localize(currentStatus));
                    return;
                }
            };

            

            vm.getAll();


            vm.checkCloseDay = function () {

                if (vm.transfervalueshown == true) {
                    vm.columnprice =
                        {
                            name: app.localize('TransferValue'),
                            field: 'transferValue',
                            cellClass: 'ui-ralign',
                            maxWidth: 100
                        };
                    vm.userGridOptions.columnDefs.splice(4, 0, vm.columnprice);
                }

                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (result.currentUserId != vm.currentUserId) {
                            vm.currentLanguage = abp.localization.currentLanguage;
                            location.href = abp.appPath + 'Localization/ChangeCulture?cultureName=' + vm.currentLanguage.name + '&returnUrl=' + window.location.href;
                            var errorMessage = app.localize('UserIdChangedErrorAlert', vm.currentUserId, result.currentUserId)
                            abp.notify.error(errorMessage);
                            abp.message.error(errorMessage);
                        }
                        if (vm.softmessagenotificationflag)
                            abp.notify.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        //if (vm.messagenotificationflag)
                        //    abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                }).finally(function () {
                });
            }

            vm.checkCloseDay();

            vm.createInterTransfer = function (argOption) {
                openDirectApprovalModal(null, argOption);
            };

            function openDirectApprovalModal(objId, argOption) {
                if (argOption == 1) {
                    $state.go("tenant.intertransferapprovalandrequestdetail", {
                        id: objId
                    });
                }
                else {
                    $state.go("tenant.intertransferrequestdetail", {
                        id: objId,
                        directTransfer: true
                    });
                }
            }



            //  Location Link Start
            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                    vm.getAll();
                });
            };

            //  Location Link End

            vm.viewInterTransferDetailParent = function (myObject) {
                vm.loading = true;
                openView(myObject.parentRequestRefId);
            }

            vm.viewInterTransferDetail = function (myObject) {
                vm.loading = true;
                openView(myObject.id);
            };

            function openView(objId) {
                vm.loading = true;
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/intertransfer/request/requestViewInGrid.cshtml',
                    controller: 'tenant.views.house.transaction.intertransfer.requestViewInGrid as vm',
                    backdrop: 'static',
                    resolve: {
                        transferId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    // vm.getAll();
                }).finally(function (result) {
                    //  vm.getAll();
                    });
                vm.loading = false;
            }

        }]);
})();

