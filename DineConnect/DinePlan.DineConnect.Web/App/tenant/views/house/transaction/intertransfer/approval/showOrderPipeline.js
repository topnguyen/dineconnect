﻿(function () {
    appModule.controller('tenant.views.house.transaction.intertransfer.approval.showOrderPipeline', [
        '$scope', '$uibModalInstance', 'data', 'materialRefId', 'materialRefName', 'uom', 'alreadyOrderedQuantity' ,
        function ($scope, $modalInstance, data, materialRefId, materialRefName, uom, alreadyOrderedQuantity) {
            var vm = this;

            vm.loading = false;
            vm.materialRefId = materialRefId;
            vm.materialRefName = materialRefName;
            vm.uom = uom;
            vm.alreadyOrderedQuantity = alreadyOrderedQuantity;

            vm.data = data;
            vm.norecordsfound = false;
            if (data.pipelineDetails.length == 0) {
                vm.norecordsfound = true;
            }

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.save = function () {
                $modalInstance.close(vm.defaultSupplier);
            };

            vm.changeDefaultSupplier = function (argData) {
                vm.defaultSupplier = argData;
                vm.data.defaultSupplier = argData;
            };

        }
    ]);
})();