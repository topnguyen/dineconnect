﻿(function () {
    appModule.controller('tenant.views.house.transaction.approval.editRequest', [
        '$scope', '$uibModalInstance', 'abp.services.app.menuItem', 'locationId', 'allItems', 'initialText',
        function ($scope, $modalInstance, menuService, locationId, allItems, initialText) {
            var vm = this;
            /* eslint-disable */
            vm.allItems = allItems;
            vm.currentText = initialText;
            vm.editingText = angular.extend({}, vm.currentText);
            vm.locationId = locationId;

            vm.permissions = {
                hideStock: abp.auth.hasPermission('Pages.Tenant.House.Transaction.DayClose.HideStockInTransfer'),
                negativeStockAllowedAuthorization: abp.auth.hasPermission('Pages.Tenant.House.Master.MaterialLedger.NegativeStock'),
                canChangeRequestQtyInApprovalScreen: abp.auth.hasPermission('Pages.Tenant.House.Transaction.InterTransferApproval.CanChangeRequestQuantity'),
            };

            vm.currentIndex = 0;
            vm.saving = false;

            //Calculating index of currentText in allItems
            for (var i = 0; i < allItems.length; i++) {
                if (vm.allItems[i] == vm.currentText) {
                    vm.currentIndex = i;
                    break;
                }
            }

            function save(close) {
                function executeAfterAction() {
                    if (close) {
                        $modalInstance.close(vm.allItems);
                    } else {
                        //Go to next
                        vm.currentIndex++;
                        if (vm.allItems.length <= vm.currentIndex) {
                            $modalInstance.close(vm.allItems);
                            return;
                        }

                        vm.currentText = vm.allItems[vm.currentIndex];
                        vm.editingText = angular.extend({}, vm.currentText);
                        if (vm.editingText.issueQty == 0)
                            vm.editingText.issueQty = "";
                    }
                }

                if (vm.editingText.requestQty == 0) {
                    abp.notify.warn(app.localize("RequestQtyShouldNotBeZero"));
                    return;
                }

                if (vm.editingText.issueQty == "")
                    vm.editingText.issueQty = 0;

                if (vm.currentText.requestQty == vm.editingText.issueQty) {

                    vm.currentText.returnFlag = false;
                    vm.currentText.returnQty = 0;
                    vm.currentText.returnMode = '';

                    //executeAfterAction();
                    //return;
                }

                if (vm.editingText.onHand < vm.editingText.issueQty) {
                    abp.notify.warn(app.localize("OnHandLessThanRequestQtyErr", vm.editingText.materialRefName, vm.editingText.onHand, vm.editingText.issueQty));
                    if (vm.permissions.negativeStockAllowedAuthorization == false)
                        return;
                }

                if (vm.editingText.issueQty != vm.editingText.requestQty) {
                    if (vm.editingText.returnFlag == false && vm.editingText.omitFlag == false && vm.editingText.diffQty > 0) {
                        abp.notify.warn(app.localize('DifferenceActionNeedsToMentioned', vm.editingText.materialRefName));
                        return;
                    }

                    abp.notify.info(app.localize('MismatchBetweenIssueQtyAndRequestQtyErr', vm.editingText.issueQty, vm.editingText.requestQty));

                    //abp.message.warn(app.localize('MismatchBetweenIssueQtyAndReceivedQtyErr',vm.editingText.receivedQty));
                    //return;


                }

                vm.currentText.issueQty = vm.editingText.issueQty;

                if (vm.editingText.omitFlag) {
                    vm.editingText.omitQty = Math.abs(vm.editingText.diffQty);
                }

                if (vm.editingText.returnFlag) {
                    vm.editingText.returnQty = Math.abs(vm.editingText.diffQty)
                }

                vm.currentText.requestQty = vm.editingText.requestQty;

                vm.currentText.omitQty = vm.editingText.omitQty;
                vm.currentText.omitMode = vm.editingText.omitMode;
                vm.currentText.omitFlag = vm.editingText.omitFlag;

                vm.currentText.returnFlag = vm.editingText.returnFlag;
                vm.currentText.returnQty = vm.editingText.returnQty;
                vm.currentText.returnMode = vm.editingText.returnMode;
                
                vm.currentText.diffQty = vm.editingText.diffQty;

                var iqty = parseFloat(vm.currentText.issueQty);
                var rqty = parseFloat(vm.currentText.requestQty);

                if (iqty == rqty)
                    vm.currentText.currentStatus = app.localize('Approved');
                else if (iqty == 0 && !vm.currentText.returnFlag)
                    vm.currentText.currentStatus = app.localize('Pending');
                else if (iqty == 0 && vm.currentText.returnFlag)
                    vm.currentText.currentStatus = app.localize('Shortage');
                else if (iqty > rqty)
                    vm.currentText.currentStatus = app.localize('Excess');
                else if (iqty < rqty)
                    vm.currentText.currentStatus = app.localize('Shortage');

                executeAfterAction();
                //abp.notify.info(app.localize('UpdatedSuccessfully'));
            };

            vm.saveAndNext = function () {
                save(false);
            }

            vm.saveAndClose = function () {
                save(true);
            }

            vm.previous = function () {
                if (vm.currentIndex <= 0) {
                    return;
                }

                vm.currentIndex--;
                vm.currentText = vm.allItems[vm.currentIndex];
                vm.editingText = angular.extend({}, vm.currentText);
            };

            vm.targetValueKeyPress = function ($event) {
                if ($event.keyCode == 13) {
                    vm.saveAndNext();
                } else if ($event.keyCode == 37) {
                    vm.previous();
                } else if ($event.keyCode == 39) {
                    vm.saveAndNext();
                }
            }

            vm.cancel = function () {
                $modalInstance.dismiss(vm.allItems);
            };

            vm.adjumentQtyCalc = function () {
                if (vm.editingText.issueQty == null || vm.editingText.issueQty == '') {
                    vm.editingText.omitMode = '';
                    vm.editingText.returnMode = '';
                    vm.editingText.returnFlag = false;
                    vm.editingText.omitFlag = false;
                    vm.editingText.diffQty = null;
                    vm.editingText.diffQty = parseFloat(vm.editingText.requestQty);

                    vm.editingText.diffQty = vm.editingText.diffQty.toFixed(6);
                    return;
                }

                if (vm.editingText.issueQty >= 0)
                    vm.editingText.diffQty = parseFloat(vm.editingText.requestQty) - parseFloat(vm.editingText.issueQty);
                else
                    vm.editingText.diffQty = parseFloat(vm.editingText.requestQty);

                vm.editingText.diffQty = vm.editingText.diffQty.toFixed(6);
                if (vm.editingText.diffQty <= 0) {
                    vm.editingText.omitMode = '';
                    vm.editingText.returnMode = '';
                    vm.editingText.omitQty = 0;
                    vm.editingText.returnQty = 0;
                    vm.editingText.returnFlag = false;
                    vm.editingText.omitFlag = false;
                }
                else if (vm.editingText.diffQty > 0) {
                    vm.editingText.omitMode = app.localize('Shortage');
                }
                //else if (vm.editingText.diffQty < 0) {
                //    vm.editingText.omitMode = ''; //app.localize('Excess');
                //    vm.editingText.omitQty = 0;
                //    vm.editingText.omitFlag = false;
                //}
            }

        }
    ]);
})();