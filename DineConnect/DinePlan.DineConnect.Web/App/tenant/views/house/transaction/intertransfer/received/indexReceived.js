﻿
(function () {
    appModule.controller('tenant.views.house.transaction.intertransfer.received.indexReceived', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.interTransfer', 'appSession', 'abp.services.app.dayClose', 'abp.services.app.material', 'abp.services.app.interTransferRequest',
        function ($scope, $state, $modal, uiGridConstants, intertransferService, appSession, daycloseService, materialService, interTransferRequestService ) {
            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.status = "";
            console.log("IndexReceived.js Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.id = null;

            vm.transfervalueshown = abp.setting.getBoolean("App.House.InterTransferValueShown");
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");


            vm.defaultLocationId = appSession.user.locationRefId;
            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Transaction.InterTransferReceived.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Transaction.InterTransferReceived.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Transaction.InterTransferReceived.Delete'),
                received: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransferReceived.Received')
            };


            vm.refmaterialgroup = [];
            function fillDropDownMaterialGroup() {
                materialService.getMaterialGroupForCombobox({}).success(function (result) {
                    vm.refmaterialgroup = result.items;
                });
            }
            fillDropDownMaterialGroup();

            vm.refmaterialgroupcategory = [];
            vm.fillDropDownMaterialGroupCategory = function () {
                materialService.getMaterialGroupCategoryForCombobox({
                    id: vm.materialGroupRefId
                }).success(function (result) {
                    vm.refmaterialgroupcategory = result.items;
                    if (result.items.length == 1) {
                        vm.materialGroupCategoryRefId = result.items[0].value;
                    }
                    else {
                        vm.materialGroupCategoryRefId = null;
                    }
                });
            }

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.advancedFiltersAreShown = false;
            vm.dateFilterApplied = false;

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };


            $('input[name="deliveryDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment().add(90, 'days').format('YYYY-MM-DD'),
            });

            vm.deliveryDateRangeOptions = app.createDateRangePickerOptions();
            vm.deliveryDateRangeOptions.ranges[app.localize('Tomorrow')] = [moment().add(1, 'days').startOf('day'), moment().add(1, 'days').endOf('day')];
            vm.deliveryDateRangeOptions.ranges[app.localize('Next7Days')] = [moment().startOf('day'), moment().add(6, 'days').endOf('day')];
            vm.deliveryDateRangeOptions.ranges[app.localize('Next30Days')] = [moment().startOf('day'), moment().add(29, 'days').endOf('day')];

            vm.deliveryDateRangeOptions.max = moment().add(90, 'days').format('YYYY-MM-DD');
            vm.deliveryDateRangeOptions.maxDate = moment().add(90, 'days').format('YYYY-MM-DD');

            vm.deliveryDateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        maxWidth: 100,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.received\" ng-click=\"grid.appScope.receivedInterTransfer(row.entity)\">" + app.localize("Receiving") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.received\" ng-click=\"grid.appScope.printReceivedInterTransfer(row.entity)\">" + app.localize("Print") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Id'),
                        field: 'id',
                        maxWidth: 80
                    },
                    {
                        name: app.localize('Location'),
                        field: 'locationRefName'
                    },
                    {
                        name: app.localize('RequestLocation'),
                        field: 'requestLocationRefName'
                    },
                    {
                        name: app.localize('CurrentStatus'),
                        field: 'currentStatus'
                    },
                    {
                        name: app.localize('Request') + ' ' + app.localize('AccountDate'),
                        field: 'requestDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                        minWidth: 100
                    },
                    {
                        name: app.localize('DeliveryDateRequested'),
                        field: 'deliveryDateRequested',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm\'',
                        cellClass:
                            function (grid, row) {
                                if (row.entity.deliverDateAlertFlag)
                                    return 'ui-pending';
                            },

                    },
                    {
                        name: app.localize('Request') + ' ' + app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm\'',
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.clear = function () {
                vm.filterText = null;
                vm.dateRangeModel = null;
                vm.dateFilterApplied = false;
                vm.status = null;
                vm.id = null;
                vm.getAll();
            }

            vm.getAll = function () {
                vm.loading = true;
                var startDate = null;
                var endDate = null;
                var currentStatus = null;

                if (vm.dateFilterApplied) {
                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }

                currentStatus = vm.status;

                var deliveryStartDate = null;
                var deliveryEndDate = null;

                if (vm.deliveryDateFilterApplied) {
                    deliveryStartDate = moment(vm.deliveryDateRangeModel.startDate).format($scope.format);
                    deliveryEndDate = moment(vm.deliveryDateRangeModel.endDate).format($scope.format);
                }
                console.log("indexReceived.js getViewInterTransfer() Call Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                interTransferRequestService.getViewInterTransfer({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    requestStatus: 'A',
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    currentStatus: currentStatus,
                    deliveryStartDate: deliveryStartDate,
                    deliveryEndDate: deliveryEndDate,
                    materialGroupRefId: vm.materialGroupRefId,
                    materialGroupCategoryRefId: vm.materialGroupCategoryRefId,
                    id: vm.id
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                    console.log("indexReceived.js getViewInterTransfer() Call End Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                });
            };

            vm.printReceivedInterTransfer = function (myObject) {
                if (myObject.currentStatus == app.localize('Received')) {
                    $state.go('tenant.intertransferreceivedprint', {
                        printid: myObject.id
                    });
                }
                else {
                    abp.notify.error(app.localize('PrintAllowedOnlyForReceived'));
                    abp.message.info(app.localize('PrintAllowedOnlyForReceived'));
                }
            };

            vm.editInterTransfer = function (myObj) {
                var currentStatus = myObj.currentStatus;
                if (currentStatus == app.localize('Pending'))
                    openCreateOrEditModal(myObj.id);
                else
                    abp.notify.info(app.localize('ReceivingAllowedOnlyForApproved'));
            };

            vm.receivedInterTransfer = function (myObj) {
                var currentStatus = myObj.currentStatus;
                if (currentStatus == app.localize('Approved'))
                    openCreateOrEditModal(myObj);
                else
                    abp.notify.error(app.localize('ReceivingAllowedOnlyForApproved'));
            }

            vm.createInterTransfer = function () {
                openCreateOrEditModal(null);
            };

            function openCreateOrEditModal(objId) {

                $state.go("tenant.intertransferreceiveddetail", {
                    id: objId.id, requestLocationRefName: objId.locationRefName
                });

                //var modalInstance = $modal.open({
                //    templateUrl: '~/App/tenant/views/house/transaction/intertransfer/createOrEditModal.cshtml',
                //    controller: 'tenant.views.house.transaction.intertransfer.createOrEditModal as vm',
                //    backdrop: 'static',
                //	keyboard: false,
                //    resolve: {
                //        intertransferId: function () {
                //            return objId;
                //        }
                //    }
                //});

                //modalInstance.result.then(function (result) {
                //    vm.getAll();
                //});
            }

            vm.exportToExcel = function () {
                vm.loading = true;
                var startDate = null;
                var endDate = null;
                var currentStatus = null;

                if (vm.dateFilterApplied) {

                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }

                currentStatus = vm.status;

                var deliveryStartDate = null;
                var deliveryEndDate = null;

                if (vm.deliveryDateFilterApplied) {
                    deliveryStartDate = moment(vm.deliveryDateRangeModel.startDate).format($scope.format);
                    deliveryEndDate = moment(vm.deliveryDateRangeModel.endDate).format($scope.format);
                }

                interTransferRequestService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    requestStatus: 'A',
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    currentStatus: currentStatus,
                    deliveryStartDate: deliveryStartDate,
                    deliveryEndDate: deliveryEndDate,
                    materialGroupRefId: vm.materialGroupRefId,
                    materialGroupCategoryRefId: vm.materialGroupCategoryRefId,
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            }

            vm.getAll();

            vm.checkCloseDay = function () {
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        //if (vm.messagenotificationflag)
                        //    abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                }).finally(function () {
                });
            }

            vm.checkCloseDay();




        }]);
})();

