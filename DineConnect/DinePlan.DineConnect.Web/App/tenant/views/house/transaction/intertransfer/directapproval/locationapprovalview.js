﻿(function () {
    appModule.controller('tenant.views.house.transaction.directapproval.locationapprovalview', [
        '$scope', '$uibModalInstance', 'materialrequestlist', 'locationRefName', 'requestlocationRefName',
        function ($scope, $modalInstance, materialrequestlist, locationRefName, requestlocationRefName) {
            var vm = this;

            vm.materialrequestlist = materialrequestlist;
            vm.locationRefName = locationRefName;
            vm.requestlocationRefName = requestlocationRefName;

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

        }
    ]);
})();