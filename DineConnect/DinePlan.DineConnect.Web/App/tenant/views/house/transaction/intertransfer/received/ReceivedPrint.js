﻿(function () {
    appModule.controller('tenant.views.house.transaction.intertransfer.ReceivedPrint', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.interTransfer', 'appSession',
        'abp.services.app.location', 'abp.services.app.adjustment',
        function ($scope, $filter, $state, $stateParams, $modal, uiGridConstants,
            intertransferService, appSession, locationService, adjustmentService) {

            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.loadingCount = 0;
            vm.connectdecimals = appSession.connectdecimals;
            vm.housedecimals = appSession.housedecimals;

            vm.permissions = {
                printMaterialCode: abp.auth.hasPermission('Pages.Tenant.House.Master.Material.PrintMaterialCode'),
            };

            vm.gotoIndex = function () {
                $state.go("tenant.intertransferreceived");
            }

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;

            vm.intertransferreceived = null;
            vm.intertransferreceiveddetail = null;

            vm.transfervalueshown = abp.setting.getBoolean("App.House.InterTransferValueShown");

            vm.intertransferrequestId = $stateParams.printid;

            vm.getData = function () {
                vm.loadingCount++;
                intertransferService.getInterReceivedForEdit({
                    Id: vm.intertransferrequestId
                }).success(function (result) {

                    vm.intertransfer = result.interTransfer;

                    if (vm.intertransfer.locationRefId != vm.defaultLocationId) {
                        $state.go("tenant.intertransferreceived");
                        return;
                    }

                    var currentStatus = vm.intertransfer.currentStatus;

                    vm.tempdetaildata = [];
                    vm.intertransferreceiveddetail = result.interTransferReceivedDetail;
                    vm.sno = 0;

                    angular.forEach(result.interTransferReceivedDetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        if (value.issueQty >= 0) {
                            vm.tempdetaildata.push({
                                'interTransferRefId': value.interTransferRefId,
                                'sno': vm.sno,
                                'materialRefId': value.materialRefId,
                                'materialPetName': value.materialPetName,
                                'materialRefName': value.materialRefName,
                                'requestQty': value.requestQty,
                                'issueQty': value.issueQty,
                                'receivedQty': value.receivedQty,
                                'currentStatus': value.currentStatus,
                                'requestRemarks': value.requestRemarks,
                                'approvedRemarks': value.approvedRemarks,
                                'id': value.id,
                                'uom': value.uom,
                                'onHand': value.onHand,
                                'materialTypeName': value.materialTypeName,
                                'price': value.price,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'defaultUnitId': value.defaultUnitId,
                                'defaultUnitName': value.defaultUnitName,
                                'lineTotal': value.lineTotal
                            });
                        }
                    });

                    if (vm.intertransfer.receivedPersonId != 0) {
                        locationService.getUserInfoBasedOnUserId({
                            id: vm.intertransfer.receivedPersonId
                        }).success(function (result) {
                            vm.user = result;
                        });
                    }

                    locationService.getLocationForEdit({
                        Id: vm.intertransfer.locationRefId
                    }).success(function (result) {
                        vm.billinglocation = result.location;
                    });

                    locationService.getLocationForEdit({
                        Id: vm.intertransfer.requestLocationRefId
                    }).success(function (result) {
                        vm.shippinglocation = result.location;
                    });

                    if (vm.intertransfer.interTransferRefId) {
                        vm.getApprovalData(vm.intertransfer.interTransferRefId);
                    }

                    if (vm.intertransfer.adjustmentRefId) {
                        vm.getAdjustmentData(vm.intertransfer.adjustmentRefId);
                    }
                }).finally(function (result) {
                    vm.loadingCount--;
                });
            }

            vm.getData();
            vm.getApprovalData = function (argTransApprovalId) {
                vm.loadingCount++;
                intertransferService.getInterTransferApprovalForEdit({
                    id: argTransApprovalId,
                }).success(function (result) {

                    vm.intertransferapproval = result.interTransfer;

                    //if (vm.intertransferapproval.requestLocationRefId != vm.defaultLocationId) {
                    //    $state.go("tenant.intertransferapproval");
                    //    return;
                    //}

                    var currentStatus = vm.intertransferapproval.currentStatus;

                    vm.approvaldata = [];
                    vm.intertransferapprovaldetail = result.interTransferDetail;
                    vm.sno = 0;

                    angular.forEach(result.interTransferDetail, function (value, key) {
                        if (value.issueQty > 0) {
                            vm.sno = vm.sno + 1;
                            vm.approvaldata.push({
                                'interTransferRefId': value.interTransferRefId,
                                'sno': vm.sno,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'requestQty': value.requestQty,
                                'issueQty': value.issueQty,
                                'price': value.price,
                                'currentStatus': value.currentStatus,
                                'requestRemarks': value.requestRemarks,
                                'approvedRemarks': value.approvedRemarks,
                                'id': value.id,
                                'uom': value.uom,
                                'onHand': value.onHand,
                                'materialTypeName': value.materialTypeName,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'defaultUnitId': value.defaultUnitId,
                                'defaultUnitName': value.defaultUnitName
                            });
                        }
                    });


                    locationservice.getuserinfobasedonuserid({
                        id: vm.intertransferapproval.approvedpersonid
                    }).success(function (result) {
                        vm.user = result;
                    });

                    locationservice.getlocationforedit({
                        id: vm.intertransferapproval.locationrefid
                    }).success(function (result) {
                        vm.billinglocation = result.location;
                    });

                    locationservice.getlocationforedit({
                        id: vm.intertransferapproval.requestlocationrefid
                    }).success(function (result) {
                        vm.shippinglocation = result.location;
                    });

                }).finally(function (result) {
                    vm.loadingCount--;
                });



            }

            vm.getAdjustmentData = function (argAdjustmentId) {
                adjustmentService.getAdjustmentForEdit({
                    Id: argAdjustmentId
                }).success(function (result) {
                    vm.adjustment = result.adjustment;
                    vm.adjustmentDetail = result.adjustmentDetail;

                    if (vm.adjustment.locationRefId != vm.defaultLocationId) {
                        $state.go("tenant.adjustment");
                        return;
                    }

                    vm.adjustmentDetailPortion = [];

                    vm.sno = 0;

                    angular.forEach(vm.adjustmentDetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.adjustmentDetailPortion.push({
                            'sno': vm.sno,
                            'adjustmentRefIf': value.adjustmentRefIf,
                            'materialRefId': value.materialRefId,
                            'materialPetName': value.materialPetName,
                            'materialRefName': value.materialRefName,
                            'adjustmentQty': value.adjustmentQty,
                            'defaultUnitId': value.defaultUnitId,
                            'defaultUnitName': value.defaultUnitName,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'adjustmentMode': value.adjustmentMode,
                            'adjustmentApprovedRemarks': value.adjustmentApprovedRemarks,
                        });
                    });

                });
            }

        }]);
})();