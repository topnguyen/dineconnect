﻿(function () {
    appModule.controller('tenant.views.house.transaction.intertransfer.approval.requestAutoPo', [
        '$scope', '$state', '$uibModal',  'abp.services.app.interTransfer', 'appSession', 'abp.services.app.location', 'abp.services.app.dayClose', 'NgTableParams', '$rootScope', 'abp.services.app.purchaseOrderExtended', 'abp.services.app.interTransferSupplementary',
        function ($scope, $state, $modal, intertransferService, appSession, locationService, daycloseService, ngTableParams, $rootScope, purchaseorderExtendedService, interTransferSupplementaryService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = true;
            vm.filterText = null;
            $rootScope.settings.layout.pageSidebarClosed = true;
            vm.currentUserId = abp.session.userId;
            vm.transfervalueshown = abp.setting.getBoolean("App.House.InterTransferValueShown");
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");
            vm.showOnlyDeliveryDateExpectedAsOnDate = false;
            vm.enterOnHandFilter = -1;
            vm.omitQueueInOrder = true;

            vm.saving = false;

            vm.defaultLocationId = appSession.user.locationRefId;

            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Transaction.InterTransferApproval.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Transaction.InterTransferApproval.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Transaction.InterTransferApproval.Delete'),
                approve: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransferApproval.Approve'),
                directapprove: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransferApproval.DirectApprove'),
                directapproveandreceive: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransferApproval.DirectApproveAndReceive'),
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };

            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                    vm.getAll();
                });
            };

            //  Location Link End
            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.deliveryDateExpected = moment().format($scope.format);
            $('input[name="dueDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: vm.deliveryDateExpected,
                minDate: $scope.minDate,
            });


            var requestParams = {
                skipCount: 0,
                maxResultCount: 10, // app.consts.grid.defaultPageSize,
                sorting: null
            };

            $scope.getLValue = function (column) {
                return app.localize(column);
            };

            vm.advancedFiltersAreShown = false;
            vm.dateFilterApplied = false;

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.clear = function () {
                vm.filterText = null;
                vm.dateRangeModel = null;
                vm.dateFilterApplied = false;
                vm.getAll();
            }

            vm.tableParams = new ngTableParams({}, { dataset: [], counts: [] });
            vm.tableParamsMaterial = new ngTableParams({}, { dataset: [], counts: [] });

            vm.requestList = [];

            vm.getAll = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var currentStatus = 'Pending';

                if (vm.dateFilterApplied) {
                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }
                console.log("requestAutoPo.js GetViewForApproved() Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));

                interTransferSupplementaryService.getViewForApproved({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    requestStatus: '',
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    currentStatus: currentStatus,
                    showOnlyDeliveryDateExpectedAsOnDate: vm.showOnlyDeliveryDateExpectedAsOnDate,
                    locationGroup: vm.locationGroup,
                    omitAlreadyPoLinkedRequest : true
                }).success(function (result) {
                    vm.tableParams = new ngTableParams({}, { dataset: result.items, counts: [] });
                    vm.requestList = result.items;
                    vm.materialPoList = [];
                    //if (vm.materialPoList.length > 0) {
                    //    vm.getMaterialPoList();
                    //}
                    vm.smartLocationSelection();
                }).finally(function () {
                    vm.loading = false;
                    console.log("requestAutoPo.js GetViewForApproved() Stop Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                });
            };

            vm.availableLocations = [];
            vm.uniqueList = [];
            vm.smartLocationSelection = function () {

                var locationIds = [];
                angular.forEach(vm.requestList, function (value, key) {
                    if (value.doesThisRequestToBeConvertedAsPO)
                        locationIds.push(value.locationRefId);
                });

                vm.uniqueList = [];
                vm.uniqueList = locationIds.filter(function (itm, i, a) {
                    return i == locationIds.indexOf(itm);
                });
                //  If More than one Location Available 
                if (vm.uniqueList.length == 1) {
                    vm.locationRefId = vm.uniqueList[0];
                    vm.shippingLocationId = vm.uniqueList[0];
                }
                else {
                    vm.locationRefId = vm.defaultLocationId;
                    vm.shippingLocationId = vm.defaultLocationId;
                }
                vm.fillSmartLocations();
            }

            vm.fillSmartLocations = function () {
                vm.availableLocations = [];

                var existFlag = false;
                vm.uniqueList.some(function (data, key) {
                    if (data == vm.defaultLocationId) {
                        existFlag = true;
                        return true;
                    }
                });
                if (existFlag == false)
                    vm.uniqueList.push(vm.defaultLocationId);

                angular.forEach(vm.uniqueList, function (value, key) {
                    vm.reflocations.some(function (data, key) {
                        if (data.id == value) {
                            vm.availableLocations.push(data);
                            return true;
                        }
                    });
                });
            }

            vm.materialPoList = [];
            vm.getMaterialPoList = function () {
                vm.materialPoList = [];
                vm.poconvertedRequestList = [];
                angular.forEach(vm.requestList, function (value, key) {
                    if (value.doesThisRequestToBeConvertedAsPO) {
                        vm.poconvertedRequestList.push(value);
                    }
                });

                if (vm.poconvertedRequestList.length == 0) {
                    //abp.notify.warn( 'No More PO Needed Request Found');
                    abp.notify.warn(app.localize('ConsolidatedPoError'));
;                    return;
                }
                vm.saving = true;

                vm.loading = true;
                console.log("requestAutoPo.js GetPoRequestBasedOnSelection() Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));

                intertransferService.getPoRequestBasedOnSelection({
                    requestBasedOnRequestList: true,
                    requestList: vm.poconvertedRequestList,
                    defaultLocationRefId: vm.defaultLocationId,
                    omitQueueInOrder: vm.omitQueueInOrder
                }).success(function (dataresult) {
                    vm.materialPoList = dataresult;
                    angular.forEach(vm.materialPoList, function (value, key) {
                        value.omitQueueInOrder = vm.omitQueueInOrder;
                    });
                    vm.tableParamsMaterial = new ngTableParams({}, { dataset: vm.materialPoList, counts: [] });
                }).finally(function () {
                    vm.saving = false;
                    vm.loading = false;
                    console.log("requestAutoPo.js GetPoRequestBasedOnSelection() End Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                });
            }

            vm.reflocations = [];
            function fillDropDownLocations() {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.reflocations = $.parseJSON(JSON.stringify(result.items));
                    if (vm.reflocations.length == 1) {
                        vm.shippingLocationId = vm.reflocations[0].id;
                        vm.selectedLocationName = vm.reflocations[0].code;
                    }
                }).finally(function (result) {
                });
            }
            fillDropDownLocations();

            vm.showPipelineOrders = function (data) {
                if (data.materialRefId == 0 || data.materialRefId == null)
                    return;

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/intertransfer/approval/showOrderPipeline.cshtml',
                    controller: 'tenant.views.house.transaction.intertransfer.approval.showOrderPipeline as vm',
                    backdrop: 'static',
                    resolve: {
                        data: function () {
                            return data
                        },
                        materialRefId: function () {
                            return data.materialRefId;
                        },
                        materialRefName: function () {
                            return data.materialRefName;
                        },
                        uom: function () {
                            return data.uom;
                        },
                        alreadyOrderedQuantity: function () {
                            return data.alreadyOrderedQuantity;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    var a = result;
                });
                vm.loading = false;
            }

            vm.changeSupplier = function (data) {

                if (data.materialRefId == 0 || data.materialRefId == null)
                    return;

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/intertransfer/approval/changeDefaultSupplier.cshtml',
                    controller: 'tenant.views.house.transaction.intertransfer.approval.changeDefaultSupplier as vm',
                    backdrop: 'static',
                    resolve: {
                        data: function () {
                            return data
                        },
                        materialRefId: function () {
                            return data.materialRefId;
                        },
                        materialRefName: function () {
                            return data.materialRefName;
                        },
                        uom: function () {
                            return data.uom;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    var a = result;
                });
                vm.loading = false;
            }

            vm.previousPriceOpen = function (data) {
                if (data.materialRefId == 0 || data.materialRefId == null)
                    return;

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/purchaseorder/previousPriceModal.cshtml',
                    controller: 'tenant.views.house.transaction.purchaseorder.previousPriceModal as vm',
                    backdrop: 'static',
                    resolve: {
                        materialRefId: function () {
                            return data.materialRefId;
                        },
                        materialRefName: function () {
                            return data.materialRefName;
                        },
                        uom: function () {
                            return data.uom;
                        }
                    }
                });

                modalInstance.result.then(function (result) {

                });
                vm.loading = false;
            }

            vm.consolidatedExcel = function () {
                vm.poconvertedRequestList = [];
                angular.forEach(vm.requestList, function (value, key) {
                    if (value.doesThisRequestToBeConvertedAsPO) {
                        vm.poconvertedRequestList.push(value);
                    }
                });

                if (vm.poconvertedRequestList.length == 0) {
                    //abp.notify.warn('No More PO Needed Request Found');
                    abp.notify.warn(app.localize('ConsolidatedPoError'));

                    return;
                }
                vm.saving = true;
                vm.loading = true;
                console.log("requestAutoPo.js ExportConsolidatedToExcel() Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));

                intertransferService.getConsolidatedPendingBasedGivenRequestList({
                    interTransferViewDtos: vm.poconvertedRequestList,
                    //doesPendingRequest : true
                }).success(function (dataresult) {
                    intertransferService.exportConsolidatedToExcel({
                        exportData: dataresult
                    }).success(function (result) {
                        app.downloadTempFile(result);
                    }).finally(function () {
                        vm.saving = false;
                        vm.loading = false;
                        console.log("requestAutoPo.js ExportConsolidatedToExcel() Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));

                    });
                }).finally(function () {

                });

            };

            vm.approveTransfer = function (myObj) {
                var currentStatus = myObj.currentStatus;

                if (currentStatus == app.localize('Received')) {
                    abp.message.error(app.localize('ApproveNotAllowedForReceived'));
                    return;
                }

                if (currentStatus == app.localize('PartiallyReceived')) {
                    abp.message.error(app.localize('ApproveNotAllowedForPartialReceived'));
                    return;
                }

                if (currentStatus == app.localize('Completed')) {
                    abp.message.error(app.localize('ApproveNotAllowedForCompleted'));
                    return;
                }

                if (currentStatus == app.localize('Approved')) {
                    abp.message.warn(app.localize('AlreadyApproved'));
                    return;
                }

                if (currentStatus == app.localize('Cancelled')) {
                    abp.message.error(app.localize('ApproveNotAllowedForCancel'));
                    return;
                }
                else
                    openCreateOrEditModal(myObj);

            }

            vm.createApproval = function () {
                openCreateOrEditModal(null);
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.intertransferapprovaldetail", {
                    id: objId.id, requestLocationRefName: objId.requestLocationRefName,
                    locationRefName: objId.locationRefName
                });
            }

            vm.printApproval = function (myObject) {
                var currentStatus = myObject.currentStatus;
                if (currentStatus == app.localize('Approved') || currentStatus == app.localize('Completed')
                    || currentStatus == app.localize('Received') || currentStatus == app.localize('PartiallyReceived')) {


                    $state.go('tenant.intertransferapprovalprint', {
                        printid: myObject.id
                    });
                }
                else {
                    abp.message.error(app.localize('CouldNotPrintApproval'), app.localize(currentStatus));
                    return;
                }
            };

            vm.getAll();

            vm.checkCloseDay = function () {
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                    //    if (vm.messagenotificationflag)
                    //        abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                }).finally(function () {
                });
            }

            vm.checkCloseDay();

            vm.createInterTransfer = function (argOption) {
                openDirectApprovalModal(null, argOption);
            };

            function openDirectApprovalModal(objId, argOption) {
                if (argOption == 1) {
                    $state.go("tenant.intertransferapprovalandrequestdetail", {
                        id: objId
                    });
                }
                else {
                    $state.go("tenant.intertransferrequestdetail", {
                        id: objId,
                        directTransfer: true
                    });
                }
            }


            //  Auto PO Details
            vm.autoPoDetails = [];
            vm.fillAutoPoDetails = function () {

                vm.requestMaterialsToBeOrdered = [];
                angular.forEach(vm.materialPoList, function (value, key) {
                    if (value.poOrderQuantity > 0)
                        vm.requestMaterialsToBeOrdered.push(value);
                    value.omitQueueInOrder
                });

                if (vm.requestMaterialsToBeOrdered.length == 0) {
                    //abp.notify.warn('No More Materials To Be Ordered');
                    abp.notify.warn(app.localize('NoMoreRecordsFound'));
                    return;
                }

                vm.autoPoloading = true;
                vm.autoPoDetails = [];
                vm.loading = true;
                console.log("requestAutoPo.js GetAutoPoBasedOnTransferRequest() Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));

                purchaseorderExtendedService.getAutoPoBasedOnTransferRequest({
                    locationRefId: vm.locationRefId,
                    shippingLocationId: vm.shippingLocationId,
                    deliveryDateExpected: vm.deliveryDateExpected,
                    requestMaterialsToBeOrdered: vm.requestMaterialsToBeOrdered
                }).success(function (result) {
                    vm.autoPoDetails = result;
                    angular.forEach(vm.autoPoDetails, function (value, key) {
                        angular.forEach(value.autoPoDto.purchaseOrderDetail, function (poDetailvalue, poKey) {
                            if (poDetailvalue.unitRefId == 0 || poDetailvalue.unitRefId == null) {
                                abp.notify.error('Unit Reference Not Exists' + poDetailvalue.materialRefName);
                            }
                        });
                    });
                    if (vm.autoPoDetails.length == 0) {
                        vm.autoPoFlag = false;
                        vm.poshowflag = true;
                        abp.notify.warn(app.localize('AutoPoMaterialNotExist'));
                        abp.message.warn(app.localize('AutoPoMaterialNotExist'));
                    }
                    else {
                        angular.forEach(vm.autoPoDetails, function (value, key) {
                            value.visibleFlag = true;
                        });
                    }
                }).finally(function () {
                    vm.autoPoloading = false;
                    vm.loading = false;
                    console.log("requestAutoPo.js GetAutoPoBasedOnTransferRequest() End Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));

                });
            }

            vm.redirectToPurchaseOrderForm = function () {
                var autoTransferPoDetails = JSON.stringify(vm.autoPoDetails);
                $state.go('tenant.purchaseorderdetail', {
                    id: null,
                    autoPoFlag: true,
                    autoPoStatus : 'TRQ',
                    autoTransferPoDetails: autoTransferPoDetails
                });
            }

            vm.deselectAllRequest = function () {
                angular.forEach(vm.requestList, function (value, key) {
                    value.doesThisRequestToBeConvertedAsPO = false;
                });
                vm.materialPoList = [];
            }

            vm.selectAllRequest = function () {
                angular.forEach(vm.requestList, function (value, key) {
                    value.doesThisRequestToBeConvertedAsPO = true;
                });
                vm.smartLocationSelection();
                vm.getMaterialPoList();
            }

            vm.showPoList = function () {

            }

            vm.viewInterTransferDetail = function (myObject) {
                vm.loading = true;
                openView(myObject.id);
            };

            function openView(objId) {
                vm.loading = true;
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/intertransfer/request/requestViewInGrid.cshtml',
                    controller: 'tenant.views.house.transaction.intertransfer.requestViewInGrid as vm',
                    backdrop: 'static',
                    resolve: {
                        transferId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    // vm.getAll();
                }).finally(function (result) {
                    //  vm.getAll();
                });
                vm.loading = false;
            }


        }]);
})();

