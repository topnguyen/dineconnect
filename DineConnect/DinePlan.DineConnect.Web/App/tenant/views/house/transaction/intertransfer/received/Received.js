﻿(function () {
    appModule.controller('tenant.views.house.transaction.intertransfer.received', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.interTransfer', 'abp.services.app.commonLookup', 'abp.services.app.location', 'uiGridConstants', 'appSession', 'abp.services.app.dayClose', 'abp.services.app.interTransferCrud',
        function ($scope, $state, $stateParams, $modal, intertransferService, commonLookupService, locationService, uiGridConstants, appSession, daycloseService, interTransferCrudService) {
            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            vm.intertransferreceived = null;
            vm.intertransferreceiveddetail = null;


            $scope.existall = true;
            $scope.dt = new Date();
            console.log("Received.js Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));

            vm.sno = 0;
            vm.loadingCount = 0;
            vm.intertransferrequestId = $stateParams.id;
            vm.requestLocationRefName = $stateParams.requestLocationRefName;

            vm.currentUserId = abp.session.userId;

            vm.defaultLocationId = appSession.user.locationRefId;

            vm.transfervalueshown = abp.setting.getBoolean("App.House.InterTransferValueShown");
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");



            //  Date Range Options
            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            //  Date Angular Js 
            $scope.today = function () {
                $scope.dt = moment(new Date()).format("DD-MMM-YYYY");
            };
            $scope.today();

            $scope.clear = function () {
                $scope.dt = null;
            };

            $scope.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            $scope.dateOptions = {
                dateDisabled: disabled,
                formatYear: 'yy',
                maxDate: new Date().setDate(new Date().getDate() + 1),
                minDate: new Date(),
                startingDay: 1
            };

            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                    mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }

            $scope.toggleMin = function () {
                $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
                $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
            };

            $scope.toggleMin();

            $scope.open1 = function () {
                $scope.popup1.opened = true;
            };

            $scope.open2 = function () {
                $scope.popup2.opened = true;
            };

            $scope.setDate = function (year, month, day) {
                $scope.dt = new Date(year, month, day);
            };

            $scope.formats = ['YYYY-MMM-DD', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];
            $scope.altInputFormats = ['M!/d!/yyyy'];

            $scope.popup1 = {
                opened: false
            };

            $scope.popup2 = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            $scope.events = [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];

            function getDayClass(data) {
                var date = data.date,
                    mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            }

            var requestParams = {
                skipCount: 0,
                maxResultCount: 5, // app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.intertransferreceiveddetaildata = [];
            vm.tempdetaildata = [];

            vm.save = function (argOption) {
                if ($scope.existall == false)
                    return;

                vm.saving = true;
                vm.intertransferreceiveddetaildata = [];

                vm.completedAll = true;
                vm.pendingcount = 0;
                vm.rejectcount = 0;
                vm.savestatus = '';

                vm.errorFlag = false;

                angular.forEach(vm.gridOptions.data, function (value, key) {
                    var receivedQty = value.receivedQty;

                    if (value.issueQty == value.receivedQty) {
                        value.adjustmentQty = 0;
                        value.adjustmentMode = '';
                        value.adjustmentFlag = false;
                        value.returnFlag = false;
                        value.returnQty = 0;
                        value.returnMode = '';
                    }
                    else {

                        vm.completedAll = false;
                        if (value.receivedQty != 0) {
                            if (value.adjustmentFlag == false && value.returnFlag == false) {
                                abp.notify.warn('DifferenceSetOffModeNeedsToMentioned', value.materialRefName);
                                vm.errorFlag = true;
                                return;
                            }
                            else {
                                receivedQty = value.issueQty;
                            }


                        }
                    }

                    vm.intertransferreceiveddetaildata.push({
                        'interTransferRefId': 0,
                        'sno': value.sno,
                        'materialRefId': value.materialRefId,
                        'materialRefName': value.materialRefName,
                        'requestQty': value.requestQty,
                        'issueQty': value.issueQty,
                        'receivedQty': receivedQty,
                        'currentStatus': value.currentStatus,
                        'uom': value.uom,
                        'id': value.id,
                        'unitRefId': value.unitRefId,
                        'unitRefName': value.unitRefName,
                        'defaultUnitId': value.defaultUnitId,
                        'defaultUnitName': value.defaultUnitName,
                        'adjustmentFlag': value.adjustmentFlag,
                        'adjustmentQty': value.adjustmentQty,
                        'adjustmentMode': value.adjustmentMode,
                        'returnFlag': value.returnFlag,
                        'returnQty': value.returnQty,
                        'returnMode': value.returnMode,
                        'price': value.price
                    });



                    if (value.currentStatus == app.localize('Rejected'))
                        vm.rejectcount = vm.rejectcount + 1;
                    else if (value.currentStatus == app.localize('Pending'))
                        vm.pendingcount = vm.pendingcount + 1;
                });

                if (vm.errorFlag) {
                    vm.loading = false;
                    vm.saving = false;
                    return;
                }


                if (vm.pendingcount > 0) {
                    abp.message.warn(app.localize('PendingSaveError', vm.pendingcount), app.localize('SaveFailed'));
                    vm.saving = false;
                    return;
                }

                if (vm.completedAll == true)
                    vm.savestatus = 'V';     //  Received Full
                else
                    vm.savestatus = 'I';     //  Partially Recived

                if (moment().format('YYYY-MMM-DD') == moment(vm.intertransferreceived.receivedTime).format('YYYY-MMM-DD'))
                    vm.intertransferreceived.receivedTime = $scope.convert(moment());



                vm.intertransferreceived.receivedPersonId = vm.currentUserId;
                console.log("Received.js createOrUpdateInterReceived() Call Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));

                interTransferCrudService.createOrUpdateInterReceived({
                    interTransfer: vm.intertransferreceived,
                    interTransferReceivedDetail: vm.intertransferreceiveddetaildata,
                    saveStatus: vm.savestatus
                }).success(function (result) {
                    vm.transactionId = result.id;

                    var msg = app.localize('ReceivedEffected', vm.transactionId);
                    if (result.adjustmentId > 0) {
                        var adjMsg = app.localize('AdjustmentForDifferenceAdded', result.adjustmentId);
                        msg = msg + ' ' + adjMsg;
                        abp.notify.warn(adjMsg);
                    }
                    if (result.returnTransferId > 0) {
                        var retMsg = app.localize('ReturnBackTransferId', result.returnTransferId);
                        msg = msg + ' ' + retMsg;
                        abp.notify.warn(retMsg);
                    }
                    abp.message.success(msg);
                    vm.saving = false;
                    vm.printReceivedInterTransfer(result.id);
                    //@@Pending Adjustment Also Should Be Printed In Another Page / Model Page
                    return;

                }).finally(function () {
                    vm.saving = false;
                    console.log("Received.js createOrUpdateInterReceived() Call End Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                });
            };




            $scope.func = function (data, val) {
                data.materialRefName = val;
            };


            vm.existall = function () {

                if (vm.intertransferreceived.Id == null) {
                    vm.existall = false;
                    return;
                }
            };

            vm.cancel = function () {
                $state.go("tenant.intertransferreceived");
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.requestParams = {
                locations: '',
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.reflocation = [];
            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;

                });
            }

            vm.tempdetaildata = [];


            function init() {
                fillDropDownLocation();
                vm.loadingCount++;
                console.log("Received.js  getInterReceivedForEdit() Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                intertransferService.getInterReceivedForEdit({
                    Id: vm.intertransferrequestId
                }).success(function (result) {

                    vm.intertransferreceived = result.interTransfer;

                    vm.checkCloseDay();

                    vm.intertransferreceived.locationRefId = vm.defaultLocationId;

                    if (vm.intertransferreceived.receivedDate == "0001-01-01T00:00:00") {
                        $scope.dt = new Date();
                    }
                    else {
                        $scope.dt = new Date(vm.intertransferreceived.receivedDate);
                    }

                    if (result.interTransfer.id != null) {

                        var currentStatus = vm.intertransferreceived.currentStatus;
                        if (currentStatus != 'A') {
                            abp.message.warn(app.localize('EditNotAllowedApproved'));
                            vm.cancel();
                            return;
                        }
                        vm.tempdetaildata = [];
                        vm.intertransferreceiveddetail = result.interTransferReceivedDetail;
                        vm.sno = 0;

                        angular.forEach(result.interTransferReceivedDetail, function (value, key) {
                            vm.tempdetaildata.push({
                                'interTransferRefId': value.interTransferRefId,
                                'sno': value.sno,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'requestQty': value.requestQty,
                                'issueQty': value.issueQty,
                                'receivedQty': value.receivedQty,
                                'currentStatus': value.currentStatus,
                                'uom': value.uom,
                                'id': value.id,
                                'price': value.price,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'defaultUnitId': value.defaultUnitId,
                                'defaultUnitName': value.defaultUnitName,
                                'adjustmentFlag': false,
                                'adjustmentQty': 0,
                                'adjustmentMode': '',
                                'returnFlag': false,
                                'returnQty': 0,
                                'returnMode': '',
                                'alertcss': ''
                            });
                        });

                        nonFilteredData = vm.tempdetaildata;
                        vm.applyFilters();
                    }
                }).finally(function (result) {
                    console.log("Received.js  getInterReceivedForEdit() End Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));

                    vm.loadingCount--;
                });
            }


            vm.printReceivedInterTransfer = function (objId) {
                $state.go('tenant.intertransferreceivedprint', {
                    printid: objId
                });
            };


            //  Include Edition in Grid Details

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            var nonFilteredData = [];
            vm.filterText = $stateParams.filterText || '';

            vm.getLocationValue = function (item) {
                return parseInt(item.value);
            };

            vm.loading = false;

            vm.gridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize('Sno'),
                        field: 'sno',
                        minWidth: 40
                    },
                    {
                        name: app.localize('MaterialName'),
                        field: 'materialRefName',
                        minWidth: 200
                    },
                    {
                        name: app.localize('requestQty'),
                        field: 'requestQty',
                        minWidth: 90,
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('IssueQty'),
                        field: 'issueQty',
                        minWidth: 90,
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('receivedQty'),
                        field: 'receivedQty',
                        minWidth: 90,
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('UOM'),
                        field: 'unitRefName',
                        minWidth: 60
                    },
                    {
                        name: app.localize('Status'),
                        field: 'currentStatus',
                        minWidth: 100
                    },
                    {
                        name: app.localize('Recived'),
                        width: 90,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.receivedCurrent(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Approve') + '"><i class="fa fa-check-circle"></i></button>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Edit'),
                        width: 90,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.editEntry(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>' +
                            '</div>'
                    },
                ],
                data: []
            };



            vm.receivedAll = function () {
                angular.forEach(vm.tempdetaildata, function (value, key) {
                    vm.receivedCurrent(value);
                });
            }

            vm.receivedCurrent = function (item) {
                if (item.receivedQty == item.issueQty) {
                    return;
                }

                if (item.currentStatus == app.localize("Rejected")) {
                    abp.notify.warn(app.localize("ReceivedNonApprovedErr"));
                    return;
                }

                if (item.currentStatus == app.localize("Approved")) {
                    item.receivedQty = item.issueQty;
                    item.currentStatus = app.localize("Received");
                }
                else {
                    abp.notify.info(app.localize('IssueQtyDifferenceWithRequestQtyInfo'));
                }

            }

            vm.editEntry = function (item) {
                $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/intertransfer/received/editReceived.cshtml',
                    controller: 'tenant.views.house.transaction.received.editReceived as vm',
                    resolve: {
                        locationId: function () {
                            return vm.intertransferreceived.requestLocationRefId;
                        },
                        allItems: function () {
                            return vm.gridOptions.data;
                        },
                        initialText: function () {
                            return item;
                        }
                    }
                });
            };

            function startPoint() {
                if (vm.transfervalueshown == true) {
                    vm.columnprice =
                    {
                        name: app.localize('Cost'),
                        field: 'price',
                        cellClass: 'ui-ralign',
                        width: 60
                    };
                    vm.gridOptions.columnDefs.splice(2, 0, vm.columnprice);
                }

                init();
            }

            startPoint();

            vm.checkCloseDay = function () {
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {

                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                    vm.intertransferreceived.receivedTime = moment(result.accountDate).format($scope.format);

                }).finally(function (result) {

                });
            }


            $scope.convert = function (thedate) {
                var tempstr = thedate.toString();
                var newstr = tempstr.toString().replace(/GMT.*/g, "")
                newstr = newstr + " UTC";
                return new Date(newstr);
            };

            vm.applyFilters = function () {
                if (!vm.filterText) {
                    vm.gridOptions.data = nonFilteredData;
                }

                var filterText = vm.filterText.trim().toLocaleLowerCase();
                vm.gridOptions.data = _.filter(nonFilteredData, function (text) {
                    if (vm.targetValueFilter == 'EMPTY' && text.targetValue) {
                        return false;
                    }

                    return (text.materialRefName && text.materialRefName.toLocaleLowerCase().indexOf(filterText) >= 0);
                });
            };



        }
    ]);
})();

