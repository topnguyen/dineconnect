﻿
(function () {
    appModule.controller('tenant.views.house.transaction.directapproval.approvalandreceived', [
        '$scope', '$state', '$stateParams', 'abp.services.app.interTransfer', 'abp.services.app.location', 'appSession', 'abp.services.app.material', 'abp.services.app.template', '$uibModal', 'abp.services.app.houseReport', '$rootScope', 'abp.services.app.dayClose', 'abp.services.app.interTransferRequest', 'abp.services.app.interTransferCrud',
        function ($scope, $state, $stateParams, intertransferService, locationService, appSession, materialService, templateService, $modal, housereportService, $rootScope, daycloseService, interTransferRequestService, interTransferCrudService) {
            var vm = this;
            /* eslint-disable */

            vm.saving = false;
            vm.lockscreen = false;

            vm.permissions = {
                directapproveandreceive: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransferApproval.DirectApproveAndReceive'),
                barcodeUsage: abp.auth.hasPermission('Pages.Tenant.House.Master.Material.Barcode'),
                transportAllowed: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransferApproval.TransportAllowed'),
            };

            console.log('Barcode : ' + vm.permissions.barcodeUsage);
            vm.loading = false;

            vm.transfervalueshown = abp.setting.getBoolean("App.House.InterTransferValueShown");
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");


            vm.uilimit = null;
            vm.intertransfer = null;
            vm.intertransferdetail = null;
            $scope.existall = true;
            vm.sno = 0;
            vm.intertransferId = $stateParams.id;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.currentrow = null;
            vm.totaldata = [];
            vm.locationwisedata = [];
            vm.tempdetaildata = [];
            vm.saveandcloseflag = false;
            vm.errorFlag = false;

            vm.requestedLocations = appSession.location.requestedLocations;

            vm.defaulttemplatetype = 4;
            vm.template = null;
            vm.selectedTemplate = null;

            vm.templatesave = null;


            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            $('input[name="reqDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize, // app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.intertransferdetaildata = [];

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            $scope.matfunc = function (currentrow, val) {

                if (vm.currentrow.barcode != null && vm.currentrow.barcode != '')
                    vm.currentrow.materialRefId = val.id;

                if (vm.currentrow.materialRefId == 0 || vm.currentrow.materialRefId == null) {
                    abp.notify.info(app.localize('SelectMaterialErr'));
                    return;
                }

                var alreadyExistflag = false;

                angular.forEach(vm.totaldata, function (value, key) {
                    if (alreadyExistflag == false) {
                        if (angular.equals(val.materialName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            alreadyExistflag = true;
                        }
                    }
                });

                if (alreadyExistflag == true) {
                    vm.tempdetaildata = [];
                    vm.sno = 0;
                    angular.forEach(vm.totaldata, function (value, key) {
                        if (angular.equals(val.materialName, value.materialRefName)) {
                            vm.tempdetaildata.push(value)
                            vm.sno = vm.sno + 1;
                        }
                    });
                }

                currentrow.materialRefName = val.materialName;
                currentrow.materialTypeName = val.materialTypeName;
                currentrow.uom = val.defaultUnitName;
                currentrow.onHand = val.onHand;
                currentrow.price = val.price;
                currentrow.pricewithuom = val.price + "/" + val.defaultUnitName;
                currentrow.unitRefId = val.unitRefId;
                currentrow.barcode = val.barcode;

                if (alreadyExistflag == false) {
                    vm.tempdetaildata = [];
                    vm.sno = 0;
                    vm.addPortion();
                }

            };


            vm.verifyuservalidation = function () {
                $scope.errmessage = "";

                if (vm.requestDate == "" || vm.requestDate == null)
                    $scope.errmessage = $scope.errmessage + app.localize('DateErr');

                if ($scope.errmessage != '') {
                    abp.notify.warn($scope.errmessage);
                    $scope.errmessage = "";
                    return false;
                }
                else {
                    return true;
                }
            }

            vm.addPortion = function () {
                if (vm.sno > 0) {

                    if (vm.verifyuservalidation() == false)
                        return;

                    var lastelement = vm.tempdetaildata[vm.tempdetaildata.length - 1];

                    if (lastelement.materialRefName == null || lastelement.materialRefName == "") {
                        abp.notify.info(app.localize('MatRequired'));
                        $("#selMaterial").focus();
                        return;
                    }

                    if (lastelement.locationRefId == null || lastelement.locationRefName == "") {
                        abp.notify.info(app.localize('LocationRequired'));
                        $("#selLocation").focus();
                        return;
                    }

                    if (lastelement.requestQty == 0 || lastelement.requestQty == null || lastelement.requestQty == "") {
                        abp.notify.info(app.localize('QtyRequired'));
                        $("#rcdqty").focus();
                        return;
                    }
                    if (lastelement.locationRefId == vm.requestLocationRefId) {
                        abp.notify.info(app.localize('SameLocationErr'));
                        $("#selLocation").focus();
                        return;
                    }

                    vm.removeMaterial(lastelement);
                }

                if (vm.tempdetaildata != null) {
                    angular.forEach(vm.tempdetaildata, function (valueinArray, keyinArray) {
                        vm.tempdetaildata[keyinArray].currenteditstatus = false;
                    });
                }

                vm.sno = vm.sno + 1;
                vm.tempdetaildata.push(
                    {
                        'interTransferRefId': 0,
                        'sno': vm.sno,
                        'requestLocationRefId': vm.defaultLocationId,
                        'locationRefId': 0,
                        'locationRefName': '',
                        'materialRefId': '',
                        'materialRefName': '',
                        'requestQty': '',
                        'issueQty': 0,
                        'price': 0,
                        'currentStatus': 'A',
                        'requestRemarks': '',
                        'approvedRemarks': '',
                        'id': 0,
                        'materialTypeName': '',
                        'uom': '',
                        'onHand': 0,
                        'usagePatternList': null,
                        'currenteditstatus': true,
                        'unitRefId': 0
                    });

                if (vm.currentrow != null) {
                    var runningOnHand = vm.currentrow.onHand;
                    var lastindex = 0;
                    angular.forEach(vm.tempdetaildata, function (valueinArray, keyinArray) {
                        vm.tempdetaildata[keyinArray].onHand = runningOnHand;
                        vm.tempdetaildata[keyinArray].currenteditstatus = false;
                        runningOnHand = runningOnHand - parseFloat(vm.tempdetaildata[keyinArray].requestQty);
                        runningOnHand = parseFloat(runningOnHand.toFixed(3));
                        lastindex = keyinArray;
                    });
                    vm.tempdetaildata[lastindex].currenteditstatus = true;
                    vm.tempdetaildata[lastindex].price = vm.currentrow.price;
                    vm.tempdetaildata[lastindex].unitRefId = vm.currentrow.unitRefId;


                    if (runningOnHand < 0) {
                        abp.notify.warn(app.localize('NegativeStockErr'));
                    }
                }

                $("#selMaterial1").focus();
            }


            vm.portionLength = function () {
                return true;
            }

            vm.editRow = function (data, productIndex) {
                data.currenteditstatus = !data.currenteditstatus;

                //var runningOnHand = vm.currentrow.onHand;
                //angular.forEach(vm.tempdetaildata, function (valueinArray, keyinArray) {
                //    vm.tempdetaildata[keyinArray].onHand = runningOnHand;
                //    runningOnHand = runningOnHand - parseFloat(vm.tempdetaildata[keyinArray].requestQty);
                //});
                //vm.tempdetaildata[productIndex].currenteditstatus = true;
            }

            vm.removeRow = function (productIndex) {
                var element = vm.tempdetaildata[productIndex];
                if (vm.tempdetaildata.length > 1) {
                    vm.tempdetaildata.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                    //@@Pending Change Current Stock 
                    var runningOnHand = vm.currentrow.onHand;
                    var lastindex = 0;

                    angular.forEach(vm.tempdetaildata, function (valueinArray, keyinArray) {
                        vm.tempdetaildata[keyinArray].onHand = runningOnHand;
                        runningOnHand = runningOnHand - parseFloat(vm.tempdetaildata[keyinArray].requestQty);
                        lastindex = keyinArray;
                    });
                    vm.tempdetaildata[lastindex].currenteditstatus = true;
                }

                else {
                    vm.tempdetaildata.splice(productIndex, 1);
                    vm.sno = 0;
                    vm.addPortion();
                }

                vm.materialRefresh();
            }

            vm.materialRefresh = function (objRemove) {
                var indexPosition = -1;

                vm.tempRefMaterial = vm.refmaterial;

                angular.forEach(vm.tempdetaildata, function (valueinArray, keyinArray) {
                    indexPosition = -1;
                    var materialtoremove = valueinArray.materialRefName;
                    angular.forEach(vm.tempRefMaterial, function (value, key) {
                        if (value.displayText == valueinArray.materialRefName)
                            indexPosition = key;
                    });
                    if (indexPosition >= 0)
                        vm.tempRefMaterial.splice(indexPosition, 1);
                });

            }


            vm.removeMaterial = function (objRemove) {
                var indexPosition = -1;
                angular.forEach(vm.tempRefMaterial, function (value, key) {
                    if (value.displayText == objRemove.materialRefName)
                        indexPosition = key;
                });

                if (indexPosition >= 0)
                    vm.tempRefMaterial.splice(indexPosition, 1);

            }

            $scope.func = function (data, val) {
                var forLoopFlag = true;
                var alreadyissued = 0;

                angular.forEach(vm.tempdetaildata, function (value, key) {
                    if (forLoopFlag) {
                        if (vm.isUndefinedOrNull(value.requestQty) == false && value.requestQty != "")
                            alreadyissued = alreadyissued + parseFloat(value.requestQty);
                        if (angular.equals(val.displayText, value.locationRefName) && angular.equals(vm.currentrow.materialRefId, value.materialRefId)) {
                            abp.notify.info(app.localize('DuplicateLocationExists'));
                            forLoopFlag = false;
                        }

                    }
                });

                if (!forLoopFlag) {
                    data.materialRefId = 0;
                    data.materialRefName = '';
                    data.uom = '';
                    data.onHand = 0;
                    data.locationRefId = 0;
                    data.locationRefName = '';
                    return;
                }
                data.materialRefId = vm.currentrow.materialRefId;
                data.materialRefName = vm.currentrow.materialRefName;
                data.materialTypeName = vm.currentrow.materialTypeName;
                data.uom = vm.currentrow.uom;

                data.locationRefId = val.value;
                data.locationRefName = val.displayText;
                data.requestLocationRefId = vm.requestLocationRefId;


                data.onHand = parseFloat((vm.currentrow.onHand - alreadyissued).toFixed(3));

                vm.tempusagepattern = [];

                angular.forEach(val.usagePatternList, function (value, key) {
                    vm.tempusagepattern.push({
                        'days': value.days,
                        'description': value.description,
                        'consumption': value.consumption,
                    });
                });

                data.usagePatternList = vm.tempusagepattern;
            };

            vm.cancel = function (argOption) {
                if (argOption == 1) {
                    abp.message.confirm(
                        app.localize('CancelEntry?'),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                $state.go('tenant.intertransferapproval');
                                $rootScope.settings.layout.pageSidebarClosed = false;
                            }
                        }
                    );
                }
                else {
                    $rootScope.settings.layout.pageSidebarClosed = false;
                    $state.go("tenant.intertransferapproval");  //$modalInstance.dismiss();
                }
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };


            vm.refbarcodelist = [];
            function fillDropDownBarCodeList() {
                vm.loading = true;
                vm.refbarcodelist = [];
                materialService.getBarcodesForMaterial().success(function (result) {
                    vm.refbarcodelist = result.barcodeList;
                    vm.loading = false;
                });
            }



            vm.refmaterial = [];
            vm.tempRefMaterial = [];
            vm.materialPrice = [];

            vm.pricesetflag = false;

            function fillDropDownMaterial() {

                materialService.getViewWithOnHandAndUsagePattern({
                    locationRefId: vm.defaultLocationId
                }).success(function (result) {
                    vm.refmaterial = result.items;
                    vm.tempRefMaterial = result.items;
                }).finally(function () {
                    vm.setPrice();
                    vm.loading = false;
                });

                housereportService.getMaterialRateView({
                    locationRefId: vm.defaultLocationId,
                    startDate: moment().format($scope.format),
                    endDate: moment().format($scope.format),
                    locations: appSession.location
                }).success(function (result) {
                    vm.materialPrice = result.materialRateView;
                }).finally(function () {
                    vm.setPrice();
                });

                fillDropDownBarCodeList();
            }



            vm.setPrice = function () {
                if (vm.pricesetflag == false && vm.refmaterial.length > 0 && vm.materialPrice.length > 0) {
                    vm.pricesetflag = true;
                    angular.forEach(vm.materialPrice, function (value, key) {
                        vm.refmaterial.some(function (matvalue, matkey) {
                            if (matvalue.id == value.materialRefId) {
                                vm.refmaterial[matkey].price = value.avgRate;
                                return true;
                            }
                        });
                    });
                    vm.loading = false;
                }
            }

            vm.requestParams = {
                locations: '',
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.reflocation = [];
            vm.filteredreflocations = [];

            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;

                    vm.requestLocationRefId = vm.defaultLocationId;

                    if (vm.requestedLocations.length > 0) {
                        locationService.getLocationsFromJson({
                            objectString: vm.requestedLocations
                        }).success(function (result) {
                            angular.forEach(vm.reflocation, function (value, key) {
                                angular.forEach(result, function (reqvalue, reqkey) {
                                    if (value.value == reqvalue.id)
                                        vm.filteredreflocations.push(value);
                                });
                            });
                        });
                    }
                    else {
                        angular.copy(vm.reflocation, vm.filteredreflocations);
                        if (vm.defaultLocationId != null && vm.defaultLocationId > 0) {
                            var indexPosition = -1;
                            angular.forEach(vm.filteredreflocations, function (value, key) {
                                if (value.value == vm.defaultLocationId)
                                    indexPosition = key;
                            });
                            if (indexPosition >= 0)
                                vm.filteredreflocations.splice(indexPosition, 1);
                        }
                    }


                }).finally(function () {
                    //vm.loading = false;
                });
            }

            vm.nextMaterial = function () {
                vm.uilimit = 20;
                var runningOnHand = vm.currentrow.onHand;
                angular.forEach(vm.tempdetaildata, function (valueinArray, keyinArray) {
                    vm.tempdetaildata[keyinArray].onHand = runningOnHand;
                    vm.tempdetaildata[keyinArray].currenteditstatus = false;
                    runningOnHand = runningOnHand - parseFloat(vm.tempdetaildata[keyinArray].requestQty);
                });

                if (runningOnHand < 0) {
                    abp.message.warn(app.localize('NegativeStockErr', Math.abs(runningOnHand)));
                    return;
                }

                var alreadyExistflag = false;
                var keystoremove = [];
                var temptotaldata = [];
                angular.copy(vm.totaldata, temptotaldata);
                vm.totaldata = [];

                angular.forEach(temptotaldata, function (existvalue, existkey) {
                    if (vm.currentrow.materialRefId == existvalue.materialRefId) {
                        keystoremove.push(existkey);
                        alreadyExistflag = true;
                    }
                    else {
                        vm.totaldata.push(existvalue);
                    }
                });

                angular.forEach(vm.tempdetaildata, function (value, key) {
                    if (value.locationRefId != null && value.locationRefName != '' && value.requestQty > 0) {
                        vm.totaldata.push(value);
                    }
                });


                vm.fillLocationwiserequest();
                vm.currentrow = [];
                vm.tempdetaildata = [];
                vm.sno = 0;
                vm.addPortion();

            }

            vm.importFromExcel = function () {
                fillDropDownMaterial();
                importModal(null);
            };

            function importModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/intertransfer/directapproval/importDirectTransfer.cshtml',
                    controller: 'tenant.views.house.transaction.intertransfer.directapproval.importDirectTransfer as vm',
                    backdrop: 'static',
                    resolve: {
                        requestLocationRefId: function () {
                            return vm.defaultLocationId;
                        },
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.loading = false;
                    var previousMaterialId = 0;
                    var previousMaterialName = "";
                    vm.tempdetaildata = [];

                    var totalErrorMessage = "";

                    var runningOnHand = 0;

                    angular.forEach(result, function (value, key) {
                        if (previousMaterialId != value.materialRefId || key == result.length - 1) {
                            if (key == result.length - 1) {
                                vm.tempdetaildata.push(value);
                                previousMaterialId = value.materialRefId;
                                previousMaterialName = value.materialRefName;
                                vm.refmaterial.some(function (matvalue, matkey) {
                                    if (matvalue.id == value.materialRefId) {
                                        runningOnHand = matvalue.onHand;
                                        return true;
                                    }
                                });
                            }

                            if (vm.tempdetaildata.length > 0) {
                                angular.forEach(vm.tempdetaildata, function (valueinArray, keyinArray) {
                                    vm.tempdetaildata[keyinArray].onHand = runningOnHand;
                                    vm.tempdetaildata[keyinArray].currenteditstatus = false;
                                    runningOnHand = runningOnHand - parseFloat(vm.tempdetaildata[keyinArray].requestQty);
                                });

                                if (runningOnHand < 0) {
                                    errorMessage = app.localize('NegativeStockErrWithName', Math.abs(runningOnHand), previousMaterialName);
                                    abp.notify.warn(errorMessage);
                                    totalErrorMessage = totalErrorMessage
                                        //+ "<br/>"
                                        + errorMessage;
                                    vm.errorFlag = true;
                                }

                                angular.forEach(vm.tempdetaildata, function (tempdetailvalue, tempdetailkey) {
                                    if (tempdetailvalue.locationRefId != null && tempdetailvalue.locationRefName != '' && tempdetailvalue.requestQty > 0) {
                                        vm.totaldata.push(tempdetailvalue);
                                    }
                                });
                            }

                            vm.refmaterial.some(function (matvalue, matkey) {
                                if (matvalue.id == value.materialRefId) {
                                    runningOnHand = matvalue.onHand;
                                    return true;
                                }
                            });
                            vm.tempdetaildata = [];
                        }
                        vm.tempdetaildata.push(value);
                        previousMaterialId = value.materialRefId;
                        previousMaterialName = value.materialRefName;
                    });


                    if (vm.errorFlag == true) {
                        abp.message.warn(totalErrorMessage);
                    }

                    //vm.totaldata = result;
                    vm.fillLocationwiserequest();
                    vm.currentrow = [];
                    vm.tempdetaildata = [];
                    vm.sno = 0;
                    vm.addPortion();

                    vm.loading = false;
                });
            }

            vm.fillLocationwiserequest = function () {
                vm.locationwisedata = [];
                angular.forEach(vm.totaldata, function (totalvalue, totalkey) {
                    locationalreadyexistflag = false;
                    angular.forEach(vm.locationwisedata, function (value, key) {
                        if (totalvalue.locationRefId == value.locationRefId) {
                            locationalreadyexistflag = true;
                            vm.locationwisedata[key].totalMaterial = parseFloat(vm.locationwisedata[key].totalMaterial) + 1;
                            vm.locationwisedata[key].totalQtyordered = parseFloat(vm.locationwisedata[key].totalQtyordered) + parseFloat(totalvalue.requestQty);
                            value.totalQtyordered = parseFloat(value.totalQtyordered.toFixed(3));
                            vm.locationwisedata[key].materialrequestlist.push(totalvalue);
                        }
                    });
                    if (locationalreadyexistflag == false) {
                        var matlist = [];
                        matlist.push(totalvalue);
                        vm.locationwisedata.push({
                            'locationRefId': totalvalue.locationRefId,
                            'locationRefName': totalvalue.locationRefName,
                            'totalMaterial': 1,
                            'totalQtyordered': parseFloat(totalvalue.requestQty),
                            'materialrequestlist': matlist,
                            'intertransferidgenerated': 0
                            //'usagePatternList': value.usagePatternList
                        });
                    }
                });

                console.log(vm.totaldata);
            }

            vm.printApproval = function (objId) {
                $state.go('tenant.intertransferapprovalprint', {
                    printid: objId
                });
            }

            vm.makeRequestAndApproval = function (locationdata, argOption) {
                if (vm.verifyuservalidation() == false)
                    return;

                vm.lockscreen = true;

                var intermasterdata = vm.intertransfer;
                var interdetaildata = [];

                intermasterdata.locationRefId = locationdata.locationRefId;
                intermasterdata.requestLocationRefId = vm.requestLocationRefId;
                intermasterdata.requestDate = vm.requestDate;
                intermasterdata.currentStatus = "A";
                intermasterdata.approvedPersonId = vm.currentUserId;
                intermasterdata.vehicleNumber = vm.intertransfer.vehicleNumber;
                intermasterdata.personInCharge = vm.intertransfer.personInCharge;
                intermasterdata.docReferenceNumber = vm.intertransfer.docReferenceNumber;

                angular.forEach(locationdata.materialrequestlist, function (value, key) {
                    interdetaildata.push({
                        'interTransferRefId': 0,
                        'sno': value.sno,
                        'materialRefId': value.materialRefId,
                        'materialRefName': value.materialRefName,
                        'requestQty': value.requestQty,
                        'issueQty': value.requestQty,
                        'unitRefId': value.unitRefId,
                        'price': value.price,
                        'currentStatus': 'A', // app.localize('Approved'),
                        'requestRemarks': value.requestRemarks,
                        'approvedRemarks': value.approvedRemarks,
                        'id': value.id,
                        'materialTypeName': value.materialTypeName,
                        'uom': value.uom,
                        'onHand': value.onHand,
                    });
                });

                var saveStatuscurrentRequest = "";
                if (argOption == 1)
                    saveStatuscurrentRequest = "DA";
                else
                    saveStatuscurrentRequest = "DR";

                interTransferCrudService.createOrUpdateInterTransfer({
                    interTransfer: intermasterdata,
                    interTransferDetail: interdetaildata,
                    templateSave: vm.templatesave,
                    saveStatus: saveStatuscurrentRequest
                }).success(function (result) {
                    var newId = result.id;
                    abp.message.info(app.localize('AcceptedRequest', newId));

                    locationdata.intertransferidgenerated = newId;
                    var indexPosition = -1;
                    angular.forEach(vm.locationwisedata, function (value, key) {
                        if (locationdata.locationRefId == value.locationRefId) {
                            indexPosition = key;
                            vm.locationwisedata[key].intertransferidgenerated = newId;
                        }
                    });

                    if (vm.saveandcloseflag == false) {
                        if (indexPosition >= 0)
                            vm.locationwisedata.splice(indexPosition, 1);
                    }
                    newId = null;

                }).finally(function () {
                    vm.lockscreen = false;
                    //if (vm.saveandcloseflag==true)
                    //{
                    var closeflag = true;
                    angular.forEach(vm.locationwisedata, function (value, key) {
                        if (value.intertransferidgenerated == 0) {
                            closeflag = false;
                        }
                    });

                    if (closeflag == true)
                        vm.cancel(0);
                    else {
                        var temptotaldata = [];
                        angular.copy(vm.totaldata, temptotaldata);
                        vm.totaldata = [];

                        angular.forEach(temptotaldata, function (existvalue, existkey) {
                            if (locationdata.locationRefId == existvalue.locationRefId) {
                                alreadyExistflag = true;
                            }
                            else {
                                vm.totaldata.push(existvalue);
                            }
                        });
                    }
                    //}
                });
            }

            vm.checkQty = function (enteredQty, data) {
                if (parseFloat(enteredQty) == 0 || enteredQty == null || enteredQty == "") {
                    return;
                }

                if (parseFloat(enteredQty) <= parseFloat(data.onHand))
                    return;
                else {
                    abp.message.warn(app.localize("RequestTransferQtyErr", enteredQty, data.onHand.toFixed(3), data.materialRefName), "Error");
                    data.requestQty = '';
                    $("#rcdqty").focus();
                }
            }

            vm.clearCurrentMaterial = function () {
                vm.currentrow = [];
                vm.tempdetaildata = [];
                vm.sno = 0;
                vm.addPortion();
            }


            vm.openApprovalView = function (locationRefName, materialrequestlist, requestlocationRefName) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/intertransfer/directapproval/locationapprovalview.cshtml',
                    controller: 'tenant.views.house.transaction.directapproval.locationapprovalview as vm',
                    backdrop: 'static',
                    resolve: {
                        locationRefName: function () {
                            return locationRefName;
                        },
                        materialrequestlist: function () {
                            return materialrequestlist;
                        },
                        requestlocationRefName: function () {
                            return requestlocationRefName;
                        }
                    }
                });

                modalInstance.result.then(function (result) {

                });
            }

            vm.saveandclose = function (argOption) {

                if (vm.verifyuservalidation() == false)
                    return;

                vm.saveandcloseflag = true;

                angular.forEach(vm.locationwisedata, function (value, key) {
                    vm.makeRequestAndApproval(value, argOption);
                });



            }

            vm.refTemplateDetail = [];
            vm.reftemplate = [];
            function fillDropDownTemplate() {
                templateService.getTemplateDetail({
                    locationRefId: vm.defaultLocationId,
                    templateType: vm.defaulttemplatetype
                }).success(function (result) {
                    vm.refTemplateDetail = result.items;

                    angular.forEach(vm.refTemplateDetail, function (value, key) {
                        vm.reftemplate.push({
                            'value': value.id,
                            'displayText': value.name,
                        });
                    });

                });
            }


            //vm.openPatternView = function (materialRefName, usagePatternList,uom) {
            //    var modalInstance = $modal.open({
            //        templateUrl: '~/App/tenant/views/house/transaction/intertransfer/request/usagePatternModal.cshtml',
            //        controller: 'tenant.views.house.transaction.intertransfer.request.usagePatternModal as vm',
            //        backdrop: 'static',
            //        resolve: {
            //            materialRefName: function () {
            //                return materialRefName;
            //            },
            //            usagePatternList: function () {
            //                return usagePatternList;
            //            },
            //            uom: function () {
            //                return uom;
            //            }
            //            }
            //    });

            //    modalInstance.result.then(function (result) {

            //    });
            //}

            vm.openPatternView = function (materialRefId, materialRefName, usagePatternList, uom) {

                vm.loading = true;
                materialService.materialUsagePattern({
                    locationRefId: vm.intertransfer.locationRefId,
                    materialRefId: materialRefId
                }).success(function (result) {

                    vm.loading = false;
                    usagePatternList = result;

                    var modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/house/transaction/intertransfer/request/usagePatternModal.cshtml',
                        controller: 'tenant.views.house.transaction.intertransfer.request.usagePatternModal as vm',
                        backdrop: 'static',
                        resolve: {
                            materialRefName: function () {
                                return materialRefName;
                            },
                            usagePatternList: function () {
                                return usagePatternList;
                            },
                            uom: function () {
                                return uom;
                            }
                        }
                    });

                    modalInstance.result.then(function (result) {

                    });

                });


            }


            vm.fillDataFromTemplate = function () {
                vm.refTemplateDetail.some(function (value, key) {
                    if (value.id == vm.template) {
                        vm.selectedTemplate = value.templateData;
                        return true;
                    }
                });

                vm.uilimit = null;
                intertransferService.getTemplateObjectForEdit({
                    objectString: vm.selectedTemplate
                }).success(function (result) {

                    vm.intertransfer = result.interTransfer;
                    vm.intertransfer.locationRefId = vm.defaultLocationId;

                    vm.intertransfer.id = null;


                    vm.templatesave = result.templatesave;

                    vm.templatesave.templateflag = false;
                    vm.templatesave.templatename = null;
                    vm.templatesave.templatescope = false;


                    vm.tempdetaildata = [];
                    vm.intertransferdetail = result.interTransferDetail;
                    vm.sno = 0;

                    vm.requestDate = moment(result.interTransfer.requestDate).format($scope.format);
                    $scope.minDate = vm.requestDate;

                    $('input[name="reqDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.requestDate,
                        minDate: $scope.minDate,
                        maxDate: moment().format($scope.format)
                    });

                    angular.forEach(result.interTransferDetail, function (value, key) {
                        vm.tempdetaildata.push({
                            'interTransferRefId': value.interTransferRefId,
                            'sno': value.sno,
                            'requestLocationRefId': value.defaultLocationId,
                            'locationRefId': value.locationRefId,
                            'locationRefName': value.locationRefName,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'requestQty': value.requestQty,
                            'issueQty': value.issueQty,
                            'price': value.price,
                            'currentStatus': value.currentStatus,
                            'requestRemarks': value.requestRemarks,
                            'approvedRemarks': value.approvedRemarks,
                            'id': value.id,
                            'materialTypeName': value.materialTypeName,
                            'uom': value.uom,
                            'onHand': value.onHand,
                            'usagePatternList': value.usagePatternList,
                            'currenteditstatus': false
                        });
                    });

                    fillDropDownMaterial();
                });
            }

            function init() {
                vm.loading = true;
                fillDropDownTemplate();

                interTransferRequestService.getInterTransferForEdit({
                    Id: vm.intertransferId
                }).success(function (result) {

                    $rootScope.settings.layout.pageSidebarClosed = true;
                    vm.intertransfer = result.interTransfer;

                    vm.templatesave = result.templatesave;

                    vm.templatesave.templateflag = false;
                    vm.templatesave.templatename = null;
                    vm.templatesave.templatescope = false;

                    fillDropDownLocation();

                    if (result.interTransfer.id != null) {
                        vm.uilimit = null;
                        if (vm.intertransfer.currentStatus != 'P') {
                            abp.message.info('RequestStatusChangedFromPending');
                            vm.cancel(0);
                            return
                        }

                        vm.tempdetaildata = [];
                        vm.intertransferdetail = result.interTransferDetail;
                        vm.sno = 0;

                        vm.requestDate = moment(result.interTransfer.requestDate).format($scope.format);
                        $scope.minDate = vm.requestDate;

                        $('input[name="reqDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: vm.requestDate,
                            minDate: $scope.minDate,
                            maxDate: moment().format($scope.format)
                        });

                        angular.forEach(result.interTransferDetail, function (value, key) {
                            vm.tempdetaildata.push({
                                'interTransferRefId': value.interTransferRefId,
                                'locationRefId': value.locationRefId,
                                'locationRefName': value.locationRefName,
                                'requestLocationRefId': value.requestLocationRefId,
                                'sno': value.sno,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'requestQty': value.requestQty,
                                'issueQty': value.issueQty,
                                'price': value.price,
                                'currentStatus': value.currentStatus,
                                'requestRemarks': value.requestRemarks,
                                'approvedRemarks': value.approvedRemarks,
                                'id': value.id,
                                'materialTypeName': value.materialTypeName,
                                'uom': value.uom,
                                'onHand': value.onHand,
                                'usagePatternList': value.usagePatternList,
                                'currenteditstatus': false
                            });
                        });
                        vm.sno = vm.tempdetaildata.length;
                    }
                    else {
                        vm.requestDate = moment().format($scope.format);
                        vm.checkCloseDay();
                        vm.uilimit = 20;
                        vm.addPortion();

                    }
                    fillDropDownMaterial();

                });
            }

            init();

            vm.checkCloseDay = function () {
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {

                        if (vm.softmessagenotificationflag)
                            abp.notify.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                    //vm.intertransferreceived.receivedTime = moment(result.accountDate).format($scope.format);
                    vm.accountDate = moment(result.accountDate).format($scope.format);

                    $('input[name="reqDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.accountDate,
                        minDate: vm.accountDate,
                        maxDate: vm.accountDate
                    });

                }).finally(function (result) {

                });
            }

            $scope.convert = function (thedate) {
                var tempstr = thedate.toString();
                var newstr = tempstr.toString().replace(/GMT.*/g, "")
                newstr = newstr + " UTC";
                return new Date(newstr);
            };

            vm.printRequest = function (objId) {
                $state.go('tenant.intertransferrequestprint', {
                    printid: objId
                });
            };

            $('#cardnumber').on('keydown', function (e) {
                var keyCode = e.keyCode || e.which;
                if (e.keyCode === 13) {
                    e.preventDefault();
                    $scope.$broadcast('UiSelectDemo1');
                }
            });


            vm.selectMaterialBasedOnBarcode = function () {
                if (vm.currentrow == null)
                    return;

                if (vm.currentrow.barcode == '' || vm.currentrow.barcode == null)
                    return;

                vm.uilimit = null;
                var selectedMaterial = null;
                vm.refmaterial.some(function (refdata, refkey) {
                    if (refdata.barcode == vm.currentrow.barcode) {
                        selectedMaterial = refdata;
                        return true;
                    }
                });

                if (selectedMaterial == null) {
                    vm.refbarcodelist.some(function (bcdata, bckey) {
                        if (bcdata.barcode == vm.currentrow.barcode) {
                            vm.refmaterial.some(function (matdata, matkey) {
                                if (bcdata.materialRefId == matdata.id) {
                                    selectedMaterial = matdata;
                                    selectedMaterial.barcode = bcdata.barcode;
                                    return true;
                                }
                            });
                            return true;
                        }
                    });
                }


                if (selectedMaterial != null) {
                    $scope.matfunc(vm.currentrow, selectedMaterial);
                }
                else {
                    abp.notify.warn(app.localize('Barcode') + ' ' + vm.currentrow.barcode + ' ' + app.localize('NotExist'));
                    vm.currentrow.barcode = '';
                    $("#barcode").focus();
                }
            }
        }
    ]);
})();

