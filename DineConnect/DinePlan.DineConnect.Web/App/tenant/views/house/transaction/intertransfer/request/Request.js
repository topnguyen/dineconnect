﻿(function () {
    appModule.controller('tenant.views.house.transaction.intertransfer.request', [
        '$scope', '$state', '$stateParams', 'abp.services.app.interTransfer', 'abp.services.app.location', 'appSession', 'abp.services.app.material', 'abp.services.app.template', '$uibModal', 'abp.services.app.unitConversion', '$rootScope', 'abp.services.app.dayClose', 'abp.services.app.interTransferRequest', 'abp.services.app.interTransferCrud',
        function ($scope, $state, $stateParams, intertransferService, locationService, appSession, materialService, templateService, $modal, unitconversionService, $rootScope, daycloseService, interTransferRequestService, interTransferCrudService) {
            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            vm.loading = true;
            vm.loadingCount = 0;
            vm.intertransfer = null;
            vm.intertransferdetail = null;
            $scope.existall = true;
            vm.sno = 0;
            vm.intertransferId = $stateParams.id;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;
            vm.directTransfer = false;
            vm.timeElapsedAlert = null;
            $rootScope.settings.layout.pageSidebarClosed = true;
            vm.materialLoading = false;
            console.log("Request.js Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));

            //$scope.dates = {};


            //$scope.dates = "11/17/2021 13:22:00";


            $scope.convert = function (thedate) {
                var tempstr = thedate.toString();
                var searchGmt = tempstr.search("GMT");
                if (searchGmt > 0) {
                    var newstr = tempstr.toString().replace(/GMT.*/g, "")
                    newstr = newstr + " UTC";
                    return new Date(newstr);
                }
                else {
                    return thedate;
                }
            };


            if ($stateParams.directTransfer == true || $stateParams.directTransfer == "true") {
                vm.directTransfer = true;
                vm.defaulttemplatetype = 9;
            }
            else {
                vm.directTransfer = false;
                vm.defaulttemplatetype = 1;
            }

            vm.materialGroupCategoryRefId = null;
            vm.materialGroupRefId = null;
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.defaultRequestLocationRefId = appSession.location.defaultRequestLocationRefId;
            vm.requestedLocations = appSession.location.requestedLocations;

            vm.permissions = {
                requestForOtherLocation: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransferApproval.RequestForOtherLocation'),
                directapproveandreceive: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransferApproval.DirectApproveAndReceive'),
                barcodeUsage: abp.auth.hasPermission('Pages.Tenant.House.Master.Material.Barcode'),
                transportAllowed: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransferApproval.TransportAllowed'),
                hideStock: abp.auth.hasPermission('Pages.Tenant.House.Transaction.DayClose.HideStockInTransfer'),
                approveRequest: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransfer.Approve'),
                editDeliveryDate: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransfer.EditDeliveryDate'),
            };

            if (vm.directTransfer == true) {
                vm.permissions.requestForOtherLocation = true;
            }


            vm.template = null;
            vm.selectedTemplate = null;

            vm.templatesave = null;
            vm.uilimit = null;


            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.maxDate = moment().add(90, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            $('input[name="reqDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });

            //$('input[name="deliveryDateRequested"]').daterangepicker({
            //    locale: {
            //        format: $scope.format
            //    },
            //    singleDatePicker: true,
            //    showDropdowns: true,
            //    startDate: moment(),
            //    minDate: $scope.minDate,
            //    maxDate: $scope.maxDate
            //});

            //  Date
            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            var sd = moment().subtract(7, 'day');
            var td = moment().subtract(1, 'day');

            vm.dateRangeModel = {
                startDate: sd,
                endDate: td
            };
            vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
            vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;


            //  Date

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize, // app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.intertransferdetaildata = [];
            vm.tempdetaildata = [];

            vm.verifyuservalidation = function () {
                $scope.errmessage = "";
                if (vm.intertransfer.locationRefId == 0 || vm.intertransfer.locationRefId == null)
                    $scope.errmessage = $scope.errmessage + app.localize('LocationErr')
                else if (vm.intertransfer.requestLocationRefId == 0 || vm.intertransfer.requestLocationRefId == null)
                    $scope.errmessage = $scope.errmessage + app.localize('RequestLocationErr');
                else if (vm.intertransfer.locationRefId == vm.intertransfer.requestLocationRefId)
                    $scope.errmessage = $scope.errmessage + app.localize('SameLocationErr');

                if (vm.intertransfer.RequestDate == "" || vm.intertransfer.requestDate == null)
                    $scope.errmessage = $scope.errmessage + app.localize('DateErr');

                if ($scope.errmessage != '') {
                    abp.notify.warn($scope.errmessage);
                    $scope.errmessage = "";
                    return false;
                }
                else {
                    return true;
                }
            }

            vm.save = function (argOption) {
                if ($scope.existall == false)
                    return;
                if (vm.verifyuservalidation() == false)
                    return;

                if (vm.templatesave.templateflag == true && vm.templatesave.templatename.length == 0) {
                    abp.message.warn(app.localize("TemplateNameRequired"));
                    return;
                }

                vm.saving = true;
                vm.intertransferdetaildata = [];

                vm.sno = 0;
                angular.forEach(vm.tempdetaildata, function (value, key) {
                    vm.sno++;
                    vm.intertransferdetaildata.push({
                        'interTransferRefId': 0,
                        'sno': vm.sno,
                        'barcode': value.barcode,
                        'materialRefId': value.materialRefId,
                        'materialRefName': value.materialRefName,
                        'requestQty': value.requestQty,
                        'issueQty': value.issueQty,
                        'currentStatus': 'P',
                        'requestRemarks': value.requestRemarks,
                        'approvedRemarks': value.approvedRemarks,
                        'id': value.id,
                        'materialTypeName': value.materialTypeName,
                        'uom': value.uom,
                        'onHand': value.onHand,
                        'usagePatternList': value.usagePatternList,
                        'unitRefId': value.unitRefId,
                        'unitRefName': value.unitRefName,
                        'changeunitflag': false,
                    });
                });

                if (vm.templatesave.templatename != null)
                    vm.templatesave.templatename = vm.templatesave.templatename.toUpperCase();

                vm.templatesave.templateType = vm.defaulttemplatetype;

                if (argOption == 'A') {
                    vm.intertransfer.currentStatus = 'P';
                }
                else {
                    vm.intertransfer.currentStatus = 'Q';
                }


                vm.intertransfer.deliveryDateRequested = $scope.convert(vm.deliveryDateRequested);

                vm.intertransfer.creatorUserId = vm.currentUserId;
                console.log("Request.js createOrUpdateInterTransfer() Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                interTransferCrudService.createOrUpdateInterTransfer({
                    interTransfer: vm.intertransfer,
                    interTransferDetail: vm.intertransferdetaildata,
                    templateSave: vm.templatesave,
                }).success(function (result) {
                    vm.transactionId = result.id;

                    abp.notify.info(app.localize('Request') + app.localize('SavedSuccessfully'));
                    if (vm.intertransferId == null)
                        abp.message.success(app.localize('RequestInitiated', vm.transactionId));
                    else
                        abp.message.success(app.localize('RequestUpdated', vm.transactionId));

                    if (argOption == 'A') {
                        vm.printRequest(vm.transactionId);
                        vm.saving = false;
                        return;
                    }
                    else {
                        vm.cancel();
                    }

                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.removeRow = function (productIndex) {
                var element = vm.tempdetaildata[productIndex];

                if (vm.tempdetaildata.length > 1) {
                    vm.tempdetaildata.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                }

                else {
                    vm.tempdetaildata.splice(productIndex, 1);
                    vm.sno = 0;
                    vm.addPortion();
                }

                vm.materialRefresh();
            }


            vm.addPortion = function (objId) {
                if (vm.sno > 0) {

                    if (vm.verifyuservalidation() == false)
                        return;
                    vm.tempdetaildata[objId].changeunitflag = false;

                    var lastelement = vm.tempdetaildata[objId];

                    if (lastelement.requestQty == 0 || lastelement.requestQty == null || lastelement.requestQty == "") {
                        //abp.notify.info(app.localize('QtyRequired'));
                        $("#rcdqty-" + objId).focus();
                        return;
                    }

                    if (lastelement.materialRefName == null || lastelement.materialRefName == "") {
                        abp.notify.info(app.localize('MatRequired'));
                        $("#uimaterialname-" + objId).focus();

                        return;
                    }
                    vm.tempdetaildata[objId].editFlag = false;
                    //vm.removeMaterial(lastelement);
                }

                vm.sno = vm.sno + 1;
                vm.tempdetaildata.push(
                    {
                        'interTransferRefId': 0,
                        'sno': vm.sno,
                        'barcode': '',
                        'materialRefId': '',
                        'materialRefName': '',
                        'requestQty': '',
                        'issueQty': 0,
                        'currentStatus': 'P',
                        'requestRemarks': '',
                        'approvedRemarks': '',
                        'id': 0,
                        'materialTypeName': '',
                        'uom': '',
                        'onHand': 0,
                        'usagePatternList': null,
                        'unitRefId': 0,
                        'unitRefName': '',
                        'changeunitflag': false,
                        'editFlag': true
                    });

                var maxid = vm.tempdetaildata.length - 1;
                if (vm.permissions.barcodeUsage)
                    $("#barcode-" + maxid).focus();
                else
                    $("#uimaterialname-" + maxid).focus();
            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeMaterial = function (objRemove) {
                var indexPosition = -1;
                angular.forEach(vm.tempRefMaterial, function (value, key) {
                    if (value.displayText == objRemove.materialRefName)
                        indexPosition = key;
                });

                if (indexPosition >= 0)
                    vm.tempRefMaterial.splice(indexPosition, 1);

            }

            vm.materialRefresh = function (objRemove) {
                var indexPosition = -1;

                vm.tempRefMaterial = vm.refmaterial;

                angular.forEach(vm.tempdetaildata, function (valueinArray, keyinArray) {
                    indexPosition = -1;
                    var materialtoremove = valueinArray.materialRefName;
                    angular.forEach(vm.tempRefMaterial, function (value, key) {
                        if (value.displayText == valueinArray.materialRefName)
                            indexPosition = key;
                    });
                    if (indexPosition >= 0)
                        vm.tempRefMaterial.splice(indexPosition, 1);
                });

            }

            $scope.func = function (data, val, index) {
                var forLoopFlag = true;

                angular.forEach(vm.tempdetaildata, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                            if (vm.permissions.barcodeUsage)
                                $("#barcode-" + index).focus();
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.materialRefId = '';
                    data.materialRefName = '';
                    data.uom = '';
                    data.barcode = '';
                    data.onHand = 0;
                    return;
                }

                data.materialRefId = val.id;
                data.materialRefName = val.materialName;
                data.materialTypeName = val.materialTypeName;
                data.uom = val.defaultUnitName;
                data.onHand = val.onHand;
                data.reservedQuantity = val.reservedQuantity;
                data.reservedIssueQuantity = val.reservedIssueQuantity;
                data.reservedYieldQuantity = val.reservedYieldQuantity;
                data.unitRefId = val.transferUnitId;
                data.unitRefName = val.transferUnitRefName;

                data.changeunitflag = false;
                data.barcode = val.barcode;
                if (data.barcode == null || data.barcode.length == 0)
                    data.barcode = data.materialRefName;

                vm.tempusagepattern = [];

                angular.forEach(val.usagePatternList, function (value, key) {
                    vm.tempusagepattern.push({
                        'days': value.days,
                        'description': value.description,
                        'consumption': value.consumption.toFixed(6),
                    });
                });

                data.usagePatternList = vm.tempusagepattern;
            };


            vm.existall = function () {

                if (vm.intertransfer.Id == null) {
                    vm.existall = false;
                    return;
                }


            };

            vm.cancel = function () {
                $rootScope.settings.layout.pageSidebarClosed = false;
                if (vm.directTransfer == true) {
                    $state.go("tenant.intertransferapproval");
                }
                else {
                    $state.go("tenant.intertransferrequest");
                }

                //$modalInstance.dismiss();
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.dsetFocus = function (obj) {
                //console.log(obj);
            }

            vm.refmaterial = [];
            vm.tempRefMaterial = [];
            vm.calculateIssueReservedQuantity = false;
            vm.calculateYieldReservedQuantity = false;

            function fillDropDownMaterial() {
                vm.materialLoading = true;
                console.log("Request.js fillDropDownMaterial() Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                materialService.getSimpleMaterialList({
                    locationRefId: vm.intertransfer.locationRefId,
                    calculateIssueReservedQuantity: vm.calculateIssueReservedQuantity,// CalculateIssueReservedQuantity
                    calculateYieldReservedQuantity: vm.calculateYieldReservedQuantity
                }).success(function (result) {
                    vm.refmaterial = result.items;
                    vm.tempRefMaterial = result.items;
                    vm.materialLoading = false;
                    console.log("Request.js fillDropDownMaterial() End Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                    //var jsonString = JSON.stringify(result);
                    //var bytes = jsonString.length / 1024;
                    //var kb = bytes / 1024;
                });
            }

            vm.refbarcodelist = [];

            function fillDropDownBarCodeList() {
                vm.refbarcodelist = [];
                vm.materialLoading = true;
                materialService.getBarcodesForMaterial().success(function (result) {
                    vm.refbarcodelist = result.barcodeList;
                    vm.materialLoading = false;
                });
            }

            fillDropDownBarCodeList();

            vm.timeElapsedAlert = null;
            vm.setTimeElapsedAlert = function () {
                vm.loading = true;
                vm.loadingCount++;
                console.log("Request.js setTimeElapsedAlert Start: " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                interTransferRequestService.getRequestTimeStatus(vm.intertransfer.locationRefId).success(function (result) {
                    vm.timeElapsedAlert = result.alertMsg;
                    vm.loading = false;
                    vm.loadingCount--;
                    console.log("Request.js setTimeElapsedAlert End: " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                });
            }

            vm.refmaterialgroup = [];
            function fillDropDownMaterialGroup() {
                materialService.getMaterialGroupForCombobox({}).success(function (result) {
                    vm.refmaterialgroup = result.items;
                });
            }
            fillDropDownMaterialGroup();

            vm.refmaterialgroupcategory = [];
            vm.fillDropDownMaterialGroupCategory = function () {
                materialService.getMaterialGroupCategoryForCombobox({
                    id: vm.materialGroupRefId
                }).success(function (result) {
                    vm.refmaterialgroupcategory = result.items;
                    if (result.items.length == 1) {
                        vm.materialGroupCategoryRefId = result.items[0].value;
                    }
                    else {
                        vm.materialGroupCategoryRefId = null;
                    }
                });
            }

            vm.requestParams = {
                locations: '',
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.allowedLocation = [];
            vm.fillUserAllowedLocation = function () {
                var lastUpdate = moment(appSession.locationsBasedOnUserLastUpdateTime);
                var currentTime = moment();
                var secDiff = currentTime.diff(lastUpdate, 'seconds') + 1;

                if (secDiff < appSession.timeOut && appSession.locationsBasedOnUser != null) {
                    vm.allowedLocation = appSession.locationsBasedOnUser.items;
                    appSession.timeOut = 90;
                }
                else {
                    locationService.getLocationBasedOnUser({
                        userId: vm.currentUserId
                    }).success(function (result) {
                        vm.allowedLocation = result.items;
                        appSession.locationsBasedOnUser = result;
                        appSession.locationsBasedOnUserLastUpdateTime = moment();
                        //var jsonString = JSON.stringify(result);
                        //var bytes = jsonString.length / 1024;
                        //var kb = bytes / 1024;
                    }).finally(function () {
                        //fillDropDownTemplate(vm.intertransfer.locationRefId);
                    });
                }
            }

            vm.fillUserAllowedLocation();
            vm.reflocation = [];
            vm.filteredreflocations = [];

            vm.fillDropDownLocation = function () {
                vm.reflocation = [];
                vm.filteredreflocations = [];
                var lastUpdate = moment(appSession.locationListLastUpdateTime);
                var currentTime = moment();
                var secDiff = currentTime.diff(lastUpdate, 'seconds') + 1;

                if (secDiff < appSession.timeOut && appSession.locationList != null) {
                    vm.reflocation = appSession.locationList.items;
                    vm.processLocationData();
                }
                else {
                    locationService.getLocations({}).success(function (result) {
                        vm.reflocation = result.items;
                        appSession.locationList = result;
                        appSession.locationListLastUpdateTime = moment();
                        vm.processLocationData();
                    }).finally(function () {
                    });
                }
            }

            vm.processLocationData = function () {
                fillDropDownMaterial();

                if (vm.directTransfer == false) {
                    vm.reflocation.some(function (value, key) {
                        if (value.id == vm.intertransfer.locationRefId) {
                            vm.requestedLocations = value.requestedLocations;
                            return;
                        }
                    });

                    if (vm.requestedLocations.length > 0) {
                        locationService.getLocationsFromJson({
                            objectString: vm.requestedLocations
                        }).success(function (result) {
                            angular.forEach(vm.reflocation, function (value, key) {
                                angular.forEach(result, function (reqvalue, reqkey) {
                                    if (value.id == reqvalue.id)
                                        vm.filteredreflocations.push(value);
                                });
                            });
                        });
                    }
                    else {
                        angular.copy(vm.reflocation, vm.filteredreflocations);
                        if (vm.intertransfer.locationRefId != null && vm.intertransfer.locationRefId > 0) {
                            var indexPosition = -1;
                            angular.forEach(vm.filteredreflocations, function (value, key) {
                                if (value.id == vm.intertransfer.locationRefId)
                                    indexPosition = key;
                            });
                            if (indexPosition >= 0)
                                vm.filteredreflocations.splice(indexPosition, 1);
                        }
                    }

                    if (vm.intertransfer.id == null) {
                        vm.intertransfer.requestLocationRefId = null;

                        if (vm.tempdetaildata.length > 0) {
                            vm.tempdetaildata = [];
                            vm.sno = 0;
                            vm.addPortion();
                        }

                        if (vm.defaultRequestLocationRefId != null && vm.defaultRequestLocationRefId > 0 && vm.defaultRequestLocationRefId != vm.intertransfer.locationRefId) {
                            vm.intertransfer.requestLocationRefId = vm.defaultRequestLocationRefId;
                        }
                    }

                }
                else {
                    vm.filteredreflocations = [];
                    vm.reflocation.some(function (value, key) {
                        if (value.id == vm.defaultLocationId) {
                            vm.filteredreflocations.push(value);
                            vm.intertransfer.requestLocationRefId = value.id;
                            if (vm.intertransfer.locationRefId == vm.intertransfer.requestLocationRefId)
                                vm.intertransfer.locationRefId = null;
                            return;
                        }
                    });
                }

                fillDropDownTemplate(vm.intertransfer.locationRefId);
            }

            vm.tempdetaildata = [];

            vm.refTemplateDetail = [];
            vm.reftemplate = [];

            function fillDropDownTemplate(argLocationRefId) {
                if (vm.defaulttemplatetype == 9)
                    argLocationRefId = vm.intertransfer.requestLocationRefId;

                templateService.getTemplateInfo({
                    locationRefId: argLocationRefId,
                    templateType: vm.defaulttemplatetype
                }).success(function (result) {
                    vm.refTemplateDetail = result.items;
                    vm.reftemplate = [];
                    angular.forEach(vm.refTemplateDetail, function (value, key) {
                        vm.reftemplate.push({
                            'value': value.id,
                            'displayText': value.name,
                        });
                    });

                });
            }

            vm.openPatternView = function (materialRefId, materialRefName, usagePatternList, uom) {

                vm.loading = true;

                materialService.materialUsagePattern({
                    locationRefId: vm.intertransfer.locationRefId,
                    materialRefId: materialRefId
                }).success(function (result) {

                    vm.loading = false;

                    usagePatternList = result;

                    var modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/house/transaction/intertransfer/request/usagePatternModal.cshtml',
                        controller: 'tenant.views.house.transaction.intertransfer.request.usagePatternModal as vm',
                        backdrop: 'static',
                        resolve: {
                            materialRefName: function () {
                                return materialRefName;
                            },
                            usagePatternList: function () {
                                return usagePatternList;
                            },
                            uom: function () {
                                return uom;
                            }
                        }
                    });

                    modalInstance.result.then(function (result) {

                    });

                });
            }

            vm.fillDataFromTemplate = function () {
                //vm.refTemplateDetail.some(function (value, key) {
                //    if (value.id == vm.template) {
                //        vm.selectedTemplate = value.templateData;
                //        return true;
                //    }
                //});
                vm.uilimit = null;
                intertransferService.getTemplateObjectForEdit({
                    locationRefId: vm.defaultLocationId,
                    templateId: vm.template,
                    templateType: vm.defaulttemplatetype,
                    //objectString : vm.selectedTemplate
                }).success(function (result) {

                    vm.intertransfer = result.interTransfer;
                    vm.intertransfer.locationRefId = vm.intertransfer.locationRefId;

                    vm.intertransfer.id = null;


                    vm.templatesave = result.templatesave;

                    vm.templatesave.templateflag = false;
                    vm.templatesave.templatename = null;
                    vm.templatesave.templatescope = false;


                    vm.tempdetaildata = [];
                    vm.intertransferdetail = result.interTransferDetail;
                    vm.sno = 0;

                    vm.intertransfer.requestDate = moment(result.interTransfer.requestDate).format($scope.format);
                    $scope.minDate = vm.intertransfer.requestDate;

                    $('input[name="reqDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.intertransfer.requestDate,
                        minDate: $scope.minDate,
                        maxDate: moment().format($scope.format)
                    });


                    vm.intertransfer.deliveryDateRequested = moment(result.interTransfer.deliveryDateRequested).format('YYYY-MMM-DD HH:mm');
                    vm.deliveryDateRequested = vm.intertransfer.deliveryDateRequested;

                    angular.forEach(result.interTransferDetail, function (value, key) {
                        vm.tempdetaildata.push({
                            'interTransferRefId': value.interTransferRefId,
                            'sno': value.sno,
                            'barcode': value.barcode,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'requestQty': value.requestQty,
                            'issueQty': value.issueQty,
                            'currentStatus': value.currentStatus,
                            'requestRemarks': value.requestRemarks,
                            'approvedRemarks': value.approvedRemarks,
                            'id': value.id,
                            'materialTypeName': value.materialTypeName,
                            'uom': value.uom,
                            'onHand': value.onHand,
                            'usagePatternList': value.usagePatternList,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'changeunitflag': false,
                        });
                    });

                    fillDropDownMaterial();
                });
            }

            function init() {

                vm.loadingCount++;
                vm.fillDropDownMaterialGroupCategory();
                console.log("Request.js init() Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                interTransferRequestService.getInterTransferForEdit({
                    Id: vm.intertransferId
                }).success(function (result) {

                    vm.intertransfer = result.interTransfer;

                    vm.templatesave = result.templatesave;

                    // vm.timeElapsedAlert = result.interTransfer.timeElapsedAlert;

                    vm.templatesave.templateflag = false;
                    vm.templatesave.templatename = null;
                    vm.templatesave.templatescope = false;

                    vm.intertransfer.locationRefId = vm.defaultLocationId;

                    if (result.interTransfer.id != null) {
                        vm.uilimit = null;
                        if (vm.intertransfer.currentStatus == 'Q') {
                            //  Do Nothing
                            abp.notify.info(app.localize('Please') + ' ' + app.localize('Verify') + ' ' + app.localize('Approve'));
                        }
                        else if (vm.intertransfer.currentStatus != 'P') {
                            abp.message.info('AlreadyApproved');
                            vm.cancel();
                            return
                        }

                        vm.tempdetaildata = [];
                        vm.intertransferdetail = result.interTransferDetail;
                        vm.sno = 0;

                        vm.intertransfer.requestDate = moment(result.interTransfer.requestDate).format($scope.format);
                        $scope.minDate = vm.intertransfer.requestDate;

                        $('input[name="reqDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: vm.intertransfer.requestDate,
                            minDate: $scope.minDate,
                            maxDate: moment().format($scope.format)
                        });


                        vm.intertransfer.deliveryDateRequested = moment(result.interTransfer.deliveryDateRequested).format('YYYY-MMM-DD HH:mm');
                        vm.deliveryDateRequested = vm.intertransfer.deliveryDateRequested;
                        //$('input[name="deliveryDateRequested"]').daterangepicker({
                        //    locale: {
                        //        format: $scope.format
                        //    },
                        //    singleDatePicker: true,
                        //    showDropdowns: true,
                        //    startDate: vm.intertransfer.deliveryDateRequested,
                        //    minDate: moment(),
                        //    maxDate: $scope.maxDate
                        //});

                        angular.forEach(result.interTransferDetail, function (value, key) {
                            vm.tempdetaildata.push({
                                'interTransferRefId': value.interTransferRefId,
                                'sno': value.sno,
                                'barcode': value.barcode,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'requestQty': value.requestQty,
                                'issueQty': value.issueQty,
                                'currentStatus': value.currentStatus,
                                'requestRemarks': value.requestRemarks,
                                'approvedRemarks': value.approvedRemarks,
                                'id': value.id,
                                'materialTypeName': value.materialTypeName,
                                'uom': value.uom,
                                'onHand': value.onHand,
                                'usagePatternList': value.usagePatternList,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'defaultUnitId': value.defaultUnitId,
                                'defaultUnitName': value.defaultUnitName,
                                'changeunitflag': false,
                            });
                        });
                        vm.sno = vm.tempdetaildata.length;
                        $rootScope.settings.layout.pageSidebarClosed = true;
                    }
                    else {
                        vm.intertransfer.requestDate = moment().format($scope.format);
                        //vm.intertransfer.deliveryDateRequested = new Date();
                        vm.deliveryDateRequested = new Date();
                        vm.addPortion();
                        vm.uilimit = 50;
                        vm.checkCloseDay();
                    }

                    vm.fillDropDownLocation();
                    vm.setTimeElapsedAlert();

                    if (vm.permissions.hideStock == true) {
                        abp.notify.error(app.localize('HideStockMessage'));
                    }
                }).finally(function (result) {
                    vm.loading = false;
                    vm.loadingCount--;
                    console.log("Request.js init() End Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                });
            }

            vm.checkCloseDay = function () {
                vm.loading = true;
                vm.loadingCount++;
                //var accountDateSetFlag = false;
                //if (appSession.accountDateDto != null)
                //{
                //    var lastUpdate = moment(appSession.accountDateDto.lastUpdateTime);
                //    var currentTime = moment();
                //    var d = currentTime.diff(lastUpdate, 'seconds') + 1;
                //    if (d <= 30) {
                //        var a = 3;
                //    }
                //    else {
                //        var a = 4;
                //    }
                //}

                console.log("Request.js checkCloseDay() Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.currentUserId != vm.currentUserId) {
                        var errorMessage = app.localize('UserIdChangedErrorAlert', vm.currentUserId, result.currentUserId)
                        abp.notify.error(errorMessage);
                        abp.message.error(errorMessage);
                        vm.currentLanguage = abp.localization.currentLanguage;
                        location.href = abp.appPath + 'Localization/ChangeCulture?cultureName=' + vm.currentLanguage.name + '&returnUrl=' + window.location.href;
                    }
                    appSession.accountDateDto = result;
                    appSession.accountDateDto.lastUpdateTime = moment();

                    if (result.id == 0) {

                        if (vm.softmessagenotificationflag)
                            abp.notify.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        //if (vm.messagenotificationflag)
                        //    abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                    vm.intertransfer.requestDate = moment(result.accountDate).format($scope.format);
                    $scope.minDate = result.accountDate;

                    $('input[name="reqDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.intertransfer.requestDate,
                        minDate: vm.intertransfer.requestDate,
                        maxDate: vm.intertransfer.requestDate
                    });

                    $('input[name="deliveryDateRequested"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: moment(),
                        minDate: moment(),
                        maxDate: $scope.maxDate
                    });

                    $rootScope.settings.layout.pageSidebarClosed = true;

                }).finally(function (result) {
                    vm.loading = false;
                    vm.loadingCount--;
                    console.log("Request.js checkCloseDay() End Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                });

            }

            init();

            vm.setDeliveryDateMinDate = function () {
                //if (vm.intertransfer.requestDate != null) {
                //    //vm.intertransfer.deliveryDateRequested = moment(result.interTransfer.requestDate).format($scope.format);
                //    $scope.deliveryDateRequested = vm.intertransfer.requestDate;

                //    $('input[name="deliveryDateRequested"]').daterangepicker({
                //        locale: {
                //            format: $scope.format
                //        },
                //        singleDatePicker: true,
                //        showDropdowns: true,
                //        startDate: vm.intertransfer.deliveryDateRequested,
                //        minDate: $scope.deliveryDateRequested,
                //        maxDate: $scope.maxDate
                //    });
                //}
            }

            vm.barcodeKeyPress = function (objId, $event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    //alert(objId);
                    $("#rcdqty-" + objId).focus();
                }
            };

            vm.qtyKeyPress = function (objId, $event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    //alert(objId);
                    $("#btnadd-" + objId).focus();
                }
            };

            vm.printApproval = function (objId) {
                $rootScope.settings.layout.pageSidebarClosed = false;
                $state.go('tenant.intertransferapprovalprint', {
                    printid: objId
                });
            };

            vm.printRequest = function (objId) {
                $rootScope.settings.layout.pageSidebarClosed = false;
                $state.go('tenant.intertransferrequestprint', {
                    printid: objId
                });
            };

            //vm.refconversionunit = [];
            //vm.fillUnitConversion = function () {
            //    unitconversionService.getAll({
            //    }).success(function (result) {
            //        vm.refconversionunit = result.items;
            //        //var jsonString = JSON.stringify(result);
            //        //var bytes = jsonString.length / 1024;
            //        //var kb = bytes / 1024;
            //    });
            //}

            //vm.fillUnitConversion();


            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.materialLoading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }
                    vm.materialLoading = false;
                });
            }

            vm.unitReference = function (selected, data) {
                data.unitRefId = selected.value;
                data.unitRefName = selected.displayText;
                data.changeunitflag = false;
            }


            vm.removeUnfilled = function () {
                var listtoremoved = [];
                tempportions = [];

                angular.forEach(vm.tempdetaildata, function (value, key) {
                    if (value.requestQty == 0 || value.requestQty == null || value.requestQty == "") {
                        listtoremoved.push(key)
                    }
                    else {
                        vm.sno++;
                        value.sno = vm.sno;
                        tempportions.push(value);
                    }
                });

                vm.tempdetaildata = tempportions;

                vm.sno = vm.tempdetaildata.length;

                if (vm.tempdetaildata.length == 0)
                    vm.addPortion();


            }

            vm.getDetailMaterial = function () {
                vm.uilimit = null;

                if (vm.materialGroupCategoryRefId == null || vm.materialGroupCategoryRefId == 0) {
                    //abp.message.warn(app.localize('CategorySelectErr'));
                    return;
                }
                if (vm.tempdetaildata != null) {
                    var element = vm.tempdetaildata[vm.tempdetaildata.length - 1];
                    if (element.materialRefId == 0 || element.materialRefId == null) {
                        productIndex = vm.tempdetaildata.length - 1;
                        vm.tempdetaildata.splice(productIndex, 1);
                        vm.sno = vm.sno - 1;
                    }
                }
                vm.loading = true;
                vm.loadingCount++;
                materialService.getIssueForCategory({
                    materialGroupRefId: vm.materialGroupRefId,
                    materialGroupCategoryRefId: vm.materialGroupCategoryRefId,
                    locationRefId: vm.intertransfer.locationRefId
                }).success(function (result) {

                    angular.forEach(result.issueDetail, function (value, key) {
                        var alreadyExist = false;
                        vm.tempdetaildata.some(function (existvalue, existkey) {
                            if (existvalue.materialRefId == value.materialRefId) {
                                alreadyExist = true;
                                return true;
                            }
                        });

                        if (alreadyExist == false) {
                            vm.sno = vm.sno + 1;
                            vm.tempdetaildata.push({
                                'interTransferRefId': 0,
                                'sno': vm.sno,
                                'barcode': value.barcode,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'requestQty': '',
                                'issueQty': 0,
                                'currentStatus': 'P',
                                'requestRemarks': '',
                                'approvedRemarks': '',
                                'id': 0,
                                'materialTypeName': value.materialTypeRefName,
                                'uom': value.defaultUnitName,
                                'onHand': value.currentInHand,
                                'usagePatternList': null,
                                'unitRefId': value.transferUnitId,
                                'unitRefName': value.transferUnitRefName,
                                'changeunitflag': false,
                            })
                        }
                    });
                }).finally(function (result) {
                    vm.loading = false;
                    vm.loadingCount--;
                });
            }

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };


            vm.autoRequest = function (argOption) {
                vm.uilimit = null;
                vm.loading = true;
                vm.loadingCount++;
                if (vm.isUndefinedOrNull(vm.dateRangeModel.startDate)) {
                    vm.dateRangeModel = {
                        startDate: sd,
                        endDate: td
                    };
                    vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
                    vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;
                }

                intertransferService.getAutoRequestProjection({
                    locationRefId: vm.intertransfer.locationRefId,
                    nextNDays: vm.nextndays,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    requestQtyBasedOn: argOption
                }).success(function (result) {

                    if (result.length > 0) {
                        if (vm.tempdetaildata != null) {
                            var element = vm.tempdetaildata[vm.tempdetaildata.length - 1];
                            if (element.materialRefId == 0 || element.materialRefId == null) {
                                productIndex = vm.tempdetaildata.length - 1;
                                vm.tempdetaildata.splice(productIndex, 1);
                                vm.sno = vm.sno - 1;
                            }
                        }
                    }

                    angular.forEach(result, function (value, key) {

                        var alreadyExist = false;
                        vm.tempdetaildata.some(function (existvalue, existkey) {
                            if (existvalue.materialRefId == value.materialRefId) {
                                alreadyExist = true;
                                return true;
                            }
                        });

                        if (alreadyExist == false && value.nextNDaysRoundedProjection > 0) {
                            vm.sno = vm.sno + 1;
                            vm.tempdetaildata.push({
                                'interTransferRefId': 0,
                                'sno': vm.sno,
                                'barcode': value.barcode,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialName,
                                'requestQty': value.nextNDaysRoundedProjection,
                                'issueQty': 0,
                                'currentStatus': 'P',
                                'requestRemarks': '',
                                'approvedRemarks': '',
                                'id': 0,
                                'materialTypeName': value.materialTypeName,
                                'uom': value.uom,
                                'onHand': value.onHand,
                                'usagePatternList': null,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.uom,
                                'changeunitflag': false,
                            })
                        }
                    });
                }).finally(function (result) {
                    vm.loading = false;
                    vm.loadingCount--;
                });

            }



            vm.selectMaterialBasedOnBarcode = function (data, index) {
                if (data.barcode == '' || data.barcode == null)
                    return;

                var forLoopFlag = true;
                angular.forEach(vm.tempdetaildata, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(data.barcode, value.barcode) && !angular.equals(data.sno, value.sno)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists') + ' BarCode : ' + data.barcode);
                            if (vm.permissions.barcodeUsage)
                                $("#barcode-" + index).focus();
                            data.barcode = '';
                            forLoopFlag = false;
                        }
                    }
                });
                if (forLoopFlag == false)
                    return;

                vm.uilimit = null;
                var selectedMaterial = null;
                vm.refmaterial.some(function (refdata, refkey) {
                    if (refdata.barcode == data.barcode) {
                        selectedMaterial = refdata;
                        return true;
                    }
                });

                vm.refbarcodelist.some(function (bcdata, bckey) {
                    if (bcdata.barcode == data.barcode) {
                        vm.refmaterial.some(function (matdata, matkey) {
                            if (bcdata.materialRefId == matdata.id) {
                                selectedMaterial = matdata;
                                selectedMaterial.barcode = data.barcode;
                                return true;
                            }
                        });
                        return true;
                    }
                });


                if (selectedMaterial != null) {
                    $scope.func(data, selectedMaterial, index);
                }
                else {
                    abp.notify.warn(app.localize('Barcode') + ' ' + data.barcode + ' ' + app.localize('NotExist'));
                    data.barcode = '';
                    $("#barcode-" + index).focus();
                }
            }


            vm.makeRequestAndApproval = function (argOption) {
                if ($scope.existall == false)
                    return;
                if (vm.verifyuservalidation() == false)
                    return;

                if (vm.templatesave.templateflag == true && vm.templatesave.templatename.length == 0) {
                    abp.message.warn(app.localize("TemplateNameRequired"));
                    return;
                }

                vm.saving = true;
                vm.intertransferdetaildata = [];

                vm.sno = 0;
                angular.forEach(vm.tempdetaildata, function (value, key) {
                    vm.sno++;
                    vm.intertransferdetaildata.push({
                        'interTransferRefId': 0,
                        'sno': vm.sno,
                        'barcode': value.barcode,
                        'materialRefId': value.materialRefId,
                        'materialRefName': value.materialRefName,
                        'requestQty': value.requestQty,
                        'issueQty': value.requestQty,
                        'currentStatus': 'A',
                        'requestRemarks': value.requestRemarks,
                        'approvedRemarks': value.approvedRemarks,
                        'id': value.id,
                        'materialTypeName': value.materialTypeName,
                        'uom': value.uom,
                        'onHand': value.onHand,
                        'usagePatternList': value.usagePatternList,
                        'unitRefId': value.unitRefId,
                        'unitRefName': value.unitRefName,
                        'changeunitflag': false,
                    });
                });

                //vm.intertransfer.requestPersonId = vm.currentUserId;
                if (vm.templatesave.templatename != null)
                    vm.templatesave.templatename = vm.templatesave.templatename.toUpperCase();

                vm.templatesave.templateType = vm.defaulttemplatetype;

                //angular.forEach(locationdata.materialrequestlist, function (value, key) {
                //    interdetaildata.push({
                //        'interTransferRefId': 0,
                //        'sno': value.sno,
                //        'materialRefId': value.materialRefId,
                //        'materialRefName': value.materialRefName,
                //        'requestQty': value.requestQty,
                //        'issueQty': value.requestQty,
                //        'unitRefId': value.unitRefId,
                //        'price': value.price,
                //        'currentStatus': 'A', // app.localize('Approved'),
                //        'requestRemarks': value.requestRemarks,
                //        'approvedRemarks': value.approvedRemarks,
                //        'id': value.id,
                //        'materialTypeName': value.materialTypeName,
                //        'uom': value.uom,
                //        'onHand': value.onHand,
                //    });
                //});

                vm.intertransfer.approvedPersonId = vm.currentUserId;
                vm.intertransfer.currentStatus = "A";

                var saveStatuscurrentRequest = "";
                if (argOption == 1)
                    saveStatuscurrentRequest = "DA";
                else
                    saveStatuscurrentRequest = "DR";

                console.log("Request.js makeRequestAndApproval - createOrUpdateInterTransfer() Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));

                interTransferCrudService.createOrUpdateInterTransfer({
                    interTransfer: vm.intertransfer,
                    interTransferDetail: vm.intertransferdetaildata,
                    templateSave: vm.templatesave,
                    saveStatus: saveStatuscurrentRequest
                }).success(function (result) {
                    vm.transactionId = result.id;

                    if (argOption == 1) {
                        abp.message.success(app.localize('DirectApprovalDone', vm.transactionId));
                    }
                    else {
                        abp.message.success(app.localize('DirectApprovalAndReceivedDone', vm.transactionId));
                    }

                    vm.saving = false;
                    vm.printApproval(vm.transactionId);
                    //vm.cancel();

                }).finally(function () {
                    vm.saving = false;
                });
            }



            //  Manual Reason Modal Start
            vm.openManualReason = function (data, argColumnName) {
                vm.loading = true;
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/manualreason/manualReason.cshtml',
                    controller: 'tenant.views.house.master.manualreason.manualReason as vm',
                    backdrop: 'static',
                    resolve: {
                        manualreasonId: function () {
                            return null;
                        },
                        selectOnly: function () {
                            return true;
                        },
                        generalIncluded: function () {
                            return true;
                        },
                        defaultManualReasonCategoryRefId: function () {
                            return 7;
                        }
                        // General = 99,
                        //PurchaseOrder = 1,
                        //Receipt = 2,
                        //Invoice = 3,
                        //PurchaseReturn = 4,
                        //Adjustment = 5,
                        //MenuWastage = 6,
                        //Request = 7
                    }
                });

                modalInstance.result.then(function (result) {
                    if (!vm.isUndefinedOrNull(result)) {
                        if (argColumnName == 'remarks')
                            data.remarks = result;
                        else if (argColumnName == 'requestRemarks') {
                            data.requestRemarks = result;
                        }
                        else if (argColumnName == 'adjustmentRemarks') {
                            data.adjustmentRemarks = result;
                        }
                        else if (argColumnName == 'adjustmentApprovedRemarks') {
                            data.adjustmentApprovedRemarks = result;
                        }
                    }
                }, function (result) {
                    vm.loading = false;
                });

                vm.loading = false;
            }
            //  Manual Reason Modal End

        }
    ]);

    appModule.directive('focusNext', function () {
        return {
            restrict: 'A',
            link: function ($scope, elem, attrs) {

                elem.bind('keydown', function (e) {
                    var code = e.keyCode || e.which;
                    if (code === 13) {
                        elem.next()[0].focus();
                    }
                });
            }
        }



    })

})();

