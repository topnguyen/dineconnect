﻿(function () {
    appModule.controller('tenant.views.house.transaction.intertransfer.ApprovalPrint', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.interTransfer', 'appSession',
        'abp.services.app.location', 'abp.services.app.interTransferCrud',
        function ($scope, $filter, $state, $stateParams, $modal, uiGridConstants,
            intertransferService, appSession, locationService, interTransferCrudService) {
            /* eslint-disable */
            var vm = this;

            vm.connectdecimals = appSession.connectdecimals;
            vm.housedecimals = appSession.housedecimals;
            vm.loadingCount = 0;
            vm.permissions = {
                transportAllowed: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransferApproval.TransportAllowed'),
                printMaterialCode: abp.auth.hasPermission('Pages.Tenant.House.Master.Material.PrintMaterialCode'),
            };

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            var countryName = abp.features.getValue('DinePlan.DineConnect.Connect.Country');
            vm.enabledforIndia = false;
            if (countryName == 'IN') {
                vm.enabledforIndia = true;
            }

            vm.gotoIndex = function () {
                $state.go("tenant.intertransferapproval");
            }

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;

            vm.intertransferapproval = null;
            vm.intertransferapprovaldetail = null;

            vm.transfervalueshown = abp.setting.getBoolean("App.House.InterTransferValueShown");

            vm.intertransferrequestId = $stateParams.printid;

            vm.getData = function () {
                vm.loadingCount++;
                interTransferCrudService.getInterTransferApprovalForEdit({
                    id: vm.intertransferrequestId,
                    printFlag: true
                }).success(function (result) {

                    vm.intertransferapproval = result.interTransfer;

                    if (vm.intertransferapproval.requestLocationRefId != vm.defaultLocationId) {
                        $state.go("tenant.intertransferapproval");
                        return;
                    }

                    var currentStatus = vm.intertransferapproval.currentStatus;

                    vm.tempdetaildata = [];
                    vm.intertransferapprovaldetail = result.interTransferDetail;
                    vm.sno = 0;

                    angular.forEach(result.interTransferDetail, function (value, key) {
                        if (value.issueQty > 0) {
                            vm.sno = vm.sno + 1;
                            vm.tempdetaildata.push({
                                'interTransferRefId': value.interTransferRefId,
                                'sno': vm.sno,
                                'materialRefId': value.materialRefId,
                                'materialPetName': value.materialPetName,
                                'materialRefName': value.materialRefName,
                                'requestQty': value.requestQty,
                                'issueQty': value.issueQty,
                                'price': value.price,
                                'currentStatus': value.currentStatus,
                                'requestRemarks': value.requestRemarks,
                                'approvedRemarks': value.approvedRemarks,
                                'id': value.id,
                                'uom': value.uom,
                                'onHand': value.onHand,
                                'materialTypeName': value.materialTypeName,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'defaultUnitId': value.defaultUnitId,
                                'defaultUnitName': value.defaultUnitName,
                                'lineTotal': value.lineTotal
                            });
                        }
                    });


                    locationService.getUserInfoBasedOnUserId({
                        id: vm.intertransferapproval.approvedPersonId
                    }).success(function (result) {
                        vm.user = result;
                    });

                    locationService.getLocationForEdit({
                        Id: vm.intertransferapproval.requestLocationRefId
                    }).success(function (result) {
                        vm.billinglocation = result.location;
                    });

                    locationService.getLocationForEdit({
                        Id: vm.intertransferapproval.locationRefId
                    }).success(function (result) {
                        vm.shippinglocation = result.location;
                    });

                    locationService.getCompanyInfo({
                        locationId: vm.intertransferapproval.requestLocationRefId
                    }).success(function (result) {
                        vm.company = result;
                    });
                    vm.loadingCount--;
                });
            }

            vm.getData();

            vm.cancel = function () {
                $modalInstance.dismiss();
            }


        }]);
})();

