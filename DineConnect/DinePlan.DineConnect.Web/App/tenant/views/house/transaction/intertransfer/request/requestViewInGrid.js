﻿
(function () {
    appModule.controller('tenant.views.house.transaction.intertransfer.requestViewInGrid', [
        '$scope', '$uibModalInstance', '$uibModal', 'uiGridConstants', 'abp.services.app.interTransfer', 'transferId', 'appSession',
        function ($scope, $modalInstance, $modal, uiGridConstants, intertransferService, transferId, appSession) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;

            vm.porefnumber = null;
            vm.requestDate = null;
            vm.manualCompleteAllowed = false;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Group'),
                        field: 'materialGroupRefName',
                        minWidth: 100
                    },
                    {
                        name: app.localize('Category'),
                        field: 'materialGroupCategoryRefName',
                        minWidth: 100
                    },
                    {
                        name: app.localize('MaterialName'),
                        field: 'materialRefName',
                        width: 250
                    },
                    {
                        name: app.localize('Qty Ord'),
                        field: 'requestQty',
                        cellClass: 'ui-ralign',
                        width: 100
                    },
                    {
                        name: app.localize('Qty Rcd'),
                        field: 'issueQty',
                        cellClass: 'ui-ralign',
                        width : 100
                    },
                    {
                        name: app.localize('Request') + ' ' + app.localize('Remarks'),
                        field: 'requestRemarks',
                        minWidth: 100
                    },
                    {
                        name: app.localize('UOM'),
                        field: 'unitRefName',
                        width: 100
                    },
                    {
                        name: app.localize('PO') + ' ' + app.localize('Remarks'),
                        field: 'poCurrentStatus',
                        minWidth: 500,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <a ng-click=\"grid.appScope.viewPoDetail(row.entity)\" ng-if=\"row.entity.poCurrentStatus.length>0\">{{row.entity.poCurrentStatus}} <a>" +
                            "</div>",
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.setTransferAsCompletion = function (data) {
                vm.loading = true;
                purchaseorderService.setCompletePurchaseOrder({
                    transferId: transferId,
                    poRemarks: 'Manual Completion'
                }).success(function (result) {
                    vm.cancel();
                }).finally(function () {
                    vm.loading = false;
                });
            };


            vm.getAll = function () {
                vm.loading = true;
                intertransferService.getInterTransferForEdit({
                    id: transferId
                }).success(function (result) {
                    vm.interTransfer = result.interTransfer;
                    vm.userGridOptions.totalItems = result.interTransferDetail.length;
                    vm.userGridOptions.data = result.interTransferDetail;
                    if (result.interTransferDetail.length > 0) {
                        if (result.interTransfer.currentStatus == 'H') {
                            //   vm.manualCompleteAllowed = true;
                        }
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.getAll();

            vm.viewPoDetail = function (myObject) {
                vm.loading = true;
                openPoView(myObject.purchaseOrderRefId);
            };

            function openPoView(objId) {
                vm.loading = true;
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/purchaseorder/podetailViewInGrid.cshtml',
                    controller: 'tenant.views.house.transaction.purchaseorder.podetailViewInGrid as vm',
                    backdrop: 'static',
                    resolve: {
                        poId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                 
                }).finally(function (result) {
                    vm.loading = false;
                });
            }

        }]);
})();