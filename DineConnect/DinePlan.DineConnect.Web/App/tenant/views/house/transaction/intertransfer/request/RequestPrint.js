﻿(function () {
    appModule.controller('tenant.views.house.transaction.intertransfer.RequestPrint', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.interTransfer', 'appSession',
        'abp.services.app.location', 'abp.services.app.interTransferRequest',
        function ($scope, $filter, $state, $stateParams, $modal, uiGridConstants,
            intertransferService, appSession, locationService, interTransferRequestService) {
            /* eslint-disable */
            var vm = this;
            vm.loading = true;
            vm.loadingCount = 0;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
			});

			vm.connectdecimals = appSession.connectdecimals;
			vm.housedecimals = appSession.housedecimals;
            vm.permissions = {
                printMaterialCode: abp.auth.hasPermission('Pages.Tenant.House.Master.Material.PrintMaterialCode'),
            };

            vm.gotoIndex = function () {
                $state.go("tenant.intertransferrequest");
            }

            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;

            vm.intertransferrequest = null;
            vm.intertransferrequestdetail = null;

            
            vm.intertransferrequestId = $stateParams.printid;

            vm.getData = function () {
                vm.loading = true;
                vm.loadingCount++;
                interTransferRequestService.getInterTransferForEdit({
                    Id: vm.intertransferrequestId
                }).success(function (result) {

                    vm.intertransfer = result.interTransfer; 

                    if (vm.intertransfer.locationRefId != vm.defaultLocationId) {
                        $state.go("tenant.intertransferrequest");
                        return;
                    }
                    var currentStatus = vm.intertransfer.currentStatus;

                    vm.tempdetaildata = [];
                    vm.intertransferrequestdetail = result.interTransferDetail;
                    vm.sno = 0;

                    angular.forEach(result.interTransferDetail, function (value, key) {
                        if (value.issueQty >= 0) {
                            vm.sno = vm.sno + 1;
                            vm.tempdetaildata.push({
                                'interTransferRefId': value.interTransferRefId,
                                'sno':  vm.sno,
                                'materialRefId': value.materialRefId,
                                'materialPetName' : value.materialPetName,
                                'materialRefName': value.materialRefName,
                                'requestQty': value.requestQty,
                                'issueQty': value.issueQty,
                                'currentStatus': value.currentStatus,
                                'requestRemarks': value.requestRemarks,
                                'approvedRemarks': value.approvedRemarks,
                                'id': value.id,
                                'uom': value.uom,
                                'onHand': value.onHand,
                                'materialTypeName': value.materialTypeName,
                                'unitRefName': value.unitRefName,
                                'unitRefId': value.unitRefId,
                                'defaultUnitId': value.defaultUnitId,
                                'defaultUnitName': value.defaultUnitName,
                                'poCurrentStatus': value.poCurrentStatus
                            });
                        }
                    });

                    locationService.getUserInfoBasedOnUserId({
                        id: vm.intertransfer.creatorUserId
                    }).success(function (result) {
                        vm.user = result;
                    });

                    locationService.getLocationForEdit({
                        Id: vm.intertransfer.locationRefId
                    }).success(function (result) {
                        vm.billinglocation = result.location;
                    });

                    locationService.getLocationForEdit({
                        Id: vm.intertransfer.requestLocationRefId
                    }).success(function (result) {
                        vm.shippinglocation = result.location;
                        vm.loading = false;
                    });
                }).finally(function () {
                    vm.loadingCount--;
                });
            
                    
            }

            vm.getData();


            vm.sendMail = function () {
                vm.potext = '';
                vm.loading = true;
                intertransferService.sendTransferRequestEmail(
                {
                    interTransferId: vm.intertransferrequestId,
                    interTransferHTML: vm.potext
                }).success(function (result) {
                    abp.message.success(app.localize('MailedSuccessfully'));
                    abp.notify.success(app.localize('MailedSuccessfully'));
                }).finally(function () {
                    vm.loading = false;
                });
            }


         
        
        }]);
})();

