﻿(function () {
    appModule.controller('tenant.views.house.transaction.intertransfer.intertransferrequestsummary', [
			'$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.interTransfer', 'appSession', 'abp.services.app.location', 'abp.services.app.material', 'abp.services.app.dayClose', 
			function ($scope, $state, $modal, uiGridConstants, intertransferService, appSession, locationService, materialService, daycloseService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.defaultLocationId = appSession.user.locationRefId;

            vm.locationRefId = null;
            vm.materialRefId = 0;

            var todayAsString = moment().format($scope.format);
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Transaction.InterTransfer.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Transaction.InterTransfer.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Transaction.InterTransfer.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
				    {
                        name: app.localize('Id'),
                        field: 'id'
				    },
                     {
                         name: app.localize('RequestLocation'),
                         field: 'requestLocationRefName'
                     },
                    {
                        name: app.localize('Location'),
                        field: 'locationRefName'
                    },
                   
                    {
                        name: app.localize('RequestDate'),
                        field: 'requestDate',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD\'',
                        minWidth: 100
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                intertransferService.getView({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    requestStatus: '',
                    defaultLocationRefId: vm.defaultLocationId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.exportToExcel = function () {
                intertransferService.getAllToExcel({
                    //locationRefId: vm.locationRefId,
                    //materialRefId: vm.materialRefId
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.checkCloseDay = function () {
							daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        abp.notify.info(app.localize("LastDayNotClosed"));
                        abp.message.warn(app.localize("LastDayNotClosed"));
                        $state.go('tenant.dayclose');
                        return false;
                    }
                    else {
                        //init();
                        return true;
                    }

                }).finally(function () {
                });
            }

            vm.checkCloseDay();

            vm.requestsummary = function () {
                vm.loading = true;
                intertransferService.getInterTransferRequestSummary({
                    locationRefId: vm.locationRefId,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    materialRefId: vm.materialRefId,
                }).success(function (result) {
                    vm.podata = result;
                    if (result != null) {
                        vm.userGridOptions.totalItems = result.length;
                        vm.userGridOptions.data = result;
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.refmaterial = [];

            function fillDropDownMaterial() {
                materialService.getMaterialForCombobox({}).success(function (result) {
                    vm.refmaterial = result.items;
                });
            }

            vm.locations = [];

            vm.getLocations = function () {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                    //if (vm.defaultLocationId != null && vm.defaultLocationId > 0) {
                    //    vm.locationRefId = vm.defaultLocationId;
                    //}
                }).finally(function (result) {

                });
            };

            fillDropDownMaterial();
            vm.getLocations();

        }]);
})();

