﻿(function () {
    appModule.controller('tenant.views.house.transaction.intertransfer.approval', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.interTransfer', 'uiGridConstants', 'appSession', '$rootScope', 'abp.services.app.dayClose', 'abp.services.app.interTransferCrud',
        function ($scope, $state, $stateParams, $modal, intertransferService, uiGridConstants, appSession, $rootScope, daycloseService, interTransferCrudService) {

            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            vm.loading = true;
            console.log("approval.js Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));

            $rootScope.settings.layout.pageSidebarClosed = true;
            vm.intertransferapproval = null;
            vm.intertransferapprovaldetail = null;
            vm.rejectallbuttonstatus = false;

            vm.transfervalueshown = abp.setting.getBoolean("App.House.InterTransferValueShown");
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.maxDate = moment().add(90, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];


            $scope.existall = true;
            vm.sno = 0;
            vm.intertransferrequestId = $stateParams.id;
            vm.requestLocationRefName = $stateParams.requestLocationRefName;
            vm.locationRefName = $stateParams.locationRefName;
            vm.currentUserId = abp.session.userId;

            vm.defaultLocationId = appSession.user.locationRefId;

            vm.permissions = {
                transportAllowed: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransferApproval.TransportAllowed'),
                hideStock: abp.auth.hasPermission('Pages.Tenant.House.Transaction.DayClose.HideStockInTransfer'),
                negativeStockAllowedAuthorization: abp.auth.hasPermission('Pages.Tenant.House.Master.MaterialLedger.NegativeStock'),
                editDeliveryDate: abp.auth.hasPermission
                    ('Pages.Tenant.House.Transaction.InterTransfer.EditDeliveryDate')
            };

            $scope.convert = function (thedate) {
                var tempstr = thedate.toString();
                var searchGmt = tempstr.search("GMT");
                if (searchGmt > 0) {
                    var newstr = tempstr.toString().replace(/GMT.*/g, "")
                    newstr = newstr + " UTC";
                    return new Date(newstr);
                }
                else {
                    return thedate;
                }
            };


            //  Date Range Options
            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            //  Date Angular Js 
            $scope.today = function () {
                $scope.dt = moment(new Date()).format("DD-MMM-YYYY");
            };
            $scope.today();

            $scope.clear = function () {
                $scope.dt = null;
            };

            $scope.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            $scope.dateOptions = {
                dateDisabled: disabled,
                formatYear: 'yy',
                maxDate: new Date().setDate(new Date().getDate() + 1),
                minDate: new Date(),
                startingDay: 1
            };

            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                    mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }

            $scope.toggleMin = function () {
                $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
                $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
            };

            $scope.toggleMin();

            $scope.open1 = function () {
                $scope.popup1.opened = true;
            };

            $scope.open2 = function () {
                $scope.popup2.opened = true;
            };

            $scope.setDate = function (year, month, day) {
                $scope.dt = new Date(year, month, day);
            };

            $scope.formats = ['YYYY-MMM-DD', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];
            $scope.altInputFormats = ['M!/d!/yyyy'];

            $scope.popup1 = {
                opened: false
            };

            $scope.popup2 = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            $scope.events = [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];

            function getDayClass(data) {
                var date = data.date,
                    mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            }

            var requestParams = {
                skipCount: 0,
                maxResultCount: 5, // app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.intertransferapprovaldetaildata = [];
            vm.tempdetaildata = [];

            vm.save = function (argOption) {
                if ($scope.existall == false)
                    return;

                vm.saving = true;
                vm.intertransferapprovaldetaildata = [];

                vm.allrejectstatus = true;
                vm.rejectcount = 0;
                vm.pendingcount = 0;

                vm.errorFlag = false;

                angular.forEach(vm.gridOptions.data, function (value, key) {
                    vm.intertransferapprovaldetaildata.push({
                        'interTransferRefId': 0,
                        'sno': value.sno,
                        'materialRefId': value.materialRefId,
                        'materialRefName': value.materialRefName,
                        'requestQty': value.requestQty,
                        'issueQty': value.issueQty,
                        'price': value.price,
                        'currentStatus': value.currentStatus,
                        'requestRemarks': value.requestRemarks,
                        'approvedRemarks': value.approvedRemarks,
                        'id': value.id,
                        'uom': value.uom,
                        'onHand': value.onHand,
                        'materialTypeName': value.materialTypeName,
                        'unitRefName': value.unitRefName,
                        'unitRefId': value.unitRefId,
                        'defaultUnitId': value.defaultUnitId,
                        'defaultUnitName': value.defaultUnitName,
                        'holdQty': value.returnQty,
                        'holdFlag': value.returnFlag,
                    });

                    if (value.issueQty < value.requestQty && value.currentStatus != app.localize('Rejected')) {
                        if (value.returnFlag == false && value.omitFlag == false && argOption == 0) {
                            abp.notify.warn(app.localize('DifferenceActionNeedsToMentioned', value.materialRefName));
                            abp.message.warn(app.localize('DifferenceActionNeedsToMentioned', value.materialRefName));
                            vm.errorFlag = true;
                            return;
                        }
                    }

                    if (value.currentStatus != app.localize('Rejected'))
                        vm.allrejectstatus = false;

                    if (value.currentStatus == app.localize('Rejected'))
                        vm.rejectcount = vm.rejectcount + 1;
                    else if (value.currentStatus == app.localize('Pending'))
                        vm.pendingcount = vm.pendingcount + 1;
                });

                if (vm.errorFlag) {
                    vm.saving = false;
                    vm.loading = false;
                    return;
                }

                vm.savestatus = "";

                if (vm.allrejectstatus) {
                    vm.savestatus = "REJECT";
                }

                if (argOption == 1) {
                    vm.intertransferapproval.completedTime = null;
                    if (vm.savestatus == "")
                        vm.savestatus = 'DRAFT';
                }
                else {
                    if (vm.pendingcount > 0) {
                        abp.notify.info(app.localize('PendingSaveError', vm.pendingcount));
                        abp.message.warn(app.localize('PendingSaveError', vm.pendingcount), app.localize('SaveFailed'));
                        vm.saving = false;
                        return;
                    }

                    vm.saving = false;
                    vm.rejectcancel = false;

                    vm.response = false;

                    vm.saving = true;

                    if (moment().format('YYYY-MMM-DD') == moment(vm.intertransferapproval.completedTime).format('YYYY-MMM-DD'))
                        vm.intertransferapproval.completedTime = $scope.convert(moment());


                    if (vm.savestatus == "")
                        vm.savestatus = "SAVE";
                }

                vm.intertransferapproval.approvedPersonId = vm.currentUserId;
                vm.intertransferapproval.deliveryDateRequested = $scope.convert(vm.deliveryDateRequested);

                console.log("approval.js createOrUpdateApproval() Call Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                interTransferCrudService.createOrUpdateApproval({
                    interTransfer: vm.intertransferapproval,
                    interTransferDetail: vm.intertransferapprovaldetaildata,
                    saveStatus: vm.savestatus
                }).success(function (result) {
                    vm.transactionId = result.id;
                    if (argOption == 1) {
                        abp.notify.info(app.localize('Draft') + app.localize('SavedSuccessfully'));
                    }
                    else {
                        abp.notify.info(app.localize('Approval') + app.localize('SavedSuccessfully'));

                        if (vm.savestatus == "SAVE") {
                            abp.message.success(app.localize('AcceptedRequest', vm.transactionId));
                            vm.printApproval(vm.transactionId);
                            vm.saving = false;
                            return;
                        }
                        else if (vm.savestatus == "REJECT")
                            abp.message.warn(app.localize('RejectRequest', vm.transactionId));
                        vm.saving = false;
                        vm.cancel();
                        return;
                    }
                }).finally(function () {
                    vm.saving = false;
                    console.log("approval.js createOrUpdateApproval() Call End Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                });
            };

            vm.printApproval = function (objId) {
                $rootScope.settings.layout.pageSidebarClosed = false;
                $state.go('tenant.intertransferapprovalprint', {
                    printid: objId
                });
            }


            $scope.func = function (data, val) {
                data.materialRefName = val;
                //data.materialRefId = data.value;
            };


            vm.existall = function () {

                if (vm.intertransferapproval.Id == null) {
                    vm.existall = false;
                    return;
                }
            };

            vm.cancel = function () {
                $rootScope.settings.layout.pageSidebarClosed = false;
                $state.go("tenant.intertransferapproval");
            };

            vm.close = function () {
                intertransferService.updateRequestStatusDraftToPending({
                    Id: vm.intertransferrequestId
                }).success(function (result) {

                });

                $state.go("tenant.intertransferapproval");
                $rootScope.settings.layout.pageSidebarClosed = false;
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.dsetFocus = function (obj) {
                //console.log(obj);
            }

            //vm.refunit = [];
            //vm.tempRefUnit = [];

            //function fillDropDownUnit() {
            //    materialService.getUnitForCombobox({}).success(function (result) {
            //        vm.refunit = result.items;
            //        vm.tempRefUnit = vm.refunit;
            //    });
            //}

            vm.requestParams = {
                locations: '',
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.tempdetaildata = [];

            vm.checkCloseDay = function () {
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {

                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                    vm.intertransferapproval.completedTime = moment(result.accountDate).format($scope.format);

                    $rootScope.settings.layout.pageSidebarClosed = true;
                }).finally(function (result) {

                });
            }



            function init() {
                vm.loading = true;
                //fillDropDownMaterial();
                //fillDropDownUnit();

                console.log("approval.js getInterTransferApprovalForEdit() Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                interTransferCrudService.getInterTransferApprovalForEdit({
                    id: vm.intertransferrequestId
                }).success(function (result) {

                    vm.intertransferapproval = result.interTransfer;
                    //fillDropDownLocation();
                    if (vm.intertransferapproval.requestDate == "0001-01-01T00:00:00") {
                        $scope.dt = new Date();
                    }
                    else {
                        $scope.dt = new Date(vm.intertransferapproval.requestDate);
                    }

                    vm.checkCloseDay();

                    if (vm.intertransferapproval.requestLocationRefId != vm.defaultLocationId) {
                        vm.cancel();
                        return;
                    }

                    //vm.intertransferapproval.deliveryDateRequested = moment(result.interTransfer.deliveryDateRequested).format($scope.format);
                    //$scope.deliveryDateRequested = vm.intertransferapproval.deliveryDateRequested;
                    //$('input[name="deliveryDateRequested"]').daterangepicker({
                    //    locale: {
                    //        format: $scope.format
                    //    },
                    //    singleDatePicker: true,
                    //    showDropdowns: true,
                    //    startDate: $scope.deliveryDateRequested,
                    //    minDate: moment(),
                    //    maxDate: $scope.maxDate
                    //});

                    vm.intertransferapproval.deliveryDateRequested = moment(result.interTransfer.deliveryDateRequested).format('YYYY-MMM-DD HH:mm');
                    vm.deliveryDateRequested = vm.intertransferapproval.deliveryDateRequested;


                    if (result.interTransfer.id != null) {

                        var currentStatus = vm.intertransferapproval.currentStatus;

                        if (currentStatus == 'R') {
                            abp.message.error(app.localize('ApproveNotAllowedForReceived'));
                            return;
                        }

                        if (currentStatus == 'I') {
                            abp.message.error(app.localize('ApproveNotAllowedForPartialReceived'));
                            return;
                        }

                        if (currentStatus == 'C') {
                            abp.message.error(app.localize('ApproveNotAllowedForCompleted'));
                            return;
                        }

                        if (currentStatus == 'A') {
                            abp.message.warn(app.localize('AlreadyApproved'));
                            return;
                        }

                        if (currentStatus == 'X') {
                            abp.message.error(app.localize('ApproveNotAllowedForCancel'));
                            return;
                        }

                        if (currentStatus != 'P' && currentStatus != 'D') {
                            abp.message.error(app.localize('ApproveAllowedForPendingDraft'));
                            vm.cancel();
                            return;
                        }

                        vm.tempdetaildata = [];
                        vm.intertransferapprovaldetail = result.interTransferDetail;
                        vm.sno = 0;

                        angular.forEach(result.interTransferDetail, function (value, key) {
                            vm.tempdetaildata.push({
                                'interTransferRefId': value.interTransferRefId,
                                'sno': value.sno,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'requestQty': value.requestQty,
                                'issueQty': value.issueQty,
                                'price': value.price,
                                'currentStatus': value.currentStatus,
                                'requestRemarks': value.requestRemarks,
                                'approvedRemarks': value.approvedRemarks,
                                'id': value.id,
                                'uom': value.uom,
                                'onHand': value.onHand,
                                'materialTypeName': value.materialTypeName,
                                'toLocationOnHand': value.toLocationOnHand,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'defaultUnitId': value.defaultUnitId,
                                'defaultUnitName': value.defaultUnitName,
                                'onHandWithUom': value.onHandWithUom,
                                'omitFlag': false,
                                'omitQty': 0,
                                'omitMode': '',
                                'returnFlag': false,
                                'returnQty': 0,
                                'returnMode': '',
                                'alertcss': '',
                                'diffQty': value.requestQty
                            });
                        });

                        vm.loading = false;
                        nonFilteredData = vm.tempdetaildata;
                        vm.applyFilters();

                    }
                    else {
                        vm.cancel();
                    }

                    if (vm.transfervalueshown == true) {
                        vm.columnprice =
                        {
                            name: app.localize('Cost'),
                            field: 'price',
                            cellClass: 'ui-ralign',
                            width: 60
                        };
                        vm.gridOptions.columnDefs.splice(2, 0, vm.columnprice);
                    }

                    if (vm.permissions.hideStock == false) {
                        vm.columnonHand =
                        {
                            name: app.localize('OnHand'),
                            field: 'onHandWithUom',
                            cellClass: function (grid, row) {
                                if (row.entity.onHand < row.entity.requestQty)
                                    return 'ui-red ui-ralign';
                                else
                                    return 'ui-ralign';
                            },
                        },
                            vm.gridOptions.columnDefs.splice(4, 0, vm.columnonHand);
                    }


                    intertransferService.updateRequestStatusDraft({
                        Id: vm.intertransferrequestId
                    }).success(function (result) {

                    });
                    vm.loading = false;

                    if (vm.permissions.hideStock == true) {
                        abp.notify.error(app.localize('HideStockMessage'));
                    }
                    console.log("approval.js getInterTransferApprovalForEdit() End Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                });
            }

            init();

            //  Include Edition in Grid Details

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            var nonFilteredData = [];
            vm.filterText = $stateParams.filterText || '';

            vm.getLocationValue = function (item) {
                return parseInt(item.value);
            };

            vm.gridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize('#'),
                        field: 'sno',
                        width: 30
                    },
                    {
                        name: app.localize('MaterialName'),
                        field: 'materialRefName',
                        cellClass: function (grid, row) {
                            if (row.entity.onHand < row.entity.requestQty)
                                return 'ui-red';
                        },
                        width: 200
                    },
                    {
                        name: app.localize('MaterialType'),
                        field: 'materialTypeName',
                        maxWidth: 50
                    },
                    //{
                    //    name: app.localize('OnHand'),
                    //    field: 'onHandWithUom',
                    //    cellClass: function (grid, row) {
                    //        if (row.entity.onHand < row.entity.requestQty)
                    //            return 'ui-red ui-ralign';
                    //        else
                    //            return 'ui-ralign';
                    //    },
                    //},
                    {
                        name: app.localize('requestQty'),
                        field: 'requestQty',
                        cellClass: function (grid, row) {
                            if (row.entity.onHand < row.entity.requestQty)
                                return 'ui-red ui-ralign';
                            else
                                return 'ui-ralign';
                        },
                        cellFilter: 'fractionFilter'

                    },
                    {
                        name: app.localize('Request') + ' ' + app.localize('Remarks'),
                        field: 'requestRemarks',
                        minWidth: 150
                    },
                    {
                        name: app.localize('uom'),
                        field: 'unitRefName',
                        minWidth: 100
                    },
                    {
                        name: app.localize('issueQty'),
                        field: 'issueQty',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                        //cellClass:
                        //    function (grid, row) {
                        //        if (row.entity.issueQty >= row.entity.requestQty)
                        //            return 'ui-approved';
                        //        else if (row.entity.issueQty == 0)
                        //            return 'ui-pending';
                        //        else if (row.entity.issueQty < row.entity.requestQty)
                        //            return 'ui-rejected';
                        //        else
                        //            return 'ui-red';
                        //    },
                    },
                    {
                        name: app.localize('AvlQty'),
                        field: 'toLocationOnHand',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: app.localize('Status'),
                        field: 'currentStatus',
                        //cellClass: row.entity.currentStatus 
                        //    function (grid, row) {
                        //    if (row.entity.currentStatus == app.localize('Pending'))
                        //        return 'ui-red';
                        //    else if (row.entity.currentStatus < app.localize('Approved') || row.entity.currentStatus == app.localize('Excess'))
                        //        return 'ui-approved';
                        //    else if (row.entity.currentStatus == app.localize('Rejected'))
                        //        return 'ui-red';
                        //    else
                        //        return 'ui-red';
                        //},
                    },
                    {
                        name: app.localize('CarryOver'),
                        field: 'returnQty',
                        cellClass: function (grid, row) {
                            if (row.entity.returnQty < 0)
                                return 'ui-red ui-ralign';
                            else
                                return 'ui-ralign';
                        },
                        //cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Approve'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.approveCurrent(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Approve') + '"><i class="fa fa-check-circle"></i></button>' +
                            '</div>',
                        width: 50
                    },
                    {
                        name: app.localize('Edit'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.editEntry(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>' +
                            '</div>',
                        width: 50
                    },
                    {
                        name: app.localize('Reject'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.rejectCurrent(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Reject') + '"><i class="fa fa-remove"></i></button>' +
                            '</div>',
                        width: 50
                    }
                ],
                data: []
            };

            vm.pendingAll = function () {
                angular.forEach(vm.tempdetaildata, function (value, key) {
                    vm.pendingCurrent(value);
                });
            }

            vm.pendingCurrent = function (item) {
                item.issueQty = 0;
                item.currentStatus = app.localize('Pending');
                vm.rejectAllCheck();
            }

            vm.approveAll = function () {
                angular.forEach(vm.tempdetaildata, function (value, key) {
                    vm.approveCurrent(value);
                });
            }

            vm.approveCurrent = function (item) {
                if (item.onHand < item.requestQty) {
                    abp.notify.warn(app.localize("OnHandLessThanRequestQtyErr", item.materialRefName, item.onHand, item.requestQty));
                    if (vm.permissions.negativeStockAllowedAuthorization == false)
                        return;
                }
                item.issueQty = item.requestQty;
                item.currentStatus = app.localize('Approved');
                vm.rejectAllCheck();
            }

            vm.rejectAll = function () {
                angular.forEach(vm.tempdetaildata, function (value, key) {
                    vm.rejectCurrent(value);
                });
            }

            vm.rejectCurrent = function (item) {
                item.issueQty = 0;
                item.currentStatus = app.localize('Rejected');
                vm.rejectAllCheck();
            }

            vm.rejectAllCheck = function () {
                vm.allrejectstatus = true
                angular.forEach(vm.gridOptions.data, function (value, key) {
                    vm.intertransferapprovaldetaildata.push({
                        'interTransferRefId': 0,
                        'sno': value.sno,
                        'materialRefId': value.materialRefId,
                        'materialRefName': value.materialRefName,
                        'requestQty': value.requestQty,
                        'issueQty': value.issueQty,
                        'currentStatus': value.currentStatus,
                        'requestRemarks': value.requestRemarks,
                        'approvedRemarks': value.approvedRemarks,
                        'id': value.id,
                        'uom': value.uom,
                        'onHand': value.onHand,
                        'materialTypeName': value.materialTypeName,
                        'unitRefId': value.unitRefId,
                        'unitRefName': value.unitRefName,
                        'defaultUnitId': value.defaultUnitId,
                        'defaultUnitName': value.defaultUnitName,
                    });

                    if (value.currentStatus != app.localize('Rejected'))
                        vm.allrejectstatus = false;

                    if (value.currentStatus == app.localize('Rejected'))
                        vm.rejectcount = vm.rejectcount + 1;
                    else if (value.currentStatus == app.localize('Pending'))
                        vm.pendingcount = vm.pendingcount + 1;
                });

                if (vm.allrejectstatus == true)
                    vm.rejectallbuttonstatus = true;
                else
                    vm.rejectallbuttonstatus = false;
            }

            vm.editEntry = function (item) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/intertransfer/approval/editRequest.cshtml',
                    controller: 'tenant.views.house.transaction.approval.editRequest as vm',
                    resolve: {
                        locationId: function () {
                            return vm.intertransferapproval.requestLocationRefId;
                        },
                        allItems: function () {
                            return vm.gridOptions.data;
                        },
                        initialText: function () {
                            return item;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.gridOptions.data = result;
                    vm.rejectAllCheck();
                });
            };




            vm.applyFilters = function () {
                if (!vm.filterText) {
                    vm.gridOptions.data = nonFilteredData;
                }
                var filterText = vm.filterText.trim().toLocaleLowerCase();
                vm.gridOptions.data = _.filter(nonFilteredData, function (text) {
                    if (vm.targetValueFilter == 'EMPTY' && text.targetValue) {
                        return false;
                    }

                    return (text.materialRefName && text.materialRefName.toLocaleLowerCase().indexOf(filterText) >= 0);
                });
            };

        }
    ])

        .filter('fractionFilter', function () {
            return function (value) {
                return value.toFixed(6);
            };
        });

})();

