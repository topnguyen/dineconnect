﻿(function () {
    appModule.controller('tenant.views.house.transaction.received.editReceived', [
       '$scope', '$uibModalInstance',  'locationId', 'allItems', 'initialText',
        function ($scope, $modalInstance, locationId, allItems, initialText) {
            var vm = this;
            /* eslint-disable */
            vm.allItems = allItems;
            vm.currentText = initialText;
            vm.editingText = angular.extend({}, vm.currentText);
            if (vm.editingText.receivedQty == 0)
                vm.editingText.receivedQty = "";

            vm.locationId = locationId;

            vm.currentIndex = 0;
            vm.saving = false;

            //Calculating index of currentText in allItems
            for (var i = 0; i < allItems.length; i++) {
                if (vm.allItems[i] == vm.currentText) {
                    vm.currentIndex = i;
                    break;
                }
            }
            
            function save(close) {
                function executeAfterAction() {
                    if (close) {
                        $modalInstance.close();
                    } else {
                        //Go to next
                        vm.currentIndex++;
                        if (vm.allItems.length <= vm.currentIndex) {
                            $modalInstance.close();
                            return;
                        }

                        vm.currentText = vm.allItems[vm.currentIndex];
                        vm.editingText = angular.extend({}, vm.currentText);

                        if (vm.editingText.receivedQty == 0)
                            vm.editingText.receivedQty = "";
                    }
                }

                if ((vm.editingText.receivedQty == "") || (vm.currentText.receivedQty == vm.editingText.receivedQty &&  vm.editingText.receivedQty!=0)) {
                    vm.currentText.adjustmentQty = 0;
                    vm.currentText.adjustmentMode = '';
                    vm.currentText.adjustmentFlag = false;

                    vm.currentText.returnFlag = false;
                    vm.currentText.returnQty = 0;
                    vm.currentText.returnMode = '';

                    executeAfterAction();
                    return;
                }
                
                if (vm.editingText.receivedQty != vm.editingText.issueQty || vm.editingText.receivedQty==0)
                {
                    if (vm.editingText.returnFlag == false && vm.editingText.adjustmentFlag == false) {
                        abp.notify.warn(app.localize('DifferenceSetOffModeNeedsToMentioned', vm.editingText.materialRefName));
                        return;
                    }

                    abp.notify.warn(app.localize('MismatchBetweenIssueQtyAndReceivedQtyErr',vm.editingText.receivedQty));

                    //abp.message.warn(app.localize('MismatchBetweenIssueQtyAndReceivedQtyErr',vm.editingText.receivedQty));
                    //return;

                 
                }

                vm.currentText.receivedQty = vm.editingText.receivedQty;

                if (vm.editingText.adjustmentFlag)
                {
                    vm.editingText.adjustmentQty = Math.abs(vm.editingText.diffQty);
                }

                if (vm.editingText.returnFlag)
                {
                    vm.editingText.returnQty = Math.abs(vm.editingText.diffQty)
                }

                vm.currentText.adjustmentQty = vm.editingText.adjustmentQty;
                vm.currentText.adjustmentMode = vm.editingText.adjustmentMode;
                vm.currentText.adjustmentFlag = vm.editingText.adjustmentFlag;

                vm.currentText.returnFlag = vm.editingText.returnFlag;
                vm.currentText.returnQty = vm.editingText.returnQty;
                vm.currentText.returnMode = vm.editingText.returnMode;


                var iqty = parseFloat(vm.currentText.issueQty);
                var rqty = parseFloat(vm.currentText.receivedQty);

                if (iqty == rqty)
                    vm.currentText.currentStatus = app.localize("Received");
                else if (iqty == 0)
                    vm.currentText.currentStatus = app.localize("Pending");
                else if (iqty < rqty)
                    vm.currentText.currentStatus = app.localize("Excess");
                else if (iqty > rqty)
                    vm.currentText.currentStatus = app.localize("Shortage");

                executeAfterAction();
                //abp.notify.info(app.localize('ModificationEffected'));

            };

            vm.saveAndNext = function() {
                save(false);
            }

            vm.saveAndClose = function () {
                save(true);
            }

            vm.previous = function () {
                if (vm.currentIndex <= 0) {
                    return;
                }

                vm.currentIndex--;
                vm.currentText = vm.allItems[vm.currentIndex];
                vm.editingText = angular.extend({}, vm.currentText);
            };

          

            vm.targetValueKeyPress = function ($event) {
             
                if ($event.keyCode == 13) {
                    vm.saveAndNext();
                } else if ($event.keyCode == 37) {
                    vm.previous();
                } else if ($event.keyCode == 39) {
                    vm.saveAndNext();
                }
            }

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };

            vm.adjumentQtyCalc = function () {
                if (vm.editingText.receivedQty == null || vm.isUndefinedOrNull(vm.editingText.receivedQty)) {
                    vm.editingText.adjustmentMode = '';
                    vm.editingText.returnMode = '';
                    vm.editingText.returnFlag = false;
                    vm.editingText.adjustmentFlag = false;
                    vm.editingText.diffQty = null;
                    return;
                }

                vm.editingText.diffQty = parseFloat(vm.editingText.issueQty) - parseFloat(vm.editingText.receivedQty);
                vm.editingText.diffQty = vm.editingText.diffQty.toFixed(3);
                if (vm.editingText.diffQty == 0) {
                    vm.editingText.adjustmentMode = '';
                    vm.editingText.returnMode = '';
                    vm.editingText.adjustmentQty = 0;
                    vm.editingText.returnQty = 0;
                    vm.editingText.returnFlag = false;
                    vm.editingText.adjustmentFlag = false;
                }
                else if (vm.editingText.diffQty > 0) {
                    vm.editingText.adjustmentMode = app.localize('Shortage');
                }
                else if (vm.editingText.diffQty < 0) {
                    vm.editingText.adjustmentMode = app.localize('Excess');
                }
            }

        }
    ]);
})();