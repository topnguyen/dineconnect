﻿
(function () {
    appModule.controller('tenant.views.house.transaction.supplierdocument.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.supplierDocument', 'supplierdocumentId', 'abp.services.app.supplier', 'FileUploader',
        function ($scope, $modalInstance, supplierdocumentService, supplierdocumentId, supplierService, fileUploader) {
            var vm = this;
            

            vm.saving = false;
            vm.supplierdocument = null;
			$scope.existall = true;
			vm.changeFlag = false;
			
			vm.uploader = new fileUploader({
			    url: abp.appPath + 'DocumentUpload/UploadPoDocumentInfo',
			    formData: [{ id: 0 }],
			    queueLimit: 1,
			    filters: [{
			        name: 'imageFilter',
			        fn: function (item, options) {
			            //File type check
			            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
			            if ('|jpg|jpeg|pdf|'.indexOf(type) === -1) {
			                abp.message.warn(app.localize('ProfilePicture_Warn_FileType'));
			                return false;
			            }
			            //File size check
			            if (item.size > 3072000) //3000KB
			            {
			                abp.message.warn(app.localize('ProfilePicture_Warn_SizeLimit'));
			                return false;
			            }
			            return true;
			        }
			    }]
			});

			vm.uploader.onBeforeUploadItem = function (fileitem) {
			    fileitem.formData.push({ documentRefId: vm.supplierdocument.documentRefId });
			    fileitem.formData.push({ supplierRefId: vm.supplierdocument.supplierRefId })
			};

			vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
			    vm.loading = true;
			    vm.loading = false;
			    vm.cancel();
			};


			$scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
			$scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
			$scope.format = $scope.formats[0];

			vm.dateOptions = [];
			vm.dateOptions.push({
			    'singleDatePicker': true,
			    'showDropdowns': true,
			    'startDate': moment(),
			    'minDate': $scope.minDate,
			    'maxDate': moment()
			});

            //date start

			$('input[name="completedTime"]').daterangepicker({
			    locale: {
			        format: $scope.format
			    },
			    singleDatePicker: true,
			    showDropdowns: true,
			    startDate: moment(),
			    minDate: $scope.minDate,
			    maxDate: moment()
			});


            vm.save = function () {
			   if ($scope.existall == false)
                    return;
                vm.saving = true;
				
                supplierdocumentService.createOrUpdateSupplierDocument({
                    supplierDocument: vm.supplierdocument
                }).success(function (result) {
                    vm.supplierdocumentid = result.id;
                    vm.uploader.uploadAll();
                   
                }).finally(function () {
                    vm.saving = false;
                    abp.notify.info('\' SupplierDocument \'' + app.localize('SavedSuccessfully'));
                    if (vm.changeFlag == false)
                        $modalInstance.close();
                });
            };

            vm.changeVariable = function () {
                vm.changeFlag = true;
            };

			 vm.existall = function ()
            {
                
			     if (vm.supplierdocument.id == null) {
			         vm.existall = false;
			         return;
			     }
				 
                supplierdocumentService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.supplierdocument.id,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.supplierdocument.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.supplierdocument.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
			 vm.refsupplier = [];

			 function fillDropDownSupplier() {
			     vm.loading = true;
			     supplierService.getSupplierForCombobox({}).success(function (result) {
			         vm.refsupplier = result.items;
			         vm.loading = false;
			     });
			 }

			 vm.refdocumenttype = [];

			 function fillDropDownDocumentType() {
			     vm.loading = true;
			     supplierService.getDocumentTypeForCombobox({}).success(function (result) {
			         vm.refdocumenttype = result.items;
			         vm.loading = false;
			     });
			 }

	        function init() {
				fillDropDownSupplier();
				fillDropDownDocumentType();
                supplierdocumentService.getSupplierDocumentForEdit({
                    Id: supplierdocumentId
                }).success(function (result) {
                    vm.supplierdocument = result.supplierDocument;
                    $scope.dt = moment().format($scope.format);
                    if (result.supplierDocument.id != null) {
                        vm.supplierdocument.completedTime = moment(result.supplierDocument.completedTime).format($scope.format);
                        $scope.minDate = vm.supplierdocument.completedTime;

                        $('input[name="completedTime"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: vm.supplierdocument.completedTime,
                            minDate: $scope.minDate,
                            maxDate: moment().format($scope.format)
                        });
                    }
                    else {
                        vm.supplierdocument.completedTime = $scope.dt;

                        $('input[name="completedTime"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            //startDate: moment(),
                            //minDate: $scope.dt,
                            //maxDate: moment()
                        });
                    }
                });
            }
            init();
        }
    ]);
})();

