﻿(function () {
    appModule.controller('tenant.views.house.transaction.inwarddirectcredit.inwardDcMainForm', [
        '$scope', '$state', '$stateParams', 'abp.services.app.inwardDirectCredit',
        'abp.services.app.location', 'appSession', 'abp.services.app.supplierMaterial', 'abp.services.app.purchaseOrder', 'abp.services.app.material', '$rootScope', 'abp.services.app.dayClose',
        function ($scope, $state, $stateParams, inwarddirectcreditService, locationService, appSession,
            suppliermaterialService, purchaseorderService, materialService, $rootScope, daycloseService) {
            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            vm.inwarddirectcredit = null;
            $rootScope.settings.layout.pageSidebarClosed = true;
            vm.inwarddirectcreditdetail = null;
            $scope.existall = true;
            vm.sno = 0;
            vm.inwarddirectcreditId = $stateParams.id;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;
            vm.loading = true;
            vm.loadingCount = 0;

            vm.permissions = {
                barcodeUsage: abp.auth.hasPermission('Pages.Tenant.House.Master.Material.Barcode'),
                isInwardPurchaseOrderReferenceFromOtherErpRequired: abp.setting.getBoolean("App.House.IsInwardPurchaseOrderReferenceFromOtherErpRequired"),
                isInwardInvoiceNumberReferenceFromOtherErpRequired: abp.setting.getBoolean("App.House.IsInwardInvoiceNumberReferenceFromOtherErpRequired")
            };

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.cancel = function () {
                $rootScope.settings.layout.pageSidebarClosed = false;
                $state.go("tenant.inwarddirectcredit");
            };

            vm.isPurchaseAllowed = appSession.location.isPurchaseAllowed;

            if (vm.isPurchaseAllowed == false) {
                abp.notify.warn(app.localize('PurchaseAllowedErr'));
                vm.cancel();
                return;
            }

            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            $('input[name="dcDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                //                            minDate: $scope.minDate,
                maxDate: moment()
            });

            vm.uilimit = null;

            vm.checkCloseDay = function () {
                vm.loading = true;
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        //if (vm.softmessagenotificationflag)
                        abp.notify.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        //if (vm.messagenotificationflag)
                        //    abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                    }

                    vm.inwarddirectcredit.accountDate = moment(result.accountDate).format($scope.format);
                    $scope.minDate = result.accountDate;

                    $('input[name=accountDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.inwarddirectcredit.accountDate,
                        minDate: $scope.minDate,
                        maxDate: vm.inwarddirectcredit.accountDate
                    });


                }).finally(function (result) {
                    vm.loading = false;
                });
            }

            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

            vm.unitReference = function (selected, data) {
                data.unitRefId = selected.value;
                data.unitRefName = selected.displayText;
                data.changeunitflag = false;
            }


            var requestParams = {
                skipCount: 0,
                maxResultCount: 5, // app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.inwarddirectcreditdetaildata = [];
            vm.tempdetaildata = [];

            vm.save = function (argoption) {
                if (vm.existAllElements() == false)
                    return;

                if ($scope.existall == false)
                    return;

                if (vm.permissions.isInwardInvoiceNumberReferenceFromOtherErpRequired) {
                    if (vm.inwarddirectcredit.invoiceNumberReferenceFromOtherErp == null || vm.inwarddirectcredit.invoiceNumberReferenceFromOtherErp == '') {
                        abp.notify.error(app.localize('InvoiceNumberReferenceFromOtherErp') + ' ?');
                    }
                }
                if (vm.permissions.isInwardPurchaseOrderReferenceFromOtherErpRequired) {
                    if (vm.inwarddirectcredit.purchaseOrderReferenceFromOtherErp == null || vm.inwarddirectcredit.purchaseOrderReferenceFromOtherErp == '') {
                        abp.notify.error(app.localize('PurchaseOrderReferenceFromOtherErp') + ' ?');
                    }
                }

                vm.saving = true;

                vm.inwarddirectcreditdetaildata = [];
                angular.forEach(vm.tempdetaildata, function (value, key) {
                    vm.inwarddirectcreditdetaildata.push({
                        "dcRefId": 0,
                        "barcode": value.barcode,
                        "materialRefId": value.materialRefId,
                        "materialRefName": value.materialRefName,
                        "dcReceivedQty": value.dcReceivedQty,
                        "unitRefId": value.unitRefId,
                        "unitRefName": value.unitRefName,
                        "materialReceivedStatus": 0,
                        "isFractional": value.isFractional,
                        'yieldPercentage': value.yieldPercentage,
                        'yieldExcessShortageQty': value.yieldExcessShortageQty,
                        'yieldMode': value.yieldMode
                    });
                });

                inwarddirectcreditService.createOrUpdateInwardDirectCredit({
                    inwardDirectCredit: vm.inwarddirectcredit,
                    inwardDirectCreditDetail: vm.inwarddirectcreditdetaildata
                }).success(function (result) {
                    vm.transactionId = result.id;

                    abp.notify.info(app.localize('InwardDirectCredit') + ' ' + app.localize('SavedSuccessfully'));
                    msg = '';
                    if (result.adjustmentId > 0) {
                        var adjMsg = app.localize('AdjustmentForDifferenceAdded', result.adjustmentId);
                        abp.notify.warn(adjMsg);
                        msg = msg + ' ' + adjMsg;
                    }
                    abp.message.success(msg);

                    if (argoption == 2) {
                        vm.cancel();
                        vm.saving = false;
                        return;
                    }
                    vm.inwarddirectcreditId = null;
                    vm.tempdetaildata = [];
                    vm.sno = 0;
                    init();
                    $("#selsupplierRefId").focus();

                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.removeRow = function (productIndex) {
                var element = vm.tempdetaildata[productIndex];

                if (vm.tempdetaildata.length > 1) {
                    vm.tempdetaildata.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                }
                else {
                    abp.notify.info(app.localize('MinimumOneDetail'));
                    return;
                }

                vm.materialRefresh();
            }

            vm.addPortion = function () {
                vm.uilimit = 50;
                if (vm.sno > 0) {

                    if (vm.existAllElements() == false)
                        return;

                    var lastelement = vm.tempdetaildata[vm.tempdetaildata.length - 1];

                    if (lastelement.materialRefName == null || lastelement.materialRefName == "") {
                        abp.notify.info(app.localize('MatRequired'));
                        $("#selMaterial1").focus();
                        return;
                    }

                    if (lastelement.dcReceivedQty == 0 || lastelement.dcReceivedQty == null || lastelement.dcReceivedQty == "") {
                        abp.notify.info(app.localize('QtyRequired'));
                        $("#rcdqty").focus();
                        return;
                    }

                    if (lastelement.unitRefId == 0 || lastelement.unitRefId == null) {
                        abp.notify.info(app.localize('UnitRequired'));
                        $("#selUnit").focus();
                        return;
                    }

                    if (!vm.extraMaterialCanBeReceived) {
                        if (vm.tempRefMaterial.length == vm.tempdetaildata.length) {
                            abp.notify.info(app.localize('NoMoreMaterial'));
                            return;
                        }
                    }
                    vm.removeMaterial(lastelement);
                }

                vm.sno = vm.sno + 1;
                vm.tempdetaildata.push({
                    'sno': vm.sno, 'dcRefId': 0, 'materialRefId': '', 'barcode': '',
                    'materialRefName': '', 'dcReceivedQty': '', 'unitRefId': 0, 'unitRefName': '', 'materialReceivedStatus': 0,
                    'id': 0, "isFractional": false, 'yieldPercentage': null, 'changeunitflag': false,
                });

                $("#selMaterial1").focus();
            }
            vm.portionLength = function () {
                return true;
            }

            vm.removeMaterial = function (objRemove) {
                var indexPosition = -1;
                angular.forEach(vm.tempRefMaterial, function (value, key) {
                    if (value.materialRefName == objRemove.materialRefName)
                        indexPosition = key;
                });

                if (indexPosition >= 0)
                    vm.tempRefMaterial.splice(indexPosition, 1);

            }

            vm.materialRefresh = function (objRemove) {
                var indexPosition = -1;

                if (!vm.extraMaterialCanBeReceived) {
                    return;
                }
                {
                    vm.tempRefMaterial = vm.refmaterial;
                }

                angular.forEach(vm.tempdetaildata, function (valueinArray, keyinArray) {
                    indexPosition = -1;
                    var materialtoremove = valueinArray.materialRefName;
                    angular.forEach(vm.tempRefMaterial, function (value, key) {
                        if (value.materialRefName == valueinArray.materialRefName)
                            indexPosition = key;
                    });
                    if (indexPosition >= 0)
                        vm.tempRefMaterial.splice(indexPosition, 1);
                });

            }

            vm.selectMaterialBasedOnBarcode = function (data, index) {
                if (data.barcode == '' || data.barcode == null)
                    return;

                var forLoopFlag = true;
                angular.forEach(vm.tempdetaildata, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(data.barcode, value.barcode) && !angular.equals(data.sno, value.sno)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists') + ' BarCode : ' + data.barcode);
                            if (vm.permissions.barcodeUsage)
                                $("#barcode-" + index).focus();
                            data.barcode = '';
                            forLoopFlag = false;
                        }
                    }
                });

                if (forLoopFlag == false)
                    return;

                vm.uilimit = null;

                var selectedMaterial = null;
                vm.refmaterial.some(function (refdata, refkey) {
                    if (refdata.barcode == data.barcode) {
                        selectedMaterial = refdata;
                        return true;
                    }
                });

                vm.refbarcodelist.some(function (bcdata, bckey) {
                    if (bcdata.barcode == data.barcode) {
                        vm.refmaterial.some(function (matdata, matkey) {
                            if (bcdata.materialRefId == matdata.materialRefId) {
                                selectedMaterial = matdata;
                                selectedMaterial.barcode = data.barcode;
                                return true;
                            }
                        });
                        return true;
                    }
                });


                if (selectedMaterial != null) {



                    $scope.func(data, selectedMaterial);
                }
                else {
                    abp.notify.warn(app.localize('Barcode') + ' ' + data.barcode + ' ' + app.localize('NotExist'));
                    data.barcode = '';
                    $("#barcode-" + index).focus();
                }

            }

            vm.barcodeKeyPress = function (objId, $event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    //alert(objId);
                    $("#rcdqty-" + objId).focus();
                }
            };

            vm.qtyKeyPress = function (objId, $event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    //alert(objId);
                    $("#btnadd-" + objId).focus();
                }
            };


            $scope.func = function (data, val, index) {

                if (val.isQuoteNeededForPurchase == true) {
                    if (vm.inwarddirectcredit.poReferenceCode == null) {
                        abp.message.warn(app.localize("MaterialCanBeReceiptOnlyThroughPO"));
                        data.materialRefId = 0;
                        data.barcode = "";
                        data.materialRefName = "";
                        return;
                    }
                }

                var forLoopFlag = true;
                angular.forEach(vm.tempdetaildata, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialRefName, value.materialRefName) && data.sno != value.sno) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            if (vm.permissions.barcodeUsage)
                                $("#barcode-" + index).focus();
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.materialRefId = 0;
                    data.materialRefName = '';
                    data.barcode = '';
                    return;
                }

                data.materialRefId = val.materialRefId;
                data.materialRefName = val.materialRefName;
                data.isFractional = val.isFractional;
                data.unitRefName = val.uom;
                data.unitRefId = val.unitRefId;
                data.barcode = val.barcode;
                if (data.barcode == null || data.barcode.length == 0)
                    data.barcode = data.materialRefName;
                data.yieldPercentage = val.yieldPercentage;

                fillDropDownUnitBasedOnMaterial(data.materialRefId, data);
            };

            vm.yieldCalculation = function (data) {
                if (data.yieldPercentage != null && data.yieldExcessShortageQty == null) {
                    var diffPercentage = 100 - data.yieldPercentage;
                    if (diffPercentage == 0) {
                        // Do Noting
                    }
                    else if (diffPercentage > 0) {
                        data.yieldMode = app.localize('Shortage');
                        data.yieldExcessShortageQty = parseFloat(data.dcReceivedQty * diffPercentage / 100);
                    }
                    else if (diffPercentage < 0) {
                        data.yieldMode = app.localize('Excess');
                        data.yieldExcessShortageQty = parseFloat(data.dcReceivedQty * (Math.abs(diffPercentage) / 100));
                    }
                    data.yieldExcessShortageQty = parseFloat(data.yieldExcessShortageQty.toFixed(2));
                }
                else {
                    if (data.yieldPercentage == null) {
                        data.yieldExcessShortageQty = 0;
                        data.yieldMode = '';
                    }
                }
            };

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.refunit = [];
                vm.tempRefUnit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    vm.tempRefUnit = vm.refunit;
                    if (vm.refunit.length == 1)
                        data.unitRefId = vm.refunit[0].value;
                });
            }


            vm.existAllElements = function () {
                $scope.existall = true;
                $scope.errmessage = "";

                if (vm.inwarddirectcredit.dcNumberGivenBySupplier == null || vm.inwarddirectcredit.dcNumberGivenBySupplier.length == 0) {
                    $scope.errmessage = $scope.errmessage + ' ' + app.localize('DcNumberErr');
                    $('#dcNumber').focus();
                }

                if (vm.inwarddirectcredit.supplierRefId == null || vm.inwarddirectcredit.supplierRefId == 0) {
                    $scope.errmessage = $scope.errmessage + ' ' + app.localize('SupplierErr');
                    $('#selsupplierRefId').focus();
                }

                if (vm.inwarddirectcredit.dcDate == null || vm.inwarddirectcredit.dcDate == 0) {
                    $scope.errmessage = $scope.errmessage + ' ' + app.localize('DcDateErr');
                    $('#dcDate').focus();
                }

                if (vm.inwarddirectcredit.locationRefId == null || vm.inwarddirectcredit.locationRefId == 0) {
                    $scope.errmessage = $scope.errmessage + ' ' + app.localize('LocationErr');
                    $('#defaultLocation').focus();
                }

                if (vm.inwarddirectcredit.locationRefId == null || vm.inwarddirectcredit.locationRefId == 0) {
                    $scope.errmessage = $scope.errmessage + ' ' + app.localize('LocationErr');
                    $('#defaultLocation').focus();
                }


                if ($scope.errmessage != "") {
                    $scope.existall = false;
                    abp.notify.warn($scope.errmessage, 'Required');
                    return false;
                }
                else
                    return true;

            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.dsetFocus = function (obj) {
                //console.log(obj);
            }

            vm.refsupplier = [];

            function fillDropDownSupplier() {
                vm.loading = true;
                vm.loadingCount++;
                suppliermaterialService.getSupplierForCombobox({}).success(function (result) {
                    vm.refsupplier = result.items;
                    vm.loading = false;
                    vm.loadingCount--;
                });
            }

            vm.refmaterial = [];
            vm.tempRefMaterial = [];

            vm.fillDropDownMaterial = function () {
                if (vm.inwarddirectcredit.supplierRefId == null) {
                    vm.refmaterial = [];
                    vm.tempRefMaterial = [];
                }
                else {
                    vm.loading = true;
                    suppliermaterialService.getMaterialBasedOnSupplierWithPrice({
                        locationRefId: vm.inwarddirectcredit.locationRefId,
                        supplierRefId: vm.inwarddirectcredit.supplierRefId
                    }).success(function (result) {
                        vm.refmaterial = angular.copy(result.items);
                        vm.tempRefMaterial = result.items;
                        vm.loading = false;
                    });
                }
            }

            vm.refbarcodelist = [];
            function fillDropDownBarCodeList() {
                vm.loading = true;
                vm.refbarcodelist = [];
                materialService.getBarcodesForMaterial().success(function (result) {
                    vm.refbarcodelist = result.barcodeList;
                    vm.loading = false;
                });
            }
            fillDropDownBarCodeList();


            vm.refunit = [];
            vm.tempRefUnit = [];

            function fillDropDownUnit() {
                materialService.getUnitForCombobox({}).success(function (result) {
                    vm.refunit = result.items;
                    vm.tempRefUnit = vm.refunit;
                });
            }


            vm.requestParams = {
                locations: '',
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.reflocation = [];
            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;

                });
            }

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };


            vm.refporeferences = [];
            vm.fillDropDownPoReference = function () {

                var tempSupplierRefId = 0;
                if (vm.isUndefinedOrNull(vm.inwarddirectcredit) || vm.isUndefinedOrNull(vm.inwarddirectcredit.supplierRefId)) {

                } else {
                    tempSupplierRefId = vm.inwarddirectcredit.supplierRefId;
                    vm.inwarddirectcredit.poReferenceCode = null;
                }
                vm.loading = true;
                vm.loadingCount++;
                purchaseorderService.getPurchaseReferenceNumber({
                    supplierRefId: tempSupplierRefId,
                    locationRefId: vm.defaultLocationId
                }).success(function (result) {
                    vm.refporeferences = result.items;
                    vm.loading = false;
                    if (vm.refporeferences.length == 1) {

                    }
                }).finally(function () {
                    vm.loadingCount--;
                });
            }

            vm.fillDropDownPoReference();

            vm.getpoid = function (item) {
                //vm.availableQty = parseFloat(dcitem.pendingQty);
                vm.inwarddirectcredit.poRefId = parseInt(item.id);
                return parseInt(item.id);
            };


            vm.extraMaterialCanBeReceived = abp.setting.getBoolean("App.House.ExtraMaterialReceivedInDc");

            vm.fillDataFromPo = function () {
                vm.tempdetaildata = [];
                vm.sno = 0;

                vm.loading = true;
                vm.uilimit = null;

                purchaseorderService.getPoDetailBasedOnPoReferenceCode({ Id: vm.inwarddirectcredit.poRefId }).success(function (result) {

                    vm.inwarddirectcredit.dcNumberGivenBySupplier = result.purchaseOrder.poReferenceCode;
                    if (vm.inwarddirectcredit.supplierRefId == 0 || vm.inwarddirectcredit.supplierRefId == null) {
                        vm.inwarddirectcredit.supplierRefId = result.purchaseOrder.supplierRefId;
                        vm.fillDropDownMaterial();
                    }

                    angular.forEach(result.purchaseOrderDetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        if (value.barcode == null || value.barcode == '') {
                            value.barcode = value.materialRefName;
                        }

                        vm.tempdetaildata.push({
                            "dcRefId": 0,
                            "barcode": value.barcode,
                            "materialRefId": value.materialRefId,
                            "materialRefName": value.materialRefName,
                            "dcReceivedQty": parseFloat(value.qtyOrdered) - parseFloat(value.qtyReceived),
                            "unitRefId": value.unitRefId,
                            "unitRefName": value.unitRefName,
                            "materialReceivedStatus": 0,
                            "isFractional": value.isFractional,
                            'yieldPercentage': value.yieldPercentage,
                            'yieldExcessShortageQty': value.yieldExcessShortageQty,
                            'yieldMode': value.yieldMode,
                            'materialSupplierAliasName': value.materialSupplierAliasName
                        });
                    });


                    if (!vm.extraMaterialCanBeReceived) {
                        var tempmaterial = [];

                        angular.forEach(vm.tempdetaildata, function (value, key) {
                            vm.refmaterial.some(function (matdata, matkey) {
                                if (matdata.materialRefId == value.materialRefId) {
                                    tempmaterial.push(matdata);
                                    return true;
                                }
                            });
                        });

                        vm.tempRefMaterial = tempmaterial;
                    }

                    if (vm.tempdetaildata.length == 0) {
                        vm.addPortion();
                    }
                    vm.loading = false;
                });
            }

            vm.tempdetaildata = [];


            function init() {
                abp.ui.setBusy('#idcDetailForm');
                fillDropDownLocation();
                fillDropDownSupplier();
                fillDropDownUnit();
                vm.errorFlag = true;

                vm.loading = true;
                inwarddirectcreditService.getInwardDirectCreditForEdit({
                    Id: vm.inwarddirectcreditId
                }).success(function (result) {
                    vm.errorFlag = false;
                    vm.inwarddirectcredit = result.inwardDirectCredit;

                    vm.inwarddirectcredit.locationRefId = vm.defaultLocationId;

                    if (result.inwardDirectCredit.id != null) {
                        vm.uilimit = null;
                        vm.tempdetaildata = [];
                        vm.inwarddirectcreditdetail = result.inwardDirectCreditDetail;
                        vm.sno = 0;
                        angular.forEach(result.inwardDirectCreditDetail, function (value, key) {
                            vm.sno = vm.sno + 1;
                            if (value.barcode == null || value.barcode.length == 0)
                                value.barcode = value.materialRefName;
                            vm.tempdetaildata.push({
                                'sno': vm.sno,
                                'dcRefId': value.dcRefId,
                                'barcode': value.barcode,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'dcReceivedQty': value.dcReceivedQty,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'materialReceivedStatus': value.materialReceivedStatus,
                                'id': value.id,
                                "isFractional": value.isFractional,
                                'yieldPercentage': value.yieldPercentage,
                                'yieldExcessShortageQty': value.yieldExcessShortageQty,
                                'yieldMode': value.yieldMode,
                                'changeunitflag': false,
                            });
                        });

                        vm.inwarddirectcredit.dcDate = moment(result.inwardDirectCredit.dcDate).format($scope.format)
                        vm.inwarddirectcredit.accountDate = moment(result.inwardDirectCredit.accountDate).format($scope.format)

                        $scope.minDate = moment(vm.inwarddirectcredit.accountDate).format($scope.format);

                        $('input[name="accountDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: vm.inwarddirectcredit.accountDate,
                            minDate: $scope.minDate,
                            maxDate: moment().format($scope.format)
                        });


                        $('input[name="dcDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: vm.inwarddirectcredit.dcDate,
//                            minDate: $scope.minDate,
                            maxDate: moment()
                        });
                    }
                    else {
                        vm.inwarddirectcredit.dcDate = moment().format($scope.format);
                        //vm.inwarddirectcredit.accountDate = moment().format($scope.format);
                        vm.checkCloseDay();
                        vm.addPortion();
                        vm.fillDropDownPoReference();
                        vm.uilimit = 50;
                    }
                    vm.fillDropDownMaterial();
                }).finally(function () {
                    if (vm.errorFlag == true)
                        vm.cancel();
                    vm.loading = false;
                });
                abp.ui.clearBusy('#idcDetailForm');
            }

            init();
        }
    ]);
})();