﻿(function () {
    appModule.controller('tenant.views.house.transaction.inwarddirectcredit.inwardDcPrint', [
        '$scope', '$state', '$stateParams', 'abp.services.app.inwardDirectCredit',
        'abp.services.app.location', 'appSession', 'abp.services.app.supplierMaterial', 'abp.services.app.purchaseOrder', 'abp.services.app.material', 'abp.services.app.supplier',
        'abp.services.app.user',
        function ($scope, $state, $stateParams, inwarddirectcreditService, locationService, appSession,
            suppliermaterialService, purchaseorderService, materialService, supplierService, userService) {

            var vm = this;
            vm.printmailflag = true;

            vm.connectdecimals = appSession.connectdecimals;
            vm.housedecimals = appSession.housedecimals;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;

            vm.smartprintourexists = abp.setting.getBoolean("App.House.SmartPrintOutExists");


            vm.inwarddirectcreditId = $stateParams.printid;

            vm.headerline = "......................";

            vm.sendMail = function () {
                vm.potext = '';
                vm.loading = true;
                inwarddirectcreditService.sendPruchaseOrderEmail(
                    {
                        inwarddirectcreditId: vm.inwarddirectcreditId,
                        inwarDirectCreditHTML: vm.potext
                    }).success(function (result) {

                        abp.message.success(app.localize('MailedSuccessfully'));
                        abp.notify.success(app.localize('MailedSuccessfully'));
                    }).finally(function () {
                        vm.loading = false;
                    });
            }

            vm.getData = function () {
                inwarddirectcreditService.getInwardDirectCreditForEdit({
                    Id: vm.inwarddirectcreditId,
                    printFlag: true
                }).success(function (result) {
                    vm.inwarddirectcredit = result.inwardDirectCredit;
                    vm.inwardDcPortion = [];

                    vm.sno = 0;
                    vm.subtotalamount = 0;
                    vm.subtaxamount = 0;
                    vm.subnetamount = 0;

                    vm.inwarddirectcreditdetail = result.inwardDirectCreditDetail;

                    angular.forEach(vm.inwarddirectcreditdetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.inwardDcPortion.push({
                            'sno': vm.sno,
                            "dcRefId": value.dcRefId,
                            "materialRefId": value.materialRefId,
                            "materialRefName": value.materialRefName,
                            "dcReceivedQty": value.dcReceivedQty,
                            "unitRefId": value.unitRefId,
                            "unitRefName": value.unitRefName,
                            "materialReceivedStatus": value.materialReceivedStatus,
                            "isFractional": value.isFractional,
                            "supplierMaterialAliasName": value.supplierMaterialAliasName
                        });
                    });

                    //userService.getUserForEdit({
                    //    Id: vm.inwarddirectcredit.creatorUserId
                    //}).success(function (result) {
                    //    vm.user = result.user;
                    //});

                    locationService.getUserInfoBasedOnUserId({
                        id: vm.inwarddirectcredit.creatorUserId
                    }).success(function (result) {
                        vm.user = result;
                    });

                    supplierService.getSupplierForEdit({
                        Id: vm.inwarddirectcredit.supplierRefId
                    }).success(function (result) {
                        vm.supplier = result.supplier;
                    });

                    locationService.getLocationForEdit({
                        Id: vm.inwarddirectcredit.locationRefId
                    }).success(function (result) {
                        vm.billinglocation = result.location;
                        vm.shippinglocation = result.location;
                    });

                });

            }

            vm.getData();

            var person = {
                firstName: "Christophe",
                lastName: "Coenraets",
                blogURL: "http://coenraets.org"
            };

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            var template = "<h1>{{firstName}} {{lastName}}</h1>Blog: {{blogURL}}";

            vm.printIn40Col = function () {
                vm.printmailflag = false;
                $state.go('tenant.inwarddcprint40col', {
                    printid: vm.inwarddirectcreditId
                });
            }

        }]);
})();

