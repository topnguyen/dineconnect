﻿(function () {
    appModule.controller('tenant.views.house.transaction.inwarddirectcredit.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.inwardDirectCredit', 'appSession', 'abp.services.app.setup', 'abp.services.app.location', 'abp.services.app.dayClose',
        function ($scope, $state, $modal, uiGridConstants, inwarddirectcreditService, appSession, setupService, locationService, daycloseService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.id = null;
            vm.defaultLocationId = appSession.user.locationRefId;

            vm.advancedFiltersAreShown = false;
            vm.dateFilterApplied = false;

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };


            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.isPurchaseAllowed = appSession.location.isPurchaseAllowed;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Transaction.InwardDirectCredit.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Transaction.InwardDirectCredit.Edit'),
                importData: abp.auth.hasPermission('Pages.Tenant.Setup.ImportData'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Transaction.InwardDirectCredit.Delete'),
                isInwardPurchaseOrderReferenceFromOtherErpRequired: abp.setting.getBoolean("App.House.IsInwardPurchaseOrderReferenceFromOtherErpRequired"),
                isInwardInvoiceNumberReferenceFromOtherErpRequired: abp.setting.getBoolean("App.House.IsInwardInvoiceNumberReferenceFromOtherErpRequired")
            };


            var requestParams = {
                skipCount: 0,
                maxResultCount: 10, // app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: 10, //app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width:120,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editInwardDirectCredit(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteInwardDirectCredit(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "      <li><a ng-click=\"grid.appScope.viewInwardDcDetail(row.entity)\">" + app.localize("ViewDetail") + "</a></li>" +
                            "      <li><a ng-click=\"grid.appScope.inwardDcPrint(row.entity)\">" + app.localize("Print") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Id'),
                        field: 'id',
                        width: 80
                    },
                    {
                        name: app.localize('Date'),
                        field: 'dcDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                        width: 110
                    },
                    {
                        name: app.localize('Receipt') + ' ' + app.localize('AccountDate'),
                        field: 'accountDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                        width: 110
                    },
                    {
                        name: app.localize('SupplierName'),
                        field: 'supplierName'
                    },
                    {
                        name: app.localize('Reference'),
                        field: 'dcNumberGivenBySupplier',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span> ' + app.localize('DocumentReferenceNumber') + ' : {{row.entity.dcNumberGivenBySupplier}} </span>' +
                            '  <br ng-if=\"grid.appScope.permissions.isInwardPurchaseOrderReferenceFromOtherErpRequired\" >' +
                            '  <span ng-if=\"grid.appScope.permissions.isInwardPurchaseOrderReferenceFromOtherErpRequired\"> ' + app.localize('PurchaseOrderReferenceFromOtherErp') + ' : {{row.entity.purchaseOrderReferenceFromOtherErp}} </span>' +
                            '  <br ng-if=\"grid.appScope.permissions.isInwardPurchaseOrderReferenceFromOtherErpRequired\" >' +
                            '  <span class=\"bold\" ng-if=\"grid.appScope.permissions.isInwardInvoiceNumberReferenceFromOtherErpRequired\"> ' + app.localize('InvoiceNumberReferenceFromOtherErp') + ' : {{row.entity.invoiceNumberReferenceFromOtherErp}} </span>' +
                            '</div>',
                    },
                    {
                        name: app.localize('BillConversion'),
                        field: 'isDcCompleted',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.isDcCompleted" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.isDcCompleted" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                        maxWidth: 80,
                    },
                    {
                        name: app.localize('InvoiceNumber'),
                        field: 'invoiceNumber',
                    },
                    {
                        name: app.localize('InvoiceDate'),
                        field: 'invoiceDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                        width: 110
                    },
                    {
                        name: app.localize('Invoice') + ' ' + app.localize('AccountDate'),
                        field: 'invoiceAccountDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                        width: 110
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.viewInwardDcDetail = function (myObject) {
                openView(myObject.id);
            };

            function openView(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/inwarddirectcredit/detailViewInGrid.cshtml',
                    controller: 'tenant.views.house.transaction.inwarddirectcredit.detailViewInGrid as vm',
                    backdrop: 'static',
                    resolve: {
                        inwarddirectcreditId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.inwardDcPrint = function (myObject) {
                $state.go('tenant.inwarddcprint', {
                    printid: myObject.id
                });
            };

            vm.clear = function () {
                vm.filterText = null;
                vm.dcNumberGivenBySupplier = null;
                vm.dateRangeModel = null;
                vm.dateFilterApplied = false;
                vm.id = null;
                vm.getAll();
            }

            vm.getAll = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var dcNumberGivenBySupplier = null;

                if (vm.dateFilterApplied) {
                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }
                dcNumberGivenBySupplier = vm.dcNumberGivenBySupplier;

                inwarddirectcreditService.getAllWithSupplierNames({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    id : vm.id,
                    supplierName: vm.filterText,
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    dcNumberGivenBySupplier: dcNumberGivenBySupplier
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editInwardDirectCredit = function (myObj) {
                if (myObj.isDcCompleted == false)
                    openCreateOrEditModal(myObj.id);
                else {
                    abp.notify.info(app.localize('AlreadyCompletedDc'));
                }
            };

            vm.createInwardDirectCredit = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteInwardDirectCredit = function (myObject) {
                if (myObject.isDcCompleted == true) {
                    abp.notify.info(app.localize('SelectedReceiptsBillConvertedErr'));
                    return;
                }

                abp.message.confirm(
                    app.localize('DeleteInwardDirectCreditWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            inwarddirectcreditService.deleteInwardDirectCredit({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };



            function openCreateOrEditModal(objId) {
                //var modalInstance = $modal.open({
                //    templateUrl: '~/App/tenant/views/house/transaction/inwarddirectcredit/createOrEditModal.cshtml',
                //    controller: 'tenant.views.house.transaction.inwarddirectcredit.createOrEditModal as vm',
                //    backdrop: 'static',
                //	keyboard: false,
                //    resolve: {
                //        inwarddirectcreditId: function () {
                //            return objId;
                //        }
                //    }
                //});

                //modalInstance.result.then(function (result) {
                //    vm.getAll();
                //});
                //debugger;
                $state.go("tenant.inwarddirectcreditmainform", {
                    id: objId
                });

            }

            vm.exportToExcel = function () {

                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var dcNumberGivenBySupplier = null;

                if (vm.advancedFiltersAreShown) {
                    if (vm.dateFilterApplied) {
                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                    dcNumberGivenBySupplier = vm.dcNumberGivenBySupplier;
                }

                inwarddirectcreditService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    supplierName: vm.filterText,
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    dcNumberGivenBySupplier: dcNumberGivenBySupplier
                }).success(function (result) {
                    app.downloadTempFile(result);
                    vm.loading = false;
                });
            };


            vm.getAll();

            vm.checkCloseDay = function () {
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                    }


                }).finally(function () {
                });
            }

            vm.checkCloseDay();

        }]);
})();

