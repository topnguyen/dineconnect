﻿(function () {
    appModule.controller('tenant.views.house.transaction.inwarddirectcredit.revisedinwardDcPrint', [
       '$scope', '$state', '$stateParams', 'abp.services.app.inwardDirectCredit',
        'abp.services.app.location', 'appSession', 'abp.services.app.supplierMaterial', 'abp.services.app.purchaseOrder', 'abp.services.app.material', 'abp.services.app.supplier',
        function ($scope, $state, $stateParams, inwarddirectcreditService, locationService, appSession,
            suppliermaterialService, purchaseorderService, materialService, supplierService) {

            var vm = this;
            vm.printmailflag = true;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
						});

						vm.connectdecimals = appSession.connectdecimals;
						vm.housedecimals = appSession.housedecimals;


            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;

            

            vm.inwarddirectcreditId = $stateParams.printid;

            vm.headerline = "~~~~~~~~~~~~~~~~~~~~";

            vm.sendMail = function () {
                vm.potext = '';
                vm.loading = true;
                inwarddirectcreditService.sendPruchaseOrderEmail(
                {
                    inwarddirectcreditId: vm.inwarddirectcreditId,
                    inwarDirectCreditHTML: vm.potext
                }).success(function (result) {
                  
                    abp.message.success(app.localize('MailedSuccessfully'));
                    abp.notify.success(app.localize('MailedSuccessfully'));
                }).finally(function () {
                    vm.loading = false;
                    });
            }


            function myFunction() {
                document.getElementById("divprintemail").style.visibility = "hidden";
                myPrint();
            }

            function myPrint() {
                window.print();
                $state.go('tenant.inwarddcprint', {
                    printid: vm.inwarddirectcreditId
                });
            }


            vm.getData = function () {

                inwarddirectcreditService.getPrintData({
                    Id: vm.inwarddirectcreditId
                }).success(function (result) {
                    vm.printresult = result;
                    
                    $("#printidtextHeader").html(result.headerText);
                    $("#printidtextBody").html(result.bodyText);
                    
                    myFunction();
                    });


            }

            vm.getData();


            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

        }]);
})();

