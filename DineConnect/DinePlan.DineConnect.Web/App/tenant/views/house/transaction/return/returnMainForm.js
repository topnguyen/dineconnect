﻿(function () {
    appModule.controller('tenant.views.house.transaction.return.returnMainForm', [
        '$scope', '$state', '$stateParams', 'abp.services.app.return', 'abp.services.app.location',
			'appSession', 'abp.services.app.issue', 'abp.services.app.material', 'abp.services.app.unitConversion', 'abp.services.app.dayClose', 
        function ($scope, $state, $stateParams, returnService, locationService, appSession,
					issueService, materialService, unitconversionService, daycloseService) {
            var vm = this;

            vm.saving = false;
            vm.return = null;
            $scope.existall = true;
            vm.returnId = $stateParams.id;
            vm.sno = 0;
            vm.returnPortions = [];
            vm.returnDetail = [];
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];
            vm.uilimit = null;
            vm.productionUnitExistsFlag = appSession.location.isProductionUnitAllowed;

            vm.refproductionunit = [];
            if (vm.productionUnitExistsFlag == true) {
                vm.refproductionunit = appSession.location.productionUnitList;
            }



            vm.categoryflag = false;
            vm.recipeflag = false;
            vm.issuerefflag = false;

            $('input[name="retDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });


            
            vm.removeUnfilled = function () {
                var listtoremoved = [];
                tempportions = [];

                angular.forEach(vm.returnPortions, function (value, key) {
                    if (value.returnQty == 0 || value.returnQty == null || value.returnQty == "") {
                        listtoremoved.push(key)
                        //vm.returnPortions.splice(key, 1);
                    }
                    else
                    {
                        tempportions.push(value);
                    }
                });

                vm.returnPortions = tempportions;

                vm.sno = vm.returnPortions.length;

                if (vm.returnPortions.length == 0)
                    vm.addPortion();
            }

            vm.save = function () {
                if ($scope.existall == false)
                    return;

                if (vm.productionUnitExistsFlag == true && vm.isUndefinedOrNull(vm.return.productionUnitRefId) == true) {
                    abp.notify.warn(app.localize('ProductionUnitNotSelectErr'));
                    return;
                }

                vm.saving = true;
                
                vm.returnDetail = [];
                if (vm.isUndefinedOrNull(vm.return.recipeRefId) || vm.return.recipeRefId ==0)
                    vm.return.recipeRefId = null;

                var errorFlag = false;
                angular.forEach(vm.returnPortions, function (value, key) {
                    if (value.returnQty == 0) {
                        errorFlag = true;
                        abp.notify.warn(app.localize('QtyErr'));
                        return;
                    }
                    if (vm.issuerefflag)
                    {
                        if (!vm.checkQty(value.returnQty, value)) {
                            errorFlag = true;
                            return;
                        }
                         
                    }
                    vm.returnDetail.push({
                        'sno': value.sno,
                        'recipeRefId': vm.return.recipeRefId,
                        'materialRefId': value.materialRefId,
                        'returnQty': value.returnQty,
                        'remarks': value.remarks,
                        'unitRefId': value.unitRefId,
                        'changeunitflag': false,
                    });
                });

                if (errorFlag == true) {
                    vm.saving = false;
                    return;
                }

                returnService.createOrUpdateReturn({
                    return: vm.return,
                    returnDetail:vm.returnDetail
                }).success(function (result) {
                    abp.notify.info('\' Return \'' + app.localize('SavedSuccessfully'));
                    abp.message.success(app.localize('ReturnSuccessMessage', result.id));
                    vm.cancel();
                    $("#defaultLocation").focus();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function resetField() {
                vm.returnPortions = [];
                vm.returnDetail = [];
                vm.return.returnPersonId = '';
                vm.return.tokenRefNumber = '';
                vm.return.recipeRefId = null;
                vm.return.returnDate = '';
                vm.return.locationRefId = '';
                vm.sno = 0;
                $scope.checked = false;
                vm.categoryflag = false;
                vm.recipeflag = false;
                vm.issuerefflag = false;
                vm.addPortion();
            }

            vm.addPortion = function () {

                if (vm.sno > 0) {
                    var errorFlag = false;

                    if (vm.productionUnitExistsFlag == true && vm.isUndefinedOrNull(vm.return.productionUnitRefId) == true) {
                        abp.notify.warn(app.localize('ProductionUnitNotSelectErr'));
                        return;
                    }

                    if (vm.refmaterial.length == vm.returnPortions.length) {
                        errorFlag = true;
                        abp.notify.info(app.localize('AllTheMaterialAdded'));
                        return;
                    }

                    if (vm.returnPortions.length <= 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }
                    else {
                        if (vm.isUndefinedOrNull(vm.returnPortions[0].materialRefId) || vm.returnPortions[0].materialRefId == 0) {
                            errorFlag = true;
                            abp.notify.info(app.localize('MinimumOneDetail'));
                            return;
                        }
                        if (vm.isUndefinedOrNull(vm.returnPortions[0].returnQty) || vm.returnPortions[0].returnQty == 0) {
                            errorFlag = true;
                            abp.notify.info(app.localize('InvalidReturnQty'));
                            return;
                        }
                    }

                    if (errorFlag) {
                        vm.saving = false;
                        vm.loading = false;
                        $scope.existall = false;
                    }

                }

                angular.forEach(vm.returnPortions, function (value, key) {
                    vm.returnPortions[key].changeunitflag = false;
                });


                vm.sno = vm.sno + 1;
                vm.returnPortions.push({
                    'sno': vm.sno, 'materialRefId': 0, 'materialRefName': '', 'currentInHand': '', 'returnQty': '', 'remarks': '', 'changeunitflag': false,
                });
            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex) {
                if (vm.returnPortions.length > 1) {
                    vm.returnPortions.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                }
                else {
                    vm.returnPortions.splice(productIndex, 1);
                    vm.sno = 0;
                    vm.addPortion();
                    abp.notify.info(app.localize('MinimumOneDetail'));
                    return;
                }
            }

            vm.existall = function () {

                if (vm.return.id == null) {
                    vm.existall = false;
                    return;
                }

                returnService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.return.id,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.return.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.return.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.setMaxTokenNumber = function (val) {
                returnService.getMaxTokenNumber(
                    moment(val).format($scope.format)
                ).success(function (result) {
                    vm.return.tokenRefNumber = result;
                });
            };

            $scope.funcReturnDate = function (selDt) {
                if (!vm.isUndefinedOrNull(selDt))
                    vm.setMaxTokenNumber(selDt);
            }


            $scope.func = function (data, val, obj) {

                if (vm.showErrorProductionUnitNotSelected() == false)
                {
                    data.materialRefId = 0;
                    data.materialRefName = '';
                    return;
                }
                var forLoopFlag = true;

                angular.forEach(vm.returnPortions, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialRefName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.materialRefId = 0;
                    data.materialRefName = '';
                    return;
                }
                data.materialRefName = val.materialRefName;
                data.defaultUnitId = val.defaultUnitId;
                data.defaultUnit = val.defaultUnitName;
                data.currentInHand = val.currentInHand;
                data.unitRefName = val.issueUnitName;
                data.unitRefId = val.issueUnitId;

            };

            $scope.RecipeFunc = function (data) {
                var selValue = data.value;
                vm.fillMaterialBasedOnRecipt(selValue);
            }

            $scope.IssueFunc = function (data) {
                var selValue = data.id;
                vm.getDetailMaterial();
            }

            vm.filterMaterial = function () {
                vm.showErrorProductionUnitNotSelected();
                if (vm.productionUnitExistsFlag && vm.return.productionUnitRefId == null)
                {
                    vm.issuerefflag = false;
                    return;
                }

                if ($('#recipeFlag').is(":checked")) {
                    // it is checked
                    vm.fillMaterialBasedOnRecipt(vm.return.recipeRefId);
                }
                else
                {
                    vm.fillMaterialBasedOnRecipt(null);
                    vm.return.recipeRefId = null;
                }
            }

            vm.cancel = function () {
                //$modalInstance.dismiss();
                $state.go("tenant.return");
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.reflocation = [];

            function fillDropDownLocation()
            {
                abp.ui.setBusy('#divLocation');
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;

                });
                abp.ui.clearBusy('#divLocation');
            }



            vm.refRecipe = [];
            function fillDropDownRecipe() {
                materialService.getMaterialRecipeTypesForCombobox({}).success(function (result) {
                    vm.refRecipe = result.items;
                });
            }

            vm.refmaterial = [];
            vm.tempRefMaterial = [];

            vm.fillMaterialBasedOnRecipt = function (selValue) {
                issueService.getMaterialForGivenRecipeCombobox({
                    locationRefId: vm.defaultLocationId,
                    recipeRefId: selValue
                }).success(function (result) {
                    vm.refmaterial = result;
                    vm.tempRefMaterial = result;
                });
            }

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.getDetailMaterial = function () {
                if (vm.recipeflag == true)
                {
                    issueService.getIssueForRecipe({
                        locationRefId : vm.defaultLocationId,
                        recipeRefId: vm.return.recipeRefId,
                        requestQtyforProduction: 0
                    }).success(function (result) {
                        vm.returnPortions = [];
                        vm.sno = 0;
                        angular.forEach(result.issueDetail, function (value, key) {
                            vm.sno = vm.sno + 1;
                            vm.returnPortions.push({
                                'sno': vm.sno,
                                'recipeRefId': vm.return.recipeRefId,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'returnQty': "",
                                'currentInHand': value.currentInHand,
                                'remarks': "",
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'defaultUnitId': value.defaultUnitId,
                                'defaultUnitName' : value.defaultUnitName,
                                'changeunitflag': false,
                            })
                        });
                    });
                }
            else if (vm.categoryflag==true)
            {
                if (vm.return.materialGroupCategoryRefId == null || vm.return.materialGroupCategoryRefId == 0)
                {
                    abp.message.warn(app.localize('CategorySelectErr'));
                    return;
                }

                materialService.getIssueForCategory({
                    materialGroupCategoryRefId: vm.return.materialGroupCategoryRefId,
                    locationRefId: vm.defaultLocationId
                }).success(function (result) {
                    vm.returnPortions = [];
                    vm.sno = 0;
                    angular.forEach(result.issueDetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.returnPortions.push({
                            'sno': vm.sno,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'returnQty': value.issueQty,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'defaultUnitId': value.defaultUnitId,
                            'defaultUnitName': value.defaultUnitName,
                            'currentInHand': value.currentInHand,
                            'remarks': "",
                            'changeunitflag': false,
                        })
                    });
                });
            }
            else if (vm.issuerefflag==true)
            {
                issueService.getIssueForEdit({
                    id: vm.return.issueRefId
                }).success(function (result) {
                    vm.returnPortions = [];
                    vm.sno = 0;
                    angular.forEach(result.issueDetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.returnPortions.push({
                            'sno': vm.sno,
                            'recipeRefId': vm.return.recipeRefId,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'issueQty' : value.issueQty,
                            'returnQty': "",
                            'currentInHand': value.currentInHand,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'defaultUnitId': value.defaultUnitId,
                            'defaultUnitName': value.defaultUnitName,
                            'remarks': "",
                            'changeunitflag': false,
                        })
                    });
                });
            }
            else 
            {
                vm.returnPortions = [];
            }

            }

            vm.checkQty = function (enteredQty, data) {
                if (vm.issuerefflag == false) {
                    return true;
                }

                if (parseFloat(enteredQty) == 0 || enteredQty == null || enteredQty == "") {
                    return true;
                }

                //@@Pending UOM Handing
                if ((parseFloat(enteredQty) > parseFloat(data.issueQty)))
                {
                    abp.message.warn(app.localize("ReturnQtyIssueQtyErr", enteredQty, data.issueQty.toFixed(2),  data.materialRefName), "Error");
                    data.productionQty = '';
                    return false;
                }
                else
                {
                    return true;
                }
            }

            vm.refmaterialgroupcategory = [];
            function fillDropDownMaterialGroupCategory() {
                vm.loading = true;
                materialService.getMaterialGroupCategoryForCombobox({}).success(function (result) {
                    vm.refmaterialgroupcategory = result.items;
                    vm.loading = false;
                });
            }

            vm.refissues = [];
            vm.fillDropDownIssueReferences = function () {
                vm.loading = true;
                issueService.getIssueIds({
                    dt: vm.return.returnDate,
                    productionUnitRefId : vm.return.productionUnitRefId
                }).success(function (result) {
                    vm.refissues = result.items;
                    vm.loading = false;
                });
            }

            vm.categoryselect = function () {

                if (vm.categoryflag == true) {
                    vm.recipeflag = false;
                    vm.issuerefflag = false;
                }
                else {
                    vm.recipeflag = false;
                    vm.issue.materialGroupCategoryRefId = 0;
                }

            }

            vm.checkCloseDay = function () {
							daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {

                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                    }

                    vm.return.returnDate = moment(result.accountDate).format($scope.format);

                    $('input[name="retDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: false,
                        startDate: vm.return.returnDate,
                        minDate: vm.return.returnDate,
                        maxDate: vm.return.returnDate
                    });

                }).finally(function (result) {

                });
            }

            function init() {
                abp.ui.setBusy('#returnForm');
                fillDropDownLocation();
                fillDropDownRecipe();
                fillDropDownMaterialGroupCategory();
                
                vm.fillMaterialBasedOnRecipt(null);
                

                returnService.getReturnForEdit({
                    Id: vm.returnId
                }).success(function (result)
                {
                    vm.return = result.return;
                    vm.return.locationRefId = vm.defaultLocationId;

                    if (result.return.id != null)
                    {
                        vm.uilimit = null;
                        vm.returnDetail = result.returnDetail;
                        vm.return.recipeRefId = result.returnDetail[0].recipeRefId;

                        if (vm.return.materialGroupCategoryRefId !=null) {
                            vm.recipeflag = false;
                            vm.categoryflag = true;
                            vm.issuerefflag = false;
                        }
                        else if (vm.return.issueRefId!=null)
                        {
                            vm.recipeflag = false;
                            vm.categoryflag = false;
                            vm.issuerefflag = true;

                        }
                        else  {
                            vm.recipeflag = true;
                            vm.categoryflag = false;
                        }

                        vm.return.returnDate = moment(result.return.returnDate).format($scope.format);
                        $scope.minDate = vm.return.returnDate;

                        $('input[name="retDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: vm.return.returnDate,
                            minDate: $scope.minDate,
                            maxDate: moment().format($scope.format)
                        });

                        angular.forEach(result.returnDetail, function (value, key) {
                            vm.returnPortions.push({
                                'sno': value.sno,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'returnQty': value.returnQty,
                                'remarks': value.remarks,
                                'defaultUnit': value.defaultUnit,
                                'currentInHand': value.currentInHand,
                                'unitRefId': value.unitRefId,
                                'unitRefName' : value.unitRefName
                            })
                        });
                    }
                    else
                    {
                        vm.return.returnDate = moment().format($scope.format);
                        vm.addPortion();
                        vm.checkCloseDay();
                        vm.uilimit = 20;
                    }

                    if (vm.productionUnitExistsFlag==false)
                        vm.fillDropDownIssueReferences();

                });

            }

            init();

            vm.showErrorProductionUnitNotSelected = function () {
                if (vm.productionUnitExistsFlag == true && vm.isUndefinedOrNull(vm.return.productionUnitRefId) == true) {
                    abp.notify.warn(app.localize('ProductionUnitNotSelectErr'));
                    return false;
                }
                return true;
            }

            vm.refconversionunit = [];
            vm.fillUnitConversion = function () {
                unitconversionService.getAll({
                }).success(function (result) {
                    vm.refconversionunit = result.items;
                });
            }

            vm.fillUnitConversion();

            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

            vm.unitReference = function (selected, data) {
                data.unitRefId = selected.value;
                data.unitRefName = selected.displayText;
                data.changeunitflag = false;
            }


        }
    ]);
})();

