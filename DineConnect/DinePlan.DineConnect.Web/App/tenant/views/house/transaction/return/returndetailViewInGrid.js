﻿
(function () {
    appModule.controller('tenant.views.house.transaction.return.returndetailViewInGrid', [
        '$scope', '$uibModalInstance', 'uiGridConstants', 'abp.services.app.return', 'returnId', 'appSession',
        function ($scope, $modalInstance, uiGridConstants, returnService, returnId, appSession) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;

            vm.tokennumber = null;
            vm.returndate = null;


            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('MaterialName'),
                        field: 'materialRefName',
                        width : 250
                    },
                    {
                        name: app.localize('ReturnQty'),
                        field: 'returnQty',
                        cellClass: 'ui-ralign',
                        cellFilter: 'number: 2'
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.returnreport = null;

            vm.getAll = function() {
                vm.loading = true;
                returnService.getReturnForEdit({
                    Id: returnId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.returnDetail.length;
                    vm.userGridOptions.data = result.returnDetail;
                    if (result.returnDetail.length > 0) {
                        vm.tokennumber = result.return.tokenRefNumber;
                        vm.returndate = result.return.returnDate;
                        vm.materialRefName = result.returnDetail.materialRefName;
                        vm.returnQty = result.returnDetail.returnQty;
                        vm.returnMode = result.returnDetail.returnMode;
                        vm.returnApprovedRemarks = result.returnDetail.returnApprovedRemarks;
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.hide = function() {
                vm.returnreport = "";
            };

            vm.getAll();
        }]);
})();