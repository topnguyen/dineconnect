﻿
(function () {
    appModule.controller('tenant.views.house.transaction.return.index', [
			'$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.return', 'appSession', 'abp.services.app.location', 'abp.services.app.dayClose', 
			function ($scope, $state, $modal, uiGridConstants, returnService, appSession, locationService, daycloseService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.defaultLocationId = appSession.user.locationRefId;
            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }
            vm.isProductionAllowed = appSession.location.isProductionAllowed;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Transaction.Return.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Transaction.Return.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Transaction.Return.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.advancedFiltersAreShown = false;
            vm.dateFilterApplied = false;

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.userGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
					{
						name: app.localize('Actions'),
						enableSorting: false,
						width: 120,

						cellTemplate:
						   "<div class=\"ui-grid-cell-contents\">" +
							    "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
							   "    <ul uib-dropdown-menu>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editReturn(row.entity)\">" + app.localize("Edit") + "</a></li>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteReturn(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                               "      <li><a ng-if=\"grid.appScope.permissions.create\" ng-click=\"grid.appScope.viewReturnDetail(row.entity)\">" + app.localize("ViewDetail") + "</a></li>" +
							   "    </ul>" +
							   "  </div>" +
							   "</div>"
					},
                    {
                        name: app.localize('Id'),
                        field: 'id'
                    },
                    {
                        name : app.localize('Reference'),
                        field : 'tokenRefNumber'
                    },
                    {
                         name: app.localize('ReturnDate'),
                         field: 'returnDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD \'',

                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.viewReturnDetail = function (myObject) {
                openView(myObject.id);
            };

            function openView(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/return/returndetailViewInGrid.cshtml',
                    controller: 'tenant.views.house.transaction.return.returndetailViewInGrid as vm',
                    backdrop: 'static',
                    resolve: {
                        returnId: function () {
                            return objId;
                        }
                    }
                });
            }

            vm.clear = function () {
                vm.filterText = null;
                vm.dateRangeModel = null;
                vm.dateFilterApplied = false;
                vm.getAll();
            }

            vm.getAll = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                //var requestSlipNumber = null;

                if (vm.advancedFiltersAreShown) {
                    if (vm.dateFilterApplied) {
                       
                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                   // requestSlipNumber = vm.requestSlipNumber;
                }

                returnService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    //requestSlipNumber: requestSlipNumber,
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editReturn = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createReturn = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteReturn = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteReturnWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            returnService.deleteReturn({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                //var modalInstance = $modal.open({
                //    templateUrl: '~/App/tenant/views/house/transaction/return/createOrEditModal.cshtml',
                //    controller: 'tenant.views.house.transaction.return.createOrEditModal as vm',
                //    backdrop: 'static',
				//	keyboard: false,
                //    resolve: {
                //        returnId: function () {
                //            return objId;
                //        }
                //    }
                //});

                //modalInstance.result.then(function (result) {
                //    vm.getAll();
                //});

                $state.go("tenant.returndetail", {
                    id:objId
                });
            }

            vm.exportToExcel = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
               // var requestSlipNumber = null;

                if (vm.advancedFiltersAreShown) {
                    if (vm.dateFilterApplied) {

                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                    //requestSlipNumber = vm.requestSlipNumber;
                }
                returnService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                   // requestSlipNumber: requestSlipNumber,
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            };


            vm.getAll();


            vm.checkCloseDay = function () {
							daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {

                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                    }
                

                }).finally(function () {
                });
            }

            vm.checkCloseDay();

            function init() {
                vm.productionUnitExistsFlag = appSession.location.isProductionUnitAllowed;
                if (vm.productionUnitExistsFlag == true) {
                    vm.columnPrdUnitRefName =
                        {
                            name: app.localize('ProductionUnit'),
                            field: 'productionUnitRefName'
                        };
                    vm.userGridOptions.columnDefs.splice(2, 0, vm.columnPrdUnitRefName);
                }
            };

            init();

        }]);
})();

