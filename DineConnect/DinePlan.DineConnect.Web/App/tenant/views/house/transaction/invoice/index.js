﻿
(function () {
    appModule.controller('tenant.views.house.transaction.invoice.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.invoice', 'abp.services.app.setup', 'appSession', 'abp.services.app.location', 'abp.services.app.dayClose',
        function ($scope, $state, $modal, uiGridConstants, invoiceService, setupService, appSession, locationService, daycloseService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.id = null;

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.defaultLocationId = appSession.user.locationRefId;

            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.advancedFiltersAreShown = false;
            vm.dateFilterApplied = false;

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.cancel = function () {
                $rootScope.settings.layout.pageSidebarClosed = false;
                $state.go('tenant.invoice');
            };

            console.log(appSession.location);
            vm.isPurchaseAllowed = appSession.location.isPurchaseAllowed;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Transaction.Invoice.Create'),
                //edit: abp.auth.hasPermission('Pages.Tenant.House.Transaction.Invoice.Edit'),
                importData: abp.auth.hasPermission('Pages.Tenant.Setup.ImportData'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Transaction.Invoice.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editInvoice(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteInvoice(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.create\" ng-click=\"grid.appScope.printInvoice(row.entity)\">" + app.localize("Print") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Id'),
                        field: 'id',
                        width : 80
                    },
                    {
                        name: app.localize('Supplier'),
                        field: 'supplierRefName',
                        minWidth : 250
                    },
                    {
                        name: app.localize('InvoiceDate'),
                        field: 'invoiceDate',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD \''
                    },
                    {
                        name: app.localize('InvoiceNumber'),
                        field: 'invoiceNumber'
                    },
                    {
                        name: app.localize('Amount'),
                        field: 'invoiceAmount',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('AccountDate'),
                        field: 'accountDate',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD \''
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.printInvoice = function (myObject) {
                $state.go('tenant.invoiceprintdetail', {
                    printid: myObject.id
                });
            };

            vm.importSampleInvoice = function () {
                abp.message.confirm(
                    app.localize('ImportInwardWarning'),
                    function (isConfirmed) {
                        vm.loading = true;
                        if (isConfirmed) {
                            setupService.seedSampleInvoice()
                                .success(function () {
                                    abp.notify.success(app.localize('SuccessfullyImported'));
                                    vm.getAll();
                                }).finally(function () {
                                    vm.loading = false;
                                });
                        }
                    });
            };

            vm.clear = function () {
                vm.filterText = null;
                vm.invoiceNumber = null;
                vm.dateRangeModel = null;
                vm.dateFilterApplied = false;
                vm.id = null;
                vm.getAll();
            }

            vm.getAll = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var invoiceNumber = null;

                if (vm.dateFilterApplied) {
                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }
                invoiceNumber = vm.invoiceNumber;

                invoiceService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    id : vm.id,
                    supplierName: vm.filterText,
                    locationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    invoiceNumber: invoiceNumber
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editInvoice = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createInvoice = function () {
                openCreateOrEditModal(null, null);
            };

            vm.createDirectInvoice = function () {
                openCreateOrEditModal(null, 'Direct');
            };

            vm.createInvoiceFromPo = function () {
                $state.go("tenant.invoicedetailfromPo", {
                    id: null,
                    invoiceType: 'PoToInvoice'
                });
            }

            vm.deleteInvoice = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteInvoiceWarning', app.localize('Id') + ' ' + myObject.id + ' , ' + app.localize('InvoiceReference') + ' ' + myObject.invoiceNumber),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            invoiceService.deleteInvoice({
                                id: myObject.id
                            }).then(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId, invoiceType) {
                $state.go("tenant.invoicedetailfromPo", {
                    id: objId,
                    invoiceType: invoiceType
                });
            }

            vm.exportToExcel = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var invoiceNumber = null;

                if (vm.advancedFiltersAreShown) {
                    if (vm.dateFilterApplied) {
                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                    invoiceNumber = vm.invoiceNumber;
                }

                invoiceService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    supplierName: vm.filterText,
                    locationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    invoiceNumber: invoiceNumber
                })
                    .then(function (result) {
                        app.downloadTempFile(result.data);
                        vm.loading = false;
                    });
            };


            vm.getAll();



            vm.checkCloseDay = function () {
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.doesDayCloseRunInBackGround == true) {
                        abp.notify.error(app.localize("StillBackGroundProcessing", moment(result.accountDate).format($scope.format)));
                        abp.message.error(app.localize("StillBackGroundProcessing", moment(result.accountDate).format($scope.format)));
                        return;
                    }

                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        //if (vm.messagenotificationflag)
                        //    abp.message.warn(app.localize("LastExecutionDate", moment(result.data.accountDate).format($scope.format)));
                    }
                }).finally(function () {
                });
            }

            vm.checkCloseDay();



        }]);
})();

