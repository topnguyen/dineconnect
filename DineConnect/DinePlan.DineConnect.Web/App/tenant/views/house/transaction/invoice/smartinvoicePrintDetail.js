﻿(function () {
    appModule.controller('tenant.views.house.transaction.invoice.smartinvoicePrintDetail', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.invoice', 'appSession',
        'abp.services.app.supplier', 'abp.services.app.location',
        function ($scope, $filter, $state, $stateParams, $modal, uiGridConstants, invoiceService, appSession,
            supplierService, locationService) {

            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.connectdecimals = appSession.connectdecimals;
            vm.housedecimals = appSession.housedecimals;


            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            //vm.defaultLocationId = appSession.user.locationRefId;
            vm.smartprintourexists = abp.setting.getBoolean("App.House.SmartPrintOutExists");


            vm.invoiceId = $stateParams.printid;

            vm.headerline = "~~~~~~~~~~~~~~~~~~~~";

            vm.getData = function () {

                invoiceService.getPrintData({
                    Id: vm.invoiceId
                }).success(function (result) {
                    vm.printresult = result;

                    $("#printidtextHeaderBold").html(result.boldHeaderText);
                    $("#printidtextBodyBold").html(result.boldBodyText);
                    $("#printidtextHeader").html(result.headerText);
                    $("#printidtextBody").html(result.bodyText);


                    myFunction();
                });


            }

            function myFunction() {
                //document.getElementById("divprintemail").style.visibility = "hidden";
                myPrint();
            }

            function myPrint() {
                window.print();
                //$state.go('tenant.invoiceprintdetail', {
                //    printid: vm.invoiceId
                //});
            }



            vm.getData();

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

        }]);
})();

