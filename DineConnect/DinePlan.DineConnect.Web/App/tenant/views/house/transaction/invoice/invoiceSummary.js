﻿(function () {
    appModule.controller('tenant.views.house.transaction.invoice.invoiceSummary', [
        '$scope','$state', '$uibModal', 'uiGridConstants', 'abp.services.app.invoice', 'abp.services.app.setup', 'appSession', 'abp.services.app.location', 'abp.services.app.supplier',
        'abp.services.app.supplierMaterial',
        function ($scope,$state, $modal, uiGridConstants, invoiceService, setupService, appSession, locationService, supplierService, suppliermaterialService) {
            var vm = this;


            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.outputDtoList = null;
            vm.selectedLocations = [];
            vm.alllocationflag = false;

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.defaultLocationId = appSession.user.locationRefId;

            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.isPurchaseAllowed = appSession.location.isPurchaseAllowed;

            vm.requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize('Supplier'),
                        field: 'supplierRefName'
                    },
                    {
                        name: app.localize('InvoiceDate'),
                        field: 'invoiceDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD \''
                    },
                    {
                        name: app.localize('Number'),
                        field: 'invoiceNumber'
                    },
                    {
                        name: app.localize('AmountWOTax'),
                        field: 'amountWithoutTax',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Tax'),
                        field: 'taxAmount',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('NetAmount'),
                        field: 'invoiceAmount',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('DirectInvoice'),
                        field: 'isDirectInvoice',
                        cellTemplate:
                         '<div class=\"ui-grid-cell-contents\">' +
                         '  <span ng-show="row.entity.isDirectInvoice" class="label label-success">' + app.localize('Yes') + '</span>' +
                         '  <span ng-show="!row.entity.isDirectInvoice" class="label label-danger">' + app.localize('No') + '</span>' +
                         '</div>'
                    },
                    {
                        name: app.localize('AccountDate'),
                        field: 'accountDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD \''
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.suppliers = [];
            vm.getSuppliers = function () {
                supplierService.getAll({
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting
                }).success(function (result) {
                    vm.suppliers = $.parseJSON(JSON.stringify(result.items));

                }).finally(function (result) {
                });
            };

            vm.printInvoice = function (myObject) {
                $state.go('tenant.invoiceprintdetail', {
                    printid: myObject.id
                });
            };

            vm.importSampleInvoice = function () {
                abp.message.confirm(
                    app.localize('ImportInwardWarning'),
                    function (isConfirmed) {
                        vm.loading = true;
                        if (isConfirmed) {
                            setupService.seedSampleInvoice()
                            .success(function () {
                                abp.notify.success(app.localize('SuccessfullyImported'));
                                vm.getAll();
                            }).finally(function () {
                                vm.loading = false;
                            });
                        }
                    });
            };

            vm.getAll = function () {
                
                if (vm.alllocationflag)
                    vm.selectedLocations = vm.locations;
                if (vm.selectedLocations.length === 0)
                {
                    abp.notify.warn(app.location('LocationErr'));
                    return;
                }
                vm.loading = true;
                invoiceService.getInvoiceSearch({
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    supplierRefId:vm.supplierRefId,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    taxShowFlag: true,
                    locations : vm.selectedLocations
                }).success(function(result){
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                    vm.outputDtoList = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.refsupplier = [];

            function fillDropDownSupplier() {
                vm.loading = true;
                suppliermaterialService.getSupplierForCombobox({}).success(function (result) {
                    vm.refsupplier = result.items;
                    vm.loading = false;
                });
            }

      
            //@@Pending to Make Excel
            vm.exportToExcel = function () {
                if (vm.alllocationflag)
                    vm.selectedLocations = vm.locations;
                if (vm.selectedLocations.length == 0) {
                    abp.notify.warn(app.location('LocationErr'));
                    return;
                }
                vm.loading = true;

                invoiceService.getInvoiceSummaryAllToExcel({
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    supplierRefId: vm.supplierRefId,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    taxShowFlag: true,
                    locations : vm.selectedLocations
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            };

            fillDropDownSupplier();
            
            vm.getSuppliers();

            vm.locations = [];

            vm.getLocations = function () {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));

                    vm.locations.some(function (refdata, refkey) {
                        if (refdata.id == vm.defaultLocationId) {
                            vm.selectedLocations.push(refdata);
                            return true;
                        }
                    });

                }).finally(function (result) {

                });
            };

            vm.getLocations();

        }
    ]);
})();