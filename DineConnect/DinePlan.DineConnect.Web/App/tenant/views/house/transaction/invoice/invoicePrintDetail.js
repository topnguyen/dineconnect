﻿(function () {
    appModule.controller('tenant.views.house.transaction.invoice.invoicePrintDetail', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.invoice', 'appSession',
        'abp.services.app.supplier', 'abp.services.app.location',
        function ($scope, $filter, $state, $stateParams, $modal, uiGridConstants, invoiceService, appSession,
            supplierService, locationService) {

            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.connectdecimals = appSession.connectdecimals;
            vm.housedecimals = appSession.housedecimals;

            vm.permissions = {
                printMaterialCode: abp.auth.hasPermission('Pages.Tenant.House.Master.Material.PrintMaterialCode'),
            };


            //vm.defaultLocationId = appSession.user.locationRefId;
            vm.smartprintourexists = abp.setting.getBoolean("App.House.SmartPrintOutExists");


            vm.invoiceId = $stateParams.printid;

            vm.headerline = "~~~~~~~~~~~~~~~~~~~~";

            vm.getData = function () {
                invoiceService.getInvoiceForEdit({
                    id: vm.invoiceId
                }).success(function (result) {
                    vm.invoice = result.invoice;
                    //vm.invoicedetail = result.invoiceDetail;

                    vm.invoicePortions = [];

                    vm.sno = 0;
                    vm.subtotalamount = 0;
                    vm.subtaxamount = 0;
                    vm.subnetamount = 0;

                    vm.invoicedetail = result.invoiceDetail;
                    vm.invoicedirectcreditlink = result.invoiceDirectCreditLink;
                    vm.refdctranid = result.invoiceDirectCreditLink;

                    angular.forEach(vm.invoicedetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.subtotalamount = vm.subtotalamount + value.totalAmount;
                        vm.subtaxamount = vm.subtaxamount + value.taxAmount;
                        vm.subnetamount = vm.subnetamount + value.netAmount;
                        vm.invoicePortions.push({
                            'sno': value.sno,
                            'dcTranid': value.dcTranId,
                            'materialRefId': value.materialRefId,
                            'materialPetName': value.materialPetName,
                            'materialRefName': value.materialRefName,
                            'totalQty': value.totalQty,
                            'price': value.price,
                            'discountFlag': value.discountFlag,
                            'discountPercentage': value.discountPercentage,
                            'discountAmount': parseFloat(vm.isUndefinedOrNull(value.discountAmount) ? 0 : value.discountAmount),
                            'totalAmount': parseFloat(parseFloat(value.totalAmount).toFixed(2)),
                            'taxAmount': parseFloat(parseFloat(value.taxAmount).toFixed(2)),
                            'netAmount': parseFloat(parseFloat(value.netAmount).toFixed(2)),
                            'remarks': value.remarks,
                            'taxForMaterial': value.taxForMaterial,
                            'uom': value.uom,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'supplierMaterialAliasName': value.supplierMaterialAliasName
                        });
                    });

                    supplierService.getSupplierForEdit({
                        Id: vm.invoice.supplierRefId
                    }).success(function (result) {
                        vm.supplier = result.supplier;
                        vm.suppliergstinformation = result.suppliergstinformation;
                    });

                    locationService.getLocationForEdit(
                        {
                            id: vm.invoice.locationRefId
                        }).success(function (result) {
                            vm.location = result.location;
                        });
                });
            }

            vm.getData();

            var person = {
                firstName: "Christophe",
                lastName: "Coenraets",
                blogURL: "http://coenraets.org"
            };

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.printIn40Col = function () {
                vm.printmailflag = false;
                $state.go('tenant.invoiceprintdetail40col', {
                    printid: vm.invoiceId
                });
            }


            var template = "<h1>{{firstName}} {{lastName}}</h1>Blog: {{blogURL}}";
            //var html = Mustache.to_html(template, person);
            //$('#sampleArea').html(html);


        }]);
})();

