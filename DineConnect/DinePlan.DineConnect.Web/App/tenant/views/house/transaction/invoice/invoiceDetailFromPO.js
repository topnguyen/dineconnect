﻿(function () {
    appModule.controller('tenant.views.house.transaction.invoice.invoiceDetailfromPo', [
        '$scope', '$filter', '$state', '$stateParams', 'abp.services.app.invoice',
       'appSession', 'abp.services.app.supplierMaterial', 'abp.services.app.location', 'abp.services.app.material',
        'abp.services.app.unitConversion', '$rootScope', 'abp.services.app.template', 'abp.services.app.dayClose', 'abp.services.app.supplier', 'abp.services.app.purchaseOrder',
        function ($scope, $filter, $state, $stateParams, invoiceService, appSession, suppliermaterialService, locationService, materialService, unitconversionService, $rootScope, templateService, daycloseService, supplierService, purchaseorderService) {
            var vm = this;
            /* eslint-disable */
            vm.detailloading = false;
            $rootScope.settings.layout.pageSidebarClosed = true;
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.permissions = {
                barcodeUsage: abp.auth.hasPermission('Pages.Tenant.House.Master.Material.Barcode'),
            };

            vm.saving = false;
            vm.invoice = null;
            vm.loadingCount = 0;
            $scope.existall = true;
            vm.invoiceId = $stateParams.id;
            vm.invoiceType = $stateParams.invoiceType;
            vm.poRefId = $stateParams.poRefId;

            vm.taxAutoFill = true;

            vm.focAlertMessageFlag = false;
            vm.reftax = [];
            vm.uilimit = null;
            vm.allTimeDcs = true;
            vm.refdctranid = [];
            vm.refPoTranIds = [];
            //vm.invoicemode = [];
            vm.invoicePortions = [];
            vm.refunit = [];
            vm.sno = 0;
            vm.invoicedetail = [];
            vm.availableQty = 0;
            vm.checkFinished = false;
            vm.totalTaxAmount = 0;
            vm.invoiceTotalAmount = 0;

            vm.defaulttemplatetype = 7;
            vm.template = null;
            vm.selectedTemplate = null;

            vm.templatesave = null;

            vm.ratechangeallowedflag = abp.setting.getBoolean("App.House.InvoiceSupplierRateChangeFlag");
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.cancel = function (argOption) {
                if (argOption == 1) {
                    abp.message.confirm(
                        app.localize('CancelEntry?'),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                $state.go('tenant.invoice');
                                $rootScope.settings.layout.pageSidebarClosed = false;
                            }
                        }
                    );
                }
                else {
                    $state.go('tenant.invoice');
                    $rootScope.settings.layout.pageSidebarClosed = false;
                }



            };

            //vm.refinvoicemode = [];
            //vm.refinvoicemode.unshift({ value: "0", displayText: app.localize('CASH') });
            //vm.refinvoicemode.unshift({ value: "1", displayText: app.localize('CREDIT') });

            vm.defaultLocationId = appSession.user.locationRefId;
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;

            vm.isPurchaseAllowed = appSession.location.isPurchaseAllowed;

            if (vm.isPurchaseAllowed == false) {
                abp.notify.warn(app.localize('PurchaseAllowedErr'));
                vm.cancel(0);
                return;
            }

            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.temptaxinfolist = [];

            $scope.minDate = moment().add(-90, 'days');  // -30 or -60 Sysadmin Table 
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];


            $('input[name="invoiceDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });

            vm.tempinvoicedetail = [];

            vm.invoicedirectcreditlink = [];
            vm.materialTotAmt = 0;

            //vm.invoicemode.push(
            //    { 'modeno': 1, 'invmode': 'CREDIT' }, { 'modeno': 2, 'invmode': 'CASH' });

            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.setDueDates = function () {
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };
            }
            vm.setDueDates();


            //////---------------- Direct Invoice

            $scope.func = function (data, val, index) {
                var forLoopFlag = true;

                angular.forEach(vm.invoicePortions, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialRefName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            if (vm.permissions.barcodeUsage)
                                $("#barcode-" + index).focus();

                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.materialRefId = 0;
                    data.barcode = '';
                    data.materialRefName = '';
                    return;
                }

                data.materialRefId = val.materialRefId;
                data.materialRefName = val.materialRefName;
                data.uom = val.uom;
                data.defaultUnitId = val.defaultUnitId;
                data.price = parseFloat(val.materialPrice);
                data.initialprice = parseFloat(val.materialPrice);
                data.unitRefName = val.unitRefName;
                data.unitRefId = val.unitRefId;
                data.barcode = val.barcode;
                if (data.barcode == null || data.barcode.length == 0)
                    data.barcode = data.materialRefName;

                data.unitRefIdForMinOrder = val.unitRefIdForMinOrder;
                data.unitRefNameForMinOrder = val.unitRefNameForMinOrder;
                if (vm.invoice.isDirectInvoice)
                    data.yieldPercentage = val.yieldPercentage;


                if (data.price == 0) {
                    data.rateeditflag = true;
                    data.price = null;
                }
                else {
                    data.rateeditflag = vm.ratechangeallowedflag;
                }


                vm.temptaxinfolist = [];

                angular.forEach(val.taxForMaterial, function (value, key) {
                    vm.temptaxinfolist.push({
                        'materialRefId': value.materialRefId,
                        'taxRefId': value.taxRefId,
                        'taxName': value.taxName,
                        'taxRate': value.taxRate,
                        'taxValue': value.taxValue,
                        'rounding': value.rounding,
                        'sortOrder': value.sortOrder,
                        'taxCalculationMethod': value.taxCalculationMethod
                    });
                });

                data.taxForMaterial = vm.temptaxinfolist;
                if (vm.taxAutoFill) {
                    if (vm.invoice.supplierTaxApplicableByDefault) {
                        data.applicableTaxes = val.applicableTaxes;
                    }
                }
                else
                    data.applicableTaxes = [];
                data.unitRefId = val.unitRefId;

                //fillDropDownUnitBasedOnMaterial(data.materialRefId, data);
            };

            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

            vm.unitReference = function (selected, data) {
                data.unitRefId = selected.value;
                data.unitRefName = selected.displayText;
                data.changeunitflag = false;

                if (data.unitRefIdForMinOrder == data.unitRefId) {
                    data.price = data.initialprice;
                    vm.amtCalc(data, data.totalQty, data.price);
                    vm.calculateTotalVlaue(vm.invoicePortions);
                }
                else {

                    //var convFactor = 0;
                    //var convExistFlag = false;
                    //vm.refconversionunit.some(function (refdata, refkey) {
                    //    if (refdata.baseUnitId == data.unitRefIdForMinOrder && refdata.refUnitId == data.unitRefId) {
                    //        convFactor = refdata.conversion;
                    //        convExistFlag = true;
                    //        return true;
                    //    }
                    //});


                    //if (convExistFlag == false) {
                    //    abp.notify.error(app.localize('ConversionFactorNotExist', data.unitRefNameForMinOrder, data.unitRefName));
                    //    data.price = 0;
                    //    vm.amtCalc(data, data.totalQty, data.price);
                    //    vm.calculateTotalVlaue(vm.invoicePortions);
                    //    return;
                    //}

                    //var converstionUnitPrice = data.initialprice * 1 / convFactor;
                    //data.price = converstionUnitPrice.toFixed(6);
                    vm.loadingCount++;
                    materialService.getConversion({
                        baseUnitId: data.unitRefIdForMinOrder,
                        refUnitId: data.unitRefId,
                        materialRefId: data.materialRefId,
                        supplierRefId: vm.invoice.supplierRefId
                    })
                        .success(function (result) {
                            var conversion = result;
                            if (conversion.id == 0 || conversion.id == null) {
                                abp.notify.error(app.localize('ConversionFactorNotExist', data.unitRefNameForMinOrder, data.unitRefName));
                                data.price = 0;
                                vm.amtCalc(data, data.totalQty, data.price);
                                vm.calculateTotalVlaue(vm.invoicePortions);
                            }
                            else {
                                var convFactor = conversion.conversion;
                                var converstionUnitPrice = data.initialprice * 1 / convFactor;
                                data.price = parseFloat(converstionUnitPrice.toFixed(4));
                                vm.amtCalc(data, data.totalQty, data.price);
                                vm.calculateTotalVlaue(vm.invoicePortions);
                            }
                        }).finally(function () {
                            vm.loadingCount--;
                        });
                }

            }


            /////------------------ Template

            vm.refTemplateDetail = [];
            vm.reftemplate = [];

            function fillDropDownTemplate() {
                vm.loading = true;
                templateService.getTemplateDetail({
                    locationRefId: vm.defaultLocationId,
                    templateType: vm.defaulttemplatetype
                }).success(function (result) {
                    vm.refTemplateDetail = result.items;
                    angular.forEach(vm.refTemplateDetail, function (value, key) {
                        vm.reftemplate.push({
                            'value': value.id,
                            'displayText': value.name,
                        });
                    });
                    vm.loading = false;
                });
            }

            fillDropDownTemplate();


            vm.fillDataFromTemplate = function () {
                vm.autoPoDetails = [];
                if (vm.template == null) {
                    abp.notify.warn(app.localize('Template') + " ?")
                    return;
                }

                vm.refTemplateDetail.some(function (value, key) {
                    if (value.id == vm.template) {
                        vm.selectedTemplate = value.templateData;
                        return true;
                    }
                });

                vm.loading = true;



                invoiceService.getTemplateObjectForEdit({
                    objectString: vm.selectedTemplate
                }).success(function (result) {

                    var accountDate = vm.invoice.accountDate;
                    vm.uilimit = null;

                    vm.invoice = result.invoice;
                    vm.invoicedetail = result.invoiceDetail;
                    vm.invoicedirectcreditlink = result.invoiceDirectCreditLink

                    if (vm.defaultLocationId != vm.invoice.locationRefId) {
                        abp.notify.warn(app.localize('DraftLocationNotMatched'));
                        vm.loading = false;
                        return;
                    }

                    vm.invoice.accountDate = accountDate;

                    vm.invoice.invoiceDate = moment(result.invoice.invoiceDate).format($scope.format);

                    $('input[name="accountDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.invoice.accountDate,
                        maxDate: vm.invoice.accountDate,
                    });

                    $('input[name="invoiceDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.invoice.invoiceDate,
                        minDate: $scope.minDate,
                        maxDate: moment()
                    });

                    //$('input[name="invoiceData"]').daterangepicker({
                    //    locale: {
                    //        format: $scope.format
                    //    },
                    //    singleDatePicker: true,
                    //    showDropdowns: true,
                    //    startDate: vm.invoice.poDate,
                    //    minDate: $scope.minDate,
                    //    maxDate: moment().format($scope.format)
                    //});

                    //$('input[name="dueDate"]').daterangepicker({
                    //    locale: {
                    //        format: $scope.format
                    //    },
                    //    singleDatePicker: true,
                    //    showDropdowns: true,
                    //    startDate: vm.purchaseorder.deliveryDateExpected,
                    //    minDate: $scope.minDate,
                    //});

                    vm.invoicePortions = [];

                    angular.forEach(vm.invoicedetail, function (value, key) {
                        vm.invoicePortions.push({
                            'sno': value.sno,
                            'dcTranid': value.dcTranId,
                            'poRefId': value.poRefId,
                            'barcode': value.barcode,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'totalQty': value.totalQty,
                            'price': value.price,
                            'discountFlag': value.discountFlag,
                            'discountPercentage': value.discountPercentage,
                            'discountAmount': parseFloat(vm.isUndefinedOrNull(value.discountAmount) ? 0 : value.discountAmount),
                            'totalAmount': parseFloat(parseFloat(value.totalAmount).toFixed(2)),
                            'taxAmount': parseFloat(parseFloat(value.taxAmount).toFixed(2)),
                            'netAmount': parseFloat(parseFloat(value.netAmount).toFixed(2)),
                            'remarks': value.remarks,
                            'taxForMaterial': value.taxForMaterial,
                            'uom': value.uom,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.uom,
                            'changeunitflag': false,
                            'applicableTaxes': value.applicableTaxes,
                            'yieldPercentage': value.yieldPercentage
                        });

                        //vm.invoicePortions.push(value);
                        //    vm.purchaseDetailPortion.push({
                        //        'sno': value.sno,
                        //        'materialRefId': value.materialRefId,
                        //        'materialRefName': value.materialRefName,
                        //        'qtyOrdered': value.qtyOrdered,
                        //        'defaultUnitId': value.defaultUnitId,
                        //        'uom': value.uom,
                        //        'unitRefId': value.unitRefId,
                        //        'unitRefName': value.unitRefName,
                        //        'price': value.price,
                        //        'initialprice': value.price,
                        //        'discountOption': vm.discFlag,
                        //        'discountValue': value.discountValue,
                        //        'totalAmount': value.totalAmount,
                        //        'taxAmount': value.taxAmount,
                        //        'netAmount': value.netAmount,
                        //        'remarks': value.remarks,
                        //        'currentInHand': vm.temponhand,
                        //        'alreadyOrderedQuantity': vm.temponorder,
                        //        'taxlist': value.taxForMaterial,
                        //        'applicableTaxes': value.taxForMaterial,
                        //        'changeunitflag': false,
                        //    });

                    });
                    vm.loading = false;
                    vm.fillDropDownMaterial();

                    vm.calculateTotalVlaue(vm.invoicePortions);

                    vm.invoiceAlreadyExists();
                });

            };



            $scope.taxCalc = function (data, totalQty, price) {
                if (vm.isUndefinedOrNull(totalQty))
                    totalQty = 0;
                if (vm.isUndefinedOrNull(price))
                    price = 0;

                data.totalAmount = parseFloat(price) * parseFloat(totalQty);
                data.totalAmount = parseFloat(data.totalAmount.toFixed(2));

                data.taxAmount = 0;
                data.taxForMaterial = [];

                taxneedstoremove = [];

                if (data.applicableTaxes.length > 0)    //  Calculate Tax 
                {
                    angular.forEach(data.applicableTaxes, function (value, key) {
                        var taxCalculationOnValue = 0.0;

                        if (value.taxCalculationMethod == 'GT' || value.taxCalculationMethod == 'ST') {
                            taxCalculationOnValue = data.totalAmount;
                        }
                        else {
                            var taxrefvalueflag = false;
                            data.taxForMaterial.some(function (taxdata, taxkey) {
                                if (taxdata.taxName == value.taxCalculationMethod) {
                                    taxCalculationOnValue = taxdata.taxValue;
                                    taxrefvalueflag = true;
                                    return true;
                                }
                            });

                            if (taxrefvalueflag == false)
                                taxneedstoremove.push({ 'taxName': value.taxName });
                        }

                        if (taxCalculationOnValue > 0) {
                            var taxamt = parseFloat(taxCalculationOnValue * value.taxRate / 100);
                            taxamt = parseFloat(taxamt.toFixed(2));
                            data.taxAmount = parseFloat(data.taxAmount) + parseFloat(taxamt);
                            data.taxAmount = parseFloat(data.taxAmount.toFixed(2));

                            if (taxamt > 0) {
                                data.taxForMaterial.push
                                    ({
                                        'materialRefId': data.materialRefId,
                                        'taxRefId': value.taxRefId,
                                        'taxName': value.taxName,
                                        'taxRate': value.taxRate,
                                        'taxValue': taxamt,
                                        'rounding': value.rounding,
                                        'sortOrder': value.sortOrder,
                                        'taxCalculationMethod': value.taxCalculationMethod
                                    });
                            }
                        }
                    });
                }


                angular.forEach(taxneedstoremove, function (value, key) {
                    angular.forEach(data.applicableTaxes, function (detvalue, detkey) {
                        if (value.taxName == detvalue.taxName)
                            data.applicableTaxes.splice(detkey, 1);
                    });
                });

                data.netAmount = parseFloat(parseFloat(data.totalAmount).toFixed(2)) + parseFloat(parseFloat(data.taxAmount).toFixed(2));
                vm.calculateTotalVlaue(vm.invoicePortions);
            }



            vm.amtCalc = function (data, totalQty, price) {
                $scope.taxCalc(data, totalQty, price);
                return;
            }

            vm.refconversionunit = [];
            vm.fillUnitConversion = function () {
                unitconversionService.getAll({
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null
                }).success(function (result) {
                    vm.refconversionunit = result.items;
                });
            }

            vm.fillUnitConversion();

            vm.refmaterial = [];
            vm.fillDropDownMaterial = function () {
                vm.detailloading = true;
                vm.loading = true;
                vm.loadingCount++;
                suppliermaterialService.getMaterialBasedOnSupplierWithPrice({
                    locationRefId: vm.invoice.locationRefId,
                    supplierRefId: vm.invoice.supplierRefId
                }).success(function (result) {
                    vm.refmaterial = result.items;
                }).finally(function () {
                    if (vm.invoice.isDirectInvoice && vm.invoicePortions.length == 0)
                        vm.addPortion();
                    vm.detailloading = false;
                    vm.loading = false;
                    vm.loadingCount--;
                });
            }

            vm.invoicePayModeList = [];
            vm.fillDropDownPayModes = function () {
                materialService.getInvoicePayModeEnumForCombobox({}).success(function (result) {
                    vm.invoicePayModeList = result.items;
                });
            }
            vm.fillDropDownPayModes();

            vm.refreshMaterial = function () {
                vm.fillDropDownMaterial();
            }

            vm.refbarcodelist = [];
            function fillDropDownBarCodeList() {
                vm.loading = true;
                vm.refbarcodelist = [];
                materialService.getBarcodesForMaterial().success(function (result) {
                    vm.refbarcodelist = result.barcodeList;
                    vm.loading = false;
                });
            }


            vm.reftax = [];
            vm.fillDropDownTaxes = function () {
                suppliermaterialService.getTaxForMaterial()
                    .success(function (result) {
                        vm.reftax = result;
                    }).finally(function () {

                    });
            }


            vm.getMaterialQuotePrice = function (data, supmaterial) {

                if (data.materialRefId > 0) {
                    if (vm.invoice.id == null) {
                        data.price = parseFloat(supmaterial.materialPrice);

                        if (data.price == 0) {
                            data.rateeditflag = true;
                            data.price = null;
                        }
                        else
                            data.rateeditflag = vm.ratechangeallowedflag;
                    }
                    data.uom = supmaterial.uom;
                }
                return parseInt(supmaterial.materialRefId);
            };

            vm.addPortion = function (objId) {

                var errorFlag = false;
                var value = vm.invoicePortions[vm.invoicePortions.length - 1];

                if (vm.sno > 0) {
                    //value = value[0];

                    if (vm.existAllElements() == false)
                        return;

                    if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
                        errorFlag = true;
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.materialRefName) || value.materialRefName == '') {
                        errorFlag = true;
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.totalQty) || value.totalQty == 0) {
                        errorFlag = true;
                        abp.notify.warn(app.localize('InvalidOrderQty'));
                        $("#qty-" + objId).focus();
                        return;
                    }

                    if ((vm.isUndefinedOrNull(value.price) || value.price == 0) && vm.invoice.isFoc == false) {
                        errorFlag = true;
                        abp.notify.warn(app.localize('InvalidMaterialPrice') + " / " + app.localize('FOC') + " ?");
                        $("#price-" + objId).focus();
                        return;
                    }

                    if ((vm.isUndefinedOrNull(value.totalAmount) || value.totalAmount == 0) && vm.invoice.isFoc == false) {
                        errorFlag = true;
                        abp.notify.warn(app.localize('Amount'));
                        return;
                    }
                    if ((vm.isUndefinedOrNull(value.netAmount) || value.netAmount == 0) && vm.invoice.isFoc == false) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialTotalAmt'));
                        return;
                    }

                }

                vm.refunit = [];

                vm.sno = vm.sno + 1;

                vm.invoicePortions.push({
                    'sno': vm.sno,
                    'dcTranid': 0,
                    'poRefId': null,
                    'barcode': '',
                    'materialRefId': 0,
                    'materialRefName': '',
                    'materialSupplierAliasName': '',
                    'totalQty': '',
                    'price': '',
                    'discountFlag': false,
                    'discountPercentage': 0,
                    'discountAmount': 0,
                    'totalAmount': 0,
                    'taxAmount': 0,
                    'netAmount': 0,
                    'remarks': '',
                    'taxForMaterial': null,
                    'uom': '',
                    'unitRefId': 0,
                    'changeunitflag': false,
                    'applicableTaxes': [],
                    'yieldPercentage': null
                });

                vm.calculateTotalVlaue(vm.invoicePortions);
                //$('input')[$('input').index(this) + 1].focus();
            }
            /////------------------------------------------

            vm.existAllElements = function () {
                $scope.existall = true;
                $scope.errmessage = "";

                if (vm.invoice.supplierRefId == null || vm.invoice.supplierRefId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('SupplierErr');
                    $('#selsupplierRefId').focus();
                }

                if (vm.invoice.invoiceDate == null) {
                    $scope.errmessage = $scope.errmessage + ' ' + app.localize('InvoiceDateErr');
                    $('#invoicedate').focus();
                }

                if (vm.invoice.accountDate == null) {
                    $scope.errmessage = $scope.errmessage + ' ' + app.localize('AccountDateErr');
                    $('#invoicedate').focus();
                }

                if (vm.invoice.invoiceNumber == null || vm.invoice.invoiceNumber.length == 0) {
                    $scope.errmessage = $scope.errmessage + ' ' + app.localize('InvoiceNumberErr');
                    $('#invoiceno').focus();
                }

                if (vm.invoice.locationRefId == null || vm.invoice.locationRefId == 0) {
                    $scope.errmessage = $scope.errmessage + ' ' + app.localize('LocationErr');
                    $('#defaultLocation').focus();
                }

                if (vm.invoice.dcFromDate == null || vm.invoice.dcToDate == null) {
                    vm.invoice.dcFromDate = vm.invoice.accountDate;
                    vm.invoice.dcToDate = vm.invoice.accountDate;
                }

                if (vm.invoice.invoicePayMode == null || vm.invoice.invoicePayMode == 0) {
                    $scope.errmessage = $scope.errmessage + ' ' + app.localize('InvoiceModeErr');
                }

                if ($scope.errmessage != "") {
                    $scope.existall = false;
                    abp.notify.error($scope.errmessage, 'Required');
                    return false;
                }
                else
                    return true;

            };



            vm.getDetailMaterial = function () {
                vm.checkFinished = false;

                if (vm.existAllElements() == false && vm.poRefId == null)
                    return;
                vm.uilimit = null;
                vm.loading = true;
                invoiceService.getMaterialsFromDcId({
                    dcRefId: vm.tempinvoicedetail.dcTranid,
                    supplierRefId: vm.invoice.supplierRefId,
                    locationRefId: vm.defaultLocationId
                }).success(function (result) {

                    angular.forEach(result.dcDetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        if (value.barcode == null || value.barcode.length == 0)
                            value.barcode = value.materialRefName;

                        var data = {
                            'sno': vm.sno,
                            'dcTranid': value.dcTranId,
                            'poRefId': null,
                            'barcode': value.barcode,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'totalQty': value.totalQty,
                            'price': value.price,
                            'initialprice': value.price,
                            'discountFlag': value.discountFlag,
                            'discountPercentage': value.discountPercentage,
                            'discountAmount': parseFloat(vm.isUndefinedOrNull(value.discountAmount) ? 0 : value.discountAmount),
                            'totalAmount': parseFloat(parseFloat(value.totalAmount).toFixed(2)),
                            'taxAmount': parseFloat(parseFloat(value.taxAmount).toFixed(2)),
                            'netAmount': parseFloat(parseFloat(value.netAmount).toFixed(2)),
                            'remarks': value.remarks,
                            'taxForMaterial': value.taxForMaterial,
                            'uom': value.uom,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'unitRefIdForMinOrder': value.unitRefId,
                            'unitRefNameForMinOrder': value.unitRefName,
                            'changeunitflag': false,
                            'applicableTaxes': value.applicableTaxes,
                            'rateeditflag': vm.ratechangeallowedflag,
                            'defaultUnitId': value.defaultUnitId,
                            'defaultUnitName': value.defaultUnitName,
                            'yieldPercentage': value.yieldPercentage,
                            'materialSupplierAliasName': value.materialSupplierAliasName
                        };
                        vm.amtCalc(data, data.totalQty, data.price);
                        vm.invoicePortions.push(data);
                        vm.calculateTotalVlaue(vm.invoicePortions);
                    });

                    vm.refdctranid.some(function (value, key) {
                        if (parseInt(value.value) == parseInt(vm.tempinvoicedetail.dcTranid)) {
                            vm.refdctranid.splice(key, 1);
                            return true;
                        }
                    });

                    vm.tempinvoicedetail = [];
                }).finally(function () {
                    vm.saving = false;
                    vm.loading = false;
                });
            }


            vm.getDetailMaterialBasedOnPo = function () {
                vm.checkFinished = false;

                if (vm.poRefId == null || vm.portionLength == 0)
                    if (vm.existAllElements() == false && vm.poRefId == null)
                        return;

                vm.uilimit = null;
                vm.loading = true;
                vm.uilimit = null;
                invoiceService.getMaterialsFromPoId({
                    poRefId: vm.tempinvoicedetail.poRefId,
                    supplierRefId: vm.invoice.supplierRefId,
                    locationRefId: vm.defaultLocationId
                }).success(function (result) {
                    angular.forEach(result.dcDetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        if (value.barcode == null || value.barcode.length == 0)
                            value.barcode = value.materialRefName;

                        var data = {
                            'sno': vm.sno,
                            'dcTranid': value.dcTranId,
                            'poRefId': value.poRefId,
                            'barcode': value.barcode,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'totalQty': value.totalQty,
                            'price': value.price,
                            'initialprice': value.price,
                            'discountFlag': value.discountFlag,
                            'discountPercentage': value.discountPercentage,
                            'discountAmount': parseFloat(vm.isUndefinedOrNull(value.discountAmount) ? 0 : value.discountAmount),
                            'totalAmount': parseFloat(parseFloat(value.totalAmount).toFixed(2)),
                            'taxAmount': parseFloat(parseFloat(value.taxAmount).toFixed(2)),
                            'netAmount': parseFloat(parseFloat(value.netAmount).toFixed(2)),
                            'remarks': value.remarks,
                            'taxForMaterial': value.taxForMaterial,
                            'uom': value.uom,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'unitRefIdForMinOrder': value.unitRefId,
                            'unitRefNameForMinOrder': value.unitRefName,
                            'changeunitflag': false,
                            'applicableTaxes': value.applicableTaxes,
                            'rateeditflag': vm.ratechangeallowedflag,
                            'defaultUnitId': value.defaultUnitId,
                            'defaultUnitName': value.defaultUnitName,
                            'yieldPercentage': value.yieldPercentage,
                            'materialSupplierAliasName': value.materialSupplierAliasName
                        };
                        vm.amtCalc(data, data.totalQty, data.price);
                        vm.invoicePortions.push(data);

                        vm.calculateTotalVlaue(vm.invoicePortions);

                    });

                    vm.refPoTranIds.some(function (value, key) {
                        if (parseInt(value.value) == parseInt(vm.tempinvoicedetail.poRefId)) {
                            vm.refPoTranIds.splice(key, 1);
                            return true;
                        }
                    });

                    vm.tempinvoicedetail = [];

                    vm.setReceiptDate(moment(result.deliverDateRequested).format($scope.format));

                }).finally(function () {
                    vm.saving = false;
                    vm.loading = false;
                });

            }

            vm.setReceiptDate = function (argDate) {
                vm.invoice.dcDate = argDate;

                $('input[name="dcDate"]').daterangepicker({
                    locale: {
                        format: $scope.format
                    },
                    singleDatePicker: true,
                    showDropdowns: true,
                    startDate: argDate,
                    //                            minDate: $scope.minDate,
                    maxDate: moment()
                });

            }

            vm.next = function () {
                vm.checkFinished = false;
                if (vm.existAllElements() == false)
                    return;

                var value = vm.tempinvoicedetail;

                var errorFlag = false;

                if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
                    errorFlag = true;
                    abp.notify.info(app.localize('MinimumOneDetail'));
                    return;
                }

                if (vm.isUndefinedOrNull(value.materialRefName) || value.materialRefName == 0) {
                    errorFlag = true;
                    abp.notify.info(app.localize('MaterialRequired'));
                    return;
                }

                if (vm.isUndefinedOrNull(value.totalQty) || value.totalQty == 0) {
                    errorFlag = true;
                    abp.notify.info(app.localize('InvalidInvoiceQty'));
                    return;
                }
                if ((vm.isUndefinedOrNull(value.price) || value.price == 0) && vm.invoice.isFoc == false) {
                    errorFlag = true;
                    abp.notify.info(app.localize('InvalidMaterialPrice'));
                    return;
                }
                if ((vm.isUndefinedOrNull(value.totalAmount) || value.totalAmount == 0) && vm.invoice.isFoc == false) {
                    errorFlag = true;
                    abp.notify.info(app.localize('InvalidMaterialTotalAmt'));
                    return;
                }

                if (vm.isUndefinedOrNull(value.dcTranid)) {
                    errorFlag = true;
                    abp.notify.info(app.localize('PleaseSelectDCTranId'));
                    return;
                }

                if ($("#discFlag").is(":checked")) {
                    if (vm.isUndefinedOrNull(value.discountAmount) || value.discountAmount == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidDiscountAmt'));
                        return;
                    }
                }

                if (errorFlag) {
                    vm.saving = false;
                    vm.loading = false;
                    $scope.existall = false;
                }

                vm.sno = vm.sno + 1;
                var netamt = parseFloat(vm.isUndefinedOrNull(value.totalAmount) ? 0 : value.totalAmount) - parseFloat(vm.isUndefinedOrNull(value.discountAmount) ? 0 : value.discountAmount);

                vm.invoicePortions.push({
                    'sno': vm.sno,
                    'dcTranid': value.dcTranid,
                    'barcode': value.barcode,
                    'materialRefId': value.materialRefId,
                    'materialRefName': value.materialRefName,
                    'totalQty': value.totalQty,
                    'price': value.price,
                    'discountFlag': value.discountFlag,
                    'discountPercentage': value.discountPercentage,
                    'discountAmount': parseFloat(vm.isUndefinedOrNull(value.discountAmount) ? 0 : value.discountAmount),
                    'totalAmount': parseFloat(parseFloat(value.totalAmount).toFixed(2)),
                    'taxAmount': parseFloat(parseFloat(value.taxAmount).toFixed(2)),
                    'netAmount': parseFloat(parseFloat(netAmount).toFixed(2)),
                    'remarks': value.remarks,
                    'taxForMaterial': value.taxForMaterial,
                    'uom': value.uom,
                    'unitRefId': value.unitRefId,
                    'changeunitflag': false,
                    'applicableTaxes': value.applicableTaxes,
                    'yieldPercentage': value.yieldPercentage,
                    'materialSupplierAliasName': value.materialSupplierAliasName
                });

                vm.tempinvoicedetail = [];
                vm.calculateTotalVlaue(vm.invoicePortions);
            };




            $scope.amtCalc = function (totalQty, price) {
                if (vm.isUndefinedOrNull(totalQty))
                    totalQty = 0;
                if (vm.isUndefinedOrNull(price))
                    price = 0;
                vm.tempinvoicedetail.totalAmount = price * totalQty;
            }


            $scope.convert = function (thedate) {
                var tempstr = thedate.toString();
                var newstr = tempstr.toString().replace(/GMT.*/g, "")
                newstr = newstr + " UTC";
                return new Date(newstr);
            };

            vm.invoiceAlreadyExists = function () {
                if (vm.invoice.invoicePayMode == null || vm.invoice.invoicePayMode == 0) {
                    abp.notify.error(app.localize('InvoiceModeErr'));
                }
                if (vm.invoice.invoiceNumber) {
                    var invoiceDate = $scope.convert(moment(vm.invoice.invoiceDate, $scope.format));
                    var resDate = $scope.convert(vm.invoice.invoiceDate);
                    invoiceService.invoiceAlreadyExists(
                        {
                            supplierRefId: vm.invoice.supplierRefId,
                            locationRefId: vm.defaultLocationId,
                            invoiceNumber: vm.invoice.invoiceNumber,
                            startDate: invoiceDate,
                            endDate: invoiceDate
                        }).success(function (result) {
                            vm.alreadyExistInvoice = result.items;
                            if (result.totalCount > 0) {
                                abp.notify.error(app.localize('InvoiceAlreadyExists', result.items[0].invoiceNumber));
                                abp.message.error(app.localize('InvoiceAlreadyExists', result.items[0].invoiceNumber));
                                vm.invoice.invoiceNumber = null;
                                vm.invoicePortions = [];
                                vm.calculateTotalVlaue(vm.invoicePortions);
                                vm.selectedTemplate = null;
                                vm.template = null;
                                vm.cancel(0);
                            }
                        });
                }
            }

            $scope.filterDcDate = function (val) {
                if (vm.invoice.isDirectInvoice == true)
                    return;
                if (vm.invoice.supplierRefId > 0) {

                    if (moment(val).isValid()) {
                        if (vm.allTimeDcs) {
                            vm.invoice.dcFromDate = null;
                            vm.invoice.dcToDate = null;
                        }
                        else {
                            vm.invoice.dcFromDate = moment(val.startDate).format('YYYY-MM-DD');
                            vm.invoice.dcToDate = moment(val.endDate).format('YYYY-MM-DD');
                        }

                        vm.dateRangeModel.startDate = vm.invoice.dcFromDate;
                        vm.dateRangeModel.endDate = vm.invoice.dcToDate;
                    }

                    fillDcLists(vm.dateRangeModel);
                }
            }

            $scope.funcMat = function (val) {
                var forLoopFlag = true;


                angular.forEach(vm.invoicePortions, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    vm.tempinvoicedetail.materialRefId = 0;
                    vm.tempinvoicedetail.barcode = '';
                    vm.tempinvoicedetail.materialRefName = '';
                    return;
                }
                vm.tempinvoicedetail.materialRefName = val;
            };

            vm.refdctranid = [];
            vm.refPoTranIds = [];

            function fillDcLists(daterange) {

                invoiceService.setDcCompleteStatus({
                    id: vm.invoice.supplierRefId
                }).success(function (result) {
                    //abp.notify.info('Completed');
                });

                if (vm.invoice.isDirectInvoice == true)
                    return;
                vm.refdctranid = [];
                invoiceService.getDcTranId($.extend({
                    allTimeDcs: vm.allTimeDcs,
                    supplierRefId: vm.invoice.supplierRefId,
                    locationRefId: vm.defaultLocationId
                }, daterange)).success(function (result) {
                    vm.refdctranid = result.items;
                    if (result.items.length > 0)
                        vm.uilimit = null;
                });


                vm.refPoTranIds = [];
                invoiceService.getDcPoTranId($.extend({
                    allTimeDcs: vm.allTimeDcs,
                    supplierRefId: vm.invoice.supplierRefId,
                    locationRefId: vm.defaultLocationId
                }, daterange)).success(function (result) {
                    vm.refPoTranIds = result.items;
                    if (result.items.length > 0)
                        vm.uilimit = null;
                });
            }

            $scope.checkPendingQty = function (enteredQty) {
                if (parseFloat(enteredQty) == parseFloat(vm.availableQty))
                    return;
                else if (parseFloat(enteredQty) > parseFloat(vm.availableQty)) {
                    abp.message.warn(app.localize("QtyErr", enteredQty, vm.availableQty), "Error");
                    vm.tempinvoicedetail.totalQty = 0;
                    $("#totqty").focus();
                }
            }

            vm.getDcQty = function (dcitem) {
                vm.availableQty = parseFloat(dcitem.pendingQty);
                return parseInt(dcitem.materialRefId);
            };

            vm.refdcmaterial = [];

            vm.fillMaterialBasedOnInwardDC = function (selValue) {
                //invoiceService.getMaterialForInwardDc({ Id: selValue }).success(function (result) {
                //    vm.refdcmaterial = result;
                //}).finally(function () {
                //    vm.loading = false;
                //});

            }

            vm.fillMaterialBasedOnPo = function (selValue) {
                //invoiceService.getMaterialForPo({ Id: selValue }).success(function (result) {
                //    vm.refdcmaterial = result;
                //}).finally(function () {
                //    vm.loading = false;
                //});
            }

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.yieldCalculation = function (data) {
                if (vm.invoice.isDirectInvoice) {
                    if (data.yieldPercentage != null && data.yieldExcessShortageQty == null) {
                        var diffPercentage = 100 - data.yieldPercentage;
                        if (diffPercentage == 0) {
                            // Do Noting
                        }
                        else if (diffPercentage > 0) {
                            data.yieldMode = app.localize('Shortage');
                            data.yieldExcessShortageQty = parseFloat(data.totalQty * diffPercentage / 100);
                        }
                        else if (diffPercentage < 0) {
                            data.yieldMode = app.localize('Excess');
                            data.yieldExcessShortageQty = parseFloat(data.totalQty * (Math.abs(diffPercentage) / 100));
                        }
                        data.yieldExcessShortageQty = parseFloat(data.yieldExcessShortageQty.toFixed(2));
                    }
                    else {
                        if (data.yieldPercentage == null) {
                            data.yieldExcessShortageQty = 0;
                            data.yieldMode = '';
                        }
                    }
                }
            };

            vm.calculateTotalVlaue = function (data) {
                var tempTotDiscAmt = 0;
                var tempTotInvAmt = 0;
                var tempTotTaxAmt = 0;
                var tempTotalAmount = 0;

                angular.forEach(data, function (val, key) {
                    data[key].changeunitflag = false;
                    tempTotDiscAmt = parseFloat(tempTotDiscAmt) + parseFloat(vm.isUndefinedOrNull(val.discountAmount) ? 0 : val.discountAmount);
                    tempTotTaxAmt = parseFloat(tempTotTaxAmt) + parseFloat(parseFloat(vm.isUndefinedOrNull(val.taxAmount) ? 0 : val.taxAmount).toFixed(2));
                    tempTotInvAmt = parseFloat(tempTotInvAmt) + parseFloat(parseFloat(vm.isUndefinedOrNull(val.netAmount) ? 0 : val.netAmount).toFixed(2));
                    tempTotalAmount = parseFloat(tempTotalAmount) + parseFloat(parseFloat(vm.isUndefinedOrNull(val.totalAmount) ? 0 : val.totalAmount).toFixed(2));
                });
                vm.invoiceTotalAmount = parseFloat(tempTotalAmount.toFixed(2));;
                vm.totalTaxAmount = parseFloat(tempTotTaxAmt.toFixed(2));

                if (vm.invoice.totalDiscountAmount == null || vm.invoice.totalDiscountAmount == "")
                    vm.invoice.totalDiscountAmount = 0;

                if (vm.invoice.totalShipmentCharges == null || vm.invoice.totalShipmentCharges == "")
                    vm.invoice.totalShipmentCharges = 0;

                if (vm.invoice.roundedAmount == null || vm.invoice.roundedAmount == "")
                    vm.invoice.roundedAmount = 0;

                if (vm.invoice.totalDiscountAmount > tempTotInvAmt) {
                    abp.notify.warn("DiscountInvoiceErr");
                    vm.invoice.totalDiscountAmount = 0;
                }

                if (vm.invoice.roundedAmount > tempTotInvAmt) {
                    abp.notify.warn("RoundedAmountInvoiceErr");
                    vm.invoice.roundedAmount = 0;
                }

                tempTotInvAmt = (tempTotInvAmt - parseFloat(vm.invoice.totalDiscountAmount) + parseFloat(vm.invoice.totalShipmentCharges) + parseFloat(vm.invoice.roundedAmount));

                vm.invoice.invoiceAmount = parseFloat(tempTotInvAmt.toFixed(2));
                //vm.invoice.totalDiscountAmount = parseFloat(tempTotDiscAmt.toFixed(2));
                //vm.invoice.totalShipmentCharges = parseFloat(vm.invoice.totalShipmentCharges.toFixed(2));
                vm.supplierTaxRemarks = '';
                if (vm.invoice.invoiceAmount > 0) {
                    if (tempTotTaxAmt > 0 && vm.invoice.supplierTaxApplicableByDefault == false) {
                        vm.supplierTaxRemarks = app.localize('SupplierTaxAddedExtraAlert');
                    }
                    else if (tempTotTaxAmt == 0 && vm.invoice.supplierTaxApplicableByDefault == true) {
                        vm.supplierTaxRemarks = app.localize('SupplierTaxMissingAlert');
                    }
                    else {
                        vm.supplierTaxRemarks = '';
                    }
                }
                else {
                    vm.supplierTaxRemarks = '';
                }
            }

            $scope.calcDiscountAmt = function (discperct, discamt) {
                if ($("#discFlag").is(":checked")) {
                    if (vm.isUndefinedOrNull(discperct))
                        discperct = 0;
                    if (vm.isUndefinedOrNull(discamt))
                        discamt = 0;

                    vm.tempinvoicedetail.discountAmount = parseFloat($filter('number')(vm.tempinvoicedetail.totalAmount * (discperct / 100), 2));
                }

            }

            vm.discFlagCheckBox = function () {
                if (!$("#discFlag").is(":checked")) {
                    vm.tempinvoicedetail.discountAmount = 0;
                    vm.tempinvoicedetail.discountPercentage = 0;
                }
            }

            $scope.calcDiscountPerc = function (discperct, discamt) {
                if ($("#discFlag").is(":checked")) {
                    if (vm.isUndefinedOrNull(discperct))
                        discperct = 0;
                    if (vm.isUndefinedOrNull(discamt))
                        discamt = 0;

                    vm.tempinvoicedetail.discountPercentage = parseFloat($filter('number')((discamt / vm.tempinvoicedetail.totalAmount) * 100, 2));
                }
            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex) {

                vm.invoicePortions.splice(productIndex, 1);
                vm.sno = vm.sno - 1;
                vm.calculateTotalVlaue(vm.invoicePortions);
                if (vm.invoicePortions.length == 0)
                    vm.addPortion();

            }

            vm.inwardDcTranIdRef = function (selDcId) {
                vm.checkFinished = false;
                if (selDcId > 0) {
                    vm.fillMaterialBasedOnInwardDC(selDcId);
                }
            }

            vm.inwardPoTranIdRef = function (selDcId) {
                vm.checkFinished = false;
                if (selDcId > 0) {
                    vm.fillMaterialBasedOnPo(selDcId);
                }
            }

            vm.save = function () {
                if ($scope.existall == false)
                    return;

                if (vm.existAllElements() == false)
                    return;

                if (parseFloat(vm.invoice.invoiceAmount) == 0 && vm.invoice.isFoc == false) {
                    abp.message.warn(app.localize("EmptyDetail"));
                    return;
                }

                if (vm.checkAmountWithAuto() == false) {
                    return false;
                    //vm.checkAmountWithAuto();
                    //if (vm.checkFinished == false)
                    //	return;
                }

                if (parseFloat(vm.invoice.invoiceamoutenteredbyuser) != parseFloat(vm.invoice.invoiceAmount)) {
                    abp.message.warn(app.localize("InvoiceAmtNotMatchedWithAutomaticCalculation"));
                    return;
                }

                vm.saving = true;
                vm.invoicedetail = [];
                vm.discFlag = 0;

                errorFlag = false;

                angular.forEach(vm.invoicePortions, function (value, key) {
                    if (value.discountFlag == true || value.discountFlag == 1)
                        vm.discFlag = 1;
                    else
                        vm.discFlag = 0;

                    if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.materialRefName)) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MaterialRequired'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.totalQty) || value.totalQty == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidOrderQty'));
                        return;
                    }

                    if ((vm.isUndefinedOrNull(value.price) || value.price == 0) && vm.invoice.isFoc == false) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialPrice') + ' - ' + value.materialRefName);
                        return;
                    }

                    if ((vm.isUndefinedOrNull(value.price) || value.price == 0) && vm.focAlertMessageFlag == false) {
                        vm.focAlertMessageFlag = true;
                        errorFlag = true;
                        return;
                    }

                    if ((vm.isUndefinedOrNull(value.totalAmount) || value.totalAmount == 0) && vm.invoice.isFoc == false) {
                        errorFlag = true;
                        abp.notify.warn(app.localize('Amount'));
                        return;
                    }
                    if ((vm.isUndefinedOrNull(value.netAmount) || value.netAmount == 0) && vm.invoice.isFoc == false) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialTotalAmt'));
                        return;
                    }

                    vm.invoicedetail.push({
                        'sno': value.sno,
                        'materialRefId': value.materialRefId,
                        'barcode': value.barcode,
                        'totalQty': value.totalQty,
                        'price': value.price,
                        'discountFlag': vm.discFlag,
                        'discountPercentage': value.discountPercentage,
                        'discountAmount': value.discountAmount,
                        'totalAmount': value.totalAmount,
                        'taxAmount': value.taxAmount,
                        'netAmount': value.netAmount,
                        'remarks': value.remarks,
                        'taxForMaterial': value.taxForMaterial,
                        'applicableTaxes': value.applicableTaxes,
                        'uom': value.uom,
                        'unitRefId': value.unitRefId,
                        'changeunitflag': false,
                        'yieldPercentage': value.yieldPercentage,
                        'yieldExcessShortageQty': value.yieldExcessShortageQty,
                        'yieldMode': value.yieldMode,
                        'poRefId': value.poRefId,

                    });
                });

                if (errorFlag == true) {
                    vm.saving = false;
                    return;
                }

                //invoicedirectcreditlink
                vm.invoicedirectcreditlink = [];
                if (vm.invoice.isDirectInvoice == false) {
                    angular.forEach(vm.invoicePortions, function (value, key) {
                        vm.invoicedirectcreditlink.push({
                            'invoiceRefId': 0,
                            'sno': value.sno,
                            'materialRefId': value.materialRefId,
                            'inwardDirectCreditRefId': value.dcTranid,
                            'materialConvertedQty': value.totalQty,
                            'unitRefId': value.unitRefId,
                            'poRefId': value.poRefId
                        });
                    });
                }

                ////vm.invoice.accountDate = vm.invoice.invoiceDate;
                invoiceService.createOrUpdateInvoice({
                    invoice: vm.invoice,
                    invoicedetail: vm.invoicedetail,
                    invoiceDirectCreditLink: vm.invoicedirectcreditlink
                }).success(function (result) {

                    var msg = app.localize('InvoiceSuccessMessage', result.id);
                    abp.notify.info(msg);
                    if (result.adjustmentId > 0) {
                        var adjMsg = app.localize('AdjustmentForDifferenceAdded', result.adjustmentId);
                        abp.notify.warn(adjMsg);
                        msg = msg + ' ' + adjMsg;
                    }
                    abp.message.success(msg);

                    invoiceService.setDcCompleteStatus({
                        id: vm.invoice.supplierRefId
                    }).success(function (result) { });

                    vm.printInvoice(result.id);
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.printInvoice = function (objId) {
                $rootScope.settings.layout.pageSidebarClosed = false;
                $state.go('tenant.invoiceprintdetail', {
                    printid: objId
                });
            };



            vm.checkAmountWithAuto = function () {
                vm.checkFinished = false;
                if (vm.existAllElements() == false)
                    return false;

                vm.calculateTotalVlaue(vm.invoicePortions);

                if (parseFloat(vm.invoice.invoiceAmount) == 0 && vm.invoice.isFoc == false) {
                    abp.notify.warn(app.localize('EmptyDetail'))
                    vm.checkFinished = false;
                    return false;
                }

                if (vm.isUndefinedOrNull(vm.invoice.invoiceamoutenteredbyuser)) {
                    vm.invoice.checkingStatus = app.localize("MatchingFailed");
                    abp.notify.error(app.localize('MatchingFailed'));
                    vm.checkFinished = false;
                    return false;
                }

                if (parseFloat(vm.invoice.invoiceAmount) == parseFloat(vm.invoice.invoiceamoutenteredbyuser)) {
                    vm.invoice.checkingStatus = app.localize("MatchingCorrect");
                    abp.notify.info(app.localize('MatchingCorrect'));
                    vm.checkFinished = true;
                    return true;
                }
                else {
                    vm.invoice.checkingStatus = app.localize("MatchingFailed");
                    abp.notify.error(app.localize('MatchingFailed'));
                    vm.checkFinished = true;
                    return false;
                }
            }

            vm.existall = function () {

                if (vm.invoice.id == null) {
                    vm.existall = false;
                    return;
                }

                invoiceService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.invoice.id,
                    operation: 'SEARCH',
                    locationRefId: vm.defaultLocationId
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.invoice.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.invoice.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };


            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.reflocation = [];

            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;

                });
            }

            vm.refpurchasecategory = [];
            function fillDropDownPurchaseCategory() {
                invoiceService.getPurchaseCategoryForCombobox({}).success(function (result) {
                    vm.refpurchasecategory = result.items;

                });

            }

            fillDropDownPurchaseCategory();

            vm.refsupplier = [];

            function fillDropDownSupplier() {
                vm.loading = true;
                supplierService.getSimpleSupplierListDtos({}).success(function (result) {
                    vm.refsupplier = result.items;
                    vm.loading = false;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.getSupplierValue = function (data) {
                vm.invoice.supplierRefId = data.id;
                vm.invoice.supplierTaxApplicableByDefault = data.taxApplicable;
                vm.invoice.invoicePayMode = data.defaultPayModeEnumRefId;
            };

            vm.checkCloseDay = function () {
                vm.loadingCount++;
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.doesDayCloseRunInBackGround == true) {
                        abp.notify.error(app.localize("StillBackGroundProcessing", moment(result.accountDate).format($scope.format)));
                        abp.message.error(app.localize("StillBackGroundProcessing", moment(result.accountDate).format($scope.format)));
                        vm.cancel(0);
                        return;
                    }
                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                    fillDropDownBarCodeList();
                    vm.invoice.accountDate = moment(result.accountDate).format($scope.format);

                    $('input[name="accountDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.invoice.accountDate,
                        maxDate: vm.invoice.accountDate,
                    });

                    $('input[name="invoiceDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.invoice.accountDate,
                        minDate: $scope.minDate,
                        maxDate: moment()
                    });

                    //vm.invoice.dcDate = vm.invoice.accountDate;

                    //$('input[name="dcDate"]').daterangepicker({
                    //    locale: {
                    //        format: $scope.format
                    //    },
                    //    singleDatePicker: true,
                    //    showDropdowns: true,
                    //    startDate: vm.invoice.dcDate,
                    //    //                            minDate: $scope.minDate,
                    //    maxDate: moment()
                    //});

                    vm.setReceiptDate(vm.invoice.accountDate);

                }).finally(function (result) {
                    vm.loadingCount--;
                });
            }


            vm.getPoDetails = function () {
                if (vm.poRefId != null && vm.poRefId > 0) {
                    vm.loading = true;
                    vm.loadingCount++;
                    purchaseorderService.getPurchaseOrderForEdit({
                        Id: vm.poRefId
                    }).success(function (result) {
                        vm.invoice.supplierRefId = result.purchaseOrder.supplierRefId;
                        vm.invoice.supplierTaxApplicableByDefault = result.purchaseOrder.supplierTaxApplicableByDefault;
                        $scope.filterDcDate(vm.dateRangeModel);
                        vm.fillDropDownMaterial();
                        vm.tempinvoicedetail.poRefId = vm.poRefId;
                        vm.inwardPoTranIdRef(vm.tempinvoicedetail.poRefId);
                        vm.getDetailMaterialBasedOnPo();
                        vm.loading = false;
                    }).finally(function (result) {
                        vm.loading = false;
                        vm.loadingCount--;
                    });
                }
            }

            function init() {
                fillDropDownLocation();
                fillDropDownSupplier();
                vm.fillDropDownTaxes();
                vm.detailloading = true;
                vm.errorFlag = true;

                invoiceService.getInvoiceForEdit({
                    Id: vm.invoiceId
                }).success(function (result) {
                    vm.errorFlag = false;
                    vm.invoice = result.invoice;
                    vm.invoice.locationRefId = vm.defaultLocationId;

                    var todayAsString = moment().format('YYYY-MM-DD');

                    vm.dateRangeModel = {
                        startDate: todayAsString,
                        endDate: todayAsString
                    };

                    $scope.filterDcDate(vm.dateRangeModel);

                    if (result.invoice.id != null) {
                        vm.uilimit = null;
                        if (vm.invoice.isDirectInvoice == true) {
                            vm.invoiceType = 'Direct';
                        }

                        vm.invoice.accountDate = moment(result.invoice.accountDate).format($scope.format);
                        vm.invoice.invoiceDate = moment(result.invoice.invoiceDate).format($scope.format);
                        $scope.minDate = vm.invoice.invoiceDate;

                        vm.fillDropDownMaterial();

                        $('input[name="invoiceDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: moment(),
                            maxDate: moment()
                        });

                        vm.invoicedetail = result.invoiceDetail;
                        vm.invoicedirectcreditlink = result.invoiceDirectCreditLink;
                        vm.refdctranid = result.invoiceDirectCreditLink;

                        angular.forEach(vm.invoicedetail, function (value, key) {
                            vm.invoicePortions.push({
                                'sno': value.sno,
                                'dcTranid': value.dcTranId,
                                'barcode': value.barcode,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'totalQty': value.totalQty,
                                'price': value.price,
                                'discountFlag': value.discountFlag,
                                'discountPercentage': value.discountPercentage,
                                'discountAmount': parseFloat(vm.isUndefinedOrNull(value.discountAmount) ? 0 : value.discountAmount),
                                'totalAmount': parseFloat(parseFloat(value.totalAmount).toFixed(2)),
                                'taxAmount': parseFloat(parseFloat(value.taxAmount).toFixed(2)),
                                'netAmount': parseFloat(parseFloat(value.netAmount).toFixed(2)),
                                'remarks': value.remarks,
                                'taxForMaterial': value.taxForMaterial,
                                'uom': value.uom,
                                'unitRefId': value.unitRefId,
                                'changeunitflag': false,
                                'applicableTaxes': value.applicableTaxes
                            });
                        });
                        abp.ui.clearBusy('#invoiceForm');
                        vm.calculateTotalVlaue(vm.invoicePortions);
                        vm.invoice.invoiceamoutenteredbyuser = parseFloat(vm.invoice.invoiceAmount).toFixed(2);
                        vm.checkAmountWithAuto();
                    }
                    else {
                        vm.invoice.invoiceDate = moment().format($scope.format);

                        if (vm.invoiceType == 'Direct')
                            vm.invoice.isDirectInvoice = true;
                        else
                            vm.invoice.isDirectInvoice = false;

                        vm.invoice.isFoc = false;

                        vm.invoice.totalDiscountAmount = 0;
                        vm.invoice.totalShipmentCharges = 0;
                        vm.invoice.roundedAmount = 0;
                        vm.checkCloseDay();
                        vm.uilimit = 20;
                        vm.getPoDetails();
                    }

                    vm.detailloading = false;
                }).finally(function () {
                    if (vm.errorFlag == true)
                        vm.cancel(0);
                });


            }

            init();

            vm.barcodeKeyPress = function (objId, $event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    //alert(objId);
                    $("#rcdqty-" + objId).focus();
                }
            };

            vm.qtyKeyPress = function (objId, $event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    //alert(objId);
                    $("#price-" + objId).focus();
                }
            };

            vm.priceKeyPress = function (objId, $event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    //alert(objId);
                    $("#btnadd-" + objId).focus();
                }
            };

            vm.selectMaterialBasedOnBarcode = function (data, index) {
                if (data.barcode == '' || data.barcode == null)
                    return;

                var forLoopFlag = true;
                angular.forEach(vm.invoicePortions, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(data.barcode, value.barcode) && !angular.equals(data.sno, value.sno)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists') + ' BarCode : ' + data.barcode);
                            if (vm.permissions.barcodeUsage)
                                $("#barcode-" + index).focus();
                            data.barcode = '';
                            forLoopFlag = false;
                        }
                    }
                });
                if (forLoopFlag == false)
                    return;

                vm.uilimit = null;
                var selectedMaterial = null;
                vm.refmaterial.some(function (refdata, refkey) {
                    if (refdata.barcode == data.barcode) {
                        selectedMaterial = refdata;
                        return true;
                    }
                });

                vm.refbarcodelist.some(function (bcdata, bckey) {
                    if (bcdata.barcode == data.barcode) {
                        vm.refmaterial.some(function (matdata, matkey) {
                            if (bcdata.materialRefId == matdata.materialRefId) {
                                selectedMaterial = matdata;
                                selectedMaterial.barcode = data.barcode;
                                return true;
                            }
                        });
                        return true;
                    }
                });


                if (selectedMaterial != null) {
                    $scope.func(data, selectedMaterial, index);
                }
                else {
                    abp.notify.warn(app.localize('Barcode') + ' ' + data.barcode + ' ' + app.localize('NotExist'));
                    data.barcode = '';
                    $("#barcode-" + index).focus();
                }


            }
        }
    ]);
})();

