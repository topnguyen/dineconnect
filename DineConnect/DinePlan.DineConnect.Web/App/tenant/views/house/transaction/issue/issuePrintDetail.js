﻿(function () {
    appModule.controller('tenant.views.house.transaction.issue.issuePrintDetail', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.issue', 'appSession',  'abp.services.app.material',
        
        function ($scope, $filter, $state, $stateParams, $modal, uiGridConstants, issueService, appSession,
           materialService) {

            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            //vm.defaultLocationId = appSession.user.locationRefId;

						vm.connectdecimals = appSession.connectdecimals;
						vm.housedecimals = appSession.housedecimals;
            
            vm.recipePortions = [];
            vm.issueId = $stateParams.printid;

            vm.headerline = "~~~~~~~~~~~~~~~~~~~~";

            vm.getData = function () {
                vm.loading = true;
             issueService.getIssueForEdit({
                    Id: vm.issueId
            }).success(function (result)
            {
                vm.issue = result.issue;
              

                    vm.issuePortions = [];

                    vm.sno = 0;
         
                    vm.recipePortions = [];
                    if (result.issueRecipeDetail != null && result.issueRecipeDetail.length > 0) {
                        vm.recipeflag = true;
                    }
                    else {
                        vm.recipeflag = false;
                        vm.recipename = app.localize("General");
                    }

                    angular.forEach(result.issueRecipeDetail, function (value, key) {
                        vm.recipePortions.push({
                            'sno': value.sno,
                            'recipeRefId': value.recipeRefId,
                            'recipeRefName': value.recipeRefName,
                            'recipeProductionQty': value.recipeProductionQty,
                            'unitRefId': value.unitRefId,
                            'defaultUnit': value.uom,
                            'multipleBatchProductionAllowed': value.multipleBatchProductionAllowed,
                            'completedQty': value.completedQty,
                            'remarks': value.remarks,
                            'completionFlag': value.completionFlag,
                            'currentInHand': value.currentInHand
                        })
                    });


                    vm.issuedetail = result.issueDetail;

                    angular.forEach(vm.issuedetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.issuePortions.push({
                            'sno': value.sno,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'issueQty': value.issueQty,
                            'defaultUnit': value.defaultUnit,
                            'unitRefName' : value.unitRefName
                        });
                    });
                    vm.loading = false;
                });
            }

            vm.getData();

        }]);
})();

