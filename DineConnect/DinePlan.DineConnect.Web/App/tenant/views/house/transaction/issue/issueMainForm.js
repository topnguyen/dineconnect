﻿(function () {
    appModule.controller('tenant.views.house.transaction.issue.issueMainForm', [
        '$scope', '$state', '$stateParams', 'abp.services.app.issue', 'abp.services.app.commonLookup', 'abp.services.app.location', 'appSession', 'abp.services.app.material', 'abp.services.app.request', 'abp.services.app.unitConversion', 'abp.services.app.dayClose', '$uibModal', '$rootScope',
        function ($scope, $state, $stateParams, issueService, commonLookupService, locationService, appSession, materialService, requestService, unitconversionService, daycloseService, $modal, $rootScope) {
            /* eslint-disable */

            var vm = this;

            vm.saving = false;
            vm.issue = null;
            $scope.existall = true;
            vm.issueId = $stateParams.id;
            vm.sno = 0;
            vm.rsno = 0;
            vm.issuePortions = [];
            vm.recipePortions = [];
            vm.loading = true;

            vm.recipeDetail = [];
            vm.issueDetail = [];
            $scope.tokenNumberToolTip = app.localize("TokenNumber");
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.categoryflag = false;
            vm.recipeflag = true;
            vm.uilimit = null;
            vm.productionUnitExistsFlag = appSession.location.isProductionUnitAllowed;

            vm.refproductionunit = [];
            if (vm.productionUnitExistsFlag == true) {
                vm.refproductionunit = appSession.location.productionUnitList;
            }

            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['llll', 'YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];


            $('input[name="issDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });


            vm.save = function (argSaveOption) {
                vm.existall();
                if ($scope.existall == false)
                    return;

                //     var lastelement = vm.issuePortions[vm.issuePortions.length - 1];
                //     if (lastelement.defaultUnitId == lastelement.unitRefId) {
                //         if (parseFloat(lastelement.issueQty) > parseFloat(lastelement.currentInHand)) {
                //             abp.notify.error(app.localize('QuantityShortage',lastelement.materialRefName));
                //             return;
                //         }
                //     }
                //     else {
                //         var convFactor = 0;
                //         var convExistFlag = false;
                //         vm.refconversionunit.some(function (data, key) {
                //             if (data.baseUnitId == lastelement.unitRefId && data.refUnitId == lastelement.defaultUnitId) {
                //                 convFactor = data.conversion;
                //                 convExistFlag = true;
                //                 return true;
                //             }
                //         });


                //         if (convExistFlag == false) {
                //abp.notify.error(app.localize('ConversionFactorNotExist', lastelement.defaultUnitName, lastelement.unitRefName));
                //             return;
                //         }

                //         var curHandConversion = lastelement.currentInHand * 1 / convFactor;
                //         if (parseFloat(lastelement.issueQty) > parseFloat(curHandConversion)) {
                //             abp.notify.error(app.localize('QuantityShortage', lastelement.materialRefName));
                //             if (vm.issue.id==null)
                //                 return;
                //         }

                //     }

                if (vm.productionUnitExistsFlag == true && vm.isUndefinedOrNull(vm.issue.productionUnitRefId) == true) {
                    abp.notify.warn(app.localize('ProductionUnitNotSelectErr'));
                    return;
                }

                vm.saving = true;

                vm.issue.requestTime = vm.issue.issueTime;

                vm.issueRecipeDetail = [];

                vm.errorflag = false;
                vm.sno = 0;

                if (vm.recipeflag == true) {
                    angular.forEach(vm.recipePortions, function (value, key) {
                        if (value.recipeProductionQty == 0) {
                            abp.message.warn(app.localize("ProdQtyShouldNotBeZero", value.recipeRefName));
                            vm.errorflag = true;
                            return;
                        }
                        if (value.recipeProductionQty != 0) {
                            vm.sno++;
                            vm.issueRecipeDetail.push({
                                'sno': vm.sno,
                                'recipeRefId': value.recipeRefId,
                                'recipeRefName': value.recipeRefName,
                                'recipeProductionQty': value.recipeProductionQty,
                                'unitRefId': value.unitRefId,
                                'multipleBatchProductionAllowed': value.multipleBatchProductionAllowed,
                                'completedQty': value.completedQty,
                                'remarks': value.remarks,
                                'completionFlag': value.completionFlag
                            });
                        }
                    });
                }
                if (vm.errorflag == true) {
                    vm.saving = false;
                    return;
                }

                vm.issueDetail = [];

                vm.errorflag = false;
                vm.sno = 0;
                angular.forEach(vm.issuePortions, function (value, key) {
                    if (value.issueQty == 0) {
                        abp.message.warn(app.localize("IssueQtyShouldNotBeZero", value.materialRefName));
                        vm.errorflag = true;
                        return;
                    }
                    if (value.unitRefId == null || value.unitRefId == 0) {
                        abp.message.warn(app.localize("UnitErr", value.materialRefName));
                        vm.errorflag = true;
                        return;
                    }
                    var lastelement = value;
                    if (lastelement.defaultUnitId == lastelement.unitRefId) {
                        if (parseFloat(lastelement.issueQty) > parseFloat(lastelement.currentInHand)) {
                            vm.errorflag = true;
                            abp.notify.error(app.localize('QuantityShortage', lastelement.materialRefName));
                            return;
                        }
                    }
                    else {
                        var convFactor = 0;
                        var convExistFlag = false;
                        vm.refconversionunit.some(function (data, key) {
                            if (data.baseUnitId == lastelement.unitRefId && data.refUnitId == lastelement.defaultUnitId) {
                                convFactor = data.conversion;
                                convExistFlag = true;
                                return true;
                            }
                        });


                        if (convExistFlag == false) {
                            abp.notify.error(app.localize('ConversionFactorNotExist', lastelement.defaultUnit, lastelement.unitRefName));
                            vm.errorflag = true;
                            return;
                        }

                        var curHandConversion = lastelement.currentInHand * 1 / convFactor;
                        if (parseFloat(lastelement.issueQty) > parseFloat(curHandConversion)) {
                            abp.notify.error(app.localize('QuantityShortage', lastelement.materialRefName));
                            vm.errorflag = true;
                            return;
                        }

                    }

                    if (lastelement.unitRefId == 0 || lastelement.unitRefId == null) {
                        abp.notify.error(app.localize('UnitErr', lastelement.materialRefName));
                        vm.errorflag = true;
                        return;
                    }

                    if (value.requestQty != 0) {
                        vm.sno++;
                        vm.issueDetail.push({
                            'sno': vm.sno,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefId,
                            'requestQty': value.issueQty,
                            'issueQty': value.issueQty,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'changeunitflag': false,
                        });
                    }
                });

                if (vm.errorflag == true) {
                    vm.saving = false;
                    return true;
                }

                vm.issue.requestSlipNumber = vm.issue.requestSlipNumber.toUpperCase();

                issueService.createOrUpdateIssue({
                    issue: vm.issue,
                    issueRecipeDetail: vm.issueRecipeDetail,
                    issueDetail: vm.issueDetail,
                }).success(function (result) {
                    abp.notify.error(app.localize('Issue') + ' ' + app.localize('SavedSuccessfully'));
                    abp.message.success(app.localize('IssueSuccessMessage', result.id));
                    if (argSaveOption == 2) {
                        vm.cancel();
                        return;
                    }

                    resetField();
                    var parDt = moment(val).format("DD-MMM-YYYY");
                    vm.setMaxTokenNumber(parDt);

                    $("#defaultLocation").focus();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function resetField() {
                vm.issuePortions = [];
                vm.recipePortions = [];
                vm.issueDetail = [];
                vm.issue.requestPersonId = '';
                vm.issue.tokenNumber = '';
                vm.issue.issueTime = '';
                vm.issue.requestSlipNumber = '';
                vm.issue.locationRefId = '';
                vm.sno = 0;
                vm.recipeflag = false;
                vm.addPortion();
            }

            vm.existall = function () {
                $scope.errmessage = "";
                if ($scope.errmessage != "") {
                    abp.notify.warn($scope.errmessage, 'Required');
                    return false;
                }
                else {
                    return true;
                }
            };

            vm.addPortion = function () {

                if (vm.sno > 0) {
                    if (vm.existall() == false)
                        return;

                    var errorFlag = false;

                    if (vm.productionUnitExistsFlag == true && vm.isUndefinedOrNull(vm.issue.productionUnitRefId) == true) {
                        abp.notify.warn(app.localize('ProductionUnitNotSelectErr'));
                        return;
                    }

                    if (vm.refmaterial.length == vm.issuePortions.length) {
                        abp.notify.info(app.localize('AllTheMaterialAdded'));
                        return;
                    }


                    if (vm.issuePortions.length <= 0) {
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }
                    else {
                        if (vm.isUndefinedOrNull(vm.issuePortions[0].materialRefId) || vm.issuePortions[0].materialRefId == 0) {
                            abp.notify.info(app.localize('MinimumOneDetail'));
                            return;
                        }
                        if (vm.isUndefinedOrNull(vm.issuePortions[0].issueQty) || vm.issuePortions[0].issueQty == 0) {
                            abp.notify.error(app.localize('InvalidIssueQty'));
                            return;
                        }
                    }

                    var lastelement = vm.issuePortions[vm.issuePortions.length - 1];

                    if (lastelement.defaultUnitId == lastelement.unitRefId) {
                        if (parseFloat(lastelement.issueQty) > parseFloat(lastelement.currentInHand)) {
                            abp.notify.error(app.localize('QuantityShortage', lastelement.materialRefName));
                            return;
                        }
                    }
                    else {
                        var convFactor = 0;
                        var convExistFlag = false;
                        vm.refconversionunit.some(function (data, key) {
                            if (data.baseUnitId == lastelement.unitRefId && data.refUnitId == lastelement.defaultUnitId) {
                                convFactor = data.conversion;
                                convExistFlag = true;
                                return true;
                            }
                        });


                        if (convExistFlag == false) {
                            abp.notify.error(app.localize('ConversionFactorNotExist', lastelement.defaultUnit, lastelement.unitRefName));
                            return;
                        }

                        var curHandConversion = lastelement.currentInHand * 1 / convFactor;
                        if (parseFloat(lastelement.issueQty) > parseFloat(curHandConversion)) {
                            abp.notify.error(app.localize('QuantityShortage', lastelement.materialRefName));
                            return;
                        }

                    }

                    if (lastelement.unitRefId == 0 || lastelement.unitRefId == null) {
                        abp.notify.info(app.localize('UnitErr', lastelement.materialRefName));
                        return;
                    }

                    if (errorFlag) {
                        vm.saving = false;
                        vm.loading = false;
                        $scope.existall = false;
                    }

                }


                vm.sno = vm.sno + 1;
                vm.issuePortions.push({
                    'sno': vm.sno, 'materialRefId': 0, 'materialRefName': '', 'issueQty': '', 'currentInHand': '',
                    'defaultUnit': "", 'unitRefId': '', 'unitRefName': '', 'changeunitflag': false,
                });
            }

            vm.addRecipePortion = function () {

                if (vm.rsno > 0) {
                    if (vm.existall() == false)
                        return;

                    var errorFlag = false;

                    if (vm.productionUnitExistsFlag == true && vm.isUndefinedOrNull(vm.issue.productionUnitRefId) == true) {
                        abp.notify.warn(app.localize('ProductionUnitNotSelectErr'));
                        return;
                    }

                    if (vm.refmaterial.length == vm.recipePortions.length) {
                        abp.notify.info(app.localize('AllTheMaterialAdded'));
                        return;
                    }


                    if (vm.recipePortions.length <= 0) {
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }
                    else {
                        if (vm.isUndefinedOrNull(vm.recipePortions[0].recipeRefId) || vm.recipePortions[0].recipeRefId == 0) {
                            abp.notify.info(app.localize('MinimumOneDetail'));
                            return;
                        }
                        if (vm.isUndefinedOrNull(vm.recipePortions[0].recipeProductionQty) || vm.recipePortions[0].recipeProductionQty == 0) {
                            abp.notify.info(app.localize('ProdQtyErr'));
                            return;
                        }
                    }

                    var lastelement = vm.recipePortions[vm.recipePortions.length - 1];

                    if (vm.isUndefinedOrNull(lastelement.recipeRefId) || lastelement.recipeRefId == 0 || lastelement.recipeRefId == '') {
                        abp.notify.info(app.localize('RecipeErr'));
                        return;
                    }

                    if (parseFloat(lastelement.recipeProductionQty) == 0 || lastelement.recipeProductionQty == '') {
                        abp.notify.info(app.localize('ProdQtyErr'));
                        return;
                    }

                    if (errorFlag) {
                        vm.saving = false;
                        vm.loading = false;
                        $scope.existall = false;
                    }

                }


                vm.rsno = vm.rsno + 1;
                vm.recipePortions.push({
                    'sno': vm.rsno,
                    'recipeRefId': 0,
                    'recipeRefName': '',
                    'recipeProductionQty': '',
                    'currentInHand': '',
                    'unitRefId': '',
                    'defaultUnit': "",
                    'multipleBatchProductionAllowed': true,
                    'remarks': '',
                    'completedQty': 0,
                    'completionFlag': false
                });
            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex) {
                if (vm.issuePortions.length > 1) {
                    vm.issuePortions.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                }
                else {
                    vm.issuePortions.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                    abp.notify.info(app.localize('MinimumOneDetail'));
                    vm.addPortion();
                    return;
                }
            }

            vm.removeRecipeRow = function (productIndex) {
                if (vm.recipePortions.length > 1) {
                    vm.recipePortions.splice(productIndex, 1);
                    vm.rsno = vm.rsno - 1;
                }
                else {
                    vm.recipePortions.splice(productIndex, 1);
                    vm.rsno = vm.rsno - 1;
                    vm.addRecipePortion();
                    return;
                }
            }

            $scope.funcIssueTime = function (selDt) {
                if (!vm.isUndefinedOrNull(selDt))
                    vm.setMaxTokenNumber(selDt);
            }

            vm.setMaxTokenNumber = function (val) {
                issueService.getMaxTokenNumber(
                    moment(val).format("DD-MMM-YYYY")
                ).success(function (result) {
                    vm.issue.tokenNumber = result;
                });
            };

            $scope.func = function (data, val, obj) {
                var forLoopFlag = true;

                angular.forEach(vm.issuePortions, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialRefName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.materialRefId = 0;
                    data.materialRefName = '';
                    return;
                }
                data.materialRefName = val.materialRefName;
                data.defaultUnitId = val.defaultUnitId;
                data.defaultUnit = val.defaultUnitName;
                data.currentInHand = val.currentInHand;
                data.unitRefName = val.issueUnitName;
                data.unitRefId = val.issueUnitId;
                //data.changeunitflag = true;
                //data.materialRefId = val.materialRefId;
            };

            $scope.funcRecipe = function (data, val, obj) {
                var forLoopFlag = true;

                angular.forEach(vm.recipePortions, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialRefName, value.recipeRefName)) {
                            abp.notify.info(app.localize('DuplicateRecipeExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.recipeRefId = 0;
                    data.recipeRefName = '';
                    return;
                }
                data.recipeRefName = val.materialRefName;
                data.defaultUnit = val.defaultUnitName;
                data.unitRefId = val.defaultUnitId;
                data.currentInHand = val.currentInHand;
            };

            $scope.RecipeFunc = function (data) {
                var selValue = data.value;
                vm.fillMaterialBasedOnRecipt(selValue);

                issueService.getCategoryIdForRecipe({
                    id: selValue
                }).success(function (result) {
                    vm.issue.materialGroupCategoryRefId = result.id;
                });
            }


            vm.refconversionunit = [];
            vm.fillUnitConversion = function () {
                unitconversionService.getAll({
                }).success(function (result) {
                    vm.refconversionunit = result.items;
                });
            }

            vm.fillUnitConversion();


            vm.refmaterial = [];
            vm.tempRefMaterial = [];

            vm.fillMaterialBasedOnRecipt = function (selValue) {
                issueService.getMaterialForGivenRecipeCombobox({
                    locationRefId: vm.defaultLocationId,
                    recipeRefId: selValue
                }).success(function (result) {
                    vm.refmaterial = result;
                    vm.tempRefMaterial = result;
                });
            }

            vm.filterMaterial = function () {
                //    vm.fillMaterialBasedOnRecipt(null);
            }

            vm.cancel = function () {
                $rootScope.settings.layout.pageSidebarClosed = false;
                $state.go("tenant.issue");
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.reflocation = [];

            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;

                });
            }




            vm.refRecipe = [];
            function fillDropDownRecipe() {
                materialService.getRecipeMaterial({
                    locationRefId: vm.defaultLocationId
                }).success(function (result) {
                    vm.refRecipe = result;
                });
            }

            vm.refrequest = [];
            function fillDropDownRequest() {
                issueService.getRequestSlipNumber({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    vm.refrequest = result.items;
                });
            }

            vm.refmaterialgroupcategory = [];

            function fillDropDownMaterialGroupCategory() {
                vm.loading = true;
                materialService.getMaterialGroupCategoryForCombobox({}).success(function (result) {
                    vm.refmaterialgroupcategory = result.items;
                    ////if (selectedGroupCategoryId != null)
                    ////    vm.material.materialGroupCategoryRefId = selectedGroupCategoryId;
                    vm.loading = false;
                });
            }

            vm.categoryselect = function () {

                if (vm.categoryflag == true) {
                    vm.recipeflag = false;
                }
                else {
                    vm.recipeflag = false;
                    vm.issue.materialGroupCategoryRefId = 0;
                }

            }


            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.getDetailMaterial = function () {

                if (vm.showErrorProductionUnitNotSelected() == false) {
                    return;
                }
                vm.uilimit = null;
                //  Based On Recipe Ingredients
                if (vm.recipeflag == true) {
                    //if (vm.issue.recipeProductionQty == 0 || vm.issue.recipeProductionQty == null) {
                    //    abp.notify.info("EnterProductionQuantity");
                    //    return;
                    //}

                    var errorFlag = false;
                    vm.requestRecipeList = [];
                    angular.forEach(vm.recipePortions, function (value, key) {
                        if (value.recipeRefId == null || value.recipeRefId == '' || value.recipeRefId == 0) {
                            errorFlag = true;
                            abp.notify.warn(app.localize('RecipeErr'));
                            return;
                        }

                        if (value.recipeProductionQty == null || value.recipeProductionQty == '' || value.recipeProductionQty == 0) {
                            errorFlag = true;
                            abp.notify.warn(app.localize('ProdQtyErr'));
                            return;
                        }

                        vm.requestRecipeList.push({
                            'recipeRefId': value.recipeRefId,
                            'requestQtyforProduction': value.recipeProductionQty
                        })
                    });
                    if (errorFlag) {
                        return;
                    }
                    vm.loading = true;
                    issueService.getIssueForRecipe({
                        locationRefId: vm.defaultLocationId,
                        requestRecipeList: vm.requestRecipeList
                    }).success(function (result) {
                        vm.issuePortions = [];
                        vm.sno = 0;
                        angular.forEach(result.issueDetail, function (value, key) {
                            vm.sno = vm.sno + 1;
                            vm.issuePortions.push({
                                'sno': vm.sno,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'issueQty': value.issueQty,
                                'defaultUnit': value.defaultUnitName,
                                'defaultUnitId': value.defaultUnitId,
                                'currentInHand': value.currentInHand,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'changeunitflag': false,
                            })
                        });
                    }).finally(function (result) {
                        vm.loading = false;
                    });;
                }
                else {
                    if (vm.issue.materialGroupCategoryRefId == null || vm.issue.materialGroupCategoryRefId == 0) {
                        abp.message.warn(app.localize('CategorySelectErr'));
                        return;
                    }
                    vm.loading = true;
                    materialService.getIssueForCategory({
                        materialGroupCategoryRefId: vm.issue.materialGroupCategoryRefId,
                        locationRefId: vm.defaultLocationId
                    }).success(function (result) {
                        vm.issuePortions = [];
                        vm.sno = 0;
                        angular.forEach(result.issueDetail, function (value, key) {
                            vm.sno = vm.sno + 1;
                            vm.issuePortions.push({
                                'sno': vm.sno,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'issueQty': value.issueQty,
                                'defaultUnit': value.defaultUnitName,
                                'defaultUnitId': value.defaultUnitId,
                                'currentInHand': value.currentInHand,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'changeunitflag': false,
                            })
                        });
                    }).finally(function (result) {
                        vm.loading = false;
                    });
                }

            }

            vm.checkCloseDay = function () {
                vm.loading = true;
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format('YYYY-MMM-DD')));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format('YYYY-MMM-DD')));

                    }
                    if (moment(result.accountDate).format("YYYY-MMM-DD") == moment().format("YYYY-MMM-DD"))
                        return;


                    vm.issue.issueTime = moment(result.accountDate).format($scope.format);

                    $('input[name="issDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: false,
                        startDate: vm.issue.issueTime,
                        minDate: vm.issue.issueTime,
                        maxDate: vm.issue.issueTime
                    });

                }).finally(function (result) {
                    vm.loading = false;
                });
            }


            function init() {
                vm.loading = true;

                fillDropDownLocation();
                fillDropDownRecipe();
                fillDropDownMaterialGroupCategory();
                fillDropDownRequest();
                vm.fillMaterialBasedOnRecipt(null);

                issueService.getIssueForEdit({
                    Id: vm.issueId
                }).success(function (result) {
                    vm.issue = result.issue;
                    vm.issue.locationRefId = vm.defaultLocationId;
                    //										$rootScope.settings.layout.pageSidebarClosed = true;

                    if (result.issue.id != null) {
                        vm.uilimit = null;
                        vm.issueDetail = result.issueDetail;
                        vm.issue.recipeRefId = result.issueDetail[0].recipeRefId;
                        if (vm.isUndefinedOrNull(vm.issue.recipeRefId)) {
                            vm.recipeflag = false;
                            if (vm.isUndefinedOrNull(vm.issue.materialGroupCategoryRefId) || vm.issue.materialGroupCategoryRefId == 0) {
                                vm.categoryflag = false;
                            }
                            else {
                                vm.categoryflag = true;
                            }

                        }
                        else {
                            vm.recipeflag = true;
                            vm.categoryflag = false;
                        }

                        vm.issue.issueTime = moment(result.issue.issueTime).format($scope.format);
                        $scope.minDate = vm.issue.issueTime;

                        vm.recipePortions = [];
                        if (result.issueRecipeDetail != null && result.issueRecipeDetail.length > 0) {
                            vm.recipeflag = true;
                        }
                        else {
                            vm.recipeflag = false;
                        }

                        var errorFlag = false;
                        var errorMessage = '';

                        angular.forEach(result.issueRecipeDetail, function (value, key) {
                            if (value.completedQty > 0) {
                                errorFlag = true;
                                errorMessage = value.recipeRefName + ' ' + value.completedQty + ' ' + value.uom;
                                return;
                            }
                            vm.recipePortions.push({
                                'sno': value.sno,
                                'recipeRefId': value.recipeRefId,
                                'recipeRefName': value.recipeRefName,
                                'recipeProductionQty': value.recipeProductionQty,
                                'unitRefId': value.unitRefId,
                                'defaultUnit': value.uom,
                                'multipleBatchProductionAllowed': value.multipleBatchProductionAllowed,
                                'completedQty': value.completedQty,
                                'remarks': value.remarks,
                                'completionFlag': value.completionFlag,
                                'currentInHand': value.currentInHand
                            })
                        });
                        vm.rsno = vm.recipePortions.length;
                        if (errorFlag) {
                            abp.message.warn(app.localize('IssueConvertedAsProductionErr') + ' ' + errorMessage);
                            $state.go('tenant.issueprint', {
                                printid: vm.issueId
                            });
                            return;
                        }

                        angular.forEach(result.issueDetail, function (value, key) {
                            vm.issuePortions.push({
                                'sno': value.sno,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'issueQty': value.issueQty,
                                'defaultUnit': value.defaultUnitName,
                                'defaultUnitId': value.defaultUnitId,
                                'currentInHand': value.currentInHand,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'changeunitflag': false,
                            })
                        });
                        vm.sno = vm.issuePortions.length;
                    }
                    else {
                        vm.issue.issueTime = moment().format($scope.format);
                        vm.addRecipePortion();
                        vm.addPortion();
                        vm.checkCloseDay();
                        vm.uilimit = 20;
                        if (vm.refproductionunit != null && vm.refproductionunit.length == 1) {
                            vm.issue.productionUnitRefId = vm.refproductionunit[0].id;
                        }
                    }
                });
            }

            init();

            vm.showErrorProductionUnitNotSelected = function () {
                if (vm.productionUnitExistsFlag == true && vm.isUndefinedOrNull(vm.issue.productionUnitRefId) == true) {
                    abp.notify.warn(app.localize('ProductionUnitNotSelectErr'));
                    return false;
                }
                return true;
            }

            vm.removeUnfilled = function () {
                var listtoremoved = [];
                tempportions = [];
                vm.loading = true;
                vm.sno = 0;
                angular.forEach(vm.issuePortions, function (value, key) {
                    if (value.issueQty == 0 || value.issueQty == null || value.issueQty == "") {
                        listtoremoved.push(key)
                    }
                    else {
                        vm.sno++;
                        value.sno = vm.sno;
                        tempportions.push(value);
                    }
                });

                vm.issuePortions = tempportions;

                vm.sno = vm.issuePortions.length;

                if (vm.issuePortions.length == 0)
                    vm.addPortion();

                vm.loading = false;
            }

            vm.getRequestData = function (item) {
                var reqId = item.value;
                vm.loading = true;
                vm.uilimit = null;

                requestService.getRequestForEdit({
                    Id: reqId
                }).success(function (result) {
                    vm.issue.requestSlipNumber = result.request.requestSlipNumber;
                    vm.issue.requestTime = moment(result.request.requestTime).format($scope.format);
                    vm.issue.multipleBatchProductionAllowed = result.request.multipleBatchProductionAllowed;
                    vm.issue.productionUnitRefId = result.request.productionUnitRefId;

                    if (result.requestRecipeDetail.length == 0) {
                        vm.issue.materialGroupCategoryRefId = result.request.materialGroupCategoryRefId;
                        vm.categoryflag = true;
                        vm.recipeflag = false;
                    }
                    else if (result.request.materialGroupCategoryRefId != null) {
                        vm.recipeflag = true;
                        vm.categoryflag = false;
                        vm.recipePortions = [];
                        angular.forEach(result.requestRecipeDetail, function (value, key) {
                            vm.recipePortions.push({
                                'sno': value.sno,
                                'recipeRefId': value.recipeRefId,
                                'recipeRefName': value.recipeRefName,
                                'recipeProductionQty': value.recipeProductionQty,
                                'unitRefId': value.unitRefId,
                                'defaultUnit': value.uom,
                                'multipleBatchProductionAllowed': value.multipleBatchProductionAllowed,
                                'completedQty': value.completedQty,
                                'remarks': value.remarks,
                                'completionFlag': value.completionFlag,
                                'currentInHand': value.currentInHand
                            })
                        });

                        vm.rsno = vm.recipePortions.length;

                    }

                    vm.issuePortions = [];
                    angular.forEach(result.requestDetail, function (value, key) {
                        vm.issuePortions.push({
                            'sno': value.sno,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'requestQty': value.requestQty,
                            'issueQty': value.requestQty,
                            'defaultUnit': value.defaultUnitName,
                            'defaultUnitId': value.defaultUnitId,
                            'currentInHand': value.currentInHand,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'changeunitflag': false,
                        })
                    });

                    vm.sno = vm.issuePortions.length;

                    vm.loading = false;
                });

            }


            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

            vm.unitReference = function (selected, data) {
                data.unitRefId = selected.value;
                data.unitRefName = selected.displayText;
                data.changeunitflag = false;
            }


            vm.importFromExcel = function () {
                //fillDropDownMaterial();
                importModal(null);
            };

            function importModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/issue/importIssue.cshtml',
                    controller: 'tenant.views.house.transaction.intertransfer.directapproval.importIssue as vm',
                    backdrop: 'static',
                    resolve: {
                        requestLocationRefId: function () {
                            return vm.defaultLocationId;
                        },
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.loading = true;
                    vm.issuePortions = [];
                    vm.uilimit = null;
                    var totalErrorMessage = "";

                    vm.sno = 1;
                    angular.forEach(result, function (value, key) {
                        vm.issuePortions.push({
                            'sno': vm.sno++,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'issueQty': value.issueQty,
                            'requestQty': value.issueQty,
                            'defaultUnit': value.defaultUnit,
                            'defaultUnitId': value.defaultUnitId,
                            'currentInHand': value.currentInHand,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'changeunitflag': false,
                        });

                        var lastelement = value;
                        if (lastelement.defaultUnitId == lastelement.unitRefId) {
                            if (parseFloat(lastelement.issueQty) > parseFloat(lastelement.currentInHand)) {
                                vm.errorflag = true;
                                abp.notify.error(app.localize('QuantityShortage', lastelement.materialRefName));
                                return;
                            }
                        }
                        else {
                            var convFactor = 0;
                            var convExistFlag = false;
                            vm.refconversionunit.some(function (data, key) {
                                if (data.baseUnitId == lastelement.unitRefId && data.refUnitId == lastelement.defaultUnitId) {
                                    convFactor = data.conversion;
                                    convExistFlag = true;
                                    return true;
                                }
                            });


                            if (convExistFlag == false) {
                                abp.notify.error(app.localize('ConversionFactorNotExist', lastelement.defaultUnit, lastelement.unitRefName));
                                vm.errorflag = true;
                                return;
                            }

                            var curHandConversion = lastelement.currentInHand * 1 / convFactor;
                            if (parseFloat(lastelement.issueQty) > parseFloat(curHandConversion)) {
                                abp.notify.error(app.localize('QuantityShortage', lastelement.materialRefName));
                                vm.errorflag = true;
                                return;
                            }

                        }

                    });

                    if (vm.recipeflag == true) {
                        if (vm.recipePortions.length == 1) {
                            var lastrecipe = vm.recipePortions[0];
                            if (lastrecipe.recipeRefId == 0 || lastrecipe.recipeRefName == '')
                                vm.recipeflag = false;
                        }
                    }

                    $("#tokenNumber").focus();


                    vm.loading = false;
                });
            }




        }
    ]);
})();


