﻿(function () {
    appModule.controller('tenant.views.house.transaction.intertransfer.directapproval.importIssue', [
        '$scope', 'appSession', '$uibModalInstance', 'FileUploader', 'requestLocationRefId', 'abp.services.app.material', 'abp.services.app.location', 'abp.services.app.issue', 'appSession',
        function ($scope, appSession, $uibModalInstance, fileUploader, requestLocationRefId, materialService, locationService, issueService) {
            var vm = this;
            vm.loading = false;

            vm.requestLocationRefId = requestLocationRefId;
            vm.stockTakenDate = moment();
            vm.allItems = [];
            vm.defaultLocationId = appSession.user.locationRefId;

            vm.locationRefId = null;

            vm.uploader = new fileUploader({
                url: abp.appPath + 'Import/ImportIssue',
                formData: [
                    {
                        "locationRefId": vm.defaultLocationId
                    },
                ],
                queueLimit: 1,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        if ('|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ImportTemplate_Warn_FileType'));
                            return false;
                        }
                        return true;
                    }
                }]
            });

            vm.refmaterialtype = [];
            function fillDropDownMaterialType() {
                vm.loading = true;
                materialService.getMaterialTypeForCombobox({}).success(function (result) {
                    vm.refmaterialtype = result.items;
                    vm.loading = false;
                });
            }

            fillDropDownMaterialType();

            vm.materialListFor = null;

            vm.getIssueTemplate = function () {
                vm.saving = true;
                vm.loading = true;
                issueService.importIssueTemplate({
                    locationRefId: vm.defaultLocationId,
                    materialListFor: vm.materialListFor
                }).success(function (result) {
                    app.downloadTempFile(result);
                }).finally(function () {
                    vm.loading = false;
                    vm.saving = false;
                });
            };

            vm.consolidatedExcel = function () {
                vm.saving = true;
                vm.loading = true;
                intertransferService.getConsolidatedToExcel({
                    requestList: vm.userGridOptions.data
                }).success(function (result) {
                    app.downloadTempFile(result);
                }).finally(function () {
                    vm.loading = false;
                    vm.saving = false;
                });
            };

            vm.save = function () {
                if (vm.stockTakenDate == null) {
                    abp.notify.warn(app.localize("SelectStockTakenDateErr"));
                    return;
                }
                if (vm.uploader.queue.length == 0) {
                    abp.notify.warn(app.localize('YouAreNotSelectAFile'));
                    return;
                }
                vm.loading = true;
                vm.uploader.uploadAll();
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.datafromexcel = [];

            vm.uploader.onErrorItem = function (fileItem, response, status, headers) {
                if (response.success) {
                    // Do nothing
                } else {
                    abp.message.error(response);
                    abp.notify.error(response);
                    vm.loading = false;
                    vm.saving = false;
                    return;
                }
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.error != null) {
                    abp.message.warn(response.error.message);
                    $uibModalInstance.dismiss();
                    vm.loading = false;
                    return;
                }
                vm.allItems = [];
                vm.datafromexcel = response.result;
                if (vm.datafromexcel == null) {
                    if (response.error != null) {
                        abp.message.warn(response.error.message);
                        vm.readfromexcelstatus = false;
                        vm.cancel();
                        return;
                    }
                }

                if (vm.datafromexcel != null) {
                    vm.sno = 0;
                    vm.issuePortions = [];
                    angular.forEach(vm.datafromexcel, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.issuePortions.push({
                            'sno': vm.sno,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'issueQty': value.issueQty,
                            'requestQty': value.issueQty,
                            'defaultUnit': value.defaultUnit,
                            'defaultUnitId': value.defaultUnitId,
                            'currentInHand': value.onHand,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.uom,
                            'changeunitflag': false,
                        })
                    });
                }
                else {
                    vm.issuePortions = [];
                }

                abp.notify.info(app.localize("FINISHED"));
                vm.loading = false;
                $uibModalInstance.close(vm.issuePortions);

            };
        }

    ]);
})();
