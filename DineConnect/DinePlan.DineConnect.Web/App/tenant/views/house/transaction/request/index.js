﻿(function () {
    appModule.controller('tenant.views.house.transaction.request.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.request', 'appSession', 'abp.services.app.location', 'abp.services.app.dayClose',
        function ($scope, $state, $modal, uiGridConstants, requestService, appSession, locationService, daycloseService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.defaultLocationId = appSession.user.locationRefId;
            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.isProductionAllowed = appSession.location.isProductionAllowed;
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Transaction.Request.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Transaction.Request.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Transaction.Request.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.advancedFiltersAreShown = false;
            vm.dateFilterApplied = false;

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };


            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editRequest(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteRequest(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.printRequest(row.entity)\">" + app.localize("Print") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Id'),
                        field: 'id'
                    },
                    {
                        name: app.localize('RequestRef'),
                        field: 'requestSlipNumber'
                    },
                    {
                        name: app.localize('RequestTime'),
                        field: 'requestTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                    },
                    {
                        name: app.localize('CompletedStatus'),
                        field: 'completedStatus',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.completedStatus" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.completedStatus" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',

                    },

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.printRequest = function (myObject) {
                $state.go('tenant.requestprint', {
                    printid: myObject.id
                });
            };

            vm.clear = function () {
                vm.filterText = null;
                vm.dateRangeModel = null;
                vm.dateFilterApplied = false;
                vm.getAll();
            }

            vm.clear = function () {
                vm.filterText = null;
                vm.dateRangeModel = null;
                vm.dateFilterApplied = false;
                vm.getAll();
            }

            vm.getAll = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var requestSlipNumber = null;

                if (vm.advancedFiltersAreShown) {
                    if (vm.dateFilterApplied) {

                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                    // requestSlipNumber = vm.requestSlipNumber;
                }

                requestService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    // requestSlipNumber: requestSlipNumber,
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editRequest = function (myObj) {
                if (myObj.completedStatus == true) {
                    abp.message.warn(app.localize("RequestConvertedAsProductionErr"));
                    return;
                }
                openCreateOrEditModal(myObj.id);
            };

            vm.createRequest = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteRequest = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteRequestWarning', myObject.requestSlipNumber),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            requestService.deleteRequest({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.requestdetail", {
                    id: objId
                });

            }

            vm.exportToExcel = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var requestSlipNumber = null;

                if (vm.advancedFiltersAreShown) {
                    if (vm.dateFilterApplied) {
                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                    requestSlipNumber = vm.requestSlipNumber;
                }
                requestService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    requestSlipNumber: requestSlipNumber,
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            };


            vm.getAll();

            vm.checkCloseDay = function () {
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        vm.permissions.create = false;
                        abp.notify.error(app.localize('RequestError1'));

                        // $state.go('tenant.dayclose');
                        return false;
                    }
                    else {
                        return true;
                    }

                }).finally(function () {
                });
            }

            vm.checkCloseDay();

            function init() {
                vm.productionUnitExistsFlag = appSession.location.isProductionUnitAllowed;
                if (vm.productionUnitExistsFlag == true) {
                    vm.columnPrdUnitRefName =
                        {
                            name: app.localize('ProductionUnit'),
                            field: 'productionUnitRefName'
                        };
                    vm.userGridOptions.columnDefs.splice(2, 0, vm.columnPrdUnitRefName);
                }
            };

            init();


        }]);
})();

