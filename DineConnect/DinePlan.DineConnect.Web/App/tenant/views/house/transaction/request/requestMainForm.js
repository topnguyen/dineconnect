﻿
(function () {
    appModule.controller('tenant.views.house.transaction.request.requestMainForm', [
        '$scope', '$state', '$stateParams', 'abp.services.app.request', 'abp.services.app.commonLookup', 'abp.services.app.location', 'appSession', 'abp.services.app.material', 'abp.services.app.issue', 'abp.services.app.unitConversion',
        function ($scope, $state, $stateParams, requestService, commonLookupService, locationService, appSession, materialService, issueService, unitconversionService) {

            var vm = this;
            vm.saving = false;
            vm.request = null;
            $scope.existall = true;
            vm.requestId = $stateParams.id;
            vm.sno = 0;
            vm.rsno = 0;
            vm.requestPortions = [];
            vm.recipePortions = [];

            vm.recipeflag = true;

            vm.requestDetail = [];
            $scope.tokenNumberToolTip = app.localize("TokenNumber");
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;

            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");

            vm.categoryflag = false;
            vm.recipeflag = false;
            vm.uilimit = null;
            vm.productionUnitExistsFlag = appSession.location.isProductionUnitAllowed;

            vm.refproductionunit = [];
            if (vm.productionUnitExistsFlag == true) {
                vm.refproductionunit = appSession.location.productionUnitList;
            }

            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['llll','YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];


            $('input[name="issDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });


            vm.save = function (argSaveOption)
            {
                vm.existall();
                if ($scope.existall == false)
                    return;

                if (vm.productionUnitExistsFlag == true && vm.isUndefinedOrNull(vm.request.productionUnitRefId) == true) {
                    abp.notify.warn(app.localize('ProductionUnitNotSelectErr'));
                    return;
                }

                vm.saving = true;
                
                vm.request.requestTime = vm.request.requestTime;


                vm.errorflag = false;
                vm.requestRecipeDetail = [];

                if (vm.recipeflag == true) {
                    angular.forEach(vm.recipePortions, function (value, key) {
                        if (value.recipeProductionQty == 0) {
                            abp.message.warn(app.localize("ProdQtyShouldNotBeZero", value.recipeRefName));
                            vm.errorflag = true;
                            return;
                        }
                        if (value.recipeProductionQty != 0) {
                            vm.requestRecipeDetail.push({
                                'sno': value.sno,
                                'recipeRefId': value.recipeRefId,
                                'recipeRefName': value.recipeRefName,
                                'recipeProductionQty': value.recipeProductionQty,
                                'unitRefId': value.unitRefId,
                                'multipleBatchProductionAllowed': value.multipleBatchProductionAllowed,
                                'remarks': value.remarks,
                            });
                        }
                    });
                }
                if (vm.errorflag == true) {
                    vm.saving = false;
                    return;
                }

                vm.requestDetail = [];
                vm.errorflag = false;
                angular.forEach(vm.requestPortions, function (value, key) {
                    if (value.requestQty == 0)
                    {
                        abp.message.warn(app.localize("RequestQtyShouldNotBeZero", value.materialRefName));
                        vm.errorflag = true;
                        return;
                    }
                    if (value.requestQty != 0) {
                        vm.requestDetail.push({
                            'sno': value.sno,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefId,
                            'requestQty': value.requestQty,
                            'currentInHand': value.currentInHand,
                            'unitRefId': value.unitRefId,
                            'unitRefName': value.unitRefName,
                            'defaultUnitId': value.defaultUnitId,
                            'defaultUnitName' : value.defaultUnitName,
                            'changeunitflag': false,
                        });
                    }
                });

                if (vm.errorflag == true) {
                    vm.saving = false;
                    return;
                }

                vm.request.requestSlipNumber = vm.request.requestSlipNumber.toUpperCase();

                requestService.createOrUpdateRequest({
                    request: vm.request,
                    requestRecipeDetail : vm.requestRecipeDetail,
                    requestDetail: vm.requestDetail
                }).success(function (result) {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    abp.message.success(app.localize('RequestSuccessMessage', result.id));
                    if (argSaveOption == 2)
                    {
                        vm.cancel();
                        return;
                    }

                    resetField();
                    var parDt = moment(val).format("DD-MMM-YYYY");
                    vm.setMaxTokenNumber(parDt);
                    
                    $("#defaultLocation").focus();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function resetField()
            {
                vm.requestPortions = [];
                vm.recipePortions = [];
                vm.requestDetail = [];
                vm.request.requestPersonId = '';
                vm.request.requestTime = '';
                vm.request.requestSlipNumber = '';
                vm.request.locationRefId = '';
                vm.sno = 0;
                vm.recipeflag = false;
                vm.addPortion();
                vm.addRecipePortion();
            }

            vm.existall = function () {
                $scope.errmessage = "";
                if ($scope.errmessage != "") {
                    abp.notify.warn($scope.errmessage, 'Required');
                    return false;
                }
                else {
                    return true;
                }
            };

            vm.addPortion = function ()
            {

                if (vm.sno > 0)
                {
                    if(vm.existall() == false)
                        return;

                    var errorFlag = false;

                    if (vm.productionUnitExistsFlag == true && vm.isUndefinedOrNull(vm.request.productionUnitRefId) == true) {
                        abp.notify.warn(app.localize('ProductionUnitNotSelectErr'));
                        return;
                    }

                    if (vm.refmaterial.length == vm.requestPortions.length)
                    {
                        errorFlag = true;
                        abp.notify.info(app.localize('AllTheMaterialAdded'));
                        return;
                    }

                    if (vm.requestPortions.length <= 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }
                    else {
                        if (vm.isUndefinedOrNull(vm.requestPortions[0].materialRefId) || vm.requestPortions[0].materialRefId == 0) {
                            errorFlag = true;
                            abp.notify.info(app.localize('MinimumOneDetail'));
                            return;
                        }
                        if (vm.isUndefinedOrNull(vm.requestPortions[0].requestQty) || vm.requestPortions[0].requestQty == 0) {
                            errorFlag = true;
                            abp.notify.info(app.localize('QtyZeroErr'));
                            return;
                        }
                    }

                    if(errorFlag)
                    {
                        vm.saving = false;
                        vm.loading = false;
                        $scope.existall = false;
                    }

                }


                vm.sno = vm.sno + 1;
                vm.requestPortions.push({
                 'sno':vm.sno,'materialRefId':0,'materialRefName':'','requestQty':'', 'currentInHand' : '',  'defaultUnitName': "", 'changeunitflag' : false
                });
            }

            vm.addRecipePortion = function () {

                if (vm.rsno > 0) {
                    if (vm.existall() == false)
                        return;

                    var errorFlag = false;

                    if (vm.productionUnitExistsFlag == true && vm.isUndefinedOrNull(vm.request.productionUnitRefId) == true) {
                        abp.notify.warn(app.localize('ProductionUnitNotSelectErr'));
                        return;
                    }

                    if (vm.refmaterial.length == vm.recipePortions.length) {
                        abp.notify.info(app.localize('AllTheMaterialAdded'));
                        return;
                    }


                    if (vm.recipePortions.length <= 0) {
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }
                    else {
                        if (vm.isUndefinedOrNull(vm.recipePortions[0].recipeRefId) || vm.recipePortions[0].recipeRefId == 0) {
                            abp.notify.info(app.localize('MinimumOneDetail'));
                            return;
                        }
                        if (vm.isUndefinedOrNull(vm.recipePortions[0].recipeProductionQty) || vm.recipePortions[0].recipeProductionQty == 0) {
                            abp.notify.info(app.localize('ProdQtyErr'));
                            return;
                        }
                    }

                    var lastelement = vm.recipePortions[vm.recipePortions.length - 1];

                    if (vm.isUndefinedOrNull(lastelement.recipeRefId) || lastelement.recipeRefId == 0 || lastelement.recipeRefId == '') {
                        abp.notify.info(app.localize('RecipeErr'));
                        return;
                    }

                    if (parseFloat(lastelement.recipeProductionQty) == 0 || lastelement.recipeProductionQty == '') {
                        abp.notify.info(app.localize('ProdQtyErr'));
                        return;
                    }

                    if (errorFlag) {
                        vm.saving = false;
                        vm.loading = false;
                        $scope.existall = false;
                    }

                }


                vm.rsno = vm.rsno + 1;
                vm.recipePortions.push({
                    'sno': vm.rsno,
                    'recipeRefId': 0,
                    'recipeRefName': '',
                    'recipeProductionQty': '',
                    'currentInHand': '',
                    'unitRefId': '',
                    'defaultUnitName': "",
                    'multipleBatchProductionAllowed': true,
                    'remarks': '',
                });
            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex)
            {
                if(vm.requestPortions.length>1)
                {
                    vm.requestPortions.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                }
                else
                {
                    abp.notify.info(app.localize('MinimumOneDetail'));
                    return;
                }
            }

            vm.removeRecipeRow = function (productIndex) {
                if (vm.recipePortions.length > 1) {
                    vm.recipePortions.splice(productIndex, 1);
                    vm.rsno = vm.rsno - 1;
                }
                else {
                    vm.recipePortions.splice(productIndex, 1);
                    vm.rsno = vm.rsno - 1;
                    vm.addRecipePortion();
                    return;
                }
            }

            $scope.funcIssueTime=function(selDt)
            {
                //if (!vm.isUndefinedOrNull(selDt))
                //    vm.setMaxTokenNumber(selDt);
            }


            $scope.func = function (data, val,obj) {
                var forLoopFlag = true;

                angular.forEach(vm.requestPortions, function (value, key)
                {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialRefName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag)
                {
                    data.materialRefId = 0;
                    data.materialRefName = '';
                    return;
                }
                data.materialRefName = val.materialRefName;
                data.currentInHand = val.currentInHand;
                data.unitRefName = val.issueUnitName;
                data.unitRefId = val.issueUnitId;
                data.defaultUnitId = val.defaultUnitId;
                data.defaultUnitName = val.defaultUnitName;

            };

            $scope.funcRecipe = function (data, val, obj) {
                var forLoopFlag = true;

                angular.forEach(vm.recipePortions, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialRefName, value.recipeRefName)) {
                            abp.notify.info(app.localize('DuplicateRecipeExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.recipeRefId = 0;
                    data.recipeRefName = '';
                    return;
                }
                data.recipeRefName = val.materialRefName;
                data.defaultUnitName = val.defaultUnitName;
                data.unitRefId = val.defaultUnitId;
                data.currentInHand = val.currentInHand;
            };

            $scope.RecipeFunc=function(data)
            {
                var selValue = data.value;
                vm.fillMaterialBasedOnRecipt(selValue);

                requestService.getCategoryIdForRecipe({
                    id : selValue
                }).success(function (result)
                {
                    vm.request.materialGroupCategoryRefId = result.id;
                });
            }

            vm.refmaterial = [];
            vm.tempRefMaterial = [];

            vm.fillMaterialBasedOnRecipt = function (selValue) {
                requestService.getMaterialForGivenRecipeCombobox({
                    locationRefId: vm.defaultLocationId,
                    recipeRefId: selValue
                }).success(function (result) {
                    vm.refmaterial = result;
                    vm.tempRefMaterial = result;
                });
            }

            vm.filterMaterial = function () {
                vm.fillMaterialBasedOnRecipt(null);
            }

            vm.cancel = function () {
                $state.go("tenant.request");
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.reflocation = [];

            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;
                
                });
            }

            vm.refRecipe = [];
            function fillDropDownRecipe()
            {
                materialService.getRecipeMaterial({
                    locationRefId: vm.defaultLocationId
                }).success(function (result) {
                    vm.refRecipe = result;
                });
            }

            vm.refmaterialgroupcategory = [];

            function fillDropDownMaterialGroupCategory() {
                vm.loading = true;
                materialService.getMaterialGroupCategoryForCombobox({}).success(function (result) {
                    vm.refmaterialgroupcategory = result.items;
                    ////if (selectedGroupCategoryId != null)
                    ////    vm.material.materialGroupCategoryRefId = selectedGroupCategoryId;
                    vm.loading = false;
                });
            }

            vm.categoryselect = function () {
              
                if (vm.categoryflag == true) {
                    vm.recipeflag = false;
                }
                else
                {
                    vm.recipeflag = false;
                    vm.request.materialGroupCategoryRefId = 0;
                }

            }

        
            vm.isUndefinedOrNull =function(val)
            {
                return angular.isUndefined(val) || val == null;
            };

            vm.getDetailMaterial = function () {
                if (vm.showErrorProductionUnitNotSelected() == false) {
                    return;
                }

                vm.uilimit = null;
                //  Based On Recipe Ingredients
                if (vm.recipeflag == true) {

                    var errorFlag = false;
                    vm.requestRecipeList = [];
                    angular.forEach(vm.recipePortions, function (value, key) {
                        if (value.recipeRefId == null || value.recipeRefId == '' || value.recipeRefId == 0) {
                            errorFlag = true;
                            abp.notify.warn(app.localize('RecipeErr'));
                            return;
                        }

                        if (value.recipeProductionQty == null || value.recipeProductionQty == '' || value.recipeProductionQty == 0) {
                            errorFlag = true;
                            abp.notify.warn(app.localize('ProdQtyErr'));
                            return;
                        }

                        vm.requestRecipeList.push({
                            'recipeRefId': value.recipeRefId,
                            'requestQtyforProduction': value.recipeProductionQty
                        })
                    });

                    if (errorFlag) {
                        return;
                    }

                    vm.loading = true;

                    issueService.getIssueForRecipe({
                        locationRefId: vm.defaultLocationId,
                        requestRecipeList: vm.requestRecipeList
                    }).success(function (result) {
                        vm.requestPortions = [];
                        vm.sno = 0;
                        angular.forEach(result.issueDetail, function (value, key) {
                            vm.sno = vm.sno + 1;
                            vm.requestPortions.push({
                                'sno': vm.sno,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'requestQty': value.issueQty,
                                'currentInHand': value.currentInHand,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'defaultUnitId': value.defaultUnitId,
                                'defaultUnitName': value.defaultUnitName,
                                'changeunitflag': false,
                            })
                        });
                        vm.loading = false;
                    });
                }
                else
                {
                    if (vm.request.materialGroupCategoryRefId==null || vm.request.materialGroupCategoryRefId==0)
                    {
                        abp.message.warn(app.localize('CategorySelectErr'));
                        return;
                    }

                    materialService.getIssueForCategory({
                        materialGroupCategoryRefId: vm.request.materialGroupCategoryRefId,
                        locationRefId: vm.defaultLocationId
                    }).success(function (result) {
                        vm.requestPortions = [];
                        vm.sno = 0;
                        angular.forEach(result.issueDetail, function (value, key) {
                            vm.sno = vm.sno + 1;
                            vm.requestPortions.push({
                                'sno': vm.sno,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'requestQty': value.requestQty,
                                'currentInHand': value.currentInHand,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'defaultUnitId': value.defaultUnitId,
                                'defaultUnitName': value.defaultUnitName,
                                'changeunitflag': false,
                            })
                        });
                    });
                }

            }



            function init() {
                abp.ui.setBusy('#requestForm');
                fillDropDownLocation();
                fillDropDownRecipe();
                fillDropDownMaterialGroupCategory();
                vm.fillMaterialBasedOnRecipt(null);

                requestService.getRequestForEdit({
                    Id: vm.requestId
                }).success(function (result)
                {
                    vm.request = result.request;                    
                    vm.request.locationRefId = vm.defaultLocationId;

                    if (result.request.id != null) {
                        vm.uilimit = null;
                        vm.requestDetail = result.requestDetail;
                   

                        vm.request.requestTime = moment(result.request.requestTime).format($scope.format);
                        $scope.minDate = vm.request.requestTime;
                            
                    

                        vm.recipePortions = [];
                        if (result.requestRecipeDetail != null && result.requestRecipeDetail.length > 0) {
                            vm.recipeflag = true;
                            vm.categoryflag = false;
                        }
                        else {
                            vm.recipeflag = false;
                            vm.categoryflag = true;
                        }

                        angular.forEach(result.requestRecipeDetail, function (value, key) {
                            vm.recipePortions.push({
                                'sno': value.sno,
                                'recipeRefId': value.recipeRefId,
                                'recipeRefName': value.recipeRefName,
                                'recipeProductionQty': value.recipeProductionQty,
                                'unitRefId': value.unitRefId,
                                'defaultUnitName': value.uom,
                                'multipleBatchProductionAllowed': value.multipleBatchProductionAllowed,
                                'remarks': value.remarks,
                                'currentInHand': value.currentInHand
                            })
                        });
                        vm.rsno = vm.recipePortions.length;

                        angular.forEach(result.requestDetail, function (value, key) {
                            vm.requestPortions.push({
                                'sno': value.sno,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'requestQty': value.requestQty,
                                'currentInHand': value.currentInHand,
                                'unitRefId': value.unitRefId,
                                'unitRefName': value.unitRefName,
                                'defaultUnitId': value.defaultUnitId,
                                'defaultUnitName': value.defaultUnitName,
                                'changeunitflag': false,
                            })
                        });
                        vm.sno = vm.requestPortions.length;
                    }
                    else
                    {
                        vm.request.requestTime = moment().format($scope.format);
                        vm.addPortion();
                        vm.addRecipePortion();
                        vm.uilimit = 20;
                        vm.recipeflag = true;
                        if (vm.refproductionunit != null && vm.refproductionunit.length == 1) {
                            vm.request.productionUnitRefId = vm.refproductionunit[0].id;
                        }
                    }
                });
                abp.ui.clearBusy('#requestForm');
            }

            init();

            vm.showErrorProductionUnitNotSelected = function () {
                if (vm.productionUnitExistsFlag == true && vm.isUndefinedOrNull(vm.request.productionUnitRefId) == true) {
                    abp.notify.warn(app.localize('ProductionUnitNotSelectErr'));
                    return false;
                }
                return true;
            }

           
            vm.removeUnfilled = function () {
                var listtoremoved = [];
                tempportions = [];

                angular.forEach(vm.requestPortions, function (value, key) {
                    if (value.requestQty == 0 || value.requestQty == null || value.requestQty == "") {
                        listtoremoved.push(key)
                    }
                    else {
                        tempportions.push(value);
                    }
                });

                vm.requestPortions = tempportions;

                vm.sno = vm.requestPortions.length;

                if (vm.requestPortions.length == 0)
                    vm.addPortion();
            }

            vm.refconversionunit = [];
            vm.fillUnitConversion = function () {
                unitconversionService.getAll({
                }).success(function (result) {
                    vm.refconversionunit = result.items;
                });
            }

            vm.fillUnitConversion();


            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.unitRefId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

            vm.unitReference = function (selected, data) {
                data.unitRefId = selected.value;
                data.unitRefName = selected.displayText;
                data.changeunitflag = false;
            }


        }
    ]);
})();

