﻿(function () {
    appModule.controller('tenant.views.house.transaction.request.requestPrintDetail', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.request', 'appSession',  'abp.services.app.material',
        
        function ($scope, $filter, $state, $stateParams, $modal, uiGridConstants, requestService, appSession,
           materialService) {

            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;
            //vm.defaultLocationId = appSession.user.locationRefId;

						vm.connectdecimals = appSession.connectdecimals;
						vm.housedecimals = appSession.housedecimals;
            

            vm.requestId = $stateParams.printid;

            vm.headerline = "~~~~~~~~~~~~~~~~~~~~";

            vm.getData = function () {
                vm.loading = true;
             requestService.getRequestForEdit({
                    Id: vm.requestId
            }).success(function (result)
            {
                vm.request = result.request;
                if (vm.request.recipeRefId != null)
                {
                    materialService.getMaterialNameForParticularCode(
                        {
                            id: vm.request.recipeRefId
                        }).success(function (result) {
                            vm.recipename = result;
                        });
                    vm.expectedproductionqty = parseFloat(vm.request.recipeProductionQty);
                }
                else
                {
                    vm.recipename = app.localize("General");
                    vm.expectedproductionqty = 0;
                }
                    //vm.requestdetail = result.requestDetail;

                    vm.requestPortions = [];

                    vm.sno = 0;
                    //vm.subtotalamount = 0;
                    //vm.subtaxamount = 0;
                    //vm.subnetamount = 0;

                    vm.requestdetail = result.requestDetail;
                    //vm.requestdirectcreditlink = result.requestDirectCreditLink;
                    //vm.refdctranid = result.requestDirectCreditLink;

                    angular.forEach(vm.requestdetail, function (value, key) {
                        vm.sno = vm.sno + 1;
                        //vm.subtotalamount = vm.subtotalamount + value.totalAmount;
                        //vm.subtaxamount = vm.subtaxamount + value.taxAmount;
                        //vm.subnetamount = vm.subnetamount + value.netAmount;
                        vm.requestPortions.push({
                            'sno': value.sno,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'requestQty': value.requestQty,
                            'unitRefName': value.unitRefName
                        });
                    });
                    vm.loading = false;
                });
            }

            vm.getData();

        }]);
})();

