﻿(function () {
    appModule.controller('tenant.views.house.numbering.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.dineHouseNumbering',
        function ($scope, $state, $modal, uiGridConstants, numberingService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.isDeleted = false;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-click=\"grid.appScope.editNumbering(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-click=\"grid.appScope.deleteNumbering(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('Active'),
                        field: 'active'
                    },
                    {
                        name: app.localize('LocationPrefix'),
                        field: 'locationPrefix'
                    },
                    {
                        name: app.localize('Prefix'),
                        field: 'prefix'
                    },
                    {
                        name: app.localize('Length'),
                        field: 'length'
                    },
                    {
                        name: app.localize('Suffix'),
                        field: 'suffix'
                    },
                    {
                        name: app.localize('Begining'),
                        field: 'begining'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                numberingService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    location: vm.location,
                    isDeleted: vm.isDeleted,
                    locationGroup: vm.locationGroup
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editNumbering = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createNumbering = function () {
                openCreateOrEditModal(null);
            };

            vm.deleteNumbering = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteNumberingWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            numberingService.deleteDineHouseNumbering({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.activateItem = function (myObject) {
                numberingService.activateItem({
                    id: myObject.id
                }).success(function (result) {
                    abp.notify.success(app.localize("Successfully"));
                    vm.getAll();
                });
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.createnumbering", {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                numberingService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    location: vm.location,
                    isDeleted: vm.isDeleted
                }).success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

          
            vm.clear = function () {
                vm.filterText = null;
                vm.getAll();
            };

            vm.getAll();
        }]);
})();