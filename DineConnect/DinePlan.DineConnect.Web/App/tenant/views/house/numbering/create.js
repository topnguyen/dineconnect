﻿(function () {
    appModule.controller('tenant.views.house.numbering.create', [
        '$scope', '$state', '$uibModal', '$stateParams', 'abp.services.app.dineHouseNumbering',
        function ($scope, $state, $modal, $stateParams, numberingService    ) {
            var vm = this;
            vm.numberingId = $stateParams.id;
            vm.saving = false;
            vm.numbering = null;
            vm.save = function () {
                vm.saving = true;
                numberingService.createOrUpdateDineHouseNumbering(vm.numbering ).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.numbering');
            };
            function init() {
                numberingService.getDineHouseNumberingForEdit({
                    Id: vm.numberingId
                }).success(function (result) {
                    vm.numbering = result;
                    console.log('numbering', result)
                });
            }
            init();
        }
    ]);
})();