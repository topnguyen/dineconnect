﻿(function () {
    appModule.controller('tenant.views.house.master.material.supplierledgerModal', [
        '$scope', '$uibModalInstance', '$uibModal', 'materialRefId', 'abp.services.app.material',
        function ($scope, $modalInstance, $modal, materialRefId, materialService) {
            var vm = this;
            vm.materialRefId = materialRefId;

            vm.getDetails = function () {
                vm.loading = true;
                materialService.getMaterialForEdit({
                    Id: vm.materialRefId
                }).success(function (result) {
                    vm.materialSupplierData = result.supplierMaterial;
                    vm.material = result.material;
                    vm.loading = false;
                });
            }

            vm.getDetails();
            
            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();