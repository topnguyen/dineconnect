﻿(function () {
    appModule.controller('tenant.views.house.master.material.materialdetail', [
        '$scope', '$filter', '$state', '$stateParams',
        '$uibModal', 'abp.services.app.material', 'abp.services.app.commonLookup', 'abp.services.app.unit',
        'abp.services.app.materialBrandsLink', 'uiGridConstants'
        , 'abp.services.app.materialIngredient', 'abp.services.app.supplierMaterial', 'appSession', 'abp.services.app.materialMenuMapping', 'abp.services.app.supplier', "lookupModal", 'abp.services.app.unitConversion',
        function ($scope, $filter, $state, $stateParams,
            $modal, materialService, commonLookupService, unitService, materialbrandslinkService,
            uiGridConstants, materialingredientService, suppliermaterialService, appSession, materialmenumappingService, supplierService, lookupModal, unitconversionService) {
            /* eslint-disable */
            var vm = this;
            vm.saving = false;
            vm.material = null;
            vm.recipe = null;

            vm.defaultAveragePriceTagRefId = abp.setting.getInt("App.House.DefaultAveragePriceTagRefId");
            vm.noOfMonthAvgTaken = abp.setting.getInt("App.House.NoOfMonthAvgTaken");

            vm.currentmaterialuom = "";
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;

            vm.permissions = {
                barcodeUsage: abp.auth.hasPermission('Pages.Tenant.House.Master.Material.Barcode'),
                changeActiveStatus : abp.auth.hasPermission(
                'Pages.Tenant.House.Master.Material.ChangeActiveStatus')
            };

            var countryName = abp.features.getValue('DinePlan.DineConnect.Connect.Country');
            vm.gstenabledforIndia = false;
            if (countryName == 'IN') {
                vm.gstenabledforIndia = true;
            }

            vm.unitRefIds = null;
            vm.filterText = null;
            vm.stockCycleRefIds = null;
            vm.loading = false;
            materialId = $stateParams.id;
            vm.callingFrom = $stateParams.callingFrom;
            vm.changeDefaultUnitFlag = false;
            vm.canchangeDefaultUnit = false;

            if ($stateParams.changeUnitFlag == true || $stateParams.changeUnitFlag == "true") {
                vm.canchangeDefaultUnit = true;
                vm.changeDefaultUnitFlag = true;
            }
            else
                vm.changeDefaultUnitFlag = false;

            vm.oldDefaultUnitId = null;
            vm.defaultUnitRetained = null;
            vm.brandRefIds = null;
            vm.tempdetaildata = [];
            vm.tempcustomerdetaildata = [];
            vm.temptagpricedata = [];
            vm.materialingredientdata = [];
            vm.autoserialnumber = null;
            vm.serialnumberproceduretorun = false;

            vm.refingrunitformaterial = [];

            //vm.materialTypeId = null;

            vm.sno = 0;
            vm.csno = 0;
            vm.isno = 0;

            //  Material Ingredients

            vm.getIngrDetailData = function () {
                vm.loading = true;
                vm.filterText = null;

                materialingredientService.getViewByRefRecipeId({
                    Id: vm.material.id
                }).success(function (result) {
                    vm.materialingredientdata = [];
                    //                    vm.recipeingredient = result.items;
                    vm.isno = 0;
                    angular.forEach(result.items, function (value, key) {
                        vm.isno = vm.isno + 1;
                        vm.materialingredientdata.push({
                            "sno": vm.isno,
                            "userSerialNumber": value.userSerialNumber,
                            "id": 0,
                            "materialRefId": value.materialRefId,
                            "materialRefName": value.materialRefName,
                            "materialUsedQty": value.materialUsedQty,
                            "unitRefId": value.unitRefId,
                            "unitRefName": value.unitRefName,
                            "variationflagForProduction": value.variationflagForProduction,
                            "materialTypeName": value.materialTypeName,
                            "ingreditflag": false
                        });
                    });

                }).finally(function () {
                    vm.loading = false;
                    vm.fillDropDownMaterial();
                });
            };

            vm.refmaterial = [];

            vm.fillDropDownMaterial = function () {
                materialService.getView({
                    skipCount: requestParams.skipCount,
                    maxResultCount: 1,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    operation: 'FillAll',
                }).success(function (result) {
                    if (vm.material.id != null) {   //  Ingridient Should Not refere the Same Material Id
                        result.items.some(function (value, key) {
                            if (value.id == vm.material.id) {
                                result.items.splice(key, 1);
                                return true;
                            }
                        });
                    }
                    vm.refmaterial = result.items;
                });
            }

            vm.editIngr = function (data) {
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data);
                angular.forEach(vm.materialingredientdata, function (value, key) {
                    vm.materialingredientdata[key].ingreditflag = false;
                });

                data.ingreditflag = !data.ingreditflag;

            }

            vm.ingraddPortion = function () {
                if (vm.isno > 0) {



                    var lastelement = vm.materialingredientdata[vm.materialingredientdata.length - 1];


                    if (lastelement.materialRefName == null | lastelement.materialRefName == "") {
                        abp.notify.info(app.localize('MatRequired'));
                        $("#selMaterial").focus();
                        return;
                    }

                    if (lastelement.materialUsedQty == 0 || lastelement.materialUsedQty == null || lastelement.materialUsedQty == "") {
                        abp.notify.info(app.localize('QtyRequired'));
                        $("#rcdqty").focus();
                        return;
                    }

                    if (lastelement.unitRefId == 0 || lastelement.unitRefId == null) {
                        abp.notify.info(app.localize('UnitRequired'));
                        $("#selUnit").focus();
                        return;
                    }


                }

                angular.forEach(vm.materialingredientdata, function (value, key) {
                    vm.materialingredientdata[key].ingreditflag = false;
                });



                //addportions
                vm.isno = vm.isno + 1;
                vm.materialingredientdata.push(
                    {
                        'sno': vm.isno,
                        'userSerialNumber': vm.isno,
                        'id': 0,
                        'materialRefId': '',
                        'materialRefName': '',
                        'materialUsedQty': '',
                        'unitRefId': 0,
                        'unitRefName': '',
                        'variationflagForProduction': 0,
                        "materialTypeName": "",
                        "ingreditflag": true
                    });
                $("#selMaterial1").focus();
            }

            vm.ingrremoveRow = function (productIndex) {

                if (vm.materialingredientdata.length > 1) {
                    vm.materialingredientdata.splice(productIndex, 1);
                    vm.isno = vm.isno - 1;
                }

                else {
                    vm.materialingredientdata.splice(productIndex, 1);
                    vm.isno = 0;
                    vm.ingraddPortion();
                    return;
                }
            }

            $scope.ingrfunc = function (data, val) {
                var forLoopFlag = true;

                angular.forEach(vm.materialingredientdata, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.materialRefId = 0;
                    data.materialRefName = '';
                    return;
                }

                data.materialRefName = val.materialName;
                data.materialTypeName = val.materialTypeName;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data);
            };

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.refingrunitformaterial = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refingrunitformaterial = result.items;
                    if (result.items.length == 1) {
                        data.unitRefId = result.items[0].value;
                        data.unitRefName = result.items[0].displayText;
                    }
                    else {
                        if (!vm.material.id) {
                            data.unitRefId = null;
                            data.unitRefName = null;
                        }
                    }
                });
            }


            // End ------------------------- Material Ingredients

            vm.OwnPreparationSelection = function () {
                if (vm.material.materialTypeId == 0) {
                    vm.material.ownPreparation = false;
                }
                else {
                    vm.material.ownPreparation = true;
                }
                if (vm.material.ownPreparation == true) {
                    vm.material.isBranded = false;
                    vm.material.isQuoteNeededForPurchase = false;
                }
            }

            $scope.funcSupplier = function (data, val, argsupplierId) {
                var forLoopFlag = true;

                angular.forEach(vm.tempdetaildata, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val, value.supplierRefName)) {
                            abp.notify.info(app.localize('DuplicateSupplierExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.supplierRefId = 0;
                    data.supplierRefName = '';
                    return;
                }
                data.supplierRefName = val;
            };

            $scope.funcCustomer = function (data, val, argcustomerId) {
                var forLoopFlag = true;

                angular.forEach(vm.tempcustomerdetaildata, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val, value.customerRefName)) {
                            abp.notify.info(app.localize('DuplicateCustomerExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.customerRefId = 0;
                    data.customerRefName = '';
                    return;
                }
                data.customerRefName = val;
            };

            vm.removeRow = function (productIndex) {
                var element = vm.tempdetaildata[productIndex];

                if (vm.tempdetaildata.length > 1) {
                    vm.tempdetaildata.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                }
                else {
                    vm.tempdetaildata.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                    vm.addPortion();
                }
                //vm.materialRefresh();
            }

            vm.removeCustomerRow = function (productIndex) {
                var element = vm.tempcustomerdetaildata[productIndex];

                if (vm.tempcustomerdetaildata.length > 1) {
                    vm.tempcustomerdetaildata.splice(productIndex, 1);
                    vm.csno = vm.csno - 1;
                }
                //vm.materialRefresh();
            }

            vm.addUnitConversionPortion = function () {
                vm.unitConversionList.push({
                    'materialRefId': materialId,
                    'baseUnitId': 0, 'baseUom': '',
                    'refUnitId': '', 'refUom': '',
                    'conversion': '', 'decimalPlaceRounding': 14,
                    'id': null, 'editFlag': true,
                    'defaultConversion': 0, 'defaultDecimalPlaceRounding': 0,
                });
            }



            vm.saveUnitConversionPortion = function (productIndex) {
                if (vm.unitConversionList.length > 0) {

                    var lastelement = vm.unitConversionList[productIndex];

                    if (lastelement.baseUnitId == null || lastelement.baseUnitId == "" || lastelement.baseUnitId == 0) {
                        abp.notify.error(app.localize('baseUnitId'));
                        return;
                    }

                    if (lastelement.refUnitId == null || lastelement.refUnitId == "" || lastelement.refUnitId == 0) {
                        abp.notify.error(app.localize('refUnitId'));
                        lastelement.conversion = 0;
                        return;
                    }

                    if (lastelement.baseUnitId == lastelement.refUnitId) {
                        abp.notify.error(app.localize('BaseAndRefUnitCanNotBeSame'));
                        abp.message.error(app.localize('BaseAndRefUnitCanNotBeSame'));
                        lastelement.conversion = 0;
                        return;
                    }

                    if (lastelement.conversion == 0 || lastelement.conversion == "" || lastelement.conversion == null) {
                        abp.notify.error(app.localize('Conversion'));
                        lastelement.conversion = 0;
                        return;
                    }

                    if (lastelement.decimalPlaceRounding == 0 || lastelement.decimalPlaceRounding == "" || lastelement.decimalPlaceRounding == null) {
                        abp.notify.error(app.localize('DecimalPlaceRounding'));
                        lastelement.conversion = 0;
                        return;
                    }

                    var sameReverseConversationExist = false;
                    angular.forEach(vm.unitConversionList, function (value, key) {
                        if (key != productIndex && (value.id == null || value.id == 0)) {
                            if (value.baseUnitId == lastelement.refUnitId && value.refUnitId == lastelement.baseUnitId) {
                                abp.notify.error(app.localize('ReverseBaseAndRefUnitCanNotBeRepeat', value.baseUom, value.refUom));
                                abp.message.error(app.localize('ReverseBaseAndRefUnitCanNotBeRepeat', value.baseUom, value.refUom));
                                sameReverseConversationExist = true;
                            }
                        }
                    });
                    if (sameReverseConversationExist == true) {
                        lastelement.conversion = 0;
                        return;
                    }
                    lastelement.editFlag = false;
                }

                //angular.forEach(vm.unitConversionList, function (value, key) {
                //    value.editFlag = false;
                //});
            }



            vm.removeUnitConversionPortion = function (index, data) {
                if (data.id > 0) {
                    var supplierNameList = '';
                    if (data.linkedSupplierList != null && data.linkedSupplierList.length > 0) {
                        angular.forEach(data.linkedSupplierList, function (value, key) {
                            supplierNameList = supplierNameList + value.name + ' ,';
                        });
                        supplierNameList = ' - ' +  supplierNameList.substring(0, supplierNameList.length - 1);
                    }

                    abp.message.confirm(
                        app.localize('DeleteUnitConversionMaterialWarning', vm.material.materialName + ' ' + supplierNameList),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                unitconversionService.deleteUnitConversionMaterial({
                                    unitConversion: data
                                }).success(function () {
                                    abp.notify.success(app.localize('SuccessfullyDeleted'));
                                    vm.getLatestUnitConversion();
                                });
                            }
                        }
                    );
                }
                else {
                    if (vm.unitConversionList.length > 1) {
                        vm.unitConversionList.splice(productIndex, 1);
                    }
                    else if (vm.unitConversionList.length == 1)
                    {
                        vm.unitConversionList = [];
                    }
                    else {
                        vm.unitConversionList.splice(productIndex, 1);
                        return;
                    }
                }
            }

            vm.changeBaseRefUnit = function (data) {
                data.editFlag = true;
            }

            vm.baseUnitNameReference = function (data, item) {
                data.baseUom = item.displayText;
            }

            vm.refUnitNameReference = function (data, item) {
                vm.loadingCount++;
                data.refUom = item.displayText;
                materialService.getDefaultUnitConversionDataForGivenData({
                    baseUnitId: data.baseUnitId,
                    refUnitId: data.refUnitId
                }).success(function (result) {
                    data.defaultConversion = result.conversion;
                    data.defaultDecimalPlaceRounding = result.decimalPlaceRounding;
                    if (result.id == null || result.id == 0 || result.conversion == 0) {
                        abp.notify.info(app.localize('NoDefaultConversionBetweenUom', data.baseUom, data.refUom));
                        //abp.message.error(app.localize('NoDefaultConversionBetweenUom', data.baseUom, data.refUom));
                    }
                }).finally(function () {
                    vm.loadingCount--;
                });
            }

            vm.addPortion = function () {
                if (vm.sno > 0) {

                    //if (vm.existAllElements() == false)
                    //    return;

                    var lastelement = vm.tempdetaildata[vm.tempdetaildata.length - 1];

                    if (lastelement.supplierRefName == null || lastelement.supplierRefName == "") {
                        abp.notify.info(app.localize('SupplierRequired'));
                        $("#selSupplier").focus();
                        return;
                    }

                    if (lastelement.materialPrice == 0 || lastelement.materialPrice == null || lastelement.materialPrice == "") {
                        abp.notify.info(app.localize('PriceRequired'));
                        $("#lastprice").focus();
                        return;
                    }

                    if (lastelement.unitRefId == 0 || lastelement.unitRefId == null) {
                        abp.notify.error(app.localize('UnitForSupplier'));
                        return;
                    }

                    vm.removeSupplier(lastelement);
                }

                vm.sno = vm.sno + 1;
                vm.tempdetaildata.push({
                    'materialRefId': 0, 'supplierRefId': '', 'supplierRefName': '', 'materialPrice': '', 'id': 0,
                    'unitRefId': vm.material.defaultUnitId,
                    'uom': vm.currentmaterialuom,
                    'minimumOrderQuantity': '',
                    'yieldPercentage': null,
                    'supplierMaterialAliasName': ''
                });

                $("#selSupplier").focus();
            }

            vm.getMaterialLocationWiseStatus = function () {
                vm.loading = true;
                materialService.getMaterialActiveStatusLocationWise({
                    id : vm.material.id
                }).success(function (result) {
                    vm.materialActiveStatusLocationWise = result;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.ChangeActiveStatus = function (data) {
                vm.loading = true;
                materialService.changeMaterialActiveStatusInLocation({
                    materialRefId: vm.material.id,
                    locationRefId : data.locationRefId,
                    isActiveInLocation: !data.isActiveInLocation
                }).success(function (result) {
                    if (result != null) {
                        data.isActiveInLocation = !data.isActiveInLocation;
                        abp.notify.info(app.localize('SavedSuccessfully'));
                    }
                }).finally(function () {
                    //vm.getMaterialLocationWiseStatus();
                    vm.loading = false;
                });
                
            }

            vm.addCustomerPortion = function () {
                if (vm.csno > 0) {

                    var lastelement = vm.tempcustomerdetaildata[vm.tempcustomerdetaildata.length - 1];

                    if (lastelement.customerRefName == null || lastelement.customerRefName == "") {
                        abp.notify.info(app.localize('CustomerRequired'));
                        $("#selCustomer").focus();
                        return;
                    }

                    if (lastelement.materialPrice == 0 || lastelement.materialPrice == null || lastelement.materialPrice == "") {
                        abp.notify.info(app.localize('PriceRequired'));
                        $("#lastprice").focus();
                        return;
                    }

                    vm.removeSupplier(lastelement);
                }

                vm.csno = vm.csno + 1;
                vm.tempcustomerdetaildata.push({ 'materialRefId': 0, 'customerRefId': '', 'customerRefName': '', 'materialPrice': '', 'id': 0 });

                $("#selCustomer").focus();
            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeSupplier = function (objRemove) {
                var indexPosition = -1;
                angular.forEach(vm.temprefSupplierName, function (value, key) {
                    if (value.displayText == objRemove.supplierRefName)
                        indexPosition = key;
                });

                if (indexPosition >= 0)
                    vm.temprefSupplierName.splice(indexPosition, 1);

            }

            $scope.focusUnit = function () {
                setTimeout(function () {
                    angular.element('#unitRefIds').focus()
                });
            };


            $scope.focusBrand = function () {
                setTimeout(function () {
                    angular.element('#bd').focus()
                });
            };

            vm.materialUnitsLink_DetailList = null;

            $scope.existall = true;

            vm.save = function (argOption) {
                if ($scope.existall == false)
                    return;

                vm.tempUnitConversionList = [];
                vm.errorFlag = false;
                angular.forEach(vm.unitConversionList, function (value, key) {
                    if (value.editFlag == true)
                        vm.errorFlag = true;
                });
                if (vm.errorFlag == true) {
                    abp.notify.warn(app.localize('UnitConversionEditSavePendingAlert'));
                    abp.message.warn(app.localize('UnitConversionEditSavePendingAlert'));
                    vm.unittab = true;
                    vm.saving = false;
                    return;
                }
                vm.unitConversionList = vm.tempUnitConversionList;

                angular.forEach(vm.tempcustomerdetaildata, function (value, key) {
                    if (parseFloat(value.materialPrice) > 0 && parseInt(value.customerRefId) > 0) {
                        vm.customerdetaildata.push({
                            "materialRefId": value.materialRefId,
                            "customerRefId": value.customerRefId,
                            "materialPrice": value.materialPrice,
                            "LastQuoteDate": "2016-04-22",
                            "LastQuoteRefNo": "Default",
                        });
                    }
                });

                $scope.errmessage = "";

                if (vm.material.defaultUnitId == 0 || vm.material.defaultUnitId == null)
                    $scope.errmessage = $scope.errmessage + app.localize('UnitNilErr');

                if (vm.material.issueUnitId == 0 || vm.material.issueUnitId == null)
                    $scope.errmessage = $scope.errmessage + app.localize('Transaction') + ' ' + app.localize('UnitNilErr');

                if (vm.material.materialGroupCategoryRefId == 0 || vm.material.materialGroupCategoryRefId == null)
                    $scope.errmessage = $scope.errmessage + app.localize('MaterialGroupCategoryNilErr');

                if (vm.material.isBranded == 0 || vm.material.isBranded == null)
                    vm.brandRefIds = null;

                if (vm.brandRefIds != null) {
                    if ((vm.brandRefIds.length == 0 || vm.brandRefIds == null) && vm.material.isBranded == true)
                        $scope.errmessage = $scope.errmessage + app.localize('BrandListEmptyErr');
                }

                if (vm.material.materialTypeId == null) {
                    $scope.errmessage = $scope.errmessage + app.localize('MaterialTypeErr');
                }

                if ($scope.errmessage != "") {
                    abp.notify.error($scope.errmessage, 'Required');
                    return;
                }

                if (vm.material.materialTypeId != 0) {

                    $scope.errmessage = "";
                    if (parseInt(vm.recipe.alarmTimeInMinutes) > parseInt(vm.recipe.lifeTimeInMinutes))
                        $scope.errmessage = $scope.errmessage + app.localize('AlarmMismatchErr', vm.recipe.alarmTimeInMinutes);

                    if (parseInt(vm.recipe.prdBatchQty) == 0 || vm.recipe.prdBatchQty == '' || vm.recipe.prdBatchQty == null)
                        $scope.errmessage = $scope.errmessage + app.localize('PrdQuantityErr');

                    if (vm.materialingredientdata[0].materialRefName == '' || vm.materialingredientdata[0].materialRefId == '' || vm.materialingredientdata[0].materialUsedQty == '') {
                        abp.notify.warn(app.localize('IngredientEmptyErr'));
                    }
                    else {
                        var lastindex = vm.materialingredientdata.length - 1;
                        if (vm.materialingredientdata[lastindex].materialRefName == '' || vm.materialingredientdata[lastindex].materialRefId == '' || vm.materialingredientdata[lastindex].materialUsedQty == '')
                            $scope.errmessage = $scope.errmessage + app.localize('IngredientEmptyErr');
                    }

                    if ($scope.errmessage != "") {
                        abp.notify.warn($scope.errmessage);
                        return;
                    }
                }
                else {
                    vm.material.wipeOutStockOnClosingDay = false;
                }

                if (vm.material.averagePriceTagRefId == 2 && (vm.material.noOfMonthAvgTaken == 0 || vm.material.noOfMonthAvgTaken == '')) {
                    abp.notify.error(app.localize('Number') + ' ' + app.localize('Of') + ' ' + app.localize('Months') + " ?");
                    vm.saving = false;
                    vm.loading = false;
                    return;
                }

                if (vm.material.averagePriceTagRefId != 2) {
                    vm.material.noOfMonthAvgTaken = 0;
                }

                vm.errorFlag = false;
                vm.supplierdetaildata = [];
                if (vm.material.ownPreparation == false) {
                    angular.forEach(vm.tempdetaildata, function (value, key) {
                        if (parseFloat(value.materialPrice) > 0 && parseInt(value.supplierRefId) > 0) {
                            if (value.unitRefId == null || value.unitRefId == 0) {
                                abp.notify.error(app.localize('UnitForSupplier'));
                                vm.errorFlag = true;
                            }
                            vm.supplierdetaildata.push({
                                "materialRefId": value.materialRefId,
                                "supplierRefId": value.supplierRefId,
                                "materialPrice": value.materialPrice,
                                'minimumOrderQuantity': value.minimumOrderQuantity,
                                'unitRefId': value.unitRefId,
                                "uom": value.uom,
                                "LastQuoteDate": new Date(),
                                "LastQuoteRefNo": "Default",
                                "yieldPercentage": value.yieldPercentage,
                                'supplierMaterialAliasName': value.supplierMaterialAliasName
                            });
                        }
                    });
                }
                else {
                    vm.supplierdetaildata = [];
                    vm.material.isQuoteNeededForPurchase = false;
                    vm.material.isBranded = false;
                }

                if (vm.errorFlag == true) {
                    vm.loading = false;
                    vm.saving = false
                    return;
                }

                vm.customerdetaildata = [];

                angular.forEach(vm.tempcustomerdetaildata, function (value, key) {
                    if (parseFloat(value.materialPrice) > 0 && parseInt(value.customerRefId) > 0) {
                        vm.customerdetaildata.push({
                            "materialRefId": value.materialRefId,
                            "customerRefId": value.customerRefId,
                            "materialPrice": value.materialPrice,
                            "LastQuoteDate": "2016-04-22",
                            "LastQuoteRefNo": "Default",
                        });
                    }
                });

                vm.customertagmaterialprice = [];

                angular.forEach(vm.temptagpricedata, function (value, key) {
                    vm.customertagmaterialprice.push({
                        "materialRefId": value.materialRefId,
                        "tagCode": value.tagCode,
                        "materialPrice": value.materialPrice
                    });
                });

                vm.ingredientsdetaildata = [];

                if (parseInt(vm.material.materialTypeId) == 0)
                    vm.materialingredientdata = [];

                var errorFlag = false;

                angular.forEach(vm.materialingredientdata, function (value, key) {

                    if (value.materialRefId > 0 && (vm.isUndefinedOrNull(value.unitRefId) || value.unitRefId == 0)) {
                        errorFlag = true;
                        abp.notify.info(app.localize('Ingredient') + ' ' + app.localize('Unit'));
                        return;
                    }

                    if (value.materialRefId > 0 && (vm.isUndefinedOrNull(value.materialUsedQty) || value.materialUsedQty == '')) {
                        errorFlag = true;
                        abp.notify.info(app.localize('Ingredient') + ' ' + app.localize('Quantity'));
                        return;
                    }


                    if (parseFloat(value.materialUsedQty) > 0 && parseInt(value.materialRefId) > 0) {
                        vm.ingredientsdetaildata.push({
                            "sno": vm.sno,
                            "userSerialNumber": value.userSerialNumber,
                            "id": 0,
                            "materialRefId": value.materialRefId,
                            "materialRefName": value.materialRefName,
                            "materialUsedQty": value.materialUsedQty,
                            "unitRefId": value.unitRefId,
                            "unitRefName": value.unitRefName,
                            "VariationflagForProduction": value.variationflagForProduction
                        });
                    }
                });

                if (errorFlag) {
                    vm.saving = false;
                    return;
                }



                vm.saving = true;
                vm.material.materialPetName = vm.material.materialPetName.toUpperCase();
                vm.material.materialName = vm.material.materialName.toUpperCase();

                if (parseFloat(vm.autoserialnumber) != parseFloat(vm.material.userSerialNumber)) {
                    vm.serialnumberproceduretorun = true;
                }

                materialService.createOrUpdateMaterial({
                    material: vm.material,
                    materialUnitsLink_DetailList: vm.materialUnitsLink_DetailList,
                    unitList: vm.unitRefIds,
                    unitConversionList: vm.unitConversionList,
                    supplierMaterial: vm.supplierdetaildata,
                    customerMaterial: vm.customerdetaildata,
                    brandList: vm.brandRefIds,
                    customerTagMaterialPrice: vm.customertagmaterialprice,
                    materialIngredient: vm.ingredientsdetaildata,
                    materialRecipeType: vm.recipe,
                    updateSerialNumberRequired: vm.serialnumberproceduretorun,
                    stockCycle: vm.stockCycleRefIds
                }).success(function (result) {

                    abp.notify.info(app.localize('SavedSuccessfully'));
                    //$modalInstance.close();
                    vm.tempdetaildata = [];
                    vm.tempcustomerdetaildata = [];
                    vm.supplierdetaildata = [];
                    vm.unitConversionList = [];
                    vm.customerdetaildata = [];
                    vm.temptagpricedata = [];
                    vm.autoserialnumber = null;
                    vm.serialnumberproceduretorun = false;
                    if (argOption == 1)
                        $state.go('tenant.material');
                    else {
                        materialId = result.id;
                        init();
                    }
                }).finally(function () {

                    vm.saving = false;
                });
            };

            vm.changeDefaultUnit = function () {
                if (vm.changeDefaultUnitFlag == true) {
                    if (vm.oldDefaultUnitId == vm.material.defaultUnitId) {
                        abp.notify.error(app.localize('NoChangeInDefaultUnit'));
                        return;
                    }

                    materialService.defaultUnitChangeProcess({
                        materialRefId: vm.material.id,
                        existUnitRefId: vm.oldDefaultUnitId,
                        revisedUnitRefId: vm.material.defaultUnitId
                    }).success(function (result) {
                        if (result.menuMappingExist == true) {
                            if (result.outputMessage != null) {
                                abp.notify.info(result.outputMessage);
                                abp.notify.error(app.localize('NeedsToChangeTheMenuMapping'));
                                //abp.message.info(result.outputMessage);
                                app.downloadTempFile(result.excelFile);
                            }
                        }
                        $state.go('tenant.material');
                    }).finally(function () {
                        vm.saving = false;
                    });
                    return;
                }
            }

            vm.cancel = function () {
                $state.go('tenant.material');
                //$modalInstance.dismiss();
            };

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };


            vm.existall = function () {

                if (vm.material.materialName == null) {
                    vm.existall = false;
                    return;
                }

                materialService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'materialName',
                    filter: vm.material.materialName,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.material.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.material.materialName + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editMaterialBrandsLink(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteMaterialBrandsLink(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },

                    {
                        name: app.localize('Id'),
                        field: 'id'
                    },
                    {
                        name: app.localize('Material Name'),
                        field: 'materialRefName'
                    },
                    {
                        name: app.localize('Brand Name'),
                        field: 'brandRefName'
                    },


                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAllBrands();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAllBrands();
                    });
                },
                data: []
            };



            vm.getAllBrands = function () {
                vm.loading = true;
                materialbrandslinkService.getView({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.deleteMaterialBrandsLink = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteMaterialBrandsLinkWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            materialbrandslinkService.deleteMaterialBrandsLink({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.openCategoryMaster = function () {
                openCreateCategoryMaster(null);
            };

            selectedGroupCategoryId = null;


            function openCreateCategoryMaster(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/materialgroupcategory/createOrEditModal.cshtml',
                    controller: 'tenant.views.house.master.materialgroupcategory.createOrEditModal as vm',
                    backdrop: 'static',
                    resolve: {
                        materialgroupcategoryId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    selectedGroupCategoryId = result.id;
                }).finally(function () {
                    fillDropDownMaterialGroupCategory();
                });
            };


            vm.openBrandMaster = function () {
                openCreateBrandMaster(null);
            };

            function openCreateBrandMaster(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/brand/createOrEditModal.cshtml',
                    controller: 'tenant.views.house.master.brand.createOrEditModal as vm',
                    backdrop: 'static',
                    resolve: {
                        brandId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {

                }).finally(function () {
                    fillDropDownBrand();
                });
            };


            vm.refmaterialgroupcategory = [];

            function fillDropDownMaterialGroupCategory() {
                vm.loading = true;
                materialService.getMaterialGroupCategoryForCombobox({}).success(function (result) {
                    vm.refmaterialgroupcategory = result.items;
                    if (selectedGroupCategoryId != null)
                        vm.material.materialGroupCategoryRefId = selectedGroupCategoryId;
                    vm.loading = false;
                });
            }

            vm.refstockcycle = [];

            function fillDropDownStockCycles() {
                materialService.getInventoryStockCycles({}).success(function (result) {
                    if (result.items != null && result.items.length > 0)
                        result.items[0].isSelected = true;
                    vm.refstockcycle = result.items;
                    //angular.forEach(vm.stockCycleRefIds, function (item,ikey) {
                    //	vm.refstockcycle.some(function (value, key) {
                    //		if (value.value == item.value) {
                    //			value.isSelected = true;
                    //			return true;
                    //		}
                    //	});
                    //});
                }).finally(function () {
                    //vm.refstockcycle[0].isSelected = true;
                });
            }


            vm.refunit = [];

            function fillDropDownUnit() {
                materialService.getUnitForCombobox({}).success(function (result) {
                    vm.refunit = result.items;
                }).finally(function () {
                    vm.refunitformaterial = vm.refunit;
                    vm.refingrunitformaterial = vm.refunit;
                });
            }

            vm.unitReference = function (item) {
                vm.currentmaterialuom = item.displayText;
                if (vm.material.id != null) {

                    abp.message.confirm(
                        app.localize('ChangeDefaultUnitInEdit'),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                vm.unitRefIds = null;
                            }
                            else {
                                vm.material.defaultUnitId = vm.defaultUnitRetained;
                                fillDropDownUnit();
                                return;
                            }
                        }
                    );
                }
                else {
                    vm.tempdetaildata[0].unitRefId = item.value;
                    vm.tempdetaildata[0].uom = vm.currentmaterialuom;
                    if (vm.material.materialTypeId == 1) {
                        vm.material.issueUnitId = vm.material.defaultUnitId;
                    }
                }
                vm.fillDropDownUnitReference();
            }

            vm.refdefaultIssueUnits = [];
            vm.refcorrespondingunits = [];
            vm.fillDropDownUnitReference = function () {

                vm.loading = true;
                unitService.getRefernceUnitName({ Id: vm.material.defaultUnitId }).success(function (result) {
                    vm.refcorrespondingunits = result.items;
                    vm.refdefaultIssueUnits = result.items;
                }).finally(function () {
                    vm.loading = false;

                    //angular.forEach(vm.refunit, function (unitvalue, key) {
                    //    if (unitvalue.value == vm.material.defaultUnitId) {
                    //        vm.refdefaultIssueUnits.push(unitvalue);
                    //        return true;
                    //    }
                    //});

                    if (vm.refdefaultIssueUnits.length == 1) {
                        vm.material.issueUnitId = vm.refdefaultIssueUnits[0].value;
                    }
                });
            }

            function setUserSno() {
                materialService.getUserSerialNumberMax().success(function (result) {
                    vm.material.userSerialNumber = result.id;
                    vm.autoserialnumber = result.id;
                });
            }

            vm.refbrand = [];

            function fillDropDownBrand() {
                commonLookupService.getBrandForCombobox({}).success(function (result) {
                    vm.refbrand = result.items;
                });
            }

            vm.refSupplierName = [];
            vm.temprefSupplierName = [];

            function fillDropDownSupplier() {
                suppliermaterialService.getSupplierForCombobox({}).success(function (result) {
                    vm.refSupplierName = result.items;
                    vm.temprefSupplierName = result.items;
                });
            }

            //vm.refCustomerName = [];
            //vm.temprefCustomerName = [];
            //function fillDropDownCustomer() {
            //    commonLookupService.getCustomerForCombobox({}).success(function (result) {
            //        vm.refCustomerName = result.items;
            //        vm.temprefCustomerName = result.items;
            //    });
            //}

            vm.refmaterialtype = [];
            function fillDropDownMaterialType() {
                materialService.getMaterialTypeForCombobox({}).success(function (result) {
                    vm.refmaterialtype = result.items;
                });
            }

            vm.refaveragepricetags = [];
            function fillDropDownAveragePriceTagType() {
                materialService.getMaterialAveragePriceTagForCombobox({}).success(function (result) {
                    vm.refaveragepricetags = result.items;
                });
            }
            fillDropDownAveragePriceTagType();

            function setData() {
                vm.unitRefId = vm.unitlist;
            }

            vm.unitConversionList = [];

            function init() {
                vm.loading = true;
                fillDropDownMaterialType();
                fillDropDownMaterialGroupCategory();
                fillDropDownUnit();
                fillDropDownSupplier();
                fillDropDownBrand();

                materialService.getMaterialForEdit({
                    Id: materialId
                }).success(function (result) {
                    vm.material = result.material;
                    vm.unitlist = result.unitList;
                    vm.brandlist = result.brandList;
                    vm.recipe = result.materialRecipeType;

                    if (vm.recipe.id == null) {
                        vm.recipe.lifeTimeInMinutes = "";
                        vm.recipe.alarmTimeInMinutes = "";
                        vm.recipe.fixedCost = "";
                        vm.recipe.prdBatchQty = "";
                    }

                    vm.menumappedtab = false;

                    if (vm.material.id == null) {
                        setUserSno();
                        vm.material.generalLifeDays = "";
                        vm.material.posReferencecodeifany = "";
                        vm.addPortion();
                        vm.addCustomerPortion();
                        vm.ingraddPortion();
                        vm.changeDefaultUnitFlag = false;
                        vm.canchangeDefaultUnit = true;
                        vm.material.yieldPercentage = null;
                        vm.material.averagePriceTagRefId = vm.defaultAveragePriceTagRefId;
                        vm.material.noOfMonthAvgTaken = vm.noOfMonthAvgTaken;
                    }
                    else {
                        if (vm.material.averagePriceTagRefId != 2) {
                            vm.material.noOfMonthAvgTaken = vm.noOfMonthAvgTaken;
                        }
                        if (vm.changeDefaultUnitFlag == true)
                            vm.canchangeDefaultUnit = true;
                        else
                            vm.canchangeDefaultUnit = false;

                        vm.oldDefaultUnitId = vm.material.defaultUnitId;
                        vm.currentmaterialuom = vm.material.uom;
                        vm.autoserialnumber = vm.material.userSerialNumber;
                        if (vm.material.posReferencecodeifany == 0)
                            vm.material.posReferencecodeifany = "";
                        vm.defaultUnitRetained = vm.material.defaultUnitId;
                        vm.tempdetaildata = result.supplierMaterial;
                        vm.tempcustomerdetaildata = result.customerMaterial;
                        vm.materialingredientdata = result.materialIngredient;
                        vm.unitConversionList = result.unitConversionList;

                        if (vm.tempdetaildata.length == 0)
                            vm.addPortion();

                        if (vm.tempcustomerdetaildata.length == 0)
                            vm.addCustomerPortion();

                        vm.temptagpricedata = result.customerTagMaterialPrice;
                        vm.fillDropDownUnitReference();
                        //vm.getIngrDetailData();

                        if (vm.materialingredientdata.length == 0)
                            vm.ingraddPortion();

                        vm.stockCycleRefIds = result.stockCycle;

                        if (vm.callingFrom == 'menumapping') {
                            vm.menumappedtab = true;
                            vm.getMappedMenuList();
                        }
                    }

                    vm.fillDropDownMaterial();
                    vm.unitRefIds = vm.unitlist;
                    vm.brandRefIds = vm.brandlist;
                    vm.temptagpricedata = result.customerTagMaterialPrice;

                    vm.getAllBrands();
                    fillDropDownStockCycles();

                });

            }

            init();

            $scope.objArrayPosition = function (arr, keyName, val) {
                return arr.map(function (a) { return a[keyName]; }).indexOf(val);
            };

            vm.addBarCode = function () {
                var idx = $scope.objArrayPosition(vm.material.barcodes, "barcode", vm.barcode);
                if (idx >= 0) {
                    abp.notify.error(app.localize("AlreadyLocalExists_F", vm.barcode));
                    return;
                } else {
                    materialService.isBarcodeExists(vm.barcode).success(function (result) {
                        if (!result) {
                            vm.material.barcodes.push({ 'id': 0, 'barcode': vm.barcode });
                            vm.barcode = "";
                        } else {
                            abp.notify.error(app.localize("AlreadyExists_F", vm.barcode));
                        }
                    }).finally(function () {
                        vm.saving = false;
                    });
                }
            };

            vm.deleteBarCode = function () {
                angular.forEach(vm.selectedBarcodes, function (item) {
                    var idx = vm.material.barcodes.indexOf(item);
                    vm.material.barcodes.splice(idx, 1);
                });
            };

            vm.readBarCodes = function () {

            };

            vm.supplierpermissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.Supplier.Create'),
            };

            vm.unitpermissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.Unit.Create'),
            }

            vm.openSupplierMaster = function () {
                openCreateSupplierMaster(null);
            };

            var selectedSupplierId = null;

            function openCreateSupplierMaster(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/supplier/createOrEditModal.cshtml',
                    controller: 'tenant.views.house.master.supplier.createOrEditModal as vm',
                    backdrop: 'static',
                    resolve: {
                        supplierId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    selectedSupplierId = result;

                }).finally(function () {
                    fillDropDownSupplier();
                });

            };

            vm.openUnitMaster = function () {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/unit/createOrEditModal.cshtml',
                    controller: 'tenant.views.house.master.unit.createOrEditModal as vm',
                    backdrop: 'static',
                    resolve: {
                        unitId: function () {
                            return null;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    fillDropDownUnit();
                });
            }

            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
            }

            vm.suprefunit = [];

            vm.setSupRefUnits = function () {
                vm.suprefunit = [];
                var defunitexists = false;
                var defissueunitexists = false;

                angular.forEach(vm.unitRefIds, function (value, key) {
                    if (vm.material.defaultUnitId == value.value)
                        defunitexists = true;

                    if (vm.material.issueUnitId == value.value)
                        defissueunitexists = true;

                    vm.suprefunit.push(value);
                });

                if (defunitexists == false) {
                    angular.forEach(vm.refunit, function (value, key) {
                        if (vm.material.defaultUnitId == value.value)
                            vm.suprefunit.push(value);
                    });
                }

                if (defissueunitexists == false) {
                    angular.forEach(vm.refunit, function (value, key) {
                        if (vm.material.issueUnitId == value.value)
                            vm.suprefunit.push(value);
                    });
                }
            }

            vm.supunitReference = function (data, item) {
                data.uom = item.displayText;
            }


            vm.mappedMenuList = [];
            vm.getMappedMenuList = function () {
                if (vm.material.id == null) {
                    return;
                }
                vm.loading = true;

                vm.mappedmaterialList = [];
                vm.mappedmaterialList.push({
                    "id": vm.material.id,
                });

                materialService.getMappedMenuForParticularMaterialList({
                    materialList: vm.mappedmaterialList
                }).success(function (result) {
                    vm.mappedMenuList = result;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.uploadMappedMenuList = function () {
                importModal(null);
            }

            vm.deleteMappedMaterial = function (deleteData) {
                abp.message.confirm(
                    app.localize('DeleteMaterialMenuMappingWarning', deleteData.posRefName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            materialmenumappingService.deleteMaterialMenuMappingBasedOnMaterial({
                                posMenuPortionRefId: deleteData.posMenuPortionRefId,
                                materialRefId: deleteData.materialRefId
                            }).success(function () {
                                vm.getMappedMenuList();
                                abp.notify.success(app.localize('MaterialMenuMapping') + ' ' + app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            }

            vm.editMappedMaterial = function (data) {
                vm.menuselection = 2;
                vm.loading = true;
                $state.go("tenant.materialmenumappingdetail", {
                    id: data.posMenuPortionRefId,
                    filter: '',
                    menuselection: vm.menuselection,
                    callingFrom: 'material',
                    materialRefId: data.materialRefId
                });
            }

            function importModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/material/importChangeMaterialMenuMappingModal.cshtml',
                    controller: 'tenant.views.house.master.material.importChangeMaterialMenuMappingModal as vm',
                    backdrop: 'static'
                });

                modalInstance.result.then(function (result) {
                    vm.getMappedMenuList();
                    vm.loading = false;
                });
            }

            vm.downloadMappedMenuList = function () {
                if (vm.material.id == null) {
                    return;
                }
                vm.loading = true;

                vm.mappedmaterialList = [];
                vm.mappedmaterialList.push({
                    "id": vm.material.id,
                });

                materialService.getMappedForParticularMaterialToExcel({
                    materialList: vm.mappedmaterialList
                }).success(function (result) {
                    app.downloadTempFile(result);
                    //materialledgerService.getMaterialLinkWithOtherMateriaalsToExcel({
                    //    id: vm.material.id
                    //}).success(function (result) {
                    //    app.downloadTempFile(result);
                    //}).finally(function () {
                    //    vm.loading = false;
                    //});
                }).finally(function () {
                    vm.loading = false;
                });
            }

            function fillDropDownUnitBasedOnMaterialForSupplier(selectmaterialId, data) {
                vm.loading = true;
                vm.suprefunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.suprefunit = result.items;
                    if (vm.suprefunit.length == 1) {
                        data.unitRefId = vm.suprefunit[0].value;
                        data.uom = vm.suprefunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

            vm.addSupplierModal = function (data) {
                vm.loading = true;
                lookupModal.open({
                    title: app.localize("Select") + " " + app.localize("Supplier"),
                    serviceMethod: supplierService.getAllSimpleSupplier,

                    canSelect: function (item) {
                        vm.loading = false;
                        return true;
                    },
                    callback: function (selectedItem) {
                        if (selectedItem != null) {
                            var alreadyExists = false;
                            if (data.linkedSupplierList == null)
                                data.linkedSupplierList = [];
                            angular.forEach(data.linkedSupplierList, function (value, key) {
                                if (value.id == selectedItem.id) {
                                    abp.notify.error(app.localize('AlreadyExists'));
                                    alreadyExists = true;
                                }
                            });
                            if (alreadyExists == false)
                                data.linkedSupplierList.push(selectedItem);
                        }
                        vm.loading = false;
                    }
                });
                vm.loading = false;
            };

            vm.removeLinkedSupplier = function (value, supindex) {
                var element = value.linkedSupplierList[supindex];
                value.linkedSupplierList.splice(supindex, 1);
            }

            vm.unitCoversionSave = function (data) {
                $scope.errmessage = "";
                if (data.baseUnitId == null || data.refUnitId == null || data.baseUnitId == 0 || data.refUnitId == 0) {
                    abp.notify.warn('MissingData');
                }
                if (data.baseUnitId == data.refUnitId)
                    $scope.errmessage = $scope.errmessage + app.localize('UnitConverSameErr');

                if (data.conversion == 0)
                    $scope.errmessage = $scope.errmessage + app.localize('ConverZeroErr');

                if ($scope.errmessage != '') {
                    abp.notify.warn($scope.errmessage);
                    return;
                }

                vm.loadingCount++;
                vm.saving = true;
                var errorFlag = true;
                unitconversionService.createOrUpdateUnitConversion({
                    unitConversion: data
                }).success(function (result) {
                    abp.notify.info(app.localize('UnitConversion') + ' ' + app.localize('SavedSuccessfully'));
                    errorFlag = false;
                }).finally(function () {
                    if (errorFlag == false) {
                        vm.getLatestUnitConversion();
                    }
                    else {
                        data.editFlag = true;
                        vm.saving = false;
                    }
                    vm.loadingCount--;
                });
            };

            vm.getLatestUnitConversion = function () {
                vm.loadingCount++;
                unitconversionService.getLinkedUnitCoversionForGivenMaterial({
                    id: materialId
                }).success(function (resultUc) {
                    vm.unitConversionList = resultUc;
                }).finally(function () {
                    vm.saving = false;
                    vm.loadingCount--;
                });
            }

            vm.unitConversionLink = function (data, argSupplierRefId, argMaterialRefId) {
                if (vm.material.id == null || vm.material.id == 0)
                    return;
                if (argSupplierRefId == null || argSupplierRefId == 0) {
                    abp.notify.error(app.localize('Supplier') + ' ?');
                    return;
                }
                vm.loading = true;
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/supplier/supplierMaterialwiseunitconversion.cshtml',
                    controller: 'tenant.views.house.master.supplier.supplierMaterialwiseunitconversion as vm',
                    backdrop: 'static',
                    resolve: {
                        supplierRefId: function () {
                            return argSupplierRefId;
                        },
                        supplierRefName: function () {
                            return '';
                        },
                        materialRefId: function () {
                            return argMaterialRefId;
                        },

                    }
                });

                if (data.id == null || data.id == 0) {
                    vm.save(0);
                }

                modalInstance.result.then(function (result) {
                    vm.getLatestUnitConversion();
                }).finally(function () {
                    vm.loading = false;
                });
                vm.loading = false;
            }

        }

    ]);
})();