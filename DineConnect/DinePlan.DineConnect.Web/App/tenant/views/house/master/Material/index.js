﻿(function () {
    appModule.controller('tenant.views.house.master.material.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.material', 'abp.services.app.materialLedger', 'abp.services.app.unitConversion',
        function ($scope, $state, $modal, uiGridConstants, materialService, materialledgerService, unitconversionService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            console.log("Material Index.js Start Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
            vm.isDeleted = false;
            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.Material.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.Material.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.Material.Delete'),
                changeDefaultUnit: abp.auth.hasPermission('Pages.Tenant.House.Master.Material.ChangeDefaultUnit'),
                supplierMaterialView: abp.auth.hasPermission('Pages.Tenant.House.Master.SupplierMaterial.Create'),
            };

            vm.defaultUOMErrorList = [];
            vm.checkDefaultUOMIssue = function () {
                vm.loading = true;
                vm.defaultUOMErrorList = [];
                unitconversionService.errorMaterialDefaultUOMReport({

                }).success(function (result) {
                    vm.defaultUOMErrorList = result;
                    if (vm.defaultUOMErrorList.length == 0) {
                        abp.notify.info(app.localize('NoMoreRecordsFound'));
                    }
                    else {
                        abp.notify.info(vm.defaultUOMErrorList.length + ' ' + app.localize('Error') + ' ' + app.localize('Records'));
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            }

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editDetailItem(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteMaterial(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.changeDefaultUnit\" ng-click=\"grid.appScope.changeDefaultUnitDetails(row.entity)\">" + app.localize("ChangeDefaultUnit") + "</a></li>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.supplierMaterialView\" ng-click=\"grid.appScope.showSupplierDetails(row.entity)\">" + app.localize("Suppliers") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.isDeleted && grid.appScope.permissions.edit\" ng-click=\"grid.appScope.activateMaterial(row.entity)\">" + app.localize("Activate") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },

                    {
                        name: app.localize('#'),
                        field: 'userSerialNumber',
                        width : 60
                    },
                    {
                        name: app.localize('Code'),
                        field: 'code'
                    },
                    {
                        name: app.localize('Type'),
                        field: 'materialTypeName',
                        width: 60
                    },
                    {
                        name: app.localize('Group'),
                        field: 'materialGroupName'
                    },
                    {
                        name: app.localize('Category'),
                        field: 'materialGroupCategoryName'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'materialName'
                    },
                    {
                        name: app.localize('Unit'),
                        field: 'defaultUnitName',
                        maxWidth : 100
                    },
                    {
                        name: app.localize('Barcode'),
                        field: 'barcode'
                    },
                    {
                        name: app.localize('AveragePriceTagRefName'),
                        field: 'averagePriceTagRefId',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span class="label label-success">{{row.entity.averagePriceTagRefName}}</span>' +
                            '</div>',
                    },

                ],
                onRegisterApi: function (gridApi) {

                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.activateMaterial = function (myObject) {
                vm.loading = true;
                materialService.activateMaterial({
                    id: myObject.id
                }).success(function () {
                    abp.notify.success(app.localize("Successfully"));
                    vm.getAll();
                    abp.notify.success(app.localize("ReactivateMaterial"));
                    abp.message.info(app.localize("ReactivateMaterial"));
                    vm.loading = false;
                });
            };

            vm.getAll = function () {
                vm.loading = true;
                console.log("Material Index.js GetView Call Time : " + moment().format('MMMM Do YYYY, HH:mm:ss:SSS '));
                materialService.getView({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    isDeleted: vm.isDeleted,
                }).success(function (result) {
                    if (vm.isDeleted) {
                        vm.colDeletionTime =
                        {
                            name: app.localize('DeletedTime'),
                            field: 'deletionTime',
                            cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                            minWidth: 100
                        };
                        vm.userGridOptions.columnDefs.splice(9, 0, vm.colDeletionTime);
                    }
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;

                }).finally(function () {

                    vm.loading = false;
                });
            };

            vm.editMaterial = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createMaterialModal = function () {
                openCreateOrEditModal(null);
            };

            vm.editDetailItem = function (myObj) {
                openDetailMenuItem(myObj.id, false);
            };

            vm.changeDefaultUnitDetails = function (myObj) {
                openDetailMenuItem(myObj.id, true);
            };

            vm.createDetailItem = function (myObj) {
                openDetailMenuItem(null, false);
            };


            function openDetailMenuItem(objId, changeUnitFlag) {
                $state.go("tenant.materialdetail", {
                    id: objId,
                    changeUnitFlag: changeUnitFlag
                });
            };

            vm.showMaterialLink = function (myObject) {
                vm.loading = true;
                materialledgerService.getMaterialLinkWithOtherMateriaalsToExcel({
                    id: myObject.id
                }).success(function (result) {
                    app.downloadTempFile(result);
                }).finally(function () {
                    vm.loading = false;
                });
            };


            vm.deleteMaterial = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteMaterialWarning', myObject.materialName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            materialService.deleteMaterial({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/material/createOrEditModal.cshtml',
                    controller: 'tenant.views.house.master.material.createOrEditModal as vm',
                    backdrop: 'static',
                    resolve: {
                        materialId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });

            }

            vm.exportToExcel = function () {
                vm.loading = true;
                materialService.getAllToExcel({
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            };

            vm.import = function () {
                importModal(null);
            };

            function importModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/material/importModal.cshtml',
                    controller: 'tenant.views.house.master.material.importModal as vm',
                    backdrop: 'static'
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                    vm.loading = false;
                });
            }

            vm.getAll();

            vm.changeDefaultUnit = function (argMaterialId, argOldUnitId, argNewUnitId) {
                if (argOldUnitId == argNewUnitId) {
                    abp.notify.error(app.localize('NoChangeInDefaultUnit'));
                    return;
                }
                vm.loading = true;
                materialService.defaultUnitChangeProcess({
                    materialRefId: argMaterialId,
                    existUnitRefId: argOldUnitId,
                    revisedUnitRefId: argNewUnitId
                }).success(function (result) {
                    if (result.menuMappingExist == true) {
                        if (result.outputMessage != null) {
                            abp.notify.info(result.outputMessage);
                            abp.notify.error(app.localize('NeedsToChangeTheMenuMapping'));
                            //abp.message.info(result.outputMessage);
                            app.downloadTempFile(result.excelFile);
                        } 
                    }
                    vm.loading = false;
                    abp.notify.info(app.localize('UpdatedSuccessfully'));
                    vm.checkDefaultUOMIssue();
                }).finally(function () {
                    vm.saving = false;
                    vm.loading = false;
                });
                return;

            }


            vm.showSupplierDetails = function (row) {
                vm.viewSupplierDetail(row);
            }

            vm.viewSupplierDetail = function (myObj) {
                openView(myObj.id);
            };

            function openView(materialRefId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/material/supplierledgermodal.cshtml',
                    controller: 'tenant.views.house.master.material.supplierledgerModal as vm',
                    backdrop: 'static',
                    resolve: {
                        materialRefId: function () {
                            return materialRefId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    //vm.getAll();
                });
            }



        }]);
})();

