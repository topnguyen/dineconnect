﻿
(function () {
    appModule.controller('tenant.views.house.master.unitconversion.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.unitConversion', 'unitconversionId', 'argunitIdForLock', 'uiGridConstants', 'abp.services.app.material',
        function ($scope, $modalInstance, unitconversionService, unitconversionId, argunitIdForLock, uiGridConstants, materialService) {
            var vm = this;
            /* eslint-disable */

            vm.saving = false;
            vm.unitconversion = {
                baseUnitId: argunitIdForLock
            };
            vm.filterText = argunitIdForLock;

            vm.loading = false;

            $scope.existall = true;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.Unit.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.Unit.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.Unit.Delete')
            };


            vm.save = function () {
                if ($scope.existall == false)
                    return;

                $scope.errmessage = "";

                if (vm.unitconversion.baseUnitId == null || vm.unitconversion.baseUnitId == 0)
                    $scope.errmessage = $scope.errmessage + app.localize('baseUnitId');

                if (vm.unitconversion.refUnitId == null || vm.unitconversion.refUnitId == 0)
                    $scope.errmessage = $scope.errmessage + app.localize('refUnitId');

                if (vm.unitconversion.baseUnitId == vm.unitconversion.refUnitId)
                    $scope.errmessage = $scope.errmessage + app.localize('UnitConverSameErr');

                if (vm.unitconversion.conversion == 0)
                    $scope.errmessage = $scope.errmessage + app.localize('ConverZeroErr');

                if ($scope.errmessage != '') {
                    abp.notify.warn($scope.errmessage);
                    return;
                }

                vm.saving = true;
                unitconversionService.createOrUpdateUnitConversion({
                    unitConversion: vm.unitconversion
                }).success(function () {
                    abp.notify.info(app.localize('UnitConversion') + ' ' + app.localize('SavedSuccessfully'));
                    //$modalInstance.close();
                    vm.getAll();
                    unitconversionId = null;
                    init();

                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {

                if (vm.unitconversion.id == null) {
                    vm.existall = false;
                    return;
                }
            };

            vm.cancel = function () {
                $modalInstance.close(true);                
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };


            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Delete'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.deleteUnitConversion(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>' +
                            '</div>',
                        width: 50
                    },
                    {
                        name: app.localize('Edit'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.editUnitConversion(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>' +
                            '</div>',
                        width: 50
                    },
                    {
                        name: app.localize('ReferenceUnit'),
                        field: 'convertedUnitName'
                    },
                    {
                        name: app.localize('Conversion'),
                        field: 'conversion'
                    },
                    {
                        name: app.localize('DecimalPlaceRounding'),
                        field: 'decimalPlaceRounding'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };


            vm.getAll = function () {
                unitconversionService.getAllWithNames({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {

                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                });
            };


            vm.editUnitConversion = function (myObj) {
                unitconversionId = myObj.id;
                //openCreateOrEditModal(myObj.id);
                init();
            };

            vm.deleteUnitConversion = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteUnitConversionWarning', myObject.baseUnitName + ' - ' + myObject.convertedUnitName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            unitconversionService.deleteUnitConversion({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.refunit = [];

            function fillDropDownUnit() {
                vm.loading = true;
                materialService.getUnitForCombobox({}).success(function (result) {
                    console.log(result);
                    vm.refunit = result.items;
                    vm.loading = false;
                });
            }

            $scope.SetExample = function (unit) {
                var runitId = vm.unitconversion.refUnitId;
                var bunitId = vm.unitconversion.baseUnitId;

                var rUnitName = $.grep(vm.units, function (unit) {
                    return unit.value == runitId;
                })[0].displayText;

                var bUnitName = $.grep(vm.units, function (unit) {
                    return unit.value == bunitId;
                })[0].displayText;


                if (runitId != 0 && bunitId != 0 && vm.unitconversion.conversion && vm.unitconversion.conversion != undefined) {
                    $scope.conversionDisplay = '1 ' + bUnitName + ' = ' + vm.unitconversion.conversion + ' ' + rUnitName + '\n';
                }
                else {
                    $scope.conversionDisplay = '';
                }
            }

            function init() {
                fillDropDownUnit();

                unitconversionService.getUnitConversionForEdit({
                    Id: unitconversionId
                }).success(function (result) {
                    vm.unitconversion = result.unitConversion;
                    if (vm.unitconversion.id == null) {
                        vm.unitconversion.conversion = "";
                        vm.unitconversion.baseUnitId = vm.filterText;
                    }
                });
            }

            init();
            vm.getAll();

        }
    ]);
})();

