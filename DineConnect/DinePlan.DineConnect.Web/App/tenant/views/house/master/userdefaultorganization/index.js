﻿
(function () {
    appModule.controller('tenant.views.house.master.userdefaultorganization.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.userDefaultOrganization', 'abp.services.app.organizationUnit',
        function ($scope, $modal, uiGridConstants, userdefaultorganizationService, organizationUnitService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.UserDefaultOrganization.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.UserDefaultOrganization.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.UserDefaultOrganization.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Edit'),
                        enableSorting: false,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.editRecord(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>' +
                            '</div>',
                        maxWidth: 60
                    },
                    {
                        name: app.localize('UserName'),
                        field: 'userName',
                        maxWidth: 180
                    },
                    {
                        name: app.localize('DefaultLocation'),
                        field: 'defaultLocationName',
                        maxWidth: 180
                    },
                    {
                        name: app.localize('AllowedLocation'),
                        field: 'locationCodeList'
                    },
                    {
                        name: app.localize('Count'),
                        field: 'locationCount',
                        maxWidth: 80
                    },
                    //{
                    //    name: app.localize('ModifiedTime'),
                    //    field: 'lastModificationTime',
                    //    cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                    //    maxWidth: 100
                    //}
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };



            vm.getAll = function () {
                vm.loading = true;
                userdefaultorganizationService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editRecord = function (myObj) {
                openCreateOrEditModal(myObj);
            };

            function openCreateOrEditModal(myObj) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/location/selectOrganisationAndLocation.cshtml',
                    controller: 'tenant.views.connect.location.select.selectOrganisationAndLocation as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        userId: function () {
                            return myObj.userId;
                        },
                        userName: function () {
                            return myObj.userName;
                        },
                        defaultLocationRefId: function () {
                            return myObj.defaultLocationRefId;
                        },
                        locationList: function () {
                            return myObj.locationList;
                        },
                        companyList: function () {
                            return myObj.companyList;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    organizationUnitService.addLocationToUser({
                        userId: myObj.userId,
                        locations: result.selectedLocations,
                        defaultLocationRefId: result.defaultLocationRefId
                    }).success(function () {
                        abp.notify.success(app.localize('SuccessfullyAdded'));
                        vm.saving = false;
                    });

                    vm.getAll();
                });
            }

            vm.exportToExcel = function () {
                userdefaultorganizationService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getAll();
        }]);
})();

