﻿
(function () {
    appModule.controller('tenant.views.house.master.productionunit.create', [
        '$scope', '$uibModalInstance', 'abp.services.app.productionUnit', 'productionunitId', 'abp.services.app.location',
        function ($scope, $modalInstance, productionunitService, productionunitId, locationService) {
            var vm = this;
            
            vm.saving = false;
            vm.productionunit = null;
			$scope.existall = true;
			vm.currentLanguage = abp.localization.currentLanguage;
			
            vm.save = function () {
			   if ($scope.existall == false)
                    return;
                vm.saving = true;
			
                vm.productionunit.code = vm.productionunit.code.toUpperCase();
                vm.productionunit.name = vm.productionunit.name.toUpperCase();
                productionunitService.createOrUpdateProductionUnit({
                    productionUnit: vm.productionunit
                }).success(function () {
                    abp.notify.info('\' ProductionUnit \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                    location.href = abp.appPath + 'Localization/ChangeCulture?cultureName=' + vm.currentLanguage.name + '&returnUrl=' + window.location.href;

                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.productionunit.code == null) {
			         vm.existall = false;
			         return;
			     }
				 
                productionunitService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'code',
                    filter: vm.productionunit.code,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.productionunit.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.productionunit.code + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
			vm.reflocations = [];

			 function fillDropDownLocations() {
			     locationService.getLocationForProudctionUnitCombobox({}).success(function (result) {
                    vm.reflocations = result.items;
                });
			 }

	        function init() {
				fillDropDownLocations();

                productionunitService.getProductionUnitForEdit({
                    Id: productionunitId
                }).success(function (result) {
                    vm.productionunit = result.productionUnit;
                });
            }
            init();
        }
    ]);
})();

