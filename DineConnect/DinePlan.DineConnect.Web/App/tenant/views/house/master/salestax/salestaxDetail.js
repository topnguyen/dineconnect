﻿(function () {
    appModule.controller('tenant.views.house.master.salestax.salestaxDetail', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'abp.services.app.salesTax', 'abp.services.app.commonLookup', 'uiGridConstants', 'abp.services.app.location',
        function ($scope, $filter, $state, $stateParams,$modal, salestaxService, commonLookupService, uiGridConstants, locationService) {
            var vm = this;
            vm.saving = false;
            vm.salestax = null;
            salestaxId = $stateParams.id;
            $scope.existall = true;
            vm.salestaxTemplateMapping = [];
            vm.sno = 0;
            vm.loading = false;
            

            vm.addPortion=function()
            {
                vm.sno = vm.sno + 1;
                vm.salestaxTemplateMapping.push({
                    'salestaxRefId': 0,
                    'materialGroupCategoryRefId': '',
                    'materialRefId': '',
                    'materialGroupRefId': '',
                    'companyRefId': '',
                    'locationRefId': ''
                });
            }



            vm.portionLength = function () {
                return true;
            }


            vm.removeRow = function (productIndex) {
                var element = vm.salestaxTemplateMapping[productIndex];

                if (vm.salestaxTemplateMapping.length > 1) {
                    vm.salestaxTemplateMapping.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;

                }
                else {
                    vm.salestaxTemplateMapping.splice(productIndex, 1);
                    vm.sno = 0;
                    return;
                }

            }

            vm.save = function () {
                if ($scope.existall == false)
                    return;
                $scope.errmessage = "";

                if (parseFloat(vm.salestax.percentage)>100)
                    $scope.errmessage = $scope.errmessage + app.localize('PercentageErr', vm.salestax.taxName);

                if (vm.salestax.taxCalculationMethod == "" || vm.salestax.taxCalculationMethod == null)
                    $scope.errmessage = $scope.errmessage + app.localize('SalesTaxCalSelErr', vm.salestax.taxCalculationMethod);

                if ($scope.errmessage != "") {
                    abp.notify.warn($scope.errmessage, app.localize('RequiredFields'));
                    return;
                }

                //  Setting up Tax Method 

                $scope.tempsalestaxMethod = "";
                if (vm.salestax.taxCalculationMethod == app.localize('OnGrandTotal')) {
                    $scope.tempsalestaxMethod = "GT";
                    vm.salestax.sortOrder = -2;
                }
                else if (vm.salestax.taxCalculationMethod == app.localize('OnSubTotal')) {
                    $scope.tempsalestaxMethod = "ST";
                    vm.salestax.sortOrder = -1;
                }
                else {
                    $scope.tempsalestaxMethod = vm.salestax.taxCalculationMethod;
                    vm.refsalestaxesonavailable.some(function (value, key) {
                        if (value.displayText == $scope.tempsalestaxMethod) {
                            vm.salestax.sortOrder = value.value;
                            return true;
                        }
                    });
                }

                vm.salestax.taxCalculationMethod = $scope.tempsalestaxMethod;
                $scope.tempsalestaxMethod = "";

                vm.saving = true;
                vm.salestax.taxName = vm.salestax.taxName.toUpperCase();

                salestaxService.createOrUpdateSalesTax({
                    salestax: vm.salestax,
                    SalesTaxTemplateMapping : vm.salestaxTemplateMapping
                }).success(function () {
                    abp.notify.info('\' SalesTax \'' + app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {
                if (vm.salestax.salestaxName == null) {
                    vm.existall = false;
                    return;
                }
                salestaxService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'salestaxName',
                    filter: vm.salestax.salestaxName,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.salestax.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.salestax.salestaxName + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $state.go('tenant.salestax');
            };



            vm.funcMaterialGroup = function (data) {
                var grpId = data.materialGroupRefId;
                fillDropDownMaterialGroupCategory(grpId);
            };

            vm.funcMaterialCategory = function (data) {
                var catId = data.materialGroupCategoryRefId;
                fillDropDownMaterial(catId);
            };

            vm.refsalestaxesonavailable = [];

            function fillDropDownSalesTaxes() {
                vm.loading = true;
                commonLookupService.getSalesTaxForCombobox({}).success(function (result) {
                    vm.refsalestaxesonavailable = result.items;
                    vm.refsalestaxesonavailable.unshift({ value: "-1", displayText: app.localize('OnSubTotal') });
                    vm.refsalestaxesonavailable.unshift({ value: "-2", displayText: app.localize('OnGrandTotal') });
                    vm.loading = false;
                });
            }

            vm.refmaterial = [];
            function fillDropDownMaterial(grpCatId) {
                vm.loading = true;
                commonLookupService.getMaterialByGroupCategory({ id: grpCatId }).success(function (result) {
                    vm.refmaterial = result.items;
                    vm.loading = false;
                });
            }

            vm.refmaterialgroupcategory = [];

            function fillDropDownMaterialGroupCategory(grpId) {
                vm.loading = true;
                commonLookupService.getMaterialGroupCategoryByGroupId({ id: grpId }).success(function (result) {
                    vm.refmaterialgroupcategory = result.items;
                    vm.loading = false;
                }).finally(function () {
                    fillDropDownMaterial(null);
            });
            }

            vm.refmaterialgroup = [];
            function fillDropDownMaterialGroup() {
                vm.loading = true;
                commonLookupService.getMaterialGroupForCombobox({}).success(function (result) {
                    vm.refmaterialgroup = result.items;
                    vm.loading = false;
                }).finally(function () {
                    fillDropDownMaterialGroupCategory(null);
                });
            }

            vm.refLocation = [];
            function fillDropDownLocation() {
                commonLookupService.getLocationForCombobox({}).success(function (result) {
                    vm.refLocation = result.items;
                });
            }

            vm.refCompany = [];
            function fillDropDownCompany() {
                locationService.getCompanyForCombobox({}).success(function (result) {
                    vm.refCompany = result.items;
                }).finally(function () {
                    fillDropDownLocation();
                });
            }

            function init() {
           

                salestaxService.getSalesTaxForEdit({
                    Id: salestaxId
                }).success(function (result) {
                    vm.salestax = result.salesTax;
                    vm.salestaxTemplateMapping = result.salesTaxTemplateMapping;

                    if (vm.salestax.id == null) {
                        vm.salestax.percentage = "";
                        vm.addPortion();
                    }
                    else {
                        $scope.tempsalestaxMethod = "";
                        if (vm.salestax.taxCalculationMethod == "GT")
                            $scope.tempsalestaxMethod = app.localize('OnGrandTotal');
                        else if (vm.salestax.taxCalculationMethod == "ST")
                            $scope.tempsalestaxMethod = app.localize('OnSubTotal');
                        else
                            $scope.tempsalestaxMethod = vm.salestax.taxCalculationMethod;

                        vm.salestax.taxCalculationMethod = $scope.tempsalestaxMethod;
                        $scope.tempsalestaxMethod = "";
                     
                    }
                    fillDropDownSalesTaxes();
                    fillDropDownCompany();
                    fillDropDownMaterialGroup();
                });
            }

            init();
        }
    ]);
})();

    