﻿
(function () {
    appModule.controller('tenant.views.house.master.customertagdefinition.pricetag_createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.customerTagDefinition', 'customertagdefinitionId', 'abp.services.app.commonLookup',
        function ($scope, $modalInstance, customertagdefinitionService, customertagdefinitionId, commonLookupService) {
            var vm = this;
            
            vm.saving = false;
            vm.loading = false;
            vm.customertagdefinition = null;
			$scope.existall = true;

            vm.save = function () {
			   if ($scope.existall == false)
                    return;
                vm.saving = true;
				
                vm.customertagdefinition.tagCode = vm.customertagdefinition.tagCode.toUpperCase();
                vm.customertagdefinition.tagCode = vm.customertagdefinition.tagCode.replace(" ","");
                vm.customertagdefinition.tagName = vm.customertagdefinition.tagName.toUpperCase();

                customertagdefinitionService.createOrUpdateCustomerTagDefinition({
                    customerTagDefinition: vm.customertagdefinition
                }).success(function () {    
                    abp.notify.info('\' CustomerTagDefinition \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    if (vm.customertagdefinition.id == null) {
                        customertagdefinitionService.addTagIntoAllMaterial().success(function ()
                        { }).finally(function () { });
                    }
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.customertagdefinition.tagCode == null) {
			         vm.existall = false;
			         return;
			     }

			     vm.loading = true;
				 
                customertagdefinitionService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'tagCode',
                    filter: vm.customertagdefinition.tagCode,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    vm.loading = false;
                    if (result.totalCount > 0 && vm.customertagdefinition.id == null) {
                        vm.saving = false;
                        
                        abp.notify.info('\' ' + vm.customertagdefinition.tagCode + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
		
	        function init() {
                customertagdefinitionService.getCustomerTagDefinitionForEdit({
                    Id: customertagdefinitionId
                }).success(function (result) {
                    vm.customertagdefinition = result.customerTagDefinition;
                });
            }
            init();
        }
    ]);
})();

