﻿(function () {
    appModule.controller('tenant.views.house.master.customertagmaterialprice.editCustomerPrice', [
        '$scope', '$uibModalInstance', 'allItems', 'initialText', 'abp.services.app.customerTagMaterialPrice', 'customerTagCode',
        function ($scope, $modalInstance, allItems, initialText, customertagmaterialpriceService, customerTagCode) {
            var vm = this;

            vm.allItems = allItems;
            vm.currentText = initialText;
            vm.editingText = angular.extend({}, vm.currentText);
            vm.editingText.revisedPrice = vm.editingText.materialPrice;

            vm.currentIndex = 0;
            vm.saving = false;
            vm.customerTagCode = customerTagCode;
            //Calculating index of currentText in allItems
            for (var i = 0; i < allItems.length; i++) {
                if (vm.allItems[i] == vm.currentText) {
                    vm.currentIndex = i;
                    break;
                }
            }
            
            function save(close) {
                function executeAfterAction() {
                    if (close) {
                        $modalInstance.close();
                    } else {
                        //Go to next
                        vm.currentIndex++;
                        if (vm.allItems.length <= vm.currentIndex) {
                            $modalInstance.close();
                            return;
                        }

                        vm.currentText = vm.allItems[vm.currentIndex];
                        vm.editingText = angular.extend({}, vm.currentText);
                        vm.editingText.revisedPrice = vm.editingText.materialPrice;

                    }
                }

                //if (vm.currentText.customerPrice == vm.editingText.revisedPrice) {
                //    executeAfterAction();
                //    return;
                //}
               
                vm.loading = true;
                vm.datatoSave = [];
                vm.datatoSave.push(vm.editingText);
                customertagmaterialpriceService.updateCustomerMaterialPriceForGivenTag({
                    CustomerTagMaterialPriceEditDto: vm.editingText
                }).success(function (result) {
                    //vm.getMaterialArray(null);
                    abp.message.success(app.localize('UpdatedSuccessfully'));
                }).finally(function () {
                    executeAfterAction();
                    vm.loading = false;
                });
            };

            vm.saveAndNext = function() {
                save(false);
            }

            vm.saveAndClose = function () {
                save(true);
            }

            vm.previous = function () {
                if (vm.currentIndex <= 0) {
                    return;
                }

                vm.currentIndex--;
                vm.currentText = vm.allItems[vm.currentIndex];
                vm.editingText = angular.extend({}, vm.currentText);
                vm.editingText.revisedPrice = vm.editingText.materialPrice;
            };

            vm.targetValueKeyPress = function ($event) {
                if ($event.keyCode == 13) {
                    vm.saveAndNext();
                } else if ($event.keyCode == 37) {
                    vm.previous();
                } else if ($event.keyCode == 39) {
                    vm.saveAndNext();
                }
            }

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();