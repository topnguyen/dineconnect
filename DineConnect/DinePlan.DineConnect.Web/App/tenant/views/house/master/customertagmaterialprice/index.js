﻿
(function () {
    appModule.controller('tenant.views.house.master.customertagmaterialprice.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.customerTagMaterialPrice', 'abp.services.app.customerTagDefinition',
        function ($scope, $state, $modal, uiGridConstants, customertagmaterialpriceService, customertagdefinitionService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.editflag = false;

            vm.customerTagCode = "";

            vm.datalist = [];
            vm.tagdata = [];


            vm.permissions = {
                //create: abp.auth.hasPermission('Pages.Tenant.House.Master.CustomerTagMaterialPrice.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.CustomerTagMaterialPrice.Edit'),
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };


            vm.userGridOptions = {
                //enableCellEditOnFocus: true ,
                //enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                //enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                //paginationPageSizes: app.consts.grid.defaultPageSizes,
                //paginationPageSize: app.consts.grid.defaultPageSize,
                //useExternalPagination: true,
                //useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',

                columnDefs: [
                    {
                        name: app.localize('Edit'),
                        width: 90,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.editEntry(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>' +
                            '</div>'
                    },
                    {
                        name: app.localize('MaterialRefName'),
                        field: 'materialRefName',
                    },
                    {
                        name: app.localize('Default'),
                        field: 'tagCode',
                    },
                    {
                        name: app.localize('Price'),
                        field: 'materialPrice',
                    },

                ],

                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };


            vm.cancel = function () {
                $state.go("tenant.customer");
            };

            vm.refcustomerpricetag = [];
            function fillDropDownCustomerTag() {
                customertagdefinitionService.getCustomerTagForCombobox({}).success(function (result) {
                    vm.refcustomerpricetag = result.items;
                    if (result.items.length == 1)
                        vm.customerTagRefId = result.items[0].value;
                });
            }

            fillDropDownCustomerTag();

            vm.getAll = function () {
                vm.editSelectedTag();
            }

            vm.editSelectedTag = function () {
                if (vm.customerTagCode == null || vm.customerTagCode.length == 0) {
                    abp.notify.info("SelectCustomerTagErr");
                    return;
                }
                var selectedTag = vm.customerTagCode;
                vm.getMaterialArray(selectedTag);
            }

            vm.getMaterialArray = function (argCustomerTag) {
                vm.loading = true;
                customertagmaterialpriceService.getMaterialPriceForGivenTag({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    SelectedPriceTagForEdit: argCustomerTag
                }).success(function (result) {
                    //vm.datalist = result;
                    vm.userGridOptions.data = result;
                    vm.userGridOptions.totalItems = result.length;
                //    vm.userGridOptions.columnDefs = result.columnDefList;
                }).finally(function () {
                    vm.loading = false;
                });
                //    customertagmaterialpriceService.getMaterialPriceDynamic({
                //        skipCount: requestParams.skipCount,
                //        maxResultCount: requestParams.maxResultCount,
                //        sorting: requestParams.sorting,
                //        filter: vm.filterText,
                //        SelectedPriceTagForEdit: argCustomerTag
                //    }).success(function (result) {
                //        vm.datalist = result.dynamicTagList;
                //        vm.userGridOptions.data = result.dynamicTagList;
                //        vm.userGridOptions.totalItems = result.dynamicTagList.length;
                //        vm.userGridOptions.columnDefs = result.columnDefList;
                //    }).finally(function () {

                //        //angular.forEach(vm.datalist, function (value, key) {
                //        //    vm.inwarddirectcreditdetaildata.push({
                //        //        "dcRefId": 0,
                //        //        "materialRefId": value.materialRefId,
                //        //        "materialRefName": value.materialRefName,
                //        //        "dcReceivedQty": value.dcReceivedQty,
                //        //        "unitRefId": value.unitRefId,
                //        //        "unitRefName": value.unitRefName,
                //        //        "materialReceivedStatus": 0,
                //        //        "isFractional": value.isFractional
                //        //    });
                //        //});


                //        vm.loading = false;
                //    });
            };

            vm.editCustomerTagMaterialPrice = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createCustomerTagMaterialPrice = function () {
                openCreateOrEditModal(null);
            };

            vm.updateCustomerTagMaterialPrice = function () {
                //var t = 1;
                //customertagmaterialpriceService.updateCustomerMaterialPrice({
                //    dynamicTagList: vm.userGridOptions.data,
                //    tagToBeUpdate: vm.customerTagCode
                //}).success(function (result) {
                //    vm.getMaterialArray(null);
                //    abp.message.success(app.localize('UpdatedSuccessfully'));
                //}).finally(function () {
                //    vm.loading = false;
                //    vm.editflag = false;
                //    vm.customerTagCode = null;
                //});
            };

            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/customertagmaterialprice/createOrEditModal.cshtml',
                    controller: 'tenant.views.house.master.customertagmaterialprice.createOrEditModal as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        customertagmaterialpriceId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.exportToExcel = function () {
                customertagmaterialpriceService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };



            vm.getAll();

            vm.editEntry = function (selectedrow) {
                var se = "as";

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/customertagmaterialprice/editCustomerPrice.cshtml',
                    controller: 'tenant.views.house.master.customertagmaterialprice.editCustomerPrice as vm',
                    resolve: {
                        //locationId: function () {
                        //    return vm.locationId;
                        //},
                        allItems: function () {
                            return vm.userGridOptions.data;
                        },
                        initialText: function () {
                            return selectedrow;
                        },
                        customerTagCode: function () {
                            return vm.customerTagCode;
                        },
                        customerTagId: function () {
                            return vm.customerTagRefId;
                        },
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.loading = true;
                    vm.getMaterialArray(vm.customerTagCode);
                    vm.loading = false;
                });
            };

        }]);
})();

