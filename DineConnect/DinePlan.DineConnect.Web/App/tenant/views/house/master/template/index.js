﻿

(function () {
    appModule.controller('tenant.views.house.master.template.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.template', 'abp.services.app.location',
        function ($scope, $modal, uiGridConstants, templateService, locationService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.templateType = null; vm.locationRefId = null;

            vm.permissions = {
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.Template.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editTemplate(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteTemplate(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Location'),
                        field: 'locationRefName'
                    },
                    {
                        name: app.localize('TemplateType'),
                        field: 'templateTypeRefName'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('View'),
                        //  field: 'templateData',
                        minWidth: 100,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '<span> <a ng-click="grid.appScope.getView(row.entity.templateData)"><i class="fa fa-eye"></i></a></span>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.reflocatiion = [];

            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;
                });
            }
            fillDropDownLocation();

            vm.refTemplateType = [];

            function fillDropDownTemplateType() {
                templateService.getTemplateTypeForCombobox({}).success(function (result) {
                    vm.refTemplateType = result.items;
                });
            }
            fillDropDownTemplateType();

            vm.getAll = function () {
                vm.loading = true;
                templateService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    templateType: vm.templateType,
                    locationRefId: vm.locationRefId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };


            vm.getView = function (myObject) {

                var jsonString = myObject;
                //'{"record":[{"sn":"Demo Sheet 1","vn":"Demo View 11"},{"sn":"Demo Sheet 1","vn":"Demo View 12"},{"sn":"Demo Sheet 2","vn":"Demo View 21"},{"sn":"Demo Sheet 1","vn":"Demo View 13"}],"recordcount":"4"}';
                var jsonObject = JSON.parse(jsonString);
                //var newArrays = {};
                //var records = jsonObject.record;

                //for (var i = 0; i < records.length; i++) {
                //    if (!newArrays[records[i].sn]) {
                //        newArrays[records[i].sn] = [];
                //    }
                //    newArrays[records[i].sn].push(records[i].vn);
                //}

                var modalInstance = $modal.open({
                    //ariaLabelledBy: 'modal-title',
                    //ariaDescribedBy: 'modal-body',
                    templateUrl: '~/App/tenant/views/house/master/template/myModalContent.cshtml',
                    controller: 'tenant.views.house.master.template.templateData as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        templateData: function () {
                            return jsonObject;
                        }
                    }
                });

            };

            vm.editTemplate = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createTemplate = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteTemplate = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteTemplateWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            templateService.deleteTemplate({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            //function openCreateOrEditModal(objId) {
            //    var modalInstance = $modal.open({
            //        templateUrl: '~/App/tenant/views/m3manage/inventory/template/createOrEditModal.cshtml',
            //        controller: 'tenant.views.m3manage.inventory.template.createOrEditModal as vm',
            //        backdrop: 'static',
            //        keyboard: false,
            //        resolve: {
            //            templateId: function () {
            //                return objId;
            //            }
            //        }
            //    });

            //    modalInstance.result.then(function (result) {
            //        vm.getAll();
            //    });
            //}

            vm.exportToExcel = function () {
                templateService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getAll();
        }]);

    appModule.controller('tenant.views.house.master.template.templateData', [
        '$scope', '$uibModalInstance', 'templateData',
        function ($scope, $modalInstance, templateData) {

            var vm = this;

            vm.templateData = templateData;

            // Grab the container DOM element
            var tree = document.getElementById('treeview');

            (function recurse(domElement, treeNode) {
                var ul = document.createElement('ul'),
                    li,
                    n;

                // Make a <ul> to hold the current tree node's children (if any)
                domElement.appendChild(ul)

                // Loop over current tree node's children
                for (n in treeNode) {
                    // Create an <li> element
                    li = document.createElement('li');
                    // Add the "key" text (eg. "Demo Sheet 1")
                    li.innerText = n;
                    // If the current tree node child is an array...
                    if (treeNode[n] instanceof Array) {
                        // Recursively get this node's children, etc
                        recurse(li, treeNode[n]);
                    }
                    else {
                        // Otherwise just print the "value" text (eg. "Demo View 11")
                        li.innerHTML = treeNode[n];
                    }
                    // Remember to add the <li> to the <ul>!
                    ul.appendChild(li);
                }
            }(tree, vm.templateData));
            vm.cancel = function () {
                $modalInstance.dismiss();
            }
        }]);
})();



//(function () {
//    appModule.controller('tenant.views.house.master.template.index', [
//        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.template',
//        function ($scope, $modal, uiGridConstants, templateService) {
//            var vm = this;

//            $scope.$on('$viewContentLoaded', function () {
//                App.initAjax();
//            });

//            vm.loading = false;
//            vm.filterText = null;
//            vm.currentUserId = abp.session.userId;

//            vm.permissions = {
//                create: abp.auth.hasPermission('Pages.Tenant.House.Master.Template.Create'),
//                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.Template.Edit'),
//                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.Template.Delete')
//            };

//            var requestParams = {
//                skipCount: 0,
//                maxResultCount: app.consts.grid.defaultPageSize,
//                sorting: null
//            };

//            vm.userGridOptions = {
//				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
//				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
//				paginationPageSizes: app.consts.grid.defaultPageSizes,
//				paginationPageSize: app.consts.grid.defaultPageSize,
//				useExternalPagination: true,
//				useExternalSorting: true,
//				appScopeProvider: vm,
//				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
//				columnDefs: [
//					{
//						name: app.localize('Actions'),
//						enableSorting: false,
//						width: 120,

//						cellTemplate:
//						   "<div class=\"ui-grid-cell-contents\">" +
//							    "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
//							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
//							   "    <ul uib-dropdown-menu>" +
//							   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editTemplate(row.entity)\">" + app.localize("Edit") + "</a></li>" +
//							   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteTemplate(row.entity)\">" + app.localize("Delete") + "</a></li>" +
//							   "    </ul>" +
//							   "  </div>" +
//							   "</div>"
//					},
//                    {
//                        name: app.localize('Name'),
//                        field: 'name'
//                    },
//                    {
//                        name: app.localize('CreationTime'),
//                        field: 'creationTime',
//                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
//                        minWidth: 100
//                    }
//                ],
//                onRegisterApi: function (gridApi) {
//                    $scope.gridApi = gridApi;
//                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
//                        if (!sortColumns.length || !sortColumns[0].field) {
//                            requestParams.sorting = null;
//                        } else {
//                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
//                        }

//                        vm.getAll();
//                    });
//                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
//                        requestParams.skipCount = (pageNumber - 1) * pageSize;
//                        requestParams.maxResultCount = pageSize;

//                        vm.getAll();
//                    });
//                },
//                data: []
//            };

//            vm.getAll = function () {
//                vm.loading = true;
//                templateService.getAll({
//                    skipCount: requestParams.skipCount,
//                    maxResultCount: requestParams.maxResultCount,
//                    sorting: requestParams.sorting,
//                    filter: vm.filterText
//                }).success(function (result) {
//                    vm.userGridOptions.totalItems = result.totalCount;
//                    vm.userGridOptions.data = result.items;
//                }).finally(function () {
//                    vm.loading = false;
//                });
//            };

//            vm.editTemplate = function (myObj) {
//                openCreateOrEditModal(myObj.id);
//            };

//            vm.createTemplate = function () {
//                openCreateOrEditModal(null);
//            };
//            vm.deleteTemplate = function (myObject) {
//                abp.message.confirm(
//                    app.localize('DeleteTemplateWarning', myObject.name),
//                    function (isConfirmed) {
//                        if (isConfirmed) {
//                            templateService.deleteTemplate({
//                                id: myObject.id
//                            }).success(function () {
//                                vm.getAll();
//                                abp.notify.success(app.localize('SuccessfullyDeleted'));
//                            });
//                        }
//                    }
//                );
//            };

//            function openCreateOrEditModal(objId) {
//                var modalInstance = $modal.open({
//                    templateUrl: '~/App/tenant/views/house/master/template/createOrEditModal.cshtml',
//                    controller: 'tenant.views.house.master.template.createOrEditModal as vm',
//                    backdrop: 'static',
//					keyboard: false,
//                    resolve: {
//                        templateId: function () {
//                            return objId;
//                        }
//                    }
//                });

//                modalInstance.result.then(function (result) {
//                    vm.getAll();
//                });
//            }

//            vm.exportToExcel = function () {
//                templateService.getAllToExcel({})
//                    .success(function (result) {
//                        app.downloadTempFile(result);
//                    });
//            };


//            vm.getAll();
//        }]);
//})();

