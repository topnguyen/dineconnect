﻿
(function () {
    appModule.controller('tenant.views.house.master.template.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.template', 'templateId', 'abp.services.app.commonLookup',
        function ($scope, $modalInstance, templateService, templateId, commonLookupService) {
            var vm = this;
            
            vm.saving = false;
            vm.template = null;
			$scope.existall = true;

			
            vm.save = function () {
			   if ($scope.existall == false)
                    return;
                vm.saving = true;
				
                templateService.createOrUpdateTemplate({
                    template: vm.template
                }).success(function () {
                    abp.notify.info('\' Template \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.template.name == null) {
			         vm.existall = false;
			         return;
			     }
				 
                templateService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'name',
                    filter: vm.template.name,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.template.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.template.name + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
	        function init() {

                templateService.getTemplateForEdit({
                    Id: templateId
                }).success(function (result) {
                    vm.template = result.template;
                });
            }
            init();
        }
    ]);
})();

