﻿
(function () {
    appModule.controller('tenant.views.house.master.materiallocationwisestock.allLocationStock', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.materialLocationWiseStock', 'abp.services.app.location', 'appSession', 'abp.services.app.material',
        function ($scope, $state, $modal, uiGridConstants, materiallocationwisestockService, locationService, appSession, materialService) {
            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;

            vm.locationRefId = appSession.user.locationRefId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.locations = [];
            vm.currentLocation = appSession.location.name;

            vm.currentUserId = abp.session.userId;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: 'materialRefId Desc'
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: 'Actions',
                        enableSorting: false,
                        width: 50,
                        headerCellTemplate: '<span></span>',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.viewMaterialLocationWiseStock(row.entity)"><i class="fa fa-list-ol"></i></button>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Material'),
                        field: 'materialRefName',
                        minWidth: 220
                    },

                    {
                        name: app.localize('LedgerBalance'),
                        field: 'currentInHand',
                        cellClass: function (grid, row) {
                            if (row.entity.currentInHand < row.entity.reOrderLevel)
                                return 'ui-red ui-ralign';
                            else
                                return 'ui-ralign';
                        },
                    },
                    {
                        name: app.localize('Sales'),
                        field: 'sales',
                        cellClass: 'ui-ralign',

                    },
                    {
                        name: app.localize('AvailableBalance'),
                        field: 'liveStock',
                        cellClass: 'ui-ralign',
                        cellClass: function (grid, row) {
                            if (row.entity.liveStock < row.entity.reOrderLevel)
                                return 'ui-red ui-ralign';
                            else
                                return 'ui-green ui-ralign';
                        },
                    },
                    {
                        name: app.localize('UOM'),
                        field: 'uom',
                        width: 80,
                        cellClass: function (grid, row) {
                            if (row.entity.liveStock < row.entity.reOrderLevel)
                                return 'ui-red ui-ralign';
                            else
                                return 'ui-green ui-ralign';
                        },
                    },
                    {
                        name: app.localize('MinStock'),
                        field: 'minimumStock',
                        cellClass: 'ui-ralign',

                    },
                    {
                        name: app.localize('MaxStock'),
                        field: 'maximumStock',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Re-OrderLevel'),
                        field: 'reOrderLevel', cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('OnHand'),
                        field: 'liveStock_TransactionUnit',
                        cellClass: 'ui-brown ui-ralign'
                    },
                    {
                        name: app.localize('TransactionUnit'),
                        field: 'transactionUnitName',
                        cellClass: 'ui-brown'
                    },

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.refmaterialtype = [];
            function fillDropDownMaterialType() {
                materialService.getMaterialTypeForCombobox({}).success(function (result) {
                    vm.refmaterialtype = result.items;
                });
            }

            fillDropDownMaterialType();

            vm.clear = function () {
                vm.materialTypeRefId = null;
                vm.material = null;
                vm.lessThanMinimumStock = false;
                vm.lessThanReorderLevel = false;
                vm.isHighValueItemOnly = false;
                vm.getAll();
            }

            vm.liveStock = true;
            vm.isHighValueItemOnly = false;

            vm.getAll = function () {
                if (vm.locations.length == 0)
                    vm.locations = [];

                vm.loading = true;
                materiallocationwisestockService.getAllLocationView({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    locationRefId: vm.locationRefId,
                    locationGroup: vm.locationGroup,
                    liveStock: vm.liveStock,
                    isHighValueItemOnly: vm.isHighValueItemOnly,
                    lessThanMinimumStock: vm.lessThanMinimumStock,
                    lessThanReorderLevel: vm.lessThanReorderLevel,
                    material: vm.material,
                    materialTypeRefId: vm.materialTypeRefId,
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.exportToExcel = function () {
                vm.loading = true;

                materiallocationwisestockService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    locationRefId: vm.locationRefId,
                    liveStock: vm.liveStock,
                    isHighValueItemOnly: vm.isHighValueItemOnly,
                    lessThanMinimumStock: vm.lessThanMinimumStock,
                    lessThanReorderLevel: vm.lessThanReorderLevel,
                    material: vm.material,
                    materialTypeRefId: vm.materialTypeRefId
                }).success(function (result) {
                    app.downloadTempFile(result);
                    vm.loading = false;
                });
            };


            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    console.log(result);

                    if (result.locations.length > 0) {
                        //vm.locationRefId = result.locations[0].id;
                        //vm.currentLocation = result.locations[0].name;
                        //vm.getAll();
                        vm.locationGroup = result;
                        vm.getAll();
                    }

                });
            }

            vm.viewMaterialLocationWiseStock = function (data) {
                if (data.locationWiseStock==null) {
                    return;
                }
                var i = 1;
                if (data.locationWiseStock.length > 7) {
                    vm.passvalue = JSON.stringify(data);

                    //var newUrl = $state.href("tenant.locationWiseStockNonModal", {
                    //    materialRefId : data.materialRefId,
                    //    materialStock: vm.passvalue,
                    //});
                    //var $nextWindow = window.open(newUrl, '_blank');
                    //$nextWindow.ms = vm.passvalue;

                    $state.go("tenant.locationWiseStockNonModal", {
                        materialStock: vm.passvalue,
                    });
                }
                else {
                    var modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/house/master/materiallocationwisestock/locationWiseStockModal.cshtml',
                        controller: 'tenant.views.house.master.materiallocationwisestock.locationWiseStockModal as vm',
                        backdrop: 'static',
                        resolve: {
                            materialRefId: function () {
                                return data.materialRefId;
                            },
                            materialRefName: function () {
                                return data.materialRefName;
                            },
                            uom: function () {
                                return data.uom;
                            },
                            materialStock: function () {
                                return data
                            },
                        }
                    });

                    modalInstance.result.then(function (result) {
                        vm.loading = false;
                    });
                  
                }
                
            }



        }]);
})();

