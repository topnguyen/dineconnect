﻿(function () {
    appModule.controller('tenant.views.house.master.materiallocationwisestock.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.materialLocationWiseStock', 'abp.services.app.commonLookup', 'abp.services.app.location', 'abp.services.app.material', "abp.services.app.tenantSettings", 'abp.services.app.dayClose', 'appSession',
        function ($scope, $state, $modal, uiGridConstants, materiallocationwisestockService, commonLookupService, locationService, materialService, tenantSettingsService, daycloseService, appSession) {
            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = true;
            vm.filterText = null;

            vm.locationRefId = appSession.user.locationRefId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.materialRefIds = [];
            vm.currentLocation = appSession.location.name;
            vm.housetransactiondate = appSession.location.houseTransactionDate;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.Material.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.Material.Edit'),
                importData: abp.auth.hasPermission('Pages.Tenant.Setup.ImportData'),
                exportData: abp.auth.hasPermission('Pages.Tenant.Setup.ExportData')
            };

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];


            vm.checkCloseDay = function () {
                vm.loading = true;
                vm.loadingCount++;
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }
                    vm.housetransactiondate = moment(result.accountDate).format($scope.format);
                }).finally(function (result) {
                    vm.loading = false;
                    vm.loadingCount--;
                });
            }

            vm.checkCloseDay();

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.import = function () {
                importModal(null);
            };

            function importModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/materiallocationwisestock/importModal.cshtml',
                    controller: 'tenant.views.house.master.materiallocationwisestock.importModal as vm',
                    backdrop: 'static',
                    resolve: {
                        locationRefId: function () {
                            return vm.locationRefId;
                        },
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                    vm.loading = false;
                });
            }

            vm.importActive = function () {
                importActiveModal(null);
            }

            function importActiveModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/materiallocationwisestock/importModalActiveInactiveSet.cshtml',
                    controller: 'tenant.views.house.master.materiallocationwisestock.importModalActiveInactiveSet as vm',
                    backdrop: 'static',
                    resolve: {
                        locationRefId: function () {
                            return vm.locationRefId;
                        },
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                    vm.loading = false;
                });
            }

            vm.detailDataShown = function (data) {
                var argOption = 'Sales';

                var ledgerDetails = {
                    locationRefId: vm.defaultLocationId,
                    ledgerDate: vm.housetransactiondate,
                    materialRefId: data.materialRefId,
                    materialRefName: data.materialRefName,
                    uom: data.uom,
                    sales: data.sales
                };

                var endDate = moment().format($scope.format);

                var modalInstance1 = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/materialledger/drillDownLevel.cshtml',
                    controller: 'tenant.views.house.master.materialledger.drillDownLevel as vm',
                    backdrop: 'static',
                    resolve: {
                        ledgerDetails: function () {
                            return ledgerDetails;
                        },
                        searchFor: function () {
                            return argOption;
                        },
                        startDate: function () {
                            return vm.housetransactiondate;
                        },
                        endDate: function () {
                            return endDate;
                        },
                    }
                });

                modalInstance1.result.then(function (result) {
                    //vm.getAll();
                });
            }


            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: 'Actions',
                        enableSorting: false,
                        width: 50,
                        headerCellTemplate: '<span></span>',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.editMaterialLocationWiseStock(row.entity)"><i class="fa fa-edit"></i></button>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Material'),
                        field: 'materialRefName',
                        minWidth: 220,
                        cellClass: 'ui-blue',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span class="bold">{{row.entity.materialRefName}}</span>' +
                            '  <span ng-show="!row.entity.isActiveInLocation"> <br/></span>' +
                            '  <span ng-show="!row.entity.isActiveInLocation" class="label label-danger">' + app.localize('Dorment') + '</span>' +
                            '</div>'
                    },
                    {
                        name: app.localize('LedgerBalance'),
                        field: 'currentInHand',
                        cellClass: function (grid, row) {
                            if (row.entity.currentInHand < row.entity.reOrderLevel)
                                return 'ui-red ui-ralign';
                            else
                                return 'ui-ralign';
                        },
                    },
                    {
                        name: app.localize('Sales'),
                        field: 'sales',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-right\">' +
                            '  <a class="bold" ng-click="grid.appScope.detailDataShown(row.entity)"><i class="fa fa-eye"> {{row.entity.sales}} </i></a>' +
                            '</div>',
                        cellClass: 'ui-ralign',
                    },
                    {
                        name: app.localize('AvailableBalance'),
                        field: 'liveStock',
                        cellClass: 'ui-ralign',
                        cellClass: function (grid, row) {
                            if (row.entity.liveStock < row.entity.reOrderLevel)
                                return 'ui-red ui-ralign';
                            else
                                return 'ui-green ui-ralign';
                        },
                    },
                    {
                        name: app.localize('UOM'),
                        field: 'uom',
                        width: 80,
                        cellClass: function (grid, row) {
                            if (row.entity.liveStock < row.entity.reOrderLevel)
                                return 'ui-red ui-ralign';
                            else
                                return 'ui-green ui-ralign';
                        },
                    },
                    {
                        name: app.localize('MinStock'),
                        field: 'minimumStock',
                        cellClass: 'ui-ralign',

                    },
                    {
                        name: app.localize('MaxStock'),
                        field: 'maximumStock',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Re-OrderLevel'),
                        field: 'reOrderLevel', cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('ConvertAsZeroStockWhenClosingStockNegative'),
                        field: 'isActiveInLocation',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.convertAsZeroStockWhenClosingStockNegative" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Issue OnHand'),
                        field: 'liveStock_TransactionUnit',
                        cellClass: 'ui-brown ui-ralign'
                    },
                    {
                        name: app.localize('Issue Unit'),
                        field: 'transactionUnitName',
                        cellClass: 'ui-brown'
                    },
                    {
                        name: app.localize('TrnUnit OnHand'),
                        field: 'liveStock_TransferUnit',
                        cellClass: 'ui-blue ui-ralign'
                    },
                    {
                        name: app.localize('Trn Unit'),
                        field: 'transferUnitRefName',
                        cellClass: 'ui-blue'
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.refmaterialtype = [];
            function fillDropDownMaterialType() {
                materialService.getMaterialTypeForCombobox({}).success(function (result) {
                    vm.refmaterialtype = result.items;
                });
            }

            fillDropDownMaterialType();


            vm.clear = function () {
                vm.materialTypeRefId = null;
                vm.material = null;
                vm.lessThanMinimumStock = false;
                vm.lessThanReorderLevel = false;
                vm.isHighValueItemOnly = false;
                vm.materialRefIds = [];
                vm.getAll();
            }

            vm.decimals = null;
            vm.refmaterials = [];
            vm.uilimit = 30;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    });

                materialService.getMaterialForCombobox({})
                    .success(function (result) {
                        vm.refmaterials = result.items;
                        vm.loading = false;
                    });
            };

            vm.$onInit = function () {
                vm.getDecimals();
            };

            vm.liveStock = true;
            vm.isHighValueItemOnly = false;

            vm.getAll = function () {

                vm.loading = true;
                materiallocationwisestockService.getView({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    locationRefId: vm.locationRefId,
                    liveStock: vm.liveStock,
                    isHighValueItemOnly: vm.isHighValueItemOnly,
                    lessThanMinimumStock: vm.lessThanMinimumStock,
                    lessThanReorderLevel: vm.lessThanReorderLevel,
                    material: vm.material,
                    materialTypeRefId: vm.materialTypeRefId,
                    materialRefIds: vm.materialRefIds
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };



            vm.editMaterialLocationWiseStock = function (myObj) {
                openCreateOrEditModal(myObj);
            };

            vm.createMaterialLocationWiseStock = function () {
                openCreateOrEditModal(null);
            };

            vm.insertMissingMaterial = function () {
                if (angular.isUndefined(vm.locationRefId) || vm.locationRefId == null) {
                    abp.notify.info(app.localize('LocationSelectErr'));
                    return;
                }
                vm.loading = true;

                materiallocationwisestockService.includeAllMaterial({
                    Id: vm.locationRefId
                }).success(function (result) {
                    vm.getAll();
                }).finally(function () {
                    vm.loading = false;
                });
            };


            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/materiallocationwisestock/createOrEditModal.cshtml',
                    controller: 'tenant.views.house.master.materiallocationwisestock.createOrEditModal as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        materiallocationwisestockId: function () {
                            return objId.id;
                        },
                        materialNameWithUom: function () {
                            return objId.materialRefName + ' - ' + objId.uom;
                        },
                        locationRefId: function () {
                            return vm.locationRefId;
                        },
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.exportToExcel = function () {
                vm.loading = true;

                materiallocationwisestockService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    locationRefId: vm.locationRefId,
                    liveStock: vm.liveStock,
                    isHighValueItemOnly: vm.isHighValueItemOnly,
                    lessThanMinimumStock: vm.lessThanMinimumStock,
                    lessThanReorderLevel: vm.lessThanReorderLevel,
                    material: vm.material,
                    materialTypeRefId: vm.materialTypeRefId
                }).success(function (result) {
                    app.downloadTempFile(result);
                    vm.loading = false;
                });
            };

            vm.openLocation = function () {

                vm.locationGroup = app.createLocationInputForCreateSingleLocation();

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    console.log(result);

                    if (result.locations.length > 0) {
                        vm.locationRefId = result.locations[0].id;
                        vm.currentLocation = result.locations[0].name;
                        vm.getAll();
                    }

                });
            }

            vm.autoPo = function () {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/purchaseorder/AutoPoSetting.cshtml',
                    controller: 'tenant.views.house.transaction.purchaseorder.autoPoSetting as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        locationId: function () {
                            return vm.defaultLocationId;
                        },
                        autoMode: function () {
                            return 'PO';
                        },
                    }
                });

                modalInstance.result.then(function (result) {

                });


                //$state.go('tenant.autopogenerate', {
                //});
                //                openCreateOrEditModal(null, true);
            };

            //vm.getAll();

            //vm.autoPo = function () {
            //    $state.go('tenant.purchaseorderdetail', {
            //        id: null,
            //        autoPoFlag: true
            //    });
            //}



        }]);
})();

