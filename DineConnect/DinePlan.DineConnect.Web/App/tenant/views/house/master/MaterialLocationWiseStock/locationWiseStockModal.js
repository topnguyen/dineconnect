﻿(function () {
    appModule.controller('tenant.views.house.master.materiallocationwisestock.locationWiseStockModal', [
        '$scope', '$uibModalInstance', 'materialRefId', 'materialRefName', 'uom', 'materialStock' ,
        function ($scope, $modalInstance, materialRefId, materialRefName, uom, materialStock) {
            var vm = this;
            /* eslint-disable */

            vm.loading = false;
            vm.materialStock = materialStock;
            vm.uom = uom;
            vm.norecordsfound = false;
            if (vm.materialStock==null || vm.materialStock.locationWiseStockList==null ||
                vm.materialStock.locationWiseStockList.length == 0)
                vm.norecordsfound = true;

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();