﻿(function () {
    appModule.controller('tenant.views.house.master.materiallocationwisestock.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.materialLocationWiseStock', 'materiallocationwisestockId', 'abp.services.app.commonLookup', 'abp.services.app.location', 'abp.services.app.material', 'materialNameWithUom', 'locationRefId', 'abp.services.app.dayClose',
        function ($scope, $modalInstance, materiallocationwisestockService, materiallocationwisestockId, commonLookupService, locationService, materialService, materialNameWithUom, locationRefId, daycloseService) {
            var vm = this;
                /* eslint-disable */
            vm.saving = false;
            vm.materiallocationwisestock = null;
            vm.materialNameWithUom = materialNameWithUom;

            vm.accountDate = null;

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.save = function () {

                $scope.errmessage = "";

                var minstock = parseFloat(vm.materiallocationwisestock.minimumStock);
                var maxstock = parseFloat(vm.materiallocationwisestock.maximumStock);
                var rol = parseFloat(vm.materiallocationwisestock.reOrderLevel);


                if (vm.materiallocationwisestock.currentInHand > 0 && vm.materiallocationwisestock.isActiveInLocation == false) {
                    abp.message.warn(app.localize('IsActiveLocationErrorMessage'));
                    return;
                }

                if (minstock > maxstock)
                    $scope.errmessage = $scope.errmessage + app.localize('MinMaxStockErr');

                if (minstock > rol)
                    $scope.errmessage = $scope.errmessage + app.localize('MinROLError');

                if (maxstock < rol)
                    $scope.errmessage = $scope.errmessage + app.localize('MaxROLError');

                if ($scope.errmessage != "") {
                    abp.message.warn($scope.errmessage, 'Required');
                    return;
                }


                vm.saving = true;
                vm.materiallocationwisestock.accountDate = vm.accountDate;
                materiallocationwisestockService.createOrUpdateMaterialLocationWiseStock({
                    materialLocationWiseStock: vm.materiallocationwisestock
                }).success(function () {
                    abp.notify.info(app.localize('Stock') + ' ' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.refmaterial = [];

            function fillDropDownMaterial() {
                materialService.getMaterialForCombobox({}).success(function (result) {
                    vm.refmaterial = result.items;
                });
            }
            fillDropDownMaterial();

            vm.reflocation = [];

            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;
                });
            }
            fillDropDownLocation();

            function init() {

                materiallocationwisestockService.getMaterialLocationWiseStockForEdit({
                    Id: materiallocationwisestockId
                }).success(function (result) {
                    vm.materiallocationwisestock = result.materialLocationWiseStock;
                    vm.defaultLocationId = vm.materiallocationwisestock.locationRefId;
                    vm.checkCloseDay();
                    
                    if (vm.materiallocationwisestock.id == null) {
                        vm.materiallocationwisestock.minimumStock = "";
                        vm.materiallocationwisestock.maximumStock = "";
                        vm.materiallocationwisestock.currentInHand = "";
                        vm.materiallocationwisestock.reOrderLevel = "";
                    }
                    else {
                        if (vm.materiallocationwisestock.currentInHand != 0)
                            $("#CurrentInHand").prop("disabled", true);
                        else
                            vm.materiallocationwisestock.currentInHand = "0.0";

                        if (vm.materiallocationwisestock.minimumStock == 0)
                            vm.materiallocationwisestock.minimumStock = "0.0";

                        if (vm.materiallocationwisestock.maximumStock == 0)
                            vm.materiallocationwisestock.maximumStock = "0.0";

                        if (vm.materiallocationwisestock.reOrderLevel == 0)
                            vm.materiallocationwisestock.reOrderLevel = "0.0"
                    }
                });
            }




            init();
            vm.checkCloseDay = function () {
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        var errMessage = app.localize("LastExecutionDate", moment(result.accountDate).format('YYYY-MMM-DD'));
                        abp.message.warn(errMessage);
                        abp.notify.warn(errMessage);
                        abp.notify.error(errMessage);
                    }
                    vm.accountDate = moment(result.accountDate).format($scope.format);
                }).finally(function (result) {

                });
            }





        }
    ]);
})();

