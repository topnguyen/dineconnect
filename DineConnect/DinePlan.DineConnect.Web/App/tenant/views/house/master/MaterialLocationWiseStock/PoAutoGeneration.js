﻿(function () {
    appModule.controller('tenant.views.house.master.materiallocationwisestock.PoAutoGeneration', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.materialLocationWiseStock', 'abp.services.app.location', 'appSession', 'abp.services.app.material',
        'abp.services.app.supplierMaterial',
        function ($scope, $modal, uiGridConstants, materiallocationwisestockService, locationService, appSession, materialService, suppliermaterialService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.locationRefId = null;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.nextndays = 7;
            vm.podata = [];
            vm.isHighValueItemOnly = false;
            vm.currentUserId = abp.session.userId;

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };




            vm.locations = [];

            vm.getLocations = function () {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                    if (vm.defaultLocationId != null && vm.defaultLocationId > 0) {
                        vm.locationRefId = vm.defaultLocationId;
                        vm.projectionPurchase();
                    }
                }).finally(function (result) {

                });
            };

            vm.exportToExcel = function () {
            	suppliermaterialService.purchaseProjectionToExcel({
            		locationRefId: vm.locationRefId,
            		nextNDays: vm.nextndays,
            		startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
            		endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
            		isHighValueItemOnly: vm.isHighValueItemOnly
            	})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.projectionPurchase = function () 
            {
                vm.loading = true;
                suppliermaterialService.getPurchaseProjection({
                    locationRefId: vm.locationRefId,
                    nextNDays : vm.nextndays,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    isHighValueItemOnly: vm.isHighValueItemOnly
                }).success(function (result) {
                    vm.podata = result;
                    //vm.userGridOptions.totalItems = result.totalCount;
                    //vm.userGridOptions.data = result.productAnalysis;
                    //nonFilteredData = result.productAnalysis;
                    //vm.userGridOptions.api.setColumnDefs();
                }).finally(function () {
                    vm.loading = false;
                });

            }

            //vm.getAll();
            //fillDropDownLocation();
            vm.getLocations();

        }]);
})();

