﻿(function () {
    appModule.controller('tenant.views.house.master.materiallocationwisestock.locationWiseStockNonModal', [
        '$scope', '$state', '$stateParams', 
        function ($scope, $state, $stateParams) {
            var vm = this;
            /* eslint-disable */

            vm.loading = false;
            //vm.ms = window.ms;
            vm.existdata = $stateParams.materialStock;
            vm.materialStock = JSON.parse(vm.existdata);

            //vm.materialRefId = materialRefId;
            //vm.materialRefName = materialRefName;
            //vm.uom = uom;
            vm.norecordsfound = false;
            if (vm.materialStock.locationWiseStock.length == 0)
                vm.norecordsfound = true;

            vm.cancel = function () {
                window.close();
            };
        }
    ]);
})();