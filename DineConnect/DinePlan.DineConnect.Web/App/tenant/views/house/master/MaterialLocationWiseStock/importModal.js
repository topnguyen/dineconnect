﻿(function () {
    appModule.controller('tenant.views.house.master.materiallocationwisestock.importModal', [
        '$scope', 'appSession', '$uibModalInstance', 'FileUploader', 'abp.services.app.materialLocationWiseStock', 'locationRefId',
        function ($scope, appSession, $uibModalInstance, fileUploader, materiallocationwisestockService,locationRefId) {
            var vm = this;
            vm.loading = false;
            vm.locationRefId = locationRefId;

            vm.uploader = new fileUploader({
                url: abp.appPath + 'Import/ImportStockWithReOrderLevel',
                queueLimit: 1,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        console.log("item : " + item);
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        console.log("Type : " + type);
                        if ('|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ImportTemplate_Warn_FileType'));
                            return false;
                        }
                        return true;
                    }
                }]
            });

            vm.save = function () {
                if (vm.uploader.queue.length == 0) {
                    abp.notify.warn(app.localize('YouAreNotSelectAFile'));
                    return;
                }
                vm.loading = true;
                vm.uploader.uploadAll();
            };


            vm.getTemplate = function () {
                vm.loading = true;
                materiallocationwisestockService.getStockExcel({
                    id: vm.locationRefId
                }).success(function (result) {
                    app.downloadTempFile(result);
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.uploader.onErrorItem = function (fileItem, response, status, headers) {
                if (response.success) {
                    abp.notify.info(app.localize("FINISHED"));
                    $uibModalInstance.close();
                    vm.loading = false;
                } else {
                    abp.message.error(response);
                    abp.notify.error(response);
                    vm.loading = false;
                    vm.saving = false;
                    return;
                }
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.error != null) {
                    abp.message.warn(response.error.message);
                    $uibModalInstance.close();
                    vm.loading = false;
                    return;
                }
                abp.notify.info(app.localize("FINISHED"));
                $uibModalInstance.close();
                vm.loading = false;
            };
        }

    ]);
})();
