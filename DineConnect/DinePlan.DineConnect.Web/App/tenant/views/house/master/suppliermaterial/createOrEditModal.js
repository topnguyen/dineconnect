﻿

(function () {
    appModule.controller('tenant.views.house.master.suppliermaterial.createOrEditModal', [
        '$scope', '$state', '$stateParams', '$q', '$uibModal', '$filter', 'abp.services.app.supplierMaterial', 'abp.services.app.material', 'appSession', 'abp.services.app.location', '$uibModal',
        function ($scope, $state, $stateParams, $q, $modal, $filter, suppliermaterialService, materialService, appSession, locationService, $modal) {
            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            vm.suppliermaterial = null;
            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;
            vm.loading = true;
            vm.currentUserId = abp.session.userId;
            vm.uilimit = null;
            vm.alllocationflag = true;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.materialDetailPortion = [];
            supplierId = $stateParams.id;
            vm.selectedLocations = [];

            vm.save = function () {
                vm.saving = true;
                vm.suppliermaterial.lastQuoteDate = "2016-04-16";   //@@Pending
                vm.suppliermaterial.lastQuoteRefNo = "QUOTE16/04";  //@@Pending

                vm.sentdetail = [];

                if (vm.alllocationflag == true) {
                    angular.forEach(vm.materialDetailPortion, function (value, key) {
                        vm.sentdetail.push({
                            'sno': vm.sno,
                            'supplierRefId': supplierId,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'unitRefId': value.unitRefId,
                            'uom': value.uom,
                            'materialPrice': value.materialPrice,
                            'minimumOrderQuantity': value.minimumOrderQuantity,
                            'lastQuoteRefNo': 'Default',
                            'lastQuoteDate': '2016-04-16',
                            'locationRefId': null,
                            'supplierMaterialAliasName': value.supplierMaterialAliasName
                        });
                    });
                }
                else {
                    if (vm.selectedLocations == null || vm.selectedLocations.length == 0) {
                        vm.saving = false;
                        abp.notify.error(app.localize('Location') + " ?");
                        return;
                    }

                    angular.forEach(vm.selectedLocations, function (locvalue, lockey) {
                        angular.forEach(vm.materialDetailPortion, function (value, key) {
                            vm.sentdetail.push({
                                'sno': vm.sno,
                                'supplierRefId': supplierId,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'unitRefId': value.unitRefId,
                                'uom': value.uom,
                                'materialPrice': value.materialPrice,
                                'minimumOrderQuantity': value.minimumOrderQuantity,
                                'lastQuoteRefNo': 'Default',
                                'lastQuoteDate': '2016-04-16',
                                'locationRefId': locvalue.id,
                                'supplierMaterialAliasName': value.supplierMaterialAliasName
                            });
                        });
                    });
                }


                suppliermaterialService.createOrUpdateSupplierMaterial({
                    supplierMaterial: vm.sentdetail
                }).success(function () {
                    abp.notify.error(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.removeRow = function (productIndex) {
                if (vm.materialDetailPortion.length > 1) {
                    vm.materialDetailPortion.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                }
                else {
                    vm.materialDetailPortion.splice(productIndex, 1);
                    vm.sno = 0;
                    vm.addPortion();
                    return;
                }
            }

            vm.editSet = function (productIndex) {
                vm.materialDetailPortion[productIndex].editflag = true;
            }

            vm.cancel = function () {
                $state.go("tenant.supplier");
            };


            $scope.func = function (data, val, obj) {
                var forLoopFlag = true;

                angular.forEach(vm.materialDetailPortion, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.materialRefId = 0;
                    data.materialRefName = '';
                    data.supplierMaterialAliasName = '';
                    return;
                }
                data.materialRefName = val.materialName;
                data.unitRefId = val.unitRefId;
                data.supplierMaterialAliasName = val.supplierMaterialAliasName;
                data.uom = val.uom;
            };


            vm.addPortion = function () {

                var errorFlag = false;

                var value = vm.materialDetailPortion[vm.materialDetailPortion.length - 1];

                if (vm.sno > 0) {
                    var value = vm.materialDetailPortion[vm.materialDetailPortion.length - 1];

                    if (vm.isUndefinedOrNull(value.materialRefId) || value.materialRefId == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.materialRefName)) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MaterialRequired'));
                        return;
                    }

                    if (vm.isUndefinedOrNull(value.materialPrice) || value.materialPrice == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('InvalidMaterialPrice'));
                        return;
                    }

                    angular.forEach(vm.materialDetailPortion, function (val, key) {
                        vm.materialDetailPortion[key].changeunitflag = false;
                        vm.materialDetailPortion[key].editflag = false;
                    });
                }

                vm.sno = vm.sno + 1;
                vm.materialDetailPortion.push({
                    'sno': vm.sno,
                    'supplierRefId': supplierId,
                    'materialRefId': 0,
                    'materialRefName': '',
                    'unitRefId': '',
                    'uom': '',
                    'materialPrice': '',
                    'minimumOrderQuantity': '',
                    'lastQuoteRefNo': '',
                    'lastQuoteDate': '',
                    'editflag': true,
                    'supplierMaterialAliasName' : ''
                });
                vm.uilimit = 50;
            }

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.userGridOptions = {
                //enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                //enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editSupplierMaterial(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteSupplierMaterial(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Id'),
                        field: 'id'
                    },
                    {
                        name: app.localize('SupplierName'),
                        field: 'supplierRefName'
                    },
                    {
                        name: app.localize('MaterialName'),
                        field: 'materialRefName'
                    },
                    {
                        name: app.localize('MaterialPrice'),
                        field: 'materialPrice'
                    },
                    {
                        name: app.localize('MOQ'),
                        field: 'minimumOrderQuantity'
                    },
                    {
                        name: app.localize('LastQuoteRefNo'),
                        field: 'lastQuoteRefNo'
                    },
                    {
                        name: app.localize('LastQuoteDate'),
                        field: 'lastQuoteDate'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.SupplierMaterial.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.SupplierMaterial.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.SupplierMaterial.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };


            vm.getAll = function () {
                vm.loading = true;
                suppliermaterialService.getView({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.refSupplierName = [];
            vm.temprefSupplierName = [];

            function fillDropDownSupplier() {
                suppliermaterialService.getSupplierForCombobox({}).success(function (result) {
                    vm.refSupplierName = result.items;
                    vm.temprefSupplierName = result.items;
                });
            }
            fillDropDownSupplier();

            vm.refmaterial = [];
            function fillDropDownMaterial() {

                //suppliermaterialService.getMaterialBasedOnSupplierWithPrice({
                //    locationRefId: vm.defaultLocationId,
                //    supplierRefId: supplierId,
                //    poRefId: null
                //}).success(function (result) {
                //    vm.refmaterial = result.items;

                materialService.getMaterialViewWithUom({}).success(function (result) {
                    vm.refmaterial = result.items;
                });
            }
            fillDropDownMaterial();


            vm.refallowedLocations = [];
            vm.fillUserAllowedLocation = function () {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.refallowedLocations = result.items;
                }).finally(function () {

                });
            }

            vm.fillUserAllowedLocation();

            vm.getDataForGivenLocations = function () {
                init();
            }

            function init() {
                if (vm.alllocationflag == false ) {
                    if (vm.selectedLocations.length == 0) {
                        abp.notify.error(app.localize('Location') + " ?");
                        return;
                    }
                }

                suppliermaterialService.getSupplierMaterialForEdit({
                    supplierRefId: supplierId,
                    selectedLocations: vm.selectedLocations
                }).success(function (result) {
                    vm.suppliermaterial = result.supplierMaterial;
                    vm.suppliermaterial.supplierRefId = supplierId;
                    vm.loading = false;
                    if (vm.suppliermaterial.length == 0) {
                        vm.sno = 0;
                        vm.addPortion();
                    }
                    else {
                        vm.sno = 0;
                        vm.materialDetailPortion = [];

                        angular.forEach(vm.suppliermaterial, function (value, key) {
                            vm.sno = vm.sno + 1;
                            vm.materialDetailPortion.push({
                                'sno': vm.sno,
                                'supplierRefId': supplierId,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'unitRefId': value.unitRefId,
                                'uom': value.uom,
                                'materialPrice': value.materialPrice,
                                'minimumOrderQuantity': value.minimumOrderQuantity,
                                'lastQuoteRefNo': value.lastQuoteRefNo,
                                'lastQuoteDate': value.lastQuoteDate,
                                'editflag': false,
                                'supplierMaterialAliasName': value.supplierMaterialAliasName
                            });

                        });


                    }
                });
            }

            init();

            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.suprefunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.suprefunit = result.items;
                    if (vm.suprefunit.length == 1) {
                        data.unitRefId = vm.suprefunit[0].value;
                        data.unitRefName = vm.suprefunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

            vm.supunitReference = function (data, item) {
                data.uom = item.displayText;
            }


            vm.clearExistRecords = function () {
                vm.materialDetailPortion = [];
            }

            vm.deleteSupplierMaterial = function (data,index) {

                abp.message.confirm(
                    app.localize('DeleteSupplierMaterialWarning', data.materialRefName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            vm.deleteMaterialList(data,index);
                        }
                    }
                );

            }

            

            vm.deleteMaterialList = function (data,index) {
                vm.sentdetail = [];
                if (vm.alllocationflag == true) {
                    vm.sentdetail.push({
                        'supplierRefId': supplierId,
                        'materialRefId': data.materialRefId,
                        'locationRefId': null
                    });
                }
                else {
                    if (vm.selectedLocations == null || vm.selectedLocations.length == 0) {
                        vm.saving = false;
                        abp.notify.error(app.localize('Location') + " ?");
                        return;
                    }

                    angular.forEach(vm.selectedLocations, function (locvalue, lockey) {
                        //angular.forEach(vm.materialDetailPortion, function (value, key) {
                            vm.sentdetail.push({
                                'supplierRefId': supplierId,
                                'materialRefId': data.materialRefId,
                                'locationRefId': locvalue.id
                            });
                        //});
                    });
                }

                vm.loading = true;
                vm.saving = true;
                suppliermaterialService.deleteSupplierMaterialList({
                    supplierWithLocations : vm.sentdetail
                }).success(function () {
                    abp.notify.error(app.localize('DeletedSuccessfully'));
                    vm.removeRow(index);
                }).finally(function () {
                    vm.saving = false;
                    vm.loading = false;
                });
            }

            vm.unitConversionLink = function (data) {
                vm.loading = true;
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/supplier/supplierMaterialwiseunitconversion.cshtml',
                    controller: 'tenant.views.house.master.supplier.supplierMaterialwiseunitconversion as vm',
                    backdrop: 'static',
                    resolve: {
                        supplierRefId: function () {
                            return supplierId;
                        },
                        supplierRefName: function () {
                            return this.supplierRefName;
                        },
                        materialRefId: function () {
                            return data.materialRefId;
                        },
                       
                    }
                });

                modalInstance.result.then(function (result) {

                }).finally(function () {
                    vm.loading = false;
                });
                vm.loading = false;
            }

        }
    ]);
})();
