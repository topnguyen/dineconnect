﻿
(function () {
    appModule.controller('tenant.views.house.master.materialbrandslink.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.materialBrandsLink', 'materialbrandslinkId', 'abp.services.app.commonLookup',
        function ($scope, $modalInstance, materialbrandslinkService, materialbrandslinkId, commonLookupService) {
            var vm = this;
            vm.saving = false;
            vm.materialbrandslink = null;
			$scope.existall = true;

			
            vm.save = function () {
			   if ($scope.existall == false)
                    return;
                vm.saving = true;
				
                materialbrandslinkService.createOrUpdateMaterialBrandsLink({
                    materialBrandsLink: vm.materialbrandslink
                }).success(function () {
                    abp.notify.info('\' MaterialBrandsLink \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.materialbrandslink.id == null) {
			         vm.existall = false;
			         return;
			     }
				 
                materialbrandslinkService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.materialbrandslink.id,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.materialbrandslink.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.materialbrandslink.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
			vm.refmaterial = [];

			 function fillDropDownMaterial() {
			     	commonLookupService.getMaterialForCombobox({}).success(function (result) {
                    vm.refmaterial = result.items;
                   // vm.refmaterial.unshift({ value: "0", displayText: app.localize('NotAssigned') });
                });
			 }
			 vm.refbrand = [];

			 function fillDropDownBrand() {
			     commonLookupService.getBrandForCombobox({}).success(function (result) {
			         vm.refbrand = result.items;
			         // vm.refmaterial.unshift({ value: "0", displayText: app.localize('NotAssigned') });
			     });
			 }
	        function init() {
				fillDropDownMaterial();
				fillDropDownBrand();
                materialbrandslinkService.getMaterialBrandsLinkForEdit({
                    Id: materialbrandslinkId
                }).success(function (result) {
                    vm.materialbrandslink = result.materialBrandsLink;
                });
            }
            init();
        }
    ]);
})();

