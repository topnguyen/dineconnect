﻿
(function () {
    appModule.controller('tenant.views.house.master.materialstoragelocation.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.materialStorageLocation', 'materialstoragelocationId', 'abp.services.app.commonLookup',
        function ($scope, $modalInstance, materialstoragelocationService, materialstoragelocationId, commonLookupService) {
            var vm = this;
            vm.saving = false;
            vm.materialstoragelocation = null;
			$scope.existall = true;

			
            vm.save = function () {
			   if ($scope.existall == false)
                    return;
                vm.saving = true;
				
                materialstoragelocationService.createOrUpdateMaterialStorageLocation({
                    materialStorageLocation: vm.materialstoragelocation
                }).success(function () {
                    abp.notify.info('\' MaterialStorageLocation \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.materialstoragelocation.id == null) {
			         vm.existall = false;
			         return;
			     }
				 
                materialstoragelocationService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.materialstoragelocation.id,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.materialstoragelocation.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.materialstoragelocation.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
			vm.refmaterial = [];

			 function fillDropDownMaterial() {
			     	commonLookupService.getMaterialForCombobox({}).success(function (result) {
                    vm.refmaterial = result.items;
                   // vm.refmaterial.unshift({ value: "0", displayText: app.localize('NotAssigned') });
                });
			 }
			 vm.refstoragelocation = [];

			 function fillDropDownstoragelocation() {
			     commonLookupService.getStorageLocationForCombobox({}).success(function (result) {
			         vm.refstoragelocation = result.items;
			         // vm.refmaterial.unshift({ value: "0", displayText: app.localize('NotAssigned') });
			     });
			 }

	        function init() {
				fillDropDownMaterial();
				fillDropDownstoragelocation();
                materialstoragelocationService.getMaterialStorageLocationForEdit({
                    Id: materialstoragelocationId
                }).success(function (result) {
                    vm.materialstoragelocation = result.materialStorageLocation;
                });
            }
            init();
        }
    ]);
})();

