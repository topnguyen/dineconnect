﻿(function () {
    appModule.controller('tenant.views.house.transaction.materialledger.closingVarriance', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.materialLedger', 'abp.services.app.location', 'abp.services.app.material', 'appSession', 'abp.services.app.houseReport',
        function ($scope, $modal, uiGridConstants, materialledgerService, locationService, materialService, appSession, housereportService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.loadingCount = 0;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.uilimit = 20;

            vm.locationRefId = vm.defaultLocationId;
            vm.materialRefId = null;
            vm.materialLedgerData = [];

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Transaction.MaterialLedger.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Transaction.MaterialLedger.Edit'),
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.clear = function () {
                vm.materialRefId = null;
            }

            vm.locations = [];

            vm.getLocations = function () {

                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                }).finally(function (result) {
                });
            };

            vm.getLocations();

            vm.refmaterial = [];

            function fillDropDownMaterial() {
                vm.loadingCount++;
                materialService.getMaterialForCombobox({}).success(function (result) {
                    vm.refmaterial = result.items;
                    vm.loadingCount--;
                    // vm.refmaterial.unshift({ value: "0", displayText: app.localize('NotAssigned') });
                });
            }

            fillDropDownMaterial();

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];


            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.getAll = function () {
                vm.loading = true;
                housereportService.getVarianceReportAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationRefId: vm.locationRefId,
                    materialRefId: vm.materialRefId,
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.output = result;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.exportToExcel = function () {
                vm.loading = true;
                housereportService.getVarianceAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationRefId: vm.locationRefId,
                    materialRefId: vm.materialRefId,
                    userId: vm.currentUserId
                })
                    .success(function (result) {
                        vm.loading = false;
                        app.downloadTempFile(result);
                    });
            };





        }]);
})();

