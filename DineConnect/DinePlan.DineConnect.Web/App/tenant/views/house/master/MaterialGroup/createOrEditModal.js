﻿
(function () {
    appModule.controller('tenant.views.house.master.materialgroup.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.materialGroup', 'materialgroupId', 'abp.services.app.commonLookup', 'uiGridConstants',
        function ($scope, $modalInstance, materialgroupService, materialgroupId, commonLookupService, uiGridConstants) {
            var vm = this;
            
            vm.saving = false;
            vm.materialgroup = null;
			$scope.existall = true;

			vm.save = function (argOption) {
			   if ($scope.existall == false)
                    return;
                vm.saving = true;
                vm.materialgroup.materialGroupName = vm.materialgroup.materialGroupName.toUpperCase();
                materialgroupService.createOrUpdateMaterialGroup({
                    materialGroup: vm.materialgroup
                }).success(function (result) {
                    abp.notify.info('\' MaterialGroup \'' + app.localize('SavedSuccessfully'));
                    if (argOption == 2) {
                        $modalInstance.close(result.id);
                        vm.saving = false;
                        return;
                    }
                    vm.getAll();
                    materialgroupId = null;
                    init();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.materialgroup.materialGroupName == null) {
			         vm.existall = false;
			         return;
			     }
				 
                materialgroupService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'materialGroupName',
                    filter: vm.materialgroup.materialGroupName,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.materialgroup.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.materialgroup.materialGroupName + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			

            function init() {

                materialgroupService.getMaterialGroupForEdit({
                    Id: materialgroupId
                }).success(function (result) {
                    vm.materialgroup = result.materialGroup;
                    
                });
            }
            init();


            //  Index JS Operation Included

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.MaterialGroup.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.MaterialGroup.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.MaterialGroup.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Delete'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.deleteMaterialGroup(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>' +
                            '</div>',
                        width: 50
                    },
                    {
                        name: app.localize('Edit'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.editMaterialGroup(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>' +
                            '</div>',
                        width: 50
                    },
                    {
                        name: app.localize('materialGroupName'),
                        field: 'materialGroupName'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                materialgroupService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editMaterialGroup = function (myObj) {
                materialgroupId = myObj.id;
                //openCreateOrEditModal(myObj.id);
                init();
            };

            //vm.createMaterialGroup = function () {
            //    openCreateOrEditModal(null);
            //};

            vm.deleteMaterialGroup = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteMaterialGroupWarning', myObject.materialGroupName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            materialgroupService.deleteMaterialGroup({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };


            vm.exportToExcel = function () {
                materialgroupService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getAll();

        }
    ]);
})();

