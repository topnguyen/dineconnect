﻿
(function () {
    appModule.controller('tenant.views.house.master.contact.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.contact', 'contactId', 'abp.services.app.commonLookup',
        function ($scope, $modalInstance, contactService, contactId, commonLookupService) {
            var vm = this;
            
            vm.saving = false;
            vm.contact = null;
			$scope.existall = true;

			
            vm.save = function () {
			   if ($scope.existall == false)
			       return;

			   $scope.errmessage = "";
			   if (vm.contact.contactCategory == "" || vm.contact.contactCategory == null)
			       $scope.errmessage = $scope.errmessage + app.localize('ContactSelErr');
			   if (vm.contact.phoneNumber1 == "" || vm.contact.phoneNumber1 == null)
			       $scope.errmessage = $scope.errmessage + app.localize('PhoneNoErr');

			   if ($scope.errmessage != "")
			   {
			       abp.message.warn($scope.errmessage);
			       return;
			   }

                vm.saving = true;
                contactService.createOrUpdateContact({
                    contact: vm.contact
                }).success(function () {
                    abp.notify.info('\' Contact \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
			     if (vm.contact.contactPersonName == null) {
			         vm.existall = false;
			         return;
			     }

                contactService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'contactPersonName',
                    filter: vm.contact.contactPersonName,
                    operation: 'SEARCH'
                }).success(function (result) {
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.contact.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.contact.contactPersonName + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                    
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
			 vm.getComboText = function (item) {
			     return item.displayText;
			 };

	        function init() {
                contactService.getContactForEdit({
                    Id: contactId
                }).success(function (result) {
                    vm.contact = result.contact;
                });
            }
            init();
        }
    ]);
})();

