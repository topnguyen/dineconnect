﻿(function () {
	appModule.controller('tenant.views.house.master.materialmenumapping.menuCostingDrillDown', [
		'$scope', '$uibModalInstance', 'portionMenuId', 'abp.services.app.houseReport', 'menuPortionRefName', 'appSession',
		function ($scope, $modalInstance, portionMenuId, housereportService, menuPortionRefName, appSession) {

			var vm = this;
			vm.menuPortionRefName = menuPortionRefName;
			vm.detailData = [];
			vm.defaultLocationId = appSession.user.locationRefId;

			vm.menucostingReport = function () {
				vm.loading = true;
				vm.detailData = [];

				housereportService.getMenuCostingReportForGivenMenuPortionIds({
					startDate: moment().format($scope.format),
					endDate: moment().format($scope.format),
					locationRefId: vm.defaultLocationId,
					menuPortionRefId: portionMenuId
				}).success(function (result) {
					vm.detailData = result.recipeWiseCosting;
				}).finally(function () {
					vm.loading = false;
				});
			};

			vm.menucostingReport();

            //vm.detailData = standardledgerDetails;
            //vm.recipeExistFlag = false;
            //angular.forEach(vm.detailData, function (value, key) {
            //	if (value.recipeRefId>0) {
            //		vm.recipeExistFlag = true;s
            //		return;
            //	}
            //});
            //vm.actualData = null;

            //vm.actualData = actualledgerDetails;

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

        }
    ]);
})();