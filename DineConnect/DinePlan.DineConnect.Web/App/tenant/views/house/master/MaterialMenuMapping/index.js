﻿(function () {
    appModule.controller('tenant.views.house.master.materialmenumapping.index', [
        '$scope', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.materialMenuMapping',
        function ($scope, $state, $stateParams, $modal, uiGridConstants, materialmenumappingService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.filterText = $stateParams.filter;
            vm.menuselection = $stateParams.menuselection;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.MaterialMenuMapping.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.MaterialMenuMapping.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.MaterialMenuMapping.Delete')
            };

            if (vm.menuselection == 1 || vm.menuselection == '' || vm.menuselection == null) {
                vm.allMenu = true;
                vm.mappedOnly = null;
                vm.nonmappedOnly = null;
            }
            else if (vm.menuselection == 2) {
                vm.allMenu = null;
                vm.mappedOnly = true;
                vm.nonmappedOnly = null;
            }
            else if (vm.menuselection == 3) {
                vm.allMenu = null;
                vm.mappedOnly = null;
                vm.nonmappedOnly = true;
            }

            //document.getElementById('mappedonly').checked = true;

            vm.import = function () {
                importModal(null);
            };

            function importModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/materialmenumapping/importMenuMappingModal.cshtml',
                    controller: 'tenant.views.house.master.menumapping.importMenuMappingModal as vm',
                    backdrop: 'static'
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                    vm.loading = false;
                });
            }

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editMaterialMenuMapping(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteMaterialMenuMapping(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.create\" ng-click=\"grid.appScope.showMenuCost(row.entity)\">" + app.localize("Costing") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('MenuPortion'),
                        field: 'posRefName'
                    },
                    {

                        name: app.localize('MaterialCount'),
                        field: 'numberOfMaterials'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                //if (document.getElementById('allMenu').checked == true)
                //    vm.listFilter = "All";
                //else if (document.getElementById('mappedonly').checked == true)
                //    vm.listFilter = "Mapped";
                //else
                //    vm.listFilter = "NonMapped";
                vm.listFilter = '';

                if (vm.allMenu == true)
                    vm.listFilter = "All";
                else if (vm.mappedOnly == true)
                    vm.listFilter = "Mapped";
                else if (vm.nonmappedOnly == true)
                    vm.listFilter = "NonMapped";
                else
                    abp.notify.error(app.localize('SelectAnyOne'));

                materialmenumappingService.getAllWithMenuWithMaterialCount({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    listFilter: vm.listFilter
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editMaterialMenuMapping = function (myObj) {
                openCreateOrEditModal(myObj.posMenuPortionRefId);
            };


            vm.createMaterialMenuMapping = function () {
                openCreateOrEditModal(null);
            };

            vm.deleteMaterialMenuMapping = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteMaterialMenuMappingWarning', myObject.posRefName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            materialmenumappingService.deleteMaterialMenuMapping({
                                id: myObject.posMenuPortionRefId
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                if (vm.allMenu) {
                    vm.menuselection = 1;
                }
                else if (vm.mappedOnly) {
                    vm.menuselection = 2;
                }
                else if (vm.nonmappedOnly) {
                    vm.menuselection = 3;
                }
                vm.loading = true;
                $state.go("tenant.materialmenumappingdetail", {
                    id: objId,
                    filter: vm.filterText,
                    menuselection: vm.menuselection,
                    callingFrom : 'index',
                    materialRefId : null
                });
            }

            vm.exportToExcel = function () {
                materialmenumappingService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.exportNonMappedToExcel = function () {
                materialmenumappingService.getNonMappedToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.exportMappedToExcel = function () {
                materialmenumappingService.getMappedToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.getAll();

            vm.showMenuCost = function (myObj) {
                openView(myObj);
            }

            function openView(ledgerObj) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/MaterialMenuMapping/menuCostingDrillDown.cshtml',
                    controller: 'tenant.views.house.master.materialmenumapping.menuCostingDrillDown as vm',
                    backdrop: 'static',
                    resolve: {
                        portionMenuId: function () {
                            return ledgerObj.posMenuPortionRefId;
                        },
                        menuPortionRefName: function () {
                            return ledgerObj.posRefName;
                        },
                    }
                });

                modalInstance.result.then(function (result) {
                    //vm.getAll();
                });
            }



        }]);
})();

