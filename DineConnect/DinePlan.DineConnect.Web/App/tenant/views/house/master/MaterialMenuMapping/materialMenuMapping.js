﻿(function () {
    appModule.controller('tenant.views.house.master.materialmenumapping.materialMenuMapping', [
        '$scope', '$state', '$stateParams', 'abp.services.app.materialMenuMapping',
        'appSession', 'abp.services.app.material', 'abp.services.app.houseReport', 'abp.services.app.location',
        function ($scope, $state, $stateParams, materialmenumappingService, appSession, materialService, housereportService, locationService) {
            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            vm.materialmenumapping = null;
            vm.materialmenumappinglist = null;
            vm.materialmenumappingId = $stateParams.id;
            vm.sno = 0;
            vm.materialmenumappingPortions = [];
            vm.loading = false;
            vm.materialRateView = [];
            vm.uilimit = null;
            vm.menufilterText = $stateParams.filter;
            vm.menuselection = $stateParams.menuselection;
            vm.callingFrom = $stateParams.callingFrom;
            vm.materialRefId = $stateParams.materialRefId;
            vm.dirtyFlag = false;

            vm.materialmenumappinglistdata = [];
            vm.tempdetaildata = [];
            vm.posMenuPortionRefId = null;
            vm.processCount = 0;
            vm.alllocationflag = true;
            vm.selectedLocationId = null;

            vm.mutlipleUomAllowed = appSession.user.multipleUomEntryAllowed;

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };


            vm.refallowedLocations = [];
            vm.fillUserAllowedLocation = function () {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.refallowedLocations = result.items;
                }).finally(function () {

                });
            };

            vm.fillUserAllowedLocation();

            vm.save = function (argoption) {

                if (vm.alllocationflag) {
                    vm.selectedLocationId = null;
                }
                else {
                    if (vm.selectedLocationId == null || vm.selectedLocationId == 0) {
                        vm.saving = false;
                        abp.notify.error(app.localize('Location') + " ?");
                        return;
                    }
                }

                var errorFlag = false;

                if (vm.menuQuantitySold <= 0 || vm.menuQuantitySold == null) {
                    errorFlag = true;
                    abp.notify.info(app.localize('MenuQuantiyErr'));
                    return;
                }

                if (vm.tempdetaildata.length <= 0) {
                    errorFlag = true;
                    abp.notify.info(app.localize('MinimumOneDetail'));
                    return;
                }
                else {
                    if (vm.tempdetaildata[0].materialRefId == 0) {
                        errorFlag = true;
                        abp.notify.info(app.localize('MinimumOneDetail'));
                        return;
                    }
                    //if (vm.tempdetaildata[0].portionQty <= 0) {
                    //    errorFlag = true;
                    //    abp.notify.info(app.localize('InvalidPortionQty'));
                    //    return;
                    //}
                }

                if (errorFlag) {
                    vm.saving = false;
                    vm.loading = false;
                    return;
                }

                vm.saving = true;
                vm.materialmenumappinglistdata = [];
                angular.forEach(vm.tempdetaildata, function (value, key) {
                    errorFlag = false;
                    var lastelement = value;

                    if (lastelement.materialRefName == null || lastelement.materialRefName == "") {
                        abp.notify.info(app.localize('RecipeRequired'));
                        $("#selRecipe").focus();
                        errorFlag = true;
                        return;
                    } else if (lastelement.portionQty == null || lastelement.portionQty == "" || lastelement.portionQty == 0) {
                        abp.notify.info(app.localize('PortionQty'));
                        $("#selProtionQty").focus();
                        errorFlag = true;
                        return;
                    } else if (lastelement.portionUnitId == null || lastelement.portionUnitId == "") {
                        abp.notify.info(app.localize('UnitRequired'));
                        $("#selUnit").focus();
                        errorFlag = true;
                        return;
                    }

                    if (!vm.isUndefinedOrNull(value.materialRefId) && errorFlag == false) {
                        vm.materialmenumappinglistdata.push({
                            'posMenuPortionRefId': vm.posMenuPortionRefId,
                            'menuQuantitySold': vm.menuQuantitySold,
                            'posRefName': value.posRefName,
                            'userSerialNumber': value.userSerialNumber,
                            'wastageExpected': value.wastageExpected,
                            'materialRefId': value.materialRefId,
                            'materialRefName': value.materialRefName,
                            'materialTypeName': value.materialTypeName,
                            'materialTypeId': value.materialTypeId,
                            'portionQty': value.portionQty,
                            'portionUnitId': value.portionUnitId,
                            'unitRefName': value.unitRefName,
                            'autoSalesDeduction': value.autoSalesDeduction,
                            'deptList': value.deptList,
                            'changeunitflag': false,
                            'locationRefId' : vm.selectedLocationId
                        });
                    }
                    errorFlag = false;
                });

                if (errorFlag) {
                    vm.loading = false;
                    vm.saving = false;
                    return;
                }

                materialmenumappingService.createOrUpdateMaterialMenuMapping({
                    materialMenuMappingDetail: vm.materialmenumappinglistdata
                }).success(function (result) {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.saving = false;
                    if (argoption == 2) {
                        vm.cancel();
                        vm.saving = false;
                        return;
                    }
                    else if (argoption == 3) {
                        vm.menucostingReport();
                        vm.addPortion();
                        return;
                    }
                    else if (argoption == 4) {
                        vm.menucostingReport();
                        return;
                    }
                    resetField();
                    vm.materialmenumappingId = null;
                    vm.tempdetaildata = [];
                    vm.sno = 0;
                    init();
                    $("#posMenuPortionRefId").focus();

                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.removeRow = function (productIndex) {
                var element = vm.tempdetaildata[productIndex];
                if (vm.tempdetaildata.length > 1) {
                    vm.tempdetaildata.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                }
                else {
                    abp.notify.info(app.localize('MinimumOneDetail'));
                    return;
                }

                if (element.portionQty <= 0 || element.portionQty == null || element.portionQty == '') {
                }
                else {
                    vm.save(4);
                }

                //vm.menucostingReport();

                //@@ Pending

                //vm.materialRefresh();
            }



            vm.addPortion = function () {
                vm.uilimit = 50;
                if (vm.sno > 0) {

                    errorFlag = false;

                    var lastelement = vm.tempdetaildata[vm.tempdetaildata.length - 1];

                    if (lastelement.materialRefName == null || lastelement.materialRefName == "") {
                        abp.notify.info(app.localize('RecipeRequired'));
                        $("#selRecipe").focus();
                        return;
                    }

                    if (lastelement.portionQty == null || lastelement.portionQty == "" || lastelement.portionQty <= 0) {
                        abp.notify.info(app.localize('PortionQty'));
                        $("#selProtionQty").focus();
                        return;
                    }

                    if (lastelement.portionUnitId == null || lastelement.portionUnitId == "") {
                        abp.notify.info(app.localize('UnitRequired'));
                        $("#selUnit").focus();
                        return;
                    }

                    if (lastelement.wastageExpected > 0) {

                        if (parseFloat(lastelement.wastageExpected) > parseFloat(lastelement.portionQty)) {
                            abp.notify.info(app.localize('WastagePercentageWrong'));
                            $("#wastage").focus();
                            return;
                        }

                        var wastagePercentage = parseFloat(lastelement.wastageExpected / lastelement.portionQty * 100);
                        if (wastagePercentage > 100) {
                            abp.notify.info(app.localize('WastagePercentageWrong'));
                            $("#wastage").focus();
                            return;
                        }

                        if (wastagePercentage > 10) {
                            abp.message.confirm(
                                app.localize('WastagePercentageMore', wastagePercentage),
                                function (isConfirmed) {
                                    if (!isConfirmed) {
                                        abp.notify.info(app.localize('WastagePercentageWrong'));
                                        $("#wastage").focus();
                                        return;
                                    }
                                }
                            );
                        }
                    }

                    if (vm.refmaterial.length == vm.tempdetaildata.length) {
                        errorFlag = true;
                        abp.notify.info(app.localize('AllTheRecipeAdded'));
                        return;
                    }

                    if (errorFlag) {
                        vm.saving = false;
                        vm.loading = false;
                    }

                }

                vm.sno = vm.sno + 1;

                vm.tempdetaildata.push({
                    'posMenuPortionRefId': vm.posMenuPortionRefId, 'materialRefId': '',
                    'materialRefName': '', 'portionQty': '', 'portionUnitId': 0, 'unitRefName': '',
                    'id': 0, 'userSerialNumber': vm.sno, 'wastageExpected': 0, 'materialTypeName': '',
                    'autoSalesDeduction': false, 'deptList': [], 'changeunitflag': false, 
                });

                $("#selRecipe1").focus();

            }

            vm.portionLength = function () {
                return true;
            }

            vm.materialRefresh = function (objRemove) {
                var indexPosition = -1;

                vm.temprefmaterial = vm.materialRefName;

                angular.forEach(vm.tempdetaildata, function (valueinArray, keyinArray) {
                    indexPosition = -1;
                    var materialtoremove = valueinArray.materialRefName;
                    angular.forEach(vm.temprefmaterial, function (value, key) {
                        if (value.displayText == valueinArray.materialRefName)
                            indexPosition = key;
                    });
                    if (indexPosition >= 0)
                        vm.temprefmaterial.splice(indexPosition, 1);
                });

            }

            $scope.func = function (data, val) {
                var forLoopFlag = true;

                angular.forEach(vm.tempdetaildata, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(val.materialName, value.materialRefName)) {
                            abp.notify.info(app.localize('DuplicateMaterialExists'));
                            forLoopFlag = false;
                        }
                    }
                });

                if (!forLoopFlag) {
                    data.materialRefId = 0;
                    data.materialRefName = '';
                    return;
                }

                data.materialRefName = val.materialName;
                data.materialTypeName = val.materialTypeName;
                data.unitRefName = val.uom;
                data.portionUnitId = val.unitRefId;
                data.materialTypeId = val.materialTypeId;
                if (val.materialTypeId == 0)
                    data.autoSalesDeduction = true;
                if (data.deptList != null && data.deptList.length == 0)
                    data.deptList.push(vm.combodtoAll);
                //fillDropDownUnitBasedOnMaterial(data.materialRefId, data);
            };


            $scope.funcDept = function (data, selectedDept) {
                if (selectedDept.length == 1) {
                    return;
                }


                if (selectedDept != null) {
                    var existDepartment = data.deptList;

                    var keytoremove = -1;
                    var allvalue = null;
                    data.deptList.some(function (deptdata, deptkey) {
                        if (deptdata.value == 0) {
                            keytoremove = deptkey;
                            allvalue = deptdata;
                            return true;
                        }
                    });

                    if (keytoremove >= 0) {
                        data.deptList = [];
                        data.deptList.push(vm.combodtoAll);
                        //data.deptList.splice(keytoremove)
                    }
                }

            };

            function resetField() {
                vm.posMenuPortionRefId = null;
                vm.tempdetaildata = [];
                vm.sno = 0;
                vm.menuQuantitySold = "";
                $scope.checked = false;
                vm.addPortion();
            }


            vm.cancel = function () {
                if (vm.callingFrom == 'material') {
                    $state.go("tenant.materialdetail", {
                        id: vm.materialRefId,
                        changeUnitFlag: false,
                        callingFrom : 'menumapping'
                    });
                }
                else {
                    $state.go("tenant.materialmenumapping", {
                        filter: vm.menufilterText,
                        menuselection: vm.menuselection
                    });
                }
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };


            vm.refdepartments = [];
            vm.combodtoAll = {
                value: "0",
                displayText: app.localize('All')
            };

            function fillDropDownDepartments() {
                materialmenumappingService.getConnectDepartments({}).success(function (result) {
                    vm.refdepartments = result;
                    vm.refdepartments.unshift(vm.combodtoAll);
                });
            }
            fillDropDownDepartments();


            vm.refmaterial = [];

            function fillDropDownRecipe() {
                materialService.getView({
                    skipCount: requestParams.skipCount,
                    maxResultCount: 1,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    operation: 'FillAll',
                }).success(function (result) {
                    vm.refmaterial = result.items;
                });
            }

            fillDropDownRecipe();

            vm.refunit = [];

            function fillDropDownUnit() {
                materialService.getUnitForCombobox({}).success(function (result) {
                    vm.refunit = result.items;
                });
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.refunit = [];
                vm.loading = true;
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1)
                        data.portionUnitId = vm.refunit[0].value;
                    vm.loading = false;
                });
            }


            vm.refmenuitem = [];

            vm.fillDropDownMenuItem = function () {
                vm.loading = true;
                vm.processCount++;
                materialService.getMenuPortionForCombobox({
                    id: vm.nonmappedOnly == true ? 1 : 0
                }).success(function (result) {
                    vm.refmenuitem = result.items;
                    vm.loading = false;
                    vm.processCount--;
                });
            }
            vm.fillDropDownMenuItem();

            vm.requestParams = {
                locations: '',
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.detailData = [];
            vm.defaultLocationId = appSession.user.locationRefId;

            vm.menucostingReport = function () {
                vm.loading = true;
                vm.detailData = [];

                housereportService.getMenuCostingReportForGivenMenuPortionIds({
                    startDate: moment().format($scope.format),
                    endDate: moment().format($scope.format),
                    locationRefId: vm.defaultLocationId,
                    menuPortionRefId: vm.posMenuPortionRefId,
                    materialRateView: []
                }).success(function (result) {
                    vm.detailData = result.recipeWiseCosting;
                    vm.materialRateView = result.materialRateView;
                    vm.dirtyFlag = false;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.clearExistRecords = function () {
                vm.tempdetaildata = [];
            }

            vm.tempdetaildata = [];

            vm.fillDetail = function () {
                vm.loading = true;
                vm.materialmenumappingId = vm.posMenuPortionRefId;
                materialmenumappingService.getMaterialMenuMappingForEdit({
                    posMenuPortionRefId: vm.materialmenumappingId,
                    locationRefId: vm.selectedLocationId
                }).success(function (result) {
                    vm.materialmenumapping = result.materialMenuMappingDetail;
                    vm.tempdetaildata = [];
                    vm.sno = 0;
                    vm.loading = false;
                    if (vm.materialmenumapping.length == 0) {
                        vm.materialmenumapping.portionQty = "";
                        vm.materialmenumapping.userSerialNumber = "";
                        vm.materialmenumapping.wastageExpected = "";
                        vm.menuQuantitySold = '';

                        vm.addPortion();
                    }
                    else {
                        vm.menuQuantitySold = result.materialMenuMappingDetail[0].menuQuantitySold;
                        vm.tempdetaildata = [];
                        vm.materialmenumappingdetail = result.materialMenuMappingDetail;

                        angular.forEach(result.materialMenuMappingDetail, function (value, key) {
                            vm.tempdetaildata.push({
                                'posMenuPortionRefId': value.posMenuPortionRefId,
                                'posRefName': value.posRefName,
                                'userSerialNumber': value.userSerialNumber,
                                'wastageExpected': value.wastageExpected,
                                'materialRefId': value.materialRefId,
                                'materialRefName': value.materialRefName,
                                'materialTypeName': value.materialTypeName,
                                'materialTypeId': value.materialTypeId,
                                'portionQty': value.portionQty,
                                'portionUnitId': value.portionUnitId,
                                'unitRefName': value.unitRefName,
                                'autoSalesDeduction': value.autoSalesDeduction,
                                'id': value.id,
                                'deptList': value.deptList,
                                'changeunitflag': false
                            });

                            vm.sno = vm.tempdetaildata.length;
                            vm.uilimit = null;
                        });

                    }

                    
                });
            }


            vm.getDataForGivenLocations = function () {
                init();
            }

            vm.nonmappedOnly = false;
            function init() {
                

                if (vm.materialmenumappingId != null && vm.materialmenumappingId > 0) {
                    vm.loading = true;
                    vm.posMenuPortionRefId = vm.materialmenumappingId;
                    $('#posSelectId').prop("disabled", true);
                    vm.fillDetail();
                    vm.menucostingReport();
                }
            }

            init();


            vm.changeUnit = function (data) {
                if (!vm.mutlipleUomAllowed) {
                    abp.notify.warn(app.localize('DoNotHaveRightsToChangeUom'));
                    return;
                }

                data.changeunitflag = true;
                fillDropDownUnitBasedOnMaterial(data.materialRefId, data)
            }

            function fillDropDownUnitBasedOnMaterial(selectmaterialId, data) {
                vm.loading = true;
                vm.refunit = [];
                materialService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    if (vm.refunit.length == 1) {
                        data.portionUnitId = vm.refunit[0].value;
                        data.unitRefName = vm.refunit[0].displayText;
                        data.changeunitflag = false;
                    }

                    vm.loading = false;
                });
            }

            vm.unitReference = function (selected, data) {
                data.portionUnitId = selected.value;
                data.unitRefName = selected.displayText;
                data.changeunitflag = false;
            }

            vm.deleteMenuMappingList = function (data, index) {
                abp.message.confirm(
                    app.localize('DeleteMaterialMenuMappingWarning', data.materialRefName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            vm.deleteMaterialList(data, index);
                        }
                    }
                );
            }



            vm.deleteMaterialList = function (data, index) {
                vm.sentdetail = [];
                if (vm.alllocationflag == true) {
                    vm.sentdetail.push({
                        'posMenuPortionRefId': vm.posMenuPortionRefId,
                        'materialRefId': data.materialRefId,
                        'locationRefId': null
                    });
                }
                else {
                    if (vm.selectedLocationId == null || vm.selectedLocationId == 0) {
                        vm.saving = false;
                        abp.notify.error(app.localize('Location') + " ?");
                        return;
                    }
                    vm.sentdetail.push({
                        'posMenuPortionRefId': vm.posMenuPortionRefId,
                        'materialRefId': data.materialRefId,
                        'locationRefId': vm.selectedLocationId
                    });
                }

                vm.loading = true;
                vm.saving = true;

                materialmenumappingService.deleteMaterialMenuMappingList({
                    materialMenuWithLocations: vm.sentdetail
                }).success(function (result) {
                    if (result == true) {
                        abp.notify.error(app.localize('SavedSuccessfully'));
                        vm.removeRow(index);
                    }
                    else {
                        vm.removeRow(index);
                    }
                }).finally(function () {
                    vm.saving = false;
                    vm.loading = false;
                });
            }


            vm.ingrdetailDataShown = function (data) {
                var modalInstance1 = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/housereport/ingrPriceListledgerModal.cshtml',
                    controller: 'tenant.views.house.master.housereport.ingrPriceListledgerModal as vm',
                    backdrop: 'static',
                    resolve: {
                        priceListdetails: function () {
                            return data.materialIngredientRequiredDtos;
                        },
                        materialRefId: function () {
                            return data.materialRefId;
                        },
                        materialRefName: function () {
                            return data.materialRefName;
                        },
                    }
                });
            }

        }

    ]);
})();
