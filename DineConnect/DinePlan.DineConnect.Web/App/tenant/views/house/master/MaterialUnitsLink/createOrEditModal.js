﻿
(function () {
    appModule.controller('tenant.views.house.master.materialunitslink.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.materialUnitsLink', 'materialunitslinkId', 'abp.services.app.commonLookup',
        function ($scope, $modalInstance, materialunitslinkService, materialunitslinkId, commonLookupService) {
            var vm = this;
            /* eslint-disable */

            vm.saving = false;
            vm.materialunitslink = null;
			$scope.existall = true;
			vm.save = function () {
			   if ($scope.existall == false)
			       return;
			   vm.saving = true;

			materialunitslinkService.createOrUpdateMaterialUnitsLink({
                materialUnitsLink: vm.materialunitslink
            }).success(function () {
                abp.notify.info('\' MaterialUnitsLink \'' + app.localize('SavedSuccessfully'));
                $modalInstance.close();
            }).finally(function () {
                vm.saving = false;
            });
        };

			 vm.existall = function ()
			 {
			     $scope.existall = true;
			     return;
                materialunitslinkService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.materialunitslink.id,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.materialunitslink.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.materialunitslink.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            
			vm.refmaterial = [];

			 function fillDropDownMaterial() {
			     	commonLookupService.getMaterialForCombobox({}).success(function (result) {
                    vm.refmaterial = result.items;
                });
			 }

			 vm.refunit = [];

			 function fillDropDownUnit() {
			     commonLookupService.getUnitForCombobox({}).success(function (result) {
			         vm.refunit = result.items;
			         vm.fillUnits();
			     });
			 }

			 vm.fillUnits = function () {
			     var totCnt = vm.refunit.length;
			     for (i = 0; i < totCnt; i++) {
			         $('#selId1').multiSelect('addOption', { value: vm.refunit[i].value, text: vm.refunit[i].displayText, index: i });
			     }
			         $('#selId1').multiSelect('select', [vm.refunit[0].value,vm.refunit[3].value]);
			     
			     
			 }

			 vm.selectedList = function () {
			     //var selectedValues = $('#selId1').val();

			     $('#selId1 :selected').each(function (i, sel) {

			     });
			 }

	        function init() {
				fillDropDownMaterial();
				fillDropDownUnit();
                materialunitslinkService.getMaterialUnitsLinkForEdit({
                    Id: materialunitslinkId
                }).success(function (result) {
                    $scope.tempopMode = "";
                    if (result.materialUnitsLink.id != null) {

                    }
                    vm.materialunitslink = result.materialUnitsLink;
                    $('#selId1').multiSelect();
             
                });
            }
            init();
        }
    ]);
})();

