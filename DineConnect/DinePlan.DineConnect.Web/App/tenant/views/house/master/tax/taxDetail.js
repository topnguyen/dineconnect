﻿(function () {
    appModule.controller('tenant.views.house.master.tax.taxDetail', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'abp.services.app.tax', 'abp.services.app.commonLookup', 'uiGridConstants', 'abp.services.app.location',
        function ($scope, $filter, $state, $stateParams,
            $modal, taxService, commonLookupService, uiGridConstants, locationService) {
            var vm = this;
            vm.saving = false;
            vm.tax = null;
            taxId = $stateParams.id;
            $scope.existall = true;
            vm.taxTemplateMapping = [];
            vm.sno = 0;
            vm.loading = false;
            

            vm.addPortion=function()
            {
                vm.sno = vm.sno + 1;
                vm.taxTemplateMapping.push({
                    'taxRefId': 0,
                    'materialGroupCategoryRefId': '',
                    'materialRefId': '',
                    'materialGroupRefId': '',
                    'companyRefId': '',
                    'locationRefId': ''
                });
            }



            vm.portionLength = function () {
                return true;
            }


            vm.removeRow = function (productIndex) {
                var element = vm.taxTemplateMapping[productIndex];

                if (vm.taxTemplateMapping.length > 1) {
                    vm.taxTemplateMapping.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;

                }
                else {
                    vm.taxTemplateMapping.splice(productIndex, 1);
                    vm.sno = 0;
                    return;
                }

            }

            vm.save = function () {
                if ($scope.existall == false)
                    return;
                $scope.errmessage = "";

                if (parseFloat(vm.tax.percentage)>100)
                    $scope.errmessage = $scope.errmessage + app.localize('PercentageErr', vm.tax.taxName);

                if (vm.tax.taxCalculationMethod == "" || vm.tax.taxCalculationMethod == null)
                    $scope.errmessage = $scope.errmessage + app.localize('TaxCalSelErr', vm.tax.taxCalculationMethod);

                if ($scope.errmessage != "") {
                    abp.notify.warn($scope.errmessage, app.localize('RequiredFields'));
                    return;
                }

                //  Setting up Tax Method 

                $scope.temptaxMethod = "";
                if (vm.tax.taxCalculationMethod == app.localize('OnGrandTotal')) {
                    $scope.temptaxMethod = "GT";
                    vm.tax.sortOrder = -2;
                }
                else if (vm.tax.taxCalculationMethod == app.localize('OnSubTotal')) {
                    $scope.temptaxMethod = "ST";
                    vm.tax.sortOrder = -1;
                }
                else {
                    $scope.temptaxMethod = vm.tax.taxCalculationMethod;
                    vm.reftaxesonavailable.some(function (value, key) {
                        if (value.displayText == $scope.temptaxMethod) {
                            vm.tax.sortOrder = value.value;
                            return true;
                        }
                    });
                }

                vm.tax.taxCalculationMethod = $scope.temptaxMethod;
                $scope.temptaxMethod = "";

                vm.saving = true;
                vm.tax.taxName = vm.tax.taxName.toUpperCase();

                taxService.createOrUpdateTax({
                    tax: vm.tax,
                    TaxTemplateMapping : vm.taxTemplateMapping
                }).success(function () {
                    abp.notify.info('\' Tax \'' + app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {
                if (vm.tax.taxName == null) {
                    vm.existall = false;
                    return;
                }
                taxService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'taxName',
                    filter: vm.tax.taxName,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.tax.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.tax.taxName + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $state.go('tenant.tax');
            };



            vm.funcMaterialGroup = function (data) {
                var grpId = data.materialGroupRefId;
                fillDropDownMaterialGroupCategory(grpId);
            };

            vm.funcMaterialCategory = function (data) {
                var catId = data.materialGroupCategoryRefId;
                fillDropDownMaterial(catId);
            };

            vm.reftaxesonavailable = [];

            function fillDropDownTaxes() {
                vm.loading = true;
                commonLookupService.getTaxForCombobox({}).success(function (result) {
                    vm.reftaxesonavailable = result.items;
                    vm.reftaxesonavailable.unshift({ value: "-1", displayText: app.localize('OnSubTotal') });
                    vm.reftaxesonavailable.unshift({ value: "-2", displayText: app.localize('OnGrandTotal') });
                    vm.loading = false;
                });
            }

            vm.refmaterial = [];
            function fillDropDownMaterial(grpCatId) {
                vm.loading = true;
                commonLookupService.getMaterialByGroupCategory({ id: grpCatId }).success(function (result) {
                    vm.refmaterial = result.items;
                    vm.loading = false;
                });
            }

            vm.refmaterialgroupcategory = [];

            function fillDropDownMaterialGroupCategory(grpId) {
                vm.loading = true;
                commonLookupService.getMaterialGroupCategoryByGroupId({ id: grpId }).success(function (result) {
                    vm.refmaterialgroupcategory = result.items;
                    vm.loading = false;
                }).finally(function () {
                    fillDropDownMaterial(null);
            });
            }

            vm.refmaterialgroup = [];
            function fillDropDownMaterialGroup() {
                vm.loading = true;
                commonLookupService.getMaterialGroupForCombobox({}).success(function (result) {
                    vm.refmaterialgroup = result.items;
                    vm.loading = false;
                }).finally(function () {
                    fillDropDownMaterialGroupCategory(null);
                });
            }

            vm.refLocation = [];
            function fillDropDownLocation() {
                commonLookupService.getLocationForCombobox({}).success(function (result) {
                    vm.refLocation = result.items;
                });
            }

            vm.refCompany = [];
            function fillDropDownCompany() {
                locationService.getCompanyForCombobox({}).success(function (result) {
                    vm.refCompany = result.items;
                }).finally(function () {
                    fillDropDownLocation();
                });
            }

            function init() {
           

                taxService.getTaxForEdit({
                    Id: taxId
                }).success(function (result) {
                    vm.tax = result.tax;
                    vm.taxTemplateMapping = result.taxTemplateMapping;

                    if (vm.tax.id == null) {
                        vm.tax.percentage = "";
                        vm.addPortion();
                    }
                    else {
                        $scope.temptaxMethod = "";
                        if (vm.tax.taxCalculationMethod == "GT")
                            $scope.temptaxMethod = app.localize('OnGrandTotal');
                        else if (vm.tax.taxCalculationMethod == "ST")
                            $scope.temptaxMethod = app.localize('OnSubTotal');
                        else
                            $scope.temptaxMethod = vm.tax.taxCalculationMethod;

                        vm.tax.taxCalculationMethod = $scope.temptaxMethod;
                        $scope.temptaxMethod = "";
                     
                    }
                    fillDropDownTaxes();
                    fillDropDownCompany();
                    fillDropDownMaterialGroup();
                });
            }

            init();
        }
    ]);
})();

    