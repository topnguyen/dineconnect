﻿(function () {
    appModule.controller('tenant.views.house.transaction.materialledger.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.materialLedger', 'abp.services.app.location', 'abp.services.app.material', 'appSession', 'abp.services.app.dayClose',
        function ($scope, $modal, uiGridConstants, materialledgerService, locationService, materialService, appSession, daycloseService) {
            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            console.log(appSession);

            vm.loading = true;
            vm.loadingCount = 0;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.uilimit = 20;
            vm.materialRefIds = [];
            vm.currentLocation = appSession.location.name;


            vm.locationRefId = vm.defaultLocationId;
            vm.materialRefId = null;
            vm.materialLedgerData = [];

            vm.permissions = {

                view: abp.auth.hasPermission('Pages.Tenant.House.Master.MaterialLedger.View'),
            };

            if (vm.permissions.view == false) {
                if (vm.permissions.create == true)
                    vm.permissions.view = true;
            }


            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.clear = function () {
                vm.materialRefId = null;
                vm.materialRefIds = [];
                vm.getAll('Clear');
            }

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',

                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll('Pagination');
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll('Pagination');
                    });
                },
                data: []
            };



            vm.refmaterials = [];

            function fillDropDownMaterial() {
                vm.loadingCount++;
                materialService.getMaterialForCombobox({}).success(function (result) {
                    vm.refmaterials = result.items;
                    vm.loadingCount--;
                    vm.loading = false;
                });
            }

            fillDropDownMaterial();

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];


            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.getAll = function (argOption) {
                vm.loading = true;
                materialledgerService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationRefId: vm.locationRefId,
                    materialRefIds: vm.materialRefIds,
                    userId: vm.currentUserId,
                    calledReason : argOption
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                    vm.materialLedgerData = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.exportToExcel = function () {
                materialledgerService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationRefId: vm.locationRefId,
                    materialRefId: vm.materialRefId,
                    userId: vm.currentUserId
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };




            vm.mediumColumnDefs = [
                {
                    name: 'Actions',
                    enableSorting: false,
                    width: 50,
                    headerCellTemplate: '<span></span>',
                    cellTemplate:
                        '<div class=\"ui-grid-cell-contents text-center\">' +
                        '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.showDetails(row.entity)"><i class="fa fa-eye"></i></button>' +
                        '</div>'
                },
                {
                    name: app.localize('Location'),
                    field: 'locationRefName'
                },
                {
                    name: app.localize('Material'),
                    field: 'materialRefName',
                    width: 180
                },
                {
                    name: app.localize('UOM'),
                    field: 'uom'
                },
                {
                    name: app.localize('Date'),
                    field: 'ledgerDate',
                    cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                },
                {
                    name: app.localize('Open'),
                    field: 'openBalance',
                    cellClass: 'ui-ralign'
                },
                {
                    name: app.localize('Received'),
                    field: 'overAllReceived',
                    cellClass: 'ui-ralign'
                },
                {
                    name: app.localize('Issued'),
                    field: 'overAllIssued',
                    cellClass: 'ui-ralign'
                },
                {
                    name: app.localize('Close'),
                    field: 'clBalance',
                    cellClass: 'ui-ralign'
                }
            ];

            vm.largeColumnDefs = [
                {
                    name: 'Actions',
                    enableSorting: false,
                    width: 50,
                    headerCellTemplate: '<span></span>',
                    cellTemplate:
                        '<div class=\"ui-grid-cell-contents text-center\">' +
                        '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.showDetails(row.entity)"><i class="fa fa-search"></i></button>' +
                        '</div>'
                },
                {
                    name: app.localize('LocationRefName'),
                    field: 'locationRefName'
                },
                {
                    name: app.localize('MaterialRefName'),
                    field: 'materialRefName',
                    width: 180
                },
                {
                    name: app.localize('LedgerDate'),
                    field: 'ledgerDate',
                    cellFilter: 'momentFormat : \'MMM Do YY\'',
                },
                {
                    name: app.localize('OpenBalance'),
                    field: 'openBalance',
                    cellClass: 'ui-ralign'
                },
                {
                    name: app.localize('Received'),
                    field: 'received',
                    cellClass: 'ui-ralign'
                },
                {
                    name: app.localize('Issued'),
                    field: 'issued',
                    cellClass: 'ui-ralign'
                },
                {
                    name: app.localize('TransferIn'),
                    field: 'transferIn',
                    cellClass: 'ui-ralign'
                },
                {
                    name: app.localize('TransferOut'),
                    field: 'transferOut',
                    cellClass: 'ui-ralign'
                },
                {
                    name: app.localize('Excess'),
                    field: 'excessReceived',
                    cellClass: 'ui-ralign'
                },
                {
                    name: app.localize('Shortage'),
                    field: 'shortage',
                    cellClass: 'ui-ralign'
                },
                {
                    name: app.localize('Damaged'),
                    field: 'damaged',
                    cellClass: 'ui-ralign'
                },
                {
                    name: app.localize('Return'),
                    field: 'return',
                    cellClass: 'ui-ralign'
                },
                {
                    name: app.localize('ClBalance'),
                    field: 'clBalance',
                    cellClass: 'ui-ralign'
                },
            ];

            vm.toggleSwitch = 1;
            vm.userGridOptions.columnDefs = vm.mediumColumnDefs;

            vm.viewLedgerDetail = function (ledgerObj) {
                openView(ledgerObj);
            };



            function openView(ledgerObj) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/materialledger/ledgermodal.cshtml',
                    controller: 'tenant.views.house.master.materialledger.ledgerModal as vm',
                    backdrop: 'static',
                    resolve: {
                        ledgerDetails: function () {
                            return ledgerObj;
                        }
                    }
                });

                modalInstance.result.then(function (result) {

                });
            }

            vm.showDetails = function (row) {
                vm.viewLedgerDetail(row);
            }


            vm.checkCloseDay = function () {
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {

                        //if (vm.softmessagenotificationflag)
                        //    abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        //if (vm.messagenotificationflag)
                        //    abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                    todayAsString = moment(result.accountDate).format('YYYY-MM-DD');

                    vm.dateRangeModel = {
                        startDate: todayAsString,
                        endDate: todayAsString
                    };

                    vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
                    vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;


                }).finally(function (result) {
                    //vm.getAll('Check Close Day');
                });
            }

            vm.checkCloseDay();


            vm.openLocation = function () {

                vm.locationGroup = app.createLocationInputForCreateSingleLocation();

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    console.log(result);

                    if (result.locations.length > 0) {
                        vm.locationRefId = result.locations[0].id;
                        vm.currentLocation = result.locations[0].name;
                        vm.getAll('Change Location');
                    }

                });
            }


        }]);
})();

