﻿(function () {
    appModule.controller('tenant.views.house.master.materialledger.drillDownLevel', [
        '$scope', '$uibModalInstance', 'ledgerDetails', 'searchFor',
        'abp.services.app.material', 'startDate', 'endDate',
        function ($scope, $modalInstance, ledgerDetails, searchFor, materialService, startDate, endDate) {
            /* eslint-disable */
            var vm = this;
            vm.materialLedgerData = ledgerDetails;
            vm.startDate = startDate;
            vm.endDate = endDate;

            if (searchFor == 'Sales') {
                vm.loading = true;
                materialService.getSalesDetail(
                    {
                        locationRefId: ledgerDetails.locationRefId,
                        transactionStartDate: vm.startDate,
                        transactionEndDate: vm.endDate,
                        materialRefId: ledgerDetails.materialRefId
                    }).success(function (result) {
                        vm.detailData = result;
                    }).finally(function () {
                        vm.loading = false;
                    });

            }

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

        }
    ]);
})();