﻿(function () {
    appModule.controller('tenant.views.house.master.materialledger.ledgerModal', [
        '$scope', '$uibModalInstance', '$uibModal', 'ledgerDetails', 'abp.services.app.unitConversion', 'abp.services.app.material',
        function ($scope, $modalInstance, $modal, ledgerDetails, unitconversionService, materialService) {
            var vm = this;
            /* eslint-disable */

            vm.materialLedgerData = ledgerDetails;
            vm.refconversionunit = [];
            vm.refunit = [];

            vm.transactionUnitRefId = vm.materialLedgerData.transactionUnitRefId;
            vm.transactionUnitRefName = vm.materialLedgerData.transactionUnitRefName;

            vm.isTransactionUnitDifferentWithDefaultUnit = false;

            vm.convFactor = 0;
            vm.decimalPlaceRounding = 0;
            vm.fillUnitConversion = function () {
            	vm.refconversionunit = [];
            	vm.loading = true;
            	unitconversionService.getAll({
            	}).success(function (result) {
            		vm.refconversionunit = result.items;
            		vm.loading = false;

            		if (vm.materialLedgerData.defaultUnitRefId != vm.transactionUnitRefId) {
            			vm.isTransactionUnitDifferentWithDefaultUnit = true;
            			vm.transactionUnitmaterialLedgerData = angular.copy(ledgerDetails);
            			vm.convFactor = 0;
            			var convExistFlag = false;
            			vm.refconversionunit.some(function (refdata, refkey) {
            				if (refdata.baseUnitId == vm.materialLedgerData.defaultUnitRefId && refdata.refUnitId == vm.transactionUnitRefId) {
            					vm.convFactor = refdata.conversion;
            					vm.decimalPlaceRounding = refdata.decimalPlaceRounding;
                                convExistFlag = true;
                                if (vm.decimalPlaceRounding > 6)
                                    vm.decimalPlaceRounding = 6;
            					return true;
            				}
            			});

            			if (convExistFlag == false) {
            				abp.notify.info(app.localize('ConversionFactorNotExist', data.uom, data.unitRefName));
            				return;
            			}

            			if (vm.convFactor != 1) {
            				vm.convertData(vm.transactionUnitmaterialLedgerData, vm.convFactor, vm.decimalPlaceRounding);
            			}
            		}

            	});
            }

            vm.fillDropDownUnitBasedOnMaterial = function(selectmaterialId) {
            	vm.loading = true;
            	materialService.getUnitForMaterialLinkCombobox({
            		id: selectmaterialId
            	}).success(function (result) {
            		vm.refunit = result.items;
            		if (vm.refunit.length == 1) {
            			vm.changeunitflag = false;
            		}
            		else {
            			vm.fillUnitConversion();
            		}
            		vm.loading = false;
            	});
            }

            vm.fillDropDownUnitBasedOnMaterial(vm.materialLedgerData.materialRefId)

            vm.convFactor = 0;

            vm.unitReference = function (selected, data) {
            	vm.transactionUnitRefId = selected.value;
            	vm.transactionUnitRefName = selected.displayText;
            	vm.changeunitflag = false;

            	vm.convFactor = 0;
            	var convExistFlag = false;
            	vm.refconversionunit.some(function (refdata, refkey) {
            		if (refdata.baseUnitId == data.defaultUnitId && refdata.refUnitId == data.unitRefId) {
            			vm.convFactor = refdata.conversion;
            			convExistFlag = true;
            			return true;
            		}
            	});

            	if (convExistFlag == false) {
            		abp.notify.info(app.localize('ConversionFactorNotExist', data.uom, data.unitRefName));
            		return;
            	}

            	vm.transactionUnitmaterialLedgerData = angular.copy(ledgerDetails);
            	if (convFactor != 1) {
            		vm.convertData(vm.transactionUnitmaterialLedgerData, vm.convFactor);
            	}
            }

            vm.convertData = function (data, conversionFactor, decimalPlaceRounding) {
            	data.openBalance = parseFloat((data.openBalance * conversionFactor).toFixed(decimalPlaceRounding));
            	data.received = parseFloat((data.received * conversionFactor).toFixed(decimalPlaceRounding));
            	data.supplierReturn = parseFloat((data.supplierReturn * conversionFactor).toFixed(decimalPlaceRounding));
            	data.issued =parseFloat((data.issued * conversionFactor).toFixed(decimalPlaceRounding));
            	data.sales = parseFloat((data.sales * conversionFactor).toFixed(decimalPlaceRounding));
            	data.transferIn = parseFloat((data.transferIn * conversionFactor).toFixed(decimalPlaceRounding));
            	data.transferOut = parseFloat((data.transferOut * conversionFactor).toFixed(decimalPlaceRounding));
            	data.excessReceived = parseFloat((data.excessReceived * conversionFactor).toFixed(decimalPlaceRounding));
            	data.shortage = parseFloat((data.shortage * conversionFactor).toFixed(decimalPlaceRounding));
            	data.damaged = parseFloat((data.damaged * conversionFactor).toFixed(decimalPlaceRounding));
            	data.return = parseFloat((data.return * conversionFactor).toFixed(decimalPlaceRounding));
            	data.clBalance = parseFloat((data.clBalance * conversionFactor).toFixed(decimalPlaceRounding));
            }

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.detailDataShown = function (argOption)
            {
                argOption = 'Sales';
                var modalInstance1 = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/materialledger/drillDownLevel.cshtml',
                    controller: 'tenant.views.house.master.materialledger.drillDownLevel as vm',
                    backdrop: 'static',
                    resolve: {
                        ledgerDetails: function () {
                            return ledgerDetails;
                        },
                        searchFor: function () {
                            return argOption;
                        },
                        startDate: function () {
                            return ledgerDetails.ledgerDate;
                        },
                        endDate: function () {
                            return ledgerDetails.ledgerDate;
                        },
                    }
                });

                modalInstance1.result.then(function (result) {
                    //vm.getAll();
                });
            }

        }
    ]);
})();