﻿
(function () {
    appModule.controller('tenant.views.house.master.customermaterial.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.customerMaterial',
        function ($scope, $modal, uiGridConstants, customermaterialService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.CustomerMaterial.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.CustomerMaterial.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.CustomerMaterial.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
					{
						name: app.localize('Actions'),
						enableSorting: false,
						width: 120,

						cellTemplate:
						   "<div class=\"ui-grid-cell-contents\">" +
							    "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
							   "    <ul uib-dropdown-menu>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editCustomerMaterial(row.entity)\">" + app.localize("Edit") + "</a></li>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteCustomerMaterial(row.entity)\">" + app.localize("Delete") + "</a></li>" +
							   "    </ul>" +
							   "  </div>" +
							   "</div>"
					},
                     {
                         name: app.localize('CustomerName'),
                         field: 'customerRefName'
                     },
                    {
                        name: app.localize('MaterialName'),
                        field: 'materialRefName'
                    },
                    {
                        name: app.localize('MaterialPrice'),
                        field: 'materialPrice'
                    },
                    {
                        name: app.localize('LastQuoteRefNo'),
                        field: 'lastQuoteRefNo'
                    },
                    {
                        name: app.localize('LastQuoteDate'),
                        field: 'lastQuoteDate'
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

         

            vm.getAll = function () {
                vm.loading = true;
                customermaterialService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editCustomerMaterial = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createCustomerMaterial = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteCustomerMaterial = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteCustomerMaterialWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            customermaterialService.deleteCustomerMaterial({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/customermaterial/createOrEditModal.cshtml',
                    controller: 'tenant.views.house.master.customermaterial.createOrEditModal as vm',
                    backdrop: 'static',
					keyboard: false,
                    resolve: {
                        customermaterialId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.exportToExcel = function () {
                customermaterialService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getAll();
        }]);
})();

