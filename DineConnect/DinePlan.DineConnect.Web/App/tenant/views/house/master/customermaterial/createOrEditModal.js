﻿(function () {
    appModule.controller('tenant.views.house.master.customermaterial.createOrEditModal', [
        '$scope', '$uibModalInstance', '$q', '$uibModal', '$filter', 'abp.services.app.customerMaterial', 'customermaterialId', 'abp.services.app.commonLookup', 'lookupModal', 'uiGridConstants',
        function ($scope, $modalInstance, $q, $modal, $filter, customermaterialService, customermaterialId, commonLookupService, lookupModal, uiGridConstants) {
            var vm = this;
            vm.saving = false;
            vm.customermaterial = null;
            $scope.existall = true;

            vm.save = function () {
                if ($scope.existall == false)
                    return;
                vm.saving = true;
                vm.customermaterial.lastQuoteDate = "2016-04-16";
                vm.customermaterial.lastQuoteRefNo = "QUOTE16/04";

                customermaterialService.createOrUpdateCustomerMaterial({
                    customerMaterial: vm.customermaterial
                }).success(function () {
                    abp.notify.info('\' CustomerMaterial \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {

                if (vm.customermaterial.id == null) {
                    vm.existall = false;
                    return;
                }

                customermaterialService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.customermaterial.id,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.customermaterial.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.customermaterial.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
					{
					    name: app.localize('Actions'),
					    enableSorting: false,
					    width: 120,

					    cellTemplate:
						   "<div class=\"ui-grid-cell-contents\">" +
							   "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
							   "    <ul uib-dropdown-menu>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editCustomerMaterial(row.entity)\">" + app.localize("Edit") + "</a></li>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteCustomerMaterial(row.entity)\">" + app.localize("Delete") + "</a></li>" +
							   "    </ul>" +
							   "  </div>" +
							   "</div>"
					},
                     {
                         name: app.localize('Id'),
                         field: 'id'
                     },
                     {
                         name: app.localize('Customer Name'),
                         field: 'customerRefName'
                     },
                    {
                        name: app.localize('Customer Name'),
                        field: 'customerRefName'
                    },
                    {
                        name: app.localize('Material Name'),
                        field: 'materialRefName'
                    },
                    {
                        name: app.localize('Material Price'),
                        field: 'materialPrice'
                    },
                    {
                        name: app.localize('LastQuoteRefNo'),
                        field: 'lastQuoteRefNo'
                    },
                    {
                        name: app.localize('LastQuoteDate'),
                        field: 'lastQuoteDate'
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.CustomerMaterial.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.CustomerMaterial.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.CustomerMaterial.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };


            vm.getAll = function () {
                vm.loading = true;
                customermaterialService.getView({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.refcustomer = [];

            function fillDropDownCustomer() {
                commonLookupService.getCustomerForCombobox({}).success(function (result) {
                    vm.refcustomer = result.items;
                });
            }


            vm.refmaterial = [];

            function fillDropDownMaterial() {
                commonLookupService.getMaterialForCombobox({}).success(function (result) {
                    vm.refmaterial = result.items;
                });
            }
            function init() {
                fillDropDownCustomer();
                fillDropDownMaterial();
                customermaterialService.getCustomerMaterialForEdit({
                    Id: customermaterialId
                }).success(function (result) {
                    vm.customermaterial = result.customerMaterial;
                    if (vm.customermaterial.id == null) {
                        vm.customermaterial.materialPrice = "";
                    }
                });
            }
            init();
        }
    ]);
})();
