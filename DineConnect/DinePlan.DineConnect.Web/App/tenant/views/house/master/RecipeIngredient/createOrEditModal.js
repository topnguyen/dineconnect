﻿
(function () {
    appModule.controller('tenant.views.house.master.recipeingredient.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.recipeIngredient', 'recipeingredientId', 'abp.services.app.commonLookup',
        function ($scope, $modalInstance, recipeingredientService, recipeingredientId, commonLookupService) {
            var vm = this;
            
            vm.saving = false;
            vm.recipeingredient = null;
            $scope.existall = true;
            
			vm.save = function () {

			    $scope.errmessage = "";

			    if (vm.recipeingredient.userSerialNumber == 0 || vm.recipeingredient.userSerialNumber == null)
			        $scope.errmessage = $scope.errmessage + app.localize('UserSrNoErr');
			    if (vm.recipeingredient.recipeRefId == 0 || vm.recipeingredient.userSerialNumber == null)
			        $scope.errmessage = $scope.errmessage + app.localize('RecipeNilErr');
			    if (vm.recipeingredient.materialRefId == 0 || vm.recipeingredient.materialRefId == null)
			        $scope.errmessage = $scope.errmessage + app.localize('MaterialNilErr');
			    if (vm.recipeingredient.materialUsedQty == 0 || vm.recipeingredient.materialUsedQty == null)
			        $scope.errmessage = $scope.errmessage + app.localize('QtyZeroErr');
			    if (vm.recipeingredient.unitRefId == 0 || vm.recipeingredient.unitRefId == null)
			        $scope.errmessage = $scope.errmessage + app.localize('UnitMisErr');
			    if ($scope.errmessage != "") {
			        $scope.existall = false;
			        $scope.errmessage = "\n" + $scope.errmessage;
			        abp.notify.warn("Required Fields " + $scope.errmessage)
			        $scope.errmessage = "";
			        return;
			    }

			    if (vm.recipeingredient.variationflagForProduction == false)
			    {
			        abp.message.confirm(
                        app.localize('Are You Sure For Multiplication Flag as False for this Material !!!\r\n This may Result in Project Error'),
                        function (isConfirmed) {
                        if (!isConfirmed) {
                            return;
                        }
                    }
                );
			    }

			   if (vm.existall == false)
                    return;
                vm.saving = true;
				
                recipeingredientService.createOrUpdateRecipeIngredient({
                    recipeIngredient: vm.recipeingredient
                }).success(function () {
                    abp.notify.info('\' RecipeIngredient \'' + app.localize('SavedSuccessfully'));
                    //$modalInstance.close();
                    vm.getDetailData();
                    vm.recipeingredient.materialUsedQty = "";
                    vm.recipeingredient.materialCode = 0;
                    vm.recipeingredient.unitRefId = 0;
                    vm.recipeingredient.userSerialNumber = "";
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
			 {
			     if (vm.recipeingredient.id == null) {
			         vm.existall = false;
			         return;
			     }
				 
                recipeingredientService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: '',
                    filter: vm.recipeingredient.id,
                    operation: 'SEARCH'
                }).success(function (result) {
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.recipeingredient.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.recipeingredient.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

			 vm.cancel = function () {
                $modalInstance.close();
            };
			

			 var requestParams = {
			     skipCount: 0,
			     maxResultCount: app.consts.grid.defaultPageSize,
			     sorting: null
			 };

            
			 vm.detailData = [];
            
			 vm.getDetailData = function () {
			     vm.loading = true;
			     vm.filterText = null;
			    
			     vm.detailData = [];
			     //if ($("#selrecipename option:selected").text() != "")
			     //    vm.filterText = $("#selrecipename option:selected").text();

			     recipeingredientService.getViewByRefRecipeId({ 
			         Id: vm.recipeingredient.recipeRefId
			     }).success(function (result) {
			         vm.detailData = result.items;
			         vm.recipeingredient.userSerialNumber = result.items.length + 1;
			     }).finally(function () {
			         vm.loading = false;
			         vm.fillDropDownMaterial();
			     });
			 };

			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
			 vm.refrecipe = [];

			function fillDropDownRecipe() {
			     	commonLookupService.getRecipeForCombobox({}).success(function (result) {
                    vm.refrecipe = result.items;
                });
			 }

			 vm.refunit = [];

			 vm.fillDropDownUnit = function() {
			     commonLookupService.getUnitForCombobox({ Id : vm.recipeingredient.materialCode}).success(function (result) {
			         vm.refunit = result.items;
			         if (vm.refunit.length == 1)
			             vm.recipeingredient.unitRefId = parseInt(vm.refunit[0].value);
			     });
			 }

			 vm.refmaterial = [];

			 vm.fillDropDownMaterial = function() {
			     commonLookupService.getMaterialExcludedFromParticularRecipeForCombobox({ Id: vm.recipeingredient.recipeRefId}).success(function (result) {
			         vm.refmaterial = result.items;
			     });
			 }


	        function init() {
	            fillDropDownRecipe();
	            
                recipeingredientService.getRecipeIngredientForEdit({
                    Id: recipeingredientId
                }).success(function (result) {
                    vm.recipeingredient = result.recipeIngredient;
                    if (result.recipeIngredient.id == null)
                    {
                        vm.recipeingredient.userSerialNumber = "";
                        vm.recipeingredient.materialUsedQty = "";
                        vm.recipeingredient.variationflagForProduction = true;
                        vm.fillDropDownMaterial();
                    }
                    else
                    {
                        vm.fillDropDownUnit();
                    }
                });

	        }

	        init();
	        
        }
    ]);
})();

