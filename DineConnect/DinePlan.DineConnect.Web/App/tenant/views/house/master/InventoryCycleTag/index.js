﻿
(function () {
    appModule.controller('tenant.views.house.master.inventorycycletag.index', [
			'$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.inventoryCycleTag',
			function ($scope, $state, $modal, uiGridConstants, inventorycycletagService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.InventoryCycleTag.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.InventoryCycleTag.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.InventoryCycleTag.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
							enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
							enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
							paginationPageSizes: app.consts.grid.defaultPageSizes,
							paginationPageSize: app.consts.grid.defaultPageSize,
							useExternalPagination: true,
							useExternalSorting: true,
							appScopeProvider: vm,
							rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
							columnDefs: [
								{
									name: app.localize('Actions'),
									enableSorting: false,
									width: 120,

									cellTemplate:
									"<div class=\"ui-grid-cell-contents\">" +
									"  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
									"    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
									"    <ul uib-dropdown-menu>" +
									"      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editInventoryCycleTag(row.entity)\">" + app.localize("Edit") + "</a></li>" +
									"      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteInventoryCycleTag(row.entity)\">" + app.localize("Delete") + "</a></li>" +
									"    </ul>" +
									"  </div>" +
									"</div>"
								},
										{
											name: app.localize('InventoryCycleMode'),
											field: 'inventoryCycleModeRefName'
										},
                    {
												name: app.localize('InventoryCycleTagCode'),
												field: 'inventoryCycleTagCode'
										},
										{
											name: app.localize('Description'),
											field: 'inventoryCycleTagDescription'
										},
									
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

         

            vm.getAll = function () {
                vm.loading = true;
                inventorycycletagService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editInventoryCycleTag = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createInventoryCycleTag = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteInventoryCycleTag = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteInventoryCycleTagWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            inventorycycletagService.deleteInventoryCycleTag({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

						function openCreateOrEditModal(objId) {
							$state.go("tenant.inventorycycletagedit", {
								id: objId
							});
            }

            vm.exportToExcel = function () {
                inventorycycletagService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getAll();
        }]);
})();

