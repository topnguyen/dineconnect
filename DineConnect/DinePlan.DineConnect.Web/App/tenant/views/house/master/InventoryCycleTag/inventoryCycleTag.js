﻿
(function () {
    appModule.controller('tenant.views.house.master.inventorycycletag.inventoryCycleTag', [
        '$scope', '$uibModal', '$state', '$stateParams', 'abp.services.app.inventoryCycleTag',
        function ($scope, $modal, $state, $stateParams, inventorycycletagService) {
            var vm = this;
            /* eslint-disable */
            inventorycycletagId = $stateParams.id;

            vm.refcyclemode = [];
            function fillDropDownMaterialType() {
                inventorycycletagService.getStockCycleForCombobox({}).success(function (result) {
                    vm.refcyclemode = result.items;
                });
            }

            vm.loading = false;
            vm.cycledata = [];
            vm.monthCycleData = [];

            vm.weekly = {
                sunday: false,
                monday: false,
                tueday: false,
                wedday: false,
                thuday: false,
                friday: false,
                satday: false
            };

            fillDropDownMaterialType();
            vm.cyclemodeSelection = function () {

            }

            vm.saving = false;
            vm.inventorycycletag = null;
            vm.save = function () {
                if (vm.inventorycycletag.inventoryCycleMode == 0) {
                    vm.cycledata = [];
                }
                else if (vm.inventorycycletag.inventoryCycleMode == 1) {
                    vm.cycledata = [];
                    if (vm.weekly.sunday == true)
                        vm.cycledata.push("0");
                    if (vm.weekly.monday == true)
                        vm.cycledata.push("1");
                    if (vm.weekly.tueday == true)
                        vm.cycledata.push("2");
                    if (vm.weekly.wedday == true)
                        vm.cycledata.push("3");
                    if (vm.weekly.thuday == true)
                        vm.cycledata.push("4");
                    if (vm.weekly.friday == true)
                        vm.cycledata.push("5");
                    if (vm.weekly.satday == true)
                        vm.cycledata.push("6");
                    if (vm.cycledata.length == 0) {
                        abp.notify.error(app.localize('WeeklyDaysNotSelectedErr'));
                        vm.loading = false;
                        vm.saving = false;
                        return;
                    }
                }
                else if (vm.inventorycycletag.inventoryCycleMode == 2) {
                    if (vm.monthCycleData.length == 0) {
                        abp.notify.error(app.localize('DataNotGiven'));
                        vm.loading = false;
                        vm.saving = false;
                        return;
                    }
                    vm.cycledata = [];
                    angular.forEach(vm.monthCycleData, function (item) {
                        vm.cycledata.push(item.day);
                    });
                }

                vm.saving = true;
                vm.loading = true;
                if (vm.inventorycycletag.doesApplicableToAllLocations == true) {
                    vm.inventorycycletag.locations = [];
                    vm.inventorycycletag.locationGroups = [];
                    vm.inventorycycletag.locationTags = [];
                }
                else {
                    vm.inventorycycletag.locations = vm.locationGroup.locations;
                    vm.inventorycycletag.locationGroups = vm.locationGroup.groups;
                    vm.inventorycycletag.locationTags = vm.locationGroup.tags;
                }
                

                inventorycycletagService.createOrUpdateInventoryCycleTag({
                    inventoryCycleTag: vm.inventorycycletag,
                    cycleData: vm.cycledata
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go("tenant.inventorycycletag", {
                });
            };

            //  Location Link Start
            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            //  Location Link End

            function init() {
                var i = 1;
                vm.loading = true;
                inventorycycletagService.getInventoryCycleTagForEdit({
                    id: inventorycycletagId
                }).success(function (result) {
                    vm.inventorycycletag = result.inventoryCycleTag;

                    if (vm.inventorycycletag.id != null) {
                        if (vm.inventorycycletag.inventoryCycleMode == 0) {
                            vm.cycledata = [];
                        }
                        else if (vm.inventorycycletag.inventoryCycleMode == 1) {
                            vm.cycledata = result.cycleData;
                            angular.forEach(vm.cycledata, function (value, key) {
                                if (value == 0)
                                    vm.weekly.sunday = true;
                                else if (value == 1)
                                    vm.weekly.monday = true;
                                else if (value == 2)
                                    vm.weekly.tueday = true;
                                else if (value == 3)
                                    vm.weekly.wedday = true;
                                else if (value == 4)
                                    vm.weekly.thuday = true;
                                else if (value == 5)
                                    vm.weekly.friday = true;
                                else if (value == 6)
                                    vm.weekly.satday = true;
                            });
                        }
                        else if (vm.inventorycycletag.inventoryCycleMode == 2) {
                            angular.forEach(result.cycleData, function (value, key) {
                                vm.monthCycleData.push({ 'id': 0, 'day': value });
                            });
                        }

                        if (vm.inventorycycletag.locationGroups.length > 0) {
                            vm.locationGroup.group = true;
                            vm.locationGroup.groups = vm.inventorycycletag.locationGroups;
                        }
                        else {
                            vm.locationGroup.group = true;
                            vm.locationGroup.groups = [];
                        }

                        if (vm.inventorycycletag.locations.length > 0) {
                            vm.locationGroup.locations = vm.inventorycycletag.locations;
                        }
                        else {
                            vm.locationGroup.locations = [];
                        }

                        if (result.inventorycycletag.locationTags.length > 0) {
                            vm.locationGroup.tags = vm.inventorycycletag.locationTags;
                        }
                        else {
                            vm.locationGroup.tags = [];
                        }
                    }
                }).finally(function () {
                    vm.loading = false;
                });

            }
            init();

            $scope.objArrayPosition = function (arr, keyName, val) {
                return arr.map(function (a) { return a[keyName]; }).indexOf(val);
            };

            vm.addMonthday = function () {
                if (vm.monthday < 1 || vm.monthday > 31) {
                    abp.notify.error(app.localize('DayValueNotInRange'));
                    return;
                }
                var idx = $scope.objArrayPosition(vm.monthCycleData, "day", vm.monthday);
                if (idx >= 0) {
                    abp.notify.error(app.localize("AlreadyLocalExists_F", vm.monthday));
                    return;
                }
                vm.monthCycleData.push({ 'id': 0, 'day': vm.monthday });
                vm.monthday = "";
            };

            vm.deleteMonthday = function () {
                angular.forEach(vm.selectedMonthdays, function (item) {
                    var idx = vm.monthCycleData.indexOf(item);
                    vm.monthCycleData.splice(idx, 1);
                });
            };

        }
    ]);
})();

