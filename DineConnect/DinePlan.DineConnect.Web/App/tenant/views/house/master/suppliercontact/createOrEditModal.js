﻿(function () {
    appModule.controller('tenant.views.house.master.suppliercontact.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.supplierContact', 'suppliercontactId',
        'abp.services.app.commonLookup', 'argsupplierIdForLock', 'uiGridConstants', 'abp.services.app.supplierMaterial',
    function ($scope, $modalInstance, suppliercontactService, suppliercontactId,
        commonLookupService, argsupplierIdForLock, uiGridConstants, suppliermaterialService) {

            var vm = this;
            vm.saving = false;
            vm.suppliercontact = null;
            vm.loading = false;
            vm.filterText = argsupplierIdForLock;

            $scope.existall = true;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.Supplier.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.Supplier.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.Supplier.Delete')
            };


            vm.save = function () {
			   if ($scope.existall == false)
                    return;
                vm.saving = true;
                vm.suppliercontact.contactPersonName = vm.suppliercontact.contactPersonName.toUpperCase();

                suppliercontactService.createOrUpdateSupplierContact({
                    supplierContact: vm.suppliercontact
                }).success(function () {
                    abp.notify.info('\' SupplierContact \'' + app.localize('SavedSuccessfully'));
                    vm.getAll();
                    suppliercontactId = null;
                    init();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.suppliercontact.id == null) {
			         vm.existall = false;
			         return;
			     }
				 
                suppliercontactService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.suppliercontact.id,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.suppliercontact.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.suppliercontact.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
			 vm.refsupplier = [];

			 vm.editSupplierContact = function (myObj) {
			     //openCreateOrEditModal(myObj.id);
			     suppliercontactId = myObj.id;
                 init();

			 };

			vm.deleteSupplierContact = function (myObject) {
			    abp.message.confirm(
                    app.localize('DeleteSupplierContactWarning', myObject.contactPersonRefName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            suppliercontactService.deleteSupplierContact({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
			};

			vm.userGridOptions = {
			    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
			    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
			    paginationPageSizes: app.consts.grid.defaultPageSizes,
			    paginationPageSize: app.consts.grid.defaultPageSize,
			    useExternalPagination: true,
			    useExternalSorting: true,
			    appScopeProvider: vm,
			    rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
			    columnDefs: [
					{
					    name: app.localize('Actions'),
					    enableSorting: false,
					    width: 120,

					    cellTemplate:
						   "<div class=\"ui-grid-cell-contents\">" +
							    "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
							   "    <ul uib-dropdown-menu>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editSupplierContact(row.entity)\">" + app.localize("Edit") + "</a></li>" +
							   "      <li><a ng-click=\"grid.appScope.deleteSupplierContact(row.entity)\">" + app.localize("Delete") + "</a></li>" +
							   "    </ul>" +
							   "  </div>" +
							   "</div>"
					},
                    {
                        name: app.localize('ContactPersonName'),
                        field: 'contactPersonName'
                    },
                    {
                        name: app.localize('Designation'),
                        field: 'designation'
                    },
                    {
                        name: app.localize('PhoneNumber'),
                        field: 'phoneNumber'
                    },
                    {
                        name: app.localize('Email'),
                        field: 'email'
                    },
			    ],
			    onRegisterApi: function (gridApi) {
			        $scope.gridApi = gridApi;
			        $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
			            if (!sortColumns.length || !sortColumns[0].field) {
			                requestParams.sorting = null;
			            } else {
			                requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
			            }

			            vm.getAll();
			        });
			        gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
			            requestParams.skipCount = (pageNumber - 1) * pageSize;
			            requestParams.maxResultCount = pageSize;

			            vm.getAll();
			        });
			    },
			    data: []
			};

			var requestParams = {
			    skipCount: 0,
			    maxResultCount: app.consts.grid.defaultPageSize,
			    sorting: null
			};

			vm.getAll = function () {
			    vm.loading = true;
			    suppliercontactService.getView({
			        skipCount: requestParams.skipCount,
			        maxResultCount: requestParams.maxResultCount,
			        sorting: requestParams.sorting,
			        filter: vm.filterText
			    }).success(function (result) {
			        vm.userGridOptions.totalItems = result.totalCount;
			        vm.userGridOptions.data = result.items;
			    }).finally(function () {
			        vm.loading = false;
			    });
			};

			 function fillDropDownSupplier() {
			     suppliermaterialService.getSupplierForCombobox({}).success(function (result) {
			     	    vm.refsupplier = result.items;
			     	    if (argsupplierIdForLock != null) {
			     	        vm.suppliercontact.supplierRefId = argsupplierIdForLock;
			     	        $("#suppliername").prop("disabled", true );
			     	    }
			     	    else
			     	    {
			     	        $("#suppliername").prop("disabled", false);
			     	    }
			     	  
                });
			 }

	        function init() {
                suppliercontactService.getSupplierContactForEdit({
                    Id: suppliercontactId
                }).success(function (result) {
                    vm.suppliercontact = result.supplierContact;
                });

                fillDropDownSupplier();
	        }

	        init();

	        vm.getAll();
        }
    ]);
})();

