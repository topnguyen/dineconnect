﻿
(function () {
    appModule.controller('tenant.views.house.master.productrecipeslink.productRecipeLink', [
        '$scope', '$state', '$stateParams', 'abp.services.app.productRecipesLink', 'abp.services.app.commonLookup', 'appSession',
        function ($scope, $state, $stateParams, productrecipeslinkService, commonLookupService,appSession) {
            var vm = this;
            
            vm.saving = false;
            vm.productrecipeslink = null;
            vm.productrecipeslinklist = null;
			$scope.existall = true;
			vm.productrecipeslinkId = $stateParams.id;
			vm.sno = 0;
			vm.productrecipeslinkPortions = [];
			
			vm.productrecipeslinklistdata = [];
			vm.tempdetaildata = [];
			vm.posRefId = null;

			$scope.menu = ['MainDish','SideDish'];

			vm.getRecipeTypes = function (search) {
			    var newRecipeType = $scope.menu.slice();
			    if (search && newRecipeType.indexOf(search) === -1) {
			        newRecipeType.unshift(search);
			    }
			    return newRecipeType;
			}

			vm.save = function (argoption) {
			   if ($scope.existall == false)
			       return;

			   var errorFlag = false;

			   if (vm.tempdetaildata.length <= 0) {
			       errorFlag = true;
			       abp.notify.info(app.localize('MinimumOneDetail'));
			       return;
			   }
			   else {
			       if (vm.tempdetaildata[0].recipeRefId == 0) {
			           errorFlag = true;
			           abp.notify.info(app.localize('MinimumOneDetail'));
			           return;
			       }
			       if ( vm.tempdetaildata[0].portionQty == 0) {
			           errorFlag = true;
			           abp.notify.info(app.localize('InvalidPortionQty'));
			           return;
			       }
			   }

			   if (errorFlag) {
			       vm.saving = false;
			       vm.loading = false;
			       $scope.existall = false;
			       return;
			   }

                vm.saving = true;
				
                vm.productrecipeslinklistdata = [];
                angular.forEach(vm.tempdetaildata, function (value, key) {
                    vm.productrecipeslinklistdata.push({
                        'posRefId': vm.posRefId,
                        'posRefName': value.posRefName,
                        'userSerialNumber': value.userSerialNumber,
                        'wastageExpected': value.wastageExpected,
                        'recipeRefId': value.recipeRefId,
                        'recipeRefName': value.recipeRefName,
                        'portionQty': value.portionQty,
                        'portionUnitId': value.portionUnitId,
                        'portionUnitName': value.portionUnitName,
                        'recipeType': 'MainDish',
                    });
                });
			    productrecipeslinkService.createOrUpdateProductRecipesLink({
			        productRecipesLinkDetail: vm.productrecipeslinklistdata
			}).success(function (result) {
                    abp.notify.info('\' ProductRecipesLink \'' + app.localize('SavedSuccessfully'));
                    if (argoption == 2)
                    {   
                        vm.cancel();
                        vm.saving = false;
                        return;
                    }
                    resetField();
                    vm.productrecipeslinkId = null;
                    vm.tempdetaildata = [];
                    vm.sno = 0;
                    init();
                    $("#posRefId").focus();

                }).finally(function () {
                    vm.saving = false;
                });
			};

			vm.removeRow = function (productIndex) {
			    var element = vm.tempdetaildata[productIndex];

			    //var matelement = element.materialRefName;
			    //if (matelement != "")
			    //    vm.tempRefMaterial.push(matelement);

			    if (vm.tempdetaildata.length > 1) {
			        vm.tempdetaildata.splice(productIndex, 1);
			        vm.sno = vm.sno - 1;

			        //vm.tempRefMaterial.find()
			    }

			    else {
			        abp.notify.info(app.localize('MinimumOneDetail'));
			        return;
			    }

			    //@@ Pending

			    //vm.materialRefresh();
			}

			 vm.existall = function ()
            {
                
			     if (vm.productrecipeslink.id == null) {
			         vm.existall = false;
			         return;
			     }
				 
                productrecipeslinkService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.productrecipeslink.id,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.productrecipeslink.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.productrecipeslink.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
			 };

			 vm.addPortion = function () {
			     if (vm.sno > 0) {

			         errorFlag = false;

			         var lastelement = vm.tempdetaildata[vm.tempdetaildata.length - 1];

			         if (lastelement.recipeRefName == null || lastelement.recipeRefName == "") {
			             abp.notify.info(app.localize('RecipeRequired'));
			             $("#selRecipe").focus();
			             return;
			         }

			         if (lastelement.portionQty == null || lastelement.portionQty == "" || lastelement.portionQty == 0) {
			             abp.notify.info(app.localize('PortionQty'));
			             $("#selProtionQty").focus();
			             return;
			         }

			         if (lastelement.portionUnitId == null || lastelement.portionUnitId == "") {
			             abp.notify.info(app.localize('UnitRequired'));
			             $("#selUnit").focus();
			             return;
			         }

			         //if (lastelement.recipeType == null || lastelement.recipeType == "") {
			         //    abp.notify.info(app.localize('RecipeTypeRequired'));
			         //    $("#selRecipeType").focus();
			         //    return;
			         //}

			         if (lastelement.wastageExpected > 0) {

			             if (parseFloat(lastelement.wastageExpected) > parseFloat(lastelement.portionQty))
			             {
			                 abp.notify.info(app.localize('WastagePercentageWrong'));
			                 $("#wastage").focus();
			                 return;
			             }

			             var wastagePercentage = parseFloat(lastelement.wastageExpected / lastelement.portionQty * 100);
			             if (wastagePercentage > 100)
			             {
			                 abp.notify.info(app.localize('WastagePercentageWrong'));
			                 $("#wastage").focus();
			                 return;
			             }

			             if (wastagePercentage > 10)
			             {
			                 abp.message.confirm(
                                   app.localize('WastagePercentageMore', wastagePercentage),
                                   function (isConfirmed) {
                                       if (!isConfirmed) {
                                           abp.notify.info(app.localize('WastagePercentageWrong'));
                                           $("#wastage").focus();
                                           return;
                                       }
                                   }
                               );
			             }
			         }

			         if (vm.refrecipe.length == vm.tempdetaildata.length) {
			             errorFlag = true;
			             abp.notify.info(app.localize('AllTheRecipeAdded'));
			             return;
			         }

			         if (errorFlag) {
			             vm.saving = false;
			             vm.loading = false;
			             $scope.existall = false;
			         }

			     }


			     vm.sno = vm.sno + 1;

			     vm.tempdetaildata.push({ 'posRefId': vm.posRefId, 'recipeRefId': '', 'recipeRefName': '', 'portionQty': '', 'portionUnitId': 0, 'portionUnitName': '', 'id': 0, 'userSerialNumber': vm.sno , 'wastageExpected' : 0});

			     $("#selRecipe1").focus();

			 }

			 vm.portionLength = function () {
			     return true;
			 }

			 vm.materialRefresh = function (objRemove) {
			     var indexPosition = -1;

			     vm.tempRefRecipe = vm.recipeRefName;

			     angular.forEach(vm.tempdetaildata, function (valueinArray, keyinArray) {
			         indexPosition = -1;
			         var materialtoremove = valueinArray.materialRefName;
			         angular.forEach(vm.tempRefRecipe, function (value, key) {
			             if (value.displayText == valueinArray.recipeRefName)
			                 indexPosition = key;
			         });
			         if (indexPosition >= 0)
			             vm.tempRefRecipe.splice(indexPosition, 1);
			     });

			 }

			 $scope.func = function (data, val) {
			     console.log(data);
			     data.recipeRefName = val;
			     //data.materialRefId = data.value;
			 };

			 function resetField() {
			     vm.posRefId = null;
			     vm.tempdetaildata = [];
			     //vm.productrecipeslink.posRefName = '';
			     //vm.productrecipeslink.recipeRefId = '';
			     //vm.productrecipeslink.recipeRefName = '';
			     //vm.productrecipeslink.portionQty = '';
			     //vm.productrecipeslink.portionUnitId = '';
			     //vm.productrecipeslink.portionUnitName = '';
			     //vm.productrecipeslink.recipeType = '';
			     vm.sno = 0;
			     $scope.checked = false;
			     vm.addPortion();
			 }


			 vm.cancel = function () {
			     $state.go("tenant.productrecipeslink");
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
			 vm.refrecipe = [];

			 function fillDropDownRecipe() {
			     	commonLookupService.getRecipeForCombobox({}).success(function (result) {
                    vm.refrecipe = result.items;
                });
			 }

			 vm.refunit = [];

			 function fillDropDownUnit() {
			     commonLookupService.getUnitForCombobox({}).success(function (result) {
			         vm.refunit = result.items;
			     });
			 }
			 vm.refmenuitem = [];

			 function fillDropDownMenuItem() {
			     commonLookupService.getMenuPortionForCombobox({}).success(function (result) {
			         vm.refmenuitem = result.items;
			     });
			 }

			 vm.requestParams = {
			     locations: '',
			     skipCount: 0,
			     maxResultCount: app.consts.grid.defaultPageSize,
			     sorting: null
			 };

			 vm.tempdetaildata = [];

			 vm.fillDetail = function () {
			     vm.productrecipeslinkId = vm.posRefId;

			     productrecipeslinkService.getProductRecipesLinkForEdit({
			         Id: vm.productrecipeslinkId
			     }).success(function (result) {
			         vm.productrecipeslink = result.productRecipesLinkDetail;
			         vm.tempdetaildata = [];
			         vm.sno = 0;
			         if (vm.productrecipeslink.length == 0) {
			             vm.productrecipeslink.portionQty = "";
			             vm.productrecipeslink.userSerialNumber = "";
			             vm.productrecipeslink.wastageExpected = "";
			             vm.addPortion();
			         }
			         else {
			             vm.tempdetaildata = [];
			             vm.productrecipeslinkdetail = result.productRecipesLinkDetail;
			             
			             angular.forEach(result.productRecipesLinkDetail, function (value, key) {
			                 vm.tempdetaildata.push({
			                     'posRefId': value.posRefId,
			                     'posRefName': value.posRefName,
			                     'userSerialNumber': value.userSerialNumber,
			                     'wastageExpected': value.wastageExpected,
			                     'recipeRefId': value.recipeRefId,
			                     'recipeRefName': value.recipeRefName,
			                     'portionQty': value.portionQty,
			                     'portionUnitId': value.portionUnitId,
			                     'portionUnitName': value.portionUnitName,
			                     'recipeType': value.recipeType,
			                     'id': value.id
			                 });

			                 vm.sno = vm.tempdetaildata.length;
			             });

			         }
			     });
			 }

			 function init() {
			     abp.ui.setBusy('#idcDetailForm');
			     fillDropDownMenuItem();
			     fillDropDownUnit();
			     fillDropDownRecipe();

			     if (vm.productrecipeslinkId != null && vm.productrecipeslinkId>0) {
			         vm.posRefId = vm.productrecipeslinkId;
			         $('#posSelectId').prop("disabled", true);
			         vm.fillDetail();
			     }
			 }
            
            init();
			 }

    ]);
})();
