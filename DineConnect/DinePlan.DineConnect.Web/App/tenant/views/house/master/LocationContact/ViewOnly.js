﻿(function () {
    appModule.controller('tenant.views.house.master.locationcontact.ViewOnly', [
        '$scope', '$uibModalInstance','$uibModal',  'abp.services.app.locationContact', 
        'abp.services.app.commonLookup', 'arglocationIdForLock', 'uiGridConstants',
    function ($scope, $modalInstance, $modal, locationcontactService, commonLookupService, arglocationIdForLock, uiGridConstants) {

            var vm = this;
            vm.loading = false;
            vm.filterText = arglocationIdForLock;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.LocationContact.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.LocationContact.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.LocationContact.Delete')
            };


            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			vm.userGridOptions = {
			    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
			    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
			    paginationPageSizes: app.consts.grid.defaultPageSizes,
			    paginationPageSize: app.consts.grid.defaultPageSize,
			    useExternalPagination: true,
			    useExternalSorting: true,
			    appScopeProvider: vm,
			    rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
			    columnDefs: [
					{
					    name: app.localize('Actions'),
					    enableSorting: false,
					    width: 120,

					    cellTemplate:
						   "<div class=\"ui-grid-cell-contents\">" +
							    "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
							   "    <ul uib-dropdown-menu>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editLocationContact(row.entity)\">" + app.localize("Edit") + "</a></li>" +
							   "      <li><a ng-click=\"grid.appScope.deleteLocationContact(row.entity)\">" + app.localize("Delete") + "</a></li>" +
							   "    </ul>" +
							   "  </div>" +
							   "</div>"
					},
                    {
                        name: app.localize('ContactPersonName'),
                        field: 'contactPerson'
                    },
                    {
                        name: app.localize('Designation'),
                        field: 'designation'
                    },
                    {
                        name: app.localize('PhoneNumber'),
                        field: 'phoneNumber'
                    },
                    {
                        name: app.localize('Email'),
                        field: 'email'
                    },
			    ],
			    onRegisterApi: function (gridApi) {
			        $scope.gridApi = gridApi;
			        $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
			            if (!sortColumns.length || !sortColumns[0].field) {
			                requestParams.sorting = null;
			            } else {
			                requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
			            }

			            vm.getAll();
			        });
			        gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
			            requestParams.skipCount = (pageNumber - 1) * pageSize;
			            requestParams.maxResultCount = pageSize;

			            vm.getAll();
			        });
			    },
			    data: []
			};

			var requestParams = {
			    skipCount: 0,
			    maxResultCount: app.consts.grid.defaultPageSize,
			    sorting: null
			};

			vm.getAll = function () {
			    vm.loading = true;
			    locationcontactService.getView({
			        skipCount: requestParams.skipCount,
			        maxResultCount: requestParams.maxResultCount,
			        sorting: requestParams.sorting,
			        filter: vm.filterText
			    }).success(function (result) {
			        vm.userGridOptions.totalItems = result.totalCount;
			        vm.userGridOptions.data = result.items;
			    }).finally(function () {
			        vm.loading = false;
			    });
			};

			vm.editLocationContact = function (myObj) {
			    openCreateOrEditModal(myObj.id);
			};

			vm.createLocationContact = function () {
			    openCreateOrEditModal(null);
			};

			vm.deleteLocationContact = function (myObject) {
			    abp.message.confirm(
                    app.localize('DeleteLocationContactWarning', myObject.contactPersonName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            locationcontactService.deleteLocationContact({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
			};

			function openCreateOrEditModal(objId) {
			    var modalInstance = $modal.open({
			        templateUrl: '~/App/tenant/views/house/master/locationcontact/createOrEditModal.cshtml',
			        controller: 'tenant.views.house.master.locationcontact.createOrEditModal as vm',
			        backdrop: 'static',
			        keyboard: false,
			        resolve: {
			            locationcontactId: function () {
			                return objId;
			            },
			            arglocationIdForLock: function () {
			                return arglocationIdForLock;
			            }
			        }
			    });

			    modalInstance.result.then(function (result) {
			        vm.getAll();
			    }).finally(function () {
			        vm.getAll();
			    });
			};

			vm.exportToExcel = function () {
			    locationcontactService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
			};


			vm.getAll();
        }
    ]);
})();

