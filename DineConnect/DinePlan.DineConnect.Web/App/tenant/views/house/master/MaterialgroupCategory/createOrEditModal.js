﻿
(function () {
    appModule.controller('tenant.views.house.master.materialgroupcategory.createOrEditModal', [
        '$scope', '$uibModalInstance', '$uibModal', 'abp.services.app.materialGroupCategory', 'materialgroupcategoryId', 'abp.services.app.commonLookup', 'uiGridConstants',
        function ($scope, $modalInstance, $modal, materialgroupcategoryService, materialgroupcategoryId, commonLookupService, uiGridConstants) {
            var vm = this;
            
            vm.saving = false;
            vm.materialgroupcategory = null;
			$scope.existall = true;

			
            vm.save = function (saveOption) {
			   if ($scope.existall == false)
			       return;
			   $scope.errmessage = "";

			   if (vm.materialgroupcategory.materialGroupRefId == 0 || vm.materialgroupcategory.materialGroupRefId == null)
			       $scope.errmessage = $scope.errmessage + app.localize('MaterialGroupNilErr', vm.materialgroupcategory.materialGroupRefId);

			   if ($scope.errmessage != "") {
			       abp.notify.warn($scope.errmessage, 'Required');
			       return;
			   }

                vm.saving = true;
                vm.materialgroupcategory.materialGroupCategoryName = vm.materialgroupcategory.materialGroupCategoryName.toUpperCase();

                materialgroupcategoryService.createOrUpdateMaterialGroupCategory({
                    materialGroupCategory: vm.materialgroupcategory
                }).success(function (result) {
                    abp.notify.info('\' MaterialGroupCategory \'' + app.localize('SavedSuccessfully'));
                    if (saveOption == 1) {
                        vm.getAll();
                        materialgroupId = null;
                        init();
                    }
                    else
                    {
                        $modalInstance.close(result);
                    }
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.materialgroupcategory.materialGroupCategoryName == null) {
			         vm.existall = false;
			         return;
			     }
				 
                materialgroupcategoryService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'materialGroupCategoryName',
                    filter: vm.materialgroupcategory.materialGroupCategoryName,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.materialgroupcategory.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.materialgroupcategory.materialGroupCategoryName + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
			 };

			 vm.openGroupMaster = function () {
			     openCreateGroupMaster(null);
			 };

			 var selectedGroupId = null;

			 function openCreateGroupMaster(objId) {
			     var modalInstance = $modal.open({
			         templateUrl: '~/App/tenant/views/house/master/materialgroup/createOrEditModal.cshtml',
			         controller: 'tenant.views.house.master.materialgroup.createOrEditModal as vm',
			         backdrop: 'static',
			         resolve: {
			             materialgroupId: function () {
			                 return objId;
			             }
			         }
			     });

			     modalInstance.result.then(function (result) {
			         selectedGroupId = result;

			     }).finally(function () {
			         fillDropDownMaterialGroup();
			         vm.getAll();
			     });
			     
			 };

			vm.refmaterialgroup = [];

			 function fillDropDownMaterialGroup() {
			     	commonLookupService.getMaterialGroupForCombobox({}).success(function (result) {
			     	    vm.refmaterialgroup = result.items;
			     	    if (selectedGroupId != null)
			     	        vm.materialgroupcategory.materialGroupRefId = selectedGroupId;
                });
			 }

	        function init() {

				fillDropDownMaterialGroup();

                materialgroupcategoryService.getMaterialGroupCategoryForEdit({
                    Id: materialgroupcategoryId
                }).success(function (result) {
                    vm.materialgroupcategory = result.materialGroupCategory;
                });
            }
	        init();

            // Index Coding Here

	        $scope.$on('$viewContentLoaded', function () {
	            App.initAjax();
	        });

	        vm.loading = false;
	        vm.filterText = null;
	        vm.currentUserId = abp.session.userId;

	        vm.permissions = {
	            create: abp.auth.hasPermission('Pages.Tenant.House.Master.MaterialGroupCategory.Create'),
	            edit: abp.auth.hasPermission('Pages.Tenant.House.Master.MaterialGroupCategory.Edit'),
	            'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.MaterialGroupCategory.Delete')
	        };

	        var requestParams = {
	            skipCount: 0,
	            maxResultCount: app.consts.grid.defaultPageSize,
	            sorting: null
	        };

	        vm.userGridOptions = {
	            enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
	            enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
	            paginationPageSizes: app.consts.grid.defaultPageSizes,
	            paginationPageSize: app.consts.grid.defaultPageSize,
	            useExternalPagination: true,
	            useExternalSorting: true,
	            appScopeProvider: vm,
	            rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
	            columnDefs: [
					//{
					//    name: app.localize('Actions'),
					//    enableSorting: false,
					//    width: 120,

					//    cellTemplate:
					//	   "<div class=\"ui-grid-cell-contents\">" +
					//		    "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
					//		   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
					//		   "    <ul uib-dropdown-menu>" +
					//		   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editMaterialGroupCategory(row.entity)\">" + app.localize("Edit") + "</a></li>" +
					//		   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteMaterialGroupCategory(row.entity)\">" + app.localize("Delete") + "</a></li>" +
					//		   "    </ul>" +
					//		   "  </div>" +
					//		   "</div>"
					//},
                    {
                        name: app.localize('Delete'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.deleteMaterialGroupCategory(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>' +
                            '</div>',
                        width: 50
                    },
                    {
                        name: app.localize('Edit'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.editMaterialGroupCategory(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>' +
                            '</div>',
                        width: 50
                    },
                    {
                        name: app.localize('materialGroupCategoryName'),
                        field: 'materialGroupCategoryName'
                    },
                    {
                        name: app.localize('materialGroupName'),
                        field: 'materialGroupRefName'
                    },

                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm\'',
                        minWidth: 100
                    }
	            ],
	            onRegisterApi: function (gridApi) {
	                $scope.gridApi = gridApi;
	                $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
	                    if (!sortColumns.length || !sortColumns[0].field) {
	                        requestParams.sorting = null;
	                    } else {
	                        requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
	                    }

	                    vm.getAll();
	                });
	                gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
	                    requestParams.skipCount = (pageNumber - 1) * pageSize;
	                    requestParams.maxResultCount = pageSize;

	                    vm.getAll();
	                });
	            },
	            data: []
	        };


	        vm.getAll = function () {
	            vm.loading = true;
	            materialgroupcategoryService.getView({
	                skipCount: requestParams.skipCount,
	                maxResultCount: requestParams.maxResultCount,
	                sorting: requestParams.sorting,
	                filter: vm.filterText
	            }).success(function (result) {
	                vm.userGridOptions.totalItems = result.totalCount;
	                vm.userGridOptions.data = result.items;
	            }).finally(function () {
	                vm.loading = false;
	            });
	        };

	        vm.editMaterialGroupCategory = function (myObj) {
	            //openCreateOrEditModal(myObj.id);
	            materialgroupcategoryId = myObj.id;
	            //openCreateOrEditModal(myObj.id);
	            init();

	        };

	        //vm.createMaterialGroupCategory = function () {
	        //    openCreateOrEditModal(null);
	        //};



	        vm.deleteMaterialGroupCategory = function (myObject) {
	            abp.message.confirm(
                    app.localize('DeleteMaterialGroupCategoryWarning', myObject.materialGroupCategoryName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            materialgroupcategoryService.deleteMaterialGroupCategory({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
	        };

	        vm.exportToExcel = function () {
	            materialgroupcategoryService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
	        };


	        vm.getAll();

        }
    ]);
})();

