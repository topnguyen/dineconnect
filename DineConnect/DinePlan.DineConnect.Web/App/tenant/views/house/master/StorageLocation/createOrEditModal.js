﻿
(function () {
    appModule.controller('tenant.views.house.master.storagelocation.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.storageLocation', 'storagelocationId', 'abp.services.app.commonLookup',
        function ($scope, $modalInstance, storagelocationService, storagelocationId, commonLookupService) {
            var vm = this;
            
            vm.saving = false;
            vm.storagelocation = null;
			$scope.existall = true;

			
            vm.save = function () {
			   if ($scope.existall == false)
			       return;

			   $scope.errmessage = "";
			   if (vm.storagelocation.locationRefId == 0 || vm.storagelocation.locationRefId == null)
			       $scope.errmessage = $scope.errmessage + app.localize('LocationErr', vm.storagelocation.locationRefId);

			   if ($scope.errmessage != "")
			   {
			       abp.message.warn($scope.errmessage);
			       return
			   }

                vm.saving = true;
				
                storagelocationService.createOrUpdateStorageLocation({
                    storageLocation: vm.storagelocation
                }).success(function () {
                    abp.notify.info('\' StorageLocation \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
			     if (vm.storagelocation.storageName == null) {
			         vm.existall = false;
			         return;
			     }
				 
                storagelocationService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'storageName',
                    filter: vm.storagelocation.storageName,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.storagelocation.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.storagelocation.storageName + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
			vm.reflocation = [];

			 function fillDropDownLocation() {
			     	commonLookupService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;
                    vm.reflocation.unshift({ value: "0", displayText: app.localize('NotAssigned') });
                });
			 }

	        function init() {
				fillDropDownLocation();
                storagelocationService.getStorageLocationForEdit({
                    Id: storagelocationId
                }).success(function (result) {
                    vm.storagelocation = result.storageLocation;
                });
            }
            init();
        }
    ]);
})();

