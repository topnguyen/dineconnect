﻿
(function () {
    appModule.controller('tenant.views.house.master.storagelocation.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.storageLocation',
        function ($scope, $modal, uiGridConstants, storagelocationService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.StorageLocation.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.StorageLocation.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.StorageLocation.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" dropdown="">' +
                            '    <button class="btn btn-xs btn-primary blue" dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul class="dropdown-menu">' +
                            '      <li><a ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.editStorageLocation(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.permissions.delete" ng-click="grid.appScope.deleteStorageLocation(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('StorageName'),
                        field: 'storageName'
                    },
                    {
                        name: app.localize('StorageReference'),
                        field: 'storageReference'
                    },
                    {
                        name: app.localize('LocationName'),
                        field: 'locationRefId'
                    },
                    {
                        name: app.localize('BinId'),
                        field: 'binId'
                    },
                    {
                        name: app.localize('SortId'),
                        field: 'sortId'
                    },
                    {
                        name: app.localize('IsFreezerAvailable'),
                        field: 'isFreezerAvailable'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

         

            vm.getAll = function () {
                vm.loading = true;
                storagelocationService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editStorageLocation = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createStorageLocation = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteStorageLocation = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteStorageLocationWarning', myObject.StorageName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            storagelocationService.deleteStorageLocation({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/storagelocation/createOrEditModal.cshtml',
                    controller: 'tenant.views.house.master.storagelocation.createOrEditModal as vm',
                    backdrop: 'static',
                    resolve: {
                        storagelocationId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.exportToExcel = function () {
                storagelocationService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getAll();
        }]);
})();

