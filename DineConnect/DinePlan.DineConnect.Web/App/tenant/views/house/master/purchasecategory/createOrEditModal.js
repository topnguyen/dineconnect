﻿
(function () {
    appModule.controller('tenant.views.house.master.purchasecategory.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.purchaseCategory', 'purchasecategoryId', 
        function ($scope, $modalInstance, purchasecategoryService, purchasecategoryId) {
            var vm = this;
            

            vm.saving = false;
            vm.purchasecategory = null;
			$scope.existall = true;

			
            vm.save = function () {
			   if ($scope.existall == false)
                    return;
                vm.saving = true;
				
                vm.purchasecategory.purchaseCategoryName = vm.purchasecategory.purchaseCategoryName.toUpperCase();

                purchasecategoryService.createOrUpdatePurchaseCategory({
                    purchaseCategory: vm.purchasecategory
                }).success(function () {
                    abp.notify.info(app.localize('PurchaseCategory') +  ' ' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.purchasecategory.purchaseCategoryName == null) {
			         vm.existall = false;
			         return;
			     }
				 
                purchasecategoryService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'purchaseCategoryName',
                    filter: vm.purchasecategory.purchaseCategoryName,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.purchasecategory.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.purchasecategory.purchaseCategoryName + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
	        function init() {

                purchasecategoryService.getPurchaseCategoryForEdit({
                    Id: purchasecategoryId
                }).success(function (result) {
                    vm.purchasecategory = result.purchaseCategory;
                });
            }
            init();
        }
    ]);
})();

