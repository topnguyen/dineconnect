﻿
(function () {
    appModule.controller('tenant.views.house.master.recipegroup.createOrEditModal', [
        '$scope', '$uibModalInstance', '$uibModal', 'abp.services.app.recipeGroup', 'recipegroupId', 'abp.services.app.commonLookup', 'uiGridConstants',
        function ($scope, $modalInstance, $modal, recipegroupService, recipegroupId, commonLookupService, uiGridConstants) {
            var vm = this;
            
            vm.saving = false;
            vm.recipegroup = null;
			$scope.existall = true;

			
			vm.save = function (argOption) {
			    
			   if ($scope.existall == false)
                    return;
			   vm.saving = true;
			   vm.recipegroup.recipeGroupName = vm.recipegroup.recipeGroupName.toUpperCase();
			   vm.recipegroup.recipeGroupShortName = vm.recipegroup.recipeGroupShortName.toUpperCase();
                recipegroupService.createOrUpdateRecipeGroup({
                    recipeGroup: vm.recipegroup
                }).success(function (result) {
                    abp.notify.info('\' RecipeGroup \'' + app.localize('SavedSuccessfully'));
                    if (argOption == 2)
                    {
                        $modalInstance.close(result.id);
                        vm.saving = false;
                        return;
                    }
                    vm.getAll();
                    recipegroupId = null;
                    init();
                    
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
			     if (vm.recipegroup.recipeGroupName == null) {
			         vm.existall = false;
			         return;
			     }
                recipegroupService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'recipeGroupName',
                    filter: vm.recipegroup.recipeGroupName,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    
                    if (result.totalCount > 0 && vm.recipegroup.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.recipegroup.recipeGroupName + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss(1);
            };
			

	        function init() {
                recipegroupService.getRecipeGroupForEdit({
                    Id: recipegroupId
                }).success(function (result) {
                    vm.recipegroup = result.recipeGroup;
                });
            }
	        init();

            //  Index JS coding

	        $scope.$on('$viewContentLoaded', function () {
	            App.initAjax();
	        });

	        vm.loading = false;
	        vm.filterText = null;
	        vm.currentUserId = abp.session.userId;

	        vm.permissions = {
	            create: abp.auth.hasPermission('Pages.Tenant.House.Master.RecipeGroup.Create'),
	            edit: abp.auth.hasPermission('Pages.Tenant.House.Master.RecipeGroup.Edit'),
	            'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.RecipeGroup.Delete')
	        };

	        var requestParams = {
	            skipCount: 0,
	            maxResultCount: app.consts.grid.defaultPageSize,
	            sorting: null
	        };

	        vm.userGridOptions = {
	            enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
	            enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
	            paginationPageSizes: app.consts.grid.defaultPageSizes,
	            paginationPageSize: app.consts.grid.defaultPageSize,
	            useExternalPagination: true,
	            useExternalSorting: true,
	            appScopeProvider: vm,
	            rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
	            columnDefs: [
					{
					    name: app.localize('Actions'),
					    enableSorting: false,
					    width: 120,

					    cellTemplate:
						   "<div class=\"ui-grid-cell-contents\">" +
							    "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
							   "    <ul uib-dropdown-menu>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editRecipeGroup(row.entity)\">" + app.localize("Edit") + "</a></li>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteRecipeGroup(row.entity)\">" + app.localize("Delete") + "</a></li>" +
							   "    </ul>" +
							   "  </div>" +
							   "</div>"
					},
                   {
                       name: app.localize('Code'),
                       field: 'recipeGroupShortName',
                       width: 100
                   },
                   {
                        name: app.localize('RecipeGroupName'),
                        field: 'recipeGroupName'
                   },
              
	            ],
	            onRegisterApi: function (gridApi) {
	                $scope.gridApi = gridApi;
	                $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
	                    if (!sortColumns.length || !sortColumns[0].field) {
	                        requestParams.sorting = null;
	                    } else {
	                        requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
	                    }

	                    vm.getAll();
	                });
	                gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
	                    requestParams.skipCount = (pageNumber - 1) * pageSize;
	                    requestParams.maxResultCount = pageSize;

	                    vm.getAll();
	                });
	            },
	            data: []
	        };



	        vm.getAll = function () {
	            vm.loading = true;
	            recipegroupService.getAll({
	                skipCount: requestParams.skipCount,
	                maxResultCount: requestParams.maxResultCount,
	                sorting: requestParams.sorting,
	                filter: vm.filterText
	            }).success(function (result) {
	                vm.userGridOptions.totalItems = result.totalCount;
	                vm.userGridOptions.data = result.items;
	            }).finally(function () {
	                vm.loading = false;
	            });
	        };

	        vm.editRecipeGroup = function (myObj) {
	            //openCreateOrEditModal(myObj.id);
	            recipegroupId = myObj.id;
	            init();
	        };

	        //vm.createRecipeGroup = function () {
	        //    openCreateOrEditModal(null);
	        //};

	        vm.deleteRecipeGroup = function (myObject) {
	            abp.message.confirm(
                    app.localize('DeleteRecipeGroupWarning', myObject.recipeGroupName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            recipegroupService.deleteRecipeGroup({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
	        };


	        vm.exportToExcel = function () {
	            recipegroupService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
	        };


	        vm.getAll();

        }
    ]);
})();

