﻿
(function () {
    appModule.controller('tenant.views.house.master.recipe.recipedetail', [
        '$scope', '$state', '$stateParams',
        //'$uibModalInstance',
        '$uibModal', 'abp.services.app.recipe', 'abp.services.app.recipeIngredient', 'abp.services.app.commonLookup', 'abp.services.app.unitConversion',
        function ($scope, $state, $stateParams,
            //$modalInstance,
            $modal, recipeService, recipeingredientService, commonLookupService, unitconversionService
            ) {
            var vm = this;

            vm.recipeId = $stateParams.id;

            vm.refmaterial = [];
            vm.tempRefMaterial = [];

            vm.saving = false;
            vm.detailediting = false;
            vm.editedItem = null;
            vm.recipe = null;
            var errcnt = 0;
            vm.sno = 0;
            $scope.existall = true;

            vm.recipeingredient = null;

            vm.recipeingredientdata = [];

           

            vm.getDetailData = function () {
                vm.loading = true;
                vm.filterText = null;

                recipeingredientService.getViewByRefRecipeId({
                    Id: vm.recipeId
                }).success(function (result) {
                    vm.recipeingredientdata = [];
                    vm.recipeingredient = result.items;
                    //vm.recipeingredientdata = result.items;
                    vm.ssno = 0;
                    angular.forEach(result.items, function (value, key) {
                        vm.ssno = vm.ssno + 1;
                        vm.recipeingredientdata.push({
                            "sno": vm.ssno,
                            "UserSerialNumber": value.userSerialNumber,
                            "id": 0,
                            "materialRefId": value.materialRefId,
                            //"materialName":({"displayText" : value.materialRefName,"isSelected":false,"value":value.materialRefId}),
                            "materialName": value.materialRefName,
                            "materialUsedQty": value.materialUsedQty,
                            "unitRefId": value.unitRefId,
                            "unitRefName": value.unitRefName,
                            "VariationflagForProduction": value.variationflagForProduction
                        });
                    });


                    if (result.items.length == 0)
                        vm.recipeingredient.id = 1;

                    //vm.recipeingredient.userSerialNumber = result.items.length + 1;
                    //vm.recipeingredient.materialUsedQty = "";
                    //vm.recipeingredient.variationflagForProduction = true;

                }).finally(function () {
                    vm.loading = false;
                    vm.fillDropDownMaterial();
                });
            };

            vm.save = function (argoption) {
                vm.setAlarmFlag();
                if ($scope.existall == false)
                    return;
                $scope.errmessage = "";
                if (parseInt(vm.recipe.alarmTimeInMinutes) > parseInt(vm.recipe.lifeTimeInMinutes))
                    $scope.errmessage = $scope.errmessage + app.localize('AlarmMismatchErr', vm.recipe.alarmTimeInMinutes);
                if (vm.recipe.lifeTimeInMinutes == 0 || vm.recipe.lifeTimeInMinutes == "")
                    $scope.errmessage = $scope.errmessage + app.localize('LifeTimeErr');
                if (vm.recipe.recipeGroupRefId == 0 || vm.recipe.recipeGroupRefId == null)
                    $scope.errmessage = $scope.errmessage + app.localize('RecipeGroupErr');
                if (vm.recipe.prdUnitId == 0 || vm.recipe.prdUnitId == null)
                    $scope.errmessage = $scope.errmessage + app.localize('PrdUnitErr');
                if (vm.recipe.selUnitId == 0 || vm.recipe.selUnitId == null)
                    $scope.errmessage = $scope.errmessage + app.localize('SelUnitErr');


                if ($scope.errmessage != "") {
                    errcnt == errcnt + 1;
                    if ($scope.errcnt == 0)
                        abp.notify.warn($scope.errmessage);
                    else {
                        abp.message.warn($scope.errmessage);
                    }
                    return;
                }

                vm.saving = true;


                vm.recipeingredientdataforadd = [];

                //@@Pending Remove Empty Elements
                angular.forEach(vm.recipeingredientdata, function (value, key) {
                    vm.recipeingredientdataforadd.push({
                        "recipeRefId": 0,
                        "materialRefId": value.materialRefId,
                        "materialName": value.materialRefName,
                        "materialUsedQty": value.materialUsedQty,
                        "unitRefId": value.unitRefId,
                        "VariationflagForProduction": value.variationflagForProduction,
                        "UserSerialNumber": value.userSerialNumber
                    });
                });


                vm.recipe.recipeName = vm.recipe.recipeName.toUpperCase();
                vm.recipe.recipeShortName = vm.recipe.recipeShortName.toUpperCase();

                recipeService.createOrUpdateRecipe({
                    recipe: vm.recipe,
                    recipeIngredientList: vm.recipeingredientdataforadd
                }).success(function () {
                    abp.notify.info('\' Recipe \'' + app.localize('SavedSuccessfully'));

                    if (argoption == 2) {
                        vm.cancel();
                        vm.saving = false;
                        return;
                    }

                    vm.recipeId = null;
                    vm.recipeingredientdata = [];
                    vm.sno = 0;
                    init();
                    $state.go("tenant.recipe");

                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {
                if (vm.recipe.recipeName == null) {
                    vm.existall = false;
                    return;
                }

                recipeService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'recipeName',
                    filter: vm.recipe.recipeName,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.recipe.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.recipe.recipeName + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.openGroupMaster = function () {
                openCreateGroupMaster(null);
            };

            var selectedRecipeGroupId = null;

            function openCreateGroupMaster(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/recipegroup/createOrEditModal.cshtml',
                    controller: 'tenant.views.house.master.recipegroup.createOrEditModal as vm',
                    backdrop: 'static',
                    resolve: {
                        recipegroupId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    selectedRecipeGroupId = result;
                }).finally(function () {
                    fillDropDownRecipeGroup();

                });
            };


            vm.cancel = function () {
                $state.go("tenant.recipe");
            };

            vm.setAlarmFlag = function () {
                if (parseInt(vm.recipe.alarmTimeInMinutes) > 0)
                    $("#recipeAlarmFlag").prop('checked', true);
                else
                    $("#recipeAlarmFlag").prop('checked', false);
            }

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };


            vm.removeRow = function (productIndex) {
                var element = vm.recipeingredientdata[productIndex];
                var matelement = element.materialRefName;
                if (matelement != "")
                    vm.tempRefMaterial.push(matelement);

                if (vm.recipeingredientdata.length > 1) {
                    vm.recipeingredientdata.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                    vm.tempRefMaterial.find()
                }

                else {
                    abp.notify.info(app.localize('MinimumOneDetail'));
                    return;
                }
            }


            vm.addPortion = function () {
                if (vm.sno > 0) {
                    var lastelement = vm.recipeingredientdata[vm.recipeingredientdata.length - 1];

                    if (lastelement.materialRefName == null | lastelement.materialRefName == "") {
                        abp.notify.info(app.localize('MatRequired'));
                        $("#selMaterial1").focus();
                        return;
                    }

                    if (lastelement.materialUsedQty == 0 || lastelement.materialUsedQty == null || lastelement.materialUsedQty == "") {
                        abp.notify.info(app.localize('QtyRequired'));
                        $("#rcdqty").focus();
                        return;
                    }

                    if (lastelement.unitRefId == 0 || lastelement.unitRefId == null) {
                        abp.notify.info(app.localize('UnitRequired'));
                        $("#selUnit").focus();
                        return;
                    }

                    vm.removeMaterial(lastelement);

                }

                //addportions
                vm.sno = vm.sno + 1;
                vm.recipeingredientdata.push(
                    {
                        'sno': vm.sno,
                        'userSerialNumber': vm.sno,
                        'id': 0,
                        'materialRefId': '',
                        'materialRefName': '',
                        'materialUsedQty': '',
                        'unitRefId': 0,
                        'unitRefName': '',
                        'variationflagForProduction': 0
                    });
                $("#selMaterial1").focus();
            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeMaterial = function (objRemove) {
                var indexPosition = vm.tempRefMaterial.indexOf(objRemove.materialRefName);
                vm.tempRefMaterial.splice(indexPosition, 1);
            }

            $scope.func = function (data, val) {
                data.materialRefName = val;
                fillDropDownUnitBasedOnMaterial(data.materialRefId);
            };

            function fillDropDownUnitBasedOnMaterial(selectmaterialId) {
                vm.refunit = [];
                vm.tempRefUnit = [];
                commonLookupService.getUnitForMaterialLinkCombobox({ Id: selectmaterialId }).success(function (result) {
                    vm.refunit = result.items;
                    vm.tempRefUnit = vm.refunit;
                });
            }

            vm.refrecipegroup = [];

            function fillDropDownRecipeGroup() {
                commonLookupService.getRecipeGroupForCombobox({}).success(function (result) {
                    vm.refrecipegroup = result.items;
                    if (selectedRecipeGroupId != null)
                        vm.recipe.recipeGroupRefId = selectedRecipeGroupId;
                });
            }


            vm.refunit = [];
            vm.tempRefUnit = [];

            function fillDropDownUnit() {
                commonLookupService.getUnitForCombobox({}).success(function (result) {
                    vm.refunit = result.items;
                    vm.tempRefUnit = result.items;
                });
            }

            vm.refunitconversion = [];
            vm.fillRefConversionUnit = function () {
                unitconversionService.getReferenceUnitForCombobox({ Id: vm.recipe.prdUnitId }).success(function (result) {
                    vm.refunitconversion = result.items;
                });
            }


            vm.reflinkedunit = [];

            vm.fillLinkedUnit = function () {
                commonLookupService.getUnitForMaterialLinkCombobox({ Id: vm.recipeingredient.materialRefId }).success(function (result) {
                    vm.reflinkedunit = result.items;
                    if (vm.reflinkedunit.length == 1)
                        vm.recipeingredient.unitRefId = parseInt(vm.reflinkedunit[0].value);
                });
            }

            vm.fillDropDownMaterial = function () {
                commonLookupService.getMaterialForCombobox({}).success(function (result) {
                    vm.refmaterial = result.items;
                    vm.tempRefMaterial = result.items;
                });
            }


            function init() {
                fillDropDownRecipeGroup();
                fillDropDownUnit();
                vm.fillDropDownMaterial();

                recipeService.getRecipeForEdit({
                    Id: vm.recipeId
                }).success(function (result) {
                    vm.recipe = result.recipe;
                    selectedRecipeGroupId = null;
                    if (result.recipe.id == null) {
                        vm.recipe.lifeTimeInMinutes = "";
                        vm.recipe.alarmTimeInMinutes = "";
                        vm.recipe.fixedCost = "";
                        vm.recipe.prdBatchQty = "";
                        vm.addPortion();
                    }
                    else {
                        // vm.fillDropDownMaterial();
                        vm.fillRefConversionUnit();
                        vm.getDetailData();
                    }
                });
            }
            init();


        }
    ]);
})();

