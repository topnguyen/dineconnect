﻿
(function () {
    appModule.controller('tenant.views.house.master.recipe.index', [
        '$scope','$state', '$uibModal', 'uiGridConstants', 'abp.services.app.recipe',
        function ($scope, $state, $modal, uiGridConstants, recipeService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.Recipe.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.Recipe.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.Recipe.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };


            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString
            });


            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            


            vm.userGridOptionsReport = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Name'),
                        field: 'recipeName',
                        width: 400
                    },
                    {
                        name: app.localize('PrdBatchQty'),
                        field: 'prdBatchQty',
                        width: 100,
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Unit'),
                        field: 'prdUnitName',
                        width: 100,
                    },
                    {
                        name: app.localize('BatchCost'),
                        field: 'batchCost',
                        width: 100,
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('PerUnitCost'),
                        field: 'perUnitCost',
                        width: 200,
                        cellClass: 'ui-ralign',
                        format :'n2'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getCostingReport();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getCostingReport();
                    });
                },
                data: []
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
					{
					    name: app.localize('Actions'),
					    enableSorting: false,
					    width: 120,

					    cellTemplate:
						   "<div class=\"ui-grid-cell-contents\">" +
							    "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
							   "    <ul uib-dropdown-menu>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editRecipe(row.entity)\">" + app.localize("Edit") + "</a></li>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteRecipe(row.entity)\">" + app.localize("Delete") + "</a></li>" +
							   "    </ul>" +
							   "  </div>" +
							   "</div>"
					},
                    {
                        name: app.localize('Group'),
                        field: 'groupName',
                        width: 200
                    },
                    {
                        name: app.localize('Code'),
                        field: 'recipeShortName',
                        width: 200
                    },
                    {
                        name: app.localize('Name'),
                        field: 'recipeName',
                        width : 300
                    },
                    {
                        name: app.localize('PrdBatchQty'),
                        field: 'prdBatchQty',
                        width: 200,
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('ProductionUnit'),
                        field: 'prdUnitName',
                        width: 100
                    },
                    {
                        name: app.localize('SellingUnit'),
                        field: 'selUnitName',
                        width: 100
                    },
                    {
                        name: app.localize('FixedCostValue'),
                        field: 'fixedCost',
                        width: 120,
                        cellClass: 'ui-ralign'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

         
            vm.getCostingReport = function ()
            {
                vm.loadingRep = true;
                recipeService.getRecipeCosting({
                    startDate: vm.dateRangeModel.startDate,
                    endDate: vm.dateRangeModel.endDate
                }).success(function (result) {
                    vm.userGridOptionsReport.totalItems = result.totalCount;
                    vm.userGridOptionsReport.data = result.items;
                }).finally(function () {
                    vm.loadingRep = false;
                });
            };


            vm.getAll = function () {
                vm.loading = true;
                recipeService.getView({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editRecipe = function (myObj) {
                //openCreateOrEditModal(myObj.id);
                openRecipeDetail(myObj.id);
            };

            vm.createRecipe = function () {
                openCreateOrEditModal(null);
            };

            vm.createRecipeDetail = function (myObj) {
                openRecipeDetail(null);
            };


            function openRecipeDetail(objId) {
                $state.go("tenant.recipedetail", {
                    id: objId
                });
            };

            vm.deleteRecipe = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteRecipeWarning', myObject.recipeName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            recipeService.deleteRecipe({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/recipe/createOrEditModal.cshtml',
                    controller: 'tenant.views.house.master.recipe.createOrEditModal as vm',
                    backdrop: 'static',
                    resolve: {
                        recipeId: function () {
                            return objId;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.getAll();
                });

            }

            vm.exportToExcel = function () {
                recipeService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getAll();
        }]);
})();

