﻿
(function () {
    appModule.controller('tenant.views.house.master.recipe.createOrEditModal', [
        '$scope', '$uibModalInstance', '$uibModal', 'abp.services.app.recipe', 'recipeId', 'abp.services.app.recipeIngredient',  'abp.services.app.commonLookup', 'abp.services.app.unitConversion',
        function ($scope, $modalInstance, $modal, recipeService, recipeId, recipeingredientService,  commonLookupService, unitconversionService
            ) {
            var vm = this;
            
            vm.saving = false;
            vm.recipe = null;
            var errcnt = 0;
			$scope.existall = true;

			vm.save = function () {
			    vm.setAlarmFlag();
			   if ($scope.existall == false)
			       return;
			   $scope.errmessage = "";
			   if (parseInt(vm.recipe.alarmTimeInMinutes) > parseInt(vm.recipe.lifeTimeInMinutes))
			       $scope.errmessage = $scope.errmessage + app.localize('AlarmMismatchErr', vm.recipe.alarmTimeInMinutes);
                if (vm.recipe.lifeTimeInMinutes == 0 || vm.recipe.lifeTimeInMinutes == "")
                    $scope.errmessage = $scope.errmessage + app.localize('LifeTimeErr'); 
                if (vm.recipe.recipeGroupRefId == 0 || vm.recipe.recipeGroupRefId == null)
                    $scope.errmessage = $scope.errmessage + app.localize('RecipeGroupErr'); 
                if (vm.recipe.prdUnitId == 0 || vm.recipe.prdUnitId == null)
                    $scope.errmessage = $scope.errmessage + app.localize('PrdUnitErr'); 
                if (vm.recipe.selUnitId == 0 || vm.recipe.selUnitId == null)
                    $scope.errmessage = $scope.errmessage + app.localize('SelUnitErr');


                if ($scope.errmessage != "")
                {
                    errcnt == errcnt + 1;
                    if ($scope.errcnt == 0)
                        abp.notify.warn($scope.errmessage);
                    else {
                        abp.message.warn($scope.errmessage);
                    }
                    return;
                }

                vm.saving = true;
                vm.recipe.recipeName = vm.recipe.recipeName.toUpperCase();
                recipeService.createOrUpdateRecipe({
                    recipe: vm.recipe
                }).success(function () {
                    abp.notify.info('\' Recipe \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
			     if (vm.recipe.recipeName == null) {
			         vm.existall = false;
			         return;
			     }
				 
                recipeService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'recipeName',
                    filter: vm.recipe.recipeName,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.recipe.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.recipe.recipeName + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
			 };

			 vm.openGroupMaster = function () {
			     openCreateGroupMaster(null);
			 };

			 function openCreateGroupMaster(objId) {
			     var modalInstance = $modal.open({
			         templateUrl: '~/App/tenant/views/house/master/recipegroup/createOrEditModal.cshtml',
			         controller: 'tenant.views.house.master.recipegroup.createOrEditModal as vm',
			         backdrop: 'static',
			         resolve: {
			             recipegroupId: function () {
			                 return objId;
			             }
			         }
			     });

			     modalInstance.result.then(function (result) {

			     }).finally(function () {
			         fillDropDownRecipeGroup();
			     });
             };


			 vm.cancel = function () {
                $modalInstance.dismiss();
			 };

			 vm.setAlarmFlag = function () {
			     if (parseInt(vm.recipe.alarmTimeInMinutes) > 0)
			         $("#recipeAlarmFlag").prop('checked', true);
			     else
			         $("#recipeAlarmFlag").prop('checked', false);
			 }
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
			vm.refrecipegroup = [];

			 function fillDropDownRecipeGroup() {
			     	commonLookupService.getRecipeGroupForCombobox({}).success(function (result) {
                    vm.refrecipegroup = result.items;
                });
			 }


			 vm.refunit = [];

			 function fillDropDownUnit() {
			     commonLookupService.getUnitForCombobox({}).success(function (result) {
			         vm.refunit = result.items;
			     });
			 }

			 vm.refunitconversion = [];
			 vm.fillRefConversionUnit = function () {
			     unitconversionService.getReferenceUnitForCombobox({ Id: vm.recipe.prdUnitId }).success(function (result) {
                 vm.refunitconversion = result.items;
			     });
			 }

			 function init() {
				fillDropDownRecipeGroup();
				fillDropDownUnit();

                recipeService.getRecipeForEdit({
                    Id: recipeId
                }).success(function (result) {
                    vm.recipe = result.recipe;
                    if (result.recipe.id == null) {
                        vm.recipe.lifeTimeInMinutes = "";
                        vm.recipe.alarmTimeInMinutes = "";
                        vm.recipe.fixedCost = "";
                        vm.recipe.prdBatchQty = "";
                    }
                    else
                    {
                        vm.fillRefConversionUnit();
                    }
                });
            }
			 init();


        }
    ]);
})();

