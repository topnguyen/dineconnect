﻿(function () {
    appModule.controller('tenant.views.house.master.supplier.supplierMaterialwiseunitconversion', [
        '$scope', '$uibModal', '$uibModalInstance', 'abp.services.app.supplier', 'supplierRefId', 'supplierRefName', 'materialRefId', 'appSession', 'abp.services.app.material', 'abp.services.app.unit', 'abp.services.app.unitConversion',
        function ($scope, $modal, $modalInstance, supplierService, supplierRefId, supplierRefName, materialRefId, appSession, materialService, unitService, unitconversionService) {
            var vm = this;
            /* eslint-disable */

            vm.saving = false;
            vm.supplierRefId = supplierRefId;
            vm.supplierRefName = supplierRefName;
            vm.materialRefId = materialRefId;
            vm.loadingCount = 0;
            vm.loading = false;

            vm.material = null;

            vm.refdefaultIssueUnits = [];
            vm.refcorrespondingunits = [];
            vm.fillDropDownUnitReference = function () {

                vm.loading = true;
                unitService.getRefernceUnitName({ Id: vm.material.defaultUnitId }).success(function (result) {
                    vm.refcorrespondingunits = result.items;
                    vm.refdefaultIssueUnits = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            }


            if (vm.supplierRefName == null || vm.supplierRefName == '' || vm.supplierRefName.length == 0) {
                vm.loadingCount++;
                vm.linkedWithMaterialUcListNonEditable = [];
                supplierService.getSupplierNameForParticularCode({
                    id: vm.supplierRefId
                }).success(function (result) {
                    vm.supplierRefName = result;
                }).finally(function () {
                    vm.loadingCount--;
                });
            }

            vm.save = function () {
                vm.errorFlag = false;
                angular.forEach(vm.unitConversionList, function (value, key) {
                    if (value.editFlag == true)
                        vm.errorFlag = true;
                });
                if (vm.errorFlag == true) {
                    abp.notify.warn(app.localize('UnitConversionEditSavePendingAlert'));
                    abp.message.warn(app.localize('UnitConversionEditSavePendingAlert'));
                    vm.saving = false;
                    return;
                }
                $modalInstance.close(true);
            };

            vm.cancel = function () {
                $modalInstance.close(true);
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.unitConversionList = [];
            vm.linkedAllWithThisMaterial = [];
            function init() {
                vm.loadingCount++;
                vm.linkedAllWithThisMaterial = [];
                unitconversionService.getLinkedUnitCoversionForGivenMaterial({
                    id: vm.materialRefId
                }).success(function (result) {
                    vm.linkedAllWithThisMaterial = result;
                    vm.unitConversionList = result;
                    vm.filterOnlyForThisSupplier();
                }).finally(function () {
                    vm.loadingCount--;
                });
            }

            vm.fillMaterial = function () {
                vm.loadingCount++;
                materialService.getMaterialForGivenMaterialId({
                    id: vm.materialRefId
                }).success(function (materialresult) {
                    vm.material = materialresult;
                }).finally(function () {
                    vm.loadingCount--;
                    vm.fillDropDownUnitReference();
                    init();
                });
            }
            vm.fillMaterial();

            // Unit Conversion Manipulation

            vm.linkedSupplierList = [];

            vm.linkedSupplierList.push({
                'id': vm.supplierRefId, 'supplierName': vm.supplierRefName, 'name': vm.supplierRefName
            });

            vm.addCurrentSupplier = function (data, productIndex) {
                var sameSupplierAlreadyExists = false;
                angular.forEach(data.linkedSupplierList, function (value, key) {
                    if (value.id == vm.supplierRefId) {
                        abp.notify.error(app.localize('AlreadyExists'));
                        sameSupplierAlreadyExists = true;
                    }
                });
                if (sameSupplierAlreadyExists == true) {
                    return;
                }
                data.linkedSupplierList.push({
                    'id': vm.supplierRefId, 'supplierName': vm.supplierRefName, 'name': vm.supplierRefName
                });
                vm.unitCoversionSave(data);
            }

            vm.addUnitConversionPortion = function () {
                var data = ({
                    'materialRefId': vm.materialRefId,
                    'baseUnitId': 0, 'baseUom': '',
                    'refUnitId': '', 'refUom': '',
                    'conversion': '', 'decimalPlaceRounding': 6,
                    'id': null, 'editFlag': true,
                    'defaultConversion': 0, 'defaultDecimalPlaceRounding': 0,
                    'linkedSupplierList': []
                });
                data.linkedSupplierList.push({
                    'id': vm.supplierRefId, 'supplierName': vm.supplierRefName, 'name': vm.supplierRefName
                });
                vm.unitConversionList.push(data);
            }


            vm.saveUnitConversionPortion = function (productIndex) {
                if (vm.unitConversionList.length > 0) {

                    var lastelement = vm.unitConversionList[productIndex];

                    if (lastelement.baseUnitId == null || lastelement.baseUnitId == "" || lastelement.baseUnitId == 0) {
                        abp.notify.error(app.localize('baseUnitId'));
                        return;
                    }

                    if (lastelement.refUnitId == null || lastelement.refUnitId == "" || lastelement.refUnitId == 0) {
                        abp.notify.error(app.localize('refUnitId'));
                        lastelement.conversion = 0;
                        return;
                    }

                    if (lastelement.baseUnitId == lastelement.refUnitId) {
                        abp.notify.error(app.localize('BaseAndRefUnitCanNotBeSame'));
                        abp.message.error(app.localize('BaseAndRefUnitCanNotBeSame'));
                        lastelement.conversion = 0;
                        return;
                    }

                    if (lastelement.conversion == 0 || lastelement.conversion == "" || lastelement.conversion == null) {
                        abp.notify.error(app.localize('Conversion'));
                        lastelement.conversion = 0;
                        return;
                    }

                    if (lastelement.decimalPlaceRounding == 0 || lastelement.decimalPlaceRounding == "" || lastelement.decimalPlaceRounding == null) {
                        abp.notify.error(app.localize('DecimalPlaceRounding'));
                        lastelement.conversion = 0;
                        return;
                    }

                    var sameReverseConversationExist = false;
                    angular.forEach(vm.unitConversionList, function (value, key) {
                        if (key != productIndex && (value.id == null || value.id == 0)) {
                            if (value.baseUnitId == lastelement.refUnitId && value.refUnitId == lastelement.baseUnitId) {
                                abp.notify.error(app.localize('ReverseBaseAndRefUnitCanNotBeRepeat', value.baseUom, value.refUom));
                                abp.message.error(app.localize('ReverseBaseAndRefUnitCanNotBeRepeat', value.baseUom, value.refUom));
                                sameReverseConversationExist = true;
                            }
                        }
                    });
                    if (sameReverseConversationExist == true) {
                        lastelement.conversion = 0;
                        return;
                    }
                    lastelement.editFlag = false;
                }
            }

            vm.removeUnitConversionPortion = function (data,index) {
                if (data.id == 0 || data.id == null) {
                    vm.unitConversionList.splice(index);
                    return;
                }
                vm.loading = true;
                unitconversionService.deleteUnitConversionMaterial({
                    unitConversion: data,
                    supplierRefId: vm.supplierRefId
                }).success(function (result) {
                    abp.notify.info(app.localize('UnitConversion') + ' ' + app.localize('DeletedSuccessfully'));
                    errorFlag = false;
                }).finally(function () {
                    if (errorFlag == false) {
                        unitconversionService.getLinkedUnitCoversionForGivenMaterial({
                            id: vm.materialRefId
                        }).success(function (resultUc) {
                            vm.unitConversionList = resultUc;
                        }).finally(function () {
                            vm.saving = false;
                            vm.loading = false;
                        });
                        vm.loading = false;
                    }
                    else {
                        data.editFlag = true;
                        vm.saving = false;
                        vm.loading = false;
                    }
                });
            }


            vm.changeBaseRefUnit = function (data) {
                data.editFlag = true;
            }

            vm.baseUnitNameReference = function (data, item) {
                data.baseUom = item.displayText;
            }

            vm.refUnitNameReference = function (data, item) {
                vm.loadingCount++;
                data.refUom = item.displayText;
                materialService.getDefaultUnitConversionDataForGivenData({
                    baseUnitId: data.baseUnitId,
                    refUnitId: data.refUnitId
                }).success(function (result) {
                    data.defaultConversion = result.conversion;
                    data.defaultDecimalPlaceRounding = result.decimalPlaceRounding;
                    if (result.id == null || result.id == 0 || result.conversion == 0) {
                        abp.notify.info(app.localize('NoDefaultConversionBetweenUom', data.baseUom, data.refUom));
                        //abp.message.error(app.localize('NoDefaultConversionBetweenUom', data.baseUom, data.refUom));
                    }
                }).finally(function () {
                    vm.loadingCount--;
                });
            }


            vm.unitCoversionSave = function (data) {
                $scope.errmessage = "";
                if (data.baseUnitId == null || data.refUnitId == null || data.baseUnitId == 0 || data.refUnitId == 0) {
                    abp.notify.warn('MissingData');
                    return;
                }
                if (data.baseUnitId == data.refUnitId)
                    $scope.errmessage = $scope.errmessage + app.localize('UnitConverSameErr');

                if (data.conversion == 0)
                    $scope.errmessage = $scope.errmessage + app.localize('ConverZeroErr');

                if ($scope.errmessage != '') {
                    abp.notify.warn($scope.errmessage);
                    return;
                }

                vm.loadingCount++;
                vm.saving = true;
                var errorFlag = true;
                unitconversionService.createOrUpdateUnitConversion({
                    unitConversion: data,
                    supplierRefId: vm.supplierRefId
                }).success(function (result) {
                    abp.notify.info(app.localize('UnitConversion') + ' ' + app.localize('SavedSuccessfully'));
                    errorFlag = false;
                }).finally(function () {
                    if (errorFlag == false) {
                        unitconversionService.getLinkedUnitCoversionForGivenMaterial({
                            id: vm.materialRefId
                        }).success(function (resultUc) {
                            vm.unitConversionList = resultUc;
                        }).finally(function () {
                            vm.saving = false;
                            vm.loadingCount--;
                        });
                    }
                    else {
                        data.editFlag = true;
                        vm.saving = false;
                        vm.loadingCount--;
                    }
                });
            };

            vm.supplierLinkedUnitConversionList = [];
            vm.filterOnlyForThisSupplier = function () {
                vm.supplierLinkedUnitConversionList = [];
                angular.forEach(vm.unitConversionList, function (value, key) {
                    if (value.linkedSupplierList != null && value.linkedSupplierList.length > 0) {
                        angular.forEach(value.linkedSupplierList, function (supvalue, supkey) {
                            if (supvalue.id == vm.supplierRefId) {
                                vm.supplierLinkedUnitConversionList.push(value);
                            }
                        });
                    }
                });
                if (vm.supplierLinkedUnitConversionList.length == 0)
                    vm.addTab = true;
            }

            vm.removeSupplier = function (value, supindex) {
                var element = value.linkedSupplierList[supindex];
                value.linkedSupplierList.splice(supindex, 1);
            }

        }
    ]);
})();

