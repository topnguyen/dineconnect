﻿(function () {
    appModule.controller('tenant.views.house.master.supplier.createOrEditModal', [
        '$scope', '$uibModal', '$uibModalInstance', 'abp.services.app.supplier', 'supplierId', 'FileUploader', 'abp.services.app.material',
        function ($scope, $modal, $modalInstance, supplierService, supplierId, fileUploader, materialService) {
            var vm = this;
            /* eslint-disable */
            //var isEnabled = abp.features.isEnabled('SampleBooleanFeature');
            //var countryName = FeatureChecker.GetValue(AppFeatures.ConnectCountry);
            var countryName = abp.features.getValue('DinePlan.DineConnect.Connect.Country');
            vm.gstenabledforIndia = false;
            if (countryName == 'IN') {
                vm.gstenabledforIndia = true;
            }

            vm.saving = false;
            vm.supplier = null;
            vm.suppliergstinformation = null;
            $scope.existall = true;
            vm.tallyIntegrationFlag = abp.setting.getBoolean("App.House.TallyIntegrationFlag");

            //FileUpload

            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/ChangeFileUploadInfo',
                formData: [{ id: supplierId }],
                queueLimit: 1,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        //File type check
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        if ('|jpg|jpeg|pdf|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ProfilePicture_Warn_FileType'));
                            return false;
                        }
                        //File size check
                        if (item.size > 3072000) //3000KB
                        {
                            abp.message.warn(app.localize('ProfilePicture_Warn_SizeLimit'));
                            return false;
                        }
                        return true;
                    }
                }]
            });

            vm.uploader.onBeforeUploadItem = function (fileitem) {
                fileitem.formData.push({ employeeRefId: vm.fileupload.employeeRefId });
                fileitem.formData.push({ clientRefId: vm.fileupload.clientRefId })
            };

            vm.uploader.onErrorItem = function (fileItem, response, status, headers) {
                if (response.success) {
                    vm.cancel();
                } else {
                    abp.message.error(response);
                    abp.notify.error(response);
                    vm.loading = false;
                    vm.saving = false;
                    return;
                }
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                vm.cancel();
            };

            vm.changePicture = function (myObject) {

                //if (vm.supplier.supplierCode == null || vm.supplier.supplierCode == "") {
                //    abp.notify.error(app.localize('SupCodeErr'));
                //    abp.m.error(app.localize('SupCodeErr'));
                //    return;
                //}

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/supplier/changeGstCertificatePicture.cshtml',
                    controller: 'tenant.views.house.master.supplier.changeGstCertificatePicture as vm',
                    backdrop: 'static',
                    resolve: {
                        gstIn: function () {
                            if (myObject == null) {
                                myObject = vm.suppliergstinformation.gstin;
                            }
                            else
                                return myObject;

                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    if (result != null) {
                        vm.suppliergstinformation.gstCertificate = result;
                    }
                    else {
                        supplierService.getSupplierForEdit({
                            Id: myObject
                        }).success(function (result) {
                            vm.suppliergstinformation.gstCertificate = result.suppliergstinformation.gstCertificate;
                        });
                    }
                });
            };

            //File Upload Ends
            vm.save = function () {

                if ($scope.existall == false)
                    return;
                $scope.errmessage = "";

                //if (vm.supplier.defaultCreditDays == 0)
                //    $scope.errmessage = $scope.errmessage + app.localize('DefaultCrDtZeroErr', vm.supplier.defaultCreditDays)

                if ($scope.errmessage != "") {
                    abp.notify.warn($scope.errmessage, 'Required');
                    return;
                }

                vm.saving = true;
                vm.supplier.supplierName = vm.supplier.supplierName.toUpperCase();
                if (vm.supplier.taxRegistrationNumber != null && vm.supplier.taxRegistrationNumber.length > 0)
                    vm.supplier.taxRegistrationNumber = vm.supplier.taxRegistrationNumber.toUpperCase();

                if (vm.suppliergstinformation != null) {
                    if (vm.suppliergstinformation.supplierLegalName != null && vm.suppliergstinformation.supplierLegalName.length > 0)
                        vm.suppliergstinformation.supplierLegalName = vm.suppliergstinformation.supplierLegalName.toUpperCase();

                    if (vm.suppliergstinformation.gstin != null && vm.suppliergstinformation.gstin.length > 0)
                        vm.suppliergstinformation.gstin = vm.suppliergstinformation.gstin.toUpperCase();
                }
                supplierService.createOrUpdateSupplier({
                    supplier: vm.supplier,
                    suppliergstinformation: vm.suppliergstinformation
                }).success(function () {
                    abp.notify.info('\' Supplier \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {

                if (vm.supplier.supplierName == null) {
                    vm.existall = false;
                    return;
                }

                supplierService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'supplierName',
                    filter: vm.supplier.supplierName,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.supplier.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.supplier.supplierName + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
            vm.isAddonEnabled = abp.features.isEnabled('DinePlan.DineConnect.Addons');

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.refgststatus = [];
            function fillDropDownGstStatus() {
                supplierService.getGstStatusForCombobox({}).success(function (result) {
                    vm.refgststatus = result.items;
                });
            }

            vm.refconstitutionStatus = [];
            function fillDropDownConstititionStatus() {
                supplierService.getConstitutionStatusForCombobox({}).success(function (result) {
                    vm.refconstitutionStatus = result.items;
                });
            }

            vm.invoicePayModeList = [];
            vm.fillDropDownPayModes = function () {
                materialService.getInvoicePayModeEnumForCombobox({}).success(function (result) {
                    vm.invoicePayModeList = result.items;
                });
            }
            vm.fillDropDownPayModes();

            function init() {
                fillDropDownGstStatus();
                fillDropDownConstititionStatus();

                supplierService.getSupplierForEdit({
                    Id: supplierId
                }).success(function (result) {
                    if (result.supplier.id == null) {
                        vm.supplier.defaultCreditDays = "";
                        vm.supplier.defaultPayModeEnumRefId = 1;
                    }
                    vm.supplier = result.supplier;
                    vm.suppliergstinformation = result.suppliergstinformation;
                    if (parseFloat(vm.supplier.defaultCreditDays) == 0)
                        vm.supplier.defaultCreditDays = "";
                });
            }

            init();

            vm.gstStatusSelected = function () {
                if (vm.suppliergstinformation.gstStatus == 0) {
                    vm.suppliergstinformation.reverseChargeApplicable = false;
                    vm.suppliergstinformation.payGstApplicable = true;
                }
                else {

                }
            }

        }
    ]);
})();

