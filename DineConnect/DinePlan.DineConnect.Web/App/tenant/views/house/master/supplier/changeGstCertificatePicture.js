﻿
(function () {
    appModule.controller('tenant.views.house.master.supplier.changeGstCertificatePicture', [
        '$scope', 'appSession', '$uibModalInstance', 'FileUploader', 'abp.services.app.supplier', 'gstIn',
        function ($scope, appSession, $uibModalInstance, fileUploader, supplierService, gstIn) {
            var vm = this;
            var new3 = gstIn;
            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/ChangeFileUploadInfo',
                    formData: [{ id: gstIn }],
                    queueLimit: 1,
                    filters: [{
                        name: 'imageFilter',
                        fn: function (item, options) {
                            //File type check
                            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                            if ('|jpg|jpeg|'.indexOf(type) === -1) {
                                abp.message.warn(app.localize('ProfilePicture_Warn_FileType'));
                                return false;
                            }
                          

                            //File size check
                            if (item.size > 30720) //3000KB
                            {
                                abp.message.warn(app.localize('ProfilePicture_Warn_SizeLimit'));
                                return false;
                            }

                        return true;
                    }
                }]
            });


            vm.save = function () {
                vm.uploader.uploadAll();
            };

            vm.uploader.onBeforeUploadItem = function (fileitem) {
                fileitem.formData.push({ gstIn: gstIn });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.success) {
                    $uibModalInstance.close(response.result);
                } else {
                    abp.message.error(response.error.message);
                    $uibModalInstance.close();
                    return;
                }
            };
        }
    ]);
})();