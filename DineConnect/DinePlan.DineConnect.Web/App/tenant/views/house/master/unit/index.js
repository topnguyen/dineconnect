﻿(function () {
    appModule.controller('tenant.views.house.master.unit.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.unit',
        function ($scope, $modal, uiGridConstants, unitService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.Unit.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.Unit.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.Unit.Delete')
            };

            vm.importMaster = function () {
                importMasterModal(null);
            };

            function importMasterModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/unit/importUnitMasterModal.cshtml',
                    controller: 'tenant.views.house.master.unit.importUnitMasterModal as vm',
                    backdrop: 'static'
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                    vm.loading = false;
                });
            }

            vm.importConversion = function () {
                importConversionModal(null);
            };

            function importConversionModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/unit/importModal.cshtml',
                    controller: 'tenant.views.house.master.unit.importModal as vm',
                    backdrop: 'static'
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                    vm.loading = false;
                });
            }
            

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editUnit(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteUnit(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "      <li><a ng-click=\"grid.appScope.openUnitConversionLink(row.entity)\">" + app.localize("Conversion") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('UnitConversion'),
                        enableSorting: false,
                        width: 180,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.openUnitConversionLink(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>' +
                            '  <br>' +
                            '</div>',
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('UnitConversion') + ' ' + app.localize('Count'),
                        enableSorting: false,
                        field: 'noOfConversions'
                    },                    
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };


            vm.openUnitConversionLink = function (myObj) {
                vm.loading = true;
                createUnitConversion(myObj.id);
            }

            function createUnitConversion(objId) {

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/unitconversion/createOrEditModal.cshtml',
                    controller: 'tenant.views.house.master.unitconversion.createOrEditModal as vm',
                    backdrop: 'static',
                    resolve: {
                        argunitIdForLock: function () {
                            return objId;
                        },
                        unitconversionId: function () {
                            return null;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                    vm.loading = false;
                });
                vm.loading = false;
            }

            vm.getAll = function () {
                vm.loading = true;

                unitService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editUnit = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createUnit = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteUnit = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteUnitWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            unitService.deleteUnit({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/unit/createOrEditModal.cshtml',
                    controller: 'tenant.views.house.master.unit.createOrEditModal as vm',
                    backdrop: 'static',
                    resolve: {
                        unitId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.exportToExcel = function () {
                vm.loading = true;
                unitService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            };


            vm.getAll();
        }]);
})();

