﻿(function () {
    appModule.controller('tenant.views.house.master.unit.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.unit', 'unitId', 
        function ($scope, $modalInstance, unitService, unitId) {
            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            vm.unit = null;
			$scope.existall = true;
			
			vm.save = function () {
			    
			   if ($scope.existall == false)
                    return;
			   vm.saving = true;
			   vm.unit.name = vm.unit.name.toUpperCase();

                unitService.createOrUpdateUnit({
                    unit: vm.unit
                }).success(function () {
                    abp.notify.info(app.localize('Unit') + ' ' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
			     if (vm.unit.name == null) {
			         vm.existall = false;
			         return;
			     }
                unitService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'name',
                    filter: vm.unit.name,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.unit.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.unit.name + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
			
	        function init() {
                unitService.getUnitForEdit({
                    Id: unitId
                }).success(function (result) {
                    vm.unit = result.unit;
                });
            }
            init();
        }
    ]);
})();

