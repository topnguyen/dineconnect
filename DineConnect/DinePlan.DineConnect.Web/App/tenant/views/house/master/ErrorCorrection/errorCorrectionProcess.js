﻿(function () {
    appModule.controller('tenant.views.house.master.errorCorrection.errorCorrectionProcess', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.stockMovement', 'appSession', 'abp.services.app.location', 'abp.services.app.material',
        function ($scope, $state, $stateParams, $modal, stockmovementService, appSession, locationService, materialService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.locations = [];
            vm.canChangeDbValue = false;

            vm.requestParams = {
                locationRefId : vm.defaultLocationId,
                locations: [],
                materials: [],
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.getLocations = function () {
                locationService.getAll({
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                    angular.forEach(vm.locations, function (value, key) {
                        if (value.id == vm.defaultLocationId)
                            vm.requestParams.locations.push(value);
                    });

                }).finally(function (result) {
                });
            };

            vm.refmaterials = [];
            function fillDropDownRecipe() {
                vm.loading = true;
                materialService.getMaterialForCombobox({}).success(function (result) {
                    vm.refmaterials = result.items;
                    vm.loading = false;
                });
            }

            vm.verifyAndChangeFlag = false;

            vm.verifyClosingStock = function ()
            {
                
                if (vm.requestParams.locationRefId == null || vm.requestParams.locations.length == 0 )
                {
                    abp.notify.warn(app.localize('LocationErr'));
                    return;
                }

                //if (vm.requestParams.materials == null || vm.requestParams.materials.length == 0)
                //{
                //    abp.notify.warn(app.localize('MaterialErr'));
                //    return;
                //}
                
                vm.loading = true;

                stockmovementService.verifyAndRectifyClosingStock({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    //endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationRefId: vm.requestParams.locationRefId,
                    materialsList: vm.requestParams.materials,
                    canChangeDbValue: vm.canChangeDbValue
                }).success(function (result) {
                    vm.output = result.outputChangesList;

                    if (vm.output!=null) {
                        vm.verifyAndChangeFlag = true;
                    }
                    else{
                        vm.verifyAndChangeFlag = false;
                    }
                    

                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.getLocations();
            fillDropDownRecipe();

        }]);
})();

