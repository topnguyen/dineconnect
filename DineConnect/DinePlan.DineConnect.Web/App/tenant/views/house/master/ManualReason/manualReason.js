﻿(function () {
    appModule.controller('tenant.views.house.master.manualreason.manualReason', [
        '$scope', '$uibModalInstance', 'abp.services.app.manualReason', 'manualreasonId', 'uiGridConstants', 'selectOnly', 'generalIncluded','defaultManualReasonCategoryRefId' ,
        function ($scope, $modalInstance, manualreasonService, manualreasonId, uiGridConstants, selectOnly, generalIncluded, defaultManualReasonCategoryRefId) {
            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            vm.manualreason = null;
            vm.newAdditionNeeded = true;
            vm.selectOnly = selectOnly;
            vm.generalIncluded = generalIncluded;
            vm.defaultManualReasonCategoryRefId = defaultManualReasonCategoryRefId;

            if (vm.selectOnly)
                vm.newAdditionNeeded = false;
			$scope.existall = true;
			
            vm.save = function (argSaveOption) {
			   if ($scope.existall == false)
                    return;

                if (vm.manualreason.manualReasonName == '' || vm.manualreason.manualReasonName == null) {
                    abp.notify.error(app.localize('ManualReason') + ' ?');
                    return;
                }
                vm.saving = true;

                manualreasonService.createOrUpdateManualReason({
                    manualReason: vm.manualreason
                }).success(function (result) {
                    abp.notify.info(app.localize('ManualReason') + ' ' + app.localize('SavedSuccessfully'));
                    if (argSaveOption == 2) {
                        $modalInstance.close(result.manualReasonName);
                        vm.saving = false;
                        return;
                    }
                    vm.getAll();
                    manualreasonId = null;
                    init();

                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.refmanualActions = [];
            function fillDropDownManualActions() {
                manualreasonService.getManualReasonCategoryForCombobox({}).success(function (result) {
                    vm.refmanualActions = result.items;
                });
            }
            fillDropDownManualActions();

	        function init() {
                manualreasonService.getManualReasonForEdit({
                    id: manualreasonId
                }).success(function (result) {
                    vm.manualreason = result.manualReason;
                    if (vm.defaultManualReasonCategoryRefId > 0)
                        vm.manualreason.manualReasonCategoryRefId = vm.defaultManualReasonCategoryRefId;
                    vm.getAll();
                });
            }
	        

            //  Index JS coding

	        $scope.$on('$viewContentLoaded', function () {
	            App.initAjax();
	        });

	        vm.loading = false;
	        vm.filterText = null;
	        vm.currentUserId = abp.session.userId;

	        vm.permissions = {
	            create: abp.auth.hasPermission('Pages.Tenant.House.Master.ManualReason.Create'),
	            edit: abp.auth.hasPermission('Pages.Tenant.House.Master.ManualReason.Edit'),
	            'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.ManualReason.Delete')
	        };

	        var requestParams = {
	            skipCount: 0,
	            maxResultCount: app.consts.grid.defaultPageSize,
	            sorting: null
	        };

	        vm.userGridOptions = {
	            enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
	            enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
	            paginationPageSizes: app.consts.grid.defaultPageSizes,
	            paginationPageSize: app.consts.grid.defaultPageSize,
	            useExternalPagination: true,
	            useExternalSorting: true,
	            appScopeProvider: vm,
	            rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
	            columnDefs: [
					//{
					//    name: app.localize('Actions'),
					//    enableSorting: false,
					//    width: 120,
					//    cellTemplate:
					//	   "<div class=\"ui-grid-cell-contents\">" +
					//		    "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
					//		   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
					//		   "    <ul uib-dropdown-menu>" +
					//		   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editManualReason(row.entity)\">" + app.localize("Edit") + "</a></li>" +
					//		   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteManualReason(row.entity)\">" + app.localize("Delete") + "</a></li>" +
					//		   "    </ul>" +
					//		   "  </div>" +
					//		   "</div>"
     //               },
                    {
                        name: app.localize("Edit"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.editManualReason(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" +
                            app.localize("Select") +
                            "\"><i class=\"fa fa-edit\"></i></button>" +
                            "</div>"
                    },
                    {
                        name: app.localize("Select"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.selectReason(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" +
                            app.localize("Select") +
                            "\"><i class=\"fa fa-check\"></i></button>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Action'),
                        field: 'manualReasonCategoryRefName'
                    },
                    {
                        name: app.localize('Reason'),
                        field: 'manualReasonName'
                    },
	            ],
	            onRegisterApi: function (gridApi) {
	                $scope.gridApi = gridApi;
	                $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
	                    if (!sortColumns.length || !sortColumns[0].field) {
	                        requestParams.sorting = null;
	                    } else {
	                        requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
	                    }

	                    vm.getAll();
	                });
	                gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
	                    requestParams.skipCount = (pageNumber - 1) * pageSize;
	                    requestParams.maxResultCount = pageSize;

	                    vm.getAll();
	                });
	            },
	            data: []
	        };

            vm.selectReason = function (data) {
                $modalInstance.close(data.manualReasonName);
            }

	        vm.getAll = function () {
                vm.loading = true;
                if (vm.newAdditionNeeded)
                    vm.generalIncluded = false;

	            manualreasonService.getAll({
	                skipCount: requestParams.skipCount,
	                maxResultCount: requestParams.maxResultCount,
	                sorting: requestParams.sorting,
                    filter: vm.filterText,
                    generalIncluded: vm.generalIncluded,
                    sorting: "ManualReasonName",
                    manualReasonCategoryRefId: vm.manualreason.manualReasonCategoryRefId
	            }).success(function (result) {
	                vm.userGridOptions.totalItems = result.totalCount;
	                vm.userGridOptions.data = result.items;
	            }).finally(function () {
	                vm.loading = false;
	            });
	        };

	        vm.editManualReason = function (myObj) {
	            //openCreateOrEditModal(myObj.id);
                vm.newAdditionNeeded = true;
	            manualreasonId = myObj.id;
	            init();
	        };


	        vm.deleteManualReason = function (myObject) {
	            abp.message.confirm(
                    app.localize('DeleteManualReasonWarning', myObject.manualReasonName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            manualreasonService.deleteManualReason({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
	        };


	        vm.exportToExcel = function () {
	            manualreasonService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
	        };


            init();

        }
    ]);
})();

