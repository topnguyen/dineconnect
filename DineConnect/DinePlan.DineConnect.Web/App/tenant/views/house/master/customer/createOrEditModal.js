﻿
(function () {
    appModule.controller('tenant.views.house.master.customer.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.customer', 'customerId', 'abp.services.app.commonLookup', 'abp.services.app.customerTagDefinition',
        function ($scope, $modalInstance, customerService, customerId, commonLookupService, customertagdefinitionService) {
            var vm = this;
            

            vm.saving = false;
            vm.customer = null;
            vm.loading = false;

			$scope.existall = true;
			
            vm.save = function () {
			   if ($scope.existall == false)
			       return;
			   $scope.errmessage = "";

			   if (vm.customer.defaultCreditDays == 0)
			   if (vm.customer.customerTagRefId == null || vm.customer.customerTagRefId==0)
			       $scope.errmessage = $scope.errmessage + app.localize('CustomerTagNotSelected', vm.customer.defaultCreditDays)

			   if ($scope.errmessage != "") {
			       abp.notify.warn($scope.errmessage, 'Required');
			       return;
			   }
                vm.saving = true;
                vm.customer.customerName = vm.customer.customerName.toUpperCase();
                customerService.createOrUpdateCustomer({
                    customer: vm.customer
                }).success(function () {
                    abp.notify.info('\' Customer \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.customer.name == null) {
			         vm.existall = false;
			         return;
			     }
				 
                customerService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'name',
                    filter: vm.customer.name,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.customer.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.customer.name + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
			 };

			 vm.refcustomerpricetag = [];
			 function fillDropDownCustomerTag() {
			     customertagdefinitionService.getCustomerTagForCombobox({}).success(function (result) {
			         vm.refcustomerpricetag = result.items;
			         if (result.items.length == 1)
			             vm.customer.customerTagRefId = result.items[0].value;
			       
			     });
			 }
			
			 function init() {
			     vm.loading = true;
	            fillDropDownCustomerTag();

                customerService.getCustomerForEdit({
                    Id: customerId
                }).success(function (result) {
                    if (result.customer.id == null) {
                        vm.customer.defaultCreditDays = "";
                    }
                    vm.customer = result.customer;
                    vm.loading = false;
                });
            }
            init();
        }
    ]);
})();

