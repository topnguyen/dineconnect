﻿(function () {
    appModule.controller('tenant.views.house.master.housereport.viewGraph', [
       '$scope', '$uibModalInstance', 'abp.services.app.houseReport',  'materialId', 'allItems', 'initialText',
        function ($scope, $modalInstance, housereportService, materialId, allItems, initialText) {
            var vm = this;

            vm.allItems = allItems;
            vm.currentText = initialText;
            vm.editingText = angular.extend({}, vm.currentText);
            vm.materialId = materialId;

            vm.currencyText = abp.features.getValue('DinePlan.DineConnect.Connect.Currency');

            vm.currentIndex = 0;
            vm.saving = false;

            //Calculating index of currentText in allItems
            for (var i = 0; i < allItems.length; i++) {
                if (vm.allItems[i] == vm.currentText) {
                    vm.currentIndex = i;
                    break;
                }
            }
            
            function moveRecord(close) {
                function executeAfterAction() {
                    if (close) {
                        $modalInstance.close();
                    } else {
                        //Go to next
                        vm.currentIndex++;
                        if (vm.allItems.length <= vm.currentIndex) {
                            $modalInstance.close();
                            return;
                        }

                        vm.currentText = vm.allItems[vm.currentIndex];
                        vm.editingText = angular.extend({}, vm.currentText);
                        vm.showDetails(vm.currentText);
                    }
                }

                executeAfterAction();
            };

            vm.next = function() {
                moveRecord(false);
            }

            vm.previous = function () {
                if (vm.currentIndex <= 0) {
                    return;
                }

                vm.currentIndex--;
                vm.currentText = vm.allItems[vm.currentIndex];
                vm.editingText = angular.extend({}, vm.currentText);
                vm.showDetails(vm.currentText);
            };

            vm.targetValueKeyPress = function ($event) {
                if ($event.keyCode == 13) {
                    vm.next();
                } else if ($event.keyCode == 37) {
                    vm.previous();
                } else if ($event.keyCode == 39) {
                    vm.next();
                }
            }

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.showDetails = function (row) {
                if (row == null)
                    row = vm.currentText;

                vm.supplierList = [];
                vm.ratelist = [];
                vm.diffprice = [];
                vm.diffwithminprice = [];

                vm.billedqtylist = []; 
                vm.purchaseamountlist = [];

                vm.systeminfominprice = [];
                vm.systeminfoavgprice = [];

                vm.minimumaxis = 0;

                angular.forEach(row.supplierAveragePrice, function (value, key) {

                    diffWithAvgPrice = parseFloat(parseFloat(parseFloat(value.price) - parseFloat(value.materialAveragePrice)).toFixed(2));

                    vm.supplierList.push(value.supplierName);
                    value.price = parseFloat(value.price.toFixed(2));
                    vm.ratelist.push(value.price);

                    vm.diffprice.push(diffWithAvgPrice);
                    if (vm.minimumaxis > (diffWithAvgPrice))
                        vm.minimumaxis = (diffWithAvgPrice);

                    minpriceformaterial = parseFloat(parseFloat(parseFloat(value.price) - parseFloat(row.minPrice)).toFixed(2));
                    vm.diffwithminprice.push(minpriceformaterial);

                    vm.billedqtylist.push(value.billedQty);
                    vm.purchaseamountlist.push(value.purchaseAmount);

                    if (value.price> row.minPrice)
                    {
                        vm.systeminfominprice.push(app.localize("PurchaseLoseBasedOnMinimumPrice", value.supplierName, value.billedQty, value.price.toFixed(2), row.minPrice.toFixed(2), Math.abs(minpriceformaterial * value.billedQty).toFixed(2)));
                    }
                    if (value.price > row.avgPrice) {
                        vm.systeminfoavgprice.push(app.localize("PurchaseLoseBasedOnAveragePrice", value.supplierName, value.billedQty, value.price.toFixed(2), row.avgPrice.toFixed(2), Math.abs(parseFloat((row.avgPrice - value.price).toFixed(2)) * value.billedQty)));
                    }
                    


                });

                vm.minimumaxis = Math.floor(vm.minimumaxis) - 1;

                $('#container').highcharts({
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Supplier Wise Price Difference'
                    },
                    xAxis: {
                        categories: vm.supplierList,
                        title: {
                            text: null
                        }
                    },
                    yAxis: {
                        min: vm.minimumaxis,
                        title: {
                            text: 'Price',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        valueSuffix: ' '
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -40,
                        y: 80,
                        floating: true,
                        borderWidth: 1,
                        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                        shadow: true
                    },
                    credits: {
                        enabled: false
                    },
                    series: [
                        {
                            name: app.localize('Price'),
                            data: vm.ratelist
                        },
                        {
                            name: app.localize('DiffWithAvg'),
                            data: vm.diffprice
                        },
                        {
                            name: app.localize('DiffWithMinPrice'),
                            data: vm.diffwithminprice
                        },
                        
                    ]
                });


                $('#purchase_stats').highcharts({
                    title: {
                        text: app.localize('SupplierWise'),
                        x: -20 //center
                    },
                    xAxis: {
                        categories: vm.supplierList, //$.parseJSON(JSON.stringify(result.dashboard.dates))
                    },
                    yAxis: {

                        plotLines: [{
                            value: 0,
                            width: 2,
                            color: '#808080'
                        }]
                    },

                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [
                       {
                           name: app.localize('PurchaseQty'),
                           data: vm.billedqtylist
                       },
                        {
                            name: app.localize('PurchaseAmount'),
                            data: vm.purchaseamountlist
                        },
                    ]
                });
            }

            vm.editingText = angular.extend({}, vm.currentText);
            vm.showDetails(vm.currentText);
            vm.next();
            vm.previous();
        }

    ]);
})();