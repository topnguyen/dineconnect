﻿
(function () {
    appModule.controller('tenant.views.house.master.housereport.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.houseReport', 'housereportId', 'abp.services.app.commonLookup',
        function ($scope, $modalInstance, housereportService, housereportId, commonLookupService) {
            var vm = this;
            
            vm.saving = false;
            vm.housereport = null;
			$scope.existall = true;

			
            vm.save = function () {
			   if ($scope.existall == false)
                    return;
                vm.saving = true;
				
                housereportService.createOrUpdateHouseReport({
                    houseReport: vm.housereport
                }).success(function () {
                    abp.notify.info('\' HouseReport \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.housereport.id == null) {
			         vm.existall = false;
			         return;
			     }
				 
                housereportService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                   // sorting: 'reportKey',
                    filter: vm.housereport.id,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.housereport.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.housereport.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			

	        function init() {
                housereportService.getHouseReportForEdit({
                    Id: housereportId
                }).success(function (result) {
                    vm.housereport = result.houseReport;
                });
            }
            init();
        }
    ]);
})();

