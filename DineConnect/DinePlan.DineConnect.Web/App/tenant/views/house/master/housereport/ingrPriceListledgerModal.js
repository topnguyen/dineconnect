﻿(function () {
    appModule.controller('tenant.views.house.master.housereport.ingrPriceListledgerModal', [
        '$scope', '$uibModalInstance', '$uibModal', 'materialRefId', 'materialRefName' , 'priceListdetails',
        function ($scope, $modalInstance, $modal, materialRefId, materialRefName, priceListdetails) {
            var vm = this;
            vm.materialRefId = materialRefId;
            vm.materialRefName = materialRefName;

            //vm.getDetails = function () {
            //    vm.loading = true;
            //    materialService.getMaterialForEdit({
            //        Id: vm.materialRefId
            //    }).success(function (result) {
            //        vm.materialSupplierData = result.supplierMaterial;
            //        vm.material = result.material;
            //        vm.loading = false;
            //    });
            //}

            //vm.getDetails();

            vm.priceListdetails = priceListdetails;

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();