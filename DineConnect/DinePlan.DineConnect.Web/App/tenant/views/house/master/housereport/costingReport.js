﻿(function () {
    appModule.controller('tenant.views.house.master.housereport.costingReport', [
        '$scope', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.houseReport', 'appSession', 'abp.services.app.location',
        function ($scope, $state, $stateParams, $modal, uiGridConstants, housereportService, appSession, locationService) {
            var vm = this;
            var nonFilteredData = [];

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.locations = [];
            vm.materialcostingflag = $stateParams.materialcostingflag;
            if (vm.materialcostingflag == "true" || vm.materialcostingflag == true)
                vm.materialcostingflag = true;
            else
                vm.materialcostingflag = false;

            vm.categorycostingflag = $stateParams.categorycostingflag;
            if (vm.categorycostingflag == "true" || vm.categorycostingflag == true)
                vm.categorycostingflag = true;
            else
                vm.categorycostingflag = false;

            vm.printmailflag = true;
            vm.indexId = null;

            vm.requestParams = {
                locations: [],
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.cancel = function () {
                //$modalInstance.dismiss();
            };

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.getLocations = function () {
                locationService.getAll({
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));

                    angular.forEach(vm.locations, function (value, key) {
                        if (value.id == vm.defaultLocationId)
                            vm.requestParams.locations.push(value);
                    });

                }).finally(function (result) {
                });
            };

            vm.getLocations();

            vm.chartData = [];
            vm.chartData.push({ name: 'Opening', y: 0});

            vm.varianceChartData = [];
            vm.varianceChartData.push({ name: 'Actual', y: 0,})
            vm.chartFlag = false;

            vm.costingReport = function ()
            {
                if (vm.requestParams.locations == null || vm.requestParams.locations.length == 0 )
                {
                    abp.notify.warn(app.localize('LocationErr'));
                    return;
                }
                vm.loading = true;
                vm.chartData = [];
                vm.varianceChartData = [];

                housereportService.getCostOfSalesReport({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locations: vm.requestParams.locations,
                    giftIncludeFlag: true,
                	compIncludeFlag: true
                }).success(function (result) {
                    vm.costofsales = result.costOfSales;
                    vm.output = result.costOfSales[0];
                    vm.changeLocationDetail(vm.output,0);
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.displayLocation = app.localize("All");

            vm.changeLocationDetail = function (data, index) {
                vm.indexId = index;
                vm.chartData = [];
                vm.varianceChartData = [];

                if (data.locations.length > 1)
                    vm.displayLocation = app.localize("All");
                else
                    vm.displayLocation = data.locations[0].name;

                vm.output = data;
                vm.chartData.push({ name: 'Opening', y: vm.output.openingCostValue});
                vm.chartData.push({ name: 'Received', y: vm.output.overAllReceivedValue});
                vm.chartData.push({ name: 'Issue', y: vm.output.overAllIssueValue});
                vm.chartData.push({ name: 'Closing', y: vm.output.closingValue});
                document.getElementById("p1").innerHTML = vm.output.mappingRemarks;
                document.getElementById("p2").innerHTML = vm.output.pricingRemarks;

                vm.varianceChartData.push({ name: 'Standard', y: vm.output.foodCostValue, });
                vm.varianceChartData.push({ name: 'Actual', y: vm.output.actualFoodCostValue, });
                vm.varianceChartData.push({ name: 'Variance', y: vm.output.foodCostValue - vm.output.actualFoodCostValue, });
                vm.chartFlag = true;
                vm.drawChart();
            }




            vm.drawChart = function () {
              
                $('#container').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Costing Report - ' + vm.displayLocation
                    },
                    xAxis: {
                        type: 'category'
                    },
                    yAxis: {
                        title: {
                            text: 'Value'
                        }

                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                                format: '{point.y:.1f}'
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> <br/>'
                    },

                    series: [{
                        name: 'Material Cost',
                        colorByPoint: true,
                        data: vm.chartData,
                    }],
              
                });

                $('#variancecontainer').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Variance Report - ' + vm.displayLocation
                    },
                    xAxis: {
                        type: 'category'
                    },
                    yAxis: {
                        title: {
                            text: 'Amount'
                        }

                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                                format: '{point.y:.1f}'
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> <br/>'
                    },

                    series: [{
                        name: 'Variance',
                        colorByPoint: true,
                        data: vm.varianceChartData,
                    }],
            
                });
            }

            vm.drawChart();

            vm.exportToExcel = function () {
                vm.loading = true;
                housereportService.getExcelOfCostOfSales(
                   {
                       costOfSales: vm.costofsales,
                       indexId : vm.indexId
                       //recipeWiseCosting: vm.output.recipeWiseCosting,
                       //totalSales: vm.output.totalSales,
                       //totalCost: vm.output.totalCost,
                       //totalProfit: vm.output.totalProfit,
                       //totalProfitMargin: vm.output.totalProfitMargin,
                       //remarks: vm.output.remarks
                   }
                    )
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            }

            vm.myFunction = function () {
                if (vm.isUndefinedOrNull(vm.output) || vm.output.length == 0) {
                    abp.notify.warn(app.localize('NoDataFound'));
                    vm.printmailflag = true;
                    return;
                }

                document.getElementById("divprintemail").style.visibility = "hidden";
                myPrint();
            }

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            function myPrint() {
                vm.printmailflag = false;
                window.print();
                document.getElementById("divprintemail").style.visibility = "visible";
                vm.printmailflag = true;
            }


            vm.detailDataShown = function (argOption) {
                var modalInstance1 = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/housereport/costingMaterialDetailDrillDown.cshtml',
                    controller: 'tenant.views.house.master.housereport.costingMaterialDetailDrillDown as vm',
                    backdrop: 'static',
                    resolve: {
                        standardledgerDetails: function () {
                            return argOption.standardMaterialUsageDetail;
                        },
                        actualledgerDetails: function () {
                        	return argOption.actualMaterialUsageDetail;
                        },
                        materialRefName: function () {
                            return argOption.materialRefName;
                        },
                        quantity: function () {
                            return argOption.standardQty;
                        }
                    }
                });

                modalInstance1.result.then(function (result) {
                    //vm.getAll();
                });
            }


        }]);
})();

