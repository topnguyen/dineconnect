﻿
(function () {
    appModule.controller('tenant.views.house.master.housereport.purchasereturndetailreport', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.location', 'appSession', 'abp.services.app.supplierMaterial', 'abp.services.app.purchaseReturn',
        function ($scope, $modal, uiGridConstants, locationService, appSession, suppliermaterialService, purchaseReturnService) {

            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.connectdecimals = appSession.connectdecimals;
            vm.housedecimals = appSession.housedecimals;
            vm.uilimit = 30;
            vm.loading = false;
            vm.isPurchaseAllowed = appSession.location.isPurchaseAllowed;
            vm.filterText = null;
            vm.locationRefId = null;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.returnData = [];
            vm.supplierRefId = null;
            vm.exactsearch = false;
            
            vm.currentUserId = abp.session.userId;

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: fromdayAsString,
                endDate: todayAsString
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.locations = [];

            vm.getLocations = function () {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                    if (vm.defaultLocationId > 0) {
                        vm.locationRefId = vm.defaultLocationId;
                    }
                }).finally(function (result) {

                });
            };
            vm.getLocations();

            vm.clear = function () {
                vm.locationRefId = null;
                vm.supplierRefId = null;
                vm.invoiceNumber = null;
            }

            //var countryName = abp.features.getValue('DinePlan.DineConnect.Connect.Country');
            //vm.specialreport = true;
            //if (countryName == 'IN') {
            //    vm.enabledforIndia = true;
            //}

            vm.purchaseReturnreports = function () {
                vm.loading = true;
                vm.returnData = [];
                vm.detailFlag = true;
                purchaseReturnService.getPurchaseReturnReport({
                    locationRefId: vm.locationRefId,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    supplierRefId: vm.supplierRefId,
                    invoiceNumber: vm.invoiceNumber,
                    exactSearchFlag: vm.exactsearch
                }).success(function (result) {
                    vm.purchaseReturn = result;
                    if (result.returnList!=null && result.returnList.length > 0) {
                        vm.returnData = result.returnList;
                        vm.detailFlag = true;
                    }
                    else {
                        vm.returnData = [];
                        vm.detailFlag = false;
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.refsupplier = [];

            function fillDropDownSupplier() {
                vm.loading = true;
                suppliermaterialService.getSupplierForCombobox({}).success(function (result) {
                    vm.refsupplier = result.items;
                    vm.loading = false;
                });
            }
            fillDropDownSupplier();

            vm.exportToExcel = function () {
                vm.loading = true;
                purchaseReturnService.getPurchaseReturnReportToExcel(
                    {
                        locationRefId: vm.locationRefId,
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        supplierRefId: vm.supplierRefId,
                        invoiceNumber: vm.invoiceNumber,
                        exactSearchFlag: vm.exactsearch
                    }
                ).success(function (result) {
                    app.downloadTempFile(result);
                    vm.loading = false;
                });
            };

            

        }]);
})();

