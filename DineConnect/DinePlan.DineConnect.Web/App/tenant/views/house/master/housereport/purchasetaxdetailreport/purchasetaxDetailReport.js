﻿
(function () {
    appModule.controller('tenant.views.house.master.housereport.purchasetaxDetailReport', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.location', 'appSession', 'abp.services.app.supplierMaterial', 'abp.services.app.invoice', 'abp.services.app.houseReport',
        function ($scope, $modal, uiGridConstants, locationService, appSession, suppliermaterialService, invoiceService, housereportService) {

            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.connectdecimals = appSession.connectdecimals;
            vm.housedecimals = appSession.housedecimals;

            vm.loading = false;
            vm.isPurchaseAllowed = appSession.location.isPurchaseAllowed;
            vm.filterText = null;
            vm.locationRefId = null;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.invoiceData = [];
            vm.supplierRefId = 0;
            vm.exactsearch = false;
            vm.detailFlag = true;

            vm.currentUserId = abp.session.userId;

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.locations = [];

            vm.getLocations = function () {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                    if (vm.defaultLocationId > 0) {
                        vm.locationRefId = vm.defaultLocationId;
                    }
                }).finally(function (result) {

                });
            };

            vm.clear = function () {
                vm.locationRefId = null;
                vm.supplierRefId = null;
                vm.invoiceNumber = null;
                vm.userGridOptions.data = [];
            }

            vm.purchasereports = function () {
                if (vm.locationRefId == null || vm.locationRefId == 0) {
                    abp.notify.error(app.localize('LocationErr'));
                    return;
                }

                vm.loading = true;
                vm.invoiceData = [];
                invoiceService.getPurchaseInvoiceTaxDetails({
                    locationRefId: vm.locationRefId,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    supplierRefId: vm.supplierRefId,
                    invoiceNumber: vm.invoiceNumber,
                    exactSearchFlag: vm.exactsearch
                }).success(function (result) {
                    vm.invoiceData = result;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.refsupplier = [];

            function fillDropDownSupplier() {
                vm.loading = true;
                suppliermaterialService.getSupplierForCombobox({}).success(function (result) {
                    vm.refsupplier = result.items;

                    vm.loading = false;
                });
            }

            vm.exportToExcel = function () {
                if (vm.locationRefId == null || vm.locationRefId == 0) {
                    abp.notify.error(app.localize('LocationErr'));
                    return;
                }

                vm.loading = true;
                vm.invoiceData = [];
                invoiceService.getPurchaseInvoiceTaxDetailsToExcel({
                    locationRefId: vm.locationRefId,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    supplierRefId: vm.supplierRefId,
                    invoiceNumber: vm.invoiceNumber,
                    exactSearchFlag: vm.exactsearch
                }).success(function (result) {
                    app.downloadTempFile(result);
                    vm.loading = false;
                }).finally(function () {
                    vm.loading = false;
                });
            };


            fillDropDownSupplier();
            vm.getLocations();


        }]);
})();

