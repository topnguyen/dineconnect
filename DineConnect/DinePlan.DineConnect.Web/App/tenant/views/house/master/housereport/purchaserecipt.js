﻿
(function () {
    appModule.controller('tenant.views.house.master.housereport.purchaserecipt', [
        '$scope', "$stateParams", '$uibModal', 'uiGridConstants', 'abp.services.app.location', 'appSession', 'abp.services.app.material',
        'abp.services.app.supplierMaterial', 'abp.services.app.invoice', 'abp.services.app.houseReport',
        function ($scope, $stateParams, $modal, uiGridConstants, locationService, appSession, materialService, suppliermaterialService, invoiceService, housereportService) {

            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.consolidatedFlag = $stateParams.consolidatedFlag;

            vm.consolidatedColumnDefs =
                [
                    {
                        name: app.localize('Material'),
                        field: 'materialRefName',
                    },
                    {
                        name: app.localize('Qty'),
                        field: 'totalQty',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Unit'),
                        field: 'defaultUnitName',
                    },
                    {
                        name: app.localize('Price'),
                        field: 'price',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Amount'),
                        field: 'totalAmount',
                        cellClass: 'ui-ralign'
                    },
                ];

            vm.detailColumnDefs =
                [
                    {
                        name: app.localize('Supplier'),
                        field: 'supplierRefName'
                    },
                    {
                        name: app.localize('InvoiceDate'),
                        field: 'invoiceDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD \''
                    },
                    {
                        name: app.localize('InvoiceNumber'),
                        field: 'invoiceNumber'
                    },
                    {
                        name: app.localize('Material'),
                        field: 'materialRefName',
                    },
                    {
                        name: app.localize('Qty'),
                        field: 'totalQty',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Purchase') + ' ' + app.localize('Unit'),
                        field: 'unitRefName',
                    },
                    {
                        name: app.localize('Price'),
                        field: 'price',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Amount'),
                        field: 'totalAmount',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('UnitPrice'),
                        field: 'priceForDefaultUnitWithTaxUOM',
                    }
                ];

            vm.loading = false;
            vm.isPurchaseAllowed = appSession.location.isPurchaseAllowed;
            vm.filterText = null;
            vm.locationRefId = null;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.podata = [];
            vm.supplierRefId = 0;
            vm.materialRefId = 0;
            vm.exactsearch = false;

            vm.currentUserId = abp.session.userId;

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Supplier'),
                        field: 'supplierRefName'
                    },
                    {
                        name: app.localize('InvoiceDate'),
                        field: 'invoiceDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD \''
                    },
                    {
                        name: app.localize('InvoiceNumber'),
                        field: 'invoiceNumber'
                    },
                    {
                        name: app.localize('Material'),
                        field: 'materialRefName',
                    },
                    {
                        name: app.localize('Qty'),
                        field: 'totalQty',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Purchase') + ' ' + app.localize('Unit'),
                        field: 'unitRefName',
                    },
                    {
                        name: app.localize('Price'),
                        field: 'price',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Amount'),
                        field: 'totalAmount',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('UnitPrice'),
                        field: 'priceForDefaultUnitWithTaxUOM',
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.locations = [];

            vm.getLocations = function () {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                    if (vm.defaultLocationId != null && vm.defaultLocationId > 0) {
                        vm.locationRefId = vm.defaultLocationId;
                    }
                }).finally(function (result) {

                });
            };

            vm.clear = function () {
                vm.locationRefId = null;
                vm.materialRefId = null;
                vm.supplierRefId = null;
                vm.invoiceNumber = null;
                vm.userGridOptions.data = [];
            }


            vm.purchasereports = function () {
                if (vm.consolidatedFlag == false) {
                    vm.loading = true;
                    vm.userGridOptions.columnDefs = vm.detailColumnDefs;
                    invoiceService.getPurchaseAnalysis({
                        locationRefId: vm.locationRefId,
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        supplierRefId: vm.supplierRefId,
                        materialRefId: vm.materialRefId,
                        invoiceNumber: vm.invoiceNumber,
                        exactSearchFlag: vm.exactsearch
                    }).success(function (result) {
                        vm.podata = result.invoiceList;
                        if (result.invoiceList != null) {
                            vm.userGridOptions.totalItems = result.invoiceList.length;
                            vm.userGridOptions.data = result.invoiceList;
                        }
                    }).finally(function () {
                        vm.loading = false;
                    });
                }
                else if (vm.consolidatedFlag == true) {
                    vm.loading = true;
                    vm.userGridOptions.columnDefs = vm.consolidatedColumnDefs;
                    invoiceService.getPurchaseAnalysisConsolidated({
                        locationRefId: vm.locationRefId,
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        supplierRefId: vm.supplierRefId,
                        materialRefId: vm.materialRefId,
                        invoiceNumber: vm.invoiceNumber,
                        exactSearchFlag: vm.exactsearch
                    }).success(function (result) {
                        vm.podata = result.invoiceList;
                        if (result.invoiceList != null) {
                            vm.userGridOptions.totalItems = result.invoiceList.length;
                            vm.userGridOptions.data = result.invoiceList;
                        }
                    }).finally(function () {
                        vm.loading = false;
                    });
                }
            }

            vm.refsupplier = [];

            function fillDropDownSupplier() {
                vm.loading = true;
                suppliermaterialService.getSupplierForCombobox({}).success(function (result) {
                    vm.refsupplier = result.items;

                    vm.loading = false;
                });
            }

            vm.refmaterial = [];

            function fillDropDownMaterial() {
                materialService.getMaterialForCombobox({}).success(function (result) {
                    vm.refmaterial = result.items;
                });
            }

            vm.exportToExcel = function () {

                if (vm.consolidatedFlag == false) {
                    vm.loading = true;
                    housereportService.getPurchaseSummary(
                        {
                            locationRefId: vm.locationRefId,
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            supplierRefId: vm.supplierRefId,
                            materialRefId: vm.materialRefId,
                            invoiceNumber: vm.invoiceNumber,
                            exactSearchFlag: vm.exactsearch
                        }
                    )
                        .success(function (result) {
                            app.downloadTempFile(result);
                            vm.loading = false;
                        });
                }
                else if (vm.consolidatedFlag == true) {
                    vm.loading = true;
                    housereportService.getPurchaseConsolidatedSummary(
                        {
                            locationRefId: vm.locationRefId,
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            supplierRefId: vm.supplierRefId,
                            materialRefId: vm.materialRefId,
                            invoiceNumber: vm.invoiceNumber,
                            exactSearchFlag: vm.exactsearch
                        }
                    )
                        .success(function (result) {
                            app.downloadTempFile(result);
                            vm.loading = false;
                        });
                }
            };


            fillDropDownMaterial();
            fillDropDownSupplier();
            vm.getLocations();

            if (vm.consolidatedFlag == true) {
                vm.userGridOptions.columnDefs = vm.consolidatedColumnDefs;
            }


        }]);
})();

