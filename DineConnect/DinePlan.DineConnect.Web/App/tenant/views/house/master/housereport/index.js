﻿(function () {
    appModule.controller('tenant.views.house.master.housereport.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.houseReport', 'abp.services.app.commonLookup', 'appSession',
        function ($scope, $modal, uiGridConstants, housereportService, commonLookupService, appSession) {
            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;

            vm.locationRefId = [];
            $scope.reportKey = null;

            
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate:todayAsString
            });

          
            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };


            vm.stockColumnDefs =
                   [
                       {
                           name: app.localize('MaterialName'),
                           field: 'materialName',
                           width: 340
                       },
                       {
                           name: app.localize('OpeningStock'),
                           field: 'openBalance',
                           cellClass: 'ui-ralign'
                       },
                       {
                           name: app.localize('Received'),
                           field: 'received',
                           cellClass: 'ui-ralign'
                       },
                       {
                           name: app.localize('Excess'),
                           field: 'excessReceived',
                           cellClass: 'ui-ralign'
                       },
                       {
                           name: app.localize('Tra.In'),
                           field: 'transferIn',
                           cellClass: 'ui-ralign'
                       },
                       {
                           name: app.localize('Tra.Out'),
                           field: 'transferOut',
                           cellClass: 'ui-ralign'
                       },
                       {
                           name: app.localize('Issued'),
                           field: 'issued',
                           cellClass: 'ui-ralign'
                       },
                         {
                             name: app.localize('Sales'),
                             field: 'sales',
                             cellClass: 'ui-ralign'
                         },
                       {
                           name: app.localize('Damage'),
                           field: 'damaged',
                           cellClass: 'ui-ralign'
                       },
                       {
                           name: app.localize('Shortage'),
                           field: 'shortage',
                           cellClass: 'ui-ralign'
                       },
                       {
                           name: app.localize('Return'),
                           field: 'return',
                           cellClass: 'ui-ralign'
                       },
                       {
                           name: app.localize('ClosingStock'),
                           field: 'clBalance',
                           cellClass: 'ui-ralign'
                       }
                   ];


            vm.rateViewColumnDefs = [
                 {
                     name: app.localize('MaterialName'),
                     field: 'materialName',
                     width: 340
                 },
                {
                    name: app.localize('NetQtyRecevied'),
                    field: 'billedQty',
                    cellClass: 'ui-ralign'
                },
                {
                    name: app.localize('NetAmount'),
                    field: 'netAmount',
                    cellClass: 'ui-ralign'
                },
                {
                    name: app.localize('AvgRate'),
                    field: 'avgRate',
                    cellClass: 'ui-ralign',
                    cellFilter: 'fractionFilter'
                }
            ]

            vm.supplierRateViewColumnDefs = [
                {
                    name: app.localize('MaterialName'),
                    field: 'materialName',
                    width: 400
                },
                {
                    name: app.localize('SupplierName'),
                    field: 'supplierName',
                    width: 400
                },
                {
                   name: app.localize('Rate'),
                   field: 'rate',
                   cellClass: 'ui-ralign',
                   cellFilter: 'fractionFilter'
                }
            ]

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.HouseReport.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.HouseReport.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.HouseReport.Delete')
            };

            vm.funcRepKey = function (val) {
                $scope.reportKey = val;
                vm.userGridOptions.totalItems = 0;
                vm.userGridOptions.data = [];
                if (val == "STOCK")
                {
                    vm.userGridOptions.columnDefs = vm.stockColumnDefs;
                }
                else if(val == "RATEVIEW")
                {
                    vm.userGridOptions.columnDefs = vm.rateViewColumnDefs;
                }
                else if (val == "SUPPLIERRATEVIEW") {
                    vm.userGridOptions.columnDefs = vm.supplierRateViewColumnDefs;
                }
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				//columnDefs:[],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.refreport = [];

            function fillDropDownReport() {
                commonLookupService.getHouseReportForCombobox({}).success(function (result) {
                    vm.refreport = result.items;
                });
            }

            vm.reflocation = [];

            function fillDropDownLocation() {
                commonLookupService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;
                    if (vm.defaultLocationId != null && vm.defaultLocationId > 0) {
                        vm.locationRefId = vm.defaultLocationId;
                    }
                });
            }

            vm.getAll = function () {
                if (vm.locationRefId == 0 || vm.locationRefId == null)
                {
                    abp.notify.info(app.localize('LocationErr'));
                    return;
                }
                if (vm.reportRefKey == 0 || vm.reportRefKey == null) {
                    abp.notify.info(app.localize('ReportNotSelected'));
                    return;
                }

                vm.loading = true;
               
                if ($scope.reportKey == "STOCK") {
                    housereportService.getStockSummary({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterText,
                        locationRefId: vm.locationRefId,
                        reportKey: $scope.reportKey,
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format)
                    }).success(function (result) {
                        vm.userGridOptions.totalItems = result.totalCount;
                        vm.userGridOptions.data = result.stockSummary;
                        vm.userGridOptions.columnDefs = vm.stockColumnDefs;
                        //vm.userGridOptions.api.setColumnDefs();
                    }).finally(function () {
                        vm.loading = false;
                    });
                }
                else if ($scope.reportKey == "RATEVIEW")
                {
                    housereportService.getMaterialRateView({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterText,
                        locationRefId: vm.locationRefId,
                        reportKey: $scope.reportKey,
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format)
                    }).success(function (result) {
                        vm.userGridOptions.totalItems = result.totalCount;
                        vm.userGridOptions.data = result.materialRateView;
                        vm.userGridOptions.columnDefs = vm.rateViewColumnDefs;
                        //vm.userGridOptions.api.setColumnDefs();
                    }).finally(function () {
                        vm.loading = false;
                    });
                }
                else if ($scope.reportKey == "SUPPLIERRATEVIEW")
                {
                    housereportService.getSupplierRateView({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterText,
                        locationRefId: vm.locationRefId,
                        reportKey: $scope.reportKey,
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format)
                    }).success(function (result) {
                        vm.userGridOptions.totalItems = result.totalCount;
                        vm.userGridOptions.data = result.supplierRateView;
                        vm.userGridOptions.columnDefs = vm.supplierRateViewColumnDefs;
                        //vm.userGridOptions.api.setColumnDefs();
                    }).finally(function () {
                        vm.loading = false;
                    });
                }
            };

            vm.editHouseReport = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createHouseReport = function () {
                openCreateOrEditModal(null);
            };

          
            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/housereport/createOrEditModal.cshtml',
                    controller: 'tenant.views.house.master.housereport.createOrEditModal as vm',
                    backdrop: 'static',
					keyboard: false,
                    resolve: {
                        housereportId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    //vm.getAll();
                    fillDropDownReport();
                });
            }

            vm.exportToExcel = function () {
                housereportService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            //vm.getAll();

            fillDropDownLocation();
            fillDropDownReport();
            //vm.getLocations();
        }])

    .filter('fractionFilter', function () {
        return function (value) {
            return value.toFixed(3);
        };
    })

	
})();

