﻿
(function () {
    appModule.controller('tenant.views.house.master.housereport.purchaseexportwithCategory', [
        '$scope', "$stateParams", '$uibModal', 'uiGridConstants', 'abp.services.app.location', 'appSession', 'abp.services.app.material',
        'abp.services.app.supplierMaterial', 'abp.services.app.invoice', 'abp.services.app.houseReport',
        function ($scope, $stateParams, $modal, uiGridConstants, locationService, appSession, materialService, suppliermaterialService, invoiceService, housereportService) {

            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.isPurchaseAllowed = appSession.location.isPurchaseAllowed;
            vm.filterText = null;
            vm.locationRefId = null;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.supplierRefId = 0;
            vm.materialRefId = 0;
            vm.exactsearch = false;
            vm.showAlreadyExported = false;

            vm.currentUserId = abp.session.userId;

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };



            vm.consolidatedColumnDefs =
                [
                    {
                        name: app.localize('Supplier'),
                        field: 'supplierRefName',
                        minWidth : 200
                    },
                    {
                        name: app.localize('PoPoReference'),
                        field: 'poReferenceNumber'
                    },
                    {
                        name: app.localize('Location'),
                        field: 'locationRefCode'
                    },
                    {
                        name: app.localize('Group'),
                        field: 'materialGroupRefName'
                    },
                    {
                        name: app.localize('Category'),
                        field: 'materialGroupCategoryRefName'
                    },
                    {
                        name: app.localize('InvoiceNumber'),
                        field: 'invoiceNumber'
                    },
                    {
                        name: app.localize('InvoiceDate'),
                        field: 'invoiceDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD \''
                    },
                    {
                        name: app.localize('WithOutTax'),
                        field: 'totalAmount',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter',
                    },
                    {
                        name: app.localize('Tax'),
                        field: 'taxAmount',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter',
                    },
                    {
                        name: app.localize('InvoiceAmount'),
                        field: 'netAmount',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter',
                    },
                    {
                        name: app.localize('AccountDate'),
                        field: 'accountDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD \'',
                    },

                ];

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: vm.consolidatedColumnDefs,
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.locations = [];

            vm.getLocations = function () {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                    if (vm.defaultLocationId != null && vm.defaultLocationId > 0) {
                        vm.locationRefId = vm.defaultLocationId;
                    }
                }).finally(function (result) {

                });
            };

            vm.setAsComplete = function () {
                vm.loading = true;
                invoiceService.getPurchaseReportCategoryWiseWithPurchaseOrderAnalysis({
                    locationRefId: vm.locationRefId,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    supplierRefId: vm.supplierRefId,
                    materialRefId: vm.materialRefId,
                    invoiceNumber: vm.invoiceNumber,
                    exactSearchFlag: vm.exactsearch,
                    showAlreadyExported: vm.showAlreadyExported,
                    updateExportCompletedFlag: true
                }).success(function (result) {
                    var colDefs = vm.consolidatedColumnDefs;
                    //if (vm.showAlreadyExported)
                    {
                        vm.coltoBeAdded =
                        {
                            name: app.localize('Export') + ' ' + app.localize('Completed'),
                            field: 'exportDateTimeForExternalImport',
                            cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm \'',
                        };
                        colDefs.splice(11, 0, vm.coltoBeAdded);
                        vm.coltoBeAddedForReverse =
                        {
                            //name: app.localize('Cancel') + ' ' + app.localize('Export'),
                            //field: 'exportDateTimeForExternalImport',
                            //cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm \'',
                            name: app.localize('Cancel') + ' ' + app.localize('Export'),
                            enableSorting: false,
                            width: 50,
                            headerCellTemplate: '<span></span>',
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-danger btn-xs" ng-click="grid.appScope.reverseExport(row.entity)"><i class="fa fa-backward"></i></button>' +
                                '</div>'
                        };

                        colDefs.splice(12, 0, vm.coltoBeAddedForReverse);
                    }
                    vm.userGridOptions.columnDefs = colDefs;
                    vm.userGridOptions.totalItems = result.invoiceCategoryWisePurchaseOrderAnalysisDtoList.length;
                    vm.userGridOptions.data = result.invoiceCategoryWisePurchaseOrderAnalysisDtoList;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.clear = function () {
                vm.locationRefId = null;
                vm.materialRefId = null;
                vm.supplierRefId = null;
                vm.invoiceNumber = null;
                vm.userGridOptions.data = [];
            }

            vm.getAll = function () {
                vm.loading = true;
                invoiceService.getPurchaseReportCategoryWiseWithPurchaseOrderAnalysis({
                    locationRefId: vm.locationRefId,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    supplierRefId: vm.supplierRefId,
                    materialRefId: vm.materialRefId,
                    invoiceNumber: vm.invoiceNumber,
                    exactSearchFlag: vm.exactsearch,
                    showAlreadyExported: vm.showAlreadyExported
                }).success(function (result) {
                    var colDefs = vm.consolidatedColumnDefs;
                    if (vm.showAlreadyExported) {
                        vm.coltoBeAdded =
                        {
                            name: app.localize('Export') + ' ' + app.localize('Completed'),
                            field: 'exportDateTimeForExternalImport',
                            cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm \'',
                        };
                        colDefs.splice(11, 0, vm.coltoBeAdded);
                        vm.coltoBeAddedForReverse =
                        {
                            //name: app.localize('Cancel') + ' ' + app.localize('Export'),
                            //field: 'exportDateTimeForExternalImport',
                            //cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm \'',
                            name: app.localize('Cancel') + ' ' + app.localize('Export'),
                            enableSorting: false,
                            //width: 50,
                            headerCellTemplate: '<span></span>',
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button ng-if="row.entity.exportDateTimeForExternalImport!=null" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Click for cancel the export status" data-placement="bottom" ng-click="grid.appScope.reverseExport(row.entity)"><i class="fa fa-backward"></i></button>' +
                                '</div>'
                        };
                        colDefs.splice(12, 0, vm.coltoBeAddedForReverse);
                    }
                    vm.userGridOptions.columnDefs = colDefs;
                    vm.userGridOptions.totalItems = result.invoiceCategoryWisePurchaseOrderAnalysisDtoList.length;
                    vm.userGridOptions.data = result.invoiceCategoryWisePurchaseOrderAnalysisDtoList;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.reverseExport = function (data) {
                vm.loading = true;
                invoiceService.reverseExportCompletion({
                    id: data.invoiceRefId
                }).success(function (result) {
                    if (result) {
                        abp.notify.info(app.localize('UpdatedSuccessfully'));
                        vm.getAll();
                    }
                    else {
                        abp.notify.error(app.localize('Failed'));
                    }
                    vm.loading = false;
                });
            }

            vm.refsupplier = [];

            function fillDropDownSupplier() {
                vm.loading = true;
                suppliermaterialService.getSupplierForCombobox({}).success(function (result) {
                    vm.refsupplier = result.items;

                    vm.loading = false;
                });
            }

            vm.refmaterial = [];

            function fillDropDownMaterial() {
                materialService.getMaterialForCombobox({}).success(function (result) {
                    vm.refmaterial = result.items;
                });
            }

            vm.exportToCategoryExcel = function () {
                vm.loading = true;
                invoiceService.getPurchaseReportCategoryWiseWithPurchaseOrderAnalysisToExcel(
                    {
                        locationRefId: vm.locationRefId,
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        supplierRefId: vm.supplierRefId,
                        materialRefId: vm.materialRefId,
                        invoiceNumber: vm.invoiceNumber,
                        exactSearchFlag: vm.exactsearch
                    }
                )
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            }

            fillDropDownMaterial();
            fillDropDownSupplier();
            vm.getLocations();



            //            vm.userGridOptions.columnDefs = vm.detailColumnDefs;


        }])

        .filter('fractionFilter', function () {
            return function (value) {
                return value.toFixed(3);
            };
        })
})();

