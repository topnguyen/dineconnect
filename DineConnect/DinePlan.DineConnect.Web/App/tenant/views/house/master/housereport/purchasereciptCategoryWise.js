﻿
(function () {
    appModule.controller('tenant.views.house.master.housereport.purchasereciptcategorywise', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.location', 'appSession', 'abp.services.app.material',
        'abp.services.app.supplierMaterial', 'abp.services.app.invoice', 'abp.services.app.houseReport',
        function ($scope, $modal, uiGridConstants, locationService, appSession, materialService, suppliermaterialService, invoiceService, housereportService) {

            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.locationRefId = null;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.podata = [];
            vm.supplierRefId = 0;
            vm.categoryRefId = 0;
            vm.exactsearch = false;

            vm.currentUserId = abp.session.userId;

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Category'),
                        field: 'categoryRefName',
                    },
                    {
                        name: app.localize('Total'),
                        field: 'totalAmount',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Tax'),
                        field: 'taxAmount',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('NetAmount'),
                        field: 'netAmount',
                        cellClass: 'ui-ralign'
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.locations = [];

            vm.getLocations = function () {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                    if (vm.defaultLocationId != null && vm.defaultLocationId > 0) {
                        vm.locationRefId = vm.defaultLocationId;
                    }
                }).finally(function (result) {

                });
            };

            vm.clear = function () {
                vm.categoryRefId = null;
                vm.supplierRefId = null;
                vm.userGridOptions.data = [];
            }

            vm.purchasecategoryreports = function () 
            {
                vm.loading = true;
                invoiceService.getPurchaseAnalysisCategoryWise({
                    locationRefId: vm.locationRefId,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    supplierRefId : vm.supplierRefId,
                    categoryRefId: vm.categoryRefId,
                }).success(function (result) {
                    vm.podata = result;
                    if (result != null) {
                        vm.userGridOptions.totalItems = result.length;
                        vm.userGridOptions.data = result;
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.refsupplier = [];

            function fillDropDownSupplier() {
                vm.loading = true;
                suppliermaterialService.getSupplierForCombobox({}).success(function (result) {
                    vm.refsupplier = result.items;

                    vm.loading = false;
                });
            }

            vm.refgroupcategory = [];

            function fillDropDownCategory() {
                materialService.getMaterialGroupCategoryForCombobox({}).success(function (result) {
                    vm.refgroupcategory = result.items;
                });
            }

            vm.exportToExcel = function () {
            	abp.ui.setBusy('#MyLoginForm');
            	vm.loading = true;
            	invoiceService.getPurchaseAnalysisCategoryWiseToExcel(
                   {
                       locationRefId: vm.locationRefId,
                       startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                       endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                       supplierRefId: vm.supplierRefId,
                       categoryRefId: vm.categoryRefId
                   }
                    )
                    .success(function (result) {
                        app.downloadTempFile(result);
                        abp.ui.clearBusy('#MyLoginForm');
                        vm.loading = false;
                    });
            };


            fillDropDownCategory();

            fillDropDownSupplier();
            vm.getLocations();

        }]);
})();

