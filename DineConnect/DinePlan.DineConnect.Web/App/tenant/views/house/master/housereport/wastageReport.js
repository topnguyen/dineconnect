﻿(function () {
    appModule.controller('tenant.views.house.master.housereport.wastageReport', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.location', 'appSession', 'abp.services.app.material',
        'abp.services.app.adjustment',
        function ($scope, $modal, uiGridConstants, locationService, appSession, materialService, adjustmentService) {
            /* eslint-disable */

            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.alllocationflag = false;
            vm.loading = false;
            vm.filterText = null;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.materialRefId = null;
            vm.materialTypeId = null;
            vm.locationsSelected = [];
            vm.adjustmentMode = "";
            vm.allAdjustmentModeFlag = true;

            $scope.formats = ['YYYY-MMM-DD', 'YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.currentUserId = abp.session.userId;

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Material'),
                        field: 'materialRefName',
                    },
                    {
                        name: app.localize('AdjustmentMode'),
                        field: 'adjustmentMode',
                        cellClass: function (grid, row) {
                            if (row.entity.adjustmentMode != app.localize('Excess'))
                                return 'ui-red';                         
                        },
                    },
                    {
                        name: app.localize('ExcessQty'),
                        field: 'excessQty',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: app.localize('ShortageQty'),
                        field: 'shortageQty',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: app.localize('Qty'),
                        field: 'totalQty',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: app.localize('UOM'),
                        field: 'uom',
                    },
                    {
                        name: app.localize('Price'),
                        field: 'price',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: app.localize('ExcessAmount'),
                        field: 'excessAmount',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: app.localize('ShortageAmount'),
                        field: 'shortageAmount',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },

                    {
                        name: app.localize('Amount'),
                        field: 'totalAmount',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                   
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.alllocations = [];

            vm.getLocations = function () {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.alllocations = $.parseJSON(JSON.stringify(result.items));
                    angular.forEach(vm.alllocations, function (value, key) {
                            if (angular.equals(value.id, vm.defaultLocationId)) {
                                vm.locationsSelected.push(value);
                                return;
                        }
                    });

                }).finally(function (result) {

                });

             
            };

            vm.clear = function () {
                vm.materialTypeId = null;
                vm.materialRefId = null;
                vm.locationsSelected = [];
                vm.userGridOptions.data = [];
                vm.alllocationflag = false;
                vm.adjustmentMode = "";
            }

            vm.refmaterialtype = [];
            function fillDropDownMaterialType() {
                materialService.getMaterialTypeForCombobox({}).success(function (result) {
                    vm.refmaterialtype = result.items;
                });
            }
            
            fillDropDownMaterialType();

            vm.wastagereports = function () 
            {
                if (vm.locationsSelected == null || vm.locationsSelected.length == 0)
                {
                    abp.notify.warn(app.localize('LocationErr'));
                    return;
                }

                vm.loading = true;

                if(vm.alllocationflag==true){
                    vm.locationsSelected = vm.alllocations;
                }

                if (vm.allAdjustmentModeFlag == true) {
                    vm.adjustmentMode = "";
                }

                adjustmentService.getAdjustmentDetailReport({
                    toLocations : vm.locationsSelected,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    materialTypeRefId : vm.materialTypeId,
                    materialRefId: vm.materialRefId,
                    adjustmentMode : vm.adjustmentMode
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.length;
                    vm.userGridOptions.data = result;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.refmaterial = [];

            function fillDropDownMaterial() {
                materialService.getMaterialForCombobox({}).success(function (result) {
                    vm.refmaterial = result.items;
                });
            }

            vm.exportToExcel = function () {
                if (vm.locationsSelected == null || vm.locationsSelected.length == 0) {
                    abp.notify.warn(app.localize('LocationErr'));
                    return;
                }

                vm.loading = true;

                if (vm.alllocationflag == true) {
                    vm.locationsSelected = vm.alllocations;
                }
                vm.loading = true;
                

                if (vm.allAdjustmentModeFlag == true) {
                    vm.adjustmentMode = "";
                }

                adjustmentService.getAdjustmentDetailReportToExcel(
                   {
                       toLocations: vm.locationsSelected,
                       startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                       endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                       materialTypeRefId: vm.materialTypeId,
                       materialRefId: vm.materialRefId,
                       adjustmentMode: vm.adjustmentMode
                   }
                    )
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            };


            fillDropDownMaterial();
            vm.getLocations();


        }])

    .filter('fractionFilter', function () {
        return function (value) {
            return value.toFixed(2);
        };
    })

})();

