﻿(function () {
    appModule.controller('tenant.views.house.master.housereport.materialTrackReport', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.stockMovement', 'appSession', 'abp.services.app.location', 'abp.services.app.material',
        function ($scope, $state, $stateParams, $modal, stockmovementService, appSession, locationService, materialService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.locations = [];

            vm.requestParams = {
                locationRefId : vm.defaultLocationId,
                locations: [],
                materials: [],
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.getLocations = function () {
                locationService.getAll({
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                    angular.forEach(vm.locations, function (value, key) {
                        if (value.id == vm.defaultLocationId)
                            vm.requestParams.locations.push(value);
                    });

                }).finally(function (result) {
                });
            };

            vm.refmaterials = [];
            function fillDropDownRecipe() {
                vm.loading = true;
                materialService.getMaterialForCombobox({}).success(function (result) {
                    vm.refmaterials = result.items;
                    vm.loading = false;
                });
            }

            vm.materialMaterialLedgerCorrection = function (argErrorDate,argMaterialRefId) {
                if (vm.requestParams.locationRefId == null || vm.requestParams.locations.length == 0) {
                    abp.notify.warn(app.localize('LocationErr'));
                    return;
                }
                if (vm.requestParams.materials == null || vm.requestParams.materials.length == 0) {
                    abp.notify.warn(app.localize('MaterialErr'));
                    return;
                }
                if (argErrorDate == null) {
                    abp.notify.warn(app.localize('DateErr'));
                    return;
                }
                vm.loading = true;

                materialService.verifyAndRectifyOpeningStockIssueForGivenDate({
                    startDate: argErrorDate, //moment(vm.dateRangeModel.startDate).format($scope.format),
                    locationRefId: vm.requestParams.locationRefId,
                    materials: vm.requestParams.materials,
                    materialRefId: argMaterialRefId
                }).success(function (result) {
                    vm.output = result;
                    vm.materialTrackReport();
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.materialTrackReport = function ()
            {
                if (vm.requestParams.locationRefId == null || vm.requestParams.locations.length == 0 )
                {
                    abp.notify.warn(app.localize('LocationErr'));
                    return;
                }
                if (vm.requestParams.materials == null || vm.requestParams.materials.length == 0)
                {
                    abp.notify.warn(app.localize('MaterialErr'));
                    return;
                }
                vm.loading = true;

                stockmovementService.getMaterialTrack({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationRefId: vm.requestParams.locationRefId,
                    materials: vm.requestParams.materials
                }).success(function (result) {
                    vm.output = result;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.getLocations();
            fillDropDownRecipe();

            vm.detailDataShown = function (argMaterialRefName, argUom, argSales,argStartDate, argEndDate,argMaterialRefId) {
                argOption = 'Sales';
                ledgerDetails = {
                    materialRefName: argMaterialRefName,
                    uom: argUom,
                    sales: argSales,
                    locationRefId: vm.requestParams.locationRefId,
                    materialRefId: argMaterialRefId
                };

                var modalInstance1 = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/materialledger/drillDownLevel.cshtml',
                    controller: 'tenant.views.house.master.materialledger.drillDownLevel as vm',
                    backdrop: 'static',
                    resolve: {
                        ledgerDetails: function () {
                            return ledgerDetails;
                        },
                        searchFor: function () {
                            return argOption;
                        },
                        startDate: function () {
                            return argStartDate;
                        },
                        endDate: function () {
                            return argEndDate;
                        },
                    }
                });

                modalInstance1.result.then(function (result) {
                    //vm.getAll();
                });
            }


        }]);
})();

