﻿(function () {
    appModule.controller('tenant.views.house.master.housereport.recipecostingReport', [
        '$scope', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.houseReport', 'appSession', 'abp.services.app.location',  'abp.services.app.material',
        function ($scope, $state, $stateParams, $modal, uiGridConstants, housereportService, appSession, locationService, materialService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.locations = [];

            vm.requestParams = {
                locations: [],
                recipes : [],
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.getLocations = function () {
                locationService.getAll({
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                    angular.forEach(vm.locations, function (value, key) {
                        if (value.id == vm.defaultLocationId)
                            vm.requestParams.locations.push(value);
                    });

                }).finally(function (result) {
                });
            };

            vm.refRecipe = [];
            function fillDropDownRecipe() {
                vm.loading = true;
                materialService.getMaterialRecipeTypesForCombobox({}).success(function (result) {
                    vm.refRecipe = result.items;
                    vm.loading = false;
                });
            }

            vm.costingReport = function ()
            {
                
                if (vm.requestParams.locations == null || vm.requestParams.locations.length == 0 )
                {
                    abp.notify.warn(app.localize('LocationErr'));
                    return;
                }
                vm.loading = true;
                housereportService.getRecipeCostingReport({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locations: vm.requestParams.locations,
                    recipes : vm.requestParams.recipes
                }).success(function (result) {
                    vm.output = result;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.getLocations();
            fillDropDownRecipe();

        }]);
})();

