﻿
(function () {
    appModule.controller('tenant.views.house.master.housereport.closingStockExcelwithGroupCategoryMaterial', [
        '$scope', "$stateParams", '$uibModal', 'uiGridConstants', 'abp.services.app.location', 'appSession', 'abp.services.app.material',
        'abp.services.app.invoice', 'abp.services.app.houseReport', 'abp.services.app.dayClose', "abp.services.app.reportBackground",
        function ($scope, $stateParams, $modal, uiGridConstants, locationService, appSession, materialService, invoiceService, housereportService, daycloseService, reportBackground) {

            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.backGroundId = null;
            vm.backGroundJobFinished = false;
            vm.exportMaterialAlso = false;
            vm.filterText = null;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.selectedGroups = [];
            vm.selectedCategories = [];
            vm.selectedMaterials = [];
            vm.selectedLocations = [];
            vm.alllocationflag = false;
            vm.tenantId = appSession.tenant.id;

            vm.currentUserId = abp.session.userId;

            vm.checkBackGrondReportStatus = function () {

            }

            vm.downLoadBackGroundReport = function (objId) {
                reportBackground.getFileDto(
                    objId
                ).success(function (result) {
                    if (result != null)
                        app.openTempFileOnly(result);
                    else
                        abp.notify.error(app.localize('StillBackGroundJobProcessing'));
                }).finally(function () {
                    vm.loading = false;
                });
            }


            vm.openHangFire = function () {
                var url = "";
                url = "http://" + window.location.host + "/hangfire";
                //$window.open(url, '_blank');
                //url = "http://localhost:7301/hangfire";
                window.open(url);
            }


            //$scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];


            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            //$('input[name="closingStockDate"]').daterangepicker({
            //    locale: {
            //        format: $scope.format
            //    },
            //    singleDatePicker: true,
            //    showDropdowns: true,
            //    startDate: moment().format($scope.format),
            //    //minDate: $scope.minDate,
            //    maxDate: moment().format($scope.format)
            //});

            //$('input[name="reportDate"]').daterangepicker({
            //    locale: {
            //        format: $scope.format
            //    },
            //    showDropdowns: true,
            //    startDate: todayAsString,
            //    endDate: todayAsString,
            //    maxDate: moment()
            //});

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.refmaterialgroup = [];
            function fillDropDownMaterialGroup() {
                materialService.getMaterialGroupForCombobox({}).success(function (result) {
                    vm.refmaterialgroup = result.items;
                });
            }
            fillDropDownMaterialGroup();

            vm.refmaterialgroupcategory = [];
            vm.fillDropDownMaterialGroupCategory = function () {
                materialService.getMaterialGroupCategoryForGivenInput({
                    comboboxItemList: vm.selectedGroups
                }).success(function (result) {
                    vm.refmaterialgroupcategory = result.items;
                    vm.selectedCategories = [];
                    if (result.items.length == 1) {
                        vm.selectedCategories.push(result.items[0]);
                    }
                });
            }


            //  Location Link Start
            vm.setDefaultLocationAsSelectedLocation = function () {
                vm.loading = true;
                locationService.getSimpleAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    locationRefId: vm.defaultLocationId
                }).success(function (result) {
                    vm.locationGroup.locations = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            }


            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForSearch();
                vm.setDefaultLocationAsSelectedLocation();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                    //vm.getAll();
                });
            };

            //  Location Link End



            vm.locations = [];
            vm.getLocations = function () {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));

                    vm.locations.some(function (refdata, refkey) {
                        if (refdata.id == vm.defaultLocationId) {
                            vm.selectedLocations.push(refdata);
                            return true;
                        }
                    });

                }).finally(function (result) {

                });
            };

            vm.getLocations();


            vm.clear = function () {
                vm.selectedLocations = [];
                vm.selectedGroups = [];
                vm.selectedCategories = [];
                vm.selectedMaterials = [];
            }

            vm.refmaterial = [];

            function fillDropDownMaterial() {
                materialService.getMaterialForCombobox({}).success(function (result) {
                    vm.refmaterial = result.items;
                });
            }

            vm.exportToCategoryExcel = function (argBackGround) {
                vm.loading = true;
                if (vm.selectedMaterials.length > 0)
                    vm.exportMaterialAlso = true;
                vm.backGroundId = null;
                housereportService.getCloseStockGroupCategoryExcel(
                    {
                        locationGroup: vm.locationGroup,
                        startDate: moment(vm.closingStockDate).format($scope.format),
                        endDate: moment(vm.closingStockDate).format($scope.format),
                        GroupList: vm.selectedGroups,
                        CategoryList: vm.selectedCategories,
                        MaterialList: vm.selectedMaterials,
                        userId: vm.currentUserId,
                        exportMaterialAlso: vm.exportMaterialAlso,
                        runInBackGround: argBackGround,
                        tenantId: vm.tenantId
                    }
                )
                    .success(function (result) {
                        if (argBackGround == true) {
                            vm.backGroundId = result.backGroudId;
                            vm.backGroundReportStatus = app.localize("HouseReportBackGroundJobStatus", vm.backGroundId);
                        }
                        else {
                            app.downloadTempFile(result);
                            vm.backGroundId = null;
                        }
                        vm.loading = false;
                    }).finally(function (result) {
                        vm.loading = false;
                    });
            }

            fillDropDownMaterial();

            vm.checkCloseDay = function () {
                vm.loading = true;
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {

                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }
                    vm.closingStockDate = moment(result.accountDate).format($scope.format);
                    $scope.maxDate = result.accountDate;

                    $('input[name="closingStockDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.closingStockDate,
                        //minDate: $scope.minDate,
                        maxDate: vm.closingStockDate
                    });


                }).finally(function (result) {
                    vm.loading = false;
                });
            }
            vm.checkCloseDay();



        }])

        .filter('fractionFilter', function () {
            return function (value) {
                return value.toFixed(3);
            };
        })
})();

