﻿(function () {
    appModule.controller('tenant.views.house.master.housereport.consolidatedClosingStockDiff', [
        '$scope','$state', '$uibModal', 'appSession', 'abp.services.app.location', 'abp.services.app.material', 'abp.services.app.houseReport',
        function ($scope, $state, $modal, appSession, locationService, materialService, housereportService) {
            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.outputDtoList = null;
            vm.selectedLocations = [];
            vm.selectedMaterials = [];
            vm.alllocationflag = true;
            vm.allmaterialflag = true;
            vm.isHighValueItem = false;

            vm.defaultLocationId = appSession.user.locationRefId;

            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            
            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];
            vm.stockDate = moment().format($scope.format);

            $('input[name="closingDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: vm.stockDate,
                maxDate: moment()
            });

            

            vm.materials = [];
            vm.getMaterials = function () {
                materialService.getMaterialForCombobox({
                }).success(function (result) {
                    vm.materials = $.parseJSON(JSON.stringify(result.items));
                }).finally(function (result) {
                });
            };

            vm.getMaterials();


            vm.locations = [];

            vm.getLocations = function () {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                    vm.locations.some(function (refdata, refkey) {
                        if (refdata.id == vm.defaultLocationId) {
                            vm.selectedLocations.push(refdata);
                            return true;
                        }
                    });

                }).finally(function (result) {

                });
            };

            vm.getLocations();

            vm.exportToExcel = function () {
                if (vm.alllocationflag)
                    vm.selectedLocations = vm.locations;
                if (vm.selectedLocations.length == 0) {
                    abp.notify.warn(app.location('LocationErr'));
                    return;
                }
                if (!vm.allmaterialflag) {
                    if (vm.selectedMaterials.length == 0) {
                        abp.notify.warn(app.location('Materials') + " ?");
                        return;
                    }
                }
                vm.loading = true;

                var dt = moment(vm.stockDate).format($scope.format);

                housereportService.getLocationWiseConsolidatedClosingStockVarianceReportExcel({
                    isAllMaterialFlag: vm.allmaterialflag,
                    isHighValueItem: vm.isHighValueItem,
                    stockTakenDate: vm.stockDate,
                    locations: vm.selectedLocations,
                    materials: vm.selectedMaterials
                }).success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    }).finally(function (result) {
                        vm.loading = false;
                });
            };


        }
    ]);
})();