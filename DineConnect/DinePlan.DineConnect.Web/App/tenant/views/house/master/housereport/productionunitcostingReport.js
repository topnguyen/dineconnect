﻿(function () {
    appModule.controller('tenant.views.house.master.housereport.productionunitcostingReport', [
        '$scope', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.houseReport', 'appSession', 'abp.services.app.location', 'abp.services.app.commonLookup',
        function ($scope, $state, $stateParams, $modal, uiGridConstants, housereportService, appSession, locationService, commonLookupService) {
            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.locations = [];
            vm.printmailflag = true;
            vm.output = [];

            vm.requestParams = {
                locations: [],
                productionunits: [],
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.allproductionunitsflag = true;

            vm.locationRefId = null;

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.productionunits = [];

            vm.getProductionUnits = function () {
                vm.productionunits = [];
                if (vm.locationRefId == null)
                    return;

                locationService.getProductionUnits(
                    {
                        id: vm.locationRefId
                    }).success(function (result) {
                        vm.productionunits = $.parseJSON(JSON.stringify(result.items));
                    }
                    );
            };


            vm.getLocations = function () {
                locationService.getLocationWithProductionUnits({
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));

                    vm.locationRefId = vm.defaultLocationId;

                }).finally(function (result) {
                    vm.getProductionUnits();
                });
            };

            vm.costingReport = function () {
                vm.requestParams.locations = [];
                angular.forEach(vm.locations, function (value, key) {
                    if (value.id == vm.locationRefId)
                        vm.requestParams.locations.push(value);
                });

                if (vm.requestParams.locations == null || vm.requestParams.locations.length == 0) {
                    abp.notify.warn(app.localize('LocationErr'));
                    return;
                }

                if (vm.allproductionunitsflag)
                    vm.requestParams.productionunits = vm.productionunits;

                vm.loading = true;
                housereportService.getProductionUnitCostingReport({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locations: vm.requestParams.locations,
                    productionunits: vm.requestParams.productionunits
                }).success(function (result) {
                    vm.output = result;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.getLocations();

            vm.myFunction = function () {
                if (vm.isUndefinedOrNull(vm.output.productionWiseCosting) || vm.output.productionWiseCosting.length == 0) {
                    abp.notify.warn(app.localize('NoDataFound'));
                    vm.printmailflag = true;
                    return;
                }

                document.getElementById("divprintemail").style.visibility = "hidden";
                myPrint();
            }


            function myPrint() {
                vm.printmailflag = false;
                window.print();
                document.getElementById("divprintemail").style.visibility = "visible";
                vm.printmailflag = true;
            }

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.exportToExcel = function () {
                if (vm.isUndefinedOrNull(vm.output.productionWiseCosting) || vm.output.productionWiseCosting.length == 0) {
                    abp.notify.warn(app.localize('NoDataFound'));
                    return;
                }
                vm.loading = true;
                housereportService.getProductionUnitCostingReportExcel({
                    productionWiseCosting: vm.output.productionWiseCosting
                }).success(function (result) {
                    app.downloadTempFile(result);
                }).finally(function () {
                    vm.loading = false;
                });
            };


        }]);
})();

