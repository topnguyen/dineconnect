﻿(function () {
    appModule.controller('tenant.views.house.master.housereport.menuitemcostingReport', [
        '$scope', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.houseReport', 'appSession', 'abp.services.app.location', 'abp.services.app.commonLookup',
        function ($scope, $state, $stateParams, $modal, uiGridConstants, housereportService, appSession, locationService, commonLookupService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.locations = [];
            vm.allmenuitemsflag = true;
            vm.alllocationflag = false;
            vm.printmailflag = true;
            vm.consolidatedFlag = false;
            vm.detailFlag = false;
            vm.includeGiftFlag = false;
			vm.includeCompFlag = false;

			$scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
			$scope.format = $scope.formats[0];

            vm.reportStatus = $stateParams.reportStatus;
            if (vm.reportStatus == 'Consolidated')
                vm.consolidatedFlag = true;
            else if (vm.reportStatus == 'Detail')
                vm.detailFlag = true;
            else if (vm.reportStatus == 'Both')
            {
                vm.consolidatedFlag = true;
                vm.detailFlag = true;
            }

            

            vm.requestParams = {
                locations: [],
                menuItems : [],
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.getLocations = function () {
                locationService.getAll({
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                    angular.forEach(vm.locations, function (value, key) {
                        if (value.id == vm.defaultLocationId)
                            vm.requestParams.locations.push(value);
                    });
                }).finally(function (result) {
                });
            };
            vm.getLocations();


            vm.refmenuitem = [];

            function fillDropDownMenuItem() {
                vm.loading = true;
                commonLookupService.getMenuPortionForCombobox({}).success(function (result) {
                    vm.refmenuitem = result.items;
                    vm.loading = false;
                });
            }

           // fillDropDownMenuItem();

            vm.myFunction = function () {
                if (vm.isUndefinedOrNull(vm.output.recipeWiseCosting) || vm.output.recipeWiseCosting.length == 0) {
                    abp.notify.warn(app.localize('NoDataFound'));
                    vm.printmailflag = true;
                    return;
                }

                document.getElementById("divprintemail").style.visibility = "hidden";
                myPrint();
            }

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            function myPrint() {
                vm.printmailflag = false;
                window.print();
                document.getElementById("divprintemail").style.visibility = "visible";
                vm.printmailflag = true;
            }

            vm.exportToExcel = function()
            {
                vm.loading = true;
                housereportService.getExcelOfMenuCost(
                   {
                       recipeWiseCosting: vm.output.recipeWiseCosting,
                       totalSales : vm.output.totalSales,
                       totalCost: vm.output.totalCost,
                       totalProfit: vm.output.totalProfit,
                       totalProfitMargin: vm.output.totalProfitMargin,
                       remarks : vm.output.remarks
                   }
                    )
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            }


            vm.costingReport = function ()
            {
                vm.loading = true;
                housereportService.getMenuItemCostingReport({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locations: vm.requestParams.locations,
                    menuItems: vm.requestParams.menuItems,
                    giftIncludeFlag: vm.includeGiftFlag,
                    compIncludeFlag: vm.includeCompFlag
                }).success(function (result) {
                    vm.output = result;
                    if (vm.output.remarks !== '' && vm.output.remarks !=null )
                        document.getElementById("p1").innerHTML = vm.output.remarks;
                    else
                        document.getElementById("p1").innerHTML = '';

                }).finally(function () {
                    vm.loading = false;
                    if (vm.allmenuitemsflag)
                        vm.requestParams.menuItems = [];
                });
            };



        }]);
})();

