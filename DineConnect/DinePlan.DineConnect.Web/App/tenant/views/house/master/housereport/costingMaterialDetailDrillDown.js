﻿(function () {
    appModule.controller('tenant.views.house.master.housereport.costingMaterialDetailDrillDown', [
        '$scope', '$uibModalInstance', 'standardledgerDetails', 'actualledgerDetails', 'materialRefName', 'quantity',
        function ($scope, $modalInstance, standardledgerDetails, actualledgerDetails, materialRefName, quantity) {

            var vm = this;
            vm.materialLedgerData = standardledgerDetails;
            vm.materialRefName = materialRefName;
            vm.quantity = quantity;

            vm.detailData = standardledgerDetails;
            vm.recipeExistFlag = false;
            angular.forEach(vm.detailData, function (value, key) {
            	if (value.recipeRefId>0) {
            		vm.recipeExistFlag = true;
            		return;
            	}
            });
            vm.actualData = null;

            vm.actualData = actualledgerDetails;

            //if (searchFor=='Sales')
            //{
            //    vm.loading = true;
            //    materialService.getSalesDetail(
            //    {
            //        locationRefId: ledgerDetails.locationRefId,
            //        transactionStartDate: ledgerDetails.ledgerDate,
            //        transactionEndDate: ledgerDetails.ledgerDate,
            //        materialRefId : ledgerDetails.materialRefId
            //    }).success(function (result) {
            //        vm.detailData = result;
            //    }).finally(function () {
            //        vm.loading = false;
            //    });

            //}

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

        }
    ]);
})();