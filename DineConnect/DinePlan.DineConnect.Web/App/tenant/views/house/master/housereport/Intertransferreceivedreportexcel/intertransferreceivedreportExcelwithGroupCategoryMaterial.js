﻿
(function () {
    appModule.controller('tenant.views.house.master.housereport.intertransferreceivedreportExcelwithGroupCategoryMaterial', [
        '$scope', "$stateParams", '$uibModal', 'uiGridConstants', 'abp.services.app.location', 'appSession', 'abp.services.app.material',
        'abp.services.app.interTransfer',
        function ($scope, $stateParams, $modal, uiGridConstants, locationService, appSession, materialService, intertransferService) {

            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.exportMaterialAlso = false;
            vm.isPurchaseAllowed = appSession.location.isPurchaseAllowed;
            vm.filterText = null;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.selectedGroups = [];
            vm.selectedCategories = [];
            vm.selectedMaterials = [];
            vm.selectedLocations = [];
            vm.alllocationflag = false;

            vm.currentUserId = abp.session.userId;

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.refmaterialgroup = [];
            function fillDropDownMaterialGroup() {
                materialService.getMaterialGroupForCombobox({}).success(function (result) {
                    vm.refmaterialgroup = result.items;
                });
            }
            fillDropDownMaterialGroup();

            vm.refmaterialgroupcategory = [];
            vm.fillDropDownMaterialGroupCategory = function () {
                materialService.getMaterialGroupCategoryForGivenInput({
                    comboboxItemList: vm.selectedGroups
                }).success(function (result) {
                    vm.refmaterialgroupcategory = result.items;
                    vm.selectedCategories = [];
                    if (result.items.length == 1) {
                        vm.selectedCategories.push(result.items[0]);
                    }
                });
            }


            //  Location Link Start
            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                    //vm.getAll();
                });
            };

            //  Location Link End



            vm.locations = [];
            vm.getLocations = function () {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));

                    vm.locations.some(function (refdata, refkey) {
                        if (refdata.id == vm.defaultLocationId) {
                            vm.selectedLocations.push(refdata);
                            return true;
                        }
                    });

                }).finally(function (result) {

                });
            };

            vm.getLocations();


            vm.clear = function () {
                vm.selectedLocations = [];
                vm.selectedGroups = [];
                vm.selectedCategories = [];
                vm.selectedMaterials = [];
            }

            vm.refmaterial = [];

            function fillDropDownMaterial() {
                materialService.getMaterialForCombobox({}).success(function (result) {
                    vm.refmaterial = result.items;
                });
            }

            vm.exportToCategoryExcel = function () {
                vm.loading = true;
                if (vm.selectedMaterials.length > 0)
                    vm.exportMaterialAlso = true;
                intertransferService.getInterTransferReceivedConsolidatedReportGroupOrCategoryOrMaterialWiseForGivenInputToExcel(
                    {
                        locationGroup: vm.locationGroup,
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        GroupList: vm.selectedGroups,
                        CategoryList: vm.selectedCategories,
                        MaterialList: vm.selectedMaterials,
                        userId: vm.currentUserId,
                        ExportMaterialAlso: vm.exportMaterialAlso
                    }
                )
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                        abp.message.info(app.localize('Download') + ' ' + app.localize('Successfully'));
                    }).finally(function (result) {
                        vm.loading = false;
                    });
            }

            fillDropDownMaterial();

        }])

        .filter('fractionFilter', function () {
            return function (value) {
                return value.toFixed(3);
            };
        })
})();

