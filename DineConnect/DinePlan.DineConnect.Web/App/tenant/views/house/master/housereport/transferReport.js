﻿    
(function () {
    appModule.controller('tenant.views.house.master.housereport.transferreport', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.location', 'appSession', 'abp.services.app.material',
        'abp.services.app.interTransfer', 'abp.services.app.houseReport', 
        function ($scope, $modal, uiGridConstants, locationService, appSession, materialService, intertransferService, housereportService) {
            /* eslint-disable */
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.alllocationflag = false;
            vm.loading = false;
            vm.filterText = null;
            vm.locationRefId = null;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.exactsearch = false;
            vm.locationstransferred = [];
            vm.materialList = [];

            $scope.formats = ['YYYY-MMM-DD', 'YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.currentUserId = abp.session.userId;

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeOptions.ranges[app.localize('Tomorrow')] = [moment().add(1, 'days').startOf('day'), moment().add(1, 'days').endOf('day')];
            vm.dateRangeOptions.ranges[app.localize('Next7Days')] = [moment().startOf('day'), moment().add(6, 'days').endOf('day')];
            vm.dateRangeOptions.ranges[app.localize('Next30Days')] = [moment().startOf('day'), moment().add(29, 'days').endOf('day')];

            vm.dateRangeOptions.max = moment().add(90, 'days').format('YYYY-MM-DD');
            vm.dateRangeOptions.maxDate = moment().add(90, 'days').format('YYYY-MM-DD');


            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };


            vm.refmaterialtype = [];
            function fillDropDownMaterialType() {
                materialService.getMaterialTypeForCombobox({}).success(function (result) {
                    vm.refmaterialtype = result.items;
                });
            }

            fillDropDownMaterialType();



            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Material'),
                        field: 'materialRefName',
                    },
                    {
                        name: app.localize('Qty'),
                        field: 'totalQty',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: app.localize('UOM'),
                        field: 'uom'
                    },
                    {
                        name: app.localize('Price'),
                        field: 'price',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: app.localize('Amount'),
                        field: 'totalAmount',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.locations = [];
            vm.alllocations = [];

            vm.getLocations = function () {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                    if (vm.defaultLocationId != null && vm.defaultLocationId > 0) {
                        vm.locationRefId = vm.defaultLocationId;
                    }
                }).finally(function (result) {

                });

                locationService.getLocations({
                }).success(function (result) {
                    vm.alllocations = $.parseJSON(JSON.stringify(result.items));
                }).finally(function (result) {

                });

            };

            vm.clear = function () {
                vm.locationRefId = null;
                //vm.locationtra
                vm.materialList = [];
                vm.invoiceNumber = null;
                vm.dateRangeModel = null;
                vm.locationstransferred = [];
                vm.userGridOptions.data = [];
                vm.alllocationflag = false;
                vm.materialTypeList = [];
            }

            vm.transferreports = function () 
            {
                if (vm.locationRefId == null || vm.locationRefId == 0)
                {
                    abp.notify.warn(app.localize('LocationErr'));
                    vm.loading = false;
                    return;
                }

                if(vm.alllocationflag==true){
                    vm.locationstransferred = vm.alllocations;
                }

                vm.loading = true;

                intertransferService.getTransferDetailReport({
                    locationRefId: vm.locationRefId,
                    toLocations : vm.locationstransferred,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    materialList: vm.materialList,
                    transferNumber: vm.transferNumber,
                    exactSearchFlag: vm.exactsearch,
                    materialTypeList: vm.materialTypeList
                    
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.consolidatedReport.length;
                    vm.userGridOptions.data = result.consolidatedReport;
                    if(vm.alllocationflag==true)
                        vm.locationstransferred = [];
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.refmaterial = [];

            function fillDropDownMaterial() {
                materialService.getMaterialNames({}).success(function (result) {
                    vm.refmaterial = result.items;
                });
            }

            vm.exportToExcel = function (exportType) {
                if (vm.locationRefId == null || vm.locationRefId == 0) {
                    abp.notify.warn(app.localize('LocationErr'));
                    return;
                }

                if (vm.alllocationflag == true) {
                    vm.locationstransferred = vm.alllocations;
                }

                vm.loading = true;

                intertransferService.getTransferReportDetailExcel(
                   {
                       locationRefId: vm.locationRefId,
                       toLocations: vm.locationstransferred,
                       startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                       endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                       materialList: vm.materialList,
                       transferNumber: vm.transferNumber,
                       exactSearchFlag: vm.exactsearch,
                       materialTypeList: vm.materialTypeList,
                       exportType: exportType 
                   }
                    )
                    .success(function (result) {
                        vm.loading = false;
                        if (result == null)
                            return;
                        if (result.length == 1) {
                            app.downloadTempFile(result[0]);
                        }
                        else if (result.length > 1) {
                            angular.forEach(result, function (value, key) {
                                app.openTempFile(result[key]);
                            });
                        }

                        //app.downloadTempFile(result);

                    }).finally(function (result) {
                        vm.loading = false;
                    });

            };


            fillDropDownMaterial();
            vm.getLocations();

            vm.batchFileCreation = function () {
                vm.loading = true;
                housereportService.apiInventoryTransferBatchFileForGivenDatePeriod({
                    locationRefId: vm.locationRefId,
                    stockDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                }).success(function (result) {
                    //app.downloadTempFileOnly(result);
                    abp.notify.info('Exported Successfully');
                    vm.loading = false;
                });
            }



        }])

    .filter('fractionFilter', function () {
        return function (value) {
            return value.toFixed(2);
        };
    })

})();

