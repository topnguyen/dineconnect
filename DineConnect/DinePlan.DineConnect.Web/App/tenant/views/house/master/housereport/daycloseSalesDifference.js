﻿(function () {
    appModule.controller('tenant.views.house.master.housereport.daycloseSalesDifference', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.stockMovement', 'appSession',
        'abp.services.app.location', 'abp.services.app.material',
        function ($scope, $state, $stateParams, $modal, stockmovementService, appSession, locationService, materialService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.locations = [];
            vm.dayCloseSyncErrorList = [];

            vm.requestParams = {
                locationRefId : vm.defaultLocationId,
                locations: [],
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.getLocations = function () {
                locationService.getAll({
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                    angular.forEach(vm.locations, function (value, key) {
                        if (value.id == vm.defaultLocationId)
                            vm.requestParams.locations.push(value);
                    });

                }).finally(function (result) {
                });
            };

            vm.trackReport = function ()
            {
                if (vm.requestParams.locationRefId == null || vm.requestParams.locations.length == 0 )
                {
                    abp.notify.warn(app.localize('LocationErr'));
                    return;
                }
                vm.loading = true;
                vm.dayCloseSyncErrorList = [];
                materialService.getDayCloseSyncReport({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationRefId: vm.requestParams.locationRefId,
                }).success(function (result) {
                    vm.dayCloseSyncErrorList= result;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.getLocations();


        }]);
})();

