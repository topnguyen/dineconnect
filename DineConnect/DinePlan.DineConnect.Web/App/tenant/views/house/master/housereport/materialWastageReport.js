﻿(function () {
    appModule.controller('tenant.views.house.transaction.housereport.materialWastageReport', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.materialLedger', 'abp.services.app.location', 'abp.services.app.material', 'appSession', 'abp.services.app.houseReport',
        function ($scope, $modal, uiGridConstants, materialledgerService, locationService, materialService, appSession, housereportService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.uilimit = 20;
            vm.locationsSelected = [];
            vm.alllocationflag = false;
            vm.selectedEntryTypes = [];

            vm.clear = function () {
                vm.locationsSelected = [];
                vm.alllocationflag = false;
            }

            vm.alllocations = [];

            vm.getLocations = function () {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.alllocations = $.parseJSON(JSON.stringify(result.items));
                    angular.forEach(vm.alllocations, function (value, key) {
                        if (angular.equals(value.id, vm.defaultLocationId)) {
                            vm.locationsSelected.push(value);
                            return;
                        }
                    });
                }).finally(function (result) {

                });
            };

            vm.getLocations();

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.entryTypes = [];
            vm.getEntryTypes = function () {
                vm.entryTypes = [];
                housereportService.getEntryTypes({})
                    .success(function (result) {
                        vm.entryTypes = result;
                    });
            }

            vm.getEntryTypes();

            vm.isCustomReportEnabled = abp.features.isEnabled('DinePlan.DineConnect.Feature.Custom');

            vm.customReportexportToExcel = function () {
                if (vm.locationsSelected == null || vm.locationsSelected.length == 0) {
                    abp.notify.error(app.localize('LocationErr'));
                    return;
                }
                if (vm.locationsSelected == null || vm.locationsSelected.length > 1) {
                    abp.notify.error(app.localize('LocationErr'));
                    abp.notify.error('Only One Location Should be Selected for this report option');
                    return;
                }

                vm.loading = true;
                housereportService.getMaterialWastageToExcel({
                    toLocations: vm.locationsSelected,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    entryTypes: vm.selectedEntryTypes,
                    exportType: 0,
                    customReportFlag: true
                }).success(function (result) {
                    vm.loading = false;

                    if (result == null)
                        return;
                    if (result.length == 1) {
                        app.downloadTempFile(result[0]);
                    }
                    else if (result.length > 1) {
                        angular.forEach(result, function (value, key) {
                            app.openTempFile(result[key]);
                        });
                    }
                }).finally(function (result) {
                    vm.loading = false;
                });
            };


            vm.exportToExcel = function (type) {

                vm.loading = true;
                housereportService.getMaterialWastageToExcel({
                    toLocations: vm.locationsSelected,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    entryTypes: vm.selectedEntryTypes,
                    exportType: type
                }).success(function (result) {
                    vm.loading = false;

                    if (result == null)
                        return;
                    if (result.length == 1) {
                        app.downloadTempFile(result[0]);
                    }
                    else if (result.length > 1) {
                        angular.forEach(result, function (value, key) {
                            app.openTempFile(result[key]);
                        });
                    }
                }).finally(function (result) {
                    vm.loading = false;
                });
            };


        }]);
})();


