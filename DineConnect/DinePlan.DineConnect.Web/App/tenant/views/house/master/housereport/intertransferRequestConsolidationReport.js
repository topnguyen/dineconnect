﻿    
(function () {
    appModule.controller('tenant.views.house.master.housereport.intertransferRequestConsolidationReport', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.location', 'appSession', 'abp.services.app.material',
        'abp.services.app.interTransfer', 'abp.services.app.houseReport', 
        function ($scope, $modal, uiGridConstants, locationService, appSession, materialService, intertransferService, housereportService) {
            /* eslint-disable */
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.alllocationflag = false;
            vm.loading = false;
            vm.filterText = null;
            vm.locationRefId = null;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.isPending = true;
            vm.isApproved = true;
            vm.isReceived = true;

            vm.clear = function () {
                vm.deliveryDateFilterApplied = false;
                vm.deliveryDateRangeModel = null;
                vm.dateRangeModel = null;
                vm.dateFilterApplied = false;
                vm.status = null;
            }
            vm.clear();

            $scope.formats = ['YYYY-MMM-DD', 'YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.currentUserId = abp.session.userId;

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {

                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            $('input[name="deliveryDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment().add(90, 'days').format('YYYY-MM-DD'),
            });

            vm.deliveryDateRangeOptions = app.createDateRangePickerOptions();
            vm.deliveryDateRangeOptions.ranges[app.localize('Tomorrow')] = [moment().add(1, 'days').startOf('day'), moment().add(1, 'days').endOf('day')];
            vm.deliveryDateRangeOptions.ranges[app.localize('Next7Days')] = [moment().startOf('day'), moment().add(6, 'days').endOf('day')];
            vm.deliveryDateRangeOptions.ranges[app.localize('Next30Days')] = [moment().startOf('day'), moment().add(29, 'days').endOf('day')];

            vm.deliveryDateRangeOptions.max = moment().add(90, 'days').format('YYYY-MM-DD');
            vm.deliveryDateRangeOptions.maxDate = moment().add(90, 'days').format('YYYY-MM-DD');

            vm.deliveryDateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.refmaterialgroup = [];
            function fillDropDownMaterialGroup() {
                materialService.getMaterialGroupForCombobox({}).success(function (result) {
                    vm.refmaterialgroup = result.items;
                });
            }
            fillDropDownMaterialGroup();

            vm.refmaterialgroupcategory = [];
            vm.fillDropDownMaterialGroupCategory = function () {
                materialService.getMaterialGroupCategoryForCombobox({
                    id: vm.materialGroupRefId
                }).success(function (result) {
                    vm.refmaterialgroupcategory = result.items;
                    if (result.items.length == 1) {
                        vm.materialGroupCategoryRefId = result.items[0].value;
                    }
                    else {
                        vm.materialGroupCategoryRefId = null;
                    }
                });
            }

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };


            vm.refmaterialtype = [];
            function fillDropDownMaterialType() {
                materialService.getMaterialTypeForCombobox({}).success(function (result) {
                    vm.refmaterialtype = result.items;
                });
            }

            fillDropDownMaterialType();

            vm.locations = [];
            vm.alllocations = [];

            vm.getLocations = function () {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                    if (vm.defaultLocationId != null && vm.defaultLocationId > 0) {
                        vm.locationRefId = vm.defaultLocationId;
                    }
                }).finally(function (result) {

                });

                locationService.getLocations({
                }).success(function (result) {
                    vm.alllocations = $.parseJSON(JSON.stringify(result.items));
                }).finally(function (result) {

                });

            };




            //  Location Link Start
            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                    //vm.getAll();
                });
            };

            //  Location Link End
            vm.status = '';
            vm.consolidatedExcel = function () {
                vm.saving = true;
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var currentStatus = null;

                var deliveryStartDate = null;
                var deliveryEndDate = null;

                if (vm.dateFilterApplied) {
                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }

                if (vm.deliveryDateFilterApplied) {
                    deliveryStartDate = moment(vm.deliveryDateRangeModel.startDate).format($scope.format);
                    deliveryEndDate = moment(vm.deliveryDateRangeModel.endDate).format($scope.format);
                }
                currentStatus = vm.status;

                intertransferService.getConsolidatedToExcel({
                    sorting: requestParams.sorting,
                    requestStatus: '',
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    currentStatus: currentStatus,
                    showOnlyDeliveryDateExpectedAsOnDate: vm.showOnlyDeliveryDateExpectedAsOnDate,
                    locationGroup: vm.locationGroup,
                    deliveryStartDate: deliveryStartDate,
                    deliveryEndDate: deliveryEndDate,
                    materialGroupRefId: vm.materialGroupRefId,
                    materialGroupCategoryRefId: vm.materialGroupCategoryRefId,
                    doesPendingRequest: vm.isPending,
                    doesApprovedRequest: vm.isApproved,
                    doesReceivedStatus: vm.isReceived
                }).success(function (result) {
                    app.downloadTempFile(result);
                    vm.saving = false;
                    vm.loading = false;
                }).finally(function (result) {
                    vm.saving = false;
                    vm.loading = false;
                });

            //    intertransferService.getConsolidatedToExcel({
            //        sorting: requestParams.sorting,
            //        requestStatus: '',
            //        defaultLocationRefId: vm.locationRefId,
            //        startDate: startDate,
            //        endDate: endDate,
            //        currentStatus: currentStatus,
            //        showOnlyDeliveryDateExpectedAsOnDate: vm.showOnlyDeliveryDateExpectedAsOnDate,
            //        locationGroup: vm.locationGroup,
            //        doesPendingRequest : vm.isPending,
            //        doesApprovedRequest:vm.isApproved,
            //        doesReceivedStatus: vm.isReceived
            //    }).success(function (result) {
            //        app.downloadTempFile(result);
            //        vm.saving = false;
            //        vm.loading = false;
            //    }).finally(function (result) {
            //        vm.saving = false;
            //        vm.loading = false;
            //    });
            };

            vm.getLocations();

        }])

    .filter('fractionFilter', function () {
        return function (value) {
            return value.toFixed(2);
        };
    })

})();

