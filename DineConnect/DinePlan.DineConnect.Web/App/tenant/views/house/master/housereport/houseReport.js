﻿(function () {
    appModule.controller('tenant.views.house.master.housereport.housereport', [
        '$scope', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.houseReport', 'appSession', 'abp.services.app.location', 'abp.services.app.material', "abp.services.app.tenantSettings", 'abp.services.app.dayClose',
        function ($scope, $state, $stateParams, $modal, uiGridConstants, housereportService, appSession, locationService, materialService, tenantSettingsService, daycloseService) {
            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.reporttorun = $stateParams.reportKey;
            vm.reportHeader = app.localize($stateParams.reportKey);
            vm.housetransactiondate = appSession.location.houseTransactionDate;
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");
            vm.firstrun = true;
            vm.reportSubHeader = '';

            vm.loading = true;
            vm.loadingCount = 0;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.daterangestring = "";
            vm.materialTypeList = [];
            vm.isNeedToBeDisplayedInSingleSheet = false;
            vm.materialRefIds = [];
            vm.isHighValueItemOnly = false;

            vm.locationRefId = [];
            vm.locationRefId = vm.defaultLocationId;
            $scope.reportKey = null;

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.refmaterialtype = [];
            function fillDropDownMaterialType() {
                materialService.getMaterialTypeForCombobox({}).success(function (result) {
                    vm.refmaterialtype = result.items;
                });
            }

            fillDropDownMaterialType();

            vm.decimals = null;
            vm.refmaterials = [];
            vm.uilimit = 30;
            vm.getDecimals = function () {
                vm.loading = true;
                vm.loadingCount++;
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    });

                materialService.getMaterialForCombobox({})
                    .success(function (result) {
                        vm.refmaterials = result.items;
                        vm.loading = false;
                        vm.loadingCount--;
                    });
            };

            vm.$onInit = function () {
                vm.getDecimals();
            };


            vm.checkCloseDay = function () {
                vm.loading = true;
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {

                        if (vm.softmessagenotificationflag)
                            abp.notify.info(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        //if (vm.messagenotificationflag)
                        //    abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }
                    vm.housetransactiondate = moment(result.accountDate).add(0, 'days').format('YYYY-MMM-DD');
                    todayAsString = moment(result.accountDate).add(-1, 'days').format('YYYY-MM-DD');

                    vm.checkVarianceAfterOrBefore();
                    vm.dateRangeModel = {
                        startDate: todayAsString,
                        endDate: todayAsString
                    };

                    if (vm.dateRangeModel.startDate == null) {
                        vm.dateRangeModel.startDate = moment().format($scope.format);
                        vm.dateRangeModel.endDate = moment().format($scope.format);
                    }

                    vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
                    vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;

                    if (vm.reporttorun == "VARIANCESTOCK") {
                        var maxDatetodayAsString = moment(result.accountDate).add(0, 'days').format('YYYY-MM-DD');
                        vm.dateRangeOptions.max = maxDatetodayAsString;
                        $('input[name="reportDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            showDropdowns: true,
                            startDate: todayAsString,
                            endDate: todayAsString,
                            maxDate: moment(result.accountDate)
                        });
                    }

                }).finally(function (result) {
                    vm.loading = false;
                });
            }

            vm.checkCloseDay();

            

            vm.stockColumnDefs =
                [
                    {
                        name: app.localize('MaterialName'),
                        field: 'materialName',
                        width: 200
                    },
                    {
                        name: app.localize('UOM'),
                        field: 'uom',
                        minWidth: 60,
                        enableSorting: false,
                    },
                    {
                        name: app.localize('OpeningStock'),
                        field: 'openBalance',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter',
                        enableSorting: false,
                    },
                    {
                        name: app.localize('Received'),
                        field: 'received',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter',
                        enableSorting: false,
                    },
                    {
                        name: app.localize('Pur') + ' ' + app.localize('Ret'),
                        field: 'supplierReturn',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter',
                        enableSorting: false,
                    },
                    {
                        name: app.localize('Excess'),
                        field: 'excessReceived',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter',
                        enableSorting: false,
                    },
                    {
                        name: app.localize('Tra.In'),
                        field: 'transferIn',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter',
                        enableSorting: false,
                    },
                    {
                        name: app.localize('Tra.Out'),
                        field: 'transferOut',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter',
                        enableSorting: false,
                    },
                    {
                        name: app.localize('Issued'),
                        field: 'issued',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter',
                        enableSorting: false,
                    },
                    {
                        name: app.localize('Sales'),
                        field: 'sales',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter',
                        enableSorting: false,
                    },
                    {
                        name: app.localize('Damage'),
                        field: 'damaged',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter',
                        enableSorting: false,
                    },
                    {
                        name: app.localize('Shortage'),
                        field: 'shortage',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter',
                        enableSorting: false,
                    },
                    {
                        name: app.localize('Return'),
                        field: 'return',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter',
                        enableSorting: false,
                    },
                    {
                        name: app.localize('ClosingStock'),
                        field: 'clBalance',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter',
                        enableSorting: false,
                    },
                    {
                        name: app.localize('AvgPrice'),
                        field: 'avgPrice',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter',
                        enableSorting: false,
                    },
                    {
                        name: app.localize('Value'),
                        field: 'stockValue',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter',
                        enableSorting: false,
                    }

                ];


            vm.variancestockColumnDefs =
                [
                    {
                        name: app.localize('Code'),
                        field: 'materialPetName',
                        minWidth: 60
                    },
                    {
                        name: app.localize('MaterialName'),
                        field: 'materialName',
                        width: 200
                    },
                    {
                        name: app.localize('AvgPrice'),
                        field: 'avgPrice',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: app.localize('UOM'),
                        field: 'uom',
                        minWidth: 60
                    },
                    {
                        name: app.localize('OpeningStock'),
                        field: 'openBalance',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: '+' + app.localize('Purchase'),
                        field: 'received',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: '-' + app.localize('Pur') + ' ' + app.localize('Ret'),
                        field: 'supplierReturn',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: '+' + app.localize('Excess'),
                        field: 'excessReceived',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: '+' + app.localize('Tra.In'),
                        field: 'transferIn',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: '-' + app.localize('Tra.Out'),
                        field: 'transferOut',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: '-' + app.localize('Issued'),
                        field: 'issued',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: '-' + app.localize('Sales'),
                        field: 'sales',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-right\">' +
                            '  <a class="bold" ng-click="grid.appScope.detailDataShown(row.entity)"><i class="fa fa-eye"> {{row.entity.sales}} </i></a>' +
                            '</div>',
                    },
                    {
                        name: '-' + app.localize('Damage'),
                        field: 'damaged',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: '-' + app.localize('Shortage'),
                        field: 'shortage',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: '-' + app.localize('Return'),
                        field: 'return',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: app.localize('Cl ShouldBe'),
                        field: 'clBalance',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: app.localize('Value'),
                        field: 'stockValue',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: app.localize('Theoretical') + ' ' + app.localize('Usage'),
                        field: 'theoreticalUsage',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: app.localize('Theoretical') + ' ' + app.localize('Usage') + ' ' + app.localize('Cost'),
                        field: 'theoreticalUsageCost',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: app.localize('Entered Cl Stk'),
                        field: 'enteredClosingStockDisplay',
                        //cellClass: 'ui-ralign',
                        //cellFilter: 'fractionFilter'
                    },
                    {
                        name: app.localize('Actual') + ' ' + app.localize('Usage'),
                        field: 'actualUsage',   //  Op + Purchase + Tin - Tout - Return - Entered Closing Stock
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: app.localize('Variance'),
                        field: 'varianceStockDisplay',
                        //cellClass: 'ui-ralign',
                        //cellFilter: 'fractionFilter'
                    },
                ];


            vm.closestockColumnDefs =
                [
                    {
                        name: app.localize('MaterialName'),
                        field: 'materialName',
                        width: 340
                    },
                    {
                        name: app.localize('MaterialType'),
                        field: 'materialTypeRefName',
                        width: 340
                    },
                    {
                        name: app.localize('ClosingStock'),
                        field: 'clBalance',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter'
                    },
                    {
                        name: app.localize('UOM'),
                        field: 'uom',
                        width: 340
                    },
                ];

            vm.rateViewColumnDefs = [
                {
                    name: app.localize('MaterialName'),
                    field: 'materialName',
                    minWidth: 340
                },
                {
                    name: app.localize('AveragePriceTagRefName'),
                    field: 'averagePriceTagRefId',
                    cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <span class="label label-success">{{row.entity.averagePriceTagRefName}}</span>' +
                        '</div>',
                },
                {
                    name: app.localize('NetQtyRecevied'),
                    field: 'billedQty',
                    cellClass: 'ui-ralign'
                },
                {
                    name: app.localize('NetAmount'),
                    field: 'netAmount',
                    cellClass: 'ui-ralign',
                    cellFilter: 'fractionFilter'
                },
                {
                    name: app.localize('AvgRate'),
                    field: 'avgRate',
                    cellClass: 'ui-ralign',
                    cellFilter: 'fractionFilter',
                    cellTemplate:
                        '<div class=\"ui-grid-cell-contents text-right\">' +
                        '  <a class="bold" ng-click="grid.appScope.ingrdetailDataShown(row.entity)"><i class="fa fa-eye"> {{row.entity.avgRate}} </i></a>' +
                        '</div>',
                },
                {
                    name: app.localize('DateRange'),
                    field: 'invoiceDateRange',
                    width: 180
                },
                {
                    name: app.localize('LastUpdatedTime'),
                    field: 'lastUpdatedTime',
                    cellFilter: 'momentFormat: \'LLL\'',
                },
            ]

            vm.supplierRateViewColumnDefs = [
                {
                    name: app.localize('MaterialName'),
                    field: 'materialName',
                    width: 400
                },
                {
                    name: app.localize('SupplierName'),
                    field: 'supplierName',
                    width: 400
                },
                {
                    name: app.localize('Rate'),
                    field: 'rate',
                    cellClass: 'ui-ralign',
                    cellFilter: 'fractionFilter'
                }
            ]

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.HouseReport.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.HouseReport.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.HouseReport.Delete')
            };

            $scope.repText = "Select Report...";

            vm.funcRepKey = function (val) {

                $scope.repText = val.displayText;
                $scope.reportKey = val.value;
                vm.reportRefKey = val.value;
                vm.userGridOptions.totalItems = 0;
                vm.userGridOptions.data = [];
                vm.executeReport(val.value);
            };

            vm.executeReport = function (report) {


                vm.userGridOptions.totalItems = 0;
                vm.userGridOptions.data = [];
                if (report == "STOCK") {
                    vm.userGridOptions.columnDefs = vm.stockColumnDefs;
                }
                else if (report == "VARIANCESTOCK") {
                    vm.userGridOptions.columnDefs = vm.variancestockColumnDefs;
                }
                else if (report == "CLOSESTOCK") {
                    vm.userGridOptions.columnDefs = vm.closestockColumnDefs;
                }
                else if (report == "RATEVIEW") {
                    vm.userGridOptions.columnDefs = vm.rateViewColumnDefs;
                }
                else if (report == "SUPPLIERRATEVIEW") {
                    vm.userGridOptions.columnDefs = vm.supplierRateViewColumnDefs;
                }
                else if (report == "PRODUCTANALYSIS") {
                    vm.viewProductAnalysis();
                }
                console.log("Execute GetAll");

                vm.getAll();
            };



            vm.viewProductAnalysis = function () {
                openView();
            };

            function openView() {
               
                $state.go("tenant.reportdetailproduct", {
                    //id: objId
                });
            }

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                      
                        console.log('Page Change ');
                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        console.log('Grid Change ' + requestParams.skipCount);
                        vm.getAll();

                    });
                },
                data: []
            };

            vm.reflocation = [];

            function fillDropDownLocation() {
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;
                    if (vm.defaultLocationId != null && vm.defaultLocationId > 0) {
                        vm.locationRefId = vm.defaultLocationId;
                    }
                });
            }

            vm.detailDataShown = function (data) {
                var argOption = 'Sales';

                var ledgerDetails = {
                    locationRefId: vm.defaultLocationId,
                    ledgerDate: vm.ledgerStartDate,
                    materialRefId: data.materialRefId,
                    materialRefName: data.materialName,
                    uom: data.uom,
                    sales: data.sales
                };

                var endDate = moment().format($scope.format);

                var modalInstance1 = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/materialledger/drillDownLevel.cshtml',
                    controller: 'tenant.views.house.master.materialledger.drillDownLevel as vm',
                    backdrop: 'static',
                    resolve: {
                        ledgerDetails: function () {
                            return ledgerDetails;
                        },
                        searchFor: function () {
                            return argOption;
                        },
                        startDate: function () {
                            return vm.ledgerStartDate;
                        },
                        endDate: function () {
                            return vm.ledgerEndDate;
                        },
                    }
                });
               
            }

            vm.checkVarianceAfterOrBefore = function () {
                if (vm.reporttorun == "VARIANCESTOCK") {
                    var tempDate = moment(vm.housetransactiondate, $scope.format);
                    if (vm.dateRangeModel.endDate >= tempDate) {
                        vm.reportSubHeader = app.localize('Before') + ' ' + app.localize('DayClose');
                    }
                    else {
                        vm.reportSubHeader = app.localize('After') + ' ' + app.localize('DayClose');
                    }
                }
            }

            vm.ledgerStartDate = null;
            vm.ledgerEndDate = null;

            vm.getAll = function () {
                if (vm.locationRefId == 0 || vm.locationRefId == null) {
                    abp.notify.info(app.localize('LocationErr'));
                    return;
                }
                if (vm.reportRefKey == 0 || vm.reportRefKey == null) {
                    abp.notify.info(app.localize('ReportNotSelected'));
                    return;
                }

                if (vm.dateRangeModel.startDate == null) {
                    vm.dateRangeModel.startDate = moment().format($scope.format);
                    vm.dateRangeModel.endDate = moment().format($scope.format);
                }

                vm.loading = true;

                if ($scope.reportKey == "STOCK") {

                   vm.userGridOptions.columnDefs = vm.stockColumnDefs;

                    housereportService.getStockSummary({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterText,
                        locationRefId: vm.locationRefId,
                        reportKey: $scope.reportKey,
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        isHighValueItemOnly: vm.isHighValueItemOnly,
                        materialTypeList: vm.materialTypeList,
                        materialName: vm.materialRefNameLike,
                        materialRefIds: vm.materialRefIds,
                        callfromIndex: !vm.isNeedToBeDisplayedInSingleSheet
                    }).success(function (result) {
                        vm.userGridOptions.totalItems = result.totalCount;
                        vm.userGridOptions.data = result.stockSummary;
                        vm.userGridOptions.columnDefs = vm.stockColumnDefs;
                        vm.daterangestring = result.dateRange;
                    }).finally(function () {
                        vm.loading = false;
                    });
                }
                else if ($scope.reportKey == "VARIANCESTOCK") {

                    vm.userGridOptions.columnDefs = vm.variancestockColumnDefs;
                    vm.checkVarianceAfterOrBefore();

                    housereportService.getStockSummary({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterText,
                        locationRefId: vm.locationRefId,
                        reportKey: $scope.reportKey,
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        isHighValueItemOnly: vm.isHighValueItemOnly,
                        materialTypeList: vm.materialTypeList,
                        materialName: vm.materialRefNameLike,
                        materialRefIds: vm.materialRefIds,
                        varianceAndTheoreticalUsageNeeded: true
                    }).success(function (result) {
                        vm.userGridOptions.totalItems = result.totalCount;
                        vm.userGridOptions.data = result.stockSummary;
                        vm.userGridOptions.columnDefs = vm.variancestockColumnDefs;
                        vm.daterangestring = result.dateRange;
                        vm.ledgerStartDate = result.startDate;
                        vm.ledgerEndDate = result.endDate;
                    }).finally(function () {
                        vm.loading = false;
                    });
                }
                else if ($scope.reportKey == "CLOSESTOCK") {
                    vm.userGridOptions.columnDefs = vm.closestockColumnDefs;

                    housereportService.getStockSummary({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterText,
                        locationRefId: vm.locationRefId,
                        reportKey: $scope.reportKey,
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        isHighValueItemOnly: vm.isHighValueItemOnly,
                        materialTypeList: vm.materialTypeList,
                        materialName: vm.materialRefNameLike,
                        materialRefIds: vm.materialRefIds,
                        excludeRateView: true,
                        callfromIndex: !vm.isNeedToBeDisplayedInSingleSheet
                    }).success(function (result) {
                        vm.userGridOptions.totalItems = result.totalCount;
                        vm.userGridOptions.data = result.stockSummary;
                        vm.userGridOptions.columnDefs = vm.closestockColumnDefs;
                    }).finally(function () {
                        vm.loading = false;
                    });
                }
                else if ($scope.reportKey == "RATEVIEW") {
                    housereportService.getMaterialRateView({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        locationRefId: vm.locationRefId,
                        reportKey: $scope.reportKey,
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        isHighValueItemOnly: vm.isHighValueItemOnly,
                        materialTypeList: vm.materialTypeList,
                        materialRefNameLike: vm.materialRefNameLike,
                        materialRefIds: vm.materialRefIds,
                        doesIngredientsDetailsRequired : true
                    }).success(function (result) {
                        vm.userGridOptions.totalItems = result.totalCount;
                        vm.userGridOptions.data = result.materialRateView;
                        vm.userGridOptions.columnDefs = vm.rateViewColumnDefs;
                    }).finally(function () {
                        vm.loading = false;
                    });
                }
                else if ($scope.reportKey == "SUPPLIERRATEVIEW") {
                    housereportService.getSupplierRateView({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterText,
                        locationRefId: vm.locationRefId,
                        reportKey: $scope.reportKey,
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        isHighValueItemOnly: vm.isHighValueItemOnly,
                        materialTypeList: vm.materialTypeList,
                        materialRefIds: vm.materialRefIds,
                    }).success(function (result) {
                        vm.userGridOptions.totalItems = result.totalCount;
                        vm.userGridOptions.data = result.supplierRateView;
                        vm.userGridOptions.columnDefs = vm.supplierRateViewColumnDefs;
                    }).finally(function () {
                        vm.loading = false;
                    });
                }

                vm.firstrun = false;
            };


            vm.batchFileCreation = function () {
                vm.loading = true;
                housereportService.apiPhysicalStockBatchFileForGivenDatePeriod({
                    locationRefId: vm.locationRefId,
                    stockDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                }).success(function (result) {
                    abp.notify.info('Exported Successfully');
                    vm.loading = false;
                });
            }

            vm.exportToPdf = function () {
                if (vm.reportRefKey == "RATEVIEW") {
                    vm.loading = true;
                    housereportService.rateViewToExcel({
                        materialRateView: vm.userGridOptions.data,
                        exportType: 2
                    }).success(function (result) {
                        app.downloadTempFileOnly(result);
                        vm.loading = false;
                    });
                }
            }

            vm.ingrdetailDataShown = function (data) {
                var modalInstance1 = $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/housereport/ingrPriceListledgerModal.cshtml',
                    controller: 'tenant.views.house.master.housereport.ingrPriceListledgerModal as vm',
                    backdrop: 'static',
                    resolve: {
                        priceListdetails: function () {
                            return data.materialIngredientRequiredDtos;
                        },
                        materialRefId: function () {
                            return null;
                        },
                        materialRefName: function () {
                            return data.materialName;
                        },
                    }
                });
            }


            vm.pdfFileExpected = false;

            vm.exportToExcel = function () {
                if (vm.isNeedToBeDisplayedInSingleSheet == false && (vm.reportRefKey == 'STOCK' || vm.reportRefKey == 'CLOSESTOCK')) {
                    abp.notify.warn("This Report do not export all Materials. If need all , kindly select " + app.localize('IsNeedToBeDisplayedInSingleSheet'));
                }

                if (vm.reportRefKey == "RATEVIEW") {
                    vm.loading = true;
                    housereportService.rateViewToExcel({
                        materialRateView: vm.userGridOptions.data
                    })
                        .success(function (result) {
                            app.downloadTempFile(result);
                            vm.loading = false;
                            vm.pdfFileExpected = false;
                        });
                }
                else if (vm.reportRefKey == "SUPPLIERRATEVIEW") {
                    vm.loading = true;
                    housereportService.supplierRateViewToExcel({
                        supplierRateView: vm.userGridOptions.data
                    })
                        .success(function (result) {
                            app.downloadTempFile(result);
                            vm.loading = false;
                            vm.pdfFileExpected = false;
                        });
                }
                else if (vm.reportRefKey == "CLOSESTOCK") {
                    vm.loading = true;
                    housereportService.getCloseStockToExcel({ stockSummary: vm.userGridOptions.data })
                        .success(function (result) {
                            app.downloadTempFile(result);
                            vm.loading = false;
                            vm.pdfFileExpected = false;
                        });
                }
                else if (vm.reportRefKey == "VARIANCESTOCK") {
                    vm.loading = true;
                    var exportType = 0;
                    if (vm.pdfFileExpected)
                        exportType = 2;

                    housereportService.getClosingStockVarianceBeforeOrAfterDayCloseReportExcel({
                        skipCount: 0,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterText,
                        locationRefId: vm.locationRefId,
                        reportKey: $scope.reportKey,
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        isHighValueItemOnly: vm.isHighValueItemOnly,
                        materialTypeList: vm.materialTypeList,
                        materialName: vm.materialRefNameLike,
                        materialRefIds: vm.materialRefIds,
                        varianceAndTheoreticalUsageNeeded: true,
                        exportType: exportType
                    }).success(function (result) {
                        app.downloadTempFile(result.excelFile);
                    }).finally(function () {
                        vm.loading = false;
                        vm.pdfFileExpected = false;
                    });
                }
                else if (vm.reportRefKey == "STOCK") {
                    vm.loading = true;
                    housereportService.getStockSummary({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterText,
                        locationRefId: vm.locationRefId,
                        reportKey: $scope.reportKey,
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        isHighValueItemOnly: vm.isHighValueItemOnly,
                        materialTypeList: vm.materialTypeList,
                        materialName: vm.materialRefNameLike,
                        materialRefIds: vm.materialRefIds,
                        callfromIndex: !vm.isNeedToBeDisplayedInSingleSheet
                    }).success(function (result) {
                        vm.userGridOptions.totalItems = result.totalCount;
                        vm.userGridOptions.data = result.stockSummary;
                        vm.userGridOptions.columnDefs = vm.stockColumnDefs;
                        vm.daterangestring = result.dateRange;
                        vm.loading = true;
                        housereportService.getStockToExcel({
                            stockSummary: vm.userGridOptions.data,
                            dateRange: vm.daterangestring,
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            isNeedToBeDisplayedInSingleSheet: vm.isNeedToBeDisplayedInSingleSheet
                        })
                            .success(function (result) {
                                app.downloadTempFile(result);
                            }).finally(function () {
                                vm.loading = false;
                                vm.pdfFileExpected = false;
                            });

                        //vm.userGridOptions.api.setColumnDefs();
                    }).finally(function () {
                        vm.loading = false;
                        vm.pdfFileExpected = false;
                    });


                }
            };

            fillDropDownLocation();
            if (vm.reporttorun != null) {
                $scope.reportKey = vm.reporttorun;
                vm.reportRefKey = vm.reporttorun;

                if (vm.reporttorun == "RATEVIEW") {
                    //  Do not fetch initial   
                }
                else if (vm.reporttorun == "SUPPLIERRATEVIEW") {
                    //  Do not fetch initial
                }
                else if (vm.reporttorun == "VARIANCESTOCK") {
                    //  Do not fetch initial   
                }
                else if (vm.reporttorun == "CLOSESTOCK") {
                    //  Do not fetch initial   
                }
                else
                    vm.executeReport(vm.reporttorun);
            }



            //vm.getLocations();
        }])

        .filter('fractionFilter', function () {
            return function (value) {
                return value.toFixed(3);
            };
        })


})();

