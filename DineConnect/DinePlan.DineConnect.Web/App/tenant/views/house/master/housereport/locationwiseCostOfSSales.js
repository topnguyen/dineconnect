﻿(function () {
    appModule.controller('tenant.views.house.master.housereport.locationwiseCostOfSSales', [
        '$scope', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.houseReport', 'appSession', 'abp.services.app.location',
        function ($scope, $state, $stateParams, $modal, uiGridConstants, housereportService, appSession, locationService) {
            var vm = this;
            var nonFilteredData = [];

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.locations = [];
            vm.materialcostingflag = $stateParams.materialcostingflag;
            if (vm.materialcostingflag == "true" || vm.materialcostingflag == true)
                vm.materialcostingflag = true;
            else
                vm.materialcostingflag = false;

            vm.categorycostingflag = $stateParams.categorycostingflag;
            if (vm.categorycostingflag == "true" || vm.categorycostingflag == true)
                vm.categorycostingflag = true;
            else
                vm.categorycostingflag = false;


            vm.requestParams = {
                locations: [],
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.getLocations = function () {
                locationService.getAll({
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                    //angular.forEach(vm.locations, function (value, key) {
                    //    if (value.id == vm.defaultLocationId)
                    //        vm.requestParams.locations.push(value);
                    //});

                }).finally(function (result) {
                });
            };


            vm.costingReport = function ()
            {
                vm.locationlist = vm.requestParams.locations;

                if (vm.locationlist==null|| vm.locationlist.length == 0)
                {
                    vm.locationlist = vm.locations;
                    abp.notify.warn(app.localize('LocationErr'));
                    return;
                }
                vm.loading = true;
                housereportService.getCostOfSalesReport({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locations: vm.locationlist
                }).success(function (result) {
                    vm.output = result;
                }).finally(function () {
                    vm.loading = false;
                });
            };


            vm.exportToExcel = function () {
                housereportService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };



            vm.getLocations();

        }]);
})();

