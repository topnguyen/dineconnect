﻿(function () {
    appModule.controller('tenant.views.house.master.housereport.productAnalysis', [
        '$scope', '$state',  '$uibModal', 'uiGridConstants', 'abp.services.app.houseReport', 'appSession', 
        function ($scope, $state, $modal, uiGridConstants, housereportService, appSession) {
            var vm = this;
            var nonFilteredData = [];

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.cancel = function () {
                //$modalInstance.dismiss();
            };

            var todayAsString = moment().format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                //paginationPageSizes: app.consts.grid.defaultPageSizes,
                //paginationPageSize:  app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
	             {
	                 name: 'Actions',
	                 enableSorting: false,
	                 width: 50,
	                 headerCellTemplate: '<span></span>',
	                 cellTemplate:
                         '<div class=\"ui-grid-cell-contents text-center\">' +
                             '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.viewEntry(row.entity)"><i class="fa fa-search"></i></button>' +
                             '</div>'
	             },
                {
                    name: app.localize('Material'),
                    field: 'materialName',
                    minWidth : 220
                },
                {
                    name: app.localize('Qty'),
                    field: 'totalQty',
                    cellClass: 'ui-ralign',
                    cellFilter: 'fractionFilter'
                },
             
                {
                    name: app.localize('AvgPrice'),
										field: 'avgPrice',
                    cellClass : function (grid, row) {
                    	if (row.entity.avgPrice.toFixed(2) != row.entity.minPrice.toFixed(2))
                    		return 'ui-red ui-ralign';
                    	else
                    		return 'ui-ralign';
										},
										cellFilter: 'currencyFractionFilter',
                },
                {
                    name: app.localize('Amount'),
                    field: 'totalAmount',
                    cellClass: 'ui-ralign',
                    cellFilter: 'currencyFractionFilter'
                },
                {
                    name: app.localize('MinPrice'),
                    field: 'minPrice',
                    cellClass: 'ui-ralign',
                    cellFilter: 'currencyFractionFilter'
                },
                {
                    name: app.localize('MaxPrice'),
                    field: 'maxPrice',
                    cellClass: 'ui-ralign',
                    cellFilter: 'currencyFractionFilter'
                },
                {
                    name: app.localize('OpenBalance'),
                    field: 'openBalance',
                    cellClass: 'ui-ralign',
                    cellFilter: 'fractionFilter'
                },
                {
                    name: app.localize('Receipt'),
                    field: 'receipt',
                    cellClass: 'ui-ralign',
                    cellFilter: 'fractionFilter'
                },
                {
                    name: app.localize('Issue'),
                    field: 'issue',
                    cellClass: 'ui-ralign',
                    cellFilter: 'fractionFilter'
                },
                {
                    name: app.localize('ClBalance'),
                    field: 'clBalance',
                    cellClass: 'ui-ralign',
                    cellFilter: 'fractionFilter'
                }

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        //vm.getAll();
                    });
                },
                data: []
            };

            vm.isHighValueItemOnly = false;

            vm.getAll = function ()
            {
                vm.loading = true;
                housereportService.getProductAnalysisView({
                    sorting: requestParams.sorting,
                    materialName: vm.filterText,
                    locationRefId: vm.defaultLocationId,
                    reportKey: $scope.reportKey,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    isHighValueItemOnly: vm.isHighValueItemOnly
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.productAnalysis;
                    nonFilteredData = result.productAnalysis;
                    //vm.userGridOptions.api.setColumnDefs();
                }).finally(function () {
                    vm.loading = false;
                });
            };


            vm.exportToExcel = function () {
                housereportService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            //vm.getAll();

            vm.showDetails = function (row) {

                vm.supplierList = [];
                vm.ratelist = [];
                vm.diffprice = [];

                vm.minimumaxis = 0;


                angular.forEach(row.supplierAveragePrice, function (value, key) {
                    vm.supplierList.push(value.supplierName);
                    vm.ratelist.push(value.price);
                    vm.diffprice.push(value.price - value.materialAveragePrice);
                    if (vm.minimumaxis > (value.price - value.materialAveragePrice))
                        vm.minimumaxis = (value.price - value.materialAveragePrice);
                });
                vm.minimumaxis = Math.floor(vm.minimumaxis) - 1;

                $('#container').highcharts({
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Supplier Wise Price Difference'
                    },
                    xAxis: {
                        categories: vm.supplierList , 
                        title: {
                            text: null
                        }
                    },
                    yAxis: {
                        min: vm.minimumaxis,
                        title: {
                            text: 'Price',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        valueSuffix: ' '
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -40,
                        y: 80,
                        floating: true,
                        borderWidth: 1,
                        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                        shadow: true
                    },
                    credits: {
                        enabled: false
                    },
                    series: [
                        {
                        name: 'Price List',
                        data:  vm.ratelist 
                        },
                        {
                            name: 'Diff With Average Price',
                            data: vm.diffprice
                        },
               
                    ]
                });

            }



            vm.viewEntry = function (item) {
                $modal.open({
                    templateUrl: '~/App/tenant/views/house/master/housereport/viewGraph.cshtml',
                    controller: 'tenant.views.house.master.housereport.viewGraph as vm',
                    resolve: {
                        materialId: function () {
                            return item.materialRefId;
                        },
                        allItems: function () {
                            return vm.userGridOptions.data;
                        },
                        initialText: function () {
                            return item;
                        }
                    }
                });
            };


            vm.applyFilters = function () {
                if (!vm.filterText) {
                    vm.userGridOptions.data = nonFilteredData;
                }
                //vm.gridOptions.data = nonFilteredData;
                //return;

                var filterText = vm.filterText.trim().toLocaleLowerCase();
                vm.userGridOptions.data = _.filter(nonFilteredData, function (text) {
                    if (vm.targetValueFilter == 'EMPTY' && text.targetValue) {
                        return false;
                    }

                    return (text.materialName && text.materialName.toLocaleLowerCase().indexOf(filterText) >= 0);
                });
            };

            vm.getAllToExcel = function () {
            	vm.loading = true;
            	housereportService.getExcelOfProductAnalysis(
                   {
                   	productAnalysis: vm.userGridOptions.data,
                   	locationRefId: vm.defaultLocationId,
                   	startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                   	endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                   }
                    )
                    .success(function (result) {
                    	app.downloadTempFile(result);
                    	vm.loading = false;
                    });
            }


        }])

    .filter('currencyFractionFilter', function () {
    	return function (value) {
    		return value.toFixed(2);
    	};
    })

})();

