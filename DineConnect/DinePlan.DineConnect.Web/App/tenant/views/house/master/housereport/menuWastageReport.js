﻿(function () {
	appModule.controller('tenant.views.house.transaction.housereport.menuWastageReport', [
		'$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.location',  'appSession', 'abp.services.app.menuItemWastage', 
		function ($scope, $modal, uiGridConstants, locationService, appSession, menuitemwastageService) {
			var vm = this;
            /* eslint-disable */
			$scope.$on('$viewContentLoaded', function () {
				App.initAjax();
			});

			vm.loading = false;
			vm.filterText = null;
			vm.currentUserId = abp.session.userId;
			vm.defaultLocationId = appSession.user.locationRefId;
			vm.uilimit = 20;
			vm.locationsSelected = [];
			vm.alllocationflag = false;
			vm.compExistFlag = false;

			vm.clear = function () {
				vm.locationsSelected = [];
				vm.alllocationflag = false;
			}

			vm.alllocations = [];

			vm.getLocations = function () {
				locationService.getLocationBasedOnUser({
					userId: vm.currentUserId
				}).success(function (result) {
					vm.alllocations = $.parseJSON(JSON.stringify(result.items));
					angular.forEach(vm.alllocations, function (value, key) {
						if (angular.equals(value.id, vm.defaultLocationId)) {
							vm.locationsSelected.push(value);
							return;
						}
					});

				}).finally(function (result) {

				});
			};

			vm.getLocations();

			$scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
			$scope.format = $scope.formats[0];


			var todayAsString = moment().format('YYYY-MM-DD');
			vm.dateRangeOptions = app.createDateRangePickerOptions();
			vm.dateRangeModel = {
				startDate: todayAsString,
				endDate: todayAsString
            };

            vm.isCustomReportEnabled = abp.features.isEnabled('DinePlan.DineConnect.Feature.Custom');


	        vm.customReportexportToExcel = function () {
				if (vm.alllocationflag == true) {
					vm.locationsSelected = vm.alllocations;
				}
				if (vm.locationsSelected == null || vm.locationsSelected.length == 0) {
					abp.notify.error(app.localize('LocationErr'));
					return;
				}
                if (vm.locationsSelected == null || vm.locationsSelected.length > 1) {
                    abp.notify.error(app.localize('LocationErr'));
                    abp.notify.error('Only One Location Should be Selected for this report option');
                    return;
                }

				vm.loading = true;


				vm.loading = true;
				menuitemwastageService.getMenuWastageToExcel({
					toLocations: vm.locationsSelected,
					startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
					endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
					compExistFlag: vm.compExistFlag,
                    customReportFlag : true
				})
					.success(function (result) {
						app.downloadTempFile(result);
					}).finally(function (result) {
						vm.loading = false;
					});
			};

			vm.exportToExcel = function (type) {
				if (vm.alllocationflag == true) {
					vm.locationsSelected = vm.alllocations;
				}
				if (vm.locationsSelected == null || vm.locationsSelected.length == 0) {
					abp.notify.warn(app.localize('LocationErr'));
					return;
				}

				vm.loading = true;


				vm.loading = true;
				menuitemwastageService.getMenuWastageToExcel({
					toLocations: vm.locationsSelected,
					startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
					endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
					compExistFlag: vm.compExistFlag,
					exportType: type
				})
					.success(function (result) {
						app.downloadTempFile(result);
					}).finally(function (result) {
						vm.loading = false;
					});
			};


		}]);
})();

