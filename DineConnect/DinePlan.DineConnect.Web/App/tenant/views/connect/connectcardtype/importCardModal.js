﻿(function () {
    appModule.controller('tenant.views.connect.card.connectcardtype.importCard', [
        '$scope', 'appSession', '$uibModalInstance', 'FileUploader', 'cardTypeRefId',
        function ($scope, appSession, $uibModalInstance, fileUploader, cardTypeRefId) {
            var vm = this;
            vm.loading = false;
            vm.cardTypeRefId = cardTypeRefId;
            vm.uploader = new fileUploader({
                url: abp.appPath + 'Import/ImportConnectCards',
                formData: [
                    {
                        "connectCardTypeId": vm.cardTypeRefId
                    }
                ],
                queueLimit: 1,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        if ('|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ImportTemplate_Warn_FileType'));
                            return false;
                        }
                        return true;
                    }
                }]
            });

            vm.importpath = abp.appPath + 'Import/ImportConnectCardTemplate';

            vm.save = function () {
                if (vm.uploader.queue.length == 0) {
                    abp.notify.warn(app.localize('YouAreNotSelectAFile'));
                    return;
                }
                vm.loading = true;
                vm.uploader.uploadAll();
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.datafromexcel = [];

            vm.uploader.onErrorItem = function (fileItem, response, status, headers) {
                if (response.success) {
                    abp.notify.info(app.localize("Finished"));
                    vm.loading = false;
                    $uibModalInstance.close();
                } else {
                    abp.message.error(response);
                    abp.notify.error(response);
                    vm.loading = false;
                    vm.saving = false;
                    return;
                }
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.error != null) {
                    abp.message.warn(response.error.message);
                    $uibModalInstance.dismiss();
                    vm.loading = false;
                    return;
                }
                vm.allItems = [];

                abp.notify.info(app.localize("Finished"));
                vm.loading = false;
                $uibModalInstance.close();
            };
        }

    ]);
})();