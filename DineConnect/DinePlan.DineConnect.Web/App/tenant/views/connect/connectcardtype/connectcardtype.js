﻿(function () {
	appModule.controller('tenant.views.connect.card.connectcardtype.connectcardtype', [
		'$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.connectCardType', 'abp.services.app.connectCard', 'abp.services.app.connectCardTypeCategory',
		function ($scope, $state, $stateParams, $modal, connectCardTypeService, connectCardService, connectCardTypeCategory) {
			var vm = this;

			vm.saving = false;
			vm.connectCardType = null;
			vm.tabtwo = false;
			vm.connectCardTypeId = $stateParams.id;
			vm.autoActivationFlag = false;
			vm.uilimit = 20;
			$scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
			$scope.format = $scope.formats[0];
			vm.minDate = new Date();

			$('input[name="validity"]').daterangepicker({
				locale: {
					format: $scope.format
				},
				singleDatePicker: true,
				showDropdowns: true,
				//minDate: vm.minDate
			});
			vm.save = function (argOption) {
				if (vm.connectCardType.length == 0) {
					abp.notify.warn(app.localize('Length must be greater than 0'))
				}
				else if (vm.connectCardType.validTill < vm.minDate.format($scope.format)) {
					abp.notify.warn(app.localize('Validity is not valid'))
				} else {
					vm.saving = true;
					vm.connectCardType.name = vm.connectCardType.name.toUpperCase();

					connectCardTypeService.createOrUpdateConnectCardType({
						connectCardType: vm.connectCardType
					}).success(function (result) {
						vm.connectCardType.id = result.connectCardType.id;
						vm.connectCardTypeId = result.connectCardType.id;
						abp.notify.info(app.localize('SavedSuccessfully'));
						if (argOption == 1)
							vm.cancel();
						else
							init();
					}).finally(function () {
						vm.saving = false;
					});
				}
			};

			vm.cancel = function () {
				$state.go('tenant.connectcardtype');
			};

			vm.connectcardtypecategorys = [];
			vm.getVoucherCategories = function () {
				connectCardTypeCategory.getConnectCardTypeCategorys({}).success(function (result) {
					vm.connectcardtypecategorys = result.items;
				}).finally(function (result) {
				});
			};
			vm.getVoucherCategories();

			function init() {
				vm.loading = true;
				connectCardTypeService.getConnectCardTypeForEdit(vm.connectCardTypeId)
					.success(function (result) {
						vm.loading = false;
						vm.connectCardType = result.connectCardType;
						vm.cardList = result.cardList;

						if (vm.connectCardType.id !== null) {
							vm.uilimit = null;
							vm.connectCardType.validTill = moment(vm.connectCardType.validTill).format($scope.format);
						}
						else {
							vm.uilimit = 20;
							vm.connectCardType.validTill = moment(vm.minDate).format($scope.format);
						}
						$('input[name="validity"]').daterangepicker({
							locale: {
								format: $scope.format
							},
							singleDatePicker: true,
							showDropdowns: true,
							startDate: moment(vm.connectCardType.validTill),
							//minDate: moment(vm.connectCardType.creationTime)
						});
						vm.minDate = moment(vm.connectCardType.validTill);
					});
			}

			init();

			vm.cardTransactionDataShown = function (argOption) {
				var modalInstance1 = $modal.open({
					templateUrl: '~/App/host/views/swipe/transaction/membercard/drillDownLevel.cshtml',
					controller: 'host.views.swipe.transaction.membercard.drillDownLevel as vm',
					backdrop: 'static',
					resolve: {
						cardNumber: function () {
							return '';
						},
						cardRefId: function () {
							return argOption;
						}
					}
				});

				modalInstance1.result.then(function (result) {
					//vm.getAll();
				});
			};

			vm.import = function () {
				importModal(null);
			};

			function importModal() {
				var modalInstance = $modal.open({
					templateUrl: '~/App/tenant/views/connect/connectCardType/importCardModal.cshtml',
					controller: 'tenant.views.connect.card.connectcardtype.importCard as vm',
					backdrop: 'static',
					resolve: {
						cardTypeRefId: function () {
							return vm.connectCardType.id;
						}
					}
				});

				modalInstance.result.then(function (result) {
					init();
				});
			}

			vm.generateCards = function () {
				if (vm.cardStartingNumber == null || vm.cardStartingNumber == 0) {
					abp.notify.error(app.localize('CardStaringNumber') + "?");
					return;
				}

				if (vm.cardCount == null || vm.cardCount == 0) {
					abp.notify.error(app.localize('CardCount') + "?");
					return;
				}

				if (vm.cardNumberOfDigits == null || vm.cardNumberOfDigits == 0) {
					abp.notify.error(app.localize('CardNumberOfDigits') + "?");
					return;
				}

				connectCardService.generateConnectCards({
					connectCardTypeId: vm.connectCardType.id,
					prefix: vm.prefix,
					suffix: vm.suffix,
					cardNumberOfDigits: vm.cardNumberOfDigits,
					cardStartingNumber: vm.cardStartingNumber,
					cardCount: vm.cardCount
				}).success(function (result) {
					abp.notify.info(vm.cardCount + ' ' + app.localize('CardsAreBeingGeneratedInBackground'));
					vm.connectCardType.id = null;
					vm.prefix = null;
					vm.suffix = null;
					vm.cardNumberOfDigits = null;
					vm.cardStartingNumber = null;
					vm.cardCount = null;
				}).finally(function () {
					vm.tabtwo = true;
					vm.cancel();
				});
			};
		}
	]);
})();