﻿(function () {
    appModule.controller('tenant.views.connect.importsetting.index',
        ['$rootScope', '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.importSetting',
            function ($rootScope, $scope, $modal, uiGridConstants, service) {
                var vm = this;
                $rootScope.settings.layout.pageSidebarClosed = false;

                $scope.$on('$viewContentLoaded', function () {
                    App.initAjax();
                });

                vm.loading = false;

                var requestParams = {
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null
                };
                vm.permissions = {
                    'delete': abp.auth.hasPermission("Pages.Tenant.Connect.ImportSetting")
                };

                vm.gridOptions = {
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                    columnDefs: [
                        {
                            name: app.localize('Actions'),
                            enableSorting: false,
                            width: 120,
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents\">' +
                                '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                                '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                                '    <ul uib-dropdown-menu>' +
                                '      <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.delete" ng-click="grid.appScope.deleteImportSetting(row.entity)">' + app.localize('Delete') + '</a></li>' +
                                '    </ul>' +
                                '  </div>' +
                                '</div>'
                        },
                        {
                            name: app.localize('Id'),
                            field: 'id',
                            width: 120
                        },
                        {
                            name: app.localize('FilePath'),
                            field: 'filePath',
                            width: '*'
                        },
                        {
                            name: app.localize('IsImported'),
                            field: 'isImported',
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents\">' +
                                '  <input id="MergeLines" class="md-check" type="checkbox" ng-model="row.entity.isImported" onclick="return false;">' +
                                '</div>',
                            width: 120
                        }
                    ],

                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                            debugger
                            if (!sortColumns.length || !sortColumns[0].field) {
                                requestParams.sorting = null;
                            } else {
                                var sortString = '';
                                sortColumns.forEach(col => {
                                    sortString += col.field + ' ' + col.sort.direction + ', ';
                                });
                                requestParams.sorting = sortString.slice(0, -2);
                            }

                            vm.getAll();
                        });
                        gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                            requestParams.skipCount = (pageNumber - 1) * pageSize;
                            requestParams.maxResultCount = pageSize;

                            vm.getAll();
                        });
                    },
                    data: []
                };

                vm.createImportSetting = function () {
                    var modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/connect/importsetting/addnew.cshtml',
                        controller: 'tenant.views.connect.importsetting.addnew as vm',
                        backdrop: 'static',
                        keyboard: false,
                        resolve: {
                        }
                    });

                    modalInstance.result.then(function (result) {
                        debugger
                        vm.getAll();
                    });
                };

                vm.getAll = function () {
                    vm.loading = true;
                    service.getAll({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        location: vm.location,
                        locationGroup: vm.locationGroup
                    }).success(function (result) {
                        vm.gridOptions.totalItems = result.totalCount;
                        vm.gridOptions.data = result.items;
                    }).finally(function () {
                        vm.loading = false;
                    });
                };
                vm.clear = function () {
                    vm.location = null;
                    vm.currentLocation = null;
                    vm.filterText = null;
                    vm.intiailizeOpenLocation();

                    vm.getAll();
                };
                vm.deleteImportSetting = function (myObject) {
                    abp.message.confirm(
                        app.localize('DeleteImportSettingWarning', myObject.id),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                service.deleteImportSetting({
                                    id: myObject.id
                                }).success(function () {
                                    vm.getAll();
                                    abp.notify.success(app.localize('SuccessfullyDeleted'));
                                });
                            }
                        }
                    );
                };

                vm.locations = [];
                vm.currentLocation = null;
                vm.intiailizeOpenLocation = function() {
                    vm.locationGroup = app.createLocationInputForSearch();
                };
                vm.intiailizeOpenLocation();

                vm.openLocation = function () {

                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        if (result.locations.length > 0) {
                            vm.currentLocation = result.locations[0];
                            vm.locations = result.locations;
                            vm.location = result.locations[0].name;
                        }
                        else if (result.groups.length > 0) {
                            vm.currentLocation = result.groups[0];
                            vm.location = result.groups[0].name;
                        }
                        else if (result.locationTags.length > 0) {
                            vm.currentLocation = result.locationTags[0];
                            vm.location = result.locationTags[0].name;
                        }
                        else {
                            vm.location = null;
                            vm.currentLocation = null;
                        }
                    });
                };

                vm.getAll();
            }]);
})();