﻿(function () {
    appModule.controller('tenant.views.connect.importsetting.addnew', [
        '$scope', '$uibModal', 'abp.services.app.importSetting', 'FileUploader', '$uibModalInstance',
        function ($scope, $modal, importSettingService, fileUploader, $modalInstance) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.importSetting = { id: null, filePath: null };

            vm.save = function () {
                vm.saving = true;
                var file = vm.uploader.queue[vm.uploader.queue.length - 1];
                file.upload();
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadFile'
            });

            vm.uploader.filters.push({
                name: 'imageFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    return true;
                }
            });

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response !== null) {
                    if (response.result !== null) {
                        debugger
                        if (response.result.fileName !== null) {
                            vm.importSetting.filePath = response.result.fileSystemName;
                            vm.saveToDB();
                        }
                    }
                }
                $modalInstance.close();
            };

            vm.saveToDB = function () {
                importSettingService.createOrUpdateImportSetting({
                    importSetting: vm.importSetting,
                    locationGroup: vm.locationGroup
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                }).finally(function () {
                    vm.saving = false;
                    $modalInstance.close();
                });
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForCreate();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            }
        }
    ]);
})();