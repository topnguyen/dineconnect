﻿(function() {
    appModule.controller("tenant.views.connect.menu.department.sortitem", [
        "$scope", "$uibModalInstance", "abp.services.app.department", 
        function ($scope, $modalInstance, departmentService) {

            var vm = this;
            vm.saving = false;

            vm.init = function () {
                vm.loading = true;
                departmentService.getDepartments({
                }).success(function (result) {
                    vm.category=result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.save = function () {
                vm.loading = true;
                vm.cateIds = [];
                angular.forEach(vm.category, function(item) {
                    vm.cateIds.push(item.id);
                });
                departmentService.saveSortSortItems(
                   vm.cateIds
                ).success(function (result) {
                    $modalInstance.close();
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.cancel = function() {
                $modalInstance.dismiss();
            };

            vm.init();

        }
    ]);
})();