﻿(function () {
    appModule.controller('tenant.views.connect.department.create', [
        '$scope', '$state', '$uibModal', '$stateParams', 'abp.services.app.department', "abp.services.app.location", "abp.services.app.connectLookup",
        function ($scope, $state, $modal, $stateParams, departmentService, locationService, lookupService) {
            var vm = this;
            vm.departmentId = $stateParams.id;
            vm.uilimit = 20;
            vm.saving = false;
            vm.department = null;
            vm.languageDescriptionType = 9;
            vm.ticketTypes = [];

            vm.save = function () {
                vm.saving = true;
                vm.department.code = vm.department.code.toUpperCase();
                vm.department.name = vm.department.name.toUpperCase();
                departmentService.createOrUpdateDepartment({
                    department: vm.department,
                    locationGroup: vm.locationGroup
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.department');
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
            vm.requestlocations = "";

            vm.openLocation = function() {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function() {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function(result) {
                    vm.locationGroup = result;
                });
            };

            vm.pricetags = [];
            vm.getTags = function () {
                lookupService.getPriceTags({}).success(function (result) {
                    console.log(result);
                    vm.pricetags = $.parseJSON(JSON.stringify(result.items));
                }).finally(function (result) {
                });
            };
            vm.getTags();

            vm.departmentgroups = [];
            vm.getDepartmentGroups = function () {
                departmentService.getDepartmentGroups({}).success(function (result) {
                    vm.departmentgroups = result.items;
                }).finally(function (result) {
                });
            };
            vm.getDepartmentGroups();

            vm.openLanguageDescriptionModal = function() {
                vm.languageDescription = {
                    id: vm.departmentId,
                    name: vm.department.name,
                    languageDescriptionType: vm.languageDescriptionType
                };
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/languageDescription/languageDescriptionModal.cshtml",
                    controller: "tenant.views.connect.languageDescription.languageDescriptionModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        languagedescription: function() {
                            return vm.languageDescription;
                        }
                    }
                });

                modalInstance.result.then(function(result) {
                    init();
                });
            };

            function init() {
                lookupService.getTicketTypes({}).success(function (result) {
                    vm.ticketTypes = result.items;
                    vm.ticketTypes.unshift({ value: "0", displayText: app.localize("NotAssigned") });
                });

                departmentService.getDepartmentForEdit({
                    Id: vm.departmentId
                }).success(function (result) {
                    vm.department = result.department;
                    vm.locationGroup = result.locationGroup;
                    if (vm.department.id == null) {
                        vm.uilimit = 20;
                    }
                    else {
                        vm.uilimit = null;
                    }
                });
            }

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };

            init();
        }
    ]);
})();