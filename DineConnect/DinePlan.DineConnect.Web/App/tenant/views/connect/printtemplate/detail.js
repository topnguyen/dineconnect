﻿(function () {
    appModule.controller('tenant.views.connect.printtemplate.detail', [
        '$scope', "$uibModal", 'uiGridConstants', '$state', '$stateParams', 'abp.services.app.printTemplate', 'abp.services.app.printConfiguration','FileUploader',
        function ($scope, $modal, uiGridConstants, $state, $stateParams, printTemplateService, printConfigurationService,fileUploader) {
            var vm = this;
            vm.printTemplateId = $stateParams.id;
            vm.printTemplate = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };

            vm.save = function () {
                if (vm.isUndefinedOrNull(vm.printConfiguration.name)) {
                    abp.notify.error('Name is mandatory');
                    return;
                }
                vm.printConfiguration.printConfigurationType = 1;
                vm.saving = true;
                printConfigurationService.createOrUpdatePrintConfiguration({
                    printConfiguration: vm.printConfiguration
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.printtemplate');
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForCreate();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.openFilterLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.filterLocationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.filterLocationGroup = result;
                    init();
                });
            };

            vm.printtemplateOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Operations"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.removePortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Delete") + "\"><i class=\"fa fa-trash\"></i></button>" +
                            "  <button ng-click=\"grid.appScope.editPortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Edit") + "\"><i class=\"fa fa-edit\"></i></button>" +
                            "</div>"
                    },
                    {
                        name: app.localize("AliasName"),
                        field: "aliasName"
                    },
                    {
                        name: app.localize("MergeLines"),
                        field: "mergeLines",
                        width: 150,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.mergeLines" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.mergeLines" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                    },
                    {
                        name: app.localize("View"),
                        width: 100,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.viewPortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("View") + "\"><i class=\"fa fa-eye\"></i></button>" +
                            "</div>"
                    },
                    {
                        name: app.localize("Clone"),
                        width: 100,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.clonePortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Clone") + "\"><i class=\"fa fa-clone\"></i></button>" +
                            "</div>"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        init();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        init();
                    });
                },
                data: []
            };

            vm.removePortion = function (myObject) {
                abp.message.confirm(
                    app.localize('Are You Sure Want to delete the record ', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            printTemplateService.deletePrinterTemplate({
                                id: myObject.id
                            }).success(function (result) {
                                abp.notify.info(app.localize('Deleted'));
                                init();
                            }).finally(function () {
                                vm.saving = false;
                            });
                        }
                    }
                );
            };
            vm.tabtwo = false;
            vm.editPortion = function (myObject) {
                vm.printTemplate = myObject;
                vm.replyFiles = angular.fromJson(vm.printTemplate.files);
                vm.uploader.queue = [];
                myObject.locationGroup.viewOnly = false;
                vm.locationGroup = myObject.locationGroup;
                vm.tabtwo = true;
            }
            vm.clonePortion = function (myObject) {
                myObject.id = null;
                vm.printTemplate = myObject;
                vm.replyFiles = angular.fromJson(vm.printTemplate.files);
                vm.uploader.queue = [];
                vm.intiailizeOpenLocation();
                vm.tabtwo = true;
            }
            vm.viewPortion = function (myObject) {
                myObject.locationGroup.viewOnly = true;
                vm.locationGroup = myObject.locationGroup;
                vm.openLocation();
                vm.intiailizeOpenLocation();
            };
            vm.savePortion = function () {
                if (vm.isUndefinedOrNull(vm.printTemplate.contents))
                {
                    abp.notify.error('Contents is mandatory');
                    return;
                }
                vm.printTemplate.printConfigurationId = vm.printConfiguration.id;
                vm.printTemplate.locationGroup = vm.locationGroup;
                vm.printTemplate.files = angular.toJson(vm.replyFiles);
                vm.uploader.queue = [];
                printTemplateService.addOrEditPrinterTemplate({
                    printTemplate: vm.printTemplate
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                }).finally(function () {
                    vm.tabone = true;
                    vm.intiailizeOpenLocation();
                    init();
                    vm.saving = false;
                });
            }

            function init() {
                vm.saving = true;
                printTemplateService.getPrintTemplateForEdit({
                     id: vm.printTemplateId,
                     LocationGroupToBeFiltered: vm.filterLocationGroup
                }).success(function (result) {
                    vm.printConfiguration = result.printConfiguration;
                    vm.printTemplate = result.printTemplate;
                    vm.printtemplateOptions.data = vm.printConfiguration.printTemplates;
                    vm.printtemplateOptions.totalItems = vm.printConfiguration.printTemplates.length;
                    vm.replyFiles = vm.printTemplate.files ? angular.fromJson(vm.printTemplate.files) : [];

                    vm.saving = false;
                });

            }

            init();
            vm.clearFilterLocation = function () {
                vm.filterLocationGroup = app.createLocationInputForCreate();
                init();
            }

            vm.clearFilterLocation();


            // #region Uploader

            vm.refreshImage = false;


            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadFile',
                queueLimit : 1
            });
            vm.uploader.onAfterAddingFile = function(item) {
                console.log(item.file.size);

                if (item.file.size > 6000000) {
                    abp.notify.error(app.localize('Size is Big'));
                    vm.uploader.clearQueue();
                } else {
                    item.upload();
                }
            };
            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (vm.replyFiles === null) {
                    vm.replyFiles = [];
                }
                if (response !== null) {
                    if (response.result !== null) {
                        if (response.result.fileName !== null) {
                            vm.replyFiles.push(response.result);
                            vm.refreshImage = true;

                            vm.uploader.queue = [];
                        }
                    }
                }
            };
            vm.downloader = function (file) {
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + file.fileName+'&url='+file.fileSystemName;
            };
            vm.removeFile = function (findex) {
                vm.replyFiles.splice(findex, 1);
                vm.refreshImage = true;
            };

            // #endregion

        }
    ]);
})();