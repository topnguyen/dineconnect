﻿
(function () {
    appModule.controller('tenant.views.connect.master.transactiontype.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.transactionType', 'transactiontypeId',
        function ($scope, $modalInstance, transactiontypeService, transactiontypeId) {
            var vm = this;

            vm.saving = false;
            vm.transactiontype = null;
            vm.save = function () {
                vm.saving = true;
                transactiontypeService.createOrUpdateTransactionType({
                    transactionType: vm.transactiontype
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $modalInstance.dismiss();
            };
            function init() {
                transactiontypeService.getTransactionTypeForEdit({
                    id: transactiontypeId
                }).success(function (result) {
                    vm.transactiontype = result.transactionType;
                });
            }
            init();
        }
    ]);
})();

