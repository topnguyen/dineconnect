﻿
(function () {
    appModule.controller('tenant.views.connect.menu.promotioncategory.detail', [
        '$scope', '$state', '$stateParams', "$uibModal",'abp.services.app.promotionCategory', 
        function ($scope, $state, $stateParams, $modal,  promotioncategoryService) {
            var vm = this;

            vm.saving = false;
            vm.promotioncategory = null;
            vm.promotioncategoryId = $stateParams.id;

            vm.save = function () {
                vm.saving = true;
              

                promotioncategoryService.createOrUpdatePromotionCategory({
                    promotionCategory: vm.promotioncategory
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $state.go('tenant.promotioncategory');
            };

           

            function init() {
                promotioncategoryService.getPromotionCategoryForEdit({
                    id: vm.promotioncategoryId
                }).success(function (result) {
                    vm.promotioncategory = result.promotionCategory;
                });
            }
            init();
        }
    ]);
})();

