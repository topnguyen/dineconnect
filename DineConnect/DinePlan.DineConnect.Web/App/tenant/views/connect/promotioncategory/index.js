﻿
(function () {
    appModule.controller('tenant.views.connect.menu.promotioncategory.index', [
        '$scope', '$state', "$uibModal", 'uiGridConstants', 'abp.services.app.promotionCategory',
        function ($scope, $state, $modal, uiGridConstants, promotioncategoryService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Connect.Menu.PromotionCategory.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Connect.Menu.PromotionCategory.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Connect.Menu.PromotionCategory.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editPromotionCategory(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deletePromotionCategory(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('PromotionCategoryCode'),
                        field: 'code'
                    },
                    {
                        name: app.localize('PromotionCategoryName'),
                        field: 'name'
                    },
                    {
                        name: app.localize('PromotionCategoryTag'),
                        field: 'tag'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };
            vm.currentLocation = null;
            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.clear = function () {
                vm.location = null;
                vm.currentLocation = null;
                vm.filterText = null;
                vm.isDeleted = false;
                vm.intiailizeOpenLocation();

                vm.getAll();
            };
            vm.getAll = function () {
                vm.loading = true;
                promotioncategoryService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    location: vm.location,
                    locationGroup: vm.locationGroup
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editPromotionCategory = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createPromotionCategory = function () {
                openCreateOrEditModal(null);
            };

            vm.deletePromotionCategory = function (myObject) {
                abp.message.confirm(
                    app.localize('DeletePromotionCategoryWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            promotioncategoryService.deletePromotionCategory({
                                id: myObject.id
                            }).success(function (result) {
                                if (result) {
                                    vm.getAll();
                                    abp.notify.success(app.localize('SuccessfullyDeleted'));
                                }
                                else {
                                    setTimeout(function () {
                                        abp.message.error(app.localize('CannotDeletePromotionCategory'));
                                    }, 500);                                   
                                }                    
                            });
                        }
                    }
                );
            };          

            function openCreateOrEditModal(objId) {
                $state.go("tenant.detailpromotioncategory", {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                promotioncategoryService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.getAll();
        }]);
})();

