﻿(function () {
    appModule.controller('tenant.views.connect.locationmenu.price', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.menuItem', 'abp.services.app.commonLookup', 'uiGridConstants',
        function ($scope, $state, $stateParams, $modal, menuService, commonService, uiGridConstants) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            var nonFilteredData = [];
            vm.locations = [];
            vm.locationId = parseInt($stateParams.location) || parseInt(0);
            vm.filterText = $stateParams.filterText || '';
            vm.currentLocation = "";

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.getLocationValue = function (item) {
                return parseInt(item.value);
            };

            vm.loading = false;

            function openCreateOrEditModal(objId) {
                if (objId == 0) {
                    abp.notify.info(app.localize('NoLocation'));
                    return;
                }
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/locationmenu/priceModal.cshtml',
                    controller: 'tenant.views.connect.locationmenu.priceModal as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        locationId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.loadMenu();
                });
            }

            vm.importPrices = function () {
                openCreateOrEditModal(vm.locationId);
            };
            vm.gridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize('Id'),
                        field: 'id',
                        minWidth: 70
                    },
                    {
                        name: app.localize('MenuName'),
                        field: 'menuName',
                        minWidth: 140
                    },
                    {
                        name: app.localize('PortionName'),
                        field: 'portionName',
                        minWidth: 140
                    }
                    ,
                    {
                        name: app.localize('Price'),
                        field: 'price',
                        minWidth: 140
                    }
                    ,
                    {
                        name: app.localize('LocationPrice'),
                        field: 'locationPrice',
                        minWidth: 140
                    },
                    {
                        name: app.localize('Edit'),
                        width: 60,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.openTextEditModal(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>' +
                            '</div>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }

                        vm.loadMenu();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.loadMenu();
                    });
                },
                data: []
            };

            vm.openTextEditModal = function (text) {
                $modal.open({
                    templateUrl: '~/App/tenant/views/connect/locationmenu/editPriceModal.cshtml',
                    controller: 'tenant.views.connect.locationmenu.editPriceModal as vm',
                    resolve: {
                        locationId: function () {
                            return vm.locationId;
                        },
                        allItems: function () {
                            return vm.gridOptions.data;
                        },
                        initialText: function () {
                            return text;
                        }
                    }
                });
            };

            vm.loadMenu = function () {
                if (vm.locationId > 0) {
                    vm.loading = true;
                    menuService.getLocationMenuItemPrices({
                        filter: vm.filterText,
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        locationId: vm.locationId
                    }).success(function (result) {
                        vm.gridOptions.totalItems = result.totalCount;
                        vm.gridOptions.data = result.items;
                    }).finally(function () {
                        vm.loading = false;
                    });
                }
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForCreateSingleLocation();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.locationId = result.locations[0].id;
                        vm.currentLocation = result.locations[0].name;
                    }
                });
            };

            function initializeFilters() {
                commonService.getLocationsForLoginUser({}).success(function (result) {
                    vm.locations = result.items;
                });

                function reloadWhenChange(variableName) {
                    $scope.$watch(variableName, function (newValue, oldValue) {
                        if (newValue == oldValue) {
                            return;
                        }
                        $state.go('tenant.locationmenuprice', {
                            location: vm.locationId,
                            filterText: vm.filterText
                        }
                        );
                    });
                }
                reloadWhenChange('vm.locationId');
            }

            vm.loadMenu();
            initializeFilters();
        }
    ]);
})();