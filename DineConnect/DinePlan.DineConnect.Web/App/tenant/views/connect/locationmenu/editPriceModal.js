﻿(function () {
    appModule.controller('tenant.views.connect.locationmenu.editPriceModal', [
       '$scope', '$uibModalInstance', 'abp.services.app.menuItem', 'locationId', 'allItems', 'initialText',
        function ($scope, $modalInstance, menuService, locationId, allItems, initialText) {
            var vm = this;

            vm.allItems = allItems;
            vm.currentText = initialText;
            vm.editingText = angular.extend({}, vm.currentText);
            vm.locationId = locationId;

            vm.currentIndex = 0;
            vm.saving = false;

            //Calculating index of currentText in allItems
            for (var i = 0; i < allItems.length; i++) {
                if (vm.allItems[i] == vm.currentText) {
                    vm.currentIndex = i;
                    break;
                }
            }
            
            function save(close) {
                function executeAfterAction() {
                    if (close) {
                        $modalInstance.close();
                    } else {
                        //Go to next
                        vm.currentIndex++;
                        if (vm.allItems.length <= vm.currentIndex) {
                            $modalInstance.close();
                            return;
                        }

                        vm.currentText = vm.allItems[vm.currentIndex];
                        vm.editingText = angular.extend({}, vm.currentText);
                    }
                }

                if (vm.currentText.locationPrice == vm.editingText.locationPrice) {
                    executeAfterAction();
                    return;
                }

                vm.saving = true;
                menuService.updateLocationPrice([{
                    LocationId: vm.locationId,
                    Price: vm.editingText.locationPrice,
                    PortionId: vm.editingText.id
                }]).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.currentText.locationPrice = vm.editingText.locationPrice;
                    executeAfterAction();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.saveAndNext = function() {
                save(false);
            }

            vm.saveAndClose = function () {
                save(true);
            }

            vm.previous = function () {
                if (vm.currentIndex <= 0) {
                    return;
                }

                vm.currentIndex--;
                vm.currentText = vm.allItems[vm.currentIndex];
                vm.editingText = angular.extend({}, vm.currentText);
            };

            vm.targetValueKeyPress = function ($event) {
                if ($event.keyCode == 13) {
                    vm.saveAndNext();
                } else if ($event.keyCode == 37) {
                    vm.previous();
                } else if ($event.keyCode == 39) {
                    vm.saveAndNext();
                }
            }

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();