﻿(function () {
    appModule.controller('tenant.views.connect.locationmenu.itemModal', [
        '$scope', 'appSession', '$uibModalInstance', 'FileUploader', 'locationId', 'abp.services.app.menuItem',
        function ($scope, appSession, $uibModalInstance, fileUploader, locationId, menuItemService) {
            var vm = this;

            vm.uploader = new fileUploader({
                url: abp.appPath + 'Import/ImportItems?locationId='+locationId,
                queueLimit: 1,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        if ('|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ImportTemplate_Warn_FileType'));
                            return false;
                        }
                        return true;
                    }
                }]
            });
            vm.importpath = function () {
                vm.loading = true;
                menuItemService.getMenuItemsTemplate(locationId)
                    .success(function (result) {
                        vm.loading = false;
                        app.downloadTempFile(result);
                    });
            };

            vm.save = function () {
                vm.uploader.uploadAll();
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                abp.notify.info("Finished");
                $uibModalInstance.close();
            };
        }

    ]);
})();
