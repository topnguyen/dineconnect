﻿(function () {
    appModule.controller('tenant.views.connect.screenmenu.detail', [
        '$scope', '$state', '$stateParams',"uiGridConstants", "$uibModal", "abp.services.app.screenMenu", "abp.services.app.location", "abp.services.app.department",
        function ($scope, $state, $stateParams,uiGridConstants, $modal, screenmenuService, locationService, deptService) {
            var vm = this;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.screenMenuId = $stateParams.id;
            vm.futuredataId = $stateParams.futureDataId;
            vm.futureDataFlag = false;
            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };

            if (vm.isUndefinedOrNull(vm.futuredataId)) {
                vm.futureDataFlag = false;
            }
            else {
                vm.futureDataFlag = true;
            }

            vm.cancel = function () {
                $state.go("tenant.screenmenu");
            };

            vm.listCategories = function () {
                $state.go("tenant.screencategory", {
                    menuid: vm.screenMenuId,
                    futureDataId: vm.futuredataId
                });
            };

            vm.departments = [];
            vm.requestdepartments = "";

            vm.getDepartments = function () {
                deptService.getDepartmentsForCombobox({}).success(function (result) {
                    vm.departments = $.parseJSON(JSON.stringify(result.items));
                }).finally(function (result) {
                });
            };

            vm.save = function () {
                if ($scope.existall == false)
                    return;
                vm.saving = true;
                screenmenuService.createOrUpdateScreenMenu({
                    futureDataId: vm.futuredataId,
                    screenMenu: vm.screenmenu,
                    locationGroup: vm.locationGroup,
                    departments: vm.requestdepartments
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    if (!vm.isUndefinedOrNull(vm.futuredataId)) {
                        $state.go('tenant.futuredata');
                    }
                    else {
                        vm.cancel();
                    }
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {
                if (vm.screenmenu.od == null) {
                    vm.existall = false;
                    return;
                }

                screenmenuService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: "od",
                    filter: vm.screenmenu.od,
                    operation: "SEARCH"
                }).success(function (result) {
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.screenmenu.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info("' " + vm.screenmenu.id + "' " + app.localize("NameExist"));
                        $scope.existall = false;
                    }
                });
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            function init() {
                screenmenuService.getScreenMenuForEdit({
                    id: vm.screenMenuId,
                    futureDataId: vm.futuredataId,
                }).success(function (result) {
                    vm.screenmenu = result.screenMenu;
                    vm.requestdepartments = result.departments;
                    vm.locationGroup = result.screenMenu.locationGroup;
                });
            }

            init();
            vm.getDepartments();


            vm.openLocation = function() {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function() {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function(result) {
                    vm.locationGroup = result;
                });
            };

            //ScreenMenu Category
            vm.permissions = {
                create: abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ScreenMenu.Create"),
                edit: abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ScreenMenu.Edit"),
                'delete': abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ScreenMenu.Delete")
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Actions"),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editCategory(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteCategory(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize("Name"),
                        field: "name"
                    },
                    {
                        name: app.localize("MainButtonColor"),
                        field: "mainButtonColor"
                    },
                    {
                        name: app.localize("MenuItems"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.listmenuItems(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("List") + "\">" + app.localize("List") + "</button> <br/>" +
                            "</div>"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }

                        vm.getAllScreenCategory();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAllScreenCategory();
                    });
                },
                data: []
            };

            vm.listmenuItems = function (objId) {
                $state.go("tenant.screenmenuitem", {
                    categoryid: objId.id,
                    menuid: vm.menuId,
                    location: vm.location
                });
            };

            vm.editCategory = function (myObj) {
                $state.go("tenant.detailscreencategory", {
                    categoryid: myObj.id,
                    menuid: vm.menuId,
                    location: vm.location
                });
            };

            vm.deleteCategory = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteScreenCategoryWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            screenmenuService.deleteScreenCategory({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.getAllScreenCategory = function () {
                vm.loading = true;
                screenmenuService.getCategories({
                    menuid: vm.menuId,
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            function createScreenMenuItem(objId) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/screenmenu/menuitem/createModal.cshtml",
                    controller: "tenant.views.connect.menu.screenmenuitem.createmodal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        categoryId: function () {
                            return objId;
                        },
                        location: function () {
                            return vm.location;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.createMenuItem = function (myObj) {
                createScreenMenuItem(myObj.id);
            };

            function createCategory(objId) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/screenmenu/category/createCategory.cshtml",
                    controller: "tenant.views.connect.menu.screenmenu.createcategory as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        screenmenuId: function () {
                            return vm.menuId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }
            function sortCate(objId) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/screenmenu/category/sortcategory.cshtml",
                    controller: "tenant.views.connect.menu.screenmenu.sortcategory as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        screenmenuId: function () {
                            return vm.menuId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.createScreenCategory = function (myObj) {
                createCategory(myObj);
            };

            vm.sortCategory = function (myObj) {
                sortCate(myObj);
            };

            vm.getAllScreenCategory();



            //Screen MenuItem
            vm.screenMenuItemOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Actions"),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editItem(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteItem(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize("Name"),
                        field: "name"
                    },
                    {
                        name: app.localize("ButtonColor"),
                        field: "buttonColor"
                    }, {
                        name: app.localize("Tag"),
                        field: "subMenuTag"
                    },
                    {
                        name: app.localize('AutoSelect'),
                        field: 'autoSelect',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.autoSelect" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.autoSelect" class="label label-default">' + app.localize('No') + '</span>' +
                            '</div>'
                    }

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }

                        vm.getAllScreenMenuItem();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAllScreenMenuItem();
                    });
                },
                data: []
            };

            vm.editItem = function (myObj) {
                $state.go("tenant.detailscreenmenuitem", {
                    itemid: myObj.id,
                    categoryid: vm.categoryId,
                    menuid: vm.menuId,
                    location: vm.location
                });
            };
            vm.deleteItem = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteScreenMenuItemWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            screenmenuService.deleteScreenMenuItem({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.getAllScreenMenuItem = function () {
                vm.loading = true;
                screenmenuService.getMenuItems({
                    categoryId: vm.categoryId,
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.screenMenuItemOptions.totalItems = result.totalCount;
                    vm.screenMenuItemOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.createMenuItem = function (myObj) {
                $state.go("tenant.createscreenmenuitem", {
                    categoryid: vm.categoryId,
                    menuid: vm.menuId,
                    location: vm.location
                });
            };

            function openSort(objId) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/screenmenu/menuitem/sortitem.cshtml",
                    controller: "tenant.views.connect.menu.screenmenuitem.sortitem as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        categoryId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.sortMenuItem = function (myObj) {
                openSort(vm.categoryId);
            };
            vm.getAllScreenMenuItem();
        }
    ]);
})();