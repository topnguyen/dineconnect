﻿(function () {
    appModule.controller("tenant.views.connect.menu.screenmenu.index", [
        "$scope", "$state", "$uibModal", "uiGridConstants", "abp.services.app.screenMenu",
        function ($scope, $state,$modal, uiGridConstants, screenmenuService) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.isDeleted = false;

            vm.permissions = {
                create: abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ScreenMenu.Create"),
                edit: abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ScreenMenu.Edit"),
                'delete': abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ScreenMenu.Delete")
            };
            vm.import = function () {
                importModal(null);
            };
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            function importModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/screenmenu/importModal.cshtml",
                    controller: "tenant.views.connect.master.screenmenu.importModal as vm",
                    backdrop: "static"
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Actions"),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editScreenMenu(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteScreenMenu(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.isDeleted && grid.appScope.permissions.edit\" ng-click=\"grid.appScope.activateItem(row.entity)\">" + app.localize("Revert") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize("Name"),
                        field: "name"
                    },
                    {
                        name: app.localize("ColumnCount"),
                        field: "categoryColumnCount"
                    },
                    {
                        name: app.localize("ColumnWidth"),
                        field: "categoryColumnWidthRate"
                    },
                    {
                        name: app.localize("Copy"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.copyMenuItems(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Generate") + "\">" + app.localize("Generate") + "</button> <br/>" +
                            "  <button ng-click=\"grid.appScope.cloneMenu(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Clone") + "\">" + app.localize("Clone") + "</button>" +
                            "</div>"
                    },
                    {
                        name: app.localize("Categories"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.listCategories(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("List") + "\">" + app.localize("List") + "</button> <br/>" +
                            "  <button ng-click=\"grid.appScope.createCategory(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Add") + "\">" + app.localize("Add") + "</button>" +
                            "</div>"
                    },
                    {
                        name: app.localize("VirtualScreen"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.showVitualScreen(row.entity)\" class=\"btn btn-default\" title=\"" + app.localize("VirtualScreen") + "\">" + app.localize("VirtualScreen") + "</button> <br/>" +
                            "</div>"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.listCategories = function (objId) {
                $state.go("tenant.screencategory", {
                    menuid: objId.id,
                    location: objId.refLocationId
                });
            };

            vm.copyMenuItems = function (objId) {

                console.log(objId);

                vm.loading = true;
                screenmenuService.generateScreenMenu({
                    id: objId.id
                }, false).success(function (result) {
                    abp.notify.success(app.localize("Done"));
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.clearMenuItems = function (objId) {
                abp.message.confirm(
                    app.localize("ClearScreenItems", objId.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            screenmenuService.clearScreenMenu({
                                id: objId.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize("Done"));
                            });
                        }
                    }
                );
            };

            vm.createCategory = function (myObj) {
                openCreateCategoryModal(myObj.id);
            };

            vm.cloneMenu = function (myObj) {
                abp.message.confirm(
                    app.localize("CloneScreenWarning_f", myObj.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            screenmenuService.cloneMenu({
                                id: myObj.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize("SavedSuccessfully"));
                            });
                        }
                    }
                );
            };

            vm.getAll = function () {
                vm.loading = true;
                screenmenuService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    location: vm.location,
                    isDeleted: vm.isDeleted,
                    locationGroup: vm.locationGroup
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            function openDetail(objId) {
                $state.go("tenant.detailscreenmenu", {
                    id: objId
                });
            }

            vm.editScreenMenu = function (myObj) {
                openDetail(myObj.id);
            };

            vm.createScreenMenu = function () {
                openDetail(null);
            };

            vm.deleteScreenMenu = function (myObject) {
                abp.message.confirm(
                    app.localize("DeleteScreenMenuWarning", myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            screenmenuService.deleteScreenMenu({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize("SuccessfullyDeleted"));
                            });
                        }
                    }
                );
            };

            vm.showVitualScreen = function (screenMenu) {
                $state.go("tenant.vitualScreenMenu", {
                    screenMenuId: screenMenu.id,
                    location: screenMenu.refLocationId
                });
            };

            function openCreateCategoryModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/screenmenu/category/createCategory.cshtml",
                    controller: "tenant.views.connect.menu.screenmenu.createcategory as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        screenmenuId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.exportToExcel = function () {
                screenmenuService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.currentLocation = null;
            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();
            vm.openLocation = function () {
                
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.currentLocation = result.locations[0];
                        vm.location = result.locations[0].name;
                    }
                    else if (result.groups.length > 0) {
                        vm.currentLocation = result.groups[0];
                        vm.location = result.groups[0].name;
                    }
                    else if (result.locationTags.length > 0) {
                        vm.currentLocation = result.locationTags[0];
                        vm.location = result.locationTags[0].name;
                    }
                    else {
                        vm.location = null;
                        vm.currentLocation = null;
                    }
                });
            };

            vm.clear = function () {
                vm.location = null;
                vm.currentLocation = null;
                vm.filterText = null;
                vm.isDeleted = false;
                vm.intiailizeOpenLocation();

                vm.getAll();
            };

            vm.activateItem = function (myObject) {
                screenmenuService.activateItem({
                    id: myObject.id
                }).success(function (result) {
                    abp.notify.success(app.localize("Successfully"));
                    vm.getAll();
                });
            };

            vm.getAll();
        }
    ]);
})();