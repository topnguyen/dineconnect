﻿(function() {
    appModule.controller("tenant.views.connect.menu.screenmenuitem.sortitem", [
        "$scope", "$uibModalInstance", "abp.services.app.screenMenu", "categoryId",
        function ($scope, $modalInstance, screenmenuService, categoryId) {

            var vm = this;
            vm.saving = false;

            vm.init = function () {
                vm.loading = true;
                screenmenuService.getMenuItems({
                    categoryId: categoryId,
                    operation: 'All'
                }).success(function (result) {
                    vm.category=result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.save = function () {
                vm.loading = true;
                vm.cateIds = [];
                angular.forEach(vm.category, function(item) {
                    vm.cateIds.push(item.id);
                });
                screenmenuService.saveSortSortItems(
                   vm.cateIds
                ).success(function (result) {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.cancel = function() {
                $modalInstance.dismiss();
            };

            vm.init();

        }
    ]);
})();