﻿(function () {
    appModule.controller('tenant.views.connect.menu.screenmenuitem.detail', [
        '$scope', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.screenMenu', "abp.services.app.commonLookup", 'abp.services.app.menuItem', 'FileUploader',
        function ($scope, $state, $stateParams, $modal, uiGridConstants, screenmenuService, commonService, mservice, fileUploader) {
            var vm = this;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.menuItemId = $stateParams.itemid;
            vm.categoryId = $stateParams.categoryid;
            vm.screenMeId = $stateParams.menuid;
            vm.location = $stateParams.location;
            vm.modelMenuItemId = 0;
            vm.modelMenuItemName = "";
            vm.confirmationTypes = [];
            vm.tags = [];
            vm.copyAll = false;
            vm.currentUserId = abp.session.userId;

            vm.save = function () {
                vm.saving = true;
                vm.uploader.uploadAll();
                vm.menuitem.files = angular.toJson(vm.replyFiles);
                vm.setSubMenuTag();
                vm.menuitem.refreshImage = vm.refreshImage;

                screenmenuService.createOrUpdateMenuItem({
                    menuItem: vm.menuitem,
                    copyToAll: vm.copyAll,
                    menuItemId: vm.modelMenuItemId,
                    categoryId: vm.categoryId
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $state.go("tenant.screenmenuitem", {
                        categoryid: vm.categoryId,
                        menuid: vm.screenMeId,
                        location: vm.location
                    });
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.menuItems = [];
            vm.getMenuItems = function () {
                commonService.getMenuItemsForComboboxByLocation(vm.location)
                    .success(function (result) {
                        vm.menuItems = $.parseJSON(JSON.stringify(result.items));
                    }).finally(function (result) {
                    });
            };

            vm.openforMenuItem = function () {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    service: mservice.getAllMenuItems
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.modelMenuItemId = result.locations[0].id;
                        vm.modelMenuItemName = result.locations[0].name;
                    }
                });
            };

            vm.cancel = function () {
                $state.go("tenant.screenmenuitem", {
                    categoryid: vm.categoryId,
                    menuid: vm.screenMeId,
                    location: vm.location
                });
            };

         

            vm.getSubMenuTags = function (tag) {
                var tags = tag.split(', ');

                angular.forEach(tags, function (item) {
                    if (item) {
                        vm.tags.push({ "text": item });
                    }
                });
            };
            vm.setSubMenuTag = function () {
                var tags = [];
                angular.forEach(vm.tags, function (item) {
                    if (item) {
                        tags.push(item.text);
                    }
                });
                vm.menuitem.subMenuTag = tags.join(", ");
            };
            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };
            function init() {
                screenmenuService.getMenuItemForEdit({
                    Id: vm.menuItemId
                }).success(function (result) {
                    vm.menuitem = result.menuItem;

                    if (vm.menuitem.subMenuTag != null)
                        vm.getSubMenuTags(vm.menuitem.subMenuTag);

                    vm.modelMenuItemId = result.menuItemId;
                    vm.modelMenuItemName = result.menuItem.name;
                    vm.replyFiles = vm.menuitem.files ? angular.fromJson(vm.menuitem.files) : [];

                });

                screenmenuService.getConfirmationTypes()
                    .success(function (result) {
                        vm.confirmationTypes = result.items;
                    });


                commonService.getCardTypes()
                    .success(function (result) {
                        vm.cardTypes = $.parseJSON(JSON.stringify(result.items));
                    }).finally(function (result) {
                    });
            }

            init();

            // #region Uploader

            vm.replyFiles = [];
            vm.refreshImage = false;


            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadFile',
                queueLimit : 1
            });
            vm.uploader.onAfterAddingFile = function(item) {
                console.log(item.file.size);

                if (item.file.size > 6000000) {
                    abp.notify.error(app.localize('Size is Big'));
                    vm.uploader.clearQueue();
                } else {
                    item.upload();
                }
            };
            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (vm.replyFiles === null) {
                    vm.replyFiles = [];
                }
                if (response !== null) {
                    if (response.result !== null) {
                        if (response.result.fileName !== null) {
                            vm.replyFiles.push(response.result);
                            vm.refreshImage = true;

                            vm.uploader.queue = [];
                        }
                    }
                }
            };
            vm.downloader = function (file) {
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + file.fileName+'&url='+file.fileSystemName;
            };
            vm.removeFile = function (findex) {
                vm.replyFiles.splice(findex, 1);
                vm.refreshImage = true;
            };

            // #endregion
        }]);
})();