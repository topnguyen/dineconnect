﻿(function() {
    appModule.controller("tenant.views.connect.menu.screenmenuitem.createmodal", [
        '$scope', '$uibModalInstance', '$state', '$stateParams', 'uiGridConstants', "abp.services.app.screenMenu", "abp.services.app.connectLookup",
        function ($scope, $modalInstance, $state,$stateParams,uiGridConstants,  screenmenuService,connectLookup ) {

            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.categoryId = $stateParams.categoryid;
            vm.menuId = $stateParams.menuid;
            vm.location = $stateParams.location;


            vm.groupOptions = {
                title: app.localize("Link"),
                filterPlaceHolder: app.localize("SearchItems"),
                labelAll: app.localize("All"),
                labelSelected: app.localize("Selected"),
                helpMessage: '',
                orderProperty: 'name',
                items: [],
                selectedItems: []
            };
            vm.saving = false;

            vm.init = function () {
                vm.saving = true;
                screenmenuService.getScreenItemsForLocation({
                    locationId: vm.location,
                    categoryId: vm.categoryId
                }).success(function (result) {
                    vm.groupOptions.selectedItems = result.selectedItems;
                    vm.groupOptions.items = result.items;
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.init();
         
            
            vm.gotoCategory = function () {
                $state.go("tenant.screenmenuitem", {
                    categoryid: vm.categoryId,
                    menuid: vm.menuId,
                    location: vm.location
                });
            }
          
            vm.cancel = function() {
                $modalInstance.dismiss();
            };


            vm.save = function () {

                vm.saving = true;
                screenmenuService.createMenuItem({
                    categoryId: vm.categoryId,
                    items: vm.groupOptions.selectedItems
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                    $modalInstance.close();
                });
            };
          
        }
    ]);
})();