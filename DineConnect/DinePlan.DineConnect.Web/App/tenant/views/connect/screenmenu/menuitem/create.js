﻿(function () {
    appModule.controller("tenant.views.connect.menu.screenmenuitem.create", [
        '$scope', '$uibModal', '$state', '$stateParams', 'uiGridConstants', "abp.services.app.screenMenu", "$window",
        function ($scope, $modalInstance, $state, $stateParams, uiGridConstants, screenmenuService, $window) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.subMenuTags = [];

            vm.categoryId = $stateParams.categoryid;
            vm.menuId = $stateParams.menuid;
            vm.location = $stateParams.location;
            vm.menuitem = {
                fontSize: 20,
                orderTags: "",
                subMenuTag: "",
                buttonColor: "Blue"
            };
            vm.confirmationTypes = [];

            vm.groupOptions = {
                title: app.localize("Link"),
                filterPlaceHolder: app.localize("SearchItems"),
                labelAll: app.localize("All"),
                labelSelected: app.localize("Selected"),
                helpMessage: '',
                orderProperty: 'name',
                items: [],
                selectedItems: []
            };
            vm.saving = false;

            vm.init = function () {
                vm.loading = true;
                screenmenuService.getScreenItemsForLocation({
                    locationId: vm.location,
                    categoryId: vm.categoryId
                }).success(function (result) {
                    vm.groupOptions.selectedItems = result.selectedItems;
                    vm.groupOptions.items = result.items;
                }).finally(function () {
                    vm.loading = false;
                });

                screenmenuService.getConfirmationTypes()
                    .success(function (result) {
                        vm.confirmationTypes = result.items;
                    });
            };
            vm.init();
            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };
            vm.gotoCategory = function () {
                $state.go("tenant.screenmenuitem", {
                    categoryid: vm.categoryId,
                    menuid: vm.menuId,
                    location: vm.location
                });
            };

            vm.setSubMenuTags = function () {
                var tags = [];
                angular.forEach(vm.subMenuTags, function (item) {
                    if (item) {
                        tags.push(item.text);
                    }
                });
                vm.menuitem.subMenuTag = tags.join(",");
            };

            vm.cancel = function () {
                $window.history.back();
            };

            vm.save = function () {
                vm.saving = true;
                vm.setSubMenuTags();
                screenmenuService.createMenuItem({
                    categoryId: vm.categoryId,
                    items: vm.groupOptions.selectedItems,
                    menuItem: vm.menuitem
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };
        }
    ]);
})();