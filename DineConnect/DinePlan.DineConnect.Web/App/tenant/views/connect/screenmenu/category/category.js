﻿(function () {
    appModule.controller("tenant.views.connect.menu.screencategory.index", [
        "$scope", "$state", "$stateParams", "$uibModal", "uiGridConstants", "abp.services.app.screenMenu",
        function ($scope, $state, $stateParams, $modal, uiGridConstants, screenmenuService) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });

            vm.menuId = $stateParams.menuid;
            vm.location = $stateParams.location;
            vm.futuredataId = $stateParams.futureDataId;

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            

            vm.permissions = {
                create: abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ScreenMenu.Create"),
                edit: abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ScreenMenu.Edit"),
                'delete': abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ScreenMenu.Delete")
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };


            //#region 

            //Date Range
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createDateRangeFuturePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: vm.dateRangeModel.startDate,
                endDate: vm.dateRangeModel.endDate
            });

            vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
            vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;
            //#endregion

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Actions"),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editCategory(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteCategory(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize("Name"),
                        field: "name"
                    },
                    {
                        name: app.localize("MainButtonColor"),
                        field: "mainButtonColor"
                    },
                    {
                        name: app.localize("MenuItems"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.listmenuItems(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("List") + "\">" + app.localize("List") + "</button> <br/>" +
                            "</div>"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.listmenuItems = function (objId) {
                $state.go("tenant.screenmenuitem", {
                    categoryid: objId.id,
                    menuid: vm.menuId,
                    location: vm.location,
                    futureDataId: vm.futuredataId
                });
            };

            vm.gotoMenu = function () {
                $state.go("tenant.screenmenu", {
                });
            };

            vm.editCategory = function (myObj) {
                $state.go("tenant.detailscreencategory", {
                    categoryid: myObj.id,
                    menuid: vm.menuId,
                    location: vm.location
                });
            };

            vm.deleteCategory = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteScreenCategoryWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            screenmenuService.deleteScreenCategory({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };
            vm.clear = function () {
                vm.filterText = null;
                vm.dateRangeModel = null;
                vm.dateFilterApplied = false;
                vm.getAll();
            };
            vm.getAll = function () {
                vm.loading = true;
                var startDate = null;
                var endDate = null;
                if (vm.dateFilterApplied) {
                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }
                screenmenuService.getCategories({
                    menuid: vm.menuId,
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    futureDataId: vm.futuredataId,
                    startDate: startDate,
                    endDate: endDate,
                    dateFilterApplied: vm.dateFilterApplied
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            function createScreenMenuItem(objId) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/screenmenu/menuitem/createModal.cshtml",
                    controller: "tenant.views.connect.menu.screenmenuitem.createmodal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        categoryId: function () {
                            return objId;
                        },
                        location: function () {
                            return vm.location;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.createMenuItem = function (myObj) {
                createScreenMenuItem(myObj.id);
            };

            function createCategory(objId) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/screenmenu/category/createCategory.cshtml",
                    controller: "tenant.views.connect.menu.screenmenu.createcategory as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        screenmenuId: function () {
                            return vm.menuId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }
            function sortCate(objId) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/screenmenu/category/sortcategory.cshtml",
                    controller: "tenant.views.connect.menu.screenmenu.sortcategory as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        screenmenuId: function () {
                            return vm.menuId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.createScreenCategory = function (myObj) {
                createCategory(myObj);
            };

            vm.sortCategory = function (myObj) {
                sortCate(myObj);
            };

            vm.getAll();
        }
    ]);
})();