﻿(function() {
    appModule.controller("tenant.views.connect.menu.screenmenu.sortcategory", [
        "$scope", "$uibModalInstance", "abp.services.app.screenMenu", "screenmenuId", 
        function($scope, $modalInstance, screenmenuService, screenmenuId) {

            var vm = this;
            vm.saving = false;

            vm.init = function () {
                vm.loading = true;
                screenmenuService.getCategories({
                    menuid: screenmenuId,
                    operation: 'All'
                }).success(function (result) {
                    vm.category=result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.save = function () {
                vm.loading = true;
                vm.cateIds = [];
                angular.forEach(vm.category, function(item) {
                    vm.cateIds.push(item.id);
                });
                screenmenuService.saveSortCategories(
                   vm.cateIds
                ).success(function (result) {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.cancel = function() {
                $modalInstance.dismiss();
            };

            vm.init();

        }
    ]);
})();