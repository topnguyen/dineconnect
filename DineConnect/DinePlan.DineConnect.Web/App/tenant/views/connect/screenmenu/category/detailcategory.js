﻿(function () {
    appModule.controller('tenant.views.connect.menu.screencategory.detail', [
        '$scope', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.screenMenu', 'abp.services.app.connectLookup', 'FileUploader', '$window',
        function ($scope, $state, $stateParams, $modal, uiGridConstants, screenmenuService, lookupService, fileUploader, $window) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.categoryId = $stateParams.categoryid;
            vm.menuId = $stateParams.menuid;
            vm.location = $stateParams.location;
            vm.returnDateRangeFlag = false;

            vm.copyAll = false;
            vm.copyColor = false;
            vm.currentUserId = abp.session.userId;

            vm.save = function () {
                vm.saving = true;

                vm.uploader.uploadAll();
                vm.category.files = angular.toJson(vm.replyFiles);
                vm.category.refreshImage = vm.refreshImage;
                vm.category.screenMenuCategorySchedule = angular.toJson(vm.category.screenMenuCategorySchedules);
                screenmenuService.createOrUpdateCategory({
                    category: vm.category,
                    copyToAll: vm.copyAll,
                    copyColor: vm.copyColor,
                    menuid: vm.menuId
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $window.history.back();
            };
           
            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createDateRangeFuturePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };
            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };
            vm.isUndefinedOrNullOrWhiteSpace = function (val) {
                return angular.isUndefined(val) || val == null || val == "";
            };
            // #region Scope
            $scope.settings = {
                dropdownToggleState: false,
                time: {
                    fromHour: "06",
                    fromMinute: "30",
                    toHour: "23",
                    toMinute: "30"
                },
                noRange: false,
                format: 24,
                noValidation: true
            };
            $scope.$on("$viewContentLoaded",
                function () {
                    App.initAjax();
                });
            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];


            $('input[name="stDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,

            });

            $('input[name="endDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true
            });
            vm.refTypes = [];
            function fillRefTypes() {
                vm.loading = true;
                lookupService.getScreenMenuCategoryKeyBoardTypes({}).success(function (result) {
                    vm.refTypes = result.items;
                }).finally(function () {
                });
            }
            vm.refday = [];
            vm.refdays = [];

            function fillDays() {
                vm.loading = true;
                lookupService.getDays({}).success(function (result) {
                    vm.refdays = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.refmonthday = [];
            vm.refmonthdays = [];

            function fillMonthDays() {
                vm.loading = true;
                lookupService.getMonthDays({}).success(function (result) {
                    vm.refmonthdays = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            }


            $scope.validateDate = function () {
                var startDate = $("#stDate").val();
                var endDate = $("#endDate").val();

                if (startDate == null || startDate == '')
                    return;

                if (endDate == null || endDate == '')
                    endDate = startDate;


                if (moment(startDate) > moment(endDate)) {
                    abp.notify.error(app.localize('shouldNotBeGreaterThan', 'StartDate', 'EndDate'));
                    vm.scheduleEndDate = null;
                    vm.returnDateRangeFlag = true;
                    return;
                }
                vm.returnDateRangeFlag = false;
            };
            // #region Schedule
            vm.removeSchedule = function (productIndex) {
                vm.category.screenMenuCategorySchedules.splice(productIndex, 1);
            };
            vm.addSchedule = function () {
                 var startDate = $("#stDate").val();
                var endDate = $("#endDate").val();

                if (vm.isUndefinedOrNullOrWhiteSpace(startDate)) {
                    abp.notify.error('Please Choose the Start Date');
                    return;
                }
                if (vm.isUndefinedOrNullOrWhiteSpace(endDate)) {
                    abp.notify.error('Please Choose the End Date');
                    return;
                }
                // if (startDate === undefined || startDate == null || !startDate || 0 === startDate.length || startDate == "") {
                //     startDate = null;
                //     vm.scheduleStartDate = null;
                //    console.log("C1");
                //    console.log(startDate);
                //    console.log(endDate);
                //}

                //if (endDate === undefined || endDate == null || !endDate || 0 === endDate.length || endDate == "") {
                //    endDate = null;
                //    vm.scheduleEndDate = null;
                //    console.log("C2");
                //    console.log(startDate);
                //    console.log(endDate);
                //}


                //if (startDate === undefined || startDate == null || !startDate || 0 === startDate.length || startDate == "") {
                //    startDate = moment().startOf("day").format('YYYY-MM-DD');
                //    endDate = moment().startOf("day").format('YYYY-MM-DD');
                //    console.log("C3");
                //    console.log(startDate);
                //    console.log(endDate);

                //}
                $scope.validateDate();
                if (!vm.returnDateRangeFlag) {
                    vm.category.screenMenuCategorySchedules.push({
                        'startDate': vm.scheduleStartDate,
                        'endDate': vm.scheduleEndDate,
                        'startHour': $scope.settings.time.fromHour,
                        'endHour': $scope.settings.time.toHour,
                        'startMinute': $scope.settings.time.fromMinute,
                        'endMinute': $scope.settings.time.toMinute,
                        'allDays': vm.refday,
                        'allMonthDays': vm.refmonthday
                    });

                    vm.refday = [];
                    vm.refmonthday = [];
                }
                vm.scheduleStartDate = null;
                vm.scheduleEndDate = null;
            };

                // #endregion
            function init() {
                vm.loading = true;
                screenmenuService.getCategoryForEdit({
                    Id: vm.categoryId
                }).success(function (result) {
                    vm.category = result.category;
                    vm.replyFiles = vm.category.files ? angular.fromJson(vm.category.files) : [];
                    $('input[name="stDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        minDate:moment(vm.category.creationTime)
                    });

                    $('input[name="endDate"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        minDate: moment(vm.category.creationTime)
                    });
                    if (vm.category.screenMenuCategorySchedules.length > 0) {
                        vm.screenMenuCategorySchedules = [];
                        angular.forEach(vm.category.screenMenuCategorySchedules, function (value, key) {
                            var data = value;
                            if (data.startDate == null || data.endDate == null) {
                                data.startDate = null;
                                data.endDate = null;
                            }
                            else {
                                data.startDate = moment(data.startDate).format($scope.format);
                                data.endDate = moment(data.endDate).format($scope.format);
                            }
                            vm.screenMenuCategorySchedules.push({
                                'startDate': data.startDate,
                                'endDate':data.endDate,
                                'startHour': data.startHour,
                                'endHour': data.endHour,
                                'startMinute': data.startMinute.toString().length == 1 ? "0" + data.startMinute : data.startMinute,
                                'endMinute': data.endMinute.toString().length == 1 ? "0" + data.endMinute : data.endMinute,
                                'allDays': data.allDays,
                                'allMonthDays': data.allMonthDays
                            });
                        });
                        vm.category.screenMenuCategorySchedules = vm.screenMenuCategorySchedules;
                    }
                    
                    fillDays();
                    fillMonthDays();
                }).finally(function () {
                    vm.loading = false;
                });
            }

            init();
            fillRefTypes();

            // #region Uploader

            vm.replyFiles = [];
            vm.refreshImage = false;


            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadFile',
                queueLimit: 1
            });
            vm.uploader.onAfterAddingFile = function (item) {
                console.log(item.file.size);

                if (item.file.size > 6000000) {
                    abp.notify.error(app.localize('Size is Big'));
                    vm.uploader.clearQueue();
                } else {
                    item.upload();
                }
            };
            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (vm.replyFiles === null) {
                    vm.replyFiles = [];
                }
                if (response !== null) {
                    if (response.result !== null) {
                        if (response.result.fileName !== null) {
                            vm.replyFiles.push(response.result);
                            vm.refreshImage = true;

                            vm.uploader.queue = [];
                        }
                    }
                }
            };
            vm.downloader = function (file) {
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + file.fileName + '&url=' + file.fileSystemName;
            };
            vm.removeFile = function (findex) {
                vm.replyFiles.splice(findex, 1);
                vm.refreshImage = true;
            };

            // #endregion


        }]);
})();