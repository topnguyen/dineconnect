﻿(function () {
    appModule.controller("tenant.views.connect.menu.screenmenu.createcategory", [
        "$scope", "$uibModalInstance", "abp.services.app.screenMenu", "screenmenuId",
        function ($scope, $modalInstance, screenmenuService, screenmenuId) {
            var vm = this;
            vm.saving = false;
            vm.screenmenu = null;
            $scope.existall = true;

            vm.save = function () {
                if ($scope.existall == false)
                    return;
                vm.saving = true;
                screenmenuService.createCategory({
                    name: vm.name,
                    menuid: screenmenuId
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();