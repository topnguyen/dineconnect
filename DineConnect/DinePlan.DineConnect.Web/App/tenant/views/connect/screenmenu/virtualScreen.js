﻿(function () {
    appModule.controller('tenant.views.connect.screenmenu.virtualScreen', [
        "$scope", "$state", "$stateParams", "$uibModal", "abp.services.app.screenMenu", '$sce',
        function ($scope, $state, $stateParams, $modal, screenmenuService, $sce) {
            var vm = this;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.screenMenuId = $stateParams.screenMenuId;
            vm.location = $stateParams.location;

            vm.cancel = function () {
                $state.go("tenant.screenmenu", {});
            };

            vm.permissions = {
                create: abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ScreenMenu.Create"),
                edit: abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ScreenMenu.Edit"),
                'delete': abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ScreenMenu.Delete")
            };

            function init() {
                vm.loading = true;
                screenmenuService.getScreenMenuForEdit({
                    id: vm.screenMenuId
                }).success(function (result) {
                    vm.screenmenu = result.screenMenu;
                    vm.requestdepartments = result.departments;
                    vm.locationGroup = result.locationGroup;

                    var colCount = vm.screenmenu.categoryColumnCount === 0 ? 1 : vm.screenmenu.categoryColumnCount;
                    vm.cateWidth = 100 / colCount + '%';
                });

                screenmenuService.getCategories({
                    menuid: vm.screenMenuId,
                    skipCount: 0,
                    maxResultCount: 1
                }).success(function (result) {
                    vm.categories = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            init();

            vm.subMenuItems = [];
            vm.categoryTags = [];
            vm.currentTag = '';
            vm.backButtonTag = '';
            vm.backButtonCaption = '';

            vm.currentCate = null;
            vm.loadMenuItem = function (cate) {
                vm.loadingMenu = true;
                var colCount = cate.columnCount === 0 ? 3 : cate.columnCount;

                vm.menuWidth = 100 / colCount + '%';
                vm.menuHeight = cate.menuItemButtonHeight + 'px';
                vm.menuWrapText = cate.wrapText;

                vm.currentCate = cate;
                vm.getMenuItemsForTag('');
            };

            vm.getMenuItemsForTag = function (tag) {
                screenmenuService.getMenuItemForVisualScreen({
                    categoryId: vm.currentCate.id,
                    skipCount: 0,
                    maxResultCount: 1,
                    currentTag: tag
                }).success(function (result) {
                    vm.subMenuItems = result.menuItems;
                    vm.categoryTags = result.categoryTags;
                }).finally(function () {
                    vm.currentTag = tag;
                    vm.backButtonTag = tag.replace(tag.split(',').pop(), '').slice(0, -1).trim();
                    vm.backButtonCaption = vm.backButtonTag.split(',').pop().trim();
                    vm.loadingMenu = false;
                });
            };

            vm.getCateName = function (item) {
                return $sce.trustAsHtml(item.name.replace("\n", "<br/>").replace("/r", "<br/>").replace("\r", "<br/>"));
            };

            vm.getItemName = function (name) {
                name = name.replace("\n", "<br/>").replace("/r", "<br/>").replace("\r", "<br/>");
                if (vm.menuWrapText)
                    return $sce.trustAsHtml(name.replace(" ", "<br/>"));
                return $sce.trustAsHtml(name);
            };

            vm.getTagName = function (name) {
                name = name.split(',').pop().trim();
                return $sce.trustAsHtml(name);
            };

            vm.editCategory = function (myObj) {
                $state.go("tenant.detailscreencategory", {
                    categoryid: myObj.id,
                    menuid: vm.screenMenuId,
                    location: vm.location
                });
            };

            vm.createScreenCategory = function (objId) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/screenmenu/category/createCategory.cshtml",
                    controller: "tenant.views.connect.menu.screenmenu.createcategory as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        screenmenuId: function () {
                            return vm.screenMenuId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });
            };

            vm.createMenuItem = function (myObj) {
                $state.go("tenant.createscreenmenuitem", {
                    categoryid: myObj.id,
                    menuid: vm.screenMenuId,
                    location: vm.location
                });
            };

            vm.sortCategory = function (objId) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/screenmenu/category/sortcategory.cshtml",
                    controller: "tenant.views.connect.menu.screenmenu.sortcategory as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        screenmenuId: function () {
                            return vm.screenMenuId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });
            };
        }
    ]);
})();