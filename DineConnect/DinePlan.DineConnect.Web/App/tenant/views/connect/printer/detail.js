﻿(function () {
    appModule.controller('tenant.views.connect.printer.detail', [
        '$scope', "$uibModal", 'uiGridConstants', '$state', '$stateParams', 'abp.services.app.printer', 'abp.services.app.printConfiguration','abp.services.app.connectLookup',
        function ($scope, $modal, uiGridConstants, $state, $stateParams, printerService, printConfigurationService,connectService) {
            var vm = this;
            vm.printConfigurationId = $stateParams.id;
            vm.printer = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };

            vm.save = function () {
                if (vm.isUndefinedOrNull(vm.printConfiguration.name))
                {
                    abp.notify.error('Name is mandatory');
                    return;
                }
                vm.printConfiguration.printConfigurationType = 0;
                vm.saving = true;
                printConfigurationService.createOrUpdatePrintConfiguration({
                    printConfiguration: vm.printConfiguration
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.printer');
            };


            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForCreate();
            };
            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.openFilterLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.filterLocationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.filterLocationGroup = result;
                    init();
                });
            };

            function getPrinterType() {
                vm.saving = true;
                printerService.getPrinterTypes()
                    .success(function (result) {
                        vm.printerTypes = result.items;
                    }).finally(function (result) {
                        vm.saving = false;
                    });
            }
            getPrinterType();

            vm.printerOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Operations"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                                "  <button ng-click=\"grid.appScope.removePortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Delete") + "\"><i class=\"fa fa-trash\"></i></button>" +
                                "  <button ng-click=\"grid.appScope.editPortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Edit") + "\"><i class=\"fa fa-edit\"></i></button>" +
                                "  <button ng-click=\"grid.appScope.viewPortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("View") + "\"><i class=\"fa fa-eye\"></i></button>" +
                                "  <button ng-click=\"grid.appScope.clonePortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Clone") + "\"><i class=\"fa fa-clone\"></i></button>" +
                                "</div>"
                    },
                    {
                        name: app.localize("AliasName"),
                        field: "aliasName"
                    },
                    {
                        name: app.localize("PrinterTypeName"),
                        field: "printerTypeName"
                    },
                    {
                        name: app.localize("CharsPerLine"),
                        field: "charsPerLine"
                    },
                    {
                        name: app.localize("CodePage"),
                        field: "codePage"
                    },

                    {
                        name: app.localize("NumberOfLines"),
                        field: "numberOfLines"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        debugger;
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        init();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        init();
                    });
                },
                data: []
            };

            vm.removePortion = function (myObject) {
                abp.message.confirm(
                    app.localize('Are You Sure Want to delete the record ', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            printerService.deletePrinter({
                                id: myObject.id
                            }).success(function (result) {
                                abp.notify.info(app.localize('Deleted'));
                                init();
                            }).finally(function () {
                                vm.saving = false;
                            });
                        }
                    }
                );
            };

            vm.tabtwo = false;
            vm.editPortion = function (myObject) {
                vm.printer = myObject;
                console.log(myObject);
                myObject.locationGroup.viewOnly = false;
                vm.locationGroup = myObject.locationGroup;
                vm.tabtwo = true;
                vm.onchangeprocessor();
            }
            vm.clonePortion = function (myObject) {
                myObject.id = null;
                vm.printer = myObject;

                vm.intiailizeOpenLocation();
                vm.tabtwo = true;
                vm.onchangeprocessor();
            }
            vm.viewPortion = function (myObject) {
                myObject.locationGroup.viewOnly = true;
                vm.locationGroup = myObject.locationGroup;
                vm.openLocation();
                vm.intiailizeOpenLocation();
            };
            vm.savePrinter = function() {

                if (vm.isUndefinedOrNull(vm.printer.charsPerLine)) {
                    abp.notify.error('No of Characters is mandatory');
                    return;
                }
                if (vm.isUndefinedOrNull(vm.printer.codePage)) {
                    abp.notify.error('Code Page is mandatory');
                    return;
                }
                vm.printer.printConfigurationId = vm.printConfiguration.id;
                vm.printer.locationGroup = vm.locationGroup;

                if (vm.printerProcessor) {
                    if (vm.isUndefinedOrNull(vm.printerProcessor.AliasName)) {
                        vm.printerProcessor.AliasName = false;
                    }
                    if (vm.isUndefinedOrNull(vm.printerProcessor.Numerator)) {
                        vm.printerProcessor.Numerator = 0;
                    }
                    vm.printer.customPrinterData = angular.toJson(vm.printerProcessor);
                }

                console.log('Hasan');
                console.log(vm.printer);
                printerService.addOrEditPrinter({
                    printer: vm.printer
                }).success(function() {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                }).finally(function() {
                    vm.tabone = true;
                    vm.intiailizeOpenLocation();
                    init();
                    vm.saving = false;
                });
            };
            vm.onchangeprocessor = function () {
                debugger;
                vm.fields = null;
                if (vm.printer.customPrinterName) {
                    connectService.getPrinterProcessorSetting({ stringId: vm.printer.customPrinterName }).success(function (result) {
                        vm.fields = result;
                    });
                }

                if (vm.printer.customPrinterData)
                    vm.printerProcessor = angular.fromJson(vm.printer.customPrinterData);

            };
            function init() {
                vm.saving = true;
                printerService.getPrinterForEdit({
                    id: vm.printConfigurationId,
                    LocationGroupToBeFiltered: vm.filterLocationGroup
                }).success(function (result) {
                    debugger;
                    vm.printConfiguration = result.printConfiguration;
                    vm.printer = result.printer;
                    vm.printerOptions.data = vm.printConfiguration.printers;
                    vm.printerOptions.totalItems = vm.printConfiguration.printers.length;
                    printerService.getFallBackPrinters({
                        id: vm.printConfiguration.id
                    })
                        .success(function (result) {
                            vm.fallBackprinters = result.items;
                        });
                    vm.saving = false;

                    connectService.getPrinterProcessors({}).success(function (result) {
                        vm.customPrinters = result.items;
                    });

                    if (vm.printer.customPrinterName) {
                        connectService.getProcessorSetting({stringId:vm.printer.customPrinterName}).success(function (result) {
                            vm.fields = result;
                        });
                        vm.printerProcessor = angular.fromJson(vm.printer.customPrinterData);
                    }

                    if (vm.printer.customPrinterData) {
                        vm.printerProcessor = angular.fromJson(vm.printer.customPrinterData);
                    }

                });
            }
            init();

            vm.clearFilterLocation = function() {
                vm.filterLocationGroup = app.createLocationInputForCreate();
                init();
            };

            vm.clearFilterLocation();

        }
    ]);
})();