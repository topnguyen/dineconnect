﻿(function () {
    appModule.controller('tenant.views.connect.menu.tickettaggroup.detail', [
         '$scope', '$state', '$uibModal', '$stateParams', 'abp.services.app.ticketTagGroup', 'abp.services.app.commonLookup', 'abp.services.app.location',
        function ($scope, $state, $modal, $stateParams, tagService, commonLookupService, locationService) {
            var vm = this;
            vm.tag = null;
            vm.uilimit = 20;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.sno = 0;
            vm.tagId = $stateParams.id;
            
            vm.cancel = function () {
                $state.go('tenant.tickettaggroup');
            };

            vm.save = function () {
                vm.saving = true;
                tagService.createOrUpdateTagGroup({
                    tag: vm.tag,
                    locationGroup: vm.locationGroup,
                    departments: vm.requestDepartments
                }).success(function (result) {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $state.go('tenant.tickettaggroup');
                }).finally(function () {
                    vm.saving = false;
                });
            };


            vm.removeTag = function (productIndex) {
                vm.tag.tags.splice(productIndex, 1);
            }
            vm.addTag = function () {
                vm.sno = vm.sno + 1;
                vm.tag.tags.push({ 'id': null, 'name': "", 'alternateName': "" });
            }
            
           

            vm.departments = [];
            vm.requestDepartments = "";
            vm.getDepartments = function () {
                commonLookupService.getDepartments({}).success(function (result) {
                    console.log(result);
                    vm.departments = $.parseJSON(JSON.stringify(result));
                }).finally(function (result) {
                });
            };
            vm.getDepartments();

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };
            vm.processors = [];

            vm.openLanguageDescriptionModal = function (data) {
                if (data == null) {
                    vm.languageDescriptionType = 7; //TicketTagGroup
                    vm.languageDescription = {
                        id: vm.tagId,
                        name: vm.tag.name,
                        languageDescriptionType: vm.languageDescriptionType
                    };
                }
                else {
                    vm.languageDescriptionType = 8; //TicketTag
                    vm.languageDescription = {
                        id: data.id,
                        name: data.name,
                        languageDescriptionType: vm.languageDescriptionType
                    };
                }
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/languageDescription/languageDescriptionModal.cshtml",
                    controller: "tenant.views.connect.languageDescription.languageDescriptionModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        languagedescription: function () {
                            return vm.languageDescription;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });
            }

            function init() {
                tagService.getTagForEdit({
                    id: vm.tagId
                }).success(function (result) {
                    vm.tag = result.tag;
                    vm.locationGroup = result.locationGroup;
                    vm.requestDepartments = result.departments;
                    if (vm.tag.id == null) {
                        vm.uilimit = 20;
                    }
                    else {
                        vm.uilimit = null;
                    }
                });

                commonLookupService.getTicketTagTypes({}).success(function (result) {
                    vm.processors = result.items;
                });
            }
            init();

            vm.openLocation = function() {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function() {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function(result) {
                    vm.locationGroup = result;
                });
            };
        }
    ]);
})();