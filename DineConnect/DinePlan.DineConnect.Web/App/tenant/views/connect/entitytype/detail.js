﻿(function () {
    appModule.controller('tenant.views.connect.master.entityttype.detail', [
         '$scope', '$state', '$stateParams','abp.services.app.entityType', 'abp.services.app.commonLookup',

            function ($scope, $state, $stateParams, entityService, commonService) {
                var vm = this;

                $scope.$on('$viewContentLoaded', function () {
                    App.initAjax();
                });

                vm.entityTypeId = $stateParams.id;
                vm.entitytype = null;

                vm.removeRow = function (productIndex) {
                    vm.entitytype.fields.splice(productIndex, 1);
                }
                vm.fieldTypes = [];

                function fillFieldTypes() {
                    commonService.getFieldTypes({}).success(function (result) {
                        console.log(result.items);
                        vm.fieldTypes = result.items;
                    });
                }
                vm.addField = function () {
                    vm.entitytype.fields.push({ 'name': "", 'fieldType': 0, 'editingFormat': "", 'valueSource': "",'hidden':0  });

                }

                vm.portionLength = function () {
                    return true;
                }

                vm.save = function () {
                    vm.saving = true;
                    entityService.createOrUpdateEntityType({
                        entityType: vm.entitytype
                    }).success(function () {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        $state.go('tenant.entitytype');
                    }).finally(function () {
                        vm.saving = false;
                    });
                };
                vm.cancel = function () {
                    $state.go('tenant.entitytype');
                };
                function init() {
                    fillFieldTypes();
                    entityService.getEntityTypeForEdit({
                        id: vm.entityTypeId
                    }).success(function (result) {
                        vm.entitytype = result.entityType;
                    });
                }
                init();
            }

    ]);
})();