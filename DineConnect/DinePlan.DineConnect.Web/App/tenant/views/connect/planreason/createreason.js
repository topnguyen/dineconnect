﻿
(function () {
    appModule.controller("tenant.views.connect.planreason.create",
        [
            "$scope", "$state", '$uibModal', "$stateParams", "abp.services.app.planReason", 'abp.services.app.connectLookup',
            function ($scope, $state, $modal, $stateParams, planService, loopupService) {
                var vm = this;
                vm.planReasonId = $stateParams.id;
                vm.saving = false;
                vm.planReason = null;
                vm.languageDescriptionType = 11;
                vm.reasonGroup = false;

                vm.isUndefinedOrNullOrWhiteSpace = function (val) {
                    return angular.isUndefined(val) || val == null || val == "";
                };

                vm.save = function () {
                    console.log(vm.planReason);
                    if (vm.reasonGroup == false) {
                        if (vm.isUndefinedOrNullOrWhiteSpace(vm.planReason.reasonGroup)) {
                            abp.notify.error(app.localize('Group') + app.localize('Mandatory'));
                            return;
                        }
                    }
                    vm.saving = true;
                    planService.createOrUpdateMessage({
                        planReason: vm.planReason,
                        locationGroup: vm.locationGroup
                    }).success(function () {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        vm.cancel();
                    }).finally(function () {
                        vm.saving = false;
                    });
                };

                vm.cancel = function () {
                    $state.go("tenant.planreason");
                };

                vm.onchangeReasonGroup = function () {
                    vm.planReason.reasonGroup = null;
                }

                vm.getComboValue = function (item) {
                    return parseInt(item.value);
                };
                vm.requestlocations = "";

                vm.getEditionValue = function (item) {
                    return item.value;
                };
                vm.openLocation = function () {
                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };
                vm.planReasonGroups = [];
                function fillFieldTypes() {
                    loopupService.getPlanReasonGroups({}).success(function (result) {
                        vm.planReasonGroups = result.items;
                    });
                }
                vm.openLanguageDescriptionModal = function () {
                    vm.languageDescription = {
                        id: vm.planReasonId,
                        name: vm.planReason.reason,
                        languageDescriptionType: vm.languageDescriptionType
                    };
                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/languageDescription/languageDescriptionModal.cshtml",
                        controller: "tenant.views.connect.languageDescription.languageDescriptionModal as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            languagedescription: function () {
                                return vm.languageDescription;
                            }
                        }
                    });

                    modalInstance.result.then(function (result) {
                        init();
                    });
                }
                function init() {
                    fillFieldTypes();

                    planService.getPlanReasonForEdit({
                        Id: vm.planReasonId
                    }).success(function (result) {
                        vm.planReason = result.planReason;
                        vm.locationGroup = result.locationGroup;
                        if (vm.planReason.reasonGroupCheckBox) {
                            vm.reasonGroup = true;
                        }
                        else {
                            vm.reasonGroup = false;
                        }
                    });
                }

                init();
            }
        ]);
})();