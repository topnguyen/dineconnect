﻿(function () {
    appModule.controller("tenant.views.connect.user.dineplanuser.create", [
        "$scope", "$state", "$uibModal", "$stateParams", "abp.services.app.dinePlanUser", "abp.services.app.connectLookup","abp.services.app.commonLookup","lookupModal",
        function ($scope, $state, $modal, $stateParams, dineplanuserService, commonLookupService,comLookup, lookupModal) {
            var vm = this;

            vm.saving = false;
            vm.dineplanuser = null;
            vm.userRoles = [];
            vm.userId = $stateParams.id;
            vm.languages = abp.localization.languages;

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };
            vm.save = function () {
                console.log(vm.dineplanuser);
                if (vm.dineplanuser.languageCode === vm.dineplanuser.alternateLanguageCode) {
                    abp.notify.error(app.localize('LangAndAltLangError'));
                    return;
                }
                vm.saving = true;
                dineplanuserService.createOrUpdateDinePlanUser({
                    dinePlanUser: vm.dineplanuser,
                    locationGroup: vm.locationGroup
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));

                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };

            vm.cancel = function () {
                $state.go("tenant.dineplanuser");
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
            

            function init() {
                console.log($stateParams.Id);
                commonLookupService.getUserRoles({}).success(function (result) {
                    vm.userRoles = result.items;
                    vm.userRoles.unshift({ value: "0", displayText: app.localize("NotAssigned") });
                });
                dineplanuserService.getDinePlanUserForEdit({
                    id: vm.userId
                }).success(function (result) {
                    console.log(result);
                    vm.dineplanuser = result.dinePlanUser;
                    if (vm.dineplanuser.id != null)
                    {
                        vm.dineplanuser.code = parseInt(vm.dineplanuser.code);
                    }
                    vm.locationGroup = result.locationGroup;
                });
            }

            init();

            vm.openUser = function() {
                console.log("test");
                lookupModal.open({
                    title: app.localize("SelectAUser"),
                    serviceMethod: comLookup.findUsers,

                    canSelect: function (item) {
                        return true;
                    },
                    callback: function(selectedItem) {
                        if (selectedItem != null) {
                            vm.dineplanuser.name = selectedItem.name;
                            vm.dineplanuser.userId = selectedItem.value;
                        }

                    }
                });
            };

            vm.cancelUser = function() {
                vm.dineplanuser.userId = null;
                vm.dineplanuser.connectUser = null;
            }
        }
    ]);
})();