﻿
(function () {
    appModule.controller('tenant.views.connect.user.dineplanuserrole.editpermission', [
        '$scope', '$uibModalInstance', 'abp.services.app.dinePlanUserRole', 'dineplanuserroleId', 'abp.services.app.commonLookup',
        function ($scope, $modalInstance, dineplanuserroleService, dineplanuserroleId, commonLookupService) {
            var vm = this;
            

            vm.saving = false;
            vm.dineplanuserrole = null;
			$scope.existall = true;

			
            vm.save = function () {
			   if ($scope.existall == false)
                    return;
                vm.saving = true;
				
                dineplanuserroleService.createOrUpdateDinePlanUserRole({
                    dinePlanUserRole: vm.dineplanuserrole
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.dineplanuserrole.id == null) {
			         vm.existall = false;
			         return;
			     }
				 
                dineplanuserroleService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.dineplanuserrole.id,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.dineplanuserrole.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.dineplanuserrole.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
			vm.reflocations = [];

			 function fillDropDownLocations() {
			     	commonLookupService.getLocationsForCombobox({}).success(function (result) {
                    vm.reflocations = result.items;
                   // vm.reflocations.unshift({ value: "0", displayText: app.localize('NotAssigned') });
                });
			 }

	        function init() {
				fillDropDownLocations();

                dineplanuserroleService.getDinePlanUserRoleForEdit({
                    Id: dineplanuserroleId
                }).success(function (result) {
                    vm.dineplanuserrole = result.dinePlanUserRole;
                });
            }
            init();
        }
    ]);
})();

