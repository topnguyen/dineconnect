﻿(function () {
    appModule.controller('tenant.views.connect.user.dineplanuserrole.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.dinePlanUserRole', 'dineplanuserroleId', 'abp.services.app.connectLookup',
        function ($scope, $modalInstance, dineplanuserroleService, dineplanuserroleId, connectLookup) {
            var vm = this;

            vm.saving = false;
            vm.dineplanuserrole = null;
            $scope.existall = true;
            vm.permissionEditData = null;
            vm.departments = [];

            vm.save = function () {
                if ($scope.existall == false)
                    return;
                vm.saving = true;

                dineplanuserroleService.createOrUpdateDinePlanUserRole({
                    dinePlanUserRole: vm.dineplanuserrole,
                    grantedPermissionNames: vm.permissionEditData.grantedPermissionNames
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {
                if (vm.dineplanuserrole.id == null) {
                    vm.existall = false;
                    return;
                }

                dineplanuserroleService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.dineplanuserrole.id,
                    operation: 'SEARCH'
                }).success(function (result) {
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.dineplanuserrole.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.dineplanuserrole.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };
            function init() {
                connectLookup.getDepartments({}).success(function (result) {
                    vm.departments = result.items;
                });
                dineplanuserroleService.getDinePlanUserRoleForEdit({
                    Id: dineplanuserroleId
                }).success(function (result) {
                    vm.dineplanuserrole = result.dinePlanUserRole;
                    vm.permissionEditData = {
                        permissions: result.permissions,
                        grantedPermissionNames: result.grantedPermissionNames
                    };
                });
            }
            init();
        }
    ]);
})();