﻿(function () {
    appModule.controller('tenant.views.connect.category.productGroupTreeModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.productGroup', 'category',
        function ($scope, $uibModalInstance, productGroupService, category) {
            var vm = this;
            vm.category = category;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };
           
            
            vm.productGroup = {

                $tree: null,
                selectedOu: {
                    id: null,
                    code: null,
                    name: null,
                    set: function (ouInProductGroup) {
                        if (!ouInProductGroup) {
                            vm.productGroup.selectedOu.id = null;
                            vm.productGroup.selectedOu.code = null;
                            vm.productGroup.selectedOu.name = null;
                        } else {
                            vm.productGroup.selectedOu.id = ouInProductGroup.id;
                            vm.productGroup.selectedOu.code = ouInProductGroup.original.code;
                            vm.productGroup.selectedOu.name = ouInProductGroup.original.name;
                        }

                    }
                },



                getTreeDataFromServer: function (callback) {
                    productGroupService.getProductGroups({}).then(function (result) {
                        var treeData = _.map(result.data.items, function (item) {
                            return {
                                id: item.id,
                                parent: item.parentId ? item.parentId : '#',
                                code: item.code,
                                name: item.name,
                                text: item.name,
                                state: {
                                    opened: true,
                                    selected:item.code==vm.category.groupCode?true:false
                                }
                            };
                        });

                        callback(treeData);
                    });
                },

                init: function () {
                    vm.productGroup.getTreeDataFromServer(function (treeData) {
                        vm.productGroup.$tree = $('#ProductGroupEditTree');

                        vm.productGroup.$tree
                            .on('changed.jstree', function (e, data) {
                                if (data.selected.length != 1) {
                                    vm.productGroup.selectedOu.set(null);
                                } else {
                                    var selectedNode = data.instance.get_node(data.selected[0]);
                                    vm.productGroup.selectedOu.set(selectedNode);
                                }
                            })
                            .jstree({
                                'core': {
                                    data: treeData,
                                    multiple: false,
                                },
                                "types": {
                                    "default": {
                                        "icon": "fa fa-folder tree-item-icon-color icon-lg"
                                    },
                                    "file": {
                                        "icon": "fa fa-file tree-item-icon-color icon-lg"
                                    }
                                },
                                'checkbox': {
                                    keep_selected_style: false,
                                    three_state: false,
                                    cascade: ''
                                },
                                plugins: ['checkbox', 'types']
                            });
                    });
                },

                reload: function () {
                    vm.productGroup.getTreeDataFromServer(function (treeData) {
                        vm.productGroup.$tree.jstree(true).settings.core.data = treeData;
                        vm.productGroup.$tree.jstree('refresh');
                    });
                }
            };
            
            vm.productGroup.init();

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
            vm.save = function () {
                $uibModalInstance.close(vm.productGroup);
            }

        }]);
})();