﻿(function () {
    appModule.controller('tenant.views.connect.menu.category.detail', [
         '$scope', '$state', '$stateParams','abp.services.app.category', 'abp.services.app.commonLookup',
        function ($scope, $state, $stateParams,categoryService,commonService) {
            var vm = this;
            vm.categoryId = $stateParams.id;
            vm.locations = [];
            vm.allLocations = [];
            vm.getLocationValue = function (item) {
                return parseInt(item.value);
            };
            vm.removeRow = function (productIndex) {
                vm.category.locations.splice(productIndex, 1);
                vm.FilterLocation();

            }
            vm.addLocation = function () {
                vm.FilterLocation();

                vm.category.locations.push({ 'locationId': 0, 'sharingPercentage': 0, 'inclusiveOfTax': true });
            }
            
            vm.save = function () {
                vm.saving = true;
                categoryService.createOrUpdateCategory({
                    category: vm.category
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $state.go('tenant.category');
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $state.go('tenant.category');
            };

            vm.FilterLocation = function () {

                console.log(vm.category.locations);
                vm.testLocations = [];
                angular.forEach(vm.allLocations, function (requestvalue, key) {
                    insertflag = true;
                    vm.category.locations.some(function (filtervalue, key) {
                        if (filtervalue.locationId == requestvalue.value) {
                            insertflag = false;
                            return false;
                        }
                    });
                    if (insertflag == true)
                        vm.testLocations.push(requestvalue);
                });

                vm.locations = vm.testLocations;
            };

            function init() {

                categoryService.getCategoryForEdit({
                    id: vm.categoryId
                }).success(function (result) {
                    vm.category = result.category;

                });

                commonService.getLocationsForLoginUser({}).success(function (result) {
                    vm.allLocations = result.items;
                    vm.FilterLocation();
                });
               

            }
            init();
        }
    ]);
})();
