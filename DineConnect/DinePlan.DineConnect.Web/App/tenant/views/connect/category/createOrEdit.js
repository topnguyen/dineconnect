﻿(function () {
    appModule.controller('tenant.views.connect.menu.category.categorydetail', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.category', 'abp.services.app.productGroup',
        function ($scope, $state, $stateParams, $modal, categoryService, productGroupService) {
            var vm = this;
            vm.categoryId = $stateParams.id;
            vm.saving = false;
            vm.category = null;
            vm.languageDescriptionType = 2;

            vm.save = function () {
                vm.saving = true;

                vm.category.code = vm.category.code.toUpperCase();
                categoryService.createOrUpdateCategory({
                    category: vm.category
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.category');
            };

            vm.openGroupMaster = function () {
                openCreateGroupMaster(null);
            };

            var selectedGroupId = null;

            function openCreateGroupMaster() {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/category/productGroupTreeModal.cshtml',
                    controller: 'tenant.views.connect.category.productGroupTreeModal as vm',
                    backdrop: 'static',
                    resolve: {
                        category: function () {
                            return vm.category;
                        }
                    }
                });
                modalInstance.result
                    .then(function (result) {
                        vm.category.productGroupId = result.selectedOu.id;
                        vm.category.groupName = result.selectedOu.name;
                        vm.category.groupCode = result.selectedOu.code;
                    }).finally(function () {
                    });
            }

            vm.productGroups = [];
            function fillDropDownGroup() {
                productGroupService.getProductGroupNames({})
                    .success(function (result) {
                        vm.productGroups = result.items;
                        if (selectedGroupId != null)
                            vm.category.productGroupId = selectedGroupId;
                    });
            }

            vm.openLanguageDescriptionModal = function () {
                vm.languageDescription = {
                    id: vm.categoryId,
                    name: vm.category.name,
                    languageDescriptionType: vm.languageDescriptionType
                };
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/languageDescription/languageDescriptionModal.cshtml",
                    controller: "tenant.views.connect.languageDescription.languageDescriptionModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        languagedescription: function () {
                            return vm.languageDescription;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });
            }

            function init() {
                categoryService.getCategoryForEdit({
                    id: vm.categoryId
                }).success(function (result) {
                    vm.category = result.category;
                });
                fillDropDownGroup();
            }
            init();
        }
    ]);
})();