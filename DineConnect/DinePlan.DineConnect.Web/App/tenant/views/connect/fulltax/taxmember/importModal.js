﻿(function () {
    appModule.controller('tenant.views.connect.fulltax.taxmember.importModal', [
        '$scope', 'appSession', '$uibModalInstance', 'FileUploader', 'abp.services.app.fullTax',
        function ($scope, appSession, $uibModalInstance, fileUploader, fullTaxService) {
            var vm = this;

            vm.uploader = new fileUploader({
                url: abp.appPath + 'Import/ImportFullTaxMember',
                queueLimit: 1,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        if ('|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ImportTemplate_Warn_FileType'));
                            return false;
                        }
                        return true;
                    }
                }]
            });

            vm.importpath = function () {
                vm.loading = true;
                fullTaxService.getTaxMemberListToExcel()
                    .success(function (result) {
                        vm.loading = false;
                        app.downloadTempFile(result);
                    });
            };

            vm.save = function () {
                vm.loading = true;
                vm.uploader.uploadAll();
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.error !== null) {
                    abp.message.warn(response.error.message);
                    //$uibModalInstance.dismiss();
                    vm.loading = false;
                    return;
                }

                abp.notify.info("Finished");
                $uibModalInstance.close();
            };
        }

    ]);
})();