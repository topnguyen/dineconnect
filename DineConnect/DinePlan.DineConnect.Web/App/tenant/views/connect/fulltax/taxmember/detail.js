﻿(function () {
    appModule.controller('tenant.views.connect.fulltax.taxmember.detail', [
        '$scope', "$uibModal", '$state', '$stateParams', 'uiGridConstants', 'abp.services.app.fullTax',
        function ($scope, $modal, $state, $stateParams, uiGridConstants, fullTaxService ) {
            var vm = this;
            vm.fullTaxMemberId = $stateParams.id;
            vm.address;
            vm.addresses = [];

            vm.save = function () {
                vm.saving = true;
                if (!vm.taxMember.id) {
                    vm.addresses.push(vm.address);
                }

                fullTaxService.createOrUpdateFullTaxMember({
                    taxMember: vm.taxMember,
                    locationGroup: vm.locationGroup,
                    addresses: vm.addresses
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $state.go('tenant.fulltaxmember');
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.deleteAddress = function (add, index) {
                vm.saving = true;

                fullTaxService.deleteAddress({
                    id: add.id
                }).success(function () {
                }).finally(function () {
                    vm.saving = false;
                    vm.addresses.splice(index, 1);
                });
            };

            vm.cancel = function () {
                $state.go('tenant.fulltaxmember');
            };

            function init() {
                vm.saving = true;
                fullTaxService.getFullTaxMemberForEdit({
                    id: vm.fullTaxMemberId
                }).success(function (result) {
                    vm.taxMember = result.taxMember;
                    vm.locationGroup = result.locationGroup;
                    vm.addresses = result.addresses;
                    vm.saving = false;
                });
            }

            init();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.addAddress = function () {
                var addressModal = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/fulltax/taxmember/addaddress.cshtml",
                    controller: "tenant.views.connect.fulltax.taxmember.addressModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        memberId: function () {
                            return vm.fullTaxMemberId;
                        }
                    }
                });

                addressModal.result.then(function (result) {
                    fullTaxService.getFullTaxMemberForEdit({
                        id: vm.fullTaxMemberId
                    }).success(function (result) {
                        vm.addresses = result.addresses;
                    });
                });
            };
        }
    ]);
})();