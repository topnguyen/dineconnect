﻿(function () {
    appModule.controller("tenant.views.connect.fulltax.taxmember.addressModal",
        ["$scope", "$uibModalInstance", "abp.services.app.fullTax", "memberId",
            function ($scope, $modalInstance, fullTaxService, memberId) {
                var vm = this;
                vm.memberId = memberId;
                vm.address;

                vm.save = function () {
                    vm.saving = true;
                    vm.address.fullMemberId = vm.memberId;

                    fullTaxService.createTaxAddress(vm.address).success(function () {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        $modalInstance.close(location);
                    }).finally(function () {
                        vm.saving = false;
                    });
                };

                vm.cancel = function () {
                    $modalInstance.dismiss();
                };
            }
        ]);
})();