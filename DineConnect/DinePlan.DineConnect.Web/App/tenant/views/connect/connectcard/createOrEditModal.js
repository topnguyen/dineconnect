﻿(function () {
    appModule.controller('tenant.views.connect.card.connectcard.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.connectCard', 'connectcardId',
        function ($scope, $modalInstance, connectcardService, connectcardId) {
            var vm = this;

            vm.saving = false;
            vm.connectcard = null;

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };

            vm.save = function () {
                if (vm.isUndefinedOrNull(vm.connectcard.connectCardTypeId)) {
                    abp.notify.error('Card Type is mandatory');
                    return;
                }
                vm.saving = true;
                connectcardService.createOrUpdateConnectCard(vm.connectcard)
                    .success(function () {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        $modalInstance.close();
                    }).finally(function () {
                        vm.saving = false;
                    });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.refCardtypes = [];
            vm.fillCardtypess = function () {
                vm.refCardtypes = [];
                vm.loading = true;
                connectcardService.getConnectCardTypes({}).success(function (result) {
                    vm.loading = false;
                    vm.refCardtypes = result;
                });
            };
            vm.fillCardtypess();

            function init() {
                connectcardService.getConnectCardForEdit({
                    Id: connectcardId
                }).success(function (result) {
                    vm.connectcard = result;
                });
            }

            init();

            vm.disableRedeemedCheckbox = false;
            vm.changeCardType = function (data) {
                vm.connectcard.redeemed = data.useOneTime ? vm.connectcard.redeemed : false;

                vm.disableRedeemedCheckbox = !data.useOneTime;
            };
        }
    ]);
})();