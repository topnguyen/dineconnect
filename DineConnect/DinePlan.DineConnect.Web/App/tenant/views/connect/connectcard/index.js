﻿(function () {
    appModule.controller('tenant.views.connect.card.connectcard.index', [
        '$scope', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.connectCard',
        function ($scope, $stateParams, $modal, uiGridConstants, connectCardService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.cardTypeId = $stateParams.cardTypeId;
            vm.isDeleted = false;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Conect.Card.Cards.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Conect.Card.Cards.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Conect.Card.Cards.Delete'),
                'import': abp.auth.hasPermission('Pages.Tenant.Conect.Card.Cards.Import'),
                'active': abp.auth.hasPermission('Pages.Tenant.Conect.Card.Cards.Active'),
                'deletemultiplecards': abp.auth.hasPermission('Pages.Tenant.Conect.Card.Cards.DeleteMultipleConnectCards')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 150,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editConnectCard(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteConnectCard(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.active &&!row.entity.active&&!row.entity.redeemed\" ng-click=\"grid.appScope.activeConnectCard(row.entity,0)\">" + app.localize("Active") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.active &&row.entity.active&&!row.entity.redeemed\" ng-click=\"grid.appScope.activeConnectCard(row.entity,1)\">" + app.localize("In") + app.localize("Active") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('CardNumber'),
                        field: 'cardNo'
                    },
                    {
                        name: app.localize('CardType'),
                        field: 'connectCardTypeName'
                    },
                    {
                        name: app.localize('Redeemed'),
                        field: 'redeemed'
                    },
                    {
                        name: app.localize('Active'),
                        field: 'active'
                    },
                    {
                        name: app.localize('Printed'),
                        field: 'printed'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm\''
                    },
                    {
                        name: app.localize('Select'),
                        enableSorting: false,
                        cellTemplate:
                            '<input type="checkbox" ng-model="row.entity.isSelected"> '
                    },


                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                connectCardService.getAllConnectCard({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    connectCardTypeId: vm.cardTypeId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.selectAllCards = function () {
                var isDeleted = document.getElementById("isDeleted");
                if (isDeleted.checked) {
                    angular.forEach(vm.userGridOptions.data, function (value, key) {
                        value.isSelected = true;
                    });
                } else {
                    angular.forEach(vm.userGridOptions.data, function (value, key) {
                        value.isSelected = false;
                    });
                }
                
            }
            
            vm.editConnectCard = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createConnectCard = function () {
                openCreateOrEditModal(null);
            };
            vm.importConnectCard = function () {
                importModal(null);
            };

            vm.isExistRedeemed = false;
            vm.multipleCardDelete = function () {
                vm.isExistRedeemed = false;
                var arr = new Array();
                for (var i = 0; i < vm.userGridOptions.data.length; i++) {
                    if (vm.userGridOptions.data[i].isSelected) {
                        var value = vm.userGridOptions.data[i].id;
                        var kv = new keyval(value);
                        arr.push(kv);
                        // check is exist redeemd
                        if (vm.userGridOptions.data[i].redeemed == true) {
                            vm.isExistRedeemed = true;
						}
                    }
                    function keyval(val) {
                        this.val = val;
                    }
                }

                vm.multipleConnectCardIds = []
                angular.forEach(arr, function (value, key) {
                    vm.multipleConnectCardIds.push({
                        'connectCardId': value.val
                    });
                });

                vm.multipleCardDeleteFunction();
                
            };
            vm.multipleCardDeleteFunction = function ()
            {
                if (vm.multipleConnectCardIds.length == 0) {
                    abp.notify.info(app.localize('PleaseSelectOneDataForDelete'));
                } else if (vm.isExistRedeemed) {
                    abp.notify.warn(app.localize('Can’tDeleteThisData'));
				}
                else {
                    connectCardService.deleteMultiCards({
                        multipleConnectCardId: vm.multipleConnectCardIds
                    }).success(function () {
                        abp.notify.success(app.localize('SuccessfullyDeleted'));
                        vm.isDeleted = false;
                        vm.getAll();
                    });
				}
            }
            function importModal() {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/connectcard/importConnectCard.cshtml',
                    controller: 'tenant.views.connect.card.connectcard.importConnectCard as vm',
                    backdrop: 'static',
                    keyboard: false,
                    //resolve: {
                    //    connectcardId: function () {
                    //        return objId;
                    //    }
                    //}
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.deleteConnectCard = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteConnectCardWarning', myObject.cardNo),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            connectCardService.deleteConnectCard({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };
            vm.activeConnectCard = function (data, option) {
                if (option == 0) {
                    data.active = true;
                }
                else {
                    data.active = false;
                }
                connectCardService.updateConnectCardInfo({
                    connectCard: data
                }).success(function (result) {
                    abp.notify.success(app.localize("UpdatedSuccessfully"));
                    vm.getUsers();
                });
            }
            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/connectcard/createOrEditModal.cshtml',
                    controller: 'tenant.views.connect.card.connectcard.createOrEditModal as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        connectcardId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.exportToExcel = function () {
                connectCardService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.getAll();
        }]);
})();