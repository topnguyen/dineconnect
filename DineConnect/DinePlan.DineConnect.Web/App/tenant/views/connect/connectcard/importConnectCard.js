﻿(function () {
    appModule.controller('tenant.views.connect.card.connectcard.importConnectCard', [
        '$scope', '$uibModalInstance', 'FileUploader', 'uiGridConstants', 'abp.services.app.connectCard', '$interval', 
        function ($scope, $uibModalInstance, fileUploader, uiGridConstants, connectcardService, $interval) {
            var vm = this;
            vm.loading = false;
            vm.excelloading = false;
            vm.errorMessage = null;
            vm.activeFlag = true;
            vm.doesRunInBackGround = false;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.uploader = new fileUploader({
                url: abp.appPath + 'Import/ImportConnectCardData',
                formData: [
                    {
                        //active: vm.activeFlag
                    }
                ],
                queueLimit: 1,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        if ('|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ImportTemplate_Warn_FileType'));
                            return false;
                        }
                        return true;
                    }
                }]
            });

            vm.uploader.onBeforeUploadItem = function (fileitem) {
                fileitem.formData.push({ active: vm.activeFlag });
                fileitem.formData.push({ jobName: vm.jobName });
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.error != null) {
                    abp.message.warn(response.error.message);
                    $uibModalInstance.close();
                    vm.loading = false;
                    return;
                }
                //if (response.result != null) {
                //    $uibModalInstance.close(response.result);
                //    abp.notify.info(app.localize("FINISHED"));
                //}

                $uibModalInstance.close();
                abp.notify.info(app.localize("FINISHED"));
                vm.loading = false;
            };

            vm.connectCards = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Job') + app.localize('Name'),
                        field: 'jobName'
                    },
                    {
                        name: app.localize('FileName'),
                        field: 'fileName'
                    },
                    //{
                    //    name: app.localize('File')+app.localize('Location'),
                    //    field: 'fileLocation'
                    //},
                    {
                        name: app.localize('StartDate'),
                        field: 'startTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                    },
                    {
                        name: app.localize('EndDate'),
                        field: 'endTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                    },
                    {
                        name: app.localize('Remarks'),
                        field: 'remarks',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <span> {{row.entity.remarks}} </span>' +
                            '  <br>' +
                            '  <br>' +
                            '  <button  ng-if="!row.entity.isFailed" ng-click="grid.appScope.viewImportedCardsDataDetail(row.entity)" class="btn btn-default btn-xs " title="' + '"><i class="fa fa-file-excel-o"></i></button>' +
                            '</div>',
                    },

                ],
                data: []
            };

            vm.importpath = abp.appPath + 'Import/ImportConnectCard';

            vm.save = function () {
                if (vm.uploader.queue.length == 0) {
                    abp.notify.warn(app.localize('YouAreNotSelectAFile'));
                    return;
                }
                vm.loading = true;
                vm.uploader.uploadAll();
            };

            vm.cancel = function () {
                vm.doesRunInBackGround = false;
                $interval.cancel(executeDayCloseInInterval);
                $uibModalInstance.dismiss();
            };

            vm.exportImportCardDataDetail = function () {
                vm.loading = true;
                connectcardService.exportImportCardDataDetail({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting
                }).success(function (result) {
                    app.downloadTempFile(result);
                });
                vm.loading = false;
            }

            vm.getAll = function () {
                connectcardService.getImportCardDataDetail({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting
                }).success(function (result) {
                    vm.connectCards.totalItems = result.length;
                    vm.connectCards.data = result;
                });
            };

            var executeImportCardInterval = null;

            $scope.theTime = new Date().toLocaleTimeString();
            vm.noofInterval = 0;
            vm.backgroundStarted = true;
            vm.doesRunInBackGround = true;
            vm.checkBackGround = function () {
                if (vm.doesRunInBackGround) {
                    executeDayCloseInInterval =
                        $interval(function () {
                            vm.backgroundStarted = true;
                            vm.getAll();
                            $scope.theTime = new Date().toLocaleTimeString();
                        }, 20000);
                }
            }
            vm.checkBackGround();
            vm.getAll();
            vm.viewImportedCardsDataDetail = function (data) {
                vm.excelloading = true;
                connectcardService.canViewImportCardDataDetail({
                    importCardDataDetail:data
                }).success(function (result) {
                    app.downloadTempFile(result);
                }).finally(function () {
                    vm.excelloading = false;
                });
            }

            vm.openHangFire = function () {
                var url = "";
                url = "http://" + window.location.host + "/hangfire";
                //$window.open(url, '_blank');
                //url = "http://localhost:7301/hangfire";
                window.open(url);
            }

           
        }

    ]);
})();