﻿(function () {
    appModule.controller('tenant.views.connect.card.connectcard.cardsModal', [
        '$scope', 'uiGridConstants', '$uibModalInstance', 'abp.services.app.connectCard', 'connectCardTypeId', 'redeemed',
        function ($scope, uiGridConstants, $modalInstance, connectCardService, connectCardTypeId, redeemed) {
            var vm = this;

            vm.loading = false;
            vm.filterText = null;
            vm.cardTypeId = connectCardTypeId;
            vm.redeemed = redeemed;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [

                    {
                        name: app.localize('CardNumber'),
                        field: 'cardNo'
                    },
                    {
                        name: app.localize('CardType'),
                        field: 'connectCardTypeName'
                    },
                    {
                        name: app.localize('Redeemed'),
                        field: 'redeemed'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm\''
                    }

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;

                connectCardService.getAllConnectCard({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    connectCardTypeId: vm.cardTypeId,
                    redeemed: redeemed
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.getAll();
        }]);
})();