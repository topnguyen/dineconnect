﻿(function () {
    appModule.controller('tenant.views.connect.master.locationTag.createLocationTag', [
        '$scope', "$uibModal", '$uibModalInstance', 'abp.services.app.locationTag', 'locationTagId',
        function ($scope, $modal, $modalInstance, locationTagService, locationTagId) {
            var vm = this;

            vm.saving = false;
            vm.locationTag = null;
            vm.locationTagId = locationTagId;
            

            vm.save = function () {
                vm.saving = true;

                locationTagService.createOrUpdateLocationTag(vm.locationTag).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            

            function init() {
                locationTagService.getLocationTagForEdit({
                    id: locationTagId
                }).success(function (result) {
                    vm.locationTag = result;
                });
            }
            init();
        }
    ]);
})();