﻿(function () {
    appModule.controller('tenant.views.connect.locationTag.index', [
        '$scope', '$uibModal', '$q', 'uiGridConstants', 'abp.services.app.organizationUnit', 'abp.services.app.commonLookup', 'lookupModal', 'abp.services.app.location', 'abp.services.app.locationTag',
        function ($scope, $uibModal, $q, uiGridConstants, organizationUnitService, commonLookupService, lookupModal, locationService, locationTagService) {
            var vm = this;
            vm.locationTagId = null;
            vm.languageDescriptionType = 14;
            vm.selectedLocationTag = null;
            vm.loading = false;
            vm.rightLoading = false;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var rightrequestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.permissions = {
                manageTag: abp.auth.hasPermission('Pages.Tenant.Connect.Master.LocationTag.Create'),
                manageLocation: abp.auth.hasPermission('Pages.Tenant.Connect.Master.Location')
            };

            vm.getLocationTags = function () {
                vm.loading = true;
                locationTagService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterlocationTagText
                }).success(function (result) {
                    vm.locationTagOptions.totalItems = result.totalCount;
                    vm.locationTagOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.locationTagOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Select"),
                        maxWidth: 100,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.selectLocationTagFromGrid(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Select") + "\"><i class=\"fa fa-dashcube\"></i></button>" +
                            "</div>"
                    },
                    {
                        name: app.localize("Id"),
                        field: "id",
                        maxWidth: 70
                    },
                    {
                        name: app.localize("Code"),
                        field: "code"
                    },
                    {
                        name: app.localize("Name"),
                        field: "name"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        vm.getLocationTags();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        vm.getLocationTags();
                    });
                },
                data: []
            };

            vm.getLocationTags();

            vm.selectLocationTagFromGrid = function (myObj) {
                vm.selectedLocationTag = myObj;
                vm.locationTagId = myObj.id;
                vm.members.load();
            };

            vm.locationTag = {
                openAddOrEditModal: function (argOption) {
                    var a = vm.locationTagId;
                    var modalInstance = $uibModal.open({
                        templateUrl: '~/App/tenant/views/connect/locationTag/locationTag.cshtml',
                        controller: 'tenant.views.connect.master.locationTag.createLocationTag as vm',
                        backdrop: 'static',
                        resolve: {
                            locationTagId: function () {
                                if (argOption === 2)
                                    return vm.locationTagId;
                                else
                                    return null;
                            }
                        }
                    });

                    modalInstance.result.then(function (result) {
                        vm.getLocationTags();
                    });
                },

                delete: function (callback) {
                    abp.message.confirm(
                        app.localize('DeleteLocationTagWarning', vm.selectedLocationTag.name),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                vm.loading = true;
                                locationTagService.deleteLocationTag({
                                    id: vm.locationTagId
                                }).success(function (result) {
                                    vm.loading = false;
                                    vm.getLocationTags();
                                    vm.locationTagId = null;
                                    vm.selectedLocationTag = null;
                                });
                            }
                        }
                    );
                }
            };

            vm.members = {
                gridOptions: {
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    columnDefs: [
                        {
                            name: app.localize('Remove'),
                            enableSorting: false,
                            width: 100,
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents\">' +
                                '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.members.remove(row.entity)" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>' +
                                '</div>'
                        },
                        {
                            name: app.localize('Code'),
                            field: 'code',
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents\" title="{{row.entity.code + \')\'}}">' +
                                '  {{COL_FIELD CUSTOM_FILTERS}} &nbsp;' +
                                '</div>',
                            minWidth: 60
                        },
                        {
                            name: app.localize('Name'),
                            field: 'name',
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents\" title="{{row.entity.name + \')\'}}">' +
                                '  {{COL_FIELD CUSTOM_FILTERS}} &nbsp;' +
                                '</div>',
                            minWidth: 140
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                            if (!sortColumns.length || !sortColumns[0].field) {
                                rightrequestParams.sorting = null;
                            } else {
                                rightrequestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                            }

                            vm.members.load();
                        });
                        gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                            rightrequestParams.skipCount = (pageNumber - 1) * pageSize;
                            rightrequestParams.maxResultCount = pageSize;

                            vm.members.load();
                        });
                    },
                    data: []
                },

                load: function () {
                    if (!vm.locationTagId) {
                        vm.members.gridOptions.totalItems = 0;
                        vm.members.gridOptions.data = [];
                        return;
                    }

                    vm.rightLoading = true;
                    locationService.getLocationsForGivenLocationTagId({
                        locationTagId: vm.locationTagId,
                        skipCount: rightrequestParams.skipCount,
                        maxResultCount: rightrequestParams.maxResultCount,
                        sorting: rightrequestParams.sorting,
                        filter: vm.filterLocationText
                    }).success(function (result) {
                        console.log(result)
                        vm.members.gridOptions.totalItems = result.totalCount;
                        vm.members.gridOptions.data = result.items;
                        vm.rightLoading = false;
                    });
                },

                remove: function (user) {
                    var ouId = vm.selectedLocationTag.id;
                    if (!ouId) {
                        return;
                    }

                    abp.message.confirm(
                        app.localize('RemoveLocationFromTagWarningMessage', user.name, vm.selectedLocationTag.name),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                var locs = [];
                                locs.push(user.id);
                                vm.loading = true;
                                locationService.removeLocationFromLocationTag({
                                    locationTagId: ouId,
                                    LocationIds: locs
                                }).success(function () {
                                    abp.notify.success(app.localize('SuccessfullyRemoved'));
                                    vm.members.load();
                                    vm.loading = false;
                                });
                            }
                        }
                    );
                }
            };

            vm.AddLocation = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '~/App/tenant/views/connect/locationTag/selectLocations.cshtml',
                    controller: 'tenant.views.connect.locationTag.selectLocations as vm',
                    backdrop: 'static',
                    resolve: {
                        organizationUnit: function () {
                            return null;
                        },
                        locationTagId: function () {
                            return vm.locationTagId;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.members.load();
                });
            };

            vm.openLanguageDescriptionModal = function () {
                vm.languageDescription = {
                    id: vm.locationTagId,
                    name: vm.selectedLocationTag.name,
                    languageDescriptionType: vm.languageDescriptionType
                };
                var modalInstance = $uibModal.open({
                    templateUrl: "~/App/tenant/views/connect/languageDescription/languageDescriptionModal.cshtml",
                    controller: "tenant.views.connect.languageDescription.languageDescriptionModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        languagedescription: function () {
                            return vm.languageDescription;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });
            }

            vm.exportToExcel = function () {
                locationTagService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterlocationTagText,
                }).success(function (result) {
                    app.downloadTempFile(result);
                });
            };
        }
    ]);
})();