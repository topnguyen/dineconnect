﻿(function () {
    appModule.controller('tenant.views.connect.report.returnproduct', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.returnProductReport', "abp.services.app.tenantSettings", "abp.services.app.commonLookup",
        function ($scope, $modal, uiGridConstants, returnProductReport, tenantSettingsService, commonLookup) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.loading = false;

            vm.totalAmount = '';
            vm.totalOrders = '';
            vm.totalItems = '';

            vm.requestParams = {
                locations: '',
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    }).finally(function () {
                    });
            };

            vm.$onInit = function () {
                vm.getTagGroups();
                vm.getDecimals();
                vm.getUsers();
                vm.getLocations();
            }

            vm.gridOptions = {
                showColumnFooter: true,
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize('LocationCode'),
                        field: 'locationCode'
                    },
                    {
                        name: app.localize('Location Name'),
                        field: 'locationName'
                    },
                    {
                        name: app.localize('DateTime'),
                        field: 'dateTimeString'
                    },
                    {
                        name: app.localize('Cashier'),
                        field: 'cashier'
                    },
                    {
                        name: app.localize('TicketNumber'),
                        field: 'ticketNumber'
                    },
                    {
                        name: app.localize('CustomerName'),
                        field: 'customerName'
                    },
                    {
                        name: app.localize('MenuCode'),
                        field: 'menuCode'
                    },
                    {
                        name: app.localize('MenuName'),
                        field: 'menuName'
                    },
                    {
                        name: app.localize('Quantity'),
                        field: 'quantity',
                        cellClass: 'ui-ralign',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue()}}</div>'
                    },
                    {
                        name: app.localize('PaymentType'),
                        field: 'paymentType'
                    },
                    {
                        name: app.localize('RefundType'),
                        field: 'refundType'
                    },
                    {
                        name: app.localize('ReturnProductDate'),
                        field: 'returnProductDateString'
                    },
                    {
                        name: app.localize('ReturnProductBy'),
                        field: 'returnProductBy'
                    },
                    {
                        name: app.localize('Reason'),
                        field: 'reason'
                    },
                    {
                        name: app.localize('Detail'),
                        field: 'detail'
                    },
                    {
                        name: app.localize('Amount'),
                        field: 'amount',
                        cellClass: 'ui-ralign',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            var sortString = '';
                            sortColumns.forEach(col => {
                                sortString += col.field + ' ' + col.sort.direction + ', ';
                            });
                            vm.requestParams.sorting = sortString.slice(0, -2);
                        }
                        vm.getOrderTagReport();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;
                        vm.getOrderTagReport();
                    });
                },
                data: []
            };

            vm.toggleFooter = function () {
                $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.toggleColumnFooter = function () {
                $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.getOrderTagReport = function () {
                vm.loading = true;
                returnProductReport.getReturnProductReport({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationGroup: vm.locationGroup,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
                }).success(function (result) {
                    vm.gridOptions.totalItems = result.data.totalCount;
                    for (let i = 0; i < result.data.items.length; i++) {
                        if (result.data.items[i].price != null) {
                            result.data.items[i].price = result.data.items[i].price.toFixed(vm.decimals);
                        }
                        if (result.data.items[i].total != null) {
                            result.data.items[i].total = result.data.items[i].total.toFixed(vm.decimals);
                        }
                    }

                    vm.gridOptions.data = result.data.items;
                    vm.totalAmount = result.dashBoardOrderDto.totalAmount;
                    vm.totalOrders = result.dashBoardOrderDto.totalOrderCount;
                    vm.totalItems = result.dashBoardOrderDto.totalItemSold;

                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.exportToExcel = function (obj) {
                if (vm.gridOptions.totalItems === 0) return;
                abp.ui.setBusy("#OrderTagReportForm");
                abp.message.confirm(app.localize("AreYouSure"), app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        returnProductReport.getReturnProductExcel({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locationGroup: vm.locationGroup,
                            sorting: vm.requestParams.sorting,
                            exportOutputType: obj,
                            runInBackground: myConfirmation,
                            dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
                        })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#OrderTagReportForm");
                            });

                    });

            };

            vm.showDetails = function (order) {
                $modal.open({
                    templateUrl: '~/App/tenant/views/connect/report/exchange/exchangeDetailModal.cshtml',
                    controller: 'tenant.views.connect.report.exchangeDetailModal as vm',
                    backdrop: 'static',
                    resolve: {
                        name: function () {
                            return order.menuItemName;
                        },
                        exchange: function () {
                            return order.orderExchange2;
                        }
                    }
                });
            };

            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            }
            vm.getTagGroups = function () {
                returnProductReport.getIds({
                }).success(function (result) {
                    vm.tagGroups = result.items;
                }).finally(function (result) {
                });
            };
            // #region Builder
            vm.locations = [];
            vm.locationsCode = [];
            vm.users = [];
            $scope.builder =
                app.createBuilder(
                    {
                        condition: "AND",
                        rules: []
                    },
                    angular.fromJson
                        (
                            [
                                {
                                    "id": "LocationCode",
                                    "label": app.localize("LocationCode"),
                                    "type": "string",
                                    "operators": [
                                        "equal",
                                        "not_equal",
                                    ],
                                    plugin: 'selectize',
                                    plugin_config: {
                                        valueField: 'value',
                                        labelField: 'displayText',
                                        searchField: 'displayText',
                                        sortField: 'displayText',
                                        create: true,
                                        maxItems: 1,
                                        plugins: ['remove_button'],
                                        onInitialize: function () {
                                            var that = this;
                                            if (vm.locationsCode.length > 0) {
                                                vm.locationsCode.forEach(function (item) {
                                                    item.value = item.displayText;
                                                    that.addOption(item);
                                                });
                                            } else {
                                                commonLookup.getLocationCodeForCombobox({
                                                }).success(function (result) {
                                                    vm.locationsCode = result.items;
                                                    vm.locationsCode.forEach(function (item) {
                                                        item.value = item.displayText;
                                                        that.addOption(item);
                                                    });
                                                }).finally(function (result) {
                                                });
                                            }
                                        }
                                    },
                                    valueSetter: function (rule, value) {
                                        rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                    }
                                },
                                {
                                    "id": "LocationName",
                                    "label": app.localize("Location Name"),
                                    "type": "string",
                                    "operators": [
                                        "equal",
                                        "not_equal",
                                    ],
                                    plugin: 'selectize',
                                    plugin_config: {
                                        valueField: 'value',
                                        labelField: 'displayText',
                                        searchField: 'displayText',
                                        sortField: 'displayText',
                                        create: true,
                                        maxItems: 1,
                                        plugins: ['remove_button'],
                                        onInitialize: function () {
                                            var that = this;
                                            if (vm.locations.length > 0) {
                                                vm.locations.forEach(function (item) {
                                                    item.value = item.displayText;
                                                    that.addOption(item);
                                                });
                                            } else {
                                                commonLookup.getLocationForCombobox({
                                                }).success(function (result) {
                                                    vm.locations = result.items;
                                                    vm.locations.forEach(function (item) {
                                                        item.value = item.displayText;
                                                        that.addOption(item);
                                                    });
                                                }).finally(function (result) {
                                                });
											}
                                          
                                        }
                                    },
                                    valueSetter: function (rule, value) {
                                        rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                    }
                                },
                                {
                                    "id": "Cashier",
                                    "label": app.localize("Cashier"),
                                    "type": "string",
                                    "operators": [
                                        "equal",
                                        "not_equal",
                                    ],
                                    type: 'string',
                                    plugin: 'selectize',
                                    plugin_config: {
                                        valueField: 'value',
                                        labelField: 'displayText',
                                        searchField: 'displayText',
                                        sortField: 'displayText',
                                        create: true,
                                        maxItems: 1,
                                        plugins: ['remove_button'],
                                        onInitialize: function () {
                                            var that = this;
                                            if (vm.users.length > 0) {
                                                vm.users.forEach(function (item) {
                                                    item.value = item.displayText;
                                                    that.addOption(item);
                                                });
                                            } else {
                                                commonLookup.getUserForCombobox({
                                                }).success(function (result) {
                                                    vm.users = result.items;
                                                    vm.users.forEach(function (item) {
                                                        item.value = item.displayText;
                                                        that.addOption(item);
                                                    });
                                                }).finally(function (result) {
                                                });
                                            }
                                        }
                                    },
                                    valueSetter: function (rule, value) {
                                        rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                    }
                                },
                                {
                                    "id": "TicketNumber",
                                    "label": app.localize("TicketNumber"),
                                    "type": "string"
                                },
                                {
                                    "id": "CustomerName",
                                    "label": app.localize("CustomerName"),
                                    "type": "string"
                                },
                                {
                                    "id": "MenuCode",
                                    "label": app.localize("MenuCode"),
                                    "type": "string"
                                },     {
                                    "id": "MenuName",
                                    "label": app.localize("MenuName"),
                                    "type": "string"
                                },
                                {
                                    "id": "Quantity",
                                    "label": app.localize("Quantity"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                }, {
                                    "id": "PaymentType",
                                    "label": app.localize("PaymentType"),
                                    "type": "string"
                                },{
                                    "id": "ReturnProductBy",
                                    "label": app.localize("ReturnProductBy"),
                                    "type": "string"
                                }, {
                                    "id": "Reason",
                                    "label": app.localize("Reason"),
                                    "type": "string"
                                },
                                {
                                    "id": "Amount",
                                    "label": app.localize("Amount"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                }
                            ]
                        )
                );
            vm.getLocations = function () {
                commonLookup.getLocationCodeForCombobox({
                }).success(function (result) {
                    vm.locationsCode = result.items;
                    console.log('code', vm.locationsCode);
                }).finally(function (result) {
                });

                commonLookup.getLocationForCombobox({
                }).success(function (result) {
                    vm.locations = result.items;
                    console.log(vm.locations);
                }).finally(function (result) {
                });
            };
            vm.getUsers = function () {
                commonLookup.getUserForCombobox({
                }).success(function (result) {
                    vm.users = result.items;
                }).finally(function (result) {
                });
            };

            vm.clear = function () {
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };
                $scope.builder.builder.reset();
                vm.getOrderTagReport();
            }


                // #endregion
        }
    ]);
})();