﻿(function () {
    appModule.controller("tenant.views.connect.report.salespromotionbypromotion.index",
        [
            "$scope", "$uibModal", "uiGridConstants", "abp.services.app.salesPromotionReport", "abp.services.app.connectLookup",
            "abp.services.app.customerReport", "abp.services.app.tenantSettings", "abp.services.app.commonLookup", 'abp.services.app.transactionType', 'lookupModal',
            'abp.services.app.department',
            'abp.services.app.paymentType',
            'uiGridGroupingConstants', 'abp.services.app.category', 'abp.services.app.productGroup',
            function ($scope,
                $modal,
                uiGridConstants,
                salesPromotionReportAppService,
                connectLookupService,
                crService,
                tenantSettingsService,
                commonLookup,
                transactionService,
                lookupModal,
                departmentService,
                paymentTypeService,
                uiGridGroupingConstants,
                categoryService,
                productGroupService
            ) {
                var vm = this;
                $scope.settings = {
                    dropdownToggleState: false,
                    time: {
                        fromHour: "06",
                        fromMinute: "00",
                        toHour: "23",
                        toMinute: "59   "
                    },
                    noRange: false,
                    format: 24,
                    noValidation: true
                };
                vm.dataChart = [];
                vm.payments = [];
                vm.transactions = [];
                vm.currencyText = abp.features.getValue("DinePlan.DineConnect.Connect.Currency");
                vm.franchise = abp.features.getValue("DinePlan.DineConnect.Connect.Franchise") == "true";
                vm.tallyAvailable = abp.setting.getBoolean("App.House.TallyIntegrationFlag");
                vm.mycustomer = abp.features.getValue("DinePlan.DineConnect.Connect.Customer");


                vm.loading = false;
                vm.advancedFiltersAreShown = false;
                $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
                $scope.format = $scope.formats[0];

                vm.totalTicketAmount = "";
                vm.totalTickets = "";
                vm.totalOrders = "";
                vm.totalItems = "";
                vm.requestParams = {
                    locations: "",
                    payments: [],
                    locationSelect: "",
                    departmentSelect: "",
                    transactions: [],
                    ticketTags: "",
                    terminalName: "",
                    lastModifiedUserName: "",
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null,
                    userId: abp.session.userId,
                    credit: false,
                    refund: false,
                    ticketNo: "",
                    view: "Top",
                    viewNumber: 10,
                    typeOfChart: "column",
                    isAddOns: false,
                    isCombo: false,
                    days: "",

                };
                vm.paymentsInput = [];
                vm.departmentsInput = [];
                vm.transactionsInput = [];
                var todayAsString = moment().format("YYYY-MM-DD");
                vm.dateRangeOptions = app.createDateRangePickerOptions();
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };

                vm.titleChart = app.localize("TopItemSalesByAmount");

                $scope.$on("$viewContentLoaded",
                    function () {
                        App.initAjax();
                    });



                vm.getEditionValue = function (item) {
                    return item.displayText;
                };

                vm.decimals = null;
                vm.getDecimals = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.decimals = result.connect.decimals;
                        }).finally(function () {
                        });
                };

                vm.lstView = [
                    { value: "Top", displayText: "Top" },
                    { value: "Down", displayText: "Down" },
                ];

                vm.lstTypeOfChart = [
                    { value: "column", displayText: "ColumnChart" },
                    { value: "bar", displayText: "BarChart" },
                    { value: "pie", displayText: "PieChart" },
                ];

                vm.lstDayOfWeek = [
                    { value: undefined, displayText: "All" },
                    { value: 1, displayText: "Monday" },
                    { value: 2, displayText: "Tuesday" },
                    { value: 3, displayText: "Wednesday" },
                    { value: 4, displayText: "Thursday" },
                    { value: 5, displayText: "Friday" },
                    { value: 6, displayText: "Saturday" },
                    { value: 0, displayText: "Sunday" },
                ];

                vm.$onInit = function () {
                    localStorage.clear();
                    vm.getDecimals();
                    vm.getTransactions();
                    vm.getPayments();
                    vm.getDepartments();
                    vm.getLocations();
                    vm.getLocationsCode();
                    vm.getUsers();
                };

                vm.openGroupMaster = function () {
                    openCreateGroupMaster(null);
                };
                function openCreateGroupMaster() {
                    var modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/connect/category/productGroupTreeModal.cshtml',
                        controller: 'tenant.views.connect.category.productGroupTreeModal as vm',
                        backdrop: 'static',
                        resolve: {
                            category: function () {
                                return vm.category;
                            }
                        }
                    });
                    modalInstance.result
                        .then(function (result) {
                            vm.category.productGroupId = result.selectedOu.id;
                            vm.category.groupName = result.selectedOu.name;
                            vm.category.groupCode = result.selectedOu.code;
                        }).finally(function () {
                        });
                }
                function fillDropDownGroup() {
                    productGroupService.getProductGroupNames({})
                        .success(function (result) {
                            vm.productGroups = result.items;
                            if (selectedGroupId != null)
                                vm.category.productGroupId = selectedGroupId;
                        });
                }
                // #region Grid

                vm.disableExport = false;


                // #endregion
                // #region Location

                vm.intiailizeOpenLocation = function () {
                    vm.locationGroup = app.createLocationInputForSearch();
                };
                vm.intiailizeOpenLocation();

                vm.openLocation = function () {
                    const modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };
                vm.showDetails = function (ticket) {
                    $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/tickets/ticket/ticketModal.cshtml",
                        controller: "tenant.views.connect.report.ticketModal as vm",
                        backdrop: "static",
                        resolve: {
                            ticket: function () {
                                return ticket.id;
                            }
                        }
                    });
                };

                // #endregion
                // #region Exports
                vm.getSalesPromotionInput = function (confirmation, obj) {
                    return {
                        startDate: moment(vm.dateRangeModel.startDate).lang("en").format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).lang("en").format($scope.format),
                        locationGroup: vm.locationGroup,
                        payments: vm.paymentsInput,
                        departments: vm.departmentsInput,
                        ticketTags: vm.requestParams.ticketTags,
                        transactions: vm.transactionsInput,
                        terminalName: vm.requestParams.terminalName,
                        lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                        skipCount: vm.requestParams.skipCount,
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: "Amount",
                        credit: vm.requestParams.credit,
                        refund: vm.requestParams.refund,
                        ticketNo: vm.requestParams.ticketNo,
                        runInBackground: confirmation,
                        exportOutputType: obj,
                        viewNumber: vm.requestParams.viewNumber,
                        view: vm.requestParams.view,
                        isCombo: vm.requestParams.isCombo,
                        isAddOns: vm.requestParams.isAddOns,
                        days: vm.requestParams.days,
                        locationSelect: vm.requestParams.locationSelect,
                        departmentSelect: vm.requestParams.departmentSelect,
                        startHour: $scope.settings.time.fromHour,
                        endHour: $scope.settings.time.toHour,
                        startMinute: $scope.settings.time.fromMinute,
                        endMinute: $scope.settings.time.toMinute,
                        listMaterial: vm.lstMaterial,
                        dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                    };
                };

                vm.getExportInput = function (confirmation, obj) {
                    var input = vm.getSalesPromotionInput;
                    input.runInBackground = confirmation;
                    input.exportOutputType = obj;
                    return input;
                };
                vm.exportToExcel = function (exportType) {
                    abp.ui.setBusy("#MyLoginForm");
                    abp.message.confirm(app.localize("AreYouSure"),
                        app.localize("RunInBackground"),
                        function (isConfirmed) {
                            var myConfirmation = false;
                            if (isConfirmed) {
                                myConfirmation = true;
                                abp.notify.info(app.localize("RunInBackground"));
                            }
                            vm.disableExport = true;
                            var input = vm.getSalesPromotionInput(myConfirmation, exportType);
                            salesPromotionReportAppService.getPromotionByPromotionToExport(input)
                                .success(function (result) {
                                    if (result != null) {
                                        app.downloadTempFile(result);
                                    }
                                })
                                .finally(function () {
                                    abp.ui.clearBusy("#MyLoginForm");
                                    vm.disableExport = false;
                                });
                        });
                };
                vm.clear = function () {
                    vm.dateRangeModel = {
                        startDate: todayAsString,
                        endDate: todayAsString
                    };
                    $scope.builder.builder.reset();

                    vm.requestParams.transactions = [];
                    vm.requestParams.payments = [];
                    vm.requestParams.departments = [];

                    vm.getAll();
                }
                // #region Builder
                $scope.builder =
                    app.createBuilder(
                        {
                            condition: "AND",
                            rules: [
                            ]
                        },
                        angular.fromJson
                            (
                                [
                                    {
                                        "id": "PromotionName",
                                        "label": app.localize("Promotion"),
                                        "type": "string",
                                        "operators": [
                                            "equal",
                                            "not_equal",
                                        ],
                                        plugin: 'selectize',
                                        plugin_config: {
                                            valueField: 'value',
                                            labelField: 'displayText',
                                            searchField: 'displayText',
                                            sortField: 'displayText',
                                            create: true,
                                            maxItems: 1,
                                            plugins: ['remove_button'],
                                            onInitialize: function () {
                                                var that = this;
                                                if (vm.promotions.length > 0) {
                                                    vm.promotions.forEach(function (item) {
                                                        item.value = item.displayText;
                                                        that.addOption(item);
                                                    });
                                                } else {
                                                    connectLookupService.getPromotions({
                                                    }).success(function (result) {
                                                        vm.promotions = result.items;
                                                        vm.promotions.forEach(function (item) {
                                                            item.value = item.displayText;
                                                            that.addOption(item);
                                                        });
                                                    }).finally(function (result) {
                                                    });
                                                }
                                            }
                                        },
                                        valueSetter: function (rule, value) {
                                            rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                        }
                                    },
                                    {
                                        "id": "PlantCode",
                                        "label": app.localize("PlantCode"),
                                        "type": "string",
                                        "operators": [
                                            "equal",
                                            "not_equal",
                                        ],
                                        plugin: 'selectize',
                                        plugin_config: {
                                            valueField: 'value',
                                            labelField: 'displayText',
                                            searchField: 'displayText',
                                            sortField: 'displayText',
                                            create: true,
                                            maxItems: 1,
                                            plugins: ['remove_button'],
                                            onInitialize: function () {
                                                var that = this;
                                                if (vm.lstLocationCode.length > 0) {
                                                    vm.lstLocationCode.forEach(function (item) {
                                                        item.value = item.displayText;
                                                        that.addOption(item);
                                                    });
                                                } else {
                                                    connectLookupService.getLocationCodeForCombobox({
                                                    }).success(function (result) {
                                                        vm.lstLocationCode = result.items;
                                                        vm.lstLocationCode.forEach(function (item) {
                                                            item.value = item.displayText;
                                                            that.addOption(item);
                                                        });
                                                    }).finally(function (result) {
                                                    });
                                                }
                                            }
                                        },
                                        valueSetter: function (rule, value) {
                                            rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                        }
                                    },
                                    {
                                        "id": "PlantName",
                                        "label": app.localize("PlantName"),
                                        "type": "string",
                                        "operators": [
                                            "equal",
                                            "not_equal",
                                        ],
                                        plugin: 'selectize',
                                        plugin_config: {
                                            valueField: 'value',
                                            labelField: 'displayText',
                                            searchField: 'displayText',
                                            sortField: 'displayText',
                                            create: true,
                                            maxItems: 1,
                                            plugins: ['remove_button'],
                                            onInitialize: function () {
                                                var that = this;
                                                if (vm.lstLocation.length > 0) {
                                                    vm.lstLocation.forEach(function (item) {
                                                        item.value = item.displayText;
                                                        that.addOption(item);
                                                    });
                                                } else {
                                                    connectLookupService.getLocationForCombobox({
                                                    }).success(function (result) {
                                                        vm.lstLocation = result.items;
                                                        vm.lstLocation.forEach(function (item) {
                                                            item.value = item.displayText;
                                                            that.addOption(item);
                                                        });
                                                    }).finally(function (result) {
                                                    });
                                                }
                                            }
                                        },
                                        valueSetter: function (rule, value) {
                                            rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                        }
                                    },
                                    {
                                        "id": "NumberItemQuantity",
                                        "label": app.localize("Quantity"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                    {
                                        "id": "BillAmount",
                                        "label": app.localize("NumberOfBill"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                    {
                                        "id": "DiscountValue",
                                        "label": app.localize("DiscountValue"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "step": 0.01
                                        }
                                    },
                                    {
                                        "id": "DiscountValuePercent",
                                        "label": app.localize("DiscountValuePercent"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                    {
                                        "id": "AverageDiscountPerItem",
                                        "label": app.localize("AverageDiscountPerItem"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                    {
                                        "id": "AverageDiscountPerBill",
                                        "label": app.localize("AverageDiscountPerBill"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                ]
                            )
                    );

                // #region LocalStorage
                vm.getTransactions = function () {
                    connectLookupService.getTransactionTypesForCombobox({
                    }).success(function (result) {
                        localStorage.transactionTypes = JSON.stringify(result.items);
                    }).finally(function (result) {
                    });
                };
                vm.getPayments = function () {
                    connectLookupService.getPaymentTypes({
                    }).success(function (result) {
                        localStorage.paymentTypes = JSON.stringify(result.items);
                    }).finally(function (result) {
                    });
                };
                vm.lstdepartment = [];
                vm.getDepartments = function () {
                    connectLookupService.getDepartments({
                    }).success(function (result) {
                        localStorage.departments = JSON.stringify(result.items);
                        vm.departments = result.items;
                        var firstItem = { value: undefined, displayText: "All" }
                        vm.departments.unshift(firstItem);
                    }).finally(function (result) {
                    });
                };

                vm.lstLocation = [];
                vm.getLocations = function () {
                    commonLookup.getLocationForCombobox({
                    }).success(function (result) {
                        var firstItem = { value: undefined, displayText: "All" }
                        vm.lstLocation = result.items;
                        vm.lstLocation.unshift(firstItem);
                    }).finally(function (result) {
                    });
                };

                vm.lstLocationCode = [];
                vm.getLocationsCode = function () {
                    commonLookup.getLocationCodeForCombobox({
                    }).success(function (result) {
                        vm.lstLocationCode = JSON.stringify(result.items);
                        var firstItem = { value: undefined, displayText: "All" }
                        vm.lstLocationCode = result.items;
                        vm.lstLocationCode.unshift(firstItem);
                    }).finally(function (result) {
                    });
                };
                vm.getUsers = function () {
                    commonLookup.getUserForCombobox({
                    }).success(function (result) {
                        vm.users = result.items;
                    }).finally(function (result) {
                    });
                };
                vm.getUsers = function () {
                    commonLookup.getUserForCombobox({
                    }).success(function (result) {
                        vm.users = result.items;
                    }).finally(function (result) {
                    });
                };

                vm.promotions = [];
                vm.promotions = function () {
                    connectLookupService.getPromotions({
                    }).success(function (result) {
                        localStorage.promotions = JSON.stringify(result.items);
                        vm.promotions = result.items;
                    }).finally(function (result) {
                    });
                };


                vm.getAll = function () {
                    vm.loading = true;
                    salesPromotionReportAppService.getPromotionByPromotion(vm.getSalesPromotionInput())
                        .success(function (result) {
                            console.log("result", result);
                            console.log("result item", result.listItem);
                            vm.data = result;
                        }).finally(function () {
                            vm.loading = false;
                        });
                };

                $scope.sum = function (items, prop) {
                    return items.reduce(function (a, b) {
                        return a + b[prop];
                    }, 0);
                };



                vm.selectDepartment = function () {

                    var requestParams = {
                        skipCount: 0,
                        maxResultCount: app.consts.grid.defaultPageSize,
                        sorting: null
                    };

                    var dataOptions = {
                        title: app.localize('Select') + " " + app.localize('Department'),
                        serviceMethod: departmentService.getAll,
                        selectedData: vm.requestParams.departments,
                        requestParams: requestParams
                    };

                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/common/selectModal.cshtml",
                        controller: "tenant.views.connect.report.selectModal as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            dataOptions: function () {
                                return dataOptions;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        if (result.length > 0) {
                            vm.requestParams.departments = result;
                            vm.departmentsInput = vm.requestParams.departments.map(el => {
                                var el = { 'Value': el.id, 'DisplayText': el.name };
                                return el;
                            });
                        }
                    });
                };

                vm.resetDepartmentSelectedItem = function () {
                    vm.requestParams.departments = [];
                };

                vm.selectPayments = function () {

                    var requestParams = {
                        skipCount: 0,
                        maxResultCount: app.consts.grid.defaultPageSize,
                        sorting: null
                    };

                    var dataOptions = {
                        title: app.localize('Select') + ' ' + app.localize('Payment'),
                        serviceMethod: paymentTypeService.getAll,
                        selectedData: vm.requestParams.payments,
                        requestParams: requestParams
                    };

                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/common/selectModal.cshtml",
                        controller: "tenant.views.connect.report.selectModal as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            dataOptions: function () {
                                return dataOptions;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        if (result.length > 0) {
                            vm.requestParams.payments = result;
                            vm.paymentsInput = vm.requestParams.payments.map(el => el.id);
                        }
                    });
                },

                    vm.resetPaymentSelectedItem = function () {
                        vm.requestParams.payments = [];
                    };

                vm.selectTransactions = function () {
                    var requestParams = {
                        skipCount: 0,
                        maxResultCount: app.consts.grid.defaultPageSize,
                        sorting: null
                    };

                    var dataOptions = {
                        title: app.localize('Select') + ' ' + app.localize('Transaction'),
                        serviceMethod: transactionService.getAll,
                        selectedData: vm.requestParams.transactions,
                        requestParams: requestParams
                    };

                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/common/selectModal.cshtml",
                        controller: "tenant.views.connect.report.selectModal as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            dataOptions: function () {
                                return dataOptions;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        if (result.length > 0) {
                            vm.requestParams.transactions = result;
                            vm.transactionsInput = vm.requestParams.transactions.map(el => {
                                var el = { 'Value': el.id, 'DisplayText': el.name };
                                return el;
                            });
                        }
                    });
                };

                vm.resetTransactionSelectedItem = function () {
                    vm.requestParams.transactions = [];
                };
                function init() {
                    fillDropDownGroup();
                }
                init();
                vm.openModalChart = function (data, type) {
                    var dataChart = {
                        typeChart: type,
                        listChart: data.listItem.dataChart,
                        title: app.localize('TopDownSellReportByAmountPlant')
                    };
                    const modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/topdownsell/sharecomponent/chartmodal/top-down-chart-modal.cshtml",
                        controller: "tenant.views.connect.report.topdownsell.sharecomponent.chartmodal as vm",
                        backdrop: "static",
                        keyboard: false,
                        windowClass: 'app-modal-window',
                        resolve: {
                            dataChart: function () {
                                return dataChart;
                            }
                        }
                    });
                }
                vm.lstMaterial = [];
                vm.openGroupMaster = function () {
                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/topdownsell/sharecomponent/materialgroup/material-group.cshtml",
                        controller: "tenant.views.connect.report.topdownsell.sharecomponent.materialgroup as vm",
                        backdrop: 'static',
                        resolve: {
                            category: function () {
                                return vm.lstMaterial;
                            }
                        }
                    });
                    modalInstance.result
                        .then(function (result) {
                            vm.lstMaterial = result;
                        }).finally(function () {
                        });
                };
            }
        ]);
})();