﻿(function () {
	appModule.controller("tenant.views.connect.report.discount", [
		"$scope", "$uibModal", "uiGridConstants", "abp.services.app.ticketPromotionReport", 'appSession', "abp.services.app.tenantSettings", 'uiGridGroupingConstants', "abp.services.app.promotion", "abp.services.app.commonLookup",
		function ($scope, $modal, uiGridConstants, connectService, appSession, tenantSettingsService, uiGridGroupingConstants, promotionService, commonLookup) {
			var vm = this;

			$scope.$on("$viewContentLoaded", function () {
				App.initAjax();
			});
			vm.locations = [];
			vm.currencyText = abp.features.getValue("DinePlan.DineConnect.Connect.Currency");
			vm.loading = false;
			vm.advancedFiltersAreShown = false;
			$scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
			$scope.format = $scope.formats[0];

			vm.decimals = null;
			vm.getDecimals = function () {
				tenantSettingsService.getAllSettings()
					.success(function (result) {
						vm.decimals = result.connect.decimals;
					});
			};

			vm.$onInit = function () {
				localStorage.clear();
				vm.getDecimals();
				vm.getLocations();
				vm.getUsers();
			};

			//vm.location = appSession.user.locationRefId;
			vm.requestParams = {
				locations: '',
				promotions: '',
				void: false,
				gift: false,
				refund: false,
				payments: "",
				department: "",
				transactions: "",
				skipCount: 0,
				maxResultCount: app.consts.grid.defaultPageSize,
				sorting: null,
				terminalName: "",
				lastModifiedUserName: "",
			};

			var todayAsString = moment().format("YYYY-MM-DD");
			vm.dateRangeOptions = app.createDateRangePickerOptions();
			vm.dateRangeModel = {
				startDate: todayAsString,
				endDate: todayAsString
			};
			vm.getEditionValue = function (item) {
				return item.displayText;
			};

			vm.exportToSummary = function (mode) {
				if (vm.gridOptions.totalItems === 0) return;
				abp.ui.setBusy("#MyLoginForm");
				abp.message.confirm(app.localize("AreYouSure"),
					app.localize("RunInBackground"),
					function (isConfirmed) {
						var myConfirmation = false;
						if (isConfirmed) {
							myConfirmation = true;
						}
						vm.disableExport = true;
						connectService.getDiscountDetailExport({
							startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
							endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
							locationGroup: vm.locationGroup,
							payments: vm.requestParams.payments,
							department: vm.requestParams.department,
							transactions: vm.requestParams.transactions,
							promotions: vm.requestParams.promotions,
							void: vm.requestParams.void,
							gift: vm.requestParams.gift,
							comp: vm.requestParams.comp,
							refund: vm.requestParams.refund,
							sorting: vm.requestParams.sorting,
							maxResultCount: vm.gridOptions.totalItems,
							exportOutputType: mode,
							runInBackground: myConfirmation,
							terminalName: vm.requestParams.terminalName,
							lastModifiedUserName: vm.requestParams.lastModifiedUserName,
						})
							.success(function (result) {
								if (result != null) {
									app.downloadTempFile(result);
								}
							}).finally(function () {
								vm.disableExport = false;
								abp.ui.clearBusy("#MyLoginForm");
							});
					});
			};
			vm.exportExcelDetail = function (mode) {
				if (vm.gridOptions.totalItems === 0) return;
				abp.ui.setBusy("#MyLoginForm");
				abp.message.confirm(app.localize("AreYouSure"),
					app.localize("RunInBackground"),
					function (isConfirmed) {
						var myConfirmation = false;
						if (isConfirmed) {
							myConfirmation = true;
						}
						vm.disableExport = true;
						connectService.getTicketPromotionDetailExport({
							startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
							endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
							locationGroup: vm.locationGroup,
							payments: vm.requestParams.payments,
							department: vm.requestParams.department,
							transactions: vm.requestParams.transactions,
							promotions: vm.requestParams.promotions,
							void: vm.requestParams.void,
							gift: vm.requestParams.gift,
							comp: vm.requestParams.comp,
							refund: vm.requestParams.refund,
							sorting: vm.requestParams.sorting,
							maxResultCount: vm.gridOptions.totalItems,
							exportOutputType: mode,
							runInBackground: myConfirmation,
							terminalName: vm.requestParams.terminalName,
							lastModifiedUserName: vm.requestParams.lastModifiedUserName,
						})
							.success(function (result) {
								if (result != null) {
									app.downloadTempFile(result);
								}
							}).finally(function () {
								vm.disableExport = false;
								abp.ui.clearBusy("#MyLoginForm");
							});
					});
			};

			vm.gridOptions = {
				showColumnFooter: true,
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				appScopeProvider: vm,
				columnDefs: [
					{
						name: app.localize('Location'),
						field: 'location',
						minWidth: 350,
						grouping: { groupPriority: 1 },
						sort: { priority: 1, direction: 'asc' },
						cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>',
						footerCellTemplate: '<div class="ui-grid-cell-contents" >' + app.localize("Total") + '</div>'
					},
					{
						name: app.localize("Department"),
						field: "departmentName",
						minWidth: 200,
						grouping: { groupPriority: 1 },
						sort: { priority: 1, direction: 'asc' },
						cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>'
					},
					{
						name: app.localize("CodeDiscount"),
						field: "codeDiscount",
						width: 150
					},
					{
						name: app.localize("DiscountName"),
						field: "discountName",
						minWidth: 150
					},
					{
						name: app.localize("Note"),
						field: "note",
						minWidth: 150
					},
					{
						name: app.localize("Qty"),
						field: "quantity",
						cellClass: "ui-ralign",
						width: 100,
						treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
						customTreeAggregationFinalizerFn: function (aggregation) {
							aggregation.rendered = aggregation.value;
						},
						footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue()}}</div>'
					},
					{
						name: app.localize("%Qty"),
						field: "quantity",
						cellClass: "ui-ralign",
						width: 100,
						cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.getQtyPercent(row.entity.quantity)}}</div>',
						footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >{{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
					},
					{
						name: app.localize("Total"),
						field: "total",
						width: 180,
						cellClass: "ui-ralign",
						treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
						customTreeAggregationFinalizerFn: function (aggregation) {
							aggregation.rendered = aggregation.value.toFixed(vm.decimals);
						},
						footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >{{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
					},
					{
						name: app.localize("%Ttl"),
						field: "total",
						cellClass: "ui-ralign",
						width: 100,
						cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.getTtlPercent(row.entity.total)}}</div>',
						footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >{{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
					}
				],
				onRegisterApi: function (gridApi) {
				},
				data: []
			};

			vm.getQtyPercent = function (value) {
				if (value) {
					return (value / vm.totalQuantity * 100).toFixed(vm.decimals) + '%';
				}
				return null;//.quantity / vm.totalQuantity * 100).toFixed(vm.decimals) + '%';
			};

			vm.getTtlPercent = function (value) {
				if (value) {
					return (value / vm.totalAmount * 100).toFixed(vm.decimals) + '%';
				}
				return null;//.quantity / vm.totalQuantity * 100).toFixed(vm.decimals) + '%';
			};

			vm.toggleFooter = function () {
				$scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
				$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
			};

			vm.toggleColumnFooter = function () {
				$scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
				$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
			};

			vm.getOrders = function () {
				vm.loading = true;
				vm.totalQuantity = 0;
				vm.totalAmount = 0;

				connectService.getDiscountDetails({
					startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
					endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
					locationGroup: vm.locationGroup,
					payments: vm.requestParams.payments,
					department: vm.requestParams.department,
					transactions: vm.requestParams.transactions,
					promotions: vm.requestParams.promotions,
					void: vm.requestParams.void,
					gift: vm.requestParams.gift,
					comp: vm.requestParams.comp,
					refund: vm.requestParams.refund,
					skipCount: vm.requestParams.skipCount,
					maxResultCount: vm.requestParams.maxResultCount,
					sorting: vm.requestParams.sorting,
					terminalName: vm.requestParams.terminalName,
					lastModifiedUserName: vm.requestParams.lastModifiedUserName,
					dynamicFilter: angular.toJson($scope.builder.builder.getRules())
				}).success(function (result) {
					for (let i = 0; i < result.items.length; i++) {
						vm.totalAmount += result.items[i].total && result.items[i].total;
						vm.totalQuantity += result.items[i].quantity && result.items[i].quantity;
						result.items[i].total = result.items[i].total && result.items[i].total.toFixed(vm.decimals);
					}

					vm.gridOptions.data = result.items;
				}).finally(function () {
					vm.loading = false;
				});
			};

			vm.intiailizeOpenLocation = function () {
				vm.locationGroup = app.createLocationInputForSearch();
			};
			vm.intiailizeOpenLocation();

			vm.openLocation = function () {
				var modalInstance = $modal.open({
					templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
					controller: "tenant.views.connect.location.select.location as vm",
					backdrop: "static",
					keyboard: false,
					resolve: {
						location: function () {
							return vm.locationGroup;
						}
					}
				});
				modalInstance.result.then(function (result) {
					vm.locationGroup = result;
				});
			};

			vm.openforPromotion = function (product) {
				vm.lgroup = {
					locations: [],
					groups: [],
					group: false,
					single: false,
					service: promotionService.getPromotions
				};

				var modalInstance = $modal.open({
					templateUrl: "~/App/common/views/lookup/select.cshtml",
					controller: "tenant.views.common.lookup.select as vm",
					backdrop: "static",
					keyboard: false,
					resolve: {
						location: function () {
							return vm.lgroup;
						}
					}
				});
				modalInstance.result.then(function (result) {
					vm.selectedPromotions = '';
					vm.requestParams.promotions = result.locations;
					if (result.locations.length > 0) {
						var selectedPros = [];
						angular.forEach((result.locations), function (item) {
							selectedPros.push(item.name);
						});

						vm.selectedPromotions = selectedPros.join(', ');
					}
				});
			};

			// #region Builder

			$scope.builder =
				app.createBuilder(
					{
						condition: "AND",
						rules: []
					},
					angular.fromJson
						(
							[
								{
									"id": "LocationID",
									"label": app.localize("Location"),
									"type": "integer",
									"operators": [
										"equal",
										"not_equal",
									],
									plugin: 'selectize',
									plugin_config: {
										valueField: 'value',
										labelField: 'displayText',
										searchField: 'displayText',
										sortField: 'displayText',
										create: true,
										maxItems: 1,
										plugins: ['remove_button'],
										onInitialize: function () {
											var that = this;
											if (localStorage.locationsCode) {
												JSON.parse(localStorage.locationsCode).forEach(function (item) {
													that.addOption(item);
												});
											}
										}
									},
									valueSetter: function (rule, value) {
										rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
									}
								},
								{
									"id": "departmentName",
									"label": app.localize("Department"),
									"type": "string"
								},
								{
									"id": "CodeDiscount",
									"label": app.localize("CodeDiscount"),
									"operators": [
										"equal",
										"less",
										"less_or_equal",
										"greater",
										"greater_or_equal"
									],
									"type": "integer"
								},
								{
									"id": "DiscountName",
									"label": app.localize("DiscountName"),
									"type": "string"
								},
								{
									"id": "Note",
									"label": app.localize("Note"),
									"type": "string"
								},
								{
									"id": "Quantity",
									"label": app.localize("Quantity"),
									"operators": [
										"equal",
										"less",
										"less_or_equal",
										"greater",
										"greater_or_equal"
									],
									"type": "double",
									"validation": {
										"min": 0,
										"step": 1
									}
								},
								{
									"id": "Total",
									"label": app.localize("Total"),
									"operators": [
										"equal",
										"less",
										"less_or_equal",
										"greater",
										"greater_or_equal"
									],
									"type": "double",
									"validation": {
										"min": 0,
										"step": 1
									}
								},
							]
						)
				);

			vm.getLocations = function () {
				commonLookup.getLocationCodeForCombobox({
				}).success(function (result) {
					localStorage.locationsCode = JSON.stringify(result.items);
				}).finally(function (result) {
				});

				commonLookup.getLocationForCombobox({
				}).success(function (result) {
					localStorage.locations = JSON.stringify(result.items);
				}).finally(function (result) {
				});
			};
			vm.getUsers = function () {
				commonLookup.getUserForCombobox({
				}).success(function (result) {
					localStorage.users = JSON.stringify(result.items);
				}).finally(function (result) {
				});
			};

			vm.clear = function () {
				vm.dateRangeModel = {
					startDate: todayAsString,
					endDate: todayAsString
				};
				$scope.builder.builder.reset();
				vm.getOrders();
			}

			// #endregion
		}
	]);
})();