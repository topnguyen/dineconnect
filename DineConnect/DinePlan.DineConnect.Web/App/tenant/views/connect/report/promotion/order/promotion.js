﻿(function () {
    appModule.controller('tenant.views.connect.report.tickets.promotion', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.orderPromotionReport', 'abp.services.app.connectLookup', "abp.services.app.tenantSettings", "abp.services.app.promotion", "abp.services.app.commonLookup", 'abp.services.app.department', 
        function ($scope, $modal, uiGridConstants, orderPromotionReportService, lookupService, tenantSettingsService, 
            promotionService, commonService, departmentService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.locations = [];
            vm.payments = [];
            vm.transactions = [];
            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.loading = false;
            vm.advancedFiltersAreShown = false;

            vm.totalAmount = '';
            vm.totalOrders = '';
            vm.totalItems = '';

            vm.selectedPromotions = '';

            vm.requestParams = {
                locations: '',
                promotions: '',
                void: false,
                gift: false,
                refund: false,
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    }).finally(function () {
                    });
            };

            vm.$onInit = function () {
                localStorage.clear();
                vm.getDecimals();
                vm.getMenuItems();
                vm.getDepartments();
                vm.getLocations();
                vm.getUsers();
            };

            vm.gridOptions = {
                showColumnFooter: true,
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: 'Actions',
                        enableSorting: false,
                        width: 50,
                        headerCellTemplate: '<span></span>',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.showDetails(row.entity)"><i class="fa fa-search"></i></button>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Ticket'),
                        field: 'ticketId'
                    },
                    {
                        name: app.localize('Location'),
                        field: 'locationName'
                    },
                    {
                        name: app.localize('MenuItem'),
                        field: 'menuItemName'
                    },
                    {
                        name: app.localize('Quantity'),
                        field: 'quantity',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Price'),
                        field: 'price',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('PromoAmount'),
                        field: 'promotionAmount',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Note'),
                        field: 'promotionNote',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('User'),
                        field: 'creatingUserName',
                        cellClass: 'ui-ralign'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getOrders();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;
                        vm.getOrders();
                    });
                },
                data: []
            };

            vm.toggleFooter = function () {
                $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.toggleColumnFooter = function () {
                $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };
            vm.getInput = function (confirmation, obj) {
                return {
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationGroup: vm.locationGroup,
                    promotions: vm.requestParams.promotions,
                    void: vm.requestParams.void,
                    gift: vm.requestParams.gift,
                    comp: vm.requestParams.comp,
                    refund: vm.requestParams.refund,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    exportOutputType: obj,
                    terminalName: vm.requestParams.terminalName,
                    lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                    dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                };
            };

            vm.getOrders = function () {
                vm.loading = true;
                orderPromotionReportService.getPromotionOrders(vm.getInput())
                    .success(function (result) {
                        vm.gridOptions.totalItems = result.orderList.totalCount;
                        vm.datatest = angular.fromJson()
                     
                        for (let i = 0; i < result.orderList.items.length; i++) {
                            var proNote = angular.fromJson(result.orderList.items[i].orderPromotionDetails);
                            result.orderList.items[i].promotionNote = proNote.map(function (elem) {
                                return elem.PNO;
                            }).join(",");
                            if (result.orderList.items[i].price != null)
                                result.orderList.items[i].price = result.orderList.items[i].price.toFixed(vm.decimals);
                            if (result.orderList.items[i].promoAmount != null)
                                result.orderList.items[i].promoAmount = result.orderList.items[i].promoAmount.toFixed(vm.decimals);
                        }
                        console.log("data", result);
                        vm.gridOptions.data = result.orderList.items;

                        vm.totalAmount = result.dashBoardDto.totalAmount;
                        vm.totalOrders = result.dashBoardDto.totalOrderCount;
                        vm.totalItems = result.dashBoardDto.totalItemSold;
                    }).finally(function () {
                        vm.loading = false;
                    });
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.getPromotions = function () {
                vm.loading = true;
                lookupService.getPromotions({
                }).success(function (result) {
                    vm.promotions = $.parseJSON(JSON.stringify(result.items));
                }).finally(function (result) {
                    vm.loading = false;
                });
            };

            vm.getPromotions();

            vm.exportToExcel = function (mode) {
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        orderPromotionReportService.getPromotionOrdersExcel(vm.getInput(myConfirmation, mode))
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });
                    });
            };
            vm.showDetails = function (order) {
                $modal.open({
                    templateUrl: '~/App/tenant/views/connect/report/tickets/ticket/ticketModal.cshtml',
                    controller: 'tenant.views.connect.report.ticketModal as vm',
                    backdrop: 'static',
                    resolve: {
                        ticket: function () {
                            return order.ticketId;
                        }
                    }
                });
            };

            vm.openforPromotion = function (product) {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: false,
                    service: promotionService.getPromotions
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.selectedPromotions = '';
                        vm.requestParams.promotions = result.locations;
                    if (result.locations.length > 0) {
                        var selectedPros = [];
                        angular.forEach((result.locations), function (item) {
                            selectedPros.push(item.name);
                        });

                        vm.selectedPromotions = selectedPros.join(', ');
                    }
                });
            };

            // #region Builder

            $scope.builder =
                app.createBuilder(
                    {
                        condition: "AND",
                        rules: []
                    },
                    angular.fromJson
                        (
                            [
                                {
                                    "id": "TicketId",
                                    "label": app.localize("Ticket"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "integer"
                                },
                                {
                                    "id": "Location_Id",
                                    "label": app.localize("Location"),
                                    "type": "integer",
                                    "operators": [
                                        "equal",
                                        "not_equal",
                                    ],
                                    plugin: 'selectize',
                                    plugin_config: {
                                        valueField: 'value',
                                        labelField: 'displayText',
                                        searchField: 'displayText',
                                        sortField: 'displayText',
                                        create: true,
                                        maxItems: 1,
                                        plugins: ['remove_button'],
                                        onInitialize: function () {
                                            var that = this;
                                            JSON.parse(localStorage.locations).forEach(function (item) {
                                                that.addOption(item);
                                            });
                                        }
                                    },
                                    valueSetter: function (rule, value) {
                                        rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                    }
                                },
                                {
                                    id: 'MenuItemId',
                                    "label": app.localize("Menu Item"),
                                    type: 'integer',
                                    plugin: 'selectize',
                                    plugin_config: {
                                        valueField: 'value',
                                        labelField: 'displayText',
                                        searchField: 'displayText',
                                        sortField: 'displayText',
                                        create: true,
                                        maxItems: 1,
                                        plugins: ['remove_button'],
                                        onInitialize: function () {
                                            var that = this;
                                            JSON.parse(localStorage.menuItems).forEach(function (item) {
                                                that.addOption(item);
                                            });
                                        }
                                    },
                                    valueSetter: function (rule, value) {
                                        rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                    }
                                },
                                {
                                    "id": "Quantity",
                                    "label": app.localize("Quantity"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 1
                                    }
                                },
                                {
                                    "id": "Price",
                                    "label": app.localize("Price"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.1
                                    }
                                },

                                {
                                    "id": "PromotionAmount",
                                    "label": app.localize("Promotion Amount"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.1
                                    }
                                },
                                {
                                    "id": "Note",
                                    "label": app.localize("Note"),
                                    "operators": [
                                        "equal",
                                        "not_equal"
                                    ],
                                    "type": "string",
                                    
                                },
                                {
                                    "id": "creatingUserName",
                                    "label": app.localize("User"),
                                    "type": "string",
                                    "operators": [
                                        "equal",
                                        "not_equal",
                                    ],
                                    type: 'string',
                                    plugin: 'selectize',
                                    plugin_config: {
                                        valueField: 'value',
                                        labelField: 'displayText',
                                        searchField: 'displayText',
                                        sortField: 'displayText',
                                        create: true,
                                        maxItems: 1,
                                        plugins: ['remove_button'],
                                        onInitialize: function () {
                                            var that = this;
                                            JSON.parse(localStorage.users).forEach(function (item) {
                                                item.value = item.displayText;
                                                that.addOption(item);
                                            });
                                        }
                                    },
                                    valueSetter: function (rule, value) {
                                        rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                    }
                                },
                               
                            ]
                        )
                );


                // #endregion
            vm.getMenuItems = function () {
                commonService.getMenuItemsForCombobox({
                }).success(function (result) {
                    localStorage.menuItems = JSON.stringify(result.items);
                }).finally(function (result) {
                });
            };

            vm.getDepartments = function () {
                departmentService.getDepartmentsForCombobox({
                }).success(function (result) {
                    localStorage.departments = JSON.stringify(result.items);
                }).finally(function (result) {
                });
            };

            vm.getLocations = function () {
                commonService.getLocationCodeForCombobox({
                }).success(function (result) {
                    localStorage.locationsCode = JSON.stringify(result.items);
                }).finally(function (result) {
                });

                commonService.getLocationForCombobox({
                }).success(function (result) {
                    localStorage.locations = JSON.stringify(result.items);
                }).finally(function (result) {
                });
            };
            vm.getUsers = function () {
                commonService.getUserForCombobox({
                }).success(function (result) {
                    localStorage.users = JSON.stringify(result.items);
                }).finally(function (result) {
                });
            };

            vm.clear = function () {
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };
                $scope.builder.builder.reset();
                vm.getOrders();
            }


        }
    ]);
})();