﻿(function () {
	appModule.controller("tenant.views.connect.report.productmix", [
		"$scope", "$uibModal", "uiGridConstants", 'abp.services.app.tenantSettings', 'appSession', 'uiGridGroupingConstants', 'abp.services.app.commonLookup', 'abp.services.app.productMixReport',
		function ($scope, $modal, uiGridConstants, tenantSettingsService, appSession, uiGridGroupingConstants, commonLookup, productMixReport) {
			var vm = this;

			$scope.$on("$viewContentLoaded", function () {
				App.initAjax();
			});
			vm.locations = [];
			vm.currencyText = abp.features.getValue("DinePlan.DineConnect.Connect.Currency");
			vm.loading = false;
			vm.advancedFiltersAreShown = false;
			$scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
			$scope.format = $scope.formats[0];

			vm.decimals = null;
			vm.getDecimals = function () {
				tenantSettingsService.getAllSettings()
					.success(function (result) {
						vm.decimals = result.connect.decimals;
					});
			};

			vm.$onInit = function () {
				localStorage.clear();
				vm.getDecimals();
				vm.getLocations();
				vm.getUsers();
			};

			//vm.location = appSession.user.locationRefId;
			vm.requestParams = {
				locations: '',
				skipCount: 0,
				maxResultCount: app.consts.grid.defaultPageSize,
				sorting: null,
			};

			var todayAsString = moment().format("YYYY-MM-DD");
			vm.dateRangeOptions = app.createDateRangePickerOptions();
			vm.dateRangeModel = {
				startDate: todayAsString,
				endDate: todayAsString
			};
			vm.getEditionValue = function (item) {
				return item.displayText;
			};

			vm.exportToSummary = function (mode) {
				abp.ui.setBusy("#MyLoginForm");
				abp.message.confirm(app.localize("AreYouSure"),
					app.localize("RunInBackground"),
					function (isConfirmed) {
						var myConfirmation = false;
						if (isConfirmed) {
							myConfirmation = true;
						}
						vm.disableExport = true;
						productMixReport.getProductMixToExport({
							startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
							endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
							locationGroup: vm.locationGroup,
							//maxResultCount: vm.gridOptions.totalItems,
							exportOutputType: mode,
							runInBackground: myConfirmation,
							terminalName: vm.requestParams.terminalName,
							lastModifiedUserName: vm.requestParams.lastModifiedUserName,
						})
							.success(function (result) {
								if (result != null) {
									app.downloadTempFile(result);
								}
							}).finally(function () {
								vm.disableExport = false;
								abp.ui.clearBusy("#MyLoginForm");
							});
					});
			};
			

			vm.gridOptions = {
				showColumnFooter: true,
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				appScopeProvider: vm,
				columnDefs: [
					{
						name: app.localize("Group"),
						field: "productName",
						width: 200,
						enableSorting: true,
						grouping: { groupPriority: 1 },
						sort: { priority: 1, direction: 'asc' },
						cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>',
						footerCellTemplate: '<div class="ui-grid-cell-contents" >' + app.localize("Total") + '</div>'
					},
					{
						name: app.localize('CategoryName'),
						field: 'categoryName',
						minWidth: 200,
						grouping: { groupPriority: 2 },
						sort: { priority: 2, direction: 'asc' }						
					},
					{
						name: app.localize("ItemCode"),
						field: "itemCode",
						width: 150
					},
					{
						name: app.localize("ItemName"),
						field: "itemName",
						minWidth: 150
					},
					{						
						name: app.localize("Quantity"),
						field: "quantity",
						minWidth: 150,
						cellClass: "ui-ralign",
						treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
						customTreeAggregationFinalizerFn: function (aggregation) {
							aggregation.rendered = aggregation.value;
						},
						footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue()}}</div>'
					},
					{
						name: app.localize("AvgUnitPrice"),
						field: "avgUnitPrice",
						cellClass: "ui-ralign",
						width: 200,
						cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.convertDecimals(row.entity.avgUnitPrice)}}</div>',
					},
					{
						name: app.localize("TotalAmount"),
						field: "totalAmount",
						minWidth: 150,
						cellClass: "ui-ralign",
						/*cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.convertDecimals(row.entity.totalAmount)}}</div>',*/
						treeAggregationType: uiGridGroupingConstants.aggregation.SUM,						
						customTreeAggregationFinalizerFn: function (aggregation) {
							aggregation.rendered = aggregation.value.toFixed(vm.decimals);
						},
						footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue()}}</div>'
					},
					{
						name: app.localize("SalesPercent"),
						field: "salesPercent",
						width: 180,
						cellClass: "ui-ralign",
	                    /*cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.getPercent(row.entity.salesPercent)}}</div>',*/
						treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
						customTreeAggregationFinalizerFn: function (aggregation) {
							aggregation.rendered = aggregation.value.toFixed(vm.decimals)+"%";
						},
						footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >{{100 | number:grid.appScope.decimals}}%</div>'
					},
					{
						name: app.localize("CatSalesPercent"),
						field: "catSalesPercent",
						cellClass: "ui-ralign",
						width: 100,
						/*treeAggregationType: uiGridGroupingConstants.aggregation.SUM,*/
						cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.getPercent(row.entity.catSalesPercent)}}</div>',
						customTreeAggregationFinalizerFn: function (aggregation) {
							var number = 100;
							aggregation.rendered = number.toFixed(vm.decimals);
						},
					}
				],
				onRegisterApi: function (gridApi) {
				},
				data: []
			};
			vm.toggleFooter = function () {
				$scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
				$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
			};

			vm.toggleColumnFooter = function () {
				$scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
				$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
			};
			vm.convertDecimals = function (value) {
				if (value) {
					return value.toFixed(vm.decimals) ;
				}
				return null;
			};
			vm.getPercent = function (value) {
				if (value) {
					return value.toFixed(vm.decimals) + '%';
				}
				return null;
			};
			vm.getProductMix = function () {
				vm.loading = true;
				productMixReport.getProductMix({
					startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
					endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
					locationGroup: vm.locationGroup,
					skipCount: vm.requestParams.skipCount,
					maxResultCount: vm.requestParams.maxResultCount,
					sorting: vm.requestParams.sorting,
					mathRound: vm.decimals,
					dynamicFilter: angular.toJson($scope.builder.builder.getRules())
				}).success(function (result) {
					console.log("datamix", result);
					vm.gridOptions.data = result.items;
				}).finally(function () {
					vm.loading = false;
				});
			};

			vm.intiailizeOpenLocation = function () {
				vm.locationGroup = app.createLocationInputForSearch();
			};
			vm.intiailizeOpenLocation();

			vm.openLocation = function () {
				var modalInstance = $modal.open({
					templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
					controller: "tenant.views.connect.location.select.location as vm",
					backdrop: "static",
					keyboard: false,
					resolve: {
						location: function () {
							return vm.locationGroup;
						}
					}
				});
				modalInstance.result.then(function (result) {
					vm.locationGroup = result;
				});
			};

			// #region Builder

			$scope.builder =
				app.createBuilder(
					{
						condition: "AND",
						rules: []
					},
					angular.fromJson
						(
							[
								{
									"id": "CategoryName",
									"label": app.localize("CategoryName"),
									"type": "string"
								},
								{
									"id": "ItemCode",
									"label": app.localize("ItemCode"),
									"type": "string"
								},
								{
									"id": "ItemName",
									"label": app.localize("ItemName"),
									"type": "string"
								},
								{
									"id": "Quantity",
									"label": app.localize("Quantity"),
									"operators": [
										"equal",
										"less",
										"less_or_equal",
										"greater",
										"greater_or_equal"
									],
									"type": "integer"
								},
								{
									"id": "AvgUnitPrice",
									"label": app.localize("AvgUnitPrice"),
									"operators": [
										"equal",
										"less",
										"less_or_equal",
										"greater",
										"greater_or_equal"
									],
									"type": "double",
									"validation": {
										"min": 0,
										"step": 1
									}
								},
								{
									"id": "TotalAmount",
									"label": app.localize("TotalAmount"),
									"operators": [
										"equal",
										"less",
										"less_or_equal",
										"greater",
										"greater_or_equal"
									],
									"type": "double",
									"validation": {
										"min": 0,
										"step": 1
									}
								},
							]
						)
				);

			vm.getLocations = function () {
				commonLookup.getLocationCodeForCombobox({
				}).success(function (result) {
					localStorage.locationsCode = JSON.stringify(result.items);
				}).finally(function (result) {
				});

				commonLookup.getLocationForCombobox({
				}).success(function (result) {
					localStorage.locations = JSON.stringify(result.items);
				}).finally(function (result) {
				});
			};
			vm.getUsers = function () {
				commonLookup.getUserForCombobox({
				}).success(function (result) {
					localStorage.users = JSON.stringify(result.items);
				}).finally(function (result) {
				});
			};

			vm.clear = function () {
				vm.dateRangeModel = {
					startDate: todayAsString,
					endDate: todayAsString
				};
				$scope.builder.builder.reset();
				vm.getProductMix();
			}

			// #endregion
		}
	]);
})();