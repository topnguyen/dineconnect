﻿(function () {
    appModule.controller('tenant.views.connect.report.tillaccount.tillaccount', [
        '$scope', '$uibModal', 'uiGridConstants', "abp.services.app.connectLookup", 'abp.services.app.location', 'abp.services.app.tillAccount',
        function ($scope, $modal, uiGridConstants, connectService, locationService, tillaccountService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.locations = [];
            vm.payments = [];
            vm.transactions = [];
            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];
            vm.loading = false;
            vm.advancedFiltersAreShown = false;

            vm.totalTicketAmount = '';
            vm.totalTickets = '';
            vm.totalOrders = '';
            vm.totalItems = '';

            vm.requestParams = {
                locations: '',
                accountTypes: '',
                accountNames: '',
                lastModifiedUserName: '',
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.gridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize('Location'),
                        field: 'locationName',
                        enableSorting: false
                    },
                    {
                        name: app.localize('Category'),
                        field: 'categoryName',
                        enableSorting: false
                    },
                    {
                        name: app.localize('MenuItem'),
                        field: 'menuItemName'
                    },
                    {
                        name: app.localize('Portion'),
                        field: 'menuItemPortionName',
                        enableSorting: false
                    },
                    {
                        name: app.localize('Quantity'),
                        field: 'quantity',
                        cellFilter: 'number: 3',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Price'),
                        field: 'price',
                        cellFilter: 'number: 3',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Total'),
                        field: 'total',
                        cellFilter: 'number: 3',
                        cellClass: 'ui-ralign',
                        enableSorting: false
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getOrders();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;
                        vm.getOrders();
                    });
                },
                data: []
            };

            vm.getOrders = function () {
                var allLocations = vm.requestParams.locations;
                if (vm.requestParams.locations == '') {
                    allLocations = vm.locations;
                }
                vm.loading = true;
                connectService.getItemSales({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locations: allLocations,
                    void: vm.requestParams.void,
                    gift: vm.requestParams.gift,
                    comp: vm.requestParams.comp,
                    noSales: vm.requestParams.noSales,
                    refund: vm.requestParams.refund,
                    terminalName: vm.requestParams.terminalName,
                    lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    stats: true
                })
                    .success(function (result) {

                        vm.gridOptions.totalItems = result.menuList.totalCount;
                        vm.gridOptions.data = result.menuList.items;

                        angular.element(document.querySelector('#topitems')).empty();
                        angular.element(document.querySelector('#topquantity')).empty();

                        Highcharts.chart('topitems', {
                            chart: {
                                plotBackgroundColor: null,
                                plotBorderWidth: null,
                                plotShadow: false,
                                type: 'pie'
                            },
                            title: {
                                text: ''
                            },
                            tooltip: {
                                pointFormat: '{point.name}: {point.y} <br/>{point.percentage:.1f} %'
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    showInLegend: true,
                                    dataLabels: {
                                        enabled: false,
                                        format: '',
                                        style: {
                                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                        }
                                    }
                                }
                            },
                            series: [{
                                name: app.localize('Quantity'),
                                colorByPoint: true,
                                data: $.parseJSON(JSON.stringify(result.quantities))
                            }]
                        });

                        Highcharts.chart('topquantity', {
                            chart: {
                                plotBackgroundColor: null,
                                plotBorderWidth: null,
                                plotShadow: false,
                                type: 'pie'
                            },
                            title: {
                                text: ''
                            },
                            tooltip: {
                                pointFormat: '{point.name}: {point.y} <br/>{point.percentage:.1f} %'
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    showInLegend: true,
                                    dataLabels: {
                                        enabled: false,
                                        format: '',
                                        style: {
                                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                        }
                                    }
                                }
                            },
                            series: [{
                                name: app.localize('Items'),
                                colorByPoint: true,
                                data: $.parseJSON(JSON.stringify(result.totals))
                            }]
                        });




                    }).finally(function () {
                        vm.loading = false;
                    });
            };

            vm.getLocations = function () {
                vm.loading = true;
                locationService.getLocationBasedOnUser({
                    userId: abp.session.userId
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));

                }).finally(function (result) {
                    vm.loading = false;
                });
            };
            vm.getLocations();

            vm.processors = [];
            vm.getaccountTypes = function () {
                connectService.getTillAccountTypes({}).success(function (result) {
                    vm.processors = result.items;
                });
            };

            vm.getaccountTypes();

            vm.accountnames = [];

            vm.getaccountnames = function () {
                tillaccountService.getTillAccounts({}).success(function (result) {
                    vm.accountnames = result;
                });
            };

            vm.getaccountnames();

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };



            vm.exportToSummary = function () {
                var allLocations = vm.requestParams.locations;
                if (vm.requestParams.locations == '') {
                    allLocations = vm.locations;
                }
                abp.ui.setBusy('#MyLoginForm');
                connectService.getItemsSummaryExcel({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locations: allLocations,
                    void: vm.requestParams.void,
                    gift: vm.requestParams.gift,
                    noSales: vm.requestParams.noSales,
                    refund: vm.requestParams.refund,
                    terminalName: vm.requestParams.terminalName,
                    lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        abp.ui.clearBusy('#MyLoginForm');
                    });
            };


            vm.showDetails = function (ticket) {
                $modal.open({
                    templateUrl: '~/App/tenant/views/connect/report/orders/order/orderModal.cshtml',
                    controller: 'tenant.views.connect.report.orderModal as vm',
                    backdrop: 'static',
                    resolve: {
                        ticket: function () {
                            return ticket;
                        }
                    }
                });
            };


            vm.exportToExcel = function () {
                var myModel = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/report/items/item/exportModal.cshtml',
                    controller: 'tenant.views.connect.report.item.exportModal as vm',
                    backdrop: 'static',
                    resolve: {
                        dto: function () {
                            abp.ui.setBusy('#MyLoginForm');
                            return {
                                startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                                endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                                locations: vm.requestParams.locations,
                                void: vm.requestParams.void,
                                gift: vm.requestParams.gift,
                                noSales: vm.requestParams.noSales,
                                refund: vm.requestParams.refund,
                                terminalName: vm.requestParams.terminalName,
                                lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                                skipCount: vm.requestParams.skipCount,
                                maxResultCount: vm.requestParams.maxResultCount,
                                sorting: vm.requestParams.sorting,
                                stats: false
                            };
                        }
                    }
                });


            };



        }
    ]);
})();