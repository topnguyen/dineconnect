﻿(function () {
    appModule.controller("tenant.views.connect.report.thailand.thaitax", [
        "$scope", "$uibModal", "uiGridConstants", "abp.services.app.connectReport", "abp.services.app.location", 'appSession', "abp.services.app.tenantSettings",
        function ($scope, $modal, uiGridConstants, connectService, locationService, appSession, tenantSettingsService) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });
            vm.locations = [];
            vm.currencyText = abp.features.getValue("DinePlan.DineConnect.Connect.Currency");
            vm.loading = false;
            vm.advancedFiltersAreShown = false;
            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];

            vm.location = appSession.user.locationRefId;
            vm.requestParams = {
                locations: "",
                payments: "",
                department: "",
                transactions: "",
                terminalName: "",
                lastModifiedUserName: "",
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };
            vm.getEditionValue = function (item) {
                return item.displayText;
            };

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    });
            };

            vm.$onInit = function () {
                vm.getDecimals();
            }

            vm.exportToSummary = function (mode) {
                if (vm.gridOptions.totalItems === 0) return;
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        connectService.getLocationSalesExcel({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locationGroup: vm.locationGroup,
                            payments: vm.requestParams.payments,
                            department: vm.requestParams.department,
                            transactions: vm.requestParams.transactions,
                            terminalName: vm.requestParams.terminalName,
                            lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                            sorting: vm.requestParams.sorting,
                            maxResultCount: vm.gridOptions.totalItems,
                            exportOutputType: mode,
                            runInBackground: myConfirmation
                        })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });

                    });       
            };

            vm.exportToSummaryDetail = function (mode) {
                console.log(vm.location);
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        connectService.getLocationSalesDetailExcel({
                                startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                                endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                                locationGroup: vm.locationGroup,
                                payments: vm.requestParams.payments,
                                department: vm.requestParams.department,
                                transactions: vm.requestParams.transactions,
                                terminalName: vm.requestParams.terminalName,
                                lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                                sorting: vm.requestParams.sorting,
                                maxResultCount: 10,
                                exportOutputType: mode,
                                runInBackground: myConfirmation
                            })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });

                    });
            };

            vm.gridOptions = {
                showColumnFooter: true,
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize("Date"),
                        field: "date",
                        type: 'date',
                        cellFilter: 'date:\'dd-MM-yyyy\'',
                        width: 150
                    },
                    {
                        name: app.localize("REG-NO"),
                        field: "regNo",
                        width: 150
                    },
                    {
                        name: app.localize("TaxInvoiceNo"),
                        field: "taxInvoiceNo",
                        minWidth: 250
                    },
                    {
                        name: app.localize("SaleAmt"),
                        field: "saleAmt",
                        cellClass: "ui-ralign",
                        aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    },
                    {
                        name: app.localize("SaleNonVat"),
                        field: "saleNonVat",
                        cellClass: "ui-ralign",
                        aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    },
                    {
                        name: app.localize("SaleVat"),
                        field: "saleVat",
                        cellClass: "ui-ralign",
                        aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    },
                    {
                        name: app.localize("Service"),
                        field: "service",
                        cellClass: "ui-ralign",
                        aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    },
                    {
                        name: app.localize("Tip"),
                        field: "tip",
                        cellClass: "ui-ralign",
                        aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    },
                    {
                        name: app.localize("Vat"),
                        field: "vat",
                        cellClass: "ui-ralign",
                        aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        vm.getOrders();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;
                        vm.getOrders();
                    });
                },
                data: []

            };
            
            vm.toggleFooter = function () {
                $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.toggleColumnFooter = function () {
                $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.getOrders = function() {
                vm.loading = true;
                var allLocations = vm.requestParams.locations;
                if (vm.requestParams.locations === "") {
                    allLocations = vm.locations;
                }

                connectService.getLocationSales({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationGroup: vm.locationGroup,
                    payments: vm.requestParams.payments,
                    department: vm.requestParams.department,
                    transactions: vm.requestParams.transactions,
                    terminalName: vm.requestParams.terminalName,
                    lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting
                }).success(function (result) {
                    vm.gridOptions.totalItems = result.totalCount;

                    for (let i = 0; i < result.items.length; i++) {
                        if (result.items[i].saleAmt != null)
                            result.items[i].saleAmt = result.items[i].saleAmt.toFixed(vm.decimals);

                        if (result.items[i].saleNonVat != null)
                            result.items[i].saleNonVat = result.items[i].saleNonVat.toFixed(vm.decimals);

                        if (result.items[i].saleVat != null)
                            result.items[i].saleVat = result.items[i].saleVat.toFixed(vm.decimals);

                        if (result.items[i].service != null)
                            result.items[i].service = result.items[i].service.toFixed(vm.decimals);

                        if (result.items[i].tip != null)
                            result.items[i].tip = result.items[i].tip.toFixed(vm.decimals);

                        if (result.items[i].vat != null)
                            result.items[i].vat = result.items[i].vat.toFixed(vm.decimals);
                    }

                    vm.gridOptions.data = result.items;

                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function() {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function() {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function(result) {
                    vm.locationGroup = result;
                });
            };
        }
    ]);
})();