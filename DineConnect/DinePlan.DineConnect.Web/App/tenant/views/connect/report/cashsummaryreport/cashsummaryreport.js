﻿(function () {
    appModule.controller("tenant.views.connect.report.cashsummaryreport",
        [
            "$scope", "$uibModal", "uiGridConstants", "abp.services.app.cashSummaryReport", "uiGridGroupingConstants",
            function ($scope, $modal, uiGridConstants, cashSummaryReportAppService, uiGridGroupingConstants) {

                var vm = this;
                vm.loading = false;
                vm.requestParams = {
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null,
                };

                vm.dateRangeOptions = app.createDateRangePickerOptions();
                var todayAsString = moment().format("YYYY-MM-DD");
                vm.gridOptions = {
                    showColumnFooter: true,
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    columnDefs: [
                       
                        {
                            name: app.localize("Plant"),
                            field: "plant",
                            minWidth: 100,
                            grouping: { groupPriority: 1 },
                            cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>',
                        },
                        {
                            name: app.localize("Date"),
                            minWidth: 100,
                            field: "date",
                            grouping: { groupPriority: 2 },
                            cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>',
                        },
                        {
                            name: app.localize("Shift"),
                            minWidth: 100,
                            field: "shift",
                            grouping: { groupPriority: 3 },
                            cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>',
                        },
                        {
                            name: app.localize("Desription"),
                            minWidth: 100,
                            field: "detail"
                        },
                        {
                            name: app.localize("Amount"),
                            minWidth: 100,
                            field: "amount",
                            treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                        },
                        {
                            name: app.localize("Tax ID"),
                            minWidth: 100,
                            field: "taxID",
                        },
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope,
                            function (grid, sortColumns) {
                                if (!sortColumns.length || !sortColumns[0].field) {
                                    vm.requestParams.sorting = null;
                                } else {
                                    vm.requestParams.sorting =
                                        sortColumns[0].field + " " + sortColumns[0].sort.direction;
                                }
                                vm.getCreditCard();
                            });
                        gridApi.pagination.on.paginationChanged($scope,
                            function (pageNumber, pageSize) {
                                vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                                vm.requestParams.maxResultCount = pageSize;
                                vm.getCreditCard();
                            });
                    },
                    data: []
                };
                vm.intiailizeOpenLocation = function () {
                    vm.locationGroup = app.createLocationInputForSearch();
                };

                vm.intiailizeOpenLocation();

                vm.openLocation = function () {
                    const modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };


                vm.exportToExcel = function (obj) {
                    cashSummaryReportAppService.getCashSummaryReportToExport(vm.getDataInput(false, obj))
                        .success(function (result) {
                            if (result != null) {
                                app.downloadTempFile(result);
                            }
                        }).finally(function () {
                            abp.ui.clearBusy("#MyLoginForm");
                            vm.disableExport = false;
                        });
                    //abp.ui.setBusy("#MyLoginForm");
                    //abp.message.confirm(app.localize("AreYouSure"),
                    //    app.localize("RunInBackground"),
                    //    function (isConfirmed) {
                    //        var myConfirmation = false;
                    //        if (isConfirmed) {
                    //            myConfirmation = true;
                    //        }
                    //        vm.disableExport = true;
                    //        cashSummaryReportAppService.getCashSummaryReportToExport(vm.getDataInput(myConfirmation, obj))
                    //            .success(function (result) {
                    //                if (result != null) {
                    //                    app.downloadTempFile(result);
                    //                }
                    //            }).finally(function () {
                    //                abp.ui.clearBusy("#MyLoginForm");
                    //                vm.disableExport = false;
                    //            });
                    //    });
                };

                vm.clear = function () {
                    vm.dateRangeModel = {
                        startDate: todayAsString,
                        endDate: todayAsString
                    };
                    $scope.builder.builder.reset();
                    vm.getData();
                }

                vm.getData = function () {
                    vm.loading = true;
                    cashSummaryReportAppService.getCashSummaryReport(vm.getDataInput())
                        .success(function (result) {
                            vm.data = result;
                            console.log('result', result);
                        }).finally(function () {
                            vm.loading = false;
                        });
                };

                vm.getDataInput = function (confirmation, obj) {
                    return {
                        startDate: moment(vm.dateRangeModel.startDate).lang("en").format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).lang("en").format($scope.format),
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting,
                        runInBackground: confirmation,
                        skipCount: vm.requestParams.skipCount,
                        locationGroup: vm.locationGroup,
                        exportOutputType: obj,
                        dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
                    };
                };

                // #region Builder

                $scope.builder =
                    app.createBuilder(
                        {
                            condition: "AND",
                            rules: []
                        },
                        angular.fromJson
                            (
                                [
                                    {
                                        "id": "Detail",
                                        "label": app.localize("Description"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "TaxID",
                                        "label": app.localize("TaxID"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "Amount",
                                        "label": app.localize("Amount"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                ]
                            )
                    );

                // #endregion

            }
        ]);
})();