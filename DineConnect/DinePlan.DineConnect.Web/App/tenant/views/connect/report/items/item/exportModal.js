﻿(function () {
    appModule.controller("tenant.views.connect.report.item.exportModal", [
        "$scope", "$uibModalInstance", "dto", "abp.services.app.connectReport",
        function ($scope, $modalInstance, dto, connectService) {
            var vm = this;

            abp.ui.clearBusy("#MyLoginForm");
            vm.disableExport = false;

            vm.dto = dto;
            vm.duration = "D";
            vm.exportOutput = "T";

            vm.close = function () {
                $modalInstance.dismiss();
            };

            vm.exportToExcel = function () {
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"), app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;

                        connectService.getItemsExcel({
                            runInBackground: myConfirmation,
                            startDate: vm.dto.startDate,
                            endDate: vm.dto.endDate,
                            locationGroup: vm.dto.locationGroup,
                            void: vm.dto.void,
                            gift: vm.dto.gift,
                            comp: vm.dto.comp,
                            noSales: vm.dto.noSales,
                            refund: vm.dto.refund,
                            menuItemIds: vm.dto.menuItemIds,
                            terminalName: vm.dto.terminalName,
                            lastModifiedUserName: vm.dto.lastModifiedUserName,
                            skipCount: vm.dto.skipCount,
                            maxResultCount: vm.dto.maxResultCount,
                            sorting: vm.dto.sorting,
                            duration: vm.duration,
                            exportOutput: vm.exportOutput,
                            bylocation: vm.bylocation,
                            filterByCategory: vm.dto.filterByCategory,
                            filterByGroup: vm.dto.filterByGroup,
                            filterByTag: vm.dto.filterByTag,
                            filterByDepartment: vm.dto.filterByDepartment,
                            dynamicFilter: vm.dto.dynamicFilter
                        })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                                $modalInstance.dismiss();
                            });
                    });
            };
        }
    ]);
})();