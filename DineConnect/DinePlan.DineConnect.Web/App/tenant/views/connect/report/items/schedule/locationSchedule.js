﻿(function () {
    appModule.controller("tenant.views.connect.report.locationschedule", [
        "$scope", "$uibModal", "uiGridConstants", "abp.services.app.connectReport", "abp.services.app.tenantSettings", "abp.services.app.location",
        function ($scope, $modal, uiGridConstants, connectService, tenantSettingsService, locationService) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });
            vm.payments = [];
            vm.transactions = [];
            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];
            vm.loading = false;
            vm.advancedFiltersAreShown = false;
            vm.tallyAvailable = abp.setting.getBoolean("App.House.TallyIntegrationFlag");

            vm.menuItemIds = [];

            vm.currentLocation = {};

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;

                        var schedules = angular.fromJson(result.connect.schedules);
                        vm.settingGrid(schedules);
                    });
            };

            vm.$onInit = function () {
                vm.getDecimals();
            };

            vm.requestParams = {
                locations: "",
                void: false,
                gift: false,
                refund: false,
                terminalName: "",
                lastModifiedUserName: "",
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.exportForTally = function () {
                abp.ui.setBusy("#MyLoginForm");
                connectService.getDepartmentTallyExcel({
                    startDate: moment(vm.dateRangeModel.startDate).format("YYYY-MM-DD"),
                    endDate: moment(vm.dateRangeModel.endDate).format("YYYY-MM-DD"),
                    locationGroup: vm.locationGroup
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        abp.ui.clearBusy("#MyLoginForm");
                    });
            };

            vm.showDetails = function (ticket) {
                $modal.open({
                    templateUrl: "~/App/tenant/views/connect/report/items/department/departmentModal.cshtml",
                    controller: "tenant.views.connect.report.departmentModal as vm",
                    backdrop: "static",
                    resolve: {
                        ticket: function () {
                            return ticket;
                        }
                    }
                });
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForCreateSingleLocation();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function() {

                vm.locationGroup = app.createLocationInputForCreateSingleLocation();
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function() {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function(result) {
                    if (result.locations.length > 0) {
                        vm.currentLocation = result.locations[0];

                    }
                });
            };


         
        

            vm.gridOptions = {
                showColumnFooter: true,
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                headerTemplate: 'header.html',
                headerAppends: {
                    index: [0, 1],
                    height: 35
                },
                columnDefs: [
                    {
                        name: 'menuItemName',
                        headers: [
                            { label: app.localize("MenuItem"), rowSpan: 2 },
                            { label: '', rowSpan: '#rowSpan' }
                        ]
                    }],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        vm.getOrders();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;
                        vm.getOrders();
                    });
                },
                data: []
            };

            vm.toggleFooter = function () {
                $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.toggleColumnFooter = function () {
                $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.getOrders = function () {
                if (!vm.currentLocation.id) {
                    abp.message.warn(app.localize('PleaseChooseLocation'));
                } else {
                    vm.loading = true;
                    locationService.getLocationSchedulesByLocation(vm.currentLocation.id)
                        .success(function (result) {
                            vm.gridOptions.columnDefs = [
                                {
                                    name: 'menuItemName',
                                    headers: [
                                        { label: app.localize("MenuItem"), rowSpan: 2 },
                                        { label: '', rowSpan: '#rowSpan' }
                                    ]
                                }];

                            vm.settingGrid(result);
                        });

                    connectService.getScheduleReport({
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        locationGroup: vm.locationGroup,
                        void: vm.requestParams.void,
                        gift: vm.requestParams.gift,
                        comp: vm.requestParams.comp,
                        noSales: vm.requestParams.noSales,
                        refund: vm.requestParams.refund,
                        terminalName: vm.requestParams.terminalName,
                        lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                        skipCount: vm.requestParams.skipCount,
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting,
                        stats: true,
                        menuItemIds: vm.menuItemIds,
                        IsSummary: true,
                        location: vm.currentLocation.id
                    })
                        .success(function (result) {
                            vm.gridOptions.totalItems = result.totalCount;

                            for (let i = 0; i < result.items.length; i++) {
                                angular.forEach(result.items[i].scheduleReportDatas, function (data) {
                                    if (data.amount != null)
                                        data.amount = data.amount.toFixed(vm.decimals);
                                });
                            }

                            vm.gridOptions.data = result.items;
                        }).finally(function () {
                            vm.loading = false;
                        });
                }
            };

            vm.exportToSummary = function (mode) {
                if (vm.gridOptions.totalItems === 0) return;
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        connectService.getScheduleReportExcel({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locationGroup: vm.locationGroup,
                            void: vm.requestParams.void,
                            gift: vm.requestParams.gift,
                            comp: vm.requestParams.comp,
                            noSales: vm.requestParams.noSales,
                            refund: vm.requestParams.refund,
                            terminalName: vm.requestParams.terminalName,
                            lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                            skipCount: 0,
                            maxResultCount: vm.gridOptions.totalItems,
                            sorting: vm.requestParams.sorting,
                            stats: true,
                            exportOutputType: mode,
                            runInBackground: myConfirmation,
                            menuItemIds: vm.menuItemIds,
                            location: vm.currentLocation.id
                        })
                            .success(function (result) {
                                if (result !== null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });
                    });
            };

            vm.selectMenuitems = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/report/items/item/selectMenuItems.cshtml",
                    controller: "tenant.views.connect.report.items.item.selectMenuItems as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.length > 0) {
                        vm.menuItemIds = result;
                    }
                });
            };

            vm.clear = function () {
                vm.menuItemIds = [];
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };

                vm.locationGroup = app.createLocationInputForCreate();
                vm.requestParams.void = false;
                vm.requestParams.gift = false;
                vm.requestParams.comp = false;
                vm.requestParams.noSales = false;
                vm.requestParams.terminalName = '';
                vm.requestParams.lastModifiedUserName = null;

                vm.getOrders();
            };

            vm.locationGroup = app.createLocationInputForCreate();


            vm.resetSelectedItem = function () {
                vm.menuItemIds = [];
            };

            vm.settingGrid = function (schedules) {
                if (schedules.length === 0) {
                    abp.message.warn(app.localize("PleaseAddSchedulesForSettings"));
                }

                angular.forEach(schedules, function (schedule, index) {
                    var title = schedule.name + ' (' + schedule.startHour + ':' + schedule.startMinute + ' - ' + schedule.endHour + ':' + schedule.endMinute + ')';

                    var rowspan = [
                        {
                            name: 'scheduleReportDatas[' + index + '].quantity',
                            headers: [
                                { label: title, colSpan: 2 },
                                { label: app.localize("Quantity") }
                            ],
                            cellClass: "ui-ralign",
                            aggregationType: uiGridConstants.aggregationTypes.sum,
                            footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue()}}</div>'
                        },
                        {
                            name: 'scheduleReportDatas[' + index + '].amount',
                            headers: [
                                { label: '', colSpan: '#colSpan' },
                                { label: app.localize("Amount") }
                            ],
                            cellClass: "ui-ralign",
                            aggregationType: uiGridConstants.aggregationTypes.sum,
                            footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                        }];

                    vm.gridOptions.columnDefs = vm.gridOptions.columnDefs.concat(rowspan);
                });
            };
        }
    ]);
})();