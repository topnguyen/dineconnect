﻿(function () {
    appModule.controller('tenant.views.connect.report.groupModal', [
        '$scope', '$uibModalInstance', 'ticket',
    function ($scope, $modalInstance, ticket) {
            var vm = this;

            vm.ticket = ticket;

            vm.close = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();