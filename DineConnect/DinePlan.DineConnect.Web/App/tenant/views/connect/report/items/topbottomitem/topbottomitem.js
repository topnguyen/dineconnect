﻿(function () {
    appModule.controller('tenant.views.connect.report.items.topdownitem', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.connectReport', "abp.services.app.tenantSettings", 'abp.services.app.commonLookup', 'abp.services.app.productGroup',
        function ($scope, $modal, uiGridConstants, connectService, tenantSettingsService, commonLookupService, productGroupService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.payments = [];
            vm.transactions = [];
            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];
            vm.loading = false;
            vm.advancedFiltersAreShown = false;

            vm.totalTicketAmount = '';
            vm.totalTickets = '';
            vm.totalOrders = '';
            vm.totalItems = '';
            vm.menuItemIds = [];
            vm.categoryIds = [];
            vm.takeType = 'desc';
            vm.takeNumber = 10;
            vm.ranking = 'Quantity';
            vm.menuItemIds = [];
            vm.productGroups = [];
            vm.categories = [];
            vm.departments = [];
            vm.selectedGroups = [];
            vm.selectedDepartments = [];

            vm.requestParams = {
                locations: '',
                void: false,
                gift: false,
                noSales: false,
                comp: false,
                terminalName: '',
                lastModifiedUserName: '',
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null,
                filterByCategory: [],
                filterByGroup: []
            };

            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    });
            };

            vm.$onInit = function () {
                vm.getDecimals();
                fillDropDownGroup();
                fillDropDownDepartments();
            };

            function fillDropDownGroup() {
                productGroupService.getProductGroupNames({})
                    .success(function (result) {
                        vm.productGroups = result.items;
                    });
            }

            

            function fillDropDownDepartments() {
                commonLookupService.getDepartments({})
                    .success(function (result) {
                        vm.departments = result;
                    })
                    .finally(function (result) {
                    });
            };
            vm.gridOptions = {
                showColumnFooter: true,
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize('Location'),
                        field: 'locationName',
                        enableSorting: false
                    },
                    {
                        name: app.localize('Category'),
                        field: 'categoryName',
                        enableSorting: false
                    },
                    {
                        name: app.localize('MenuItem'),
                        field: 'menuItemName'
                    },
                    {
                        name: app.localize('Portion'),
                        field: 'menuItemPortionName',
                        enableSorting: false
                    },
                    {
                        name: app.localize('Quantity'),
                        field: 'quantity',
                        cellClass: "ui-ralign",
                        aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue()}}</div>'
                    },
                    {
                        name: app.localize('Price'),
                        field: 'price',
                        cellClass: "ui-ralign"
                    },
                    {
                        name: app.localize('Total'),
                        field: 'total',
                        enableSorting: false,
                        cellClass: "ui-ralign",
                        aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getOrders();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;
                        vm.getOrders();
                    });
                },
                data: []
            };

            vm.toggleFooter = function () {
                $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.toggleColumnFooter = function () {
                $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.getOrders = function () {
                vm.requestParams.filterByCategory = vm.categoryIds;

                vm.requestParams.filterByGroup = vm.selectedGroups;

                vm.loading = true;
                connectService.getTopOrBottomItemSales({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationGroup: vm.locationGroup,
                    void: vm.requestParams.void,
                    gift: vm.requestParams.gift,
                    comp: vm.requestParams.comp,
                    noSales: vm.requestParams.noSales,
                    refund: vm.requestParams.refund,
                    terminalName: vm.requestParams.terminalName,
                    lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    stats: true,
                    menuItemIds: vm.menuItemIds,
                    takeNumber: vm.takeNumber,
                    takeType: vm.takeType,
                    ranking: vm.ranking,
                    filterByCategory: vm.requestParams.filterByCategory,
                    filterByGroup: vm.requestParams.filterByGroup,
                    filterByDepartment: vm.selectedDepartments,
                    dynamicFilter: angular.toJson($scope.builder.builder.getRules())

                }).success(function (result) {
                    vm.gridOptions.totalItems = result.menuList.totalCount;

                    for (let i = 0; i < result.menuList.items.length; i++) {
                        if (result.menuList.items[i].price !== null)
                            result.menuList.items[i].price = result.menuList.items[i].price.toFixed(vm.decimals);

                        if (result.menuList.items[i].total !== null)
                            result.menuList.items[i].total = result.menuList.items[i].total.toFixed(vm.decimals);
                    }

                    vm.gridOptions.data = result.menuList.items;

                    angular.element(document.querySelector('#topitems')).empty();
                    angular.element(document.querySelector('#topquantity')).empty();

                    Highcharts.chart('topitems', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{point.name}: {point.y} <br/>{point.percentage:.1f} %'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                showInLegend: true,
                                dataLabels: {
                                    enabled: false,
                                    format: '',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            name: app.localize('Quantity'),
                            colorByPoint: true,
                            data: $.parseJSON(JSON.stringify(result.quantities))
                        }]
                    });

                    Highcharts.chart('topquantity', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{point.name}: {point.y} <br/>{point.percentage:.1f} %'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                showInLegend: true,
                                dataLabels: {
                                    enabled: false,
                                    format: '',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            name: app.localize('Items'),
                            colorByPoint: true,
                            data: $.parseJSON(JSON.stringify(result.totals))
                        }]
                    });
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.showDetails = function (ticket) {
                $modal.open({
                    templateUrl: '~/App/tenant/views/connect/report/orders/order/orderModal.cshtml',
                    controller: 'tenant.views.connect.report.orderModal as vm',
                    backdrop: 'static',
                    resolve: {
                        ticket: function () {
                            return ticket;
                        }
                    }
                });
            };

            vm.selectMenuitems = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/report/items/item/selectMenuItems.cshtml",
                    controller: "tenant.views.connect.report.items.item.selectMenuItems as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.length > 0) {
                        vm.menuItemIds = result;
                    }
                });
            };

            vm.selectCategory = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/report/items/item/selectCategory.cshtml",
                    controller: "tenant.views.connect.report.items.item.selectCategory as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.length > 0) {
                        vm.categoryIds = result;
                    }
                });
            };
            vm.resetSelectedCategory = function () {
                vm.categoryIds = [];
            };
            vm.resetSelectedItem = function () {
                vm.menuItemIds = [];
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.preventNonNumericalInput = function (e) {
                e = e || window.event;
                var charCode = (typeof e.which === "undefined") ? e.keyCode : e.which;
                var charStr = String.fromCharCode(charCode);

                if (!charStr.match(/^[0-9]+$/))
                    e.preventDefault();
            };

            vm.exportToExcel = function () {
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.loading = true;

                        connectService.getTopOrBottomItemSalesToExcel({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locationGroup: vm.locationGroup,
                            void: vm.requestParams.void,
                            gift: vm.requestParams.gift,
                            comp: vm.requestParams.comp,
                            noSales: vm.requestParams.noSales,
                            refund: vm.requestParams.refund,
                            terminalName: vm.requestParams.terminalName,
                            lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                            skipCount: vm.requestParams.skipCount,
                            maxResultCount: vm.requestParams.maxResultCount,
                            sorting: vm.requestParams.sorting,
                            stats: true,
                            menuItemIds: vm.menuItemIds,
                            takeNumber: vm.takeNumber,
                            takeType: vm.takeType,
                            runInBackground: myConfirmation,
                            ranking: vm.ranking,
                            isExport: true,
                            filterByCategory: vm.requestParams.filterByCategory,
                            filterByGroup: vm.requestParams.filterByGroup,
                            filterByDepartment: vm.selectedDepartments
                        }).success(function (result) {
                            if (result != null) {
                                app.downloadTempFile(result);
                            }
                        }).finally(function () {
                            vm.loading = false;
                        });
                    });
            };
            // #region Builder

            $scope.builder =
                app.createBuilder(
                    {
                        condition: "AND",
                        rules: []
                    },
                    angular.fromJson
                        (
                            [
                                {
                                    "id": "categoryName",
                                    "label": app.localize("Category"),
                                    "type": "string"
                                },
                                {
                                    "id": "menuItemName",
                                    "label": app.localize("MenuItem"),
                                    "type": "string"
                                },
                                {
                                    "id": "menuItemPortionName",
                                    "label": app.localize("Portion"),
                                    "type": "string"
                                },
                                {
                                    "id": "Quantity",
                                    "label": app.localize("Quantity"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                                {
                                    "id": "Price",
                                    "label": app.localize("Price"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                                {
                                    "id": "Total",
                                    "label": app.localize("Total"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },

                            ]
                        )
                );

            vm.clear = function () {
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };
                $scope.builder.builder.reset();
                vm.getOrders();
            }
                // #endregion
        }
    ]);
})();