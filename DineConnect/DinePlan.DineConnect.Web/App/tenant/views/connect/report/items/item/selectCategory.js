﻿(function () {
    appModule.controller('tenant.views.connect.report.items.item.selectCategory', [
        '$scope', '$uibModalInstance', 'abp.services.app.category', 'uiGridConstants',
        function ($scope, $uibModalInstance, categoryService, uiGridConstants) {
            var vm = this;
            vm.loading = false;
            vm.saving = false;

            vm.selectedCategories = [];

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.getCategories = function () {
                vm.loading = true;
                categoryService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.categoryOptions.totalItems = result.totalCount;
                    vm.categoryOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.categoryOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                enableRowSelection: true,
                rowHeight: 30,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Id"),
                        field: "id",
                        maxWidth: 70
                    },
                    {
                        name: app.localize("Name"),
                        field: "name"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        vm.getCategories();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        vm.getCategories();
                    });
                },
                data: []
            };

            vm.getCategories();

            vm.save = function () {
                vm.selectedCategories = [];

                vm.selectedRows = $scope.gridApi.selection.getSelectedRows();
                angular.forEach(vm.selectedRows, function (row) {
                    vm.selectedCategories.push(row.id);
                });

                $uibModalInstance.close(vm.selectedCategories);
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }
    ]);
})();