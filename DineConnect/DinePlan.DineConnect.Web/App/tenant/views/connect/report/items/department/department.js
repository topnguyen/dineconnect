﻿(function () {
    appModule.controller("tenant.views.connect.report.items.department", [
        "$scope", "$uibModal", "uiGridConstants", "abp.services.app.connectReport", "abp.services.app.location",
        function ($scope, $modal, uiGridConstants, connectService, locationService) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });
            vm.payments = [];
            vm.transactions = [];
            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];
            vm.loading = false;
            vm.advancedFiltersAreShown = false;
            vm.tallyAvailable = abp.setting.getBoolean("App.House.TallyIntegrationFlag");
            vm.totalQuantity = 0;
            vm.totalResult = 0;
            vm.requestParams = {
                locations: "",
                void: false,
                gift: false,
                refund: false,
                terminalName: "",
                lastModifiedUserName: "",
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.exportToExcel = function () {
                vm.loading = true;
                connectService.getDepartmentExcel({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationGroup: vm.locationGroup,
                    void: vm.requestParams.void,
                    gift: vm.requestParams.gift,
                    refund: vm.requestParams.refund,
                    terminalName: vm.requestParams.terminalName,
                    lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    }).finally(function () {
                        vm.loading = false;
                    });
            };
            vm.exportForTally = function () {
                abp.ui.setBusy("#MyLoginForm");
                connectService.getDepartmentTallyExcel({
                    startDate: moment(vm.dateRangeModel.startDate).format("YYYY-MM-DD"),
                    endDate: moment(vm.dateRangeModel.endDate).format("YYYY-MM-DD"),
                    locationGroup: vm.locationGroup
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        abp.ui.clearBusy("#MyLoginForm");
                    });
            };

            vm.showDetails = function (ticket) {
                $modal.open({
                    templateUrl: "~/App/tenant/views/connect/report/items/department/departmentModal.cshtml",
                    controller: "tenant.views.connect.report.departmentModal as vm",
                    backdrop: "static",
                    resolve: {
                        ticket: function () {
                            return ticket;
                        }
                    }
                });
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.gridOptionsNew = {
                showColumnFooter: true,
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize("Category"),
                        field: "categoryName",
                        footerCellTemplate: '<div class="ui-grid-cell-contents" >Total</div>'
                    },
                    {
                        name: app.localize("MenuItem"),
                        field: "menuItemName"
                    },
                    {
                        name: app.localize("Portion"),
                        field: "menuItemPortionName"
                    },
                    {
                        name: app.localize("DepartmentName"),
                        field: "departmentName"
                    },
                    {
                        name: app.localize("Quantity"),
                        field: "quantity",
                        footerCellTemplate: '<div class="ui-grid-cell-contents" >{{grid.appScope.totalQuantity}}</div>'
                    },
                    {
                        name: app.localize("Total"),
                        field: "total",
                        footerCellTemplate: '<div class="ui-grid-cell-contents" >{{grid.appScope.totalResult}}</div>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        vm.gridOptions();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;
                        vm.gridOptions();
                    });
                },
                data: []
            };

            vm.toggleFooter = function () {
                $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.toggleColumnFooter = function () {
                $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.gridOptions = function () {
                vm.loading = true;
                connectService.getItemSalesByDepartment({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationGroup: vm.locationGroup,
                    void: vm.requestParams.void,
                    gift: vm.requestParams.gift,
                    comp: vm.requestParams.comp,
                    noSales: vm.requestParams.noSales,
                    refund: vm.requestParams.refund,
                    terminalName: vm.requestParams.terminalName,
                    lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    stats: true
                })
                    .success(function (result) {
                        vm.gridOptions.totalItems = result.menuList.totalCount;
                        vm.gridOptions.data = result.menuList.items;

                        vm.totalQuantity = result.totalQuantity;
                        vm.totalResult = result.totalResult;
                    }).finally(function () {
                        vm.loading = false;
                    });
            };

            vm.exportToExcelNew = function () {
                if (vm.gridOptionsNew.totalItems === 0) return;
                connectService.getItemSalesByDepartmentExcel({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationGroup: vm.locationGroup,
                    void: vm.requestParams.void,
                    gift: vm.requestParams.gift,
                    comp: vm.requestParams.comp,
                    noSales: vm.requestParams.noSales,
                    refund: vm.requestParams.refund,
                    terminalName: vm.requestParams.terminalName,
                    lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                    skipCount: 0,
                    maxResultCount: vm.gridOptionsNew.totalItems,
                    sorting: vm.requestParams.sorting,
                    stats: true
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
        }
    ]);
})();