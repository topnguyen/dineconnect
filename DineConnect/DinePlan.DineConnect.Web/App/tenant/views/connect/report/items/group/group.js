﻿(function () {
    appModule.controller("tenant.views.connect.report.items.group",
        [
            "$scope", "$uibModal", "uiGridConstants", "abp.services.app.connectReport", "abp.services.app.tenantSettings",
            function ($scope, $modal, uiGridConstants, connectService, tenantSettingsService) {
                var vm = this;

                $scope.$on("$viewContentLoaded",
                    function () {
                        App.initAjax();
                    });
                vm.locations = [];
                vm.payments = [];
                vm.transactions = [];
                $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
                $scope.format = $scope.formats[0];
                vm.loading = false;
                vm.advancedFiltersAreShown = false;

                vm.totalTicketAmount = "";
                vm.totalTickets = "";
                vm.totalOrders = "";
                vm.totalItems = "";

                vm.requestParams = {
                    locations: "",
                    void: false,
                    gift: false,
                    refund: false,
                    terminalName: "",
                    lastModifiedUserName: "",
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null
                };

                var todayAsString = moment().format("YYYY-MM-DD");
                vm.dateRangeOptions = app.createDateRangePickerOptions();
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };

                vm.decimals = null;
                vm.getDecimals = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.decimals = result.connect.decimals;
                        }).finally(function () {
                        });
                };

                vm.$onInit = function () {
                    vm.getDecimals();
                }

                vm.gridOptions = {
                    showColumnFooter: true,
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    columnDefs: [
                        {
                            name: "Actions",
                            enableSorting: false,
                            width: 50,
                            headerCellTemplate: "<span></span>",
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.showDetails(row.entity)"><i class="fa fa-search"></i></button>' +
                                "</div>"
                        },
                        {
                            name: app.localize("Group"),
                            field: "groupName",
                            enableSorting: false
                        },
                        {
                            name: app.localize("Quantity"),
                            field: "quantity",
                            enableSorting: false,
                            cellClass: "ui-ralign",
                            aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                            footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue()}}</div>'
                        },
                        {
                            name: app.localize("Total"),
                            enableSorting: false,
                            field: "total",
                            cellClass: "ui-ralign",
                            aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                            footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope,
                            function (grid, sortColumns) {
                                if (!sortColumns.length || !sortColumns[0].field) {
                                    vm.requestParams.sorting = null;
                                } else {
                                    vm.requestParams.sorting =
                                        sortColumns[0].field + " " + sortColumns[0].sort.direction;
                                }
                                vm.getOrders();
                            });
                        gridApi.pagination.on.paginationChanged($scope,
                            function (pageNumber, pageSize) {
                                vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                                vm.requestParams.maxResultCount = pageSize;
                                vm.getOrders();
                            });
                    },
                    data: []
                };

                vm.toggleFooter = function () {
                    $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                };

                vm.toggleColumnFooter = function () {
                    $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                };

                vm.getOrders = function () {
                    vm.loading = true;
                    connectService.getGroupSales({
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        locationGroup: vm.locationGroup,
                        void: vm.requestParams.void,
                        gift: vm.requestParams.gift,
                        refund: vm.requestParams.refund,
                        terminalName: vm.requestParams.terminalName,
                        lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                        skipCount: vm.requestParams.skipCount,
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting,
                        dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
                    })
                        .success(function (result) {
                            vm.gridOptions.totalItems = result.groupList.totalCount;

                            for (let i = 0; i < result.groupList.items.length; i++) {
                                if (result.groupList.items[i].total != null)
                                    result.groupList.items[i].total = result.groupList.items[i].total.toFixed(vm.decimals);
                            }

                            vm.gridOptions.data = result.groupList.items;

                            angular.element(document.querySelector("#topitems")).empty();
                            //console.log(result.chartOutput);
                            //console.log($.parseJSON(JSON.stringify(result.chartOutput)));

                            Highcharts.chart("topitems",
                                {
                                    chart: {
                                        type: "column"
                                    },
                                    title: {
                                        text: app.localize("GroupSales")
                                    },
                                    subtitle: {
                                        text: ""
                                    },
                                    xAxis: {
                                        groups: $.parseJSON(JSON.stringify(result.groups)),
                                        crosshair: true
                                    },
                                    yAxis: {
                                        min: 0,
                                        title: {
                                            text: app.localize("Total")
                                        }
                                    },
                                    tooltip: {
                                        headerFormat: "<table>",
                                        pointFormat:
                                            '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                                        footerFormat: "</table>",
                                        shared: true,
                                        useHTML: true
                                    },
                                    plotOptions: {
                                        column: {
                                            pointPadding: 0.2,
                                            borderWidth: 0
                                        }
                                    },
                                    series: $.parseJSON(JSON.stringify(result.chartOutput))
                                });
                        }).finally(function () {
                            vm.loading = false;
                        });
                };
                vm.disableExport = false;

                vm.exportToExcel = function (mode) {
                    abp.ui.setBusy("#MyLoginForm");
                    abp.message.confirm(app.localize("AreYouSure"),
                        app.localize("RunInBackground"),
                        function (isConfirmed) {
                            var myConfirmation = false;
                            if (isConfirmed) {
                                myConfirmation = true;
                            }
                            vm.disableExport = true;
                            connectService.getGroupExcel({
                                startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                                endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                                locationGroup: vm.locationGroup,
                                void: vm.requestParams.void,
                                gift: vm.requestParams.gift,
                                refund: vm.requestParams.refund,
                                terminalName: vm.requestParams.terminalName,
                                lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                                skipCount: vm.requestParams.skipCount,
                                maxResultCount: vm.requestParams.maxResultCount,
                                sorting: vm.requestParams.sorting,
                                runInBackground: myConfirmation,
                                exportOutputType: mode,
                                dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
                            })
                                .success(function (result) {
                                    if (result != null && !myConfirmation)
                                        app.downloadTempFile(result);
                                }).finally(function () {
                                    abp.ui.clearBusy("#MyLoginForm");
                                    vm.disableExport = false;
                                });
                        });
                };
                vm.showDetails = function (ticket) {
                    $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/items/group/groupModal.cshtml",
                        controller: "tenant.views.connect.report.groupModal as vm",
                        backdrop: "static",
                        resolve: {
                            ticket: function () {
                                return ticket;
                            }
                        }
                    });
                };

                vm.intiailizeOpenLocation = function() {
                    vm.locationGroup = app.createLocationInputForSearch();
                };
                vm.intiailizeOpenLocation();

                vm.openLocation = function () {
                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };

                vm.clear = function () {
                    vm.dateRangeModel = {
                        startDate: todayAsString,
                        endDate: todayAsString
                    };
                    $scope.builder.builder.reset();
                    vm.getOrders();
                };

                $scope.builder =
                    app.createBuilder(
                        {
                            condition: "AND",
                            rules: []
                        },
                        angular.fromJson
                            (
                                [
                                    {
                                        "id": "groupName",
                                        "label": app.localize("Group"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "Quantity",
                                        "label": app.localize("Quantity"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                    {
                                        "id": "Total",
                                        "label": app.localize("Total"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                ]
                            )
                    );

            }
        ]);
})();