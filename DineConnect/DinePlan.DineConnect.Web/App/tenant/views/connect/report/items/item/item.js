﻿(function () {
    appModule.controller('tenant.views.connect.report.items.item', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.connectReport', "abp.services.app.tenantSettings", 'uiGridGroupingConstants', 'abp.services.app.commonLookup', 'abp.services.app.productGroup', 'abp.services.app.customerReport', 'abp.services.app.department' ,
        function ($scope, $modal, uiGridConstants, connectService, tenantSettingsService, uiGridGroupingConstants, commonLookupService, productGroupService, customerReportService, departmentService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.payments = [];
            vm.transactions = [];
            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];
            vm.loading = false;
            vm.advancedFiltersAreShown = false;
            vm.mycustomer = abp.features.getValue("DinePlan.DineConnect.Connect.Customer");
            vm.totalTicketAmount = '';
            vm.totalTickets = '';
            vm.totalOrders = '';
            vm.totalItems = '';
            vm.menuItemIds = [];
            vm.productGroups = [];
            vm.categories = [];
            vm.departments = [];
            vm.terminals = [];
            vm.users = [];
            vm.selectedGroups = [];
            vm.categoryIds = [];
            vm.selectedDepartments = [];
            vm.selectedTerminals = [];
            vm.selectedUsers = [];
            vm.tags = [];
            vm.requestParams = {
                locations: '',
                void: false,
                gift: false,
                noSales: false,
                comp: false,
                terminalName: '',
                lastModifiedUserName: '',
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null,
                filterByCategory: [],
                filterByGroup: [],
                filterByTag: []
            };

            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    });
            };
           
            vm.$onInit = function () {
                vm.getDecimals();
                fillDropDownGroup();
                fillDropDownDepartments();
                fillTerminalDropdown();
                fillUserDropdown();
                fillTagDropdown();
            };

            function fillDropDownGroup() {
                productGroupService.getProductGroupNames({})
                    .success(function (result) {
                        vm.productGroups = result.items;
                    });
            }

            

            function fillDropDownDepartments() {
                commonLookupService.getDepartments({})
                    .success(function (result) {
                        vm.departments = result;
                    })
                    .finally(function (result) {
                    });
            };

            function fillTerminalDropdown() {
                commonLookupService.getTerminalForCombobox().success(function (result) {
                    vm.terminals = result.items;
                }).finally(function (result) {
                });
            }

            function fillUserDropdown() {
                commonLookupService.getUserForCombobox().success(function (result) {
                    vm.users = result.items;
                }).finally(function (result) {
                });
            }

            function fillTagDropdown() {
              
            }

            vm.gridOptions = {
                showColumnFooter: true,
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                enableSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize('Location'),
                        field: 'locationName',
                        enableSorting: true,
                        grouping: { groupPriority: 1 },
                        sort: { priority: 1, direction: 'asc' },
                        minWidth: 180,
                        cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>',
                        footerCellTemplate: '<div class="ui-grid-cell-contents" >' + app.localize("Total") + '</div>'
                    },
                    {
                        name: app.localize('Group'),
                        field: 'groupName',
                        grouping: { groupPriority: 2 },
                        sort: { priority: 2, direction: 'asc' }
                    },
                    {
                        name: app.localize('Category'),
                        field: 'categoryName',
                        grouping: { groupPriority: 3 },
                        sort: { priority: 3, direction: 'asc' }
                    },
                    {
                        name: app.localize('MenuItem'),
                        field: 'menuItemName',
                        minWidth: 250
                    },
                    {
                        name: app.localize('Portion'),
                        field: 'menuItemPortionName',
                        enableSorting: false,
                        width: 150
                    },
                    {
                        name: app.localize('AvgPrice'),
                        field: 'price',
                        cellClass: "ui-ralign",
                        width: 120
                    },
                    {
                        name: app.localize('Quantity'),
                        field: 'quantity',
                        cellClass: "ui-ralign",
                        width: 120,
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                        customTreeAggregationFinalizerFn: function (aggregation) {
                            aggregation.rendered = aggregation.value;
                        },
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue()}}</div>'
                    },
                    {
                        name: app.localize('TotalAmount'),
                        field: 'total',
                        enableSorting: false,
                        cellClass: "ui-ralign",
                        width: 120,
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                        customTreeAggregationFinalizerFn: function (aggregation) {
                            aggregation.rendered = aggregation.value.toFixed(vm.decimals);
                        },
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue()}}</div>'
                    },
                    {
                        name: app.localize('%Qty(All)'),
                        field: 'total',
                        cellClass: "ui-ralign",
                        width: 120,
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                        customTreeAggregationFinalizerFn: function (aggregation) {
                            aggregation.rendered = (aggregation.value / vm.totalPrice * 100).toFixed(vm.decimals) + '%';
                        },
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue()}}</div>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                },
                data: []
            };

            vm.toggleFooter = function () {
                $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.toggleColumnFooter = function () {
                $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.getOrders = function () {
                vm.loading = true;
                vm.requestParams.filterByCategory = vm.categoryIds;

                vm.requestParams.filterByGroup = vm.selectedGroups;

                connectService.getItemSales({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationGroup: vm.locationGroup,
                    void: vm.requestParams.void,
                    gift: vm.requestParams.gift,
                    comp: vm.requestParams.comp,
                    noSales: vm.requestParams.noSales,
                    refund: vm.requestParams.refund,
                    filterByTerminal: vm.selectedTerminals,
                    filterByUser: vm.selectedUsers,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    stats: true,
                    menuItemIds: vm.menuItemIds,
                    filterByCategory: vm.requestParams.filterByCategory,
                    filterByGroup: vm.requestParams.filterByGroup,
                    filterByTag: vm.requestParams.filterByTag,
                    filterByDepartment: vm.selectedDepartments,
                    dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
                    //isSummary: true
                }).success(function (result) {
                    vm.totalPrice = 0;

                    for (let i = 0; i < result.menuList.items.length; i++) {
                        result.menuList.items[i].price = result.menuList.items[i].price && result.menuList.items[i].price.toFixed(vm.decimals) || 0;
                        if (result.menuList.items[i].total != null) {
                            vm.totalPrice += result.menuList.items[i].total;
                            result.menuList.items[i].total = result.menuList.items[i].total.toFixed(vm.decimals);
                        }
                    }

                    vm.gridOptions.data = result.menuList.items;

                    angular.element(document.querySelector('#topitems')).empty();
                    angular.element(document.querySelector('#topquantity')).empty();

                    Highcharts.chart('topitems', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{point.name}: {point.y} <br/>{point.percentage:.1f} %'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                showInLegend: true,
                                dataLabels: {
                                    enabled: false,
                                    format: '',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            name: app.localize('Quantity'),
                            colorByPoint: true,
                            data: $.parseJSON(JSON.stringify(result.quantities))
                        }]
                    });

                    Highcharts.chart('topquantity', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{point.name}: {point.y} <br/>{point.percentage:.1f} %'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                showInLegend: true,
                                dataLabels: {
                                    enabled: false,
                                    format: '',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            name: app.localize('Items'),
                            colorByPoint: true,
                            data: $.parseJSON(JSON.stringify(result.totals))
                        }]
                    });
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.exportToSummary = function () {
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"), app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;

                        connectService.getItemsSummaryExcel({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locationGroup: vm.locationGroup,
                            void: vm.requestParams.void,
                            gift: vm.requestParams.gift,
                            noSales: vm.requestParams.noSales,
                            refund: vm.requestParams.refund,
                            filterByTerminal: vm.selectedTerminals,
                            filterByUser: vm.selectedUsers,
                            skipCount: vm.requestParams.skipCount,
                            maxResultCount: vm.requestParams.maxResultCount,
                            sorting: vm.requestParams.sorting,
                            runInBackground: myConfirmation,
                            menuItemIds: vm.menuItemIds,
                            comp: vm.requestParams.comp,
                            filterByCategory: vm.requestParams.filterByCategory,
                            filterByGroup: vm.requestParams.filterByGroup,
                            filterByTag: vm.requestParams.filterByTag,
                            filterByDepartment: vm.selectedDepartments,
                            dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
                        }).success(function (result) {
                            if (result != null) {
                                app.downloadTempFile(result);
                            }
                        }).finally(function () {
                            vm.disableExport = false;
                            abp.ui.clearBusy("#MyLoginForm");
                        });
                    });
            };

            vm.exportToSummaryPrices = function () {
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"), app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;

                        connectService.getItemSalesByPriceExcel({
                            runInBackground: myConfirmation,
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locationGroup: vm.locationGroup,
                            void: vm.requestParams.void,
                            gift: vm.requestParams.gift,
                            noSales: vm.requestParams.noSales,
                            refund: vm.requestParams.refund,
                            filterByTerminal: vm.selectedTerminals,
                            filterByUser: vm.selectedUsers,
                            skipCount: vm.requestParams.skipCount,
                            maxResultCount: vm.requestParams.maxResultCount,
                            sorting: vm.requestParams.sorting,
                            menuItemIds: vm.menuItemIds,
                            comp: vm.requestParams.comp,
                            filterByCategory: vm.requestParams.filterByCategory,
                            filterByGroup: vm.requestParams.filterByGroup,
                            filterByTag: vm.requestParams.filterByTag,
                            filterByDepartment: vm.selectedDepartments,
                            dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
                        }).success(function (result) {
                            if (result != null) {
                                app.downloadTempFile(result);
                            }
                        }).finally(function () {
                            vm.disableExport = false;
                            abp.ui.clearBusy("#MyLoginForm");
                        });
                    });
            };

            vm.showDetails = function (ticket) {
                $modal.open({
                    templateUrl: '~/App/tenant/views/connect/report/orders/order/orderModal.cshtml',
                    controller: 'tenant.views.connect.report.orderModal as vm',
                    backdrop: 'static',
                    resolve: {
                        ticket: function () {
                            return ticket;
                        }
                    }
                });
            };

            vm.exportToExcel = function () {
                $modal.open({
                    templateUrl: '~/App/tenant/views/connect/report/items/item/exportModal.cshtml',
                    controller: 'tenant.views.connect.report.item.exportModal as vm',
                    backdrop: 'static',
                    resolve: {
                        dto: function () {
                            abp.ui.setBusy('#MyLoginForm');
                            return {
                                startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                                endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                                locationGroup: vm.locationGroup,
                                void: vm.requestParams.void,
                                gift: vm.requestParams.gift,
                                noSales: vm.requestParams.noSales,
                                refund: vm.requestParams.refund,
                                filterByTerminal: vm.selectedTerminals,
                                filterByUser: vm.selectedUsers,
                                skipCount: vm.requestParams.skipCount,
                                maxResultCount: vm.requestParams.maxResultCount,
                                sorting: vm.requestParams.sorting,
                                stats: false,
                                menuItemIds: vm.menuItemIds,
                                comp: vm.requestParams.comp,
                                filterByCategory: vm.requestParams.filterByCategory,
                                filterByGroup: vm.requestParams.filterByGroup,
                                filterByTag: vm.requestParams.filterByTag,
                                filterByDepartment: vm.selectedDepartments,
                                dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
                            };
                        }
                    }
                });
            };
            vm.exportWithOrderTagToExcel = function () {
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"), app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;

                        connectService.getItemsAndOrderTagExcel({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locationGroup: vm.locationGroup,
                            void: vm.requestParams.void,
                            gift: vm.requestParams.gift,
                            noSales: vm.requestParams.noSales,
                            refund: vm.requestParams.refund,
                            filterByTerminal: vm.selectedTerminals,
                            filterByUser: vm.selectedUsers,
                            skipCount: vm.requestParams.skipCount,
                            maxResultCount: vm.requestParams.maxResultCount,
                            sorting: vm.requestParams.sorting,
                            runInBackground: myConfirmation,
                            menuItemIds: vm.menuItemIds,
                            comp: vm.requestParams.comp,
                            filterByCategory: vm.requestParams.filterByCategory,
                            filterByGroup: vm.requestParams.filterByGroup,
                            filterByTag: vm.requestParams.filterByTag,
                            filterByDepartment: vm.selectedDepartments
                        }).success(function (result) {
                            if (result != null) {
                                app.downloadTempFile(result);
                            }
                        }).finally(function () {
                            vm.disableExport = false;
                            abp.ui.clearBusy("#MyLoginForm");
                        });
                    });
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.selectMenuitems = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/report/items/item/selectMenuItems.cshtml",
                    controller: "tenant.views.connect.report.items.item.selectMenuItems as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        selectedItems: function () {
                            return vm.menuItemIds;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.length > 0) {
                        vm.menuItemIds = result;
                    }
                });
            };

            vm.selectCategory = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/report/items/item/selectCategory.cshtml",
                    controller: "tenant.views.connect.report.items.item.selectCategory as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.length > 0) {
                        vm.categoryIds = result;
                    }
                });
            };
            vm.clear = function () {
                vm.menuItemIds = [];
                vm.categoryIds = [];
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };

                vm.intiailizeOpenLocation();
                vm.requestParams.void = false;
                vm.requestParams.gift = false;
                vm.requestParams.comp = false;
                vm.requestParams.noSales = false;
                vm.requestParams.terminalName = '';
                vm.requestParams.lastModifiedUserName = null;
                vm.requestParams.filterByCategory = [];
                vm.requestParams.filterByGroup = [];
                vm.requestParams.filterByTag = null;
                $scope.builder.builder.reset();
                vm.selectedDepartments = [];
                vm.departmentsInput = [];
                vm.getOrders();
            };

            vm.resetSelectedItem = function () {
                vm.menuItemIds = [];
            };
            vm.resetSelectedCategory = function () {
                vm.categoryIds = [];
            };
            vm.exportComboToExcel = function () {
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"), app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;

                        connectService.getItemComboSalesExcel({
                            runInBackground: myConfirmation,
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locationGroup: vm.locationGroup,
                            void: vm.requestParams.void,
                            gift: vm.requestParams.gift,
                            noSales: vm.requestParams.noSales,
                            refund: vm.requestParams.refund,
                            filterByTerminal: vm.selectedTerminals,
                            filterByUser: vm.selectedUsers,
                            skipCount: vm.requestParams.skipCount,
                            maxResultCount: vm.requestParams.maxResultCount,
                            sorting: vm.requestParams.sorting,
                            menuItemIds: vm.menuItemIds,
                            comp: vm.requestParams.comp,
                            filterByCategory: vm.requestParams.filterByCategory,
                            filterByGroup: vm.requestParams.filterByGroup,
                            filterByTag: vm.requestParams.filterByTag,
                            filterByDepartment: vm.selectedDepartments
                        }).success(function (result) {
                            if (result != null) {
                                app.downloadTempFile(result);
                            }
                        }).finally(function () {
                            vm.disableExport = false;
                            abp.ui.clearBusy("#MyLoginForm");
                        });
                    });
            };

            vm.exportWritersCafeToExcel = function () {
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"), app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;

                        customerReportService.getWritersCafeToExcel({
                            runInBackground: myConfirmation,
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locationGroup: vm.locationGroup,
                            void: vm.requestParams.void,
                            gift: vm.requestParams.gift,
                            noSales: vm.requestParams.noSales,
                            refund: vm.requestParams.refund,
                            filterByTerminal: vm.selectedTerminals,
                            filterByUser: vm.selectedUsers,
                            skipCount: vm.requestParams.skipCount,
                            maxResultCount: vm.requestParams.maxResultCount,
                            sorting: vm.requestParams.sorting,
                            menuItemIds: vm.menuItemIds,
                            comp: vm.requestParams.comp,
                            filterByCategory: vm.requestParams.filterByCategory,
                            filterByGroup: vm.requestParams.filterByGroup,
                            filterByTag: vm.requestParams.filterByTag,
                            filterByDepartment: vm.selectedDepartments
                        }).success(function (result) {
                            if (result != null) {
                                app.downloadTempFile(result);
                            }
                        }).finally(function () {
                            vm.disableExport = false;
                            abp.ui.clearBusy("#MyLoginForm");
                        });
                    });
            };

            $scope.builder =
                app.createBuilder(
                    {
                        condition: "AND",
                        rules: []
                    },
                    angular.fromJson
                        (
                            [
                                {
                                    "id": "groupName",
                                    "label": app.localize("Group"),
                                    "type": "string"
                                }, {
                                    "id": "categoryName",
                                    "label": app.localize("Category"),
                                    "type": "string"
                                },
                                {
                                    "id": "menuItemName",
                                    "label": app.localize("MenuItem"),
                                    "type": "string"
                                },
                                {
                                    "id": "menuItemPortionName",
                                    "label": app.localize("Portion"),
                                    "type": "string"
                                },

                                {
                                    "id": "price",
                                    "label": app.localize("AvgPrice"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                                {
                                    "id": "Quantity",
                                    "label": app.localize("Quantity"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                                {
                                    "id": "TotalAmount",
                                    "label": app.localize("TotalAmount"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                            ]
                        )
                );

            vm.departmentsInput = [];
            vm.selectDepartment = function () {

                var requestParams = {
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null
                };

                var dataOptions = {
                    title: app.localize('Select') + " " + app.localize('Department'),
                    serviceMethod: departmentService.getAll,
                    selectedData: vm.departmentsInput,
                    requestParams: requestParams
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/report/common/selectModal.cshtml",
                    controller: "tenant.views.connect.report.selectModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        dataOptions: function () {
                            return dataOptions;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.length > 0) {
                        vm.departmentsInput= result;
                        vm.selectedDepartments = vm.departmentsInput.map(el => el.name);
                    }
                });
            };

            vm.resetDepartmentSelectedItem = function () {
                vm.selectedDepartments = [];
                vm.departmentsInput = [];
            };



        }
    ]);
})();