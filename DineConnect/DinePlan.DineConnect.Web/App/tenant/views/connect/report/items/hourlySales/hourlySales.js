﻿(function () {
    appModule.controller("tenant.views.connect.report.hourlySales", [
        "$scope", "$uibModal", "uiGridConstants", "abp.services.app.connectReport", "abp.services.app.tenantSettings", "abp.services.app.commonLookup",
        function ($scope, $modal, uiGridConstants, connectService, tenantSettingsService, lookupService) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });
            vm.payments = [];
            vm.transactions = [];
            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];
            vm.loading = false;
            vm.advancedFiltersAreShown = false;
            vm.tallyAvailable = abp.setting.getBoolean("App.House.TallyIntegrationFlag");

            vm.menuItemIds = [];

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    });
            };

            vm.$onInit = function () {
                vm.getDecimals();
            };

            vm.requestParams = {
                locations: "",
                void: false,
                gift: false,
                refund: false,
                terminalName: "",
                lastModifiedUserName: "",
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.exportForTally = function () {
                abp.ui.setBusy("#MyLoginForm");
                connectService.getDepartmentTallyExcel({
                    startDate: moment(vm.dateRangeModel.startDate).format("YYYY-MM-DD"),
                    endDate: moment(vm.dateRangeModel.endDate).format("YYYY-MM-DD"),
                    locationGroup: vm.locationGroup
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        abp.ui.clearBusy("#MyLoginForm");
                    });
            };

            vm.showDetails = function (ticket) {
                $modal.open({
                    templateUrl: "~/App/tenant/views/connect/report/items/department/departmentModal.cshtml",
                    controller: "tenant.views.connect.report.departmentModal as vm",
                    backdrop: "static",
                    resolve: {
                        ticket: function () {
                            return ticket;
                        }
                    }
                });
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.gridOptions = {
                showColumnFooter: true,
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize("Category"),
                        field: "categoryName"
                    },
                    {
                        name: app.localize("MenuItem"),
                        field: "menuItemName"
                    },
                    {
                        name: app.localize("Portion"),
                        field: "menuItemPortionName"
                    },
                    {
                        name: app.localize("Hours"),
                        field: "hour"
                    },
                    {
                        name: app.localize("Quantity"),
                        field: "quantity",
                        cellClass: "ui-ralign",
                        aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue()}}</div>'
                    },
                    {
                        name: app.localize("Price"),
                        field: "price",
                        cellClass: "ui-ralign"
                    },
                    {
                        name: app.localize("Amount"),
                        field: "total",
                        cellClass: "ui-ralign",
                        aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        vm.getOrders();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;
                        vm.getOrders();
                    });
                },
                data: []
            };

            vm.toggleFooter = function () {
                $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.toggleColumnFooter = function () {
                $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.getOrders = function () {
                vm.loading = true;
                connectService.getItemHourlySales({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationGroup: vm.locationGroup,
                    void: vm.requestParams.void,
                    gift: vm.requestParams.gift,
                    comp: vm.requestParams.comp,
                    noSales: vm.requestParams.noSales,
                    refund: vm.requestParams.refund,
                    terminalName: vm.requestParams.terminalName,
                    lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    stats: true,
                    menuItemIds: vm.menuItemIds,
                    dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                })
                    .success(function (result) {
                        vm.gridOptions.totalItems = result.menuList.totalCount;

                        for (let i = 0; i < result.menuList.items.length; i++) {
                            if (result.menuList.items[i].price != null)
                                result.menuList.items[i].price = result.menuList.items[i].price.toFixed(vm.decimals);

                            if (result.menuList.items[i].total != null)
                                result.menuList.items[i].total = result.menuList.items[i].total.toFixed(vm.decimals);
                        }
                        vm.gridOptions.data = result.menuList.items;
                    }).finally(function () {
                        vm.loading = false;
                    });
            };

            vm.exportToSummary = function (mode) {
                abp.ui.setBusy("#salesForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        connectService.getItemHourlySalesExcel({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locationGroup: vm.locationGroup,
                            void: vm.requestParams.void,
                            gift: vm.requestParams.gift,
                            comp: vm.requestParams.comp,
                            noSales: vm.requestParams.noSales,
                            refund: vm.requestParams.refund,
                            terminalName: vm.requestParams.terminalName,
                            lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                            skipCount: 0,
                            maxResultCount: vm.gridOptions.totalItems,
                            sorting: vm.requestParams.sorting,
                            stats: true,
                            exportOutputType: mode,
                            runInBackground: myConfirmation,
                            menuItemIds: vm.menuItemIds
                        })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#salesForm");
                            });
                    });
            };

            vm.selectMenuitems = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/report/items/item/selectMenuItems.cshtml",
                    controller: "tenant.views.connect.report.items.item.selectMenuItems as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.length > 0) {
                        vm.menuItemIds = result;
                    }
                });
            };

            vm.clear = function () {
                vm.menuItemIds = [];
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };

                vm.intiailizeOpenLocation();

                vm.requestParams.void = false;
                vm.requestParams.gift = false;
                vm.requestParams.comp = false;
                vm.requestParams.noSales = false;
                vm.requestParams.terminalName = '';
                vm.requestParams.lastModifiedUserName = null;

                vm.getOrders();
            };

            vm.resetSelectedItem = function () {
                vm.menuItemIds = [];
            };

            // #region Builder

            $scope.builder =
                app.createBuilder(
                    {
                        condition: "AND",
                        rules: []
                    },
                    angular.fromJson
                        (
                            [
                                {
                                    "id": "PortionName",
                                    "label": app.localize("Portion"),
                                    "type": "string"
                                },
                                {
                                    "id": "Hour",
                                    "label": app.localize("Hour"),
                                    "type": "integer"
                                },
                                {
                                    "id": "Quantity",
                                    "label": app.localize("Quantity"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.1
                                    }
                                },
                                {
                                    "id": "Price",
                                    "label": app.localize("Price"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.1
                                    }
                                },
                                {
                                    "id": "Total",
                                    "label": app.localize("Total"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.1
                                    }
                                },

                            ]
                        )
                );


                // #endregion
        }
    ]);
})();