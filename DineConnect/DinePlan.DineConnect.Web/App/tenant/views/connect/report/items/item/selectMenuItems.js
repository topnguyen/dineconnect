﻿(function () {
    appModule.controller('tenant.views.connect.report.items.item.selectMenuItems', [
        '$scope', '$uibModalInstance', 'abp.services.app.menuItem', 'uiGridConstants', 'selectedItems',
        function ($scope, $uibModalInstance, menuItemService, uiGridConstants, selectedItems) {
            var vm = this;
            vm.loading = false;
            vm.saving = false;

            vm.selectedMenuItems = selectedItems;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.getMenuItems = function () {
                vm.loading = true;
                menuItemService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    isDeleted: false
                }).success(function (result) {
                    vm.menuItemOptions.totalItems = result.totalCount;
                    vm.menuItemOptions.data = result.items;
                    vm.setSelection();
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.menuItemOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                enableRowSelection: true,
                rowHeight: 30,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Id"),
                        field: "id",
                        maxWidth: 70
                    },
                    {
                        name: app.localize("Name"),
                        field: "name"
                    }
                    //, {
                    //    name: app.localize("AliasCode"),
                    //    field: "aliasCode"
                    //}, {
                    //    name: app.localize("AliasName"),
                    //    field: "aliasName"
                    //}
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        vm.getMenuItems();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.getSelectedItems();
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        vm.getMenuItems();
                    });
                },
                data: []
            };

            vm.getMenuItems();

            vm.save = function () {
                vm.getSelectedItems();
                $uibModalInstance.close(vm.selectedMenuItems);
            };

            vm.cancel = function () {
                vm.selectedMenuItems = [];
                $uibModalInstance.dismiss();
            };

            vm.getSelectedItems = function () {
                vm.selectedRows = $scope.gridApi.selection.getSelectedRows();
                angular.forEach(vm.selectedRows, function (row) {
                    if (!vm.selectedMenuItems) {
                        vm.selectedMenuItems = [];
                    }
                    vm.selectedMenuItems.push(row.id);
                });
            }

            vm.setSelection = function () {
                if (vm.selectedMenuItems.length == 0) {
                    $scope.gridApi.selection.clearSelectedRows();
                    return;
                }
                $scope.gridApi.grid.modifyRows(vm.menuItemOptions.data);
                $scope.gridApi.selection.clearSelectedRows();
                angular.forEach(vm.menuItemOptions.data,
                    function (value, key) {
                        vm.selectedMenuItems.some(function (selectedData, selectedKey) {
                            if (value.id == selectedData) {
                                $scope.gridApi.selection.selectRow(vm.menuItemOptions.data[key]);
                                return true;
                            }
                        });
                    });
            };
        }
    ]);
})();