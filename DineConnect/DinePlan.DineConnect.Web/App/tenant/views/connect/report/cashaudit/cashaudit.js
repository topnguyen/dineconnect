﻿(function () {
	appModule.controller("tenant.views.connect.report.cashaudit",
		[
			"$scope", "$uibModal", "uiGridConstants", "abp.services.app.cashAudit", "abp.services.app.commonLookup", "abp.services.app.tenantSettings",
			function ($scope, $modal, uiGridConstants, cashAuditAppService, commonLookup, tenantSettingsService) {

				var vm = this;

				vm.loading = false;

				vm.requestParams = {
					skipCount: 0,
					maxResultCount: app.consts.grid.defaultPageSize,
					sorting: null,
				};
				$scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
                $scope.format = $scope.formats[0];
				vm.dateRangeOptions = app.createDateRangePickerOptions();
				vm.$onInit = function () {
					vm.getUsers();
					vm.getLocations();
					vm.getDatetimeFormat();
				}
				vm.datetimeFormat = "YYYY-MM-DD HH:mm:ss";
				vm.dateFormat = "YYYY-MM-DD";

				vm.getDatetimeFormat = function () {
					tenantSettingsService.getAllSettings()
						.success(function (result) {
							console.log("data", result);

							vm.datetimeFormat = result.connect.dateTimeFormat;
							vm.dateFormat = result.connect.simpleDateFormat;
						});
				};

				vm.formatDatetime = function (value) {
					if (value) {
						var format = moment(value).format(vm.datetimeFormat.toUpperCase());
						return format;
					}
					return null;
				}
				vm.formatDate = function (value) {
					if (value) {
						var format = moment(value).format(vm.dateFormat.toUpperCase());
						return format;
					}
					return null;
				}

				vm.gridOptions = {
					showColumnFooter: true,
					enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
					enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
					paginationPageSizes: app.consts.grid.defaultPageSizes,
					paginationPageSize: app.consts.grid.defaultPageSize,
					useExternalPagination: true,
					useExternalSorting: true,
					appScopeProvider: vm,
					columnDefs: [
						{
							name: app.localize("LocationCode"),
							field: "locationCode"
						},
						{
							name: app.localize("Date"),
							field: "creationTime",
							cellClass: 'ui-calign',
							cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.formatDate(row.entity.creationTime)}}</div>',
						},
						{
							name: app.localize("Time"),
							field: "creationTime",
							cellClass: 'ui-calign',
							cellFilter: 'momentFormat: \'HH:mm\''
						},
						{
							name: app.localize("Terminal"),
							field: "pos"
						},
						{
							name: app.localize("CATotalCash_R"),
							field: "amount",
							cellClass: 'ui-ralign',
							cellFilter: 'number: 2'
						},
						{
							name: app.localize("Actual"),
							field: "actual",
							cellClass: 'ui-ralign',
							cellFilter: 'number: 2'
						},
						{
							name: app.localize("Difference"),
							field: "difference",
							cellClass: 'ui-ralign',
							cellFilter: 'number: 2'
						},
						{
							name: app.localize("Reason"),
							field: "reason",
							cellClass: 'ui-lalign',
						},
						{
							name: app.localize("Remarks"),
							field: "reMarks",
							cellClass: 'ui-lalign',
						}
						,
						{
							name: app.localize("ApprovedBy"),
							field: "approvedBy",
							cellClass: 'ui-lalign',
						},
						{
							name: app.localize("DoneBy"),
							field: "doneBy",
							cellClass: 'ui-lalign',
						}
					],
					onRegisterApi: function (gridApi) {
						$scope.gridApi = gridApi;
						$scope.gridApi.core.on.sortChanged($scope,
							function (grid, sortColumns) {
								if (!sortColumns.length || !sortColumns[0].field) {
									vm.requestParams.sorting = null;
								} else {
									vm.requestParams.sorting =
										sortColumns[0].field + " " + sortColumns[0].sort.direction;
								}
								vm.getCashAudits();
							});
						gridApi.pagination.on.paginationChanged($scope,
							function (pageNumber, pageSize) {
								vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
								vm.requestParams.maxResultCount = pageSize;
								vm.getCashAudits();
							});
					},
					data: []
				};

				vm.intiailizeOpenLocation = function () {
					vm.locationGroup = app.createLocationInputForSearch();
				};

				vm.intiailizeOpenLocation();
				vm.openLocation = function () {
					const modalInstance = $modal.open({
						templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
						controller: "tenant.views.connect.location.select.location as vm",
						backdrop: "static",
						keyboard: false,
						resolve: {
							location: function () {
								return vm.locationGroup;
							}
						}
					});
					modalInstance.result.then(function (result) {
						vm.locationGroup = result;
					});
				};

				vm.getCurrentCashAuditInput = function (confirmation, obj) {
					return {
						startDate: moment(vm.dateRangeModel.startDate).lang("en").format($scope.format),
						endDate: moment(vm.dateRangeModel.endDate).lang("en").format($scope.format),
						maxResultCount: vm.requestParams.maxResultCount,
						sorting: vm.requestParams.sorting,
						runInBackground: confirmation,
						skipCount: vm.requestParams.skipCount,
						locationGroup: vm.locationGroup,
						exportOutputType: obj,
						dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
					};
				};

				vm.exportToExcel = function (obj) {
					abp.ui.setBusy("#MyLoginForm");
					abp.message.confirm(app.localize("AreYouSure"),
						app.localize("RunInBackground"),
						function (isConfirmed) {
							var myConfirmation = false;
							if (isConfirmed) {
								myConfirmation = true;
							}
							vm.disableExport = true;
							cashAuditAppService.getCashAuditToExcel(vm.getCurrentCashAuditInput(myConfirmation, obj))
								.success(function (result) {
									if (result != null) {
										app.downloadTempFile(result);
									}
								}).finally(function () {
									abp.ui.clearBusy("#MyLoginForm");
									vm.disableExport = false;
								});
						});
				};

				vm.getCashAudits = function () {
					vm.loading = true;
					cashAuditAppService.getCashAudits(vm.getCurrentCashAuditInput())
						.success(function (result) {
							vm.gridOptions.totalItems = result.totalCount;
							vm.gridOptions.data = result.items;
						}).finally(function () {
							vm.loading = false;
						});
				};
				// #region Builder
				vm.locationsCode = [];
				$scope.builder =
					app.createBuilder(
						{
							condition: "AND",
							rules: []
						},
						angular.fromJson
							(
								[{
									"id": "pos",
									"label": app.localize("Terminal"),
									"type": "string"
								},
								{
									"id": "amount",
									"label": app.localize("CATotalCash_R"),
									"operators": [
										"equal",
										"less",
										"less_or_equal",
										"greater",
										"greater_or_equal"
									],
									"type": "double",
									"validation": {
										"min": 0,
										"step": 0.01
									}
								},
								{
									"id": "actual",
									"label": app.localize("Actual"),
									"operators": [
										"equal",
										"less",
										"less_or_equal",
										"greater",
										"greater_or_equal"
									],
									"type": "double",
									"validation": {
										"min": 0,
										"step": 0.01
									}
								},
								{
									"id": "reason",
									"label": app.localize("Reason"),
									"type": "string"
								},
								{
									"id": "reMarks",
									"label": app.localize("Remarks"),
									"type": "string"
								},
								{
									"id": "ApprovedBy",
									"label": app.localize("ApprovedBy"),
									"type": "string",
									"operators": [
										"equal",
										"not_equal",
									],
									type: 'string',
									plugin: 'selectize',
									plugin_config: {
										valueField: 'value',
										labelField: 'displayText',
										searchField: 'displayText',
										sortField: 'displayText',
										create: true,
										maxItems: 1,
										plugins: ['remove_button'],
										onInitialize: function () {
											var that = this;
											if (vm.users.length > 0) {
												vm.users.forEach(function (item) {
													item.value = item.displayText;
													that.addOption(item);
												});
											} else {
												commonLookup.getUserForCombobox({
												}).success(function (result) {
													vm.users = result.items;
													vm.users.forEach(function (item) {
														item.value = item.displayText;
														that.addOption(item);
													});
												}).finally(function (result) {
												});
											}
										}
									},
									valueSetter: function (rule, value) {
										rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
									}
								},
								{
									"id": "DoneBy",
									"label": app.localize("DoneBy"),
									"type": "string",
									"operators": [
										"equal",
										"not_equal",
									],
									type: 'string',
									plugin: 'selectize',
									plugin_config: {
										valueField: 'value',
										labelField: 'displayText',
										searchField: 'displayText',
										sortField: 'displayText',
										create: true,
										maxItems: 1,
										plugins: ['remove_button'],
										onInitialize: function () {
											var that = this;
											if (vm.users.length > 0) {
												vm.users.forEach(function (item) {
													item.value = item.displayText;
													that.addOption(item);
												});
											} else {
												commonLookup.getUserForCombobox({
												}).success(function (result) {
													vm.users = result.items;
													vm.users.forEach(function (item) {
														item.value = item.displayText;
														that.addOption(item);
													});
												}).finally(function (result) {
												});
											}
										}
									},
									valueSetter: function (rule, value) {
										rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
									}
								}
								]
							)
					);


				vm.getLocations = function () {
					commonLookup.getLocationCodeForCombobox({
					}).success(function (result) {
						vm.locationsCode = result.items;
						console.log('code', vm.locationsCode);
					}).finally(function (result) {
					});

					commonLookup.getLocationForCombobox({
					}).success(function (result) {
						vm.locations = result.items;
						console.log(vm.locations);
					}).finally(function (result) {
					});
				};
				vm.getUsers = function () {
					commonLookup.getUserForCombobox({
					}).success(function (result) {
						vm.users = result.items;
					}).finally(function (result) {
					});
				};
				var todayAsString = moment().format("YYYY-MM-DD");
				vm.clear = function () {
					vm.dateRangeModel = {
						startDate: todayAsString,
						endDate: todayAsString
					};
					$scope.builder.builder.reset();
					vm.getCashAudits();
				}

				// #endregion
			}
		]);
})();