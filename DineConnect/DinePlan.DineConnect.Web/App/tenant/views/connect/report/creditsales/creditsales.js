﻿(function () {
	appModule.controller("tenant.views.connect.report.creditsales",
		[
			"$scope", "$uibModal", "uiGridConstants", "abp.services.app.creditSalesReport", "abp.services.app.commonLookup", "abp.services.app.tenantSettings",
			function ($scope, $modal, uiGridConstants, creditSalesReportAppService, commonLookup, tenantSettingsService) {

				var vm = this;

				vm.loading = false;

				vm.requestParams = {
					skipCount: 0,
					maxResultCount: app.consts.grid.defaultPageSize,
					sorting: null,
				};

				vm.$onInit = function () {
					vm.getUsers();
					vm.getLocations();
					vm.getDatetimeFormat();
				}
								
				vm.datetimeFormat = "YYYY-MM-DD HH:mm:ss";
				vm.dateFormat = "YYYY-MM-DD";

				vm.getDatetimeFormat = function () {
					tenantSettingsService.getAllSettings()
						.success(function (result) {
							vm.datetimeFormat = result.connect.dateTimeFormat;
							vm.dateFormat = result.connect.simpleDateFormat;
						});
				};

				vm.formatDatetime = function (value) {
					if (value) {
						var format = moment(value).format(vm.datetimeFormat.toUpperCase());
						return format;
					}
					return null;
				}
				vm.formatDate = function (value) {
					if (value) {
						var format = moment(value).format(vm.dateFormat.toUpperCase());
						return format;
					}
					return null;
				}


				vm.dateRangeOptions = app.createDateRangePickerOptions();
				vm.locationsCode = [];

				vm.gridOptions = {
					showColumnFooter: true,
					enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
					enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
					paginationPageSizes: app.consts.grid.defaultPageSizes,
					paginationPageSize: app.consts.grid.defaultPageSize,
					useExternalPagination: true,
					useExternalSorting: true,
					appScopeProvider: vm,
					columnDefs: [
						{
							name: app.localize("Plant"),
							field: "plant"
						},
						{
							name: app.localize("DateTime"),
							field: "dateTime",
							cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.formatDatetime(row.entity.dateTime)}}</div>',
						},
						{
							name: app.localize("SalesOrderNo"),
							field: "salesOrderNo"
						},
						{
							name: app.localize("BillStatus"),
							field: "billStatus",
						},
						{
							name: app.localize("BillNumber"),
							field: "billNumber",
						},
						{
							name: app.localize("FullTaxInvoiceNumber"),
							field: "fullTaxInvoiceNumber",
						},
						{
							name: app.localize("RefInvoiceNo"),
							field: "refInvoiceNo",
						},
						{
							name: app.localize("CashierName"),
							field: "cashierName",
						},
						{
							name: app.localize("CustomerCode"),
							field: "customerCode"
						}
						,
						{
							name: app.localize("CustomerName"),
							field: "customerName",
							enableSorting: false,
						},
						{
							name: app.localize("CustomerAddress"),
							field: "customerAddress",
							enableSorting: false,
						},
						{
							name: app.localize("TaxIdenticationNumber"),
							field: "taxIdenticationNumber",
						}, {
							name: app.localize("CreditTerm"),
							field: "creditTerm",
							enableSorting: false,
						}, {
							name: app.localize("CustomerPhoneNumber"),
							field: "customerPhoneNumber"
						},
						{
							name: app.localize("RecipientName"),
							field: "recipientName"
						},
						{
							name: app.localize("SaleMode"),
							field: "saleMode"
						},
						{
							name: app.localize("SaleChannel"),
							field: "saleChannel"
						},

						{
							name: app.localize("ProductCostExcludeVAT"),
							field: "productCostExcludeVAT",
							cellFilter: 'number: 2',
							aggregationType: uiGridConstants.aggregationTypes.sum,
							footerCellTemplate:
								'<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'
						},
						{
							name: app.localize("ProductCostIncludeVAT"),
							field: "productCostIncludeVAT",
							cellFilter: 'number: 2',
							aggregationType: uiGridConstants.aggregationTypes.sum,
							footerCellTemplate:
								'<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'
						},
						{
							name: app.localize("ServiceCostExcludeVAT"),
							field: "serviceCostExcludeVAT",
							cellFilter: 'number: 2',
							aggregationType: uiGridConstants.aggregationTypes.sum,
							footerCellTemplate:
								'<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'
						},
						{
							name: app.localize("ServiceCostIncludeVAT"),
							field: "serviceCostIncludeVAT",
							cellFilter: 'number: 2',
							aggregationType: uiGridConstants.aggregationTypes.sum,
							footerCellTemplate:
								'<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'
						},
						{
							name: app.localize("WithholdingTaxAmountFromSAP"),
							field: "withholdingTaxAmountFromSAP",
							cellFilter: 'number: 2',
							aggregationType: uiGridConstants.aggregationTypes.sum,
							footerCellTemplate:
								'<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'
						},
						{
							name: app.localize("TotalCostIncludeVAT"),
							field: "totalCostIncludeVAT",
							cellFilter: 'number: 2',
							aggregationType: uiGridConstants.aggregationTypes.sum,
							footerCellTemplate:
								'<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'
						},
						{
							name: app.localize("WithholdingTaxDescriptionFromSAP"),
							field: "withholdingTaxDescriptionFromSAP",
						},
						{
							name: app.localize("Note"),
							field: "note"
						}
					],
					onRegisterApi: function (gridApi) {
						$scope.gridApi = gridApi;
						$scope.gridApi.core.on.sortChanged($scope,
							function (grid, sortColumns) {
								if (!sortColumns.length || !sortColumns[0].field) {
									vm.requestParams.sorting = null;
								} else {
									vm.requestParams.sorting =
										sortColumns[0].field + " " + sortColumns[0].sort.direction;
								}
								vm.getCreditSales();
							});
						gridApi.pagination.on.paginationChanged($scope,
							function (pageNumber, pageSize) {
								vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
								vm.requestParams.maxResultCount = pageSize;
								vm.getCreditSales();
							});
					},
					data: []
				};



				vm.intiailizeOpenLocation = function () {
					vm.locationGroup = app.createLocationInputForSearch();
				};

				vm.intiailizeOpenLocation();

				vm.openLocation = function () {
					const modalInstance = $modal.open({
						templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
						controller: "tenant.views.connect.location.select.location as vm",
						backdrop: "static",
						keyboard: false,
						resolve: {
							location: function () {
								return vm.locationGroup;
							}
						}
					});
					modalInstance.result.then(function (result) {
						vm.locationGroup = result;
					});
				};

				vm.getCurrentCreditSalesInput = function (confirmation, obj) {
					console.log($scope.builder);
					return {
						startDate: moment(vm.dateRangeModel.startDate).lang("en").format($scope.format),
						endDate: moment(vm.dateRangeModel.endDate).lang("en").format($scope.format),
						maxResultCount: vm.requestParams.maxResultCount,
						sorting: vm.requestParams.sorting,
						runInBackground: confirmation,
						skipCount: vm.requestParams.skipCount,
						locationGroup: vm.locationGroup,
						exportOutputType: obj,
						dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
					};
				};

				vm.exportToExcel = function (obj) {
					abp.ui.setBusy("#MyLoginForm");
					abp.message.confirm(app.localize("AreYouSure"),
						app.localize("RunInBackground"),
						function (isConfirmed) {
							var myConfirmation = false;
							if (isConfirmed) {
								myConfirmation = true;
							}
							vm.disableExport = true;
							creditSalesReportAppService.getCreditSalesToExcel(vm.getCurrentCreditSalesInput(myConfirmation, obj))
								.success(function (result) {
									if (result != null) {
										app.downloadTempFile(result);
									}
								}).finally(function () {
									abp.ui.clearBusy("#MyLoginForm");
									vm.disableExport = false;
								});
						});
				};

				vm.getCreditSales = function () {
					vm.loading = true;
					creditSalesReportAppService.getCreditSalesReport(vm.getCurrentCreditSalesInput())
						.success(function (result) {
							vm.gridOptions.totalItems = result.totalCount;
							vm.gridOptions.data = result.items;
							console.log(' result.items;', result.items);
						}).finally(function () {
							vm.loading = false;
						});
				};

				vm.billStatus = ["Sale", "Return", "CN", "DN"];
				// #region Builder
				$scope.builder =
					app.createBuilder(
						{
							condition: "AND",
							rules: []
						},
						angular.fromJson
							(
								[
									{
										"id": "SalesOrderNo",
										"label": app.localize("SalesOrderNo"),
										"type": "string"
									},
									{
										"id": "CashierName",
										"label": app.localize("CashierName"),
										"type": "string"
									},
									{
										"id": "CustomerCode",
										"label": app.localize("CustomerCode"),
										"type": "string"
									},
									{
										"id": "CustomerName",
										"label": app.localize("CustomerName"),
										"type": "string"
									},
									{
										"id": "BillStatus",
										"label": app.localize("BillStatus"),
									    "type": "string",
									    "operators": [
									        "equal",
									        "not_equal",
									    ],
									    plugin: 'selectize',
									    plugin_config: {
									        valueField: 'value',
									        labelField: 'displayText',
									        searchField: 'displayText',
									        sortField: 'displayText',
									        create: true,
									        maxItems: 1,
									        plugins: ['remove_button'],
									        onInitialize: function () {
									            var that = this;
												vm.billStatus.forEach(function (item) {
													item.value = item.displayText;
													let input = {
														'value': item,
														'displayText': item
													}
													that.addOption(input);
											  }).finally(function (result) {
											});
									        }
									    },
									    valueSetter: function (rule, value) {
									        rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
									    }
									},
									 {
										"id": "FullTaxInvoiceNumber",
										"label": app.localize("FullTaxInvoiceNumber"),
										"type": "string"
									}, {
										"id": "RefInvoiceNo",
										"label": app.localize("RefInvoiceNo"),
										"type": "string"
									}, {
										"id": "SaleMode",
										"label": app.localize("SaleMode"),
										"type": "string"
									}, {
										"id": "SaleChannel",
										"label": app.localize("SaleChannel"),
										"type": "string"
									},
								]
							)
					);

				vm.getLocations = function () {
					commonLookup.getLocationCodeForCombobox({
					}).success(function (result) {
						vm.locationsCode = result.items;
					}).finally(function (result) {
					});

					commonLookup.getLocationForCombobox({
					}).success(function (result) {
						vm.locations = result.items;
					}).finally(function (result) {
					});
				};
				vm.getUsers = function () {
					commonLookup.getUserForCombobox({
					}).success(function (result) {
						vm.users = result.items;
					}).finally(function (result) {
					});
				};
				var todayAsString = moment().format("YYYY-MM-DD");
				vm.clear = function () {
					vm.dateRangeModel = {
						startDate: todayAsString,
						endDate: todayAsString
					};
					$scope.builder.builder.reset();
					vm.getCreditSales();
				}
				// #endregion
			}
		]);
})();