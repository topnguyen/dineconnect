﻿(function () {
    //'use strict';
    appModule.component('topDownChart', {
        templateUrl: '~/App/tenant/views/connect/report/topdownsell/sharecomponent/chart/top-down-chart.cshtml',
        bindings: {
            title: '=',
            data: '=',
        },
        controller: TopDownChartController
    });
})();


function TopDownChartController($scope, $element, $attrs) {
    var vm = this;
    $scope.titleChart = vm.title;
    $scope.$on('changeChartTopDownSellReport', function (event, data) {
        if (data.typeChart == 'pie') {
            vm.buildPieChart(data.dataChart);
        } else if (data.typeChart == 'column') {
            vm.buildColumnChart(data.dataChart);
        } else {
            vm.buildBarChart(data.dataChart);
        }
    });

    vm.buildPieChart = function (dataChart) {
        angular.element(document.querySelector('#topdownsellchart')).empty();
        Highcharts.chart('topdownsellchart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{point.name}: {point.y}'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    showInLegend: true,
                    dataLabels: {
                        enabled: true,
                        format: '',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: app.localize('Quantity'),
                colorByPoint: true,
                data: $.parseJSON(JSON.stringify(dataChart))
            }]
        });
    }
    vm.buildColumnChart = function (dataChart) {
        angular.element(document.querySelector('#topdownsellchart')).empty();
        Highcharts.chart('topdownsellchart', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.2f}'
                    }
                }
            },

            tooltip: {
                enabled: false
            },

            series: [
                {
                    colorByPoint: true,
                    data: $.parseJSON(JSON.stringify(dataChart))
                }
            ]
        }
        );
    }
    vm.buildBarChart = function (dataChart) {
        angular.element(document.querySelector('#topdownsellchart')).empty();
        var lstCategory = dataChart.map(x => x.name);
        Highcharts.chart('topdownsellchart', {
            chart: {
                type: 'bar'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: lstCategory,
            },
            yAxis: {
                min: 0,
                title: {
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                enabled: false
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 80,
                floating: true,
                borderWidth: 1,
                backgroundColor: '#FFFFFF',
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [
                {
                    pointWidth: 20,
                    showInLegend: false,
                    data: dataChart
                },
            ]
        });
    }
}