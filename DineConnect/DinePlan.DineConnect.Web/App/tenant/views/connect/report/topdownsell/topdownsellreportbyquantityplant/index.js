﻿(function () {
    appModule.controller("tenant.views.connect.report.topdownsellreportbyquantityplant.index",
        [
            "$scope", "$uibModal", "uiGridConstants", "abp.services.app.topDownSellReport", "abp.services.app.connectLookup",
            "abp.services.app.customerReport", "abp.services.app.tenantSettings", "abp.services.app.commonLookup", 'abp.services.app.transactionType', 'lookupModal',
            'abp.services.app.department',
            'abp.services.app.paymentType',
            'uiGridGroupingConstants', 'abp.services.app.category', 'abp.services.app.productGroup',
            function ($scope,
                $modal,
                uiGridConstants,
                topDownSellReportAppService,
                connectLookupService,
                crService,
                tenantSettingsService,
                commonLookup,
                transactionService,
                lookupModal,
                departmentService,
                paymentTypeService,
                uiGridGroupingConstants,
                categoryService,
                productGroupService
            ) {
                var vm = this;
          
                vm.dataChart = [];
                vm.payments = [];
                vm.transactions = [];
                vm.currencyText = abp.features.getValue("DinePlan.DineConnect.Connect.Currency");
                vm.franchise = abp.features.getValue("DinePlan.DineConnect.Connect.Franchise") == "true";
                vm.tallyAvailable = abp.setting.getBoolean("App.House.TallyIntegrationFlag");
                vm.mycustomer = abp.features.getValue("DinePlan.DineConnect.Connect.Customer");


                vm.loading = false;
                vm.advancedFiltersAreShown = false;
                $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
                $scope.format = $scope.formats[0];

                vm.totalTicketAmount = "";
                vm.totalTickets = "";
                vm.totalOrders = "";
                vm.totalItems = "";
                vm.requestParams = {
                    locations: "",
                    payments: [],
                    locationSelect: "",
                    departmentSelect: "",
                    transactions: [],
                    ticketTags: "",
                    terminalName: "",
                    lastModifiedUserName: "",
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null,
                    userId: abp.session.userId,
                    credit: false,
                    refund: false,
                    ticketNo: "",
                    view: "Top",
                    viewNumber: 10,
                    typeOfChart: "column",
                    isAddOns: false,
                    isCombo: false,
                    days: "",

                };
                vm.paymentsInput = [];
                vm.departmentsInput = [];
                vm.transactionsInput = [];
                $scope.settings = {
                    dropdownToggleState: false,
                    time: {
                        fromHour: "06",
                        fromMinute: "00",
                        toHour: "23",
                        toMinute: "59"
                    },
                    noRange: false,
                    format: 24,
                    noValidation: true
                };
                var todayAsString = moment().format("YYYY-MM-DD");
                vm.dateRangeOptions = app.createDateRangePickerOptions();
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };

                vm.titleChart = app.localize("TopItemSalesByAmount");

                $scope.$on("$viewContentLoaded",
                    function () {
                        App.initAjax();
                    });



                vm.getEditionValue = function (item) {
                    return item.displayText;
                };

                vm.decimals = null;
                vm.getDecimals = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.decimals = result.connect.decimals;
                        }).finally(function () {
                        });
                };

                vm.lstView = [
                    { value: "Top", displayText: "Top" },
                    { value: "Down", displayText: "Down" },
                ];

                vm.lstTypeOfChart = [
                    { value: "column", displayText: "ColumnChart" },
                    { value: "bar", displayText: "BarChart" },
                    { value: "pie", displayText: "PieChart" },
                ];

                vm.lstDayOfWeek = [
                    { value: undefined, displayText: "All" },
                    { value: 1, displayText: "Monday" },
                    { value: 2, displayText: "Tuesday" },
                    { value: 3, displayText: "Wednesday" },
                    { value: 4, displayText: "Thursday" },
                    { value: 5, displayText: "Friday" },
                    { value: 6, displayText: "Saturday" },
                    { value: 0, displayText: "Sunday" },
                ];
                vm.locations = [];

                vm.$onInit = function () {
                    localStorage.clear();
                    vm.getDecimals();
                    vm.getTransactions();
                    vm.getPayments();
                    vm.getDepartments();
                    vm.getLocations();
                    vm.getUsers();
                };

                vm.lstMaterial = [];
                vm.openGroupMaster = function () {
                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/topdownsell/sharecomponent/materialgroup/material-group.cshtml",
                        controller: "tenant.views.connect.report.topdownsell.sharecomponent.materialgroup as vm",
                        backdrop: 'static',
                        resolve: {
                            category: function () {
                                return vm.lstMaterial;
                            }
                        }
                    });
                    modalInstance.result
                        .then(function (result) {
                            vm.lstMaterial = result;
                        }).finally(function () {
                        });
                };
               
                // #region Grid

                vm.disableExport = false;


                // #endregion
                // #region Location

                vm.intiailizeOpenLocation = function () {
                    vm.locationGroup = app.createLocationInputForSearch();
                };
                vm.intiailizeOpenLocation();

                vm.openLocation = function () {
                    const modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };
                vm.showDetails = function (ticket) {
                    $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/tickets/ticket/ticketModal.cshtml",
                        controller: "tenant.views.connect.report.ticketModal as vm",
                        backdrop: "static",
                        resolve: {
                            ticket: function () {
                                return ticket.id;
                            }
                        }
                    });
                };

                // #endregion
                // #region Exports
                vm.getTopDownSellInput = function (confirmation, obj) {
                    console.log('date', moment(vm.dateRangeModel.startDate).lang("en").format($scope.format));
                    return {
                        startDate: moment(vm.dateRangeModel.startDate).lang("en").format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).lang("en").format($scope.format),
                        locationGroup: vm.locationGroup,
                        payments: vm.paymentsInput,
                        departments: vm.departmentsInput,
                        ticketTags: vm.requestParams.ticketTags,
                        transactions: vm.transactionsInput,
                        terminalName: vm.requestParams.terminalName,
                        lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                        skipCount: vm.requestParams.skipCount,
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: "Amount",
                        credit: vm.requestParams.credit,
                        refund: vm.requestParams.refund,
                        ticketNo: vm.requestParams.ticketNo,
                        runInBackground: confirmation,
                        exportOutputType: obj,
                        viewNumber: vm.requestParams.viewNumber,
                        view: vm.requestParams.view,
                        isCombo: vm.requestParams.isCombo,
                        isAddOns: vm.requestParams.isAddOns,
                        days: vm.requestParams.days,
                        locationSelect: vm.requestParams.locationSelect,
                        departmentSelect: vm.requestParams.departmentSelect,
                        startHour: $scope.settings.time.fromHour,
                        endHour: $scope.settings.time.toHour,
                        startMinute: $scope.settings.time.fromMinute,
                        endMinute: $scope.settings.time.toMinute,
                        listMaterial: vm.lstMaterial,
                        dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                    };
                };               
                vm.getExportInput = function (confirmation, obj) {
                    var input = vm.getTopDownSellInput;
                    input.runInBackground = confirmation;
                    input.exportOutputType = obj;
                    return input;
                };
                vm.exportToExcel = function (exportType) {
                    abp.ui.setBusy("#MyLoginForm");
                    abp.message.confirm(app.localize("AreYouSure"),
                        app.localize("RunInBackground"),
                        function (isConfirmed) {
                            var myConfirmation = false;
                            if (isConfirmed) {
                                myConfirmation = true;
                                abp.notify.info(app.localize("RunInBackground"));
                            }
                            vm.disableExport = true;
                            var input = vm.getTopDownSellInput(myConfirmation, exportType);
                            topDownSellReportAppService.getTopDownSellReportByQuantityPlantReportToExport(input)
                                .success(function (result) {
                                    if (result != null) {
                                        app.downloadTempFile(result);
                                    }
                                })
                                .finally(function () {
                                    abp.ui.clearBusy("#MyLoginForm");
                                    vm.disableExport = false;
                                });
                        });
                };
                vm.clear = function () {
                    vm.dateRangeModel = {
                        startDate: todayAsString,
                        endDate: todayAsString
                    };
                    $scope.builder.builder.reset();
                    vm.lstMaterial = []; 
                    vm.requestParams.transactions = [];
                    vm.requestParams.payments = [];
                    vm.requestParams.departments = [];

                    vm.getAll();
                }
                // #region Builder
                $scope.builder =
                    app.createBuilder(
                        {
                            condition: "AND",
                            rules: [
                            ]
                        },
                        angular.fromJson
                            (
                                [
                                    {
                                        "id": "ProductCode",
                                        "label": app.localize("ProductCode"),
                                        "type": "string"
                                    }, {
                                        "id": "ProductName",
                                        "label": app.localize("ProductName"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "UnitPrice",
                                        "label": app.localize("UnitPrice"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                    {
                                        "id": "Quantity",
                                        "label": app.localize("TotalQuantity"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                ]
                            )
                    );

                // #region LocalStorage
                vm.getTransactions = function () {
                    connectLookupService.getTransactionTypesForCombobox({
                    }).success(function (result) {
                        localStorage.transactionTypes = JSON.stringify(result.items);
                    }).finally(function (result) {
                    });
                };
                vm.getPayments = function () {
                    connectLookupService.getPaymentTypes({
                    }).success(function (result) {
                        localStorage.paymentTypes = JSON.stringify(result.items);
                    }).finally(function (result) {
                    });
                };
                vm.lstdepartment = [];
                vm.getDepartments = function () {
                    connectLookupService.getDepartments({
                    }).success(function (result) {
                        localStorage.departments = JSON.stringify(result.items);
                        vm.departments = result.items;
                        var firstItem = { value: undefined, displayText: "All" }
                        vm.departments.unshift(firstItem);
                    }).finally(function (result) {
                    });
                };
                vm.lstLocation = [];
                vm.getLocations = function () {
                    commonLookup.getLocationForCombobox({
                    }).success(function (result) {
                        vm.locations = JSON.stringify(result.items);
                        var firstItem = { value: undefined, displayText: "All" }
                        vm.lstLocation = result.items;
                        vm.lstLocation.unshift(firstItem);
                    }).finally(function (result) {
                    });
                };
                vm.getUsers = function () {
                    commonLookup.getUserForCombobox({
                    }).success(function (result) {
                        vm.users = result.items;
                    }).finally(function (result) {
                    });
                };

                // #endregion

                vm.getAll = function () {
                    vm.loading = true;
                    topDownSellReportAppService.getTopDownSellReportByQuantityPlant(vm.getTopDownSellInput())
                        .success(function (result) {
                            console.log("result", result);
                            vm.data = result;
                            vm.dataChart = result.dataChart;
                            $scope.$parent.$broadcast('changeChartTopDownSellReport', {
                                typeChart: vm.requestParams.typeOfChart,
                                dataChart: vm.dataChart
                            });
                        }).finally(function () {
                            vm.loading = false;
                        });
                };

                $scope.sum = function (items, prop) {
                    return items.reduce(function (a, b) {
                        return a + b[prop];
                    }, 0);
                };



                vm.selectDepartment = function () {

                    var requestParams = {
                        skipCount: 0,
                        maxResultCount: app.consts.grid.defaultPageSize,
                        sorting: null
                    };

                    var dataOptions = {
                        title: app.localize('Select') + " " + app.localize('Department'),
                        serviceMethod: departmentService.getAll,
                        selectedData: vm.requestParams.departments,
                        requestParams: requestParams
                    };

                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/common/selectModal.cshtml",
                        controller: "tenant.views.connect.report.selectModal as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            dataOptions: function () {
                                return dataOptions;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        if (result.length > 0) {
                            vm.requestParams.departments = result;
                            vm.departmentsInput = vm.requestParams.departments.map(el => {
                                var el = { 'Value': el.id, 'DisplayText': el.name };
                                return el;
                            });
                        }
                    });
                };

                vm.resetDepartmentSelectedItem = function () {
                    vm.requestParams.departments = [];
                };

                vm.selectPayments = function () {

                    var requestParams = {
                        skipCount: 0,
                        maxResultCount: app.consts.grid.defaultPageSize,
                        sorting: null
                    };

                    var dataOptions = {
                        title: app.localize('Select') + ' ' + app.localize('Payment'),
                        serviceMethod: paymentTypeService.getAll,
                        selectedData: vm.requestParams.payments,
                        requestParams: requestParams
                    };

                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/common/selectModal.cshtml",
                        controller: "tenant.views.connect.report.selectModal as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            dataOptions: function () {
                                return dataOptions;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        if (result.length > 0) {
                            vm.requestParams.payments = result;
                            vm.paymentsInput = vm.requestParams.payments.map(el => el.id);
                        }
                    });
                },

                    vm.resetPaymentSelectedItem = function () {
                        vm.requestParams.payments = [];
                    };

                vm.selectTransactions = function () {
                    var requestParams = {
                        skipCount: 0,
                        maxResultCount: app.consts.grid.defaultPageSize,
                        sorting: null
                    };

                    var dataOptions = {
                        title: app.localize('Select') + ' ' + app.localize('Transaction'),
                        serviceMethod: transactionService.getAll,
                        selectedData: vm.requestParams.transactions,
                        requestParams: requestParams
                    };

                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/common/selectModal.cshtml",
                        controller: "tenant.views.connect.report.selectModal as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            dataOptions: function () {
                                return dataOptions;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        if (result.length > 0) {
                            vm.requestParams.transactions = result;
                            vm.transactionsInput = vm.requestParams.transactions.map(el => {
                                var el = { 'Value': el.id, 'DisplayText': el.name };
                                return el;
                            });
                        }
                    });
                };

                vm.resetTransactionSelectedItem = function () {
                    vm.requestParams.transactions = [];
                };

                vm.openModalChart = function (data, type) {
                    var dataChart = {
                        typeChart : type,
                        listChart: data.listItem.dataChart,
                        title: app.localize('TopDownSellReportByQuantityPlant')
                    };
                    const modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/topdownsell/sharecomponent/chartmodal/top-down-chart-modal.cshtml",
                        controller: "tenant.views.connect.report.topdownsell.sharecomponent.chartmodal as vm",
                        backdrop: "static",
                        keyboard: false,
                        windowClass: 'app-modal-window',
                        resolve: {
                            dataChart: function () {
                                return dataChart;
                            }
                        }
                    });
                }
            }
        ]);
})();