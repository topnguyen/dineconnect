﻿(function () {
    appModule.controller("tenant.views.connect.report.topdownsell.sharecomponent.chartmodal",
        ["$scope", "$uibModalInstance", "dataChart",
            function ($scope, $modalInstance, dataChart) {
                var vm = this;
                vm.titleChart = dataChart.title;
                vm.data = {
                    typeChart: dataChart.typeChart,
                    dataChart: dataChart.listChart
                };
                vm.initDataChart = function () {
                    $scope.$parent.$broadcast('changeChartTopDownSellReport', {
                        typeChart: vm.data.typeChart,
                        dataChart: vm.data.dataChart
                    });
                };
                setTimeout(function () { vm.initDataChart(); }, 300);

                vm.cancel = function () {
                    $modalInstance.dismiss();
                };
            }
        ]);
})();