﻿(function () {
    appModule.controller('tenant.views.connect.report.topdownsell.sharecomponent.materialgroup', [
        '$scope', '$uibModalInstance', 'abp.services.app.productGroup', 'category',
        function ($scope, $uibModalInstance, productGroupService, category) {
            var vm = this;
            //vm.lstCategory = category;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };
            vm.lstProductGroupSelect = category;
            var inTreeChangeEvent = false;
            vm.productGroup = {

                $tree: null,
                selectedOu: {
                    id: null,
                    code: null,
                    name: null,
                    set: function (ouInProductGroup) {
                        if (!ouInProductGroup) {
                            vm.productGroup.selectedOu.id = null;
                            vm.productGroup.selectedOu.code = null;
                            vm.productGroup.selectedOu.name = null;
                        } else {
                            vm.productGroup.selectedOu.id = ouInProductGroup.id;
                            vm.productGroup.selectedOu.code = ouInProductGroup.original.code;
                            vm.productGroup.selectedOu.name = ouInProductGroup.original.name;
                        }
                    }
                },



                getTreeDataFromServer: function (callback) {
                    productGroupService.getProductGroups({}).then(function (result) {
                        var treeData = _.map(result.data.items, function (item) {
                            console.log(' item.id', vm.lstCategory, item.id);
                            var index = vm.lstProductGroupSelect.findIndex(x => x == item.id);
                            return {
                                id: item.id,
                                parent: item.parentId ? item.parentId : '#',
                                code: item.code,
                                name: item.name,
                                text: item.name,
                                state: {
                                    opened: true,
                                    selected: index >= 0? true: false
                                    //selected: item.code == vm.lstCategory.groupCode ? true : false
                                }
                            };
                        });

                        callback(treeData);
                    });
                },

                init: function () {
                    vm.productGroup.getTreeDataFromServer(function (treeData) {
                        vm.productGroup.$tree = $('#ProductGroupEditTree');

                        vm.productGroup.$tree
                            .on('changed.jstree', function (e, data) {
                                //if (data.selected.length != 1) {
                                //    vm.productGroup.selectedOu.set(null);
                                //} else {
                                //    var selectedNode = data.instance.get_node(data.selected[0]);
                                //    vm.productGroup.selectedOu.set(selectedNode);
                                //}
                                if (!data.node) {
                                    return;
                                }

                                var wasInTreeChangeEvent = inTreeChangeEvent;
                                if (!wasInTreeChangeEvent) {
                                    inTreeChangeEvent = true;
                                }

                                var childrenNodes;

                                if (data.node.state.selected) {
                                    selectNodeAndAllParents(vm.productGroup.$tree.jstree('get_parent', data.node));

                                    childrenNodes = $.makeArray(vm.productGroup.$tree.jstree('get_children_dom', data.node));
                                    vm.productGroup.$tree.jstree('select_node', childrenNodes);

                                } else {
                                    childrenNodes = $.makeArray(vm.productGroup.$tree.jstree('get_children_dom', data.node));
                                    vm.productGroup.$tree.jstree('deselect_node', childrenNodes);
                                }

                                if (!wasInTreeChangeEvent) {
                                    $scope.$apply(function () {
                                        vm.lstProductGroupSelect = getSelectedProductNames();
                                        console.log('vm.lstProductGroupSelect', vm.lstProductGroupSelect);
                                    });
                                    inTreeChangeEvent = false;
                                }
                            })
                            .jstree({
                                'core': {
                                    data: treeData,
                                    multiple: true,
                                    //multiple: true,
                                },
                                "types": {
                                    "default": {
                                        "icon": "fa fa-folder tree-item-icon-color icon-lg"
                                    },
                                    "file": {
                                        "icon": "fa fa-file tree-item-icon-color icon-lg"
                                    }
                                },
                                'checkbox': {
                                    keep_selected_style: false,
                                    three_state: false,
                                    cascade: ''
                                },
                                plugins: ['checkbox', 'types']
                            });
                    });
                },

                reload: function () {
                    vm.productGroup.getTreeDataFromServer(function (treeData) {
                        vm.productGroup.$tree.jstree(true).settings.core.data = treeData;
                        vm.productGroup.$tree.jstree('refresh');
                    });
                }
            };

            function selectNodeAndAllParents(node) {
                vm.productGroup.$tree.jstree('select_node', node, true);
                var parent = vm.productGroup.$tree.jstree('get_parent', node);
                if (parent) {
                    selectNodeAndAllParents(parent);
                }
            };
            function getSelectedProductNames() {
                var permissionNames = [];

                var selectedPermissions = vm.productGroup.$tree.jstree('get_selected', true);
                for (var i = 0; i < selectedPermissions.length; i++) {
                    permissionNames.push(selectedPermissions[i].original.id);
                }

                return permissionNames;
            };

            vm.productGroup.init();

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
            vm.save = function () {
                console.log('abcs', vm.productGroup);
                $uibModalInstance.close(vm.lstProductGroupSelect);
            }

        }]);
})();