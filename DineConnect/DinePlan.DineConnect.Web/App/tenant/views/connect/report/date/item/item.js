﻿(function () {
    appModule.controller('tenant.views.connect.report.date.item', [
        '$scope', '$uibModal', 'uiGridConstants',  'abp.services.app.connectReport', 'abp.services.app.location',
        function ($scope, $modal, uiGridConstants, connectService, locationService) {
            var vm = this;
            vm.requestParams = {
                locations: ''
            }
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.today = moment();
            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];
            vm.selectedDates = [];
            vm.removeFromSelected = function (dt) {
                vm.selectedDates.splice(vm.selectedDates.indexOf(dt), 1);
            }

            vm.gridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize('Location'),
                        field: 'locationName',
                        enableSorting: false
                    },
                    {
                        name: app.localize('Category'),
                        field: 'categoryName',
                        enableSorting: false
                    },
                    {
                        name: app.localize('MenuItem'),
                        field: 'menuItemName'
                    },
                    {
                        name: app.localize('Portion'),
                        field: 'menuItemPortionName',
                        enableSorting: false
                    },
                    {
                        name: app.localize('Quantity'),
                        field: 'quantity',
                        cellFilter: 'number: 3',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Price'),
                        field: 'price',
                        cellFilter: 'number: 3',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Total'),
                        field: 'total',
                        cellFilter: 'number: 3',
                        cellClass: 'ui-ralign',
                        enableSorting: false
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getOrders();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;
                        vm.getOrders();
                    });
                },
                data: []
            };

            $scope.convertMyDate = function (thedate) {
                var tempstr = thedate.toString();
                var newstr = tempstr.toString().replace(/GMT.*/g, "");
                newstr = newstr + " UTC";
                return new Date(newstr);
            };

            $scope.convertAll = function(allDates) {
                var cDate = [];
                for (var i = 0, len = allDates.length; i < len; i++) {
                    cDate.push($scope.convertMyDate(allDates[i]));
                }
                return cDate;
            }
           
            vm.getItems = function () {
                vm.loading = true;
                connectService.getDaysItemSales({
                    allDates: $scope.convertAll(vm.selectedDates),
                    locations: vm.requestParams.locations
                }).success(function (result) {
                    vm.result = result;
                }).finally(function (result) {
                    vm.loading = false;
                });
            }
           
            vm.getLocations = function () {
                vm.loading = true;
                locationService.getLocationBasedOnUser({
                    userId: abp.session.userId
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                }).finally(function (result) {
                    vm.loading = false;
                });
            };

           
           

            vm.getLocations();

        }
    ]);
})();