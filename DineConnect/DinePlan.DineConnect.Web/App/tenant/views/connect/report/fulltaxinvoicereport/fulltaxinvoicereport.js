﻿(function () {
    appModule.controller("tenant.views.connect.report.fulltaxinvoicereport",
        [
            "$scope", "$uibModal", "uiGridConstants", "abp.services.app.fullTaxInvoiceReport", "uiGridGroupingConstants", "abp.services.app.tenantSettings",
            function ($scope, $modal, uiGridConstants, fullTaxInvoiceReportAppService, uiGridGroupingConstants, tenantSettingsService) {

                var vm = this;
                vm.loading = false;
                vm.requestParams = {
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null,
                };
                                
                vm.datetimeFormat = "YYYY-MM-DD HH:mm:ss";
                vm.dateFormat = "YYYY-MM-DD";

                vm.getDatetimeFormat = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.datetimeFormat = result.connect.dateTimeFormat;
                            vm.dateFormat = result.connect.simpleDateFormat;
                        });
                };

                vm.formatDatetime = function (value) {
                    if (value) {
                        var format = moment(value).format(vm.datetimeFormat.toUpperCase());
                        return format;
                    }
                    return null;
                }
                vm.formatDate = function (value) {
                    if (value) {
                        var format = moment(value).format(vm.dateFormat.toUpperCase());
                        return format;
                    }
                    return null;
                }

                vm.dateRangeOptions = app.createDateRangePickerOptions();
                var todayAsString = moment().format("YYYY-MM-DD");              
                vm.intiailizeOpenLocation = function () {
                    vm.locationGroup = app.createLocationInputForSearch();
                };
                vm.$onInit = function () {
                    vm.getDatetimeFormat();
                }
                vm.intiailizeOpenLocation();

                vm.openLocation = function () {
                    const modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };


                vm.exportToExcel = function (obj) {
                    fullTaxInvoiceReportAppService.getFullTaxInvoiceReportToExport(vm.getDataInput(false, obj))
                        .success(function (result) {
                            if (result != null) {
                                app.downloadTempFile(result);
                            }
                        }).finally(function () {
                            abp.ui.clearBusy("#MyLoginForm");
                            vm.disableExport = false;
                        });
                };

                vm.clear = function () {
                    vm.dateRangeModel = {
                        startDate: todayAsString,
                        endDate: todayAsString
                    };
                    $scope.builder.builder.reset();
                    vm.getData();
                }

                vm.getData = function () {
                    vm.loading = true;
                    console.log('fullTaxInvoiceReportAppService', fullTaxInvoiceReportAppService);
                    var input = vm.getDataInput(false);
                    console.log('input', vm.getDataInput(false));
                    fullTaxInvoiceReportAppService.getFullTaxInvoiceReport(input)
                        .success(function (result) {
                            vm.data = result;
                            console.log('result', result);
                        }).finally(function () {
                            vm.loading = false;
                        });
                };
                vm.isUndefinedOrNull = function (val) {
                    return angular.isUndefined(val) || val == null || val == '';
                };
                vm.getDataInput = function (confirmation, obj) {                   
                    return {
                        startDate: moment(vm.dateRangeModel.startDate).lang("en").format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).lang("en").format($scope.format),
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting,
                        runInBackground: confirmation,
                        skipCount: vm.requestParams.skipCount,
                        locationGroup: vm.locationGroup,
                        exportOutputType: obj,
                        dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
                    };
                };

                // #region Builder

                $scope.builder =
                    app.createBuilder(
                        {
                            condition: "AND",
                            rules: []
                        },
                        angular.fromJson
                            (
                                [       
                                    {
                                        "id": "FullTaxInvoiceNumber",
                                        "label": app.localize("FullTaxInvoiceNumber"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "CustomerName",
                                        "label": app.localize("CustomerName"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "TaxId",
                                        "label": app.localize("TaxId"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "PlantId",
                                        "label": app.localize("PlantId"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "PlantName",
                                        "label": app.localize("PlantName"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "Service",
                                        "label": app.localize("Service"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "AmountExcludeVAT",
                                        "label": app.localize("AmountExcludeVAT"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                    {
                                        "id": "Vat",
                                        "label": app.localize("Vat"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                    {
                                        "id": "TotalAmount",
                                        "label": app.localize("TotalAmount"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                    {
                                        "id": "Remark",
                                        "label": app.localize("Remark"),
                                        "type": "string"
                                    },
                                ]
                            )
                    );

                // #endregion

            }
        ]);
})();