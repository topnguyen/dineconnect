﻿ (function () {
    appModule.controller("tenant.views.connect.report.internalsalemeetingreport",
        [
            "$scope", "$uibModal", "uiGridConstants", "abp.services.app.internalSaleReport", "uiGridGroupingConstants", "abp.services.app.tenantSettings",
            function ($scope, $modal, uiGridConstants, internalSaleReportAppService, uiGridGroupingConstants, tenantSettingsService) {

                var vm = this;

                vm.loading = false;
                var todayAsString = moment().format("YYYY-MM-DD");
                vm.requestParams = {
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null,
                };
                               
                vm.datetimeFormat = "YYYY-MM-DD HH:mm:ss";
                vm.dateFormat = "YYYY-MM-DD";
                vm.decimals = null;
                vm.getDecimals = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.decimals = result.connect.decimals;
                        }).finally(function () {
                        });
                };
                vm.getDatetimeFormat = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.datetimeFormat = result.connect.dateTimeFormat;
                            vm.dateFormat = result.connect.simpleDateFormat;
                        });
                };

                vm.formatDatetime = function (value) {
                    if (value) {
                        var format = moment(value).format(vm.datetimeFormat.toUpperCase());
                        return format;
                    }
                    return null;
                }
                vm.formatDate = function (value) {
                    if (value) {
                        var format = moment(value).format(vm.dateFormat.toUpperCase());
                        return format;
                    }
                    return null;
                }
                vm.$onInit = function () {
                    vm.getDatetimeFormat();
                    vm.getDecimals();
                }


                vm.dateRangeOptions = app.createDateRangePickerOptions();
                vm.title = "";
                vm.intiailizeOpenLocation = function () {
                    vm.locationGroup = app.createLocationInputForSearch();
                };

                vm.intiailizeOpenLocation();

                vm.openLocation = function () {
                    const modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };


                vm.exportToExcel = function (obj) {
                    abp.ui.setBusy("#MyLoginForm");
                    vm.disableExport = true;
                    internalSaleReportAppService.getInternalSaleReportToExcel(vm.getDataInput(false, obj))
                        .success(function (result) {
                            if (result != null) {
                                app.downloadTempFile(result);
                            }
                        }).finally(function () {
                            abp.ui.clearBusy("#MyLoginForm");
                            vm.disableExport = false;
                        });
                };

                vm.getData = function () {
                    vm.title = 'During the day ' + moment(vm.dateRangeModel.startDate).lang("en").format(this.dateFormat.toUpperCase()) + ' To ' + moment(vm.dateRangeModel.endDate).lang("en").format(this.dateFormat.toUpperCase());
                    vm.loading = true;
                    internalSaleReportAppService.getInternalSaleReport(vm.getDataInput())
                        .success(function (result) {
                            vm.data = result;
                            console.log(' vm.dataRoot', vm.data);
                        }).finally(function () {
                            vm.loading = false;
                        });
                };

                vm.getDataInput = function (confirmation, obj) {
                    return {
                        startDate: moment(vm.dateRangeModel.startDate).lang("en").format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).lang("en").format($scope.format),
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting,
                        runInBackground: confirmation,
                        skipCount: vm.requestParams.skipCount,
                        locationGroup: vm.locationGroup,
                        exportOutputType: obj,
                        dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
                    };
                };
                // #region Builder

                $scope.builder =
                    app.createBuilder(
                        {
                            condition: "AND",
                            rules: []
                        },
                        angular.fromJson
                            (
                                [
                                    {
                                        "id": "TransactionNo",
                                        "label": app.localize("TransactionNo"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "InternalSaleType",
                                        "label": app.localize("InternalSaleType"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "OREmployeeID",
                                        "label": app.localize("OREmployeeID"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "ORDepartmentCode",
                                        "label": app.localize("ORDepartmentCode"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "ORDepartmentName",
                                        "label": app.localize("ORDepartmentName"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "ORCostCenter",
                                        "label": app.localize("ORCostCenter"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "ORIO",
                                        "label": app.localize("ORIO"),
                                        "type": "string"
                                    },
                                ]
                            )
                    );
                vm.clear = function () {
                    vm.dateRangeModel = {
                        startDate: todayAsString,
                        endDate: todayAsString
                    };
                    $scope.builder.builder.reset();
                    vm.getData();
                }
                // #endregion

            }
        ])
})();