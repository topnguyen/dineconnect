﻿(function () {
    appModule.controller("tenant.views.connect.report.speedofservicereport",
        [
            "$scope", "$uibModal", "uiGridConstants", "abp.services.app.speedOfServiceReport", "uiGridGroupingConstants", "stats", "abp.services.app.tenantSettings",
            function ($scope, $modal, uiGridConstants, speedOfReportAppService, uiGridGroupingConstants, stats, tenantSettingsService) {

                var vm = this;

                vm.loading = false;

                vm.requestParams = {
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null,
                };
                               
                vm.datetimeFormat = "YYYY-MM-DD HH:mm:ss";
                vm.dateFormat = "YYYY-MM-DD";

                vm.getDatetimeFormat = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.datetimeFormat = result.connect.dateTimeFormat;
                            vm.dateFormat = result.connect.simpleDateFormat;
                        });
                };

                vm.formatDatetime = function (value) {
                    if (value) {
                        var format = moment(value).format(vm.datetimeFormat.toUpperCase());
                        return format;
                    }
                    return null;
                }
                vm.formatDate = function (value) {
                    if (value) {
                        var format = moment(value).format(vm.dateFormat.toUpperCase());
                        return format;
                    }
                    return null;
                }
                vm.$onInit = function () {
                    vm.getDatetimeFormat();
                }
                vm.dateRangeOptions = app.createDateRangePickerOptions();
                vm.title = "Speed of Service"
                vm.gridOptions = {
                    showColumnFooter: true,
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    treeCustomAggregations: {
                        //variance: { label: 'AVG: ', menuTitle: 'Agg: Var', aggregationFn: stats.aggregator.sumSquareErr, cellFilter: 'datetimefilter',},
                        variance: { aggregationFn: stats.aggregator.sumSquareErr, cellFilter: 'datetimefilter',},
                    },
                    columnDefs: [
                        {
                            name: app.localize("POS Name"),
                            field: "pos",
                            minWidth: 100,
                            grouping: { groupPriority: 1 },
                            cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>',
                        },
                        {
                            name: app.localize("Plant"),
                            minWidth: 100,
                            field: "plant",
                            grouping: { groupPriority: 2 },
                            cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>',
                        }, 
                        {
                            name: app.localize("PlantName"),
                            minWidth: 100,
                            field: "plantName",
                        } ,
                        {
                            name: app.localize("Staff"),
                            minWidth: 100,
                            field: "staff"
                        },
                        {
                            name: app.localize("TicketNumber"),
                            minWidth: 100,
                            field: "ticketNumber",
                        },
                        {
                            name: app.localize("Date&Time"),
                            minWidth: 100,
                            field: "dateTime",
                            cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.formatDatetime(row.entity.dateTime)}}</div>',
                            footerCellTemplate: '<div class="ui-grid-cell-contents" >AVG</div>'
                        },
                        {
                            name: app.localize("OrderTime"),
                            minWidth: 100,
                            field: "decimal_OrderTime",
                            cellFilter: 'datetimefilter',
                            treeAggregationType: 'variance',
                            footerCellTemplate: '<div class="ui-grid-cell-contents" >{{col.getAggregationValue() | datetimefilter }}</div>',
                            enableSorting: false,
                        },
                        {
                            name: app.localize("PackingTime"),
                            minWidth: 100,
                            field: "decimal_PackingTime",
                            treeAggregationType: 'variance',
                            cellFilter: 'datetimefilter',
                            footerCellTemplate: '<div class="ui-grid-cell-contents" >{{col.getAggregationValue() | datetimefilter }}</div>',
                            enableSorting: false,
                        },
                        {
                            name: app.localize("TotalTime"),
                            minWidth: 100,
                            field: "decimal_TotalTime",
                            treeAggregationType: 'variance',
                            cellFilter: 'datetimefilter',
                            footerCellTemplate: '<div class="ui-grid-cell-contents" >{{col.getAggregationValue() | datetimefilter }}</div>',
                            enableSorting: false,
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope,
                            function (grid, sortColumns) {
                                if (!sortColumns.length || !sortColumns[0].field) {
                                    vm.requestParams.sorting = null;
                                } else {
                                    vm.requestParams.sorting =
                                        sortColumns[0].field + " " + sortColumns[0].sort.direction;
                                }
                                vm.getCreditCard();
                            });
                        gridApi.pagination.on.paginationChanged($scope,
                            function (pageNumber, pageSize) {
                                vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                                vm.requestParams.maxResultCount = pageSize;
                                vm.getCreditCard();
                            });
                    },
                    data: []
                };
                vm.intiailizeOpenLocation = function () {
                    vm.locationGroup = app.createLocationInputForSearch();
                };

                vm.intiailizeOpenLocation();

                vm.openLocation = function () {
                    const modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };


                vm.exportToExcel = function (obj) {
                    abp.ui.setBusy("#MyLoginForm");
                    abp.message.confirm(app.localize("AreYouSure"),
                        app.localize("RunInBackground"),
                        function (isConfirmed) {
                            var myConfirmation = false;
                            if (isConfirmed) {
                                myConfirmation = true;
                            }
                            vm.disableExport = true;
                            speedOfReportAppService.getSpeedOfServiceReportToExport(vm.getDataInput(myConfirmation, obj))
                                .success(function (result) {
                                    if (result != null) {
                                        app.downloadTempFile(result);
                                    }
                                }).finally(function () {
                                    abp.ui.clearBusy("#MyLoginForm");
                                    vm.disableExport = false;
                                });
                        });
                };

                vm.getData = function () {
                    vm.loading = true;
                    speedOfReportAppService.getSpeedOfServiceReport(vm.getDataInput())
                        .success(function (result) {
                            vm.data = result;
                            vm.title = result.title;
                            console.log(' vm.dataRoot', vm.data);
                        }).finally(function () {
                            vm.loading = false;
                        });
                };

                vm.getDataInput = function (confirmation, obj) {

                    console.log($scope.builder);

                    return {
                        startDate: moment(vm.dateRangeModel.startDate).lang("en").format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).lang("en").format($scope.format),
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting,
                        runInBackground: confirmation,
                        skipCount: vm.requestParams.skipCount,
                        locationGroup: vm.locationGroup,
                        exportOutputType: obj,
                        dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
                    };
                };
                $scope.formatTimeSpan = function (input) {
                    var indexDot = input.indexOf(".");
                    if (indexDot > 0) {
                        return input.slice(0, indexDot);
                    } else {
                        return input;
					}
				}

                // #region Builder

                $scope.builder =
                    app.createBuilder(
                        {
                            condition: "AND",
                            rules: []
                        },
                        angular.fromJson
                            (
                                [
                                    {
                                        "id": "Plant",
                                        "label": app.localize("Plant"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "PlantName",
                                        "label": app.localize("PlantName"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "Pos",
                                        "label": app.localize("POSName"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "Staff",
                                        "label": app.localize("Staff"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "TicketNumber",
                                        "label": app.localize("TicketNumber"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "DateTime",
                                        "label": app.localize("DateTime"),
                                        "type": "string",
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                    },
                                    //{
                                    //    "id": "OrderTime",
                                    //    "label": app.localize("OrderTime"),
                                    //    "type": "string",
                                    //    "operators": [
                                    //        "equal",
                                    //        "less",
                                    //        "less_or_equal",
                                    //        "greater",
                                    //        "greater_or_equal"
                                    //    ],
                                    //},
                                    //{
                                    //    "id": "PackingTime",
                                    //    "label": app.localize("PackingTime"),
                                    //    "type": "string",
                                    //    "operators": [
                                    //        "equal",
                                    //        "less",
                                    //        "less_or_equal",
                                    //        "greater",
                                    //        "greater_or_equal"
                                    //    ],
                                    //},
                                    //{
                                    //    "id": "TotalTime",
                                    //    "label": app.localize("TotalTime"),
                                    //    "type": "string",
                                    //    "operators": [
                                    //        "equal",
                                    //        "less",
                                    //        "less_or_equal",
                                    //        "greater",
                                    //        "greater_or_equal"
                                    //    ],
                                    //}

                                ]
                            )
                    );

                var todayAsString = moment().format("YYYY-MM-DD");
                vm.clear = function () {
                    vm.dateRangeModel = {
                        startDate: todayAsString,
                        endDate: todayAsString
                    };
                    $scope.builder.builder.reset();
                    vm.getData();
                }

                // #endregion

            }
        ]).service('stats', function () {
            var service = {
                aggregator: {
                    sumSquareErr: function (aggregation, fieldValue, numValue, row) {
                        if (!isNaN(numValue) && row.entity.taxId == null) {
                            if (typeof (aggregation.value) === 'undefined') {
                                aggregation.value = numValue;
                            } else {
                                aggregation.value += numValue;
                                aggregation.value = aggregation.value / 2;
                            }
                        }
                    },
                   
                }
            }
            return service;
        }).filter("datetimefilter", function ($filter) {
            return function (value) {
                console.log('datetimefilter', value)
                //if (!value) { return; }
                if (value == 0) {
                    return "00:00:00";
                } else if (!value) {
                    return;
				}
                function z(n) { return (n < 10 ? '0' : '') + n; }
                var seconds = Math.floor(value % 60);
                var minutes = Math.floor(value % 3600 / 60);
                var hours = Math.floor(value / 3600);
                var result = (z(hours) + ':' + z(minutes) + ':' + z(seconds));
                return result;
            }
        });


})();