﻿(function () {
    appModule.controller("tenant.views.connect.report.departmentSummary", [
        "$scope", "$uibModal", "uiGridConstants", "abp.services.app.connectReport", "abp.services.app.location", "abp.services.app.tenantSettings",
        function ($scope, $modal, uiGridConstants, connectService, locationService, tenantSettingsService) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });
            vm.payments = [];
            vm.transactions = [];
            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];
            vm.loading = false;
            vm.advancedFiltersAreShown = false;
            vm.tallyAvailable = abp.setting.getBoolean("App.House.TallyIntegrationFlag");

            vm.requestParams = {
                locations: "",
                void: false,
                gift: false,
                refund: false,
                terminalName: "",
                lastModifiedUserName: "",
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    }).finally(function () {
                    });
            };

            vm.$onInit = function () {
                vm.getDecimals();
            }

            vm.gridOptions = {
                showColumnFooter: true,
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize("Department"),
                        field: "departmentName",
                        enableSorting: false

                    },
                    {
                        name: app.localize("TotalTicketCount"),
                        field: "totalTicketCount",
                        enableSorting: false,
                        cellClass: "ui-ralign",
                        aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue()}}</div>'
                    },
                    {
                        name: app.localize("Total"),
                        field: "total",
                        enableSorting: false,
                        cellClass: "ui-ralign",
                        aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        vm.getOrders();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;
                        vm.getOrders();
                    });
                },
                data: []
            };

            vm.toggleFooter = function () {
                $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.toggleColumnFooter = function () {
                $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.getOrders = function () {

                vm.loading = true;
                connectService.getDepartmentSales({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationGroup: vm.locationGroup,
                    void: vm.requestParams.void,
                    gift: vm.requestParams.gift,
                    refund: vm.requestParams.refund,
                    terminalName: vm.requestParams.terminalName,
                    lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                })
                    .success(function (result) {
                        vm.gridOptions.totalItems = result.departmentList.totalCount;
                        for (let i = 0; i < result.departmentList.items.length; i++) {
                            if (result.departmentList.items[i].total != null) {
                                result.departmentList.items[i].total = result.departmentList.items[i].total.toFixed(vm.decimals);
                            }                            
                        }
                        vm.gridOptions.data = result.departmentList.items;

                    }).finally(function () {
                        vm.loading = false;
                    });
            };


            vm.exportToExcel = function (mode) {
                if (vm.gridOptions.totalItems === 0) return;
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        connectService.getDepartmentExcel({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locationGroup: vm.locationGroup,
                            void: vm.requestParams.void,
                            gift: vm.requestParams.gift,
                            refund: vm.requestParams.refund,
                            terminalName: vm.requestParams.terminalName,
                            lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                            skipCount: vm.requestParams.skipCount,
                            maxResultCount: vm.requestParams.maxResultCount,
                            sorting: vm.requestParams.sorting,
                            exportOutputType: mode,
                            runInBackground: myConfirmation
                        })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });

                    });

            };
            vm.exportForTally = function () {

                abp.ui.setBusy("#MyLoginForm");
                connectService.getDepartmentTallyExcel({
                    startDate: moment(vm.dateRangeModel.startDate).format("YYYY-MM-DD"),
                    endDate: moment(vm.dateRangeModel.endDate).format("YYYY-MM-DD"),
                    locationGroup: vm.locationGroup
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        abp.ui.clearBusy("#MyLoginForm");
                    });
            };

            vm.showDetails = function (ticket) {
                $modal.open({
                    templateUrl: "~/App/tenant/views/connect/report/items/department/departmentModal.cshtml",
                    controller: "tenant.views.connect.report.departmentModal as vm",
                    backdrop: "static",
                    resolve: {
                        ticket: function () {
                            return ticket;
                        }
                    }
                });
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            $scope.builder =
                app.createBuilder(
                    {
                        condition: "AND",
                        rules: []
                    },
                    angular.fromJson
                        (
                            [
                                {
                                    "id": "departmentName",
                                    "label": app.localize("Department"),
                                    "type": "string"
                                },
                                {
                                    "id": "TotalTicketCount",
                                    "label": app.localize("TotalTicketCount"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                                {
                                    "id": "Total",
                                    "label": app.localize("Total"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                            ]
                        )
                );
            vm.clear = function () {
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };
                $scope.builder.builder.reset();
                vm.getOrders();
            }

        }
    ]);
})();