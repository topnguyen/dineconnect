﻿(function () {
    appModule.controller('tenant.views.connect.report.till.tilltransaction', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.tillAccount',
        'abp.services.app.location', 'abp.services.app.connectLookup', "abp.services.app.tenantSettings",
        function ($scope, $modal, uiGridConstants, tillService, locationService, lookupService, tenantSettingsService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.loading = false;
            vm.advancedFiltersAreShown = false;

            vm.requestParams = {
                locations: '',
                accounts: '',
                accountType: 0,
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null,
                lastModifiedUserName: ''
            };

            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    }).finally(function () {
                    });
            };

            vm.datetimeFormat = "YYYY-MM-DD HH:mm:ss";

            vm.getDatetimeFormat = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.datetimeFormat = result.connect.dateTimeFormat;
                    });
            };

            vm.formatDatetime = function (value) {
                if (value) {
                    var format = moment(value).format(vm.datetimeFormat.toUpperCase());
                    return format;
                }
                return null;
            }

            vm.$onInit = function() {
                vm.getDecimals();
                vm.getDatetimeFormat();
            };

            vm.gridOptions = {
                showColumnFooter: true,
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize('Location'),
                        field: 'locationName'
                    },
                    {
                        name: app.localize('Time'),
                        field: 'transactionTime',
                        cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.formatDatetime(row.entity.transactionTime)}}</div>',
                    },
                    {
                        name: app.localize('Account'),
                        field: 'tillAccountName'
                    },
                    {
                        name: app.localize('Remarks'),
                        field: 'remarks'
                    },
                    {
                        name: app.localize('User'),
                        field: 'user'
                    },
                    {
                        name: app.localize('Total'),
                        field: 'totalAmount',
                        cellClass: 'ui-ralign',
                        aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'
                    }

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getOrders();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;
                        vm.getOrders();
                    });
                },
                data: []
            };

            vm.getOrders = function () {
                var allAccounts = vm.requestParams.accounts;
                if (vm.requestParams.accounts == '') {
                    allAccounts = vm.accounts;
                }

                vm.loading = true;
                tillService.getTillTransactionReport({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locations: vm.locations,
                    accounts: allAccounts,
                    userName: vm.requestParams.lastModifiedUserName,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                })
                    .success(function (result) {
                        vm.gridOptions.totalItems = result.totalCount;
                        for (let i = 0; i < result.items.length; i++) {
                            if (result.items[i].totalAmount != null) {
                                result.items[i].totalAmount = result.items[i].totalAmount.toFixed(vm.decimals);
                            }
                        }
                        vm.gridOptions.data = result.items;
                    }).finally(function () {
                        vm.loading = false;
                    });
            };

            vm.accounts = [];
            vm.getAccounts = function () {
                vm.loading = true;
                lookupService.getTillAccounts({
                }).success(function (result) {
                    vm.accounts = $.parseJSON(JSON.stringify(result.items));
                }).finally(function (result) {
                    vm.loading = false;
                });
            };
            vm.getAccounts();

            vm.exportToSummary = function (mode) {
                var allLocations = vm.requestParams.locations;
                if (vm.requestParams.locations == '') {
                    allLocations = vm.locations;
                }

                var allAccounts = vm.requestParams.accounts;
                if (vm.requestParams.accounts == '') {
                    allAccounts = vm.accounts;
                }
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        tillService.getTillTransactionReportExcel({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locations: allLocations,
                            accounts: allAccounts,
                            userName: vm.requestParams.lastModifiedUserName,
                            skipCount: vm.requestParams.skipCount,
                            maxResultCount: vm.requestParams.maxResultCount,
                            sorting: vm.requestParams.sorting,
                            exportOutputType: mode,
                            runInBackground: myConfirmation,
                            dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                        })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });
                    });
            };

          
            vm.locationGroup = app.createLocationInputForCreateSingleLocation();

            vm.currentLocation = null;
            vm.openLocation = function () {

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.currentLocation = result.locations[0];
                        vm.locations = result.locations;
                    } else {
                        vm.location = null;
                        vm.currentLocation = null;
                    }
                });
            };

            vm.clear = function () {
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };
                $scope.builder.builder.reset();
                vm.getOrders();
            };
            $scope.builder =
                app.createBuilder(
                    {
                        condition: "AND",
                        rules: []
                    },
                    angular.fromJson
                        (
                            [
                                {
                                    "id": "TillAccount.Name",
                                    "label": app.localize("Account"),
                                    "type": "string",
                                },
                                {
                                    "id": "Remarks",
                                    "label": app.localize("Remarks"),
                                    "type": "string",
                                },
                                {
                                    "id": "User",
                                    "label": app.localize("User"),
                                    "type": "string",
                                },
                                {
                                    "id": "totalAmount",
                                    "label": app.localize("Total"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                }
                            ]
                        )
                );
        }
    ]);
})();