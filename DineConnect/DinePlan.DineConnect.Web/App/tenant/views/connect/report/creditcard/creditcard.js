﻿(function () {
    appModule.controller("tenant.views.connect.report.creditcard",
        [
            "$scope", "$uibModal", "uiGridConstants", "abp.services.app.creditCardReport", "abp.services.app.commonLookup", "abp.services.app.tenantSettings",
            function ($scope, $modal, uiGridConstants, credicashReportAppService, commonLookup, tenantSettingsService) {

                var vm = this;

                vm.loading = false;

                vm.requestParams = {
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null,
                };

                vm.$onInit = function () {
                    vm.getUsers();
                    vm.getLocations();
                    vm.getDatetimeFormat();
                }
                                
                vm.datetimeFormat = "YYYY-MM-DD HH:mm:ss";
                vm.dateFormat = "YYYY-MM-DD";

                vm.getDatetimeFormat = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.datetimeFormat = result.connect.dateTimeFormat;
                            vm.dateFormat = result.connect.simpleDateFormat;
                        });
                };

                vm.formatDatetime = function (value) {
                    if (value) {
                        var format = moment(value).format(vm.datetimeFormat.toUpperCase());
                        return format;
                    }
                    return null;
                }
                vm.formatDate = function (value) {
                    if (value) {
                        var format = moment(value).format(vm.dateFormat.toUpperCase());
                        return format;
                    }
                    return null;
                }


                vm.dateRangeOptions = app.createDateRangePickerOptions();
                vm.locationsCode = [];

                vm.gridOptions = {
                    showColumnFooter: true,
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    columnDefs: [
                        {
                            name: app.localize("LocationCode"),
                            field: "locationCode"
                        }, 
                        {
                            name: app.localize("MID"),
                            field: "mid",
                            enableSorting: false,
                        },
                        {
                            name: app.localize("TID"),
                            field: "tid",
                            enableSorting: false,
                        },
                        {
                            name: app.localize("TicketNo"),
                            field: "ticketNo"
                        },
                        {
                            name: app.localize("DepartmentGroup"),
                            field: "deftGroup",
                        },
                        {
                            name: app.localize("Deft"),
                            field: "deft",
                        },
                        {
                            name: app.localize("DateWithTime"),
                            field: "dateWithTime",
                            cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.formatDatetime(row.entity.dateWithTime)}}</div>',
                        },
                        {
                            name: app.localize("TotalAmount"),
                            field: "totalAmount",
                            cellFilter: 'number: 2'
                        },
                        {
                            name: app.localize("PaidAmount"),
                            field: "paidAmount",
                            cellFilter: 'number: 2'
                        },
                        {
                            name: app.localize("PaymentType"),
                            field: "paymentType"
                        }
                        ,
                        {
                            name: app.localize("CardIssuer"),
                            field: "cardIssuer",
                            enableSorting: false,
                        },
                        {
                            name: app.localize("CardNo"),
                            field: "cardNo",
                            enableSorting: false,
                        },
                        {
                            name: app.localize("TransactonID"),
                            field: "transactonID",
                            enableSorting: false,

                        }, {
                            name: app.localize("BlueCardNo"),
                            field: "blueCardNo",
                            enableSorting: false,
                        }, {
                            name: app.localize("Cashier"),
                            field: "cashier"
                        },
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope,
                            function (grid, sortColumns) {
                                if (!sortColumns.length || !sortColumns[0].field) {
                                    vm.requestParams.sorting = null;
                                } else {
                                    vm.requestParams.sorting =
                                        sortColumns[0].field + " " + sortColumns[0].sort.direction;
                                }
                                vm.getCreditCard();
                            });
                        gridApi.pagination.on.paginationChanged($scope,
                            function (pageNumber, pageSize) {
                                vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                                vm.requestParams.maxResultCount = pageSize;
                                vm.getCreditCard();
                            });
                    },
                    data: []
                };

             

                vm.intiailizeOpenLocation = function () {
                    vm.locationGroup = app.createLocationInputForSearch();
                };

                vm.intiailizeOpenLocation();

                vm.openLocation = function () {
                    const modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };

                vm.getCurrentCreditCardInput = function (confirmation, obj) {

                    console.log($scope.builder);

                    return {
                        startDate: moment(vm.dateRangeModel.startDate).lang("en").format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).lang("en").format($scope.format),
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting,
                        runInBackground: confirmation,
                        skipCount: vm.requestParams.skipCount,
                        locationGroup: vm.locationGroup,
                        exportOutputType: obj,
                        dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
                    };
                };

                vm.exportToExcel = function (obj) {
                    abp.ui.setBusy("#MyLoginForm");
                    abp.message.confirm(app.localize("AreYouSure"),
                        app.localize("RunInBackground"),
                        function (isConfirmed) {
                            var myConfirmation = false;
                            if (isConfirmed) {
                                myConfirmation = true;
                            }
                            vm.disableExport = true;
                            credicashReportAppService.getCreditCardToExcel(vm.getCurrentCreditCardInput(myConfirmation, obj))
                                .success(function (result) {
                                    if (result != null) {
                                        app.downloadTempFile(result);
                                    }
                                }).finally(function () {
                                    abp.ui.clearBusy("#MyLoginForm");
                                    vm.disableExport = false;
                                });
                        });
                };

                vm.getCreditCard = function () {
                    vm.loading = true;
                    credicashReportAppService.getCreditCardReport(vm.getCurrentCreditCardInput())
                        .success(function (result) {
                            vm.gridOptions.totalItems = result.totalCount;
                            vm.gridOptions.data = result.items;
                            console.log(' result.items;', result.items);
                        }).finally(function () {
                            vm.loading = false;
                        });
                };
                // #region Builder
                $scope.builder =
                    app.createBuilder(
                        {
                            condition: "AND",
                            rules:[]
                        },
                        angular.fromJson
                            (
                                [
                                    //{
                                    //    "id": "LocationCode",
                                    //    "label": app.localize("LocationCode"),
                                    //    "type": "string",
                                    //    "operators": [
                                    //        "equal",
                                    //        "not_equal",
                                    //    ],
                                    //    plugin: 'selectize',
                                    //    plugin_config: {
                                    //        valueField: 'value',
                                    //        labelField: 'displayText',
                                    //        searchField: 'displayText',
                                    //        sortField: 'displayText',
                                    //        create: true,
                                    //        maxItems: 1,
                                    //        plugins: ['remove_button'],
                                    //        onInitialize: function () {
                                    //            var that = this;
                                    //            if (vm.locationsCode.length > 0) {
                                    //                vm.locationsCode.forEach(function (item) {
                                    //                    item.value = item.displayText;
                                    //                    that.addOption(item);
                                    //                });
                                    //            } else {
                                    //                commonLookup.getLocationCodeForCombobox({
                                    //                }).success(function (result) {
                                    //                    vm.locationsCode = result.items;
                                    //                    vm.locationsCode.forEach(function (item) {
                                    //                        item.value = item.displayText;
                                    //                        that.addOption(item);
                                    //                    });
                                    //                }).finally(function (result) {
                                    //                });
                                    //            }
                                    //        }
                                    //    },
                                    //    valueSetter: function (rule, value) {
                                    //        rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                    //    }
                                    //},
                                    {
                                        "id": "MID",
                                        "label": app.localize("MID"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "TID",
                                        "label": app.localize("TID"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "TicketNo",
                                        "label": app.localize("TicketNo"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "DeftGroup",
                                        "label": app.localize("DepartmentGroup"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "Deft",
                                        "label": app.localize("Department"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "FormatDateWithTime",
                                        "label": app.localize("DateWithTime"),
                                        "type": "date",
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "placeholder": 'yyyy/mm/dd',
                                    },
                                    {
                                        "id": "TotalAmount",
                                        "label": app.localize("TotalAmount"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                    {
                                        "id": "PaidAmount",
                                        "label": app.localize("PaidAmount"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                    {
                                        "id": "PaymentType",
                                        "label": app.localize("PaymentType"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "CardIssuer",
                                        "label": app.localize("CardIssuer"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "CardNo",
                                        "label": app.localize("CardNo"),
                                        "type": "string"
                                    },    {
                                        "id": "TransactonID",
                                        "label": app.localize("TransactonID"),
                                        "type": "string"
                                    },  {
                                        "id": "BlueCardNo",
                                        "label": app.localize("BlueCardNo"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "Cashier",
                                        "label": app.localize("Cashier"),
                                        "type": "string",
                                        "operators": [
                                            "equal",
                                            "not_equal",
                                        ],
                                        type: 'string',
                                        plugin: 'selectize',
                                        plugin_config: {
                                            valueField: 'value',
                                            labelField: 'displayText',
                                            searchField: 'displayText',
                                            sortField: 'displayText',
                                            create: true,
                                            maxItems: 1,
                                            plugins: ['remove_button'],
                                            onInitialize: function () {
                                                var that = this;
                                                if (vm.users.length > 0) {
                                                    vm.users.forEach(function (item) {
                                                        item.value = item.displayText;
                                                        that.addOption(item);
                                                    });
                                                } else {
                                                    commonLookup.getUserForCombobox({
                                                    }).success(function (result) {
                                                        vm.users = result.items;
                                                        vm.users.forEach(function (item) {
                                                            item.value = item.displayText;
                                                            that.addOption(item);
                                                        });
                                                    }).finally(function (result) {
                                                    });
                                                }
                                            }
                                        },
                                        valueSetter: function (rule, value) {
                                            rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                        }
                                    }
                                ]
                            )
                    );

                vm.getLocations = function () {
                    commonLookup.getLocationCodeForCombobox({
                    }).success(function (result) {
                        vm.locationsCode = result.items;
                    }).finally(function (result) {
                    });

                    commonLookup.getLocationForCombobox({
                    }).success(function (result) {
                        vm.locations = result.items;
                    }).finally(function (result) {
                    });
                };
                vm.getUsers = function () {
                    commonLookup.getUserForCombobox({
                    }).success(function (result) {
                        vm.users = result.items;
                    }).finally(function (result) {
                    });
                };
                var todayAsString = moment().format("YYYY-MM-DD");
                vm.clear = function () {
                    vm.dateRangeModel = {
                        startDate: todayAsString,
                        endDate: todayAsString
                    };
                    $scope.builder.builder.reset();
                    vm.getCreditCard();
                }
                // #endregion
            }
        ]);
})();