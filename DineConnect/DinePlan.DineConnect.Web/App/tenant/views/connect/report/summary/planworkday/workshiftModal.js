﻿(function () {
    appModule.controller('tenant.views.connect.report.summary.workShiftModal', [
        '$scope', "abp.services.app.centralReport", '$uibModalInstance', 'workday', "abp.services.app.tenantSettings",
        function($scope, centralReportAppService, $modalInstance, workday, tenantSettingsService) {
            var vm = this;
            vm.workday = workday;

            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];

            vm.uniqueId = null;
            vm.decimals = null;
            vm.getDecimals = function() {
                tenantSettingsService.getAllSettings()
                    .success(function(result) {
                        vm.decimals = result.connect.decimals;
                    }).finally(function() {
                    });
            };

            vm.getDecimals();

            vm.close = function() {
                $modalInstance.dismiss();
            };

            vm.datetimeFormat = "YYYY-MM-DD HH:mm:ss";

            vm.getDatetimeFormat = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.datetimeFormat = result.connect.dateTimeFormat;
                    });
            };

            vm.formatDatetime = function (value) {
                if (value) {
                    var format = moment(value).format(vm.datetimeFormat.toUpperCase());
                    return format;
                }
                return null;
            }
            vm.getDatetimeFormat();
            vm.getWorkdayData = function() {
                vm.loading = true;
                console.log(vm.workday);
                centralReportAppService.getCentralShiftReport({
                        workPeriodId: vm.workday.wid,
                        workShiftId: vm.uniqueId
                    })
                    .success(function(result) {
                        vm.ticket = result;
                    }).finally(function() {
                        vm.loading = false;
                    });
            };

            $scope.selectedWorkShift = function(value) {
                vm.uniqueId = value;
            }

            vm.sum = function(items, prop) {
                if (items == null) {
                    return 0;
                }
                return items.reduce(function(a, b) {
                        return b[prop] == null ? a : a + b[prop];
                }, 0);
            };

            vm.exportToSummary = function(mode) {
                vm.loading = true;

                centralReportAppService.getCentralShiftExport({
                    centralShiftReportInput: {
                        workPeriodId: vm.workday.wid,
                        workShiftId: vm.uniqueId
                    },
                    exportOutputType: mode,
                    workPeriod: vm.workday,
                    dateReport: moment(vm.workday.reportStartDay).format($scope.format)
                })
                    .success(function (result) {
                        if (result != null) {
                            app.downloadTempFile(result);
                        }
                    }).finally(function () {
                        vm.loading = false;
                    });
            };
        }
    ]);
})();