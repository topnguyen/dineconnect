﻿(function () {
    appModule.controller('tenant.views.connect.report.summary.terminalModal', [
        '$scope', "abp.services.app.centralReport", '$uibModalInstance', 'workday', "abp.services.app.tenantSettings", "abp.services.app.terminal",
        function ($scope, centralReportAppService, $modalInstance, workday, tenantSettingsService,terminalService) {
            var vm = this;
            vm.workday = workday;
            vm.refTerminalName = [];
            vm.terminals = [];

            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    }).finally(function () {
                    });
            };

            vm.getTerminals = function () {
                terminalService.apiGetTerminals({})
                    .success(function (result) {
                    vm.terminals = result;
                }).finally(function () {
                });
            };
            vm.datetimeFormat = "YYYY-MM-DD HH:mm:ss";

            vm.getDatetimeFormat = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.datetimeFormat = result.connect.dateTimeFormat;
                        console.log("sdas", vm.datetimeFormat);
                    });
            };

            vm.formatDatetime = function (value) {
                if (value) {
                    var format = moment(value).format(vm.datetimeFormat.toUpperCase());
                    return format;
                }
                return null;
            }
            vm.getDatetimeFormat();
            vm.getDecimals();
            vm.getTerminals();

            vm.close = function () {
                $modalInstance.dismiss();
            };

            vm.getWorkdayData = function () {
                var terminalNames = [];
                angular.forEach(vm.refTerminalName, function (item) { 
                    terminalNames.push(item.name);
                });

                vm.loading = true;
                centralReportAppService.getCentralTerminalReport({
                    workPeriodId: vm.workday.wid,
                    terminals: terminalNames
                    })
                    .success(function (result) {
                        vm.ticket = result;
                    }).finally(function () {
                        vm.loading = false;
                    });
            };

            vm.sum = function (items, prop) {
                if (items == null) {
                    return 0;
                }
                return items.reduce(function (a, b) {
                    return b[prop] == null ? a : a + b[prop];
                }, 0);
            };

            vm.exportToSummary = function (mode) {
                vm.loading = true;

                var terminalNames = [];
                angular.forEach(vm.refTerminalName, function (item) {
                    terminalNames.push(item.name);
                });

                centralReportAppService.getCentralTerminalExport({
                        centralTerminalReportInput: {
                            workPeriodId: vm.workday.wid,
                        terminals: terminalNames
                        },
                        exportOutputType: mode,
                        dateReport: moment(vm.workday.reportStartDay).format($scope.format)
                    })
                    .success(function (result) {
                        if (result != null) {
                            app.downloadTempFile(result);
                        }
                    }).finally(function () {
                        vm.loading = false;
                    });
            };
        }
    ]);
})();