﻿(function () {
    appModule.controller("tenant.views.connect.report.summary.log", [
        "$scope", "$rootScope", "$state", "$uibModal", "uiGridConstants", "abp.services.app.planLogger", "abp.services.app.location", "abp.services.app.connectLookup", "abp.services.app.tenantSettings",
        function ($scope, $rootScope, $state, $modal, uiGridConstants, planLoggerService, locationService, lookupService, tenantSettingsService) {
            var vm = this;
            $rootScope.settings.layout.pageSidebarClosed = true;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });
            vm.locations = [];
            vm.payments = [];
            vm.transactions = [];
            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];
            vm.loading = false;
            vm.advancedFiltersAreShown = false;

            vm.totalTicketAmount = "";
            vm.totalTickets = "";
            vm.totalOrders = "";
            vm.totalItems = "";
            vm.datetimeFormat = "YYYY-MM-DD HH:mm:ss";
            vm.$onInit = function () {
                vm.getDatetimeFormat();
            }
           
            vm.getDatetimeFormat = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.datetimeFormat = result.connect.dateTimeFormat;
                    });
            };

            vm.formatDatetime = function (value) {
                if (value) {
                    var format = moment(value).format(vm.datetimeFormat.toUpperCase());
                    return format;
                }
                return null;
            }

            vm.requestParams = {
                locations: "",
                events: "",
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null,
                unfinished: false
            };


            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };
            vm.userGridOptions = {
                showColumnFooter: true,
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
					{
					    name: app.localize('Actions'),
					    enableSorting: false,
					    width: 120,
					    cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.showDetails(row.entity)"><i class="fa fa-search"></i></button>' +
                                '</div>'
					},
                     {
                         name: app.localize('EventName'),
                         field: 'eventName'
                     },
                      {
                          name: app.localize('EventTime'),
                          field: 'eventTime',
                          cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.formatDatetime(row.entity.eventTime)}}</div>',
                          minWidth: 100
                      },
                       {
                           name: app.localize('User'),
                           field: 'userName'
                       },
                    {
                        name: app.localize('TicketNo'),
                        field: 'ticketNo'
                    },

                    {
                        name: app.localize('TicketTotal'),
                        field: 'ticketTotal',
                        aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                    },
                     {
                         name: app.localize('Location'),
                         field: 'locationName'
                     },


                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;
                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {

                vm.loading = true;
                var input = {
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationGroup: vm.locationGroup,
                    events: vm.requestParams.events,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    unfinished: vm.requestParams.unfinished,
                    dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                };
                planLoggerService.getAll(input)
                    .success(function (result) {
                        vm.userGridOptions.totalItems = result.totalCount;
                        vm.userGridOptions.data = result.items;
                    }).finally(function () {
                        vm.loading = false;
                    });
            };


            vm.exportToExcel = function (type) {                             
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;

                        var input = {
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locationGroup: vm.locationGroup,
                            events: vm.requestParams.events,
                            skipCount: vm.requestParams.skipCount,
                            maxResultCount: vm.requestParams.maxResultCount,
                            sorting: vm.requestParams.sorting,
                            unfinished: vm.requestParams.unfinished,
                            exportType: type,
                            runInBackground: myConfirmation,
                            exportOutputType: type,
                            dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                        };
                        planLoggerService.getExcel(input)
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });

                    });
            };

            vm.showDetails = function (log) {
                $modal.open({
                    templateUrl: "~/App/tenant/views/connect/report/summary/log/logModal.cshtml",
                    controller: "tenant.views.connect.report.logModal as vm",
                    backdrop: "static",
                    resolve: {
                        log: function () {
                            return log;
                        }
                    }
                });
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            }


            vm.getEvents = function () {
                vm.loading = true;
                lookupService.getLogTypes({}).success(function (result) {
                    vm.events = $.parseJSON(JSON.stringify(result.items));
                }).finally(function (result) {
                    vm.loading = false;
                });
            };
            vm.getEvents();

            vm.clear = function () {
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };
                $scope.builder.builder.reset();
                vm.getAll();
            }

            $scope.builder =
                app.createBuilder(
                    {
                        condition: "AND",
                        rules: []
                    },
                    angular.fromJson
                        (
                            [
                                {
                                    "id": "EventName",
                                    "label": app.localize("EventName"),
                                    "type": "string",
                                },
                                {
                                    "id": "userName",
                                    "label": app.localize("User"),
                                    "type": "string",
                                },
                                {
                                    "id": "TicketNo",
                                    "label": app.localize("TicketNo"),
                                    "type": "string",
                                },
                                {
                                    "id": "TicketTotal",
                                    "label": app.localize("TicketTotal"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                                {
                                    "id": "Location.Name",
                                    "label": app.localize("Location"),
                                    "type": "string",
                                },
                            ]
                        )
                );

        }
    ]);
})();