﻿(function () {
    appModule.controller("tenant.views.connect.report.weekend.summary", [
        "$scope", "$uibModal", "abp.services.app.connectReport", "abp.services.app.workPeriod", "abp.services.app.tenantSettings",
        function ($scope, $modal, connectService, wpService, tenantSettingsService) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });
            vm.locations = [];
            vm.currencyText = abp.features.getValue("DinePlan.DineConnect.Connect.Currency");
            vm.loading = false;
            vm.advancedFiltersAreShown = false;
            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];

            vm.weekEnds = [];
            vm.dayDetails = [];

            vm.chartTicket = [];
            vm.chartSales = [];
            vm.chartAverageSales = [];
            vm.requestParams = {
                locations: "",
                payments: "",
                department: "",
                transactions: "",
                terminalName: "",
                lastModifiedUserName: "",
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };
            vm.getEditionValue = function (item) {
                return item.displayText;
            };

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    }).finally(function () {
                    });
            };

            vm.$onInit = function () {
                vm.getDecimals();
            };

            vm.exportToSummary = function (mode) {
                var allLocations = vm.requestParams.locations;
                if (vm.requestParams.locations === "") {
                    allLocations = vm.locationGroup.locations;
                }
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        wpService.getWeekEndSalesOutput({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locations: allLocations,
                            exportOutputType: mode,
                            runInBackground: myConfirmation
                        })
                            .success(function (result) {
                                if (result !== null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });
                    });
            };

            vm.getResult = function () {
                vm.loading = true;
                var allLocations = vm.requestParams.locations;
                if (vm.requestParams.locations === "") {
                    allLocations = vm.locationGroup.locations;
                }
                wpService.getWeekEndSales({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locations: allLocations
                }).success(function (result) {
                    vm.weekEnds = result;
                    vm.chartSales = new Array(vm.weekEnds.length).fill(0);

                    for (var i = 0; i < vm.weekEnds.length; i++) {
                        vm.chartSales[i] = vm.weekEnds[i].total;
                    }

                    var dateNames = [];
                    vm.weekEnds.forEach(function (item) {
                        dateNames.push(item.date);
                    });

                    Highcharts.chart('daychart', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Weekend Sales'
                        },
                        xAxis: {
                            categories: dateNames,
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Total'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">Day: {point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.2f} </b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        series: [{
                            name: 'Sales',
                            data: vm.chartSales
                        }]
                    });
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.intiailizeOpenLocation=function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.sum = function (items, prop) {
                if (items === null) {
                    return 0;
                }
                return items.reduce(function (a, b) {
                    return b[prop] === null ? a : a + b[prop];
                }, 0);
            };

            vm.average = function (items) {
                var count = vm.sum(items, "totalTickets");
                var total = vm.sum(items, "total");

                if (count === 0) {
                    return 0;
                }
                return total / count;
            };
        }
    ]);
})();