﻿(function () {
    appModule.controller("tenant.views.connect.report.summary.workdayModal",
        [
            "$scope", "abp.services.app.centralReport", "$uibModalInstance", "workday",
            "abp.services.app.tenantSettings",
            function ($scope, centralReportAppService, $modalInstance, workday, tenantSettingsService) {
                var vm = this;
                vm.workday = workday;

                $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
                $scope.format = $scope.formats[0];

                vm.decimals = null;
                vm.getDecimals = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.decimals = result.connect.decimals;
                        }).finally(function () {
                        });
                };
                vm.getDecimals();

                vm.close = function () {
                    $modalInstance.dismiss();
                };

                vm.getWorkdayData = function () {
                    vm.loading = true;
                    centralReportAppService.getCentralDayReport(vm.workday.wid)
                        .success(function (result) {
                            vm.ticket = result;
                            console.log(result);
                        }).finally(function () {
                            vm.loading = false;
                        });
                };
                vm.datetimeFormat = "YYYY-MM-DD HH:mm:ss";

                vm.getDatetimeFormat = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.datetimeFormat = result.connect.dateTimeFormat;
                        });
                };

                vm.formatDatetime = function (value) {
                    if (value) {
                        var format = moment(value).format(vm.datetimeFormat.toUpperCase());
                        return format;
                    }
                    return null;
                }

                vm.getDatetimeFormat();


                vm.getWorkdayData();

                vm.sum = function (items, prop) {
                    if (items == null) {
                        return 0;
                    }
                    return items.reduce(function (a, b) {
                        return b[prop] == null ? +a : +a + +b[prop];
                    },
                        0);
                };

                vm.exportToSummary = function (mode) {
                    vm.disableExport = true;
                    abp.ui.setBusy("#MyLoginForm");
                    abp.message.confirm(app.localize("AreYouSure"), app.localize("RunInBackground"),
                        function (isConfirmed) {
                            var myConfirmation = false;
                            if (isConfirmed) {
                                myConfirmation = true;
                            }
                            vm.disableExport = true;

                            centralReportAppService.getCentralDayExport({
                                workPeriodId: vm.workday.wid,
                                exportOutputType: mode,
                                dateReport: moment(vm.workday.reportStartDay).format($scope.format),
                                runInBackground: myConfirmation
                            }).success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });
                        });
                };
            }
        ]);
})();