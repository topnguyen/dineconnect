﻿(function () {
    appModule.controller('tenant.views.connect.report.summary.saleprint', [
        '$scope', '$filter', '$state', '$stateParams',  'abp.services.app.connectReport',
        function ($scope, $filter, $state, $stateParams,  connectReport) {

            var vm = this;
            
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.loading = false;
            vm.filterText = null;
            vm.input = $stateParams.ticketInput;
            console.log(vm.input);

            vm.headerline = "------------------";
            function myPrint() {
                window.print();
            }
            function myFunction() {
                //document.getEle`mentById("divprintemail").style.visibility = "hidden";
                myPrint();
            }

            vm.getData = function () {
                connectReport.getSaleSummaryPrint(vm.input).success(function (result) {
                    vm.printresult = result;

                    $("#printidtextHeaderBold").html(result.boldHeaderText);
                    $("#printidtextBodyBold").html(result.boldBodyText);
                    $("#printidtextHeader").html(result.headerText);
                    $("#printidtextBody").html(result.bodyText);

                    myFunction();
                });
            }
            vm.getData();
            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };
        
        }]);
})();

