﻿(function() {
    appModule.controller("tenant.views.connect.report.workday.saleprint",
        [
            "$scope", "$filter", "$state", "$stateParams", "abp.services.app.workPeriod",
            function($scope, $filter, $state, $stateParams, workPeriod) {

                var vm = this;

                $scope.$on("$viewContentLoaded",
                    function() {
                        App.initAjax();
                    });
                vm.loading = false;
                vm.filterText = null;

                vm.headerline = "------------------";

                function myPrint() {
                    window.print();
                }

                function myFunction() {
                    //document.getElementById("divprintemail").style.visibility = "hidden";
                    myPrint();
                }

                vm.getData = function() {
                    workPeriod.getSummaryPrint(
                        $stateParams.periodInput
                    ).success(function(result) {
                        vm.printresult = result;

                        $("#printidtextHeaderBold").html(result.boldHeaderText);
                        $("#printidtextBodyBold").html(result.boldBodyText);
                        $("#printidtextHeader").html(result.headerText);
                        $("#printidtextBody").html(result.bodyText);


                        myFunction();
                    });
                };
                vm.getData();
                vm.isUndefinedOrNull = function(val) {
                    return angular.isUndefined(val) || val == null;
                };

            }
        ]);
})();