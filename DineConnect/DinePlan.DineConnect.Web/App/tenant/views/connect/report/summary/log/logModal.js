﻿(function() {
    appModule.controller("tenant.views.connect.report.logModal", [
        "$scope", "$uibModalInstance", "abp.services.app.planLogger", "log",
        function($scope, $modalInstance,planLoggerService, log) {
            var vm = this;

            vm.log = log;
            vm.events = [];
            vm.close = function() {
                $modalInstance.dismiss();
            };
            vm.getEvents = function () {
                vm.loading = true;
                planLoggerService.getTimelineLogs(
                    { id: vm.log.id }).success(function (result) {

                    console.log(result);
                    vm.events = result.eventOutputs;
                        vm.pairs = result.descriptions;
                    console.log(result);
                }).finally(function (result) {
                    vm.loading = false;
                });
            };
            vm.getEvents();
        }
    ]);
})();