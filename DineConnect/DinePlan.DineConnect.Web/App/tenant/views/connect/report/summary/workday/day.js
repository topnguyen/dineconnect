﻿(function() {
    appModule.controller("tenant.views.connect.report.workday.day", [
        "$scope", "$state",  "$uibModal", "$rootScope", "uiGridConstants", "abp.services.app.location",
        "abp.services.app.workPeriod", "abp.services.app.connectReport", "abp.services.app.tenantSettings",
        function ($scope, $state,$modal, $rootScope, uiGridConstants, locationService, wpService, connectReport, tenantSettingsService) {
            var vm = this;
            $scope.$on("$viewContentLoaded", function() {
                App.initAjax();
            });
            vm.requestParams = {
                locations: ""
            };
            vm.dashboard = {
                totalSales: 0,
                totalTickets: 0,
                average: 0
            };
            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];
            vm.mycustomer = abp.features.getValue("DinePlan.DineConnect.Connect.Customer");
           
            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            
            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function() {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function() {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function(result) {
                    vm.locationGroup = result;
                    vm.locations = [];

                    if (vm.locationGroup.locations.length > 0) {
                        vm.locations = vm.locationGroup.locations;
                    }
                    if (vm.locationGroup.group && vm.locationGroup.groups.length > 0) {
                        vm.locations = vm.locationGroup.locations;
                    }

                });
            };
            vm.getLocations = function() {
                vm.loading = true;
                locationService.getLocationBasedOnUser({
                    userId: abp.session.userId
                }).success(function(result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                }).finally(function(result) {
                    vm.loading = false;
                });
            };
            vm.getLocations();

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    }).finally(function () {
                    });
            };
            vm.$onInit = function() {
                vm.getDecimals();
            };

            vm.getDetails = function() {
                abp.ui.setBusy("#MyLoginForm");
                vm.dayDetails = "";
                vm.loading = true;
                wpService.getSummaryReport(vm.buildInput()).success(function(result) {
                    vm.dayDetails = result.periods;
                    vm.dashboard = result.dashBoard;
                    abp.ui.clearBusy("#MyLoginForm");
                }).finally(function(result) {
                    vm.loading = false;
                });
            };

            vm.print40Col = function() {
                $state.go("tenant.daysummaryprint",
                    {
                        periodInput: vm.buildInput()
                    });
            };

            vm.buildInput = function() {
                return {
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationGroup: vm.locationGroup
                };
            };

            vm.exportToSummary = function (mode) {
                var allLocations = vm.requestParams.locations;
                if (vm.requestParams.locations == "") {
                    allLocations = vm.locations;
                }
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        wpService.getSummaryExcel({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locations: allLocations,
                            exportOutputType: mode,
                            runInBackground: myConfirmation
                        })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });

                    });
            };
            vm.exportToDetails = function (mode) {
                var allLocations = vm.requestParams.locations;
                if (vm.requestParams.locations == "") {
                    allLocations = vm.locations;
                }
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        wpService.getDetailExcel({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locations: allLocations,
                            exportOutputType: mode,
                            runInBackground: myConfirmation
                        })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });

                    });
            };
            vm.exportToExcel = function (mode) {
                var allLocations = vm.requestParams.locations;
                if (vm.requestParams.locations == "") {
                    allLocations = vm.locations;
                }
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        wpService.getSummaryOutput({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locations: allLocations,
                            exportOutputType: mode,
                            runInBackground: myConfirmation
                        })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });

                    });
            };
            vm.exportWeekDay = function (mode) {
                var allLocations = vm.requestParams.locations;
                if (vm.requestParams.locations == "") {
                    allLocations = vm.locations;
                }
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        wpService.getWeekDaySalesOutput({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locations: allLocations,
                            exportOutputType: mode,
                            runInBackground: myConfirmation
                        })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });

                    });
            }; 
            vm.exportWeekEnd = function (mode) {
                var allLocations = vm.requestParams.locations;
                if (vm.requestParams.locations == "") {
                    allLocations = vm.locations;
                }
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        wpService.getWeekEndSalesOutput({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locations: allLocations,
                            exportOutputType: mode,
                            runInBackground: myConfirmation
                        })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });

                    });
            };
            vm.exportSchdule = function (mode) {
                var allLocations = vm.requestParams.locations;
                if (vm.requestParams.locations == "") {
                    allLocations = vm.locations;
                }
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        wpService.getScheduleSalesOutput({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locations: allLocations,
                            exportOutputType: mode,
                            runInBackground: myConfirmation
                        })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });

                    });
            };
            vm.exportItemSales = function(lid) {
                vm.loading = true;
                abp.ui.setBusy("#MyLoginForm");
                connectReport.getItemSalesForWorkPeriodExcel({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    location: lid
                }).success(function(result) {
                    app.downloadTempFile(result);
                    abp.ui.clearBusy("#MyLoginForm");
                }).finally(function(result) {
                    vm.loading = false;
                });
            };
        }
    ]);
})();