﻿(function () {
    appModule.controller("tenant.views.connect.report.summary.sale",
        [
            "$scope", "$rootScope", "$state", "$uibModal", "uiGridConstants", "abp.services.app.connectReport",
            "abp.services.app.location",
            "abp.services.app.customerReport",
            "abp.services.app.tenantSettings",
            "abp.services.app.consolidated",
            "abp.services.app.connectLookup",
            function ($scope,
                $rootScope,
                $state,
                $modal,
                uiGridConstants,
                connectService,
                locationService,
                crService,
                tenantSettingsService,
                consolidatedService,
                connectLookupService
            ) {
                var vm = this;

                $scope.$on("$viewContentLoaded",
                    function () {
                        App.initAjax();
                    });
                vm.payments = [];
                vm.transactions = [];
                $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
                $scope.format = $scope.formats[0];
                vm.loading = false;
                vm.advancedFiltersAreShown = false;

                vm.totalTicketAmount = "";
                vm.totalTickets = "";
                vm.totalOrders = "";
                vm.totalItems = "";
                vm.customer = abp.features.getValue("DinePlan.DineConnect.Connect.Customer");
                console.log(vm.customer);
                vm.requestParams = {
                    locations: "",
                    void: false,
                    gift: false,
                    refund: false,
                    terminalName: "",
                    lastModifiedUserName: "",
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null
                };

                var todayAsString = moment().format("YYYY-MM-DD");
                vm.dateRangeOptions = app.createDateRangePickerOptions();
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };

                vm.decimals = null;
                vm.getDecimals = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.decimals = result.connect.decimals;
                        }).finally(function () {
                        });
                };
                vm.datetimeFormat = "YYYY-MM-DD HH:mm:ss";

                vm.getDatetimeFormat = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.datetimeFormat = result.connect.dateTimeFormat;
                        });
                };

                vm.formatDatetime = function (value) {
                    if (value) {
                        var format = moment(value).format(vm.datetimeFormat.toUpperCase());
                        return format;
                    }
                    return null;
                }
                vm.dateFormat = "YYYY-MM-DD";

                vm.getDateFormat = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.dateFormat = result.connect.simpleDateFormat;
                        });
                };
                vm.formatDate = function (value) {
                    if (value) {
                        var format = moment(value).format(vm.dateFormat.toUpperCase());
                        return format;
                    }
                    return null;
                }
                vm.$onInit = function () {
                    vm.getDecimals();
                    vm.getPaymentTypes();
                    vm.getDatetimeFormat();
                    vm.getDateFormat();
                };

                vm.paymentTypes = [];
                vm.paymentType = null;
                vm.getPaymentTypes = function () {
                    connectLookupService.getPaymentTypes({
                    }).success(function (result) {
                        vm.paymentTypes = result.items;
                    }).finally(function (result) {
                    });
                };

                vm.getOrders = function () {
                    abp.ui.setBusy("#MyLoginForm");
                    connectService.getSaleSummary({
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        payments: vm.payments.map(t => t.value),
                        locationGroup: vm.locationGroup,
                        void: vm.requestParams.void,
                        gift: vm.requestParams.gift,
                        refund: vm.requestParams.refund,
                        terminalName: vm.requestParams.terminalName,
                        lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                        skipCount: vm.requestParams.skipCount,
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting,
                        reportDisplay: true
                    })
                        .success(function (result) {
                            vm.summaryOrders = result.sale.items;
                            Highcharts.chart("date_stats",
                                {
                                    title: {
                                        text: app.localize("DateWise"),
                                        x: -20 //center
                                    },
                                    xAxis: {
                                        categories: $.parseJSON(JSON.stringify(result.dashboard.dates))
                                    },
                                    yAxis: {
                                        plotLines: [
                                            {
                                                value: 0,
                                                width: 2,
                                                color: "#808080"
                                            }
                                        ]
                                    },

                                    legend: {
                                        layout: "vertical",
                                        align: "right",
                                        verticalAlign: "middle",
                                        borderWidth: 0
                                    },
                                    series: [
                                        {
                                            name: app.localize("Total"),
                                            data: $.parseJSON(JSON.stringify(result.dashboard.totals))
                                        }
                                    ]
                                });
                            abp.ui.clearBusy("#MyLoginForm");
                        })
                        .finally(function () {
                            vm.loading = false;
                        });
                };

                vm.print40Col = function () {
                    $state.go("tenant.salessummaryprint",
                        {
                            ticketInput: vm.getCurrentTicketInput()
                        });
                };
                vm.consolidated = function () {
                    abp.ui.setBusy("#MyLoginForm");
                    abp.message.confirm(app.localize("AreYouSure"),
                        app.localize("RunInBackground"),
                        function (isConfirmed) {
                            var myConfirmation = false;
                            if (isConfirmed) {
                                myConfirmation = true;
                            }
                            vm.disableExport = true;
                            consolidatedService.getConsolidatedExport({
                                startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                                endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                                payments: vm.payments.map(t => t.value),
                                locationGroup: vm.locationGroup,
                                void: vm.requestParams.void,
                                gift: vm.requestParams.gift,
                                refund: vm.requestParams.refund,
                                terminalName: vm.requestParams.terminalName,
                                lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                                skipCount: vm.requestParams.skipCount,
                                maxResultCount: vm.requestParams.maxResultCount,
                                sorting: vm.requestParams.sorting,
                                runInBackground: myConfirmation
                            })
                                .success(function (result) {
                                    if (result != null) {
                                        //console.log(result);
                                        app.downloadTempFile(result);
                                    }
                                }).finally(function () {
                                    vm.disableExport = false;
                                    abp.ui.clearBusy("#MyLoginForm");
                                });
                        });
                };
                vm.getCurrentTicketInput = function () {
                    return {
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        payments: vm.payments.map(t => t.value),
                        locationGroup: vm.locationGroup,
                        void: vm.requestParams.void,
                        gift: vm.requestParams.gift,
                        refund: vm.requestParams.refund,
                        terminalName: vm.requestParams.terminalName,
                        lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                        skipCount: vm.requestParams.skipCount,
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting
                    };
                };

                vm.exportToExcel = function () {
                    abp.ui.setBusy("#MyLoginForm");
                    abp.message.confirm(app.localize("AreYouSure"),
                        app.localize("RunInBackground"),
                        function (isConfirmed) {
                            var myConfirmation = false;
                            if (isConfirmed) {
                                myConfirmation = true;
                            }
                            vm.disableExport = true;
                            connectService.getSummaryExcel({
                                startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                                endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                                payments: vm.payments.map(t => t.value),
                                locationGroup: vm.locationGroup,
                                void: vm.requestParams.void,
                                gift: vm.requestParams.gift,
                                refund: vm.requestParams.refund,
                                terminalName: vm.requestParams.terminalName,
                                lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                                skipCount: vm.requestParams.skipCount,
                                maxResultCount: vm.requestParams.maxResultCount,
                                sorting: vm.requestParams.sorting,
                                runInBackground: myConfirmation
                            })
                                .success(function (result) {
                                    if (result != null) {
                                        app.downloadTempFile(result);
                                    }
                                }).finally(function () {
                                    vm.disableExport = false;
                                    abp.ui.clearBusy("#MyLoginForm");
                                });
                        });
                };
                vm.exportToExcelNew = function () {
                    abp.ui.setBusy("#MyLoginForm");
                    abp.message.confirm(app.localize("AreYouSure"),
                        app.localize("RunInBackground"),
                        function (isConfirmed) {
                            var myConfirmation = false;
                            if (isConfirmed) {
                                myConfirmation = true;
                            }
                            vm.disableExport = true;
                            connectService.getSummarySecondExcel({
                                startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                                endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                                payments: vm.payments.map(t => t.value),
                                locationGroup: vm.locationGroup,
                                void: vm.requestParams.void,
                                gift: vm.requestParams.gift,
                                refund: vm.requestParams.refund,
                                terminalName: vm.requestParams.terminalName,
                                lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                                skipCount: vm.requestParams.skipCount,
                                maxResultCount: vm.requestParams.maxResultCount,
                                sorting: vm.requestParams.sorting,
                                runInBackground: myConfirmation
                            })
                                .success(function (result) {
                                    if (result != null) {
                                        app.downloadTempFile(result);
                                    }
                                }).finally(function () {
                                    vm.disableExport = false;
                                    abp.ui.clearBusy("#MyLoginForm");
                                });
                        });
                };
                vm.showDetails = function (ticket) {
                    $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/summary/sale/saleModal.cshtml",
                        controller: "tenant.views.connect.report.saleModal as vm",
                        backdrop: "static",
                        resolve: {
                            ticket: function () {
                                return ticket;
                            }
                        }
                    });
                };

                vm.intiailizeOpenLocation = function () {
                    vm.locationGroup = app.createLocationInputForSearch();
                };
                vm.intiailizeOpenLocation();

                vm.exportToPdf = function () {
                    abp.ui.setBusy("#MyLoginForm");
                    connectService.getSummaryOutput({
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        payments: vm.payments.map(t => t.value),
                        locationGroup: vm.locationGroup,
                        void: vm.requestParams.void,
                        gift: vm.requestParams.gift,
                        refund: vm.requestParams.refund,
                        terminalName: vm.requestParams.terminalName,
                        lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                        skipCount: vm.requestParams.skipCount,
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting,
                        exportOutputType: 2
                    })
                        .success(function (result) {
                            app.downloadTempFile(result);
                            abp.ui.clearBusy("#MyLoginForm");
                        });
                };

                vm.getRRExport = function () {
                    abp.ui.setBusy("#MyLoginForm");
                    crService.getRRExport({
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        payments: vm.payments.map(t => t.value),
                        locationGroup: vm.locationGroup,
                        void: vm.requestParams.void,
                        gift: vm.requestParams.gift,
                        refund: vm.requestParams.refund,
                        terminalName: vm.requestParams.terminalName,
                        lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                        skipCount: vm.requestParams.skipCount,
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting
                    })
                        .success(function (result) {
                            app.downloadTempFile(result);
                            abp.ui.clearBusy("#MyLoginForm");
                        });
                };
              
                vm.getPBExport = function () {
                    abp.ui.setBusy("#MyLoginForm");
                    crService.getSalesSummaryForPB({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            payments: vm.payments.map(t => t.value),
                            locationGroup: vm.locationGroup,
                            void: vm.requestParams.void,
                            gift: vm.requestParams.gift,
                            refund: vm.requestParams.refund,
                            terminalName: vm.requestParams.terminalName,
                            lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                            skipCount: vm.requestParams.skipCount,
                            maxResultCount: vm.requestParams.maxResultCount,
                            sorting: vm.requestParams.sorting
                    })
                        .success(function (result) {
                            app.downloadTempFile(result);
                            abp.ui.clearBusy("#MyLoginForm");
                        });
                };

                vm.openLocation = function () {
                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };

                vm.sum = function (items, prop) {
                    if (items == null) {
                        return 0;
                    }
                    return items.reduce(function (a, b) {
                        return b[prop] == null ? a : a + b[prop];
                    },
                        0);
                };

                vm.exportLocationComparison = function () {
                    abp.ui.setBusy("#MyLoginForm");
                    abp.message.confirm(app.localize("AreYouSure"),
                        app.localize("RunInBackground"),
                        function (isConfirmed) {
                            var myConfirmation = false;
                            if (isConfirmed) {
                                myConfirmation = true;
                            }
                            vm.disableExport = true;
                            connectService.getLocationComparisonReportToExcel({
                                startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                                endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                                locationGroup: vm.requestParams.locationGroup,
                                void: vm.requestParams.void,
                                gift: vm.requestParams.gift,
                                noSales: vm.requestParams.noSales,
                                refund: vm.requestParams.refund,
                                terminalName: vm.requestParams.terminalName,
                                lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                                skipCount: vm.requestParams.skipCount,
                                maxResultCount: vm.requestParams.maxResultCount,
                                sorting: vm.requestParams.sorting,
                                runInBackground: myConfirmation
                            }).success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });
                        });
                };
            }
        ]);
})();