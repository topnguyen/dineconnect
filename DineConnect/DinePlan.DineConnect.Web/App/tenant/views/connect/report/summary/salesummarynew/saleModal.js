﻿(function () {
    appModule.controller('tenant.views.connect.report.saleModal', [
        '$scope', '$uibModalInstance', 'ticket',
    function ($scope, $modalInstance, ticket) {
            var vm = this;

            vm.ticket = ticket;
          
            vm.close = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();