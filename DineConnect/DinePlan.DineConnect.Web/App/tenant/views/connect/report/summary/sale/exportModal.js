﻿(function () {
    appModule.controller("tenant.views.connect.report.sale.summary.exportModal", [
        "$scope", "$uibModalInstance", "dto", "abp.services.app.connectReport",
        function ($scope, $modalInstance, dto, connectService) {
            var vm = this;

            abp.ui.clearBusy("#MyLoginForm");
            vm.disableExport = false;

            vm.dto = dto;
            vm.excludeTax = false;

            vm.close = function () {
                $modalInstance.dismiss();
            };

            vm.exportToExcel = function () {
                abp.ui.setBusy("#MyLoginForm");

                vm.disableExport = true;

                connectService.getLocationComparisonReportToExcel({
                    startDate: vm.dto.startDate,
                    endDate: vm.dto.endDate,
                    locationGroup: vm.dto.locationGroup,
                    void: vm.dto.void,
                    gift: vm.dto.gift,
                    noSales: vm.dto.noSales,
                    refund: vm.dto.refund,
                    terminalName: vm.dto.terminalName,
                    lastModifiedUserName: vm.dto.lastModifiedUserName,
                    skipCount: vm.dto.skipCount,
                    maxResultCount: vm.dto.maxResultCount,
                    sorting: vm.dto.sorting,
                    includeTax: vm.includeTax
                }).success(function (result) {
                    if (result !== null) {
                        app.downloadTempFile(result);
                    }
                }).finally(function () {
                    vm.disableExport = false;
                    abp.ui.clearBusy("#MyLoginForm");
                    $modalInstance.dismiss();
                });
            };
        }
    ]);
})();