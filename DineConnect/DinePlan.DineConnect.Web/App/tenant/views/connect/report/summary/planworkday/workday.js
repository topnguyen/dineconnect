﻿(function () {
    appModule.controller("tenant.views.connect.report.summary.workdayCentral",
        [
            "$scope", "$uibModal", "uiGridConstants", "abp.services.app.connectReport", "abp.services.app.tenantSettings",
            function ($scope, $modal, uiGridConstants, connectService, tenantSettingsService) {
                var vm = this;

                $scope.$on("$viewContentLoaded",
                    function () {
                        App.initAjax();
                    });
                vm.locations = [];
                vm.payments = [];
                vm.transactions = [];
                $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
                $scope.format = $scope.formats[0];
                vm.loading = false;
                vm.advancedFiltersAreShown = false;

                vm.requestParams = {
                    locations: "",
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null
                };

                var todayAsString = moment().format("YYYY-MM-DD");
                vm.dateRangeOptions = app.createDateRangePickerOptions();
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };

                vm.decimals = null;
                vm.getDecimals = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.decimals = result.connect.decimals;
                        }).finally(function () {
                        });
                };
                vm.datetimeFormat = "YYYY-MM-DD HH:mm:ss";

                vm.getDatetimeFormat = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.datetimeFormat = result.connect.dateTimeFormat;
                        });
                };

                vm.formatDatetime = function (value) {
                    if (value) {
                        var format = moment(value).format(vm.datetimeFormat.toUpperCase());
                        return format;
                    }
                    return null;
                }

                vm.$onInit = function () {
                    vm.getDecimals();
                    vm.getDatetimeFormat();
                };

                vm.gridOptions = {
                    showColumnFooter: true,
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    columnDefs: [
                        {
                            name: app.localize("LocationName"),
                            field: "locationName",
                            enableSorting: true
                        },
                        {
                            name: app.localize("LocationCode"),
                            field: "locationCode",
                            enableSorting: true
                        },
                        {
                            name: app.localize("WorkDay"),
                            field: "reportStartDay",
                            enableSorting: true,
                            cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.formatDatetime(row.entity.reportStartDay)}}</div>',

                        },
                        {
                            name: "Actions",
                            enableSorting: false,
                            width: '400',
                            headerCellTemplate: "<span></span>",
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-primary btn-xs" ng-click="grid.appScope.showDayDetails(row.entity)">Day Report</button>' +
                                '  <button class="btn btn-primary btn-xs" ng-click="grid.appScope.showShiftDetails(row.entity)">Shift Report</button>' +
                                '  <button class="btn btn-primary btn-xs" ng-click="grid.appScope.showTerminalDetails(row.entity)">Terminal Report</button>' +
                                "</div>"
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope,
                            function (grid, sortColumns) {
                                if (!sortColumns.length || !sortColumns[0].field) {
                                    vm.requestParams.sorting = null;
                                } else {
                                    vm.requestParams.sorting =
                                        sortColumns[0].field + " " + sortColumns[0].sort.direction;
                                }
                                vm.getWorkdays();
                            });
                        gridApi.pagination.on.paginationChanged($scope,
                            function (pageNumber, pageSize) {
                                vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                                vm.requestParams.maxResultCount = pageSize;
                                vm.getWorkdays();
                            });
                    },
                    data: []
                };

                vm.toggleFooter = function () {
                    $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                };

                vm.toggleColumnFooter = function () {
                    $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                };

                vm.getWorkdays = function () {
                    vm.loading = true;
                    connectService.getWorkPeriodByLocationForDatesWithPaging({
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        locations: vm.locationGroup.locations,
                        skipCount: vm.requestParams.skipCount,
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting
                    })
                        .success(function (result) {
                            vm.gridOptions.totalItems = result.totalCount;

                            for (let i = 0; i < result.items.length; i++) {
                                if (result.items[i].total != null)
                                    result.items[i].total = result.items[i].total.toFixed(vm.decimals);
                            }

                            vm.gridOptions.data = result.items;

                        }).finally(function () {
                            vm.loading = false;
                        });
                };
                vm.disableExport = false;

                vm.exportToExcel = function (mode) {
                    abp.ui.setBusy("#MyLoginForm");
                    abp.message.confirm(app.localize("AreYouSure"),
                        app.localize("RunInBackground"),
                        function (isConfirmed) {
                            var myConfirmation = false;
                            if (isConfirmed) {
                                myConfirmation = true;
                            }
                            vm.disableExport = true;
                            connectService.getWorkPeriodByLocationForDatesWithPagingToExcel({
                                startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                                endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                                locationGroup: vm.locationGroup,
                                skipCount: vm.requestParams.skipCount,
                                maxResultCount: vm.requestParams.maxResultCount,
                                sorting: vm.requestParams.sorting,
                                runInBackground: myConfirmation,
                                exportOutputType: mode
                            })
                                .success(function (result) {
                                    if (result != null && !myConfirmation)
                                        app.downloadTempFile(result);
                                }).finally(function () {
                                    abp.ui.clearBusy("#MyLoginForm");
                                    vm.disableExport = false;
                                });
                        });


                };
                vm.showDayDetails = function (workday) {
                    $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/summary/planworkday/workdayModal.cshtml",
                        controller: "tenant.views.connect.report.summary.workdayModal as vm",
                        backdrop: "static",
                        resolve: {
                            workday: function () {
                                return workday;
                            }
                        }
                    });
                };

                vm.showShiftDetails = function (workday) {
                    $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/summary/planworkday/workshiftModal.cshtml",
                        controller: "tenant.views.connect.report.summary.workShiftModal as vm",
                        backdrop: "static",
                        resolve: {
                            workday: function () {
                                return workday;
                            }
                        }
                    });
                };

                vm.showTerminalDetails = function (workday) {
                    $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/summary/planworkday/terminalModal.cshtml",
                        controller: "tenant.views.connect.report.summary.terminalModal as vm",
                        backdrop: "static",
                        resolve: {
                            workday: function () {
                                return workday;
                            }
                        }
                    });
                };

                vm.intiailizeOpenLocation = function() {
                    vm.locationGroup = app.createLocationInputForSearch();
                };
                vm.intiailizeOpenLocation();

                vm.openLocation = function () {
                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };

            }
        ]);
})();