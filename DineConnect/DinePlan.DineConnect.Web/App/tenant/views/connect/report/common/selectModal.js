﻿(function () {
    appModule.controller('tenant.views.connect.report.selectModal', [
        '$scope', '$uibModalInstance', 'uiGridConstants','dataOptions',
        function ($scope, $uibModalInstance, uiGridConstants, dataOptions) {
            var vm = this;
            vm.loading = false;
            vm.saving = false;
            vm.autoSelection = false;

            vm.selectedData = dataOptions.selectedData;
            vm.title = dataOptions.title;

            vm.getData = function () {
                vm.loading = true; 
                var prms = angular.extend({
                    filter: vm.filterText
                }, dataOptions.requestParams);
                dataOptions.serviceMethod(
                    prms).success(function (result) {
                    vm.selectOptions.totalItems = result.totalCount;
                    vm.selectOptions.data = result.items;
                    vm.setDataSelection();
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.selectOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                enableRowSelection: true,
                rowHeight: 30,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Id"),
                        field: "id",
                        maxWidth: 70
                    },
                    {
                        name: app.localize("Name"),
                        field: "name"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    vm.departmentGridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        vm.getData();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        vm.getData();
                    });
                    gridApi.selection.on.rowSelectionChanged($scope,
                        function (row) {
                            if (row.isSelected) {
                                vm.pushData(row.entity);
                            } else {
                                for (let i = 0; i < vm.selectedData.length; i++) {
                                    const obj = vm.selectedData[i];

                                    if (obj.id == row.entity.id) {
                                        vm.selectedData.splice(i, 1);
                                    }
                                }

                            }
                        });
                },
                data: []
            };

            vm.pushData = function (myObj) {
                if (myObj != null) {
                    var myIndex = -1;
                    if (vm.selectedData != null) {
                        vm.selectedData.some(function (value, key) {
                            if (value.id == myObj.id) {
                                myIndex = value;
                                return true;
                            }
                        });
                    }
                    if (myIndex === -1) {
                        vm.selectedData.push(myObj);
                    } else {
                        if (vm.autoSelection == false)
                            abp.notify.error(myObj.id + "  " + app.localize("Available"));
                    }
                }
            };

            vm.setDataSelection = function () {
                if (vm.selectedData.length == 0) {
                    vm.departmentGridApi.selection.clearSelectedRows();
                    return;
                }
                vm.departmentGridApi.grid.modifyRows(vm.selectOptions.data);
                vm.departmentGridApi.selection.clearSelectedRows();
                vm.autoSelection = true;
                angular.forEach(vm.selectOptions.data,
                    function (value, key) {
                        vm.selectedData.some(function (selectedData, selectedKey) {
                            if (value.id == selectedData.id) {
                                vm.departmentGridApi.selection.selectRow(vm.selectOptions.data[key]);
                                return true;
                            }
                        });
                    });
                vm.autoSelection = false;
            };

            vm.getData();

            vm.save = function () {
                vm.selectedData = [];
                vm.selectedRows = $scope.gridApi.selection.getSelectedRows();
                angular.forEach(vm.selectedRows, function (row) {
                    vm.selectedData.push(row);
                });                                         

                $uibModalInstance.close(vm.selectedData);
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }
    ]);
})();