﻿(function () {
    appModule.controller('tenant.views.connect.report.exchangereport', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.ticket',
        'abp.services.app.connectReport', 'abp.services.app.location', "abp.services.app.tenantSettings", "abp.services.app.commonLookup", "abp.services.app.connectLookup",
        function ($scope, $modal, uiGridConstants, ticketService, connectService, locationService, tenantSettingsService, commonService, connectLookupService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.payments = [];
            vm.transactions = [];
            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.loading = false;
            vm.advancedFiltersAreShown = false;

            vm.totalAmount = '';
            vm.totalOrders = '';
            vm.totalItems = '';

            vm.requestParams = {
                locations: '',
                terminalName: '',
                lastModifiedUserName: '',
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.datetimeFormat = "YYYY-MM-DD HH:mm:ss";
            vm.dateFormat = "YYYY-MM-DD";

            vm.getDatetimeFormat = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.datetimeFormat = result.connect.dateTimeFormat;
                        vm.dateFormat = result.connect.simpleDateFormat;
                    });
            };

            vm.formatDatetime = function (value) {
                if (value) {
                    var format = moment(value).format(vm.datetimeFormat.toUpperCase());
                    return format;
                }
                return null;
            }
            vm.formatDate = function (value) {
                if (value) {
                    var format = moment(value).format(vm.dateFormat.toUpperCase());
                    return format;
                }
                return null;
            }

            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    }).finally(function () {
                    });
            };

            // #region LocalStorage

            vm.getPaymentTypes = function () {
                connectLookupService.getPaymentTypes({
                }).success(function (result) {
                    localStorage.paymentType = JSON.stringify(result.items);
                }).finally(function (result) {
                });
            };

            vm.getReason = function () {
                connectService.getReason({
                }).success(function (result) {
                    localStorage.reason = JSON.stringify(result.items);
                }).finally(function (result) {
                });
            };

                // #endregion

            vm.$onInit = function () {
                localStorage.clear();
                vm.getDecimals();
                vm.getPaymentTypes();
                vm.getReason();
                vm.getDatetimeFormat();
            }

            vm.gridOptions = {
                showColumnFooter: true,
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    //{
                    //    name: 'Actions',
                    //    enableSorting: false,
                    //    width: 50,
                    //    headerCellTemplate: '<span></span>',
                    //    cellTemplate:
                    //        '<div class=\"ui-grid-cell-contents text-center\">' +
                    //            '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.showDetails(row.entity)"><i class="fa fa-search"></i></button>' +
                    //            '</div>'
                    //},
                    {
                        name: app.localize('PlantCode'),
                        field: 'plantCode'
                    },
                    {
                        name: app.localize('PlantName'),
                        field: 'plantName'
                    },
                    {
                        name: app.localize('DateTime'),
                        field: 'dateTime',
                        cellFilter: 'date:"dd/MM/yyyy hh:mm"'
                    },
                    {
                        name: app.localize('SellerName'),
                        field: 'sellerName'
                    },
                    {
                        name: app.localize('ReceiptNumber'),
                        field: 'receiptNumber',
                        //cellClass: 'ui-ralign',
                        //aggregationType: uiGridConstants.aggregationTypes.sum,
                        //footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue()}}</div>'
                    },                    
                    {
                        name: app.localize('CustomerName'),
                        field: 'customerName',
                    },
                    {
                        name: app.localize('Tender'),
                        field: 'tender'
                    },
                    {
                        name: app.localize('Material_Group'),
                        field: 'materialGroup'
                    },
                    {
                        name: app.localize('MaterialCode'),
                        field: 'materialCode'
                    },
                    {
                        name: app.localize('MaterialName'),
                        field: 'materialName'
                    },
                    {
                        name: app.localize('Quantity'),
                        field: 'quantity',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate:
                            '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue()}}</div>'
                    },
                    {
                        name: app.localize('Amount'),
                        field: 'amount',
                        cellClass: 'ui-ralign',
                        cellFilter: 'number:grid.appScope.decimals',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate:
                            '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'
                    },
                    {
                        name: app.localize('ExchangeDate'),
                        field: 'exchangeDate',
                        cellFilter: 'date:"dd/MM/yyyy hh:mm"'
                    },
                    {
                        name: app.localize('ExchangeBy'),
                        field: 'exchangeBy'
                    },
                    {
                        name: app.localize('Reason'),
                        field: 'reason'
                    },
                    {
                        name: app.localize('Detail'),
                        field: 'detail'
                    },
                 
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getOrders();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;
                        vm.getOrders();
                    });
                },
                data: []
            };

            vm.toggleFooter = function () {
                $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.toggleColumnFooter = function () {
                $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            // #region Builder
            vm.locationsCode = [];
            vm.locations = [];
            vm.users = [];
            vm.productGroups = [];
            vm.paymentTypes = [];
            vm.reasons = [];
            vm.getDataForCombobox = function () {
                commonService.getLocationCodeForCombobox({
                }).success(function (result) {
                    vm.locationsCode = result.items;
                }).finally(function (result) {
                });

                commonService.getLocationForCombobox({
                }).success(function (result) {
                    vm.locations = result.items;
                }).finally(function (result) {
                });

                commonService.getUserForCombobox({
                }).success(function (result) {
                    vm.locations = result.items;
                }).finally(function (result) {
                });

                commonService.getProductGroupCombobox({
                }).success(function (result) {
                    vm.productGroups = result.items;
                }).finally(function (result) {
                });

                commonService.getPaymentTypeCombobox({
                }).success(function (result) {
                    vm.paymentTypes = result.items;
                }).finally(function (result) {
                });

                commonService.getReasonExchangeCombobox({
                }).success(function (result) {
                    vm.reasons = result.items;
                }).finally(function (result) {
                });


            };
            vm.getDataForCombobox();


            // Fix for Bootstrap Datepicker
            $('#builder-widgets').on('afterUpdateRuleValue.queryBuilder', function (e, rule) {
                if (rule.filter.plugin === 'datepicker') {
                    rule.$el.find('.rule-value-container input').datepicker('update');
                }
            });



            $scope.builder =
                app.createBuilder(
                    {
                        condition: "AND",
                        rules: []
                    },
                    angular.fromJson
                        (
                            [
                                {
                                    "id": "PlantCode",
                                    "label": app.localize("PlantCode"),
                                    "type": "string",
                                    "operators": [
                                        "equal",
                                        "not_equal",
                                    ],
                                    plugin: 'selectize',
                                    plugin_config: {
                                        valueField: 'value',
                                        labelField: 'displayText',
                                        searchField: 'displayText',
                                        sortField: 'displayText',
                                        create: true,
                                        maxItems: 1,
                                        plugins: ['remove_button'],
                                        onInitialize: function () {
                                            var that = this;
                                            if (vm.locationsCode.length > 0) {
                                                vm.locationsCode.forEach(function (item) {
                                                    item.value = item.displayText;
                                                    that.addOption(item);
                                                });
                                            } else {
                                                commonService.getLocationCodeForCombobox({
                                                }).success(function (result) {
                                                    vm.locationsCode = result.items;
                                                    vm.locationsCode.forEach(function (item) {
                                                        item.value = item.displayText;
                                                        that.addOption(item);
                                                    });
                                                }).finally(function (result) {
                                                });
                                            }
                                        }
                                    },
                                    valueSetter: function (rule, value) {
                                        rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                    }
                                },
                                {
                                    "id": "PlantName",
                                    "label": app.localize("PlantName"),
                                    "type": "string",
                                    "operators": [
                                        "equal",
                                        "not_equal",
                                    ],
                                    plugin: 'selectize',
                                    plugin_config: {
                                        valueField: 'value',
                                        labelField: 'displayText',
                                        searchField: 'displayText',
                                        sortField: 'displayText',
                                        create: true,
                                        maxItems: 1,
                                        plugins: ['remove_button'],
                                        onInitialize: function () {
                                            var that = this;
                                            if (vm.locations.length > 0) {
                                                vm.locations.forEach(function (item) {
                                                    item.value = item.displayText;
                                                    that.addOption(item);
                                                });
                                            } else {
                                                commonService.getLocationForCombobox({
                                                }).success(function (result) {
                                                    vm.locationsCode = result.items;
                                                    vm.locations.forEach(function (item) {
                                                        item.value = item.displayText;
                                                        that.addOption(item);
                                                    });
                                                }).finally(function (result) {
                                                });
                                            }
                                        }
                                    },
                                    valueSetter: function (rule, value) {
                                        rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                    }
                                },
                                {
                                    "id": "FormatPaymentCreate",
                                    "label": app.localize("DateTime"),
                                    "type": "date",
                                    'validation': {
                                        format: 'YYYY/MM/DD'
                                    },
                                    'plugin': 'datetimepicker',
                                    "operators": [
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal",
                                        "equal",
                                    ],
                                    plugin_config: {
                                        format: 'YYYY/MM/DD'
                                    }
                                },
                                {
                                    "id": "SellerName",
                                    "label": app.localize("SellerName"),
                                    "type": "string",
                                    "operators": [
                                        "equal",
                                        "not_equal",
                                    ],
                                    plugin: 'selectize',
                                    plugin_config: {
                                        valueField: 'value',
                                        labelField: 'displayText',
                                        searchField: 'displayText',
                                        sortField: 'displayText',
                                        create: true,
                                        maxItems: 1,
                                        plugins: ['remove_button'],
                                        onInitialize: function () {
                                            var that = this;
                                            if (vm.users.length > 0) {
                                                vm.users.forEach(function (item) {
                                                    item.value = item.displayText;
                                                    that.addOption(item);
                                                });
                                            } else {
                                                commonService.getUserForCombobox({
                                                }).success(function (result) {
                                                    vm.users = result.items;
                                                    vm.users.forEach(function (item) {
                                                        item.value = item.displayText;
                                                        that.addOption(item);
                                                    });
                                                }).finally(function (result) {
                                                });
                                            }
                                        }
                                    },
                                    valueSetter: function (rule, value) {
                                        rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                    }
                                },
                                {
                                    "id": "ReceiptNumber",
                                    "label": app.localize("ReceiptNumber"),
                                    "type": "string"
                                },
                                {
                                    "id": "CustomerName",
                                    "label": app.localize("CustomerName"),
                                    "type": "string"
                                },
                                {
                                    "id": "Tender",
                                    "label": app.localize("Tender"),
                                    "type": "string",
                                    "operators": [
                                        "equal",
                                        "not_equal",
                                    ],
                                    plugin: 'selectize',
                                    plugin_config: {
                                        valueField: 'value',
                                        labelField: 'displayText',
                                        searchField: 'displayText',
                                        sortField: 'displayText',
                                        create: true,
                                        maxItems: 1,
                                        plugins: ['remove_button'],
                                        onInitialize: function () {
                                            var that = this;
                                            if (vm.paymentTypes.length > 0) {
                                                vm.paymentTypes.forEach(function (item) {
                                                    item.value = item.displayText;
                                                    that.addOption(item);
                                                });
                                            } else {
                                                commonService.getPaymentTypeCombobox({
                                                }).success(function (result) {
                                                    vm.paymentTypes = result.items;
                                                    vm.paymentTypes.forEach(function (item) {
                                                        item.value = item.displayText;
                                                        that.addOption(item);
                                                    });
                                                }).finally(function (result) {
                                                });
                                            }
                                        }
                                    },
                                    valueSetter: function (rule, value) {
                                        rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                    }
                                },
                                {
                                    "id": "MaterialGroup",
                                    "label": app.localize("Material_Group"),
                                    "type": "string",
                                    "operators": [
                                        "equal",
                                        "not_equal",
                                    ],
                                    plugin: 'selectize',
                                    plugin_config: {
                                        valueField: 'value',
                                        labelField: 'displayText',
                                        searchField: 'displayText',
                                        sortField: 'displayText',
                                        create: true,
                                        maxItems: 1,
                                        plugins: ['remove_button'],
                                        onInitialize: function () {
                                            var that = this;
                                            if (vm.productGroups.length > 0) {
                                                vm.productGroups.forEach(function (item) {
                                                    item.value = item.displayText;
                                                    that.addOption(item);
                                                });
                                            } else {
                                                commonService.getProductGroupCombobox({
                                                }).success(function (result) {
                                                    vm.productGroups = result.items;
                                                    vm.productGroups.forEach(function (item) {
                                                        item.value = item.displayText;
                                                        that.addOption(item);
                                                    });
                                                }).finally(function (result) {
                                                });
                                            }
                                        }
                                    },
                                    valueSetter: function (rule, value) {
                                        rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                    }
                                },
                                {
                                    "id": "MaterialCode",
                                    "label": app.localize("MaterialCode"),
                                    "type": "string"
                                },
                                {
                                    "id": "MaterialName",
                                    "label": app.localize("MaterialName"),
                                    "type": "string"
                                },
                                {
                                    "id": "Quantity",
                                    "label": app.localize("Quantity"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                                {
                                    "id": "Amount",
                                    "label": app.localize("Amount"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                                {
                                    "id": "FormatExchangeDate",
                                    "label": app.localize("ExchangeDate"),
                                    "type": "date",
                                    'validation': {
                                        format: 'YYYY/MM/DD'
                                    },
                                    'plugin': 'datetimepicker',
                                    "operators": [
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal",
                                        "equal",
                                    ],
                                    plugin_config: {
                                        format: 'YYYY/MM/DD'
                                    }
                                },
                                {
                                    "id": "ExchangeBy",
                                    "label": app.localize("ExchangeBy"),
                                    "type": "string",
                                    "operators": [
                                        "equal",
                                        "not_equal",
                                    ],
                                    plugin: 'selectize',
                                    plugin_config: {
                                        valueField: 'value',
                                        labelField: 'displayText',
                                        searchField: 'displayText',
                                        sortField: 'displayText',
                                        create: true,
                                        maxItems: 1,
                                        plugins: ['remove_button'],
                                        onInitialize: function () {
                                            var that = this;
                                            if (vm.users.length > 0) {
                                                vm.users.forEach(function (item) {
                                                    item.value = item.displayText;
                                                    that.addOption(item);
                                                });
                                            } else {
                                                commonService.getUserForCombobox({
                                                }).success(function (result) {
                                                    vm.users = result.items;
                                                    vm.users.forEach(function (item) {
                                                        item.value = item.displayText;
                                                        that.addOption(item);
                                                    });
                                                }).finally(function (result) {
                                                });
                                            }
                                        }
                                    },
                                    valueSetter: function (rule, value) {
                                        rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                    }
                                },
                                {
                                    "id": "Reason",
                                    "label": app.localize("Reason"),
                                    "type": "string",
                                    "operators": [
                                        "equal",
                                        "not_equal",
                                    ],
                                    plugin: 'selectize',
                                    plugin_config: {
                                        valueField: 'value',
                                        labelField: 'displayText',
                                        searchField: 'displayText',
                                        sortField: 'displayText',
                                        create: true,
                                        maxItems: 1,
                                        plugins: ['remove_button'],
                                        onInitialize: function () {
                                            var that = this;
                                            if (vm.reasons.length > 0) {
                                                vm.reasons.forEach(function (item) {
                                                    item.value = item.displayText;
                                                    that.addOption(item);
                                                });
                                            } else {
                                                commonService.getReasonExchangeCombobox({
                                                }).success(function (result) {
                                                    vm.reasons = result.items;
                                                    vm.reasons.forEach(function (item) {
                                                        item.value = item.displayText;
                                                        that.addOption(item);
                                                    });
                                                }).finally(function (result) {
                                                });
                                            }
                                        }
                                    },
                                    valueSetter: function (rule, value) {
                                        rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                    }
                                },
                                {
                                    "id": "Detail",
                                    "label": app.localize("Detail"),
                                    "type": "string"
                                },
                            ]
                        )
                );


                // #endregion
            
            vm.getOrders = function () {
                vm.loading = true;
                connectService.getOrdersExchange({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationGroup: vm.locationGroup,
                    refund: vm.requestParams.refund,
                    terminalName: vm.requestParams.terminalName,
                    lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                })
                    .success(function (result) {
                        console.log("data", result);
                        //for (let i = 0; i < result.orderViewList.items.length; i++) {
                        //    if (result.orderViewList.items[i].price != null)
                        //        result.orderViewList.items[i].price = result.orderViewList.items[i].price.toFixed(vm.decimals);
                        //}
                        //vm.gridOptions.data = result.orderViewList.items;
                        vm.data = result;
                        vm.totalAmount = result.dashBoardDto.totalAmount;
                        vm.totalOrders = result.dashBoardDto.totalOrderCount;
                        vm.totalItems = result.dashBoardDto.totalItemSold;

                    }).finally(function () {
                        vm.loading = false;
                    });
            };

            vm.getOrderTagReport = function (obj) {
                abp.ui.setBusy('#MyLoginForm');
                connectService.getOrderReportWithTagExcel({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationGroup: vm.locationGroup,
                    refund: vm.requestParams.refund,
                    terminalName: vm.requestParams.terminalName,
                    lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    exportOutputType: obj
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        abp.ui.clearBusy('#MyLoginForm');
                    });
            };


            vm.exportToExcel = function (obj) {
                if (vm.gridOptions.totalItems === 0) return;
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        connectService.getExchangeReportExcel({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locationGroup: vm.locationGroup,
                            refund: vm.requestParams.refund,
                            terminalName: vm.requestParams.terminalName,
                            lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                            skipCount: vm.requestParams.skipCount,
                            maxResultCount: vm.requestParams.maxResultCount,
                            sorting: vm.requestParams.sorting,
                            exportOutputType: obj,
                            runInBackground: myConfirmation,
                            dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                        })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });

                    });

            };

            //vm.showDetails = function (order) {
            //    $modal.open({
            //        templateUrl: '~/App/tenant/views/connect/report/exchangereport/exchangereportModal.cshtml',
            //        controller: 'tenant.views.connect.report.exchangereportModal as vm',
            //        backdrop: 'static',
            //        resolve: {
            //            ticket: function () {
            //                return order.ticketId;
            //            }
            //        }
            //    });
            //};
            vm.showDetails = function (ticket) {
                $modal.open({
                    templateUrl: "~/App/tenant/views/connect/report/tickets/ticket/ticketModal.cshtml",
                    controller: "tenant.views.connect.report.ticketModal as vm",
                    backdrop: "static",
                    resolve: {
                        ticket: function () {
                            return ticket.ticketId;
                        }
                    }
                });
            };
            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.clear = function () {
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };
                $scope.builder.builder.reset();
                vm.getOrders();
            }

            
        }
    ]);
})();