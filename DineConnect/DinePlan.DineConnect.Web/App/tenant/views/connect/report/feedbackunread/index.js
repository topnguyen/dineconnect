﻿(function () {
    appModule.controller("tenant.views.connect.report.feedbackunread",
        [
            "$scope", "$uibModal", "uiGridConstants", "abp.services.app.feedbackUnread", 
            function ($scope,
                $modal,
                uiGridConstants,
                feedbackUnreadAppService
            ) {
                var vm = this;
                $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
                $scope.format = $scope.formats[0];

                vm.loading = false;
                vm.filterText = "";
                vm.unread = false;
                vm.acknowledgement = false;
                vm.replies = false;

                $scope.unreadChange = function () {
                    if (vm.unread == true) {
                        vm.acknowledgement = false;
                        vm.replies = false;
                    }
                };

                $scope.acknowledgementChange = function () {
                    if (vm.acknowledgement == true) {
                        vm.unread = false;
                    }
                };

                $scope.repliesChange = function () {
                    if (vm.replies == true) {
                        vm.unread = false;
                    }
                };
                var todayAsString = moment().format("YYYY-MM-DD");
                vm.dateRangeOptions = app.createDateRangePickerOptions();
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };

                vm.requestParams = {
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null,
                    userId: abp.session.userId,
                };

                vm.getInput = function () {
                    return {
                        filter: vm.filterText,
                        unread: vm.unread,
                        acknowledgement: vm.acknowledgement,
                        replies: vm.replies,
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        locationGroup: vm.locationGroup,
                        skipCount: vm.requestParams.skipCount,
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting,
                        userId: vm.requestParams.userId,
                        dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
                    };
                };

                vm.getExportInput = function (confirmation, obj) {
                    var input = vm.getInput();
                    input.runInBackground = confirmation;
                    input.exportOutputType = obj;
                    return input;
                };

                $scope.$on("$viewContentLoaded",
                    function () {
                        App.initAjax();
                    });

                vm.$onInit = function () {
                    localStorage.clear();
                };

                // #region Grid
                vm.gridOptions = {
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                    columnDefs: [
                        {
                            name: "Actions",
                            enableSorting: false,
                            width: 50,
                            headerCellTemplate: "<span></span>",
                            cellTemplate:
                                "<div class=\"ui-grid-cell-contents text-center\">" +
                                "  <button class=\"btn btn-default btn-xs\" ng-click=\"grid.appScope.showDetails(row.entity)\"><i class=\"fa fa-search\"></i></button>" +
                                "</div>"
                        },
                        {
                            name: app.localize("MessageHeader"),
                            field: "messageHeader"
                        },
                        {
                            name: app.localize("Type"),
                            field: "type"
                        },
                        {
                            enableSorting: false,
                            name: app.localize("Locations"),
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button ng-click="grid.appScope.openLocationModel(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Location') + '"><i class="fa fa-dashcube"></i></button>' +
                                '</div>'
                        },
                        {
                            enableSorting: false,
                            name: app.localize("Unread"),
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button ng-click="grid.appScope.openUnreadModel(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Unread') + '"><i class="fa fa-dashcube"></i></button>' +
                                '</div>'
                        },


                        {
                            name: app.localize('Acknowledgement'),
                            enableSorting: false,
                            cellTemplate:
                                '<div ng-if="row.entity.acknowledgement" class=\"ui-grid-cell-contents text-center\">' +
                                '  <button ng-click="grid.appScope.openAcknowledgedModel(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Acknowledgement') + '"><i class="fa fa-dashcube"></i></button>' +
                                    '</div>'
                        },
                        {
                            name: app.localize('Replies'),
                            enableSorting: false,
                            cellTemplate:
                                '<div ng-if="row.entity.reply" class=\"ui-grid-cell-contents text-center\">' +
                                    '  <button ng-click="grid.appScope.openRepliesModel(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Replies') + '"><i class="fa fa-dashcube"></i></button>' +
                                    '</div>'
                        },
                        {
                            name: app.localize("Content"),
                            cellTemplate:
                                '<div class="ui-grid-cell-contents">' +
                                '   <div ng-bind-html="row.entity.content" ng-show="row.entity.isTextContent"></div>' +
                                '   <div ng-show="!row.entity.isTextContent">{{ row.entity.type }}</div>' +
                                '</div>'
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope,
                            function (grid, sortColumns) {
                                if (!sortColumns.length || !sortColumns[0].field) {
                                    vm.requestParams.sorting = null;
                                } else {
                                    vm.requestParams.sorting =
                                        sortColumns[0].field + " " + sortColumns[0].sort.direction;
                                }
                                vm.getReport();
                            });
                        gridApi.pagination.on.paginationChanged($scope,
                            function (pageNumber, pageSize) {
                                vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                                vm.requestParams.maxResultCount = pageSize;
                                vm.getReport();
                            });
                    },
                    data: []
                };

                vm.disableExport = false;

                vm.toggleFooter = function () {
                    $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                };

                vm.toggleColumnFooter = function () {
                    $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                };

                // #endregion
                // #region Location

                vm.intiailizeOpenLocation = function () {
                    vm.locationGroup = app.createLocationInputForSearch();
                };
                vm.intiailizeOpenLocation();

                vm.openLocation = function () {
                    const modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };

                vm.showDetails = function (feedback) {
                    $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/feedbackunread/detail.cshtml",
                        controller: "tenant.views.connect.report.feedbackunread.detail as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            feedback: function () {
                                return feedback.id;
                            }
                        }
                    });
                };

                vm.exportToFile = function (exportType) {
                    abp.ui.setBusy("#MyLoginForm");
                    abp.message.confirm(app.localize("AreYouSure"),
                        app.localize("RunInBackground"),
                        function (isConfirmed) {
                            var myConfirmation = false;
                            if (isConfirmed) {
                                myConfirmation = true;
                                abp.notify.info(app.localize("RunInBackground"));
                            }
                            vm.disableExport = true;
                            var input = vm.getExportInput(myConfirmation, exportType);
                            feedbackUnreadAppService.getReportToFile(input)
                                .success(function (result) {
                                    if (result != null) {
                                        app.downloadTempFile(result);
                                    }
                                })
                                .finally(function () {
                                    abp.ui.clearBusy("#MyLoginForm");
                                    vm.disableExport = false;
                                });
                        });
                };

                vm.getReport = function () {
                    vm.loading = true;
                    var input = vm.getInput();
                    feedbackUnreadAppService.getAllReport(input)
                        .success(result => {
                            console.log(result);
                            vm.gridOptions.totalItems = result.totalCount;
                            vm.gridOptions.data = result.items;
                        })
                        .finally(() => {
                            vm.loading = false;
                        })
                };
            

                vm.openRepliesModel = function (myObj) {
                    vm.openModel(myObj.id, true, false, false, false);
                };

                vm.openAcknowledgedModel = function (myObj) {
                    vm.openModel(myObj.id, false, true, false, false);
                };

                vm.openUnreadModel = function (myObj) {
                    vm.openModel(myObj.id, false, false, true, false);
                };

                vm.openLocationModel = function (myObj) {
                    vm.openModel(myObj.id, false, false, false, true);
                };

                vm.openModel = function (tickId, isReply, acknowledged, isUnread, isAllLocations) {
                    $modal.open({
                        templateUrl: "~/App/tenant/views/tickmessages/reply.cshtml",
                        controller: "tenant.views.tickmessages.reply as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            isReply: function () {
                                return isReply;
                            },
                            acknowledged: function () {
                                return acknowledged;
                            },
                            tickMessageId: function () {
                                return tickId;
                            },
                            isUnread: function () {
                                return isUnread;
                            },
                            isAllLocations: function () {
                                return isAllLocations;
                            },
                            isNonReply: function () {
                                return false;
                            }
                        }
                    });
                };

                vm.clear = function () {
                    vm.locationGroup = app.createLocationInputForSearch();
                    vm.filterText = "";
                    vm.unread = false;
                    vm.acknowledgement = false;
                    vm.replies = false;

                    vm.dateRangeModel = {
                        startDate: todayAsString,
                        endDate: todayAsString
                    };
                    $scope.builder.builder.reset();
                    vm.getReport();
                };
                $scope.builder =
                    app.createBuilder(
                        {
                            condition: "AND",
                            rules: [
                                {
                                    id: "MessageHeader",
                                    operator: "equal",
                                    empty: true
                                }
                            ]
                        },
                        angular.fromJson
                            (
                                [
                                    {
                                        "id": "MessageHeader",
                                        "label": app.localize("MessageHeader"),
                                        "type": "string",
                                    },
                                    {
                                        "id": "Type",
                                        "label": app.localize("Type"),
                                        "type": "string",
                                    },
                                    {
                                        "id": "Content",
                                        "label": app.localize("Content"),
                                        "type": "string",
                                    }
                                ]
                            )
                    );
            }
        ]);
})();