﻿(function () {
    appModule.controller('tenant.views.connect.report.feedbackunread.detail', [
        '$scope', '$uibModalInstance', "$uibModal", 'feedback', 'abp.services.app.feedbackUnread',
        function ($scope, $modalInstance, $modal, feedback, feedbackUnreadService) {
            var vm = this;

            vm.close = function () {
                $modalInstance.dismiss();
            };

            vm.init = function () {
                abp.ui.setBusy();
                feedbackUnreadService.getDetail({
                    id: feedback
                }).success(function (result) {
                    vm.plantId = result.plantId;
                    vm.tickMessageId = result.tickMessageId;
                    vm.location = result.location;
                    vm.messageHeader = result.messageHeader;
                    vm.type = result.type;
                    vm.isTextContent = result.isTextContent;
                    vm.createdOn = result.createdOn;
                    vm.unread = result.unread;
                    vm.acknowledgement = result.acknowledgement;
                    vm.reply = result.reply;
                    vm.replies = result.replies;
                    vm.content = result.content;
                    vm.user = result.user;
                    vm.respondedOn = result.responsedOn;
                    if (vm.isTextContent) {
                        document.getElementById("content").innerHTML = vm.content;
                    }
                }).finally(function () {
                    abp.ui.clearBusy();
                });
            };

            vm.init();
            vm.openModel = function (tickId, isReply, acknowledged, isUnread, isAllLocations) {
                $modal.open({
                    templateUrl: "~/App/tenant/views/tickmessages/reply.cshtml",
                    controller: "tenant.views.tickmessages.reply as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        isReply: function () {
                            return isReply;
                        },
                        acknowledged: function () {
                            return acknowledged;
                        },
                        tickMessageId: function () {
                            return tickId;
                        },
                        isUnread: function () {
                            return isUnread;
                        },
                        isAllLocations: function () {
                            return isAllLocations;
                        },
                        isNonReply: function () {
                            return false;
                        }
                    }
                });
            };
        }
    ]);
})();