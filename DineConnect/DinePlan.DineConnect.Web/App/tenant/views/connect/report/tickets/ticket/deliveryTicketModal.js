﻿(function () {
    appModule.controller('tenant.views.connect.report.deliveryticketModal', [
        '$scope', '$uibModalInstance', 'ticket','abp.services.app.delivery',
        function ($scope, $modalInstance, ticket,ticketService) {
            var vm = this;

            vm.close = function () {
                $modalInstance.dismiss();
            };

            vm.init = function() {
                ticketService.getDeliveryTicketById({
                    id: ticket
                }).success(function (result) {
                    console.log(result);
                    vm.ticket = result;
                    vm.ctime = moment(vm.ticket.creationTime).format('LLL');
                    vm.otime = moment(vm.ticket.lastUpdateTime).format('LLL');
                });
            };

            vm.init();
        }
    ]);
})();