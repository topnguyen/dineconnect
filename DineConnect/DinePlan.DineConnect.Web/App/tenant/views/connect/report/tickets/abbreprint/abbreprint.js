﻿(function () {
	appModule.controller("tenant.views.connect.report.tickets.abbreprint",
		[
			"$scope", "$uibModal", "uiGridConstants", "abp.services.app.connectReport", "abp.services.app.connectLookup",
			"abp.services.app.customerReport", "abp.services.app.tenantSettings", "abp.services.app.commonLookup",
			function ($scope,
				$modal,
				uiGridConstants,
				connectReportAppService,
				connectLookupService,
				crService,
				tenantSettingsService,
				commonLookup
			) {
				var vm = this;
				vm.payments = [];
				vm.transactions = [];
				vm.ticketNumber = '';
				vm.isMoreThanOneReprintChecked = false;
				vm.saleStatus = 'All';
				vm.loading = false;
				vm.advancedFiltersAreShown = false;
				$scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
				$scope.format = $scope.formats[0];

				vm.requestParams = {
					locations: "",
					payments: [],
					departments: "",
					transactions: "",
					ticketTags: "",
					terminalName: "",
					lastModifiedUserName: "",
					skipCount: 0,
					maxResultCount: app.consts.grid.defaultPageSize,
					sorting: null,
					userId: abp.session.userId,
					credit: false,
					refund: false,
					ticketNo: ""
				};
				vm.users = [];
				var todayAsString = moment().format("YYYY-MM-DD");
				vm.dateRangeOptions = app.createDateRangePickerOptions();
				vm.dateRangeModel = {
					startDate: todayAsString,
					endDate: todayAsString
				};

				$scope.$on("$viewContentLoaded",
					function () {
						App.initAjax();
					});



				vm.getEditionValue = function (item) {
					return item.displayText;
				};

				vm.decimals = null;
				vm.getDecimals = function () {
					tenantSettingsService.getAllSettings()
						.success(function (result) {
							vm.decimals = result.connect.decimals;
						}).finally(function () {
						});
				};
				vm.locations = [];
				vm.locationsCode = [];

				vm.$onInit = function () {
					localStorage.clear();
					vm.getDecimals();
					vm.getTransactions();
					vm.getPayments();
					vm.getDepartments();
					vm.getLocations();
					vm.getUsers();
				};

				// #region Grid
				vm.gridOptions = {
					showColumnFooter: true,
					enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
					enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
					paginationPageSizes: app.consts.grid.defaultPageSizes,
					paginationPageSize: app.consts.grid.defaultPageSize,
					useExternalPagination: true,
					useExternalSorting: true,
					appScopeProvider: vm,
					columnDefs: [
						{
							name: app.localize("PlantCode"),
							field: "plantCode"
						},
						{
							name: app.localize("PlantName"),
							field: "plantName"
						},
						{
							name: app.localize("Cashier"),
							field: "cashier"
						},
						{
							name: app.localize("Date"),
							field: "date"
						},
						{
							name: app.localize("TicketNumber"),
							field: "ticketNumber"
						},
						{
							name: app.localize("Status"),
							field: "status"
						},
						{
							name: app.localize("NoOfReprint"),
							field: "noOfReprint"
						},
						{
							name: app.localize("TerminalName"),
							field: "terminalName"
						},
						{
							name: app.localize("Total"),
							field: "total"
						},
					],
					onRegisterApi: function (gridApi) {
						$scope.gridApi = gridApi;
						$scope.gridApi.core.on.sortChanged($scope,
							function (grid, sortColumns) {
								if (!sortColumns.length || !sortColumns[0].field) {
									vm.requestParams.sorting = null;
								} else {
									vm.requestParams.sorting =
										sortColumns[0].field + " " + sortColumns[0].sort.direction;
								}
								vm.getReport();
							});
						gridApi.pagination.on.paginationChanged($scope,
							function (pageNumber, pageSize) {
								vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
								vm.requestParams.maxResultCount = pageSize;
								vm.getReport();
							});
					},
					data: []
				};
				vm.disableExport = false;

				vm.toggleFooter = function () {
					$scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
					$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
				};

				vm.toggleColumnFooter = function () {
					$scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
					$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
				};

				// #endregion
				// #region Location

				vm.intiailizeOpenLocation = function () {
					vm.locationGroup = app.createLocationInputForSearch();
				};

				vm.intiailizeOpenLocation();

				vm.openLocation = function () {
					const modalInstance = $modal.open({
						templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
						controller: "tenant.views.connect.location.select.location as vm",
						backdrop: "static",
						keyboard: false,
						resolve: {
							location: function () {
								return vm.locationGroup;
							}
						}
					});
					modalInstance.result.then(function (result) {
						vm.locationGroup = result;
					});
				};

				// #endregion
				// #region Exports
				vm.getCurrentReportInput = function (confirmation, obj) {

					console.log($scope.builder);

					return {
						startDate: moment(vm.dateRangeModel.startDate).lang("en").format($scope.format),
						endDate: moment(vm.dateRangeModel.endDate).lang("en").format($scope.format),
						locationGroup: vm.locationGroup,
						payments: vm.requestParams.payments,
						departments: vm.requestParams.departments,
						ticketTags: vm.requestParams.ticketTags,
						transactions: vm.requestParams.transactions,
						terminalName: vm.requestParams.terminalName,
						lastModifiedUserName: vm.requestParams.lastModifiedUserName,
						skipCount: vm.requestParams.skipCount,
						maxResultCount: vm.requestParams.maxResultCount,
						sorting: vm.requestParams.sorting,
						credit: vm.requestParams.credit,
						refund: vm.requestParams.refund,
						ticketNo: vm.requestParams.ticketNo,
						runInBackground: confirmation,
						exportOutputType: obj,
						dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
						SaleStatus: vm.saleStatus,
						IsMoreThanOneReprintChecked: vm.isMoreThanOneReprintChecked,
						ticketNumber: vm.ticketNumber
					};
				};

				vm.getReport = function () {
					vm.loading = true;
					connectReportAppService.getABBReprintReport(vm.getCurrentReportInput())
						.success(function (result) {
							vm.gridOptions.data = result.items;
							vm.totalAverage = result.totalCount;
						}).finally(function () {
							vm.loading = false;
						});
				};

				vm.exportToExcel = function (obj) {
					abp.ui.setBusy("#MyLoginForm");
					abp.message.confirm(app.localize("AreYouSure"),
						app.localize("RunInBackground"),
						function (isConfirmed) {
							var myConfirmation = false;
							if (isConfirmed) {
								myConfirmation = true;
							}
							vm.disableExport = true;
							connectReportAppService.getABBReportExport(vm.getCurrentReportInput(myConfirmation, obj))
								.success(function (result) {
									if (result != null) {
										app.downloadTempFile(result);
									}
								}).finally(function () {
									abp.ui.clearBusy("#MyLoginForm");
									vm.disableExport = false;
								});
						});
				};

				// #region Builder

				$scope.builder =
					app.createBuilder(
						{
							condition: "AND",
							rules: [
							]
						},
						angular.fromJson
							(
								[
									{
										"id": "PlantCode",
										"label": app.localize("PlantCode"),
										"type": "string",
										"operators": [
											"equal",
											"not_equal",
										],
										plugin: 'selectize',
										plugin_config: {
											valueField: 'value',
											labelField: 'displayText',
											searchField: 'displayText',
											sortField: 'displayText',
											create: true,
											maxItems: 1,
											plugins: ['remove_button'],
											onInitialize: function () {
												var that = this;
												if (vm.locationsCode.length > 0) {
													vm.locationsCode.forEach(function (item) {
														item.value = item.displayText;
														that.addOption(item);
													});
												} else {
													commonLookup.getLocationCodeForCombobox({
													}).success(function (result) {
														vm.locationsCode = result.items;
														console.log(vm.locationsCode);
														vm.locationsCode.forEach(function (item) {
															item.value = item.displayText;
															that.addOption(item);
														});
													}).finally(function (result) {
													});
												}
											}
										},
										valueSetter: function (rule, value) {
											rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
										}
									},
									{
										"id": "PlantName",
										"label": app.localize("PlantName"),
										"type": "string",
										"operators": [
											"equal",
											"not_equal",
										],
										plugin: 'selectize',
										plugin_config: {
											valueField: 'value',
											labelField: 'displayText',
											searchField: 'displayText',
											sortField: 'displayText',
											create: true,
											maxItems: 1,
											plugins: ['remove_button'],
											onInitialize: function () {
												var that = this;
												vm.locations.forEach(function (item) {
													item.value = item.displayText;
													that.addOption(item);
												});
											}
										},
										valueSetter: function (rule, value) {
											rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
										}
									},
									{
										"id": "Cashier",
										"label": app.localize("Cashier"),
										"type": "string",
										"operators": [
											"equal",
											"not_equal",
										],
										type: 'string',
										plugin: 'selectize',
										plugin_config: {
											valueField: 'value',
											labelField: 'displayText',
											searchField: 'displayText',
											sortField: 'displayText',
											create: true,
											maxItems: 1,
											plugins: ['remove_button'],
											onInitialize: function () {
												var that = this;
												if (vm.users.length > 0) {
													vm.users.forEach(function (item) {
														item.value = item.displayText;
														that.addOption(item);
													});
												} else {
													commonLookup.getUserForCombobox({
													}).success(function (result) {
														vm.users = result.items;
														vm.users.forEach(function (item) {
															item.value = item.displayText;
															that.addOption(item);
														});
													}).finally(function (result) {
													});
												}
											}
										},
										valueSetter: function (rule, value) {
											rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
										}
									},
									{
										"id": "Date",
										"label": app.localize("Date"),
										"type": "string",
										"operators": [
											"equal",
											"less",
											"less_or_equal",
											"greater",
											"greater_or_equal"
										],
										"placeholder": 'dd-mm-yyyy',
									},
									{
										"id": "TicketNumber",
										"label": app.localize("TicketNumber"),
										"type": "string"
									},
									{
										"id": "Status",
										"label": app.localize("Status"),
										"type": "string",
										plugin: 'selectize',
										plugin_config: {
											valueField: 'value',
											labelField: 'displayText',
											searchField: 'displayText',
											sortField: 'displayText',
											create: true,
											maxItems: 1,
											plugins: ['remove_button'],
											onInitialize: function () {
												var that = this;
												var sale = { "value": "Sale", "displayText": "Sale" };
												var Return = { "value": "Return", "displayText": "Return" };
												var refund = { "value": "Refund", "displayText": "Refund" };
												var paid = { "value": "Paid", "displayText": "Paid" };
												that.addOption(sale);
												that.addOption(Return);
												that.addOption(refund);
												that.addOption(paid);
											}
										}

									}, {
										"id": "NoOfReprint",
										"label": app.localize("NoOfReprint"),
										"type": "double"
									}, {
										"id": "TerminalName",
										"label": app.localize("TerminalName"),
										"type": "string"
									},
									{
										"id": "Total",
										"label": app.localize("Total"),
										"operators": [
											"equal",
											"less",
											"less_or_equal",
											"greater",
											"greater_or_equal"
										],
										"type": "double",
										"validation": {
											"min": 0,
											"step": 0.01
										}
									},
								]
							)
					);


				// #endregion
				// #region LocalStorage

				vm.getTransactions = function () {
					connectLookupService.getTransactionTypesForCombobox({
					}).success(function (result) {
						localStorage.transactionTypes = JSON.stringify(result.items);
					}).finally(function (result) {
					});
				};
				vm.getPayments = function () {
					connectLookupService.getPaymentTypes({
					}).success(function (result) {
						localStorage.paymentTypes = JSON.stringify(result.items);
					}).finally(function (result) {
					});
				};
				vm.getDepartments = function () {
					connectLookupService.getDepartments({
					}).success(function (result) {
						localStorage.departments = JSON.stringify(result.items);
					}).finally(function (result) {
					});
				};
				vm.getLocations = function () {
					commonLookup.getLocationCodeForCombobox({
					}).success(function (result) {
						vm.locationsCode = result.items;
						console.log('code', vm.locationsCode);
					}).finally(function (result) {
					});

					commonLookup.getLocationForCombobox({
					}).success(function (result) {
						vm.locations = result.items;
						console.log(vm.locations);
					}).finally(function (result) {
					});
				};
				vm.getUsers = function () {
					commonLookup.getUserForCombobox({
					}).success(function (result) {
						vm.users = result.items;
					}).finally(function (result) {
					});
				};

				vm.clear = function () {
					vm.dateRangeModel = {
						startDate: todayAsString,
						endDate: todayAsString
					};
					$scope.builder.builder.reset();
					vm.getReport();
				}

				// #endregion

			}
		]);
})();