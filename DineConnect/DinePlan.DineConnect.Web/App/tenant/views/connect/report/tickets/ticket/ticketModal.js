﻿(function () {
    appModule.controller('tenant.views.connect.report.ticketModal', [
        '$scope', '$uibModalInstance', 'ticket','abp.services.app.ticket',
        function ($scope, $modalInstance, ticket,ticketService) {
            var vm = this;
            vm.credit = false;
            vm.close = function () {
                $modalInstance.dismiss();
            };
            
            vm.init = function () {
                abp.ui.setBusy();
                ticketService.getInputTicket({
                    id: ticket
                }).success(function (result) {
                    console.log(result);
                    vm.ticket = result;
                    console.log(vm.ticket);
                    vm.ctime = moment(vm.ticket.ticketCreatedTime).format('LLL');
                    vm.otime = moment(vm.ticket.lastOrderTime).format('LLL');
                    vm.ptime = moment(vm.ticket.lastPaymentTime).format('LLL');
                    vm.credit = vm.ticket.credit;
                }).finally(function () {
                    abp.ui.clearBusy();
                });
            };

            vm.init();
        }
    ]);
})();