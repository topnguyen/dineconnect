﻿(function () {
	appModule.controller("tenant.views.connect.report.tickets.ticket",
		[
			"$scope", "$uibModal", "uiGridConstants", "abp.services.app.connectReport", "abp.services.app.connectLookup",
			"abp.services.app.customerReport", "abp.services.app.tenantSettings", "abp.services.app.commonLookup", 'abp.services.app.transactionType', 'lookupModal',
			'abp.services.app.department',
			'abp.services.app.paymentType',
			function ($scope,
				$modal,
				uiGridConstants,
				connectReportAppService,
				connectLookupService,
				crService,
				tenantSettingsService,
				commonLookup,
				transactionService,
				lookupModal,
				departmentService,
				paymentTypeService
			) {
				var vm = this;
				vm.payments = [];
				vm.transactions = [];
				vm.currencyText = abp.features.getValue("DinePlan.DineConnect.Connect.Currency");
				vm.franchise = abp.features.getValue("DinePlan.DineConnect.Connect.Franchise") == "true";
				vm.tallyAvailable = abp.setting.getBoolean("App.House.TallyIntegrationFlag");
				vm.mycustomer = abp.features.getValue("DinePlan.DineConnect.Connect.Customer");


				vm.loading = false;
				vm.advancedFiltersAreShown = false;
				$scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
				$scope.format = $scope.formats[0];

				vm.totalTicketAmount = "";
				vm.totalTickets = "";
				vm.totalOrders = "";
				vm.totalItems = "";
				vm.requestParams = {
					locations: "",
					payments: [],
					departments: [],
					transactions: [],
					ticketTags: "",
					terminalName: "",
					lastModifiedUserName: "",
					skipCount: 0,
					maxResultCount: app.consts.grid.defaultPageSize,
					sorting: null,
					userId: abp.session.userId,
					credit: false,
					refund: false,
					ticketNo: ""
				};
				vm.paymentsInput = [];
				vm.departmentsInput = [];
				vm.transactionsInput = [];

				var todayAsString = moment().format("YYYY-MM-DD");
				vm.dateRangeOptions = app.createDateRangePickerOptions();
				vm.dateRangeModel = {
					startDate: todayAsString,
					endDate: todayAsString
				};

				$scope.$on("$viewContentLoaded",
					function () {
						App.initAjax();
					});



				vm.getEditionValue = function (item) {
					return item.displayText;
				};

				vm.decimals = null;
				vm.getDecimals = function () {
					tenantSettingsService.getAllSettings()
						.success(function (result) {
							vm.decimals = result.connect.decimals;
						}).finally(function () {
						});
				};
				vm.locations = [];

				vm.$onInit = function () {
					localStorage.clear();
					vm.getDecimals();
					vm.getTransactions();
					vm.getPayments();
					vm.getDepartments();
					vm.getLocations();
					vm.getUsers();

				};

				// #region Grid
				vm.gridOptions = {
					showColumnFooter: true,
					enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
					enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
					paginationPageSizes: app.consts.grid.defaultPageSizes,
					paginationPageSize: app.consts.grid.defaultPageSize,
					useExternalPagination: true,
					useExternalSorting: true,
					appScopeProvider: vm,
					columnDefs: [
						{
							name: "Actions",
							enableSorting: false,
							width: 50,
							headerCellTemplate: "<span></span>",
							cellTemplate:
								"<div class=\"ui-grid-cell-contents text-center\">" +
								"  <button class=\"btn btn-default btn-xs\" ng-click=\"grid.appScope.showDetails(row.entity)\"><i class=\"fa fa-search\"></i></button>" +
								"</div>"
						},
						{
							name: app.localize("Id"),
							field: "id"
						}, {
							name: app.localize("TicketNo"),
							field: "ticketNumber"
						},
						{
							name: app.localize("Location"),
							field: "locationName"
						},
						{
							name: app.localize("Terminal"),
							field: "terminalName"
						},
						{
							name: app.localize("Department"),
							field: "departmentName"
						},
						{
							name: app.localize("User"),
							field: "lastModifiedUserName"
						},
						{
							name: app.localize("TableNumber"),
							field: "tableNumber"
						},
						{
							name: app.localize("Pax"),
							field: "pax",
							cellClass: "ui-ralign",
							aggregationType: uiGridConstants.aggregationTypes.sum,
							footerCellTemplate:
								'<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'
						},
						{
							name: app.localize("TotalAmount"),
							field: "totalAmount",
							cellClass: "ui-ralign",
							aggregationType: uiGridConstants.aggregationTypes.sum,
							footerCellTemplate:
								'<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'
						}
					],
					onRegisterApi: function (gridApi) {
						$scope.gridApi = gridApi;
						$scope.gridApi.core.on.sortChanged($scope,
							function (grid, sortColumns) {
								if (!sortColumns.length || !sortColumns[0].field) {
									vm.requestParams.sorting = null;
								} else {
									vm.requestParams.sorting =
										sortColumns[0].field + " " + sortColumns[0].sort.direction;
								}
								vm.getTickets();
							});
						gridApi.pagination.on.paginationChanged($scope,
							function (pageNumber, pageSize) {
								vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
								vm.requestParams.maxResultCount = pageSize;
								vm.getTickets();
							});
					},
					data: []
				};
				vm.disableExport = false;

				vm.toggleFooter = function () {
					$scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
					$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
				};

				vm.toggleColumnFooter = function () {
					$scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
					$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
				};

				// #endregion
				// #region Location

				vm.intiailizeOpenLocation = function () {
					vm.locationGroup = app.createLocationInputForSearch();
				};
				vm.intiailizeOpenLocation();

				vm.openLocation = function () {
					const modalInstance = $modal.open({
						templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
						controller: "tenant.views.connect.location.select.location as vm",
						backdrop: "static",
						keyboard: false,
						resolve: {
							location: function () {
								return vm.locationGroup;
							}
						}
					});
					modalInstance.result.then(function (result) {
						vm.locationGroup = result;
					});
				};
				vm.showDetails = function (ticket) {
					$modal.open({
						templateUrl: "~/App/tenant/views/connect/report/tickets/ticket/ticketModal.cshtml",
						controller: "tenant.views.connect.report.ticketModal as vm",
						backdrop: "static",
						resolve: {
							ticket: function () {
								return ticket.id;
							}
						}
					});
				};

				// #endregion
				// #region Exports
			
				vm.getCurrentTicketInput = function (confirmation, obj) {
					return {
						startDate: moment(vm.dateRangeModel.startDate).lang("en").format($scope.format),
						endDate: moment(vm.dateRangeModel.endDate).lang("en").format($scope.format),
						locationGroup: vm.locationGroup,
						payments: vm.paymentsInput,
						departments: vm.departmentsInput,
						ticketTags: vm.requestParams.ticketTags,
						transactions: vm.transactionsInput,
						terminalName: vm.requestParams.terminalName,
						lastModifiedUserName: vm.requestParams.lastModifiedUserName,
						skipCount: vm.requestParams.skipCount,
						maxResultCount: vm.requestParams.maxResultCount,
						sorting: vm.requestParams.sorting,
						credit: vm.requestParams.credit,
						refund: vm.requestParams.refund,
						ticketNo: vm.requestParams.ticketNo,
						runInBackground: confirmation,
						exportOutputType: obj,
						dynamicFilter: angular.toJson($scope.builder.builder.getRules())
					};
				};
				vm.exportToExcel = function (obj) {
					abp.ui.setBusy("#MyLoginForm");
					abp.message.confirm(app.localize("AreYouSure"),
						app.localize("RunInBackground"),
						function (isConfirmed) {
							var myConfirmation = false;
							if (isConfirmed) {
								myConfirmation = true;
							}
							vm.disableExport = true;
							connectReportAppService.getAllToExcel(vm.getCurrentTicketInput(myConfirmation, obj))
								.success(function (result) {
									if (result != null) {
										app.downloadTempFile(result);
									}
								}).finally(function () {
									abp.ui.clearBusy("#MyLoginForm");
									vm.disableExport = false;
								});
						});
				};
				vm.exportForTally = function () {
					abp.ui.setBusy("#MyLoginForm");
					abp.message.confirm(app.localize("AreYouSure"),
						app.localize("RunInBackground"),
						function (isConfirmed) {
							var myConfirmation = false;
							if (isConfirmed) {
								myConfirmation = true;
							}
							vm.disableExport = true;
							connectReportAppService.getTallyExport(vm.getCurrentTicketInput(myConfirmation, null))
								.success(function (result) {
									if (result != null) {
										app.downloadTempFile(result);
									}
								}).finally(function () {
									abp.ui.clearBusy("#MyLoginForm");
									vm.disableExport = false;
								});
						});
				};
				vm.exportToFranchise = function () {
					abp.ui.setBusy("#MyLoginForm");
					abp.message.confirm(app.localize("AreYouSure"),
						app.localize("RunInBackground"),
						function (isConfirmed) {
							var myConfirmation = false;
							if (isConfirmed) {
								myConfirmation = true;
							}
							vm.disableExport = true;
							connectReportAppService.getFranchiseExcel(vm.getCurrentTicketInput(myConfirmation, null))
								.success(function (result) {
									if (result != null) {
										app.downloadTempFile(result);
									}
								}).finally(function () {
									abp.ui.clearBusy("#MyLoginForm");
									vm.disableExport = false;
								});
						});
				};

				vm.clear = function () {
					vm.dateRangeModel = {
						startDate: todayAsString,
						endDate: todayAsString
					};
					$scope.builder.builder.reset();

					vm.requestParams.transactions = [];
					vm.requestParams.payments = [];
					vm.requestParams.departments = [];

					vm.getTickets();
				}

				//Customization
				vm.getEatcReport = function () {
					abp.ui.setBusy("#MyLoginForm");
					abp.message.confirm(app.localize("AreYouSure"),
						app.localize("RunInBackground"),
						function (isConfirmed) {
							var myConfirmation = false;
							if (isConfirmed) {
								myConfirmation = true;
							}
							vm.disableExport = true;
							crService.getTicketDetailExport(vm.getCurrentTicketInput(myConfirmation, null))
								.success(function (result) {
									if (result != null) {
										app.downloadTempFile(result);
									}
								}).finally(function () {
									abp.ui.clearBusy("#MyLoginForm");
									vm.disableExport = false;
								});
						});
				};
				vm.exportRefundDetailToExcel = function (obj) {
					abp.ui.setBusy("#MyLoginForm");
					abp.message.confirm(app.localize("AreYouSure"),
						app.localize("RunInBackground"),
						function (isConfirmed) {
							var myConfirmation = false;
							if (isConfirmed) {
								myConfirmation = true;
							}
							vm.disableExport = true;
							connectReportAppService
								.getRefundDetaiTicketsToExcel(vm.getCurrentTicketInput(myConfirmation, obj))
								.success(function (result) {
									if (result != null) {
										app.downloadTempFile(result);
									}
								}).finally(function () {
									abp.ui.clearBusy("#MyLoginForm");
									vm.disableExport = false;
								});
						});
				};
				vm.exportWritersCafeToExcel = function () {
					abp.ui.setBusy("#MyLoginForm");
					abp.message.confirm(app.localize("AreYouSure"),
						app.localize("RunInBackground"),
						function (isConfirmed) {
							var myConfirmation = false;
							if (isConfirmed) {
								myConfirmation = true;
							}
							vm.disableExport = true;

							crService.getWritersCafeForTicketToExcel(
								vm.getCurrentTicketInput(myConfirmation, null)
							).success(function (result) {
								if (result != null) {
									app.downloadTempFile(result);
								}
							}).finally(function () {
								vm.disableExport = false;
								abp.ui.clearBusy("#MyLoginForm");
							});
						});
				};

				// #endregion
				// #region Builder

				$scope.builder =
					app.createBuilder(
						{
							condition: "AND",
							rules: [
							]
						},
						angular.fromJson
							(
								[
									{
										"id": "ID",
										"label": app.localize("ID"),
										"type": "double"
									},
									{
										"id": "ticketNumber",
										"label": app.localize("TicketNo"),
										"type": "string"
									},
									{
										"id": "Location.Name",
										"label": app.localize("Location"),
										"type": "string",
										"operators": [
											"equal",
											"not_equal",
										],
										plugin: 'selectize',
										plugin_config: {
											valueField: 'value',
											labelField: 'displayText',
											searchField: 'displayText',
											sortField: 'displayText',
											create: true,
											maxItems: 1,
											plugins: ['remove_button'],
											onInitialize: function () {
												var that = this;
												JSON.parse(vm.locations).forEach(function (item) {
													item.value = item.displayText;
													that.addOption(item);
												});
											}
										},
										valueSetter: function (rule, value) {
											rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
										}
									},
									{
										"id": "terminalName",
										"label": app.localize("Terminal"),
										"type": "string"
									},
									{
										id: 'DepartmentName',
										"label": app.localize("Department"),
										"operators": [
											"equal",
											"not_equal",
										],
										type: 'string',
										plugin: 'selectize',
										plugin_config: {
											valueField: 'value',
											labelField: 'displayText',
											searchField: 'displayText',
											sortField: 'displayText',
											create: true,
											maxItems: 1,
											plugins: ['remove_button'],
											onInitialize: function () {
												var that = this;
												JSON.parse(localStorage.departments).forEach(function (item) {
													item.value = item.displayText;
													that.addOption(item);
												});
											}
										},
										valueSetter: function (rule, value) {
											rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
										}
									},
									{
										"id": "LastModifiedUserName",
										"label": app.localize("User"),
										"type": "string",
										"operators": [
											"equal",
											"not_equal",
										],
										type: 'string',
										plugin: 'selectize',
										plugin_config: {
											valueField: 'value',
											labelField: 'displayText',
											searchField: 'displayText',
											sortField: 'displayText',
											create: true,
											maxItems: 1,
											plugins: ['remove_button'],
											onInitialize: function () {
												var that = this;
											   vm.users.forEach(function (item) {
													item.value = item.displayText;
													that.addOption(item);
												});
											}
										},
										valueSetter: function (rule, value) {
											rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
										}
									},
									{
										"id": "TotalAmount",
										"label": app.localize("TotalAmount"),
										"operators": [
											"equal",
											"less",
											"less_or_equal",
											"greater",
											"greater_or_equal"
										],
										"type": "double",
										"validation": {
											"min": 0,
											"step": 0.01
										}
									},
								]
							)
					);


				// #endregion
				// #region LocalStorage

				vm.getTransactions = function () {
					connectLookupService.getTransactionTypesForCombobox({
					}).success(function (result) {
						localStorage.transactionTypes = JSON.stringify(result.items);
					}).finally(function (result) {
					});
				};
				vm.getPayments = function () {
					connectLookupService.getPaymentTypes({
					}).success(function (result) {
						localStorage.paymentTypes = JSON.stringify(result.items);
					}).finally(function (result) {
					});
				};
				vm.getDepartments = function () {
					connectLookupService.getDepartments({
					}).success(function (result) {
						localStorage.departments = JSON.stringify(result.items);
					}).finally(function (result) {
					});
				};
				vm.getLocations = function () {
					commonLookup.getLocationForCombobox({
					}).success(function (result) {
						vm.locations = JSON.stringify(result.items);
					}).finally(function (result) {
					});
				};
				vm.getUsers = function () {
					commonLookup.getUserForCombobox({
					}).success(function (result) {
						vm.users = result.items;
					}).finally(function (result) {
					});
				};

				// #endregion

				vm.getTickets = function () {
					vm.loading = true;
					connectReportAppService.getTickets(vm.getCurrentTicketInput())
						.success(function (result) {
							vm.gridOptions.totalItems = result.ticketViewList.totalCount;

							for (let i = 0; i < result.ticketViewList.items.length; i++) {
								if (result.ticketViewList.items[i].totalAmount != null)
									result.ticketViewList.items[i].totalAmount =
										result.ticketViewList.items[i].totalAmount.toFixed(vm.decimals);
							}
							console.log("items ticket", result.ticketViewList.items);
							vm.gridOptions.data = result.ticketViewList.items;
							vm.totalTicketAmount = result.dashBoardDto.totalAmount;
							vm.totalTickets = result.dashBoardDto.totalTicketCount;
							vm.totalOrders = result.dashBoardDto.totalOrderCount;
							vm.totalItems = result.dashBoardDto.totalItemSold;
							vm.totalAverage = result.dashBoardDto.averageTicketAmount;
						}).finally(function () {
							vm.loading = false;
						});
				};


				vm.selectDepartment = function () {

					var requestParams = {
						skipCount: 0,
						maxResultCount: app.consts.grid.defaultPageSize,
						sorting: null
					};

					var dataOptions = {
						title: app.localize('Select') + " " + app.localize('Department'),
						serviceMethod: departmentService.getAll,
						selectedData: vm.requestParams.departments,
						requestParams: requestParams
					};

					var modalInstance = $modal.open({
						templateUrl: "~/App/tenant/views/connect/report/common/selectModal.cshtml",
						controller: "tenant.views.connect.report.selectModal as vm",
						backdrop: "static",
						keyboard: false,
						resolve: {
							dataOptions: function () {
								return dataOptions;
							}
						}
					});
					modalInstance.result.then(function (result) {
						if (result.length > 0) {
							vm.requestParams.departments = result;
							vm.departmentsInput = vm.requestParams.departments.map(el => {
								var el = { 'Value': el.id, 'DisplayText': el.name };
								return el;
							});
						}
					});
				};

				vm.resetDepartmentSelectedItem = function () {
					vm.requestParams.departments = [];
				};

				vm.selectPayments = function () {

					var requestParams = {
						skipCount: 0,
						maxResultCount: app.consts.grid.defaultPageSize,
						sorting: null
					};

					var dataOptions = {
						title: app.localize('Select') + ' ' + app.localize('Payment'),
						serviceMethod: paymentTypeService.getAll,
						selectedData: vm.requestParams.payments,
						requestParams: requestParams
					};

					var modalInstance = $modal.open({
						templateUrl: "~/App/tenant/views/connect/report/common/selectModal.cshtml",
						controller: "tenant.views.connect.report.selectModal as vm",
						backdrop: "static",
						keyboard: false,
						resolve: {
							dataOptions: function () {
								return dataOptions;
							}
						}
					});
					modalInstance.result.then(function (result) {
						if (result.length > 0) {
							vm.requestParams.payments = result;
							vm.paymentsInput = vm.requestParams.payments.map(el => el.id);
						}
					});
				},

				vm.resetPaymentSelectedItem = function () {
					vm.requestParams.payments = [];
				};

				vm.selectTransactions = function () {
					var requestParams = {
						skipCount: 0,
						maxResultCount: app.consts.grid.defaultPageSize,
						sorting: null
					};

					var dataOptions = {
						title: app.localize('Select') + ' ' + app.localize('Transaction'),
						serviceMethod: transactionService.getAll,
						selectedData: vm.requestParams.transactions,
						requestParams: requestParams
					};

					var modalInstance = $modal.open({
						templateUrl: "~/App/tenant/views/connect/report/common/selectModal.cshtml",
						controller: "tenant.views.connect.report.selectModal as vm",
						backdrop: "static",
						keyboard: false,
						resolve: {
							dataOptions: function () {
								return dataOptions;
							}
						}
					});
					modalInstance.result.then(function (result) {
						if (result.length > 0) {
							vm.requestParams.transactions = result;
							vm.transactionsInput = vm.requestParams.transactions.map(el => {
								var el = { 'Value': el.id, 'DisplayText': el.name };
								return el;
							});
						}
					});
				};

				vm.resetTransactionSelectedItem = function () {
					vm.requestParams.transactions = [];
				};

			}
		]);
})();