﻿(function () {
	appModule.controller("tenant.views.connect.report.tickets.ticketsync",
		[
			"$scope", "$uibModal", "uiGridConstants", "abp.services.app.connectReport", "abp.services.app.commonLookup", "abp.services.app.tenantSettings",
			function ($scope, $modal, uiGridConstants, connectReportAppService, commonLookup,tenantSettingsService) {
				
				var vm = this;

				vm.loading = false;

				vm.requestParams = {
					skipCount: 0,
					maxResultCount: app.consts.grid.defaultPageSize,
					sorting: null,
				};
				vm.getDatetimeFormat = function () {
					tenantSettingsService.getAllSettings()
						.success(function (result) {
							vm.datetimeFormat = result.connect.dateTimeFormat;
						});
				};
				vm.datetimeFormat = "YYYY-MM-DD HH:mm:ss";
				vm.formatDatetime = function (value) {
					if (value) {
						var format = moment(value).format(vm.datetimeFormat.toUpperCase());
						return format;
					}
					return null;
				}

				vm.dateRangeOptions = app.createDateRangePickerOptions();
				var todayAsString = moment().format("YYYY-MM-DD");
				vm.gridOptions = {
					showColumnFooter: true,
					enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
					enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
					paginationPageSizes: app.consts.grid.defaultPageSizes,
					paginationPageSize: app.consts.grid.defaultPageSize,
					useExternalPagination: true,
					useExternalSorting: true,
					appScopeProvider: vm,
					columnDefs: [
						{
							name: app.localize("Location"),
							field: "locationCode"
						},
						{
							name: app.localize("Name"),
							field: "locationName"
						}, {
							name: app.localize("TicketSyncCount"),
							field: "ticketSyncCount"
						},
						{
							name: app.localize("TicketTotalAmount"),
							field: "ticketTotalAmount"
						},
						{
							name: app.localize("WorkStartDate"),
							field: "workStartDate",
							cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.formatDatetime(row.entity.workStartDate)}}</div>',
						},
						{
							name: app.localize("WorkCloseDay"),
							field: "workCloseDate",
							cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.formatDatetime(row.entity.workCloseDate)}}</div>',
						},
						{
							name: app.localize("TotalTickets"),
							field: "totalTickets"
						},
						{
							name: app.localize("WorkTotalAmount"),
							field: "workTotalAmount"
						},
						{
							name: app.localize("Difference"),
							field: "isDifference",
							cellTemplate:
								'<div class=\"ui-grid-cell-contents\">' +
								'  <span ng-show="row.entity.isDifference" class="label label-success">' + app.localize('Yes') + '</span>' +
								'  <span ng-show="!row.entity.isDifference" class="label label-default">' + app.localize('No') + '</span>' +
								'</div>'
						}
					],
					onRegisterApi: function (gridApi) {
						$scope.gridApi = gridApi;
						$scope.gridApi.core.on.sortChanged($scope,
							function (grid, sortColumns) {
								if (!sortColumns.length || !sortColumns[0].field) {
									vm.requestParams.sorting = null;
								} else {
									vm.requestParams.sorting =
										sortColumns[0].field + " " + sortColumns[0].sort.direction;
								}
								vm.getTicketSyncs();
							});
						gridApi.pagination.on.paginationChanged($scope,
							function (pageNumber, pageSize) {
								vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
								vm.requestParams.maxResultCount = pageSize;
								vm.getTicketSyncs();
							});
					},
					data: []
				};

				vm.intiailizeOpenLocation = function () {
					vm.locationGroup = app.createLocationInputForSearch();
				};

				vm.intiailizeOpenLocation();
				vm.locations = [];
				vm.locationsCode = [];
				vm.$onInit = function () {
					vm.getLocations();
					vm.getDatetimeFormat();
				};

				vm.openLocation = function () {
					const modalInstance = $modal.open({
						templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
						controller: "tenant.views.connect.location.select.location as vm",
						backdrop: "static",
						keyboard: false,
						resolve: {
							location: function () {
								return vm.locationGroup;
							}
						}
					});
					modalInstance.result.then(function (result) {
						vm.locationGroup = result;
					});
				};

				vm.getCurrentTicketInput = function (confirmation, obj) {

					console.log($scope.builder);

					return {
						startDate: moment(vm.dateRangeModel.startDate).lang("en").format($scope.format),
						endDate: moment(vm.dateRangeModel.endDate).lang("en").format($scope.format),
						maxResultCount: vm.requestParams.maxResultCount,
						sorting: vm.requestParams.sorting,
						runInBackground: confirmation,
						skipCount: vm.requestParams.skipCount,
						locationGroup: vm.locationGroup,
						exportOutputType: obj,
						dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
					};
				};

				vm.exportToExcel = function (obj) {
					abp.ui.setBusy("#MyLoginForm");
					abp.message.confirm(app.localize("AreYouSure"),
						app.localize("RunInBackground"),
						function (isConfirmed) {
							var myConfirmation = false;
							if (isConfirmed) {
								myConfirmation = true;
							}
							vm.disableExport = true;
							connectReportAppService.getTicketSyncToExcel(vm.getCurrentTicketInput(myConfirmation, obj))
								.success(function (result) {
									if (result != null) {
										app.downloadTempFile(result);
									}
								}).finally(function () {
									abp.ui.clearBusy("#MyLoginForm");
									vm.disableExport = false;
								});
						});
				};

				vm.getTicketSyncs = function () {
					vm.loading = true;
					connectReportAppService.getTicketSyncs(vm.getCurrentTicketInput())
						.success(function (result) {
							vm.gridOptions.totalItems = result.totalCount;
							vm.gridOptions.data = result.items;
						}).finally(function () {
							vm.loading = false;
						});
				};
				// #region Builder

				$scope.builder =
					app.createBuilder(
						{
							condition: "AND",
							rules: [
							]
						},
						angular.fromJson
							(
								[
									{
										"id": "LocationCode",
										"label": app.localize("LocationCode"),
										"type": "string",
										"operators": [
											"equal",
											"not_equal",
										],
										plugin: 'selectize',
										plugin_config: {
											valueField: 'value',
											labelField: 'displayText',
											searchField: 'displayText',
											sortField: 'displayText',
											create: true,
											maxItems: 1,
											plugins: ['remove_button'],
											onInitialize: function () {
												var that = this;
												if (vm.locationsCode.length > 0) {
													vm.locationsCode.forEach(function (item) {
														item.value = item.displayText;
														that.addOption(item);
													});
												} else {
													commonLookup.getLocationCodeForCombobox({
													}).success(function (result) {
														vm.locationsCode = result.items;
														console.log(vm.locationsCode);
														vm.locationsCode.forEach(function (item) {
															item.value = item.displayText;
															that.addOption(item);
														});
													}).finally(function (result) {
													});
												}
											}
										},
										valueSetter: function (rule, value) {
											rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
										}
									},
									{
										"id": "LocationName",
										"label": app.localize("LocationName"),
										"type": "string",
										"operators": [
											"equal",
											"not_equal",
										],
										plugin: 'selectize',
										plugin_config: {
											valueField: 'value',
											labelField: 'displayText',
											searchField: 'displayText',
											sortField: 'displayText',
											create: true,
											maxItems: 1,
											plugins: ['remove_button'],
											onInitialize: function () {
												var that = this;
												vm.locations.forEach(function (item) {
													item.value = item.displayText;
													that.addOption(item);
												});
											}
										},
										valueSetter: function (rule, value) {
											rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
										}
									},
									{
										"id": "TicketSyncCount",
										"label": app.localize("TicketSyncCount"),
										"operators": [
											"equal",
											"less",
											"less_or_equal",
											"greater",
											"greater_or_equal"
										],
										"type": "double",
										"validation": {
											"min": 0,
											"step": 0.01
										}
									},
									{
										"id": "TicketTotalAmount",
										"label": app.localize("TicketTotalAmount"),
										"operators": [
											"equal",
											"less",
											"less_or_equal",
											"greater",
											"greater_or_equal"
										],
										"type": "double",
										"validation": {
											"min": 0,
											"step": 0.01
										}
									},
									{
										"id": "FormatWorkStartDate",
										"label": app.localize("WorkStartDate"),
										"type": "date",
										"operators": [
											"equal",
											"less",
											"less_or_equal",
											"greater",
											"greater_or_equal"
										],
										"placeholder": 'yyyy/mm/dd',
									},
									{
										"id": "FormatWorkCloseDate",
										"label": app.localize("WorkCloseDate"),
										"type": "date",
										"operators": [
											"equal",
											"less",
											"less_or_equal",
											"greater",
											"greater_or_equal"
										],
										"placeholder": 'yyyy/mm/dd',
									},
									{
										"id": "TotalTickets",
										"label": app.localize("TotalTickets"),
										"operators": [
											"equal",
											"less",
											"less_or_equal",
											"greater",
											"greater_or_equal"
										],
										"type": "double",
										"validation": {
											"min": 0,
											"step": 0.01
										}
									},

									{
										"id": "WorkTotalAmount",
										"label": app.localize("WorkTotalAmount"),
										"operators": [
											"equal",
											"less",
											"less_or_equal",
											"greater",
											"greater_or_equal"
										],
										"type": "double",
										"validation": {
											"min": 0,
											"step": 0.001
										}
									},
									{
										"id": "IsDifference",
										"label": app.localize("Difference"),
										type: 'boolean',
										plugin: 'selectize',
										plugin_config: {
											valueField: 'value',
											labelField: 'displayText',
											searchField: 'displayText',
											sortField: 'displayText',
											create: true,
											maxItems: 1,
											plugins: ['remove_button'],
											onInitialize: function () {
												var that = this;
												var yes = { "value": 1, "displayText": "Yes" };
												var no = { "value": 0, "displayText": "No" };
												that.addOption(yes);
												that.addOption(no);
											}
										}
									}
								]
							)
					);
				vm.clear = function () {
					vm.dateRangeModel = {
						startDate: todayAsString,
						endDate: todayAsString
					};
					$scope.builder.builder.reset();
					vm.getTicketSyncs();
				}

				vm.getLocations = function () {
					commonLookup.getLocationCodeForCombobox({
					}).success(function (result) {
						vm.locationsCode = result.items;
						console.log(vm.locationsCode);
					}).finally(function (result) {
					});

					commonLookup.getLocationForCombobox({
					}).success(function (result) {
						vm.locations = result.items;
						console.log(vm.locations);
					}).finally(function (result) {
					});
				};



				// #endregion
			}
		]);
})();