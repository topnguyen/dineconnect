﻿(function () {
    appModule.controller("tenant.views.connect.report.paymentexcess", [
        "$scope", "$uibModal", "uiGridConstants", "abp.services.app.paymentExcessReport", "abp.services.app.tenantSettings",
        function ($scope, $modal, uiGridConstants, paymentExcessReportService, tenantSettingsService) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });
            vm.locations = [];
            vm.currencyText = abp.features.getValue("DinePlan.DineConnect.Connect.Currency");
            vm.loading = false;
            vm.advancedFiltersAreShown = false;
            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    });
            };

            vm.$onInit = function () {
                vm.getDecimals();
            };

            vm.paymentTags = [];

            vm.requestParams = {
                locations: "",
                payments: [],
                paymentTags: [],
                departments: "",
                transactions: "",
                ticketTags: "",
                terminalName: "",
                lastModifiedUserName: "",
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null,
                userId: abp.session.userId,
                credit: false,
                refund: false,
                ticketNo: ""
            };

            vm.setPaymentTag = function () {
                vm.requestParams.paymentTags = [];
                angular.forEach(vm.paymentTags, function (item) {
                    if (item) {
                        vm.requestParams.paymentTags.push(item.text);
                    }
                });
            };

            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.gridOptions = {
                showColumnFooter: true,
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize("PaymentName"),
                        field: "paymentType",
                        footerCellTemplate: '<div class="ui-grid-cell-contents" >Total:</div>'
                    },
                    {
                        name: app.localize("Actual"),
                        field: 'actual',
                        cellClass: "ui-ralign",
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign">{{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    },
                    {
                        name: app.localize("Tendered"),
                        field: "tendered",
                        cellClass: "ui-ralign",
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign">{{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'

                    },
                    {
                        name: app.localize("Excess"),
                        field: "excess",
                        cellClass: "ui-ralign",
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign">{{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'

                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getReport();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;
                        vm.getReport();
                    });
                },
                data: []
            };

            vm.toggleFooter = function () {
                $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.toggleColumnFooter = function () {
                $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.getReport = function () {
                vm.setPaymentTag();

                vm.loading = true;
                paymentExcessReportService.getPaymentExcessSummary({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationGroup: vm.locationGroup,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    paymentTags: vm.requestParams.paymentTags,
                    dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                }).success(function (result) {
                    angular.forEach(result.items, function (item) {
                        item.actual = item.actual && item.actual.toFixed(vm.decimals);
                        item.tendered = item.tendered && item.tendered.toFixed(vm.decimals);
                        item.excess = item.excess && item.excess.toFixed(vm.decimals);
                    });

                    vm.gridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.getEditionValue = function (item) {
                return item.displayText;
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.exportToSummary = function (mode) {
                vm.setPaymentTag();
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        paymentExcessReportService.getPaymentExcessExport({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locationGroup: vm.locationGroup,
                            sorting: vm.requestParams.sorting,
                            maxResultCount: vm.requestParams.maxResultCount,
                            exportOutputType: mode,
                            runInBackground: myConfirmation,
                            paymentTags: vm.requestParams.paymentTags,
                            dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                        })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });
                    });
            };

            vm.exportDetail = function (mode) {
                vm.setPaymentTag();
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        paymentExcessReportService.getPaymentExcessDetailExport({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locationGroup: vm.locationGroup,
                            sorting: vm.requestParams.sorting,
                            exportOutputType: mode,
                            runInBackground: myConfirmation,
                            paymentTags: vm.requestParams.paymentTags
                        })
                            .success(function (result) {
                                if (result !== null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });
                    });
            };

            vm.clear = function () {
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };
                $scope.builder.builder.reset();
                vm.getReport();
            };
            $scope.builder =
                app.createBuilder(
                    {
                        condition: "AND",
                        rules: []
                    },
                    angular.fromJson
                        (
                            [
                                {
                                    "id": "paymentType",
                                    "label": app.localize("PaymentName"),
                                    "type": "string",
                                },
                                {
                                    "id": "Actual",
                                    "label": app.localize("Actual"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                                {
                                    "id": "Tendered",
                                    "label": app.localize("Tendered"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                                {
                                    "id": "Excess",
                                    "label": app.localize("Excess"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                }
                            ]
                        )
                );
        }
    ]);
})();