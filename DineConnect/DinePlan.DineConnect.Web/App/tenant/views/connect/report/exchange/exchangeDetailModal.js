﻿(function () {
    appModule.controller('tenant.views.connect.report.exchangeDetailModal', [
        '$scope', '$uibModalInstance', 'name', 'exchange',
        function ($scope, $modalInstance, name, exchange) {
            var vm = this;
            vm.name = name;
            vm.exchange = exchange;
            vm.close = function () {
                $modalInstance.dismiss();
            };

            vm.getExchanges = function () {
                return vm.exchange.split('\r\n');
            };
        }
    ]);
})();