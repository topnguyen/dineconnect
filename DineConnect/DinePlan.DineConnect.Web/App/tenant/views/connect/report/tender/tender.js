﻿(function () {
    appModule.controller("tenant.views.connect.report.tender", [
        "$scope", "$uibModal", "uiGridConstants", "abp.services.app.connectReport", "abp.services.app.location", 'uiGridGroupingConstants', "abp.services.app.tenantSettings", "abp.services.app.connectLookup",
        function ($scope, $modal, uiGridConstants, connectService, locationService, uiGridGroupingConstants, tenantSettingsService, connectLookupService) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });
            vm.locations = [];
            vm.currencyText = abp.features.getValue("DinePlan.DineConnect.Connect.Currency");
            vm.loading = false;
            vm.advancedFiltersAreShown = false;
            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    });
            };

            vm.$onInit = function () {
                vm.getDecimals();
                vm.getDatetimeFormat();
            };
            vm.datetimeFormat = "YYYY-MM-DD HH:mm:ss";
            vm.dateFormat = "YYYY-MM-DD";

            vm.getDatetimeFormat = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.datetimeFormat = result.connect.dateTimeFormat;
                        vm.dateFormat = result.connect.simpleDateFormat;
                    });
            };

            vm.formatDatetime = function (value) {
                if (value) {
                    var format = moment(value).format(vm.datetimeFormat.toUpperCase());
                    return format;
                }
                return null;
            }
            vm.formatDate = function (value) {
                if (value) {
                    var format = moment(value).format(vm.dateFormat.toUpperCase());
                    return format;
                }
                return null;
            }


            vm.payments = [];

            vm.requestParams = {
                locations: "",
                payments: [],
                departments: "",
                transactions: "",
                ticketTags: "",
                terminalName: "",
                lastModifiedUserName: "",
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null,
                userId: abp.session.userId,
                credit: false,
                refund: false,
                ticketNo: ""
            };

            vm.requestParams = {
                locations: "",
                payments: [],
                department: "",
                transactions: "",
                terminalName: "",
                lastModifiedUserName: "",
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };
            vm.getEditionValue = function (item) {
                return item.displayText;
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.exportToSummary = function (mode) {
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        connectService.getTenderDetailsExport({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locationGroup: vm.locationGroup,
                            payments: vm.requestParams.payments,
                            department: vm.requestParams.department,
                            transactions: vm.requestParams.transactions,
                            terminalName: vm.requestParams.terminalName,
                            lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                            sorting: vm.requestParams.sorting,
                            maxResultCount: vm.gridOptions.totalItems,
                            exportOutputType: mode,
                            runInBackground: myConfirmation,
                            dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                        })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });
                    });
            };

            vm.gridOptions = {
                showColumnFooter: true,
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize("PaymentName"),
                        field: "tenderName",
                        grouping: { groupPriority: 1 },
                        sort: { priority: 1, direction: 'asc' },
                        cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>'
                    },
                    {
                        name: app.localize("Datetime"),
                        type: 'date',
                        field: 'dateTime',
                        cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.formatDatetime(row.entity.dateTime)}}</div>',
                    },
                    {
                        name: app.localize("InvoiceNo"),
                        field: "invoiceNo"
                    },
                    {
                        name: app.localize("Table"),
                        field: "table"
                    },
                    {
                        name: app.localize("Employee"),
                        field: "employee"
                    },
                    {
                        name: app.localize("CardNumber"),
                        field: "cardNumber"
                    },
                    {
                        name: app.localize("ReferenceNo"),
                        field: "referenceNo"
                    },
                    {
                        name: app.localize("SubTotal"),
                        field: "subTotal",
                        cellClass: "ui-ralign",
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                        customTreeAggregationFinalizerFn: function (aggregation) {
                            aggregation.rendered = aggregation.value.toFixed(vm.decimals);
                        },
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue()}}</div>'
                    },
                    {
                        name: app.localize("Discount"),
                        field: "discount",
                        cellClass: "ui-ralign",
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                        customTreeAggregationFinalizerFn: function (aggregation) {
                            aggregation.rendered = aggregation.value.toFixed(vm.decimals);
                        },
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue()}}</div>'
                    },
                    {
                        name: app.localize("ServiceCharge"),
                        field: "serviceCharge",
                        cellClass: "ui-ralign",
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                        customTreeAggregationFinalizerFn: function (aggregation) {
                            aggregation.rendered = aggregation.value.toFixed(vm.decimals);
                        },
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue()}}</div>'
                    },
                    {
                        name: app.localize("Vat"),
                        field: "vat",
                        cellClass: "ui-ralign",
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                        customTreeAggregationFinalizerFn: function (aggregation) {
                            aggregation.rendered = aggregation.value.toFixed(vm.decimals);
                        },
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue()}}</div>'
                    },
                    {
                        name: app.localize("Total"),
                        field: "total",
                        cellClass: "ui-ralign",
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                        customTreeAggregationFinalizerFn: function (aggregation) {
                            aggregation.rendered = aggregation.value.toFixed(vm.decimals);
                        },
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue()}}</div>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                },
                data: []
            };

            for (var i = 0; i < vm.gridOptions.columnDefs.length; i++) {
                vm.gridOptions.columnDefs[i].width = '15%';
            }

            vm.toggleFooter = function () {
                $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.toggleColumnFooter = function () {
                $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.getOrders = function () {
                vm.loading = true;
                connectService.getTenderDetails({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationGroup: vm.locationGroup,
                    payments: vm.requestParams.payments,
                    department: vm.requestParams.department,
                    transactions: vm.requestParams.transactions,
                    terminalName: vm.requestParams.terminalName,
                    lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                }).success(function (result) {
                    angular.forEach(result.items, function (item) {
                        item.total = item.total && item.total.toFixed(vm.decimals);
                        item.subTotal = item.subTotal && item.subTotal.toFixed(vm.decimals);
                        item.vat = item.vat && item.vat.toFixed(vm.decimals);
                        item.discount = item.discount && item.discount.toFixed(vm.decimals);
                        item.serviceCharge = item.serviceCharge && item.serviceCharge.toFixed(vm.decimals);
                    });

                    vm.gridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.getPayments = function () {
                connectLookupService.getPaymentTypes({
                }).success(function (result) {
                    vm.payments = result.items;
                }).finally(function (result) {
                });
            };

            vm.getPayments();
            vm.clear = function () {
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };
                $scope.builder.builder.reset();
                vm.getOrders();
            }

            $scope.builder =
                app.createBuilder(
                    {
                        condition: "AND",
                        rules: []
                    },
                    angular.fromJson
                        (
                            [
                                {
                                    "id": "InvoiceNo",
                                    "label": app.localize("InvoiceNo"),
                                    "type": "string",
                                },
                                {
                                    "id": "Table",
                                    "label": app.localize("Table"),
                                    "type": "string",
                                },
                                {
                                    "id": "Employee",
                                    "label": app.localize("Employee"),
                                    "type": "string",
                                },
                                {
                                    "id": "CardNumber",
                                    "label": app.localize("CardNumber"),
                                    "type": "string",
                                },
                                {
                                    "id": "ReferenceNo",
                                    "label": app.localize("ReferenceNo"),
                                    "type": "string",
                                },
                                {
                                    "id": "SubTotal",
                                    "label": app.localize("SubTotal"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                                {
                                    "id": "Discount",
                                    "label": app.localize("Discount"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                                {
                                    "id": "ServiceCharge",
                                    "label": app.localize("ServiceCharge"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                                 {
                                     "id": "Vat",
                                     "label": app.localize("Vat"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                }  ,
                                  {
                                      "id": "Total",
                                      "label": app.localize("Total"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                }
                            ]
                        )
                );
        }
    ]);
})();