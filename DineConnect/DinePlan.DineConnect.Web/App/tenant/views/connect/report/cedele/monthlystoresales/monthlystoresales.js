﻿(function () {
	appModule.controller("tenant.views.connect.report.monthlystoresales",
		[
			"$scope", "$uibModal", "uiGridConstants", "abp.services.app.monthlySalesReport", "uiGridGroupingConstants", "stats", "abp.services.app.connectLookup",
			function ($scope, $modal, uiGridConstants, monthlySalesReportAppService, uiGridGroupingConstants, stats, connectLookupService) {

				var vm = this;
				vm.loading = false;
				vm.requestParams = {
					skipCount: 0,
					maxResultCount: app.consts.grid.defaultPageSize,
					sorting: null,
				};
				vm.startDate = new Date();
				vm.endDate = new Date();
				//vm.dateRangeOptions = app.createDateRangePickerOptions();
				vm.dateRangeOptions = {
					format: "mm-yyyy",
					startView: "months",
					minViewMode: "months"
				};
				vm.intiailizeOpenLocation = function () {
					vm.locationGroup = app.createLocationInputForSearch();
				};

				vm.intiailizeOpenLocation();

				vm.lstDepartment = [];
				vm.getDepartments = function () {
					connectLookupService.getDepartments({
					}).success(function (result) {
						vm.lstDepartment = result.items;
					}).finally(function (result) {
					});
				};
				vm.getDepartments();
				vm.openLocation = function () {
					const modalInstance = $modal.open({
						templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
						controller: "tenant.views.connect.location.select.location as vm",
						backdrop: "static",
						keyboard: false,
						resolve: {
							location: function () {
								return vm.locationGroup;
							}
						}
					});
					modalInstance.result.then(function (result) {
						vm.locationGroup = result;
					});
				};


				vm.exportToExcel = function (obj, type) {
					abp.ui.setBusy("#MyLoginForm");
					abp.message.confirm(app.localize("AreYouSure"),
						app.localize("RunInBackground"),
						function (isConfirmed) {
							var myConfirmation = false;
							if (isConfirmed) {
								myConfirmation = true;
							}
							vm.disableExport = true;
							monthlySalesReportAppService.getMonthlyStoreSalesReportToExport(vm.getDataInput(myConfirmation, obj, type))
								.success(function (result) {
									if (result != null) {
										app.downloadTempFile(result);
									}
								}).finally(function () {
									abp.ui.clearBusy("#MyLoginForm");
									vm.disableExport = false;
								});
						});
				};

				vm.getData = function () {
					vm.loading = true;
					monthlySalesReportAppService.getMonthlyStoreSalesReports(vm.getDataInput())
						.success(function (result) {
							vm.data = result;
							vm.title = result.title;
						}).finally(function () {
							vm.loading = false;
						});
				};

				vm.getDataInput = function (confirmation, obj, type) {
					var fromDate = $("#fromDate").val().substring(0, 2) + '/01/' + $("#fromDate").val().substring(3);
					var toDate = $("#endDate").val().substring(0, 2) + '/01/' + $("#endDate").val().substring(3);
					return {
						startDate: moment(fromDate).lang("en").format($scope.format),
						endDate: moment(toDate).lang("en").format($scope.format),
						maxResultCount: vm.requestParams.maxResultCount,
						sorting: vm.requestParams.sorting,
						runInBackground: confirmation,
						skipCount: vm.requestParams.skipCount,
						locationGroup: vm.locationGroup,
						exportOutputType: obj,
						exportBy: type
					};
				};
				$scope.formatTimeSpan = function (input) {
					var indexDot = input.indexOf(".");
					if (indexDot > 0) {
						return input.slice(0, indexDot);
					} else {
						return input;
					}
				}

				// #region Builder

				var todayAsString = moment().format("YYYY-MM-DD");
				vm.clear = function () {
					vm.dateRangeModel = {
						startDate: todayAsString,
						endDate: todayAsString
					};
					$("#fromDate").val(moment().format("MM-YYYY"));
					$("#endDate").val(moment().format("MM-YYYY"));
					vm.getData();
				}

				// #endregion

				$scope.getYearFromDate = function (input) {
					var current = new Date(input);
					return current.getFullYear();
				}



				vm.init = function () {

					$('.from').datetimepicker({
						viewMode: 'months',
						format: 'MM/YYYY',
						toolbarPlacement: "top",
						allowInputToggle: true,
						icons: {
							time: 'fa fa-time',
							date: 'fa fa-calendar',
							up: 'fa fa-chevron-up',
							down: 'fa fa-chevron-down',
							previous: 'fa fa-chevron-left',
							next: 'fa fa-chevron-right',
							today: 'fa fa-screenshot',
							clear: 'fa fa-trash',
							close: 'fa fa-remove'
						}
					}).on('changeDate', function (selected) {
						vm.startDate = new Date(selected.date.valueOf());
						vm.startDate.setDate(vm.startDate.getDate(new Date(selected.date.valueOf())));
						$('.to').datepicker('setStartDate', vm.startDate);
					});

					$('.to').datetimepicker({
						viewMode: 'months',
						format: 'MM/YYYY',
						toolbarPlacement: "top",
						allowInputToggle: true,
						icons: {
							time: 'fa fa-time',
							date: 'fa fa-calendar',
							up: 'fa fa-chevron-up',
							down: 'fa fa-chevron-down',
							previous: 'fa fa-chevron-left',
							next: 'fa fa-chevron-right',
							today: 'fa fa-screenshot',
							clear: 'fa fa-trash',
							close: 'fa fa-remove'
						}
					})
						.on('changeDate', function (selected) {
							vm.endDate = new Date(selected.date.valueOf());
							vm.endDate.setDate(vm.endDate.getDate(new Date(selected.date.valueOf())));
							$('.from').datepicker('setEndDate', vm.endDate);
						});

					$(".from").on("dp.show", function (e) {
						$(e.target).data("DateTimePicker").viewMode("months");
					});
					$(".to").on("dp.show", function (e) {
						$(e.target).data("DateTimePicker").viewMode("months");
					});

					$("#fromDate").val(moment().format("MM-YYYY"));
					$("#endDate").val(moment().format("MM-YYYY"));
				}
				vm.init();


				vm.getTotalAmount = function (department, data) {
					let index = data.findIndex(x => x.department == department.displayText);
					if (index >= 0) {
						return data[index].totalAmount;
					} else {
						return 0;
					}

				}
			}
		])
})();