﻿(function () {
	appModule.controller("tenant.views.connect.report.mealtimesales",
		[
			"$scope", "$uibModal", "uiGridConstants", "abp.services.app.mealTimeSalesReport", "uiGridGroupingConstants", "stats", "abp.services.app.connectLookup",
			function ($scope, $modal, uiGridConstants, mealTimeSaleReportAppService, uiGridGroupingConstants, stats, connectLookupService) {

				var vm = this;
				vm.loading = false;
				vm.requestParams = {
					skipCount: 0,
					maxResultCount: app.consts.grid.defaultPageSize,
					sorting: null,
				};
				vm.dateRangeOptions = app.createDateRangePickerOptions();
				vm.intiailizeOpenLocation = function () {
					vm.locationGroup = app.createLocationInputForSearch();
				};

				var todayAsString = moment().format("YYYY-MM-DD");
				vm.dateRangeModel = {
					startDate: todayAsString,
					endDate: todayAsString
				};


				vm.intiailizeOpenLocation();

				vm.lstDepartment = [];
				vm.getDepartments = function () {
					connectLookupService.getDepartments({
					}).success(function (result) {
						vm.lstDepartment = result.items;
					}).finally(function (result) {
					});
				};
				vm.getDepartments();
				vm.openLocation = function () {
					const modalInstance = $modal.open({
						templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
						controller: "tenant.views.connect.location.select.location as vm",
						backdrop: "static",
						keyboard: false,
						resolve: {
							location: function () {
								return vm.locationGroup;
							}
						}
					});
					modalInstance.result.then(function (result) {
						vm.locationGroup = result;
					});
				};


				vm.exportToExcel = function (obj) {
					abp.ui.setBusy("#MyLoginForm");
					abp.message.confirm(app.localize("AreYouSure"),
						app.localize("RunInBackground"),
						function (isConfirmed) {
							var myConfirmation = false;
							if (isConfirmed) {
								myConfirmation = true;
							}
							vm.disableExport = true;
							mealTimeSaleReportAppService.getMealTimeSalesReportToExport(vm.getDataInput(myConfirmation, obj))
								.success(function (result) {
									if (result != null) {
										app.downloadTempFile(result);
									}
								}).finally(function () {
									abp.ui.clearBusy("#MyLoginForm");
									vm.disableExport = false;
								});
						});
				};

				vm.exportToExcelByCategory = function (obj) {
					abp.ui.setBusy("#MyLoginForm");
					abp.message.confirm(app.localize("AreYouSure"),
						app.localize("RunInBackground"),
						function (isConfirmed) {
							var myConfirmation = false;
							if (isConfirmed) {
								myConfirmation = true;
							}
							vm.disableExport = true;
							mealTimeSaleReportAppService.getMealTimeSalesReportByCategoryToExport(vm.getDataInput(myConfirmation, obj))
								.success(function (result) {
									if (result != null) {
										app.downloadTempFile(result);
									}
								}).finally(function () {
									abp.ui.clearBusy("#MyLoginForm");
									vm.disableExport = false;
								});
						});
				};

				vm.getData = function () {
					vm.loading = true;
					mealTimeSaleReportAppService.getMealTimeSalesReports(vm.getDataInput())
						.success(function (result) {
							vm.data = result;
							console.log('meal time', result)
						}).finally(function () {
							vm.loading = false;
						});
				};

				vm.getDataInput = function (confirmation, obj) {
					return {
						startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
						endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
						maxResultCount: vm.requestParams.maxResultCount,
						sorting: vm.requestParams.sorting,
						runInBackground: confirmation,
						skipCount: vm.requestParams.skipCount,
						locationGroup: vm.locationGroup,
						exportOutputType: obj,
					};
				};
				// #region Builder

				vm.clear = function () {
					vm.dateRangeModel = {
						startDate: todayAsString,
						endDate: todayAsString
					};
					vm.getData();
				}

				// #endregion
				vm.processTime = function (input) {
					var times = input.split(':');
					return times[0] + ':' + times[1];
				}
			}
		])
})();