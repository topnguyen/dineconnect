﻿(function () {
	appModule.controller("tenant.views.connect.report.monthlystoresalesstats",
		[
			"$scope", "$uibModal", "uiGridConstants", "abp.services.app.monthlySalesStatsReport", "uiGridGroupingConstants", "stats", "abp.services.app.connectLookup", "abp.services.app.commonLookup",
			function ($scope, $modal, uiGridConstants, monthlySalesStatsReportAppService, uiGridGroupingConstants, stats, connectLookupService, commonLookupService) {

				var vm = this;
				vm.loading = false;

				vm.lstDepartmentGroup = [];
				vm.lstLocation = [];
				vm.departmentGroup1 = {};
				vm.departmentGroup2 = {};

				vm.departmentGroup_Selected_1 = null;
				vm.departmentGroup_Selected_2 = null;

				vm.getDepartmentGroups = function () {
					connectLookupService.getDepartmentGroups({
					}).success(function (result) {
						vm.lstDepartmentGroup = result.items;
						var defaultDepartment = { displayText: "", value: undefined, isSelected: false }
						vm.lstDepartmentGroup.unshift(defaultDepartment);
						vm.departmentGroup_Selected_1 = vm.lstDepartmentGroup[0];
						vm.departmentGroup_Selected_2 = vm.lstDepartmentGroup[0];
					}).finally(function (result) {
					});

				};
				vm.onInit = function () {
					vm.getDepartmentGroups();
				}
				vm.onInit();

				vm.requestParams = {
					skipCount: 0,
					maxResultCount: app.consts.grid.defaultPageSize,
					sorting: null,
				};
				vm.dateRangeOptions = app.createDateRangePickerOptions();
				vm.intiailizeOpenLocation = function () {
					vm.locationGroup = app.createLocationInputForSearch();
				};

				var todayAsString = moment().format("YYYY-MM-DD");
				vm.dateRangeModel = {
					startDate: todayAsString,
					endDate: todayAsString
				};


				vm.intiailizeOpenLocation();

				vm.openLocation = function () {
					const modalInstance = $modal.open({
						templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
						controller: "tenant.views.connect.location.select.location as vm",
						backdrop: "static",
						keyboard: false,
						resolve: {
							location: function () {
								return vm.locationGroup;
							}
						}
					});
					modalInstance.result.then(function (result) {
						vm.locationGroup = result;
					});
				};


				vm.exportToExcel = function (obj) {
					abp.ui.setBusy("#MyLoginForm");
					monthlySalesStatsReportAppService.getMonthlyStoreSalesReportToExport(vm.getDataInput(false, obj))
						.success(function (result) {
							if (result != null) {
								app.downloadTempFile(result);
							}
						}).finally(function () {
							abp.ui.clearBusy("#MyLoginForm");
							vm.disableExport = false;
						});
				};
				vm.delivery = {};  
				vm.tickets = {};  
				vm.aov = {};  

				vm.isComparison = false;
				vm.getData = function () {
					vm.loading = true;
					vm.ticketsLocation = {};
					//case 1: comparison 2 department group
					if (vm.departmentGroup_Selected_1 != null && vm.departmentGroup_Selected_1.value != undefined && vm.departmentGroup_Selected_2 != null && vm.departmentGroup_Selected_2.value != undefined) {
						vm.isComparison = true;
						monthlySalesStatsReportAppService.getMonthlyStoreSalesReports(vm.getDataInput())
							.success(function (result) {
								vm.data = result;
								vm.departmentGroup1 = angular.fromJson(result.departmentGroup_1.departmentGroupWithLocation);
								vm.departmentGroup2 = angular.fromJson(result.departmentGroup_2.departmentGroupWithLocation);

								vm.delivery = angular.fromJson(result.delivery);
								vm.tickets = angular.fromJson(result.tickets);
								vm.aov = angular.fromJson(result.aov);
								vm.lstLocation = result.locations;
							}).finally(function () {
								vm.loading = false;
							});
					} else {
						vm.isComparison = false;
						//case 2: get all department
						monthlySalesStatsReportAppService.getMonthlyStoreSalesReportByAllDepartment(vm.getDataInput())
							.success(function (result) {
								vm.departments = angular.fromJson(result.listDepartment);
								vm.departmentPercent = angular.fromJson(result.listDepartmentPercent);
								vm.totalTickets = angular.fromJson(result.totalTickets);
								vm.lstLocation = result.locations;
							}).finally(function () {
								vm.loading = false;
							});
					}
				};

				vm.getDataInput = function (confirmation, obj) {
					return {
						departmentGroupName_1: vm.departmentGroup_Selected_1.displayText,
						departmentGroupName_2: vm.departmentGroup_Selected_2.displayText,
						startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
						endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
						maxResultCount: vm.requestParams.maxResultCount,
						sorting: vm.requestParams.sorting,
						runInBackground: confirmation,
						skipCount: vm.requestParams.skipCount,
						locationGroup: vm.locationGroup,
						exportOutputType: obj,
					};
				};

				vm.clear = function () {
					vm.dateRangeModel = {
						startDate: todayAsString,
						endDate: todayAsString
					};
					vm.isComparison = true;
					vm.departmentGroup_Selected_1 = vm.lstDepartmentGroup[0];
					vm.departmentGroup_Selected_2 = vm.lstDepartmentGroup[0];
					vm.getData();
				}

				vm.getLocationValue = function (location, departmentWithLocation) {
					var result = angular.fromJson(departmentWithLocation);
					return result[location];
				};
				vm.getLocationValue1 = function (location, departmentWithLocation) {
					return departmentWithLocation[location];
				};
				vm.getDepartmentName = function (department) {
					return department["DepartmentName"];
				};
				vm.getTotalTicketByLocation = function (location) {
					return vm.totalTickets[location];
				};
				vm.getPercentByLocation = function (location, department) {
					return vm.departmentPercent[department["DepartmentName"]][location];
				};
			}
		])
})();