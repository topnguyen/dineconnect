﻿(function () {
	appModule.controller("tenant.views.connect.report.salesperiod",
		[
			"$scope", "$uibModal", "uiGridConstants", "abp.services.app.salesByPeriodReport", "uiGridGroupingConstants", "stats", "abp.services.app.connectLookup", "abp.services.app.tenantSettings",
			function ($scope, $modal, uiGridConstants, salesByPeriodReportAppService, uiGridGroupingConstants, stats, connectLookupService,tenantSettingsService) {
				
				var vm = this;
				vm.loading = false;
				vm.requestParams = {
					skipCount: 0,
					maxResultCount: app.consts.grid.defaultPageSize,
					sorting: null,
				};
				vm.dateRangeOptions = app.createDateRangePickerOptions();
				vm.intiailizeOpenLocation = function () {
					vm.locationGroup = app.createLocationInputForSearch();
				};

				var todayAsString = moment().format("YYYY-MM-DD");
				vm.dateRangeModel = {
					startDate: todayAsString,
					endDate: todayAsString
				};


				vm.intiailizeOpenLocation();

				vm.lstDepartment = [];
				vm.getDepartments = function () {
					connectLookupService.getDepartments({
					}).success(function (result) {
						vm.lstDepartment = result.items;
					}).finally(function (result) {
					});
				};
				vm.getDepartments();
				vm.openLocation = function () {
					const modalInstance = $modal.open({
						templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
						controller: "tenant.views.connect.location.select.location as vm",
						backdrop: "static",
						keyboard: false,
						resolve: {
							location: function () {
								return vm.locationGroup;
							}
						}
					});
					modalInstance.result.then(function (result) {
						vm.locationGroup = result;
					});
				};
				vm.isUndefinedOrNull = function (val) {
					return angular.isUndefined(val) || val == null || val == '';
				};
				vm.exportToExcel = function (obj) {
					vm.disableExport = true;
					var myConfirmation = false;
					var input = vm.getDataInput(myConfirmation, obj);
					salesByPeriodReportAppService.getSalesReportToExport(input)
						.success(function (result) {
							if (result != null) {
								app.downloadTempFile(result);
							}
						}).finally(function () {
							abp.ui.clearBusy("#MyLoginForm");
							vm.disableExport = false;
						});
				}
				vm.decimals = null;
				vm.getDecimals = function () {
					tenantSettingsService.getAllSettings()
						.success(function (result) {
							vm.decimals = result.connect.decimals;
						}).finally(function () {
						});
				};
				vm.getData = function () {
					vm.loading = true;
					var input = vm.getDataInput();
					salesByPeriodReportAppService.getSalesReports(input)
						.success(function (result) {
							console.log('data sales', result);
							vm.data = result;
						}).finally(function () {
							vm.loading = false;
						});
				};

				vm.getDataInput = function (confirmation, obj) {
					return {
						startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
						endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
						maxResultCount: vm.requestParams.maxResultCount,
						sorting: vm.requestParams.sorting,
						runInBackground: confirmation,
						skipCount: vm.requestParams.skipCount,
						locationGroup: vm.locationGroup,
						exportOutputType: obj,
					};
				};
				// #region Builder

				vm.clear = function () {
					vm.dateRangeModel = {
						startDate: todayAsString,
						endDate: todayAsString
					};
					vm.getData();
				}

				// #endregion
				vm.processTime = function (input) {
					var times = input.split(':');
					return times[0] + ':' + times[1];
				}
			}
		])
})();