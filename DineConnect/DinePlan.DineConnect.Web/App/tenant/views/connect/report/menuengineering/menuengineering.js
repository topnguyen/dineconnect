﻿(function () {
    appModule.controller("tenant.views.connect.report.menuengineering",
        [
            "$scope", "$uibModal", "uiGridConstants", "abp.services.app.menuItem", "abp.services.app.commonLookup", 'abp.services.app.menuItem', 'abp.services.app.category', "abp.services.app.tenantSettings", 'abp.services.app.productGroup',
            function ($scope, $modal, uiGridConstants, menuitemService, commonLookup, menuItemService, categoryService, tenantSettingsService, productGroupService) {

                var vm = this;

                vm.loading = false;

                vm.requestParams = {
                    categories: [],
                    menuItems: [],
                    menuItemPortions: [],
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null,
                    isMenuItems: false
                };

                vm.dateRangeOptions = app.createDateRangePickerOptions();
                vm.$onInit = function () {
                    vm.getUsers();
                    vm.getLocations();
                    vm.getAll();
                }

                //vm.textTooltipDefault = 'Menu Item \n';
                vm.textTooltipDefault = '';
                vm.toolTipHouse = 'No data';
                vm.toolTipStar = 'No data';
                vm.toolTipDog = 'No data';
                vm.toolTipPuzzle = 'No data';
                vm.selectedGroups = [];
                vm.gridOptions = {
                    showColumnFooter: true,
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    columnDefs: [],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope,
                            function (grid, sortColumns) {
                                if (!sortColumns.length || !sortColumns[0].field) {
                                    vm.requestParams.sorting = null;
                                } else {
                                    vm.requestParams.sorting =
                                        sortColumns[0].field + " " + sortColumns[0].sort.direction;
                                }
                                vm.getAll();
                            });
                        gridApi.pagination.on.paginationChanged($scope,
                            function (pageNumber, pageSize) {
                                vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                                vm.requestParams.maxResultCount = pageSize;
                                vm.getAll();
                            });
                    },
                    data: []
                };
                vm.templateColumnDef = [
                    {
                        name: app.localize("Category"),
                        field: "categoryName"
                    },
                    {
                        name: app.localize("MenuItem"),
                        field: "menuItemName",
                    },
                    {
                        name: app.localize("Portion"),
                        field: "menuItemPortionName",
                    },
                    {
                        name: app.localize("Quantity"),
                        field: "quantity",
                        cellFilter: 'number: 2',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate:
                            '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'

                    },
                    {
                        name: app.localize("MenuMix%(QTY)"),
                        field: "menuMix",
                        cellFilter: 'number: 2',
                        customTreeAggregationFinalizerFn: function (aggregation) {
                            aggregation.rendered = (aggregation.value).toFixed(vm.decimals) + '%';
                        },
                    },
                    {
                        name: app.localize("FoodCost"),
                        field: "costPrice",
                        cellFilter: 'number: 2'
                    },
                    {
                        name: app.localize("Price"),
                        field: "price",
                        cellFilter: 'number: 2'
                    },
                    {
                        name: app.localize("ItemCM"),
                        field: "menuCM",
                        cellFilter: 'number: 2'
                    }
                    ,
                    {
                        name: app.localize("MenuCost"),
                        field: "menuCost",
                        cellFilter: 'number: 2',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate:
                            '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'

                    },
                    {
                        name: app.localize("MenuRevenue"),
                        field: "menuRevenue",
                        cellFilter: 'number: 2',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate:
                            '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'

                    },
                    {
                        name: app.localize("MenuCM"),
                        field: "menuCM",
                        cellFilter: 'number: 2',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate:
                            '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'
                    },
                    {
                        name: app.localize("CMCAT"),
                        field: "cmcat"
                    },
                    {
                        name: app.localize("MMCAT"),
                        field: "mmcat"
                    },
                    {
                        name: app.localize("MenuItemClass"),
                        field: "menuItemClass"
                    }];
                vm.templateColumnDefHidePortion = [
                    {
                        name: app.localize("Category"),
                        field: "categoryName"
                    },
                    {
                        name: app.localize("MenuItem"),
                        field: "menuItemName",
                    },
                    {
                        name: app.localize("Quantity"),
                        field: "quantity",
                        cellFilter: 'number: 2',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate:
                            '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'

                    },
                    {
                        name: app.localize("MenuMix%(QTY)"),
                        field: "menuMix",
                        cellFilter: 'number: 2',
                        customTreeAggregationFinalizerFn: function (aggregation) {
                            aggregation.rendered = (aggregation.value).toFixed(vm.decimals) + '%';
                        },
                    },
                    {
                        name: app.localize("FoodCost"),
                        field: "costPrice",
                        cellFilter: 'number: 2'
                    },
                    {
                        name: app.localize("Price"),
                        field: "price",
                        cellFilter: 'number: 2'
                    },
                    {
                        name: app.localize("ItemCM"),
                        field: "menuCM",
                        cellFilter: 'number: 2'
                    }
                    ,
                    {
                        name: app.localize("MenuCost"),
                        field: "menuCost",
                        cellFilter: 'number: 2',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate:
                            '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'

                    },
                    {
                        name: app.localize("MenuRevenue"),
                        field: "menuRevenue",
                        cellFilter: 'number: 2',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate:
                            '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'

                    },
                    {
                        name: app.localize("MenuCM"),
                        field: "menuCM",
                        cellFilter: 'number: 2',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate:
                            '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals }}</div>'
                    },
                    {
                        name: app.localize("CMCAT"),
                        field: "cmcat"
                    },
                    {
                        name: app.localize("MMCAT"),
                        field: "mmcat"
                    },
                    {
                        name: app.localize("MenuItemClass"),
                        field: "menuItemClass"
                    }];
                vm.intiailizeOpenLocation = function () {
                    vm.locationGroup = app.createLocationInputForSearch();
                };

                vm.intiailizeOpenLocation();

                vm.openLocation = function () {
                    const modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };


                vm.getMenuEngineeringInput = function (confirmation, obj) {
                    return {
                        startDate: moment(vm.dateRangeModel.startDate).lang("en").format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).lang("en").format($scope.format),
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting,
                        runInBackground: confirmation,
                        skipCount: vm.requestParams.skipCount,
                        locationGroup: vm.locationGroup,
                        exportOutputType: obj,
                        menuItemIds: vm.requestParams.menuItems,
                        menuItemPortionIds: vm.requestParams.menuItemPortions,
                        filterByCategory: vm.requestParams.categories,
                        filterByGroup: vm.selectedGroups,
                        isMenuItems: vm.requestParams.isMenuItems
                    };
                };

                vm.exportToExcel = function (obj) {
                    abp.ui.setBusy("#MyLoginForm");
                    menuitemService.getMenuEngineeringForExport(vm.getMenuEngineeringInput(false, obj))
                        .success(function (result) {
                            if (result != null) {
                                app.downloadTempFile(result);
                            }
                        }).finally(function () {
                            abp.ui.clearBusy("#MyLoginForm");
                            vm.disableExport = false;
                        });
                };
                vm.exportToExcelByLocation = function (obj) {
                    abp.ui.setBusy("#MyLoginForm");
                    menuitemService.getMenuEngineeringByLocationForExport(vm.getMenuEngineeringInput(false, obj))
                        .success(function (result) {
                            if (result != null) {
                                app.downloadTempFile(result);
                            }
                        }).finally(function () {
                            abp.ui.clearBusy("#MyLoginForm");
                            vm.disableExport = false;
                        });
                };
                vm.exportToExcelByMonth = function (obj) {
                    abp.ui.setBusy("#MyLoginForm");
                    menuitemService.getMenuEngineeringByMonthForExport(vm.getMenuEngineeringInput(false, obj))
                        .success(function (result) {
                            if (result != null) {
                                app.downloadTempFile(result);
                            }
                        }).finally(function () {
                            abp.ui.clearBusy("#MyLoginForm");
                            vm.disableExport = false;
                        });
                };

                vm.countMenuItem = 0;
                vm.menuFoodCost = 0;
                vm.averageCM = 0;
                vm.percentageMenuMix = 0;
                vm.getAll = function () {
                    vm.loading = true;                   
                    menuitemService.getMenuEngineeringReport(vm.getMenuEngineeringInput()).success(function (result) {
                        if (vm.requestParams.isMenuItems) {
                            vm.gridOptions.columnDefs = vm.templateColumnDefHidePortion;
                        }
                        else {
                            vm.gridOptions.columnDefs = vm.templateColumnDef;
                        }
                     
                        vm.gridOptions.totalItems = result.countMenuItem;
                        vm.gridOptions.data = result.listItem;                      
                        vm.countMenuItem = result.countMenuItem;
                        vm.menuFoodCost = result.menuFoodCost;
                        vm.averageCM = result.averageCM;
                        vm.percentageMenuMix = result.percentageMenuMix;

                        vm.toolTipHouse = (result.dashboard.horse.length > 0 ? vm.textTooltipDefault + result.dashboard.horse : 'No data');
                        vm.toolTipStar = (result.dashboard.star.length > 0 ? vm.textTooltipDefault + result.dashboard.star : 'No data');
                        vm.toolTipDog = (result.dashboard.dog.length > 0 ? vm.textTooltipDefault + result.dashboard.dog : 'No data');
                        vm.toolTipPuzzle = (result.dashboard.puzzle.length > 0 ? vm.textTooltipDefault + result.dashboard.puzzle : 'No data');

                        console.log('result', result);
                    }).finally(function () {
                        vm.loading = false;
                    });
                };
                // #region Builder
                vm.locationsCode = [];

                vm.getLocations = function () {
                    commonLookup.getLocationCodeForCombobox({
                    }).success(function (result) {
                        vm.locationsCode = result.items;
                    }).finally(function (result) {
                    });

                    commonLookup.getLocationForCombobox({
                    }).success(function (result) {
                        vm.locations = result.items;
                    }).finally(function (result) {
                    });
                };
                vm.getUsers = function () {
                    commonLookup.getUserForCombobox({
                    }).success(function (result) {
                        vm.users = result.items;
                    }).finally(function (result) {
                    });
                };

                var todayAsString = moment().format("YYYY-MM-DD");
                vm.clear = function () {
                    vm.countMenuItem = 0;
                    vm.menuFoodCost = 0;
                    vm.averageCM = 0;
                    vm.percentageMenuMix = 0;
                    vm.categoriesSelect = [];
                    vm.requestParams.categories = [];
                    vm.menuItemsSelected = [];
                    vm.requestParams.menuItems = [];
                    vm.requestParams.menuItemPortions = [];
                    vm.menuItemPortionsSelected = [];
                    vm.selectedGroups = [];
                    vm.requestParams.isMenuItems = false;
                    vm.dateRangeModel = {
                        startDate: todayAsString,
                        endDate: todayAsString
                    };
                    vm.getAll();
                }

                vm.menuItemsSelected = [];
                vm.selectMenuItem = function () {
                    var requestParams = {
                        skipCount: 0,
                        maxResultCount: app.consts.grid.defaultPageSize,
                        sorting: null
                    };

                    var dataOptions = {
                        title: app.localize('Select') + ' ' + app.localize('MenuItem'),
                        serviceMethod: menuItemService.getAll,
                        selectedData: vm.menuItemsSelected,
                        requestParams: requestParams
                    };

                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/common/selectModal.cshtml",
                        controller: "tenant.views.connect.report.selectModal as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            dataOptions: function () {
                                return dataOptions;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        if (result.length > 0) {
                            console.log('menu item selected', result);
                            vm.menuItemsSelected = result;
                            vm.requestParams.menuItems = vm.menuItemsSelected.map(el => el.id);
                        }
                    });
                }

                vm.resetMenuItemSelectedItem = function () {
                    vm.menuItemsSelected = [];
                    vm.requestParams.menuItems = [];
                };

                vm.menuItemPortionsSelected = [];
                vm.selectMenuItemPortions = function () {
                    var requestParams = {
                        skipCount: 0,
                        maxResultCount: app.consts.grid.defaultPageSize,
                        sorting: null
                    };

                    var dataOptions = {
                        title: app.localize('Select') + ' ' + app.localize('MenuItemPortions'),
                        serviceMethod: menuItemService.getAllMenuItemPortions,
                        selectedData: vm.menuItemPortionsSelected,
                        requestParams: requestParams
                    };

                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/common/selectModal.cshtml",
                        controller: "tenant.views.connect.report.selectModal as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            dataOptions: function () {
                                return dataOptions;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        if (result.length > 0) {
                            console.log('menu item selected', result);
                            vm.menuItemPortionsSelected = result;
                            vm.requestParams.menuItemPortions = vm.menuItemPortionsSelected.map(el => el.id);
                        }
                    });
                }

                vm.resetMenuItemPortionsSelectedItem = function () {
                    vm.menuItemPortionsSelected = [];
                    vm.requestParams.menuItemPortions = [];
                };



                vm.categoriesSelect = [];
                vm.selectCategory = function () {
                    var requestParams = {
                        skipCount: 0,
                        maxResultCount: app.consts.grid.defaultPageSize,
                        sorting: null
                    };

                    var dataOptions = {
                        title: app.localize('Select') + ' ' + app.localize('Category'),
                        serviceMethod: categoryService.getAll,
                        selectedData: vm.categoriesSelect,
                        requestParams: requestParams
                    };

                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/common/selectModal.cshtml",
                        controller: "tenant.views.connect.report.selectModal as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            dataOptions: function () {
                                return dataOptions;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        if (result.length > 0) {
                            console.log('menu item selected', result);
                            vm.categoriesSelect = result;
                            vm.requestParams.categories = vm.categoriesSelect.map(el => el.id);
                        }
                    });
                }

                vm.resetCategorySelectedItem = function () {
                    vm.categoriesSelect = [];
                    vm.requestParams.categories = [];
                };
                vm.decimals = null;
                vm.getDecimals = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.decimals = result.connect.decimals;
                        }).finally(function () {
                        });
                };

                vm.$onInit = function () {
                    fillDropDownGroup();
                };
                vm.productGroups = [];
                function fillDropDownGroup() {
                    productGroupService.getProductGroupNames({})
                        .success(function (result) {
                            vm.productGroups = result.items;
                        });
                }

                // #endregion
            }
        ]);

    appModule.directive('tooltip', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.hover(function () {
                    // on mouseenter
                    element.tooltip('show');
                }, function () {
                    // on mouseleave
                    element.tooltip('hide');
                });
            }
        };
    });
})();