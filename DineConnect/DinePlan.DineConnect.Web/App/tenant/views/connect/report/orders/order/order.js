﻿(function () {
	appModule.controller('tenant.views.connect.report.orders.order', [
		'$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.ticket',
		'abp.services.app.connectReport', 'abp.services.app.location', "abp.services.app.tenantSettings", "abp.services.app.commonLookup", 'abp.services.app.department', "abp.services.app.commonLookup",
		function ($scope, $modal, uiGridConstants, ticketService, connectService, locationService, tenantSettingsService, commonService, departmentService, commonLookup) {
			var vm = this;

			$scope.$on('$viewContentLoaded', function () {
				App.initAjax();
			});
			vm.payments = [];
			vm.transactions = [];
			$scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
			$scope.format = $scope.formats[0];
																														  
			vm.loading = false;
			vm.advancedFiltersAreShown = false;

			vm.totalAmount = '';
			vm.totalOrders = '';
			vm.totalItems = '';

			vm.requestParams = {
				locations: '',
				terminalName: '',
				lastModifiedUserName: '',
				skipCount: 0,
				maxResultCount: app.consts.grid.defaultPageSize,
				sorting: null,
				void: false,
				gift: false,
				refund: false,
				comp: false
			};

			var todayAsString = moment().format('YYYY-MM-DD');
			vm.dateRangeOptions = app.createDateRangePickerOptions();
			vm.dateRangeModel = {
				startDate: todayAsString,
				endDate: todayAsString
			};

			vm.decimals = null;
			vm.getDecimals = function () {
				tenantSettingsService.getAllSettings()
					.success(function (result) {
						vm.decimals = result.connect.decimals;
					}).finally(function () {
					});
			};

			vm.$onInit = function () {
				localStorage.clear();
				vm.getDecimals();
				vm.getMenuItems();
				vm.getDepartments();
				vm.getLocations();
				vm.getUsers();
			}

			vm.gridOptions = {
				showColumnFooter: true,
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				columnDefs: [
					{
						name: 'Actions',
						enableSorting: false,
						width: 50,
						headerCellTemplate: '<span></span>',
						cellTemplate:
							'<div class=\"ui-grid-cell-contents text-center\">' +
							'  <button class="btn btn-default btn-xs" ng-click="grid.appScope.showDetails(row.entity)"><i class="fa fa-search"></i></button>' +
							'</div>'
					},
					{
						name: app.localize('Ticket'),
						field: 'ticketId'
					},
					{
						name: app.localize('Location'),
						field: 'locationName'
					},
					{
						name: app.localize('MenuItem'),
						field: 'menuItemName'
					},
					{
						name: app.localize('Portion'),
						field: 'portionName'
					},
					{
						name: app.localize('Quantity'),
						field: 'quantity',
						cellClass: 'ui-ralign',
						aggregationType: uiGridConstants.aggregationTypes.sum,
						footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue()}}</div>'
					},
					{
						name: app.localize('Price'),
						field: 'price',
						cellClass: 'ui-ralign',
						aggregationType: uiGridConstants.aggregationTypes.sum,
						footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
					},
					{
						name: app.localize('User'),
						field: 'creatingUserName'
					}
				],
				onRegisterApi: function (gridApi) {
					$scope.gridApi = gridApi;
					$scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
						if (!sortColumns.length || !sortColumns[0].field) {
							vm.requestParams.sorting = null;
						} else {
							vm.requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
						}
						vm.getOrders();
					});
					gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
						vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
						vm.requestParams.maxResultCount = pageSize;
						vm.getOrders();
					});
				},
				data: []
			};

			vm.toggleFooter = function () {
				$scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
				$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
			};

			vm.toggleColumnFooter = function () {
				$scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
				$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
			};
			vm.locationsCode = [];
			// #region Builder

			$scope.builder =
				app.createBuilder(
					{
						condition: "AND",
						rules: []
					},
					angular.fromJson
						(
							[
								{
									"id": "TicketId",
									"label": app.localize("Ticket"),
									"operators": [
										"equal",
										"less",
										"less_or_equal",
										"greater",
										"greater_or_equal"
									],
									"type": "integer"
								},
								{
									"id": "Location.Name",
									"label": app.localize("Location"),
									"type": "string",
									"operators": [
										"equal",
										"not_equal",
									],
									plugin: 'selectize',
									plugin_config: {
										valueField: 'value',
										labelField: 'displayText',
										searchField: 'displayText',
										sortField: 'displayText',
										create: true,
										maxItems: 1,
										plugins: ['remove_button'],
										onInitialize: function () {
											var that = this;
											if (vm.locations.length > 0) {
												vm.locations.forEach(function (item) {
													item.value = item.displayText;
													that.addOption(item);
												});
											} else {
												commonLookup.getLocationForCombobox({
												}).success(function (result) {
													vm.locations = result.items;
													vm.locations.forEach(function (item) {
														item.value = item.displayText;
														that.addOption(item);
													});
												}).finally(function (result) {
												});
											}
										}
									},
									valueSetter: function (rule, value) {
										rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
									}
								},
								{
									id: 'MenuItemId',
									"label": app.localize("Menu Item"),
									"operators": [
										"equal",
										"not_equal",
									],
									type: 'integer',
									plugin: 'selectize',
									plugin_config: {
										valueField: 'value',
										labelField: 'displayText',
										searchField: 'displayText',
										sortField: 'displayText',
										create: true,
										maxItems: 1,
										plugins: ['remove_button'],
										onInitialize: function () {
											var that = this;
											JSON.parse(localStorage.menuItems).forEach(function (item) {
												that.addOption(item);
											});
										}
									},
									valueSetter: function (rule, value) {
										rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
									}
								},
								{
									"id": "portionName",
									"label": app.localize("Portion"),
									"type": "string"
								},
								{
									"id": "Quantity",
									"label": app.localize("Quantity"),
									"operators": [
										"equal",
										"less",
										"less_or_equal",
										"greater",
										"greater_or_equal"
									],
									"type": "double",
									"validation": {
										"min": 0,
										"step": 0.01
									}
								},
								{
									"id": "Price",
									"label": app.localize("Price"),
									"operators": [
										"equal",
										"less",
										"less_or_equal",
										"greater",
										"greater_or_equal"
									],
									"type": "double",
									"validation": {
										"min": 0,
										"step": 0.01
									}
								},
								{
									"id": "CreatingUserName",
									"label": app.localize("User"),
									"type": "string",
									"operators": [
										"equal",
										"not_equal",
									],
									type: 'string',
									plugin: 'selectize',
									plugin_config: {
										valueField: 'value',
										labelField: 'displayText',
										searchField: 'displayText',
										sortField: 'displayText',
										create: true,
										maxItems: 1,
										plugins: ['remove_button'],
										onInitialize: function () {
											var that = this;
											if (vm.users.length > 0) {
												vm.users.forEach(function (item) {
													item.value = item.displayText;
													that.addOption(item);
												});
											} else {
												commonLookup.getUserForCombobox({
												}).success(function (result) {
													vm.users = result.items;
													vm.users.forEach(function (item) {
														item.value = item.displayText;
														that.addOption(item);
													});
												}).finally(function (result) {
												});
											}
										}
									},
									valueSetter: function (rule, value) {
										rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
									}
								}
							]
						)
				);


			// #endregion
			// #region LocalStorage

			vm.getMenuItems = function () {
				commonService.getMenuItemsForCombobox({
				}).success(function (result) {
					localStorage.menuItems = JSON.stringify(result.items);
				}).finally(function (result) {
				});
			};

			vm.getDepartments = function () {
				departmentService.getDepartmentsForCombobox({
				}).success(function (result) {
					localStorage.departments = JSON.stringify(result.items);
				}).finally(function (result) {
				});
			};

			// #endregion
			vm.getOrders = function () {
				vm.loading = true;
				connectService.getOrders({
					startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
					endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
					locationGroup: vm.locationGroup,
					refund: vm.requestParams.refund,
					terminalName: vm.requestParams.terminalName,
					lastModifiedUserName: vm.requestParams.lastModifiedUserName,
					skipCount: vm.requestParams.skipCount,
					maxResultCount: vm.requestParams.maxResultCount,
					sorting: vm.requestParams.sorting,
					void: vm.requestParams.void,
					gift:vm.requestParams.gift,
					comp:vm.requestParams.comp,
					dynamicFilter: angular.toJson($scope.builder.builder.getRules())
				})
					.success(function (result) {
						vm.gridOptions.totalItems = result.orderViewList.totalCount;

						for (let i = 0; i < result.orderViewList.items.length; i++) {
							if (result.orderViewList.items[i].price != null)
								result.orderViewList.items[i].price = result.orderViewList.items[i].price.toFixed(vm.decimals);
						}

						vm.gridOptions.data = result.orderViewList.items;

						vm.totalAmount = result.dashBoardDto.totalAmount;
						vm.totalOrders = result.dashBoardDto.totalOrderCount;
						vm.totalItems = result.dashBoardDto.totalItemSold;

					}).finally(function () {
						vm.loading = false;
					});
			};

			vm.getOrderTagReport = function (obj) {
				abp.ui.setBusy('#MyLoginForm');
				connectService.getOrderReportWithTagExcel({
					startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
					endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
					locationGroup: vm.locationGroup,
					refund: vm.requestParams.refund,
					terminalName: vm.requestParams.terminalName,
					lastModifiedUserName: vm.requestParams.lastModifiedUserName,
					skipCount: vm.requestParams.skipCount,
					maxResultCount: vm.requestParams.maxResultCount,
					sorting: vm.requestParams.sorting,
					void: vm.requestParams.void,
					gift: vm.requestParams.gift,
					comp: vm.requestParams.comp,
					exportOutputType: obj
				})
					.success(function (result) {
						app.downloadTempFile(result);
						abp.ui.clearBusy('#MyLoginForm');
					});
			};


			vm.exportToExcel = function (obj) {
				if (vm.gridOptions.totalItems === 0) return;
				abp.ui.setBusy("#MyLoginForm");
				abp.message.confirm(app.localize("AreYouSure"),
					app.localize("RunInBackground"),
					function (isConfirmed) {
						var myConfirmation = false;
						if (isConfirmed) {
							myConfirmation = true;
						}
						vm.disableExport = true;
						connectService.getOrdersExcel({
							startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
							endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
							locationGroup: vm.locationGroup,
							refund: vm.requestParams.refund,
							terminalName: vm.requestParams.terminalName,
							lastModifiedUserName: vm.requestParams.lastModifiedUserName,
							skipCount: vm.requestParams.skipCount,
							maxResultCount: vm.requestParams.maxResultCount,
							sorting: vm.requestParams.sorting,
							exportOutputType: obj,
							runInBackground: myConfirmation,
							void: vm.requestParams.void,
							gift: vm.requestParams.gift,
							comp: vm.requestParams.comp,
							dynamicFilter: angular.toJson($scope.builder.builder.getRules())
						})
							.success(function (result) {
								if (result != null) {
									app.downloadTempFile(result);
								}
							}).finally(function () {
								vm.disableExport = false;
								abp.ui.clearBusy("#MyLoginForm");
							});

					});

			};

			vm.showDetails = function (order) {
				$modal.open({
					templateUrl: '~/App/tenant/views/connect/report/tickets/ticket/ticketModal.cshtml',
					controller: 'tenant.views.connect.report.ticketModal as vm',
					backdrop: 'static',
					resolve: {
						ticket: function () {
							return order.ticketId;
						}
					}
				});
			};

			vm.intiailizeOpenLocation = function () {
				vm.locationGroup = app.createLocationInputForSearch();
			};
			vm.intiailizeOpenLocation();

			vm.openLocation = function () {
				var modalInstance = $modal.open({
					templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
					controller: "tenant.views.connect.location.select.location as vm",
					backdrop: "static",
					keyboard: false,
					resolve: {
						location: function () {
							return vm.locationGroup;
						}
					}
				});
				modalInstance.result.then(function (result) {
					vm.locationGroup = result;
				});
			}
			vm.getLocations = function () {
				commonLookup.getLocationCodeForCombobox({
				}).success(function (result) {
					vm.locationsCode = result.items;
					console.log('code', vm.locationsCode);
				}).finally(function (result) {
				});

				commonLookup.getLocationForCombobox({
				}).success(function (result) {
					vm.locations = result.items;
					console.log(vm.locations);
				}).finally(function (result) {
				});
			};
			vm.getUsers = function () {
				commonLookup.getUserForCombobox({
				}).success(function (result) {
					vm.users = result.items;
				}).finally(function (result) {
				});
			};

			vm.clear = function () {
				vm.dateRangeModel = {
					startDate: todayAsString,
					endDate: todayAsString
				};
				vm.requestParams.void = false;
				vm.requestParams.gift = false;
				vm.requestParams.comp = false;
				vm.requestParams.lastModifiedUserName = "";
				vm.requestParams.terminalName = "";
				$scope.builder.builder.reset();
				vm.getOrders();
			}


		}
	]);
})();