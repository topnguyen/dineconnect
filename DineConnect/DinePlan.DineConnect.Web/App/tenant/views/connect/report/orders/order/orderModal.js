﻿(function () {
    appModule.controller('tenant.views.connect.report.orderModal', [
        '$scope', '$uibModalInstance', 'ticket', 'abp.services.app.ticket',
    function ($scope, $modalInstance, ticket,ticketService) {
            var vm = this;
            vm.close = function () {
                $modalInstance.dismiss();
            };

            vm.getOrders = function () {
                vm.loading = true;
                ticketService.getInputTicket({ id: ticket.ticketId })
                    .success(function (result) {
                        vm.ticket = result;
                        vm.ctime = moment(vm.ticket.ticketCreatedTime).format('LLL');
                        vm.otime = moment(vm.ticket.lastOrderTime).format('LLL');
                        vm.ptime = moment(vm.ticket.lastPaymentTime).format('LLL');
                    }).finally(function () {
                        vm.loading = false;
                    });
            };

            vm.getOrders();
        }
    ]);
})();