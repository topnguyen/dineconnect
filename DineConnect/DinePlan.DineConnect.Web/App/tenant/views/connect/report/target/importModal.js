﻿(function () {
    appModule.controller('tenant.views.connect.report.target.importModal', [
        '$scope', 'appSession', '$uibModalInstance', 'FileUploader', 'abp.services.app.saleTarget', 'locationGroup',
        function ($scope, appSession, $uibModalInstance, fileUploader, targetService, locationGroup) {
            var vm = this;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null,
                userId: abp.session.userId
            };
            vm.locationGroup = locationGroup;
            vm.fileName = "";
            vm.importpath = function () {
                console.log(vm.locationGroup);
                vm.loading = true;
                targetService.getSaleTargetTemplateAsync({
                    locationInput: {
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterLocationText,
                        userId: requestParams.userId
                    },
                    locationGroup: vm.locationGroup,
                    exportOutputType: 0

                }).success(function (result) {
                    if (result != null) {
                        app.downloadTempFile(result);
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            }
            vm.uploader = new fileUploader({
                formData: [
                    {
                        "IsLocal": true
                    },
                ],
                url: abp.appPath + 'FileUpload/UploadFileSalesTarget',
                queueLimit: 1,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        if ('|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ImportTemplate_Warn_FileType'));
                            return false;
                        }
                        return true;
                    }
                }]
            });

            vm.save = function () {
                vm.loading = true;
                vm.uploader.uploadAll();
            };
            
            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.error !== null) {
                    abp.message.warn(response.error.message);
                    //$uibModalInstance.dismiss();
                    vm.loading = false;
                    return;
                }
                vm.fileName = response.result.fileName;
                abp.notify.info("Finished");
                $uibModalInstance.close(vm.fileName);
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }

    ]);
})();