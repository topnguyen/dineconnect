﻿(function () {
    appModule.controller("tenant.views.connect.report.saletarget", [
        "$scope", "$uibModal", "uiGridConstants", "abp.services.app.saleTarget", "abp.services.app.tenantSettings",
        function ($scope, $modal, uiGridConstants, targetService, tenantSettingsService) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });
            vm.locations = [];

            vm.fileName = "";

            vm.currencyText = abp.features.getValue("DinePlan.DineConnect.Connect.Currency");
            vm.loading = false;
            vm.advancedFiltersAreShown = false;
            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];

            vm.datetimeFormat = "YYYY-MM-DD HH:mm:ss";
            vm.dateFormat = "YYYY-MM-DD";

            vm.getDatetimeFormat = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.datetimeFormat = result.connect.dateTimeFormat;
                        vm.dateFormat = result.connect.simpleDateFormat;
                    });
            };

            vm.formatDatetime = function (value) {
                if (value) {
                    var format = moment(value).format(vm.datetimeFormat.toUpperCase());
                    return format;
                }
                return null;
            }
            vm.formatDate = function (value) {
                if (value) {
                    var format = moment(value).format(vm.dateFormat.toUpperCase());
                    return format;
                }
                return null;
            }
    
            vm.dayDetails = [];

            vm.requestParams = {
                locations: "",
                void: false,
                gift: false,
                refund: false,
                terminalName: "",
                lastModifiedUserName: "",
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };
            vm.getEditionValue = function (item) {
                return item.displayText;
            };

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    });
            };

            vm.$onInit = function () {
                vm.getDecimals(); 
                vm.getDatetimeFormat();
            }

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            }

            vm.gridOptions = {
                showColumnFooter: true,
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize("Date"),
                        field: "date",
                        cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.formatDate (row.entity.date)}}</div>',
                    },
                    {
                        name: app.localize("LocationCode"),
                        field: "locationCode",
                    },
                    {
                        name: app.localize("LocationName"),
                        field: "locationName",
                    },
                    {
                        name: app.localize("Day"),
                        field: "day",
                    },
                    {
                        name: app.localize("SalesTarget"),
                        field: "saleTarget",
                        cellClass: "ui-ralign",
                        aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    },
                    {
                        name: app.localize("SalesValue"),
                        field: "saleValue",
                        cellClass: "ui-ralign",
                        aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    },
                    {
                        name: app.localize("Difference"),
                        field: "difference",
                        cellClass: "ui-ralign",
                        aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%',
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        vm.getResult();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;
                        vm.getResult();
                    });
                },
                data: []

            };

            vm.toggleFooter = function () {
                $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.toggleColumnFooter = function () {
                $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.importSaleTarget = function () {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/report/target/importModal.cshtml',
                    controller: 'tenant.views.connect.report.target.importModal as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        locationGroup: function () {
                            return vm.locationGroup;
                        },                        
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.fileName = result;
                    vm.getResult();
                });
            };            
            vm.getResult = function () {
                vm.loading = true;
                targetService.getAllAsync({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationGroup: vm.locationGroup,
                    void: vm.requestParams.void,
                    gift: vm.requestParams.gift,
                    refund: vm.requestParams.refund,
                    terminalName: vm.requestParams.terminalName,
                    lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    fileName: vm.fileName,
                    dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                }).success(function (result) {
                    vm.gridOptions.totalItems = result.totalCount;

                    for (let i = 0; i < result.items.length; i++) {
                        if (result.items[i].saleTarget != null)
                            result.items[i].saleTarget = result.items[i].saleTarget.toFixed(vm.decimals);

                        if (result.items[i].saleValue != null)
                            result.items[i].saleValue = result.items[i].saleValue.toFixed(vm.decimals);

                        if (result.items[i].difference != null)
                            result.items[i].difference = result.items[i].difference.toFixed(vm.decimals);
                    }

                    vm.gridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.exportToSummary = function (mode) {
                if (vm.gridOptions.totalItems === 0) return;
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        targetService.getSaleTargetExcelAsync({
                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                            locationGroup: vm.locationGroup,
                            void: vm.requestParams.void,
                            gift: vm.requestParams.gift,
                            refund: vm.requestParams.refund,
                            terminalName: vm.requestParams.terminalName,
                            lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                            skipCount: vm.requestParams.skipCount,
                            maxResultCount: vm.requestParams.maxResultCount,
                            sorting: vm.requestParams.sorting,
                            exportOutputType: mode,
                            runInBackground: myConfirmation,
                            fileName: vm.fileName,
                            dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                        })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });
                    });
            };
            vm.clear = function () {
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };
                $scope.builder.builder.reset();
                vm.getResult();
            };

            $scope.builder =
                app.createBuilder(
                    {
                        condition: "AND",
                        rules: []
                    },
                    angular.fromJson
                        (
                            [
                                {
                                    "id": "LocationCode",
                                    "label": app.localize("LocationCode"),
                                    "type": "string",
                                },
                                {
                                    "id": "LocationName",
                                    "label": app.localize("LocationName"),
                                    "type": "string",
                                },
                                {
                                    "id": "Day",
                                    "label": app.localize("Day"),
                                    "type": "string",
                                },
                                {
                                    "id": "SalesTarget",
                                    "label": app.localize("SalesTarget"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                                {
                                    "id": "SalesValue",
                                    "label": app.localize("SalesValue"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                                {
                                    "id": "Difference",
                                    "label": app.localize("Difference"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                }
                            ]
                        )
                );

        }
    ]);
})();