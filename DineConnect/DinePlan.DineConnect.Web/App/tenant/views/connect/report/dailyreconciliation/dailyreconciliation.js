﻿(function () {
    appModule.controller("tenant.views.connect.report.dailyreconciliation.dailyreconciliation",
        [
            "$scope", "$uibModal", "uiGridConstants", "abp.services.app.connectReport", "abp.services.app.tenantSettings", "abp.services.app.centralReport",
            function ($scope, $modal, uiGridConstants, connectService, tenantSettingsService, centralReportAppService) {
                var vm = this;

                $scope.$on("$viewContentLoaded",
                    function () {
                        App.initAjax();
                    });
                vm.locations = [];
                vm.payments = [];
                vm.transactions = [];
                $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
                $scope.format = $scope.formats[0];
                vm.loading = false;
                vm.advancedFiltersAreShown = false;

                vm.requestParams = {
                    locations: "",
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null
                };

                var todayAsString = moment().format("YYYY-MM-DD");
                vm.dateRangeOptions = app.createDateRangePickerOptions();
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };

                vm.decimals = null;
                vm.getDecimals = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.decimals = result.connect.decimals;
                        }).finally(function () {
                        });
                };

                vm.$onInit = function () {
                    vm.getDecimals();
                    vm.getDatetimeFormat();
                };
                vm.datetimeFormat = "YYYY-MM-DD HH:mm:ss";
                vm.dateFormat = "YYYY-MM-DD";

                vm.getDatetimeFormat = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.datetimeFormat = result.connect.dateTimeFormat;
                            vm.dateFormat = result.connect.simpleDateFormat;
                        });
                };

                vm.formatDatetime = function (value) {
                    if (value) {
                        var format = moment(value).format(vm.datetimeFormat.toUpperCase());
                        return format;
                    }
                    return null;
                }
                vm.formatDate = function (value) {
                    if (value) {
                        var format = moment(value).format(vm.dateFormat.toUpperCase());
                        return format;
                    }
                    return null;
                }

                vm.gridOptions = {
                    showColumnFooter: true,
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    columnDefs: [
                        {
                            name: app.localize("LocationName"),
                            field: "locationName",
                            enableSorting: true
                        },
                        {
                            name: app.localize("LocationCode"),
                            field: "locationCode",
                            enableSorting: true
                        },
                        {
                            name: app.localize("WorkDay"),
                            field: "reportStartDay",
                            enableSorting: true,
                            cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.formatDate (row.entity.reportStartDay)}}</div>',
                        },
                        {
                            name: "Actions",
                            enableSorting: false,
                            width: '400',
                            headerCellTemplate: "<span></span>",
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-primary btn-xs" ng-click="grid.appScope.showDailyreconciliation(row.entity)">Daily Reconciliation</button>' +
                                "</div>"
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope,
                            function (grid, sortColumns) {
                                if (!sortColumns.length || !sortColumns[0].field) {
                                    vm.requestParams.sorting = null;
                                } else {
                                    vm.requestParams.sorting =
                                        sortColumns[0].field + " " + sortColumns[0].sort.direction;
                                }
                                vm.getWorkdays();
                            });
                        gridApi.pagination.on.paginationChanged($scope,
                            function (pageNumber, pageSize) {
                                vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                                vm.requestParams.maxResultCount = pageSize;
                                vm.getWorkdays();
                            });
                    },
                    data: []
                };

                vm.toggleFooter = function () {
                    $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                };

                vm.toggleColumnFooter = function () {
                    $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                };

                vm.getWorkdays = function () {
                    vm.loading = true;
                    connectService.getWorkPeriodByLocationDailyForDatesWithPaging({
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        locations: vm.locationGroup.locations,
                        skipCount: vm.requestParams.skipCount,
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting
                    })
                        .success(function (result) {
                            vm.gridOptions.totalItems = result.totalCount;

                            for (let i = 0; i < result.items.length; i++) {
                                if (result.items[i].total != null)
                                    result.items[i].total = result.items[i].total.toFixed(vm.decimals);
                            }

                            vm.gridOptions.data = result.items;

                        }).finally(function () {
                            vm.loading = false;
                        });
                };
                vm.disableExport = false;

                vm.getDailyReconciliationData = function () {
                    vm.loading = true;
                    centralReportAppService.getDailyReconciliationAll({
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        locations: vm.locationGroup.locations,
                        locationGroup: vm.locationGroup
                    }).success(function (result) {
                        vm.allData = result;
                        console.log("All Data", vm.allData);
                    }).finally(function () {
                        vm.loading = false;
                    });
                };

                //vm.exportToExcel = function (mode) {
                //    abp.ui.setBusy("#MyLoginForm");
                //    abp.message.confirm(app.localize("AreYouSure"),
                //        app.localize("RunInBackground"),
                //        function (isConfirmed) {
                //            var myConfirmation = false;
                //            if (isConfirmed) {
                //                myConfirmation = true;
                //            }
                //            vm.disableExport = true;
                //            categoryReportService.getCategoryExcel({
                //                startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                //                endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                //                locationGroup: vm.locationGroup,
                //                skipCount: vm.requestParams.skipCount,
                //                maxResultCount: vm.requestParams.maxResultCount,
                //                sorting: vm.requestParams.sorting,
                //                runInBackground: myConfirmation,
                //                exportOutputType: mode
                //            })
                //                .success(function (result) {
                //                    if (result != null && !myConfirmation)
                //                        app.downloadTempFile(result);
                //                }).finally(function () {
                //                    abp.ui.clearBusy("#MyLoginForm");
                //                    vm.disableExport = false;
                //                });
                //        });


                //};
                vm.showDailyreconciliation = function (workday) {
                    $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/dailyreconciliation/dailyreconciliationModal.cshtml",
                        controller: "tenant.views.connect.report.dailyreconciliation.dailyreconciliationModal as vm",
                        backdrop: "static",
                        resolve: {
                            workday: function () {
                                return workday;
                            }
                        }
                    });
                };

                vm.showAll = function () {
                    $modal.open({
                        templateUrl: "~/App/tenant/views/connect/report/dailyreconciliation/dailyreconciliationAllModal.cshtml",
                        controller: "tenant.views.connect.report.dailyreconciliation.dailyreconciliationAllModal as vm",
                        backdrop: "static",
                        windowClass: 'daily-reconciliation-all-modal',
                        resolve: {
                            startDate: function () {
                                return moment(vm.dateRangeModel.startDate).format($scope.format);
                            },
                            endDate: function () {
                                return moment(vm.dateRangeModel.endDate).format($scope.format);
                            },
                            locations: function () {
                                return vm.locationGroup.locations;
                            },
                        }
                    });
                };

                vm.intiailizeOpenLocation = function () {
                    vm.locationGroup = app.createLocationInputForSearch();
                };
                vm.intiailizeOpenLocation();

                vm.openLocation = function () {
                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };

                vm.exportToSummary = function (mode) {
                    vm.disableExport = true;
                    abp.ui.setBusy("#MyLoginForm");
                    abp.message.confirm(app.localize("AreYouSure"), app.localize("RunInBackground"),
                        function (isConfirmed) {
                            var myConfirmation = false;
                            if (isConfirmed) {
                                myConfirmation = true;
                            }
                            vm.disableExport = true;

                            centralReportAppService.getDailyReconciliationAllExport({
                                startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                                endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                                locations: vm.locationGroup.locations,
                                locationGroup: vm.locationGroup,
                                exportOutputType: mode,
                                runInBackground: myConfirmation
                            }).success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });
                        });
                };

            }
        ]);
})();