﻿(function () {
	appModule.controller("tenant.views.connect.report.monthlySalesAdvanceReport",
		[
			"$scope", "$uibModal", "uiGridConstants", "abp.services.app.monthlySalesAdvanceReport", "uiGridGroupingConstants", "stats", "abp.services.app.connectLookup", "abp.services.app.tenantSettings",
			function ($scope, $modal, uiGridConstants, monthlySalesAdvanceReport, uiGridGroupingConstants, stats, connectLookupService, tenantSettingsService) {

				var vm = this;
				vm.loading = false;
				vm.requestParams = {
					skipCount: 0,
					maxResultCount: app.consts.grid.defaultPageSize,
					sorting: null,
				};
				vm.dateRangeOptions = app.createDateRangePickerOptions();
				vm.intiailizeOpenLocation = function () {
					vm.locationGroup = app.createLocationInputForSearch();
				};

				var todayAsString = moment().format("YYYY-MM-DD");
				vm.dateRangeModel = {
					startDate: todayAsString,
					endDate: todayAsString
				};
				vm.decimals = 2;
				vm.getDecimals = function () {
					tenantSettingsService.getAllSettings()
						.success(function (result) {
							vm.decimals = result.connect.decimals;
						}).finally(function () {
						});
				};
				vm.getDecimals();
				vm.intiailizeOpenLocation();

				vm.lstDepartment = [];
				vm.getDepartments = function () {
					connectLookupService.getDepartments({
					}).success(function (result) {
						vm.lstDepartment = result.items;
					}).finally(function (result) {
					});
				};
				vm.getDepartments();
				vm.openLocation = function () {
					const modalInstance = $modal.open({
						templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
						controller: "tenant.views.connect.location.select.location as vm",
						backdrop: "static",
						keyboard: false,
						resolve: {
							location: function () {
								return vm.locationGroup;
							}
						}
					});
					modalInstance.result.then(function (result) {
						vm.locationGroup = result;
					});
				};


				vm.exportToExcel = function (obj) {
					abp.ui.setBusy("#MyLoginForm");
					abp.message.confirm(app.localize("AreYouSure"),
						app.localize("RunInBackground"),
						function (isConfirmed) {
							var myConfirmation = false;
							if (isConfirmed) {
								myConfirmation = true;
							}
							vm.disableExport = true;
							monthlySalesAdvanceReport.getMonthlySalesReportsToExport(vm.getDataInput(myConfirmation, obj))
								.success(function (result) {
									if (result != null) {
										app.downloadTempFile(result);
									}
								}).finally(function () {
									abp.ui.clearBusy("#MyLoginForm");
									vm.disableExport = false;
								});
						});
				};
		
				vm.getData = function () {
					
					vm.loading = true;
					monthlySalesAdvanceReport.getMonthlySalesReports(vm.getDataInput())
						.success(function (result) {
							vm.data = result;
							vm.food = result.foodList;
							vm.beverage = result.beverageList;
							vm.others = result.other1;
							vm.totalFBItem = result.totalFBItem;
							vm.grossSales = result.grossSales;
							vm.netSales = result.netSales;
							vm.cover = result.cover;
							console.log('monthly time', result)
						}).finally(function () {
							vm.loading = false;
						});
				};

				$scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
				$scope.format = $scope.formats[0];
				vm.getDataInput = function (confirmation, obj) {
					return {
						startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
						endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
						maxResultCount: vm.requestParams.maxResultCount,
						sorting: vm.requestParams.sorting,
						runInBackground: confirmation,
						skipCount: vm.requestParams.skipCount,
						locationGroup: vm.locationGroup,
						exportOutputType: obj,
					};
				};
				// #region Builder

				vm.clear = function () {
					vm.dateRangeModel = {
						startDate: todayAsString,
						endDate: todayAsString
					};
					vm.getData();
				}

				// #endregion
				vm.processTime = function (input) {
					var times = input.split(':');
					return times[0] + ':' + times[1];
				}
			}
		])
})();