﻿(function () {
    appModule.controller("tenant.views.connect.report.monthlysalestaxreport", [
        "$scope", "$uibModal", "uiGridConstants", "abp.services.app.customize", "abp.services.app.location", 'uiGridGroupingConstants', "abp.services.app.tenantSettings", "abp.services.app.connectLookup","abp.services.app.commonLookup",
        function ($scope, $modal, uiGridConstants, customizeService, locationService, uiGridGroupingConstants, tenantSettingsService, connectLookupService, commonLookupService) {
            var vm = this;

                $scope.$on("$viewContentLoaded", function () {
                    App.initAjax();
                });
                vm.locations = [];
                vm.currencyText = abp.features.getValue("DinePlan.DineConnect.Connect.Currency");
                vm.loading = false;
                vm.advancedFiltersAreShown = false;
                $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
                $scope.format = $scope.formats[0];

                vm.decimals = null;
                vm.getDecimals = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.decimals = result.connect.decimals;
                        });
                };

                vm.$onInit = function () {
                    vm.getDecimals();
                };

                vm.payments = [];

                vm.payments = [];
                vm.transactions = [];
                vm.ticketNumber = '';
                vm.isMoreThanOneReprintChecked = false;
                vm.saleStatus = 'All';
                vm.loading = false;
                vm.advancedFiltersAreShown = false;
                $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
                $scope.format = $scope.formats[0];
                vm.minDate = new Date();
                vm.selectedCondition = null;
                vm.selectedMonthAndYear = null;
                vm.selectedPOS = null;
                vm.locations = [];

                vm.changeCondition = function () {
                    console.log(vm.selectedCondition);
                }
                $('input[name="selectDate"]').daterangepicker({
                    locale: {
                        format: $scope.format
                    },
                    singleDatePicker: true,
                    showDropdowns: true
                });

                vm.requestParams = {
                    locations: "",
                    payments: [],
                    departments: "",
                    transactions: "",
                    ticketTags: "",
                    terminalName: "",
                    lastModifiedUserName: "",
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null,
                    userId: abp.session.userId,
                    credit: false,
                    refund: false,
                    ticketNo: ""
                };

                var todayAsString = moment().format("YYYY-MM-DD");
                vm.dateRangeOptions = app.createDateRangePickerOptions();
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };
                vm.selectedDate = todayAsString;

                vm.date = todayAsString;

                $scope.$on("$viewContentLoaded",
                    function () {
                        App.initAjax();
                    });



                vm.getEditionValue = function (item) {
                    return item.displayText;
                };

                vm.decimals = null;
                vm.getDecimals = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.decimals = result.connect.decimals;
                        }).finally(function () {
                        });
                };



                vm.$onInit = function () {
                    localStorage.clear();
                    vm.getDecimals();
                    vm.getTransactions();
                    vm.getPayments();
                    vm.getDepartments();
                    vm.CreateListMonthYear();
                    vm.getPOS();
                };
                vm.lstMonthYear = [];
                vm.CreateListMonthYear = function () {
                    let result = [];
                    for (let year = 2010; year <= 2050; year++) {
                        for (let j = 1; j <= 12; j++) {
                            let month = j < 10 ? '0' + j : j;
                            let input = month + '/' + year;
                            vm.lstMonthYear.push(input);
                        }
                    }
                }


                // #region Grid
                vm.gridOptions = {
                    showColumnFooter: true,
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    appScopeProvider: vm,
                    columnDefs: [
                        {
                            name: app.localize('Month'),
                            field: 'month',
                            grouping: { groupPriority: 1 },
                            minWidth: 100,
                            //sort: { priority: 1, direction: 'asc' },
                            cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>',
                            footerCellTemplate: '<div class="ui-grid-cell-contents" > <h5> ' + app.localize("Total") + '</h5></div>'
                        },
                        {
                            name: app.localize('Date'),
                            field: 'date',
                            //grouping: { groupPriority: 1 },
                            minWidth: 100,
                            //sort: { priority: 1, direction: 'asc' },
                            //cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>',
                            //footerCellTemplate: '<div class="ui-grid-cell-contents" > <h5> ' + app.localize("Total") + '</h5></div>'
                        },
                        {
                            name: app.localize('POS'),
                            field: 'pos',
                            minWidth: 100,
                            //grouping: { groupPriority: 2 },
                        },
                        {
                            name: app.localize("TicketNumber"),
                            field: "ticketNumber",
                            minWidth: 100,
                            cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>'
                        },
                        //{
                        //    name: app.localize("Discription"),
                        //    field: "discription",
                        //    width: 300,
                        //},
                        {
                            name: app.localize("Value Before VAT"),
                            field: "valueBeforeVAT",
                            minWidth: 150,
                            cellFilter: 'number:grid.appScope.decimals',
                            treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                            customTreeAggregationFinalizerFn: function (aggregation) {
                                aggregation.rendered = aggregation.value;
                            },
                            footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                        },
                        {
                            name: app.localize("Vat"),
                            field: "vat",
                            cellClass: "ui-ralign",
                            width: 150,
                            cellFilter: 'number:grid.appScope.decimals',
                            treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                            customTreeAggregationFinalizerFn: function (aggregation) {
                                aggregation.rendered = aggregation.value;
                            },
                            footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                        },
                        {
                            name: app.localize("Amount"),
                            field: "amount",
                            cellClass: "ui-ralign",
                            width: 150,
                            cellFilter: 'number:grid.appScope.decimals',
                            treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                            customTreeAggregationFinalizerFn: function (aggregation) {
                                aggregation.rendered = aggregation.value;
                            },
                            //cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.getQtyPercent(row.entity.quantity)}}</div>',
                            footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >{{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                        },
                        {
                            name: app.localize("ReferentReceiptNumber"),
                            field: "referenceReceiptNumber",
                            width: 150,
                        },
                    ],
                    onRegisterApi: function (gridApi) {
                    },
                    data: []
                };
                vm.disableExport = false;

                vm.toggleFooter = function () {
                    $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                };

                vm.toggleColumnFooter = function () {
                    $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                };

                // #endregion
                // #region Location

                vm.intiailizeOpenLocation = function () {
                    vm.locationGroup = app.createLocationInputForSearch();
                };

                vm.intiailizeOpenLocation();

                vm.openLocation = function () {
                    const modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };

                // #endregion

                
                vm.getData = function () {
                        vm.loading = true;
                        vm.totalQuantity = 0;
                        vm.totalAmount = 0;
                  
                    customizeService.getMonthlySalesTaxReport(
                            vm.monthlySalesTaxReportInput(false, null)
                        ).success(function (result) {
                            for (let i = 0; i < result.items.length; i++) {
                                vm.totalAmount += result.items[i].total && result.items[i].total;
                                vm.totalQuantity += result.items[i].quantity && result.items[i].quantity;
                                result.items[i].total = result.items[i].total && result.items[i].total.toFixed(vm.decimals);
                            }

                            vm.gridOptions.data = result.items;
                            console.log("data", result.items);

                        }).finally(function () {
                            vm.loading = false;
                        });
            };

                // #region Exports
                vm.exportToExcel = function (obj) {
                    vm.disableExport = true;
                    customizeService.getMonthlySalesTaxReportExport(vm.monthlySalesTaxReportInput(false, obj))
                        .success(function (result) {
                            if (result != null) {
                                app.downloadTempFile(result);
                            }
                        }).finally(function () {
                            vm.disableExport = false;
                        });
                };
                //  #endregion
                vm.monthlySalesTaxReportInput = function (confirmation, obj) {

                    let selectedMonthAndYear = null;
                    let startDate = null;
                    let endDate = null;
                    let selectedDate = null;
                    let pos = null;

                    if (vm.selectedCondition === "selectedMonthAndYear") {
                        selectedMonthAndYear = vm.selectedMonthAndYear;
                    } else if (vm.selectedCondition === "selectedDateRange") {
                        startDate = moment(vm.dateRangeModel.startDate).lang("en").format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).lang("en").format($scope.format);
                    } else if (vm.selectedCondition === "selectedDate") {
                        startDate = moment(vm.selectedDate).lang("en").format($scope.format);
                        endDate = moment(vm.selectedDate).lang("en").format($scope.format);
                    } else if (vm.selectedCondition === "selectedPOS") {
                        pos = vm.selectedPOS;
					}

                    return {
                        startDate: startDate,
                        endDate: endDate,
                        locationGroup: vm.locationGroup,
                        payments: vm.requestParams.payments,
                        department: vm.requestParams.department,
                        transactions: vm.requestParams.transactions,
                        promotions: vm.requestParams.promotions,
                        void: vm.requestParams.void,
                        gift: vm.requestParams.gift,
                        comp: vm.requestParams.comp,
                        refund: vm.requestParams.refund,
                        skipCount: vm.requestParams.skipCount,
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting,
                        dynamicFilter: $scope.builder.builder.getRules() ? angular.toJson($scope.builder.builder.getRules()) : "",
                        selectedMonthAndYear: selectedMonthAndYear,
                        exportOutputType: obj,
                        pos: pos
                    };
                };

              

                // #region Builder

                $scope.builder =
                    app.createBuilder(
                        {
                            condition: "AND",
                            rules: [
                                {
                                    id: "TotalAmount",
                                    operator: "greater_or_equal",
                                    value: 0
                                }
                            ]
                        },
                        angular.fromJson
                            (
                                [
                                    {
                                        "id": "TotalAmount",
                                        "label": app.localize("TotalAmount"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.1
                                        }
                                    },
                                    {
                                        "id": "TicketNumber",
                                        "label": app.localize("TicketNumber"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "ReferenceNumber",
                                        "label": app.localize("ReferenceNumber"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "TicketTags",
                                        "label": app.localize("TicketTags"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "TicketStates",
                                        "label": app.localize("TicketStates"),
                                        "type": "string"
                                    },

                                    {
                                        id: 'DepartmentName',
                                        "label": app.localize("DepartmentName"),
                                        type: 'string',
                                        plugin: 'selectize',
                                        plugin_config: {
                                            valueField: 'value',
                                            labelField: 'displayText',
                                            searchField: 'displayText',
                                            sortField: 'displayText',
                                            create: true,
                                            maxItems: 1,
                                            plugins: ['remove_button'],
                                            onInitialize: function () {
                                                var that = this;
                                                JSON.parse(localStorage.departments).forEach(function (item) {
                                                    that.addOption(item);
                                                });
                                            }
                                        },
                                        valueSetter: function (rule, value) {
                                            rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                        }
                                    },


                                    {
                                        id: 'Transactions.TransactionType.Name',
                                        label: app.localize("TransactionType"),
                                        type: 'string',
                                        plugin: 'selectize',
                                        plugin_config: {
                                            valueField: 'value',
                                            labelField: 'displayText',
                                            searchField: 'displayText',
                                            sortField: 'displayText',
                                            create: true,
                                            maxItems: 1,
                                            plugins: ['remove_button'],
                                            onInitialize: function () {
                                                var that = this;
                                                JSON.parse(localStorage.transactionTypes).forEach(function (item) {
                                                    that.addOption(item);
                                                });
                                            }
                                        },
                                        valueSetter: function (rule, value) {
                                            rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                        }
                                    },
                                    {
                                        id: 'Payments.PaymentType.Name',
                                        label: app.localize("PaymentType"),
                                        type: 'string',
                                        plugin: 'selectize',
                                        plugin_config: {
                                            valueField: 'value',
                                            labelField: 'displayText',
                                            searchField: 'displayText',
                                            sortField: 'displayText',
                                            create: true,
                                            maxItems: 1,
                                            plugins: ['remove_button'],
                                            onInitialize: function () {
                                                var that = this;
                                                JSON.parse(localStorage.paymentTypes).forEach(function (item) {
                                                    that.addOption(item);
                                                });
                                            }
                                        },
                                        valueSetter: function (rule, value) {
                                            rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                        }
                                    }
                                    ,
                                    {
                                        "id": "Credit",
                                        "label": app.localize("Credit"),
                                        type: 'boolean'

                                    },
                                    {
                                        "id": "Refund",
                                        "label": app.localize("Refund"),
                                        type: 'boolean'

                                    },
                                    {
                                        "id": "LastModifiedUserName",
                                        "label": app.localize("User"),
                                        "type": "string"
                                    }

                                ]
                            )
                    );
                // #endregion

                // #region LocalStorage
                vm.getTransactions = function () {
                    connectLookupService.getTransactionTypesForCombobox({
                    }).success(function (result) {
                        localStorage.transactionTypes = JSON.stringify(result.items);
                    }).finally(function (result) {
                    });
                };
                vm.getPayments = function () {
                    connectLookupService.getPaymentTypes({
                    }).success(function (result) {
                        localStorage.paymentTypes = JSON.stringify(result.items);
                    }).finally(function (result) {
                    });
                };
                vm.getDepartments = function () {
                    connectLookupService.getDepartments({
                    }).success(function (result) {
                        localStorage.departments = JSON.stringify(result.items);
                    }).finally(function (result) {
                    });
                };
                vm.getPOS = function () {
                commonLookupService.getLocationForCombobox().success(function (result) {
                    vm.locations = result.items;
                }).finally(function (result) {
                    });
                };
                // #endregion

            }
        ]);
})();