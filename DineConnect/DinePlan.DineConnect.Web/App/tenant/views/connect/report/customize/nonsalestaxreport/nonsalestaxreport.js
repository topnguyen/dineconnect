﻿(function () {
    appModule.controller("tenant.views.connect.report.nonsalestaxreport", [
        "$scope", "$uibModal", "uiGridConstants", "abp.services.app.customize", "abp.services.app.location", 'uiGridGroupingConstants', "abp.services.app.tenantSettings", "abp.services.app.connectLookup", "abp.services.app.commonLookup",
        function ($scope, $modal, uiGridConstants, customizeService, locationService, uiGridGroupingConstants, tenantSettingsService, connectLookupService, commonLookupService) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });
            vm.locations = [];
            vm.currencyText = abp.features.getValue("DinePlan.DineConnect.Connect.Currency");
            vm.loading = false;
            vm.advancedFiltersAreShown = false;
            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    });
            };

            vm.$onInit = function () {
                vm.getDecimals();
            };

            vm.payments = [];

            vm.payments = [];
            vm.transactions = [];
            vm.ticketNumber = '';
            vm.isMoreThanOneReprintChecked = false;
            vm.saleStatus = 'All';
            vm.loading = false;
            vm.advancedFiltersAreShown = false;
            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];
            vm.minDate = new Date();
            vm.selectedCondition = null;
            vm.selectedMonthAndYear = null;
            vm.selectedPOS = null;
            vm.locations = [];

            vm.changeCondition = function () {
                console.log(vm.selectedCondition);
            }
            $('input[name="selectDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true
            });

            vm.requestParams = {
                locations: "",
                payments: [],
                departments: "",
                transactions: "",
                ticketTags: "",
                terminalName: "",
                lastModifiedUserName: "",
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null,
                userId: abp.session.userId,
                credit: false,
                refund: false,
                ticketNo: ""
            };

            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };
            vm.selectedDate = todayAsString;

            vm.date = todayAsString;

            $scope.$on("$viewContentLoaded",
                function () {
                    App.initAjax();
                });



            vm.getEditionValue = function (item) {
                return item.displayText;
            };

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    }).finally(function () {
                    });
            };



            vm.$onInit = function () {
                localStorage.clear();
                vm.getDecimals();
                vm.getTransactions();
                vm.getPayments();
                vm.getDepartments();
                vm.CreateListMonthYear();
                vm.getPOS();
            };
            vm.lstMonthYear = [];
            vm.CreateListMonthYear = function () {
                let result = [];
                for (let year = 2010; year <= 2050; year++) {
                    for (let j = 1; j <= 12; j++) {
                        let month = j < 10 ? '0' + j : j;
                        let input = month + '/' + year;
                        vm.lstMonthYear.push(input);
                    }
                }
            }


            // #region Grid
            vm.gridOptions = {
                showColumnFooter: true,
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize('POS'),
                        field: 'pos',
                        grouping: { groupPriority: 1 },
                        minWidth: 100,
                        //sort: { priority: 1, direction: 'asc' },
                        cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>',
                        footerCellTemplate: '<div class="ui-grid-cell-contents" > <h5> ' + app.localize("Total") + '</h5></div>'
                    },
                    {
                        name: app.localize('Date'),
                        field: 'date',
                        minWidth: 100,
                        grouping: { groupPriority: 2 },
                    },
                    {
                        name: app.localize("ABB.NO"),
                        field: "abbno",
                        minWidth: 100,
                        cellTemplate: '<div><div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div></div>'
                    },
                    {
                        name: app.localize("Discription"),
                        field: "discription",
                        width: 300,
                    },
                    {
                        name: app.localize("Value Before VAT"),
                        field: "valueBeforeVAT",
                        minWidth: 150,
                        cellFilter: 'number:grid.appScope.decimals',
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                        customTreeAggregationFinalizerFn: function (aggregation) {
                            aggregation.rendered = aggregation.value;
                        },
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    },
                    {
                        name: app.localize("Vat"),
                        field: "vat",
                        cellClass: "ui-ralign",
                        width: 150,
                        cellFilter: 'number:grid.appScope.decimals',
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                        customTreeAggregationFinalizerFn: function (aggregation) {
                            aggregation.rendered = aggregation.value;
                        },
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    },
                    {
                        name: app.localize("Amount"),
                        field: "amount",
                        cellClass: "ui-ralign",
                        width: 150,
                        cellFilter: 'number:grid.appScope.decimals',
                        treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                        customTreeAggregationFinalizerFn: function (aggregation) {
                            aggregation.rendered = aggregation.value;
                        },
                        //cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{grid.appScope.getQtyPercent(row.entity.quantity)}}</div>',
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >{{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    },
                    {
                        name: app.localize("ReferentReceiptNumber"),
                        field: "referenceReceiptNumber",
                        width: 150,
                    },
                ],
                onRegisterApi: function (gridApi) {
                },
                data: []
            };
            vm.disableExport = false;

            vm.toggleFooter = function () {
                $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            vm.toggleColumnFooter = function () {
                $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
            };

            // #endregion
            // #region Location

            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForSearch();
            };

            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                const modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            // #endregion

            vm.getData = function () {
                vm.loading = true;
                vm.totalQuantity = 0;
                vm.totalAmount = 0;

                customizeService.getNonSalesTaxReport(
                    vm.saleTaxReportReportInput(false, null)
                ).success(function (result) {
                    vm.dataRoot = result;
                    console.log("data", result);

                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.exportToExcel = function (obj) {
                vm.disableExport = true;
                customizeService.getNonSalesTaxReportExport(vm.saleTaxReportReportInput(false, obj))
                    .success(function (result) {
                        if (result != null) {
                            app.downloadTempFile(result);
                        }
                    }).finally(function () {
                        vm.disableExport = false;
                    });
            };

            // #region Exports
            vm.saleTaxReportReportInput = function (confirmation, obj) {

                let selectedMonthAndYear = null;
                let startDate = moment(vm.dateRangeModel.startDate).lang("en").format($scope.format);
                let endDate = moment(vm.dateRangeModel.endDate).lang("en").format($scope.format);
                let selectedDate = null;
                let pos = null;
                return {
                    startDate: startDate,
                    endDate: endDate,
                    locationGroup: vm.locationGroup,
                    payments: vm.requestParams.payments,
                    department: vm.requestParams.department,
                    transactions: vm.requestParams.transactions,
                    promotions: vm.requestParams.promotions,
                    void: vm.requestParams.void,
                    gift: vm.requestParams.gift,
                    comp: vm.requestParams.comp,
                    refund: vm.requestParams.refund,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    dynamicFilter: $scope.builder.builder.getRules() ? angular.toJson($scope.builder.builder.getRules()) : "",
                    selectedMonthAndYear: selectedMonthAndYear,
                    exportOutputType: obj,
                    pos: pos
                };
            };



            // #region Builder

            $scope.builder =
                app.createBuilder(
                    {
                        condition: "AND",
                        rules: []
                    },
                    angular.fromJson
                        (
                            [
                                {
                                    "id": "abbno",
                                    "label": app.localize("ABB.NO"),
                                    "type": "string"
                                },
                                {
                                    "id": "Description",
                                    "label": app.localize("Description"),
                                    "type": "string"
                                },
                                {
                                    "id": "ValueBeforeVAT",
                                    "label": app.localize("ValueBeforeVAT"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                                {
                                    "id": "Vat",
                                    "label": app.localize("Vat"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "step": 0.01
                                    }
                                },
                                {
                                    "id": "Amount",
                                    "label": app.localize("Amount"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "type": "double",
                                    "validation": {
                                        "min": 0,
                                        "step": 0.01
                                    }
                                },
                                {
                                    "id": "ReferenceReceiptNumber",
                                    "label": app.localize("ReferenceReceiptNumber"),
                                    "type": "string"
                                },
                                //{
                                //    "id": "Location",
                                //    "label": app.localize("Location"),
                                //    type: 'string'
                                //},
                                {
                                    "id": "pos",
                                    "label": app.localize("Terminal"),
                                    type: 'string'
                                },
                                {
                                    "id": "Date",
                                    "label": app.localize("Date"),
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                    "placeholder": 'MM/DD/YYYY',
                                }
                            ]
                        )
                );


            // #endregion
            // #region LocalStorage

            vm.getTransactions = function () {
                connectLookupService.getTransactionTypesForCombobox({
                }).success(function (result) {
                    localStorage.transactionTypes = JSON.stringify(result.items);
                }).finally(function (result) {
                });
            };
            vm.getPayments = function () {
                connectLookupService.getPaymentTypes({
                }).success(function (result) {
                    localStorage.paymentTypes = JSON.stringify(result.items);
                }).finally(function (result) {
                });
            };
            vm.getDepartments = function () {
                connectLookupService.getDepartments({
                }).success(function (result) {
                    localStorage.departments = JSON.stringify(result.items);
                }).finally(function (result) {
                });
            };
            vm.getPOS = function () {
                commonLookupService.getLocationForCombobox().success(function (result) {
                    vm.locations = result.items;
                }).finally(function (result) {
                });
            };

            vm.clear = function () {
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };
                $scope.builder.builder.reset();
                vm.getData();
            };

            // #endregion

        }
    ]);
})();