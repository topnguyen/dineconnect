﻿(function () {
    appModule.controller("tenant.views.connect.report.hour.summary", [
        "$scope", "$uibModal", "uiGridConstants", "abp.services.app.connectReport", "abp.services.app.location",
        "abp.services.app.connectLookup", "abp.services.app.paymentType", "abp.services.app.tenantSettings",
        function ($scope, $modal, uiGridConstants, connectService, locationService, connectLookupService,
            paymentService, tenantSettingsService) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });
            vm.locations = [];

            vm.currencyText = abp.features.getValue("DinePlan.DineConnect.Connect.Currency");
            vm.loading = false;
            vm.advancedFiltersAreShown = false;
            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];

            vm.dayDetails = [];
            vm.requestParams = {
                locations: "",
                payments: [],
                department: "",
                transactions: "",
                terminalName: "",
                lastModifiedUserName: "",
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };
            vm.getEditionValue = function (item) {
                return item.displayText;
            };
            vm.excel = function () {
                var myModel = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/report/hour/summary/exportModal.cshtml',
                    controller: 'tenant.views.connect.report.hour.summary.exportModal as vm',
                    backdrop: 'static',
                    resolve: {
                        dto: function () {
                            abp.ui.setBusy('#MyLoginForm');
                            return {
                                startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                                endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                                locationGroup: vm.locationGroup,
                                payments: vm.requestParams.payments,
                                department: vm.requestParams.department,
                                transactions: vm.requestParams.transactions,
                                terminalName: vm.requestParams.terminalName,
                                lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                                skipCount: vm.requestParams.skipCount,
                                maxResultCount: vm.requestParams.maxResultCount,
                                sorting: vm.requestParams.sorting
                            };
                        }
                    }
                });
            };

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    }).finally(function () {
                    });
            };

            vm.$onInit = function () {
                vm.getDecimals();
            }

            vm.getResult = function () {
                vm.loading = true;
                connectService.getHourlySalesSummary({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    locationGroup: vm.locationGroup,
                    payments: vm.requestParams.payments,
                    department: vm.requestParams.department,
                    transactions: vm.requestParams.transactions,
                    terminalName: vm.requestParams.terminalName,
                    lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting
                })
                    .success(function (result) {
                        if (result != null) {
                            if (result.hourList != null) {
                                vm.dayDetails = result.hourList[0].hours;

                                Highcharts.chart('daychart', {
                                    chart: {
                                        type: 'column'
                                    },
                                    title: {
                                        text: ''
                                    },
                                    xAxis: {
                                        categories: [
                                            '0',
                                            '1',
                                            '2',
                                            '3',
                                            '4',
                                            '5',
                                            '6',
                                            '7',
                                            '8',
                                            '9',
                                            '10',
                                            '11',
                                            '12',
                                            '13',
                                            '14',
                                            '15',
                                            '16',
                                            '17',
                                            '18',
                                            '19',
                                            '20',
                                            '21',
                                            '22',
                                            '23'
                                        ],
                                        crosshair: true
                                    },
                                    yAxis: {
                                        min: 0,
                                        title: {
                                            text: app.localize('Total')
                                        }
                                    },
                                    tooltip: {
                                        headerFormat: '<span style="font-size:12px">Hour : {point.key}</span><table>',
                                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                                        footerFormat: '</table>',
                                        shared: true,
                                        useHTML: true
                                    },
                                    plotOptions: {
                                        column: {
                                            pointPadding: 0.2,
                                            borderWidth: 0
                                        }
                                    },
                                    series: [{
                                        name: app.localize('Total'),
                                        data: $.parseJSON(JSON.stringify(result.chartTotal))
                                    },
                                    {
                                        name: app.localize('Tickets'),
                                        data: $.parseJSON(JSON.stringify(result.chartTicket))
                                    }]
                                });
                            }
                        }
                    }).finally(function () {
                        vm.loading = false;
                    });
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            }

            vm.departments = [];
            vm.getDepartments = function () {
                connectLookupService.getDepartments({
                }).success(function (result) {
                    vm.departments = result.items;
                }).finally(function (result) {
                });
            };
            vm.getDepartments();

            vm.payments = [];
            vm.getPayments = function () {
                connectLookupService.getPaymentTypes({
                }).success(function (result) {
                    vm.payments = result.items;
                }).finally(function (result) {
                });
            };

            vm.getPayments();

            vm.sum = function (items, prop) {
                if (items == null) {
                    return 0;
                }
                return items.reduce(function (a, b) {
                    return b[prop] == null ? a : a + b[prop];
                }, 0);
            };
        }
    ]);
})();