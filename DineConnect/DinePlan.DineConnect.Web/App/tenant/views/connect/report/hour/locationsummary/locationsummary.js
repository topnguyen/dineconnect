﻿(function() {
    appModule.controller("tenant.views.connect.report.hour.locationsummary", [
        "$scope", "$uibModal", "uiGridConstants", "abp.services.app.connectReport", "abp.services.app.location",
        "abp.services.app.connectLookup",
        function($scope, $modal, uiGridConstants, connectService, locationService, connectLookupService) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function() {
                App.initAjax();
            });
            vm.locations = [];

            vm.currencyText = abp.features.getValue("DinePlan.DineConnect.Connect.Currency");
            vm.loading = false;
            vm.advancedFiltersAreShown = false;
            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];

            vm.dayDetails = [];
            vm.requestParams = {
                locations: "",
                payments: "",
                department: "",
                transactions: "",
                terminalName: "",
                lastModifiedUserName: "",
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };
            vm.getEditionValue = function(item) {
                return item.displayText;
            };
            vm.exportToExcel = function() {

            };


            vm.getResult = function() {
                var allLocations = vm.requestParams.locations;
                if (vm.requestParams.locations == "") {
                    allLocations = vm.locations;
                }
                vm.loading = true;
                connectService.getLocationHourySales({
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        locations: allLocations,
                        payments: vm.requestParams.payments,
                        department: vm.requestParams.department,
                        transactions: vm.requestParams.transactions,
                        terminalName: vm.requestParams.terminalName,
                        lastModifiedUserName: vm.requestParams.lastModifiedUserName,
                        skipCount: vm.requestParams.skipCount,
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting
                    })
                    .success(function (result) {
                        if (result != null) {
                            if (result.hourList != null) {
                                console.log(result);
                                vm.dayDetails = result.hourList[0].hours;

                            }
                        }
                    }).finally(function() {
                        vm.loading = false;
                    });
            };

            vm.getLocations = function() {
                vm.loading = true;
                locationService.getLocationBasedOnUser({
                    userId: abp.session.userId
                }).success(function(result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                }).finally(function(result) {
                    vm.loading = false;
                });
            };

            vm.getLocations();

        }
    ]);
})();