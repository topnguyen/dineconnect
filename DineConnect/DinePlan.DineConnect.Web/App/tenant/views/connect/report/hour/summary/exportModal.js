﻿(function() {
    appModule.controller("tenant.views.connect.report.hour.summary.exportModal", [
        "$scope", "$uibModalInstance", "dto", "abp.services.app.connectReport",
        function($scope, $modalInstance, dto, connectService) {
            var vm = this;

            abp.ui.clearBusy("#MyLoginForm");

            vm.dto = dto;
            vm.duration = "D";
            vm.exportOutput = "T";

            vm.close = function() {
                $modalInstance.dismiss();
            };

            vm.exportToSummary = function (mode) {
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        connectService.getHourlySalesExport({
                                startDate: vm.dto.startDate,
                                endDate: vm.dto.endDate,
                                locationGroup: vm.dto.locationGroup,
                                payments: vm.dto.payments,
                                department: vm.dto.department,
                                transactions: vm.dto.transactions,
                                terminalName: vm.dto.terminalName,
                                lastModifiedUserName: vm.dto.lastModifiedUserName,
                                duration: vm.duration,
                                exportOutput: vm.exportOutput,
                                bylocation: vm.bylocation,
                                runInBackground: myConfirmation,
                                exportOutputType: mode
                            })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                                $modalInstance.dismiss();
                            });
                    });
            };
        }
    ]);
})();