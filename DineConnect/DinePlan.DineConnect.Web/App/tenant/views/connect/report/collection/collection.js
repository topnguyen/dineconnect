﻿(function () {
    appModule.controller("tenant.views.connect.report.collection",
        [
            "$scope", "$uibModal", "uiGridConstants", "abp.services.app.collectionReport", "abp.services.app.tenantSettings",
            function ($scope, $modal, uiGridConstants, collectionService, tenantSettingsService) {
                var vm = this;

                $scope.$on("$viewContentLoaded",
                    function () {
                        App.initAjax();
                    });
                vm.locations = [];
                vm.payments = [];
                vm.transactions = [];
                $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
                $scope.format = $scope.formats[0];
                vm.loading = false;
                vm.exporting = false;
                vm.advancedFiltersAreShown = false;

                vm.totalTicketAmount = "";
                vm.totalTickets = "";
                vm.totalOrders = "";
                vm.totalItems = "";

                vm.requestParams = {
                    locations: "",
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null
                };

                var todayAsString = moment().format("YYYY-MM-DD");
                vm.dateRangeOptions = app.createDateRangePickerOptions();
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };

                vm.decimals = null;
                vm.getDecimals = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.decimals = result.connect.decimals;
                        }).finally(function () {
                        });
                };

                vm.$onInit = function () {
                    vm.getDecimals();
                };

                vm.gridOptions = {
                    showColumnFooter: true,
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    columnDefs: [
                        {
                            name: app.localize("Location"),
                            field: "locationName",
                            minWidth: '150',
                            footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total:</div>'
                        },
                        {
                            name: app.localize("Sales"),
                            field: "itemSales",
                            cellFilter: 'number: grid.appScope.decimals',
                            cellClass: "ui-ralign",
                            aggregationType: uiGridConstants.aggregationTypes.sum,
                            footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                        },
                        {
                            name: app.localize("Discount"),
                            field: "ticketDiscountTotal",
                            cellFilter: 'number: grid.appScope.decimals',
                            cellClass: "ui-ralign",
                            aggregationType: uiGridConstants.aggregationTypes.sum,
                            footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue() | number: grid.appScope.decimals}}</div>'
                        },
                        {
                            name: app.localize("OrderDiscount"),
                            field: "itemDiscountTotal",
                            cellFilter: 'number: grid.appScope.decimals',
                            cellClass: "ui-ralign",
                            aggregationType: uiGridConstants.aggregationTypes.sum,
                            footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue() | number: grid.appScope.decimals}}</div>'
                        },
                        {
                            name: app.localize("NetSales"),
                            field: "subTotal",
                            cellFilter: 'number: grid.appScope.decimals',
                            cellClass: "ui-ralign",
                            aggregationType: uiGridConstants.aggregationTypes.sum,
                            footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue() | number: grid.appScope.decimals}}</div>'
                        },
                        {
                            name: app.localize("Tax"),
                            field: "tax",
                            cellFilter: 'number: grid.appScope.decimals',
                            cellClass: "ui-ralign",
                            aggregationType: uiGridConstants.aggregationTypes.sum,
                            footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue() | number: grid.appScope.decimals}}</div>'
                        },
                        {
                            name: app.localize("Total"),
                            field: "totalSales",
                            cellFilter: 'number: grid.appScope.decimals',
                            cellClass: "ui-ralign",
                            aggregationType: uiGridConstants.aggregationTypes.sum,
                            footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" > {{col.getAggregationValue() | number: grid.appScope.decimals}}</div>'
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope,
                            function (grid, sortColumns) {
                                if (!sortColumns.length || !sortColumns[0].field) {
                                    vm.requestParams.sorting = null;
                                } else {
                                    vm.requestParams.sorting =
                                        sortColumns[0].field + " " + sortColumns[0].sort.direction;
                                }
                                vm.getOrders();
                            });
                        gridApi.pagination.on.paginationChanged($scope,
                            function (pageNumber, pageSize) {
                                vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                                vm.requestParams.maxResultCount = pageSize;
                                vm.getOrders();
                            });
                    },
                    data: []
                };

                vm.toggleFooter = function () {
                    $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                };

                vm.toggleColumnFooter = function () {
                    $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                };
                vm.intiailizeOpenLocation = function() {
                    vm.locationGroup = app.createLocationInputForSearch();
                };
                vm.intiailizeOpenLocation();
                vm.getOrders = function () {
                    vm.loading = true;

                    collectionService.getCollectionReport({
                        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                        locationGroup: vm.locationGroup,
                        skipCount: vm.requestParams.skipCount,
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting,
                        dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                    })
                        .success(function (result) {
                            vm.gridOptions.totalItems = result.totalCount;
                            vm.gridOptions.data = result.items;

                            console.log(result);
                        }).finally(function () {
                            vm.loading = false;
                        });
                };
                vm.disableExport = false;

                vm.exportToExcel = function (mode) {
                    abp.ui.setBusy("#MyLoginForm");
                    abp.message.confirm(app.localize("AreYouSure"),
                        app.localize("RunInBackground"),
                        function (isConfirmed) {
                            var myConfirmation = false;
                            if (isConfirmed) {
                                myConfirmation = true;
                            }
                            vm.exporting = true;

                            collectionService.getCollectionReportForExport({
                                runInBackground: myConfirmation,
                                startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                                endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                                locationGroup: vm.locationGroup,
                                skipCount: vm.requestParams.skipCount,
                                maxResultCount: vm.requestParams.maxResultCount,
                                sorting: vm.requestParams.sorting
                            }).success(function (result) {
                                if (result != null && !myConfirmation)
                                    app.downloadTempFile(result);
                            }).finally(function () {
                                abp.ui.clearBusy("#MyLoginForm");
                                vm.disableExport = false;
                                vm.exporting = false;
                            });
                        });
                };
                

              

                vm.openLocation = function () {
                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };
                $scope.builder =
                    app.createBuilder(
                        {
                            condition: "AND",
                            rules: []
                        },
                        angular.fromJson
                            (
                                [
                                    {
                                        "id": "itemSales",
                                        "label": app.localize("Sales"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                    {
                                        "id": "ticketDiscountTotal",
                                        "label": app.localize("Discount"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "step": 0.01
                                        }
                                    },
                                    {
                                        "id": "itemDiscountTotal",
                                        "label": app.localize("OrderDiscount"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                    {
                                        "id": "subTotal",
                                        "label": app.localize("NetSales"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                  
                                    {
                                        "id": "Tax",
                                        "label": app.localize("Tax"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                    {
                                        "id": "totalSales",
                                        "label": app.localize("Total"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.01
                                        }
                                    },
                                ]
                            )
                    );

                vm.clear = function () {
                    vm.dateRangeModel = {
                        startDate: todayAsString,
                        endDate: todayAsString
                    };
                    $scope.builder.builder.reset();
                    vm.getOrders();
                }

            }
        ]);
})();