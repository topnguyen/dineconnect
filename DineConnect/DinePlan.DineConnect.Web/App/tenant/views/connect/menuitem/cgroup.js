﻿
(function() {
    appModule.controller("tenant.views.connect.menu.combo.creategroup", [
        "$scope", "$uibModalInstance", "group", 
        function ($scope, $modalInstance, group) {

            var vm = this;
            vm.saving = false;
            vm.group = group;

            vm.save = function() {
                $modalInstance.result = vm.group;
                $modalInstance.close();
            };
            vm.cancel = function() {
                $modalInstance.dismiss();
            };
        }
    ]);
})();