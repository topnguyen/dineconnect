﻿(function () {
    appModule.controller("tenant.views.connect.menu.menuitem.detail", [
        "$scope", "$uibModal", "$state", "$stateParams", "abp.services.app.menuItem", 'abp.services.app.category',
        "abp.services.app.connectLookup", "abp.services.app.location", 'FileUploader', 
        function ($scope, $modal, $state, $stateParams, menuService, categoryService, lookupService, locationService, fileUploader) {
            var vm = this;
            vm.locationPrices = [];
            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });

            $scope.settings = {
                dropdownToggleState: false,
                time: {
                    fromHour: '06',
                    fromMinute: '30',
                    toHour: '23',
                    toMinute: '30'
                },
                noRange: false,
                format: 24,
                noValidation: true
            };
            vm.urbanPiperEnabled = abp.features.isEnabled('DinePlan.DineConnect.Connect.UrbanPiper');

            vm.removeSchedule = function (productIndex) {
                vm.menuitem.menuItemSchedules.splice(productIndex, 1);
            };
            vm.refday = [];
            vm.addSchedule = function () {
                vm.menuitem.menuItemSchedules.push({
                    'startHour': $scope.settings.time.fromHour,
                    'endHour': $scope.settings.time.toHour,
                    'startMinute': $scope.settings.time.fromMinute,
                    'endMinute': $scope.settings.time.toMinute,
                    'allDays': vm.refday
                });

                vm.refday = [];
            };

            vm.menuItemId = $stateParams.id;
            vm.menuitem = null;
            vm.categories = [];
            vm.replyFiles = [];
            vm.refreshImage = false;
            vm.allComboItems = [];
            vm.countPortions = 0;
            vm.foodTypes = [];
            vm.transactionTypes = [];
            vm.locations = [];
            vm.selectedBarcodes = [];
            vm.filterText = $stateParams.filter;
            vm.skipCount = $stateParams.page;

            vm.productType = 1;
            vm.showComboItems = 0;
            vm.selectedComboIndex = 0;
            vm.selectedComboGroupIndex = 0;
            vm.selectedComboItemIndex = 0;
            vm.combos = [];
            vm.barCodes = ["12", "123", "312"];

            var countryName = abp.features.getValue('DinePlan.DineConnect.Connect.Country');
            vm.gstenabledforIndia = false;
            if (countryName === 'IN') {
                vm.gstenabledforIndia = true;
            }
            vm.removeRow = function (productIndex) {
                vm.menuitem.portions.splice(productIndex, 1);
                vm.countPortions = vm.menuitem.portions.length;
            };
            vm.updateProducType = function () {
                if (vm.productType == 2 && vm.countPortions > 1) {
                    var count = vm.countPortions;
                    for (var i = 1; i < count ; i++) {
                        vm.menuitem.portions.splice(1, 1);                 
                    }
                }
                vm.countPortions = vm.menuitem.portions.length;
            }
            vm.addUp = function () {
                vm.menuitem.upMenuItems.push({ 'id': 0, 'refMenuItemId': 0, 'addBaseProductPrice': false, 'addQuantity': 1 });
            };
            vm.removeUp = function (productIndex) {
                vm.menuitem.upMenuItems.splice(productIndex, 1);
            };

            vm.addUpItems = function () {
                vm.menuitem.upSingleMenuItems.push({ 'id': 0, 'refMenuItemId': 0, 'name': '', 'addBaseProductPrice': false });
            };
            vm.removeUpItem = function (upIdex) {
                console.log(upIdex);
                console.log(vm.menuitem.upSingleMenuItems);
                vm.menuitem.upSingleMenuItems.splice(upIdex, 1);
                console.log(vm.menuitem.upSingleMenuItems);
            };
            vm.addPortion = function () {
                 vm.menuitem.portions.push({ 'id': 0, 'name': "NORMAL", 'aliasName': "NULL", 'multiPlier': 1, 'price': 0, 'costPrice': 0, 'preparationTime': 0, 'numberOfPax': 0});            
                vm.countPortions = vm.menuitem.portions.length;
            };
            vm.addComboGroup = function () {
                addorEditGroup({ 'id': 0, 'name': "", 'color': "", 'sortOrder': vm.combos.length, 'minimum': 1, 'maximum': 1, 'comboItems': [] }, false);
            };

            vm.editExistingComboGroup = function () {
                vm.comboGroup = {
                    combogroups: [],
                    single:true
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/combogroup/select.cshtml",
                    controller: "tenant.views.connect.combogroup.select.combogroup as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        combogroup: function () {
                            return vm.comboGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.comboGroup = result;
                    var successFlag = false;
                    if (result.combogroups.length > 0) {
                        angular.forEach(result.combogroups, function (value, key) {
                            vm.combos.some(function (selectedData, selectedKey) {
                                if (value.id == selectedData.id) {
                                    abp.notify.error(value.name + " is already Exists");
                                    successFlag = true;
                                    return successFlag;
                                }
                            });
                        });
                        if (!successFlag)
                        {
                            angular.forEach(vm.comboGroup.combogroups, function (value, key) {
                                var data = value;
                                vm.combos.push({
                                    'id': data.id,
                                    'name': data.name,
                                    'sortOrder': data.sortOrder,
                                    'minimum': data.minimum,
                                    'maximum': data.maximum,
                                    'color': data.color,
                                    'comboItems': data.comboItems
                                });
                            });
                        }
                    }
                });
            };
            vm.editComboGroup = function (index) {
                vm.selectedComboGroupIndex = index;
                addorEditGroup(vm.combos[index], true);
            };
            vm.deleteComboGroup = function (productIndex) {
                vm.combos.splice(productIndex, 1);
            };

            vm.sortComboGroup = function () {
                opensortComboGroup();
            };

            function opensortComboGroup() {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/menuitem/sortComboGroup.cshtml",
                    controller: "tenant.views.connect.menuitem.sortComboGroup as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        menuItemId: function () {
                            return vm.menuItemId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });
            }
           
            vm.listComboItems = function (productIndex) {
                vm.comboItems = vm.combos[productIndex].comboItems;
                vm.showComboItems = 1;
                vm.selectedComboIndex = productIndex;
            };
            vm.addComboItem = function (productIndex) {
                vm.listComboItems(productIndex);
                addorEditItem({ 'name': "", 'menuItemId': 0,'buttonColor':'#E6E784', 'autoSelect': false, 'price': 0, 'sortOrder': vm.combos[productIndex].comboItems.length, 'count': 1, 'addSeperately': false }, false);
            };
            vm.editComboItem = function (productIndex) {
                vm.selectedComboItemIndex = productIndex;
                addorEditItem(vm.comboItems[productIndex], true);
            };
            vm.deleteComboItem = function (productIndex) {
                vm.comboItems.splice(productIndex, 1);
            };
            vm.portionLength = function () {
                return true;
            };

            vm.languages = abp.localization.languages;
            vm.addDescriptions = function () {
                vm.menuitem.menuItemDescriptions.push({ 'id': 0, 'menuItemId': vm.menuItemId });
            };
            vm.removeDescription = function (index) {
                vm.menuitem.menuItemDescriptions.splice(index, 1);
            };

            function addorEditGroup(object, replace) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/menuitem/cgroup.cshtml",
                    controller: "tenant.views.connect.menu.combo.creategroup as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        group: function () {
                            return object;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (replace)
                        vm.combos[vm.selectedComboGroupIndex] = object;
                    else {

                        vm.combos.push(object);
                    }
                });
            }

            function addorEditItem(object, replace) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/menuitem/citem.cshtml",
                    controller: "tenant.views.connect.menu.combo.createitem as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        group: function () {
                            return object;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (object.menuItemId == vm.menuitem.id) {
                        abp.notify.info(vm.menuitem.name + "  " + app.localize("SameIdError"));
                        return;
                    }

                    if (replace)
                        vm.combos[vm.selectedComboIndex].comboItems[vm.selectedComboItemIndex] = object;
                    else
                        vm.combos[vm.selectedComboIndex].comboItems.push(object);
                });
            }

            vm.save = function () {
                vm.saving = true;
                vm.setMenuTag();

                if (vm.replyFiles && vm.replyFiles.length>0) {
                    vm.addOnOutput.replyFiles = angular.toJson(vm.replyFiles);
                    console.log(vm.addOnOutput);
                }
                var locationItem = vm.locationPrices.length;

                if (locationItem == 0) {
                    vm.menuitem.locations = null;               
                }
                menuService.createOrUpdateMenuItem({
                    
                    productType: vm.productType,
                    combo: vm.combos,
                    locationGroup: vm.locationGroup,
                    refreshImage: vm.refreshImage,
                    menuItemLocationPrices: vm.locationPrices,
                    menuItem: vm.menuitem,
                    urbanPiper: vm.addOnOutput
                }).success(function () {
                    abp.notify.info(vm.menuitem.name + "  " + app.localize("SavedSuccessfully"));
                    $state.go("tenant.menuitem", {
                        filter: vm.filterText
                    });
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go("tenant.menuitem", {
                    filter: vm.filterText,
                    page: vm.skipCount
                });
            };

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };

            $scope.objArrayPosition = function (arr, keyName, val) {
                return arr.map(function (a) { return a[keyName]; }).indexOf(val);
            };

            vm.addBarCode = function () {
                var idx = $scope.objArrayPosition(vm.menuitem.barcodes, "barcode", vm.barcode);
                if (idx >= 0) {
                    abp.notify.error(app.localize("AlreadyLocalExists_F", vm.barcode));
                    return;
                } else {
                    menuService.isBarcodeExists(vm.barcode).success(function (result) {
                        if (!result) {
                            vm.menuitem.barcodes.push({ 'id': 0, 'barcode': vm.barcode });
                            vm.barcode = "";
                        } else {
                            abp.notify.error(app.localize("AlreadyExists_F", vm.barcode));
                        }
                    }).finally(function () {
                        vm.saving = false;
                    });
                }
            };
            vm.deleteBarCode = function () {
                angular.forEach(vm.selectedBarcodes, function (item) {
                    var idx = vm.menuitem.barcodes.indexOf(item);
                    vm.menuitem.barcodes.splice(idx, 1);
                });
            };
            vm.readBarCodes = function () {
            };

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.openforMenuItem = function (product) {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    locationTags: [],
                    group: false,
                    single: true,
                    locationTag: false,
                    service: menuService.getNonComboItems
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        product.refMenuItemId = result.locations[0].id;
                        product.name = result.locations[0].name;
                    }
                });
            };

            vm.refdays = [];
            function fillDays() {
                vm.loading = true;
                lookupService.getDays({}).success(function (result) {
                    vm.refdays = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.menuItemTags = [];
            vm.getmenuTags = function (tag) {
                var tags = tag.split(',');

                angular.forEach(tags, function (item) {
                    if (item) {
                        vm.menuItemTags.push({ "text": item });
                    }
                });
            };

            vm.setMenuTag = function () {
                var tags = [];
                angular.forEach(vm.menuItemTags, function (item) {
                    if (item) {
                        tags.push(item.text);
                    }
                });
                vm.menuitem.tag = tags.join(",");
            };

            vm.openforCategoryItem = function () {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    service: categoryService.getAllCategoyItems
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.menuitem.categoryId = result.locations[0].id;
                        vm.menuitem.categoryName = result.locations[0].name;
                    }
                });
            };

            function init() {
                vm.saving = true;

                lookupService.getTransactionTypesForCombobox({}).success(function (result) {
                    vm.transactionTypes = result.items;
                    vm.transactionTypes.unshift({ value: "0", displayText: app.localize("NotAssigned") });
                });
                fillDays();

                menuService.getComboItems({}).success(function (result) {
                    vm.allComboItems = result.items;
                });

                menuService.getLocationPricesForMenu({
                    menuItemId: vm.menuItemId
                }).success(function (result) {
                    vm.locationPrices = result;
                });
                vm.countPortions = 0;
                menuService.getMenuItemForEdit({
                    id: vm.menuItemId
                }).success(function (result) {
                    vm.menuitem = result.menuItem;
                    vm.combos = result.combo;
                    vm.productType = result.productType;
                    vm.requestlocations = result.locations;
                    console.log("data-edit", result);
                    vm.locationGroup = result.locationGroup;
                    vm.countPortions = result.menuItem.portions.length;
                    console.log("count edit", vm.countPortions);
                    if (vm.menuitem.tag != null)
                        vm.getmenuTags(vm.menuitem.tag);

                    if (result.urbanPiper != null) {
                        vm.addOnOutput = result.urbanPiper;

                        if (vm.replyFiles != null)
                            vm.replyFiles = JSON.parse(vm.addOnOutput.replyFiles);
                    }
                });

                lookupService.getProductTypes({}).success(function (result) {
                    vm.productTypes = result.items;
                });

                

                menuService.getFoodTypes().success(function (result) {
                    vm.foodTypes = result.items;
                });
                vm.saving = false;
            }

            init();


            vm.openLanguageDescriptionModal = function() {
             
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/languageDescription/languageDescriptionModal.cshtml",
                    controller: "tenant.views.connect.languageDescription.languageDescriptionModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        languagedescription: function() {
                            return {
                                id: vm.menuitem.id,
                                name: vm.menuitem.name,
                                languageDescriptionType: 3
                            };
                        }
                    }
                });

                modalInstance.result.then(function(result) {
                    init();
                });

            };

            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadFile'
            });

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (vm.replyFiles === null) {
                    vm.replyFiles = [];
                }
                if (response !== null) {
                    if (response.result !== null) {
                        if (response.result.fileName !== null) {
                            vm.replyFiles.push(response.result);
                            vm.refreshImage = true;
                        }
                    }
                }
            };

            vm.downloader = function (file) {
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + file.fileName+'&url='+file.fileSystemName;
            };

            vm.removeFile = function (findex) {
                vm.replyFiles.splice(findex, 1);
                vm.refreshImage = true;
            };

            vm.importDescriptions = function () {
                importModal();
            };

            function importModal() {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/menuitem/importDescriptionsModal.cshtml',
                    controller: 'tenant.views.connect.master.menuitem.importDescriptionsModal as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        menuItemId: function () {
                            return vm.menuItemId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });
            }

            vm.addLocationPrices = function () {
                vm.locationPrices.push({ 'locationId': 0, 'menuItemId': vm.menuItemId });
            };

            vm.removeLocationPrice = function (index) {
                vm.locationPrices.splice(index, 1);
            };

            

            vm.openSelectLocation = function (locationPrice) {
                vm.locationGroup1 = app.createLocationInputForCreateSingleLocation();

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup1;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.isLocationExists = false;

                        angular.forEach(vm.locationPrices, function (item) {
                            if (result.locations[0].id === item.locationId) {
                                vm.isLocationExists = true;
                            }
                        });

                        if (!vm.isLocationExists) {
                            locationPrice.locationId = result.locations[0].id;
                            locationPrice.locationName = result.locations[0].name;

                            locationPrice.menuItemPortions = [];
                            angular.forEach(vm.menuitem.portions, function (item) {
                                if (item.id !== 0) {
                                    locationPrice.menuItemPortions.push({
                                        'id': item.id,
                                        'locationId': locationPrice.locationId,
                                        'menuItemId': vm.menuItemId,
                                        'price': item.price,
                                        'costPrice': item.costPrice,
                                        'portionName': item.name,
                                        'aliasName': item.aliasName,
                                        'locationPrice': 0
                                    });
                                }
                            });
                        }
                    }
                });
            };


            vm.exportDescriptions = function () {
                vm.loading = true;

                menuService.getMenuItemDescriptionForExport({ menuItemId: vm.menuItemId, isExport: true })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    })
                    .finally(function () {
                        vm.loading = false;
                    });
            };

            vm.openLocationPriceModal = function (data) {
                $state.go("tenant.upsellinglocationprice", {
                    id: vm.menuItemId,
                    upmenuItemId: data.id
                });
            }
        }
    ]);
})();