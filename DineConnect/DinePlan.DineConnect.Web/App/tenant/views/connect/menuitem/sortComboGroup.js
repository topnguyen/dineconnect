﻿(function () {
    appModule.controller("tenant.views.connect.menuitem.sortComboGroup", [
        "$scope", "$uibModalInstance", "abp.services.app.menuItem", 'menuItemId',
        function ($scope, $modalInstance, menuService, menuItemId) {

            var vm = this;
            vm.saving = false;
            vm.menuItemId = menuItemId;
            vm.init = function () {
                vm.loading = true;
                menuService.getProductComboGroupsBasedOnMenuItem({
                    id: menuItemId
                }).success(function (result) {
                    vm.combogroups = result;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.save = function () {
                vm.loading = true;
                vm.combogroupIds = [];
                angular.forEach(vm.combogroups, function (item) {
                    vm.combogroupIds.push(item.id);
                });
                menuService.saveSortCombogroupItems(
                    {
                        combogroupItems: vm.combogroupIds,
                        menuItemId: vm.menuItemId
                    }).success(function (result) {
                        $modalInstance.close(result);
                    }).finally(function () {
                        vm.loading = false;
                    });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.init();

        }
    ]);
})();