﻿(function () {
    appModule.controller('tenant.views.connect.master.menuitem.importDescriptionsModal', [
        '$scope', '$uibModalInstance', 'FileUploader', "abp.services.app.menuItem", 'menuItemId',
        function ($scope, $uibModalInstance, fileUploader, menuItemService, menuItemId) {
            var vm = this;
            vm.menuItemId = menuItemId;

            vm.uploader = new fileUploader({
                url: abp.appPath + 'Import/ImportMenuItemDescription',
                queueLimit: 1,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        console.log("item : " + item);
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        console.log("Type : " + type);
                        if ('|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ImportTemplate_Warn_FileType'));
                            return false;
                        }
                        return true;
                    }
                }]
            });

            vm.importpath = function () {
                vm.loading = true;
                menuItemService.getMenuItemDescriptionForExport({ menuItemId: vm.menuItemId, isExport: false })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    })
                    .finally(function () {
                        vm.loading = false; 
                    });
            };

            vm.save = function () {
                vm.loading = true;
                vm.uploader.uploadAll();
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.error != null) {
                    abp.message.warn(response.error.message);
                    $uibModalInstance.close();
                    vm.loading = false;
                    return;
                }
                abp.notify.info(app.localize("Finished"));
                $uibModalInstance.close();
                vm.loading = false;
            };
        }

    ]);
})();