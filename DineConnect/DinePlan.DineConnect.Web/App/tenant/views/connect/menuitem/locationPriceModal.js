﻿
(function () {
    appModule.controller('tenant.views.connect.menuitem.locationPriceModal', [
        '$scope', "$state", "$stateParams", "$uibModal", 'uiGridConstants', "abp.services.app.menuItem",
        function ($scope, $state, $stateParams, $modal, uiGridConstants, menuService) {

            var vm = this;
            vm.saving = false;
            vm.loading = false;
            vm.upmenuItemlocationPrice = null;
            vm.menuId = $stateParams.id;
            vm.upmenuItemId = $stateParams.upmenuItemId;
            vm.editFlag = false;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForCreateSingleLocation();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                    vm.upMenuItemLocationPrice.locationId = result.locations[0].id;
                    vm.upMenuItemLocationPrice.locationName = result.locations[0].name;
                    if (vm.editFlag == false) {
                        angular.forEach(vm.userGridOptions.data,
                            function (value, key) {
                                if (value.locationId == vm.upMenuItemLocationPrice.locationId) {
                                    abp.message.error(app.localize('LocationPriceWarning', vm.upMenuItemLocationPrice.locationName));
                                    vm.upMenuItemLocationPrice.locationId = 0;
                                    vm.intiailizeOpenLocation();
                                    return;
                                }
                            });
                    }
                });
            };


            vm.save = function () {
                if (vm.upMenuItemLocationPrice.locationId == 0) {
                    abp.notify.error(app.localize('Location') + " " + "is mandatory");
                    return;
                }
                vm.saving = true;
                vm.upMenuItemLocationPrice.upMenuItemId = vm.upmenuItemId;
                menuService.createUpMenuItemLocationPrice({
                    upMenuItemLocationPrice: vm.upMenuItemLocationPrice
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.getAll();
                    vm.clear();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go("tenant.detailmenuitem", {
                    id: vm.menuId
                });
            };

            vm.clear = function () {
                vm.intiailizeOpenLocation();
                init();
            };
            //  Index File
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize("Operations"),
                        width: 100,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.editlocationPrice(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Edit") + "\"><i class=\"fa fa-edit\"></i></button>" +
                            "  <button ng-click=\"grid.appScope.deletelocationPrice(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Delete") + "\"><i class=\"fa fa-trash\"></i></button>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Id'),
                        field: 'locationId',
                        width: 100
                    },
                    {
                        name: app.localize('Name'),
                        field: 'locationName'
                    },
                    {
                        name: app.localize('Price'),
                        field: 'price'
                    },

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                menuService.getAllUpMenuItemLocationPrice({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    upMenuItemId: vm.upmenuItemId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.getAll();

            vm.editlocationPrice = function (myObject) {
                var data = {};
                angular.copy(myObject, data);
                vm.upMenuItemLocationPrice = data;
                vm.locationGroup.locations.push({
                    'id': data.locationId,
                    'name': data.locationName
                });
                vm.editFlag = true;
            };

            vm.deletelocationPrice = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteLocationPriceWarning', myObject.locationName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            menuService.deleteUpMenuItemLocationPrice({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function init() {
                vm.loading = true;
                menuService.getUpMenuItemLocationPriceForEdit({
                    Id: vm.upmenuItemlocationPrice
                }).success(function (result) {
                    vm.upMenuItemLocationPrice = result.upMenuItemLocationPrice;
                }).finally(function () {
                    vm.loading = false;
                });
            }
            init();
        }
    ]);
})();

