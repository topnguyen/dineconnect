﻿(function () {
    appModule.controller("tenant.view.connect.menu.menuitem.index", [
        "$scope", "$state", "$uibModal", "uiGridConstants", "abp.services.app.menuItem", 'abp.services.app.category', "abp.services.app.connectLookup", '$stateParams', 'abp.services.app.delAggItem',
        function ($scope, $state, $modal, uiGridConstants, menuitemService, categoryService, connectService, $stateParams, delaggitemService) {
            var vm = this;
            vm.isDeleted = false;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });
            vm.categories = [];
            vm.productTypes = [];
            vm.loading = false;
            vm.skipCount = $stateParams.page;

            if (vm.skipCount) {
                requestParams.skipCount = vm.skipCount;
            } else {
                vm.skipCount = 0;
            }
            vm.filterText = $stateParams.filter;
            vm.currentUserId = abp.session.userId;
            vm.categoryInput = null;
            vm.menuTypeInput = 0;

            vm.permissions = {
                create: abp.auth.hasPermission("Pages.Tenant.Connect.Menu.MenuItem.Create"),
                edit: abp.auth.hasPermission("Pages.Tenant.Connect.Menu.MenuItem.Edit"),
                'delete': abp.auth.hasPermission("Pages.Tenant.Connect.Menu.MenuItem.Delete"),
                'cloneAsMaterial': abp.auth.hasPermission("Pages.Tenant.Connect.Menu.MenuItem.CloneAsMaterial"),
                'delAggEdit': abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggItem.Edit')
            };

            vm.import = function () {
                importModal(null);
            };

            function importModal() {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/menuitem/importModal.cshtml',
                    controller: 'tenant.views.connect.master.menuitem.importModal as vm',
                    backdrop: 'static'
                });

                modalInstance.result.then(function () {
                    vm.getAll();
                });
            }

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Actions"),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editDetailItem(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteMenuItem(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.cloneAsMaterial\" ng-click=\"grid.appScope.cloneMaterial(row.entity)\">" + app.localize("CloneAsMaterial") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.isDeleted && grid.appScope.permissions.edit\" ng-click=\"grid.appScope.activateItem(row.entity)\">" + app.localize("Activate") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delAggEdit\" ng-click=\"grid.appScope.convertAsDelAggItem(row.entity)\">" + app.localize("CloneAsDelAggItem") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize("Name"),
                        field: "name"
                    }, {
                        name: app.localize("AliasCode"),
                        field: "aliasCode"
                    }, {
                        name: app.localize("Price"),
                        field: "firstPortionPrice",
                        enableSorting: false,
                        cellClass: "ui-ralign"
                    },
                    {
                        name: app.localize("Category"),
                        enableSorting: false,
                        field: "categoryName"
                    },
                    {
                        name: app.localize("CreationTime"),
                        field: "creationTime",
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                menuitemService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    categoryId: vm.categoryInput,
                    categoryName: vm.categoryName,
                    menuItemType: vm.menuTypeInput,
                    isDeleted: vm.isDeleted,
                    locationGroup: vm.locationGroup
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editDetailItem = function (myObj) {
                openDetailMenuItem(myObj.id);
            };

            vm.createDetailItem = function () {
                openDetailMenuItem(null);
            };
            vm.openforCategoryItem = function () {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    service: categoryService.getAllCategoyItems
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.categoryInput = result.locations[0].id;
                        vm.categoryName = result.locations[0].name;
                    }
                });
            };

            function init() {
                connectService.getProductTypes({}).success(function (result) {
                    console.log(result);
                    vm.productTypes = result.items;
                    vm.productTypes.unshift({ value: "0", displayText: app.localize("NotAssigned") });
                });
            }

            vm.deleteMenuItem = function (myObject) {
                abp.message.confirm(
                    app.localize("DeleteMenuItemWarning", myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            menuitemService.deleteMenuItem({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize("SuccessfullyDeleted"));
                            });
                        }
                    }
                );
            };

            vm.cloneMaterial = function (myObject) {
                abp.message.confirm(
                    app.localize("CloneMenuItemAsMaterialWarning", myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            menuitemService.cloneMenuItemAsMaterial({
                                id: myObject.id
                            }).success(function () {
                                abp.notify.success(app.localize("Successfully") + ' ' + app.localize('Material') + ' ' + app.localize('Cloned'));
                            });
                        }
                    }
                );
            };


            vm.convertAsDelAggItem = function (myObject) {
                delaggitemService.convertAsMenuItemAsDelAggItem({
                    id: myObject.id
                }).success(function (result) {
                    if (result.successFlag == true) {
                        abp.notify.success(app.localize("Successfully") + ' ' + app.localize('DelAggItem') + ' ' + app.localize('Cloned'));
                    }
                    else {
                        abp.notify.error(result.errorMessage);
                        abp.message.error(result.errorMessage);
                    }
                });
            }

            vm.activateItem = function (myObject) {
                menuitemService.activateItem({
                    id: myObject.id
                }).success(function () {
                    abp.notify.success(app.localize("Successfully"));
                    vm.getAll();
                });
            };

            function openDetailMenuItem(objId) {
                $state.go("tenant.detailmenuitem", {
                    id: objId,
                    filter: vm.filterText,
                    page: requestParams.skipCount
                });
            }

            vm.exportToExcel = function () {
                vm.loading = true;
                menuitemService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
                vm.loading = false;
            };

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };

            vm.intiailizeOpenLocation=function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {

                    if (result.locations.length > 0) {
                        vm.currentLocation = result.locations[0];
                    }
                    else if (result.groups.length > 0) {
                        vm.currentLocation = result.groups[0];
                    }
                    else if (result.locationTags.length > 0) {
                        vm.currentLocation = result.locationTags[0];
                    }
                    else {
                        vm.currentLocation = null;
                    }
                });
            };
         
            vm.clear = function () {
                vm.currentLocation = null;
                vm.filterText = null;
                vm.categoryInput = null;
                vm.menuTypeInput = 0;
                vm.categoryName = null;
                vm.intiailizeOpenLocation();


                vm.getAll();
            };
            vm.showDeletedItems = function () {
                vm.isDeleted = true;
                vm.getAll();
            };

            init();
            vm.getAll();
        }
    ]);
})();