﻿(function () {
    appModule.controller("tenant.views.connect.menu.combo.createitem", [
        "$scope", "$uibModalInstance", '$uibModal', "group", 'abp.services.app.menuItem',
        function ($scope, $modalInstance, $modal, group, mservice) {
            var vm = this;
            vm.saving = false;
            vm.group = group;
            console.log(vm.group);
            vm.save = function () {
                $modalInstance.result = vm.group;
                $modalInstance.close();
            };
            vm.cancel = function () {
                $modalInstance.dismiss();
            };
            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };

            vm.openforMenuItem = function (product) {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    service: mservice.getAllMenuItemPortions
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        product.menuItemId = result.locations[0].menuItemId;
                        product.name = result.locations[0].name;
                        product.menuItemPortionId = result.locations[0].id;
                    }
                });
            };
        }
    ]);
})();