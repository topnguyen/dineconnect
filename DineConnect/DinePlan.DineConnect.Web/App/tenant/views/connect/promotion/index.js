﻿(function () {
    appModule.controller("tenant.views.connect.menu.promotion.index", [
        "$scope", "appSession", "$state", "$uibModal", "uiGridConstants", "abp.services.app.promotion", "abp.services.app.connectLookup",
        function ($scope, appSession, $state, $modal, uiGridConstants, promotionService, lookService) {
            var vm = this;
            vm.types = [];

            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];
            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createFlexibleDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.getEditionValue = function (item) {
                return item.displayText;
            };

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });

            vm.allPromotion = false;
            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.isDeleted = false;

            vm.permissions = {
                create: abp.auth.hasPermission("Pages.Tenant.Connect.Menu.Promotion.Create"),
                edit: abp.auth.hasPermission("Pages.Tenant.Connect.Menu.Promotion.Edit"),
                'delete': abp.auth.hasPermission("Pages.Tenant.Connect.Menu.Promotion.Delete")
            };

            function openSort() {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/promotion/sortitem.cshtml",
                    controller: "tenant.views.connect.promotion.sortitem as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        params:
                        {
                            startDate: moment((vm.dateRangeModel ? vm.dateRangeModel.startDate : todayAsString)).lang("en").format("YYYY-MM-DD"),
                            endDate: moment((vm.dateRangeModel ? vm.dateRangeModel.endDate : todayAsString)).lang("en").format("YYYY-MM-DD"),
                            promotionTypes: vm.types,
                            filter: vm.filterText,
                            location: vm.location,
                            deleted: vm.isDeleted,
                            allPromotion: vm.allPromotion,
                            locationGroup: vm.locationGroup
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.sortPromotion = function () {
                openSort();
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: 'id'
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Actions"),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editPromotion(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deletePromotion(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.isDeleted && grid.appScope.permissions.edit\" ng-click=\"grid.appScope.activateItem(row.entity)\">" + app.localize("Revert") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize("Name"),
                        field: "name"
                    },
                    {
                        name: app.localize("StartDate"),
                        field: "startDate",
                        cellFilter: "momentFormat: 'YYYY-MM-DD'"
                    },
                    {
                        name: app.localize("EndDate"),
                        field: "endDate",
                        cellFilter: "momentFormat: 'YYYY-MM-DD'"
                    },
                    {
                        name: app.localize("PromotionType"),
                        field: "promotionType"
                    },
                    {
                        name: app.localize("Status"),
                        field: "active",
                        cellTemplate: "<div class='ui-grid-cell-contents'>{{row.entity.active ? 'Active' : 'Inactive'}}</div>"
                    },
                    {
                        name: app.localize("CreationTime"),
                        field: "creationTime",
                        cellFilter: "momentFormat: 'YYYY-MM-DD HH:mm:ss'"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                promotionService.getAll({
                    startDate: moment(vm.dateRangeModel.startDate).lang("en").format("YYYY-MM-DD"),
                    endDate: moment(vm.dateRangeModel.endDate).lang("en").format("YYYY-MM-DD"),
                    promotionTypes: vm.types,
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    location: vm.location,
                    deleted: vm.isDeleted,
                    allPromotion: vm.allPromotion,
                    locationGroup: vm.locationGroup
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editPromotion = function (myObj) {
                openPromotion(myObj.id);
            };

            vm.createPromotion = function () {
                openPromotion(null);
            };

            vm.deletePromotion = function (myObject) {
                abp.message.confirm(
                    app.localize("DeletePromotionWarning", myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            promotionService.deletePromotion({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize("SuccessfullyDeleted"));
                            });
                        }
                    }
                );
            };

            function openPromotion(objId) {
                $state.go("tenant.detailpromotion", {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                promotionService.getAllToExcel(requestParams.sorting)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            vm.exportPromotion = function () {
                promotionService.getExportPromotions({ locationId: appSession.location.id })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.getPromotionTypes = function () {
                lookService.getPromotionTypes({
                }).success(function (result) {
                    vm.promotionTypes = result.items;
                }).finally(function (result) {
                });
            };

            vm.getPromotionTypes();
            vm.getAll();

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.currentLocation = null;
            vm.openLocation = function () {
                
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.currentLocation = result.locations[0];
                        vm.location = result.locations[0].name;
                    }
                    else if (result.groups.length > 0) {
                        vm.currentLocation = result.groups[0];
                        vm.location = result.groups[0].name;
                    }
                    else if (result.locationTags.length > 0) {
                        vm.currentLocation = result.locationTags[0];
                        vm.location = result.locationTags[0].name;
                    }
                    else {
                        vm.location = null;
                        vm.currentLocation = null;
                    }
                });
            };

            vm.clear = function () {
                vm.location = null;
                vm.currentLocation = null;
                vm.filterText = null;
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };
                vm.isDeleted = false;
                vm.intiailizeOpenLocation();

                vm.getAll();
            };

            vm.activateItem = function (myObject) {
                promotionService.activateItem({
                    id: myObject.id
                }).success(function (result) {
                    abp.notify.success(app.localize("Successfully"));
                    vm.getAll();
                });
            };
        }
    ]);
})();