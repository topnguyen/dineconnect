﻿(function () {
    appModule.controller("tenant.views.connect.promotion.detail",
        [
            "$scope", "$state", "$uibModal", "$timeout", "$stateParams", "abp.services.app.promotion", "abp.services.app.connectLookup", "abp.services.app.menuItem",
            'abp.services.app.category', 'abp.services.app.promotionCategory',  'abp.services.app.commonLookup',
            function ($scope,
                $state,
                $modal,
                $timeout,
                $stateParams,
                promotionService,
                lookService,
                menuService,
                categoryService, promotioncategoryService,
                 commonService) {

                var vm = this;
                vm.disabledSaveforLastTab = false;
                vm.promotionTypes = [];
                vm.connectCardTypes = [];
                vm.deparmentItemList = [];
                vm.promotionTypeId = 0;
                vm.resetType = 0;
                vm.saving = false;
                vm.showRules = false;
                vm.showWeek = false;
                vm.fromCountLocked = false;
                vm.fromCountValueLocked = false;
                vm.promotionId = $stateParams.id;
                vm.languageDescriptionType = 4;
                vm.allLocation = "";
                vm.stepDiscountPromotionInfo = [];
                vm.ticketDiscountPromotionDistribution = null;
                vm.totalPercentage = 0;
                vm.returnDateRangeFlag = false;

                var todayAsString = moment().format("YYYY-MM-DD");
                vm.dateRangeOptions = app.createDateRangeFuturePickerOptions();
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };
                vm.getEditionValue = function (item) {
                    return parseInt(item.value);
                };
                vm.isUndefinedOrNullOrWhiteSpace = function (val) {
                    return angular.isUndefined(val) || val == null || val == "";
                };
                // #region Scope
                $scope.settings = {
                    dropdownToggleState: false,
                    time: {
                        fromHour: "00",
                        fromMinute: "00",
                        toHour: "23",
                        toMinute: "59"
                    },
                    noRange: false,
                    format: 24,
                    noValidation: true
                };
                $scope.$on("$viewContentLoaded",
                    function () {
                        App.initAjax();                        
                    });
                $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
                $scope.format = $scope.formats[0];


                $('input[name="stDate"]').daterangepicker({
                    locale: {
                        format: $scope.format
                    },
                    singleDatePicker: true,
                    showDropdowns: true,

                });

                $('input[name="endDate"]').daterangepicker({
                    locale: {
                        format: $scope.format
                    },
                    singleDatePicker: true,
                    showDropdowns: true
                });

               

                $scope.validateDate = function () {
                    var startDate = $("#stDate").val();
                    var endDate = $("#endDate").val();

                    if (startDate == null || startDate == '')
                        return;

                    if (endDate == null || endDate == '')
                        endDate = startDate;

                    var scheduleStartDate = moment(startDate).format($scope.format);
                    var scheduleEndDate = moment(endDate).format($scope.format);

                    vm.promotion.startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    vm.promotion.endDate = moment(vm.dateRangeModel.endDate).format($scope.format);

                    if ((moment(scheduleStartDate) >= moment(vm.promotion.startDate)) && (moment(scheduleStartDate) <= moment(vm.promotion.endDate))) {
                        // Do Nothing
                    }
                    else {
                        abp.notify.error(app.localize('ScheduleDateAlert', 'Schedule' + 'StartDate'));
                        vm.scheduleStartDate = null;
                        vm.returnDateRangeFlag = true;
                        return;
                    }
                    if ((moment(scheduleEndDate) >= moment(vm.promotion.startDate)) && (moment(scheduleEndDate) <= moment(vm.promotion.endDate))) {
                        // Do Nothing
                    }
                    else {
                        abp.notify.error(app.localize('ScheduleDateAlert', 'Schedule' + 'EndDate'));
                        vm.scheduleEndDate = null;
                        vm.returnDateRangeFlag = true;
                        return;
                    }
                    if (moment(scheduleStartDate) > moment(scheduleEndDate)) {
                        abp.notify.error(app.localize('shouldNotBeGreaterThan', 'Schedule' + 'EndDate', 'Schedule' + 'StartDate'));
                        vm.returnDateRangeFlag = true;
                        return;
                    }
                    if (moment(vm.promotion.startDate) > moment(scheduleStartDate)) {
                        abp.notify.error(app.localize('shouldNotBeGreaterThan', 'Schedule' + 'StartDate', 'StartDate'));
                        vm.returnDateRangeFlag = true;
                        return;
                    }
                    if (moment(vm.promotion.startDate) > moment(scheduleEndDate)) {
                        abp.notify.error(app.localize('shouldNotBeGreaterThan', 'Schedule' + 'EndDate', 'StartDate'));
                        vm.returnDateRangeFlag = true;
                        return;
                    }
                    if (!vm.isUndefinedOrNullOrWhiteSpace(scheduleStartDate)) {
                        if (moment(scheduleStartDate) > moment(vm.promotion.endDate)) {
                            abp.notify.error(app.localize('shouldNotBeGreaterThan', 'Schedule' + 'StartDate', 'EndDate'));
                            vm.returnDateRangeFlag = true;
                            return;
                        }
                    }
                    if (vm.promotion.endDate != vm.promotion.startDate) {
                        if (moment(scheduleEndDate) > moment(vm.promotion.endDate)) {
                            abp.notify.error(app.localize('shouldNotBeGreaterThan', 'Schedule' + 'EndDate', 'EndDate'));
                            vm.returnDateRangeFlag = true;
                            return;
                        }
                    }
                    vm.returnDateRangeFlag = false;
                };

                vm.datePickerData = function () {
                    if (vm.promotion) {
                        vm.promotion.startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        vm.promotion.endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                        vm.scheduleStartDate = vm.promotion.startDate;
                        vm.scheduleEndDate = vm.promotion.endDate;
                    }

                    console.log("Picker date");
                    if (vm.promotion)
                        vm.promotion.promotionSchedules = [];
                };
                $scope.buildDisable = true;
                $scope.builder =
                    app.createBuilder(
                        {
                            condition: "AND",
                            rules: [
                                {
                                    id: "Total",
                                    operator: "greater_or_equal",
                                    value: 0
                                }
                            ]
                        },
                        angular.fromJson
                            (
                                [
                                    {
                                        "id": "Total",
                                        "label": app.localize("Total"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 0.1
                                        }
                                    }, 
                                    {
                                        "id": "DepartmentGroup",
                                        "label": app.localize("DepartmentGroup"),
                                        "type": "string",
                                        plugin: 'selectize',
                                        plugin_config: {
                                            valueField: 'value',
                                            labelField: 'displayText',
                                            searchField: 'displayText',
                                            sortField: 'displayText',
                                            create: true,
                                            maxItems: 1,
                                            plugins: ['remove_button'],
                                            onInitialize: function () {
                                                var that = this;
                                                JSON.parse(localStorage.deparmentGroups).forEach(function (item) {
                                                    that.addOption(item);
                                                });
                                            },
                                            onChange: function (value) {
                                                localStorage.departments = [];
                                                $scope.buildDisable = true;
                                                var that = this;
                                                if (value !== '') {
                                                    vm.findDepartmentList(value);                                                  
                                                }
                                                $scope.buildDisable = false;
                                            }
                                        },
                                        valueSetter: function (rule, value) {
                                            rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                        }
                                    },
                                    {
                                        "id": "Department",
                                        "label": app.localize("Department"),
                                        "type": "string",
                                        plugin: 'selectize',
                                        plugin_config: {
                                            valueField: 'value',
                                            labelField: 'displayText',
                                            searchField: 'displayText',
                                            sortField: 'displayText',
                                            create: true,
                                            maxItems: 1,
                                            plugins: ['remove_button'],                                           
                                            onInitialize: function () {
                                                var that = this;
                                                if (localStorage.departments.length == 0) {
                                                    localStorage.departments = localStorage.allDepartments;
                                                }
                                                console.log("item", JSON.parse(localStorage.departments));
                                                JSON.parse(localStorage.departments).forEach(function (item) {
                                                    that.addOption(item);
                                                });
                                            }
                                        },
                                        valueSetter: function (rule, value) {
                                            rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                        }
                                    },
                                    {
                                        "id": "Member",
                                        "label": app.localize("Member"),
                                        "type": "boolean"
                                    },
                                   
                                    {
                                        id: 'PaymentType',
                                        label: app.localize("PaymentType"),
                                        type: 'string',
                                        plugin: 'selectize',
                                        plugin_config: {
                                            valueField: 'value',
                                            labelField: 'displayText',
                                            searchField: 'displayText',
                                            sortField: 'displayText',
                                            create: true,
                                            maxItems: 1,
                                            plugins: ['remove_button'],
                                            onInitialize: function () {
                                                var that = this;
                                                JSON.parse(localStorage.paymentTypes).forEach(function (item) {
                                                    that.addOption(item);
                                                });
                                            }
                                        },
                                        valueSetter: function (rule, value) {
                                            rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                        }
                                    }
                                ]
                            )
                    );

                // #endregion


                // #region Schedule
                vm.removeSchedule = function (productIndex) {
                    vm.promotion.promotionSchedules.splice(productIndex, 1);
                };
                vm.addSchedule = function () {
                    var startDate = $("#stDate").val();
                    var endDate = $("#endDate").val();

                    if (startDate === undefined || startDate == null || !startDate || 0 === startDate.length) {
                        startDate = vm.dateRangeModel.startDate;
                        console.log("C1");
                        console.log(startDate);
                        console.log(endDate);
                    }

                    if (endDate === undefined || endDate == null || !endDate || 0 === endDate.length) {
                        endDate = vm.dateRangeModel.endDate;
                        console.log("C2");
                        console.log(startDate);
                        console.log(endDate);
                    }


                    if (startDate === undefined || startDate == null || !startDate || 0 === startDate.length) {
                        startDate = moment().startOf("day").format('YYYY-MM-DD');
                        endDate = moment().startOf("day").format('YYYY-MM-DD');
                        console.log("C3");
                        console.log(startDate);
                        console.log(endDate);

                    }
                    $scope.validateDate();
                    if (!vm.returnDateRangeFlag) {
                        vm.promotion.promotionSchedules.push({
                            'startDate': vm.scheduleStartDate,
                            'endDate': vm.scheduleEndDate,
                            'startHour': $scope.settings.time.fromHour,
                            'endHour': $scope.settings.time.toHour,
                            'startMinute': $scope.settings.time.fromMinute,
                            'endMinute': $scope.settings.time.toMinute,
                            'allDays': vm.refday,
                            'allMonthDays': vm.refmonthday
                        });

                        vm.refday = [];
                        vm.refmonthday = [];
                    }
                    vm.scheduleStartDate = null;
                    vm.scheduleEndDate = null;
                };

                // #endregion
                // #region Save
                vm.save = function () {
                    vm.isValidate = true;
                    vm.promotion.startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    vm.promotion.endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    if (vm.promotion.startDate == todayAsString && vm.promotion.endDate == todayAsString)
                    {
                        abp.notify.error(app.localize('PromotionDateRangeError'));
                        return;
                    }
                    vm.promotion.promotionTypeId = vm.promotionTypeId;

                    if (vm.promotion.promotionTypeId == 9) {
                        vm.promotion.promotionContents = angular.toJson(vm.demandPromotion);
                    }

                    if (vm.promotion.promotionTypeId == 10 || vm.promotion.promotionTypeId == 8) {
                        if (vm.ticketDiscountPromotion.ticketDiscountPromotionDistributions.length > 0 &&
                            vm.totalPercentage != 100) {
                            abp.notify.error('Total Percentage Should be 100');
                            return;
                        }
                        vm.promotion.promotionContents = angular.toJson(vm.ticketDiscountPromotion);
                    }
                    if (vm.promotion.promotionTypeId == 11) {
                        vm.promotion.promotionContents = angular.toJson(vm.buyXAndYAtZValuePromotion);
                    }
                    if (vm.promotion.promotionTypeId == 12) {
                        vm.promotion.promotionContents = angular.toJson(vm.stepDiscountPromotion);
                    }
                    vm.validateForSave();

                    if (vm.isValidate) {
                        vm.saving = true;
                        promotionService.createOrUpdatePromotion({
                            promotion: vm.promotion,
                            locationGroup: vm.locationGroup,
                            timerPromotion: vm.timerPromotion,
                            fixPromotions: vm.fixedpromotion,
                            fixPromotionsPer: vm.fixedpromotionper,
                            freePromotion: vm.freePromotion,
                            freeValuePromotion: vm.freeValuePromotion,
                            demandPromotion: vm.demandPromotion,
                            freeItemPromotion: vm.freeItemPromotion,
                            ticketDiscountPromotion: vm.ticketDiscountPromotion,
                            buyXAndYAtZValuePromotion: vm.buyXAndYAtZValuePromotion,
                            stepDiscountPromotion: vm.stepDiscountPromotion,
                            promotionQuotas: vm.promotionQuotas,
                            filter: angular.toJson($scope.builder.builder.getRules())
                        }).success(function () {
                            abp.notify.info(app.localize("SavedSuccessfully"));
                            vm.cancel();
                        }).finally(function () {
                            vm.saving = false;
                        });
                    }
                };
                vm.isValidate = true;
                vm.validateForSave = function () {
                    angular.forEach(vm.promotion.promotionRestrictItems,
                        function (item) {
                            if (item.menuItemId == 0 && vm.isValidate) {
                                abp.notify.warn(app.localize("PleaseChooseMenuItemForRestrictItem"));
                                vm.isValidate = false;
                                return;
                            }
                        });

                    //Validate promotion quota
                    
                    var allSelected = false;
                    angular.forEach(vm.promotionQuotas,
                        function (item) {
                            if (item.quotaAmount === undefined || item.resetType === undefined) {
                                abp.notify.error(app.localize("Invalid Quota data"));
                                vm.isValidate = false;
                                return;
                            }
                            if (item.quotaAmount < 0) {
                                abp.notify.error(app.localize("Invalid Quota Count"));
                                vm.isValidate = false;
                                return;
                            }
                            if (item.resetType == 1 && item.resetWeekValue == null) {
                                abp.notify.error(app.localize("Invalid Quota Reset value"));
                                vm.isValidate = false;
                                return;
                            }
                            if ((item.resetType == 2 || item.resetType == 3) && !item.resetMonthValue) {
                                abp.notify.error(app.localize("Invalid Quota Reset value"));
                                vm.isValidate = false;
                                return;
                            }
                            if (!item.plants) {
                                abp.notify.error(app.localize("Invalid Plants"));
                                vm.isValidate = false;
                                return;
                            }
                            if (item.plants === '*') {
                                allSelected = true;
                            }
                        });
                    if (vm.promotionQuotas && vm.promotionQuotas.length > 1 && allSelected) {
                        abp.notify.error(app.localize("All PLANTS are selected!"));
                        vm.isValidate = false;
                        return;
                    }
                };
                vm.cancel = function () {
                    $state.go("tenant.promotion");
                };


                // #endregion
                // #region Promotion


                vm.openforMenuItemPortions = function (data) {
                    vm.lgroup = {
                        locations: [],
                        groups: [],
                        group: false,
                        single: true,
                        menuItemId: data.menuItemId,
                        service: !data.menuItemId || data.menuItemId == 0 ? menuService.getAllMenuItemPortions : menuService.getAllMenuItemPortionsBasedOnMenuItemId
                    };

                    const modalInstance = $modal.open({
                        templateUrl: "~/App/common/views/lookup/select.cshtml",
                        controller: "tenant.views.common.lookup.select as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.lgroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        if (result.locations.length > 0) {
                            data.menuItemPortionId = result.locations[0].id;
                            data.portionName = result.locations[0].name;
                        }
                    });
                };

                vm.refday = [];
                vm.refdays = [];

                function fillDays() {
                    lookService.getDays({}).success(function (result) {
                        vm.refdays = result.items;
                    });
                }

                vm.refmonthday = [];
                vm.refmonthdays = [];

                function fillMonthDays() {
                    lookService.getMonthDays({}).success(function (result) {
                        vm.refmonthdays = result.items;
                    });
                }

                vm.refStepDiscountTypes = [];

                function fillStepDiscountTypes() {
                    promotionService.getPromotionStepDiscountTypes({}).success(function (result) {
                        vm.refStepDiscountTypes = result.items;
                    });
                }
                fillStepDiscountTypes();

                vm.refPromotionApplyTypes = [];

                function fillPromotionApplyTypes() {
                    promotionService.getPromotionApplyTypes({}).success(function (result) {
                        vm.refPromotionApplyTypes = result.items;
                    })
                }
                fillPromotionApplyTypes();
                vm.addFixProsPer = function () {
                    vm.fixedpromotionper.push({
                        'productGroupId': 0,
                        'categoryId': 0,
                        'menuItemId': 0,
                        'menuItemPortionId': 0,
                        'promotionValueType': 1,
                        'promotionValue': 0
                    });
                };
                vm.removeFixProsPer = function (productIndex) {
                    vm.fixedpromotionper.splice(productIndex, 1);
                };
                vm.removeFixPros = function (productIndex) {
                    vm.fixedpromotion.splice(productIndex, 1);
                };
                vm.addFixPros = function () {
                    vm.fixedpromotion.push({
                        'productGroupId': 0,
                        'categoryId': 0,
                        'menuItemId': 0,
                        'menuItemPortionId': 0,
                        'promotionValueType': 2,
                        'promotionValue': 0
                    });
                };

                vm.addFreeFrom = function () {
                    vm.freePromotion.from.push({ 'productGroupId': 0, 'categoryId': 0, 'menuItemId': 0, 'menuItemPortionId': 0 });
                    vm.disabledForLastTab(vm.freePromotion.from);
                };
                vm.removeFreeFrom = function (productIndex) {
                    vm.freePromotion.from.splice(productIndex, 1);
                    vm.disabledForLastTab(vm.freePromotion.from);
                };
                vm.addFreeValueFrom = function () {
                    vm.freeValuePromotion.from.push({ 'productGroupId': 0, 'categoryId': 0, 'menuItemId': 0, 'menuItemPortionId': 0 });
                    vm.disabledForLastTab(vm.freeValuePromotion.from);
                };
                vm.removeFreeValueFrom = function (productIndex) {
                    vm.freeValuePromotion.from.splice(productIndex, 1);
                    vm.disabledForLastTab(vm.freeValuePromotion.from);
                };
                vm.addFreeItem = function () {
                    vm.freeItemPromotion.executions.push({ 'menuItemPortionId': 0 });
                    vm.disabledForLastTab(vm.freeItemPromotion.executions);
                };
                vm.removeFreeItem = function (productIndex) {
                    vm.freeItemPromotion.executions.splice(productIndex, 1);
                    vm.disabledForLastTab(vm.freeItemPromotion.executions);
                };
                vm.addFreeTo = function () {
                    vm.freePromotion.to.push({ 'productGroupId': 0, 'categoryId': 0, 'menuItemId': 0, 'menuItemPortionId': 0 });
                    vm.disabledForLastTab(vm.freePromotion.to);
                };
                vm.removeFreeTo = function (productIndex) {
                    vm.freePromotion.to.splice(productIndex, 1);
                    vm.disabledForLastTab(vm.freePromotion.to);
                };
                vm.addFreeValueTo = function () {
                    vm.freeValuePromotion.to.push({ 'productGroupId': 0, 'categoryId': 0, 'menuItemId': 0, 'menuItemPortionId': 0 });
                    vm.disabledForLastTab(vm.freeValuePromotion.to);
                };
                vm.removeFreeValueTo = function (productIndex) {
                    vm.freeValuePromotion.to.splice(productIndex, 1);
                    vm.disabledForLastTab(vm.freeValuePromotion.to);
                };

                vm.addDemandPromosExes = function () {
                    vm.demandPromotion.demandPromotionExecutions.push({ 'productGroupId': 0, 'categoryId': 0, 'menuItemId': 0, 'menuItemPortionId': 0 });
                };
                vm.removeDemandPromosExes = function (productIndex) {
                    vm.demandPromotion.demandPromotionExecutions.splice(productIndex, 1);
                };
                vm.addRestrictItems = function () {
                    vm.promotion.promotionRestrictItems.push({ 'id': 0, 'menuItemId': 0, 'name': "" });
                };
                vm.removeRestrictItem = function (productIndex) {
                    vm.promotion.promotionRestrictItems.splice(productIndex, 1);
                };

                vm.removebuyXAndYAtZValue = function (productIndex) {
                    vm.buyXAndYAtZValuePromotion.buyXAndYAtZValueExecutions.splice(productIndex, 1);
                };
                vm.addbuyXAndYAtZValue = function () {
                    vm.buyXAndYAtZValuePromotion.buyXAndYAtZValueExecutions.push({
                        'productGroupId': 0,
                        'categoryId': 0,
                        'menuItemId': 0,
                        'menuItemPortionId': 0,
                        'promotionValueType': 11,
                        'promotionValue': 0
                    });
                };

                vm.removeStepDiscount = function (productIndex) {
                    vm.stepDiscountPromotion.stepDiscountExecutions.splice(productIndex, 1);
                };
                vm.addStepDiscount = function (data) {
                    if (vm.isUndefinedOrNullOrWhiteSpace(data.promotionStepTypeValue)) {
                        if (vm.stepDiscountPromotion.stepDiscountType == 0) {
                            abp.notify.error(app.localize('Please choose the Step Type'));
                            return;
                        }
                        else if (vm.stepDiscountPromotion.stepDiscountType == 1) {
                            abp.notify.error(app.localize('Please fill the Quantity'));
                            return;
                        }
                        else {
                            abp.notify.error(app.localize('Please fill the Price'));
                            return;
                        }
                    }
                    if (vm.isUndefinedOrNullOrWhiteSpace(data.promotionValueType)) {
                        abp.notify.error(app.localize('Please Choose Type'));
                        return;
                    }
                    if (vm.isUndefinedOrNullOrWhiteSpace(data.promotionValue)) {
                        abp.notify.error(app.localize('Please fill the promtion value'));
                        return;
                    }
                    if (data.promotionValueType == 1) {
                        data.promotionValueTypeName = "Percentage";
                    }
                    else {
                        data.promotionValueTypeName = "Value";
                    }
                    vm.stepDiscountPromotion.stepDiscountExecutions.push({
                        'id': null,
                        'promotionStepTypeValue': data.promotionStepTypeValue,
                        'promotionValueType': data.promotionValueType,
                        'promotionValue': data.promotionValue,
                        'promotionValueTypeName': data.promotionValueTypeName
                    });
                    vm.stepDiscountPromotionInfo = [];
                };
                vm.removeStepDiscountMapping = function (productIndex) {
                    vm.stepDiscountPromotion.stepDiscountMappingExecutions.splice(productIndex, 1);
                };
                vm.addStepDiscountMapping = function () {
                    if (!vm.stepDiscountPromotion.stepDiscountMappingExecutions)
                        vm.stepDiscountPromotion.stepDiscountMappingExecutions = [];
                    vm.stepDiscountPromotion.stepDiscountMappingExecutions.push({
                        'productGroupId': 0,
                        'categoryId': 0,
                        'menuItemId': 0,
                        'menuItemPortionId': 0
                    });
                };
                vm.addTicketDistributions = function (data) {

                    if (data != null) {
                        if (data.productGroupId == 0) {
                            data.productGroupName = "*";
                        }
                        if (data.categoryId == 0) {
                            data.categoryName = "*";
                        }
                        if (data.menuItemId == 0) {
                            data.menuItemGroupCode = "*";
                        }
                        if (data.menuItemPortionId == 0) {
                            data.portionName = "*";
                        }
                        var total = 0;
                        angular.forEach(vm.ticketDiscountPromotion.ticketDiscountPromotionDistributions,
                            function (value, key) {
                                var totalPercentage = value.percentage;
                                total = total + totalPercentage;
                            });
                        total = total + data.percentage;
                        vm.totalPercentage = total;
                        if (vm.totalPercentage > 100) {
                            abp.notify.error('Total percentage should not exist more than 100');
                            return;
                        }
                        vm.ticketDiscountPromotion.ticketDiscountPromotionDistributions.push({
                            'id': null,
                            'productGroupId': data.productGroupId,
                            'productGroupName': data.productGroupName,
                            'categoryId': data.categoryId,
                            'categoryName': data.categoryName,
                            'menuItemId': data.menuItemId,
                            'menuItemGroupCode': data.menuItemGroupCode,
                            'menuItemPortionId': data.menuItemPortionId,
                            'portionName': data.portionName,
                            'percentage': data.percentage
                        });
                    }
                    vm.ticketDiscountPromotionDistribution =
                    {
                        'id': null,
                        'productGroupId': 0,
                        'productGroupName': null,
                        'categoryId': 0,
                        'categoryName': null,
                        'menuItemId': 0,
                        'menuItemGroupCode': null,
                        'menuItemPortionId': 0,
                        'portionName': null,
                        'percentage': 0
                    };

                };

                vm.gettotalPercentage = function () {
                    var total = 0;
                    angular.forEach(vm.ticketDiscountPromotion.ticketDiscountPromotionDistributions, function (value, key) {
                        var totalPercentage = value.percentage;
                        total = total + totalPercentage;
                    });
                    vm.totalPercentage = total;
                }

                vm.removeTicketDistributions = function (index) {
                    vm.ticketDiscountPromotion.ticketDiscountPromotionDistributions.splice(index, 1);
                };

                vm.changeFromCount = function (status) {
                    if (status) {
                        if (vm.freePromotion.from.length == 0) {
                            vm.freePromotion.fromCount = 1;
                        } else {
                            vm.freePromotion.fromCount = vm.freePromotion.from.length;
                        }
                        vm.fromCountLocked = true;
                    } else {
                        vm.fromCountLocked = false;
                    }
                };
                vm.changeFromValueCount = function (status) {
                    if (status) {
                        if (vm.freeValuePromotion.from.length == 0) {
                            vm.freeValuePromotion.fromCount = 1;
                        } else {
                            vm.freeValuePromotion.fromCount = vm.freeValuePromotion.from.length;
                        }
                        vm.fromCountValueLocked = true;
                    } else {
                        vm.fromCountValueLocked = false;
                    }
                };

                vm.typeChanged = function () {
                    switch (vm.promotionTypeId) {
                        case 4:
                            vm.freePromotion.from = [];
                            vm.freePromotion.to = [];
                            vm.addFreeFrom();
                            vm.addFreeTo();
                            break;
                        case 5:
                            vm.freeValuePromotion.from = [];
                            vm.freeValuePromotion.to = [];
                            vm.addFreeValueFrom();
                            vm.addFreeValueTo();
                            break;
                        case 7:
                            vm.freeItemPromotion.executions = [];
                            vm.addFreeItem();
                            break;
                        case 8:
                            vm.ticketDiscountPromotion.ticketDiscountPromotionDistributions = [];
                            vm.addTicketDistributions();
                            break;
                        case 9:
                            console.log($scope.builder);
                            $scope.builder.options.filters.push({
                                'id': "PaymentType",
                                'label': "PaymentType",
                                'type': "string"
                            });
                            break;

                        case 10:

                            $scope.builder.options.filters.push({
                                'id': "PaymentType",
                                'label': "PaymentType",
                                'type': "string"
                            });
                            vm.ticketDiscountPromotion.ticketDiscountPromotionDistributions = [];
                            vm.addTicketDistributions();
                            break;
                        case 11:
                            vm.buyXAndYAtZValuePromotion.buyXAndYAtZValueExecutions = [];
                            vm.addbuyXAndYAtZValue();
                            break;
                        case 12:
                            vm.stepDiscountPromotion.stepDiscountExecutions = [];
                            vm.stepDiscountPromotion.stepDiscountMappingExecutions = [];
                            vm.addStepDiscount();
                            vm.addStepDiscountMapping();
                            break;
                        default:
                            break;
                    }
                };


                // #endregion
                vm.openLanguageDescriptionModal = function () {
                    vm.languageDescription = {
                        id: vm.promotionId,
                        name: vm.promotion.name,
                        languageDescriptionType: vm.languageDescriptionType
                    };
                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/languageDescription/languageDescriptionModal.cshtml",
                        controller: "tenant.views.connect.languageDescription.languageDescriptionModal as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            languagedescription: function () {
                                return vm.languageDescription;
                            }
                        }
                    });

                    modalInstance.result.then(function (result) {
                        init();
                    });
                }

                vm.$onInit = function () {

                    $timeout(function () {
                        // wait till load event fires so all resources are available
                        $('#builder').on('beforeDeleteRule.queryBuilder', function (e, rule) {
                            var deleteRule = $(".rule-filter-container > select", rule.$el).val();
                            if (deleteRule == 'DepartmentGroup') {
                                localStorage.departments = [];
                            }


                        });
                    });
                    
                    vm.saving = true;
                    vm.loading = true;
                    vm.getDepartmentListFilter();
                    vm.getPromotionsCombine();
                    vm.getPayments();
                    vm.getDepartments();
                    vm.getDepartmentGroups();

                    lookService.getPromotionTypes({}).success(function (result) {
                        vm.promotionTypes = result.items;
                        vm.promotionTypes.unshift({ value: "0", displayText: app.localize("NotAssigned") });
                    });

                    lookService.getConnectCardTypes({}).success(function (result) {
                        vm.connectCardTypes = result.items;
                        vm.connectCardTypes.unshift({ value: "0", displayText: app.localize("NotAssigned") });
                    });

                    lookService.getPromotionValueTypes({}).success(function (result) {
                        vm.promotionValueTypes = result.items;
                    });

                    promotionService.getResetType({}).success(function (result) {
                        vm.resetTypes = result.items;
                    });
                    promotionService.getPromotionPositions({}).success(function (result) {
                        vm.promotionPositions = result.items;
                    });
                    lookService.getPriceTags({}).success(function (result) {
                        vm.priceTags = result.items;
                        vm.priceTags.unshift({ value: "0", displayText: app.localize("NotAssigned") });
                    });
                    fillDays();
                    fillMonthDays();


                    promotionService.getPromotionForEdit({
                        id: vm.promotionId
                    }).success(function (result) {

                        vm.promotion = result.promotion;
                       
                        $('input[name="stDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true
                        });

                        $('input[name="endDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true
                        });
                        vm.scheduleStartDate = null;
                        vm.scheduleEndDate =null;
                        if (vm.promotion.promotionSchedules.length > 0) {
                            vm.promotionSchedules = [];
                            angular.forEach(vm.promotion.promotionSchedules, function (value, key) {
                                var data = value;
                                if (data.startHour < 10) {
                                    data.startHour = '0' + data.startHour;
                                }
                                if (data.endHour < 10) {
                                    data.endHour = '0' + data.endHour;
                                }
                                if (data.startMinute < 10) {
                                    data.startMinute = '0' + data.startMinute;
                                }
                                if (data.endMinute < 10) {
                                    data.endMinute = '0' + data.endMinute;
                                }
                                vm.promotionSchedules.push({
                                    'startDate': moment(data.startDate).format($scope.format),
                                    'endDate': moment(data.endDate).format($scope.format),
                                    'startHour': data.startHour,
                                    'endHour': data.endHour,
                                    'startMinute': data.startMinute,
                                    'endMinute': data.endMinute,
                                    'allDays': data.allDays,
                                    'allMonthDays': data.allMonthDays
                                });
                            });
                            vm.promotion.promotionSchedules = vm.promotionSchedules;
                        }
                        if (vm.promotion.startDate == "0001-01-01T00:00:00" || vm.promotion.startDate == null || vm.promotion.startDate == "") {
                            vm.promotion.startDate = null;
                        }
                        else {
                            vm.dateRangeModel = {
                                startDate: result.promotion.startDate,
                                endDate: result.promotion.endDate
                            };
                        }
                        if (vm.promotion.endDate == "0001-01-01T00:00:00" || vm.promotion.endDate == null || vm.promotion.endDate == "") {
                            vm.promotion.endDate = null;
                        }
                        else {
                            vm.dateRangeModel = {
                                startDate: result.promotion.startDate,
                                endDate: result.promotion.endDate
                            };
                        }

                        vm.locationGroup = result.locationGroup;
                        vm.promotionTypeId = result.promotion.promotionTypeId;
                        vm.timerPromotion = result.timerPromotion;
                        vm.fixedpromotion = result.fixPromotions;
                        vm.fixedpromotionper = result.fixPromotionsPer;
                        vm.freePromotion = result.freePromotion;
                        vm.freeValuePromotion = result.freeValuePromotion;
                        vm.demandPromotion = result.demandPromotion;
                        vm.freeItemPromotion = result.freeItemPromotion;
                        vm.ticketDiscountPromotion = result.ticketDiscountPromotion;
                        vm.ticketDiscountPromotionDistribution = result.ticketDiscountPromotionDistribution;
                        vm.buyXAndYAtZValuePromotion = result.buyXAndYAtZValuePromotion;
                        vm.stepDiscountPromotion = result.stepDiscountPromotion;
                        vm.promotionQuotas = result.promotionQuotas;
                       
                        angular.forEach(vm.promotionQuotas, function (quota) {
                            if (quota.resetMonthValue) {
                                quota.resetMonthValue = new Date(Date.parse(moment(quota.resetMonthValue).format('YYYY-MM-DD')));
                            }
                            quota.showDatePicker = false;
                        });

                        if (result.filter != null)
                            $scope.builder.builder.setRules(angular.fromJson(result.filter));
                        if (vm.ticketDiscountPromotion.ticketDiscountPromotionDistributions.length > 0) {
                            var total = 0;
                            angular.forEach(vm.ticketDiscountPromotion.ticketDiscountPromotionDistributions, function (value, key) {
                                var totalPercentage = value.percentage;
                                total = total + totalPercentage;
                            });
                            vm.totalPercentage = total;
                        }

                        if (vm.promotion.combineProValues) {
                            if (vm.promotion.combineProValues == "*") {
                                vm.combinePros = vm.promotion.combineProValues;
                            }
                            else {
                                var names = [];
                                var proIds = vm.promotion.combineProValues.split(',');
                                angular.forEach(proIds, function (id) {

                                    angular.forEach(vm.combineProList.items, function (cp) {
                                        if (cp.id == id) {
                                            names.push(id + ' - ' + cp.name);
                                        }
                                    });
                                });
                                vm.combinePros = names.join(', ');
                            }
                        }
                    })
                        .finally(function () {
                            vm.loading = false;
                        });


                    
                    vm.saving = false;
                };

                vm.getPayments = function () {
                    lookService.getPaymentTypes({
                    }).success(function (result) {
                        //debugger
                        localStorage.paymentTypes = JSON.stringify(result.items);
                    });
                };

                vm.openRules = function () {
                    vm.showRules = !vm.showRules;
                };
                vm.intiailizeOpenLocation = function () {
                    vm.locationGroup = app.createLocationInputForCreate();
                };
                vm.openLocation = function () {
                    const modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };

                vm.openforMenuItemModal = function (product) {
                    vm.lgroup = {
                        locations: [],
                        groups: [],
                        group: false,
                        single: true,
                        service: menuService.getNonComboItems
                    };

                    const modalInstance = $modal.open({
                        templateUrl: "~/App/common/views/lookup/select.cshtml",
                        controller: "tenant.views.common.lookup.select as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.lgroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        if (result.locations.length > 0) {
                            var duplicate = false;
                            angular.forEach(vm.promotion.promotionRestrictItems,
                                function (item) {
                                    if (+item.menuItemId === + result.locations[0].id) {
                                        duplicate = true;
                                        return;
                                    }
                                });
                            if (!duplicate) {
                                product.menuItemId = result.locations[0].id;
                                product.name = result.locations[0].name;
                            } else {
                                abp.notify.info(app.localize("MenuItemIsDuplicate"));
                            }
                        }
                    });
                };

                vm.openforProductGroupItem = function (data) {
                    var modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/connect/category/productGroupTreeModal.cshtml',
                        controller: 'tenant.views.connect.category.productGroupTreeModal as vm',
                        backdrop: 'static',
                        resolve: {
                            category: function () {
                                return data;
                            }
                        }
                    });
                    modalInstance.result
                        .then(function (result) {
                            vm.ticketDiscountPromotionDistribution.productGroupId = result.selectedOu.id;
                            vm.ticketDiscountPromotionDistribution.productGroupName = result.selectedOu.name;
                            vm.ticketDiscountPromotionDistribution.categoryId = 0;
                            data.productGroupId = result.selectedOu.id;
                            data.productGroupName = result.selectedOu.name;
                            data.groupCode = result.selectedOu.code;
                            data.categoryId = 0;
                        }).finally(function () {
                        });
                };

                vm.openforCategoryItem = function (data) {
                    vm.lgroup = {
                        locations: [],
                        groups: [],
                        group: false,
                        single: true,
                        productGroupId: data.productGroupId,
                        service: data.productGroupId == 0 ? categoryService.getAllCategoyItems : categoryService.getAllCatgeoriesBasedOnProductGroupId
                    };

                    var modalInstance = $modal.open({
                        templateUrl: "~/App/common/views/lookup/select.cshtml",
                        controller: "tenant.views.common.lookup.select as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.lgroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        if (result.locations.length > 0) {
                            data.categoryId = result.locations[0].id;
                            data.categoryName = result.locations[0].name;
                            vm.ticketDiscountPromotionDistribution.categoryId = result.locations[0].id;
                            vm.ticketDiscountPromotionDistribution.categoryName = result.locations[0].name;
                            data.menuItemId = 0;
                            vm.ticketDiscountPromotionDistribution.menuItemId = 0;
                        }
                    });
                };

                vm.openforMenuItem = function (data) {
                    vm.lgroup = {
                        locations: [],
                        groups: [],
                        group: false,
                        single: true,
                        categoryId: data.categoryId,
                        service: data.categoryId == 0 ? menuService.getAllMenuItems : menuService.getAllMenuItemsBasedOnCatgeoryId
                    };

                    var modalInstance = $modal.open({
                        templateUrl: "~/App/common/views/lookup/select.cshtml",
                        controller: "tenant.views.common.lookup.select as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.lgroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        if (result.locations.length > 0) {
                            data.menuItemId = result.locations[0].id;
                            data.menuItemGroupCode = result.locations[0].name;
                            data.menuItemPortionId = 0;
                            vm.ticketDiscountPromotionDistribution.menuItemId = result.locations[0].id;
                            vm.ticketDiscountPromotionDistribution.menuItemGroupCode = result.locations[0].name;
                        }
                    });
                };

                vm.disabledForLastTab = function (items) {
                    vm.disabledSaveforLastTab = false;
                    vm.disabledSaveforLastTab = !items.length;
                };

                vm.promotioncategorys = [];
                vm.getPromotionCategories = function () {
                    promotioncategoryService.getPromotionCategorys({}).success(function (result) {
                        vm.promotioncategorys = result.items;
                    }).finally(function (result) {
                    });
                };
                vm.getPromotionCategories();

                //Promotion quota

                vm.addPromotionQuota = function () {
                    vm.lgroup = {
                        locations: [],
                        groups: [],
                        group: false,
                        single: false,
                        service: menuService.getNonComboItems
                    };

                    vm.promotionQuotas.push({ locationGroup: vm.lgroup, plants: '*' });
                };

                vm.dateOptionsYear = {
                    formatYear: 'yyyy',
                    'datepicker-mode': "'year'",
                    'min-mode': "year"
                };

                vm.dateOptionsMonth = {
                    formatYear: 'mm',
                    'datepicker-mode': "'month'",
                    'min-mode': "month"
                };

                vm.openQuotaLocation = function (quota) {
                    const modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return angular.copy(quota.locationGroup);
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        var selectedLocations = [];
                        var ok = true;
                        angular.forEach(vm.promotionQuotas, function (qt) {
                            if (qt.id != quota.id && ok) {
                                if (qt.plants === '*') {
                                    abp.notify.error('All PLANTS are selected!');
                                    ok = false;
                                }
                                angular.forEach(qt.locationGroup.locations, function (loc) {
                                    selectedLocations.push(loc.id);
                                });
                            }
                        });

                        if (ok) {
                            angular.forEach(result.locations, function (loc) {
                                if (selectedLocations.indexOf(loc.id) !== -1) {
                                    abp.notify.error('Location ' + loc.name + ' is selected already');
                                    ok = false;
                                }
                            });
                        }
                        

                        if (ok) {
                            quota.locationGroup = angular.copy(result);
                            var locations = [];
                            angular.forEach(quota.locationGroup.locations, function (loc) {
                                if (loc) {
                                    locations.push(loc.name);
                                }
                            });
                            quota.plants = locations.join(",");
                        }
                    });
                };

                $scope.open3 = function (quota) {
                    quota.showDatePicker = !quota.showDatePicker;
                };

                vm.removeQuota = function (index) {
                    vm.promotionQuotas.splice(index, 1);
                };

                vm.showQuotaDetail = function (objId) {
                    $state.go("tenant.plantquota", {
                        id: objId
                    });
                };

                vm.combineProList = [];
                vm.getPromotionsCombine = function () {
                    promotionService.getPromotionsCombine({
                        maxResultCount: 10000,
                        promotionQuotaId: $stateParams.id,
                    }).success(function (result) {
                        vm.combineProList = result;
                    });
                };

                vm.chooseCombinePromotion = function () {
                    if (!vm.promotion.combineAllPro) {

                        var selectedItems = [];
                        if (vm.promotion.combineProValues) {
                            var proIds = vm.promotion.combineProValues.split(',');
                            angular.forEach(proIds, function (id) {

                                angular.forEach(vm.combineProList.items, function (cp) {
                                    if (cp.id == id) {
                                        selectedItems.push(cp);
                                    }
                                });
                            });
                        }

                        vm.lgroup = {
                            locations: selectedItems,
                            groups: [],
                            group: false,
                            single: false,
                            service: function (args) {
                                var params = args || {};
                                params["promotionQuotaId"] = $stateParams.id;
                                return promotionService.getPromotionsCombine(params);
                            }
                        };

                        var modalInstance = $modal.open({
                            templateUrl: "~/App/common/views/lookup/select.cshtml",
                            controller: "tenant.views.common.lookup.select as vm",
                            backdrop: "static",
                            keyboard: false,
                            resolve: {
                                location: function () {
                                    return vm.lgroup;
                                }
                            }
                        });
                        modalInstance.result.then(function (result) {
                            vm.promotion.combineProValues = '';
                            vm.combinePros = '';
                            if (result.locations.length > 0) {
                                var ids = [];
                                var names = [];
                                angular.forEach(result.locations, function (loc) {
                                    if (loc) {
                                        ids.push(loc.id);
                                        names.push(loc.id + ' - ' + loc.name);
                                    }
                                });
                                vm.promotion.combineProValues = ids.join(',');
                                vm.combinePros = names.join(', ');
                            }
                        });
                    }
                };

                vm.combineProCheckChange = function () {
                    if (vm.promotion) {
                        if (vm.promotion.combineAllPro) {
                            vm.promotion.combineProValues = '*';
                            vm.combinePros = '*';
                        }
                        else {
                            vm.promotion.combineProValues = '';
                            vm.combinePros = '';
                        }

                    }
                };

                vm.findDepartmentList = function (value) {
                    vm.deparmentItemList.forEach(function (item) {
                        if (item.departmentGroup == value) {
                            localStorage.departments = JSON.stringify(item.listItem);
                        }
                    });
                    
                };

                vm.getDepartmentListFilter = function () {
                    commonService.getDepartmentsFilter().success(function (result) {
                        vm.deparmentItemList = result;
                    });
                };
                vm.getDepartments = function () {
                    commonService.getDepartments({}).success(function (result) {
                        localStorage.allDepartments = localStorage.departments = JSON.stringify(result);
                       
                    });
                };
                vm.getDepartmentGroups = function () {
                    commonService.getDepartmentGroups({}).success(function (result) {
                        localStorage.deparmentGroups = JSON.stringify(result);
                    });
                };
                vm.changepromotionApplyOnce = function () {
                    var promotionapplyonce = document.getElementById("promotionApplyOnce");
                    if (promotionapplyonce.checked) {
                        vm.demandPromotion.promotionApplyType = vm.refPromotionApplyTypes[0].value;
                    }
                    else {
                        vm.demandPromotion.promotionApplyType = null;
                    }
                };
                vm.changeTicketPromotionApplyOnce = function () {
                    var promotionapplyonce = document.getElementById("ticketPromotionApplyOnce");
                    if (promotionapplyonce.checked) {
                        vm.ticketDiscountPromotion.promotionApplyType = vm.refPromotionApplyTypes[0].value;
                    }
                    else {
                        vm.ticketDiscountPromotion.promotionApplyType = null;
                    }
                };
            }
        ]);
})();