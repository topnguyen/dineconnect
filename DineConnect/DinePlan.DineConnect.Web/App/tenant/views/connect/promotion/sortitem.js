﻿(function() {
    appModule.controller("tenant.views.connect.promotion.sortitem", [
        "$scope", "$uibModalInstance", "abp.services.app.promotion", "params", 
        function ($scope, $modalInstance, promotionService, params) {

            var vm = this;
            vm.saving = false;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.init = function () {
                vm.loading = true;
                params.skipCount = requestParams.skipCount;
                params.maxResultCount = requestParams.maxResultCount;
                params.sorting = requestParams.sorting;
                promotionService.getAll(params).success(function (result) {
                    vm.category=result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.save = function () {
                vm.loading = true;
                vm.cateIds = [];
                angular.forEach(vm.category, function(item) {
                    vm.cateIds.push(item.id);
                });
                promotionService.saveSortOrder(
                   vm.cateIds
                ).success(function (result) {
                    $modalInstance.close();
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.cancel = function() {
                $modalInstance.dismiss();
            };

            vm.init();

        }
    ]);
})();