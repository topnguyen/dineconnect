﻿(function () {
    appModule.controller('tenant.views.connect.promotion.plantquota', [
        '$scope', '$uibModal', "$stateParams", "$state", 'uiGridConstants', 'abp.services.app.connectReport', 'abp.services.app.promotion',
        function ($scope, $modal, $stateParams, $state, uiGridConstants, connectService, promotionService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.payments = [];
            vm.transactions = [];
            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.loading = false;
            vm.disableExport = false;
            vm.tickets = '';
            vm.items = '';
            vm.usedquota = '';

            vm.requestParams = {
                locations: '',
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.$onInit = function () {
                vm.getQuotaDetail();
            }

            vm.gridOptions = {
                showColumnFooter: true,
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: 'Actions',
                        enableSorting: false,
                        width: 50,
                        headerCellTemplate: '<span></span>',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.showDetails(row.entity)"><i class="fa fa-search"></i></button>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Plant'),
                        field: 'plant'
                    },
                    {
                        name: app.localize('TotalQuotaEachPlant'),
                        field: 'quota',
                        cellClass: 'ui-ralign',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    },
                    {
                        name: app.localize('TotalUsed'),
                        field: 'totalUsed',
                        cellClass: 'ui-ralign',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getQuotaDetail();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;
                        vm.getQuotaDetail();
                    });
                },
                data: []
            };

            vm.getQuotaDetail = function () {
                vm.loading = true;
                var lcId = 0;
                if (vm.locationGroup.locations.length) {
                    lcId = vm.locationg.locations[0].id;
                }
                promotionService.getQuotaDetail({
                    promotionQuotaId: $stateParams.id,
                    locationId: lcId,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting
                }).success(function (result) {
                    vm.gridOptions.totalItems = result.quotaDetails.totalCount;

                    for (let i = 0; i < result.quotaDetails.items.length; i++) {
                        if (result.quotaDetails.items[i].price != null)
                            result.quotaDetails.items[i].price = result.quotaDetails.items[i].price.toFixed(vm.decimals);
                    }

                    vm.gridOptions.data = result.quotaDetails.items;

                    vm.quota = result.dashBoardDto.totalAmount;
                    vm.remain = result.dashBoardDto.totalOrderCount;
                    vm.usedquota = result.dashBoardDto.totalItemSold;

                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.exportToExcel = function (dataFormat) {
                if (vm.gridOptions.totalItems === 0) return;
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        var lcId = 0;
                        if (vm.locationGroup.locations.length) {
                            lcId = vm.locationg.locations[0].id;
                        }
                        promotionService.getQuotasExcel({
                            promotionQuotaId: $stateParams.id,
                            locationId: lcId,
                            skipCount: 0,
                            maxResultCount: 999,
                            sorting: vm.requestParams.sorting,
                            exportOutputType: dataFormat,
                            runInBackground: myConfirmation
                        })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });

                    });

            };
            
            vm.showDetails = function (data) {
                debugger;
                $state.go("tenant.quotaticket", {
                    quotaId: $stateParams.id,
                    locationId: data.plantId,
                    plantNo: data.plantNo,
                    plant: data.plant,
                    promotionId: data.promotionId
                });
            };

            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            }


        }
    ]);
})();