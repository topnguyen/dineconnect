﻿(function () {
    appModule.controller('tenant.views.connect.promotion.quotadetail', [
        '$scope', '$uibModal', "$stateParams", 'uiGridConstants', 'abp.services.app.promotion', "abp.services.app.tenantSettings",
        function ($scope, $modal, $stateParams, uiGridConstants, promoService, tenantSettingsService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.payments = [];
            vm.transactions = [];
            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.loading = false;
            vm.disableExport = false;
            vm.tickets = '';
            vm.items = '';
            vm.usedquota = '';

            vm.requestParams = {
                locations: '',
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.decimals = null;
            vm.getDecimals = function () {
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.decimals = result.connect.decimals;
                    }).finally(function () {
                    });
            };

            vm.$onInit = function () {
                vm.getQuotaTickets();
                vm.getDecimals();
            }

            vm.gridOptions = {
                showColumnFooter: true,
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: 'Actions',
                        enableSorting: false,
                        width: 50,
                        headerCellTemplate: '<span></span>',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.showDetails(row.entity)"><i class="fa fa-search"></i></button>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Id'),
                        field: 'ticketId'
                    },
                    {
                        name: app.localize('TicketNo'),
                        field: 'ticketNo'
                    },
                    {
                        name: app.localize('InvoiceNo'),
                        field: 'invoiceNo'
                    },
                    {
                        name: app.localize('TaxInvoiceNo'),
                        field: 'taxInvoiceNo'
                    },
                    {
                        name: app.localize('PlantNo'),
                        field: 'plantNo'
                    },
                    {
                        name: app.localize('PlantName'),
                        field: 'plant'
                    },
                    {
                        name: app.localize('PosNo'),
                        field: 'posNo'
                    },
                    {
                        name: app.localize('SaleMode'),
                        field: 'saleMode'
                    },
                    {
                        name: app.localize('SaleChannel'),
                        field: 'saleChannel'
                    },
                    {
                        name: app.localize('User'),
                        field: 'user'
                    },
                    {
                        name: app.localize('Date'),
                        field: 'dateStr'
                    },
                    {
                        name: app.localize('Time'),
                        field: 'timeStr'
                    },
                    {
                        name: app.localize('Used'),
                        field: 'quotaUsed'
                    },
                    {
                        name: app.localize('BlueCardId'),
                        field: 'blueCardId'
                    },
                    {
                        name: app.localize('TotalAmount'),
                        field: 'totalAmount',
                        cellClass: 'ui-ralign',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >Total: {{col.getAggregationValue() | number:grid.appScope.decimals}}</div>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getQuotaTickets();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;
                        vm.getQuotaTickets();
                    });
                },
                data: []
            };

            // #region Builder

            $scope.builder =
                app.createBuilder(
                    {
                        condition: "AND",
                        rules: [
                            {
                                id: "TicketId",
                                operator: "greater_or_equal",
                                value: 0
                            }
                        ]
                    },
                    angular.fromJson
                    (
                        [
                            {
                                "id": "TicketId",
                                "label": app.localize("Ticket"),
                                "operators": [
                                    "equal",
                                    "less",
                                    "less_or_equal",
                                    "greater",
                                    "greater_or_equal"
                                ],
                                "type": "integer"
                            },
                            {
                                "id": "TicketNo",
                                "label": app.localize("TicketNo"),
                                "type": "string"
                            },
                            {
                                "id": "Plant",
                                "label": app.localize("Plant"),
                                "type": "string"
                            },
                            {
                                "id": "Terminal",
                                "label": app.localize("Terminal"),
                                "type": "string"
                            },
                            {
                                "id": "SaleMode",
                                "label": app.localize("SaleMode"),
                                "type": "string"
                            },
                            {
                                "id": "User",
                                "label": app.localize("User"),
                                "type": "string"
                            },
                            {
                                "id": "BlueCardId",
                                "label": app.localize('BlueCardId'),
                                "type": "string"
                            },
                            {
                                "id": "TotalAmount",
                                "label": app.localize("TotalAmount"),
                                "operators": [
                                    "equal",
                                    "less",
                                    "less_or_equal",
                                    "greater",
                                    "greater_or_equal"
                                ],
                                "type": "double",
                                "validation": {
                                    "min": 0,
                                    "step": 0.1
                                }
                            }
                        ]
                    )
                );

            vm.getQuotaTickets = function () {
                vm.loading = true;
                debugger
                promoService.getQuotaTickets({
                    promotionQuotaId: $stateParams.quotaId,
                    promotionId: $stateParams.promotionId,
                    locationId: $stateParams.locationId,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting
                })
                .success(function (result) {
                    vm.gridOptions.totalItems = result.quotaTickets.totalCount;

                    for (let i = 0; i < result.quotaTickets.items.length; i++) {
                        if (result.quotaTickets.items[i].totalAmount != null)
                            result.quotaTickets.items[i].totalAmount = result.quotaTickets.items[i].totalAmount.toFixed(vm.decimals);

                        debugger;
                        result.quotaTickets.items[i].plant = $stateParams.plant;
                        result.quotaTickets.items[i].plantNo = $stateParams.plantNo;

                        result.quotaTickets.items[i].dateStr = moment(result.quotaTickets.items[i].date).format($scope.format);
                        result.quotaTickets.items[i].timeStr = moment(result.quotaTickets.items[i].date).format('hh:mm:ss');
                    }

                    vm.gridOptions.data = result.quotaTickets.items;

                    vm.tickets = result.dashBoardDto.totalAmount;
                    vm.items = result.dashBoardDto.totalOrderCount;
                    vm.usedquota = result.dashBoardDto.totalItemSold;

                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.exportToExcel = function (dataFormat) {
                if (vm.gridOptions.totalItems === 0) return;
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        promoService.getQuotaTicketsExcel({
                            locationId: $stateParams.locationId,
                            skipCount: 0,
                            maxResultCount: 999,
                            sorting: vm.requestParams.sorting,
                            exportOutputType: dataFormat,
                            runInBackground: myConfirmation
                        })
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                vm.disableExport = false;
                                abp.ui.clearBusy("#MyLoginForm");
                            });

                    });

            };

            vm.showDetails = function (order) {
                $modal.open({
                    templateUrl: '~/App/tenant/views/connect/report/tickets/ticket/ticketModal.cshtml',
                    controller: 'tenant.views.connect.report.ticketModal as vm',
                    backdrop: 'static',
                    resolve: {
                        ticket: function () {
                            return order.ticketId;
                        }
                    }
                });
            };
        }
    ]);
})();