﻿
(function () {
    appModule.controller('tenant.views.connect.master.foreigncurrency.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.foreignCurrency', 'abp.services.app.connectLookup', 'foreigncurrencyId',
        function ($scope, $modalInstance, foreigncurrencyService, payService, foreigncurrencyId) {
            var vm = this;
            vm.saving = false;
            vm.foreigncurrency = null;
            $scope.existall = true;


            vm.save = function () {
                if ($scope.existall == false)
                    return;
                vm.saving = true;

                foreigncurrencyService.createOrUpdateForeignCurrency({
                    foreignCurrency: vm.foreigncurrency
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {

                if (vm.foreigncurrency.id == null) {
                    vm.existall = false;
                    return;
                }

                foreigncurrencyService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.foreigncurrency.id,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.foreigncurrency.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.foreigncurrency.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.refPaymentTypes = [];

            function fillDropDownLocations() {
                payService.getPaymentTypes({}).success(function (result) {
                    vm.refPaymentTypes = result.items;
                    // vm.reflocations.unshift({ value: "0", displayText: app.localize('NotAssigned') });
                });
            }


            function init() {
                fillDropDownLocations();
                foreigncurrencyService.getForeignCurrencyForEdit({
                    Id: foreigncurrencyId
                }).success(function (result) {
                    vm.foreigncurrency = result.foreignCurrency;
                });
            }
            init();
        }
    ]);
})();

