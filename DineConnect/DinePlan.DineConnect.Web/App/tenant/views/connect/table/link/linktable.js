﻿
(function () {
    appModule.controller('tenant.views.connect.table.link.table', [
         '$scope', '$uibModal', '$state', '$stateParams', 'uiGridConstants', 'abp.services.app.connectTableGroup', 'abp.services.app.location',
        function ($scope, $modal,$state, $stateparams, uiGridConstants, groupService,locationService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.groupId = $stateparams.id;
            vm.currentUserId = abp.session.userId;

            vm.group = null;

            vm.groupOptions = {
                title: app.localize("LinkTables"),
                filterPlaceHolder: app.localize("SearchItems"),
                labelAll: app.localize("All"),
                labelSelected: app.localize("Selected"),
                helpMessage: '',
                orderProperty: 'name',
                items: [],
                selectedItems: []
            };
            vm.cancel = function () {
                vm.moveIndex();
            };

            vm.moveIndex = function() {
                $state.go("tenant.tablegroup", {
                    filter: vm.filterText
                });
            }

            vm.save = function() {
                vm.saving = true;
                vm.group.name = vm.group.name.toUpperCase();
                vm.group.connectTables = vm.groupOptions.selectedItems;

                groupService.createOrUpdateConnectTableGroup({
                    connectTableGroup: vm.group,
                    locationGroup: vm.locationGroup
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.moveIndex();
                }).finally(function () {
                    vm.saving = false;
                });
            };


            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
            vm.openLocation = function() {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function() {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function(result) {
                    vm.locationGroup = result;
                });
            };


            function init() {
                vm.saving = true;
                groupService.getConnectTableGroupForEdit({
                    id: vm.groupId
                }).success(function (result) {
                    vm.group = result.connectTableGroup;
                    vm.groupOptions.selectedItems = vm.group.connectTables;
                    vm.groupOptions.items = vm.group.allTables;
                    vm.locationGroup = result.locationGroup;
                });
                vm.saving = false;

            }

            init();

        }]);
})();

