﻿
(function() {
    appModule.controller("tenant.views.connect.table.createTables", [
        "$scope", "$uibModalInstance", "abp.services.app.connectTableGroup", "tableId",
        function($scope, $modalInstance, groupService, groupId) {
            var vm = this;

            vm.saving = false;
            vm.table = null;
            $scope.existall = true;


            vm.save = function() {
                if ($scope.existall == false)
                    return;
                vm.saving = true;
                vm.table.name = vm.table.name.toUpperCase();
                groupService.createOrUpdateConnectTable({
                    table: vm.table
                }).success(function() {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function() {
                    vm.saving = false;
                });
            };

            vm.existall = function() {

                if (vm.table.id == null) {
                    vm.existall = false;
                    return;
                }

                groupService.getAllTables({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: "id",
                    filter: vm.table.id,
                    operation: "SEARCH"
                }).success(function(result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.table.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info("' " + vm.table.id + "' " + app.localize("NameExist"));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function() {
                $modalInstance.dismiss();
            };


            function init() {
                groupService.getConnectTableForEdit({
                    Id: groupId
                }).success(function(result) {
                    vm.table = result.table;
                });
            }

            init();
        }
    ]);
})();