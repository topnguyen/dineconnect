﻿(function() {
    appModule.controller("tenant.views.connect.dineplantax.createOrEditModal", [
        "$scope", "$uibModalInstance", "abp.services.app.dinePlanTax", "dineplantaxId", "abp.services.app.connectLookup",
        function($scope, $modalInstance, dineplantaxService, dineplantaxId, connectLookupService) {
            var vm = this;

            vm.saving = false;
            vm.dineplantax = null;
            $scope.existall = true;
            vm.transactionTypes = [];

            vm.save = function() {
                if ($scope.existall == false)
                    return;
                vm.saving = true;
                vm.dineplantax.taxName = vm.dineplantax.taxName.toUpperCase();
                dineplantaxService.createOrUpdateDinePlanTax({
                    dinePlanTax: vm.dineplantax
                }).success(function() {
                    abp.notify.info(app.localize("SavedSuccessfully"));
                    $modalInstance.close();
                }).finally(function() {
                    vm.saving = false;
                });
            };

            vm.existall = function() {
                if (vm.dineplantax.Id == null) {
                    vm.existall = false;
                    return;
                }

            };

            vm.cancel = function() {
                $modalInstance.dismiss();
            };

            vm.getComboValue = function(item) {
                return parseInt(item.value);
            };

            function init() {

                connectLookupService.getTransactionTypesForCombobox({}).success(function(result) {
                    vm.transactionTypes = result.items;
                });
                dineplantaxService.getDinePlanTaxForEdit({
                    Id: dineplantaxId
                }).success(function(result) {
                    vm.dineplantax = result.dinePlanTax;
                });
            }

            init();
        }
    ]);
})();