﻿(function () {
    appModule.controller('tenant.views.connect.dineplantax.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.dinePlanTax',
        function ($scope, $state, $modal, uiGridConstants, dineplantaxService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.isDeleted = false;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Connect.Master.DinePlanTax.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Connect.Master.DinePlanTax.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Connect.Master.DinePlanTax.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editDinePlanTax(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteDinePlanTax(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            //"      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.create\" ng-click=\"grid.appScope.taxMapping(row.entity)\">" + app.localize("TaxMapping") + "</a></li>" +
                            '      <li><a ng-if="grid.appScope.isDeleted && grid.appScope.permissions.edit" ng-click="grid.appScope.activateItem(row.entity)">' + app.localize('Revert') + '</a></li>' +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('TaxName'),
                        field: 'taxName'
                    },
                    {
                        name: app.localize('TransactionType'),
                        field: 'transactionTypeName'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                dineplantaxService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    location: vm.location,
                    isDeleted: vm.isDeleted
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editDinePlanTax = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createDinePlanTax = function () {
                openCreateOrEditModal(null);
            };

            vm.deleteDinePlanTax = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteDinePlanTaxWarning', myObject.taxName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            dineplantaxService.deleteDinePlanTax({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.taxMapping = function (myObject) {
                $state.go("tenant.dineplantaxdetail", {
                    id: myObject.id,
                    taxName: myObject.taxName
                });
            };

            function openCreateOrEditModal(objId) {
                //var modalInstance = $modal.open({
                //    templateUrl: '~/App/tenant/views/connect/dineplantax/createOrEditModal.cshtml',
                //    controller: 'tenant.views.connect.dineplantax.createOrEditModal as vm',
                //    backdrop: 'static',
                //	keyboard: false,
                //    resolve: {
                //        dineplantaxId: function () {
                //            return objId;
                //        }
                //    }
                //});

                //modalInstance.result.then(function (result) {
                //    vm.getAll();
                //});

                $state.go("tenant.dineplantaxdetail", {
                    id: objId,
                    taxName: null
                });
            }

            vm.exportToExcel = function () {
                dineplantaxService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    location: vm.location,
                    isDeleted: vm.isDeleted
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.activateItem = function (myObject) {
                dineplantaxService.activateItem({
                    id: myObject.id
                }).success(function (result) {
                    abp.notify.success(app.localize("Successfully"));
                    vm.getAll();
                });
            };

            vm.currentLocation = null;

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.currentLocation = result.locations[0];
                        vm.location = result.locations[0].name;
                    }
                    else if (result.groups.length > 0) {
                        vm.currentLocation = result.groups[0];
                        vm.location = result.groups[0].name;
                    }
                    else if (result.locationTags.length > 0) {
                        vm.currentLocation = result.locationTags[0];
                        vm.location = result.locationTags[0].name;
                    }
                    else {
                        vm.location = null;
                        vm.currentLocation = null;
                    }
                });
            };

            vm.clear = function () {
                vm.location = null;
                vm.currentLocation = null;
                vm.filterText = null;
                vm.intiailizeOpenLocation();

                vm.getAll();
            };

            vm.getAll();
        }]);
})();