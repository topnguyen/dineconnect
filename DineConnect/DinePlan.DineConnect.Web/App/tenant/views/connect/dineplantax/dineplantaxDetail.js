﻿(function () {
    appModule.controller("tenant.views.connect.dineplantax.dineplantaxDetail", [
        "$scope", "$filter", "$state", "$stateParams", "$uibModal", "abp.services.app.dinePlanTax", 'abp.services.app.category', "uiGridConstants",
        "abp.services.app.menuItem", "abp.services.app.connectLookup", 'appSession',
        function ($scope, $filter, $state, $stateParams,
            $modal, taxService, categoryService, uiGridConstants, menuService,
            connectLookupService, appSession) {
            var vm = this;

            vm.saving = false;
            vm.dineplantax = null;
            vm.transactionTypes = [];

            vm.edittaxlocationflag = false;
            vm.taxName = null;
            vm.taxlocation = null;
            vm.taxRefId = $stateParams.id;
            vm.futuredataId = $stateParams.futureDataId;
            vm.loading = false;
            vm.sno = 0;
            vm.taxLocationId = null;
            vm.loading = false;
            vm.uilimit = 50;
            vm.futureDataFlag = false;
            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.addPortion = function () {
                var map = {
                    'taxRefId': vm.taxRefId,
                    'dinePlanTaxLocationRefId': 0,
                    'departmentId': "",
                    'categoryId': null,
                    'menuItemId': null
                };
                vm.taxlocation.dinePlanTaxMappings.push(map);
            };
            if (vm.isUndefinedOrNull(vm.futuredataId)) {
                vm.futureDataFlag = false;
            }
            else {
                vm.taxRefId = null;
                vm.futureDataFlag = true;
            }

            vm.removeRow = function (productIndex) {
                var element = vm.taxlocation.dinePlanTaxMappings[productIndex];

                if (vm.taxlocation.dinePlanTaxMappings.length > 1) {
                    vm.taxlocation.dinePlanTaxMappings.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                } else {
                    vm.taxlocation.dinePlanTaxMappings.splice(productIndex, 1);
                    vm.sno = 0;
                    return;
                }
            };

            vm.locationGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Delete'),
                        enableSorting: false,
                        width: 80,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button class=\"btn btn-default btn-xs\"  ng-click=\"grid.appScope.deletetaxdetail(row.entity)\">" +
                            "    <i class=\"fa fa-trash\"></i><span></span></button>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Edit'),
                        enableSorting: false,
                        width: 80,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button  class=\"btn btn-default btn-xs\" ng-click=\"grid.appScope.edittaxdetail(row.entity)\">" +
                            "    <i class=\"fa fa-edit\"></i><span></span></button>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Percentage'),
                        field: 'taxPercentage'
                    },
                    {
                        name: app.localize('View'),
                        enableSorting: false,
                        width: 80,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button class=\"btn btn-default btn-xs\"  ng-click=\"grid.appScope.viewtaxdetail(row.entity)\">" +
                            "    <i class=\"fa fa-eye\"></i><span></span></button>" +
                            "</div>"
                    },
                    {
                        name: app.localize('CloneMapping'),
                        enableSorting: false,
                        width: 150,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button class=\"btn btn-default btn-xs\"  ng-click=\"grid.appScope.clone(row.entity)\">" +
                            "    <i class=\"fa fa-clone\"></i><span></span></button>" +
                            "</div>"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getLocationTaxes();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getLocationTaxes();
                    });
                },
                data: []
            };

            vm.save = function () {
                console.log('datatax');
                vm.dineplantax.taxName = vm.dineplantax.taxName.toUpperCase();
                vm.saving = true;
                taxService.createOrUpdateDinePlanTax({
                    futureDataId: vm.futuredataId,
                    dinePlanTax: vm.dineplantax,
                    dinePlanTaxLocation: vm.taxlocation
                }).success(function (result) {
                    console.log('datatax', result);
                    if (result.id != 0) {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        vm.taxlocation.dinePlanTaxRefId = result.id;
                        if (vm.futureDataFlag) {
                            $state.go('tenant.futuredata');
                        }
                        else {
                            vm.cancel();
                        }
                    }
                    else {
                        abp.notify.error(app.localize('TransactionTypeDuplicate'));
                    }
                  
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go("tenant.dineplantax");
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForCreate();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.openFilterLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.filterLocationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.filterLocationGroup = result;
                    init();
                });
            };
            vm.clearFilterLocation = function () {
                vm.filterLocationGroup = app.createLocationInputForCreate();
                vm.taxRefId = $stateParams.id;
                init();
            }

            vm.clearFilterLocation();

            vm.reftaxesonavailable = [];

            function fillDropDownTaxes() {
                vm.loading = true;
                taxService.getDinePlanTaxForCombobox({}).success(function (result) {
                    vm.reftaxesonavailable = result.items;
                    vm.loading = false;
                });
            }

            vm.refdepartments = [];

            function fillDropDownDepartment() {
                vm.loading = true;
                connectLookupService.getDepartments({}).success(function (result) {
                    vm.refdepartments = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.refcategories = [];

            
            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };

            function fillTransactionTypeCombo() {
                vm.loading = true;
                connectLookupService.getTransactionTypesForCombobox({}).success(function (result) {
                    vm.transactionTypes = result.items;
                    vm.loading = false;
                });
            }
            fillTransactionTypeCombo();



            vm.clone = function (cloneData) {
                var data = {};
                angular.copy(cloneData, data);
                vm.currentLocation = '';
                vm.intiailizeOpenLocation();
                data.id = null;
                vm.dinePlanTaxMappings = [];
                angular.forEach(data.dinePlanTaxMappings, function (value, key) {
                    vm.dinePlanTaxMappings.push({
                        'id': null,
                        'departmentId': value.departmentId,
                        'categoryId': value.categoryId,
                        'menuItemId': value.menuItemId,
                        'categoryName': value.categoryName,
                        'menuItemGroupCode': value.menuItemGroupCode
                    });
                });
                data.dinePlanTaxMappings = vm.dinePlanTaxMappings;
                data.cloneFlag = true;
                data.futureDataSno = 0;
                vm.taxlocation = data;
                vm.tabtwo = true;
            }

            vm.tabtwo = false;
            vm.edittaxdetail = function (data) {
                data.locationGroup.viewOnly = false;
                data.cloneFlag = false;
                vm.taxlocation = data;
                vm.tabtwo = true;
                vm.edittaxlocationflag = true;
                vm.futureDataSno = data.futureDataSno;

                vm.locationGroup = data.locationGroup;
            };

            vm.viewtaxdetail = function (myObject) {
                myObject.locationGroup.viewOnly = true;
                vm.locationGroup = myObject.locationGroup;
                vm.openLocation();
                vm.intiailizeOpenLocation();
            };
            vm.deletetaxdetail = function (data) {
                vm.taxlocation = data;
                vm.loading = true;
                taxService.deleteDinePlanTaxLocation({
                    id: data.id,
                    futureDataId: vm.futuredataId,
                    dinePlanTaxLocation: vm.taxlocation
                }).success(function () {
                    abp.notify.info(app.localize("DeletedSuccessfully"));
                }).finally(function () {
                    vm.loading = false;
                    init();
                });
            };

            vm.saveMapping = function () {
                if (parseFloat(vm.taxlocation.taxPercentage) > 100) {
                    abp.notify.error('Percentage is more than 100');
                    return;
                }
                vm.saving = true;
                vm.taxlocation.dinePlanTaxRefId = vm.dineplantax.id;
                vm.taxlocation.locationGroup = vm.locationGroup;
                taxService.addOrEditDinePlanTaxLocation({
                    futureDataId: vm.futuredataId,
                    dinePlanTax: vm.dineplantax,
                    dinePlanTaxLocation: vm.taxlocation
                }).success(function () {
                    abp.notify.info(app.localize("SavedSuccessfully"));
                }).finally(function () {
                    vm.tabone = true;
                    vm.intiailizeOpenLocation();
                    init();
                    vm.saving = false;
                });
            };

            vm.openforCategoryItem = function (data) {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    service: categoryService.getAllCategoyItems
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        data.categoryId = result.locations[0].id;
                        data.categoryName = result.locations[0].name;
                        data.menuItemId = null;
                    }
                });
            };

            vm.openforMenuItem = function (data) {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    categoryId: data.categoryId,
                    service: data.categoryId == null ? menuService.getAllMenuItems : menuService.getAllMenuItemsBasedOnCatgeoryId
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        data.menuItemId = result.locations[0].id;
                        data.menuItemGroupCode = result.locations[0].name;
                    }
                });
            };

            function init() {
                vm.loading = true;
                taxService.getDinePlanTaxForEdit({
                    Id: vm.taxRefId,
                    FutureDataId: vm.futuredataId,
                    LocationGroupToBeFiltered: vm.filterLocationGroup
                }).success(function (result) {
                    vm.dineplantax = result.dinePlanTax;
                    vm.taxlocation = result.dinePlanTaxLocation;
                    vm.locationGridOptions.data = vm.dineplantax.dinePlanTaxLocations;
                    vm.locationGridOptions.totalItems = vm.dineplantax.dinePlanTaxLocations.length;

                    if (vm.dineplantax.id !== null) {
                        vm.uilimit = null;
                    }
                    else {
                        vm.uilimit = 50;
                    }
                });
                fillDropDownDepartment();
            }
            init();
        }
    ]);
})();