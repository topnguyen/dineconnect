﻿(function () {
    appModule.controller('tenant.views.futuredata.detail', [
        '$scope', "$uibModal", '$state', '$stateParams', 'abp.services.app.futureData', 'abp.services.app.commonLookup', 'abp.services.app.dinePlanTax',
        function ($scope, $modal, $state, $stateParams, futureDataService, commonService, dinePlanTax) {
            var vm = this;
            vm.futureDataId = $stateParams.id;
            vm.futureDataTypes = [];
            vm.screenMenus = [];
            vm.menuItems = [];
            vm.priceTags = [];
            vm.taxes = [];
            vm.changeType = [];
            vm.contentPrice = [];
            vm.minDate = new Date();
            vm.minDate.setDate(vm.minDate.getDate() + 1);

            $('input[name="futureDate"]').daterangepicker({
                locale: {
                    format: 'L'
                },
                singleDatePicker: true,
                showDropdowns: true,
                minDate: vm.minDate
            });

            $('input[name="futureDateTo"]').daterangepicker({
                locale: {
                    format: 'L'
                },
                singleDatePicker: true,
                showDropdowns: true,
                minDate: vm.minDate
            });

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };

            vm.save = function () {
                if (vm.isUndefinedOrNull(vm.futureData.name)) {
                    abp.notify.error('Name is mandatory');
                    return;
                }
                if (vm.isUndefinedOrNull(vm.futureData.futureDateType)) {
                    abp.notify.error('Type is mandatory');
                    return;
                }
                if (vm.isUndefinedOrNull(vm.futureData.futureDate)) {
                    abp.notify.error('Future Data is mandatory');
                    return;
                }
                if (vm.isUndefinedOrNull(vm.futureData.contents) && vm.futureData.futureDateType == 5) {
                    abp.notify.error('Change Type is mandatory');
                    return;
                } if (vm.futureData.value <= 0 && vm.futureData.futureDateType == 5) {
                    abp.notify.error('Value must be greater than 0');
                    return;
                }
                angular.forEach(vm.futureDataTypes, function (data, key)
                {
                    if (data.value == vm.futureData.futureDateType)
                    {
                        vm.contentName = data.displayText;
                    }

                });
                if (vm.isUndefinedOrNull(vm.futureData.contents)) {
                    abp.notify.error(vm.contentName + ' is mandatory');
                    return;
                }
                vm.saving = true;

                futureDataService.createOrUpdateFutureData({
                    futureData: vm.futureData,
                    locationGroup: vm.locationGroup
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $state.go('tenant.futuredata');
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.futuredata');
            };

            function init() {
                vm.saving = true;
                futureDataService.getFutureDataForEdit({
                    id: vm.futureDataId
                }).success(function (result) {
                    console.log("EDIT", result);
                    vm.futureData = result.futureData;                    
                    vm.locationGroup = result.locationGroup;
                    if (vm.futureData.id == null) {
                        vm.futureData.futureDate = null;
                        vm.futureData.futureDateTo = null;
                    }
                    else
                    {
                        if (vm.futureData.futureDate)
                            vm.futureData.futureDate = moment(vm.futureData.futureDate).format("L");
                        if (!vm.isUndefinedOrNull(vm.futureData.futureDateTo)) {
                            vm.futureData.futureDateTo = moment(vm.futureData.futureDateTo).format("L");
                        }
                        else
                        {
                            vm.futureData.futureDateTo = null;
                        }
                    }
                    vm.futureData.isIncrease = "true";
                    if (vm.futureData.futureDateType == 5) {
                        var deseOj = angular.fromJson(vm.futureData.objectContents);
                        vm.futureData.value = deseOj.Value;
                        if (!deseOj.IsIncrease) {
                            vm.futureData.isIncrease = "false"
                        }
                    }
                   
                }).finally(function () {
                    vm.saving = false;
                });

                futureDataService.getFutureDataTypes()
                    .success(function (result) {
                        vm.futureDataTypes = result.items;
                    });
                futureDataService.getEnumChangeTypes()
                    .success(function (result) {
                        vm.changeType = result.items;
                    });
                commonService.getMenuItemForCombobox()
                    .success(function (result) {
                        vm.menuItems = result.items;
                    });

                commonService.getPriceTagForCombobox({
                    futureData: true
                })
                    .success(function (result) {
                        vm.priceTags = result.items;
                    });
                dinePlanTax.getDinePlanTaxForCombobox({
                    futureData: true
                })
                    .success(function (result) {
                        vm.taxes = result.items;
                    });
                commonService.getScreenMenuForCombobox({
                    futureData: true                })
                    .success(function (result) {
                        vm.screenMenus = result.items;
                    });
            }

            init();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };

            vm.onChangedType = function () {
                vm.futureData.contents = null;
            };
        }
    ]);
})();