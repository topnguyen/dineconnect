﻿(function () {
    appModule.controller('tenant.views.futuredata.menuitem.detail', [
        '$scope', "$uibModal", '$state', '$stateParams', "uiGridConstants", 'abp.services.app.futureData',
        function ($scope, $modal, $state, $stateParams, uiGridConstants, futureDataService) {
            var vm = this;
            vm.futureDataId = $stateParams.id;
            vm.menuitems = [];

            vm.filterText = $stateParams.filterText || '';

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.cancel = function () {
                $state.go('tenant.futuredata');
            };

            vm.gridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                enableSorting: true,
                appScopeProvider: vm,
                rowTemplate:
                    "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Id"),
                        field: "id"
                    },
                    {
                        name: app.localize("MenuName"),
                        field: "menuName"
                    },
                    {
                        name: app.localize("PortionName"),
                        field: "portionName"
                    },
                    {
                        name: app.localize("CurrentPrice"),
                        field: "price"
                    },
                    {
                        name: app.localize("FuturePrice"),
                        field: "locationPrice"
                    },
                    {
                        name: app.localize('Edit'),
                        width: 60,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.openTextEditModal(row.entity, rowRenderIndex )" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>' +
                            '</div>'
                    }

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.loadPrices();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.loadPrices();
                    });
                },
                data: []
            };

            vm.loadPrices = function () {
                vm.saving = true;
                futureDataService.getMenuItemPricesForFutureData({
                    futureId: vm.futureDataId,
                    filter: vm.filterText,
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting
                }).success(function (result) {
                    vm.gridOptions.data = result.items;
                    vm.gridOptions.totalItems = result.totalCount;
                })
                    .finally(function () {
                        vm.saving = false;
                    });
            };

            vm.loadPrices();

            vm.createMenuItem = function () {
                var item = {
                    menuName: "",
                    price: 0,
                    locationPrice: 0
                };
                vm.index = vm.menuitems.length;
                vm.openTextEditModal(item, vm.index);
            };

            vm.openTextEditModal = function (text, index) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/futuredata/editPriceModal.cshtml',
                    controller: 'tenant.views.futuredata.menuitem.editPriceModal as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        futureDataId: function () {
                            return vm.futureDataId;
                        },
                        allItems: function () {
                            return vm.gridOptions.data;
                        },
                        initialText: function () {
                            return text;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.loadPrices();
                });
            };

            vm.importPrices = function () {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/futuredata/importItemModal.cshtml',
                    controller: 'tenant.views.futuredata.menuitem.importItemModal as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        futureDataId: function () {
                            return vm.futureDataId;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.loadPrices();
                });
            };
        }
    ]);
})();