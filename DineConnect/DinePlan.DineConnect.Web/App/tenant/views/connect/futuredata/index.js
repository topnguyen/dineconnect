﻿(function () {
    appModule.controller('tenant.views.futuredata.index', ['$rootScope', '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.futureData',
        function ($rootScope, $scope, $state, $modal, uiGridConstants, futureDataService) {
            var vm = this;
            $rootScope.settings.layout.pageSidebarClosed = false;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.futureDate = null;
            vm.isDeleted = false;
            vm.futureDataTypes = [];

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Connect.FutureData.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Connect.FutureData.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Connect.FutureData.Delete')
            };

            $scope.formats = ['YYYY-MM-DD', 'DD-MM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            $('input[name="futureDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true
            });

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.edit" ng-click="grid.appScope.editFutureData(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.delete" ng-click="grid.appScope.deleteFutureData(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.isDeleted && grid.appScope.permissions.edit" ng-click="grid.appScope.activateItem(row.entity)">' + app.localize('Revert') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('Type'),
                        field: 'futureDateType',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            //'  <span ng-show="row.entity.futureDateType == 1" >' + app.localize('MenuItem') + '</span>' +
                            '  <span ng-show="row.entity.futureDateType == 2" >' + app.localize('PriceTag') + '</span>' +
                            '  <span ng-show="row.entity.futureDateType == 3" >' + app.localize('Tax') + '</span>' +
                            '  <span ng-show="row.entity.futureDateType == 4" >' + app.localize('ScreenMenu') + '</span>' +
                            '  <span ng-show="row.entity.futureDateType == 5" >' + app.localize('MenuItemPriceFuture') + '</span>' +
                            '</div>'
                    },
                    {
                        name: app.localize('FutureDate'),
                        field: 'futureDate',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD\'',
                        minWidth: 100
                    },
                    {
                        name: app.localize('Operations'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <button class=" btn-default" ng-if=\"row.entity.futureDateType != 5\" ng-click=\"grid.appScope.openTypePage(row.entity)\">' + app.localize('Open') + '</button>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Status'),
                        field: 'status',
                    },

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            function openDetailFutureDate(objId) {
                $state.go("tenant.detailfuturedata", {
                    id: objId
                });
            }
            vm.editFutureData = function (myObj) {
                openDetailFutureDate(myObj.id);
            };
            vm.createFutureData = function () {
                openDetailFutureDate(null);
            };

            vm.getAll = function () {
                vm.loading = true;
                futureDataService.getFutureDatas({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    location: vm.currentLocation ? vm.currentLocation.name : null,
                    futureDate: vm.futureDate,
                    isDeleted: vm.isDeleted,
                    type: vm.futureDateType,
                    location: vm.location,
                    locationGroup: vm.locationGroup
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.deleteFutureData = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteFutureDataWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            futureDataService.deleteFutureData({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.exportToExcel = function () {
                futureDataService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.currentLocation = null;
            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.currentLocation = result.locations[0];
                        vm.location = result.locations[0].name;
                    }
                    else if (result.groups.length > 0) {
                        vm.currentLocation = result.groups[0];
                        vm.location = result.groups[0].name;
                    }
                    else if (result.locationTags.length > 0) {
                        vm.currentLocation = result.locationTags[0];
                        vm.location = result.locationTags[0].name;
                    }
                    else {
                        vm.location = null;
                        vm.currentLocation = null;
                    }
                });
            };
            vm.clear = function () {
                vm.location = null;
                vm.currentLocation = null;
                vm.filterText = null;
                vm.futureDate = null;
                vm.isDeleted = false;
                vm.futureDateType = null;
                vm.intiailizeOpenLocation();

                vm.getAll();
            };

            vm.activateItem = function (myObject) {
                futureDataService.activateItem({
                    id: myObject.id
                }).success(function (result) {
                    abp.notify.success(app.localize("Successfully"));
                    vm.getAll();
                });
            };

            vm.getAll();

            vm.openTypePage = function (data) {
                if (data.status == "Updated" && data.entityId > 0) {
                    abp.notify.error('Cant able to Open, Future Data is already updated');
                    return;
                }
                switch (data.futureDateType) {
                    case 2:
                        if (data.contents)
                            $state.go("tenant.detailpricetag", { id: data.contents, futureDataId: data.id });
                        else
                            $state.go("tenant.pricetag");
                        break;
                    case 3:
                        if (data.contents)
                            $state.go("tenant.dineplantaxdetail", { id: data.contents, futureDataId: data.id });
                        else
                            $state.go("tenant.dineplantaxdetail", { futureDataId: data.id });
                        break;
                    case 4:
                        if (data.contents)
                            $state.go("tenant.detailscreenmenu", { id: data.contents, futureDataId: data.id });
                        else
                            $state.go("tenant.detailscreenmenu", { id: null, futureDataId: data.id });

                        break;
                    default:
                        break;
                }
            };

            init();
            function init() {
                futureDataService.getFutureDataTypes()
                    .success(function (result) {
                        vm.futureDataTypes = result.items;
                    });
            }

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };
        }]);
})();