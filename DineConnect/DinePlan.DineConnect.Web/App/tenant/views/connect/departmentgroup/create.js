﻿
(function () {
    appModule.controller('tenant.views.connect.departmentgroup.create', [
        '$scope', '$state',  '$stateParams', 'abp.services.app.department', 
        function ($scope, $state,  $stateParams, departmentService) {
            var vm = this;

            vm.departmentgroupId = $stateParams.id;
            vm.saving = false;
            vm.departmentgroup = null;

            vm.save = function () {
                vm.saving = true;
                vm.departmentgroup.code = vm.departmentgroup.code.toUpperCase();
                vm.departmentgroup.name = vm.departmentgroup.name.toUpperCase();
                departmentService.createOrUpdateDepartmentGroup({
                    departmentGroup: vm.departmentgroup
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.departmentgroup');
            };

            function init() {
                
                departmentService.getDepartmentGroupForEdit({
                    Id: vm.departmentgroupId
                }).success(function (result) {
                    vm.departmentgroup = result.departmentGroup;
                });
            }
            init();
        }
    ]);
})();

