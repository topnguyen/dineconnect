﻿
(function () {
    appModule.controller('tenant.views.connect.departmentgroup.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.department',
        function ($scope, $state, $modal, uiGridConstants, departmentService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.isDeleted = false;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Connect.Master.DepartmentGroup.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Connect.Master.DepartmentGroup.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Connect.Master.DepartmentGroup.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editDepartmentGroup(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteDepartmentGroup(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            '      <li><a ng-if="grid.appScope.isDeleted && grid.appScope.permissions.edit" ng-click="grid.appScope.activateItem(row.entity)">' + app.localize('Revert') + '</a></li>' +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Id'),
                        field: 'id'
                    },
                    {
                        name: app.localize('Code'),
                        field: 'code'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                       
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };



            vm.getAll = function () {
                vm.loading = true;
                departmentService.getAllDepartmentGroup({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    isDeleted: vm.isDeleted
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editDepartmentGroup = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createDepartmentGroup = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteDepartmentGroup = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteDepartmentGroupWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            departmentService.deleteDepartmentGroup({
                                id: myObject.id
                            }).success(function (result) {
                                if (result == true) {
                                    abp.notify.error('Department Group is already Mapped with department');
                                    return;
                                }
                                else
                                {
                                    vm.getAll();
                                    abp.notify.success(app.localize('SuccessfullyDeleted'));
                                }
                                
                            });
                        }
                    }
                );
            };

            vm.activateItem = function (myObject) {
                departmentService.activateDepartmentGroupItem({
                    id: myObject.id
                }).success(function (result) {
                    abp.notify.success(app.localize("Successfully"));
                    vm.getAll();
                });
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.createdepartmentgroup", {
                    id: objId
                });
            }
            
            vm.exportToExcel = function () {
                departmentService.getAllDepartmentGroupToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            
            vm.getAll();
        }]);
})();

