﻿(function () {
    appModule.controller('tenant.views.connect.pricetag.editPriceModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.priceTag', 'tagId', 'allItems', 'initialText', 'futureDataId',
        function ($scope, $modalInstance, tagService, tagId, allItems, initialText, futureDataId) {
            var vm = this;

            vm.allItems = allItems;
            vm.currentText = initialText;
            vm.editingText = angular.extend({}, vm.currentText);
            vm.tagId = tagId;
            vm.futureDataId = futureDataId;
            vm.currentIndex = 0;
            vm.saving = false;

            //Calculating index of currentText in allItems
            for (var i = 0; i < allItems.length; i++) {
                if (vm.allItems[i] == vm.currentText) {
                    vm.currentIndex = i;
                    break;
                }
            }

            function save(close) {
                function executeAfterAction() {
                    if (close) {
                        $modalInstance.close();
                    } else {
                        //Go to next
                        vm.currentIndex++;
                        if (vm.allItems.length <= vm.currentIndex) {
                            $modalInstance.close();
                            return;
                        }

                        vm.currentText = vm.allItems[vm.currentIndex];
                        vm.editingText = angular.extend({}, vm.currentText);
                    }
                }

                if (vm.currentText.tagPrice == vm.editingText.tagPrice) {
                    executeAfterAction();
                    return;
                }

                vm.saving = true;
                tagService.updateTagPrice({
                    TagId: vm.tagId,
                    Price: vm.editingText.tagPrice,
                    PortionId: vm.editingText.id,
                    futureDataId: vm.futureDataId
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.currentText.tagPrice = vm.editingText.tagPrice;
                    executeAfterAction();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.saveAndNext = function () {
                save(false);
            }

            vm.saveAndClose = function () {
                save(true);
            }

            vm.previous = function () {
                if (vm.currentIndex <= 0) {
                    return;
                }

                vm.currentIndex--;
                vm.currentText = vm.allItems[vm.currentIndex];
                vm.editingText = angular.extend({}, vm.currentText);
            };

            vm.targetValueKeyPress = function ($event) {
                if ($event.keyCode == 13) {
                    vm.saveAndNext();
                } else if ($event.keyCode == 37) {
                    vm.previous();
                } else if ($event.keyCode == 39) {
                    vm.saveAndNext();
                }
            }

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();