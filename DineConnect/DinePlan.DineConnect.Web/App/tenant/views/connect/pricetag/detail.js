﻿(function () {
    appModule.controller("tenant.views.connect.pricetag.detail", [
        "$scope", "$state", "$stateParams", "$uibModal", "abp.services.app.priceTag", "abp.services.app.commonLookup", "uiGridConstants",
        function ($scope, $state, $stateParams, $modal, tagService, commonService, uiGridConstants) {
            var vm = this;
            vm.tagId = $stateParams.id;
            vm.futuredataId = $stateParams.futureDataId;
            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });

            var nonFilteredData = [];
            vm.filterText = $stateParams.filterText || "";
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.loading = false;

            vm.gridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                appScopeProvider: vm,
                useExternalPagination: true,
                useExternalSorting: true,
                columnDefs: [
                    {
                        name: app.localize("Id"),
                        field: "id",
                        minWidth: 70
                    },
                    {
                        name: app.localize("MenuName"),
                        field: "menuName",
                        minWidth: 140
                    },
                    {
                        name: app.localize("PortionName"),
                        field: "portionName",
                        minWidth: 140
                    },
                    {
                        name: app.localize("Price"),
                        field: "price",
                        minWidth: 140
                    },
                    {
                        name: app.localize("TagPrice"),
                        field: "tagPrice",
                        minWidth: 140
                    },
                    {
                        name: app.localize("LocationPrice"),
                        minWidth: 140,
                        cellTemplate:
                         '<div class=\"ui-grid-cell-contents\">' +
                             '  <button class=" btn-default" ng-click=\"grid.appScope.openLocationPriceModal(row.entity)\">' + app.localize('LocationPrice') + '</button>' +
                            '</div>'
                    },
                    {
                        name: app.localize("Edit"),
                        width: 60,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.openTextEditModal(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Edit") + "\"><i class=\"fa fa-edit\"></i></button>" +
                            "</div>"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }

                        vm.loadMenu();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.loadMenu();
                    });
                },
                data: []
            };
            vm.openLocationPriceModal = function (data)
            {
                vm.pricetagDefintionId = data.priceTagDefinitionId;
                if (data.tagPrice == 0 && data.priceTagDefinitionId == 0) {
                    vm.saving = true;
                    tagService.updateTagPriceInPricetagDefinition({
                        TagId: data.tagId,
                        Price: data.tagPrice,
                        PortionId: data.id
                    }).success(function (result) {
                        vm.pricetagDefintionId = result;
                    }).finally(function () {
                        $state.go("tenant.pricetaglocationprice", {
                            id: data.tagId,
                            pricetagDefinitionId: vm.pricetagDefintionId
                        });
                        vm.saving = false;
                    });
                }
                else {

                    $state.go("tenant.pricetaglocationprice", {
                        id: data.tagId,
                        pricetagDefinitionId: vm.pricetagDefintionId
                    });
                }
            }
            vm.openTextEditModal = function (text) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/pricetag/editPriceModal.cshtml",
                    controller: "tenant.views.connect.pricetag.editPriceModal as vm",
                    resolve: {
                        tagId: function () {
                            return vm.tagId;
                        },
                        allItems: function () {
                            return vm.gridOptions.data;
                        },
                        initialText: function () {
                            return text;
                        },
                        futureDataId: function () {
                            return vm.futuredataId;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.loadMenu();
                });
            };

            vm.applyFilters = function () {
                if (!vm.filterText) {
                    vm.gridOptions.data = nonFilteredData;
                }

                var filterText = vm.filterText.trim().toLocaleLowerCase();
                vm.gridOptions.data = _.filter(nonFilteredData, function (text) {
                    if (vm.targetValueFilter == "EMPTY" && text.targetValue) {
                        return false;
                    }

                    return (text.menuName && text.menuName.toLocaleLowerCase().indexOf(filterText) >= 0) ||
                        (text.portionName && text.portionName.toLocaleLowerCase().indexOf(filterText) >= 0);
                });
            };

            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/pricetag/importModal.cshtml",
                    controller: "tenant.views.connect.pricetag.importModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        name: function () {
                            return objId;
                        },
                        futureDataId: function () {
                            return vm.futuredataId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.loadMenu();
                });
            }

            vm.importPriceTag = function () {
                openCreateOrEditModal(vm.priceTag.name);
            };
            vm.exportPriceTag = function () {
                vm.loading = true;
                tagService.getImportTemplate(vm.priceTag.name)
                    .success(function (result) {
                        app.downloadTempFile(result);
                        abp.ui.clearBusy("#MyLoginForm");
                    }).finally(function () {
                        vm.loading = false;
                    });
            };
            vm.loadMenu = function () {
                if (vm.tagId > 0) {
                    vm.loading = true;
                    tagService.getMenuPricesForTag({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        tagId: vm.tagId,
                        filter: vm.filterText,
                        futureDataId: vm.futuredataId
                    }).success(function (result) {
                        nonFilteredData = result.items;
                        vm.gridOptions.totalItems = result.totalCount;
                        vm.applyFilters();
                    }).finally(function () {
                        vm.loading = false;
                    });
                }

                if (vm.tagId > 0) {
                    vm.loading = true;
                    tagService.getPriceTagForEdit({
                        id: vm.tagId
                    }).success(function (result) {
                        vm.priceTag = result.priceTag;
                    }).finally(function () {
                        vm.loading = false;
                    });
                }
            };

            function initializeFilters() {
                commonService.getLocationsForLoginUser({}).success(function (result) {
                    vm.locations = result.items;
                });

                function reloadWhenChange(variableName) {
                    $scope.$watch(variableName, function (newValue, oldValue) {
                        if (newValue == oldValue) {
                            return;
                        }
                        $state.go("tenant.locationmenuprice", {
                            location: vm.locationId,
                            filterText: vm.filterText
                        }
                        );
                    });
                }

                reloadWhenChange("vm.locationId");
            }

            vm.loadMenu();
            initializeFilters();
        }
    ]);
})();