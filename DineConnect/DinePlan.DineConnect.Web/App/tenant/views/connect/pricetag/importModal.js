﻿(function () {
    appModule.controller('tenant.views.connect.pricetag.importModal', [
        '$scope', 'appSession', '$uibModalInstance', 'FileUploader', 'name', 'abp.services.app.priceTag', 'futureDataId',
        function ($scope, appSession, $uibModalInstance, fileUploader, name, priceTagService, futureDataId) {
            var vm = this;
            
            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };
            if (vm.isUndefinedOrNull(futureDataId)) {
                vm.futureDataId = 0;
            }
            else
            {
                vm.futureDataId = futureDataId;
            }
            vm.uploader = new fileUploader({
                url: abp.appPath + 'Import/ImportPriceTag?futureDataId=' + vm.futureDataId,
                queueLimit: 1,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        console.log("item : " + item);
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        console.log("Type : " + type);
                        if ('|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ImportTemplate_Warn_FileType'));
                            return false;
                        }
                        return true;
                    }
                }]
            });
            vm.importpath = function () {
                vm.loading = true;
                priceTagService.getImportTemplate(name)
                    .success(function (result) {
                        vm.loading = false;
                        app.downloadTempFile(result);
                    });
            };

            vm.save = function () {
                vm.loading = true;
                vm.uploader.uploadAll();
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.error != null) {
                    abp.message.warn(response.error.message);
                    $uibModalInstance.close();
                    vm.loading = false;
                    return;
                }
                abp.notify.info(app.localize("Finished"));
                $uibModalInstance.close();
                vm.loading = false;
            };
        }

    ]);
})();
