﻿(function () {
    appModule.controller("tenant.views.connect.menu.pricetag.create",
        [
            "$scope", "$state", "$uibModal", "$stateParams", "abp.services.app.priceTag", "abp.services.app.location",
            "abp.services.app.connectLookup",
            function ($scope, $state, $modal, $stateParams, pricetagService, locationService, loopupService) {
                var vm = this;
                vm.saving = false;
                vm.pricetag = null;
                vm.tagId = $stateParams.id;



                vm.openLocation = function () {
                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };


                $scope.existall = true;

                vm.save = function () {
                    vm.saving = true;
                    pricetagService.createOrUpdatePriceTag({
                        priceTag: vm.pricetag,
                        locationGroup: vm.locationGroup
                    }).success(function () {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        vm.cancel();
                    }).finally(function () {
                        vm.saving = false;
                    });
                };

                vm.cancel = function () {
                    $state.go("tenant.pricetag");
                };


                function init() {
                    pricetagService.getPriceTagForEdit({
                        id: vm.tagId
                    }).success(function (result) {
                        vm.pricetag = result.priceTag;
                        vm.locationGroup = result.locationGroup;
                    });
                }

                init();
            }
        ]);
})();