﻿
(function () {
    appModule.controller('tenant.views.connect.company.changeProfilePicture', [
        '$scope', 'appSession', '$uibModalInstance', 'FileUploader', 'abp.services.app.company', 'companyId',
        function ($scope, appSession, $uibModalInstance, fileUploader, companyService, companyId) {
            var vm = this;

            vm.uploader = new fileUploader({
                url: abp.appPath + 'Profile/ChangeCompanyProPicture',
                    formData: [{ id: companyId }],
                    queueLimit: 1,
                    filters: [{
                        name: 'imageFilter',
                        fn: function (item, options) {
                            //File type check
                            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                            if ('|jpg|jpeg|'.indexOf(type) === -1) {
                                abp.message.warn(app.localize('ProfilePicture_Warn_FileType'));
                                return false;
                            }
                          

                            //File size check
                            if (item.size > 30720) //3000KB
                            {
                                abp.message.warn(app.localize('ProfilePicture_Warn_SizeLimit'));
                                return false;
                            }

                        return true;
                    }
                }]
            });


            vm.save = function () {
                vm.uploader.uploadAll();
            };

            vm.uploader.onBeforeUploadItem = function (fileitem) {
                fileitem.formData.push({ data: vm.testdata });
                fileitem.formData.push({ compid: vm.compid });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.success) {
                    $uibModalInstance.close(response.result);
                } else {
                    abp.message.error(response.error.message);
                    $uibModalInstance.close();
                    return;
                }
            };
        }
    ]);
})();