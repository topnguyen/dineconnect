﻿(function () {
    appModule.controller('tenant.views.connect.company.detail', [
        "$scope", "$uibModal", "$state", "$stateParams", 'abp.services.app.company',
        function ($scope, $modal, $state, $stateParams, companyService) {
            var vm = this;
            vm.companyId = $stateParams.id;
            vm.saving = false;
            vm.company = null;
            $scope.existall = true;

            vm.save = function () {
                if ($scope.existall === false)
                    return;
                vm.saving = true;
                vm.company.name = vm.company.name.toUpperCase();
                vm.company.code = vm.company.code.toUpperCase();

                companyService.createOrUpdateCompany({
                    company: vm.company
                }).success(function () {

                    abp.notify.info(app.localize('SavedSuccessfully'));

                    $state.go("tenant.company");
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {
                if (vm.company.name === null) {
                    vm.existall = false;
                    return;
                }

                companyService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'name',
                    filter: vm.company.name,
                    operation: 'SEARCH'
                }).success(function (result) {
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.company.id === null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.company.name + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $state.go("tenant.company");
            };

            vm.changePicture = function (myObject) {
                if (vm.company.code === null || vm.company.code === "") {
                    abp.notify.warn(app.localize('CompanyCodeErr'));
                    abp.m.warn(app.localize('CompanyCodeErr'));
                    return;
                }

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/company/changeProfilePicture.cshtml',
                    controller: 'tenant.views.connect.company.changeProfilePicture as vm',
                    backdrop: 'static',
                    resolve: {
                        companyId: function () {
                            if (myObject === null) {
                                myObject = vm.company.code;
                            }
                            else
                                return myObject;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    if (result !== null) {
                        vm.company.companyProfilePictureId = result;
                    }
                    else {
                        companyService.getCompanyForEdit({
                            Id: myObject
                        }).success(function (result) {
                            vm.company.companyProfilePictureId = result.company.companyProfilePictureId;
                        });
                    }
                });
            };

            function init() {
                companyService.getCompanyForEdit({
                    Id: vm.companyId
                }).success(function (result) {
                    vm.company = result.company;
                    if (vm.company.id === null)
                        vm.company.govtApprovalId = "";
                });
            }
            init();
        }
    ]);
})();