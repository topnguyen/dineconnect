﻿(function () {
    appModule.controller('tenant.views.connect.company.index', [
        '$scope', '$state', 'uiGridConstants', 'abp.services.app.company',
        function ($scope, $state, uiGridConstants, companyService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.isDeleted = false;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Connect.Master.Company.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Connect.Master.Company.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Connect.Master.Company.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editCompany(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteCompany(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            '      <li><a ng-if="grid.appScope.isDeleted && grid.appScope.permissions.edit" ng-click="grid.appScope.activateItem(row.entity)">' + app.localize('Activate') + '</a></li>' +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Id'),
                        field: 'id'
                    },
                    {
                        name: app.localize('Code'),
                        field: 'code'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('Address 1'),
                        field: 'address1'
                    },
                    {
                        name: app.localize('State'),
                        field: 'state'
                    },
                    {
                        name: app.localize('City'),
                        field: 'city'
                    },
                    {
                        name: app.localize('PhoneNumber'),
                        field: 'phoneNumber'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                companyService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    isDeleted: vm.isDeleted
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editCompany = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createCompany = function () {
                openCreateOrEditModal(null);
            };

            vm.deleteCompany = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteCompanyWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            companyService.deleteCompany({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.activateItem = function (myObject) {
                companyService.activateItem({
                    id: myObject.id
                }).success(function (result) {
                    abp.notify.success(app.localize("Successfully"));
                    vm.getAll();
                });
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.companydetail", {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                companyService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.getAll();
        }]);
})();