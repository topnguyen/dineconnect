﻿(function () {
    appModule.controller('tenant.views.connect.master.tillaccount.create', [
         '$scope', '$state', '$uibModal', '$stateParams', 'abp.services.app.tillAccount', "abp.services.app.connectLookup", "abp.services.app.location",
        function ($scope, $state, $modal, $stateParams, tillaccountService, connectService, locationService) {
            var vm = this;
            
            vm.saving = false;
            vm.tillaccount = null;
            vm.processors = [];
            vm.accountId = $stateParams.id;
            vm.languageDescriptionType = 15;

            vm.save = function () {
                vm.saving = true;
                tillaccountService.createOrUpdateTillAccount({
                    tillAccount: vm.tillaccount,
                    locationGroup: vm.locationGroup
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $state.go('tenant.tillaccount');
            };

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            }
            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };

            vm.openLanguageDescriptionModal = function () {
                vm.languageDescription = {
                    id: vm.accountId,
                    name: vm.tillaccount.name,
                    languageDescriptionType: vm.languageDescriptionType
                };
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/languageDescription/languageDescriptionModal.cshtml",
                    controller: "tenant.views.connect.languageDescription.languageDescriptionModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        languagedescription: function () {
                            return vm.languageDescription;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });
            }

            function init() {
                tillaccountService.getTillAccountForEdit({
                    id: vm.accountId
                }).success(function (result) {
                    vm.tillaccount = result.tillAccount;
                    vm.locationGroup = result.locationGroup;
                });

                connectService.getTillAccountTypes({}).success(function (result) {
                    vm.processors = result.items;
                });
            }

            init();
        }
    ]);
})();

