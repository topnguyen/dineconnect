﻿(function () {
    appModule.controller('tenant.views.connect.menu.day', [
        '$scope', '$state', '$stateParams', 'abp.services.app.menuItem', 'abp.services.app.category',
        function ($scope, $state, $stateParams, menuService, categoryService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.today = moment();
            vm.menuItemId = $stateParams.id;
            vm.menuitem = null;
            vm.categories = [];
            vm.dateSelected = [];
            vm.removeRow = function (productIndex) {
                vm.menuitem.portions.splice(productIndex, 1);
            }

            vm.addPortion = function () {
                vm.menuitem.portions.push({ 'name': "", 'multiplier':0, 'price': 0 });

               
            }

            vm.portionLength = function () {
                return true;
            }

            vm.save = function () {
                console.log(vm.menuitem);
                vm.saving = true;
                menuService.createOrUpdateMenuItem({
                    menuItem: vm.menuitem
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $state.go('tenant.menuitem');
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $state.go('tenant.menuitem');
            };

            

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };

            vm.openforCategoryItem = function () {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    service: categoryService.getAllCategoyItems
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.menuitem.categoryId = result.locations[0].id;
                        vm.menuitem.categoryName = result.locations[0].name;
                    }
                });
            };
           
            function init() {
                
                menuService.getMenuItemForEdit({
                    id: vm.menuItemId
                }).success(function (result) {
                    vm.menuitem = result.menuItem;
                });
            }
            init();
        }
    ]);
})();