﻿(function () {
    appModule.controller('tenant.views.connect.master.location.createOrEditModal', [
        '$scope', "$uibModal", '$state', '$stateParams', 'abp.services.app.location', 'abp.services.app.locationGroup',
        'abp.services.app.locationTag', 'abp.services.app.urbanPiperIntegrate',
        function ($scope, $modal, $state, $stateParams, locationService, locationGroupService, locationTagService, ubService) {
            var vm = this;

            vm.requestlocations = '';
            vm.saving = false;
            vm.location = null;
            vm.locationBranch = null;
            $scope.existall = true;
            vm.defaultCompanyId = null;
            vm.datachangeallowed = true;
            vm.languageDescriptionType = 12;
            locationId = $stateParams.id;

            vm.tallyIntegrationFlag = abp.setting.getBoolean("App.House.TallyIntegrationFlag");
            vm.currentLanguage = abp.localization.currentLanguage;

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            $scope.settings = {
                dropdownToggleState: false,
                time: {
                    fromHour: '06',
                    fromMinute: '30',
                    toHour: '23',
                    toMinute: '30'
                },
                noRange: false,
                format: 24,
                noValidation: true
            };

            vm.urbanPiperEnabled = abp.features.isEnabled('DinePlan.DineConnect.Connect.UrbanPiper');
            vm.isHouseEnabled = abp.features.isEnabled('DinePlan.DineConnect.House');
            vm.isFranchiseEnabled = abp.features.isEnabled('DinePlan.DineConnect.Connect.Franchise');
            vm.isAddonEnabled = abp.features.isEnabled('DinePlan.DineConnect.Addons');
            vm.istouchenabled = abp.features.isEnabled('DinePlan.DineConnect.Touch');

            $scope.minDate = moment().add(-10, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.removeSchedule = function (productIndex) {
                vm.location.schedules.splice(productIndex, 1);
            };

            vm.addSchedule = function () {
                if (!vm.scname || 0 === vm.scname.length) {
                    return;
                }
                if ($scope.settings.time.fromHour > $scope.settings.time.toHour
                    || ($scope.settings.time.fromHour === $scope.settings.time.toHour && $scope.settings.time.fromMinute >= $scope.settings.time.toMinute)) {
                    abp.notify.error(app.localize("WrongDateRange"));
                    return;
                }
                vm.location.schedules.push({
                    'startHour': $scope.settings.time.fromHour,
                    'endHour': $scope.settings.time.toHour,
                    'startMinute': $scope.settings.time.fromMinute,
                    'endMinute': $scope.settings.time.toMinute,
                    'name': vm.scname,
                    'locationId': locationId
                });
                vm.scname = "";
            };

            vm.syncMenuItems = function () {
                vm.saving = true;
                ubService.pushMenuItemsBackground({
                    location: vm.location.code
                }).success(function () {
                    abp.notify.info(app.localize("Done"));
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.syncStoreItems = function () {
                vm.saving = true;
                ubService.updateItems(
                    vm.location.id
                ).success(function () {
                    abp.notify.info(app.localize("Done"));
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.pushLocation = function () {
                vm.saving = true;
                ubService.updateStore(vm.location.id).success(function () {
                    abp.notify.info(app.localize("Done"));
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.saveReceipt = function () {
                vm.save(true);
            };
            vm.save = function (saveReceiptOnly) {
                if ($scope.existall == false)
                    return;

                existflag = false;

                angular.forEach(vm.requestlocations, function (value, key) {
                    if (value.id == vm.location.defaultRequestLocationRefId) {
                        existflag = true;
                    }
                });

                if ((vm.location.transferRequestLockHours > 0 && vm.location.transferRequestGraceHours > 0) && (vm.location.transferRequestGraceHours <= vm.location.transferRequestLockHours)) {
                    abp.notify.warn(app.localize("TransferRequestGraceHoursAlert"));
                    return;

                }

                if ((vm.location.transferRequestLockHours == 0 && vm.location.transferRequestGraceHours > 0)) {
                    abp.notify.warn(app.localize("TransferRequestLockHoursAlert"));
                }


                if (existflag == false && vm.location.defaultRequestLocationRefId != null && vm.location.defaultRequestLocationRefId > 0) {
                    abp.notify.warn("DefaultLocationError");
                    return;
                }

                vm.filterlocation = [];
                angular.forEach(vm.requestlocations, function (requestvalue, key) {
                    insertflag = true;
                    vm.filterlocation.some(function (filtervalue, key) {
                        if (filtervalue.id == requestvalue.id) {
                            insertflag = false;
                            return false;
                        }
                    });
                    if (insertflag == true)
                        vm.filterlocation.push(requestvalue);
                });

                vm.saving = true;
                vm.location.name = vm.location.name.toUpperCase();
                vm.location.code = vm.location.code.toUpperCase();
                if (!vm.isUndefinedOrNull(vm.locationBranch.code)) {
                    vm.locationBranch.code = vm.locationBranch.code.toUpperCase();
                }
                if (!vm.isUndefinedOrNull(vm.locationBranch.name)) {
                    vm.locationBranch.name = vm.locationBranch.name.toUpperCase();
                }
                if (saveReceiptOnly) {
                    locationService.updateLocationReceipt({
                        location: vm.location,
                        locationBranch: vm.locationBranch,
                        requestLocations: vm.filterlocation,
                        urbanPiper: vm.addOnOutput
                    }).success(function () {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                    }).finally(function () {
                        vm.saving = false;
                    });
                    return;
                }
                locationService.createOrUpdateLocation({
                    location: vm.location,
                    locationBranch: vm.locationBranch,
                    requestLocations: vm.filterlocation,
                    urbanPiper: vm.addOnOutput
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $state.go("tenant.location");
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {
                if (vm.location.name == null) {
                    vm.existall = false;
                    return;
                }
            };

            vm.cancel = function () {
                // $modalInstance.dismiss();
                $state.go("tenant.location");
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.getLocationId = function (loc) {
                return parseInt(loc.id);
            };

            vm.getComboText = function (item) {
                return (item.displayText);
            };

            vm.refcompany = [];

            function fillDropDownCompany() {
                locationService.getCompanyForCombobox({}).success(function (result) {
                    vm.refcompany = result.items;
                    if (vm.refcompany.length == 1)
                        vm.location.companyRefId = parseInt(vm.refcompany[0].value);
                });
            }

            vm.locations = [];

            vm.getLocations = function () {
                locationService.getAll({
                    skipCount: 0,
                    maxResultCount: 1000,
                    sorting: null
                }).success(function (result) {
                    vm.locationlist = [];

                    angular.forEach(result.items, function (value, key) {
                        if (value.id != vm.location.id) {
                            vm.locationlist.push(value);
                        }
                    });

                    vm.locations = $.parseJSON(JSON.stringify(vm.locationlist));
                }).finally(function (result) {
                });
            };

            vm.locationGroups = [];
            function fillDropDownLocationGroup() {
                locationGroupService.getAll({ maxResultCount: 1000 }).success(function (result) {
                    vm.locationGroups = result.items;
                });
            }

            vm.loadLocationGroups = function ($query) {
                return vm.locationGroups.filter(function (group) {
                    return group.name.toLowerCase().indexOf($query.toLowerCase()) !== -1;
                });
            };

            vm.locationTags = [];
            function fillDropDownLocationTag() {
                locationTagService.getAll({ maxResultCount: 1000 }).success(function (result) {
                    vm.locationTags = result.items;
                });
            }

            vm.loadLocationTags = function ($query) {
                return vm.locationTags.filter(function (tag) {
                    return tag.name.toLowerCase().indexOf($query.toLowerCase()) !== -1;
                });
            };

            vm.openLanguageDescriptionModal = function () {
                vm.languageDescription = {
                    id: locationId,
                    name: vm.location.name,
                    languageDescriptionType: vm.languageDescriptionType
                };
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/languageDescription/languageDescriptionModal.cshtml",
                    controller: "tenant.views.connect.languageDescription.languageDescriptionModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        languagedescription: function () {
                            return vm.languageDescription;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });
            }

            function init() {
                fillDropDownLocationGroup();
                fillDropDownLocationTag();

                locationService.getLocationForEdit({
                    Id: locationId
                }).success(function (result) {
                    vm.location = result.location;
                    vm.locationBranch = result.locationBranch;
                    if (result.urbanPiper != null) {
                        vm.addOnOutput = result.urbanPiper;
                        console.log(vm.addOnOutput);
                    }

                    vm.requestlocations = result.requestLocations;
                    vm.getLocations();
                    fillDropDownCompany();

                    if (vm.location.houseTransactionDate != null) {
                        vm.datachangeallowed = false;
                        $('input[name="transactionDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: vm.location.houseTransactionDate,
                            minDate: vm.location.houseTransactionDate,
                            maxDate: moment().add(10, 'days')
                        });
                    }
                    else {
                        vm.datachangeallowed = true;
                        vm.location.houseTransactionDate = moment().add(-1, 'days');
                        $('input[name="transactionDate"]').daterangepicker({
                            locale: {
                                format: $scope.format
                            },
                            singleDatePicker: true,
                            showDropdowns: true,
                            minDate: $scope.minDate,
                            maxDate: moment().add(15, 'days')
                        });
                    }
                });
            }
            init();
        }
    ]);
})();