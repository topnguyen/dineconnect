﻿(function () {
    appModule.controller('tenant.views.connect.master.location.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.location',
        function ($scope, $state, $modal, uiGridConstants, locationService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.isDeleted = false;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Connect.Master.Location.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Connect.Master.Location.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Connect.Master.Location.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editLocation(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteLocation(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.edit\" ng-click=\"grid.appScope.locationcontactLink(row.entity)\">" + app.localize("Contacts") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.isDeleted && grid.appScope.permissions.edit\" ng-click=\"grid.appScope.activateItem(row.entity)\">" + app.localize("Revert") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Id'),
                        field: 'id'
                    },
                    {
                        name: app.localize('Organization'),
                        field: 'companyName'
                    },
                    {
                        name: app.localize('Code'),
                        field: 'code'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('Address 1'),
                        field: 'address1'
                    },
                    {
                        name: app.localize('City'),
                        field: 'city'
                    },
                    {
                        name: app.localize('PhoneNumber'),
                        field: 'phoneNumber'
                    },
                    {
                        name: app.localize('AccountDate'),
                        field: 'houseTransactionDate',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD\''
                    }

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.locationcontactLink = function (myObj) {
                createLocationContact(myObj.id);
            };

            createLocationContact = function (objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/locationcontact/ViewOnly.cshtml',
                    controller: 'tenant.views.connect.locationcontact.ViewOnly as vm',
                    backdrop: 'static',
                    resolve: {
                        arglocationIdForLock: function () {
                            return objId;
                        },
                        locationcontactId: function () {
                            return null;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            };

            vm.getAll = function () {
                vm.loading = true;
                locationService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    isDeleted: vm.isDeleted
                }).success(function (result) {
                    console.log(result);
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editLocation = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createLocation = function () {
                openCreateOrEditModal(null);
            };

            function importModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/location/importModal.cshtml',
                    controller: 'tenant.views.connect.master.location.importModal as vm',
                    backdrop: 'static'
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.import = function () {
                importModal(null);
            };

            vm.deleteLocation = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteLocationWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            locationService.deleteLocation({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.createlocation", {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                locationService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.activateItem = function (myObject) {
                locationService.activateItem({
                    id: myObject.id
                }).success(function (result) {
                    abp.notify.success(app.localize("Successfully"));
                    vm.getAll();
                });
            };

            vm.getAll();
        }]);
})();