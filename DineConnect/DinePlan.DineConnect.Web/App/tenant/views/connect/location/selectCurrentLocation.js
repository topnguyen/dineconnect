﻿(function () {
    appModule.controller("tenant.views.connect.location.select.selectCurrentLocation", [
        "$scope", "$uibModalInstance", "uiGridConstants", "abp.services.app.location", "abp.services.app.locationGroup", "userId", "defaultLocationRefId", "userName", "abp.services.app.userDefaultOrganization",
        function ($scope, $modalInstance, uiGridConstants, locationService, locationgroupService, userId, defaultLocationRefId, userName, userdefaultorganizationService) {
            var vm = this;
            vm.userName = userName;
            vm.userId = userId;
            vm.defaultLocationRefId = defaultLocationRefId;
            vm.selectedLocations = [];
            vm.allowedCompany = [];

            vm.saving = false;
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.locationOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Select"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.pushLocation(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Select") + "\"><i class=\"fa fa-dashcube\"></i></button>" +
                            "</div>"
                    },
                    {
                        name: app.localize("Code"),
                        field: "code"
                    },
                    {
                        name: app.localize("Name"),
                        field: "name"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        vm.getLocations();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        vm.getLocations();
                    });
                },
                data: []
            };

            vm.getLocations = function () {
                vm.loading = true;
                locationService.getLocationBasedOnUser({
                    locationName: vm.filterLocationText,
                    companyList: vm.allowedCompany,
                    userId: vm.userId
                }).success(function (result) {
                    vm.locationOptions.totalItems = result.totalCount;
                    vm.locationOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            //vm.getLocations();

            vm.pushLocation = function (myObj) {
                vm.selectedLocation = myObj;
                vm.save();
            };

            vm.save = function () {
                location.defaultLocationRefId = vm.defaultLocationRefId;
                location.selectedLocation = vm.selectedLocation;
                $modalInstance.close(location);
            };

            vm.cancel = function () {
                $modalInstance.close(null);
            };

            vm.refcompany = [];

            function fillDropDownCompany() {
                locationService.getCompanyForCombobox({}).success(function (result) {
                    vm.refcompany = result.items;
                    if (vm.refcompany.length == 1 && vm.selectedLocations == null) {
                        vm.allowedCompany = vm.refcompany;
                    }
                    init();
                });
            }

            fillDropDownCompany();

            function init() {
                vm.loading = true;
                userdefaultorganizationService.getUserInfo({
                    id: vm.userId
                }).success(function (result) {
                    vm.locationOptions.totalItems = result.locationList.length;
                    vm.locationOptions.data = result.locationList;
                    vm.selectedLocations = result.locationList;
                    vm.companyList = result.companyList;
                }).finally(function () {
                    vm.loading = false;
                });
            }
        }
    ]);
})();