﻿(function () {
    appModule.controller("tenant.views.connect.location.select.location",
        [
            "$scope", "$uibModalInstance", "uiGridConstants", "abp.services.app.location",
            "abp.services.app.locationGroup", "abp.services.app.locationTag", "location",
            function ($scope,
                $modalInstance,
                uiGridConstants,
                locationService,
                locationGroupService,
                locationTagService,
                location) {

                var vm = this;
                var requestParams = {
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null,
                    userId: abp.session.userId
                };

                vm.saving = false;
                vm.autoSelection = false;

                vm.isUndefinedOrNull = function (val) {
                    return angular.isUndefined(val) || val == null;
                };
                vm.rowSelectionFlag = true;
                if (!vm.isUndefinedOrNull(location.viewOnly)) {
                    if (location.viewOnly == true || location.viewOnly == "true")
                        vm.rowSelectionFlag = false;
                }


                // #region Init Variables

                vm.group = location.group;
                vm.isLocationTag = location.locationTag;

                vm.selectedLocations = location.locations;
                vm.selectedGroups = location.groups;
                vm.selectedTags = location.locationTags;
                vm.selectedRestrictLocations = location.nonLocations;

                vm.singleLocation = location.single;
                vm.multiple = !location.single;

                vm.selectedType = "location";

                vm.hideGroupDisplay = location.hideGroupDisplay;
                vm.hideTagDisplay = location.hideTagDisplay;
                vm.hideRestrictLocation = location.hideRestrictLocation;


                if (vm.group) {
                    vm.selectedType = "group";
                }
                if (vm.isLocationTag) {
                    vm.selectedType = "tag";
                }

                // #endregion
                // #region Events

                vm.pushLocation = function (myObj) {
                    if (vm.singleLocation)
                        vm.save();

                    if (myObj != null) {
                        var myIndex = -1;
                        if (vm.selectedLocations != null) {
                            vm.selectedLocations.some(function (value, key) {
                                if (value.id == myObj.id) {
                                    myIndex = value;
                                    return true;
                                }
                            });
                        }

                        if (myIndex == -1) {
                            vm.selectedLocations.push(myObj);
                        } else {
                            if (vm.autoSelection == false)
                                abp.notify.error(myObj.name + "  " + app.localize("Available"));
                        }
                    }
                };
                vm.pushNonLocation = function (myObj) {
                    if (myObj != null) {
                        var myIndex = -1;
                        if (vm.selectedRestrictLocations != null) {
                            vm.selectedRestrictLocations.some(function (value, key) {
                                if (value.id == myObj.id) {
                                    myIndex = value;
                                    return true;
                                }
                            });
                        }

                        if (myIndex === -1) {
                            vm.selectedRestrictLocations.push(myObj);
                        } else {
                            if (vm.autoSelection == false)
                                abp.notify.error(myObj.name + "  " + app.localize("Available"));
                        }
                    }
                };
                vm.pushGroup = function (myObj) {
                    if (vm.singleLocation)
                        vm.save();

                    if (myObj != null) {
                        var myIndex = -1;
                        if (vm.selectedGroups != null) {
                            vm.selectedGroups.some(function (value, key) {
                                if (value.id == myObj.id) {
                                    myIndex = value;
                                    return true;
                                }
                            });
                        }

                        if (myIndex === -1) {
                            vm.selectedGroups.push(myObj);
                        } else {
                            if (vm.autoSelection == false)
                                abp.notify.error(myObj.name + "  " + app.localize("Available"));
                        }
                    }
                };
                vm.pushTag = function (myObj) {
                    if (vm.singleLocation)
                        vm.save();

                    if (myObj != null) {
                        var myIndex = -1;
                        if (vm.selectedTags != null) {
                            vm.selectedTags.some(function (value, key) {
                                if (value.id == myObj.id) {
                                    myIndex = value;
                                    return true;
                                }
                            });
                        }

                        if (myIndex === -1) {
                            vm.selectedTags.push(myObj);
                        } else {
                            if (vm.autoSelection == false)
                                abp.notify.error(myObj.name + "  " + app.localize("Available"));
                        }
                    }
                };
                //console.log('My Type ' + vm.selectedType);
                vm.locationTypeSelect = function () {
                  //  console.log("Coming Inside");
                    switch (vm.selectedType) {
                        case "location":
                            vm.locationOptions.totalItems = vm.dataForLocationGrid.totalCount;
                            vm.locationOptions.data = vm.dataForLocationGrid.items;
                            break;

                        case "group":
                            vm.locationGroupOptions.totalItems = vm.dataForLocationGroupGrid.totalCount;
                            vm.locationGroupOptions.data = vm.dataForLocationGroupGrid.items;
                            break;
                        case "tag":
                            vm.locationTagOptions.totalItems = vm.dataForLocationTagGrid.totalCount;
                            vm.locationTagOptions.data = vm.dataForLocationTagGrid.items;
                            break;

                        default:
                            break;
                    }
                };

                vm.save = function () {
                    //console.log('I am saving');

                    vm.group = false;
                    vm.isLocationTag = false;
                    location.locations = vm.selectedLocations;

                    switch (vm.selectedType) {
                        case "location":
                            break;
                        case "group":
                            vm.group = true;
                            break;
                        case "tag":
                            vm.isLocationTag = true;
                            break;
                        default:
                            break;
                    }

                    location.group = vm.group;
                    location.groups = vm.selectedGroups;
                    location.nonLocations = vm.selectedRestrictLocations;
                    location.locationTag = vm.isLocationTag;
                    location.locationTags = vm.selectedTags;
                    $modalInstance.close(location);
                };
                vm.cancel = function () {
                    $modalInstance.dismiss();
                };
                vm.clear = function () {
                    vm.filterLocationText = null;
                    vm.filterGroupText = null;
                    vm.group = false;
                    vm.selectedLocations = [];
                    vm.selectedGroups = [];
                    vm.selectedTags = [];
                    vm.selectedRestrictLocations = [];
                    if (vm.selectedType == "group" || vm.selectedType == "tag") {
                        vm.selectedType = "location";
                    } else {
                        vm.selectedType = "group";
                    }

                    if (!vm.isUndefinedOrNull(vm.locationGridApi))
                        vm.locationGridApi.selection.clearSelectedRows();

                    if (!vm.isUndefinedOrNull(vm.locationgrpGridApi))
                        vm.locationgrpGridApi.selection.clearSelectedRows();

                    if (!vm.isUndefinedOrNull(vm.locationTagGridApi))
                        vm.locationTagGridApi.selection.clearSelectedRows();

                    if (!vm.isUndefinedOrNull(vm.restrictLocationGridApi))
                        vm.restrictLocationGridApi.selection.clearSelectedRows();
                };

                vm.setRestrictLocationSelection = function () {
                    if (vm.isUndefinedOrNull(vm.restrictLocationGridApi))
                        return;
                    if (vm.isUndefinedOrNull(vm.restrictLocationGridApi.grid))
                        return;
                    if (vm.selectedRestrictLocations.length == 0) {
                        vm.restrictLocationGridApi.selection.clearSelectedRows();
                        return;
                    }
                    vm.restrictLocationGridApi.grid.modifyRows(vm.restrictLocationOptions.data);
                    vm.restrictLocationGridApi.selection.clearSelectedRows();
                    vm.autoSelection = true;
                    angular.forEach(vm.restrictLocationOptions.data,
                        function (value, key) {
                            vm.selectedRestrictLocations.some(function (selectedData, selectedKey) {
                                if (value.id == selectedData.id) {
                                    vm.restrictLocationGridApi.selection.selectRow(
                                        vm.restrictLocationOptions.data[key]);
                                    return true;
                                }
                            });
                        });
                    vm.autoSelection = false;
                }
                vm.restrictLocationSelect = function () {
                    vm.restrictLocationOptions.totalItems = vm.dataForRestrictLocationGrid.totalCount;
                    vm.restrictLocationOptions.data = vm.dataForRestrictLocationGrid.items;
                    vm.setRestrictLocationSelection();
                };
                vm.selectRestrict = function () {
                    vm.restrictLocationSelect();
                };

                // #endregion
                // #region Initial Load

                vm.dataForLocationGrid = [];
                vm.dataForRestrictLocationGrid = [];
                vm.dataForLocationGroupGrid = [];
                vm.dataForLocationTagGrid = [];

                vm.setLocationTag = function () {
                    if (vm.isUndefinedOrNull(vm.locationTagGridApi))
                        return;
                    if (vm.selectedTags.length == 0) {
                        vm.locationTagGridApi.selection.clearSelectedRows();
                        return;
                    }
                    if (vm.isUndefinedOrNull(vm.locationTagGridApi.grid))
                        return;
                    vm.locationTagGridApi.grid.modifyRows(vm.locationTagOptions.data);
                    vm.locationTagGridApi.selection.clearSelectedRows();
                    vm.autoSelection = true;
                    angular.forEach(vm.locationTagOptions.data,
                        function (value, key) {
                            vm.selectedTags.some(function (selectedData, selectedKey) {
                                if (value.id == selectedData.id) {
                                    vm.locationTagGridApi.selection.selectRow(vm.locationTagOptions.data[key]);
                                    return true;
                                }
                            });
                        });
                    vm.autoSelection = false;
                };
                vm.getLocationTags = function () {
                    vm.loading = true;
                    locationTagService.getAll({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterLocationText
                    }).success(function (result) {
                        vm.locationTagOptions.totalItems = result.totalCount;
                        vm.locationTagOptions.data = result.items;

                        vm.dataForLocationTagGrid = result;
                        vm.setLocationTag();
                    }).finally(function () {
                        vm.loading = false;
                    });
                };

                vm.setLocationGroupSelection = function () {
                    //console.log('Test');
                    if (vm.isUndefinedOrNull(vm.locationgrpGridApi))
                        return;
                    if (vm.selectedGroups.length == 0) {
                        vm.locationgrpGridApi.selection.clearSelectedRows();
                        return;
                    }
                    if (vm.isUndefinedOrNull(vm.locationgrpGridApi.grid))
                        return;
                    vm.locationgrpGridApi.grid.modifyRows(vm.locationGroupOptions.data);
                    vm.locationgrpGridApi.selection.clearSelectedRows();
                    vm.autoSelection = true;
                    angular.forEach(vm.locationGroupOptions.data,
                        function (value, key) {
                            vm.selectedGroups.some(function (selectedData, selectedKey) {
                                if (value.id == selectedData.id) {
                                    vm.locationgrpGridApi.selection.selectRow(vm.locationGroupOptions.data[key]);
                                    return true;
                                }
                            });
                        });
                    vm.autoSelection = false;
                };
                vm.getLocationGroups = function () {
                    vm.loading = true;
                    locationGroupService.getSimpleAll({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterGroupText
                    }).success(function (result) {
                        vm.locationGroupOptions.totalItems = result.totalCount;
                        vm.locationGroupOptions.data = result.items;

                        vm.dataForLocationGroupGrid = result;
                        vm.setLocationGroupSelection();
                    }).finally(function () {
                        vm.loading = false;
                    });
                };

                vm.setLocationSelection = function () {
                    if (vm.selectedLocations.length == 0) {
                        vm.locationGridApi.selection.clearSelectedRows();
                        return;
                    }
                    vm.locationGridApi.grid.modifyRows(vm.locationOptions.data);
                    vm.locationGridApi.selection.clearSelectedRows();
                    vm.autoSelection = true;
                    angular.forEach(vm.locationOptions.data,
                        function (value, key) {
                            vm.selectedLocations.some(function (selectedData, selectedKey) {
                                if (value.id == selectedData.id) {
                                    vm.locationGridApi.selection.selectRow(vm.locationOptions.data[key]);
                                    return true;
                                }
                            });
                        });
                    vm.autoSelection = false;
                };
                vm.getLocations = function () {
                    vm.loading = true;
                    locationService.getSimpleAll({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterLocationText,
                        userId: requestParams.userId
                    }).success(function (result) {
                        vm.locationOptions.totalItems = result.totalCount;
                        vm.locationOptions.data = result.items;
                        vm.dataForLocationGrid = result;
                        vm.dataForRestrictLocationGrid = angular.copy(result);
                        vm.setLocationSelection();
                    }).finally(function () {
                        vm.loading = false;
                    });
                };


                vm.getLocations();
                vm.getLocationGroups();
                vm.getLocationTags();

                // #endregion
                // #region Grid
                vm.restrictLocationOptions = {
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    enableRowSelection: true,
                    enableRowHeaderSelection: vm.rowSelectionFlag,
                    enableSelectAll: false,
                    isRowSelectable: function () {
                        if (vm.rowSelectionFlag == false) {
                            return false;
                        } else {
                            return true;
                        }

                    },
                    rowHeight: 30,
                    appScopeProvider: vm,
                    rowTemplate:
                        "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                    columnDefs: [
                        {
                            name: app.localize("Id"),
                            field: "id",
                            width: "50"
                        },
                        {
                            name: app.localize("Code"),
                            field: "code",
                            width: "70"
                        },
                        {
                            name: app.localize("Name"),
                            field: "name",
                            minWidth: "450"
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.resgridApi = gridApi;
                        vm.restrictLocationGridApi = gridApi;
                        $scope.resgridApi.core.on.sortChanged($scope,
                            function (grid, sortColumns) {
                                if (!sortColumns.length || !sortColumns[0].field) {
                                    requestParams.sorting = null;
                                } else {
                                    requestParams.sorting =
                                        sortColumns[0].field + " " + sortColumns[0].sort.direction;
                                }
                                vm.getLocations();
                            });
                        gridApi.pagination.on.paginationChanged($scope,
                            function (pageNumber, pageSize) {
                                requestParams.skipCount = (pageNumber - 1) * pageSize;
                                requestParams.maxResultCount = pageSize;
                                vm.getLocations();
                            });
                        gridApi.selection.on.rowSelectionChanged($scope,
                            function (row) {
                                if (row.isSelected) {
                                    vm.pushNonLocation(row.entity);
                                } else {
                                    for (let i = 0; i < vm.selectedRestrictLocations.length; i++) {
                                        const obj = vm.selectedRestrictLocations[i];

                                        if (obj.id == row.entity.id) {
                                            vm.selectedRestrictLocations.splice(i, 1);
                                        }
                                    }

                                }
                            });
                    },
                    data: []
                };
                vm.locationOptions = {
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    enableRowSelection: true,
                    enableRowHeaderSelection: vm.rowSelectionFlag,
                    enableSelectAll: false,
                    isRowSelectable: function () {
                        if (vm.rowSelectionFlag == false) {
                            return false;
                        } else {
                            return true;
                        }

                    },
                    rowHeight: 30,
                    appScopeProvider: vm,
                    rowTemplate:
                        "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                    columnDefs: [
                        {
                            name: app.localize("Id"),
                            field: "id",
                            maxWidth: 50
                        },
                        {
                            name: app.localize("Code"),
                            field: "code",
                            maxWidth: 70
                        },
                        {
                            name: app.localize("Name"),
                            field: "name"
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        vm.locationGridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope,
                            function (grid, sortColumns) {
                                if (!sortColumns.length || !sortColumns[0].field) {
                                    requestParams.sorting = null;
                                } else {
                                    requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                                }
                                vm.getLocations();
                            });
                        gridApi.pagination.on.paginationChanged($scope,
                            function (pageNumber, pageSize) {
                                requestParams.skipCount = (pageNumber - 1) * pageSize;
                                requestParams.maxResultCount = pageSize;
                                vm.getLocations();
                            });
                        gridApi.selection.on.rowSelectionChanged($scope,
                            function (row) {
                                if (row.isSelected) {
                                    vm.pushLocation(row.entity);
                                } else {
                                    for (let i = 0; i < vm.selectedLocations.length; i++) {
                                        const obj = vm.selectedLocations[i];

                                        if (obj.id == row.entity.id) {
                                            vm.selectedLocations.splice(i, 1);
                                        }
                                    }

                                }
                            });

                    },
                    data: []
                };
                vm.locationGroupOptions = {
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    enableRowSelection: true,
                    enableSelectAll: false,
                    enableRowHeaderSelection: vm.rowSelectionFlag,
                    isRowSelectable: function () {
                        if (vm.rowSelectionFlag == false) {
                            return false;
                        } else {
                            return true;
                        }

                    },
                    rowHeight: 30,
                    appScopeProvider: vm,
                    rowTemplate:
                        "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                    columnDefs: [
                        {
                            name: app.localize("Id"),
                            field: "id",
                            maxWidth: 50
                        },
                        {
                            name: app.localize("Name"),
                            field: "name"
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.locgrpgridApi = gridApi;
                        vm.locationgrpGridApi = gridApi;
                        $scope.locgrpgridApi.core.on.sortChanged($scope,
                            function (grid, sortColumns) {
                                if (!sortColumns.length || !sortColumns[0].field) {
                                    requestParams.sorting = null;
                                } else {
                                    requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                                }
                                vm.getLocationGroups();
                            });
                        gridApi.pagination.on.paginationChanged($scope,
                            function (pageNumber, pageSize) {
                                requestParams.skipCount = (pageNumber - 1) * pageSize;
                                requestParams.maxResultCount = pageSize;
                                vm.getLocationGroups();
                            });
                        gridApi.selection.on.rowSelectionChanged($scope,
                            function (row) {
                                if (row.isSelected) {
                                    vm.pushGroup(row.entity);
                                } else {
                                    for (let i = 0; i < vm.selectedGroups.length; i++) {
                                        const obj = vm.selectedGroups[i];

                                        if (obj.id == row.entity.id) {
                                            vm.selectedGroups.splice(i, 1);
                                        }
                                    }

                                }
                            });
                    },
                    data: []
                };
                vm.locationTagOptions = {
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    enableRowSelection: true,
                    enableSelectAll: false,
                    enableRowHeaderSelection: vm.rowSelectionFlag,
                    isRowSelectable: function () {
                        if (vm.rowSelectionFlag == false) {
                            return false;
                        } else {
                            return true;
                        }

                    },
                    rowHeight: 30,
                    appScopeProvider: vm,
                    rowTemplate:
                        "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                    columnDefs: [
                        {
                            name: app.localize("Id"),
                            field: "id",
                            maxWidth: 50
                        },
                        {
                            name: app.localize("Name"),
                            field: "name"
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.loctaggridApi = gridApi;
                        vm.locationTagGridApi = gridApi;
                        $scope.loctaggridApi.core.on.sortChanged($scope,
                            function (grid, sortColumns) {
                                if (!sortColumns.length || !sortColumns[0].field) {
                                    requestParams.sorting = null;
                                } else {
                                    requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                                }
                                vm.getLocationTags();
                            });
                        gridApi.pagination.on.paginationChanged($scope,
                            function (pageNumber, pageSize) {
                                requestParams.skipCount = (pageNumber - 1) * pageSize;
                                requestParams.maxResultCount = pageSize;
                                vm.getLocationTags();
                            });
                        gridApi.selection.on.rowSelectionChanged($scope,
                            function (row) {
                                if (row.isSelected) {
                                    vm.pushTag(row.entity);
                                } else {
                                    for (let i = 0; i < vm.selectedTags.length; i++) {
                                        const obj = vm.selectedTags[i];

                                        if (obj.id == row.entity.id) {
                                            vm.selectedTags.splice(i, 1);
                                        }
                                    }

                                }
                            });
                    },
                    data: []
                };
                // #endregion

            }
        ]);
})();