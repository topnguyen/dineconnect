﻿(function() {
    appModule.controller("tenant.views.connect.location.select.selectOrganisationAndLocation", [
			"$scope", "$uibModalInstance", 'uiGridConstants', "abp.services.app.location", "abp.services.app.locationGroup", "userId", "defaultLocationRefId", "locationList", "companyList", "userName", 
				function ($scope, $modalInstance, uiGridConstants, locationService, locationgroupService, userId, defaultLocationRefId, locationList, companyList, userName) {

					var vm = this;
					vm.userName = userName;
            vm.group = [];
						vm.selectedLocations = locationList;
            vm.selectedGroups = [];
						vm.allowedCompany = companyList;
						vm.userId = userId;
						vm.defaultLocationRefId = defaultLocationRefId;

            vm.saving = false;
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.locationOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                     {
                         name: app.localize('Select'),
                         cellTemplate:
                             '<div class=\"ui-grid-cell-contents text-center\">' +
                             '  <button ng-click="grid.appScope.pushLocation(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Select') + '"><i class="fa fa-dashcube"></i></button>' +
                             '</div>'
                     },
                    {
                        name: app.localize('Id'),
                        field: 'id'
                    },
                    {
                        name: app.localize('Code'),
                        field: 'code'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getLocations();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        vm.getLocations();
                    });
                },
                data: []
            };

            vm.locationGroupOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                     {
													name: app.localize('Select'),
													enableSorting: false,
                         cellTemplate:
                             '<div class=\"ui-grid-cell-contents text-center\">' +
                             '  <button ng-click="grid.appScope.pushGroup(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Select') + '"><i class="fa fa-dashcube"></i></button>' +
                             '</div>'
                     },
                    {
                        name: app.localize('Id'),
                        field: 'id'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getLocationGroups();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        vm.getLocationGroups();
                    });
                },
                data: []
            };

						vm.pushLocation = function (myObj) {
							var alreadyExists = false;
							vm.selectedLocations.some(function (data, key) {
								if (data.id == myObj.id) {
										abp.notify.error(app.localize('AlreadyExists'));
										alreadyExists = true;
										return true;
									}
								});

							if (alreadyExists == false)
								vm.selectedLocations.push(myObj);
            };

            vm.getLocations = function () {
                vm.loading = true;
								locationService.getLocationsAllForGivenOrganisationList({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
										filter: vm.filterLocationText,
										companyList: vm.allowedCompany
                }).success(function (result) {
                    vm.locationOptions.totalItems = result.totalCount;
                    vm.locationOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.getLocationGroups = function () {
                vm.loading = true;
                locationgroupService.getSimpleAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterLocationText
                }).success(function (result) {
                    vm.locationGroupOptions.totalItems = result.totalCount;
                    vm.locationGroupOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            }

						vm.save = function () {
							var defaultExists = false;

							vm.selectedLocations.some(function (data, key) {
								if (data.id == vm.defaultLocationRefId) {
									defaultExists = true;
									return true;
								}
							});
							if (defaultExists == false) {
								vm.defaultLocationRefId = null;
								abp.notify.error(app.localize('DefaultLocation') + ' ?');
								return;
							}

                location.group = vm.group;
                location.groups = vm.selectedGroups;
								location.selectedLocations = vm.selectedLocations;
								location.defaultLocationRefId = vm.defaultLocationRefId;
                $modalInstance.close(location);
            };

            vm.cancel = function() {
                $modalInstance.dismiss();
            };



            vm.getLocations();
						vm.getLocationGroups();


						vm.refcompany = [];

						function fillDropDownCompany() {
							locationService.getCompanyForCombobox({}).success(function (result) {
								vm.refcompany = result.items;
								if (vm.refcompany.length == 1 && vm.selectedLocations == null) {
										vm.allowedCompany = vm.refcompany;
								}
								init();
							});
						}

						fillDropDownCompany();

						function init() {
							
						}



        }
    ]);
})();