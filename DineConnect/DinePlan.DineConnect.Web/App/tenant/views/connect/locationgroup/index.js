﻿(function () {
    appModule.controller('tenant.views.connect.locationgroup.index', [
        '$scope', '$uibModal', '$q', 'uiGridConstants', 'abp.services.app.organizationUnit', 'abp.services.app.commonLookup', 'lookupModal', 'abp.services.app.location', 'abp.services.app.locationGroup',
        function ($scope, $uibModal, $q, uiGridConstants, organizationUnitService, commonLookupService, lookupModal, locationService, locationGroupService) {
            var vm = this;
            vm.locationGroupId = null;
            vm.languageDescriptionType = 13;
            vm.selectedLocationGroup = null;
            vm.loading = false;
            vm.rightLoading = false;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var rightrequestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.permissions = {
                manageGroup: abp.auth.hasPermission('Pages.Tenant.Connect.Master.LocationGroup.Create'),
                manageLocation: abp.auth.hasPermission('Pages.Tenant.Connect.Master.Location')
            };

            vm.getLocationGroups = function () {
                vm.loading = true;
                locationGroupService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterlocationGroupText,
                    companyList: vm.allowedCompany
                }).success(function (result) {
                    vm.locationGroupOptions.totalItems = result.totalCount;
                    vm.locationGroupOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.locationGroupOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Select"),
                        maxWidth: 100,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.selectLocationGroupFromGrid(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Select") + "\"><i class=\"fa fa-dashcube\"></i></button>" +
                            "</div>"
                    },
                    {
                        name: app.localize("Id"),
                        field: "id",
                        maxWidth: 70
                    },
                    {
                        name: app.localize("Code"),
                        field: "code"
                    },
                    {
                        name: app.localize("Name"),
                        field: "name"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        vm.getLocationGroups();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        vm.getLocationGroups();
                    });
                },
                data: []
            };

            vm.getLocationGroups();

            vm.selectLocationGroupFromGrid = function (myObj) {
                vm.selectedLocationGroup = myObj;
                vm.locationGroupId = myObj.id;
                vm.members.load();
            };

            vm.locationGroup = {
                openAddOrEditModal: function (argOption) {
                    var a = vm.locationGroupId;
                    var modalInstance = $uibModal.open({
                        templateUrl: '~/App/tenant/views/connect/locationgroup/locationgroup.cshtml',
                        controller: 'tenant.views.connect.master.locationgroup.createlocationgroup as vm',
                        backdrop: 'static',
                        resolve: {
                            locationgroupId: function () {
                                if (argOption == 2)
                                    return vm.locationGroupId;
                                else
                                    return null;
                            }
                        }
                    });

                    modalInstance.result.then(function (result) {
                        vm.getLocationGroups();
                    });
                },

                delete: function (callback) {
                    abp.message.confirm(
                        app.localize('DeleteLocationGroupWarning', vm.selectedLocationGroup.name),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                vm.loading = true;
                                locationGroupService.deleteLocationGroup({
                                    id: vm.locationGroupId
                                }).success(function (result) {
                                    vm.loading = false;
                                    vm.getLocationGroups();
                                    vm.locationGroupId = null;
                                });
                            }
                        }
                    );
                }
            };

            vm.members = {
                gridOptions: {
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    columnDefs: [
                        {
                            name: app.localize('Remove'),
                            enableSorting: false,
                            width: 100,
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents\">' +
                                '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.members.remove(row.entity)" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>' +
                                '</div>'
                        },
                        {
                            name: app.localize('Code'),
                            field: 'code',
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents\" title="{{row.entity.code + \')\'}}">' +
                                '  {{COL_FIELD CUSTOM_FILTERS}} &nbsp;' +
                                '</div>',
                            minWidth: 60
                        },
                        {
                            name: app.localize('Name'),
                            field: 'name',
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents\" title="{{row.entity.name + \')\'}}">' +
                                '  {{COL_FIELD CUSTOM_FILTERS}} &nbsp;' +
                                '</div>',
                            minWidth: 140
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                            if (!sortColumns.length || !sortColumns[0].field) {
                                rightrequestParams.sorting = null;
                            } else {
                                rightrequestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                            }

                            vm.members.load();
                        });
                        gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                            rightrequestParams.skipCount = (pageNumber - 1) * pageSize;
                            rightrequestParams.maxResultCount = pageSize;

                            vm.members.load();
                        });
                    },
                    data: []
                },

                load: function () {
                    if (!vm.locationGroupId) {
                        vm.members.gridOptions.totalItems = 0;
                        vm.members.gridOptions.data = [];
                        return;
                    }

                    vm.rightLoading = true;
                    locationService.getLocationsForGivenLocationGroupId({
                        locationGroupId: vm.locationGroupId,
                        skipCount: rightrequestParams.skipCount,
                        maxResultCount: rightrequestParams.maxResultCount,
                        sorting: rightrequestParams.sorting,
                        filter: vm.filterLocationText,
                    }).success(function (result) {
                        vm.members.gridOptions.totalItems = result.totalCount;
                        vm.members.gridOptions.data = result.items;
                        vm.rightLoading = false;
                    });
                },

                add: function (userId) {
                    var ouId = vm.selectedLocationGroup.id;
                    if (!ouId) {
                        return;
                    }

                    organizationUnitService.addUserToOrganizationUnit({
                        organizationUnitId: ouId,
                        userId: userId
                    }).success(function () {
                        abp.notify.success(app.localize('SuccessfullyAdded'));
                        vm.members.load();
                    });
                },

                remove: function (user) {
                    var ouId = vm.selectedLocationGroup.id;
                    if (!ouId) {
                        return;
                    }

                    abp.message.confirm(
                        app.localize('RemoveLocationFromGroupWarningMessage', user.name, vm.selectedLocationGroup.name),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                var locs = [];
                                locs.push(user.id);
                                vm.loading = true;
                                locationService.removeLocationFromLocationGroup({
                                    locationGroupId: ouId,
                                    LocationIds: locs
                                }).success(function () {
                                    abp.notify.success(app.localize('SuccessfullyRemoved'));
                                    vm.members.load();
                                    vm.loading = false;
                                });
                            }
                        }
                    );
                },

                openAddModal: function () {
                    var ouId = vm.selectedLocationGroup.id;
                    if (!ouId) {
                        return;
                    }

                    lookupModal.open({
                        title: app.localize('SelectAUser'),
                        serviceMethod: commonLookupService.findUsers,

                        canSelect: function (item) {
                            return $q(function (resolve, reject) {
                                organizationUnitService.isInOrganizationUnit({
                                    userId: item.value,
                                    organizationUnitId: ouId
                                }).success(function (result) {
                                    if (result) {
                                        abp.message.warn(app.localize('UserIsAlreadyInTheOrganizationUnit'));
                                    }

                                    resolve(!result);
                                }).catch(function () {
                                    reject();
                                });
                            });
                        },

                        callback: function (selectedItem) {
                            vm.members.add(selectedItem.value);
                        }
                    });
                },

                init: function () {
                    //if (!vm.permissions.manageMembers) {
                    //	vm.members.gridOptions.columnDefs.shift();
                    //}
                }
            };

            vm.members.init();
            //vm.locationGroup.init();

            vm.AddLocation = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '~/App/tenant/views/connect/locationgroup/selectLocations.cshtml',
                    controller: 'tenant.views.connect.locationgroup.selectLocations as vm',
                    backdrop: 'static',
                    resolve: {
                        organizationUnit: function () {
                            return null;
                        },
                        locationGroupId: function () {
                            return vm.locationGroupId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    //closeCallback && closeCallback(result);
                    vm.members.load();
                });
            };
            vm.openLanguageDescriptionModal = function () {
                vm.languageDescription = {
                    id: vm.locationGroupId,
                    name: vm.selectedLocationGroup.name,
                    languageDescriptionType: vm.languageDescriptionType
                };
                var modalInstance = $uibModal.open({
                    templateUrl: "~/App/tenant/views/connect/languageDescription/languageDescriptionModal.cshtml",
                    controller: "tenant.views.connect.languageDescription.languageDescriptionModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        languagedescription: function () {
                            return vm.languageDescription;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    
                });
            }

            vm.exportToExcel = function () {
                locationGroupService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterlocationGroupText,
                    companyList: vm.allowedCompany
                }).success(function (result) {
                    app.downloadTempFile(result);
                });
            };
        }
    ]);
})();