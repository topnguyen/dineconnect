﻿
(function () {
    appModule.controller('tenant.views.connect.master.locationgroup.createlocationgroup', [
        '$scope',  '$uibModalInstance', 'abp.services.app.locationGroup', 'locationgroupId',
        function ($scope,  $modalInstance, locationgroupService, locationgroupId) {
            var vm = this;

            vm.saving = false;
            vm.locationgroup = null;
            vm.locationgroupId = locationgroupId;
            
            vm.save = function () {
                vm.saving = true;

                locationgroupService.createOrUpdateLocationGroup({
                    locationGroup: vm.locationgroup
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };


            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
            
            function init() {
                locationgroupService.getLocationGroupForEdit({
                    id: locationgroupId
                }).success(function (result) {
                    vm.locationgroup = result.locationGroup;
                });
            }
            init();
        }
    ]);
})();

