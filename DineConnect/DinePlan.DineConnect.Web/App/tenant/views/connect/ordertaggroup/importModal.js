﻿(function () {
    appModule.controller('tenant.views.connect.master.ordertaggroup.importModal', [
        '$scope', 'appSession', '$uibModalInstance', 'FileUploader', 'abp.services.app.orderTagGroup',
        function ($scope, appSession, $uibModalInstance, fileUploader, ordertaggroupService) {
            var vm = this;

            vm.uploader = new fileUploader({
                url: abp.appPath + 'Import/ImportOrderTagGroup',
                queueLimit: 1,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        console.log("item : " + item);
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        console.log("Type : " + type);
                        if ('|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ImportTemplate_Warn_FileType'));
                            return false;
                        }
                        return true;
                    }
                }]
            });

            vm.save = function () {
                vm.loading = true;
                vm.uploader.uploadAll();
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.error != null) {
                    abp.message.warn(response.error.message);
                    $uibModalInstance.close();
                    vm.loading = false;
                    return;
                }
                abp.notify.info(app.localize("Finished"));
                $uibModalInstance.close();
                vm.loading = false;
            };

            vm.getImportTemplate = function (obj) {
                abp.ui.setBusy("#importModal");
                vm.disableExport = true;
                ordertaggroupService.getAllToExcel(true).success(function (result) {
                    if (result != null) {
                        app.downloadTempFile(result);
                    }
                }).finally(function () {
                    abp.ui.clearBusy("#importModal");
                    vm.disableExport = false;
                });
            };
        }

    ]);
})();
