﻿(function () {
    appModule.controller('tenant.views.connect.menu.ordertaggroup.detail', [
        '$scope', '$state', '$uibModal', '$stateParams', 'abp.services.app.orderTagGroup', 'abp.services.app.commonLookup', 'abp.services.app.menuItem', 'abp.services.app.category',
        function ($scope, $state, $modal, $stateParams, tagService, commonLookupService, menuService, categoryService) {
            var vm = this;
            vm.tag = null;
            vm.successflag = false;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.sno = 0;
            vm.tagId = $stateParams.id;


            vm.cancel = function () {
                $state.go('tenant.ordertaggroup');
            };

            vm.save = function (argId) {
                vm.saving = true;
                tagService.createOrUpdateOrderTagGroup({
                    orderTagGroup: vm.tag,
                    locationGroup: vm.locationGroup,
                    departments: vm.requestDepartments
                }).success(function (result) {
                    vm.saving = false;
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    if (argId==0) {
                        $state.go('tenant.ordertaggroup');
                    }
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.departments = [];
            vm.requestDepartments = "";
            vm.getDepartments = function () {
                commonLookupService.getDepartments({})
                    .success(function (result) {
                        vm.departments = $.parseJSON(JSON.stringify(result));
                    }).finally(function (result) {
                    });
            };

            vm.removeTag = function (productIndex) {
                vm.tag.tags.splice(productIndex, 1);
            };

            vm.removeMap = function (productIndex) {
                vm.tag.maps.splice(productIndex, 1);
            };

            vm.addTag = function () {
                vm.sno = vm.sno + 1;
                vm.tag.tags.push({ 'id': null, 'name': "", 'alternateName': "", 'menuItemId': null, 'maxQuantity': 1, 'price': 0 });
            };

            vm.addMap = function () {
                vm.tag.maps.push({ 'name': "", 'menuItemId': null, 'categoryId': null });
            };

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };
            vm.openforCategoryItem = function (data) {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    service: categoryService.getAllCategoyItems
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        data.categoryId = result.locations[0].id;
                        data.categoryName = result.locations[0].name;
                        data.menuItemId = null;
                    }
                });
            };

            vm.openforMenuItem = function (data) {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    categoryId: data.categoryId,
                    service: data.categoryId == null ? menuService.getAllMenuItems : menuService.getAllMenuItemsBasedOnCatgeoryId
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        data.menuItemId = result.locations[0].id;
                        data.menuItemGroupCode = result.locations[0].name;
                    }
                });
            };

            vm.openforMenuItemPortion = function (data) {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    categoryId: data.categoryId,
                    service: menuService.getAllMenuItemPortions
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        data.menuItemId = result.locations[0].id;
                        data.menuItemGroupCode = result.locations[0].name;
                    }
                });
            };

            vm.openLanguageDescriptionModal = function (data) {
                if (data == null) {
                    vm.languageDescriptionType = 5; //OrderTagGroup
                    vm.languageDescription = {
                        id: vm.tagId,
                        name: vm.tag.name,
                        languageDescriptionType: vm.languageDescriptionType
                    };
                }
                else {
                    vm.languageDescriptionType = 6; //OrderTag
                    vm.languageDescription = {
                        id: data.id,
                        name: data.name,
                        languageDescriptionType: vm.languageDescriptionType
                    };
                }

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/languageDescription/languageDescriptionModal.cshtml",
                    controller: "tenant.views.connect.languageDescription.languageDescriptionModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        languagedescription: function () {
                            return vm.languageDescription;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });
            }

            vm.openLocationPriceModal = function (data) {
                $state.go("tenant.ordertaggrouplocationprice", {
                    id: vm.tagId,
                    ordertagId: data.id
                });
            }
            vm.openfileUploaderModal = function (product, arg) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/ordertaggroup/fileUploadModal.cshtml',
                    controller: 'tenant.views.connect.menu.ordertaggroup.fileUploadModal as vm',
                    backdrop: 'static',
                    resolve:
                    {
                        tagId: function () {
                            return product.id;
                        },
                        argId: function () {
                            return arg;
                        }
                    }
                });
                modalInstance.result.then(function (response) {
                    if (response !== null) {
                        vm.replyFiles = response;
                        product.files = angular.toJson(vm.replyFiles);
                        vm.save(1);
                    }
                });
            }

            function init() {
                vm.getDepartments();

                tagService.getTagForEdit({
                    id: vm.tagId
                }).success(function (result) {
                    vm.tag = result.orderTagGroup;
                    vm.replyFiles = vm.tag.files ? angular.fromJson(vm.tag.files) : [];
                    vm.locationGroup = result.locationGroup;
                    vm.requestDepartments = result.departments;
                });
            }

            init();
        }
    ]);
})();