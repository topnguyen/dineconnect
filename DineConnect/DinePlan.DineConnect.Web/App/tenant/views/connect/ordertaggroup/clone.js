﻿(function () {
    appModule.controller('tenant.views.connect.menu.ordertaggroup.clone', [
        '$scope', '$uibModalInstance', 'abp.services.app.orderTagGroup', 'orderTagId',
        function ($scope, $uibModalInstance, orderTagGroupService, orderTagId) {
            var vm = this;

            vm.oldOrderTagId = orderTagId;
            vm.newName = null;
            vm.saving = false;

            vm.save = function () {
                vm.tag.id = null;
                vm.tag.name = vm.newName;

                vm.saving = true;
                orderTagGroupService.createOrUpdateOrderTagGroup({
                    orderTagGroup: vm.tag,
                    locationGroup: vm.locationGroup
                }).success(function (result) {
                    vm.saving = true;
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $uibModalInstance.close(result);
                }).finally(function (result) {
                    vm.saving = false;
                });
            };

            function init() {
                orderTagGroupService.getTagForEdit({
                    id: vm.oldOrderTagId
                }).success(function (result) {
                    vm.tag = result.orderTagGroup;
                    vm.locationGroup = result.locationGroup;
                });
            }

            init();

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }
    ]);
})();