﻿(function () {
    appModule.controller('tenant.views.connect.menu.ordertaggroup.fileUploadModal', [
        '$scope', '$uibModalInstance', 'FileUploader', 'tagId', 'abp.services.app.orderTagGroup', 'argId',
        function ($scope, $uibModalInstance, fileUploader, tagId, tagService, argId) {
            var vm = this;
            vm.tagId = tagId;
            vm.argId = argId;
            vm.isUpload = false;
            vm.replyFiles = [];

            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadFile'
            });

            vm.uploader.filters.push({
                name: 'imageFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    return true;
                }
            });
            vm.uploader.onAfterAddingFile = function (item) {
                console.log(item.file.size);

                if (item.file.size > 6000000) {
                    abp.notify.error(app.localize('Size is Big'));
                    vm.uploader.clearQueue();
                } else {
                    item.upload();
                }
            };
            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response !== null) {
                    if (response.result !== null) {
                        if (response.result.fileName !== null) {
                            vm.replyFiles.push(response.result);
                            vm.refreshImage = true;
                        }
                    }
                    $("#tagfile").val(null);
                }
            };

            vm.downloader = function (file) {
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + file.fileName + '&url=' + file.fileSystemName;
            };

            vm.removeFile = function (findex) {
                vm.replyFiles.splice(findex, 1);
                vm.refreshImage = true;
            };

            vm.uploadFile = function (isuload) {
                vm.isUpload = isuload;
                var file = vm.uploader.queue[vm.uploader.queue.length - 1];
                file.upload();
            };

            vm.save = function () {
                $uibModalInstance.close(vm.replyFiles);
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            function init() {
                if (vm.argId == 0) {
                    tagService.getTagForEdit({
                        id: vm.tagId
                    }).success(function (result) {
                        vm.tag = result.orderTagGroup;
                        vm.replyFiles = vm.tag.files ? angular.fromJson(vm.tag.files) : [];
                        vm.locationGroup = result.locationGroup;
                        vm.requestDepartments = result.departments;
                    });
                }
                else
                {
                    tagService.getOrdertagForEdit({
                        id: vm.tagId
                    }).success(function (result) {
                        vm.ordertag = result;
                        vm.replyFiles = vm.ordertag.files ? angular.fromJson(vm.ordertag.files) : [];
                    });
                }
                
            }

            init();
        }

    ]);
})();