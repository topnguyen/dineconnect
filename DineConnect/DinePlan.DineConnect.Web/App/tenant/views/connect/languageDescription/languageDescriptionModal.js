﻿
(function () {
    appModule.controller('tenant.views.connect.languageDescription.languageDescriptionModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.languageDescription', 'abp.services.app.language',  'uiGridConstants','languagedescription',
        function ($scope, $modalInstance, languageDescriptionService, languageService, uiGridConstants, languagedescription) {

            var vm = this;
            vm.saving = false;
            vm.languageDescription = null;

            vm.id = languagedescription.id;
            vm.name = languagedescription.name;
            vm.languageDescriptionType = languagedescription.languageDescriptionType;

            vm.languageDescriptionId = null;

            vm.save = function (argSaveOption) {
                vm.saving = true;
                vm.languageDescription.referenceId = vm.id;
                vm.languageDescription.languageDescriptionType = vm.languageDescriptionType;
                languageDescriptionService.createOrUpdateLanguageDescription({
                    languageDescription: vm.languageDescription
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    if (argSaveOption == 2) {
                        $modalInstance.close();
                        vm.saving = false;
                        return;
                    }
                    vm.getAll();
                    vm.languageDescriptionId = null;
                    init();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
                vm.getAll();
            };

            vm.clear = function () {
                vm.languageDescription = null;
                vm.filterText = null;
            };
            //  Index File
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;


            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Delete'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.deletelanguageDescription(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>' +
                            '</div>',
                        width: 50
                    },
                    {
                        name: app.localize('Edit'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.editlanguageDescription(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>' +
                            '</div>',
                        width: 50
                    },
                    {
                        name: app.localize('LanguageCode'),
                        field: 'languageCode',
                        width: 100
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('Description'),
                        field: 'description'
                    },
                    
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };



            vm.getAll = function () {
                vm.loading = true;
                languageDescriptionService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    id: vm.id,
                    languageDescriptionType: vm.languageDescriptionType
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editlanguageDescription = function (myObj) {
                vm.languageDescriptionId = myObj.id;
                init();

            };

            vm.deletelanguageDescription = function (myObject) {
                abp.message.confirm(
                    app.localize('DeletelanguageDescriptionWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            languageDescriptionService.deleteLanguageDescription({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            
            function fillDropDownlanguageDescriptionType() {
                languageDescriptionService.getLanguageDescriptionTypes({
                }).success(function (result) {
                    vm.reflanguageDescription = result.items;
                }).finally(function () {

                });
            }
            function fillDropDownLanguageCode() {
                languageService.getLanguages({
                }).success(function (result) {
                    console.log(result);
                    vm.reflanguageCode = result.items;
                }).finally(function () {

                });
            }
            fillDropDownlanguageDescriptionType();
            fillDropDownLanguageCode();
            vm.getAll();

            function init() {

                languageDescriptionService.getLanguageDescriptionForEdit({
                    id: vm.languageDescriptionId
                }).success(function (result) {
                    console.log(result);
                    vm.languageDescription = result.languageDescription;
                }).finally(function () {

                });
            }
            init();

        }
    ]);
})();

