﻿(function () {
    appModule.controller('tenant.views.connect.master.calculation.detail', [
         '$scope', '$state', '$uibModal', '$stateParams', 'abp.services.app.calculation', 'abp.services.app.connectLookup', 'abp.services.app.commonLookup',
        function ($scope, $state, $modal, $stateParams, calService, connectLookupService,commonService) {
            var vm = this;
            vm.calculation = null;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.sno = 0;
            vm.calculationId = $stateParams.id;

            vm.cancel = function () {
                $state.go('tenant.calculation');
            };

            vm.save = function () {
                vm.saving = true;
                calService.createOrUpdateCalculation({
                    calculation: vm.calculation,
                    locationGroup: vm.locationGroup,
                    departments: vm.requestDepartments
                }).success(function (result) {
                    vm.saving = true;
                }).finally(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $state.go('tenant.calculation');
                    vm.saving = false;
                });
            };

            vm.departments = [];
            vm.requestDepartments = "";
            vm.getDepartments = function () {
                commonService.getDepartments({}).success(function (result) {
                    console.log(result);
                    vm.departments = $.parseJSON(JSON.stringify(result));
                }).finally(function (result) {
                });
            };
            vm.getDepartments();

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };
            vm.transactionTypes = [];
            vm.confirmationTypes = [];

            vm.processors = [];

            function init() {
                calService.getCalculationForEdit({
                    id: vm.calculationId
                }).success(function (result) {
                    vm.calculation = result.calculation;
                    vm.locationGroup = result.locationGroup;
                    vm.requestDepartments = result.departments;
                });

                connectLookupService.getCalculationTypes({}).success(function (result) {
                    vm.processors = result.items;
                });

                connectLookupService.getTransactionTypesForCombobox({}).success(function (result) {
                    vm.transactionTypes = result.items;
                });

                connectLookupService.getConfirmationTypes({}).success(function (result) {
                    vm.confirmationTypes = result.items;
                });
            }
            init();
            

            vm.openLocation = function() {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function() {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function(result) {
                    vm.locationGroup = result;
                });
            };
        }
    ]);
})();