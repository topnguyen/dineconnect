﻿(function () {
    appModule.controller('tenant.views.connect.printJob.detail', [
        '$scope', "$uibModal", '$state', '$stateParams', 'abp.services.app.printJob', "abp.services.app.printer", 'abp.services.app.menuItem', 'abp.services.app.category', 'abp.services.app.printConfiguration', 'uiGridConstants', 'abp.services.app.department',
        function ($scope, $modal, $state, $stateParams, printJobService, printerService, menuService, categoryService, printConfigurationService, uiGridConstants, departmentService) {
            var vm = this;
            vm.printConfigurationId = $stateParams.id;
            vm.printJob = null;
            vm.whatToPrints = [];
            vm.refMenuItems = [];
            vm.printers = [];
            vm.templates = [];
            vm.uilimit = 20;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };

            vm.save = function () {
                if (vm.isUndefinedOrNull(vm.printConfiguration.name)) {
                    abp.notify.error('Name is mandatory');
                    return;
                }
                vm.printConfiguration.printConfigurationType = 2;
                vm.saving = true;
                printConfigurationService.createOrUpdatePrintConfiguration({
                    printConfiguration: vm.printConfiguration
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.printJob');
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForCreate();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.openFilterLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.filterLocationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.filterLocationGroup = result;
                    init();
                });
            };

            vm.printJobOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Operations"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.removePortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Delete") + "\"><i class=\"fa fa-trash\"></i></button>" +
                            "  <button ng-click=\"grid.appScope.editPrintJobMapping(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Edit") + "\"><i class=\"fa fa-edit\"></i></button>" +
                            "  <button ng-click=\"grid.appScope.clonePrintJobMapping(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Clone") + "\"><i class=\"fa fa-clone\"></i></button>" +
                            "  <button ng-click=\"grid.appScope.viewPrintJobMapping(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("View") + "\"><i class=\"fa fa-eye\"></i></button>" +
                            "</div>"
                    },
                    {
                        name: app.localize("AliasName"),
                        field: "aliasName"
                    },
                    {
                        name: app.localize("WhatToPrint"),
                        field: "whatToPrintType"
                    },
                    {
                        name: app.localize("JobGroup"),
                        field: "jobGroup"
                    },
                    {
                        name: app.localize("ExcludeTax"),
                        field: "excludeTax",
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.excludeTax" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.excludeTax" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                    },
                    {
                        name: app.localize("Negative"),
                        field: "negative",
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.negative" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.negative" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>'
                    }

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        init();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        init();
                    });
                },
                data: []
            };

            vm.removePortion = function (myObject) {
                abp.message.confirm(
                    app.localize('Are You Sure Want to delete the record ', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            printJobService.deletePrintJob({
                                id: myObject.id
                            }).success(function (result) {
                                abp.notify.info(app.localize('Deleted'));
                                init();
                            }).finally(function () {
                                vm.saving = false;
                            });
                        }
                    }
                );
            };

            vm.tabtwo = false;
            vm.editPrintJobMapping = function (myObject) {
                vm.printJob = myObject;
                myObject.locationGroup.viewOnly = false;
                vm.locationGroup = myObject.locationGroup;
                vm.tabtwo = true;
            }

            vm.viewPrintJobMapping = function (myObject) {
                myObject.locationGroup.viewOnly = true;
                vm.locationGroup = myObject.locationGroup;
                vm.openLocation();
                vm.intiailizeOpenLocation();

            };

            vm.clonePrintJobMapping = function (myObject) {
                vm.intiailizeOpenLocation();

                myObject.id = null;
                vm.printjobMaps = [];
                angular.forEach(myObject.printerMaps, function (value, key) {
                    vm.printjobMaps.push({
                        'id': null,
                        'printerId': value.printerId,
                        'printTemplateId': value.printTemplateId,
                        'categoryId': value.categoryId,
                        'menuItemId': value.menuItemId,
                        'menuItemGroupCode': value.menuItemGroupCode,
                        'categoryName': value.categoryName
                    });
                });
                myObject.printerMaps = vm.printjobMaps;
                vm.printJob = myObject;
                vm.tabtwo = true;
            }

            vm.savePrintJobMapping = function () {
                if (vm.printJob.printerMaps.length == 0) {
                    abp.notify.error('Please Map with the Printer & Template');
                    return;
                }
                else {
                    var successFlag = false;
                    vm.printJob.printerMaps.some(function (value, key) {
                        if (value.printerId == 0 || value.printTemplateId == 0) {
                            abp.notify.error('Please fill the details');
                            successFlag = true;
                            return successFlag;
                        }
                    });
                }
                if (!successFlag) {
                    vm.saving = true;
                    vm.printJob.printConfigurationId = vm.printConfiguration.id;
                    vm.printJob.locationGroup = vm.locationGroup;
                    printJobService.addOrEditPrintJob({
                        printJob: vm.printJob
                    }).success(function () {
                        abp.notify.info(app.localize("SavedSuccessfully"));
                    }).finally(function () {
                        vm.tabone = true;
                        vm.intiailizeOpenLocation();
                        init();
                        vm.saving = false;
                    });
                }
            };

            vm.clearFilterLocation = function() {
                vm.filterLocationGroup = app.createLocationInputForCreate();
                init();
            };

            vm.clearFilterLocation();



            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };



            vm.addPrintMap = function () {
                var map = {
                    id: null,
                    menuItemId: 0,
                    printerId: 0,
                    printTemplateId: 0,
                    printerName: "",
                    printTemplateName: "",
                    menuItemName: "",
                    categoryId: 0,
                    categoryName: null,
                    menuItemGroupCode: "",
                    departmentId:0
                };
                vm.printJob.printerMaps.push(map);
            };

            vm.removePrintMap = function (index) {
                vm.printJob.printerMaps.splice(index, 1);
            };

            
            vm.getMaps = function () {
                angular.forEach(vm.printJob.printerMaps, function (item) {
                    item.menuItemId = item.menuItemId === 0 ? null : item.menuItemId;
                });

                console.log(vm.printJob.printerMaps);
            };

            vm.openforDepartment = function (product) {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    service: departmentService.getDepartments
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    console.log("dep",result)
                    if (result.locations.length > 0) {
                        product.departmentId = result.locations[0].id;
                        product.departmentName = result.locations[0].name;
                    }
                    else
                    {
                        product.departmentId = 0;
                    }
                });
            };

            vm.openforCategoryItem = function (product) {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    service: categoryService.getAllCategoyItems
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        product.categoryId = result.locations[0].id;
                        product.categoryName = result.locations[0].name;
                        product.menuItemId = 0;
                    }
                    else {
                        product.categoryId = 0;
                        product.menuItemId = 0;
                    }
                });
            };

            vm.openforMenuItem = function (product) {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    categoryId: product.categoryId,
                    service: product.categoryId == 0 ? menuService.getAllMenuItems:menuService.getAllMenuItemsBasedOnCatgeoryId
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        product.menuItemId = result.locations[0].id;
                        product.menuItemGroupCode = result.locations[0].name;
                    }
                    else {
                        product.menuItemId = 0;
                    }
                });
            };
            function init() {
                vm.loading = true;
                printJobService.getPrintJobForEdit({
                    id: vm.printConfigurationId,
                    LocationGroupToBeFiltered: vm.filterLocationGroup
                }).success(function (result) {
                    vm.printConfiguration = result.printConfiguration;
                    vm.printJob = result.printJob;
                    vm.printJobOptions.data = vm.printConfiguration.printJobs;
                    vm.printJobOptions.totalItems = vm.printConfiguration.printJobs.length;
                    vm.loading = false;
                    vm.getMaps();
                    if (vm.printJob.id == null) {
                        vm.uilimit = 20;
                    }
                    else {
                        vm.uilimit = null;
                    }
                });

                printJobService.getWhatToPrints()
                    .success(function (result) {
                        vm.whatToPrints = result.items;
                    });

                printerService.getPrinterTemplates()
                    .success(function (result) {
                        vm.templates = result.items;
                    });

                printerService.getPrintersForCombobox()
                    .success(function (result) {
                        vm.printers = result.items;
                    });

                menuService.getSinglePortionMenuItems().success(function (result) {
                    vm.defMenuItems = result.items;
                });
            }
            init();
        }
    ]);
})();