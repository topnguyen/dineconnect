﻿(function () {
    appModule.controller("tenant.views.connect.printJob.createmap", [
        "$scope", "$uibModalInstance", '$uibModal', "abp.services.app.printer", 'abp.services.app.menuItem', "printerMap",
        function ($scope, $modalInstance, $modal, printerService, mservice, printerMap) {
            var vm = this;
            vm.saving = false;
            vm.printerMap = printerMap;
            $scope.existall = true;
            vm.templates = [];
            vm.printers = [];

            vm.save = function () {
                $modalInstance.close(vm.printerMap);
            };

            vm.openforMenuItem = function () {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    service: mservice.getAllMenuItems
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.printerMap.menuItemId = result.locations[0].id;
                        vm.printerMap.menuItemName = result.locations[0].name;

                        mservice.getMenuItemForEdit({ id: vm.printerMap.menuItemId })
                            .success(function (result) {
                                vm.printerMap.menuItemGroupCode = result.menuItem.categoryName;
                            });
                    }
                });
            };

            vm.getPrinterValue = function (item) {
                vm.printerMap.printerId = item.value;
                vm.printerMap.printerName = item.displayText;
            };

            vm.getTemplateValue = function (item) {
                vm.printerMap.printTemplateId = item.value;
                vm.printerMap.printTemplateName = item.displayText;
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            function init() {
                printerService.getPrinterTemplates()
                    .success(function (result) {
                        vm.templates = result.items;
                    });

                printerService.getPrintersForCombobox()
                    .success(function (result) {
                        vm.printers = result.items;
                    });
            }

            init();
        }
    ]);
})();