﻿(function () {
    appModule.controller('tenant.views.connect.master.tickettype.create', [
        "$scope", '$state', "$uibModal", '$stateParams', 'uiGridConstants', 'abp.services.app.ticketTypeConfiguration', 'abp.services.app.ticketType', "abp.services.app.connectLookup", 
        function ($scope,$state, $modal, $stateParams, uiGridConstants, ticketTypeConfigurationService, ticketTypeService, connectService) {
            var vm = this;
            vm.saving = false;
            vm.ticketTypeConfigurationId = $stateParams.id;
            vm.ticketType = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.ticketTypeOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Operations"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.removePortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Delete") + "\"><i class=\"fa fa-trash\"></i></button>" +
                            "  <button ng-click=\"grid.appScope.editPortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Edit") + "\"><i class=\"fa fa-edit\"></i></button>" +
                            "  <button ng-click=\"grid.appScope.viewPortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("View") + "\"><i class=\"fa fa-eye\"></i></button>" +
                            "  <button ng-click=\"grid.appScope.clonePortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Clone") + "\"><i class=\"fa fa-clone\"></i></button>" +
                            "</div>"
                    },
                    {
                        name: app.localize("TicketNumerator"),
                        field: "ticketNumeratorName"
                    },
                    {
                        name: app.localize("OrderNumerator"),
                        field: "orderNumeratorName"
                    },
                    {
                        name: app.localize("SaleTransactionType"),
                        field: "saleTransactionTypeName"
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        debugger;
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        init();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        init();
                    });
                },
                data: []
            };

            function init() {
                vm.saving = true;
                ticketTypeService.getTicketTypeForEdit({
                    id: vm.ticketTypeConfigurationId,
                    LocationGroupToBeFiltered: vm.filterLocationGroup
                }).success(function (result) {
                    vm.ticketTypeConfiguration = result.ticketTypeConfiguration;
                    vm.ticketTypeOptions.data = vm.ticketTypeConfiguration.ticketTypes;
                    vm.ticketTypeOptions.totalItems = vm.ticketTypeConfiguration.ticketTypes.length;
                    vm.ticketType = result.ticketType;
                    connectService.getNumerators({}).success(function (result) {
                        vm.numerators = result.items;
                    });

                    connectService.getTransactionTypesForCombobox({}).success(function (result) {
                        vm.transactionTypes = result.items;
                    });

                    connectService.getScreenMenus({}).success(function (result) {
                        vm.screenMenus = result.items;
                    });
                }).finally(function () {
                    vm.saving = false;
                });
            }

            init();

            vm.removePortion = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteTicketTypeWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            ticketTypeService.deleteTicketTypeMapping({
                                id: myObject.id
                            }).success(function (result) {
                                abp.notify.info(app.localize('Deleted'));
                                init();
                            }).finally(function () {
                                vm.saving = false;
                            });
                        }
                    }
                );
            };
            vm.editPortion = function (myObject) {
                vm.ticketType = myObject;
                myObject.locationGroup.viewOnly = false;
                vm.locationGroup = myObject.locationGroup;
                vm.tabtwo = true;
            }
            vm.clonePortion = function (myObject) {
                myObject.id = null;
                vm.ticketType = myObject;
                vm.intiailizeOpenLocation();
                vm.tabtwo = true;
            }
            vm.viewPortion = function (myObject) {
                myObject.locationGroup.viewOnly = true;
                vm.locationGroup = myObject.locationGroup;
                vm.openLocation();
                vm.intiailizeOpenLocation();
            };

            vm.cancel = function () {
                $state.go("tenant.tickettype");
            };

            vm.save = function () {
                vm.saving = true;
                vm.saveToDB();
            };

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };

            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForCreate();
            };
            vm.intiailizeOpenLocation();
            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.saveTicketType = function () {
                if (vm.isUndefinedOrNull(vm.ticketType.saleTransactionType_Id)) {
                    abp.notify.error(app.localize('SaleTransactionTypeIsMandatory'));
                    return;
                }
                if (vm.isUndefinedOrNull(vm.ticketType.ticketNumeratorId)) {
                    abp.notify.error(app.localize('TicketNumeratorIsMandatory'));
                    return;
                }
                if (vm.isUndefinedOrNull(vm.ticketType.ticketInvoiceNumeratorId)) {
                    abp.notify.error(app.localize('TicketInvoiceNumeratorIsMandatory'));
                    return;
                }
                if (vm.isUndefinedOrNull(vm.ticketType.orderNumeratorId)) {
                    abp.notify.error(app.localize('OrderNumeratorIsMandatory'));
                    return;
                }
                vm.ticketType.ticketTypeConfigurationId = vm.ticketTypeConfiguration.id;
                vm.ticketType.locationGroup = vm.locationGroup;

                ticketTypeService.addOrEditTicketType({
                    ticketType: vm.ticketType
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                }).finally(function () {
                    vm.tabone = true;
                    vm.intiailizeOpenLocation();
                    init();
                    vm.saving = false;
                });
            };

            vm.saveToDB = function () {
                ticketTypeConfigurationService.createOrUpdateTicketTypeConfiguration({
                    ticketTypeConfiguration: vm.ticketTypeConfiguration
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };

            vm.clearFilterLocation = function () {
                vm.filterLocationGroup = app.createLocationInputForCreate();
                init();
            };

            vm.clearFilterLocation();

            vm.openFilterLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.filterLocationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.filterLocationGroup = result;
                    init();
                });
            };
        }
    ]);
})();