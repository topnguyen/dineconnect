﻿(function () {
    appModule.controller('tenant.views.connect.master.tickettype.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.ticketType',
        function ($scope, $state, $modal, uiGridConstants, ticketTypeService) {
            var vm = this;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Connect.Master.TicketType.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Connect.Master.TicketType.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Connect.Master.TicketType.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useUiGridDraggableRowsHandle: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.edit" ng-click="grid.appScope.editTicketType(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.delete" ng-click="grid.appScope.deleteTicketType(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.isDeleted && grid.appScope.permissions.edit" ng-click="grid.appScope.activateItem(row.entity)">' + app.localize('Revert') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Id'),
                        field: 'id'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\''
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.createTicketType = function () {
                openCreateOrEditModal(null);
            };

            vm.editTicketType = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.createtickettype", {
                    id: objId
                });
            }

            vm.getAll = function () {
                vm.loading = true;
                ticketTypeService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    location: vm.location,
                    acceptChange: vm.acceptChange,
                    isDeleted: vm.isDeleted,
                    locationGroup: vm.locationGroup
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.deleteTicketType = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteTicketTypeWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            ticketTypeService.deleteTicketType({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.activateItem = function (myObject) {
                ticketTypeService.activateItem({
                    id: myObject.id
                }).success(function (result) {
                    abp.notify.success(app.localize("Successfully"));
                    vm.getAll();
                });
            };

            vm.exportToExcel = function () {
                ticketTypeService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    location: vm.location,
                    acceptChange: vm.acceptChange,
                    isDeleted: vm.isDeleted,
                    locationGroup: vm.locationGroup
                }).success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.currentLocation = null;
            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();
            vm.openLocation = function () {

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.currentLocation = result.locations[0];
                        vm.location = result.locations[0].name;
                    }
                    else if (result.groups.length > 0) {
                        vm.currentLocation = result.groups[0];
                        vm.location = result.groups[0].name;
                    }
                    else if (result.locationTags.length > 0) {
                        vm.currentLocation = result.locationTags[0];
                        vm.location = result.locationTags[0].name;
                    }
                    else {
                        vm.location = null;
                        vm.currentLocation = null;
                    }
                });
            };

            vm.clear = function () {
                vm.location = null;
                vm.currentLocation = null;
                vm.filterText = null;
                vm.acceptChange = null;
                vm.isDeleted = false;
                vm.intiailizeOpenLocation();

                vm.getAll();
            };

            vm.getAll();
        }]);
})();