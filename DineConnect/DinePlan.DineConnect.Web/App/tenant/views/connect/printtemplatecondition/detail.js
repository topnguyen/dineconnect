﻿(function () {
    appModule.controller("tenant.views.connect.printtemplatecondition.detail",
        [
            "$scope", "$state", 'uiGridConstants', "$uibModal", "$stateParams", "abp.services.app.printTemplateCondition",
            "abp.services.app.connectLookup", 'abp.services.app.printConfiguration', 'abp.services.app.commonLookup',
            function ($scope, $state, uiGridConstants, $modal, $stateParams, conditionService, lookService, printConfigurationService, commonService) {
                var vm = this;

                vm.conditionSchedules = [];
                vm.showRules = false;
                vm.fromCountLocked = false;
                vm.fromCountValueLocked = false;

                vm.printConfigurationId = $stateParams.id;
                vm.condition = null;

                var requestParams = {
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null
                };
                vm.isUndefinedOrNull = function (val) {
                    return angular.isUndefined(val) || val == null || val == '';
                };
                vm.save = function () {
                    if (vm.isUndefinedOrNull(vm.printConfiguration.name)) {
                        abp.notify.error('Name is mandatory');
                        return;
                    }
                    vm.printConfiguration.printConfigurationType = 3;
                    vm.saving = true;
                    printConfigurationService.createOrUpdatePrintConfiguration({
                        printConfiguration: vm.printConfiguration
                    }).success(function () {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        vm.cancel();
                    }).finally(function () {
                        vm.saving = false;
                    });
                };
                $scope.settings = {
                    dropdownToggleState: false,
                    time: {
                        fromHour: "00",
                        fromMinute: "00",
                        toHour: "23",
                        toMinute: "59"
                    },
                    noRange: false,
                    format: 24,
                    noValidation: true
                };

                $scope.$on("$viewContentLoaded",
                    function () {
                        App.initAjax();
                    });

                vm.allLocation = "";
                $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
                $scope.format = $scope.formats[0];
                const todayAsString = moment().format("YYYY-MM-DD");
                vm.dateRangeOptions = app.createDateRangeFuturePickerOptions();
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };

                vm.removeSchedule = function (productIndex) {
                    vm.conditionSchedules.splice(productIndex, 1);
                };

                vm.addSchedule = function () {
                    vm.conditionSchedules.push({
                        'startHour': $scope.settings.time.fromHour,
                        'endHour': $scope.settings.time.toHour,
                        'startMinute': $scope.settings.time.fromMinute,
                        'endMinute': $scope.settings.time.toMinute,
                        'allDays': vm.refday,
                        'allMonthDays': vm.refmonthday
                    });

                    vm.refday = [];
                    vm.refmonthday = [];
                };

                vm.changeFromCount = function (status) {
                    if (status) {
                        if (vm.freePrintTemplateCondition.from.length === 0) {
                            vm.freePrintTemplateCondition.fromCount = 1;
                        } else {
                            vm.freePrintTemplateCondition.fromCount = vm.freePrintTemplateCondition.from.length;
                        }
                        vm.fromCountLocked = true;
                    } else {
                        vm.fromCountLocked = false;
                    }
                };

                vm.changeFromValueCount = function (status) {
                    if (status) {
                        if (vm.freeValuePrintTemplateCondition.from.length === 0) {
                            vm.freeValuePrintTemplateCondition.fromCount = 1;
                        } else {
                            vm.freeValuePrintTemplateCondition.fromCount =
                                vm.freeValuePrintTemplateCondition.from.length;
                        }
                        vm.fromCountValueLocked = true;
                    } else {
                        vm.fromCountValueLocked = false;
                    }
                };



                vm.cancel = function () {
                    $state.go("tenant.printtemplatecondition");
                };

                vm.getEditionValue = function (item) {
                    return parseInt(item.value);
                };

                vm.refday = [];
                vm.refdays = [];

                function fillDays() {
                    vm.loading = true;
                    lookService.getDays({}).success(function (result) {
                        vm.refdays = result.items;
                    }).finally(function () {
                        vm.loading = false;
                    });
                }

                vm.refmonthday = [];
                vm.refmonthdays = [];

                function fillMonthDays() {
                    vm.loading = true;
                    lookService.getMonthDays({}).success(function (result) {
                        vm.refmonthdays = result.items;
                    }).finally(function () {
                        vm.loading = false;
                    });
                }

                $scope.builder =
                    app.createBuilder(
                        {
                            condition: "AND",
                            rules: [
                                {
                                    id: "Total",
                                    operator: "greater_or_equal",
                                    value: 0
                                }
                            ]
                        },
                        angular.fromJson
                            (
                                [
                                    {
                                        "id": "Total",
                                        "label": "Total",
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 1
                                        }
                                    },
                                    {
                                        "id": "Department",
                                        "label": app.localize("Department"),
                                        "type": "string",
                                        plugin: 'selectize',
                                        plugin_config: {
                                            valueField: 'value',
                                            labelField: 'displayText',
                                            searchField: 'displayText',
                                            sortField: 'displayText',
                                            create: true,
                                            maxItems: 1,
                                            plugins: ['remove_button'],
                                            onInitialize: function () {
                                                var that = this;
                                                JSON.parse(localStorage.departments).forEach(function (item) {
                                                    that.addOption(item);
                                                });
                                            }
                                        },
                                        valueSetter: function (rule, value) {
                                            rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                        }
                                    },
                                    {
                                        "id": "Member",
                                        "label": "IsMember",
                                        "type": "boolean"
                                    },
                                    {
                                        "id": "ProductGroup",
                                        "label": "Product Group",
                                        "type": "string",
                                        plugin: 'selectize',
                                        plugin_config: {
                                            valueField: 'value',
                                            labelField: 'displayText',
                                            searchField: 'displayText',
                                            sortField: 'displayText',
                                            create: true,
                                            maxItems: 1,
                                            plugins: ['remove_button'],
                                            onInitialize: function () {
                                                var that = this;
                                                JSON.parse(localStorage.productGroups).forEach(function (item) {
                                                    that.addOption(item);
                                                });
                                            },
                                            onChange: function (value) {
                                                localStorage.categories = [];
                                                $scope.buildDisable = true;
                                                if (value !== '') {
                                                    commonService.getCategoriesForCombobox(value).success(function (result) {
                                                        localStorage.categories = JSON.stringify(result.items);
                                                    });
                                                }
                                                $scope.buildDisable = false;

                                            }
                                        },
                                        valueSetter: function (rule, value) {
                                            rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                        }
                                    },
                                    {
                                        "id": "Category",
                                        "label": "Category",
                                        "type": "string",
                                        plugin: 'selectize',
                                        plugin_config: {
                                            valueField: 'value',
                                            labelField: 'displayText',
                                            searchField: 'displayText',
                                            sortField: 'displayText',
                                            create: true,
                                            maxItems: 1,
                                            plugins: ['remove_button'],
                                            onInitialize: function () {
                                                var that = this;
                                                JSON.parse(localStorage.categories).forEach(function (item) {
                                                    that.addOption(item);
                                                });
                                            },
                                            onChange: function (value) {
                                                localStorage.menuItems = [];
                                                $scope.buildDisable = true;
                                                if (value !== '') {
                                                    commonService.getMenuItemForCombobox(value).success(function (result) {
                                                        localStorage.menuItems = JSON.stringify(result.items);
                                                    });
                                                }
                                                $scope.buildDisable = false;

                                            }
                                        },
                                        valueSetter: function (rule, value) {
                                            rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                        }
                                    },
                                    {
                                        "id": "MenuItem",
                                        "label": "Menu Item",
                                        "type": "string",
                                        plugin: 'selectize',
                                        plugin_config: {
                                            valueField: 'value',
                                            labelField: 'displayText',
                                            searchField: 'displayText',
                                            sortField: 'displayText',
                                            create: true,
                                            maxItems: 1,
                                            plugins: ['remove_button'],
                                            onInitialize: function () {
                                                var that = this;
                                                JSON.parse(localStorage.menuItems).forEach(function (item) {
                                                    that.addOption(item);
                                                });
                                            }
                                        },
                                        valueSetter: function (rule, value) {
                                            rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                        }
                                    },
                                    {
                                        "id": "ItemCount",
                                        "label": "Item Count",
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "double",
                                        "validation": {
                                            "min": 0,
                                            "step": 1
                                        }
                                    }
                                ]
                            )
                    );

                vm.intiailizeOpenLocation = function() {
                    vm.locationGroup = app.createLocationInputForCreate();
                };
                vm.intiailizeOpenLocation();

                vm.openLocation = function () {
                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });

                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };

                vm.openFilterLocation = function () {
                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.filterLocationGroup;
                            }
                        }
                    });

                    modalInstance.result.then(function (result) {
                        vm.filterLocationGroup = result;
                        init();
                    });
                };
                vm.printtemplateconditionOptions = {
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                    columnDefs: [
                        {
                            name: app.localize("Operations"),
                            cellTemplate:
                                "<div class=\"ui-grid-cell-contents text-center\">" +
                                "  <button ng-click=\"grid.appScope.removePortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Delete") + "\"><i class=\"fa fa-trash\"></i></button>" +
                                "  <button ng-click=\"grid.appScope.editPortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Edit") + "\"><i class=\"fa fa-edit\"></i></button>" +
                                "  <button ng-click=\"grid.appScope.viewPortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("View") + "\"><i class=\"fa fa-eye\"></i></button>" +
                                "</div>"
                        },
                        {
                            name: app.localize("AliasName"),
                            field: "aliasName"
                        },
                        {
                            name: app.localize("StartDate"),
                            field: "startDate",
                            cellFilter: 'momentFormat: \'DD-MMM-YYYY\''
                        },
                        {
                            name: app.localize("EndDate"),
                            field: "endDate",
                            cellFilter: 'momentFormat: \'DD-MMM-YYYY\''
                        },
                        {
                            name: app.localize("Contents"),
                            field: "contents"
                        },
                        {
                            name: app.localize("Clone"),
                            width: 70,
                            cellTemplate:
                                "<div class=\"ui-grid-cell-contents text-center\">" +
                                "  <button ng-click=\"grid.appScope.clonePortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Clone") + "\"><i class=\"fa fa-clone\"></i></button>" +
                                "</div>"
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                            if (!sortColumns.length || !sortColumns[0].field) {
                                requestParams.sorting = null;
                            } else {
                                requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                            }
                            init();
                        });
                        gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                            requestParams.skipCount = (pageNumber - 1) * pageSize;
                            requestParams.maxResultCount = pageSize;
                            init();
                        });
                    },
                    data: []
                };

                vm.removePortion = function (myObject) {
                    abp.message.confirm(
                        app.localize('Are You Sure Want to delete the record ', myObject.id),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                conditionService.deletePrintTemplateCondition({
                                    id: myObject.id
                                }).success(function (result) {
                                    abp.notify.info(app.localize('Deleted'));
                                    init();
                                }).finally(function () {
                                    vm.saving = false;
                                });
                            }
                        }
                    );
                };
                vm.tabtwo = false;
                vm.editPortion = function (myObject) {
                    vm.condition = myObject;
                    if (vm.condition.id) {
                        vm.dateRangeModel = {
                            startDate: vm.condition.startDate,
                            endDate: vm.condition.endDate
                        };
                    }
                    if (vm.condition.conditionSchedules)
                        vm.conditionSchedules = angular.fromJson(vm.condition.conditionSchedules);

                    if (vm.condition.filter !== null)
                        $scope.builder.builder.setRules(angular.fromJson(vm.condition.filter));
                    myObject.locationGroup.viewOnly = false;
                    vm.locationGroup = myObject.locationGroup;
                    vm.tabtwo = true;
                }
                vm.clonePortion = function (myObject) {
                    myObject.id = null;
                    vm.condition = myObject;
                    if (vm.condition.id) {
                        vm.dateRangeModel = {
                            startDate: vm.condition.startDate,
                            endDate: vm.condition.endDate
                        };
                    }
                    if (vm.condition.conditionSchedules)
                        vm.conditionSchedules = angular.fromJson(vm.condition.conditionSchedules);

                    if (vm.condition.filter !== null)
                        $scope.builder.builder.setRules(angular.fromJson(vm.condition.filter));
                    vm.intiailizeOpenLocation();

                    vm.tabtwo = true;
                }
                vm.viewPortion = function (myObject) {
                    myObject.locationGroup.viewOnly = true;
                    vm.locationGroup = myObject.locationGroup;
                    vm.openLocation();
                    vm.intiailizeOpenLocation();

                };

                vm.savePortion = function () {
                    //if (vm.locationGroup.locations.length == 0 && vm.locationGroup.locationTags.length == 0 && vm.locationGroup.groups.length == 0 && vm.locationGroup.nonLocations.length == 0) {
                    //    abp.notify.error('Please Choose the location');
                    //    return;
                    //}
                    if (vm.isUndefinedOrNull(vm.condition.contents)) {
                        abp.notify.error('Contents is mandatory');
                        return;
                    }
                    vm.saving = true;
                    vm.condition.startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    vm.condition.endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    vm.condition.filter = angular.toJson($scope.builder.builder.getRules());
                    vm.condition.conditionSchedules = angular.toJson(vm.conditionSchedules);
                    vm.condition.printConfigurationId = vm.printConfiguration.id;
                    vm.condition.locationGroup = vm.locationGroup;
                    conditionService.addOrEditPrintTemplateCondition({
                        conditionEditDto: vm.condition
                    }).success(function () {
                        abp.notify.info(app.localize("SavedSuccessfully"));
                    }).finally(function () {
                        vm.tabone = true;
                        vm.intiailizeOpenLocation();

                        init();
                        vm.saving = false;
                    });
                };
                vm.clearFilterLocation = function() {
                    vm.filterLocationGroup = app.createLocationInputForCreate();
                    init();
                };


                vm.getDepartments = function () {
                    commonService.getDepartments({}).success(function (result) {
                        localStorage.departments = JSON.stringify(result);
                    });
                };
                vm.getProductGroupCombobox = function () {
                    commonService.getProductGroupCombobox().success(function (result) {
                        localStorage.productGroups = JSON.stringify(result.items);
                    });
                };
                vm.getCategoriesForCombobox = function (group) {
                    commonService.getCategoriesForCombobox(group).success(function (result) {
                        localStorage.categories = JSON.stringify(result.items);
                    });
                };
                vm.getMenuItemForCombobox = function (category) {
                    commonService.getMenuItemForCombobox(category).success(function (result) {
                        localStorage.menuItems = JSON.stringify(result.items);
                       });
                };

                vm.clearFilterLocation();

                function init() {
                    vm.getDepartments();
                    vm.getProductGroupCombobox();
                    vm.getCategoriesForCombobox('');
                    vm.getMenuItemForCombobox('');

                    vm.saving = true;
                    conditionService.getPrintTemplateConditionForEdit({
                        id: vm.printConfigurationId,
                        LocationGroupToBeFiltered: vm.filterLocationGroup
                    }).success(function (result) {
                        vm.printConfiguration = result.printConfiguration;
                        vm.condition = result.printTemplateCondition;
                        vm.printtemplateconditionOptions.data = vm.printConfiguration.printTemplateConditions;
                        vm.printtemplateconditionOptions.totalItems = vm.printConfiguration.printTemplateConditions.length;
                    });

                    fillDays();
                    fillMonthDays();
                    vm.saving = false;
                }

                init();

                vm.openRules = function () {
                    vm.showRules = !vm.showRules;
                };
            }
        ]);
})();