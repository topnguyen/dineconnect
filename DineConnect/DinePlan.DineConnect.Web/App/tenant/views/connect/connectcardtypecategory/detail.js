﻿
(function () {
    appModule.controller('tenant.views.connect.card.connectcardtypecategory.detail', [
        '$scope', '$state', '$stateParams', 'abp.services.app.connectCardTypeCategory', 
        function ($scope, $state, $stateParams,  connectcardtypecategoryService) {
            var vm = this;

            vm.saving = false;
            vm.connectcardtypecategory = null;
            vm.connectcardtypecategoryId = $stateParams.id;


            vm.save = function () {
                vm.saving = true;
                vm.connectcardtypecategory.code = vm.connectcardtypecategory.code.toUpperCase();
                vm.connectcardtypecategory.name = vm.connectcardtypecategory.name.toUpperCase();
                connectcardtypecategoryService.createOrUpdateConnectCardTypeCategory({
                    connectCardTypeCategory: vm.connectcardtypecategory
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $state.go('tenant.connectcardtypecategory');
            };
            function init() {
                connectcardtypecategoryService.getConnectCardTypeCategoryForEdit({
                    id: vm.connectcardtypecategoryId
                }).success(function (result) {
                    vm.connectcardtypecategory = result.connectCardTypeCategory;
                });
            }
            init();
        }
    ]);
})();

