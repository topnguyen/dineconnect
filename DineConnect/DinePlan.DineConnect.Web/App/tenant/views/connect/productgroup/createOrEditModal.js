﻿(function () {
    appModule.controller('tenant.views.connect.menu.productgroup.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.productGroup', 'groupId', 'uiGridConstants',
        function ($scope, $modalInstance, productgroupService, productgroupId, uiGridConstants) {
            var vm = this;

            vm.saving = false;
            vm.productgroup = null;
            $scope.existall = true;

            vm.save = function (argOption) {
                if ($scope.existall == false)
                    return;
                vm.saving = true;
                vm.productgroup.name = vm.productgroup.name.toUpperCase();
                vm.productgroup.code = vm.productgroup.code.toUpperCase();
                productgroupService.createOrUpdateProductGroup({
                    productGroup: vm.productgroup
                }).success(function (result) {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    if (argOption == 2) {
                        $modalInstance.close(result.id);
                        vm.saving = false;
                        return;
                    }
                    vm.getAll();
                    productgroupId = null;
                    init();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {
                if (vm.productgroup.name == null) {
                    vm.existall = false;
                    return;
                }

                productgroupService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'name',
                    filter: vm.productgroup.name,
                    operation: 'SEARCH'
                }).success(function (result) {
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.productgroup.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.productgroup.name + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            function init() {
                productgroupService.getProductGroupForEdit({
                    id: productgroupId
                }).success(function (result) {
                    vm.productgroup = result;
                });
            }
            init();

            //  Index JS Operation Included

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.House.Master.ProductGroup.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.House.Master.ProductGroup.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.House.Master.ProductGroup.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Delete'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.deleteProductGroup(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>' +
                            '</div>',
                        width: 50
                    },
                    {
                        name: app.localize('Edit'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.editProductGroup(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>' +
                            '</div>',
                        width: 50
                    },
                    {
                        name: app.localize('Code'),
                        field: 'code'
                    },
                    {
                        name: app.localize('name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                productgroupService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editProductGroup = function (myObj) {
                productgroupId = myObj.id;
                //openCreateOrEditModal(myObj.id);
                init();
            };

            //vm.createProductGroup = function () {
            //    openCreateOrEditModal(null);
            //};

            vm.deleteProductGroup = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteProductGroupWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            productgroupService.deleteProductGroup({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.getAll();
        }
    ]);
})();