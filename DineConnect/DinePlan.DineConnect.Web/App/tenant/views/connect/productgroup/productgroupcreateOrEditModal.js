﻿(function () {
    appModule.controller('tenant.views.connect.productgroup.productgroupcreateOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.productGroup', 'productGroup',
        function ($scope, $uibModalInstance, productGroupService, productGroup) {
            var vm = this;

            vm.productGroup = productGroup;

            vm.saving = false;

            vm.save = function () {
                if (vm.productGroup.id) {
                    productGroupService
                        .updateProductGroup(vm.productGroup)
                        .then(function (result) {
                            abp.notify.info(app.localize('SavedSuccessfully'));
                            $uibModalInstance.close(result.data);
                        });
                } else {
                    productGroupService
                        .createProductGroup(vm.productGroup)
                        .then(function (result) {
                            abp.notify.info(app.localize('SavedSuccessfully'));
                            $uibModalInstance.close(result.data);
                        });
                }
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }
    ]);
})();