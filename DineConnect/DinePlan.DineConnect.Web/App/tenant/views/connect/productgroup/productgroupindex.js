﻿(function () {
    appModule.controller('tenant.views.connect.productgroup.productgroupindex', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.productGroup',
        function ($scope, $uibModal, uiGridConstants, productGroupService) {
            var vm = this;
            vm.languageDescriptionType = 1;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.permissions = {
                manageProductGroup: abp.auth.hasPermission('Pages.Tenant.Connect.Menu.ProductGroup.ManageProductGroup')
            };

            vm.requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.productGroup = {

                $tree: null,

                unitCount: 0,

                setUnitCount: function (unitCount) {
                    $scope.safeApply(function () {
                        vm.productGroup.unitCount = unitCount;
                    });
                },

                refreshUnitCount: function () {
                    vm.productGroup.setUnitCount(vm.productGroup.$tree.jstree('get_json').length);
                },

                selectedOu: {
                    id: null,
                    code: null,
                    name: null,


                    set: function (ouInProductGroup) {
                        if (!ouInProductGroup) {
                            vm.productGroup.selectedOu.id = null;
                            vm.productGroup.selectedOu.code = null;
                            vm.productGroup.selectedOu.name = null;
                        } else {
                            vm.productGroup.selectedOu.id = ouInProductGroup.id;
                            vm.productGroup.selectedOu.code = ouInProductGroup.original.code;
                            vm.productGroup.selectedOu.name = ouInProductGroup.original.name;
                        }

                        vm.categories.load();
                    }
                },

                contextMenu: function (node) {

                    var items = {
                        editUnit: {
                            label: app.localize('Edit'),
                            _disabled: !vm.permissions.manageProductGroup,
                            action: function (data) {
                                var instance = $.jstree.reference(data.reference);

                                vm.productGroup.openCreateOrEditUnitModal({
                                    id: node.id,
                                    code: node.original.code,
                                    name: node.original.name
                                }, function (updatedOu) {
                                    node.original.code = updatedOu.code;
                                    node.original.name = updatedOu.name;
                                    instance.rename_node(node, vm.productGroup.generateTextOnTree(updatedOu));
                                });
                            }
                        },

                        addSubUnit: {
                            label: app.localize('AddSubUnit'),
                            _disabled: !vm.permissions.manageProductGroup,
                            action: function () {
                                vm.productGroup.addUnit(node.id);
                            }
                        },
                        'delete': {
                            label: app.localize("Delete"),
                            _disabled: !vm.permissions.manageProductGroup,
                            action: function (data) {
                                var instance = $.jstree.reference(data.reference);

                                abp.message.confirm(
                                    app.localize('ProductGroupDeleteWarningMessage', node.original.name),
                                    function (isConfirmed) {
                                        if (isConfirmed) {
                                            productGroupService.deleteProductGroup({
                                                id: node.id
                                            }).then(function () {
                                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                                instance.delete_node(node);
                                                vm.productGroup.refreshUnitCount();
                                            });
                                        }
                                    }
                                );
                            }
                        }
                    }

                    return items;
                },

                addUnit: function (parentId) {
                    var instance = $.jstree.reference(vm.productGroup.$tree);
                    vm.productGroup.openCreateOrEditUnitModal({
                        parentId: parentId
                    }, function (newOu) {
                        instance.create_node(
                            parentId ? instance.get_node(parentId) : '#',
                            {
                                id: newOu.id,
                                parent: newOu.parentId ? newOu.parentId : '#',
                                code: newOu.code,
                                name: newOu.name,
                                memberCount: 0,
                                text: vm.productGroup.generateTextOnTree(newOu),
                                state: {
                                    opened: true
                                }
                            });

                        vm.productGroup.refreshUnitCount();
                    });
                },

                openCreateOrEditUnitModal: function (productGroup, closeCallback) {
                    var modalInstance = $uibModal.open({
                        templateUrl: '~/App/tenant/views/connect/productgroup/productgroupcreateOrEditModal.cshtml',
                        controller: 'tenant.views.connect.productgroup.productgroupcreateOrEditModal as vm',
                        backdrop: 'static',
                        resolve: {
                            productGroup: function () {
                                return productGroup;
                            }
                        }
                    });

                    modalInstance.result.then(function (result) {
                        vm.productGroup.reload();
                        closeCallback && closeCallback(result);
                    });
                },

                generateTextOnTree: function (ou) {
                    var itemClass = ou.memberCount > 0 ? ' ou-text-has-members' : ' ou-text-no-members';
                    return '<span title="' + ou.code + '" class="ou-text' + itemClass + '" data-ou-id="' + ou.id + '">' + ou.name + ' (<span class="ou-text-member-count">' + ou.memberCount + '</span>) <i class="fa fa-caret-down text-muted"></i></span>';
                },

                incrementMemberCount: function (ouId, incrementAmount) {
                    var treeNode = vm.productGroup.$tree.jstree('get_node', ouId);
                    treeNode.original.memberCount = treeNode.original.memberCount + incrementAmount;
                    vm.productGroup.$tree.jstree('rename_node', treeNode, vm.productGroup.generateTextOnTree(treeNode.original));
                },

                getTreeDataFromServer: function (callback) {
                    productGroupService.getProductGroups({}).then(function (result) {
                        var treeData = _.map(result.data.items, function (item) {
                            item.memberCount = item.categories.length;
                            return {
                                id: item.id,
                                parent: item.parentId ? item.parentId : '#',
                                code: item.code,
                                name: item.name,
                                memberCount: item.memberCount,
                                text: vm.productGroup.generateTextOnTree(item),
                                state: {
                                    opened: true
                                }
                            };
                        });

                        callback(treeData);
                    });
                },

                init: function () {
                    vm.productGroup.getTreeDataFromServer(function (treeData) {
                        vm.productGroup.setUnitCount(treeData.length);

                        vm.productGroup.$tree = $('#ProductGroupEditTree');

                        var jsTreePlugins = [
                            'types',
                            'contextmenu',
                            'wholerow'
                        ];

                        if (vm.permissions.manageProductGroup) {
                            jsTreePlugins.push('dnd');
                        }

                        vm.productGroup.$tree
                            .on('changed.jstree', function (e, data) {
                                $scope.safeApply(function () {
                                    if (data.selected.length != 1) {
                                        vm.productGroup.selectedOu.set(null);
                                    } else {
                                        var selectedNode = data.instance.get_node(data.selected[0]);
                                        vm.productGroup.selectedOu.set(selectedNode);
                                    }
                                });

                            })
                            .on('move_node.jstree', function (e, data) {

                                if (!vm.permissions.manageProductGroup) {
                                    vm.productGroup.$tree.jstree('refresh'); //rollback
                                    return;
                                }

                                var parentNodeName = (!data.parent || data.parent == '#')
                                    ? app.localize('Root')
                                    : vm.productGroup.$tree.jstree('get_node', data.parent).original.name;

                                abp.message.confirm(
                                    app.localize('ProductGroupMoveConfirmMessage', data.node.original.name, parentNodeName),
                                    function (isConfirmed) {
                                        if (isConfirmed) {
                                            productGroupService.moveProductGroup({
                                                id: data.node.id,
                                                newParentId: data.parent
                                            }).then(function () {
                                                abp.notify.success(app.localize('SuccessfullyMoved'));
                                                vm.productGroup.reload();
                                            }).catch(function (err) {
                                                vm.productGroup.$tree.jstree('refresh'); //rollback
                                                setTimeout(function () { abp.message.error(err.data.message); }, 500);
                                            });
                                        } else {
                                            vm.productGroup.$tree.jstree('refresh'); //rollback
                                        }
                                    }
                                );
                            })
                            .jstree({
                                'core': {
                                    data: treeData,
                                    multiple: false,
                                    check_callback: function (operation, node, node_parent, node_position, more) {
                                        return true;
                                    }
                                },
                                types: {
                                    "default": {
                                        "icon": "fa fa-folder tree-item-icon-color icon-lg"
                                    },
                                    "file": {
                                        "icon": "fa fa-file tree-item-icon-color icon-lg"
                                    }
                                },
                                contextmenu: {
                                    items: vm.productGroup.contextMenu
                                },
                                
                                plugins: jsTreePlugins
                            });

                        vm.productGroup.$tree.on('click', '.ou-text .fa-caret-down', function (e) {
                            e.preventDefault();

                            var ouId = $(this).closest('.ou-text').attr('data-ou-id');
                            setTimeout(function () {
                                vm.productGroup.$tree.jstree('show_contextmenu', ouId);
                            }, 100);
                        });
                    });
                },

                reload: function () {
                    vm.productGroup.getTreeDataFromServer(function (treeData) {
                        vm.productGroup.setUnitCount(treeData.length);
                        vm.productGroup.$tree.jstree(true).settings.core.data = treeData;
                        vm.productGroup.$tree.jstree('refresh');
                    });
                }
            };

            vm.categories = {

                gridOptions: {
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    columnDefs: [
                        {
                            name: app.localize('Actions'),
                            enableSorting: false,
                            width: 100,
                        },
                        {
                            name: app.localize('Name'),
                            field: 'name',
                            minWidth: 140
                        },
                        {
                            name: app.localize('Code'),
                            field: 'code',
                            minWidth: 100
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                            if (!sortColumns.length || !sortColumns[0].field) {
                                vm.requestParams.sorting = null;
                            } else {
                                vm.requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                            }

                            vm.categories.load();
                        });
                        gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                            vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                            vm.requestParams.maxResultCount = pageSize;

                            vm.categories.load();
                        });
                    },
                    data: []
                },

                load: function () {
                    if (!vm.productGroup.selectedOu.id) {
                        vm.categories.gridOptions.totalItems = 0;
                        vm.categories.gridOptions.data = [];
                        return;
                    }

                    productGroupService.getProductGroupCategories($.extend({ id: vm.productGroup.selectedOu.id }, vm.requestParams))
                        .then(function (result) {
                            vm.categories.gridOptions.totalItems = result.data.totalCount;
                            vm.categories.gridOptions.data = result.data.items;
                        });
                },


                init: function () {
                    vm.categories.gridOptions.columnDefs.shift();
                }
            }

            vm.categories.init();
            vm.productGroup.init();

            vm.openLanguageDescriptionModal = function () {
                vm.languageDescription = {
                    id: vm.productGroup.selectedOu.id,
                    name: vm.productGroup.selectedOu.name,
                    languageDescriptionType: vm.languageDescriptionType
                };
                var modalInstance = $uibModal.open({
                    templateUrl: "~/App/tenant/views/connect/languageDescription/languageDescriptionModal.cshtml",
                    controller: "tenant.views.connect.languageDescription.languageDescriptionModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        languagedescription: function () {
                            return vm.languageDescription;
                        }
                    }
                });

                modalInstance.result.then(function (result) {

                });
            }

            vm.openCategory = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: "~/App/tenant/views/connect/report/items/item/selectCategory.cshtml",
                    controller: "tenant.views.connect.report.items.item.selectCategory as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.length > 0) {
                        vm.categoryIds = result;
                        vm.updateProductGroupCategory(vm.categoryIds);
                    }
                });
            };
            vm.updateProductGroupCategory = function (categoryIds)
            {
                productGroupService.updateCategoryProductgroupId({
                    productGroupId: vm.productGroup.selectedOu.id,
                    categoryIds: categoryIds
                }).success(function () {
                    abp.notify.info(app.localize('Category') + " " + app.localize('LinkedSuccessfully'));
                    vm.productGroup.reload();
                    vm.categories.load();
                });
            }
        }]);
})();