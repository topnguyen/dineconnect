﻿(function () {
    appModule.controller('tenant.views.connect.programSetting.detail', [
        '$scope', "$uibModal", '$state', '$stateParams', 'abp.services.app.programSetting', 'abp.services.app.programSettingTemplate',
        function ($scope, $modal, $state, $stateParams, programSettingService, programSettingTemplate) {
            var vm = this;
            vm.programSettingId = $stateParams.id;
            vm.templates = [];
            vm.values = [];

            vm.save = function () {
                vm.saving = true;

                programSettingService.createOrUpdateProgramSetting({
                    programSetting: vm.programSetting,
                    locationGroup: vm.locationGroup
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $state.go('tenant.programSetting');
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.programSetting');
            };

            function init() {
                vm.saving = true;
                programSettingService.getProgramSettingForEdit({
                    id: vm.programSettingId
                }).success(function (result) {
                    vm.programSetting = result.programSetting;
                    vm.locationGroup = result.locationGroup;
                    vm.saving = false;
                });

                programSettingService.getProgramSettingTemplates()
                    .success(function (result) {
                        vm.templates = result.items;
                    });
            }

            vm.programSettingTemplate = null;
            vm.selectedTemplate = function () {
                programSettingTemplate.getProgramSettingTemplateForEdit({
                    id: vm.programSetting.programSettingTemplateId
                }).success(function (result) {
                    vm.programSettingTemplate = result.programSettingTemplate;

                    vm.values = angular.fromJson(vm.programSettingTemplate.possibleValues);
                }).finally(function () {
                });
            };

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };

            init();
        }
    ]);
})();