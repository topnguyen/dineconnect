﻿(function () {
    appModule.controller('tenant.views.connect.pptOrSetting.index', [
        '$rootScope',
        '$scope',
        '$state',
        '$uibModal',
        'uiGridConstants',
        'abp.services.app.programSettingTemplate',
        function ($rootScope, $scope, $state, $modal, uiGridConstants, programSettingTemplateService) {
            var vm = this;
            vm.saving = false;
            vm.pttOrSettingsIds = null;
            vm.programSettingTemplate = null;
            vm.programSettingValue = null;
            vm.programSettingTemplateId = 8;
            vm.dataModel = {};

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            $rootScope.settings.layout.pageSidebarClosed = false;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };

            vm.save = function (refresh = false) {
                if (vm.programSettingTemplate.defaultValue == null || vm.programSettingTemplate.defaultValue == '') {
                    abp.notify.error('Default Value is mandatory');
                    return;
                }
                vm.saving = true;
                programSettingTemplateService
                    .createOrUpdateProgramSettingTemplate({
                        programSettingTemplate: vm.programSettingTemplate
                    })
                    .success(function (result) {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        vm.programSettingTemplate.id = result;
                        if (refresh)
                            vm.intiailizeOpenLocation();
                    })
                    .finally(function () {
                        if (refresh)
                            init();
                        vm.saving = false;
                    });
            };

            vm.savePortion = function () {
                if (
                    vm.locationGroup.locations.length == 0 &&
                    vm.locationGroup.locationTags.length == 0 &&
                    vm.locationGroup.groups.length == 0 &&
                    vm.locationGroup.nonLocations.length == 0
                ) {
                    abp.notify.error('Please Choose the location');
                    return;
                }
                if (!vm.programSettingValue) {
                    vm.programSettingValue = {};
                }
                if (vm.programSettingValue.actualValue == null || vm.programSettingValue.actualValue == '') {
                    abp.notify.error('Value is mandatory');
                    return;
                }
                vm.programSettingValue.programSettingTemplateId = vm.programSettingTemplate.id;
                vm.programSettingValue.locationGroup = vm.locationGroup;

                programSettingTemplateService
                    .addOrEditProgramSettingValues({
                        programSettingValueEditDto: vm.programSettingValue
                    })
                    .success(function (result) {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        vm.intiailizeOpenLocation();
                    })
                    .finally(function () {
                        init();
                        vm.saving = false;
                    });
            };

            vm.removePortion = function (myObject) {
                abp.message.confirm(app.localize('Are You Sure Want to delete the record ', myObject.id), function (isConfirmed) {
                    if (isConfirmed) {
                        programSettingTemplateService
                            .deleteProgramSettingValues({
                                id: myObject.id
                            })
                            .success(function (result) {
                                abp.notify.info(app.localize('Deleted'));
                                init();
                            })
                            .finally(function () {
                                vm.saving = false;
                            });
                    }
                });
            };

            vm.editPortion = function (myObject) {
                vm.programSettingValue = myObject;
                myObject.locationGroup.viewOnly = false;
                vm.locationGroup = vm.programSettingValue.locationGroup;
            };

            vm.openPortion = function () {
                if (
                    vm.locationGroup.locations.length == 0 &&
                    vm.locationGroup.locationTags.length == 0 &&
                    vm.locationGroup.groups.length == 0 &&
                    vm.locationGroup.nonLocations.length == 0
                ) {
                    abp.notify.error('Please Choose the location');
                    return;
                }
                programSettingTemplateService.getDepartmentList({
                    id: vm.programSettingValue.id,
                    programSettingTemplateId: vm.programSettingTemplate.id,
                    programSettingTemplateName: vm.programSettingTemplate.name,
                    locationGroup: vm.locationGroup,
                    actualValue: vm.programSettingValue.actualValue
                }).success(function (result) {
                    vm.programSettingValue = result;
                    openFormModal();
                });
            };

            vm.viewPortion = function (myObject) {
                myObject.locationGroup.viewOnly = true;
                vm.locationGroup = myObject.locationGroup;
                vm.openLocation();
                vm.intiailizeOpenLocation();
            };

            vm.editDefaultValue = function () {
                vm.programSettingTemplate.paymentDepartments = [];
                programSettingTemplateService.getDepartmentList({
                    id: vm.programSettingValue.id,
                    programSettingTemplateId: vm.programSettingTemplate.id,
                    programSettingTemplateName: vm.programSettingTemplate.name,
                    actualValue: vm.programSettingTemplate.defaultValue
                }).success(function (result) {
                    vm.programSettingTemplate.paymentDepartments = result.paymentDepartments;
                    openFormModal(true);
                });
            };

            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForCreate();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/location/select.cshtml',
                    controller: 'tenant.views.connect.location.select.location as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.openFilterLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/location/select.cshtml',
                    controller: 'tenant.views.connect.location.select.location as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.filterLocationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.filterLocationGroup = result;
                    init();
                });
            };

            vm.clearFilterLocation = function () {
                vm.filterLocationGroup = app.createLocationInputForSearch();
                init();
            };

            vm.onChangeTab = function (pttOrSettingsId) {
                vm.programSettingTemplate = null;
                vm.programSettingValue = null;
                vm.intiailizeOpenLocation();
                vm.programSettingTemplateId = pttOrSettingsId;

                init();
            };

            vm.programSettingValueOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate:
                    '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Delete'),
                        cellTemplate:
                            '<div class="ui-grid-cell-contents text-center">' +
                            '  <button ng-click="grid.appScope.removePortion(row.entity)" class="btn btn-default btn-xs" title="' +
                            app.localize('Delete') +
                            '"><i class="fa fa-trash"></i></button>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Edit'),
                        cellTemplate:
                            '<div class="ui-grid-cell-contents text-center">' +
                            '  <button ng-click="grid.appScope.editPortion(row.entity)" class="btn btn-default btn-xs" title="' +
                            app.localize('Edit') +
                            '"><i class="fa fa-edit"></i></button>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Value'),
                        cellTemplate: '<span class="cell-custom">{{grid.appScope.getActualValueName(row.entity)}}</span>'
                    },
                    {
                        name: app.localize('View'),
                        cellTemplate:
                            '<div class="ui-grid-cell-contents text-center">' +
                            '  <button ng-click="grid.appScope.viewPortion(row.entity)" class="btn btn-default btn-xs" title="' +
                            app.localize('View') +
                            '"><i class="fa fa-eye"></i></button>' +
                            '</div>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        init();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        init();
                    });
                },
                data: []
            };

            getPttOrSettingsIds();
            init();
            vm.clearFilterLocation();
            
            vm.getActualValueName = function (entity) {
                if (entity && entity.actualValue) {
                    value = angular.fromJson(entity.actualValue);
                    if (value.name || value.code || value.userName)
                        return value.name || value.code || value.userName;
                    else
                        return 'Click to edit data';
                } else {
                    return 'Click to edit data';
                }
            }

            function openFormModal(isDefault = false) {
                vm.dataModel = {
                    pttOrSettingsIds: vm.pttOrSettingsIds,
                    programSettingValue: vm.programSettingValue,
                    programSettingTemplate: vm.programSettingTemplate,
                    isDefault: isDefault
                };
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/programsettings/pttorsetting/templateModal/templateModal.cshtml',
                    controller: 'tenant.views.connect.pptOrSetting.templateModal as vm',
                    backdrop: 'static',
                    size: 'lg',
                    keyboard: false,
                    resolve: {
                        data: function () {
                            return vm.dataModel;
                        },
                    }
                });
                modalInstance.result.then(function (result ) {
                    if (isDefault) {
                        vm.programSettingTemplate.defaultValue = result.programSettingTemplate.defaultValue;
                        vm.programSettingTemplate.paymentDepartments = result.programSettingTemplate.paymentDepartments;
                        vm.save(true);

                    } else {
                        vm.programSettingValue.actualValue = result.programSettingValue.actualValue;
                        vm.programSettingValue.paymentDepartments = result.programSettingValue.paymentDepartments;
                    }
                });
            }

            function getPttOrSettingsIds() {
                vm.saving = true;
                programSettingTemplateService
                    .getPttOrSettingsIds()
                    .success(function (result) {
                        vm.pttOrSettingsIds = result;
                        vm.programSettingTemplateId = vm.pttOrSettingsIds.lms;
                    })
                    .finally(function () {
                        vm.saving = false;
                    });
            }

            function init() {
                vm.saving = true;
                programSettingTemplateService
                    .getProgramSettingTemplateForEdit({
                        id: vm.programSettingTemplateId,
                        LocationGroupToBeFiltered: vm.filterLocationGroup
                    })
                    .success(function (result) {
                        vm.programSettingTemplate = result.programSettingTemplate;
                        vm.programSettingValue = result.programSettingValueEditDto;
                        vm.programSettingValueOptions.data = vm.programSettingTemplate.programSettingValues;
                        vm.programSettingValueOptions.totalItems = vm.programSettingTemplate.programSettingValues.length;
                        vm.intiailizeOpenLocation();
                    })
                    .finally(function () {
                        vm.saving = false;
                    });
            }
        }
    ]);
})();
