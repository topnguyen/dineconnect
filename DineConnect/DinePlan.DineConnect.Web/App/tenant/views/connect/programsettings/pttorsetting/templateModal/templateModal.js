﻿(function () {
    appModule.controller('tenant.views.connect.pptOrSetting.templateModal', [
        '$scope',
        '$uibModalInstance',
        'data',
        function ($scope, $modalInstance, data) {
            var vm = this;
            vm.dataModel = data || {};
            vm.data = angular.fromJson(vm.dataModel.isDefault ? vm.dataModel.programSettingTemplate.defaultValue || {} : vm.dataModel.programSettingValue.actualValue || {});
            vm.programSettingTemplateId = vm.dataModel.programSettingTemplate.id;
            vm.pttOrSettingsIds = vm.dataModel.pttOrSettingsIds || {};
            
            vm.paymentDepartments = [];

            if (vm.dataModel.isDefault) {
                angular.forEach(vm.dataModel.programSettingTemplate.paymentDepartments, function (value, key) {
                    vm.paymentDepartments.push({
                        'id': value.id,
                        'uiId': value.uiId,
                        'name': value.name,
                        'isSelected': value.isSelected
                    });
                });
            }
            else {
                angular.forEach(vm.dataModel.programSettingValue.paymentDepartments, function (value, key) {
                    vm.paymentDepartments.push({
                        'id': value.id,
                        'uiId': value.uiId,
                        'name': value.name,
                        'isSelected': value.isSelected
                    });
                });
            }

            vm.ftiForms = [
                {
                    value: 'A4',
                    displayText: 'A4'
                },
                {
                    value: 'Slip',
                    displayText: 'Slip'
                }
            ];


            vm.close = function () {
                $modalInstance.dismiss();
            };

            vm.save = function () {

                if (vm.dataModel.isDefault) {
                    vm.dataModel.programSettingTemplate.paymentDepartments = [];
                    vm.dataModel.programSettingTemplate.defaultValue = angular.toJson(vm.data);

                    angular.forEach(vm.paymentDepartments, function (value, key) {
                        vm.dataModel.programSettingTemplate.paymentDepartments.push({
                            'id': value.id,
                            'uIId': value.uIId,
                            'name': value.name,
                            'isSelected': value.isSelected
                        });
                    });
                }
                else {
                    vm.dataModel.programSettingValue.paymentDepartments = [];

                    vm.data.lmsPaymentDepartments = '';
                    vm.data.hanaDepartments = '';

                    angular.forEach(vm.paymentDepartments, function (value, key) {
                        vm.dataModel.programSettingValue.paymentDepartments.push({
                            'id': value.id,
                            'uIId': value.uIId,
                            'name': value.name,
                            'isSelected': value.isSelected
                        });
                        if (value.isSelected) {
                            if (vm.data.lmsPaymentDepartments)
                                vm.data.lmsPaymentDepartments = vm.data.lmsPaymentDepartments + ', ' + value.id;
                            else
                                vm.data.lmsPaymentDepartments = value.id;

                            if (vm.data.hanaDepartments)
                                vm.data.hanaDepartments = vm.data.hanaDepartments + ', ' + value.id;
                            else
                                vm.data.hanaDepartments = value.id;
                        }
                    });
                    vm.dataModel.programSettingValue.actualValue = angular.toJson(vm.data);
                }

                $modalInstance.close(vm.dataModel);
            };
        }
    ]);
})();
