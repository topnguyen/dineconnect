﻿(function () {
    appModule.controller('tenant.views.connect.programSettingTemplate.detail', [
        '$scope', "$uibModal", 'uiGridConstants', '$state', '$stateParams', 'abp.services.app.programSettingTemplate',
        function ($scope, $modal, uiGridConstants, $state, $stateParams, programSettingTemplateService) {
            var vm = this;
            vm.programSettingTemplateId = $stateParams.id;
            vm.programSettingTypes = [];

            vm.programSettingTemplate = null;
            vm.programSettingValue = null;

            vm.save = function (argOption) {
                if (vm.programSettingTemplate.name == null || vm.programSettingTemplate.name == '') {
                    abp.notify.error('Name is mandatory');
                    return;
                }
                if (vm.programSettingTemplate.defaultValue == null || vm.programSettingTemplate.defaultValue == '') {
                    abp.notify.error('Default Value is mandatory');
                    return;
                }
                vm.saving = true;
                programSettingTemplateService.createOrUpdateProgramSettingTemplate({
                    programSettingTemplate: vm.programSettingTemplate
                }).success(function (result) {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.programSettingTemplate.id = result;
                    if (argOption == 2)
                        $state.go('tenant.programSettingTemplate');
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForCreate();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };



            vm.openFilterLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.filterLocationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.filterLocationGroup = result;
                    init();
                });
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.cancel = function () {
                $state.go('tenant.programSettingTemplate');
            };

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };


            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.savePortion = function () {
                if (vm.locationGroup.locations.length == 0 && vm.locationGroup.locationTags.length == 0 && vm.locationGroup.groups.length == 0 && vm.locationGroup.nonLocations.length == 0) {
                    abp.notify.error('Please Choose the location');
                    return;
                }
                if (vm.programSettingValue.actualValue == null || vm.programSettingValue.actualValue == '') {
                    abp.notify.error('Value is mandatory');
                    return;
                }
                vm.programSettingValue.programSettingTemplateId = vm.programSettingTemplate.id;
                vm.programSettingValue.locationGroup = vm.locationGroup;

                programSettingTemplateService.addOrEditProgramSettingValues({
                    programSettingValueEditDto: vm.programSettingValue
                }).success(function (result) {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.intiailizeOpenLocation();
                }).finally(function () {
                    init();
                    vm.saving = false;
                });
            }


            //vm.setGrid = function () {

            vm.programSettingValueOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Delete"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.removePortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Delete") + "\"><i class=\"fa fa-trash\"></i></button>" +
                            "</div>"
                    },
                    {
                        name: app.localize("Edit"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.editPortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Edit") + "\"><i class=\"fa fa-edit\"></i></button>" +
                            "</div>"
                    },
                    {
                        name: app.localize("Value"),
                        field: "actualValue"
                    },
                    {
                        name: app.localize("View"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.viewPortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("View") + "\"><i class=\"fa fa-eye\"></i></button>" +
                            "</div>"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        init();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        init();
                    });
                },
                data: []
            };
            //}

            //vm.setGrid();

            vm.removePortion = function (myObject) {
                abp.message.confirm(
                    app.localize('Are You Sure Want to delete the record ', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            programSettingTemplateService.deleteProgramSettingValues({
                                id: myObject.id
                            }).success(function (result) {
                                abp.notify.info(app.localize('Deleted'));
                                init();
                            }).finally(function () {
                                vm.saving = false;
                            });
                        }
                    }
                );
            };

            vm.editPortion = function (myObject) {
                vm.programSettingValue = myObject;
                myObject.locationGroup.viewOnly = false;
                vm.locationGroup = myObject.locationGroup;
            }

            vm.viewPortion = function (myObject) {
                myObject.locationGroup.viewOnly = true;
                vm.locationGroup = myObject.locationGroup;
                vm.openLocation();
                vm.intiailizeOpenLocation();
            };


            function init() {
                vm.saving = true;
                programSettingTemplateService.getProgramSettingTemplateForEdit({
                    id: vm.programSettingTemplateId,
                    LocationGroupToBeFiltered: vm.filterLocationGroup
                }).success(function (result) {
                    vm.programSettingTemplate = result.programSettingTemplate;
                    vm.programSettingValue = result.programSettingValueEditDto;
                    vm.programSettingValueOptions.data = vm.programSettingTemplate.programSettingValues;
                    vm.programSettingValueOptions.totalItems = vm.programSettingTemplate.programSettingValues.length;
                }).finally(function () {
                    vm.saving = false;
                });
            }


            vm.clearFilterLocation = function () {
                vm.filterLocationGroup = app.createLocationInputForSearch();
                init();
            }

            vm.clearFilterLocation();

        }
    ]);
})();