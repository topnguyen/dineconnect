﻿(function () {
    appModule
        .controller('tenant.views.connect.dinedevice.detail', [
            '$scope',
            '$uibModal',
            '$state',
            '$stateParams',
            'abp.services.app.dineDevice',
            'abp.services.app.printer',
            function ($scope, $modal, $state, $stateParams, dinedeviceService, printerService) {
                var vm = this;

                vm.saving = false;
                vm.dinedevice = null;
                vm.dinedeviceId = $stateParams.id;
                vm.dinedeviceTypes = [];
                vm.uilimit = 50;
                vm.dinedevicefields = [];
                vm.printers = [];

                vm.dineChefFields = [];
                vm.dineQueueFields = [];
                vm.dineChefModel = [];
                vm.dineQueueModel = [];
                vm.isVisibleBind = false;
                vm.isUndefinedOrNull = function (val) {
                    return angular.isUndefined(val) || val == null || val == '';
                };

                vm.getEditionValue = function (item) {
                    return parseInt(item.value);
                };

                function fillDropDownForDineDeviceType() {
                    dinedeviceService.getDineDeviceTypes({}).success(function (result) {
                        vm.dinedeviceTypes = result.items;
                    });
                }
                fillDropDownForDineDeviceType();

                vm.save = function () {
                    if (vm.form.$invalid) {
                        if (vm.form.$error.max && vm.form.$error.max[0].$name.includes('ElapsedSeconds')) {
                            abp.message.error(app.localize('ElapsedSecondsMustLessThanOrEqualTo60'));
                            return;
                        }
                        abp.message.error(app.localize('InvalidFormMessage'));
                        return;
                    }

                    if (vm.dinedeviceType.TicketSettingData) {
                        const TicketStates = vm.dinedeviceType.TicketSettingData.TicketStates || [];

                        if (TicketStates.length > 1) {
                            for (let index = 1; index < TicketStates.length; index++) {
                                const previousE = TicketStates[index - 1];
                                const currentE = TicketStates[index];

                                if (previousE.ElapsedMinutes * 60 + previousE.ElapsedSeconds >= currentE.ElapsedMinutes * 60 + currentE.ElapsedSeconds) {
                                    abp.message.error(app.localize('InconsistencyBetweenElapsedTimeLevel'));
                                    return;
                                }
                            }
                        }
                    }
                    vm.dinedevice.settings = angular.toJson(vm.dinedeviceType);
                    vm.saving = true;
                    dinedeviceService
                        .createOrUpdateDineDevice({
                            dineDevice: vm.dinedevice,
                            locationGroup: vm.locationGroup
                        })
                        .success(function () {
                            abp.notify.info(app.localize('SavedSuccessfully'));
                            vm.cancel();
                        })
                        .finally(function () {
                            vm.saving = false;
                        });
                };

                vm.onchangedinedevicetype = function () {
                    initSettings();
                };

                vm.cancel = function () {
                    $state.go('tenant.dinedevice');
                };

                vm.openLocation = function () {
                    var modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/connect/location/select.cshtml',
                        controller: 'tenant.views.connect.location.select.location as vm',
                        backdrop: 'static',
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });

                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };

                function init() {
                    dinedeviceService
                        .getDineDeviceForEdit({
                            id: vm.dinedeviceId
                        })
                        .success(function (result) {
                            vm.dinedevice = result.dineDevice;
                            vm.locationGroup = result.locationGroup;

                            printerService.getPrintersForCombobox().success(function (result) {
                                vm.printers = result.items || [];

                                initSettings();
                            });

                            if (vm.dinedevice.id == null) {
                                vm.uilimit = 50;
                                vm.dinedevice.deviceType = null;
                            } else {
                                vm.uilimit = null;
                            }
                        });
                }

                function initSettings() {
                    initFormly();

                    if (vm.dinedevice.deviceType == 1) {
                        vm.dinedevicefields = vm.dineQueueFields;
                    } else if (vm.dinedevice.deviceType == 0) {
                        vm.dinedevicefields = vm.dineChefFields;
                    } else {
                        vm.dinedevicefields = [];
                    }

                    if (vm.dinedevice.settings) {
                        vm.dinedeviceType = angular.fromJson(vm.dinedevice.settings);
                        console.log({ setting: vm.dinedeviceType });
                    } else {
                        if (vm.dinedevice.deviceType == 1) {
                            vm.dinedeviceType = vm.dineQueueModel;
                        } else if (vm.dinedevice.deviceType == 0) {
                            vm.dinedeviceType = vm.dineChefModel;
                        } else {
                            vm.dinedeviceType = null;
                        }
                    }
                }

                function initFormly() {
                    vm.dineChefFields = [
                        {
                            key: 'TicketSettingData',
                            type: 'section',
                            templateOptions: {
                                label: 'Ticket Setting Data',
                                fields: [
                                    {
                                        key: 'UserPreferences',
                                        type: 'section',
                                        templateOptions: {
                                            label: 'User Preferences',
                                            fields: [
                                                {
                                                    key: 'PageSize',
                                                    type: 'input',
                                                    className: 'col-md-12',
                                                    templateOptions: {
                                                        label: 'Number of tickets per page',
                                                        type: 'number',
                                                        onKeypress: function (model, options, _this, evt) {
                                                            if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                evt.preventDefault();
                                                            }
                                                        }
                                                    }
                                                },
                                                {
                                                    key: 'PageSizeTicket',
                                                    type: 'input',
                                                    className: 'col-md-12',
                                                    templateOptions: {
                                                        label: 'Number of item per ticket',
                                                        type: 'number',
                                                        onKeypress: function (model, options, _this, evt) {
                                                            if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                evt.preventDefault();
                                                            }
                                                        }
                                                    }
                                                },
                                                {
                                                    type: 'checkbox',
                                                    key: 'CompleteTicket',
                                                    className: 'col-md-3',
                                                    templateOptions: {
                                                        label: 'Complete ticket when all orders is completed'
                                                    }
                                                },
                                                {
                                                    type: 'checkbox',
                                                    key: 'NewTicketWillBeShow',
                                                    className: 'col-md-3',
                                                    templateOptions: {
                                                        label: 'New ticket will be shown at the beginning'
                                                    }
                                                },
                                                {
                                                    type: 'checkbox',
                                                    key: 'ShowPartialCompleted',
                                                    className: 'col-md-3',
                                                    templateOptions: {
                                                        label: 'Show partial completed orders in completed page'
                                                    }
                                                },
                                                {
                                                    type: 'checkbox',
                                                    key: 'ShowOnlyInCompletedOrders',
                                                    className: 'col-md-3',
                                                    templateOptions: {
                                                        label: 'Show Only In Completed Orders'
                                                    }
                                                },
                                                {
                                                    type: 'checkbox',
                                                    key: 'ShowQueueNumber',
                                                    className: 'col-md-3',
                                                    templateOptions: {
                                                        label: 'Show Queue Number'
                                                    }
                                                },
                                                {
                                                    type: 'checkbox',
                                                    key: 'UpdateQueue',
                                                    className: 'col-md-3',
                                                    templateOptions: {
                                                        label: 'Update Queue'
                                                    }
                                                },
                                                {
                                                    type: 'checkbox',
                                                    key: 'AdditionalOrder',
                                                    className: 'col-md-3',
                                                    templateOptions: {
                                                        label: 'Additional Order On Top'
                                                    }
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        key: 'AllStates',
                                        type: 'section',
                                        templateOptions: {
                                            label: 'All States',
                                            fields: [
                                                {
                                                    key: 'Font',
                                                    type: 'input',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        label: 'Font',
                                                        type: 'string'
                                                    }
                                                },
                                                {
                                                    key: 'Header1FontSize',
                                                    type: 'input',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        label: 'Header 1 Font Size',
                                                        type: 'number',
                                                        onKeypress: function (model, options, _this, evt) {
                                                            if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                evt.preventDefault();
                                                            }
                                                        }
                                                    }
                                                },
                                                {
                                                    type: 'sampleText',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        isFontSize: true,
                                                        fieldFontSize: 'Header1FontSize'
                                                    }
                                                },
                                                {
                                                    key: 'Header2FontSize',
                                                    type: 'input',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        label: 'Header 2 Font Size',
                                                        type: 'number',
                                                        onKeypress: function (model, options, _this, evt) {
                                                            if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                evt.preventDefault();
                                                            }
                                                        }
                                                    }
                                                },
                                                {
                                                    type: 'sampleText',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        isFontSize: true,
                                                        fieldFontSize: 'Header2FontSize'
                                                    }
                                                },
                                                {
                                                    key: 'TableNameFontSize',
                                                    type: 'input',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        label: 'Table Name Font Size',
                                                        type: 'number',
                                                        onKeypress: function (model, options, _this, evt) {
                                                            if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                evt.preventDefault();
                                                            }
                                                        }
                                                    }
                                                },
                                                {
                                                    type: 'sampleText',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        isFontSize: true,
                                                        fieldFontSize: 'TableNameFontSize'
                                                    }
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        key: 'NewState',
                                        type: 'section',
                                        templateOptions: {
                                            label: 'New',
                                            fields: [
                                                {
                                                    key: 'Background',
                                                    type: 'colorPicker',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        label: 'Background',
                                                        type: 'string'
                                                    }
                                                },
                                                {
                                                    key: 'Foreground',
                                                    type: 'colorPicker',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        label: 'Foreground',
                                                        type: 'string'
                                                    }
                                                },
                                                {
                                                    type: 'sampleText',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        isBackgroundColor: true,
                                                    }
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        key: 'TicketStates',
                                        type: 'repeatSection',
                                        templateOptions: {
                                            label: 'States',
                                            btnText: 'Add State',
                                            btnRemoveText: 'Remove',
                                            fields: [
                                                {
                                                    key: 'Level',
                                                    type: 'input',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        label: 'Level',
                                                        type: 'string'
                                                    }
                                                },
                                                {
                                                    key: 'ElapsedMinutes',
                                                    type: 'input',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        label: 'Elapsed Minutes',
                                                        type: 'number',
                                                        onKeypress: function (model, options, _this, evt) {
                                                            if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                evt.preventDefault();
                                                            }
                                                        }
                                                    }
                                                },
                                                {
                                                    key: 'ElapsedSeconds',
                                                    type: 'input',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        label: 'Elapsed Seconds',
                                                        type: 'number',
                                                        max: 60,
                                                        onKeypress: function (model, options, _this, evt) {
                                                            if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                evt.preventDefault();
                                                            }
                                                        }
                                                    }
                                                },
                                                {
                                                    key: 'Background',
                                                    type: 'colorPicker',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        label: 'Background',
                                                        type: 'text'
                                                    }
                                                },
                                                {
                                                    key: 'Foreground',
                                                    type: 'colorPicker',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        label: 'Foreground',
                                                        type: 'text'
                                                    }
                                                },
                                                {
                                                    type: 'sampleText',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        isBackgroundColor: true,
                                                    }
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        key: 'CompletedState',
                                        type: 'section',
                                        templateOptions: {
                                            label: 'Completed',
                                            fields: [
                                                {
                                                    key: 'Background',
                                                    type: 'colorPicker',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        label: 'Background',
                                                        type: 'string'
                                                    }
                                                },
                                                {
                                                    key: 'Foreground',
                                                    type: 'colorPicker',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        label: 'Foreground',
                                                        type: 'string'
                                                    }
                                                },
                                                {
                                                    type: 'sampleText',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        isBackgroundColor: true,
                                                    }
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        key: 'ComboColorSettings',
                                        type: 'section',
                                        templateOptions: {
                                            label: 'Combo Color',
                                            fields: [
                                                {
                                                    key: 'Background',
                                                    type: 'colorPicker',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        label: 'Background',
                                                        type: 'string'
                                                    }
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        key: 'Items',
                                        type: 'section',
                                        templateOptions: {
                                            label: 'Items',
                                            fields: [
                                                {
                                                    key: 'Cooking',
                                                    className: 'row',
                                                    fieldGroup: [
                                                        {
                                                            key: 'Background',
                                                            type: 'colorPicker',
                                                            className: 'col-md-2',
                                                            templateOptions: {
                                                                label: 'Background (Cooking)',
                                                                type: 'text'
                                                            }
                                                        },
                                                        {
                                                            key: 'Foreground',
                                                            type: 'colorPicker',
                                                            className: 'col-md-2',
                                                            templateOptions: {
                                                                label: 'Foreground (Cooking)',
                                                                type: 'text'
                                                            }
                                                        },
                                                        {
                                                            type: 'sampleText',
                                                            className: 'col-md-2',
                                                            templateOptions: {
                                                                isBackgroundColor: true,
                                                            }
                                                        }
                                                    ]
                                                },
                                                {
                                                    key: 'Done',
                                                    className: 'row',
                                                    fieldGroup: [
                                                        {
                                                            key: 'Background',
                                                            type: 'colorPicker',
                                                            className: 'col-md-2',
                                                            templateOptions: {
                                                                label: 'Background (Done)',
                                                                type: 'text'
                                                            }
                                                        },
                                                        {
                                                            key: 'Foreground',
                                                            type: 'colorPicker',
                                                            className: 'col-md-2',
                                                            templateOptions: {
                                                                label: 'Foreground (Done)',
                                                                type: 'text'
                                                            }
                                                        },
                                                        {
                                                            type: 'sampleText',
                                                            className: 'col-md-2',
                                                            templateOptions: {
                                                                isBackgroundColor: true,
                                                            }
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        key: 'Button',
                                        type: 'section',
                                        templateOptions: {
                                            label: 'Button',
                                            fields: [
                                                {
                                                    key: 'Done',
                                                    className: 'row',
                                                    fieldGroup: [
                                                        {
                                                            key: 'Background',
                                                            type: 'colorPicker',
                                                            className: 'col-md-2',
                                                            templateOptions: {
                                                                label: 'Background (Done)',
                                                                type: 'text'
                                                            }
                                                        },
                                                        {
                                                            key: 'Foreground',
                                                            type: 'colorPicker',
                                                            className: 'col-md-2',
                                                            templateOptions: {
                                                                label: 'Foreground (Done)',
                                                                type: 'text'
                                                            }
                                                        },
                                                        {
                                                            type: 'sampleText',
                                                            className: 'col-md-2',
                                                            templateOptions: {
                                                                isBackgroundColor: true,
                                                            }
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        key: 'TicketHeight',
                                        type: 'section',
                                        templateOptions: {
                                            label: 'Ticket Height',
                                            fields: [
                                                {
                                                    key: 'Classic',
                                                    className: 'row',
                                                    fieldGroup: [
                                                        {
                                                            type: 'labelText',
                                                            className: 'col-md-1',
                                                            templateOptions: {
                                                                labelText: 'Classic/Sticky',
                                                                isVisible: true,
                                                            }
                                                        },
                                                        {
                                                            key: 'Height',
                                                            type: 'input',
                                                            className: 'col-md-2',
                                                            templateOptions: {
                                                                label: 'Height',
                                                                type: 'number',
                                                                onKeypress: function (model, options, _this, evt) {
                                                                    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                        evt.preventDefault();
                                                                    }
                                                                }
                                                            }
                                                        },
                                                        {
                                                            key: 'Width',
                                                            type: 'input',
                                                            className: 'col-md-2',
                                                            templateOptions: {
                                                                label: 'Width',
                                                                type: 'number',
                                                                onKeypress: function (model, options, _this, evt) {
                                                                    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                        evt.preventDefault();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    ]
                                                },
                                                {
                                                    key: 'Compact',
                                                    className: 'row',
                                                    fieldGroup: [
                                                        {
                                                            type: 'labelText',

                                                            className: 'col-md-1',
                                                            templateOptions: {
                                                                labelText: 'Compact',
                                                                isVisible: true,
                                                            }
                                                        },
                                                        {
                                                            key: 'Height',
                                                            type: 'input',
                                                            className: 'col-md-2',
                                                            templateOptions: {
                                                                label: 'Height',
                                                                type: 'number',
                                                                onKeypress: function (model, options, _this, evt) {
                                                                    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                        evt.preventDefault();
                                                                    }
                                                                }
                                                            }
                                                        },
                                                        {
                                                            key: 'Width',
                                                            type: 'input',
                                                            className: 'col-md-2',
                                                            templateOptions: {
                                                                label: 'Width',
                                                                type: 'number',
                                                                onKeypress: function (model, options, _this, evt) {
                                                                    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                        evt.preventDefault();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    ]
                                                },
                                                {
                                                    key: 'Grid',
                                                    className: 'row',
                                                    fieldGroup: [
                                                        {
                                                            type: 'labelText',
                                                            className: 'col-md-1',

                                                            templateOptions: {
                                                                labelText: 'Grid',
                                                                isVisible: true,
                                                            }
                                                        },
                                                        {
                                                            key: 'Height',
                                                            type: 'input',
                                                            className: 'col-md-2',
                                                            templateOptions: {
                                                                label: 'Height',
                                                                type: 'number',
                                                                onKeypress: function (model, options, _this, evt) {
                                                                    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                        evt.preventDefault();
                                                                    }
                                                                }
                                                            }
                                                        },
                                                        {
                                                            key: 'Width',
                                                            type: 'input',
                                                            className: 'col-md-2',
                                                            templateOptions: {
                                                                label: 'Width',
                                                                type: 'number',
                                                                onKeypress: function (model, options, _this, evt) {
                                                                    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                        evt.preventDefault();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        key: 'OrderItemName',
                                        type: 'section',
                                        templateOptions: {
                                            label: 'Order Item Name',
                                            fields: [
                                                {
                                                    key: 'ShowMenuItemName',
                                                    type: 'checkbox',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        label: 'Show Menu Item Name'
                                                    }
                                                },
                                                {
                                                    key: 'ShowAliasName',
                                                    type: 'checkbox',
                                                    className: 'col-md-2',
                                                    templateOptions: {
                                                        label: 'Show Alias Name'
                                                    }
                                                }
                                            ]
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            key: 'Categories',
                            type: 'repeatSection',
                            templateOptions: {
                                label: 'Show Order Categories',
                                btnText: 'Add new Categories',
                                btnRemoveText: 'Remove',
                                fields: [
                                    {
                                        key: 'Name',
                                        type: 'input',
                                        className: 'col-md-6',
                                        templateOptions: {
                                            type: 'string'
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            key: 'TicketStatues',
                            type: 'section',
                            templateOptions: {
                                label: 'Ticket Statues',
                                fields: [
                                    {
                                        key: 'UpdateStatus',
                                        type: 'checkbox',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Update Status'
                                        }
                                    },
                                    {
                                        key: 'ReceiveOrderState',
                                        type: 'input',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Receive Order State',
                                            type: 'text'
                                        }
                                    },
                                    {
                                        key: 'SendOrderState',
                                        type: 'input',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'SendOrderState',
                                            type: 'text'
                                        }
                                    },
                                    {
                                        key: 'StartTicketState',
                                        type: 'input',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Start Ticket State',
                                            type: 'text'
                                        }
                                    },
                                    {
                                        key: 'FinishedTicketState',
                                        type: 'input',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Finished Ticket State',
                                            type: 'text'
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            key: 'DepartmentFiltersPosition',
                            type: 'dineSelect',
                            className: 'col-md-12',
                            templateOptions: {
                                label: 'Department Filters Position',
                                valueProp: 'id',
                                labelProp: 'name',
                                options: [
                                    {
                                        name: '--Select--',
                                        id: ''
                                    },
                                    {
                                        name: 'Right',
                                        id: 'right'
                                    },
                                    {
                                        name: 'Bottom',
                                        id: 'bottom'
                                    }
                                ]
                            }
                        },
                        {
                            key: 'Theme',
                            type: 'dineSelect',
                            className: 'col-md-12',
                            templateOptions: {
                                label: 'Theme',
                                valueProp: 'id',
                                labelProp: 'name',
                                options: [
                                    {
                                        name: '--Select--',
                                        id: ''
                                    },
                                    {
                                        name: 'White',
                                        id: 'white'
                                    },
                                    {
                                        name: 'Green',
                                        id: 'green'
                                    },
                                    {
                                        name: 'Blue',
                                        id: 'blue'
                                    },
                                    {
                                        name: 'Yellow',
                                        id: 'yellow'
                                    }
                                ]
                            }
                        },
                        {
                            key: 'Culturel',
                            type: 'dineSelect',
                            className: 'col-md-12',
                            templateOptions: {
                                label: 'Culture (Application need to restart)',
                                valueProp: 'id',
                                labelProp: 'name',
                                options: [
                                    {
                                        name: '--Select--',
                                        id: ''
                                    },
                                    {
                                        name: 'System',
                                        id: 'system'
                                    },
                                    {
                                        name: 'Chinese Simplified',
                                        id: 'chinese simplified'
                                    }
                                ]
                            }
                        },
                        {
                            key: 'Others',
                            type: 'section',
                            templateOptions: {
                                label: 'Others',
                                fields: [
                                    {
                                        key: 'WindowScale',
                                        type: 'input',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Window Scale',
                                            type: 'number'
                                        }
                                    },
                                    {
                                        key: 'AutoStart',
                                        type: 'checkbox',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Auto Start',
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            key: 'Actions',
                            type: 'section',
                            templateOptions: {
                                label: 'Actions',
                                fields: [
                                    //{
                                    //    key: 'ClearData',
                                    //    type: 'checkbox',
                                    //    className: 'col-md-12',
                                    //    templateOptions: {
                                    //        label:
                                    //            'Clear Data (you will lose all of your data and will reset automatically once successfully deleted data upon save)'
                                    //    }
                                    //},
                                    {
                                        key: 'ShowDone',
                                        type: 'checkbox',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Show Done (Prepared ALL) button for every tickets'
                                        }
                                    },
                                    {
                                        key: 'ShowCallNumber',
                                        type: 'checkbox',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Show Call Number'
                                        }
                                    },
                                    {
                                        key: 'AutoSyncSettings',
                                        type: 'checkbox',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Auto Sync'
                                        }
                                    },
                                    {
                                        key: 'ShowCollections',
                                        type: 'checkbox',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Show Collections'
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            key: 'PlaySoundPathSettings',
                            type: 'section',
                            templateOptions: {
                                label: 'Alert Sound',
                                fields: [
                                    {
                                        key: 'AlertSoundPath',
                                        type: 'uploadFileSound',
                                        className: 'col-md-8',
                                        templateOptions: {
                                            label: 'Sound Path',
                                            type: 'string'
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            key: 'TicketTagSettings',
                            type: 'section',
                            templateOptions: {
                                label: 'Ticket Tag',
                                fields: [
                                    {
                                        key: 'TicketTagName',
                                        type: 'input',
                                        className: 'col-md-6',
                                        templateOptions: {
                                            label: 'Display by Ticket Tag Name',
                                            type: 'string'
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            key: 'EnableBindState',
                            type: 'section',
                            templateOptions: {
                                label: 'Bind Order Status',
                                fields: [
                                    {
                                        key: 'EnableBindState',
                                        type: 'checkbox',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Enable Bind State',
                                            type: 'text',
                                            //onClick: function (model, options, _this, evt) {
                                            //    $(".bindorderstate").show();
                                            //    if (!model) {
                                            //        $(".bindorderstate").hide();
                                            //    }
                                            //}
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            key: 'BindOrderStatusSettings',
                            type: 'repeatSection',
                            hideExpression: function () {
                                return !vm.dinedeviceType.EnableBindState.EnableBindState;
                            },
                            templateOptions: {
                                className: 'bindorderstate',
                                btnText: 'Add',
                                btnRemoveText: 'Remove',
                                fields: [
                                    {
                                        type: 'checkbox',
                                        key: 'Enable',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Enable'
                                        }
                                    },
                                    {
                                        type: 'checkbox',
                                        key: 'IsCompleted',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Complete'
                                        }
                                    },
                                    {
                                        type: 'checkbox',
                                        key: 'IsTimer',
                                        className: 'col-md-4',
                                        templateOptions: {
                                            label: 'Timer'
                                        }
                                    },
                                    {
                                        type: 'checkbox',
                                        key: 'AutoStartTimer',
                                        className: 'col-md-4',
                                        templateOptions: {
                                            label: 'Auto Start Timer'
                                        }
                                    },
                                    {
                                        type: 'checkbox',
                                        key: 'ChangeStatusWhenFinishTimer',
                                        className: 'col-md-4',
                                        templateOptions: {
                                            label: 'Change Status When Finish Timer',
                                        }
                                    },
                                    {
                                        key: 'AlertType',
                                        type: 'dineSelect',
                                        className: 'col-md-6',
                                        templateOptions: {
                                            label: 'Alert',
                                            valueProp: 'id',
                                            labelProp: 'name',
                                            options: [
                                                {
                                                    name: 'after',
                                                    id: 'after'
                                                },
                                                {
                                                    name: 'before',
                                                    id: 'before'
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        key: 'AlertMinutes',
                                        type: 'input',
                                        className: 'col-md-6',
                                        templateOptions: {
                                            label: 'Minutes',
                                            type: 'number',
                                            onKeypress: function (model, options, _this, evt) {
                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                    evt.preventDefault();
                                                }
                                            }
                                        }
                                    },
                                    {
                                        type: 'checkbox',
                                        key: 'PlaySound',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Play Sound'
                                        }
                                    },
                                    {
                                        key: 'PlaySoundPath',
                                        type: 'uploadFileSound',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Sound Path',
                                            type: 'string'
                                        }
                                    },
                                    {
                                        key: 'BackgroundColor',
                                        type: 'colorPicker',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Color',
                                            type: 'string',
                                        }
                                    },
                                    {
                                        key: 'Status',
                                        type: 'input',
                                        className: 'col-md-6',
                                        templateOptions: {
                                            label: 'Status',
                                            type: 'string'
                                        }
                                    },
                                    {
                                        key: 'ChangedStatus',
                                        type: 'input',
                                        className: 'col-md-6',
                                        templateOptions: {
                                            label: 'Changed Status',
                                            type: 'string'
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            key: 'BindHeaderSetting',
                            hideExpression: function () {
                                return !vm.dinedeviceType.EnableBindState.EnableBindState;
                            },
                            type: 'section',
                            templateOptions: {
                                className: 'bindorderstate',
                                label: 'Bind Header',
                                fields: [
                                    {
                                        key: 'FontBindHeader',
                                        className: 'row',
                                        fieldGroup: [
                                            {
                                                key: 'FontSize',
                                                type: 'input',
                                                className: 'col-md-2',
                                                templateOptions: {
                                                    label: 'Font Size',
                                                    type: 'number'
                                                }
                                            },
                                            {
                                                key: 'Background',
                                                type: 'colorPicker',
                                                className: 'col-md-2',
                                                templateOptions: {
                                                    label: 'Background',
                                                    type: 'text'
                                                }
                                            },
                                            {
                                                key: 'Foreground',
                                                type: 'colorPicker',
                                                className: 'col-md-2',
                                                templateOptions: {
                                                    label: 'Foreground',
                                                    type: 'text'
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        key: 'Columns',
                                        fieldGroup: [
                                            {
                                                key: 'Quantity',
                                                className: 'row',
                                                fieldGroup: [
                                                    {
                                                        key: 'Id',
                                                        type: 'labelText',
                                                        templateOptions: {
                                                            isVisible: false,
                                                        }
                                                    },
                                                    {
                                                        key: 'HeaderColumns',
                                                        type: 'input',
                                                        className: 'col-md-2',
                                                        templateOptions: {
                                                            label: 'Header',
                                                            type: 'string'
                                                        }
                                                    },
                                                    {
                                                        key: 'ShowColumns',
                                                        type: 'checkbox',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Show',
                                                        }
                                                    },
                                                    {
                                                        key: 'WidthColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Width',
                                                            type: 'text',
                                                        }
                                                    },
                                                    {
                                                        key: 'OrderColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Order',
                                                            type: 'number',
                                                            onKeypress: function (model, options, _this, evt) {
                                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                    evt.preventDefault();
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                key: 'Item',
                                                className: 'row',
                                                fieldGroup: [
                                                    {
                                                        key: 'Id',
                                                        type: 'labelText',
                                                        templateOptions: {
                                                            isVisible: false,
                                                        }
                                                    },
                                                    {
                                                        key: 'HeaderColumns',
                                                        type: 'input',
                                                        className: 'col-md-2',
                                                        templateOptions: {
                                                            label: 'Header',
                                                            type: 'string'
                                                        }
                                                    },
                                                    {
                                                        key: 'ShowColumns',
                                                        type: 'checkbox',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Show',
                                                        }
                                                    },
                                                    {
                                                        key: 'WidthColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Width',
                                                            type: 'text',
                                                        }
                                                    },
                                                    {
                                                        key: 'OrderColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Order',
                                                            type: 'number',
                                                            onKeypress: function (model, options, _this, evt) {
                                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                    evt.preventDefault();
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                key: 'Portion',
                                                className: 'row',
                                                fieldGroup: [
                                                    {
                                                        key: 'Id',
                                                        type: 'labelText',
                                                        templateOptions: {
                                                            isVisible: false,
                                                        }
                                                    },
                                                    {
                                                        key: 'HeaderColumns',
                                                        type: 'input',
                                                        className: 'col-md-2',
                                                        templateOptions: {
                                                            label: 'Header',
                                                            type: 'string'
                                                        }
                                                    },
                                                    {
                                                        key: 'ShowColumns',
                                                        type: 'checkbox',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Show',
                                                        }
                                                    },
                                                    {
                                                        key: 'WidthColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Width',
                                                            type: 'text',
                                                        }
                                                    },
                                                    {
                                                        key: 'OrderColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Order',
                                                            type: 'number',
                                                            onKeypress: function (model, options, _this, evt) {
                                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                    evt.preventDefault();
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                key: 'Time',
                                                className: 'row',
                                                fieldGroup: [
                                                    {
                                                        key: 'Id',
                                                        type: 'labelText',
                                                        templateOptions: {
                                                            isVisible: false,
                                                        }
                                                    },
                                                    {
                                                        key: 'HeaderColumns',
                                                        type: 'input',
                                                        className: 'col-md-2',
                                                        templateOptions: {
                                                            label: 'Header',
                                                            type: 'string'
                                                        }
                                                    },
                                                    {
                                                        key: 'ShowColumns',
                                                        type: 'checkbox',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Show',
                                                        }
                                                    },
                                                    {
                                                        key: 'WidthColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Width',
                                                            type: 'text',
                                                        }
                                                    },
                                                    {
                                                        key: 'OrderColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Order',
                                                            type: 'number',
                                                            onKeypress: function (model, options, _this, evt) {
                                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                    evt.preventDefault();
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                key: 'Status',
                                                className: 'row',
                                                fieldGroup: [
                                                    {
                                                        key: 'Id',
                                                        type: 'labelText',
                                                        templateOptions: {
                                                            isVisible: false,
                                                        }
                                                    },
                                                    {
                                                        key: 'HeaderColumns',
                                                        type: 'input',
                                                        className: 'col-md-2',
                                                        templateOptions: {
                                                            label: 'Header',
                                                            type: 'string'
                                                        }
                                                    },
                                                    {
                                                        key: 'ShowColumns',
                                                        type: 'checkbox',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Show',
                                                        }
                                                    },
                                                    {
                                                        key: 'WidthColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Width',
                                                            type: 'text',
                                                        }
                                                    },
                                                    {
                                                        key: 'OrderColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Order',
                                                            type: 'number',
                                                            onKeypress: function (model, options, _this, evt) {
                                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                    evt.preventDefault();
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                key: 'CookingTime',
                                                className: 'row',
                                                fieldGroup: [
                                                    {
                                                        key: 'Id',
                                                        type: 'labelText',
                                                        templateOptions: {
                                                            isVisible: false,
                                                        }
                                                    },
                                                    {
                                                        key: 'HeaderColumns',
                                                        type: 'input',
                                                        className: 'col-md-2',
                                                        templateOptions: {
                                                            label: 'Header',
                                                            type: 'string'
                                                        }
                                                    },
                                                    {
                                                        key: 'ShowColumns',
                                                        type: 'checkbox',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Show',
                                                        }
                                                    },
                                                    {
                                                        key: 'WidthColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Width',
                                                            type: 'text',
                                                        }
                                                    },
                                                    {
                                                        key: 'OrderColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Order',
                                                            type: 'number',
                                                            onKeypress: function (model, options, _this, evt) {
                                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                    evt.preventDefault();
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                key: 'Timer',
                                                className: 'row',
                                                fieldGroup: [
                                                    {
                                                        key: 'Id',
                                                        type: 'labelText',
                                                        templateOptions: {
                                                            isVisible: false,
                                                        }
                                                    },
                                                    {
                                                        key: 'HeaderColumns',
                                                        type: 'input',
                                                        className: 'col-md-2',
                                                        templateOptions: {
                                                            label: 'Header',
                                                            type: 'string'
                                                        }
                                                    },
                                                    {
                                                        key: 'ShowColumns',
                                                        type: 'checkbox',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Show',
                                                        }
                                                    },
                                                    {
                                                        key: 'WidthColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Width',
                                                            type: 'text',
                                                        }
                                                    },
                                                    {
                                                        key: 'OrderColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Order',
                                                            type: 'number',
                                                            onKeypress: function (model, options, _this, evt) {
                                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                    evt.preventDefault();
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                key: 'TimeLeft',
                                                className: 'row',
                                                fieldGroup: [
                                                    {
                                                        key: 'Id',
                                                        type: 'labelText',
                                                        templateOptions: {
                                                            isVisible: false,
                                                        }
                                                    },
                                                    {
                                                        key: 'HeaderColumns',
                                                        type: 'input',
                                                        className: 'col-md-2',
                                                        templateOptions: {
                                                            label: 'Header',
                                                            type: 'string'
                                                        }
                                                    },
                                                    {
                                                        key: 'ShowColumns',
                                                        type: 'checkbox',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Show',
                                                        }
                                                    },
                                                    {
                                                        key: 'WidthColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Width',
                                                            type: 'text',
                                                        }
                                                    },
                                                    {
                                                        key: 'OrderColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Order',
                                                            type: 'number',
                                                            onKeypress: function (model, options, _this, evt) {
                                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                    evt.preventDefault();
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                key: 'Action',
                                                className: 'row',
                                                fieldGroup: [
                                                    {
                                                        key: 'Id',
                                                        type: 'labelText',
                                                        templateOptions: {
                                                            isVisible: false,
                                                        }
                                                    },
                                                    {
                                                        key: 'HeaderColumns',
                                                        type: 'input',
                                                        className: 'col-md-2',
                                                        templateOptions: {
                                                            label: 'Header',
                                                            type: 'string'
                                                        }
                                                    },
                                                    {
                                                        key: 'ShowColumns',
                                                        type: 'checkbox',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Show',
                                                        }
                                                    },
                                                    {
                                                        key: 'WidthColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Width',
                                                            type: 'text',
                                                        }
                                                    },
                                                    {
                                                        key: 'OrderColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Order',
                                                            type: 'number',
                                                            onKeypress: function (model, options, _this, evt) {
                                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                    evt.preventDefault();
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                key: 'Table',
                                                className: 'row',
                                                fieldGroup: [
                                                    {
                                                        key: 'Id',
                                                        type: 'labelText',
                                                        templateOptions: {
                                                            isVisible: false,
                                                        }
                                                    },
                                                    {
                                                        key: 'HeaderColumns',
                                                        type: 'input',
                                                        className: 'col-md-2',
                                                        templateOptions: {
                                                            label: 'Header',
                                                            type: 'string'
                                                        }
                                                    },
                                                    {
                                                        key: 'ShowColumns',
                                                        type: 'checkbox',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Show',
                                                        }
                                                    },
                                                    {
                                                        key: 'WidthColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Width',
                                                            type: 'text',
                                                        }
                                                    },
                                                    {
                                                        key: 'OrderColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Order',
                                                            type: 'number',
                                                            onKeypress: function (model, options, _this, evt) {
                                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                    evt.preventDefault();
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                key: 'Order',
                                                className: 'row',
                                                fieldGroup: [
                                                    {
                                                        key: 'Id',
                                                        type: 'labelText',
                                                        templateOptions: {
                                                            isVisible: false,
                                                        }
                                                    },
                                                    {
                                                        key: 'HeaderColumns',
                                                        type: 'input',
                                                        className: 'col-md-2',
                                                        templateOptions: {
                                                            label: 'Header',
                                                            type: 'string'
                                                        }
                                                    },
                                                    {
                                                        key: 'ShowColumns',
                                                        type: 'checkbox',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Show',
                                                        }
                                                    },
                                                    {
                                                        key: 'WidthColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Width',
                                                            type: 'text',
                                                        }
                                                    },
                                                    {
                                                        key: 'OrderColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Order',
                                                            type: 'number',
                                                            onKeypress: function (model, options, _this, evt) {
                                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                    evt.preventDefault();
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                key: 'OrderTime',
                                                className: 'row',
                                                fieldGroup: [
                                                    {
                                                        key: 'Id',
                                                        type: 'labelText',
                                                        templateOptions: {
                                                            isVisible: false,
                                                        }
                                                    },
                                                    {
                                                        key: 'HeaderColumns',
                                                        type: 'input',
                                                        className: 'col-md-2',
                                                        templateOptions: {
                                                            label: 'Header',
                                                            type: 'string'
                                                        }
                                                    },
                                                    {
                                                        key: 'ShowColumns',
                                                        type: 'checkbox',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Show',
                                                        }
                                                    },
                                                    {
                                                        key: 'WidthColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Width',
                                                            type: 'text',
                                                        }
                                                    },
                                                    {
                                                        key: 'OrderColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Order',
                                                            type: 'number',
                                                            onKeypress: function (model, options, _this, evt) {
                                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                    evt.preventDefault();
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                key: 'User',
                                                className: 'row',
                                                fieldGroup: [
                                                    {
                                                        key: 'Id',
                                                        type: 'labelText',
                                                        templateOptions: {
                                                            isVisible: false,
                                                        }
                                                    },
                                                    {
                                                        key: 'HeaderColumns',
                                                        type: 'input',
                                                        className: 'col-md-2',
                                                        templateOptions: {
                                                            label: 'Header',
                                                            type: 'string'
                                                        }
                                                    },
                                                    {
                                                        key: 'ShowColumns',
                                                        type: 'checkbox',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Show',
                                                        }
                                                    },
                                                    {
                                                        key: 'WidthColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Width',
                                                            type: 'text',
                                                        }
                                                    },
                                                    {
                                                        key: 'OrderColumns',
                                                        type: 'input',
                                                        className: 'col-md-1',
                                                        templateOptions: {
                                                            label: 'Order',
                                                            type: 'number',
                                                            onKeypress: function (model, options, _this, evt) {
                                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                                    evt.preventDefault();
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        }
                    ];
                    vm.dineQueueFields = [
                        {
                            key: 'Name',
                            type: 'input',
                            className: 'col-md-12',
                            templateOptions: {
                                label: 'Name',
                                type: 'string'
                            }
                        },
                        {
                            key: 'Columns',
                            type: 'repeatSection',
                            templateOptions: {
                                label: 'Columns',
                                btnText: 'Add',
                                btnRemoveText: 'Delete',
                                fields: [
                                    {
                                        key: 'Width',
                                        type: 'input',
                                        className: 'col-md-6',
                                        templateOptions: {
                                            label: 'Width',
                                            type: 'string'
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            key: 'Rows',
                            type: 'repeatSection',
                            templateOptions: {
                                label: 'Rows',
                                btnText: 'Add',
                                btnRemoveText: 'Delete',
                                fields: [
                                    {
                                        key: 'Height',
                                        type: 'input',
                                        className: 'col-md-6',
                                        templateOptions: {
                                            label: 'Height',
                                            type: 'string'
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            key: 'QueueWidgetsSettings',
                            type: 'section',
                            templateOptions: {
                                label: 'Queue Widgets Settings',
                                fields: [
                                    {
                                        key: 'Rows',
                                        type: 'input',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Number Of Rows',
                                            type: 'number',
                                            onKeypress: function (model, options, _this, evt) {
                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                    evt.preventDefault();
                                                }
                                            }
                                        }
                                    },
                                    {
                                        key: 'Columns',
                                        type: 'input',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Number Of Columns',
                                            type: 'number',
                                            onKeypress: function (model, options, _this, evt) {
                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                    evt.preventDefault();
                                                }
                                            }
                                        }
                                    },
                                    {
                                        key: 'Orientation',
                                        type: 'dineSelect',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Orientation',
                                            valueProp: 'id',
                                            labelProp: 'name',
                                            options: [
                                                {
                                                    name: 'Horizontal',
                                                    id: 'Horizontal'
                                                },
                                                {
                                                    name: 'Vertical',
                                                    id: 'Vertical'
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        key: 'Font',
                                        type: 'input',
                                        className: 'col-md-3',
                                        templateOptions: {
                                            label: 'Font',
                                            type: 'string'
                                        }
                                    },
                                    {
                                        key: 'PageSize',
                                        type: 'input',
                                        className: 'col-md-3',
                                        templateOptions: {
                                            label: 'Number of tickets per page',
                                            type: 'number',
                                            onKeypress: function (model, options, _this, evt) {
                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                    evt.preventDefault();
                                                }
                                            }
                                        }
                                    },
                                    {
                                        key: 'HeaderFontSize',
                                        type: 'input',
                                        className: 'col-md-3',
                                        templateOptions: {
                                            label: 'Header Font Size',
                                            type: 'number',
                                            onKeypress: function (model, options, _this, evt) {
                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                    evt.preventDefault();
                                                }
                                            }
                                        }
                                    },
                                    {
                                        key: 'QueueNumberFontSize',
                                        type: 'input',
                                        className: 'col-md-3',
                                        templateOptions: {
                                            label: 'Queue Number Font Size',
                                            type: 'number',
                                            onKeypress: function (model, options, _this, evt) {
                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                    evt.preventDefault();
                                                }
                                            }
                                        }
                                    },
                                    {
                                        key: 'Header3',
                                        type: 'input',
                                        className: 'col-md-3',
                                        templateOptions: {
                                            label: 'Department Fontsize',
                                            type: 'number',
                                            onKeypress: function (model, options, _this, evt) {
                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                    evt.preventDefault();
                                                }
                                            }
                                        }
                                    },
                                    {
                                        key: 'Width',
                                        type: 'input',
                                        className: 'col-md-3',
                                        templateOptions: {
                                            label: 'Ticket Width',
                                            type: 'number',
                                            onKeypress: function (model, options, _this, evt) {
                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                    evt.preventDefault();
                                                }
                                            }
                                        }
                                    },
                                    {
                                        key: 'Height',
                                        type: 'input',
                                        className: 'col-md-3',
                                        templateOptions: {
                                            label: 'Ticket Height',
                                            type: 'number',
                                            onKeypress: function (model, options, _this, evt) {
                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                    evt.preventDefault();
                                                }
                                            }
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            key: 'AllQueueWidgetsSettings',
                            type: 'repeatSection',
                            templateOptions: {
                                label: 'All Queues Widgets Settings',
                                fields: [
                                    {
                                        key: 'StatusName',
                                        type: 'input',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Status Name',
                                            type: 'string'
                                        }
                                    },
                                    {
                                        key: 'HeaderText',
                                        type: 'input',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Header Text',
                                            type: 'string'
                                        }
                                    },
                                    {
                                        key: 'HeaderTextColor',
                                        type: 'colorPicker',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Header Text Color',
                                            type: 'string'
                                        }
                                    },
                                    {
                                        key: 'QueueNumberColor',
                                        type: 'colorPicker',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Queue Number Color',
                                            type: 'string'
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            key: 'ThemeSettings',
                            type: 'section',
                            templateOptions: {
                                label: 'Theme',
                                fields: [
                                    {
                                        key: 'Theme',
                                        type: 'dineSelect',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Theme',
                                            valueProp: 'id',
                                            labelProp: 'name',
                                            options: [
                                                {
                                                    name: 'White',
                                                    id: 'white'
                                                },
                                                {
                                                    name: 'Green',
                                                    id: 'green'
                                                },
                                                {
                                                    name: 'Blue',
                                                    id: 'blue'
                                                },
                                                {
                                                    name: 'Yellow',
                                                    id: 'yellow'
                                                }
                                            ]
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            key: 'ActionSettings',
                            type: 'section',
                            templateOptions: {
                                label: 'Actions',
                                fields: [
                                    {
                                        key: 'ShowPrepareByOrderLevel',
                                        type: 'checkbox',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Show Prepare By Order Level',
                                            disabled: false
                                        }
                                    },
                                    {
                                        key: 'EnableVoice',
                                        type: 'checkbox',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Enable Voice',
                                            disabled: false
                                        }
                                    },
                                    {
                                        key: 'EnableCallForEachItemInTicket',
                                        type: 'checkbox',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Calling when each item in ticket finished',
                                            disabled: false
                                        }
                                    },
                                    {
                                        key: 'VoiceScript',
                                        type: 'input',
                                        className: 'col-md-12',
                                        templateOptions: {
                                            label: 'Voice Script',
                                            type: 'string'
                                        }
                                    }

                                ]
                            }
                        },
                        {
                            key: 'TicketTagSettings',
                            type: 'section',
                            templateOptions: {
                                label: 'Ticket Tag',
                                fields: [
                                    {
                                        key: 'TicketTagName',
                                        type: 'input',
                                        className: 'col-md-6',
                                        templateOptions: {
                                            label: 'Display by Ticket Tag Name',
                                            type: 'string'
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            key: 'SoundPathSettings',
                            type: 'section',
                            templateOptions: {
                                label: 'Alert Sound',
                                fields: [
                                    {
                                        key: 'AlertSoundPath',
                                        type: 'uploadFileSound',
                                        className: 'col-md-9',
                                        templateOptions: {
                                            label: 'Alert Sound Path',
                                            type: 'string'
                                        }
                                    },
                                    {
                                        key: 'AlertDuration',
                                        type: 'input',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Alert Duration',
                                            type: 'number',
                                            onKeypress: function (model, options, _this, evt) {
                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                    evt.preventDefault();
                                                }
                                            }
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            key: 'Widgets',
                            type: 'repeatSection',
                            templateOptions: {
                                label: 'Widgets',
                                btnText: 'Add Widget',
                                btnRemoveText: 'Remove',
                                fields: [
                                    {
                                        key: 'Name',
                                        type: 'input',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Name',
                                            type: 'string'
                                        }
                                    },
                                    {
                                        key: 'Type',
                                        type: 'dineSelect',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Type',
                                            valueProp: 'id',
                                            labelProp: 'name',
                                            options: [
                                                {
                                                    name: 'Image',
                                                    id: 'image'
                                                },
                                                {
                                                    name: 'PreparedQueue',
                                                    id: 'prepared'
                                                },
                                                {
                                                    name: 'PreparingQueue',
                                                    id: 'preparing'
                                                },
                                                {
                                                    name: 'Video',
                                                    id: 'video'
                                                },
                                                {
                                                    name: 'Slides',
                                                    id: 'slides'
                                                },
                                                {
                                                    name: 'Text',
                                                    id: 'text'
                                                },
                                                {
                                                    name: 'Department',
                                                    id: 'department'
                                                },
                                                {
                                                    name: 'Ticket',
                                                    id: 'ticket'
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        key: 'Row',
                                        type: 'input',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Row',
                                            type: 'number',
                                            onKeypress: function (model, options, _this, evt) {
                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                    evt.preventDefault();
                                                }
                                            }
                                        }
                                    },
                                    {
                                        key: 'Column',
                                        type: 'input',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Column',
                                            type: 'number',
                                            onKeypress: function (model, options, _this, evt) {
                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                    evt.preventDefault();
                                                }
                                            }
                                        }
                                    },
                                    {
                                        key: 'RowSpan',
                                        type: 'input',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Row Span',
                                            type: 'number',
                                            onKeypress: function (model, options, _this, evt) {
                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                    evt.preventDefault();
                                                }
                                            }
                                        }
                                    },
                                    {
                                        key: 'ColumnSpan',
                                        type: 'input',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Column Span',
                                            type: 'number',
                                            onKeypress: function (model, options, _this, evt) {
                                                if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                                                    evt.preventDefault();
                                                }
                                            }
                                        }
                                    },
                                    {
                                        key: 'Content',
                                        type: 'input',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Content',
                                            type: 'string'
                                        }
                                    },
                                    {
                                        key: 'LeftText',
                                        type: 'input',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Left Text',
                                            type: 'string'
                                        }
                                    },
                                    {
                                        key: 'CenterText',
                                        type: 'input',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Center Text',
                                            type: 'string'
                                        }
                                    },
                                    {
                                        key: 'RightText',
                                        type: 'input',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Right Text',
                                            type: 'string'
                                        }
                                    },
                                    {
                                        key: 'BackgroundColor',
                                        type: 'colorPicker',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Background Color',
                                            type: 'text'
                                        }
                                    },
                                    {
                                        key: 'Foreground',
                                        type: 'colorPicker',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Foreground',
                                            type: 'text'
                                        }
                                    },
                                    {
                                        key: 'FontSize',
                                        type: 'input',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Font Size',
                                            type: 'number'
                                        }
                                    },
                                    {
                                        key: 'AllWidgetTypes',
                                        type: 'input',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'All Widget Types',
                                            type: 'string'
                                        }
                                    },
                                    {
                                        key: 'IsVideo',
                                        type: 'checkbox',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Is video',
                                            disabled: true
                                        }
                                    },
                                    {
                                        key: 'IsImage',
                                        type: 'checkbox',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Is Image',
                                            disabled: true
                                        }
                                    },
                                    {
                                        key: 'IsSlides',
                                        type: 'checkbox',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Is Slides',
                                            disabled: true
                                        }
                                    },
                                    {
                                        key: 'IsText',
                                        type: 'checkbox',
                                        className: 'col-md-2',
                                        templateOptions: {
                                            label: 'Is Text',
                                            disabled: true
                                        }
                                    },
                                    {
                                        key: 'FilePaths',
                                        type: 'repeatSection',
                                        templateOptions: {
                                            label: 'File Paths',
                                            btnText: 'Add file path',
                                            btnRemoveText: 'Remove',
                                            fields: [
                                                {
                                                    key: 'FilePath',
                                                    type: 'uploadFile',
                                                    className: 'col-md-12',
                                                    templateOptions: {
                                                        type: 'string'
                                                    }
                                                },
                                            ]
                                        }
                                    }
                                ]
                            }
                        }
                    ];
                    vm.dineChefModel = {
                        TicketSettingData: {
                            UserPreferences: {
                                PageSize: null,
                                PageSizeTicket: null,
                                CompleteTicket: false,
                                NewTicketWillBeShow: false,
                                AdditionalOrder: false,
                                ShowPartialCompleted: false,
                                ShowOnlyInCompletedOrders: false,
                                UpdateQueue: false,
                                ShowQueueNumber: false
                            },
                            AllStates: {
                                Font: 'Cosolas',
                                Header1FontSize: 15,
                                Header2FontSize: 11,
                                TableNameFontSize: 12
                            },
                            NewState: {
                                Foreground: '#ffffff',
                                Background: '#4b0082'
                            },

                            TicketStates: [
                                {
                                    Level: 'Level 1',
                                    ElapsedMinutes: 1,
                                    ElapsedSeconds: 0,
                                    Background: '#ff0000',
                                    Foreground: '#ffffff'
                                },
                                {
                                    Level: 'Level 2',
                                    ElapsedMinutes: 2,
                                    ElapsedSeconds: 0,
                                    Background: '#b8860b',
                                    Foreground: '#ffffff'
                                },
                                {
                                    Level: 'Level 3',
                                    ElapsedMinutes: 3,
                                    ElapsedSeconds: 0,
                                    Background: '#8b0000',
                                    Foreground: '#ffffff'
                                },
                                {
                                    Level: 'Level 4',
                                    ElapsedMinutes: 4,
                                    ElapsedSeconds: 0,
                                    Background: '#ff4500',
                                    Foreground: '#ffffff'
                                }
                            ],
                            CompletedState: {
                                Foreground: '#ffffff',
                                Background: '#0000ff'
                            },
                            ComboColorSettings: {
                                Background: '##ffff00'
                            },
                            EnableBindState: {
                                EnableBindState: false
                            },
                            Items: {
                                Cooking: {
                                    Foreground: '#000000',
                                    Background: '#40e0d0'
                                },
                                Done: {
                                    Foreground: '#000000',
                                    Background: '#9acd32'
                                }
                            },
                            Button: {
                                Done: {
                                    Foreground: '#f5f5dc',
                                    Background: '#a9a9a9'
                                }
                            },
                            TicketHeight: {
                                Classic: {
                                    Height: 400,
                                    Width: 300
                                },
                                Compact: {
                                    Height: 400,
                                    Width: 300
                                },
                                Grid: {
                                    Height: 400,
                                    Width: 300
                                }
                            },
                            OrderItemName: {
                                ShowMenuItemName: false,
                                ShowAliasName: false
                            }
                        },
                        TicketStatues: {
                            UpdateStatus: false,
                            ReceiveOrderState: null,
                            SendOrderState: null,
                            StartTicketState: null,
                            FinishedTicketState: null
                        },
                        Others: {
                            WindowScale: 1,
                            AutoStart: true
                        },
                        BindHeaderSetting: {
                            FontBindHeader: {
                                FontSize: 16,
                                Foreground: '#ffffff',
                                Background: '#4b0082'
                            },
                            Columns: {
                                Quantity: {
                                    Id: 1,
                                    OrderColumns: 1,
                                    WidthColumns: '60',
                                    ShowColumns: true,
                                    HeaderColumns: 'Quantity'
                                },
                                Item: {
                                    Id: 2,
                                    OrderColumns: 2,
                                    WidthColumns: '*',
                                    ShowColumns: true,
                                    HeaderColumns: 'Item'
                                },
                                Portion: {
                                    Id: 3,
                                    OrderColumns: 3,
                                    WidthColumns: 100,
                                    ShowColumns: true,
                                    HeaderColumns: 'Portion'
                                },
                                Time: {
                                    Id: '4',
                                    OrderColumns: 4,
                                    WidthColumns: 60,
                                    ShowColumns: true,
                                    HeaderColumns: 'Time'
                                },
                                Status: {
                                    Id: 5,
                                    OrderColumns: 5,
                                    WidthColumns: '60',
                                    ShowColumns: true,
                                    HeaderColumns: 'Status'
                                },
                                CookingTime: {
                                    Id: 6,
                                    OrderColumns: 6,
                                    WidthColumns: '140',
                                    ShowColumns: true,
                                    HeaderColumns: 'Cooking Time (min)'
                                },
                                Timer: {
                                    Id: 7,
                                    OrderColumns: 7,
                                    WidthColumns: '60',
                                    ShowColumns: true,
                                    HeaderColumns: 'Timer'
                                },
                                TimeLeft: {
                                    Id: 8,
                                    OrderColumns: 8,
                                    WidthColumns: '70',
                                    ShowColumns: true,
                                    HeaderColumns: 'Time Left'
                                },
                                Action: {
                                    Id: '9',
                                    OrderColumns: 9,
                                    WidthColumns: '140',
                                    ShowColumns: true,
                                    HeaderColumns: 'Action'
                                },
                                Table: {
                                    Id: 10,
                                    OrderColumns: 10,
                                    WidthColumns: '60',
                                    ShowColumns: true,
                                    HeaderColumns: 'Table'
                                },
                                Order: {
                                    Id: 11,
                                    OrderColumns: 11,
                                    WidthColumns: '80',
                                    ShowColumns: true,
                                    HeaderColumns: 'Order #'
                                },
                                OrderTime: {
                                    Id: 12,
                                    OrderColumns: 12,
                                    WidthColumns: '80',
                                    ShowColumns: false,
                                    HeaderColumns: 'Order Time'
                                },
                                User: {
                                    Id: 13,
                                    OrderColumns: 13,
                                    WidthColumns: '80',
                                    ShowColumns: false,
                                    HeaderColumns: 'User'
                                },
                            }

                        },
                        DepartmentFiltersPosition: null,
                        Theme: null,
                        Culturel: null,
                        Actions: {
                            ShowDone: false,
                            ShowCallNumber: false,
                            ShowCollections: false,
                            AutoSyncSettings: false
                        },
                        //BindOrderStatusSettings: [{
                        //    ChangedStatus: null,
                        //    Status: null,
                        //    BackgroundColor: '#4b0082',
                        //    PlaySound: false,
                        //    PlaySoundPath: null,
                        //    IsTimer: false,
                        //    IsCompleted: false,
                        //    Enable: true,
                        //    AlertType: 'after',
                        //    AlertMinutes: 0,
                        //    ChangeStatusWhenFinishTimer: false,
                        //    AutoStartTimer: false,
                        //}]
                    };
                    vm.dineQueueModel = {
                        Name: '',
                        Columns: [
                            {
                                Width: ''
                            }
                        ],
                        Rows: [
                            {
                                Height: ''
                            }
                        ],
                        QueueWidgetsSettings: {
                            Rows: 3,
                            Columns: 3,
                            Orientation: 'Horizontal',
                            Font: 'Consolas',
                            PageSize: 3,
                            HeaderFontSize: 48,
                            QueueNumberFontSize: 48,
                            Header3: 48,
                            Width: 80,
                            Height: 80
                        },
                        TicketTagSettings: {
                            TicketTagName:''
                        },
                        AllQueueWidgetsSettings: [
                            {
                                StatusName: 'Preparing',
                                HeaderText: 'NOW PREPARING',
                                HeaderTextColor: '#FFC9C9',
                                QueueNumberColor: '#FFC9C9'
                            },
                            {
                                StatusName: 'Prepared',
                                HeaderText: 'PREPARED',
                                HeaderTextColor: '#FFF796',
                                QueueNumberColor: '#FFF796'
                            }
                        ],
                        ThemeSettings: {
                            Theme: null
                        },
                        ActionSettings: {
                            ShowPrepareByOrderLevel: false,
                            EnableVoice: false,
                            EnableCallForEachItemInTicket: false,
                            VoiceScript: 'Queue number {0} is prepared'

                        },
                        AlertSoundPath: {
                            SoundPath: '',
                            AlertDuration: 5
                        },
                        Widgets: [
                            {
                                Name: '',
                                Type: 'image',
                                Row: 0,
                                Column: 0,
                                RowSpan: 1,
                                ColumnSpan: 1,
                                Content: null,
                                LeftText: null,
                                CenterText: null,
                                RightText: null,
                                BackgroundColor: '#FF5D82',
                                Foreground: '#000000',
                                FontSize: 20,
                                AllWidgetTypes: null,
                                IsVideo: false,
                                IsImage: true,
                                IsSlides: false,
                                IsText: false,
                                FilePaths: [
                                    {
                                        FilePath: ''
                                    }
                                ]
                            }
                        ]
                    };
                }

                init();
            }
        ])
        .config([
            'formlyConfigProvider',
            function (formlyConfigProvider) {
                var uniqueRepeatSection = 1;
                var uniqueSection = 1;
                formlyConfigProvider.setType({
                    name: 'repeatSection',
                    templateUrl: 'repeatSection.html',
                    controller: [
                        '$scope',
                        function ($scope) {
                            $scope.formOptions = { formState: $scope.formState };
                            $scope.addNew = addNew;

                            $scope.copyFields = copyFields;

                            function init() {
                                if ($scope.options.key == 'Widgets') {
                                    ($scope.model[$scope.options.key] || []).forEach(function (item) {
                                        if (item.Type == 'image' || item.Type == 'video' || item.Type == 'slides') {
                                            item['FilePaths'] = item['FilePaths'] || [{ FilePath: '' }];
                                        } else {
                                            item['FilePaths'] = null;
                                        }
                                    })
                                }
                            }

                            function copyFields(fields) {
                                fields = angular.copy(fields);
                                addRandomIds(fields);
                                return fields;
                            }

                            function addNew() {
                                $scope.model[$scope.options.key] = $scope.model[$scope.options.key] || [];
                                var repeatsection = $scope.model[$scope.options.key];
                                var lastSection = repeatsection[repeatsection.length - 1];
                                var newsection = {};
                                if (lastSection) {
                                    newsection = angular.copy(lastSection);
                                    empty(newsection);
                                }
                                if ($scope.options.key == 'Widgets') {
                                    newsection = {
                                        Name: '',
                                        Type: 'image',
                                        Row: 0,
                                        Column: 0,
                                        RowSpan: 1,
                                        ColumnSpan: 1,
                                        Content: null,
                                        LeftText: null,
                                        CenterText: null,
                                        RightText: null,
                                        BackgroundColor: '#FF5D82',
                                        Foreground: '#000000',
                                        FontSize: 20,
                                        AllWidgetTypes: null,
                                        IsVideo: false,
                                        IsImage: true,
                                        IsSlides: false,
                                        IsText: false,
                                        FilePaths: [
                                            {
                                                FilePath: ''
                                            }
                                        ]
                                    }
                                }

                                if ($scope.options.key == 'BindOrderStatusSettings'/* && $scope.model.EnableBindState.EnableBindState == true*/) {
                                    newsection = {
                                        ChangedStatus: null,
                                        Status: null,
                                        BackgroundColor: '#4b0082',
                                        PlaySound: false,
                                        PlaySoundPath: null,
                                        IsTimer: false,
                                        IsCompleted: false,
                                        Enable: true,
                                        AlertType: 'after',
                                        AlertMinutes: 0,
                                        ChangeStatusWhenFinishTimer: false,
                                        AutoStartTimer: false,
                                    }

                                } else if (lastSection) {
                                    newsection = angular.copy(lastSection);
                                    empty(newsection);
                                }

                                $scope.model[$scope.options.key].push(newsection);
                            }

                            function empty(object) {
                                Object.keys(object).forEach(function (k) {
                                    if (object[k] && typeof object[k] === 'object') {
                                        return empty(object[k]);
                                    }
                                    object[k] = '';
                                });
                            }
                            function addRandomIds(fields) {
                                uniqueRepeatSection++;
                                angular.forEach(fields, function (field, index) {
                                    if (field.fieldGroup) {
                                        addRandomIds(field.fieldGroup);
                                        return; // fieldGroups don't need an ID
                                    }

                                    if (field.templateOptions && field.templateOptions.fields) {
                                        addRandomIds(field.templateOptions.fields);
                                    }

                                    field.id = field.id || field.key + '_' + index + '_' + uniqueRepeatSection + getRandomInt(0, 9999);
                                });
                            }

                            function getRandomInt(min, max) {
                                return Math.floor(Math.random() * (max - min)) + min;
                            }

                            init();
                        }
                    ]
                });
                formlyConfigProvider.setType({
                    name: 'section',
                    templateUrl: 'section.html',
                    controller: [
                        '$scope',
                        function ($scope) {
                            $scope.formOptions = { formState: $scope.formState };
                            $scope.copyFields = copyFields;

                            function copyFields(fields) {
                                fields = angular.copy(fields);
                                addRandomIds(fields);
                                return fields;
                            }

                            function addRandomIds(fields) {
                                uniqueSection++;
                                angular.forEach(fields, function (field, index) {
                                    if (field.fieldGroup) {
                                        addRandomIds(field.fieldGroup);
                                        return;
                                    }

                                    if (field.templateOptions && field.templateOptions.fields) {
                                        addRandomIds(field.templateOptions.fields);
                                    }

                                    field.id = field.id || field.key + '_' + index + '_' + uniqueSection + getRandomInt(0, 9999);
                                });
                            }

                            function getRandomInt(min, max) {
                                return Math.floor(Math.random() * (max - min)) + min;
                            }
                        }
                    ]
                });
                formlyConfigProvider.setType({
                    name: 'dineSelect',
                    templateUrl: 'dineSelect.html',
                    controller: [
                        '$scope',
                        function ($scope) {
                            $scope.formOptions = { formState: $scope.formState };
                            $scope.onChangeDineSelect = onChangeDineSelect;

                            function onChangeDineSelect() {
                                const widgetType = $scope.model[$scope.options.key];
                                const key = $scope.options.key;

                                /* WidgetType */
                                if (key == 'Type') {
                                    $scope.model['IsImage'] = widgetType == 'image' ? true : false;
                                    $scope.model['IsSlides'] = widgetType == 'slides' ? true : false;
                                    $scope.model['IsText'] = widgetType == 'text' ? true : false;
                                    $scope.model['IsVideo'] = widgetType == 'video' ? true : false;

                                    if (widgetType == 'image' || widgetType == 'video' || widgetType == 'slides') {
                                        $scope.model['FilePaths'] = $scope.model['FilePaths'] || [{ FilePath: '' }];
                                    } else {
                                        $scope.model['FilePaths'] = null;
                                    }
                                }
                            }
                        }
                    ]
                });
                formlyConfigProvider.setType({
                    name: 'colorPicker',
                    templateUrl: 'colorPicker.html'
                });
                formlyConfigProvider.setType({
                    name: 'sampleText',
                    templateUrl: 'sampleText.html'
                });
                formlyConfigProvider.setType({
                    name: 'labelText',
                    templateUrl: 'labelText.html'
                });
                formlyConfigProvider.setType({
                    name: 'uploadFile',
                    templateUrl: 'uploadFile.html',
                    controller: [
                        '$scope',
                        'FileUploader',
                        function ($scope, fileUploader) {
                            $scope.formOptions = { formState: $scope.formState };
                            $scope.uploader = new fileUploader({
                                url: abp.appPath + 'FileUpload/UploadFile',
                                queueLimit: 1
                            });

                            function init() {
                                $scope.model.Type = 'image';
                            }

                            $scope.uploader.onAfterAddingFile = function (fileItem) {
                                $scope.uploader.uploadAll();
                            };

                            $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                                if (response.error != null) {
                                    abp.message.warn(response.error.message);
                                    $uibModalInstance.close();
                                    return;
                                }
                                if (response.result) {
                                    $scope.model.FilePath = response.result.fileSystemName;
                                }
                                abp.notify.info(app.localize('Finished'));
                            };

                            init();
                        }
                    ]
                });
                formlyConfigProvider.setType({
                    name: 'uploadFileSound',
                    templateUrl: 'uploadFileSound.html',
                    controller: [
                        '$scope',
                        'FileUploader',
                        function ($scope, fileUploader) {
                            $scope.formOptions = { formState: $scope.formState };
                            $scope.uploader = new fileUploader({
                                url: abp.appPath + 'FileUpload/UploadFile',
                                queueLimit: 1
                            });
                            $scope.uploader.onAfterAddingFile = function (fileItem) {
                                $scope.uploader.uploadAll();
                            };

                            $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                                if (response.error != null) {
                                    abp.message.warn(response.error.message);
                                    $uibModalInstance.close();
                                    return;
                                }
                                if (response.result) {
                                    $scope.model.FilePath = response.result.fileSystemName;
                                }
                                abp.notify.info(app.localize('Finished'));
                            };

                        }
                    ]
                });
            }
        ]);
})();
