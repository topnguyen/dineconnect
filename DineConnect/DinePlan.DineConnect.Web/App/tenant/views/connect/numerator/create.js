﻿(function () {
    appModule.controller('tenant.views.connect.master.numerator.create', [
        '$scope', '$state', '$uibModal', '$stateParams', 'abp.services.app.numerator', "abp.services.app.connectLookup",
        function ($scope, $state, $modal, $stateParams, numeratorService, connectService) {
            var vm = this;
            vm.resetNumeratorTypes = [];
            vm.numeratorId = $stateParams.id;

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };

            function init() {
                numeratorService.getNumeratorForEdit({
                    id: vm.numeratorId
                }).success(function (result) {

                    console.log(result);

                    vm.numerator = result.numerator;
                    if (vm.numerator.id == null)
                    {
                        vm.numerator.resetNumeratorType = null;
                    }
                    numeratorService.getResetNumeratorTypes()
                        .success(function (result) {
                            vm.resetNumeratorTypes = result.items;
                        });
                });
            }

            init();

            vm.cancel = function () {
                $state.go("tenant.numerator");
            };

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };

            vm.save = function () {
                if (vm.isUndefinedOrNull(vm.numerator.name)) {
                    abp.notify.error(app.localize('NameIsMandatory'));
                    return;
                }
                vm.saving = true;
                vm.saveToDB();
            };

            vm.saveToDB = function () {
                numeratorService.createOrUpdateNumerator({
                    numerator: vm.numerator,
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };
        }
    ]);
})();