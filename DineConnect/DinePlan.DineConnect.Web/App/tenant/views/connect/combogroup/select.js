﻿(function () {
    appModule.controller("tenant.views.connect.combogroup.select.combogroup",
        ["$scope", "$uibModalInstance", "uiGridConstants", "abp.services.app.comboGroup", "combogroup",
            function ($scope, $modalInstance, uiGridConstants, comboGroupService, combogroup) {
                //console.log(location);
                /* eslint-disable */
                var vm = this;
                vm.saving = false;
                vm.autoSelection = false;
                vm.selectedComboGroups = combogroup.combogroups;

                vm.isUndefinedOrNull = function (val) {
                    return angular.isUndefined(val) || val == null;
                };

                var requestParams = {
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null,
                    userId: abp.session.userId
                };

               
                vm.comboGroupOptions = {
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    enableRowSelection: true,
                    rowHeight: 30,
                    appScopeProvider: vm,
                    rowTemplate:
                        "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                    columnDefs: [
                        
                        {
                            name: app.localize("Id"),
                            field: "id",
                            maxWidth: 50
                        },
                        {
                            name: app.localize("Name"),
                            field: "name"
                        },
                        {
                            name: app.localize("SortOrder"),
                            field: "sortOrder",
                            maxWidth: 70
                        },
                        {
                            name: app.localize("Minimum"),
                            field: "minimum",
                            maxWidth: 70
                        },
                        {
                            name: app.localize("Maximum"),
                            field: "maximum",
                            maxWidth: 70
                        },
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        vm.combogroupGridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope,
                            function (grid, sortColumns) {
                                if (!sortColumns.length || !sortColumns[0].field) {
                                    requestParams.sorting = null;
                                } else {
                                    requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                                }
                                vm.getComboGroups();
                            });
                        gridApi.pagination.on.paginationChanged($scope,
                            function (pageNumber, pageSize) {
                                requestParams.skipCount = (pageNumber - 1) * pageSize;
                                requestParams.maxResultCount = pageSize;
                                vm.getComboGroups();
                            });
                        gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                            if (row.isSelected) {
                                vm.pushComboGroup(row.entity);
                            }
                            else {
                                for (var i = 0; i < vm.selectedComboGroups.length; i++) {
                                    var obj = vm.selectedComboGroups[i];

                                    if (obj.id == row.entity.id) {
                                        vm.selectedComboGroups.splice(i, 1);
                                    }
                                }

                            }
                        });
                    },
                    data: []
                };

                vm.pushComboGroup = function (myObj) {
                    if (myObj != null) {
                        var myIndex = -1;
                        if (vm.selectedComboGroups != null) {
                            vm.selectedComboGroups.some(function (value, key) {
                                if (value.id == myObj.id) {
                                    myIndex = value;
                                    return true;
                                }
                            });
                        }

                        if (myIndex == -1) {
                            vm.selectedComboGroups.push(myObj);
                        } else {
                            if (vm.autoSelection == false)
                                abp.notify.error(myObj.name + '  ' + app.localize('Available'));
                        }
                    }
                };

                
                vm.getComboGroups = function () {
                    vm.loading = true;
                    comboGroupService.getAllComboGroups({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterComboGroupText
                    }).success(function (result) {
                        vm.comboGroupOptions.totalItems = result.totalCount;
                        vm.comboGroupOptions.data = result.items;
                    }).finally(function () {
                        vm.loading = false;
                    });
                };

                vm.setComboGroupSelection = function () {
                    if (vm.selectedComboGroups.length == 0)
                        return;
                    vm.combogroupGridApi.grid.modifyRows(vm.comboGroupOptions.data);
                    vm.combogroupGridApi.selection.clearSelectedRows();
                    vm.autoSelection = true;
                    angular.forEach(vm.comboGroupOptions.data, function (value, key) {
                        vm.selectedComboGroups.some(function (selectedData, selectedKey) {
                            if (value.id == selectedData.id) {
                                vm.combogroupGridApi.selection.selectRow(vm.comboGroupOptions.data[key]);
                                return true;
                            }
                        });
                    });
                    vm.autoSelection = false;
                }

              
                vm.save = function () {
                    combogroup.combogroups = vm.selectedComboGroups;
                    $modalInstance.close(combogroup);
                };

                vm.cancel = function () {
                    $modalInstance.dismiss();
                };

                vm.clear = function () {
                    vm.selectedComboGroups = [];
                    vm.filterComboGroupText = null;
                    if (!vm.isUndefinedOrNull(vm.combogroupGridApi))
                        vm.combogroupGridApi.selection.clearSelectedRows();

                };

                

                vm.getComboGroups();
            }
        ]);
})();