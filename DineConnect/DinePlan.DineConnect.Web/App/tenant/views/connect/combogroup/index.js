﻿(function () {
    appModule.controller("tenant.view.connect.menu.combogroup.index", [
        "$scope", "$state", "$uibModal", "uiGridConstants", "abp.services.app.comboGroup", '$stateParams',
        function ($scope, $state, $modal, uiGridConstants, comboGroupService, $stateParams) {
            var vm = this;
            vm.isDeleted = false;
            
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.skipCount = $stateParams.page;

            if (vm.skipCount === "") {
                vm.skipCount = 0;
            } else {
                requestParams.skipCount = vm.skipCount;
            }

            vm.currentUserId = abp.session.userId;
            vm.categoryInput = "";
            vm.menuTypeInput = 0;

            vm.permissions = {
                create: abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ComboGroup.Create"),
                edit: abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ComboGroup.Edit"),
                'delete': abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ComboGroup.Delete")
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Actions"),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editDetailItem(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteComboGroup(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize("Name"),
                        field: "name"
                    }, {
                        name: app.localize("SortOrder"),
                        field: "sortOrder"
                    }, {
                        name: app.localize("Minimum"),
                        field: "minimum",
                        cellClass: "ui-ralign"
                    }, 
                    {
                        name: app.localize("Maximum"),
                        field: "maximum",
                        cellClass: "ui-ralign"
                    }
                    , 
                    {
                        name: app.localize('ComboItems'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button ng-click="grid.appScope.comboItems(row.entity)"  class="btn btn-default btn-xs" title="' + app.localize('Generate') + '"><i class="fa fa-dashcube"></i></button>' +
                                '</div>'
                    },
                    {
                        name: app.localize('MenuItems'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.menuItems(row.entity)"  class="btn btn-default btn-xs" title="' + app.localize('Generate') + '"><i class="fa fa-dashcube"></i></button>' +
                            '</div>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };
            
            vm.getAll = function () {
                vm.loading = true;
                comboGroupService.getAllComboGroups({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    comboGroupSelectionIds:vm.selectedComboGroupIds
                }).success(function (result) {
                    console.log("resultcombo", result);
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editDetailItem = function (myObj) {
                openDetailComboGroup(myObj.id);
            };

            vm.createDetailItem = function (myObj) {
                openDetailComboGroup(null);
            };

            vm.deleteComboGroup = function (myObject) {
                abp.message.confirm(
                    app.localize("DeleteComboGroupWarning", myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            comboGroupService.delete({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize("SuccessfullyDeleted"));
                            });
                        }
                    }
                );
            };
            vm.comboItems = function (myObject)
            {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/combogroup/showComboItems.cshtml",
                    controller: "tenant.views.connect.combogroup.showComboItems.combogroup as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        comboitems: function () {
                            return myObject.comboItems;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                });
            }

            vm.menuItems = function (myObject) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/combogroup/showMenuItems.cshtml",
                    controller: "tenant.views.connect.combogroup.showMenuItems.combogroup as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        menuitems: function () {
                            return myObject.menuItems;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                });
            }

            function openDetailComboGroup(objId) {
                $state.go("tenant.detailcombogroup", {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                vm.loading = true;
                comboGroupService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
                vm.loading = false;
            };

            vm.getAll();

            
            vm.openComboGroup = function () {
                vm.comboGroup = {
                    combogroups: [],
                    multiple: true
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/combogroup/select.cshtml",
                    controller: "tenant.views.connect.combogroup.select.combogroup as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        combogroup: function () {
                            return vm.comboGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.comboGroup = result;
                    if (result.combogroups.length > 0)
                    {
                        vm.selectedComboGroupIds = [];
                        angular.forEach(result.combogroups, function (value, key) {
                            vm.selectedComboGroupIds.push(value.id);
                        });
                        
                    }
                });
            };

            vm.clear = function () {
                vm.filterText = null;
                vm.selectedComboGroupIds = [];
                vm.comboGroup = {
                    combogroups: [],
                    multiple: true
                };
                vm.getAll();
            };
        }
    ]);
})();