﻿(function () {
    appModule.controller("tenant.views.connect.combogroup.showMenuItems.combogroup",
        ["$scope", "$uibModalInstance", "menuitems",
            function ($scope, $modalInstance, menuitems) {
                //console.log(location);
                /* eslint-disable */
                var vm = this;
                vm.menuItems = menuitems;

                vm.close = function () {
                    $modalInstance.close();
                };

            }
        ]);
})();