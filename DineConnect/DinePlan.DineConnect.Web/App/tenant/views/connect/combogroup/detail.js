﻿(function () {
    appModule.controller("tenant.views.connect.menu.combogroup.detail", [
        "$scope", "$uibModal", "$state", "$stateParams", "abp.services.app.comboGroup",
        function ($scope, $modal, $state, $stateParams, comboGroupService) {
            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });
            var vm = this;

            vm.comboGroupId = $stateParams.id;
            vm.group = null;
            vm.selectedComboItemIndex = null;
            vm.saving = false;

            function init() {
                comboGroupService.getComboGroupForEdit({
                    id: vm.comboGroupId
                }).success(function (result) {
                    vm.group = result;
                });
            }

            vm.addComboItem = function () {
                addorEditItem({ 'name': "", 'menuItemId': 0, 'buttonColor': '#E6E784', 'autoSelect': false, 'price': 0, 'sortOrder': vm.group.comboItems.length, 'count': 1, 'addSeperately': false }, false);
            };

            vm.editComboItem = function (productIndex) {
                vm.selectedComboItemIndex = productIndex;
                var comboItem = Object.assign({}, vm.group.comboItems[productIndex]);
                addorEditItem(comboItem, true);
                //addorEditItem(vm.group.comboItems[productIndex], true);
            };
            vm.deleteComboItem = function (productIndex) {
                vm.group.comboItems.splice(productIndex, 1);
            };

            function addorEditItem(object, replace) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/menuitem/citem.cshtml",
                    controller: "tenant.views.connect.menu.combo.createitem as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        group: function () {
                            return object;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (replace)
                        vm.group.comboItems[vm.selectedComboItemIndex] = object;
                    else
                        vm.group.comboItems.push(object);
                });
            }

            vm.openLanguageDescriptionModal = function (data) {
                if (data == null) {
                    vm.languageDescriptionType = 16; //OrderTagGroup
                    vm.languageDescription = {
                        id: vm.group.id,
                        name: vm.group.name,
                        languageDescriptionType: vm.languageDescriptionType
                    };
                }
                else {
                    vm.languageDescriptionType = 6; //OrderTag
                    vm.languageDescription = {
                        id: data.id,
                        name: data.name,
                        languageDescriptionType: vm.languageDescriptionType
                    };
                }

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/languageDescription/languageDescriptionModal.cshtml",
                    controller: "tenant.views.connect.languageDescription.languageDescriptionModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        languagedescription: function () {
                            return vm.languageDescription;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });
            }

            vm.openfileUploaderModal = function (product) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/combogroup/fileUploadModal.cshtml',
                    controller: 'tenant.views.connect.combogroup.fileUploadModal as vm',
                    backdrop: 'static',
                    resolve:
                    {
                        comboGroupId: function () {
                            return product.id;
                        }
                    }
                });
                modalInstance.result.then(function (response) {
                    if (response !== null) {
                        vm.replyFiles = response;
                        product.files = angular.toJson(vm.replyFiles);
                        vm.save(1);
                    }
                });
            }

            vm.cancel = function () {
                $state.go("tenant.combogroup");
            };
            vm.save = function (argId) {
                vm.saving = true;
                comboGroupService.createOrUpdateComboGroup(vm.group)
                    .success(function () {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        if (argId==0) {
                            $state.go('tenant.combogroup');
                        }
                    }).finally(function () {
                        vm.saving = false;
                    });
            };

            init();
        }
    ]);
})();