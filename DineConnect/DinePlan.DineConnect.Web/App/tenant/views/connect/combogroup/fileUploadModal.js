﻿(function () {
    appModule.controller('tenant.views.connect.combogroup.fileUploadModal', [
        '$scope', '$uibModalInstance', 'FileUploader', 'comboGroupId', "abp.services.app.comboGroup", 
        function ($scope, $uibModalInstance, fileUploader, comboGroupId, comboGroupService) {
            var vm = this;
            vm.comboGroupId = comboGroupId;
            vm.isUpload = false;
            vm.replyFiles = [];

            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadFile'
            });

            vm.uploader.filters.push({
                name: 'imageFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    return true;
                }
            });
            vm.uploader.onAfterAddingFile = function (item) {
                console.log(item.file.size);

                if (item.file.size > 6000000) {
                    abp.notify.error(app.localize('Size is Big'));
                    vm.uploader.clearQueue();
                } else {
                    item.upload();
                }
            };
            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response !== null) {
                    if (response.result !== null) {
                        if (response.result.fileName !== null) {
                            vm.replyFiles.push(response.result);
                            vm.refreshImage = true;
                        }
                    }
                    $("#combogroup").val(null);
                }
            };

            vm.downloader = function (file) {
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + file.fileName + '&url=' + file.fileSystemName;
            };

            vm.removeFile = function (findex) {
                vm.replyFiles.splice(findex, 1);
                vm.refreshImage = true;
            };

            vm.uploadFile = function (isuload) {
                vm.isUpload = isuload;
                var file = vm.uploader.queue[vm.uploader.queue.length - 1];
                file.upload();
            };

            vm.save = function () {
                $uibModalInstance.close(vm.replyFiles);
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            function init() {
                comboGroupService.getComboGroupItemForEdit({
                    id: vm.comboGroupId
                }).success(function (result) {
                    vm.group = result;
                    vm.replyFiles = vm.group.files ? angular.fromJson(vm.group.files) : [];
                });
            }

            init();
        }

    ]);
})();