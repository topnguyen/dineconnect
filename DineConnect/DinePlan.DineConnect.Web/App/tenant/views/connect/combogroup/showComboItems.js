﻿(function () {
    appModule.controller("tenant.views.connect.combogroup.showComboItems.combogroup",
        ["$scope", "$uibModalInstance", "comboitems",
            function ($scope, $modalInstance, comboitems) {
                //console.log(location);
                /* eslint-disable */
                var vm = this;
                vm.comboItems = comboitems;

                vm.close = function () {
                    $modalInstance.close();
                };

            }
        ]);
})();