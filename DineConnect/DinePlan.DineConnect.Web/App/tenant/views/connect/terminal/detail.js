﻿(function () {
    appModule.controller('tenant.views.connect.terminal.detail', [
        '$scope', "$uibModal", '$state', '$stateParams', 'abp.services.app.terminal', "abp.services.app.connectLookup", 'FileUploader', "abp.services.app.menuItem",
        function ($scope, $modal, $state, $stateParams, terminalService, connectService, fileUploader, menuService) {
            var vm = this;
            vm.terminalId = $stateParams.id;
            vm.templateTypes = [];
            vm.ticketTypes = [];
            vm.lstDevice = [];
            vm.isUndefinedOrNullOrWhiteSpace = function (val) {
                return angular.isUndefined(val) || val == null || val == "";
            };

            vm.save = function () {
                if (vm.isUndefinedOrNullOrWhiteSpace(vm.terminal.terminalCode)) {
                    abp.notify.error(app.localize('Code is mandatory'));
                    return;
                }
                if (vm.isUndefinedOrNullOrWhiteSpace(vm.terminal.name)) {
                    abp.notify.error(app.localize('Name is mandatory'));
                    return;
                }

                if (vm.terminal.closeSetting && vm.terminal.autoDayClose) {
                    if (vm.terminal.closeSetting.inputTime)
                        vm.terminal.closeSetting.closeTime = moment(vm.terminal.closeSetting.inputTime).format('HH:mm');

                    if (_.uniq(vm.terminal.closeSetting.notifications, 'time').length != vm.terminal.closeSetting.notifications.length) {
                        abp.notify.error(app.localize('Duplicate Notification time'));
                        return;
                    }
                }
                vm.saving = true;
                var messagingServerPort = vm.terminalServiceSetting.MessagingServerPort;
                if (vm.isUndefinedOrNullOrWhiteSpace(messagingServerPort)) {
                    vm.terminalServiceSetting.MessagingServerPort = 0;
                }

                var windowScale = vm.terminaDisplaylSetting.WindowScale;
                if (vm.isUndefinedOrNullOrWhiteSpace(windowScale)) {
                    vm.terminaDisplaylSetting.WindowScale = 0;
                }

                var refreshEntityInSeconds = vm.terminaDisplaylSetting.RefreshEntityInSeconds;
                if (vm.isUndefinedOrNullOrWhiteSpace(refreshEntityInSeconds)) {
                    vm.terminaDisplaylSetting.RefreshEntityInSeconds = 0;
                }

                vm.terminalSetting = Object.assign({}, vm.terminalGeneralSetting, vm.terminaDisplaylSetting, vm.terminalServiceSetting, vm.terminalDineFlySetting, vm.terminalDineMenuSetting);
                vm.terminalSetting.AdditionalDevices = angular.toJson(vm.lstDevice);
                vm.terminalSetting.ResolutionWidth = + vm.terminaDisplaylSetting.Resolution.split('_')[0];
                vm.terminalSetting.ResolutionHeight = + vm.terminaDisplaylSetting.Resolution.split('_')[1];

                vm.terminal.settings = angular.toJson(vm.terminalSetting);

                terminalService.createOrUpdateTerminal({
                    terminal: vm.terminal,
                    locationGroup: vm.locationGroup
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $state.go('tenant.terminal');
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.terminal');
            };

            function init() {
                vm.saving = true;
                connectService.getTicketTypes({}).success(function (result) {
                    vm.ticketTypes = result.items;
                    vm.ticketTypes.unshift({ value: "0", displayText: app.localize("NotAssigned") });
                });

                terminalService.getTerminalForEdit({
                    id: vm.terminalId
                }).success(function (result) {
                    vm.terminal = result.terminal;

                    if (vm.terminal.closeSetting && vm.terminal.closeSetting.closeTime) {
                        var hm = vm.terminal.closeSetting.closeTime.split(':');
                        vm.terminal.closeSetting.inputTime = new Date(1970, 01, 01, hm[0], hm[1]);
                    }

                    vm.locationGroup = result.locationGroup;

                    if (vm.terminal.settings && angular.fromJson(vm.terminal.settings).AdditionalDevices) {
                        vm.lstDevice = angular.fromJson(angular.fromJson(vm.terminal.settings).AdditionalDevices);
                    }

                    vm.terminalSettingId = 0;
                    vm.settingObject = angular.fromJson(vm.terminal.settings);
                    terminalService.getTerminalSetting({ id: vm.terminalSettingId }).success(function (result) {
                        vm.terminalSettingFields = result;
                    });

                    terminalService.getTerminalSetting({ id: 1 }).success(function (result) {
                        vm.terminalGeneralFields = result;
                        vm.terminalGeneralSetting = vm.createObjectFromField(vm.terminalGeneralFields, vm.settingObject);
                    });
                    terminalService.getTerminalSetting({ id: 2 }).success(function (result) {
                        vm.terminalDisplayFields = result;
                        vm.terminaDisplaylSetting = vm.createObjectFromField(vm.terminalDisplayFields, vm.settingObject);
                    });
                    terminalService.getTerminalSetting({ id: 3 }).success(function (result) {
                        vm.terminalServiceFields = result;
                        vm.terminalServiceSetting = vm.createObjectFromField(vm.terminalServiceFields, vm.settingObject);
                    });
                    terminalService.getTerminalSetting({ id: 4 }).success(function (result) {
                        vm.terminalDineFlyFields = result;
                        vm.terminalDineFlySetting = vm.createObjectFromField(vm.terminalDineFlyFields, vm.settingObject);
                    });
                    terminalService.getTerminalSetting({ id: 5 }).success(function (result) {
                        vm.terminalDineMenuFields = result;
                        vm.terminalDineMenuSetting = vm.createObjectFromField(vm.terminalDineMenuFields, vm.settingObject);
                    });
                    vm.saving = false;
                });
                terminalService.getNotifyTypes({}).success(function (result) {
                    vm.notifyTypes = result.items;
                });
            }

            init();

            vm.createObjectFromField = function (fields, settings) {
                const obj = {};
                var keyFields = fields.map(a => a.key);

                var keySettings = settings != null ? Object.keys(settings) : null;
                for (const key of keyFields) {
                    if (keySettings != null && keySettings.includes(key)) {
                        obj[key] = settings[key];
                    }
                }
                return obj;
            }


            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };

            vm.addAutoClose = function () {
                vm.terminal.closeSetting.notifications.push({ time: '5' });
            };
            vm.deleteAutoClose = function (index) {
                vm.terminal.closeSetting.notifications.splice(index, 1);
            };

            $scope.itemSelected = function (evt, cityName) {
                var i, x, tablinks;
                x = document.getElementsByClassName("option_terminal");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                tablinks = document.getElementsByClassName("tablink");
                for (i = 0; i < x.length; i++) {
                    tablinks[i].className = tablinks[i].className.replace("text-danger", "");
                }
                document.getElementById(cityName).style.display = "block";
                evt.currentTarget.className += " text-danger";
            };
            vm.addOrEditDevices = function (data) {
                vm.deviceNameSelected = [];
                vm.lstDevice.forEach(el => {
                    vm.deviceNameSelected.push(el.DeviceName);
                });
                console.log('deviceNameSelected', vm.deviceNameSelected);

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/terminal/createOrEditDevicesModal.cshtml",
                    controller: "tenant.views.connect.terminal.createOrEditDevicesModal as vm",
                    backdrop: "static",
                    resolve: {
                        data: function () {
                            return data;
                        },
                        deviceNameSelected: function () {
                            return vm.deviceNameSelected;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    var index = vm.lstDevice.findIndex(x => x.DeviceName == result.DeviceName);
                    if (index >= 0) {
                        vm.lstDevice[index] = result;
                    } else {
                        vm.lstDevice.push(result);
                    }
                });
            };
            vm.deleteDevices = function (index) {
                vm.lstDevice.splice(index, 1);
            };
            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadFile'
            });
            vm.uploader.filters.push({
                name: 'imageFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    return true;
                }
            });
            vm.uploader.onAfterAddingFile = function (item) {
                console.log(item.file.size);

                if (item.file.size > 6000000) {
                    abp.notify.error(app.localize('Size is Big'));
                    vm.uploader.clearQueue();
                } else {
                    item.upload();
                }
            };
            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response !== null) {
                    if (response.result !== null) {
                        vm.terminalDineMenuSetting.DineMenuImagePath = response.result.fileSystemName;
                    }
                }
            };
            vm.uploaderLogoPath = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadFile'
            });
            vm.uploaderLogoPath.filters.push({
                name: 'imageFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    return true;
                }
            });
            vm.uploaderLogoPath.onAfterAddingFile = function (item) {
                console.log(item.file.size);

                if (item.file.size > 6000000) {
                    abp.notify.error(app.localize('Size is Big'));
                    vm.uploaderLogoPath.clearQueue();
                } else {
                    item.upload();
                }
            };
            vm.uploaderLogoPath.onSuccessItem = function (fileItem, response, status, headers) {
                if (response !== null) {
                    if (response.result !== null) {
                        vm.terminalGeneralSetting.LogoPath = response.result.fileSystemName;
                    }
                }
            };
        }
    ]);
})();