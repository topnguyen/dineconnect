﻿(function () {
    appModule.controller('tenant.views.connect.terminal.createOrEditDevicesModal', [
        '$scope', '$uibModalInstance', 'data', 'deviceNameSelected','abp.services.app.terminal',
        function ($scope, $modalInstance, data, deviceNameSelected, terminalService) {
            var vm = this;
            vm.credit = false;
            vm.deviceName = "";
            vm.dataModal= null;
            vm.lstDeviceName = [
                "Generic Modem",
                "Artech",
                "Call Center Client",
                "Mettler Toledo",
                "Custom Scale",
            ];
            vm.save = function () {
               

                terminalService.generationKey({
                }).success(function (result) {
                    vm.dataModal.DeviceName = vm.deviceName;
                    vm.dataModal.Key = result;
                    console.log('vm.dataModal', vm.dataModal);
                    $modalInstance.close(vm.dataModal);
                }).finally(function () {
                    vm.saving = false;
                });

            };
            vm.close = function () {
                $modalInstance.dismiss();
            };
          
            vm.init = function () {
                console.log('accc', deviceNameSelected);
                vm.dataModal = data;
                if (data && data.DeviceName) {
                    vm.deviceName = data.DeviceName;
                    vm.isDisable = true;
                } else {
                    var dataNotSelected = [];
                    angular.forEach(vm.lstDeviceName, function (value, key) {
                        if (!deviceNameSelected.includes(value)) {
                            dataNotSelected.push(value)
						}
                    });
                    console.log('dataNotSelected', dataNotSelected);
                    vm.lstDeviceName = dataNotSelected;
                    vm.deviceName = vm.lstDeviceName[0];
                    vm.isDisable = false;
				}
            };

            vm.init();

        }
    ]);
})();