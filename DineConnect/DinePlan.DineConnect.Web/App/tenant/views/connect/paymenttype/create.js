﻿(function () {
    appModule.controller('tenant.views.connect.master.paymenttype.create', [
        '$scope', '$state', '$uibModal', '$stateParams', 'abp.services.app.paymentType', "abp.services.app.connectLookup", 'FileUploader', "abp.services.app.department",
        function ($scope, $state, $modal, $stateParams, paymenttypeService, connectService, fileUploader, deptService) {
            var vm = this;
            vm.saving = false;
            vm.paymenttype = null;
            vm.processors = [];
            vm.setting = null;
            vm.isUpload = false;
            vm.languageDescriptionType = 10;
            vm.paymentTypeId = $stateParams.id;
            vm.departments = [];
            vm.requestdepartments = "";

            vm.save = function () {
                vm.saving = true;
                vm.setPaymentTag();

                vm.paymenttype.processors = null;
                if (!vm.isUpload && vm.uploader.queue.length) {
                    vm.uploadFile(false);
                }
                else {
                    vm.saveToDB();
                }
            };

            vm.saveToDB = function () {
                vm.paymenttype.files = angular.toJson(vm.replyFiles);
                vm.paymenttype.departments = angular.toJson(vm.requestdepartments);

                if (vm.paymentProcessor)
                    vm.paymenttype.processors = angular.toJson(vm.paymentProcessor);

                paymenttypeService.createOrUpdatePaymentType({
                    paymentType: vm.paymenttype,
                    locationGroup: vm.locationGroup
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };

            vm.paymentTags = [];
            vm.getPaymentTags = function (tag) {
                var tags = tag.split(',');

                angular.forEach(tags, function (item) {
                    if (item) {
                        vm.paymentTags.push({ "text": item });
                    }
                });
            };

            vm.setPaymentTag = function () {
                var tags = [];
                angular.forEach(vm.paymentTags, function (item) {
                    if (item) {
                        tags.push(item.text);
                    }
                });
                vm.paymenttype.paymentTag = tags.join(",");
            };

            vm.cancel = function () {
                $state.go("tenant.paymenttype");
            };

            vm.onchangeprocessor = function () {
                vm.fields = null;
                connectService.getProcessorSetting({id:vm.paymenttype.paymentProcessor}).success(function (result) {
                    vm.fields = result;
                });
            };

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };

            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadFile'
            });

            vm.uploader.filters.push({
                name: 'imageFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    return true;
                }
            });
            vm.uploader.onAfterAddingFile = function(item) {
                console.log(item.file.size);

                if (item.file.size > 6000000) {
                    abp.notify.error(app.localize('Size is Big'));
                    vm.uploader.clearQueue();
                } else {
                    item.upload();
                }
            };
            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response !== null) {
                    if (response.result !== null) {
                        vm.replyFiles = [];
                        if (response.result.fileName !== null) {
                            vm.replyFiles.push(response.result);
                            vm.refreshImage = true;
                            if (!vm.isUpload) {
                                vm.saveToDB();
                            }
                        }
                    }
                }
            };

            vm.downloader = function (file) {
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + file.fileName+'&url='+file.fileSystemName;
            };

            vm.removeFile = function (findex) {
                vm.replyFiles.splice(findex, 1);
                vm.refreshImage = true;
            };

            vm.uploadFile = function (isuload) {
                vm.isUpload = isuload;
                var file = vm.uploader.queue[vm.uploader.queue.length - 1];
                file.upload();
            };

            vm.openLanguageDescriptionModal = function () {
                vm.languageDescription = {
                    id: vm.paymentTypeId,
                    name: vm.paymenttype.name,
                    languageDescriptionType: vm.languageDescriptionType
                };
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/languageDescription/languageDescriptionModal.cshtml",
                    controller: "tenant.views.connect.languageDescription.languageDescriptionModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        languagedescription: function () {
                            return vm.languageDescription;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });
            }

            function init() {
                paymenttypeService.getPaymentTypeForEdit({
                    id: vm.paymentTypeId
                }).success(function (result) {
                  
                    console.log(result);

                    vm.paymenttype = result.paymentType;
                    vm.locationGroup = result.locationGroup;
                    vm.replyFiles = vm.paymenttype.files ? angular.fromJson(vm.paymenttype.files) : [];
                    vm.requestdepartments = result.departments;

                    if (vm.paymenttype.processors) {
                        vm.paymentProcessor = angular.fromJson(vm.paymenttype.processors);
                        connectService.getProcessorSetting({id:vm.paymenttype.paymentProcessor}).success(function (result) {
                            vm.fields = result;
                        });
                    }

                    if (vm.paymenttype.paymentTag != null)
                        vm.getPaymentTags(vm.paymenttype.paymentTag);
                });

                connectService.getPaymentProcessors({}).success(function (result) {
                    vm.processors = result.items;
                    vm.processors.unshift({ value: "0", displayText: app.localize("NotAssigned") });
                });

                vm.getDepartments();
                vm.getPaymentTypeTenders();
            }

            vm.getDepartments = function () {
                deptService.getDepartmentsForCombobox({})
                    .success(function (result) {
                        vm.departments = result.items;
                    })
                    .finally(function (result) {
                    });
            };

            vm.getPaymentTypeTenders = function () {
                console.log("Coming");
                paymenttypeService.getPaymentTypeTenders({})
                    .success(function (result) {
                        console.log(result);
                        vm.paymentTypeTenders = result.items;
                    })
                    .finally(function (result) {
                    });
            };
            init();
        }
    ]);
})();