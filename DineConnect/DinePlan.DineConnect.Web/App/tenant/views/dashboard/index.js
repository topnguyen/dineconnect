﻿(function () {
    appModule.controller('tenant.views.dashboard.index', [
        '$scope', '$uibModal', '$interval', 'appSession', '$rootScope', 'abp.services.app.tenantDashboard', 'abp.services.app.ticket',
        function ($scope, $modal, $interval, appSession, $rootScope, tenantDashboardService, setupService) {
            var vm = this;
            $rootScope.settings.layout.pageSidebarClosed = true;

            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeOptions.opens = 'left';

            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];
            var todayAsString = moment().format($scope.format);
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };
            vm.allLocation = false;
            vm.totalTicketAmount = 0;
            vm.totalTickets = 0;
            vm.totalOrders = 0;
            vm.totalItems = 0;
            vm.totalAverage = 0;

            vm.currentLocation = appSession.location != null ? appSession.location.name : "";
            vm.currentLocationId = appSession.location != null ? appSession.location.id : 0;

            vm.allLocation = false;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });



            vm.currencyText = abp.features.getValue('DinePlan.DineConnect.Connect.Currency');


        
            vm.paymentsChart = function () {
                setupService.getPaymentChart({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    location: vm.allLocation ? 0 : vm.currentLocationId
                }).success(function (result) {
                    Highcharts.chart('payment_stats', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{point.percentage:.1f} %'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.y} <br/>{point.percentage:.1f} %',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            name: app.localize('Payment'),
                            colorByPoint: true,
                            data: $.parseJSON(JSON.stringify(result))
                        }]
                    });
                });
            };
            vm.transactionChart = function () {

                setupService.getTransactionChart({
                    location: vm.allLocation ? 0 : vm.currentLocationId,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format)
                }).success(function (result) {
                    Highcharts.chart('transaction_stats', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{point.percentage:.1f} %'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.y} <br/>{point.percentage:.1f} %',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            name: app.localize('Payment'),
                            colorByPoint: true,
                            data: $.parseJSON(JSON.stringify(result))
                        }]
                    });

                });
            };
            vm.departmentChart = function () {
                setupService.getDepartmentChart({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    location: vm.allLocation ? 0 : vm.currentLocationId
                }).success(function (result) {
                    Highcharts.chart('department_stats', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{point.percentage:.1f} %'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.y} <br/>{point.percentage:.1f} %',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            name: app.localize('Department'),
                            colorByPoint: true,
                            data: $.parseJSON(JSON.stringify(result))
                        }]
                    });
                });
            };

            
            vm.itemChart = function () {
                setupService.getItemChart({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    location: vm.allLocation ? 0 : vm.currentLocationId
                }).success(function (result) {
                    Highcharts.chart('item_stats', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{point.percentage:.1f} %'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.y}',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            name: app.localize('Department'),
                            colorByPoint: true,
                            data: $.parseJSON(JSON.stringify(result))
                        }]
                    });

                });
            };

            vm.getTicketDetails = function () {
                setupService.getDashboardChart({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    location: vm.allLocation ? 0 : vm.currentLocationId
                }).success(function (result) {
                    if (result != null) {
                        vm.totalTicketAmount = result.dashBoardDto.totalAmount;
                        vm.totalTickets = result.dashBoardDto.totalTicketCount;
                        vm.totalOrders = result.dashBoardDto.totalOrderCount;
                        vm.totalItems = result.dashBoardDto.totalItemSold;
                        vm.totalAverage = result.dashBoardDto.averageTicketAmount;
                    }
                });
            }

            vm.refreshPromise = null;
            vm.autoRefresh = false;
            vm.autoRefreshScreen = function () {

                if (vm.autoRefresh) {
                    vm.refreshPromise = $interval(vm.refresh, 10000);
                } else {
                    if (vm.refreshPromise != null)
                        $interval.cancel(vm.refreshPromise);
                }
            }

            vm.refresh = function() {

                if (vm.allLocation) {
                    vm.currentLocationId = 0;
                    vm.currentLocation = app.localize('All');
                }

                vm.getTicketDetails();
                vm.paymentsChart();
                vm.transactionChart();
                vm.departmentChart();
                vm.itemChart();
            };
            vm.refresh();

            vm.openLocation = function () {

                vm.locationGroup = app.createLocationInputForCreateSingleLocation();
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    size: 'lg',
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {

                    if (result.locations.length > 0) {
                        vm.currentLocationId = result.locations[0].id;
                        vm.currentLocation = result.locations[0].name;
                        vm.refresh();
                    }

                });
            }


            vm.openModalTickets = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/dashboard/modal/tickets/modal.cshtml",
                    controller: "tenant.views.connect.dashboard.tickets.modal as vm",
                    backdrop: "static",
                    keyboard: false,
                    size: 'lg'
                });
                modalInstance.result.then(function (result) {



                });
            }
            vm.openModalPayment = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/dashboard/modal/payment/modal.cshtml",
                    controller: "tenant.views.connect.dashboard.payment.modal as vm",
                    backdrop: "static",
                    keyboard: false,
                    size: 'lg'
                });
                modalInstance.result.then(function (result) {



                });
            }
            vm.openModalDepartment = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/dashboard/modal/department/modal.cshtml",
                    controller: "tenant.views.connect.dashboard.department.modal as vm",
                    backdrop: "static",
                    keyboard: false,
                    size: 'lg'
                });
                modalInstance.result.then(function (result) {



                });
            }
            vm.openModalTransaction = function() {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/dashboard/modal/transaction/modal.cshtml",
                    controller: "tenant.views.connect.dashboard.transaction.modal as vm",
                    backdrop: "static",
                    keyboard: false,
                    size: 'lg'
                });
                modalInstance.result.then(function(result) {


                });
            };
            vm.openModalItem = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/dashboard/modal/item/modal.cshtml",
                    controller: "tenant.views.connect.dashboard.item.modal as vm",
                    backdrop: "static",
                    keyboard: false,
                    size: 'lg'
                });
                modalInstance.result.then(function (result) {



                });
            }

        }
    ]);
})();