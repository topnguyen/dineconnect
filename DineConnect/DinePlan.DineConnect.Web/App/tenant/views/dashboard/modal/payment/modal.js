﻿(function () {
    appModule.controller('tenant.views.connect.dashboard.payment.modal', [
        '$scope', '$uibModalInstance', 'abp.services.app.ticket',
        function ($scope, $modalInstance, ticketService) {
            var vm = this;

            vm.close = function () {
                $modalInstance.dismiss();
            };

            vm.init = function() {
                
            };

            vm.init();
        }
    ]);
})();