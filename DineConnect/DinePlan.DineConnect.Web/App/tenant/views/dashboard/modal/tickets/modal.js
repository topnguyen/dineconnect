﻿(function () {
    appModule.controller('tenant.views.connect.dashboard.tickets.modal', [
        '$scope', '$uibModalInstance', 'abp.services.app.ticket', 'abp.services.app.connectReport',
        function ($scope, $modalInstance, ticketService, connectService) {
            var vm = this;

            vm.down = false;
            vm.display = false;

            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeOptions.opens = 'right';

            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];
            var todayAsString = moment().format($scope.format);
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.cdateRangeOptions = app.createDateRangePickerOptions();
            vm.cdateRangeOptions.opens = 'left';
            vm.cdateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.close = function () {
                $modalInstance.dismiss();
            };
            vm.totalTicketAmount = 0;
            vm.totalTickets = 0;
            vm.totalOrders = 0;
            vm.totalItems = 0;
            vm.totalAverage = 0;

            vm.refresh = function() {
                ticketService.getDashboardChart({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    location: vm.allLocation ? 0 : vm.currentLocationId
                }).success(function (result) {
                    if (result != null) {
                        vm.totalTicketAmount = result.dashBoardDto.totalAmount;
                        vm.totalTickets = result.dashBoardDto.totalTicketCount;
                        vm.totalOrders = result.dashBoardDto.totalOrderCount;
                        vm.totalItems = result.dashBoardDto.totalItemSold;
                        vm.totalAverage = result.dashBoardDto.averageTicketAmount;
                    }
                });

                vm.refreshSaleChart();
            }

            vm.refreshSaleChart = function() {
                connectService.getHourlySales({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format)
                }).success(function (result) {
                    if (result != null) {
                        console.log(result);

                        //Highcharts.chart('hourly_chart', {
                        //    chart: {
                        //        type: 'column'
                        //    },
                        //    title: {
                        //        text: ''
                        //    },
                        //    xAxis: {
                        //        categories: ['0', '1', '2', '3', '4']
                        //    },
                        //    yAxis: {
                        //        min: 0,
                        //        title: {
                        //            text: ''
                        //        }
                        //    },
                        //    legend: {
                        //        reversed: true
                        //    },
                        //    series: [{
                        //        name: 'Total',
                        //        data: [5, 3, 4, 7, 2]
                        //    }, {
                        //        name: 'Ticket',
                        //        data: [2, 2, 3, 2, 1]
                        //    }]
                        //});


                       
                    }
                });
            }

            vm.ctotalTicketAmount = 0;
            vm.ctotalTickets = 0;
            vm.ctotalOrders = 0;
            vm.ctotalItems = 0;
            vm.ctotalAverage = 0;

            vm.crefresh = function () {
                ticketService.getDashboardChart({
                    startDate: moment(vm.cdateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.cdateRangeModel.endDate).format($scope.format),
                    location: vm.allLocation ? 0 : vm.currentLocationId
                }).success(function (result) {
                    if (result != null) {
                        vm.ctotalTicketAmount = result.dashBoardDto.totalAmount;
                        vm.ctotalTickets = result.dashBoardDto.totalTicketCount;
                        vm.ctotalOrders = result.dashBoardDto.totalOrderCount;
                        vm.ctotalItems = result.dashBoardDto.totalItemSold;
                        vm.ctotalAverage = result.dashBoardDto.averageTicketAmount;

                        vm.display = true;

                        if (vm.ctotalTicketAmount > vm.totalTicketAmount) {
                            vm.down = true;
                        } else {
                            vm.down = false;
                        }

                        if (vm.ctotalTickets > vm.totalTickets) {
                            vm.downTickets = true;
                        } else {
                            vm.downTickets = false;
                        }

                        if (vm.ctotalOrders > vm.totalOrders) {
                            vm.downOrders = true;
                        } else {
                            vm.downOrders = false;
                        }

                        if (vm.ctotalItems > vm.totalItems) {
                            vm.downItems = true;
                        } else {
                            vm.downItems = false;
                        }

                        if (vm.ctotalAverage > vm.totalAverage) {
                            vm.downAvg = true;
                        } else {
                            vm.downAvg = false;
                        }
                    }
                });

            }
        }
    ]);
})();