﻿(function () {
    appModule.controller('tenant.views.customreport.index', [
        '$scope', 
        function ($scope) {
            var vm = this;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
        }
    ]);
})();