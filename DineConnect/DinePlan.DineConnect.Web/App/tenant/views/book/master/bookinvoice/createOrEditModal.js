﻿
(function () {
    appModule.controller('tenant.views.book.master.bookinvoice.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.bookInvoice', 'bookinvoiceId', 'abp.services.app.commonLookup',
        function ($scope, $modalInstance, bookinvoiceService, bookinvoiceId, commonLookupService) {
            var vm = this;
            
			alert('create');

            vm.saving = false;
            vm.bookinvoice = null;
			$scope.existall = true;

			
            vm.save = function () {
			   if ($scope.existall == false)
                    return;
                vm.saving = true;
				
                bookinvoiceService.createOrUpdateBookInvoice({
                    bookInvoice: vm.bookinvoice
                }).success(function () {
                    abp.notify.info('\' BookInvoice \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.bookinvoice.name == null) {
			         vm.existall = false;
			         return;
			     }
				 
                bookinvoiceService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'name',
                    filter: vm.bookinvoice.name,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.bookinvoice.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.bookinvoice.name + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
			vm.reflocations = [];

			 function fillDropDownLocations() {
			     	commonLookupService.getLocationsForCombobox({}).success(function (result) {
                    vm.reflocations = result.items;
                   // vm.reflocations.unshift({ value: "0", displayText: app.localize('NotAssigned') });
                });
			 }

	        function init() {
				alert('init');
				fillDropDownLocations();

                bookinvoiceService.getBookInvoiceForEdit({
                    Id: bookinvoiceId
                }).success(function (result) {
                    vm.bookinvoice = result.bookInvoice;
                });
            }
            init();
        }
    ]);
})();

