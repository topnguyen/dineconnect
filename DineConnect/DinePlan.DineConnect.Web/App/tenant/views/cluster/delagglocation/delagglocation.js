﻿
(function () {
    appModule.controller('tenant.views.cluster.delagglocation.delagglocation', [
        '$scope', '$uibModal', '$state', '$stateParams', 'abp.services.app.delAggLocation', 'abp.services.app.delAggLocationGroup', 'abp.services.app.delAggLocMapping', 'FileUploader',
        function ($scope, $modal, $state, $stateParams, delagglocationService, delagglocationgroupService, delagglocmappingService, fileUploader) {
            /* eslint-disable */
            var vm = this;
            
            vm.saving = false;
            vm.delagglocation = null;
            delagglocationId = $stateParams.id;
            vm.delaggimage = [];
            vm.imagePath = [];

            vm.save = function () {
                if (vm.imagePath.length > 0) {
                    abp.notify.info("Unsaved Data in ImagePath");
                    abp.message.error("Unsaved Data in ImagePath");
                    return true;
                }
                vm.saving = true;

                delagglocationService.createOrUpdateDelAggLocation({
                    delAggLocation: vm.delagglocation,
                    delAggImage: vm.delaggimage
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $state.go('tenant.delagglocation');
            };
            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForCreateSingleLocation();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                    vm.delagglocation.locationId = result.locations[0].id;
                    vm.delagglocation.locationName = result.locations[0].name;
                    if (vm.editFlag == false) {
                        angular.forEach(vm.userGridOptions.data,
                            function (value, key) {
                                if (value.locationId == vm.delagglocation.locationId) {
                                    abp.message.error(app.localize('LocationPriceWarning', vm.delagglocation.locationName));
                                    vm.delagglocation.locationId = 0;
                                    vm.intiailizeOpenLocation();
                                    return;
                                }
                            });
                    }
                });
            };


            vm.locationGroups = [];
            function fillDropDownLocationGroup() {
                delagglocationgroupService.getAll({ maxResultCount: 1000 }).success(function (result) {
                    vm.locationGroups = result.items;
                });
            }

            function init() {
                vm.loading = true;
                fillDropDownLocationGroup();
              
                delagglocationService.getDelAggLocationForEdit({
                    id: delagglocationId
                }).success(function (result) {
                    vm.delagglocation = result.delAggLocation;
                    vm.delaggimage = result.delAggImage;

                    angular.forEach(vm.delaggimage, function (delImage, delkey) {
                        vm.delAggTypes.some(function (value, key) {
                            if (delImage.delAggTypeRefId == null) {
                                delImage.delAggTypeRefName = app.localize("Default");
                                return true;
                            }
                            else if (value.value == delImage.delAggTypeRefId) {
                                delImage.delAggTypeRefName = value.displayText;
                                return true;
                            }
                        });
                        delImage.displayImagePath = delImage.imagePath ? angular.fromJson(delImage.imagePath) : [];
                    });
                }).finally(function () {
                    vm.loading = false;
                });
            }

            //image
            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            var img = [];
            vm.addImagePortion = [];
            fillDropDownDelAggType();
            vm.addImage = function (argsDelAggTypeRefId, argImgPath) {
                var delAggTypeRefName = '';
                // Check already null exists
                var argErrorFlag = false;
                if (vm.isUndefinedOrNull(argsDelAggTypeRefId)) {
                    vm.delaggimage.some(function (delImage, key) {
                        if (vm.isUndefinedOrNull(delImage.delAggTypeRefId)) {
                            abp.notify.error(app.localize("AlreadyDefaultImageUploadedError"));
                            argErrorFlag = true;
                            return true;
                        }
                    });
                    if (argErrorFlag == true)
                        return;
                }

                vm.delAggTypes.some(function (value, key) {
                    if (argsDelAggTypeRefId == null) {
                        delAggTypeRefName = app.localize("Default");
                    }
                    else if (value.value == argsDelAggTypeRefId) {
                        delAggTypeRefName = value.displayText;
                        return true;
                    }
                });
                angular.forEach(argImgPath, function (value, key) {
                    img = {
                        'delAggTypeRefId': argsDelAggTypeRefId,
                        'delAggTypeRefName': delAggTypeRefName,
                        'imagePath': angular.toJson(value)
                    }
                    vm.delaggimage.push(img);
                });
                angular.forEach(vm.delaggimage, function (delImage, delkey) {
                    delImage.displayImagePath = delImage.imagePath ? angular.fromJson(delImage.imagePath) : [];
                });
                vm.delaggimage.delAggTypeRefId = null;
                vm.imagePath = [];
            }

            vm.delAggTypes = [];
            function fillDropDownDelAggType() {
                delagglocmappingService.getDelAggTypeForCombobox({}).success(function (result) {
                    vm.delAggTypes = result.items;
                    init();
                });
            }

            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadFile'
            });

            vm.uploader.filters.push({
                name: 'imageFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    return true;
                }
            });

            vm.uploader.onBeforeUploadItem = function (fileitem) {
                fileitem.formData.push({ fileTag: vm.fileTag });
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response !== null) {
                    if (response.result.fileTag == 'Image') {
                        if (response.result !== null) {
                            if (response.result.fileName !== null) {
                                vm.imagePath.push(response.result);
                                vm.refreshImage = true;
                                if (!vm.isUpload) {
                                    vm.save();
                                }
                            }
                        }
                    }
                    $("#imageFile").val(null);
                }
            };

            vm.viewer = function (file) {
                var fileName = angular.fromJson(file.imagePath);
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + fileName.fileName + '&url=' + fileName.fileSystemName;
            };

            vm.downloader = function (file) {
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + file.fileName + '&url=' + file.fileSystemName;
            };

            vm.deleteImage = function (index) {
                vm.delaggimage.splice(index, 1);
            }

            vm.removeFile = function (findex) {
                if (vm.fileTag == 'Image') {
                    vm.imagePath.splice(findex, 1);
                }

                if (vm.imagePath.length > 0)
                    $scope.btnStatus = true;
                else
                    $scope.btnStatus = false;
                vm.refreshImage = true;
            };
            $scope.btnStatus = false;
            vm.uploadFile = function (isuload) {
                $scope.btnStatus = true;
                vm.isUpload = isuload;
                var file = vm.uploader.queue[vm.uploader.queue.length - 1];
                file.upload();
            };
        }
    ]);
})();

