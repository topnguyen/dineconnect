﻿
(function () {
    appModule.controller('tenant.views.cluster.delaggvariantgroup.delaggvariantgroup', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'abp.services.app.delAggVariantGroup', 'abp.services.app.delAggVariant', 'abp.services.app.delAggLocMapping', 'FileUploader',
        function ($scope, $filter, $state, $stateParams, $modal, delaggvariantgroupService, delaggvariantService, delagglocmappingService, fileUploader) {
            /* eslint-disable */
            var vm = this;
            
            vm.saving = false;
            vm.delaggvariantgroup = null;
            delaggvariantgroupId = $stateParams.id;
            $scope.existall = true;
            vm.sno = 0;
            vm.loading = false;
            vm.delaggimage = [];
            vm.imagePath = [];


            vm.save = function () {
                if (vm.imagePath.length > 0) {
                    abp.notify.info("Unsaved Data in ImagePath");
                    abp.message.error("Unsaved Data in ImagePath");
                    return true;
                }
                vm.saving = true;
                delaggvariantgroupService.createOrUpdateDelAggVariantGroup({
                    delAggVariantGroup: vm.delaggvariantgroup,
                    delAggImage: vm.delaggimage
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $state.go('tenant.delaggvariantgroup');
            };

            function init() {
                delaggvariantgroupService.getDelAggVariantGroupForEdit({
                    id: delaggvariantgroupId
                }).success(function (result) {
                    vm.delaggvariantgroup = result.delAggVariantGroup;
                    vm.delaggimage = result.delAggImage;

                    angular.forEach(vm.delaggimage, function (delImage, delkey) {
                        vm.delAggTypes.some(function (value, key) {
                            if (delImage.delAggTypeRefId == null) {
                                delImage.delAggTypeRefName = app.localize("Default");
                                return true;
                            }
                            else if (value.value == delImage.delAggTypeRefId) {
                                delImage.delAggTypeRefName = value.displayText;
                                return true;
                            }
                        });
                        delImage.displayImagePath = delImage.imagePath ? angular.fromJson(delImage.imagePath) : [];
                    });

                    delaggvariantService.getAll({
                        delAggVariantGroupId: delaggvariantgroupId
                    }).success(function (result) {
                        vm.delaggVariantItems = result.items;
                    }).finally(function () {
                    });
                });
            }
            

            vm.openLanguageDescriptionModal = function () {

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/cluster/clusterlanguageDescription/languageDescriptionModal.cshtml",
                    controller: "tenant.views.cluster.clusterlanguageDescription.languageDescriptionModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        delAggLanguage: function () {
                            return {
                                id: vm.delaggvariantgroup.id,
                                language: vm.delaggvariantgroup.name,
                                languageDescriptionType: 4 
                            };
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });

            };

            //image
            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            var img = [];
            vm.addImagePortion = [];
            fillDropDownDelAggType();
            vm.addImage = function (argsDelAggTypeRefId, argImgPath) {
                var delAggTypeRefName = '';
                // Check already null exists
                var argErrorFlag = false;
                if (vm.isUndefinedOrNull(argsDelAggTypeRefId)) {
                    vm.delaggimage.some(function (delImage, key) {
                        if (vm.isUndefinedOrNull(delImage.delAggTypeRefId)) {
                            abp.notify.error(app.localize("AlreadyDefaultImageUploadedError"));
                            argErrorFlag = true;
                            return true;
                        }
                    });
                    if (argErrorFlag == true)
                        return;
                }

                vm.delAggTypes.some(function (value, key) {
                    if (argsDelAggTypeRefId == null) {
                        delAggTypeRefName = app.localize("Default");
                    }
                    else if (value.value == argsDelAggTypeRefId) {
                        delAggTypeRefName = value.displayText;
                        return true;
                    }
                });
                angular.forEach(argImgPath, function (value, key) {
                    img = {
                        'delAggTypeRefId': argsDelAggTypeRefId,
                        'delAggTypeRefName': delAggTypeRefName,
                        'imagePath': angular.toJson(value)
                    }
                    vm.delaggimage.push(img);
                });
                angular.forEach(vm.delaggimage, function (delImage, delkey) {
                    delImage.displayImagePath = delImage.imagePath ? angular.fromJson(delImage.imagePath) : [];
                });
                vm.delaggimage.delAggTypeRefId = null;
                vm.imagePath = [];
            }

            vm.delAggTypes = [];
            function fillDropDownDelAggType() {
                delagglocmappingService.getDelAggTypeForCombobox({}).success(function (result) {
                    vm.delAggTypes = result.items;
                    init();
                });
            }

            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadFile'
            });

            vm.uploader.filters.push({
                name: 'imageFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    return true;
                }
            });

            vm.uploader.onBeforeUploadItem = function (fileitem) {
                fileitem.formData.push({ fileTag: vm.fileTag });
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response !== null) {
                    if (response.result.fileTag == 'Image') {
                        if (response.result !== null) {
                            if (response.result.fileName !== null) {
                                vm.imagePath.push(response.result);
                                vm.refreshImage = true;
                                if (!vm.isUpload) {
                                    vm.save();
                                }
                            }
                        }
                    }
                    $("#imageFile").val(null);
                }
            };
            vm.deleteImage = function (index) {
                vm.delaggimage.splice(index, 1);
            }
            vm.viewer = function (file) {
                var fileName = angular.fromJson(file.imagePath);
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + fileName.fileName + '&url=' + fileName.fileSystemName;
            };

            vm.downloader = function (file) {
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + file.fileName + '&url=' + file.fileSystemName;
            };

            vm.removeFile = function (findex) {
                if (vm.fileTag == 'Image') {
                    vm.imagePath.splice(findex, 1);
                }
                if (vm.imagePath.length > 0)
                    $scope.btnStatus = true;
                else
                    $scope.btnStatus = false;
                vm.refreshImage = true;
            };

            $scope.btnStatus = false;
            vm.uploadFile = function (isuload) {
                $scope.btnStatus = true;
                vm.isUpload = isuload;
                var file = vm.uploader.queue[vm.uploader.queue.length - 1];
                file.upload();
            };

            vm.addVariant = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/cluster/delaggvariantgroup/delaggvariant.cshtml",
                    controller: "tenant.views.cluster.delaggvariantgroup.delaggvariant as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        groupId: vm.delaggvariantgroup.id,
                        variantId: null
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.delaggVariantItems.push(result);
                });
            };

            vm.editVariant = function (variantId) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/cluster/delaggvariantgroup/delaggvariant.cshtml",
                    controller: "tenant.views.cluster.delaggvariantgroup.delaggvariant as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        groupId: vm.delaggvariantgroup.id,
                        variantId: variantId
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.delaggVariantItems = vm.delaggVariantItems.filter(x => x.id != result.id);
                    vm.delaggVariantItems.push(result);
                });
            };

            vm.deleteVariant = function (variant) {
                abp.message.confirm(
                    app.localize('DeleteDelAggVariantWarning', variant.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            delaggvariantService.deleteDelAggVariant({
                                id: variant.id
                            }).success(function () {
                                vm.delaggVariantItems = vm.delaggVariantItems.filter(x => x.id != variant.id);
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };
        }
    ]);
})();

