﻿(function () {
    appModule.controller('tenant.views.cluster.delaggLanguages.textsdelaggLanguages', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.delAggLanguage', 'uiGridConstants',
        function ($scope, $state, $stateParams, $uibModal, languageService, uiGridConstants) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            //Grid data
            var nonFilteredData = [];

            //Combobox values
            vm.sourceNames = [];
            vm.languages = [];

            //Filters
            vm.targetLanguageName = $stateParams.languageName;
            vm.sourceName = $stateParams.sourceName || 'DineConnect';
            vm.baseLanguageName = $stateParams.baseLanguageName || abp.localization.currentLanguage.name;
            vm.targetValueFilter = $stateParams.targetValueFilter || 'ALL';
            vm.filterText = $stateParams.filterText || '';

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.loading = false;

            vm.gridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize('Key'),
                        field: 'key',
                        minWidth: 70
                    },
                    {
                        name: app.localize('Value'),
                        field: 'languageValue',
                        minWidth: 140
                    },
                    {
                        name: app.localize('Edit'),
                        width: 60,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.openTextEditModal(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>' +
                            '</div>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.loadTexts();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.loadTexts();
                    });
                },
                data: []
            };

            vm.loadTexts = function () {
                vm.loading = true;
                languageService.getDelAggLanguageTexts({
                    sourceName: vm.sourceName,
                    baseLanguageName: vm.baseLanguageName,
                    targetLanguageName: vm.targetLanguageName,
                    filterText: vm.filterText,
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting
                }).success(function (result) {
                    vm.gridOptions.totalItems = result.totalCount;
                    vm.gridOptions.data = result.items;
                    console.log(vm.gridOptions);
                    }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.createText = function () {
                vm.openTextEditModal();
            };

            vm.openTextEditModal = function (text) {
                var modalInstance = $uibModal.open({
                    templateUrl: '~/App/tenant/views/cluster/delaggLanguages/editTextModal.cshtml',
                    controller: 'tenant.views.cluster.delaggLanguages.editTextModal as vm',
                    resolve: {
                        sourceName: function () {
                            return vm.sourceName;
                        },
                        baseLanguageName: function () {
                            return vm.baseLanguageName;
                        },
                        languageName: function () {
                            return vm.targetLanguageName;
                        },
                        allTexts: function () {
                            return vm.gridOptions.data;
                        },
                        initialText: function () {
                            return text;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.loadTexts();
                });
            };

            function initializeFilters() {
                var sources = _.filter(abp.localization.sources, function (source) {
                    return source.type == 'MultiTenantLocalizationSource';
                });

                vm.sourceNames = _.map(sources, function (value) {
                    return value.name;
                });

                vm.languages = abp.localization.languages;

                function reloadWhenChange(variableName) {
                    $scope.$watch(variableName, function (newValue, oldValue) {
                        if (newValue == oldValue) {
                            return;
                        }

                        $state.go('delaggLanguageTexts', {
                            language: vm.targetLanguageName,
                            sourceName: vm.sourceName,
                            baseLanguageName: vm.baseLanguageName,
                            targetValueFilter: vm.targetValueFilter,
                            filterText: vm.filterText
                        }, {
                                location: 'replace'
                            });
                    });
                }

                reloadWhenChange('vm.targetLanguageName');
            }

            vm.exportToExcel = function () {
                languageService.getDelAggLanguageTextsForExcel({
                    sourceName: vm.sourceName,
                    baseLanguageName: vm.baseLanguageName,
                    targetLanguageName: vm.targetLanguageName,
                    filterText: vm.filterText,
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting
                }).success(function (result) {
                    app.downloadTempFile(result);
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.importLanguages = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '~/App/tenant/views/cluster/delaggLanguages/importModal.cshtml',
                    controller: 'tenant.views.cluster.delaggLanguages.importModal as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        languageName: function () {
                            return vm.targetLanguageName;
                        },
                        sourceName: function () {
                            return vm.sourceName;
                        },
                        baseLanguageName: function () {
                            return vm.baseLanguageName;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.loadTexts();
                });
            };

            initializeFilters();
            vm.loadTexts();
        }
    ]);
})();