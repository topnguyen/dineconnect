﻿
(function () {
    appModule.controller('tenant.views.cluster.delaggvariant.indexdelaggvariant', [
        '$scope', '$uibModal','$state', 'uiGridConstants', 'abp.services.app.delAggVariant', "abp.services.app.menuItem", 'abp.services.app.delAggVariantGroup',
        function ($scope, $modal,$state, uiGridConstants, delaggvariantService, menuService, delaggvariantgroupService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggVariant.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggVariant.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggVariant.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editDelAggVariant(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteDelAggVariant(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('DelAggVariantGroup'),
                        field: 'delAggVariantGroupName'
                    },
                    {
                        name: app.localize('MenuItemPortion'),
                        field: 'menuItemPortionName'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.variantgroups = [];
            function fillDropDownVariantGroup() {
                delaggvariantgroupService.getNames({}).success(function (result) {
                    vm.variantgroups = result.items;
                });
            }
            fillDropDownVariantGroup();

            vm.delaggvariant =
            {
                'id': null,
                'productGroupId': 0,
                'productGroupName': null,
                'categoryId': 0,
                'categoryName': null,
                'menuItemId': 0,
                'menuItemGroupCode': null,
                'menuItemPortionId': 0,
                'portionName': null,
                'percentage': 0
            };

            vm.openforMenuItemPortions = function (data) {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    menuItemId: data.menuItemId,
                    service: !data.menuItemId || data.menuItemId == 0 ? menuService.getAllMenuItemPortions : menuService.getAllMenuItemPortionsBasedOnMenuItemId
                };

                const modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        data.menuItemPortionId = result.locations[0].id;
                        data.portionName = result.locations[0].name;
                    }
                });
            };

            vm.getAllClear = function () {
                vm.filterText = null;
                vm.delAggVariantGroupId = null;
                vm.delaggvariant.menuItemPortionId = 0;
            }

            vm.getAll = function () {
                vm.loading = true;
                delaggvariantService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    delAggVariantGroupId: vm.delAggVariantGroupId,
                    menuItemPortionId: vm.delaggvariant.menuItemPortionId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editDelAggVariant = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createDelAggVariant = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteDelAggVariant = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteDelAggVariantWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            delaggvariantService.deleteDelAggVariant({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.delaggvariantcreate", {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                delaggvariantService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    delAggVariantGroupId: vm.delAggVariantGroupId,
                    menuItemPortionId: vm.delaggvariant.menuItemPortionId
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getAll();
        }]);
})();

