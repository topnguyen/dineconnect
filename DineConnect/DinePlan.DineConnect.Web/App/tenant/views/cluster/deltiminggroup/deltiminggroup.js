﻿
(function () {
    appModule.controller('tenant.views.cluster.deltiminggroup.deltiminggroup', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.delTimingGroup', "abp.services.app.connectLookup",
        function ($scope, $state, $stateParams, $modal, deltiminggroupService, lookupService) {
            var vm = this;
            
            vm.saving = false;
            vm.deltiminggroup = null;

            deltiminggroupId = $stateParams.id;
            $scope.existall = true;
            vm.sno = 0;
            vm.loading = false;

            $scope.settings = {
                dropdownToggleState: false,
                time: {
                    fromHour: '06',
                    fromMinute: '30',
                    toHour: '23',
                    toMinute: '30'
                },
                noRange: false,
                format: 24,
                noValidation: true
            };

            vm.removeSchedule = function (productIndex) {
                vm.deltiminggroup.delTimingDetails.splice(productIndex, 1);
            };
            vm.refday = [];
            vm.addSchedule = function () {
                vm.deltiminggroup.delTimingDetails.push({
                    'startHour': $scope.settings.time.fromHour,
                    'endHour': $scope.settings.time.toHour,
                    'startMinute': $scope.settings.time.fromMinute,
                    'endMinute': $scope.settings.time.toMinute,
                    'allDays': vm.refday
                });

                vm.refday = [];
            };
            vm.save = function () {
                vm.saving = true;
                deltiminggroupService.createOrUpdateDelTimingGroup({
                    delTimingGroup: vm.deltiminggroup
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go("tenant.deltiminggroup");
            };

            vm.refdays = [];
            function fillDays() {
                vm.loading = true;
                lookupService.getDays({}).success(function (result) {
                    vm.refdays = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            }


            function init() {
                fillDays();
                deltiminggroupService.getDelTimingGroupForEdit({
                    id: deltiminggroupId
                }).success(function (result) {
                    vm.deltiminggroup = result.delTimingGroup;
                });
            }
            init();
        }
    ]);
})();

