﻿
(function () {
    appModule.controller('tenant.views.cluster.delaggcategory.delaggcategory', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.delAggCategory', 'abp.services.app.delTimingGroup',
        function ($scope, $state, $stateParams, $modal,delaggcategoryService, deltiminggroupService) {
            var vm = this;
            
            vm.saving = false;
            vm.delaggcategory = null;
            delaggcategoryId = $stateParams.id;
            vm.save = function () {
                vm.saving = true;
                delaggcategoryService.createOrUpdateDelAggCategory({
                    delAggCategory: vm.delaggcategory
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $state.go('tenant.virtualdelaggcategory');
            };

            vm.timingGroups = [];
            function fillDropDownTimingGroup() {
                deltiminggroupService.getTimingGroupNames({ maxResultCount: 1000 }).success(function (result) {
                    vm.timingGroups = result.items;
                });
            }

            function init() {
                fillDropDownTimingGroup();
                delaggcategoryService.getDelAggCategoryForEdit({
                    id: delaggcategoryId
                }).success(function (result) {
                    vm.delaggcategory = result.delAggCategory;
                });
            }
            init();
            vm.openLanguageDescriptionModal = function () {

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/cluster/clusterlanguageDescription/languageDescriptionModal.cshtml",
                    controller: "tenant.views.cluster.clusterlanguageDescription.languageDescriptionModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        delAggLanguage: function () {
                            return {
                                id: vm.delaggcategory.id,
                                language: vm.delaggcategory.name,
                                languageDescriptionType: 2
                            };
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });

            };
        }
    ]);
})();

