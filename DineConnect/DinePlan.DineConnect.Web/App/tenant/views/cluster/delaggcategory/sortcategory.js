﻿(function() {
    appModule.controller("tenant.views.cluster.delaggcategory.sortcategory", [
        "$scope", "$uibModalInstance",  'abp.services.app.delAggCategory',
        function ($scope, $modalInstance,  delaggcategoryService) {

            var vm = this;
            vm.saving = false;

            vm.init = function () {
                vm.loading = true;
                delaggcategoryService.getDelAggCategories({
                    operation: 'All'
                }).success(function (result) {
                    vm.category=result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.save = function () {
                vm.loading = true;
                vm.cateIds = [];
                angular.forEach(vm.category, function(item) {
                    vm.cateIds.push(item.id);
                });
                delaggcategoryService.saveSortDelAggCategories(
                   vm.cateIds
                ).success(function (result) {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.cancel = function() {
                $modalInstance.dismiss();
            };

            vm.init();

        }
    ]);
})();