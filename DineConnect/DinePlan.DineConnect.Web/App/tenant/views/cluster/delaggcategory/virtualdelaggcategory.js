﻿(function () {
    appModule.controller('tenant.views.cluster.delaggcategory.virtualdelaggcategory', [
        "$scope", '$window', "$state", "$stateParams", "$uibModal", '$sce', 'abp.services.app.delAggCategory',
        function ($scope, $window, $state, $stateParams, $modal, $sce, delaggcategoryService) {
            var vm = this;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.spanFlag = false;

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Cluster.DelAggCategory.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Cluster.DelAggCategory.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Cluster.DelAggCategory.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.delAggCategoryId = null;
            if (!vm.isUndefinedOrNull($stateParams.delAggCategoryId))
                vm.delAggCategoryId = $stateParams.delAggCategoryId;

        
            vm.subItems = [];
            vm.categoryTags = [];
            vm.currentTag = '';
            vm.backButtonTag = '';
            vm.backButtonCaption = '';

            vm.currentCate = null;
            vm.loadItem = function (cate) {
                vm.loadingItem = true;
                var colCount = cate.categoryColumnCount === 0 ? 3 : cate.categoryColumnCount;

                vm.itemWidth = 100 / colCount + '%';
                vm.itemHeight = cate.itemButtonHeight + 'px';
                vm.itemWrapText = cate.wrapText;

                vm.currentCate = cate;
                vm.getItemsForCategoryTag('');
            };

            vm.getItemsForCategoryTag = function (tag) {
                delaggcategoryService.getItemForVisualScreen({
                    delAggCategoryId: vm.currentCate.id,
                    skipCount: 0,
                    maxResultCount: 1,
                    currentTag: tag
                }).success(function (result) {
                    vm.subItems = result.delAggItems;
                    vm.spanFlag = true;
                }).finally(function () {
                    vm.currentTag = tag;
                    vm.backButtonTag = tag.replace(tag.split(',').pop(), '').slice(0, -1).trim();
                    vm.backButtonCaption = vm.backButtonTag.split(',').pop().trim();
                });
            };

            vm.getCateName = function (item) {
                return $sce.trustAsHtml(item.name.replace("\n", "<br/>").replace("/r", "<br/>").replace("\r", "<br/>"));
            };

            vm.getItemName = function (name) {
                name = name.replace("\n", "<br/>").replace("/r", "<br/>").replace("\r", "<br/>");
                if (vm.itemWrapText)
                    return $sce.trustAsHtml(name.replace(" ", "<br/>"));
                return $sce.trustAsHtml(name);
            };

            vm.getTagName = function (name) {
                name = name.split(',').pop().trim();
                return $sce.trustAsHtml(name);
            };

            vm.editCategory = function (myObj) {
                $state.go('tenant.delaggcategorycreate', {
                    id: myObj.id
                });
            };
            vm.init = function () {
                init();
            }

            vm.createCategory = function (objId) {
                $state.go("tenant.delaggcategorycreate", {
                    id: objId
                });
            };

            vm.createItem = function (myObj) {
                $state.go('tenant.delaggitemdetail', {
                    id: null,
                    delAggCatId: myObj.id,
                    categoryFlag: true
                });
            };

            vm.sortCategory = function (objId) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/cluster/delaggcategory/sortcategory.cshtml",
                    controller: "tenant.views.cluster.delaggcategory.sortcategory as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });
            };

            function init() {
                vm.loading = true;
                delaggcategoryService.getDelAggCategories({
                    operation: 'All'
                }).success(function (result) {
                    vm.categories = result.items;
                    var argSelectedCategory = null;
                    angular.forEach(vm.categories, function (value, key) {
                        if (value.id == vm.delAggCategoryId) {
                            argSelectedCategory = value;
                        }
                        var colCount = value.categoryColumnCount === 0 ? 1 : value.categoryColumnCount;
                        vm.cateWidth = 100 / colCount + '%';
                    });

                    if (argSelectedCategory != null)
                        vm.loadItem(argSelectedCategory)
                }).finally(function () {
                    vm.loading = false;
                });
            }

            init();

        }
    ]);
})();