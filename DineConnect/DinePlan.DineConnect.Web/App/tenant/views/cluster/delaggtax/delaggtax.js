﻿(function () {
    appModule.controller('tenant.views.cluster.delaggtax.delaggtax', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'abp.services.app.delAggTax', 'abp.services.app.delAggLocationGroup', 'uiGridConstants', 'abp.services.app.delAggItemGroup',
        function ($scope, $filter, $state, $stateParams, $modal, delaggtaxService, delagglocationgroupService, uiGridConstants, delaggitemgroupService) {
            var vm = this;
            vm.saving = false;
            vm.delAggTax = null;
            delAggTaxId = $stateParams.id;
            $scope.existall = true;
            vm.delAggTaxMapping = [];
            vm.sno = 0;
            vm.loading = false;
            

            vm.addPortion=function()
            {
                vm.sno = vm.sno + 1;
                vm.delAggTaxMapping.push({
                    'delAggTaxId': 0,
                    'delAggLocationGroupId': '',
                    'delItemGroupId': '',
                });
            }



            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex) {
                var element = vm.delAggTaxMapping[productIndex];

                if (vm.delAggTaxMapping.length > 1) {
                    vm.delAggTaxMapping.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;

                }
                else {
                    vm.delAggTaxMapping.splice(productIndex, 1);
                    vm.sno = 0;
                    return;
                }
            }

            vm.save = function () {
                if ($scope.existall == false)
                    return;
                $scope.errmessage = "";

                if (parseFloat(vm.delAggTax.percentage)>100)
                    $scope.errmessage = $scope.errmessage + app.localize('PercentageErr', vm.delAggTax.taxName);

                if ($scope.errmessage != "") {
                    abp.notify.warn($scope.errmessage, app.localize('RequiredFields'));
                    return;
                }

                //  Setting up delAggTax Method 

                vm.saving = true;
                vm.delAggTax.name = vm.delAggTax.name.toUpperCase();

                delaggtaxService.createOrUpdateDelAggTax({
                    delAggTax: vm.delAggTax,
                    delAggTaxMapping : vm.delAggTaxMapping
                }).success(function () {
                    abp.notify.info('\' delAggTax \'' + app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {
                if (vm.delAggTax.name == null) {
                    vm.existall = false;
                    return;
                }
                delaggtaxService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'taxName',
                    filter: vm.delAggTax.name,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.delAggTax.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.delAggTax.name + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $state.go('tenant.delaggtax');
            };

            vm.delAggTaxTypes = [];
            function fillDropDownDelAggType() {
                delaggtaxService.getDelAggTaxTypeForCombobox({}).success(function (result) {
                    vm.delAggTaxTypes = result.items;
                });
            }

            vm.reflocationgroup = [];
            function fillDropDownLocationGroup() {
                delagglocationgroupService.getNames({}).success(function (result) {
                    vm.reflocationgroup = result.items;
                });
            }

            vm.refitemgroup = [];
            function fillDropDownItemGroup() {
                delaggitemgroupService.getNames({}).success(function (result) {
                    vm.refitemgroup = result.items;
                });
            }


            function init() {
                fillDropDownDelAggType();
                fillDropDownLocationGroup();
                fillDropDownItemGroup();

                delaggtaxService.getDelAggTaxForEdit({
                    Id: delAggTaxId
                }).success(function (result) {
                    vm.delAggTax = result.delAggTax;
                    vm.delAggTaxMapping = result.delAggTaxMapping;

                    if (vm.delAggTax.id == null) {
                        vm.delAggTax.percentage = "";
                        vm.delAggTax.taxTypeId = null;
                    }
                    else {
                    }
                });
            }

            init();
        }
    ]);
})();

    