﻿
(function () {
    appModule.controller('tenant.views.cluster.delaggtax.indexdelaggtax', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.delAggTax',
        function ($scope, $state, $modal, uiGridConstants, delaggtaxService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggTax.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggTax.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggTax.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
					{
					    name: app.localize('Actions'),
					    enableSorting: false,
					    width: 120,

					    cellTemplate:
						   "<div class=\"ui-grid-cell-contents\">" +
							    "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
							   "    <ul uib-dropdown-menu>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editDelAggTax(row.entity)\">" + app.localize("Edit") + "</a></li>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteDelAggTax(row.entity)\">" + app.localize("Delete") + "</a></li>" +
							   "    </ul>" +
							   "  </div>" +
							   "</div>"
					},
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    }, {
                        name: app.localize('TaxType'),
                        field: 'taxTypeName'
                    },
                    {
                        name: app.localize('Percentage'),
                        field: 'taxPercentage',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('LocalRefCode'),
                        field: 'localRefCode',
                        cellClass: 'ui-ralign'
                    },

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.delAggTaxTypes = [];
            function fillDropDownDelAggType() {
                delaggtaxService.getDelAggTaxTypeForCombobox({}).success(function (result) {
                    vm.delAggTaxTypes = result.items;
                });
            }
            fillDropDownDelAggType();
            vm.getAllClear = function () {
                vm.filterText = null;
                vm.taxTypeId = null;
            }

            vm.getAll = function () {
                vm.loading = true;
                delaggtaxService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    taxTypeId: vm.taxTypeId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editDelAggTax = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createDelAggTax = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteDelAggTax = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteTaxWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            delaggtaxService.deleteDelAggTax({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.delaggtaxdetail", {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                vm.loading = true;
                delaggtaxService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            };


            vm.getAll();
        }]);
})();

