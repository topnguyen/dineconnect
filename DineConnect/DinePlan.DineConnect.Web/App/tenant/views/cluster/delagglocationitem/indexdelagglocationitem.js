﻿
(function () {
    appModule.controller('tenant.views.cluster.delagglocationitem.indexdelagglocationitem', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.delAggLocationItem', 'abp.services.app.delAggItem', 'abp.services.app.delAggLocMapping', 'abp.services.app.delAggVariant', 'abp.services.app.delAggModifier',
        function ($scope, $state, $modal, uiGridConstants, delagglocationitemService, delaggitemService, delagglocmappingService, delaggvariantService, delaggmodifierService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggLocationItem.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggLocationItem.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggLocationItem.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.editDelAggLocationItem(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.permissions.delete" ng-click="grid.appScope.deleteDelAggLocationItem(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('DelAggLocMapping'),
                        field: 'delAggLocMappingName'
                    },
                    {
                        name: app.localize('DelAggItem'),
                        field: 'delAggItemName'
                    },
                    {
                        name: app.localize('DelAggVariant'),
                        field: 'delAggVariantName'
                    },
                    {
                        name: app.localize('DelAggModifier'),
                        field: 'delAggModifierName'
                    },
                    {
                        name: app.localize('Price'),
                        field: 'price'
                    },
                    {
                        name: app.localize('DelPriceType'),
                        field: 'delAggPriceTypeRefName'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };
            vm.getAllClear = function () {
                vm.loading = true;
                vm.filterText = null;
                vm.delAggLocMappingId = null;
                vm.delAggItemId = null;
                vm.delAggVariantId = null;
                vm.delAggModifierId = null;
                vm.delAggPriceTypeRefI = null;
            }
         

            vm.getAll = function () {
                vm.loading = true;
                delagglocationitemService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    delAggLocMappingId: vm.delAggLocMappingId,
                    delAggItemId:vm.delAggItemId,
                    delAggVariantId: vm.delAggVariantId,
                    delAggModifierId:vm.delAggModifierId,
                    delAggPriceTypeRefId: vm.delAggPriceTypeRefId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editDelAggLocationItem = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createDelAggLocationItem = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteDelAggLocationItem = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteDelAggLocationItemWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            delagglocationitemService.deleteDelAggLocationItem({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                //var modalInstance = $modal.open({
                //    templateUrl: '~/App/tenant/views/cluster/delagglocationitem/delagglocationitem.cshtml',
                //    controller: 'tenant.views.cluster.delagglocationitem.delagglocationitem as vm',
                //    backdrop: 'static',
                //    resolve: {
                //        delagglocationitemId: function () {
                //            return objId;
                //        }
                //    }
                //});

                //modalInstance.result.then(function (result) {
                //    vm.getAll();
                //});
                $state.go("tenant.delagglocationitemcreate", {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                delagglocationitemService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.refitem = [];
            function fillDropDownItem() {
                delaggitemService.getNames({}).success(function (result) {
                    vm.refitem = result.items;
                });
            }
            vm.reflocmap = [];
            function fillDropDownLocMap() {
                delagglocmappingService.getAggLocationMapNames({}).success(function (result) {
                    vm.reflocmap = result.items;
                });
            }
            vm.refvariant = [];
            function fillDropDownVariant() {
                delaggvariantService.getNames({}).success(function (result) {
                    vm.refvariant = result.items;
                });
            }
            vm.refmodifier = [];
            function fillDropDownModifier() {
                delaggmodifierService.getNames({}).success(function (result) {
                    vm.refmodifier = result.items;
                });
            }
            vm.delpriceTypes = [];
            function fillDropDownDelPriceType() {
                delagglocmappingService.getDelPriceTypeForCombobox({}).success(function (result) {
                    vm.delpriceTypes = result.items;
                });
            }

            fillDropDownItem();
            fillDropDownLocMap();
            fillDropDownVariant();
            fillDropDownModifier();
            fillDropDownDelPriceType();

            vm.getAll();
        }]);
})();

