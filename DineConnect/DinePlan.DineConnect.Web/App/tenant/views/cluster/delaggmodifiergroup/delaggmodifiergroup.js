﻿
(function () {
    appModule.controller('tenant.views.cluster.delaggmodifiergroup.delaggmodifiergroup', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'abp.services.app.delAggModifierGroup', 'abp.services.app.delAggModifier', 'abp.services.app.orderTagGroup', 'abp.services.app.delAggItem', 'abp.services.app.delAggLocMapping', 'FileUploader',
        function ($scope, $filter, $state, $stateParams, $modal, delaggmodifiergroupService, delaggmodifierService,  ordertaggroupService, delaggitemService, delagglocmappingService, fileUploader) {
            /* eslint-disable */
            var vm = this;
            
            vm.saving = false;
            vm.delaggmodifiergroup = null;
            delaggmodifiergroupId = $stateParams.id;
            $scope.existall = true;
            vm.delaggmodifiergroupitem = [];
            vm.sno = 0;
            vm.loading = false;
            vm.delaggimage = [];
            vm.imagePath = [];


            vm.addPortion = function () {
                vm.sno = vm.sno + 1;
                vm.delaggmodifiergroupitem.push({
                    'delAggModifierGroupId': 0,
                    'delAggItemGroupId': '',
                });
            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex) {
                var element = vm.delaggmodifiergroupitem[productIndex];

                if (vm.delaggmodifiergroupitem.length > 1) {
                    vm.delaggmodifiergroupitem.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;

                }
                else {
                    vm.delaggmodifiergroupitem.splice(productIndex, 1);
                    vm.sno = 0;
                    return;
                }
            }

            vm.save = function () {
                if (vm.imagePath.length > 0) {
                    abp.notify.info("Unsaved Data in ImagePath");
                    abp.message.error("Unsaved Data in ImagePath");
                    return true;
                }
                vm.saving = true;
                delaggmodifiergroupService.createOrUpdateDelAggModifierGroup({
                    delAggModifierGroup: vm.delaggmodifiergroup,
                    delAggImage: vm.delaggimage,
                    delAggModifierGroupItems: vm.delaggmodifiergroupitem
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $state.go('tenant.delaggmodifiergroup');
            };

            vm.ordertaggroups = [];
            function fillDropDownOrderTagGroup() {
                ordertaggroupService.getOrderTagGroupNames({}).success(function (result) {
                    vm.ordertaggroups = result.items;
                });
            }

            vm.refitem = [];
            function fillDropDownItem() {
                delaggitemService.getNames({}).success(function (result) {
                    vm.refitem = result.items;
                    init();
                });
            }

            vm.addModifier = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/cluster/delaggmodifiergroup/delaggmodifier.cshtml",
                    controller: "tenant.views.cluster.delaggmodifiergroup.delaggmodifier as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        groupId: function () {
                            return vm.delaggmodifiergroup.id;
                        },
                        modifierId: null
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.delaggmodifierItems.push(result);
                });
            };

            vm.editModifier = function (modifierId) {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/cluster/delaggmodifiergroup/delaggmodifier.cshtml",
                    controller: "tenant.views.cluster.delaggmodifiergroup.delaggmodifier as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        groupId: function () {
                            return vm.delaggmodifiergroup.id;
                        },
                        modifierId: modifierId
                    }
                });
            };

            vm.deleteModifier = function (modifier) {
                abp.message.confirm(
                    app.localize('DeleteDelAggModifierWarning', modifier.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            delaggmodifierService.deleteDelAggModifier({
                                id: modifier.id
                            }).success(function () {
                                vm.delaggmodifierItems = vm.delaggmodifierItems.filter(x => x.id != modifier.id);
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function init() {
                fillDropDownOrderTagGroup();
                delaggmodifiergroupService.getDelAggModifierGroupForEdit({
                    id: delaggmodifiergroupId
                }).success(function (result) {
                    vm.delaggmodifiergroup = result.delAggModifierGroup;
                    vm.delaggimage = result.delAggImage;
                    vm.delaggmodifiergroupitem = result.delAggModifierGroupItem;
                    angular.forEach(vm.delaggimage, function (delImage, delkey) {
                        vm.delAggTypes.some(function (value, key) {
                            if (delImage.delAggTypeRefId == null) {
                                delImage.delAggTypeRefName = app.localize("Default");
                                return true;
                            }
                            else if (value.value == delImage.delAggTypeRefId) {
                                delImage.delAggTypeRefName = value.displayText;
                                return true;
                            }
                        });
                        delImage.displayImagePath = delImage.imagePath ? angular.fromJson(delImage.imagePath) : [];
                    });

                    delaggmodifierService.getAll({
                        delAggModifierGroupId: vm.delaggmodifiergroup.id
                    }).success(function (result) {
                        vm.delaggmodifierItems = result.items;
                    });

                });

            }

            fillDropDownItem();

            //image
            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            var img = [];
            vm.addImagePortion = [];
            fillDropDownDelAggType();
            vm.addImage = function (argsDelAggTypeRefId, argImgPath) {
                var delAggTypeRefName = '';
                // Check already null exists
                var argErrorFlag = false;
                if (vm.isUndefinedOrNull(argsDelAggTypeRefId)) {
                    vm.delaggimage.some(function (delImage, key) {
                        if (vm.isUndefinedOrNull(delImage.delAggTypeRefId)) {
                            abp.notify.error(app.localize("AlreadyDefaultImageUploadedError"));
                            argErrorFlag = true;
                            return true;
                        }
                    });
                    if (argErrorFlag == true)
                        return;
                }

                vm.delAggTypes.some(function (value, key) {
                    if (argsDelAggTypeRefId == null) {
                        delAggTypeRefName = app.localize("Default");
                    }
                    else if (value.value == argsDelAggTypeRefId) {
                        delAggTypeRefName = value.displayText;
                        return true;
                    }
                });
                angular.forEach(argImgPath, function (value, key) {
                    img = {
                        'delAggTypeRefId': argsDelAggTypeRefId,
                        'delAggTypeRefName': delAggTypeRefName,
                        'imagePath': angular.toJson(value)
                    }
                    vm.delaggimage.push(img);
                });
                angular.forEach(vm.delaggimage, function (delImage, delkey) {
                    delImage.displayImagePath = delImage.imagePath ? angular.fromJson(delImage.imagePath) : [];
                });
                vm.delaggimage.delAggTypeRefId = null;
                vm.imagePath = [];
            }

            vm.delAggTypes = [];
            function fillDropDownDelAggType() {
                delagglocmappingService.getDelAggTypeForCombobox({}).success(function (result) {
                    vm.delAggTypes = result.items;
                    init();
                });
            }

            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadFile'
            });

            vm.uploader.filters.push({
                name: 'imageFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    return true;
                }
            });

            vm.uploader.onBeforeUploadItem = function (fileitem) {
                fileitem.formData.push({ fileTag: vm.fileTag });
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response !== null) {
                    if (response.result.fileTag == 'Image') {
                        if (response.result !== null) {
                            if (response.result.fileName !== null) {
                                vm.imagePath.push(response.result);
                                vm.refreshImage = true;
                                if (!vm.isUpload) {
                                    vm.save();
                                }
                            }
                        }
                    }
                    $("#imageFile").val(null);
                }
            };

            vm.viewer = function (file) {
                var fileName = angular.fromJson(file.imagePath);
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + fileName.fileName + '&url=' + fileName.fileSystemName;
            };

            vm.downloader = function (file) {
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + file.fileName + '&url=' + file.fileSystemName;
            };
            vm.deleteImage = function (index) {
                vm.delaggimage.splice(index, 1);
            }

            vm.removeFile = function (findex) {
                if (vm.fileTag == 'Image') {
                    vm.imagePath.splice(findex, 1);
                }

                if (vm.imagePath.length > 0)
                    $scope.btnStatus = true;
                else
                    $scope.btnStatus = false;
                vm.refreshImage = true;
            };
            $scope.btnStatus = false;   
            vm.uploadFile = function (isuload) {
                $scope.btnStatus = true;
                vm.isUpload = isuload;
                var file = vm.uploader.queue[vm.uploader.queue.length - 1];
                file.upload();
            };
            vm.openLanguageDescriptionModal = function () {

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/cluster/clusterlanguageDescription/languageDescriptionModal.cshtml",
                    controller: "tenant.views.cluster.clusterlanguageDescription.languageDescriptionModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        delAggLanguage: function () {
                            return {
                                id: vm.delaggmodifiergroup.id,
                                language: vm.delaggmodifiergroup.name,
                                languageDescriptionType: 6
                            };
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });

            };
        }
    ]);
})();

