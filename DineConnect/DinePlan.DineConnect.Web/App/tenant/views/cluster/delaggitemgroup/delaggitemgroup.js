﻿
(function () {
    appModule.controller('tenant.views.cluster.delaggitemgroup.delaggitemgroup', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.delAggItemGroup',
        function ($scope, $state, $stateParams, $modal, delaggitemgroupService) {
            var vm = this;
            
            vm.saving = false;
            vm.delaggitemgroup = null;
            delaggitemgroupId = $stateParams.id;
            vm.save = function () {
                vm.saving = true;
                delaggitemgroupService.createOrUpdateDelAggItemGroup({
                    delAggItemGroup: vm.delaggitemgroup
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel ();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $state.go('tenant.delaggitemgroup');
            };
            function init() {
                delaggitemgroupService.getDelAggItemGroupForEdit({
                    id: delaggitemgroupId
                }).success(function (result) {
                    vm.delaggitemgroup = result.delAggItemGroup;
                });
            }
            init();
            vm.openLanguageDescriptionModal = function () {

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/cluster/clusterlanguageDescription/languageDescriptionModal.cshtml",
                    controller: "tenant.views.cluster.clusterlanguageDescription.languageDescriptionModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        delAggLanguage: function () {
                            return {
                                id: vm.delaggitemgroup.id,
                                language: vm.delaggitemgroup.name,
                                languageDescriptionType: 1
                            };
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });

            };
        }
    ]);
})();

