﻿
(function () {
    appModule.controller('tenant.views.cluster.delaggcharge.delaggcharge', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'abp.services.app.delAggCharge', 'abp.services.app.delAggLocationGroup', 'uiGridConstants', 'abp.services.app.delAggItemGroup',
        function ($scope, $filter, $state, $stateParams, $modal, delaggchargeService, delagglocationgroupService, uiGridConstants, delaggitemgroupService) {
            var vm = this;
            
            vm.saving = false;
            vm.delaggcharge = null;
            delaggchargeId = $stateParams.id;
            $scope.existall = true;
            vm.delaggchargemapping = [];
            vm.sno = 0;
            vm.loading = false;

            vm.addPortion = function () {
                vm.sno = vm.sno + 1;
                vm.delaggchargemapping.push({
                    'delAggChargeId': 0,
                    'delAggLocationGroupId': '',
                    'delItemGroupId': '',
                });
            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex) {
                var element = vm.delaggchargemapping[productIndex];

                if (vm.delaggchargemapping.length > 1) {
                    vm.delaggchargemapping.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;

                }
                else {
                    vm.delaggchargemapping.splice(productIndex, 1);
                    vm.sno = 0;
                    return;
                }
            }
            vm.save = function () {
                vm.saving = true;
                delaggchargeService.createOrUpdateDelAggCharge({
                    delAggCharge: vm.delaggcharge,
                    delAggChargeMapping: vm.delaggchargemapping
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.delaggcharge');
            };

            vm.reflocationgroup = [];
            function fillDropDownLocationGroup() {
                delagglocationgroupService.getNames({}).success(function (result) {
                    vm.reflocationgroup = result.items;
                });
            }

            vm.refitemgroup = [];
            function fillDropDownItemGroup() {
                delaggitemgroupService.getNames({}).success(function (result) {
                    vm.refitemgroup = result.items;
                });
            }

            function init() {
                fillDropDownLocationGroup();
                fillDropDownItemGroup();
                delaggchargeService.getDelAggChargeForEdit({
                    id: delaggchargeId
                }).success(function (result) {
                    vm.delaggcharge = result.delAggCharge;
                    vm.delaggchargemapping = result.delAggChargeMapping;
                });
            }
            init();
        }
    ]);
})();

