﻿
(function () {
    appModule.controller('tenant.views.cluster.clusterlanguageDescription.languageDescriptionModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.delAggLanguage', 'abp.services.app.language',  'uiGridConstants','delAggLanguage',
        function ($scope, $modalInstance, delAggLanguageService, languageService, uiGridConstants, delAggLanguage) {

            var vm = this;
            vm.saving = false;
            vm.delAggLanguage = null;

            vm.id = delAggLanguage.id;
            vm.language = delAggLanguage.language;
            vm.delAggLanguageType = delAggLanguage.delAggLanguageType;

            vm.delAggLanguageId = null;

            vm.save = function (argSaveOption) {
                vm.saving = true;
                vm.delAggLanguage.referenceId = vm.id;
                vm.delAggLanguage.languageDescriptionType = vm.languageDescriptionType;
                delAggLanguageService.createOrUpdateDelAggLanguage({
                    delAggLanguage: vm.delAggLanguage
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    if (argSaveOption == 2) {
                        $modalInstance.close();
                        vm.saving = false;
                        return;
                    }
                    vm.getAll();
                    vm.delAggLanguageId = null;
                    init();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
                vm.getAll();
            };

            vm.clear = function () {
                vm.delAggLanguage = null;
                vm.filterText = null;
            };
            //  Index File
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;


            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Delete'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.deletedelAggLanguage(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>' +
                            '</div>',
                        width: 50
                    },
                    {
                        name: app.localize('Edit'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.editdelAggLanguage(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>' +
                            '</div>',
                        width: 50
                    },
                   
                    {
                        name: app.localize('Language'),
                        field: 'language'
                    },
                    {
                        name: app.localize('LanguageValue'),
                        field: 'languageValue',
                        width: 100
                    },
                    
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };



            vm.getAll = function () {
                vm.loading = true;
                delAggLanguageService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    id: vm.id,
                    languageDescriptionType: vm.languageDescriptionType
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editdelAggLanguage = function (myObj) {
                vm.delAggLanguageId = myObj.id;
                init();

            };

            vm.deletedelAggLanguage = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteDelAggLanguageWarning', myObject.language),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            delAggLanguageService.deleteDelAggLanguage({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            
            function fillDropDowndelAggLanguageType() {
                delAggLanguageService.getLanguageDescriptionTypes({
                }).success(function (result) {
                    vm.refdelAggLanguage = result.items;
                }).finally(function () {

                });
            }
            function fillDropDownLanguageCode() {
                languageService.getLanguages({
                }).success(function (result) {
                    console.log(result);
                    vm.reflanguageCode = result.items;
                }).finally(function () {

                });
            }
            fillDropDowndelAggLanguageType();
            fillDropDownLanguageCode();
            vm.getAll();

            function init() {

                delAggLanguageService.getDelAggLanguageForEdit({
                    id: vm.delAggLanguageId
                }).success(function (result) {
                    console.log(result);
                    vm.delAggLanguage = result.delAggLanguage;
                }).finally(function () {

                });
            }
            init();

        }
    ]);
})();

