﻿
(function () {
    appModule.controller('tenant.views.cluster.delagglocationitem.delagglocationitem1', [
        '$scope', '$filter', '$state', '$stateParams', '$uibModal', 'abp.services.app.delAggLocationItem', 'abp.services.app.delAggItem', 'abp.services.app.delAggLocMapping', 'abp.services.app.delAggVariant', 'abp.services.app.delAggModifier',
        function ($scope, $filter, $state, $stateParams, $modal, delagglocationitemService, delaggitemService, delagglocmappingService, delaggvariantService, delaggmodifierService) {
            var vm = this;
            
            vm.saving = false;
            vm.delagglocationitem = null;
            delagglocationitemId = $stateParams.id;
            vm.save = function () {
                vm.saving = true;
                vm.delagglocationitem.delAggPriceTypeRefId = vm.delpriceTypes[0];
                delagglocationitemService.createOrUpdateDelAggLocationItem({
                    delAggLocationItem: vm.delagglocationitem
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $state.go('tenant.delagglocationitem1');
            };
            vm.refitem = [];
            function fillDropDownItem() {
                delaggitemService.getNames({}).success(function (result) {
                    vm.refitem = result.items;
                });
            }
            vm.reflocmap = [];
            function fillDropDownLocMap() {
                delagglocmappingService.getAggLocationMapNames({}).success(function (result) {
                    vm.reflocmap = result.items;
                });
            }
            vm.refvariant = [];
            function fillDropDownVariant() {
                delaggvariantService.getNames({}).success(function (result) {
                    vm.refvariant = result.items;
                });
            }
            vm.refmodifier = [];
            function fillDropDownModifier() {
                delaggmodifierService.getNames({}).success(function (result) {
                    vm.refmodifier = result.items;
                });
            }
            vm.delpriceTypes = [];
            function fillDropDownDelPriceType() {
                delagglocmappingService.getDelPriceTypeForCombobox({}).success(function (result) {
                    vm.delpriceTypes = result.items;
                });
            }
            function init() {
                fillDropDownItem();
                fillDropDownLocMap();
                fillDropDownVariant();
                fillDropDownModifier();
                fillDropDownDelPriceType();
                delagglocationitemService.getDelAggLocationItemForEdit({
                    id: delagglocationitemId
                }).success(function (result) {
                    vm.delagglocationitem = result.delAggLocationItem;
                    if (vm.delagglocationitem.id == null) {
                        vm.delagglocationitem.delAggPriceTypeRefId = null;
                    }
                });
            }
            init();

            vm.openforItem = function (data) {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    menuItemId: data.delAggItemId,
                    service: delaggitemService.getNames
                };

                const modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        data.delAggItemId= result.locations[0].id;
                        data.delAggItemName = result.locations[0].name;
                    }
                });
            };
        }
    ]);
})();

