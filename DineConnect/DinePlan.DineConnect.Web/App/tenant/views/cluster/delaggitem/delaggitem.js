﻿(function () {
    appModule.controller('tenant.views.cluster.delaggitem.delaggitem', [
        '$scope', '$uibModal', '$state', '$stateParams', 'abp.services.app.delAggItem', "abp.services.app.menuItem", 'abp.services.app.delAggItemGroup', 'abp.services.app.delAggTax', 'abp.services.app.delAggCategory', 'abp.services.app.delAggLocMapping', 'FileUploader', 'abp.services.app.delAggModifierGroup',
        function ($scope, $modal, $state, $stateParams, delaggitemService, menuService, delaggitemgroupService, delaggtaxService, delaggcategoryService, delagglocmappingService, fileUploader, delaggmodifiergroupService) {
            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            vm.delaggitem = null;
            vm.delAggVariants = [];
            delaggitemId = $stateParams.id;
            vm.delAggCatId = $stateParams.delAggCatId;
            vm.categoryFlag = $stateParams.categoryFlag;
            
            vm.delAggVariantGroup = null;
            vm.sno = 0;
            var portionLengthFlag;
            vm.portionLength = null;
            vm.delaggimage = [];
            vm.imagePath = [];
            vm.delaggmodifiergroupitemids = [];
         

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.addPortion = function () {
                vm.sno = vm.sno + 1;
                vm.delAggVariants.push({
                    'name': null,
                    'code': null,
                    'delAggVariantGroupId': 0,
                    'menuItemId': vm.delaggitem.menuItemId,
                    'menuItemPortionId': 0,
                    'portionName': null,
                    'salesPrice': 0,
                    'markupPrice': 0,
                    'sortOrder': 1,
                });
                //} else {
                //    abp.notify.info(app.localize('Only one MenuItemPortion is Linked this MenuItem'));
                //    return true;
                //}
            }

            vm.editImage = function (item) {
                vm.delaggimage.delAggTypeRefId = item.delAggTypeRefId;
                vm.imagePath = item.imagePath;
            }

            vm.deleteImage = function (index) {
                vm.delaggimage.splice(index, 1);
            }

            vm.portionLength = function () {
                return true;
            }

            vm.removeRow = function (productIndex) {
                var element = vm.delAggVariants[productIndex];

                if (vm.delAggVariants.length > 1) {
                    vm.delAggVariants.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;

                }
                else {
                    vm.delAggVariants.splice(productIndex, 1);
                    vm.sno = 0;
                    return;
                }
            }

            vm.save = function () {
              
                if (vm.delAggVariantGroup != null) {
                    if (vm.delAggVariantGroup.id == null) {
                        vm.delAggVariantGroup.sortOrder = 1;
                    }
                }
                if (vm.imagePath.length > 0) {
                    abp.notify.info("Unsaved Data in ImagePath");
                    abp.message.error("Unsaved Data in ImagePath");
                    return true;
                }
                vm.saving = true;
                delaggitemService.createOrUpdateDelAggItem({
                    delAggItem: vm.delaggitem,
                    delAggVariantGroup: vm.delAggVariantGroup,
                    delAggVariant: vm.delAggVariants,
                    delAggImage: vm.delaggimage,
                    delAggModifierGroupItem: vm.delaggmodifiergroupitemids
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                if (vm.categoryFlag == false)
                    $state.go('tenant.delaggitem');
                else {
                    $state.go('tenant.virtualdelaggcategory', {
                        delAggCategoryId: vm.delAggCatId,
                    });
                }
                    
            };

            vm.openforMenuItem = function (data) {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    categoryId: data.categoryId,
                    service: !data.categoryId || data.categoryId == 0 ? menuService.getAllMenuItems : menuService.getAllMenuItemsBasedOnCatgeoryId
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {

                    if (result.locations.length > 0) {
                        data.menuItemId = result.locations[0].id;
                        data.menuItemName = result.locations[0].name;
                        vm.delAggVariantGroup.name = data.menuItemId + " - " + data.menuItemName;
                        vm.delAggVariants[0].menuItemPortionId = 0;
                        vm.delAggVariants[0].menuItemPortionName = null;
                    }
                });
            };

            vm.openforMenuItemPortions = function (data) {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    menuItemId: vm.delaggitem.menuItemId,
                    service: !vm.delaggitem.menuItemId || vm.delaggitem.menuItemId == 0 ? menuService.getAllMenuItemPortions : menuService.getAllMenuItemPortionsBasedOnMenuItemId
                };

                const modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length == 1) {
                        portionLengthFlag = true;
                        data.menuItemPortionId = result.locations[0].id;
                        data.menuItemPortionName = result.locations[0].name;
                        data.name = data.menuItemPortionId + " - " + data.menuItemPortionName;
                    } else {
                        portionLengthFlag = false;
                    }
                });
            };

            vm.itemgroups = [];
            function fillDropDownItemGroup() {
                delaggitemgroupService.getNames({}).success(function (result) {
                    vm.itemgroups = result.items;
                });
            }

            vm.taxes = [];
            function fillDropDownAggTax() {
                delaggtaxService.getDelAggTaxForCombobox({}).success(function (result) {
                    vm.taxes = result.items;
                });
            }

            vm.categories = [];
            function fillDropDownAggCategory() {
                delaggcategoryService.getNames({}).success(function (result) {
                    vm.categories = result.items;
                });
            }

            vm.modifiergroups = [];
            function fillDropDownModifierGroup() {
                delaggmodifiergroupService.getNames({}).success(function (result) {
                    vm.modifiergroups = result.items;
                });
            }
            

            function init() {
                fillDropDownItemGroup();
                fillDropDownAggTax();
                fillDropDownAggCategory();
                fillDropDownModifierGroup();
                delaggitemService.getDelAggItemForEdit({
                    id: delaggitemId
                }).success(function (result) {
                    vm.delaggitem = result.delAggItem;
                    vm.delAggVariantGroup = result.delAggVariantGroup;
                    vm.delAggVariants = result.delAggVariant;
                    vm.delaggimage = result.delAggImage;
                    vm.delaggmodifiergroupitemids = result.delAggModifierGroupItem;
                    if (vm.categoryFlag == true || vm.categoryFlag == 'true')
                        vm.delaggitem.delAggCatId = vm.delAggCatId;

                    angular.forEach(vm.delaggimage, function (delImage, delkey) {
                        vm.delAggTypes.some(function (value, key) {
                            if (delImage.delAggTypeRefId == null) {
                                delImage.delAggTypeRefName = app.localize("Default");
                                return true;
                            }
                            else if (value.value == delImage.delAggTypeRefId) {
                                delImage.delAggTypeRefName = value.displayText;
                                return true;
                            }
                        });
                        delImage.displayImagePath = delImage.imagePath ? angular.fromJson(delImage.imagePath) : [];
                    });

                    if (vm.delaggitem.id == null) {
                        vm.addPortion();
                    }
                    else {
                        if (vm.delAggVariants.length == 0) {
                            vm.addPortion();
                        }
                    }
                });
            }
           


            //Image
            var img = [];
            vm.addImagePortion = [];
            fillDropDownDelAggType();
            $scope.btnStatus = false;
            vm.addImage = function (argsDelAggTypeRefId, argImgPath) {
                var delAggTypeRefName = '';
                // Check already null exists
                var argErrorFlag = false;
                if (vm.isUndefinedOrNull(argsDelAggTypeRefId)) {
                    vm.delaggimage.some(function (delImage, key) {
                        if (vm.isUndefinedOrNull(delImage.delAggTypeRefId)) {
                            abp.notify.error(app.localize("AlreadyDefaultImageUploadedError"));
                            argErrorFlag = true;
                            return true;
                        }
                    });
                    if (argErrorFlag == true)
                        return;
                }

                vm.delAggTypes.some(function (value, key) {
                    if (argsDelAggTypeRefId == null) {
                        delAggTypeRefName = app.localize("Default");
                    }
                    else if (value.value == argsDelAggTypeRefId) {
                        delAggTypeRefName = value.displayText;
                        return true;
                    }
                });
                angular.forEach(argImgPath, function (value, key) {
                    img = {
                        'delAggTypeRefId': argsDelAggTypeRefId,
                        'delAggTypeRefName': delAggTypeRefName,
                        'imagePath': angular.toJson(value)
                    }
                    vm.delaggimage.push(img);
                });
                angular.forEach(vm.delaggimage, function (delImage, delkey) {
                    delImage.displayImagePath = delImage.imagePath ? angular.fromJson(delImage.imagePath) : [];
                });
                vm.delaggimage.delAggTypeRefId = null;
                 vm.imagePath = [];
            }

            vm.delAggTypes = [];
            function fillDropDownDelAggType() {
                delagglocmappingService.getDelAggTypeForCombobox({}).success(function (result) {
                    vm.delAggTypes = result.items;
                    init();
                });
            }

            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadFile'
            });

            vm.uploader.filters.push({
                name: 'imageFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    return true;
                }
            });

            vm.uploader.onBeforeUploadItem = function (fileitem) {
                fileitem.formData.push({ fileTag: vm.fileTag });
            };

            vm.uploader.onAfterAddingFile = function(item) {
                console.log(item);

                if (item.file.size > 500000) {
                    abp.notify.error(app.localize('Size is Big'));
                    vm.uploader.clearQueue();
                    return;
                }
            }

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response !== null) {
                    if (response.result.fileTag == 'Image') {
                        if (response.result !== null) {
                            if (response.result.fileName !== null) {
                                vm.imagePath.push(response.result);
                                vm.refreshImage = true;
                                if (!vm.isUpload) {
                                    vm.save();
                                }
                            }
                        }
                    }
                    $("#imageFile").val(null);
                }
            };

            vm.viewer = function (file) {
                var fileName = angular.fromJson(file.imagePath);
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + fileName.fileName + '&url=' + fileName.fileSystemName;
            };

            vm.downloader = function (file) {
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + file.fileName + '&url=' + file.fileSystemName;
            };

            vm.removeFile = function (findex) {
                if (vm.fileTag == 'Image') {
                    vm.imagePath.splice(findex, 1);
                }
                if (vm.imagePath.length > 0)
                    $scope.btnStatus = true;
                else
                    $scope.btnStatus = false;
                vm.refreshImage = true;
            };

            vm.uploadFile = function (isuload) {
                $scope.btnStatus = true;
                vm.isUpload = isuload;
                var file = vm.uploader.queue[vm.uploader.queue.length - 1];
                file.upload();
            };

            vm.openLanguageDescriptionModal = function () {

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/cluster/clusterlanguageDescription/languageDescriptionModal.cshtml",
                    controller: "tenant.views.cluster.clusterlanguageDescription.languageDescriptionModal as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        delAggLanguage: function () {
                            return {
                                id: vm.delaggitem.id,
                                language: vm.delaggitem.name,
                                languageDescriptionType: 2
                            };
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });

            };
        }
    ]);
})();