﻿
(function () {
    appModule.controller('tenant.views.cluster.master.delaggitem.indexdelaggitem', [
        '$scope', '$uibModal', '$state', 'uiGridConstants', 'abp.services.app.delAggItem', 'abp.services.app.delAggCategory', 'abp.services.app.delAggItemGroup', 'abp.services.app.delAggTax', 'abp.services.app.menuItem',
        function ($scope, $modal, $state, uiGridConstants, delaggitemService, delaggcategoryService, delaggitemgroupService, delaggtaxService, menuService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggItem.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggItem.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggItem.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.editDelAggItem(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.permissions.delete" ng-click="grid.appScope.deleteDelAggItem(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Code'),
                        field: 'code'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('FoodType'),
                        field: 'foodType'
                    },
                    {
                        name: app.localize('MenuItem'),
                        field: 'menuItemName'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };
            vm.itemgroups = [];
            function fillDropDownItemGroup() {
                delaggitemgroupService.getNames({}).success(function (result) {
                    vm.itemgroups = result.items;
                });
            }
            fillDropDownItemGroup();
            vm.categories = [];
            function fillDropDownAggCategory() {
                delaggcategoryService.getNames({}).success(function (result) {
                    vm.categories = result.items;
                });
            }
            fillDropDownAggCategory();
            vm.taxes = [];
            function fillDropDownAggTax() {
                delaggtaxService.getDelAggTaxForCombobox({}).success(function (result) {
                    vm.taxes = result.items;
                });
            }
            fillDropDownAggTax();
            vm.getAllClear = function () {
                vm.delAggCatId = null;
                vm.delAggTaxId = null;
                vm.delAggItemGroupId = null;
                vm.filterText = null;
                vm.delaggitem.menuItemId = 0;
                vm.delaggitem.menuItemPortionId = 0;
                vm.getAll();
            }
            
            vm.openHangFire = function () {
                var url = "";
                url = "http://" + window.location.host + "/hangfire";
                window.open(url);
            }
            vm.delaggitem =
            {
                'id': null,
                'categoryId': 0,
                'menuItemId': 0,
                'menuItemPortionId': 0,
            };

            vm.openforMenuItem = function (data) {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    categoryId: data.categoryId,
                    service: !data.categoryId || data.categoryId == 0 ? menuService.getAllMenuItems : menuService.getAllMenuItemsBasedOnCatgeoryId
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        data.menuItemId = result.locations[0].id;
                        data.menuItemName = result.locations[0].name;
                    }
                });
            };

            vm.openforMenuItemPortions = function (data) {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    group: false,
                    single: true,
                    menuItemId: data.menuItemId,
                    service: !data.menuItemId || data.menuItemId == 0 ? menuService.getAllMenuItemPortions : menuService.getAllMenuItemPortionsBasedOnMenuItemId
                };

                const modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        data.menuItemPortionId = result.locations[0].id;
                        data.menuItemPortionName = result.locations[0].name;
                    }
                });
            };
            vm.getAll = function () {
                vm.loading = true;
                delaggitemService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    delAggCatId: vm.delAggCatId,
                    delAggTaxId: vm.delAggTaxId,
                    delAggItemGroupId: vm.delAggItemGroupId,
                    menuItemId: vm.delaggitem.menuItemId,
                    menuItemPortionId: vm.delaggitem.menuItemPortionId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editDelAggItem = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createDelAggItem = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteDelAggItem = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteDelAggItemWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            delaggitemService.deleteDelAggItem({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.delaggitemdetail", {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                vm.loading = true;
                delaggitemService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
                vm.loading = false;
            };
          
            vm.cloneDelAggItem = function () {

                delaggitemService.initiateBackGroundClone({
                }).success(function () {
                    vm.getAll();
                });

                abp.notify.success(app.localize('StillBackGroundJobProcessing'));
            };
            vm.importDelAggItem = function () {
                importModal(null);
            };

            function importModal() {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/cluster/delaggitem/importDelAggItem.cshtml',
                    controller: 'tenant.views.cluster.delaggitem.importDelAggItem as vm',
                    backdrop: 'static',
                    keyboard: false,
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.getAll();
    }]);
})();

