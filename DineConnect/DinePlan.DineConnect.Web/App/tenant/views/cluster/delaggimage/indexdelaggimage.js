﻿
(function () {
    appModule.controller('tenant.views.cluster.delaggimage.indexdelaggimage', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.delAggImage', 'abp.services.app.delAggLocMapping',
        function ($scope, $state, $modal, uiGridConstants, delaggimageService, delagglocmappingService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Cluster.DelAggImage.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Cluster.DelAggImage.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Cluster.DelAggImage.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.editDelAggImage(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.permissions.delete" ng-click="grid.appScope.deleteDelAggImage(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('ImageType'),
                        field: 'delAggImageTypeRefName'
                    },
                    {
                        name: app.localize('AggType'),
                        field: 'delAggTypeRefName'
                    },
                    {
                        name: app.localize('Download'),
                        enableSorting: false,
                        cellTemplate:
                            '  <button type="button" class="btn btn-success btn-xs" ng-if="row.entity.imagePath.length>5" ng-click="grid.appScope.downloader(row.entity)"><i class="fa fa-download"></i>  </button>'
                    },

                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.delAggTypes = [];
            function fillDropDownDelAggType() {
                delagglocmappingService.getDelAggTypeForCombobox({}).success(function (result) {
                    vm.delAggTypes = result.items;
                });
            }
            fillDropDownDelAggType();

            vm.delAggImageTypes = [];
            function fillDropDownDelAggImageType() {
                delagglocmappingService.getDelAggImageTypeForCombobox({}).success(function (result) {
                    vm.delAggImageTypes = result.items;
                });
            }
            fillDropDownDelAggImageType();

            vm.downloader = function (argFile) {
                vm.imagePath = argFile.imagePath ? angular.fromJson(argFile.imagePath) : [];
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + vm.imagePath[0].fileName + '&url=' + vm.imagePath[0].fileSystemName;
            };

            vm.getAllClear = function () {
                vm.filterText = null;
                vm.delAggTypeRefId = null;
                vm.delAggImageTypeRefId = null;
                vm.getAll();
            }
            vm.getAll = function () {
                vm.loading = true;
                delaggimageService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    delAggImageTypeRefId: vm.delAggImageTypeRefId,
                    delAggTypeRefId: vm.delAggTypeRefId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editDelAggImage = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createDelAggImage = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteDelAggImage = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteDelAggImageWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            delaggimageService.deleteDelAggImage({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.delaggimagecreate", {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                delaggimageService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getAll();
        }]);
})();

