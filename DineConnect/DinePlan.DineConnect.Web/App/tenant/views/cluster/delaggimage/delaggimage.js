﻿
(function () {
    appModule.controller('tenant.views.cluster.delaggimage.delaggimage', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.delAggImage', 'abp.services.app.delAggLocMapping', 'FileUploader',
        function ($scope, $state, $stateParams, $modal, delaggimageService, delagglocmappingService, fileUploader) {
            var vm = this;
            
            vm.saving = false;
            vm.delaggimage = null;

            delaggimageId = $stateParams.id;
            $scope.existall = true;
            vm.sno = 0;
            vm.loading = false;
            vm.imagePath = [];

            vm.save = function () {
                vm.saving = true;
                vm.delaggimage.imagePath = angular.toJson(vm.imagePath);
                delaggimageService.createOrUpdateDelAggImage({
                    delAggImage: vm.delaggimage
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $state.go("tenant.delaggimage");
            };
            vm.delAggTypes = [];
            function fillDropDownDelAggType() {
                delagglocmappingService.getDelAggTypeForCombobox({}).success(function (result) {
                    vm.delAggTypes = result.items;
                });
            }
            vm.delAggImageTypes = [];
            function fillDropDownDelAggImageType() {
                delagglocmappingService.getDelAggImageTypeForCombobox({}).success(function (result) {
                    vm.delAggImageTypes = result.items;
                });
            }

            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadFile'
            });

            vm.uploader.filters.push({
                name: 'imageFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    return true;
                }
            });

            vm.uploader.onBeforeUploadItem = function (fileitem) {
                fileitem.formData.push({ fileTag: vm.fileTag });
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response !== null) {
                    if (response.result.fileTag == 'Image') {
                        vm.imagePath = [];
                        if (response.result !== null) {
                            if (response.result.fileName !== null) {
                                vm.imagePath.push(response.result);
                                vm.refreshImage = true;
                                if (!vm.isUpload) {
                                    vm.save();
                                }
                            }
                        }
                    }
                    $("#imageFile").val(null);
                }
            };

            vm.downloader = function (file) {
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + file.fileName + '&url=' + file.fileSystemName;
            };

            vm.removeFile = function (findex) {
                if (vm.fileTag == 'Image') {
                    vm.imagePath.splice(findex, 1);
                }

                vm.refreshImage = true;
            };

            vm.uploadFile = function (isuload) {
                vm.isUpload = isuload;
                var file = vm.uploader.queue[vm.uploader.queue.length - 1];
                file.upload();
            };


            function init() {
                fillDropDownDelAggType();
                fillDropDownDelAggImageType();
                delaggimageService.getDelAggImageForEdit({
                    id: delaggimageId
                }).success(function (result) {
                    vm.delaggimage = result.delAggImage;
                    if (vm.delaggimage.id == null) {
                        vm.uilimit = 20;
                        vm.delaggimage.delAggImageTypeRefId = null;
                    }
                    else {
                        vm.uilimit = null;
                        vm.imagePath = vm.delaggimage.imagePath ? angular.fromJson(vm.delaggimage.imagePath) : [];
                    }
                });
            }
            init();
        }
    ]);
})();

