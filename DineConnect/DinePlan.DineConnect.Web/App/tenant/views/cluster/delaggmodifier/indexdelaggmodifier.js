﻿
(function () {
    appModule.controller('tenant.views.cluster.delaggmodifier.indexdelaggmodifier', [
        '$scope', '$uibModal','$state', 'uiGridConstants', 'abp.services.app.delAggModifier', 'abp.services.app.orderTagGroup', 'abp.services.app.delAggModifierGroup',
        function ($scope, $modal,$state, uiGridConstants, delaggmodifierService, ordertaggroupService, delaggmodifiergroupService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggModifier.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggModifier.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggModifier.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn- dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editDelAggModifier(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteDelAggModifier(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('Code'),
                        field: 'code'
                    },
                    {
                        name: app.localize('ModifierGroup'),
                        field: 'delAggModifierGroupName'
                    },
                    {
                        name: app.localize('OrderTag'),
                        field: 'orderTagName'
                    },
                    {
                        name: app.localize('Price'),
                        field: 'price'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.modifiergroups = [];
            function fillDropDownModifierGroup() {
                delaggmodifiergroupService.getNames({}).success(function (result) {
                    vm.modifiergroups = result.items;
                });
            }
            fillDropDownModifierGroup();
            vm.ordertags = [];
            function fillDropDownOrderTag() {
                ordertaggroupService.getOrderTagNames({}).success(function (result) {
                    vm.ordertags = result.items;
                });
            }
            fillDropDownOrderTag();

            vm.getAllClear = function () {
                vm.filterText = null;
                vm.delAggModifierGroupId = null;
                vm.orderTagId = null;
            }

            vm.getAll = function () {
                vm.loading = true;
                delaggmodifierService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    orderTagId: vm.orderTagId,
                    delAggModifierGroupId: vm.delAggModifierGroupId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editDelAggModifier = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createDelAggModifier = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteDelAggModifier = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteDelAggModifierWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            delaggmodifierService.deleteDelAggModifier({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.delaggmodifiercreate", {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                delaggmodifierService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getAll();
        }]);
})();

