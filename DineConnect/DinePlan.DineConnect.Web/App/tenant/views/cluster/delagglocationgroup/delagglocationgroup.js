﻿
(function () {
    appModule.controller('tenant.views.cluster.delagglocationgroup.delagglocationgroup', [
        '$scope', '$uibModal', '$state', '$stateParams', 'abp.services.app.delAggLocationGroup',
        function ($scope, $modal, $state, $stateParams, delagglocationgroupService) {
            var vm = this;
            
            vm.saving = false;
            vm.delagglocationgroup = null;
            delagglocationgroupId= $stateParams.id;
            vm.save = function () {
                vm.saving = true;
                delagglocationgroupService.createOrUpdateDelAggLocationGroup({
                    delAggLocationGroup: vm.delagglocationgroup
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $state.go('tenant.delagglocationgroup');
            };
            function init() {
                delagglocationgroupService.getDelAggLocationGroupForEdit({
                    id: delagglocationgroupId
                }).success(function (result) {
                    vm.delagglocationgroup = result.delAggLocationGroup;
                });
            }
            init();
        }
    ]);
})();

