﻿
(function () {
    appModule.controller('tenant.views.cluster.master.delagglocmapping.indexdelagglocmapping', [
        '$scope','$state', '$uibModal', 'uiGridConstants', 'abp.services.app.delAggLocMapping',
        function ($scope, $state,$modal, uiGridConstants, delagglocmappingService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.editFlag = false;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggLocMapping.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggLocMapping.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggLocMapping.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                         cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editDelAggLocMapping(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteDelAggLocMapping(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Id'),
                        field: 'id'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('Location'),
                        field: 'delAggLocationName'
                    },
                    {
                        name: app.localize('AggType'),
                        field: 'delAggTypeRefName'
                    },
                    {
                        name: app.localize('Active'),
                        field: 'active',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                                '  <span ng-show="row.entity.active" class="label label-success">' + app.localize('Yes') + '</span>' +
                                '  <span ng-show="!row.entity.active" class="label label-default">' + app.localize('No') + '</span>' +
                                '</div>',
                        minWidth: 80
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAllClear = function () {
                vm.filterText = null;
                vm.delAggTypeRefId = null;
                vm.locationId = null;
            }


            vm.getAll = function () {
                vm.loading = true;
                delagglocmappingService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    delAggLocationId: vm.locationId,
                    delAggTypeRefId:vm.delAggTypeRefId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForCreateSingleLocation();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                    vm.locationId = result.locations[0].id;
                    vm.locationName = result.locations[0].name;
                    if (vm.editFlag == false) {
                        angular.forEach(vm.userGridOptions.data,function (value, key) {
                                if (value.locationId == vm.locationId) {
                                    abp.message.error(app.localize('LocationPriceWarning', vm.locationName));
                                    vm.locationId = 0;
                                    vm.intiailizeOpenLocation();
                                    return;
                                }
                            });
                    }
                });
            };
            vm.delAggTypes = [];
            function fillDropDownDelAggType() {
                delagglocmappingService.getDelAggTypeForCombobox({}).success(function (result) {
                    vm.delAggTypes = result.items;
                });
            }
            fillDropDownDelAggType();
            vm.editDelAggLocMapping = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createDelAggLocMapping = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteDelAggLocMapping = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteDelAggLocMappingWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            delagglocmappingService.deleteDelAggLocMapping({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.delagglocmappingcreate", {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                delagglocmappingService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getAll();
        }]);
})();

