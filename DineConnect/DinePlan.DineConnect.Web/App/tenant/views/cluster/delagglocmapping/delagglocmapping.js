﻿
(function () {
    appModule.controller('tenant.views.cluster.delagglocmapping.delagglocmapping', [
        '$scope', '$uibModal', '$state', '$stateParams', 'abp.services.app.delAggLocMapping', 'abp.services.app.delAggLocation','abp.services.app.grab','abp.services.app.delAggApi',
        function ($scope, $modal, $state, $stateParams, delagglocmappingService, delagglocationService,grabService,apiService ) {
            var vm = this;

            vm.saving = false;
            vm.delagglocmapping = null;
            delagglocmappingId= $stateParams.id;

            vm.SyncGrabMenu  = function() {

            }

            vm.save = function () {
                vm.saving = true;
                delagglocmappingService.createOrUpdateDelAggLocMapping({
                    delAggLocMapping: vm.delagglocmapping
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.syncData = function () {
                grabService.syncToGrabForMenu(vm.delagglocmapping.remoteCode)
                    .success(function (result) {
                        if (result)
                            abp.notify.info(app.localize("Done"));
                        else
                            abp.notify.info("Failed! " + result.statusDescription);
                    }).finally(function () {
                        vm.loading = false;
                    });
            };
            vm.syncMenuStatus = function () {
                grabService.syncMenuStatus(vm.delagglocmapping.remoteCode)
                    .success(function (result) {
                        console.log(result);
                        if (result)
                            abp.notify.info(app.localize("Done"));
                        else
                            abp.notify.info("Failed! ");
                    }).finally(function () {
                        vm.loading = false;
                    });
            };

            vm.grabMenuOutput = function () {
                apiService.apiForGrab({
                        locationId: vm.delagglocmapping.delAggLocationId,
                        merchantCode: vm.delagglocmapping.remoteCode
                    })
                    .success(function (result) {
                        console.log(JSON.stringify(result));
                    }).finally(function () {
                        vm.loading = false;
                    });
            };
            vm.cancel = function () {
                $state.go('tenant.delagglocmapping');
            };
            vm.locations = [];
            function fillDropDownLocation() {
                delagglocmappingService.getAggLocationNames({ }).success(function (result) {
                    console.log(result);
                    vm.locations = result.items;
                });
            }
            vm.delAggTypes = [];
            function fillDropDownDelAggType() {
                delagglocmappingService.getDelAggTypeForCombobox({ }).success(function (result) {
                    vm.delAggTypes = result.items;
                });
            }
            function init() {
                vm.loading = true;
                fillDropDownLocation();
                fillDropDownDelAggType();
                delagglocmappingService.getDelAggLocMappingForEdit({
                    id: delagglocmappingId
                }).success(function (result) {
                    vm.delagglocmapping = result.delAggLocMapping;
                    if (vm.delagglocmapping.id == null) {
                        vm.delagglocmapping.active = null;
                        vm.delagglocmapping.delAggTypeRefId = null;
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            }
            init();
        }
    ]);
})();

