﻿(function () {
    appModule.controller('tenant.views.wheel.ordersummary', [
        '$scope', "$uibModal", '$rootScope', '$uibModal', '$interval', 'abp.services.app.delivery',
        function ($scope,$modal, $rootScope, $uibModal, $interval, deliveryService) {
            var vm = this;
            $rootScope.settings.layout.pageSidebarClosed = true;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };


            vm.tabCurrent = 0;
            vm.location = 0;
            vm.currentLocation = null;
            vm.currentMember = null;
            vm.memberId = 0;

            vm.getStatus = function () {
                vm.status = null;

                deliveryService.getDeliveryStatus({
                    member: vm.memberId,
                    location:  vm.location
                }).success(function (result) {
                    vm.status = result;
                }).finally(function (result) {
                    vm.fillTab(vm.tabCurrent);
                });
            };
            var allInt=$interval(function() {
                vm.getStatus();
            }, 10000);

            vm.fillTab = function (tabposition) {
                vm.updateTab(tabposition);
            }
            vm.openOrder = function(order) {
                $modal.open({
                    templateUrl: "~/App/tenant/views/connect/report/tickets/ticket/deliveryTicketModal.cshtml",
                    controller: "tenant.views.connect.report.deliveryticketModal as vm",
                    backdrop: "static",
                    resolve: {
                        ticket: function () {
                            return order;
                        }
                    }
                });
            }
            vm.updateTab = function (updatepos) {
                vm.tabCurrent = updatepos;
                deliveryService.getDeliveryTickets({
                    location: vm.location,
                    status: vm.tabCurrent,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting
                }).success(function (result) {
                    console.log(result);
                    if (vm.tabCurrent == 0) {
                        vm.notassigned = result.items;
                    }

                    if (vm.tabCurrent == 1) {
                        vm.preparing = result.items;
                    }

                    if (vm.tabCurrent == 2) {
                        vm.delivering = result.items;
                    }

                    if (vm.tabCurrent == 3) {
                        vm.delivered = result.items;
                    }
                }).finally(function (result) {
                });
            }
            vm.getStatus();

            vm.openLocation = function () {

                vm.locationGroup = app.createLocationInputForCreateSingleLocation();
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.currentLocation = result.locations[0];
                        vm.location = result.locations[0].id;
                    } else {
                        vm.location = 0;
                        vm.currentLocation = null;
                    }
                });

                vm.getStatus();
            }
            vm.openMember = function() {
                vm.locationGroup = app.createLocationInputForCreateSingleLocation();

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/wheel/order/selectMember.cshtml",
                    controller: "tenant.views.wheel.order.selectMember as vm",
                    backdrop: "static",
                    keyboard: false
                });
                modalInstance.result.then(function(result) {
                    if (result != null) {
                        vm.currentMember = result;
                        vm.memberId = result.id;
                    } else {
                        vm.currentMember = null;
                        vm.memberId = 0;
                    }
                });

                vm.getStatus();
            };

            vm.changeLocation = function (ticket) {
                
                vm.locationGroup = app.createLocationInputForCreateSingleLocation();
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.changeLocationId = result.locations[0].id;
                        deliveryService.changeLocation({
                            locationId: vm.changeLocationId,
                            ticketId: ticket
                        }).success(function (result) {

                        }).finally(function (result) {
                        });
                    }
                });

                
            }

        }
    ]);
})();