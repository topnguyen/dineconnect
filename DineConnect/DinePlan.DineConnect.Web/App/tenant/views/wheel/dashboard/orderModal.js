﻿(function () {
    appModule.controller('tenant.views.engage.ordersummary.orderModal', [
        '$scope', '$uibModalInstance', 'ticket','abp.services.app.delivery',
        function ($scope, $modalInstance, ticket,ticketService) {
            var vm = this;

            vm.close = function () {
                $modalInstance.dismiss();
            };

            vm.init = function() {
                ticketService.getInputTicket({
                    id: ticket
                }).success(function (result) {
                    console.log(result);
                    vm.ticket = result;
                    vm.ctime = moment(vm.ticket.ticketCreatedTime).format('LLL');
                    vm.otime = moment(vm.ticket.lastOrderTime).format('LLL');
                    vm.ptime = moment(vm.ticket.lastPaymentTime).format('LLL');
                });
            };

            vm.init();
        }
    ]);
})();