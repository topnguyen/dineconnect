﻿(function () {
    appModule.controller('tenant.views.wheel.order.selectMember', [
        '$scope', '$uibModalInstance','uiGridConstants', "$window", "hotkeys", 'abp.services.app.location', "abp.services.app.member",
        function ($scope, $modalInstance, uiGridConstants, $window, hotKeys, locationService, memberService) {
            var vm = this;
            vm.filterText = '';
            vm.member = null;
            vm.memberList = null;


            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            hotKeys.add({
                combo: 'esc',
                callback: function (event, hotkey) {
                    vm.cancel();
                }
            });

           
            vm.cancel = function () {
                $modalInstance.close(null);
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };


            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Select'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.selectMember(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Generate') + '"><i class="fa fa-dashcube"></i></button>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Code'),
                        field: 'memberCode'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                     {
                         name: app.localize('Email'),
                         field: 'emailId'
                     },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };


            vm.getAll = function () {
                vm.loading = true;
                memberService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.selectMember = function(member) {
                $modalInstance.close(member);
            }

            vm.createNewMember = function() {
                $modalInstance.close(null);
            }

        }
    ]);
})();