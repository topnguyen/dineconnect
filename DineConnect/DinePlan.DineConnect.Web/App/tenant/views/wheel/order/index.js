﻿(function () {
    appModule.controller("tenant.views.wheel.order", [
        "$scope", "$uibModal", "$window",'appSession', "indexeddb", "abp.services.app.menuItem", "hotkeys", "abp.services.app.member", 'abp.services.app.location', 'abp.services.app.delivery',
        function ($scope, $modal, $window,appSession, indexeddb, menuService, hotKeys, memberService, locationService, dService) {
            var vm = this;
            $scope.settings.layout.pageSidebarClosed = true;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });

            $scope.setMyFocus = function (str) {
                var element = $window.document.getElementById(str);
                element.focus();
            };

            vm.memberList = [];
            vm.locationList = [];

            vm.selectedMember = [];

            vm.memberDetail = app.localize("SelectMember");
            vm.memberAddress = "";

            vm.selectedLocation = [];

            vm.locationDetail = app.localize('Select') + " " + app.localize('Location');
            vm.locationCode = 0;

            vm.selectedItem = null;
            vm.selectedQuantity = "";
            vm.selectedNotes = "";

            vm.noofProducts = 0;
            vm.noofQuantities = 0;
            vm.productTotalAmount = 0;
            vm.taxTotalAmount = 0;
            vm.orderAmount = 0;


            vm.orderDetails = [];

            vm.clickDB = function () {
            };

            vm.menuSearch = function (str) {
                var matches = [];
                vm.menuForLocation.forEach(function (menu) {
                    if ((menu.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
                    (menu.aliasCode != null && menu.aliasCode.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)) {
                        matches.push(menu);
                    }
                });
                return matches;
            };

            vm.removeRow = function (menuIndex) {
                vm.orderDetails.splice(menuIndex, 1);
                vm.calculateTotalVlaue(vm.orderDetails);
            };
            vm.calculateTotalVlaue = function (data) {
                var tempTotProductAmount = 0;
                var tempTotDiscAmt = 0;
                var tempTotTaxAmt = 0;
                var tempTotOrderAmount = 0;
                var tempnoofquantities = 0;

                angular.forEach(data, function (val) {
                    tempTotProductAmount = parseFloat(tempTotProductAmount) + parseFloat(parseFloat(vm.isUndefinedOrNull(val.totalAmount) ? 0 : val.totalAmount).toFixed(2));
                    //tempTotDiscAmt = parseFloat(tempTotDiscAmt) + parseFloat(vm.isUndefinedOrNull(val.discountAmount) ? 0 : val.discountAmount);
                    tempTotTaxAmt = parseFloat(tempTotTaxAmt) + parseFloat(parseFloat(vm.isUndefinedOrNull(val.taxAmount) ? 0 : val.taxAmount).toFixed(2));
                    tempTotOrderAmount = parseFloat(tempTotOrderAmount) + parseFloat(parseFloat(vm.isUndefinedOrNull(val.netAmount) ? 0 : val.netAmount).toFixed(2));
                    tempnoofquantities = parseFloat(tempnoofquantities) + parseFloat(parseFloat(vm.isUndefinedOrNull(val.quantity) ? 0 : val.quantity).toFixed(2));
                });

                vm.noofProducts = data.length;
                vm.noofQuantities = tempnoofquantities;
                vm.productTotalAmount = parseFloat(tempTotProductAmount.toFixed(2));;
                vm.taxTotalAmount = parseFloat(tempTotTaxAmt.toFixed(2));
                vm.orderAmount = parseFloat(tempTotOrderAmount.toFixed(2));
            };

            $scope.clearInput = function (id) {
                if (id) {
                    $scope.$broadcast("angucomplete-alt:clearInput", id);
                } else {
                    $scope.$broadcast("angucomplete-alt:clearInput");
                }
            };

            vm.refreshDB = function () {
                vm.loading = true;
                menuService.apiItemsForSpecificLocation(appSession.user.locationRefId).success(function (result) {
                    angular.forEach(result, function (value, key) {
                        indexeddb.then(function (db) {
                            db.models.menus.delete(value.locationId).then(function () {
                            });
                            var menu = {
                                locationId: value.locationId,
                                items: value.menus
                            };
                            db.models.menus.add(menu);
                        });
                    });

                }).finally(function () {
                    vm.loading = false;
                    vm.getmemberDetails();
                    initializeMenu();
                });
            };

            vm.choseLocation = function () {
               
            };

            function callMemberSelection(result) {
                var otherInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/wheel/order/memberSearch.cshtml",
                    controller: "tenant.views.wheel.order.memberSearch as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        member: function () {
                            return result;
                        }
                    }
                });
                otherInstance.result.then(function (result) {
                    if (result != null) {

                        vm.selectedMember = result;
                        vm.memberDetail = vm.selectedMember.memberCode + "/" + vm.selectedMember.name;
                        vm.memberAddress = vm.selectedMember.address;
                        vm.locationCode = vm.selectedMember.locationRefId;
                        if (vm.locationCode != null) {
                            locationService.getLocationById({
                                id: vm.locationCode
                            }).success(function(result) {
                                vm.selectedLocation = result;
                                vm.locationDetail = result.name;
                            }).finally(function() {
                            });
                        }

                    }

                    hotKeys.add({
                        combo: "enter",
                        allowIn: ["INPUT"],
                        callback: function (event, hotkey) {
                            vm.addItemToOrder();
                        }
                    });

                    vm.getmemberDetails();
                });

                initializeMenu();
            }
            vm.choseMember = function () {

                vm.selectedMember = [];
                hotKeys.del("enter");
                hotKeys.del("*");
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/wheel/order/selectMember.cshtml",
                    controller: "tenant.views.wheel.order.selectMember as vm",
                    backdrop: "static",
                    keyboard: false
                });

                modalInstance.result.then(function (result) {

                    callMemberSelection(result);

                });

            };
            vm.save = function () {
                if (vm.selectedMember == null || vm.selectedMember == "") {
                    abp.notify.warn(app.localize('Select') + " " + app.localize("Member"));
                    return;
                }

                if (vm.selectedLocation == null || vm.selectedLocation == "") {
                    abp.notify.warn(app.localize('LocationErr'));
                    return;
                }
                if (vm.orderAmount == 0) {
                    abp.notify.warn(app.localize('MenuItem') + " ? ");
                    return;
                }

                vm.orderDate = moment();

                vm.userAddress = vm.selectedMember.address + "\n" + vm.selectedMember.city;
                abp.message.confirm(
                    app.localize("SaveItems_f", vm.orderAmount),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            var outputOrders = [];
                            angular.forEach(vm.orderDetails, function (value, key) {
                                outputOrders.push({
                                    menuItemPortionId: value.item.originalObject.portionId,
                                    note: value.note,
                                    quantity: value.quantity,
                                    tax: value.taxAmount,
                                    price: value.item.originalObject.price
                                });
                            });
                            dService.createTicket({
                                locationId: vm.selectedLocation.id,
                                deliveryTicketType: 0,
                                memberId: vm.selectedMember.id,
                                total: vm.orderAmount,
                                orders: outputOrders,
                                address: vm.userAddress,
                                name: vm.selectedMember.name,
                                locality: vm.selectedMember.locality
                            }).success(function (result) {
                                abp.notify.success(app.localize("OrderSent_f", vm.locationDetail));
                            }).finally(function () {
                                vm.loading = false;
                            });
                            clearAll();

                        }
                    }
                );
            };
            vm.clear = function () {
                abp.message.confirm(
                    app.localize("ClearItemsWarning"),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            clearAll();
                        }
                    }
                );
            };
            function clearAll() {
                vm.selectedMember = [];

                vm.memberDetail = app.localize("SelectMember");
                vm.memberAddress = "";

                vm.selectedLocation = [];

                vm.locationDetail = app.localize('Select') + " " + app.localize('Location');
                vm.locationCode = 0;

                vm.selectedItem = null;
                vm.selectedQuantity = "";
                vm.selectedNotes = "";

                vm.noofProducts = 0;
                vm.noofQuantities = 0;
                vm.productTotalAmount = 0;
                vm.taxTotalAmount = 0;
                vm.orderAmount = 0;
                vm.orderDetails = [];
            }

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.notefocushappened = false;
            vm.focusOnNotes = function () {
                vm.notefocushappened = true;
            };

            vm.addItemToOrder = function () {
                if (vm.selectedItem == null) {
                    abp.notify.error(app.localize("NoItem"));
                    $scope.setMyFocus("searMenuItem_value");
                    return;
                }

                //  Check whether the Product Already Exists or not 
                angular.forEach(vm.orderDetails, function (value, key) {
                    vm.orderDetails[key].alreadyExists = false;
                    if (value.item.originalObject.userString == vm.selectedItem.originalObject.userString) {
                        vm.orderDetails[key].alreadyExists = true;
                    }
                });

                if (vm.selectedQuantity == 0) {
                    abp.notify.warn(app.localize("NoQuantity"));
                    $scope.setMyFocus("searQuantity");
                    return;
                }

                if (vm.selectedNotes == "" && vm.notefocushappened == false) {
                    abp.notify.info(app.localize("NoNotes"));
                    $scope.setMyFocus("searNote");
                    return;
                }

                var totalQty;
                var price;

                //Calculation for Tax
                if (vm.isUndefinedOrNull(vm.selectedQuantity)) {
                    totalQty = 0;
                } else {
                    totalQty = vm.selectedQuantity;
                }

                if (vm.isUndefinedOrNull(vm.selectedItem.originalObject.price)) {
                    price = 0;
                } else {
                    price = vm.selectedItem.originalObject.price;
                }

                var producttotalAmount = parseFloat(price) * parseFloat(totalQty);
                producttotalAmount = parseFloat(producttotalAmount.toFixed(2));

                var producttaxAmount = 0;
                var taxForMenu = [];

                taxneedstoremove = [];

                data = vm.selectedItem.originalObject;

                if (data.applicableTaxes.length > 0) //  Calculate Tax 
                {
                    angular.forEach(data.applicableTaxes, function (value, key) {
                        var taxCalculationOnValue = 0.0;

                        if (value.taxCalculationMethod == "GT" || value.taxCalculationMethod == "ST") {
                            taxCalculationOnValue = producttotalAmount;
                        } else { //  Not Applicable For Now, May be used when Sort Order , With in Tax Reference 
                            var taxrefvalueflag = false;
                            taxForMenu.some(function (taxdata, taxkey) {
                                if (taxdata.taxName == value.taxCalculationMethod) {
                                    taxCalculationOnValue = taxdata.taxValue;
                                    taxrefvalueflag = true;
                                    return true;
                                }
                            });

                            if (taxrefvalueflag == false)
                                taxneedstoremove.push({ 'taxName': value.taxName });
                        }

                        if (taxCalculationOnValue > 0) {
                            var taxamt = parseFloat(taxCalculationOnValue * value.dinePlanTaxRate / 100);
                            taxamt = parseFloat(taxamt.toFixed(2));
                            producttaxAmount = parseFloat(producttaxAmount) + parseFloat(taxamt);
                            producttaxAmount = parseFloat(producttaxAmount.toFixed(2));

                            if (taxamt > 0) {
                                taxForMenu.push
                                ({
                                    'menuItemId': data.menuItemId,
                                    'dinePlanTaxRefId': value.dinePlanTaxRefId,
                                    'dinePlanTaxName': value.dinePlanTaxName,
                                    'dinePlanTaxRate': value.dinePlanTaxRate,
                                    'dinePlanTaxValue': taxamt,
                                    'rounding': value.rounding,
                                    'sortOrder': value.sortOrder,
                                    'taxCalculationMethod': value.taxCalculationMethod
                                });
                            }
                        }
                    });
                }


                angular.forEach(taxneedstoremove, function (value, key) {
                    angular.forEach(data.applicableTaxes, function (detvalue, detkey) {
                        if (value.taxName == detvalue.taxName)
                            data.applicableTaxes.splice(detkey, 1);
                    });
                });

                var productnetAmount = parseFloat(parseFloat(producttotalAmount).toFixed(2)) + parseFloat(parseFloat(producttaxAmount).toFixed(2));

                // Tax Calculation Ends Here 


                vm.orderDetails.push({
                    item: vm.selectedItem,
                    quantity: vm.selectedQuantity,
                    note: vm.selectedNotes,
                    totalAmount: producttotalAmount,
                    taxAmount: producttaxAmount,
                    netAmount: productnetAmount,
                    taxForMenu: taxForMenu,
                    'alreadyExists': false
                });

                vm.selectedItem = null;
                vm.selectedQuantity = "";
                vm.selectedNotes = "";
                $scope.clearInput();
                vm.notefocushappened = false;
                $scope.setMyFocus("searMenuItem_value");
                vm.calculateTotalVlaue(vm.orderDetails);
            };
            vm.orderqtyRefresh = function () {
                angular.forEach(vm.orderDetails, function (value, key) {
                    value.totalAmount = value.item.originalObject.price * value.quantity;
                });
            };
          
            vm.menuForLocation = [];
            function initializeMenu() {
                indexeddb.then(function (db) {
                    db.models.menus.select("locationId").equal(vm.locationCode).find().then(function (record) {
                        if (record != null)
                            vm.menuForLocation = record.items;
                        else {
                            db.models.menus.select("locationId").equal(0).find().then(function (record) {
                                vm.menuForLocation = record.items;
                            });
                        }
                    }).finally(function () {

                    });
                });
                vm.getmemberDetails();
            };
            vm.getmemberDetails = function () {
               
            };
        
            vm.openLocation = function () {
                if (vm.selectedMember == null) {
                    vm.locationDetail = "";
                }
                
                vm.locationGroup = app.createLocationInputForCreateSingleLocation();

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.locationCode = result.locations[0].id;
                        vm.selectedLocation = result.locations[0];
                        vm.locationDetail = vm.selectedLocation.name;
                    }

                });

                initializeMenu();
            }

            hotKeys.add({
                combo: "enter",
                allowIn: ["INPUT"],
                callback: function (event, hotkey) {
                    vm.addItemToOrder();
                }
            });
            hotKeys.add({
                combo: "ctrl+up",
                description: app.localize("SelectMember"),
                callback: function (event, hotkey) {
                    vm.choseMember();
                }
            });
            hotKeys.add({
                combo: "alt+up",
                description: app.localize("SelectLocation"),
                callback: function (event, hotkey) {
                    vm.openLocation();
                }
            });
            hotKeys.add({
                combo: "ctrl+down",
                description: app.localize("CreateMember"),
                callback: function (event, hotkey) {
                    callMemberSelection(null);
                }
            });
            hotKeys.add({
                combo: "ctrl+right",
                allowIn: ["INPUT"],
                description: app.localize("SendOrderToLocation"),
                callback: function (event, hotkey) {
                    vm.save();
                }
            });
            hotKeys.add({
                combo: "ctrl+left",
                allowIn: ["INPUT"],
                description: app.localize("ClearOrder"),
                callback: function (event, hotkey) {
                    vm.clear();
                }
            });

        }
    ]);
})();