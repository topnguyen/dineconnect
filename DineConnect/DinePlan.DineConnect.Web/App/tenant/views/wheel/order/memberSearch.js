﻿(function () {
    appModule.controller('tenant.views.wheel.order.memberSearch', [
        '$scope', '$uibModalInstance', 'member', "$window", "hotkeys", 'abp.services.app.location', "abp.services.app.member",
        function ($scope, $modalInstance, member, $window, hotKeys, locationService, memberService) {
            var vm = this;

            vm.searchedmemberList = [];
            vm.filterText = '';
            vm.member = null;

            vm.editAddressFlag = false;
            vm.addOneMoreNewAddress = false;
            vm.createnewmemberFlag = false;

           

            $scope.setMyFocus = function (str) {
                var element = $window.document.getElementById(str);
            };
            hotKeys.add({
                combo: 'esc',
                callback: function (event, hotkey) {
                    vm.cancel();
                }
            });
            hotKeys.add({
                combo: 'f2',
                allowIn: ['BUTTON', 'INPUT'],
                callback: function (event, hotkey) {
                    vm.editAllowed();
                }
            });
            hotKeys.add({
                combo: "+",
                allowIn: ['BUTTON', 'INPUT'],
                description: app.localize("Add") + ' ' + app.localize('Member'),
                callback: function (event, hotkey) {
                    vm.save();
                }
            });



         
            vm.editAllowed = function () {
                vm.editAddressFlag = true;
            }

            vm.selectMember = function (data) {

                if (data != null) {
                    vm.member = data;
                    vm.searchedmemberList = [];
                    vm.searchedmemberList.push(vm.member);
                    $scope.setMyFocus('saveButton');
                } else {
                    vm.createNewMember();
                    $scope.setMyFocus('memberCode');

                }
            }
            vm.selectOtherAddress = function (data, otheraddress) {
                vm.member = data;
                vm.member.address = otheraddress.address;
                vm.member.locality = otheraddress.locality;
                vm.member.city = otheraddress.city;
                vm.member.postalCode = otheraddress.postalCode;
                vm.member.city = otheraddress.city;
                vm.member.state = otheraddress.state;
                vm.member.country = otheraddress.country;
                vm.member.editAddressRefId = otheraddress.id;

                vm.searchedmemberList = [];
                vm.searchedmemberList.push(vm.member);
                $scope.setMyFocus('saveButton');
            }
            vm.createNewMember = function () {
                vm.searchedmemberList = [];
                vm.editAddressFlag = true;
                vm.createnewmemberFlag = true;
                vm.filterText = "";

                memberService.getMemberForEdit({
                    id: null
                }).success(function (result) {
                    vm.member = result.member;
                    $scope.setMyFocus("memberCode");
                });
                $scope.setMyFocus("memberCode");
            }
            vm.editMemberAddress = function (data) {
                //vm.selectMember(data);
                vm.filterText = data.memberCode;
                vm.editAddressFlag = true;
                $scope.setMyFocus("Address");
            }
            vm.addOneMoreAddress = function (data) {
                vm.filterText = data.memberCode;
                vm.editAddressFlag = true;

                memberService.getMemberForEdit({
                    id: data.id
                }).success(function (result) {
                    vm.member = result.member;
                    vm.member.address = '';
                    vm.member.locality = '';
                    vm.member.city = '';
                    vm.member.postalCode = '';
                    vm.member.state = '';
                    vm.member.country = '';
                    vm.addOneMoreNewAddress = true;
                });

            }
            vm.saveAddress = function() 
            {
                vm.saving = true;
                if (vm.editAddressFlag == true) {
                    vm.member.name = vm.member.name.toUpperCase();
                    vm.member.address = vm.member.address.toUpperCase();

                    memberService.createOrUpdateMember({
                        member: vm.member,
                        addOneMoreNewAddress: vm.addOneMoreNewAddress
                    }).success(function (result) {
                        if (vm.addOneMoreNewAddress==true)
                            abp.notify.info(app.localize('NewAddressAddedSuccessfully'));
                        else
                            abp.notify.info(app.localize('SavedSuccessfully'));

                        vm.editAddressFlag = false
                        vm.member.id = result.id;
                    }).finally(function () {
                        vm.saving = false;
                        vm.addOneMoreNewAddress = false;
                        vm.createnewmemberFlag = false;

                        if (vm.closeflag)
                            $modalInstance.close(vm.member);
                        else
                            vm.getmemberDetails();

                    });
                }
            }
            vm.closeflag = false;
            vm.save = function () {
                if (vm.member == null) {
                    abp.notify.error(app.localize('SelectMember') + " ?");
                    return;
                }
                vm.closeflag = true;
                vm.saveAddress();

                if (vm.editAddressFlag == true) {
                    vm.closeflag = true;
                  
                }
                else {
                    $modalInstance.close(vm.member);
                }
            }
            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.selectMember(member);
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.getmemberDetails = function () {
                vm.loading = true;
                memberService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: ''
                }).success(function (result) {
                    vm.memberList = result.items;
                }).finally(function () {
                    vm.loading = false;
                    vm.filterText = vm.member.memberCode;
                });
            };

        }
    ]);
})();