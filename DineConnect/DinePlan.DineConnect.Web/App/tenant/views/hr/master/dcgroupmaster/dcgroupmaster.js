﻿(function () {
    appModule.controller('tenant.views.hr.master.dcgroupmaster.dcgroupmaster', [
        '$scope', '$uibModalInstance', 'abp.services.app.dcGroupMaster', 'dcgroupmasterId', 'abp.services.app.dcHeadMaster', 'abp.services.app.personalInformation',
        function ($scope, $modalInstance, dcgroupmasterService, dcgroupmasterId, dcheadmasterService, personalinformationService) {
            var vm = this;

            /* eslint-disable */

            vm.saving = false;
            vm.dcgroupmaster = null;
            $scope.existall = true;


            vm.save = function () {

                vm.saving = true;

                vm.dcgroupmaster.groupName = vm.dcgroupmaster.groupName.toUpperCase();

                dcgroupmasterService.createOrUpdateDcGroupMaster({
                    dcGroupMaster: vm.dcgroupmaster,
                    linkedSkillSetList: vm.skillSetRefId
                }).success(function () {
                    abp.notify.info(app.localize('DcGroupMaster') + ' ' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.skillSetRefId = [];


            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.refdcheadmaster = [];

            function fillDropDownDcHeadMaster() {
                dcheadmasterService.getHeadNames({}).success(function (result) {
                    vm.refdcheadmaster = result.items;
                });
            }

            vm.refskillset = [];

            function fillDropDownSkillSet() {
                personalinformationService.getSkillSetForCombobox({}).success(function (result) {
                    vm.refskillset = result.items;
                });
            }
            fillDropDownSkillSet();


            function init() {
                fillDropDownDcHeadMaster();

                dcgroupmasterService.getDcGroupMasterForEdit({
                    Id: dcgroupmasterId
                }).success(function (result) {
                    vm.dcgroupmaster = result.dcGroupMaster;
                    vm.skillSetRefId = result.linkedSkillSetList;
                });
            }
            init();
        }
    ]);
})();

