﻿(function () {
    appModule.controller('tenant.views.hr.master.salaryinfo.index', [
        '$scope', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.salaryInfo', 'abp.services.app.incentiveTag',
        function ($scope, $state, $stateParams, $modal, uiGridConstants, salaryinfoService, incentivetagService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.passwordRequired = false;
            if ($stateParams.passwordRequired != "") {
                if ($stateParams.passwordRequired == false || $stateParams.passwordRequired == 'false')
                    vm.passwordRequired = false;
            }

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Hr.Master.SalaryInfo.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Hr.Master.SalaryInfo.Edit'),
                showSalary: abp.auth.hasPermission('Pages.Tenant.Hr.Master.SalaryInfo.ShowSalary'),
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: 10, //app.consts.grid.defaultPageSize,
                sorting: null
            };

            if (vm.passwordRequired == true) {
                var dayofyear = moment().dayOfYear();
                var entered = prompt("Please enter PIN");

                if (entered != dayofyear) {
                    abp.notify.error(app.localize('Wrong'));
                    $state.go('tenant.employeedashboard');
                    return;
                }

                var password = prompt("Please enter Password");
                if (password != 'qwerty123#') {
                    abp.notify.error(app.localize('Wrong'));
                    $state.go('tenant.employeedashboard');
                    return;
                }
            }

            vm.defineGrid = function () {
                vm.userGridOptions = {
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: 10, // app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                    columnDefs: [
                        {
                            name: app.localize('Actions'),
                            enableSorting: false,
                            width: 120,
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.editSalaryInfo(row.entity)"><i class="fa fa-money"></i></button>' +
                                '</div>'
                        },
                        {
                            name: app.localize('EmployeeId'),
                            field: 'employeeRefId',
                            maxWidth: 80,
                            cellClass: function (grid, row) {
                                if (row.entity.resignedFlag)
                                    return 'ui-redcolor';
                            },
                        },
                        {
                            name: app.localize('EmployeeCode'),
                            field: 'employeeRefCode',
                            maxWidth: 120,
                            cellClass: function (grid, row) {
                                if (row.entity.resignedFlag)
                                    return 'ui-redcolor';
                            },
                        },
                        {
                            name: app.localize('Image'),
                            field: 'employeeRefName',
                            enableSorting: false,
                            maxWidth: 80,
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents\">' +
                                '  <img ng-if="row.entity.profilePictureId" ng-src="' + abp.appPath + 'Profile/GetProfilePictureById?id={{row.entity.profilePictureId}}" width="50" height="50" class="img-rounded img-profile-picture-in-grid" />' +
                                '  <img ng-if="!row.entity.profilePictureId" src="' + abp.appPath + 'Common/Images/default-profile-picture.png" width="22" height="22" class="img-rounded" />' +
                                //' <br>  {{COL_FIELD CUSTOM_FILTERS}} &nbsp;' +
                                '</div>',
                        },
                        {
                            name: app.localize('EmployeeName'),
                            field: 'employeeRefName',
                            maxWidth: 300,
                            //cellTemplate:
                            //    '<div class=\"ui-grid-cell-contents\">' +
                            //    '  <img ng-if="row.entity.profilePictureId" ng-src="' + abp.appPath + 'Profile/GetProfilePictureById?id={{row.entity.profilePictureId}}" width="50" height="50" class="img-rounded img-profile-picture-in-grid" />' +
                            //    '  <img ng-if="!row.entity.profilePictureId" src="' + abp.appPath + 'Common/Images/default-profile-picture.png" width="22" height="22" class="img-rounded" />' +
                            //    ' <br>  {{COL_FIELD CUSTOM_FILTERS}} &nbsp;' +
                            //    '</div>',
                            cellClass: function (grid, row) {
                                if (row.entity.resignedFlag)
                                    return 'ui-redcolor';
                            },
                        },                        
                        {
                            name: app.localize('Company'),
                            field: 'companyRefCode',
                            cellClass: function (grid, row) {
                                if (row.entity.resignedFlag)
                                    return 'ui-redcolor';
                            },
                        },
                        {
                            name: app.localize('IncentiveAndOTCalculationPeriodRefName'),
                            field: 'incentiveAndOTCalculationPeriodRefId',
                            cellClass: function (grid, row) {
                                if (row.entity.resignedFlag)
                                    return 'ui-redcolor';
                            },
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents\">' +
                                '  <span ng-show="row.entity.incentiveAndOTCalculationPeriodRefId==1" class="label label-success">' + app.localize('Salary') + ' ' + app.localize('Month') + '</span>' +
                                '  <span ng-show="row.entity.incentiveAndOTCalculationPeriodRefId==2" class="label label-danger">' + app.localize('Previous') + ' ' + app.localize('Month') + '</span>' +
                                '</div>',
                        },
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                            if (!sortColumns.length || !sortColumns[0].field) {
                                requestParams.sorting = null;
                            } else {
                                requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                            }

                            vm.getAll();
                        });
                        gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                            requestParams.skipCount = (pageNumber - 1) * pageSize;
                            requestParams.maxResultCount = pageSize;

                            vm.getAll();
                        });
                    },
                    data: []
                };
            }

            vm.defineGrid();



            vm.getAll = function () {
                vm.loading = true;
                salaryinfoService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editSalaryInfo = function (myObj) {
                if (myObj.resignedFlag) {
                    abp.notify.error(app.localize('EmployeeResignedAlready', myObj.employeeRefName, myObj.lastWorkingDate));
                    abp.message.error(app.localize('EmployeeResignedAlready', myObj.employeeRefName, myObj.lastWorkingDate));
                }
                openCreateOrEditModal(myObj.id, null);
            };

            vm.editIncentiveInfo = function (myObj) {
                
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/salaryinfo/salaryinfo.cshtml',
                    controller: 'tenant.views.hr.master.salaryinfo.salaryinfo as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        salaryinfoId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.importStatutory = function () {
                vm.saving = true;
                incentivetagService.importSystemIncentiveList({

                }).success(function (result) {
                    if (result == true) {
                        abp.notify.success(app.localize('Successfully') + ' ' + app.localize('Added'));
                    }
                    vm.saving = false;
                });
            }

            vm.createSalaryInfo = function () {
                openCreateOrEditModal(null, null);
            };
            vm.deleteSalaryInfo = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteSalaryInfoWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            salaryinfoService.deleteSalaryInfo({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId, cloneFlag) {
                $state.go("tenant.salaryinfodetail", {
                    id: objId,
                    cloneFlag: cloneFlag
                });

                //var modalInstance = $modal.open({
                //    templateUrl: '~/App/tenant/views/hr/master/salaryinfo/salaryinfo.cshtml',
                //    controller: 'tenant.views.hr.master.salaryinfo.salaryinfo as vm',
                //    backdrop: 'static',
                //    keyboard: false,
                //    resolve: {
                //        salaryinfoId: function () {
                //            return objId;
                //        }
                //    }
                //});

                //modalInstance.result.then(function (result) {
                //    vm.getAll();
                //});
            }

            vm.exportToExcel = function () {
                salaryinfoService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getAll();
            vm.showSalary = false;

            vm.addSalaryShowColumns = function () {
                if (vm.permissions.showSalary == true && vm.showSalary) {
                    vm.colbasicSalary =
                        {
                            name: app.localize('BasicSalary'),
                            field: 'basicSalary',
                            cellClass: 'ui-ralign',
                            width: 60
                        };

                    vm.colallowance =
                        {
                            name: app.localize('Allowance'),
                            field: 'allowance',
                            cellClass: 'ui-ralign',
                            width: 60
                        };

                    vm.colot1PerHour =
                        {
                            name: app.localize('OT1'),
                            field: 'ot1PerHour',
                            cellClass: 'ui-ralign',
                            width: 60
                        };

                    vm.colot2PerHour =
                        {
                            name: app.localize('OT2'),
                            field: 'ot2PerHour',
                            cellClass: 'ui-ralign',
                            width: 60
                        };

                    vm.colaccomodation =
                        {
                            name: app.localize('Accomodation'),
                            field: 'accomodation',
                            cellClass: 'ui-ralign',
                            width: 60
                        };

                    vm.collevy =
                        {
                            name: app.localize('Levy'),
                            field: 'levy',
                            cellClass: 'ui-ralign',
                            width: 60
                        };

                    vm.coldeduction =
                        {
                            name: app.localize('Deduction'),
                            field: 'deduction',
                            cellClass: 'ui-ralign',
                            width: 60
                        };

                    vm.userGridOptions.columnDefs.splice(5, 0, vm.coldeduction);
                    vm.userGridOptions.columnDefs.splice(5, 0, vm.collevy);
                    vm.userGridOptions.columnDefs.splice(5, 0, vm.colaccomodation);
                    vm.userGridOptions.columnDefs.splice(5, 0, vm.colot2PerHour);
                    vm.userGridOptions.columnDefs.splice(5, 0, vm.colot1PerHour);
                    vm.userGridOptions.columnDefs.splice(5, 0, vm.colallowance);
                    vm.userGridOptions.columnDefs.splice(5, 0, vm.colbasicSalary);
                    vm.getAll();
                }
                else {
                    vm.getAll();
                }

                //{
                //	name: app.localize('BasicSalary'),
                //		field: 'basicSalary', cellClass: 'ui-ralign'
                //},
                //{
                //	name: app.localize('Allowance'),
                //	field: 'allowance', cellClass: 'ui-ralign'
                //},
                //{
                //	name: app.localize('OT1'),
                //	field: 'ot1PerHour', cellClass: 'ui-ralign'
                //},
                //{
                //	name: app.localize('OT2'),
                //	field: 'ot2PerHour', cellClass: 'ui-ralign'
                //},
                //{
                //	name: app.localize('Accomodation'),
                //	field: 'accomodation', cellClass: 'ui-ralign'
                //},
                //{
                //	name: app.localize('Levy'),
                //	field: 'levy', cellClass: 'ui-ralign'
                //},
                //{
                //	name: app.localize('Deduction'),
                //	field: 'deduction', cellClass: 'ui-ralign'
                //},
            }
        }]);
})();

