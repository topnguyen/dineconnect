﻿(function () {
    appModule.controller('tenant.views.hr.master.salaryinfo.salaryinfo', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.salaryInfo', 'abp.services.app.personalInformation', 'abp.services.app.incentiveTag', 'appSession', 'abp.services.app.employeeDocumentInfo',
        function ($scope, $state, $stateParams, $modal, salaryinfoService,
            personalinformationService, incentivetagService, appSession, employeedocumentinfoService) {
            /* eslint-disable */
            var vm = this;

            vm.salaryinfoId = $stateParams.id;
            vm.cloneFlag = false;
            vm.tenantName = appSession.tenant.name;
            if ($stateParams.cloneFlag == true || $stateParams.cloneFlag == 'true')
                vm.cloneFlag = true;

            vm.saving = false;
            vm.salaryinfo = null;
            $scope.existall = true;
            vm.incentiveList = [];
            vm.otSlabList = [];
            vm.editFlag = false;

            vm.validate = function (argOption) {
                salaryinfoService.validateBusinessRules({
                    salaryInfo: vm.salaryinfo,
                    incentiveList: vm.incentiveList
                }).success(function () {
                    abp.notify.info(app.localize('Validation') + ' ' + app.localize('Done') + ' ' + app.localize('SavedSuccessfully'));
                }).finally(function () {
                    vm.saving = false;
                });
            }

            vm.slabAddorEditPendingErrorExists = function () {
                var slabErrorExists = false;
                if (vm.otSlabList.length > 0) {
                    vm.otSlabList.some(function (value, detkey) {
                        if (value.editFlag == true) {
                            abp.notify.error(app.localize('OT') + ' ' + app.localize('Slabs') + ' ' + app.localize('Pending') + ' ?');
                            vm.addOtSlabPortion(value, 2);
                            slabErrorExists = true;
                            return true;
                        }
                    });
                }
                return slabErrorExists;
            }

            vm.save = function (argOption) {

                if (vm.slabAddorEditPendingErrorExists() == true)
                    return;

                //@@ Validation Required
                if (vm.salaryinfo.incentiveAndOTCalculationPeriodRefId == 0 || vm.salaryinfo.incentiveAndOTCalculationPeriodRefId == null) {
                    abp.notify.error(app.localize('OT') + ' ' + app.localize('Incentive') + ' ' + app.localize('Period') + ' ?');
                    return;
                }

                if (vm.salaryinfo.salaryMode == 2) {
                    if (vm.salaryinfo.hourlySalaryModeRefId >= 1 && vm.salaryinfo.hourlySalaryModeRefId <= 3) {
                        //   Do Nothing
                    }
                    else {
                        abp.notify.error(app.localize('Hourly') + ' ' + app.localize('Calculation') + ' ' + app.localize('Mode') + ' ?');
                        return;
                    }
                }
                else {
                    vm.salaryinfo.hourlySalaryModeRefId = null;
                }

                //IF OT Slabs exists then Salary Info - OT1 and OT2, Report OT Should Be Zero
                if (vm.otSlabList.length > 0) {
                    if (vm.salaryinfo.ot1PerHour > 0 || vm.salaryinfo.ot2PerHour > 0 || vm.salaryinfo.reportOtPerHour > 0) {
                        abp.notify.error(app.localize('If OTSlab List, Exists then you should change OT1 , OT2 and Report OT Should Be Zero'));
                        return;
                    }
                }

                vm.saving = true;

                salaryinfoService.createOrUpdateSalaryInfo({
                    salaryInfo: vm.salaryinfo,
                    incentiveList: vm.incentiveList
                }).success(function () {
                    abp.notify.info(app.localize('SalaryInfo') + ' ' + app.localize('SavedSuccessfully'));

                    if (argOption == 3) {
                        vm.cancel(0);
                    }
                    else {
                        vm.formatNumberofSalary();
                    }
                    vm.editFlag = false;
                }).finally(function () {
                    vm.saving = false;
                    if (currentDate == effectiveDate) {
                        abp.notify.error('Please Verify Effective From ' + currentDate);
                        abp.notify.error('Please Verify Effective From ' + currentDate);
                    }
                    vm.editFlag = false;
                });

            };

            vm.cancel = function (argOption) {
                if (vm.slabAddorEditPendingErrorExists() == true)
                    return;

                if (argOption == 1) {
                    abp.message.confirm(
                        app.localize('CancelEntry?'),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                $state.go("tenant.salaryinfo", {
                                    passwordRequired: false
                                });
                            }
                        }
                    );
                }
                else {
                    $state.go("tenant.salaryinfo", {
                        passwordRequired: false
                    });
                }
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.refHourlySalaryModes = [];
            function fillDropDownHourlySalaryModes() {
                salaryinfoService.getHourlySalaryModeForCombobox({}).success(function (result) {
                    vm.refHourlySalaryModes = result.items;
                });
            }
            fillDropDownHourlySalaryModes();


            vm.refSalaryCalculationType = [];
            function fillDropDownSalaryCalculationType() {
                salaryinfoService.getSalaryCalculationTypeForCombobox({}).success(function (result) {
                    vm.refSalaryCalculationType = result.items;
                });
            }
            fillDropDownSalaryCalculationType();

            vm.refIncentiveTags = [];

            function fillDropDownIncentiveTags() {
                vm.loadingCount++;
                incentivetagService.getIncentiveTagCodes({}).success(function (result) {
                    vm.refIncentiveTags = result.items;
                    vm.loadingCount--;
                });
            }

            vm.refOTAndIncentiveCalculationTypes = [];
            function fillDropDownOTAndIncentiveCalculationTypes() {
                salaryinfoService.getOTAndIncentivePeriodTagsForCombobox({}).success(function (result) {
                    vm.refOTAndIncentiveCalculationTypes = result.items;
                });
            }
            fillDropDownOTAndIncentiveCalculationTypes();


            vm.refreshDailyIncentiveList = function () {
                vm.refdailyIncentivePending = [];
                angular.forEach(vm.refIncentiveTags, function (value, key) {
                    var alreadyExist = false;
                    vm.incentiveList.some(function (detvalue, detkey) {
                        if (value.id == detvalue.id) {
                            alreadyExist = true;
                            return true;
                        }
                    });
                    if (alreadyExist == false) {
                        vm.refdailyIncentivePending.push(value);
                    }
                });
            }

            fillDropDownIncentiveTags();

            ////vm.refSalaryTags = [];

            ////function fillDropDownSalaryTagCodes() {
            ////    vm.loadingCount++;
            ////    incentivetagService.getSalaryTagCodes({}).success(function (result) {
            ////        vm.refSalaryTags = result.items;
            ////        vm.loadingCount--;
            ////    });
            ////}

            ////fillDropDownSalaryTagCodes();

            var modalInstance = null;

            vm.addAllowanceOrDeduction = function (argAllowOrDed, argData) {
                if (vm.slabAddorEditPendingErrorExists() == true)
                    return;

                if (vm.salaryinfo.incentiveAndOTCalculationPeriodRefId == 0 || vm.salaryinfo.incentiveAndOTCalculationPeriodRefId == null) {
                    abp.notify.error(app.localize('OT') + ' ' + app.localize('Incentive') + ' ' + app.localize('Period') + ' ?');
                    vm.loading = false;
                    vm.saving = false;
                    return;
                }

                if (vm.editFlag == true) {
                    vm.save(1);

                }

                vm.loading = true;
                vm.newAddPortion = "";

                if (argAllowOrDed == 1) {
                    vm.newAddPortion = "Allowance";
                }
                else if (argAllowOrDed == 2) {
                    vm.newAddPortion = "Deduction";
                }
                else if (argAllowOrDed == 3) {
                    vm.newAddPortion = "Statutory Allowance";
                }
                else if (argAllowOrDed == 4) {
                    vm.newAddPortion = "Statutory Deduction";
                }

                if (argData == null) {
                    argData = {
                        id: null,
                        incentivePayModeId: null,
                        allowanceOrDeductionRefId: argAllowOrDed,
                        employeeRefId: vm.salaryinfo.employeeRefId,
                        employeeRefName: vm.salaryinfo.employeeRefName,
                        dateHired: vm.salaryinfo.dateHired,
                        dateOfBirth: vm.salaryinfo.dateOfBirth,
                        salaryRefId: null,
                        salaryRefName: "",
                        effectiveFrom: null, // moment(),
                        rollBackFrom: null,
                        amount: 0,
                        remarks: "",
                        calculationPayModeRefId: null,
                        calculationPayModeRefName: "",
                        salaryTagAliasName: ""
                    };
                }
                else {
                    argData.employeeRefId = vm.salaryinfo.employeeRefId;
                    if (argData.salaryTagAliasName == '' || argData.salaryTagAliasName == null) {
                        argData.salaryTagAliasName = argData.salaryTagAliasName;
                    }
                }

                argData.employeeRefCode = vm.salaryinfo.employeeRefCode;
                argData.dateHired = vm.salaryinfo.dateHired;
                argData.dateOfBirth = vm.salaryinfo.dateOfBirth;

                var transferDto = angular.copy(argData);

                modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/salaryinfo/allowanceDeduction.cshtml',
                    controller: 'tenant.views.hr.master.salaryinfo.allowanceDeduction as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        allowanceOrDeduction: function () {
                            return vm.newAddPortion;
                        },
                        salaryTag: function () {
                            return transferDto;
                        },
                        employeeSkillSetList: function () {
                            return vm.employeeSkillSetList;
                        }
                    }
                });

                vm.loading = false;

                modalInstance.result.then(function (result) {
                    if (result = true) {
                        init();
                    }
                    vm.loading = false;
                });
            };

            vm.removeAllowance = function (argIncentive) {
                if (vm.slabAddorEditPendingErrorExists() == true)
                    return;

                abp.message.confirm(
                    app.localize('Delete Incentive ' + argIncentive.salaryRefName + " ?"),
                    function (isConfirmed) {
                        if (isConfirmed) {

                            vm.saving = true;
                            vm.loading = true;
                            var successFlag = false;
                            salaryinfoService.deleteAllowanceOrDeduction({
                                employeeVsSalaryTag: argIncentive
                            }).success(function (result) {
                                if (result == true) {
                                    successFlag = true;
                                    abp.notify.info(app.localize("Deleted") + ' ' + app.localize('Successfully'));
                                    init();
                                }
                            }).finally(function () {
                                if (successFlag == false) {
                                    abp.notify.info(app.localize("Deleted") + ' ' + app.localize('Action') + ' ' + app.localize('Failed'));
                                }
                                vm.saving = false;
                                vm.loading = false;
                            });
                        }
                    }
                );
            }

            vm.getEmployeeStaffType = function () {
                personalinformationService.getStaffTypeForSelectedEmp({
                    employeeRefId: vm.salaryinfo.employeeRefId
                }).success(function (result) {
                    vm.personalinfo = result.items;
                    angular.forEach(vm.personalinfo, function (value, key) {
                        if (value.id == vm.salaryinfo.employeeRefId) {
                            // vm.staffType = value.staffType;
                            //if (value.staffType == "Freelancer") {
                            //    document.getElementById("cpf").disabled = true;
                            //}
                        }
                    })
                });
            }

            vm.allowanceSalaryTagList = [];

            vm.deductionSalaryTagList = [];
            vm.statutoryAllowanceSalaryTagList = [];
            vm.statutoryDeductionSalaryTagList = [];

            function init() {
                vm.loadingCount++;
                salaryinfoService.getSalaryInfoForEdit({
                    id: vm.salaryinfoId
                }).success(function (result) {
                    vm.loadingCount--;
                    vm.salaryinfo = result.salaryInfo;
                    if (vm.salaryinfo.id == null) {
                        abp.notify.warn(app.localize('SalaryError'));
                        abp.message.warn(app.localize('SalaryError'));
                        vm.cancel(0);
                    }
                    vm.getEmployeeStaffType();
                    vm.incentiveList = result.incentiveList;
                    vm.allowanceSalaryTagList = result.allowanceSalaryTagList;
                    vm.deductionSalaryTagList = result.deductionSalaryTagList;
                    vm.statutoryAllowanceSalaryTagList = result.statutoryAllowanceList;
                    vm.statutoryDeductionSalaryTagList = result.statutoryDeductionList;
                    vm.employeeSkillSetList = result.skillSetList;
                    vm.manualOTHoursList = result.manualOTHoursList;
                    vm.workDayList = result.workDayListDtos;
                    vm.refreshDailyIncentiveList();


                    vm.otSlabList = [];
                    angular.forEach(result.otSlabList, function (value, key) {
                        //vm.otSlabList.push({
                        //    'id': value.id,
                        //    'employeeRefId': value.employeeRefId,
                        //    'oTRefId': value.oTRefId,
                        //    'fromHours': value.fromHours,
                        //    'toHours': value.toHours,
                        //    'oTAmount': value.oTAmount,
                        //    'editFlag': false,
                        //});
                        vm.otSlabList.push(value);
                    });
                });
            }

            init();

            vm.allowEdit = function () {
                if (vm.salaryinfo.basicSalary == 0 || vm.salaryinfo.basicSalary == null)
                    vm.editFlag = true;
                else {
                    var dayofyear = moment().dayOfYear();
                    var entered = prompt("Please enter PIN");
                    if (entered != dayofyear) {
                        abp.notify.error(app.localize('Wrong'));
                        //$state.go('tenant.employeedashboard');
                        return;
                    }
                    vm.editFlag = true;
                }
            }

            vm.changePicture = function (myObject) {

                if (vm.salaryinfo.employeeRefCode == null || vm.salaryinfo.employeeRefCode == "") {
                    abp.notify.error(app.localize('EmpCodeErr'));
                    return;
                }

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/personalinformation/changeProfilePicture.cshtml',
                    controller: 'tenant.views.hr.master.personalinformation.changeProfilePicture as vm',
                    backdrop: 'static',
                    resolve: {
                        personalinformationId: function () {
                            if (myObject == null) {
                                myObject = vm.salaryinfo.employeeRefCode;
                            }
                            else
                                return myObject;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    if (result != null) {
                        vm.salaryinfo.profilePictureId = result;
                    }
                    else {
                        //personalinformationService.getPersonalInformationForEdit({
                        //    Id: myObject
                        //}).success(function (result) {
                        //    vm.personalinformation.profilePictureId = result.personalInformation.profilePictureId;
                        //});
                    }
                });
            };


            vm.setFullPost = function (el) {
                vm.fullPost = el;
                //vm.setIncentive(vm.fullPost);
            };

            vm.setIncentive = function (argIncentive) {

                vm.saving = true;
                vm.loadingCount++;
                var incentivetransferDto = angular.copy(vm.incentiveList);
                incentivetransferDto.push(argIncentive);
                var addedFlag = false;

                salaryinfoService.checkBusinessRuleForIncentiveTag({
                    salaryInfo: vm.salaryinfo,
                    incentiveList: incentivetransferDto
                }).success(function (result) {
                    if (result == true) {
                        abp.notify.info(app.localize('Validation') + ' ' + app.localize('Done') + ' ' + app.localize('Successfully'));
                        vm.incentiveList.push(argIncentive);
                        vm.refreshDailyIncentiveList();
                        vm.fullPost = null;
                        addedFlag = true;
                    }
                }).finally(function (result) {

                    if (addedFlag == false) {
                        abp.notify.error(app.localize('Validation') + ' ' + app.localize('Done') + ' ' + app.localize('Fail'));
                    }
                    vm.saving = false;
                    vm.loadingCount--;
                });


            }


            ////  Employee Docuemnt Info
            vm.employeeDocumentInfos = [];
            vm.existEmployeeRefId = null;
            vm.getAllEmployeeDocumentInfo = function () {
                vm.loading = true;
                vm.loadingCount++;
                var startDate = null;
                var endDate = null;

                employeedocumentinfoService.getAll({
                    employeeRefId: vm.salaryinfo.employeeRefId,
                    documentInfoRefId: null,
                    startDate: startDate,
                    endDate: endDate,
                    //alertPendingFlag: true,
                    //alertExpiredFlag: true,
                    activeEmployeeOnly: true,
                    sorting: ''
                }).success(function (result) {
                    vm.employeeDocumentInfos = result.items;
                    vm.existEmployeeRefId = vm.currentEmployeeRefId;
                }).finally(function () {
                    vm.loading = false;
                    vm.loadingCount--;
                });
            };

            vm.directDownloadEmployeeDocument = function (item) {
                vm.importpath = abp.appPath + 'PersonalInformation/DownloadPersonalDocumentTemplate?employeeRefId=' + item.employeeRefId + "&documentInfoRefId=" + item.documentInfoRefId;
                window.open(vm.importpath);
            }

            var documentModalInstance = null;
            vm.loginStatus = 'EMP';
            function openCreateOrEditModal(objId) {
                if (vm.loginStatus == 'EMP') {
                    documentModalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/hr/master/employeedocumentinfo/empDocumentInfo.cshtml',
                        controller: 'tenant.views.hr.master.employeedocumentinfo.empDocumentInfo as vm',
                        backdrop: 'static',
                        keyboard: false,
                        resolve: {
                            employeedocumentinfoId: function () {
                                return objId;
                            }
                        }
                    });
                }
                else {
                    var modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/hr/master/employeedocumentinfo/employeeDocumentInfo.cshtml',
                        controller: 'tenant.views.hr.master.employeedocumentinfo.employeeDocumentInfo as vm',
                        backdrop: 'static',
                        keyboard: false,
                        resolve: {
                            employeedocumentinfoId: function () {
                                return objId;
                            }

                        }
                    });
                }



                modalInstance.result.then(function (result) {
                    vm.getAll();
                }).finally(function (result) {
                    vm.getAll();
                });
            }

            // End Employee Document Info -----------------------------


            //  Start Employee Skill Set
            var skillModal = null;
            vm.openEmployeeSkillSetModal = function (employeeRefId) {
                skillModal = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/employeeskillset/createOrEditModal.cshtml',
                    controller: 'tenant.views.hr.master.employeeskillset.createOrEditModal as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        employeeskillsetId: function () {
                            return employeeRefId;
                        }
                    }
                });

                skillModal.result.then(function (result) {
                    if (result != null && result != '') {
                        if (result.length > 0)
                            vm.employeeSkillSetList = result;
                    }
                });
            }
            //  End Employee Skill Set

            //  Salary - Work Day Start
            vm.editWorkDay = function (data) {
                openCreateOrEditWorkDayModal(data.id);
            }

            var modalWorkDayInstance = null;

            function openCreateOrEditWorkDayModal(objId) {
                vm.loading = true;
                modalWorkDayInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/workday/createOrEditModal.cshtml',
                    controller: 'tenant.views.hr.master.workday.createOrEditModal as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        workdayId: function () {
                            return objId;
                        },
                        employeeRefId: function () {
                            return vm.salaryinfo.employeeRefId;
                        },
                        employeeRefCode: function () {
                            return vm.salaryinfo.employeeRefCode;
                        },
                        employeeRefName: function () {
                            return vm.salaryinfo.employeeRefName;
                        }
                    }
                });

                modalWorkDayInstance.result.then(function (result) {
                    if (result == true)
                        init();
                    vm.loading = false;
                });

                vm.loading = false;
            }

            //  Salary  -   Work Day End


            // OT Slab Start
            //  Client Report Template

            vm.refottypes = [];
            vm.refottypes = [];
            function fillDropDownOtTypes() {
                salaryinfoService.getOTTypeTagsForCombobox({}).success(function (result) {
                    vm.refottypes = result.items;
                });
            }
            fillDropDownOtTypes();

            vm.editData = function (data) {
                angular.forEach(vm.otSlabList, function (value, key) {
                    value.editFlag = false;
                });
                data.editFlag = true;
            }

            vm.addOtSlabPortion = function (data, argOption) {
                //IF OT Slabs exists then Salary Info - OT1 and OT2, Report OT Should Be Zero
                if (vm.salaryinfo.ot1PerHour > 0 || vm.salaryinfo.ot2PerHour > 0 || vm.salaryinfo.reportOtPerHour > 0) {
                    abp.notify.error(app.localize('If OTSlab List, Exists then you should change OT1 , OT2 and Report OT Should Be Zero'));
                    return;
                }

                var lastOTRefId = null;
                var nextFromHours = null;

                if (vm.otSlabList.length == 0) {
                    vm.otSlabList.push({
                        'id': null,
                        'employeeRefId': vm.salaryinfo.employeeRefId,
                        'otRefId': lastOTRefId,
                        'fromHours': nextFromHours,
                        'toHours': null,
                        'otAmount': null,
                        'fixedOTAmount': null,
                        'editFlag': true,
                    });
                    return;
                }

                if (vm.otSlabList.length > 0) {

                    var lastelement = data;

                    if (lastelement.employeeRefId == null || lastelement.employeeRefId == "") {
                        abp.notify.info(app.localize('Employee') + ' ?');
                        return;
                    }

                    if (lastelement.otRefId == null || lastelement.otRefId == "" || lastelement.otRefId == 0) {
                        abp.notify.info(app.localize('OT') + ' ' + app.localize('Type') + ' ' + '?');
                        return;
                    }
                    if (lastelement.fromHours == null || lastelement.fromHours == "") {
                        abp.notify.info(app.localize('FromHours'));
                        return;
                    }
                    if (lastelement.toHours == null || lastelement.toHours == "") {
                        abp.notify.info(app.localize('ToHours'));
                        return;
                    }
                    if (lastelement.otAmount == null || lastelement.otAmount == "") {
                        if (lastelement.otAmount == 0) {

                        }
                        else {
                            abp.notify.info(app.localize('OT') + ' ' + app.localize('Amount'));
                            return;
                        }
                    }
                    if (lastelement.otAmount > 0 && lastelement.fixedOTAmount > 0) {
                        abp.notify.info('Either OT Amount or Fixed Amount should have value');
                        return;
                    }

                    angular.forEach(vm.otSlabList, function (value, key) {
                        value.editFlag = false;
                    });

                    data.editFlag = true;
                    lastOTRefId = lastelement.otRefId;
                    nextFromHours = lastelement.toHours;
                }

                salaryinfoService.addOrEditOtSlabs({
                    otBasedOnSlab: data,
                }).success(function (result) {
                    if (result != null) {
                        data.id = result.id;
                        data.editFlag = false;
                        abp.notify.info(app.localize('OT') + ' ' + app.localize('Slabs') + ' ' + app.localize('SavedSuccessfully'));
                        if (argOption == 1) {
                            vm.otSlabList.push({
                                'id': null,
                                'employeeRefId': vm.salaryinfo.employeeRefId,
                                'otRefId': lastOTRefId,
                                'fromHours': nextFromHours,
                                'toHours': null,
                                'otAmount': null,
                                'fixedOTAmount': null,
                                'editFlag': true,
                            });
                        }
                    }
                    else {
                        abp.notify.info(app.localize('OT') + ' ' + app.localize('Slabs') + ' ' + app.localize('Failed'));
                        data.editFlag = true;
                    }
                }).finally(function () {
                    vm.saving = false;
                });
            }

            vm.removeOtSlabRow = function (data) {

                salaryinfoService.deleteOtSlabs({
                    otBasedOnSlab: data,
                }).success(function (result) {
                    abp.notify.info(app.localize('OT') + ' ' + app.localize('Slabs') + ' ' + app.localize('Deleted') + ' ' + app.localize('Failed'));
                    init();
                }).finally(function () {
                    vm.saving = false;
                });

            }

            // OT Slab End



        }
    ]);
})();