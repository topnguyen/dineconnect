﻿(function () {
    appModule.controller('tenant.views.hr.master.salaryinfo.salaryModal', [
        '$scope', '$uibModalInstance', 'salaryPayable', 'abp.services.app.salaryInfo', 'abp.services.app.timeSheetMaster',
        function ($scope, $modalInstance, salaryPayable, salaryinfoService, timesheetmasterService) {
            var vm = this;
            /* eslint-disable */

            vm.loading = false;

            vm.refSalaryCalculationType = [];
            function fillDropDownSalaryCalculationType() {
                salaryinfoService.getSalaryCalculationTypeForCombobox({}).success(function (result) {
                    vm.refSalaryCalculationType = result.items;
                });
            }
            fillDropDownSalaryCalculationType();

            vm.editFlag = false;

            vm.saving = false;
            vm.salaryPayable = salaryPayable;

            vm.cancel = function () {
                $modalInstance.dismiss(null);
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.formatNumberofSalary = function () {
                vm.salaryinfo.basicSalary = vm.salaryinfo.basicSalary.toFixed(2);
                vm.salaryinfo.ot1PerHour = vm.salaryinfo.ot1PerHour.toFixed(2);
                vm.salaryinfo.ot2PerHour = vm.salaryinfo.ot2PerHour.toFixed(2);
                vm.salaryinfo.reportOtPerHour = vm.salaryinfo.reportOtPerHour.toFixed(2);
            }

            vm.excelOrPdf = function (arg) {
                vm.loading = true;
                vm.saving = true;
                timesheetmasterService.getSalarySlipExcelOrPdf({
                    employeeRefId: salaryPayable.inputData.employeeRefId,
                    monthYear: salaryPayable.inputData.monthYear,
                    startDate: salaryPayable.inputData.startDate,
                    endDate: salaryPayable.inputData.endDate,
                    exportTypeRefId: arg
                }).success(function (result) {
                    vm.directDownload(salaryPayable.inputData, arg);
                }).finally(function () {
                    vm.loading = false;
                    vm.saving = false;
                });
            };

            vm.directDownload = function (item, option) {
                extn = '';
                if (option == 1)
                    extn = "pdf";
                else if (option == null || option == 0)
                    extn = "xlsx";

                var monthyearstring = moment(salaryPayable.inputData.monthYear).format('MMMYYYY');
                vm.importpath = abp.appPath + 'PersonalInformation/SalarySlipReportDownload?employeeRefId=' + item.employeeRefId + "&monthYearString=" + monthyearstring + "&fileExtenstionType=" + extn /*+ "&tenantName=" + appSession.tenant.name + ""*/;
                window.open(vm.importpath);
            }
            function init(result) {
                vm.salaryinfo = result.salaryInfo;
                vm.formatNumberofSalary();
                vm.personalInformation = result.personalInformation;
                vm.incentiveList = result.incentiveList;
                vm.allowanceSalaryTagList = result.allowanceSalaryTagList;
                vm.deductionSalaryTagList = result.deductionSalaryTagList;
                vm.statutoryAllowanceTagList = result.statutoryAllowanceTagList;
                vm.statutoryDeductionTagList = result.statutoryDeductionTagList;
                vm.employeeSkillSetList = result.skillSetList;
                vm.incentiveConsolidatedList = result.incentiveConsolidatedList;
                vm.netSalaryDescription = app.localize("SalaryPayableDays", result.salaryCalculatedDays, result.noOfWorkedDays, result.noOfPaidLeaveDays, result.noOfPublicHolidays, result.noOfIdleDays, result.noOfAbsentDays, result.noOfStandardWorkingDays);
            };

            init(vm.salaryPayable);



        }
    ]);
})();

