﻿
(function () {
    appModule.controller('tenant.views.hr.master.salaryinfo.allowanceDeduction', [
        '$scope', '$uibModalInstance', 'allowanceOrDeduction', 'abp.services.app.salaryInfo', 'abp.services.app.incentiveTag', 'salaryTag', 'employeeSkillSetList', 
        function ($scope, $modalInstance, allowanceOrDeduction, salaryinfoService,
            incentivetagService, salaryTag, employeeSkillSetList) {
            var vm = this;
            /* eslint-disable */

            vm.loading = false;

            vm.saving = false;
            vm.allowanceFlag = false;
            vm.deductionFlag = false;
            vm.salaryTagMode = "";
            vm.salaryTag = salaryTag;
            vm.referenceTagList = [];
            vm.employeeSkillSetList = employeeSkillSetList;

            var omittedTagsEmployeeRefId = null;
            if (vm.salaryTag.id == null || vm.salaryTag.id==0) {
                omittedTagsEmployeeRefId = vm.salaryTag.employeeRefId;
            }

            if (allowanceOrDeduction == 'Allowance') {
                vm.allowanceFlag = true;
                vm.salaryTagMode = app.localize("Allowance");
                vm.loadingCount++;
                
                incentivetagService.getAllowanceSalaryTagCodes({
                    employeeRefId: omittedTagsEmployeeRefId
                }).success(function (result) {
                    vm.referenceTagList = result.items;
                    vm.loadingCount--;
                });
            }
            else if (allowanceOrDeduction == 'Deduction') {
                vm.deductionFlag = true;
                vm.salaryTagMode = app.localize("Deduction");
                vm.loadingCount++;
                incentivetagService.getDeductionSalaryTagCodes({
                    employeeRefId: omittedTagsEmployeeRefId
                }).success(function (result) {
                    vm.referenceTagList = result.items;
                    vm.loadingCount--;
                });
            }
            else if (allowanceOrDeduction == 'Statutory Allowance') {
                vm.deductionFlag = true;
                vm.salaryTagMode = app.localize("Statutory Allowance");
                vm.loadingCount++;
                incentivetagService.getStatutoryAllowanceSalaryTagCodes({
                    employeeRefId: omittedTagsEmployeeRefId
                }).success(function (result) {
                    vm.referenceTagList = result.items;
                    vm.loadingCount--;
                });
            }
            else if (allowanceOrDeduction == 'Statutory Deduction') {
                vm.deductionFlag = true;
                vm.salaryTagMode = app.localize("Statutory Deduction");
                vm.loadingCount++;
                incentivetagService.getStatutoryDeductionSalaryTagCodes({
                    employeeRefId: omittedTagsEmployeeRefId
                }).success(function (result) {
                    vm.referenceTagList = result.items;
                    vm.loadingCount--;
                });
            }

            if (vm.salaryTag.effectiveFrom == "0001-01-01T00:00:00" || vm.salaryTag.effectiveFrom == null) {
                $scope.dt1 = new Date();
                vm.salaryTag.effectiveFrom = null; // $scope.dt1;
            }
            else {
                $scope.projStDate = new Date(vm.salaryTag.effectiveFrom);
                vm.salaryTag.effectiveFrom = $scope.projStDate;
            }


            vm.getIncentive = function (item) {
                var t = item;
            }

            //date start
            //  Date Range Options
            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            //  Date Angular Js 
            $scope.today = function () {
                $scope.dt = moment(new Date()).format("DD-MMM-YYYY");
            };

            $scope.today();

            $scope.clear = function () {
                $scope.dt = null;
            };

            $scope.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            $scope.dateOptions = {
                dateDisabled: disabled,
                formatYear: 'yy',
                maxDate: new Date(2030, 5, 22),
                minDate: new Date(),
                startingDay: 1,
                showWeeks: false
            };

            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                    mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }

            $scope.toggleMin = function () {
                $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
                $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
            };

            $scope.toggleMin();

            $scope.open1 = function () {
                $scope.popup1.opened = true;
            };

            $scope.open2 = function () {
                $scope.popup2.opened = true;
            };

            $scope.setDate = function (year, month, day) {
                $scope.dt = new Date(year, month, day);
            };

            $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];
            //$scope.altInputFormats = ['M!/d!/yyyy'];
            $scope.altInputFormats = ['dd-MMMM-yyyy'];

            $scope.popup1 = {
                opened: false
            };

            $scope.popup2 = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            $scope.events = [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];

            function getDayClass(data) {
                var date = data.date,
                    mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            }

            //date end


            //function CompareDate() {
            //    var startDate = $('#stDate').val();
            //    var endDate = $('#endDate').val();

            //    if (startDate > endDate) {
            //        abp.notify.warn(app.localize('ReqDateErr'));
            //        abp.message.info(app.localize('ReqDateErr'));
            //        //alert("End date can be greater than Start date");
            //        return false;
            //    }
            //}

            vm.cancel = function () {
                $modalInstance.dismiss(null);
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.refincentivePayMode = [];
            function fillDropDownIncentivePayModes() {
                incentivetagService.getIncentivePayModeForCombobox({}).success(function (result) {
                    vm.refincentivePayMode = result.items;
                });
            }
            fillDropDownIncentivePayModes();

            vm.validationStart = false;

            vm.save = function () {

                if (vm.salaryTag.employeeRefId == null || vm.salaryTag.employeeRefId == 0) {
                    abp.notify.error(app.localize('Employee') + " ?");
                    return;
                }

                if (vm.salaryTag.salaryRefId == null || vm.salaryTag.salaryRefId == 0) {
                    abp.notify.error(app.localize('Salary') + " " + app.localize('Tag') + " ?");
                    return;
                }

                if (vm.salaryTag.effectiveFrom == null) {
                    abp.notify.error(app.localize('Effective') + " " + app.localize('Date') + " ?");
                    return;
                }

                var currentDate = moment().format('DD-MMM-YYYY');
                var effectiveDate = moment(vm.salaryTag.effectiveFrom).format('DD-MMM-YYYY'); //moment(vm.salaryTag.effectiveFrom, 'DD-MMM-YYYY').format('DD-MMM-YYYY');

                if (currentDate == effectiveDate) {
                    if (vm.validationStart == false) {
                        vm.validationStart = true;
                        abp.message.confirm(
                            app.localize('Effective Date From Today ' + currentDate + " ?"),
                            function (isConfirmed) {
                                if (isConfirmed) {
                                    vm.save();
                                }
                                else {
                                    vm.validationStart = false;
                                }
                            }
                        );
                    }
                    else {
                        vm.validationStart = false;
                    }
                }
                else {
                    vm.validationStart = false;
                }

                if (vm.validationStart == true) {
                    return;
                }



                vm.saving = true;

                vm.salaryTag.effectiveFrom = moment(vm.salaryTag.effectiveFrom).format('YYYY-MM-DD');
                vm.salaryTag.rollBackFrom = null; // moment(vm.salaryTag.roll).format('YYYY-MM-DD');

                //if (CompareDate() == false) {
                //    vm.saving = false;
                //    vm.salaryTag.effectiveFrom = null;
                //    vm.salaryTag.rollBackFrom = null;
                //    return;
                //}

                var successFlag = false;
                salaryinfoService.addOrEditAllowanceOrDeduction({
                    employeeVsSalaryTag: vm.salaryTag
                }).success(function (result) {
                    successFlag = true;
                    abp.notify.info(vm.salaryTagMode + ' ' + app.localize('SavedSuccessfully'));
                    $modalInstance.close(true);
                    }).finally(function () {
                        if (successFlag == false) {
                            if (vm.salaryTag.effectiveFrom == "0001-01-01T00:00:00" || vm.salaryTag.effectiveFrom == null) {
                                $scope.dt1 = new Date();
                                vm.salaryTag.effectiveFrom = $scope.dt1;
                            }
                            else {
                                $scope.projStDate = new Date(vm.salaryTag.effectiveFrom);
                                vm.salaryTag.effectiveFrom = $scope.projStDate;
                            }
                        }
                    vm.saving = false;
                });
            };


            function init() {
                //fillDropDownClient();
                //            clientprojectService.getClientProjectForEdit({
                //                Id: clientprojectId
                //            }).success(function (result) {

                //                vm.clientproject = result.clientProject;
                //                if (vm.clientproject.projStDate == "0001-01-01T00:00:00" || vm.clientproject.projFinDate == "0001-01-01T00:00:00") {
                //                    $scope.dt1 = new Date();
                //                    $scope.dt2 = new Date();
                //                    vm.clientproject.projStDate = $scope.dt1;
                //                    vm.clientproject.projFinDate = $scope.dt2;
                //                }
                //                else {
                //                    $scope.projStDate = new Date(vm.clientproject.projStDate);
                //                    vm.clientproject.projStDate = $scope.projStDate;
                //                    $scope.projFinDate = new Date(vm.clientproject.projFinDate);
                //                    vm.clientproject.projFinDate = $scope.projFinDate;
                //                }

                //                if (vm.clientproject.id==null)
                //                {

                //                }
                //                else
                //                {
                //                    vm.getClientLocation();
                //                }

                //            });
            }

            init();

            //  Start


            $scope.fullscreenMsg = '';

            $scope.posts = [{
                title: "Title1",
                message: "Message1"
            }, {
                title: "Title2",
                message: "Message2"
            }, {
                title: "Title3",
                message: "Message3"
            }, {
                title: "Title4",
                message: "Message4"
            }, {
                title: "Title5",
                message: "Message5"
            }, {
                title: "Title6",
                message: "Message6"
            }, {
                title: "Title7",
                message: "Message7"
            }, {
                title: "Title8",
                message: "Message8"
            }, {
                title: "Title9",
                message: "Message9"
            }, {
                title: "Title10",
                message: "Message10"
            }];

            vm.setFullPost = function (el) {
                vm.fullPost = el;

                vm.setIncentive(vm.fullPost);
            };

            vm.setIncentive = function (argIncentive) {

                if (vm.salaryTag.id != null || vm.salaryTag.id > 0) {
                    abp.notify.error("You can not change the Salary Tag once created, If you want to change then you need to delete this tag and create a new one !!!");
                    return;
                }

                vm.salaryTag.salaryRefId = argIncentive.id;
                if (vm.salaryTag.amount==0 || vm.salaryTag.amount==null)
                    vm.salaryTag.amount = argIncentive.incentiveAmount;

                vm.salaryTag.salaryTagAliasName = argIncentive.incentiveTagCode;
                vm.salaryTag.calculationPayModeRefId = 2;
                vm.salaryTag.isSytemGeneratedIncentive = argIncentive.isSytemGeneratedIncentive;
                vm.salaryTag.isFixedAmount = argIncentive.isFixedAmount;
                $('#Amount').focus();
                
            }
        }
    ]);
})();

