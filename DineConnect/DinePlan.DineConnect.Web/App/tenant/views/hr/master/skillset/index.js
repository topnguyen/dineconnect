﻿
(function () {
    appModule.controller('tenant.views.hr.master.skillset.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.skillSet', 'abp.services.app.setup', 'appSession', 'abp.services.app.personalInformation',
        function ($scope, $modal, uiGridConstants, skillsetService, setupService, appSession, personalinformationService) {
            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.loadingCount = 0;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.locationRefId = appSession.location.id;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Hr.Master.SkillSet.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Hr.Master.SkillSet.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Hr.Master.SkillSet.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
					{
						name: app.localize('Actions'),
						enableSorting: false,
						width: 120,

						cellTemplate:
						   "<div class=\"ui-grid-cell-contents\">" +
							   "  <div class=\"btn-group dropdown\" uib-dropdown=\"\">" +
							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
							   "    <ul uib-dropdown-menu>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editSkillSet(row.entity)\">" + app.localize("Edit") + "</a></li>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteSkillSet(row.entity)\">" + app.localize("Delete") + "</a></li>" +
							   "    </ul>" +
							   "  </div>" +
							   "</div>"
					},
                    {
                        name: app.localize('Code'),
                        field: 'skillCode'
                    },
                    {
                        name: app.localize('Description'),
                        field: 'description'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

         

            vm.getAll = function () {
                vm.loading = true;
                skillsetService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editSkillSet = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createSkillSet = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteSkillSet = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteSkillSetWarning', myObject.code),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            skillsetService.deleteSkillSet({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/skillset/createOrEditModal.cshtml',
                    controller: 'tenant.views.hr.master.skillset.createOrEditModal as vm',
                    backdrop: 'static',
					keyboard: false,
                    resolve: {
                        skillsetId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.exportToExcel = function () {
                skillsetService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getAll();

            vm.setupPer = {
                setup: abp.auth.hasPermission('Pages.Tenant.Setup'),
                importData: abp.auth.hasPermission('Pages.Tenant.Setup.ImportSeedData'),
                clearData: abp.auth.hasPermission('Pages.Tenant.Setup.CleanSeedData')
            };


            vm.importTickData = function () {
                abp.message.confirm(
                    app.localize('ImportWarning'),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            vm.loading = true;
                            vm.loadingCount++;
                            setupService.seedTickAll({
                                id: vm.locationRefId
                            }
                            )
                                .success(function () {
                                    abp.notify.success(app.localize('Master') + ' ' + app.localize('SuccessfullyImported'));
                                    vm.loadingCount++;
                                    personalinformationService.importPersonalInformationSampleList({
                                        id: vm.locationRefId
                                    }).success(function (result) {
                                        abp.notify.success(app.localize('Employee') + ' ' + app.localize('SuccessfullyImported'));
                                        vm.getAll();
                                    }).finally(function () {
                                        vm.loading = false;
                                        vm.loadingCount--;
                                    });
                                }).finally(function () {
                                    vm.loading = false;
                                    vm.loadingCount--;
                                });

                        }
                    }
                );
            }

            vm.cleanTickAll = function () {
                abp.message.confirm(
                    app.localize('CleanAllWarning'),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            vm.loading = true;
                            setupService.clearTickTransaction()
                                .success(function () {
                                    abp.notify.success(app.localize('SuccessfullyCleared'));
                                    vm.loading = false;
                                    vm.getAll();
                                });
                        }
                    }
                );
            };

        }]);
})();

