﻿
(function () {
    appModule.controller('tenant.views.hr.master.skillset.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.skillSet', 'skillsetId', 
        function ($scope, $modalInstance, skillsetService, skillsetId) {
            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            vm.skillset = null;
			$scope.existall = true;

            vm.save = function () {
                vm.saving = true;
				
                vm.skillset.skillCode = vm.skillset.skillCode.toUpperCase();

                skillsetService.createOrUpdateSkillSet({
                    skillSet: vm.skillset
                }).success(function () {
                    abp.notify.info(app.localize('SkillSet') + ' ' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
	        function init() {
                skillsetService.getSkillSetForEdit({
                    Id: skillsetId
                }).success(function (result) {
                    vm.skillset = result.skillSet;
                });
            }
            init();
        }
    ]);
})();

