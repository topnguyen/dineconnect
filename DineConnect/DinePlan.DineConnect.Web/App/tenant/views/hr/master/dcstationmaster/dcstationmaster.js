﻿(function () {
    appModule.controller('tenant.views.hr.master.dcstationmaster.dcstationmaster', [
        '$scope', '$uibModalInstance', '$uibModal', 'abp.services.app.dcStationMaster', 'dcstationmasterId', 'abp.services.app.dcHeadMaster', 'abp.services.app.dcGroupMaster', 'appSession', 'abp.services.app.personalInformation',
        function ($scope, $modalInstance, $modal, dcstationmasterService, dcstationmasterId, dcheadmasterService, dcgroupmasterService, appSession, personalinformationService) {
            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            vm.dcstationmaster = null;
            vm.showErrorFlag = false;

            vm.save = function () {

                if (parseFloat(vm.dcstationmaster.minimumStaffRequired) > parseFloat(vm.dcstationmaster.maximumStaffRequired)) {
                    abp.notify.error(app.localize('MinimumStaffRequiredShouldNotBeGreaterThanMaximum', vm.dcstationmaster.minimumStaffRequired, vm.dcstationmaster.maximumStaffRequired));
                    return;
                }

                vm.dcstationmaster.timeIn = parseInt(vm.timeinhours) * 60 + parseInt(vm.timeinminutes);
                vm.dcstationmaster.timeOut = parseInt(vm.timeouthours) * 60 + parseInt(vm.timeoutminutes);

                if (vm.dcstationmaster.breakFlag == true) {
                    vm.dcstationmaster.breakIn = parseInt(vm.breakinhours) * 60 + parseInt(vm.breakinminutes);
                    vm.dcstationmaster.breakOut = parseInt(vm.breakouthours) * 60 + parseInt(vm.breakoutminutes);
                }
                else {
                    vm.dcstationmaster.breakIn = 0;
                    vm.dcstationmaster.breakOut = 0;
                }

                if (vm.dcstationmaster.nightFlag == false) {
                    if (vm.dcstationmaster.timeOut < vm.dcstationmaster.timeIn) {
                        errorFlag = true;
                        abp.notify.error(app.localize('NightFlagInfo'));
                        vm.saving = false;
                        return;
                    }
                }

                if (vm.dcstationmaster.breakFlag == false) {
                    if (vm.dcstationmaster.nightFlag == false) {
                        if (vm.dcstationmaster.timeIn > vm.dcstationmaster.timeOut) {
                            errorFlag = true;
                            abp.notify.error(app.localize('NightFlagInfo'));
                            vm.saving = false;
                            return;
                        }
                    }
                }
                else if (vm.dcstationmaster.breakFlag == true) {
                    if (vm.dcstationmaster.nightFlag == false) {
                        if (vm.dcstationmaster.timeIn > vm.dcstationmaster.breakOut) {
                            errorFlag = true;
                            abp.notify.error(app.localize('TimeInBreakOutInfo'));
                            vm.saving = false;
                            return;
                        }
                        if (vm.dcstationmaster.breakOut > vm.dcstationmaster.breakIn) {
                            errorFlag = true;
                            abp.notify.error(app.localize('BreakFlagInfo'));
                            vm.saving = false;
                            return;
                        }
                        if (vm.dcstationmaster.breakIn > vm.dcstationmaster.timeOut) {
                            errorFlag = true;
                            abp.notify.error(app.localize('TimeOutBreakInInfo'));
                            vm.saving = false;
                            return;
                        }
                    }
                }


                vm.saving = true;
                vm.loadingCount++;

                vm.dcstationmaster.stationName = vm.dcstationmaster.stationName.toUpperCase();

                dcstationmasterService.createOrUpdateDcStationMaster({
                    dcStationMaster: vm.dcstationmaster
                }).success(function () {
                    abp.notify.info(app.localize('DcStationMaster') + ' ' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                    vm.loadingCount--;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.refdcheadmaster = [];

            function fillDropDownDcHeadMaster() {
                dcheadmasterService.getHeadNames({}).success(function (result) {
                    vm.refdcheadmaster = result.items;
                });
            }
            fillDropDownDcHeadMaster();

            vm.refdcgroupmaster = [];
            vm.fillDropDownDcGroupMaster = function () {
                vm.loadinCount++;
                dcgroupmasterService.getGroupNames({
                    dcHeadRefId: vm.dcstationmaster.dcHeadRefId
                }).success(function (result) {
                    vm.refdcgroupmaster = result.items;
                }).finally(function () {
                    vm.loadingCount--;
                });
            }

            function init() {

                dcstationmasterService.getDcStationMasterForEdit({
                    id: dcstationmasterId
                }).success(function (result) {
                    vm.dcstationmaster = result.dcStationMaster;
                    if (vm.dcstationmaster.id == null) {
                        vm.getMaxUserSerialNumber();
                        vm.dcstationmaster.locationRefId = appSession.location.id;
                        vm.currentLocation = appSession.location.name;
                        vm.currentCompanyRefId = appSession.location.companyRefId;
                        vm.getCompanyName();
                        vm.dcstationmaster.isMandatory = true;
                        vm.dcstationmaster.isActive = true;
                        vm.dcstationmaster.minimumStaffRequired = 1;
                        vm.dcstationmaster.maximumStaffRequired = 1;

                        vm.dcstationmaster.timeIn = "";
                        vm.dcstationmaster.timeOut = "";
                        vm.dcstationmaster.breakIn = "";
                        vm.dcstationmaster.breakOut = "";
                    }
                    else {
                        vm.fillDropDownDcGroupMaster();
                        vm.currentLocation = vm.dcstationmaster.locationRefName;
                        vm.currentCompany = vm.dcstationmaster.companyRefName;
                        vm.currentCompanyRefId = vm.dcstationmaster.companyRefId;
                        vm.fillDropDownDcGroupMaster();

                        vm.timeinhours = $scope.getHours(parseInt(vm.dcstationmaster.timeIn));
                        vm.timeinminutes = $scope.getMinutes(parseInt(vm.dcstationmaster.timeIn));
                        vm.breakinhours = $scope.getHours(parseInt(vm.dcstationmaster.breakIn));
                        vm.breakinminutes = $scope.getMinutes(parseInt(vm.dcstationmaster.breakIn));

                        vm.timeouthours = $scope.getHours(parseInt(vm.dcstationmaster.timeOut));
                        vm.timeoutminutes = $scope.getMinutes(parseInt(vm.dcstationmaster.timeOut));
                        vm.breakouthours = $scope.getHours(parseInt(vm.dcstationmaster.breakOut));
                        vm.breakoutminutes = $scope.getMinutes(parseInt(vm.dcstationmaster.breakOut));
                    }

                    vm.setTimeDesc();

                });
            }

            init();

            vm.getMaxUserSerialNumber = function () {
                vm.loadingCount++;
                dcstationmasterService.getMaxUserSerialNumber({
                }).success(function (result) {
                    vm.dcstationmaster.priorityLevel = result;
                }).finally(function () {
                    vm.loadingCount--;
                });
            }


            //------        Time Handling Start
            $scope.getHours = function (mts) {
                var hrs = Math.floor(mts / 60);
                return hrs.toString();
            }

            $scope.getMinutes = function (mts) {
                var minutes = mts % 60;
                return minutes.toString();
            }

            vm.setTimeDesc = function () {
                vm.dcstationmaster.timeDescription = app.localize('TI') + " : " + vm.timeinhours + ':' + String("0" + vm.timeinminutes).slice(-2);

                if (vm.dcstationmaster.nightFlag == true)
                    vm.dcstationmaster.timeDescription = vm.dcstationmaster.timeDescription + ' (N)';

                if (vm.dcstationmaster.breakFlag == true) {
                    vm.dcstationmaster.timeDescription = vm.dcstationmaster.timeDescription + ' - ' + app.localize('BO') + " : " + vm.breakouthours + ':' + String("0" + vm.breakoutminutes).slice(-2);  
                    vm.dcstationmaster.timeDescription = vm.dcstationmaster.timeDescription + ' - ' + app.localize('BI') + " : " + vm.breakinhours + ':' + String("0" + vm.breakinminutes).slice(-2);   
                }

                vm.dcstationmaster.timeDescription = vm.dcstationmaster.timeDescription + " - " + app.localize('TO') + " : " + vm.timeouthours + ':' + String("0" + vm.timeoutminutes).slice(-2);
            }


            vm.timeinhours = "8";
            vm.timeinminutes = "0";

            vm.timeouthours = "17";
            vm.timeoutminutes = "0";

            vm.breakinhours = "8";
            vm.breakinminutes = "0";

            vm.breakouthours = "17";
            vm.breakoutminutes = "0";


            //-------       Time End 

            vm.getCompanyName = function () {
                vm.loadingCount++;
                personalinformationService.getCompanyNameById({
                    id: vm.currentCompanyRefId
                }).success(function (result) {
                    vm.currentCompany = result;
                }).finally(function () {
                    vm.loadingCount--;
                });
            }



            vm.openLocation = function () {

                vm.locationGroup = app.createLocationInputForCreateSingleLocation();

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        if (result.locations[0].id != vm.dcstationmaster.locationRefId) {
                            vm.dcstationmaster.locationRefId = result.locations[0].id;
                            vm.currentLocation = result.locations[0].name;
                            vm.companyRefId = result.locations[0].companyRefId;
                            vm.getCompanyName();
                        }
                    }

                });

            }

        }
    ]);
})();

