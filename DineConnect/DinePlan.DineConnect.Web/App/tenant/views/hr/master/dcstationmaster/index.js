﻿(function () {
    appModule.controller('tenant.views.hr.master.dcstationmaster.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.dcStationMaster', 'abp.services.app.dcHeadMaster', 'abp.services.app.dcShiftMaster',
        function ($scope, $modal, uiGridConstants, dcstationmasterService, dcheadmasterService, dcshiftmasterService) {
            var vm = this;
/* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Hr.Master.DcStationMaster.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Hr.Master.DcStationMaster.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Hr.Master.DcStationMaster.Delete'),
                cloneAsShift: abp.auth.hasPermission('Pages.Tenant.Hr.Master.DcShiftMaster.Create'),
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.refdcheadmaster = [];

            function fillDropDownDcHeadMaster() {
                dcheadmasterService.getHeadNames({}).success(function (result) {
                    vm.refdcheadmaster = result.items;
                });
            }
            fillDropDownDcHeadMaster();

            vm.userGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
					{
						name: app.localize('Actions'),
						enableSorting: false,
						width: 120,

						cellTemplate:
						   "<div class=\"ui-grid-cell-contents\">" +
							    "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
							   "    <ul uib-dropdown-menu>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editDcStationMaster(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteDcStationMaster(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.cloneAsShift\" ng-click=\"grid.appScope.cloneAsShift(row.entity)\">" + app.localize("CloneAsShift") + "</a></li>" +
							   "    </ul>" +
							   "  </div>" +
							   "</div>"
                    },
                    {
                        name: app.localize('Location'),
                        field: 'locationRefName',
                        maxWidth: 100
                    },
                    {
                        name: app.localize('GroupName'),
                        field: 'dcGroupRefName'
                    },
                    {
                        name: app.localize('StationName'),
                        field: 'stationName'
                    },
                    {
                        name: app.localize('MinimumStaffRequired'),
                        field: 'minimumStaffRequired',
                        maxWidth : 100
                    },
                    {
                        name: app.localize('MaximumStaffRequired'),
                        field: 'maximumStaffRequired',
                        maxWidth: 100
                    },
                    {
                        name: app.localize('IsMandatory'),
                        field: 'isMandatory',
                        maxWidth: 60,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.isMandatory" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.isMandatory" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                    },
                    {
                        name: app.localize('Break'),
                        field: 'breakFlag',
                        enableSorting: false,
                        maxWidth: 60,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.breakFlag" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.breakFlag" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                    },
                    {
                        name: app.localize('WorkTime'),
                        field: 'timeDescription',
                        enableSorting: false,
                    },
                   
                    //{
                    //    name: app.localize('BreakTime'),
                    //    field: 'breakTimeDesc'
                    //},
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

         

            vm.getAll = function () {
                vm.loading = true;
                dcstationmasterService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    stationName : vm.stationName
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editDcStationMaster = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createDcStationMaster = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteDcStationMaster = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteDcStationMasterWarning', myObject.groupName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            dcstationmasterService.deleteDcStationMaster({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/dcstationmaster/dcstationmaster.cshtml',
                    controller: 'tenant.views.hr.master.dcstationmaster.dcstationmaster as vm',
                    backdrop: 'static',
					keyboard: false,
                    resolve: {
                        dcstationmasterId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.exportToExcel = function () {
                dcstationmasterService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.cloneAsShift = function (myObject) {
                abp.message.confirm(
                    app.localize("CloneStationAsShiftWarning", myObject.stationName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            dcshiftmasterService.cloneStationAsShift({
                                id: myObject.id
                            }).success(function (result) {
                                if (result !=null && result.id >0) {
                                    abp.notify.success(app.localize("Successfully") + ' ' + app.localize('Shift') + ' ' + app.localize('Cloned'));

                                    abp.message.confirm(
                                        app.localize('DoYouWantToView', app.localize('Shift')),
                                        function (isConfirmed) {
                                            if (isConfirmed) {
                                                openShiftModal(result.id);
                                            }
                                        }
                                    );
                                }
                            });
                        }
                    }
                );
            };


            function openShiftModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/dcshiftmaster/dcshiftmaster.cshtml',
                    controller: 'tenant.views.hr.master.dcshiftmaster.dcshiftmaster as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        dcshiftmasterId: function () {
                            return objId;
                        }
                    }
                });
            }

            vm.getAll();

        }]);
})();