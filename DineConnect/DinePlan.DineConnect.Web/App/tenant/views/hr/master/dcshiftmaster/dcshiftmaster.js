﻿(function () {
    appModule.controller('tenant.views.hr.master.dcshiftmaster.dcshiftmaster', [
        '$scope', '$uibModalInstance', 'abp.services.app.dcShiftMaster', 'dcshiftmasterId', 'abp.services.app.dcHeadMaster', 'abp.services.app.dcGroupMaster', 'appSession', 'abp.services.app.personalInformation', 'abp.services.app.dcStationMaster',
        function ($scope, $modalInstance, dcshiftmasterService, dcshiftmasterId, dcheadmasterService, dcgroupmasterService, appSession, personalinformationService, dcstationmasterService) {
            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            vm.dcshiftmaster = null;
            vm.showErrorFlag = false;

            vm.save = function () {
                if (parseFloat(vm.dcshiftmaster.minimumStaffRequired) > parseFloat(vm.dcshiftmaster.maximumStaffRequired)) {
                    abp.notify.error(app.localize('MinimumStaffRequiredShouldNotBeGreaterThanMaximum', vm.dcshiftmaster.minimumStaffRequired, vm.dcshiftmaster.maximumStaffRequired));
                    return;
                }

                vm.dcshiftmaster.timeIn = parseInt(vm.timeinhours) * 60 + parseInt(vm.timeinminutes);
                vm.dcshiftmaster.timeOut = parseInt(vm.timeouthours) * 60 + parseInt(vm.timeoutminutes);

                if (vm.dcshiftmaster.breakFlag == true) {
                    vm.dcshiftmaster.breakIn = parseInt(vm.breakinhours) * 60 + parseInt(vm.breakinminutes);
                    vm.dcshiftmaster.breakOut = parseInt(vm.breakouthours) * 60 + parseInt(vm.breakoutminutes);
                }
                else {
                    vm.dcshiftmaster.breakIn = 0;
                    vm.dcshiftmaster.breakOut = 0;
                }

                if (vm.dcshiftmaster.nightFlag == false) {
                    if (vm.dcshiftmaster.timeOut < vm.dcshiftmaster.timeIn) {
                        errorFlag = true;
                        abp.notify.error(app.localize('NightFlagInfo'));
                        vm.saving = false;
                        return;
                    }
                }

                if (vm.dcshiftmaster.breakFlag == false) {
                    if (vm.dcshiftmaster.nightFlag == false) {
                        if (vm.dcshiftmaster.timeIn > vm.dcshiftmaster.timeOut) {
                            errorFlag = true;
                            abp.notify.error(app.localize('NightFlagInfo'));
                            vm.saving = false;
                            return;
                        }
                    }
                }
                else if (vm.dcshiftmaster.breakFlag == true) {
                    if (vm.dcshiftmaster.nightFlag == false) {
                        if (vm.dcshiftmaster.timeIn > vm.dcshiftmaster.breakOut) {
                            errorFlag = true;
                            abp.notify.error(app.localize('TimeInBreakOutInfo'));
                            vm.saving = false;
                            return;
                        }
                        if (vm.dcshiftmaster.breakOut > vm.dcshiftmaster.breakIn) {
                            errorFlag = true;
                            abp.notify.error(app.localize('BreakFlagInfo'));
                            vm.saving = false;
                            return;
                        }
                        if (vm.dcshiftmaster.breakIn >= vm.dcshiftmaster.timeOut) {
                            errorFlag = true;
                            abp.notify.error(app.localize('TimeOutBreakInInfo'));
                            vm.saving = false;
                            return;
                        }
                    }
                }

                vm.saving = true;
                vm.loadingCount++;

                vm.dcshiftmaster.dutyDesc = vm.dcshiftmaster.dutyDesc.toUpperCase();

                dcshiftmasterService.createOrUpdateDcShiftMaster({
                    dcShiftMaster: vm.dcshiftmaster
                }).success(function () {
                    abp.notify.info(app.localize('DcShiftMaster') + ' '  + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {

                if (vm.dcshiftmaster.dutyDesc == null) {
                    vm.existall = false;
                    return;
                }

                dcshiftmasterService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'dutyDesc',
                    filter: vm.dcshiftmaster.dutyDesc,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.dcshiftmaster.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.dcshiftmaster.dutyDesc + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.refdcheadmaster = [];

            function fillDropDownDcHeadMaster() {
                dcheadmasterService.getHeadNames({}).success(function (result) {
                    vm.refdcheadmaster = result.items;
                });
            }
            fillDropDownDcHeadMaster();

            vm.refdcstationmaster = [];
            vm.fillDropDownDcStationMaster = function () {
                vm.loadinCount++;
                dcstationmasterService.getStationNames({
                    locationRefId : vm.dcshiftmaster.locationRefId,
                    dcGroupRefId: vm.dcshiftmaster.dcGroupRefId
                }).success(function (result) {
                    vm.refdcstationmaster = result.items;
                    if (vm.refdcstationmaster.length == 1) {
                        vm.dcshiftmaster.stationRefId = vm.refdcstationmaster[0].id;
                    }
                }).finally(function () {
                    vm.loadingCount--;
                });
            }
            

            vm.refdcgroupmaster = [];
            vm.fillDropDownDcGroupMaster = function () {
                vm.loadinCount++;
                dcgroupmasterService.getGroupNames({
                    dcHeadRefId: vm.dcshiftmaster.dcHeadRefId
                }).success(function (result) {
                    vm.refdcgroupmaster = result.items;
                }).finally(function () {
                    vm.loadingCount--;
                    vm.fillDropDownDcStationMaster();
                });
            }

            vm.handleTime = function (argItem) {
                vm.timeinhours = $scope.getHours(parseInt(argItem.timeIn));
                vm.timeinminutes = $scope.getMinutes(parseInt(argItem.timeIn));
                vm.breakinhours = $scope.getHours(parseInt(argItem.breakIn));
                vm.breakinminutes = $scope.getMinutes(parseInt(argItem.breakIn));

                vm.timeouthours = $scope.getHours(parseInt(argItem.timeOut));
                vm.timeoutminutes = $scope.getMinutes(parseInt(argItem.timeOut));
                vm.breakouthours = $scope.getHours(parseInt(argItem.breakOut));
                vm.breakoutminutes = $scope.getMinutes(parseInt(argItem.breakOut));
            }

            function init() {
                dcshiftmasterService.getDcShiftMasterForEdit({
                    Id: dcshiftmasterId
                }).success(function (result) {
                    vm.dcshiftmaster = result.dcShiftMaster;
                    if (vm.dcshiftmaster.id == null) {
                        vm.getMaxUserSerialNumber();
                        vm.dcshiftmaster.locationRefId = appSession.location.id;
                        vm.currentLocation = appSession.location.name;
                        vm.currentCompanyRefId = appSession.location.companyRefId;
                        vm.getCompanyName();
                        vm.dcshiftmaster.isMandatory = true;
                        vm.dcshiftmaster.isActive = true;
                        vm.dcshiftmaster.minimumStaffRequired = 1;
                        vm.dcshiftmaster.maximumStaffRequired = 1;

                        vm.dcshiftmaster.timeIn = "";
                        vm.dcshiftmaster.timeOut = "";
                        vm.dcshiftmaster.breakIn = "";
                        vm.dcshiftmaster.breakOut = "";
                    }
                    else {
                        vm.currentLocation = vm.dcshiftmaster.locationRefName;
                        vm.currentCompany = vm.dcshiftmaster.companyRefName;
                        vm.currentCompanyRefId = vm.dcshiftmaster.companyRefId;
                        vm.fillDropDownDcGroupMaster();
                        

                        vm.timeinhours = $scope.getHours(parseInt(vm.dcshiftmaster.timeIn));
                        vm.timeinminutes = $scope.getMinutes(parseInt(vm.dcshiftmaster.timeIn));
                        vm.breakinhours = $scope.getHours(parseInt(vm.dcshiftmaster.breakIn));
                        vm.breakinminutes = $scope.getMinutes(parseInt(vm.dcshiftmaster.breakIn));

                        vm.timeouthours = $scope.getHours(parseInt(vm.dcshiftmaster.timeOut));
                        vm.timeoutminutes = $scope.getMinutes(parseInt(vm.dcshiftmaster.timeOut));
                        vm.breakouthours = $scope.getHours(parseInt(vm.dcshiftmaster.breakOut));
                        vm.breakoutminutes = $scope.getMinutes(parseInt(vm.dcshiftmaster.breakOut));
                    }

                    vm.setTimeDesc();
                });
            }
            init();

            vm.getMaxUserSerialNumber = function () {
                vm.loadingCount++;
                dcshiftmasterService.getMaxUserSerialNumber({
                }).success(function (result) {
                    vm.dcshiftmaster.priorityLevel = result;
                }).finally(function () {
                    vm.loadingCount--;
                });
            }


            //------        Time Handling Start
            $scope.getHours = function (mts) {
                var hrs = Math.floor(mts / 60);
                return hrs.toString();
            }

            $scope.getMinutes = function (mts) {
                var minutes = mts % 60;
                return minutes.toString();
            }

            vm.setTimeDesc = function () {
                vm.dcshiftmaster.timeDescription = app.localize('TI') + " : " + vm.timeinhours + ':' + String("0" + vm.timeinminutes).slice(-2);

                if (vm.dcshiftmaster.nightFlag == true)
                    vm.dcshiftmaster.timeDescription = vm.dcshiftmaster.timeDescription + ' (N)';

                if (vm.dcshiftmaster.breakFlag == true) {
                    vm.dcshiftmaster.timeDescription = vm.dcshiftmaster.timeDescription + ' - ' + app.localize('BO') + " : " + vm.breakouthours + ':' + String("0" + vm.breakoutminutes).slice(-2);
                    vm.dcshiftmaster.timeDescription = vm.dcshiftmaster.timeDescription + ' - ' + app.localize('BI') + " : " + vm.breakinhours + ':' + String("0" + vm.breakinminutes).slice(-2);
                }

                vm.dcshiftmaster.timeDescription = vm.dcshiftmaster.timeDescription + " - " + app.localize('TO') + " : " + vm.timeouthours + ':' + String("0" + vm.timeoutminutes).slice(-2);
                vm.setDutyDesc();
            }

            vm.setDutyDesc = function () {
                if (vm.dcshiftmaster.id == null || vm.dcshiftmaster.dutyDesc == null || vm.dcshiftmaster.dutyDesc =='') {
                    vm.dcshiftmaster.dutyDesc = vm.timeinhours + ':' + String("0" + vm.timeinminutes).slice(-2);

                    if (vm.dcshiftmaster.nightFlag == true)
                        vm.dcshiftmaster.dutyDesc = vm.dcshiftmaster.dutyDesc + ' (N)';

                    if (vm.dcshiftmaster.breakFlag == true) {
                        vm.dcshiftmaster.dutyDesc = vm.dcshiftmaster.dutyDesc + ' - ' + app.localize('BO') + " : " + vm.breakouthours + ':' + String("0" + vm.breakoutminutes).slice(-2);
                        vm.dcshiftmaster.dutyDesc = vm.dcshiftmaster.dutyDesc + ' - ' + app.localize('BI') + " : " + vm.breakinhours + ':' + String("0" + vm.breakinminutes).slice(-2);
                    }

                    vm.dcshiftmaster.dutyDesc = vm.dcshiftmaster.dutyDesc + " - " + vm.timeouthours + ':' + String("0" + vm.timeoutminutes).slice(-2);
                }
            }

            vm.timeinhours = "8";
            vm.timeinminutes = "0";

            vm.timeouthours = "17";
            vm.timeoutminutes = "0";

            vm.breakinhours = "8";
            vm.breakinminutes = "0";

            vm.breakouthours = "17";
            vm.breakoutminutes = "0";


            //-------       Time End 

            vm.getCompanyName = function () {
                vm.loadingCount++;
                personalinformationService.getCompanyNameById({
                    id: vm.currentCompanyRefId
                }).success(function (result) {
                    vm.currentCompany = result;
                }).finally(function () {
                    vm.loadingCount--;
                });
            }



            vm.openLocation = function () {

                vm.locationGroup = app.createLocationInputForCreateSingleLocation();

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        if (result.locations[0].id != vm.dcshiftmaster.locationRefId) {
                            vm.dcshiftmaster.locationRefId = result.locations[0].id;
                            vm.currentLocation = result.locations[0].name;
                            vm.companyRefId = result.locations[0].companyRefId;
                            vm.getCompanyName();
                        }
                    }

                });

            }

        }
    ]);
})();

