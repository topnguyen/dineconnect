﻿
(function () {
    appModule.controller('tenant.views.hr.master.publicholiday.publicHoliday', [
        '$scope', '$uibModalInstance', 'abp.services.app.publicHoliday', 'publicholidayId', 'abp.services.app.commonLookup',
        function ($scope, $modalInstance, publicholidayService, publicholidayId, commonLookupService) {
            var vm = this;
            /* eslint-disable */

            vm.saving = false;
            vm.publicholiday = null;
			$scope.existall = true;
            //date start
            //  Date Range Options
			var todayAsString = moment().format('YYYY-MM-DD');
			vm.dateRangeOptions = app.createDateRangePickerOptions();
			vm.dateRangeModel = {
			    startDate: todayAsString,
			    endDate: todayAsString
			};

            //  Date Angular Js 
			$scope.today = function () {
			    $scope.dt = moment(new Date()).format("DD-MMM-YYYY");

			};
			$scope.today();

			$scope.clear = function () {
			    $scope.dt = null;
			};

			$scope.inlineOptions = {
			    customClass: getDayClass,
			    minDate: new Date(),
			    showWeeks: true
			};

			$scope.dateOptions = {
			    dateDisabled: disabled,
			    formatYear: 'yy',
			    maxDate: new Date(2020, 5, 22),
			    minDate: new Date(),
			    startingDay: 1,
			    showWeeks: false
			};

            // Disable weekend selection
			function disabled(data) {
			    var date = data.date,
                  mode = data.mode;
			    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
			}

			$scope.toggleMin = function () {
			    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
			    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
			};

			$scope.toggleMin();

			$scope.open1 = function () {
			    $scope.popup1.opened = true;
			};

			$scope.open2 = function () {
			    $scope.popup2.opened = true;
			};

			$scope.open3 = function () {
			    $scope.popup3.opened = true;
			};

			$scope.open4 = function () {
			    $scope.popup4.opened = true;
			};

			$scope.setDate = function (year, month, day) {
			    $scope.dt = new Date(year, month, day);
			};

			$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
			$scope.format = $scope.formats[0];
            //$scope.altInputFormats = ['M!/d!/yyyy'];
			$scope.altInputFormats = ['dd-MMMM-yyyy'];

			$scope.popup1 = {
			    opened: false
			};

			$scope.popup2 = {
			    opened: false
			};

			$scope.popup3 = {
			    opened: false
			};

			$scope.popup4 = {
			    opened: false
			};

			var tomorrow = new Date();
			tomorrow.setDate(tomorrow.getDate() + 1);
			var afterTomorrow = new Date();
			afterTomorrow.setDate(tomorrow.getDate() + 1);
			$scope.events = [
              {
                  date: tomorrow,
                  status: 'full'
              },
              {
                  date: afterTomorrow,
                  status: 'partially'
              }
			];

			function getDayClass(data) {
			    var date = data.date,
                  mode = data.mode;
			    if (mode === 'day') {
			        var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

			        for (var i = 0; i < $scope.events.length; i++) {
			            var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

			            if (dayToCheck === currentDay) {
			                return $scope.events[i].status;
			            }
			        }
			    }

			    return '';
			}

            //date end

			
            vm.save = function () {
			   if ($scope.existall == false)
                    return;
                vm.saving = true;
                vm.publicholiday.holidayDate = moment(vm.publicholiday.holidayDate).format('YYYY-MM-DD');
                publicholidayService.createOrUpdatePublicHoliday({
                    publicHoliday: vm.publicholiday
                }).success(function () {
                    abp.notify.info('\' PublicHoliday \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			
            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			

	        function init() {

                publicholidayService.getPublicHolidayForEdit({
                    Id: publicholidayId
                }).success(function (result) {
                     vm.publicholiday = result.publicHoliday;
                    if(vm.publicholiday.id== null)
                    {
                        vm.publicholiday.holidayDate = null;
                    }
                     else
                    {
                        $scope.dt = new Date(vm.publicholiday.holidayDate);
                        vm.publicholiday.holidayDate = $scope.dt;
                    }
                });
            }
            init();
        }
    ]);
})();

