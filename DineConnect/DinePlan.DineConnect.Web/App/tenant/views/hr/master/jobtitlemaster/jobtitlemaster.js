﻿
(function () {
    appModule.controller('tenant.views.hr.master.jobtitlemaster.jobtitlemaster', [
        '$scope', '$uibModalInstance', 'abp.services.app.jobTitleMaster', 'jobtitlemasterId', 
        function ($scope, $modalInstance, jobtitlemasterService, jobtitlemasterId) {
            var vm = this;
            /* eslint-disable */

            vm.saving = false;
            vm.jobtitlemaster = null;
			  
		     vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.save = function () {
                vm.saving = true;
				
                jobtitlemasterService.createOrUpdateJobTitleMaster({
                    jobTitleMaster: vm.jobtitlemaster
                }).success(function () {
                    abp.notify.info('\' JobTitleMaster \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

	        function init() {
                jobtitlemasterService.getJobTitleMasterForEdit({
                    Id: jobtitlemasterId
                }).success(function (result) {
                    vm.jobtitlemaster = result.jobTitleMaster;
                });
            }
            init();
        }
    ]);
})();

