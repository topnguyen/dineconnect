﻿
(function () {
    appModule.controller('tenant.views.hr.master.leavetype.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.leaveType',
        function ($scope, $state, $modal, uiGridConstants, leavetypeService) {
            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Hr.Master.LeaveType.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Hr.Master.LeaveType.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Hr.Master.LeaveType.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editLeaveType(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteLeaveType(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('LeaveTypeName'),
                        field: 'leaveTypeName'
                    },
                    {
                        name: app.localize('LeaveTypeShortName'),
                        field: 'leaveTypeShortName'
                    },
                    {
                        name: app.localize('LeaveAllowed'),
                        field: 'numberOfLeaveAllowed'
                    },
                    {
                        name: app.localize('DefaultNumberOfLeaveAllowed'),
                        field: 'defaultNumberOfLeaveAllowed'
                    },
                    {
                        name: app.localize('MaleGenderAllowed'),
                        field: 'maleGenderAllowed',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.maleGenderAllowed" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.maleGenderAllowed" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                    },
                    {
                        name: app.localize('FemaleGenderAllowed'),
                        field: 'femaleGenderAllowed',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.femaleGenderAllowed" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.femaleGenderAllowed" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                    },
                    {
                        name: app.localize('AllowedForMarriedEmployeesOnly'),
                        field: 'allowedForMarriedEmployeesOnly',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.allowedForMarriedEmployeesOnly" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.allowedForMarriedEmployeesOnly" class="label label-danger">' + app.localize('N/A') + '</span>' +
                            '</div>',
                    },

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };



            vm.getAll = function () {
                vm.loading = true;
                leavetypeService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editLeaveType = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createLeaveType = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteLeaveType = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteLeaveTypeWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            leavetypeService.deleteLeaveType({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/leavetype/createOrEditModal.cshtml',
                    controller: 'tenant.views.hr.master.leavetype.createOrEditModal as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        leavetypeId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                    if (result == true) {
                        vm.refreshLeaveType();
                    }
                });
            }

            vm.exportToExcel = function () {
                leavetypeService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getAll();
            vm.loadingCount = 0;
            vm.refreshLeaveType = function () {
                vm.loading = true;
                vm.loadingCount++;
                var acYear = moment().format("YYYY");
                yearwiseleaveallowedforemployeeService.updateLeaveTypeForGivenAcYearForAll({
                    acYear: acYear
                }).success(function (result) {
                    abp.notify.info(result.successMessage);
                }).finally(function () {
                    vm.loading = false;
                    vm.loadingCount--;
                });
            }

        }]);
})();

