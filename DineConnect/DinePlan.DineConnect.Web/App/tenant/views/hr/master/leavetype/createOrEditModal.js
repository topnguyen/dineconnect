﻿(function () {
    appModule.controller('tenant.views.hr.master.leavetype.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.leaveType', 'leavetypeId', 'abp.services.app.commonLookup',
        function ($scope, $modalInstance, leavetypeService, leavetypeId, commonLookupService) {
            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            vm.leavetype = null;
            $scope.existall = true;
            vm.leaveExhaustedList = [];
            //date start
            //  Date Range Options
            //var todayAsString = moment().format('YYYY-MM-DD');
            //vm.dateRangeOptions = app.createDateRangePickerOptions();
            //vm.dateRangeModel = {
            //    startDate: todayAsString,
            //    endDate: todayAsString
            //};

            //  Date Angular Js 
            $scope.today = function () {
                $scope.dt = moment(new Date()).format("DD-MMM-YYYY");

            };
            $scope.today();

            $scope.clear = function () {
                $scope.dt = null;
            };

            $scope.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            $scope.dateOptions = {
                dateDisabled: disabled,
                formatYear: 'yy',
                dateFormat: 'dd/mm/yy',
                maxDate: new Date(2020, 5, 22),
                minDate: new Date(),
                startingDay: 1
            };

            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                    mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }

            $scope.toggleMin = function () {
                $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
                $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
            };

            $scope.toggleMin();

            $scope.open1 = function () {
                $scope.popup1.opened = true;
            };

            $scope.open2 = function () {
                $scope.popup2.opened = true;
            };

            $scope.open3 = function () {
                $scope.popup3.opened = true;
            };

            $scope.setDate = function (year, month, day) {
                $scope.dt = new Date(year, month, day);
            };

            $scope.formats = ['yyyy-MM-dd'];
            $scope.format = $scope.formats[0];
            $scope.altInputFormats = ['dd-MMMM-yyyy'];

            $('input[name="stDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                // singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: $scope.maxDate
            });

            $('input[name="endDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                //singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: moment(),
                maxDate: $scope.maxDate
            });


            $scope.popup1 = {
                opened: false
            };

            $scope.popup2 = {
                opened: false
            };

            $scope.popup3 = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            $scope.events = [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];

            function getDayClass(data) {
                var date = data.date,
                    mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            }

            //date end



            vm.save = function () {
                if ($scope.existall == false)
                    return;

                if (parseFloat(vm.leavetype.defaultNumberOfLeaveAllowed) > parseFloat(vm.leavetype.numberOfLeaveAllowed)) {
                    abp.notify.error(app.localize('LeaveType') + ' ' + app.localize('NotShouldGreaterThan', app.localize('DefaultNumberOfLeaveAllowed'), app.localize('MaximumNumberOfLeaveAllowed')));
                    return;
                }

                vm.saving = true;

                vm.leavetype.leaveTypeName = vm.leavetype.leaveTypeName.toUpperCase();
                vm.leavetype.leaveTypeShortName = vm.leavetype.leaveTypeShortName.toUpperCase();
                vm.leavetype.calculationPeriodStarts = moment(vm.leavetype.calculationPeriodStarts).format('YYYY-MM-DD');
                vm.leavetype.calculationPeriodEnds = moment(vm.leavetype.calculationPeriodEnds).format('YYYY-MM-DD');

                leavetypeService.createOrUpdateLeaveType({
                    leaveType: vm.leavetype,
                    leaveExhaustedList: vm.leaveExhaustedList
                }).success(function () {
                    abp.notify.info(app.localize('LeaveType') + ' ' + app.localize('SavedSuccessfully'));
                    if (vm.editExistRecord) {
                        if (vm.oldRecord.maleGenderAllowed != vm.leavetype.maleGenderAllowed || vm.oldRecord.femaleGenderAllowed != vm.leavetype.femaleGenderAllowed || vm.oldRecord.allowedForMarriedEmployeesOnly != vm.leavetype.allowedForMarriedEmployeesOnly) {
                            $modalInstance.close(true);
                        }
                        else {
                            $modalInstance.close(false);
                        }
                    }
                    else {
                        $modalInstance.close(false);
                    }

                }).finally(function () {
                    vm.saving = false;
                });
            };



            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.refleavetypes = [];

            function fillDropDownLeaveType() {
                vm.refleavetypes = [];

                leavetypeService.getLeaveTypes({
                }).success(function (result) {
                    vm.refleavetypes = result.items;
                    if (vm.leavetype.id != null) {
                        vm.templist = [];
                        angular.forEach(vm.refleavetypes, function (value, key) {
                            if (value.id != vm.leavetype.id) {
                                vm.templist.push(value);
                            }
                        });
                        vm.refleavetypes = vm.templist;
                    }
                    vm.loading = true;
                });
            }



            vm.oldRecord = null;

            function init() {
                leavetypeService.getLeaveTypeForEdit({
                    Id: leavetypeId
                }).success(function (result) {
                    vm.leavetype = result.leaveType;

                    if (vm.leavetype.id == null) {
                        vm.leavetype.numberOfLeaveAllowed = "";
                        $scope.dt = new Date();
                        vm.leavetype.calculationPeriodStarts = $scope.dt;
                        vm.leavetype.calculationPeriodEnds = $scope.dt;
                        vm.leaveExhaustedList = [];
                        vm.editExistRecord = false;
                    }
                    else {
                        vm.editExistRecord = true;
                        vm.oldRecord = angular.copy(result.leaveType);
                        $scope.calculationPeriodStarts = new Date(vm.leavetype.calculationPeriodStarts);
                        vm.leavetype.calculationPeriodStarts = $scope.calculationPeriodStarts;

                        $scope.calculationPeriodEnds = new Date(vm.leavetype.calculationPeriodEnds);
                        vm.leavetype.calculationPeriodEnds = $scope.calculationPeriodEnds;
                        vm.leaveExhaustedList = result.leaveExhaustedList;
                    }
                    fillDropDownLeaveType();
                });
            }
            init();
        }
    ]);
})();

