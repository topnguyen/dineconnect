﻿
(function () {
    appModule.controller('tenant.views.hr.master.monthwiseworkday.monthWiseWorkDays', [
        '$scope', '$uibModalInstance', 'abp.services.app.monthWiseWorkDay', 'monthwiseworkdayId', 'abp.services.app.commonLookup',
        function ($scope, $modalInstance, monthwiseworkdayService, monthwiseworkdayId, commonLookupService) {
            var vm = this;

            vm.saving = false;
            vm.monthwiseworkday = null;
			$scope.existall = true;

            //date start
            //  Date Range Options
			var todayAsString = moment().format('YYYY-MM-DD');
			vm.dateRangeOptions = app.createDateRangePickerOptions();
			vm.dateRangeModel = {
			    startDate: todayAsString,
			    endDate: todayAsString
			};

            //  Date Angular Js 
			$scope.today = function () {
			    $scope.dt = moment(new Date()).format("DD-MMM-YYYY");

			};
			$scope.today();

			$scope.clear = function () {
			    $scope.dt = null;
			};

			$scope.inlineOptions = {
			    customClass: getDayClass,
			    minDate: new Date(),
			    showWeeks: true
			};

			$scope.dateOptions = {
			    dateDisabled: disabled,
			    formatYear: 'yy',
			    maxDate: new Date(2020, 5, 22),
			    minDate: new Date(),
			    startingDay: 1,
                showWeeks:false
			};

            // Disable weekend selection
			function disabled(data) {
			    var date = data.date,
                  mode = data.mode;
			    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
			}

			$scope.toggleMin = function () {
			    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
			    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
			};

			$scope.toggleMin();

			$scope.open1 = function () {
			    $scope.popup1.opened = true;
			};

			$scope.open2 = function () {
			    $scope.popup2.opened = true;
			};

			$scope.open3 = function () {
			    $scope.popup3.opened = true;
			};

			$scope.setDate = function (year, month, day) {
			    $scope.dt = new Date(year, month, day);
			};

			$scope.formats = ['yyyy-MM-dd'];
			$scope.format = $scope.formats[0];
            //$scope.altInputFormats = ['M!/d!/yyyy'];
			$scope.altInputFormats = ['dd-MMMM-yyyy'];

			$scope.minDate = moment().add(-30, 'days');
			$scope.maxDate = moment().add(90, 'days');

			$scope.dt1 = moment(new Date()).format("MMM-YYYY");


			$scope.dateOptions1 = {
			    formatYear: 'yyyy',
			    startingDay: 1,
			    minMode: 'month'
			};

			$scope.formats1 = ['MMMM-yyyy'];
			$scope.format1 = $scope.formats1[0];

			$('input[name="monthYear"]').daterangepicker({
			    locale: {
			        format: $scope.format1
			    },
			    singleDatePicker: true,
			    showDropdowns: true,
			    startDate: moment(),
			    minDate: $scope.minDate,
			    maxDate: $scope.maxDate,
			});

			$('input[name="stxDate"]').daterangepicker({
			    locale: {
			        format: $scope.format
			    },
			    singleDatePicker: true,
			    showDropdowns: true,
			    startDate: moment(),
			    minDate: $scope.minDate,
			    maxDate: $scope.maxDate
			});

			$('input[name="endDate"]').daterangepicker({
			    locale: {
			        format: $scope.format
			    },
			    singleDatePicker: true,
			    showDropdowns: true,
			    startDate: moment(),
			    minDate: $scope.minDate,
			    maxDate: $scope.maxDate
			});

			$scope.popup1 = {
			    opened: false
			};

			$scope.popup2 = {
			    opened: false
			};

			$scope.popup3 = {
			    opened: false
			};

			var tomorrow = new Date();
			tomorrow.setDate(tomorrow.getDate() + 1);
			var afterTomorrow = new Date();
			afterTomorrow.setDate(tomorrow.getDate() + 1);
			$scope.events = [
              {
                  date: tomorrow,
                  status: 'full'
              },
              {
                  date: afterTomorrow,
                  status: 'partially'
              }
			];

			function getDayClass(data) {
			    var date = data.date,
                  mode = data.mode;
			    if (mode === 'day') {
			        var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

			        for (var i = 0; i < $scope.events.length; i++) {
			            var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

			            if (dayToCheck === currentDay) {
			                return $scope.events[i].status;
			            }
			        }
			    }

			    return '';
			}

            //date end
			
            vm.save = function () {
			   if ($scope.existall == false)
                    return;
                vm.saving = true;
				
                if (CompareDate() == false) {
                    vm.saving = false;
                    return;
                }

                if (vm.validateTotalDate() == false) {
                    vm.saving = false;
                    return;
                }

                vm.monthwiseworkday.monthYear = moment(vm.monthwiseworkday.monthYear).format("MMMM-YYYY");

                monthwiseworkdayService.createOrUpdateMonthWiseWorkDay({
                    monthWiseWorkDay: vm.monthwiseworkday
                }).success(function () {
                    abp.notify.info('\' MonthWiseWorkDay \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.validateTotalDate = function () {
                var totalDays =parseInt(vm.monthwiseworkday.noOfWorkDays) +parseInt(vm.monthwiseworkday.noOfLeaves) + parseInt(vm.monthwiseworkday.noOfGovernmentHolidays);
                if (vm.monthwiseworkday.noOfDays < totalDays) {
                    alert("Please Check Days Entry");
                    return false;
                }
            }


            function CompareDate() {
                var startDate = $('#stDate').val();
                var endDate = $('#endDate').val();

                if (startDate > endDate) {
                    abp.notify.warn(app.localize('ReqDateErr'));
                    abp.message.info(app.localize('ReqDateErr'));
                   // alert("End date can be greater than Start date");
                    return false;
                }
            }

            vm.validateDate = function () {
                // get dates from input fields
                var startDate = moment(vm.monthwiseworkday.startDate).format("YYYY-MM-DD");
                var endDate = moment(vm.monthwiseworkday.endDate).format("YYYY-MM-DD");

                var sdate = startDate.split("-");
                var edate = endDate.split("-");

                var future = moment(startDate);
                var start = moment(endDate);
                var d = start.diff(future, 'days') + 1;

                vm.monthwiseworkday.noOfDays = d;
            }
            vm.CalculateDate = function () {
                var month = moment(vm.monthwiseworkday.monthYear).format("MM");
                var year = moment(vm.monthwiseworkday.monthYear).format("YYYY");

                var FirstDay = new Date(year, month-1, 1);
                var LastDay = new Date(year, month, 0);

                vm.monthwiseworkday.startDate = FirstDay;
                vm.monthwiseworkday.endDate = LastDay;
                vm.validateDate();
            }
			 vm.existall = function ()
            {
			     if (vm.monthwiseworkday.monthYear == null) {
			         vm.existall = false;
			         return;
			     }
				 
                monthwiseworkdayService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'monthYear',
                    filter: vm.monthwiseworkday.monthYear,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.monthwiseworkday.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.monthwiseworkday.monthYear + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
			 };

			

	        function init() {
                monthwiseworkdayService.getMonthWiseWorkDayForEdit({
                    Id: monthwiseworkdayId
                }).success(function (result) {
                    vm.monthwiseworkday = result.monthWiseWorkDay;
                    if (vm.monthwiseworkday.id == null) {
                        vm.monthwiseworkday.numberOfDays = "";
                        vm.monthwiseworkday.noOfWorkDays = "";
                        vm.monthwiseworkday.noOfGovernmentHolidays = "";
                        vm.monthwiseworkday.noOfLeaves = "";
                        vm.monthwiseworkday.salaryCalculationDays = "";
                        vm.monthwiseworkday.noOfDays = "";
                        $scope.dt = moment(new Date());
                        vm.monthwiseworkday.monthYear = null;
                        vm.monthwiseworkday.startDate = null;
                        vm.monthwiseworkday.endDate = null;
                    }

                    //else {
                    //    vm.monthwiseworkday.monthYear = vm.monthwiseworkday.monthYear;
                    //    //var a = "01-" + vm.monthwiseworkday.monthYear;

                    //    //vm.monthwiseworkday.monthYear = moment(a);
                    //    //$scope.monthyear = moment(vm.monthwiseworkday.monthYear).format("MMMM-yyyy");

                    //    vm.monthwiseworkday.startDate = moment(vm.monthwiseworkday.startDate).format("YYYY-MM-DD")
                    //    //$scope.minDate = moment(vm.monthwiseworkday.startDate).format("YYYY-MM-DD");


                    //    vm.monthwiseworkday.endDate = moment(vm.monthwiseworkday.endDate).format("YYYY-MM-DD")
                    //   // $scope.miniDate = moment(vm.monthwiseworkday.endDate).format("YYYY-MM-DD");

                    //    //$('input[name="monthYear"]').daterangepicker({
                    //    //    locale: {
                    //    //        format: 'YYYY-MM-DD'
                    //    //    },
                    //    //    singleDatePicker: true,

                    //    //    showDropdowns: true,
                    //    //    startDate: vm.monthwiseworkday.monthYear,
                    //    //    minDate: $scope.minDate,
                    //    //    maxDate: $scope.maxDate
                    //    //});

                    //    //$('input[name="stDate"]').daterangepicker({
                    //    //    locale: {
                    //    //        format: 'YYYY-MM-DD'
                    //    //    },
                    //    //    singleDatePicker: true,
                            
                    //    //    showDropdowns: true,
                    //    //    startDate: vm.monthwiseworkday.startDate,
                    //    //    minDate: $scope.monthyear,
                    //    //    maxDate: $scope.maxDate
                    //    //});

                    //    //$('input[name="endDate"]').daterangepicker({
                    //    //    locale: {
                    //    //        format: $scope.format
                    //    //    },
                    //    //    singleDatePicker: true,
                    //    //    showDropdowns: true,
                    //    //    startDate: vm.monthwiseworkday.endDate,
                    //    //    minDate: $scope.miniDate,
                    //    //    maxDate: $scope.maxDate
                    //    //});

                    //}
                });
            }
            init();
        }
    ]);
})();

