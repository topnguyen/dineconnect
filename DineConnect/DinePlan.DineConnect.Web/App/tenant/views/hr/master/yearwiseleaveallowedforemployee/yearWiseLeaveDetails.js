﻿
(function () {
    appModule.controller('tenant.views.hr.master.yearwiseleaveallowedforemployee.yearWiseLeaveDetails', [
        '$scope', '$uibModalInstance', 'abp.services.app.yearWiseLeaveAllowedForEmployee', 'abp.services.app.personalInformation',
        'abp.services.app.leaveRequest', 'employeeRefId', 'acYear', 'gender',
        function ($scope, $modalInstance, yearwiseleaveallowedforemployeeService, personalinformationService, leaverequestService, employeeRefId, acYear, gender) {
            var vm = this;

            //alert('create');

            vm.saving = false;
            vm.loadingCount = 0;
            vm.yearwiseleaveallowedforemployee = null;
            vm.employeeRefId = employeeRefId;
            vm.acYear = acYear;
            vm.gender = gender;

            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            //  Date Angular Js 
            $scope.today = function () {
                $scope.dt = moment(new Date()).format("DD-MMM-YYYY");

            };
            $scope.today();

            $scope.clear = function () {
                $scope.dt = null;
            };

            $scope.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            $scope.dateOptions = {
                dateDisabled: disabled,
                formatYear: 'yy',
                maxDate: new Date(2020, 5, 22),
                minDate: new Date(),
                startingDay: 1
            };

            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                    mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }

            $scope.toggleMin = function () {
                $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
                $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
            };

            $scope.toggleMin();

            $scope.open1 = function () {
                $scope.popup1.opened = true;
            };

            $scope.open2 = function () {
                $scope.popup2.opened = true;
            };

            $scope.open3 = function () {
                $scope.popup3.opened = true;
            };

            $scope.setDate = function (year, month, day) {
                $scope.dt = new Date(year, month, day);
            };

            $scope.formats = ['yyyy-MM-dd'];
            $scope.format = $scope.formats[0];
            //$scope.altInputFormats = ['M!/d!/yyyy'];
            $scope.altInputFormats = ['dd-MMMM-yyyy'];

            $scope.minDate = moment().add(-30, 'days');
            $scope.maxDate = moment().add(90, 'days');

            $('input[name="stDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: $scope.maxDate
            });

            $('input[name="endDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: $scope.maxDate
            });


            $scope.popup1 = {
                opened: false
            };

            $scope.popup2 = {
                opened: false
            };

            $scope.popup3 = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            $scope.events = [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];

            function getDayClass(data) {
                var date = data.date,
                    mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            }

            //date end


            vm.save = function () {
                vm.employeeleaveMasterDetails = [];
                vm.errorflag = false;
                angular.forEach(vm.leaveDetailPortion, function (value, key) {
                    if (value.numberOfLeaveAllowed > value.maximumLeaveAllowed) {
                        abp.notify.error(app.localize('AllowedLeaveVsMaximumLeave', value.leaveTypeRefName, value.numberOfLeaveAllowed, value.maximumLeaveAllowed));
                        vm.errorflag = true;
                        return;
                    }
                    vm.employeeleaveMasterDetails.push({
                        'acYear': value.acYear,
                        'employeeRefId': value.employeeRefId,
                        'leaveTypeRefCode': value.leaveTypeRefCode,
                        'leaveTypeRefName': value.leaveTypeRefName,
                        'numberOfLeaveAllowed': value.numberOfLeaveAllowed,
                        'editflag': false
                    });
                });

                if (vm.errorflag) {
                    return;
                }

                vm.saving = true;

                yearwiseleaveallowedforemployeeService.createOrUpdateYearWiseLeaveAllowedForEmployee({
                    yearWiseLeaveAllowedForEmployee: vm.employeeleaveMasterDetails,
                    acYear: vm.acYear,
                    employeeRefId : vm.employeeRefId
                }).success(function () {
                    abp.notify.info(app.localize('YearWiseLeaveAllowedForEmployee') + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };


            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.fillLeaveTypes = function () {
                if (vm.gender == "Male") {
                    fillDropDownLeaveTypeForMale();
                }
                else {
                    fillDropDownLeaveTypeForFemale();
                }
            }


            vm.refleavetype = [];

            function fillDropDownLeaveTypeForMale() {
                vm.refleavetype = [];
                vm.loading = true;
                vm.loadingCount++;
                leaverequestService.getLeaveTypesForMale({}).success(function (result) {
                    vm.refleavetype = result;
                    vm.loading = false;
                    vm.loadingCount--;
                });
            }

            function fillDropDownLeaveTypeForFemale() {
                vm.refleavetype = [];
                vm.loading = true;
                vm.loadingCount++;
                leaverequestService.getLeaveTypesForFemale({}).success(function (result) {
                    vm.refleavetype = result;
                    vm.loading = false;
                    vm.loadingCount--;
                });
            }

            vm.fillLeaveTypes();

            vm.editSet = function (productIndex) {
                angular.forEach(vm.leaveDetailPortion, function (value, key) {
                    if (key == productIndex)
                        value.editflag = true;
                    else
                        value.editflag = false;
                });
            }


            vm.leaveDetailPortion = [];
            function init() {
                yearwiseleaveallowedforemployeeService.getYearWiseLeaveAllowedForEmployeeForEdit({
                    employeeRefId: vm.employeeRefId,
                    acYear: vm.acYear
                }).success(function (result) {
                    vm.sno = 0;
                    vm.leaveDetailPortion = [];
                    angular.forEach(result.yearWiseLeaveAllowedForEmployee, function (value, key) {
                        vm.sno = vm.sno + 1;
                        vm.leaveDetailPortion.push({
                            'acYear': value.acYear,
                            'employeeRefId': value.employeeRefId,
                            'leaveTypeRefCode': value.leaveTypeRefCode,
                            'leaveTypeRefName': value.leaveTypeRefName,
                            'numberOfLeaveAllowed': value.numberOfLeaveAllowed,
                            'maximumLeaveAllowed': value.maximumLeaveAllowed,
                            'numberOfLeaveTaken': value.numberOfLeaveTaken,
                            'numberOfLeaveRemaining': value.numberOfLeaveRemaining,
                            'editflag': false
                        });

                    });
                });
            }

            vm.refemployeecode = [];
            function fillDropDownEmployee() {
                vm.loadingCount++;
                personalinformationService.getEmployeeWithSkillSetForCombobox({}).success(function (result) {
                    vm.refemployeecode = result;
                    init();
                    vm.loadingCount--;
                });
            }

            fillDropDownEmployee();



        }
    ]);
})();

