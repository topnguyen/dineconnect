﻿(function () {
    appModule.controller('tenant.views.hr.master.dcheadmaster.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.dcHeadMaster', 'dcheadmasterId', 'abp.services.app.commonLookup',
        function ($scope, $modalInstance, dcheadmasterService, dcheadmasterId, commonLookupService) {
            var vm = this;
            
			/* eslint-disable */

            vm.saving = false;
            vm.dcheadmaster = null;
			$scope.existall = true;

			
            vm.save = function () {
                vm.saving = true;

                vm.dcheadmaster.headName = vm.dcheadmaster.headName.toUpperCase();
                dcheadmasterService.createOrUpdateDcHeadMaster({
                    dcHeadMaster: vm.dcheadmaster
                }).success(function () {
                    abp.notify.info(app.localize('DcHeadMaster')  + ' ' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
	        function init() {
                dcheadmasterService.getDcHeadMasterForEdit({
                    Id: dcheadmasterId
                }).success(function (result) {
                    vm.dcheadmaster = result.dcHeadMaster;
                });
            }
            init();
        }
    ]);
})();

