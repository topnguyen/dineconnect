﻿(function () {
    appModule.controller('tenant.views.hr.master.documentinfo.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.documentInfo', 'documentinfoId', 'abp.services.app.commonLookup', 'abp.services.app.personalInformation',
        function ($scope, $modalInstance, documentinfoService, documentinfoId, commonLookupService, personalinformationService) {
            var vm = this;
            /* eslint-disable */

            vm.saving = false;
            vm.documentinfo = null;
            $scope.existall = true;

            vm.refskillset = [];

            function fillDropDownSkillSet() {
                personalinformationService.getSkillSetForCombobox({}).success(function (result) {
                    vm.refskillset = result.items;
                });
            }

            fillDropDownSkillSet();

            vm.refdocumentType = [];

            function fillDropDownDocumentType() {
                documentinfoService.getDocumentTypeForCombobox({}).success(function (result) {
                    vm.refdocumentType = result.items;
                });
            }
            fillDropDownDocumentType();
            vm.save = function () {
                if ($scope.existall == false)
                    return;
                vm.saving = true;

                vm.documentinfo.documentName = vm.documentinfo.documentName.toUpperCase();

                if (vm.documentinfo.lifeTimeFlag == true)
                    vm.documentinfo.defaultAlertDays = null;
                documentinfoService.createOrUpdateDocumentInfo({
                    documentInfo: vm.documentinfo,
                    skillSetList: vm.skillSetRefId
                }).success(function () {
                    abp.notify.info('\' DocumentInfo \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {

                if (vm.documentinfo.documentName == null) {
                    vm.existall = false;
                    return;
                }

                documentinfoService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'documentName',
                    filter: vm.documentinfo.documentName,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.documentinfo.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.documentinfo.documentName + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.showCheck = function () {

                var content = vm.documentinfo.defaultAlertDays;

                if (content == null || content == "")
                    $('#islifetime').show();
            };

            vm.hideCheck = function () {

                var content = vm.documentinfo.defaultAlertDays;

                if (content == null || content == "") {
                    $('#islifetime').hide();
                    vm.documentinfo.lifeTimeFlag = false;
                }

            };

            function init() {

                documentinfoService.getDocumentInfoForEdit({
                    Id: documentinfoId
                }).success(function (result) {
                    vm.documentinfo = result.documentInfo;
                    vm.skillSetRefId = result.skillSetList;
                    if (vm.documentinfo.id == null)
                        vm.documentinfo.defaultAlertDays = "";

                });
            }
            init();
        }
    ]);
})();

