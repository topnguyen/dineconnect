﻿(function () {
    appModule.controller('tenant.views.hr.master.personalinformation.salaryHolding', [
        '$scope', '$uibModalInstance', '$uibModal', 'abp.services.app.personalInformation', 'personalinformationId',
        function ($scope, $modalInstance, $modal, personalinformationService, personalinformationId) {
            var vm = this;
            
            vm.saving = false;
            vm.personalinformation = null;
            $scope.existall = true;
            vm.personalinformationId = personalinformationId;
            vm.autoserialnumber = null;
            vm.serialnumberproceduretorun = false;

            //Employee Status
            $scope.empstatus = ['Confirmed', 'Consolidated'].sort();

            vm.getEmpStatus = function (search) {
                var newSupes = $scope.empstatus.slice();
                if (search && newSupes.indexOf(search) === -1) {
                    newSupes.unshift(search);
                }
                return newSupes;
            }

            //date start
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.dateOptions = [];
            vm.dateOptions.push({
                'singleDatePicker': true,
                'showDropdowns': true,
                'startDate': moment(),
                'minDate': $scope.minDate,
                'maxDate': moment()
            });

            $scope.minDate = moment().add(-150, 'days');

            $('input[name="calDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });

            $('input[name="nextcalDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });

            //date end

            vm.save = function () {
                if ($scope.existall == false)
                    return;

								var person = prompt("Please enter Employee name", "");
								if (person.toLowerCase() == vm.personalinformation.employeeName.toLowerCase()) {
								}
								else
                {
                    abp.notify.warn(app.localize('NameWrong'));
                    return;
                }
								
								//vm.personalinformation.lastWorkingDate = $('#nextcalDate').val();
								vm.personalinformation.lastOfficialWorkingDate = vm.personalinformation.lastWorkingDate;

                if (CompareDate() == false) {
                    vm.saving = false;
                    return;
								}

								var leftDate = prompt("Please enter Resigned Date in format YYYY-MMM-DD , Example (2017-JAN-01) ","");
								var d1 = moment(leftDate);
								var d2 = moment(leftDate, 'YYYY-MMM-DD');
								var d3 = moment(vm.personalinformation.lastWorkingDate);
								var d4 = moment(vm.personalinformation.lastWorkingDate,'YYYY-MMM-DD');

								if (moment(d2).format('YYYY-MMM-DD') != moment(d4).format('YYYY-MMM-DD')) {
									abp.notify.error(app.localize('LastWorkingDate') + ' ' + app.localize('NotMatched'));
									vm.saving = false;
									return;
								}

                vm.saving = true;
                personalinformationService.updateResignationPersonalInformation({
                    personalInformation: vm.personalinformation,
                }).success(function () {
                    abp.notify.info('\' Resignation \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function CompareDate() {
                var lastOfficialWorkingDate = $('#calDate').val();
                var lastWorkingDate = $('#nextcalDate').val();
                var dateHired =moment(vm.personalinformation.dateHired).format('YYYY-MM-DD') ;
                var dateConfirm =moment(vm.personalinformation.dateConfirmed).format('YYYY-MM-DD') ;
                if (vm.personalinformation.dateHired !== null && vm.personalinformation.dateConfirmed !== null) {
                    if (lastOfficialWorkingDate < dateConfirm && lastOfficialWorkingDate < dateHired) {
                        abp.notify.warn(app.localize('LeftDateChkErr'));
                        abp.message.info(app.localize('LeftDateChkErr'));
                       // alert("Left Date Must be greater than Date of Conformed");
                        return false;
                    }
                }
                else
                {
                    abp.notify.warn(app.localize('HireDateChkErr'));
                    abp.message.info(app.localize('HireDateChkErr'));
                   // alert("Please Check Or Re-Enter DateHired & DateConfirmed ");
                    return false;
                }
            }

            vm.existall = function () {

                if (vm.personalinformation.employeeName == null) {
                    vm.existall = false;
                    return;
                }

                personalinformationService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'employeeName',
                    filter: vm.personalinformation.employeeName,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.personalinformation.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.personalinformation.employeeName + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };


            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.getComboValueInString = function (item) {
                return item.value;
            };

            vm.refcompany = [];

            function fillDropDownCompany() {
                personalinformationService.getCompanyForCombobox({}).success(function (result) {
                    vm.refcompany = result.items;
                });
            }

            function init() {
                fillDropDownCompany();
                personalinformationService.getPersonalInformationForEdit({
                    Id: vm.personalinformationId
                }).success(function (result) {
                    vm.personalinformation = result.personalInformation;

                    if (vm.personalinformation.id != null) {

                        if (vm.personalinformation.lastOfficialWorkingDate == null && vm.personalinformation.lastWorkingDate == null) {
                            $('input[name="calDate"]').daterangepicker({
                                locale: {
                                    format: $scope.format
                                },
                                singleDatePicker: true,
                                showDropdowns: true,
                                minDate: $scope.minDate,
                                maxDate: moment()
                            });

                            $('input[name="nextcalDate"]').daterangepicker({
                                locale: {
                                    format: $scope.format
                                },
                                singleDatePicker: true,
                                showDropdowns: true,
                                minDate: $scope.minDate,
                                maxDate: moment()
                            });
                        }
                        else {
                            vm.personalinformation.lastOfficialWorkingDate = moment(vm.personalinformation.lastOfficialWorkingDate).format('YYYY-MM-DD');
                            vm.personalinformation.lastWorkingDate = moment(vm.personalinformation.lastWorkingDate).format('YYYY-MM-DD');
                        }
                        if (vm.personalinformation.dateHired != null) {
                            vm.personalinformation.dateHired = moment(vm.personalinformation.dateHired).format('YYYY-MM-DD');
                        }
                        else {
                            vm.personalinformation.dateHired = null;
                        }
                        if (vm.personalinformation.dateConfirmed != null) {
                            vm.personalinformation.dateConfirmed = moment(vm.personalinformation.dateConfirmed).format('YYYY-MM-DD');
                        }
                        else {
                            vm.personalinformation.dateConfirmed = null;
                        }


                    }

                });
            }
            init();
        }
    ]);
})();

