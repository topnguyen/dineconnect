﻿(function () {
    appModule.controller('tenant.views.hr.master.personalinformation.viewemployee', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.personalInformation', 'appSession',
        function ($scope, $state, $stateParams, $modal, personalinformationService, appSession) { 
            /* eslint-disable */

        var vm = this;
            
        vm.saving = false;
        vm.personalinformation = null;
	    $scope.existall = true;
	    vm.errorFlag = false;

	    if (appSession.useremployeerefid == null || appSession.useremployeerefid == 0) {
	        abp.notify.warn(app.localize('EmployeeCodeNotAssignedToThisUser', appSession.user.userName));
	        abp.message.warn(app.localize('EmployeeCodeNotAssignedToThisUser', appSession.user.userName));
	        vm.errorFlag = true;
	    }
	    vm.currentEmployeeRefId = appSession.useremployeerefid;

	    vm.personalinformationId = appSession.useremployeerefid;

	    vm.autoserialnumber = null;
	    vm.serialnumberproceduretorun = false;

            //date start
            //  Date Range Options
	    var todayAsString = moment().format('YYYY-MM-DD');
	    vm.dateRangeOptions = app.createDateRangePickerOptions();
	    vm.dateRangeModel = {
	        startDate: todayAsString,
	        endDate: todayAsString
	    };

            //  Date Angular Js 
	    $scope.today = function () {
	        $scope.dt = moment(new Date()).format("DD-MMM-YYYY");

	    };
	    $scope.today();

	    $scope.clear = function () {
	        $scope.dt = null;
	    };

	    $scope.inlineOptions = {
	        customClass: getDayClass,
	        minDate: new Date(),
	        showWeeks: true
	    };

	    $scope.dateOptions = {
	        dateDisabled: disabled,
	        formatYear: 'yy',
	        maxDate: new Date(2020, 5, 22),
	        minDate: new Date(),
	        startingDay: 1,
            showWeeks:false
	    };

            // Disable weekend selection
	    function disabled(data) {
	        var date = data.date,
              mode = data.mode;
	        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
	    }

	    $scope.toggleMin = function () {
	        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
	        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
	    };

	    $scope.toggleMin();

	    $scope.open1 = function () {
	        $scope.popup1.opened = true;
	    };

	    $scope.open2 = function () {
	        $scope.popup2.opened = true;
	    };

	    $scope.open3 = function () {
	        $scope.popup3.opened = true;
	    };

	    $scope.open4 = function () {
	        $scope.popup4.opened = true;
	    };

	    $scope.setDate = function (year, month, day) {
	        $scope.dt = new Date(year, month, day);
	    };

	    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
	    $scope.format = $scope.formats[0];
            //$scope.altInputFormats = ['M!/d!/yyyy'];
	    $scope.altInputFormats = ['dd-MMMM-yyyy'];

	    $scope.popup1 = {
	        opened: false
	    };

	    $scope.popup2 = {
	        opened: false
	    };

	    $scope.popup3 = {
	        opened: false
	    };

	    $scope.popup4 = {
	        opened: false
	    };

	    var tomorrow = new Date();
	    tomorrow.setDate(tomorrow.getDate() + 1);
	    var afterTomorrow = new Date();
	    afterTomorrow.setDate(tomorrow.getDate() + 1);
	    $scope.events = [
          {
              date: tomorrow,
              status: 'full'
          },
          {
              date: afterTomorrow,
              status: 'partially'
          }
	    ];

	    function getDayClass(data) {
	        var date = data.date,
              mode = data.mode;
	        if (mode === 'day') {
	            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

	            for (var i = 0; i < $scope.events.length; i++) {
	                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

	                if (dayToCheck === currentDay) {
	                    return $scope.events[i].status;
	                }
	            }
	        }

	        return '';
	    }

            //date end

            //Resident Status

	    $scope.residents = ['PR', 'Citizen','Foreigner'].sort();

	    vm.getResidents = function (search) {
	        var newSupes = $scope.residents.slice();
	        if (search && newSupes.indexOf(search) === -1) {
	            newSupes.unshift(search);
	        }
	        return newSupes;
	    }

            //Gender
	    $scope.genders = ['Male', 'Female'].sort();

	    vm.getGenders = function (search) {
	        var newSupes = $scope.genders.slice();
	        if (search && newSupes.indexOf(search) === -1) {
	            newSupes.unshift(search);
	        }
	        return newSupes;
	    }
            //Staff type
	    $scope.stafftypes = ['Permanent', 'Temporary'].sort();

	    vm.getStaffTypes = function (search) {
	        var newSupes = $scope.stafftypes.slice();
	        if (search && newSupes.indexOf(search) === -1) {
	            newSupes.unshift(search);
	        }
	        return newSupes;
	    }

            //Race
	    $scope.races = ['Chinese', 'Eurasian', 'Iban', 'Indian', 'Kadazan', 'Malay', 'Others'].sort();

	    vm.getRaces = function (search) {
	        var newSupes = $scope.races.slice();
	        if (search && newSupes.indexOf(search) === -1) {
	            newSupes.unshift(search);
	        }
	        return newSupes;
	    }

            //Religion
	    $scope.religions = ['Buddhist', 'Christian', 'Hindu', 'Islam', 'None', 'Others'].sort();

	    vm.getReligions = function (search) {
	        var newSupes = $scope.religions.slice();
	        if (search && newSupes.indexOf(search) === -1) {
	            newSupes.unshift(search);
	        }
	        return newSupes;
	    }

            //Department
	    $scope.departments = ['Administration', 'Director', 'Finance', 'Human Resource', 'Manager', 'Marketing', 'NDT & ANDT', 'Research and Development', 'Sales', 'Support'].sort();

	    vm.getDepartments = function (search) {
	        var newSupes = $scope.departments.slice();
	        if (search && newSupes.indexOf(search) === -1) {
	            newSupes.unshift(search);
	        }
	        return newSupes;
	    }

            //Marital
            $scope.maritals = ['Married', 'Single', 'Divorced', 'Widowed '].sort();

	    vm.getMaritals = function (search) {
	        var newSupes = $scope.maritals.slice();
	        if (search && newSupes.indexOf(search) === -1) {
	            newSupes.unshift(search);
	        }
	        return newSupes;
	    }
            //Nationality
	    $scope.nationalitys = ['MALAYSIAN', 'INDIAN', 'SINGAPORE CITIZEN'].sort();

	    vm.getNationalitys = function (search) {
	        var newSupes = $scope.nationalitys.slice();
	        if (search && newSupes.indexOf(search) === -1) {
	            newSupes.unshift(search);
	        }
	        return newSupes;
	    }
            //Employee Status
	    $scope.empstatus = ['Confirmed', 'Consolidated'].sort();

	    vm.getEmpStatus = function (search) {
	        var newSupes = $scope.empstatus.slice();
	        if (search && newSupes.indexOf(search) === -1) {
	            newSupes.unshift(search);
	        }
	        return newSupes;
	    }
            //Bank Status
	    $scope.bank = ['DBS', 'HSBC', 'IBG', 'OCBC', 'POSB', 'UOB'].sort();

	    vm.getBanks = function (search) {
	        var newSupes = $scope.bank.slice();
	        if (search && newSupes.indexOf(search) === -1) {
	            newSupes.unshift(search);
	        }
	        return newSupes;
	    }

            //Cost Centre
	    $scope.costcentre = ['CCA', 'CCB', 'CCC'].sort();

	    vm.getCostCentres = function (search) {
	        var newSupes = $scope.costcentre.slice();
	        if (search && newSupes.indexOf(search) === -1) {
	            newSupes.unshift(search);
	        }
	        return newSupes;
	    }

            //JobLevel
	    $scope.joblevel = ['Director', 'Clerical', 'General Worker', 'Non Professional', 'Professional', 'Sales', 'Semi Skilled', 'Skilled', 'Supervisory', 'Technical', 'Unskilled'].sort();

	    vm.getJobLevels = function (search) {
	        var newSupes = $scope.joblevel.slice();
	        if (search && newSupes.indexOf(search) === -1) {
	            newSupes.unshift(search);
	        }
	        return newSupes;
	    }

	    vm.save = function () {
			   if ($scope.existall == false)
			       return;

			   if (vm.personalinformation.userSerialNumber == null)
			   {
			       abp.notify.info("SerialNumberErr");
			       return;
			   }
                
			   vm.saving = true;

                if (vm.personalinformation.maritalStatus == 'Bachelor')
                    vm.personalinformation.maritalDate = null;
                

                vm.personalinformation.employeeName = vm.personalinformation.employeeName.toUpperCase();
                vm.personalinformation.employeeCode = vm.personalinformation.employeeCode.toUpperCase();

                if (parseFloat(vm.autoserialnumber) != parseFloat(vm.personalinformation.userSerialNumber)) {
                    vm.serialnumberproceduretorun = true;
                }

                personalinformationService.createOrUpdatePersonalInformation({
                    personalInformation: vm.personalinformation,
                    updateSerialNumberRequired: vm.serialnumberproceduretorun
                }).success(function () {
                    abp.notify.info('\' PersonalInformation \'' + app.localize('SavedSuccessfully'));
                    vm.autoserialnumber = null;
                    vm.serialnumberproceduretorun = false;
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.personalinformation.employeeName == null) {
			         vm.existall = false;
			         return;
			     }
				 
                personalinformationService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'employeeName',
                    filter: vm.personalinformation.employeeName,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.personalinformation.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.personalinformation.employeeName + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $state.go("tenant.personalinformation");
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
			 };

			 vm.getComboValueInString = function (item) {
			     return item.value;
			 };
			
			vm.refcompany = [];

			 function fillDropDownCompany() {
			     personalinformationService.getCompanyForCombobox({}).success(function (result) {
                    vm.refcompany = result.items;
                });
			 }
			 vm.refskillset = [];

			 function fillDropDownSkillSet() {
			     personalinformationService.getSkillSetForCombobox({}).success(function (result) {
			         vm.refskillset = result.items;
			     });
			 }

			 function setUserSno() {
			     personalinformationService.getUserSerialNumberMax().success(function (result) {
			         vm.personalinformation.userSerialNumber = result.id;
			         vm.autoserialnumber = result.id;
			     });
			 }

			 vm.openDocumentInfoModal = function () {
			     var modalInstance = $modal.open({
			         templateUrl: '~/App/tenant/views/hr/master/documentinfo/createOrEditModal.cshtml',
			         controller: 'tenant.views.hr.master.documentinfo.createOrEditModal as vm',
			         backdrop: 'static',
			     	keyboard: false,
			         resolve: {
			             documentinfoId: function () {
			                 return null;
			             }
			         }
			     });

			     modalInstance.result.then(function (result) {
			         vm.getAll();
			     });
			 }


			 $scope.data = 'none';
			 vm.add = function () {
			     var f = document.getElementById('file').files[0],
                     r = new FileReader();
			     r.onloadend = function (e) {
			         $scope.data = e.target.result;
			     }
			     r.readAsText(f)
			     //r.readAsBinaryString(f);
			 }

			 vm.changePicture = function (myObject) {
			     var modalInstance = $modal.open({
			         templateUrl: '~/App/tenant/views/hr/master/personalinformation/changeProfilePicture.cshtml',
			         controller: 'tenant.views.hr.master.personalinformation.changeProfilePicture as vm',
			         backdrop: 'static',
			         resolve: {
			             personalinformationId: function () {
			                 return myObject;
			             }
			         }
			     });
			     modalInstance.result.then(function (result) {
			         personalinformationService.getPersonalInformationForEdit({
			             Id: myObject
			         }).success(function (result) {
			             vm.personalinformation.profilePictureId = result.personalInformation.profilePictureId;
			         });

			     });
			 };

			 function init() {
			     if (vm.errorFlag == true)
			         return;
			    abp.ui.setBusy('#personalinformationForm');
				fillDropDownCompany();
				fillDropDownSkillSet();

                personalinformationService.getPersonalInformationForEdit({
                    Id: vm.personalinformationId
                }).success(function (result) {
                    vm.personalinformation = result.personalInformation;

                    if (vm.personalinformation.id == null) {
                        setUserSno();
                        $scope.dt = new Date();
                        vm.personalinformation.dateOfBirth =null;
                        vm.personalinformation.maritalDate = $scope.dt;
                        vm.personalinformation.dateHired = $scope.dt;
                      
                    } else {
                        $scope.dt = new Date(vm.personalinformation.dateOfBirth);
                        vm.personalinformation.dateOfBirth = $scope.dt;
                        vm.autoserialnumber = vm.personalinformation.userSerialNumber;
                    }

                });
		abp.ui.clearBusy('#personalinformationForm');
            }
            init();
        }
    ]);
})();

