﻿(function () {
    appModule.controller('tenant.views.hr.master.personalinformation.employee', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.personalInformation', 'FileUploader', 'appSession', '$rootScope', "abp.services.app.dinePlanUser",
        function ($scope, $state, $stateParams, $modal, personalinformationService, fileUploader, appSession, $rootScope, dineplanuserService) {
            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            $rootScope.settings.layout.pageSidebarClosed = true;
            vm.personalinformation = null;
            $scope.existall = true;
            vm.personalinformationId = $stateParams.id;
            vm.cloneFlag = false;
            vm.tenantName = appSession.tenant.name;
            if ($stateParams.cloneFlag == true || $stateParams.cloneFlag == 'true')
                vm.cloneFlag = true;
            vm.selectedUser = null;
            vm.autoserialnumber = null;
            vm.serialnumberproceduretorun = false;

            vm.skillSetRefId = [];

            vm.permissions = {
                'changeEmployeeCode': abp.auth.hasPermission('Pages.Tenant.Hr.Master.PersonalInformation.ChangeEmployeeCode')
            };


            vm.refemployeecode = [];
            function fillDropDownEmployee() {
                vm.loading = true;
                personalinformationService.getEmployeeWithSkillSetForCombobox({}).success(function (result) {
                    vm.refemployeecode = result;
                    vm.loading = false;
                });
            }
            fillDropDownEmployee();


            vm.refactiveemployeecode = [];
            function fillDropDownEmployeeActive() {
                vm.loading = true;
                personalinformationService.getEmployeeWithSkillSetForCombobox({
                    id: 1
                }).success(function (result) {
                    vm.refactiveemployeecode = result;
                    vm.loading = false;
                });
            }

            fillDropDownEmployeeActive();

            //date start
            //  Date Range Options
            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            //  Date Angular Js 
            $scope.today = function () {
                $scope.dt = moment(new Date()).format("DD-MMM-YYYY");

            };
            $scope.today();

            $scope.clear = function () {
                $scope.dt = null;
            };

            $scope.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            $scope.dateOptions = {
                dateDisabled: disabled,
                formatYear: 'yy',
                maxDate: new Date(),
                minDate: new Date(),
                startingDay: 1,
                showWeeks: false
            };

            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                    mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }

            $scope.toggleMin = function () {
                $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
                $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
            };

            $scope.toggleMin();

            $scope.open1 = function () {
                $scope.popup1.opened = true;
            };

            $scope.open2 = function () {
                $scope.popup2.opened = true;
            };

            $scope.open3 = function () {
                $scope.popup3.opened = true;
            };

            $scope.open4 = function () {
                $scope.popup4.opened = true;
            };

            $scope.setDate = function (year, month, day) {
                $scope.dt = new Date(year, month, day);
            };

            $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];
            //$scope.altInputFormats = ['M!/d!/yyyy'];
            $scope.altInputFormats = ['dd-MMMM-yyyy'];

            $scope.popup1 = {
                opened: false
            };

            $scope.popup2 = {
                opened: false
            };

            $scope.popup3 = {
                opened: false
            };

            $scope.popup4 = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            $scope.events = [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];

            function getDayClass(data) {
                var date = data.date,
                    mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            }

            //date end

            //Resident Status

            $scope.residents = [];// ['PR', 'Citizen', 'WP', 'SP', 'EP', 'RC KPL', 'RC DYN', ' ].sort();
            if (vm.tenantName.toUpperCase() == 'SINGAPORE' || vm.tenantName.toUpperCase() == 'SUNMEC' || vm.tenantName.toUpperCase() == 'SUNMARINE' || vm.tenantName.toUpperCase() == 'MASTERTECH' || vm.tenantName.toUpperCase() == 'SNDT') {
                $scope.residents = ['Citizen', 'PR', 'WP', 'SP', 'EP', 'RC KPL', 'RC DYN', 'LOC'].sort();
            }
            else {
                $scope.residents = ['Citizen', 'PR', 'WP'].sort();
            }


            vm.getResidents = function (search) {
                var newSupes = $scope.residents.slice();
                return newSupes;
            }

            //Gender
            $scope.genders = ['Male', 'Female'].sort();

            vm.getGenders = function (search) {
                var newSupes = $scope.genders.slice();
                return newSupes;
            }
            //Staff type
            $scope.stafftypes = ['Permanent', 'Freelancer'].sort();

            vm.getStaffTypes = function (search) {
                var newSupes = $scope.stafftypes.slice();
                return newSupes;
            }

            //Race
            $scope.races = ['Chinese', 'Eurasian', 'Iban', 'Indian', 'Kadazan', 'Malay', 'Burmese', 'Others'].sort();

            vm.getRaces = function (search) {
                var newSupes = $scope.races.slice();
                //if (search && newSupes.indexOf(search) === -1) {
                //    newSupes.unshift(search);
                //}
                return newSupes;
            }

            //Religion
            $scope.religions = ['Buddhist', 'Christian', 'Hindu', 'Islam', 'None', 'Others'].sort();

            vm.getReligions = function (search) {
                var newSupes = $scope.religions.slice();
                //if (search && newSupes.indexOf(search) === -1) {
                //    newSupes.unshift(search);
                //}
                return newSupes;
            }

            //Department
            $scope.departments = ['Administration', 'Director', 'Finance', 'Human Resource', 'Manager', 'Marketing', 'NDT & ANDT', 'Research and Development', 'Sales', 'Support'].sort();

            vm.getDepartments = function (search) {
                var newSupes = $scope.departments.slice();
                //if (search && newSupes.indexOf(search) === -1) {
                //    newSupes.unshift(search);
                //}
                return newSupes;
            }

            //Marital
            $scope.maritals = ['Married', 'Single', 'Divorced', 'Widowed '].sort();

            vm.getMaritals = function (search) {
                var newSupes = $scope.maritals.slice();
                //if (search && newSupes.indexOf(search) === -1) {
                //    newSupes.unshift(search);
                //}
                return newSupes;
            }
            //Nationality
            $scope.nationalitys = ['MALAYSIAN', 'INDIAN', 'SINGAPORE', 'BANGLADESH', 'PHILLIPINES', 'INDONESIA', 'SOUTH AFRICA', 'AMERICAN', 'UK', 'MYANMAR'].sort();

            vm.getNationalitys = function (search) {
                var newSupes = $scope.nationalitys.slice();
                //if (search && newSupes.indexOf(search) === -1) {
                //    newSupes.unshift(search);
                //}
                return newSupes;
            }
            //Employee Status
            $scope.empstatus = ['Confirmed', 'Consolidated'].sort();

            vm.getEmpStatus = function (search) {
                var newSupes = $scope.empstatus.slice();
                //if (search && newSupes.indexOf(search) === -1) {
                //    newSupes.unshift(search);
                //}
                return newSupes;
            }
            //Bank Status
            $scope.bank = ['DBS', 'HSBC', 'IBG', 'OCBC', 'POSB', 'UOB'].sort();

            vm.getBanks = function (search) {
                var newSupes = $scope.bank.slice();
                return newSupes;
            }

            //Cost Centre
            $scope.costcentre = ['ADMIN', 'SNR-MGMT', 'OPERATION', 'TRANSPORT', 'OTHERS', 'MAINTENANCE'].sort();

            vm.getCostCentres = function (search) {
                var newSupes = $scope.costcentre.slice();
                return newSupes;
            }

            //WorkingSector 
            $scope.workingSector = ['General', 'Marine', 'Process', 'Construction', 'Others'].sort();
            vm.getWorkingSector = function (search) {
                var newSupes = $scope.workingSector.slice();
                return newSupes;
            }

            //JobLevel
            $scope.joblevel = ['Director', 'Clerical', 'General Worker', 'Non Professional', 'Professional', 'Sales', 'Semi Skilled', 'Skilled', 'Supervisory', 'Technical', 'Unskilled'].sort();

            vm.getJobLevels = function (search) {
                var newSupes = $scope.joblevel.slice();
                return newSupes;


            }

            //Concadinate Given Name and Surname
            vm.mergeGivenNameandSurName = function () {
                vm.personalinformation.employeeName = vm.personalinformation.employeeGivenName + " " + vm.personalinformation.employeeSurName;
            }

            vm.save = function () {
                if ($scope.existall == false)
                    return;

                if (vm.personalinformation.userSerialNumber == null) {
                    abp.notify.error("SerialNumberErr");
                    return;
                }

                if (vm.personalinformation.loginUserName == null) {
                    abp.notify.error("UserName");
                    return;
                }

                if (vm.personalinformation.email == vm.personalinformation.personalEmail) {
                    vm.personalinformation.personalEmail = '';
                }

                //if (vm.personalinformation.inchargeEmployeeRefId == null && vm.refactiveemployeecode.length>0) {
                //    abp.notify.error("InchargeEmployeeShouldBeActive");
                //    return;
                //}

                if (vm.personalinformation.defaultSkillRefId == null || vm.personalinformation.defaultSkillRefId == 0) {
                    abp.notify.error(app.localize('DefaultSkillSetCodeErr'));
                    abp.message.info(app.localize('DefaultSkillSetCodeErr'));
                    return;
                }

                if (vm.personalinformation.dateOfBirth == null) {
                    abp.notify.error(app.localize('DateOfBirthErr'));
                    abp.message.info(app.localize('DateOfBirthErr'));
                    return;
                }

                if (vm.personalinformation.dateHired == null) {
                    abp.notify.error(app.localize('DateHiredErr'));
                    abp.message.info(app.localize('DateHiredErr'));
                    return;
                }

                if (vm.personalinformation.staffType == null) {
                    abp.notify.error(app.localize('StaffTypeErr'));
                    abp.message.info(app.localize('StaffTypeErr'));
                    return;
                }

                if (vm.personalinformation.jobTitleRefId == null || vm.personalinformation.jobTitleRefId == 0) {
                    abp.notify.error(app.localize('JobTitleErr'));
                    abp.message.info(app.localize('JobTitleErr'));
                    return;
                }

                if (vm.personalinformation.gender == null) {
                    abp.notify.error(app.localize('Gender'));
                    abp.message.info(app.localize('Gender'));
                    return;
                }

                if (vm.personalinformation.employeeResidentStatusRefId == null) {
                    abp.notify.error(app.localize('ResidentStatus Empty'));
                    return;
                }

                if (vm.personalinformation.raceRefId == null) {
                    abp.notify.error(app.localize('Race Empty'));
                    return;
                }
                if (vm.personalinformation.religionRefId == null) {
                    abp.notify.error(app.localize('Religion Empty'));
                    return;
                }

                if (vm.personalinformation.employeeCostCentreRefId == null) {
                    abp.notify.error(app.localize('Cost Center Empty'));
                    return;
                }
                vm.personalinformation.employeeCode = vm.personalinformation.employeeCode.toUpperCase();

                if (vm.selectedUser != null)
                    vm.personalinformation.dinePlanUserId = vm.selectedUser.id;

                if (parseFloat(vm.autoserialnumber) != parseFloat(vm.personalinformation.userSerialNumber)) {
                    vm.serialnumberproceduretorun = true;
                }

                vm.personalinformation.dateHired = moment(vm.personalinformation.dateHired).format('YYYY-MM-DD');
                vm.personalinformation.dateOfBirth = moment(vm.personalinformation.dateOfBirth).format('YYYY-MM-DD');

                vm.saving = true;
                vm.errorFlag = true;
                personalinformationService.createOrUpdatePersonalInformation({
                    personalInformation: vm.personalinformation,
                    skillSetList: vm.skillSetRefId,
                    updateSerialNumberRequired: vm.serialnumberproceduretorun
                }).success(function (result) {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.autoserialnumber = null;
                    vm.serialnumberproceduretorun = false;
                    vm.cancel();
                    vm.errorFlag = false;
                    }).finally(function (result) {
                        
                        if (vm.errorFlag) {
                            $scope.dt = new Date(vm.personalinformation.dateOfBirth);
                            vm.personalinformation.dateOfBirth = $scope.dt;

                            if (vm.personalinformation.dateHired == null) {
                                $scope.dthire = null;
                            }
                            else {
                                $scope.dthire = new Date(vm.personalinformation.dateHired);
                            }
                            vm.personalinformation.dateHired = $scope.dthire;
                        }

                        vm.saving = false;
                });
            };

          

            vm.existall = function () {

                if (vm.personalinformation.employeeName == null) {
                    vm.existall = false;
                    return;
                }

                personalinformationService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'employeeName',
                    filter: vm.personalinformation.employeeName,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.personalinformation.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.personalinformation.employeeName + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $state.go("tenant.personalinformation");
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.getComboValueInString = function (item) {
                return item.value;
            };

            vm.refcompany = [];

            function fillDropDownCompany() {
                personalinformationService.getCompanyForCombobox({
                }).success(function (result) {
                    vm.refcompany = result.items;
                    if (result.items.length == 1) {
                        if (vm.personalinformation != null && vm.personalinformation.id == null) {
                            vm.personalinformation.companyRefId = result.items[0].value;
                            vm.getMaximumInfoDetails()
                        }
                    }
                });
            }

            vm.refWeekdays = [];
            function fillWeekDays() {
                personalinformationService.getWeekDaysForCombobox({
                }).success(function (result) {
                    vm.refWeekdays = result;
                });
            }
            fillWeekDays();

            vm.refskillset = [];

            function fillDropDownSkillSet() {
                personalinformationService.getSkillSetForCombobox({}).success(function (result) {
                    vm.refskillset = result.items;
                });
            }

            function setUserSno() {
                personalinformationService.getUserSerialNumberMax().success(function (result) {
                    vm.personalinformation.userSerialNumber = result.id;
                    vm.autoserialnumber = result.id;
                });
            }

            vm.changePicture = function (myObject) {

                if (vm.personalinformation.employeeCode == null || vm.personalinformation.employeeCode == "") {
                    abp.notify.error(app.localize('EmpCodeErr'));
                    return;
                }

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/personalinformation/changeProfilePicture.cshtml',
                    controller: 'tenant.views.hr.master.personalinformation.changeProfilePicture as vm',
                    backdrop: 'static',
                    resolve: {
                        personalinformationId: function () {
                            if (myObject == null) {
                                myObject = vm.personalinformation.employeeCode;
                            }
                            else
                                return myObject;

                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    if (result != null) {
                        vm.personalinformation.profilePictureId = result;
                    }
                    else {
                        personalinformationService.getPersonalInformationForEdit({
                            Id: myObject
                        }).success(function (result) {
                            vm.personalinformation.profilePictureId = result.personalInformation.profilePictureId;
                        });
                    }
                });
            };

            vm.getMaximumInfoDetails = function () {
                if (vm.personalinformation.id == null) {
                    personalinformationService.getMaximumEmpCodeAndBioMetricNumber({
                        companyRefId: vm.personalinformation.companyRefId
                    }).success(function (result) {
                        vm.personalinformation.employeeCode = result.employeeCode;
                        vm.personalinformation.bioMetricCode = result.bioMetricCode;
                    });
                }
            }

            function init() {
                vm.loading = true;
                fillDropDownSkillSet();
                personalinformationService.getPersonalInformationForEdit({
                    id: vm.personalinformationId
                }).success(function (result) {
                    vm.personalinformation = result.personalInformation;

                    dineplanuserService.getAll({
                        skipCount: 0,
                        maxResultCount: 10000,
                        sorting: null,
                    }).success(function (userresult) {
                        vm.users = userresult.items;
                        vm.selectedUser = vm.users.filter(function (item) {
                            return item.id === vm.personalinformation.dinePlanUserId;
                        })[0];
                    });

                    
                    fillDropDownCompany();
                    if (vm.personalinformation.id == null) {
                        setUserSno();
                        $scope.dt = new Date();
                        vm.personalinformation.dateOfBirth = null;
                        vm.personalinformation.maritalDate = null;
                        vm.personalinformation.dateHired = null;
                        vm.personalinformation.isAttendanceRequired = true;
                        vm.permissions.changeEmployeeCode = true;
                        vm.personalinformation.activeStatus = true;
                        vm.personalinformation.locationRefId = appSession.user.locationRefId;
                        vm.currentLocation = appSession.location.name;
                        vm.personalinformation.companyRefId = appSession.location.companyRefId;
                        vm.getCompanyName();
                    } else {
                        vm.currentLocation = result.personalInformation.locationRefName;
                        vm.skillSetRefId = result.skillSetList;
                        if (vm.cloneFlag == true) {
                            vm.personalinformation.id = null;
                            vm.personalinformation.dateHired = null;
                            vm.personalinformation.dateConfirmed = null;
                            vm.personalinformation.lastOfficialWorkingDate = null;
                            vm.personalinformation.lastWorkingDate = null;
                            vm.personalinformation.activeStatus = true;
                            vm.getMaximumInfoDetails();
                        }

                        $scope.dt = new Date(vm.personalinformation.dateOfBirth);
                        vm.personalinformation.dateOfBirth = $scope.dt;

                        if (vm.personalinformation.dateHired == null) {
                            $scope.dthire = null;
                        }
                        else {
                            $scope.dthire = new Date(vm.personalinformation.dateHired);
                        }
                        vm.personalinformation.dateHired = $scope.dthire;

                        //if (vm.personalinformation.maritalDate != null)
                        //    $scope.maritalDate = new Date(vm.personalinformation.maritalDate);
                        //else
                        //    $scope.maritalDate = null;

                        //vm.personalinformation.maritalDate = $scope.maritalDate;

                        vm.autoserialnumber = vm.personalinformation.userSerialNumber;

                        if (vm.personalinformation.lastWorkingDate == null) {
                            $scope.dtlwdt = null;
                        }
                        else {
                            $scope.dtlwdt = new Date(vm.personalinformation.lastWorkingDate);
                        }

                        vm.personalinformation.lastWorkingDate = $scope.dtlwdt;


                    }


                });
                abp.ui.clearBusy('#personalinformationForm');
            }
            init();

            vm.copyAddressFromPermanent = function () {
                vm.personalinformation.comAddress1 = vm.personalinformation.perAddress1;
                vm.personalinformation.comAddress2 = vm.personalinformation.perAddress2;
                vm.personalinformation.comAddress3 = vm.personalinformation.perAddress3;
                vm.personalinformation.comAddress4 = vm.personalinformation.perAddress4;
                vm.personalinformation.zipCode = vm.personalinformation.postalCode;
            }

            vm.getCompanyName = function () {
                
                vm.loading = true;
                personalinformationService.getCompanyNameById({
                    id : vm.personalinformation.companyRefId
                }).success(function (result) {
                    vm.personalinformation.companyRefName = result;
                    vm.loading = false;
                });
            }

            vm.refresidentList = [];

            function fillDropDownResidentStatus() {
                personalinformationService.getResidentStatusList({
                }).success(function (result) {
                    vm.refresidentList = result.items;
                });
            }
            fillDropDownResidentStatus();

            vm.refraces = [];

            function fillDropDownRaceStatus() {
                personalinformationService.getRacesStatusList({
                }).success(function (result) {
                    vm.refraces = result.items;
                });
            }
            fillDropDownRaceStatus();

            vm.refcostcentres = [];

            function fillDropDownCostCentre() {
                personalinformationService.getCostCentreMasterList({
                }).success(function (result) {
                    vm.refcostcentres = result.items;
                });
            }
            fillDropDownCostCentre();

            vm.refreligions = [];

            function fillDropDownReligions() {
                personalinformationService.getReligionList({
                }).success(function (result) {
                    vm.refreligions = result.items;
                });
            }
            fillDropDownReligions();

            vm.refjotitle = [];

            function fillDropDownJobTitle() {
                personalinformationService.getJobTitleDetails({
                }).success(function (result) {
                    vm.refjobtitle = result.items;
                });
            }
            fillDropDownJobTitle();

            vm.openLocation = function () {
                vm.locationGroup = vm.locationGroup = app.createLocationInputForCreateSingleLocation();

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    console.log(result);

                    if (result.locations.length > 0) {
                        vm.personalinformation.locationRefId = result.locations[0].id;
                        vm.currentLocation = result.locations[0].name;
                        vm.personalinformation.companyRefId = result.locations[0].companyRefId;
                        vm.getCompanyName();
                    }

                });

            }

        }
    ]);
})();

