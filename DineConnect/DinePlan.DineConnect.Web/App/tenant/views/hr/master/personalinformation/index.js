﻿
(function () {
    appModule.controller('tenant.views.hr.master.personalinformation.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.personalInformation', 'abp.services.app.company', 'abp.services.app.location','appSession' ,
        function ($scope, $state, $modal, uiGridConstants, personalinformationService, companyService, locationService, appSession) {
            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.allcompanyflag = true;

            vm.uilimit = 30;
            vm.refcompany = [];
            vm.locationRefId = appSession.location.id;

            function fillDropDownCompany() {
                personalinformationService.getCompanyForCombobox({}).success(function (result) {
                    vm.refcompany = result.items;
                });
            }

            fillDropDownCompany();

            vm.importSampleData = function () {
                vm.loading = true;
                personalinformationService.importPersonalInformationSampleList({
                    id: vm.locationRefId
                }).success(function (result) {
                    vm.getAll();
                    vm.loading = false;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.setupPer = {
                setup: abp.auth.hasPermission('Pages.Tenant.Setup'),
                importData: abp.auth.hasPermission('Pages.Tenant.Setup.ImportSeedData'),
                clearData: abp.auth.hasPermission('Pages.Tenant.Setup.CleanSeedData')
            };


            vm.refInchargeEmployeeList = [];
            vm.getInchargeList = function () {
                personalinformationService.getSimpleInchargeEmployeeDetails({}).success(function (result) {
                    vm.refInchargeEmployeeList = result.items;
                    vm.refInchargeEmployeeList.unshift({ id: 0, employeeCode: 0, employeeName: app.localize('All').toUpperCase() + ' ' + app.localize('Manager').toUpperCase() });
                    vm.inchargeFilter = 0;
                });
            }
            vm.getInchargeList();

            vm.refSupervisorEmployeeList = [];
            vm.getSupervisorList = function () {
                personalinformationService.getSimpleSupervisorEmployeeDetails({}).success(function (result) {
                    vm.refSupervisorEmployeeList = result.items;
                    vm.refSupervisorEmployeeList.unshift({ id: 0, employeeCode: 0, employeeName: ('All').toUpperCase() + ' ' + app.localize('Reporting').toUpperCase() + ' ' + app.localize('Supervisor').toUpperCase() });
                    vm.supervisorFilter = 0;
                });
            }
            vm.getSupervisorList();



            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.resignedFlag = false;
            vm.firstFlag = true;
            vm.acYear = parseInt(moment().format('YYYY'));
            vm.nextacYearShowFlag = false;
            var month = parseInt(moment().format('MM'));
            if (month > 9) {
                vm.nextacYearShowFlag = true;
                vm.nextacYear = parseInt(vm.acYear + 1);
            }


            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Hr.Master.PersonalInformation.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Hr.Master.PersonalInformation.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Hr.Master.PersonalInformation.Delete'),
                'import': abp.auth.hasPermission('Pages.Tenant.Hr.Master.PersonalInformation.Import'),
                'clone': abp.auth.hasPermission('Pages.Tenant.Hr.Master.PersonalInformation.Clone'),
                'leaveGrant': abp.auth.hasPermission('Pages.Tenant.Hr.Master.PersonalInformation.LeaveGrant'),
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: 15,// app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.advancedFiltersAreShown = false;
            vm.dateFilterApplied = false;

            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format($scope.format);
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                //startDate: todayAsString,
                endDate: todayAsString,
                maxDate: moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: 10, // app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 60,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editPersonalInformation(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.resignEmployee(row.entity)\">" + app.localize("Resignation") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.checkUserName(row.entity)\">" + app.localize("UserName") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.clone && row.entity.activeStatus==false\" ng-click=\"grid.appScope.clonePersonalInformation(row.entity)\">" + app.localize("Clone") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.leaveGrant && row.entity.activeStatus==true\" >" + app.localize("------------------") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.leaveGrant && row.entity.activeStatus==true\" ng-click=\"grid.appScope.openLeaveDetails(row.entity,grid.appScope.acYear)\">" + app.localize("LeaveMasterAcYear", vm.acYear) + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.nextacYearShowFlag && grid.appScope.permissions.leaveGrant && row.entity.activeStatus==true\" ng-click=\"grid.appScope.openLeaveDetails(row.entity,grid.appScope.acYear)\">" + app.localize("LeaveMasterAcYear", (vm.nextacYear)) + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.nextacYearShowFlag && grid.appScope.permissions.leaveGrant && row.entity.activeStatus==true\" ng-click=\"grid.appScope.openLeaveDetails(row.entity,grid.appScope.acYear)\">" + app.localize("LeaveMasterAcYear", (vm.nextacYear)) + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"


                        //cellTemplate:
                        //        '<div class=\"ui-grid-cell-contents text-center\">' +
                        //        '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.editPersonalInformation(row.entity)"><i class="fa fa-edit"></i></button>' +
                        //         ' <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteNdtProcedure(row.entity)\">" + app.localize("Delete") + "</a></li>' +
                        //        '</div>'
                    },
                    {
                        name: app.localize('CompanyCode'),
                        field: 'companyRefCode',
                        width: 70

                    },
                    {
                        name: app.localize('Bio#'),
                        field: 'bioMetricCode',
                        width: 60,
                        cellClass: function (grid, row) {
                            if (!row.entity.activeStatus)
                                return 'ui-redcolor';
                        },
                    },
                    {
                        name: app.localize('EmployeeCode'),
                        field: 'employeeCode',
                        width: 100,
                        cellClass: function (grid, row) {
                            if (!row.entity.activeStatus)
                                return 'ui-redcolor';
                        },
                    },
                    {
                        name: app.localize('EmployeeName'),
                        field: 'employeeName',
                        minWidth: 200,
                        cellClass: function (grid, row) {
                            if (!row.entity.activeStatus)
                                return 'ui-redcolor';
                            else
                                return 'ui-bold';
                        },
                    },
                    {
                        name: app.localize('DOJ') + "/" + app.localize('Resigned'),
                        field: 'dateHired',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span> {{row.entity.dateHired | date :\'dd-MMM-yyyy\'}}  <span ng-if=\'row.entity.lastWorkingDate\' class=\"bold\" > to {{row.entity.lastWorkingDate | date :\'dd-MMM-yyyy\' }} </span> </span>' +
                            //'  <br ng-if=\'row.entity.lastWorkingDate.hasValue\'>' +
                            //'  <br ng-if=\'row.entity.lastWorkingDate\'>' +
                            //'  <span ng-if=\'row.entity.lastWorkingDate\' class=\"bold\" > {{row.entity.lastWorkingDate | date :\'dd-MMM-yyyy\' }} </span>' +
                            '</div>',
                        cellClass: function (grid, row) {
                            if (!row.entity.activeStatus)
                                return 'ui-redcolor';
                            else
                                return 'ui-bold';
                        },
                    },
                    {
                        name: app.localize('DefaultSkillCode'),
                        field: 'defaultSkillSetCode',
                        minWidth: 100,
                        cellClass: function (grid, row) {
                            if (!row.entity.activeStatus)
                                return 'ui-redcolor';
                        },
                    },
                    {
                        name: app.localize('JobTitle'),
                        field: 'jobTitle',
                        cellClass: function (grid, row) {
                            if (!row.entity.activeStatus)
                                return 'ui-redcolor';
                        },
                    },


                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            $scope.arrworkStatus = [app.localize('All'), app.localize('Active'), app.localize('Resigned')];
            vm.getWorkStatus = function (search) {
                var newSupes = $scope.arrworkStatus.slice();
                return newSupes;
            };
            vm.workStatus = app.localize('Active');

            vm.getAll = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var jobTitle = null;
                var defaultSkillSetCode = null;
                var resignedFlag = null;
                var activeFlag = null;
                if (vm.allcompanyflag == true)
                    vm.companies = null;

                if (vm.workStatus == app.localize('All')) {
                    resignedFlag = null;
                    activeFlag = null;
                }
                else if (vm.workStatus == app.localize('Active')) {
                    resignedFlag = null;
                    activeFlag = true;
                }
                else if (vm.workStatus == app.localize('Resigned')) {
                    resignedFlag = true;
                    activeFlag = null;
                }

                if (vm.advancedFiltersAreShown) {
                    if (vm.dateFilterApplied) {
                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                    jobTitle = vm.jobTitle;
                    defaultSkillSetCode = vm.defaultSkillSetCode;
                }

                var supervisorEmployeeRefId = null;
                if (vm.supervisorFilter != 0 && vm.supervisorFilter > 0) {
                    supervisorEmployeeRefId = vm.supervisorFilter;
                }

                var inchargeEmployeeRefId = null;
                if (vm.inchargeFilter != 0 && vm.inchargeFilter > 0) {
                    inchargeEmployeeRefId = vm.inchargeFilter;
                }


                personalinformationService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    startDate: startDate,
                    endDate: endDate,
                    jobTitle: jobTitle,
                    defaultSkillSetCode: defaultSkillSetCode,
                    resignedFlag: resignedFlag,
                    activeFlag: activeFlag,
                    employeeRefCode: vm.employeeRefCode,
                    companies: vm.companies,
                    supervisorEmployeeRefId: supervisorEmployeeRefId,
                    inchargeEmployeeRefId: inchargeEmployeeRefId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.checkUserName = function (myObj) {
                vm.loading = true;
                personalinformationService.getEmployeeDetailsBasedOnUserName({
                    userName: vm.filterText
                }).success(function (result) {
                    var a = result;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.editPersonalInformation = function (myObj) {
                openCreateOrEditModal(myObj.id, false);
            };

            vm.createPersonalInformation = function () {
                openCreateOrEditModal(null, false);
            };

            vm.deletePersonalInformation = function (myObject) {
                abp.message.confirm(
                    app.localize('DeletePersonalInformationWarning', myObject.employeeName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            personalinformationService.deletePersonalInformation({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.changePicture = function (myObject) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/personalinformation/changeProfilePicture.cshtml',
                    controller: 'tenant.views.hr.master.personalinformation.changeProfilePicture as vm',
                    backdrop: 'static',
                    resolve: {
                        personalinformationId: function () {
                            return myObject.id;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            };

            vm.resignEmployee = function (myObj) {
                employeeResignationForm(myObj.id);
            };

            function employeeResignationForm(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/personalinformation/resignation.cshtml',
                    controller: 'tenant.views.hr.master.personalinformation.resignation as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        personalinformationId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.clonePersonalInformation = function (myObj) {
                openCreateOrEditModal(myObj.id, true);
            };

            function openCreateOrEditModal(objId, cloneFlag) {
                $state.go("tenant.personalinformationdetail", {
                    id: objId,
                    cloneFlag: cloneFlag
                });
            }

            vm.clear = function () {
                vm.dateRangeModel = null;
                startDate = null;
                endDate = null;
                vm.poReferenceCode = null;
                vm.defaultLocationId = null;
                vm.companyRefId = "";
                vm.jobTitle = "";
                vm.defaultSkillSetCode = "";
                vm.filterText = null;
                vm.dateFilterApplied = false;
                vm.getAll();
            };

            vm.exportToExcel = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var jobTitle = null;
                var defaultSkillSetCode = null;
                var resignedFlag = null;
                var activeFlag = null;
                if (vm.allcompanyflag == true)
                    vm.companies = null;

                if (vm.workStatus == app.localize('All')) {
                    resignedFlag = null;
                    activeFlag = null;
                }
                else if (vm.workStatus == app.localize('Active')) {
                    resignedFlag = null;
                    activeFlag = true;
                }
                else if (vm.workStatus == app.localize('Resigned')) {
                    resignedFlag = true;
                    activeFlag = null;
                }

                if (vm.advancedFiltersAreShown) {
                    if (vm.dateFilterApplied) {
                        startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                        endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                    }
                    jobTitle = vm.jobTitle;
                    defaultSkillSetCode = vm.defaultSkillSetCode;
                }

                var supervisorEmployeeRefId = null;
                if (vm.supervisorFilter != 0 && vm.supervisorFilter > 0) {
                    supervisorEmployeeRefId = vm.supervisorFilter;
                }

                var inchargeEmployeeRefId = null;
                if (vm.inchargeFilter != 0 && vm.inchargeFilter > 0) {
                    inchargeEmployeeRefId = vm.inchargeFilter;
                }


                personalinformationService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    startDate: startDate,
                    endDate: endDate,
                    jobTitle: jobTitle,
                    defaultSkillSetCode: defaultSkillSetCode,
                    resignedFlag: resignedFlag,
                    activeFlag: activeFlag,
                    employeeRefCode: vm.employeeRefCode,
                    companies: vm.companies,
                    supervisorEmployeeRefId: supervisorEmployeeRefId,
                    inchargeEmployeeRefId: inchargeEmployeeRefId
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        vm.loading = false;
                    });
            };

            vm.import = function () {
                importModal();
            };

            function importModal() {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/personalinformation/importModal.cshtml',
                    controller: 'tenant.views.hr.master.personalinformation.importModal as vm',
                    backdrop: 'static'
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                    vm.loading = false;
                });
            }


            vm.getAll();

            vm.openLeaveDetails = function (data, argAcYear) {

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/yearwiseleaveallowedforemployee/yearWiseLeaveDetails.cshtml',
                    controller: 'tenant.views.hr.master.yearwiseleaveallowedforemployee.yearWiseLeaveDetails as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        employeeRefId: function () {
                            return data.id;
                        },
                        acYear: function () {
                            return argAcYear;
                        },
                        gender: function () {
                            return data.gender;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    //
                });
            }

        }]);
})();

