﻿
(function () {
    appModule.controller('tenant.views.hr.master.personalinformation.changeProfilePicture', [
        '$scope', 'appSession', '$uibModalInstance', 'FileUploader', 'abp.services.app.personalInformation', 'personalinformationId',
        function ($scope, appSession, $uibModalInstance, fileUploader, personalinformationService, personalinformationId) {
            var vm = this;
            /* eslint-disable */

            vm.uploader = new fileUploader({
                    url: abp.appPath + 'PersonalInformation/ChangePerInfoPicture',
                    formData: [{ id: personalinformationId }],
                    queueLimit: 1,
                    filters: [{
                        name: 'imageFilter',
                        fn: function (item, options) {
                            //File type check
                            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                            if ('|jpg|jpeg|'.indexOf(type) === -1) {
                                abp.message.warn(app.localize('ProfilePicture_Warn_FileType'));
                                return false;
                            }
                          

                            //File size check
                            if (item.size > 30720) //3000KB
                            {
                                abp.message.warn(app.localize('ProfilePicture_Warn_SizeLimit'));
                                return false;
                            }

                        return true;
                    }
                }]
            });

            vm.testdata = "Raja";
            vm.empid = 3;

            vm.save = function () {
                vm.testdata = "Siva";
                vm.empid = 2;

                vm.uploader.uploadAll();
            };

            vm.uploader.onBeforeUploadItem = function (fileitem) {
                fileitem.formData.push({ data: vm.testdata });
                fileitem.formData.push({ empid: vm.empid });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.uploader.onErrorItem = function (fileItem, response, status, headers) {
                if (response.success) {
                    $uibModalInstance.close(response.result);
                } else {
                    abp.message.error(response);
                    abp.notify.error(response);
                    vm.loading = false;
                    vm.saving = false;
                    return;
                }
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.success) {
                    $uibModalInstance.close(response.result);
                } else {
                    abp.message.error(response.error.message);
                    $uibModalInstance.close();
                    return;
                }
            };
        }
    ]);
})();