﻿
(function () {
    appModule.controller('tenant.views.hr.master.employeeskillset.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.employeeSkillSet', 'employeeskillsetId', 'abp.services.app.personalInformation',
        function ($scope, $modalInstance, employeeskillsetService, employeeskillsetId, personalinformationService) {
            var vm = this;
            /*eslint eqeqeq: "error"*/
            /* eslint-disable */

            vm.saving = false;
            vm.employeeskillset = null;
            $scope.existall = true;
            vm.skillSetRefId = [];

            vm.save = function () {
                if ($scope.existall == false)
                    return;
                vm.saving = true;

                employeeskillsetService.createOrUpdateEmployeeSkillSet({
                    employeeSkillSet: vm.employeeskillset,
                    skillSetList: vm.skillSetRefId
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {

                if (vm.employeeskillset.id == null) {
                    vm.existall = false;
                    return;
                }

                employeeskillsetService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.employeeskillset.id,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.employeeskillset.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.employeeskillset.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.refemployeecode = [];

            function fillDropDownPersonalinformation() {
                personalinformationService.getEmployeeForCombobox({}).success(function (result) {
                    vm.refemployeecode = result.items;
                });
            }

            vm.refskillset = [];

            function fillDropDownSkillSet() {
                personalinformationService.getSkillSetForCombobox({}).success(function (result) {
                    vm.refskillset = result.items;
                });
            }

            function init() {
                fillDropDownPersonalinformation();
                fillDropDownSkillSet();
                employeeskillsetService.getEmployeeSkillSetForEdit({
                    Id: employeeskillsetId
                }).success(function (result) {
                    vm.employeeskillset = result.employeeSkillSet;
                    vm.skillSetRefId = result.skillSetList;
                });
            }
            init();
        }
    ]);
})();

