﻿    (function () {
        appModule.controller('tenant.views.hr.master.employeedocumentinfo.fileDownload', [
    '$scope', '$uibModalInstance', '$uibModal', 'abp.services.app.employeeDocumentInfo', 'employeedocumentinfoId', 'abp.services.app.personalInformation',  'appSession',
        function ($scope, $modalInstance, $modal, employeedocumentinfoService, employeedocumentinfoId, personalinformationService,  appSession) {

            var vm = this;
            
            vm.saving = false;
            vm.employeedocumentinfo = null;
						$scope.existall = true;

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
						vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
						 vm.refemployeecode = [];

						 function fillDropDownPersonalinformation() {
								 personalinformationService.getEmployeeForCombobox({}).success(function (result) {
										 vm.refemployeecode = result.items;
								 });
						 }
						 vm.refdocumentinfo = [];

						 function fillDropDownDocumentInfo() {
								 employeedocumentinfoService.getDocumentInfo({}).success(function (result) {
										 vm.refdocumentinfo = result;
								 });
						 }

			 function init() {
				    
                    employeedocumentinfoService.getEmployeeDocumentInfoForEdit({
                        Id: employeedocumentinfoId
                    }).success(function (result) {
                        vm.employeedocumentinfo = result.employeeDocumentInfo;

                        if(vm.employeedocumentinfo.id != null){
                            if (vm.employeedocumentinfo.fileName == null) {
                                vm.cancel();
                                alert("No File Is Uploaded To Download");
                                
                            }
                            else {
                                
                                fillDropDownPersonalinformation();
                                fillDropDownDocumentInfo();

                                $("#filebrowse").hide();

                                DownloadPersonalDocumentTemplate
                                vm.importpath = abp.appPath + 'PersonalInformation/DownloadPersonalDocumentTemplate?employeeRefId=' + vm.employeedocumentinfo.employeeRefId
																	+ "&documentInfoRefId=" + vm.employeedocumentinfo.documentInfoRefId;

																window.open(vm.importpath);

                            } 
                        }
                                          
                    });
			     }

			     init();

			  
        }
    ]);
})();

