﻿(function () {
    appModule.controller('tenant.views.hr.master.employeedocumentinfo.employeeDocumentInfo', [
        '$scope', '$uibModalInstance', '$uibModal', '$stateParams', 'abp.services.app.employeeDocumentInfo', 'employeedocumentinfoId', 'abp.services.app.personalInformation', 'FileUploader', 'appSession',
        function ($scope, $modalInstance, $modal, $stateParams, employeedocumentinfoService, employeedocumentinfoId, personalinformationService, fileUploader, appSession) {

            var vm = this;

            vm.saving = false;
            vm.employeedocumentinfo = null;
            $scope.existall = true;
            vm.changeFlag = false;

            vm.loginStatus = $stateParams.loginStatus;
            vm.currentEmployeeRefId = null;
            vm.uilimit = 20;


            if (vm.loginStatus == 'EMP') {
                if (appSession.useremployeerefid == null || appSession.useremployeerefid == 0) {
                    abp.notify.warn(app.localize('EmployeeCodeNotAssignedToThisUser', appSession.user.userName));
                    abp.message.warn(app.localize('EmployeeCodeNotAssignedToThisUser', appSession.user.userName));
                    vm.errorFlag = true;
                }
                vm.currentEmployeeRefId = appSession.useremployeerefid;

            }

            vm.uploader = new fileUploader({
                url: abp.appPath + 'PersonalInformation/ChangeEmployeeDocumentInfo',
                formData: [{ id: employeedocumentinfoId }],
                queueLimit: 1,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        //File type check
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        if ('|jpg|jpeg|pdf|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('EmployeeDocument_Warn_FileType'));
                            return false;
                        }
                        //File size check
                        if (item.size > 5242880) //3000KB
                        {
                            abp.message.warn(app.localize('EmployeeDocument_Warn_SizeLimit'));
                            return false;
                        }
                        return true;
                    }
                }]
            });

            vm.uploader.onBeforeUploadItem = function (fileitem) {
                fileitem.formData.push({ employeeRefId: vm.employeedocumentinfo.employeeRefId });
                fileitem.formData.push({ documentInfoRefId: vm.employeedocumentinfo.documentInfoRefId })
            };

            vm.uploader.onErrorItem = function (fileItem, response, status, headers) {
                if (response.success) {
                    vm.cancel();
                } else {
                    abp.message.error(response);
                    abp.notify.error(response);
                    vm.loading = false;
                    vm.saving = false;
                    return;
                }
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                vm.cancel();
            };

            //date start
            //  Date Range Options
            var todayAsString = moment().format('YYYY-MMM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };


            //  Date Angular Js 
            $scope.today = function () {
                $scope.dt = moment(new Date()).format("DD-MMM-YYYY");

            };
            $scope.today();

            $scope.clear = function () {
                $scope.dt = null;
            };

            $scope.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            $scope.dateOptions = {
                dateDisabled: disabled,
                formatYear: 'yy',
                maxDate: new Date(2100, 1, 01),
                minDate: new Date(),
                startingDay: 1,
                showWeeks: false
            };

            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                    mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }

            $scope.toggleMin = function () {
                $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
                $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
            };

            $scope.toggleMin();

            $scope.open1 = function () {
                $scope.popup1.opened = true;
            };

            $scope.open2 = function () {
                $scope.popup2.opened = true;
            };

            $scope.open3 = function () {
                $scope.popup3.opened = true;
            };

            $scope.open4 = function () {
                $scope.popup4.opened = true;
            };

            $scope.setDate = function (year, month, day) {
                $scope.dt = new Date(year, month, day);
            };

            $scope.formats = ['yyyy-MMM-dd'];
            //$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];
            //$scope.altInputFormats = ['M!/d!/yyyy'];
            $scope.altInputFormats = ['dd-MMMM-yyyy'];

            $scope.popup1 = {
                opened: false
            };

            $scope.popup2 = {
                opened: false
            };

            $scope.popup3 = {
                opened: false
            };

            $scope.popup4 = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            $scope.events = [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];

            function getDayClass(data) {
                var date = data.date,
                    mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            }

            //date end

            vm.documentAlreadyExist = function () {

                if (vm.employeedocumentinfo.id == null) {
                    vm.loading = true;
                    employeedocumentinfoService.employeeDocumentAlreadyExist({
                        employeeRefId: vm.employeedocumentinfo.employeeRefId,
                        documentInfoRefId: vm.employeedocumentinfo.documentInfoRefId
                    }).success(function (result) {
                        if (result == true) {
                            abp.notify.warn(app.localize('DocumentAlreadyExists'));
                            abp.message.warn(app.localize('DocumentAlreadyExists'));
                        }
                        vm.loading = false;
                    }).finally(function () {

                    });
                }
            };



            vm.save = function () {
                if ($scope.existall == false)
                    return;

                $scope.errmessage = '';
                if (vm.employeedocumentinfo.id == null) {
                    if (vm.employeedocumentinfo.lifeTimeFlag == true) {
                        vm.employeedocumentinfo.startDate = "";
                        vm.employeedocumentinfo.endDate = "";
                        vm.employeedocumentinfo.defaultAlertDays = "";

                    }
                }
                else {
                    if (CompareDate() == false) {
                        vm.saving = false;
                        return;
                    }
                }
                if (vm.employeedocumentinfo.employeeRefId == null || vm.employeedocumentinfo.employeeRefId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('EmployeeCodeErr');
                }
                if (vm.employeedocumentinfo.documentInfoRefId == null || vm.employeedocumentinfo.documentInfoRefId == 0) {
                    $scope.errmessage = $scope.errmessage + app.localize('DocumentInfoErr');
                }

                if ($scope.errmessage != '') {
                    abp.message.warn($scope.errmessage);
                    abp.notify.warn($scope.errmessage);
                    return;
                }
                vm.saving = true;

                //vm.employeedocumentinfo.fileExtenstionType = "PDF";

                employeedocumentinfoService.createOrUpdateEmployeeDocumentInfo({
                    employeeDocumentInfo: vm.employeedocumentinfo
                }).success(function (result) {
                    if (vm.uploader.queue.length == 0 && vm.employeedocumentinfo.fileName == null) {
                        abp.notify.warn(app.localize('YouAreNotSelectAFile'));
                        return;
                    }
                    vm.documentinfoid = result.id;
                    vm.uploader.uploadAll();
                }).finally(function () {
                    vm.saving = false;
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    if (vm.uploader.queue.length == 0) {
                        vm.cancel();
                    }
                });
            };

            function CompareDate() {
                var startDate = $('#stDate').val();
                var endDate = $('#endDate').val();

                if (startDate >= endDate) {
                    abp.notify.warn(app.localize('ReqDateErr'));
                    abp.message.info(app.localize('ReqDateErr'));
                    // alert("End date can be greater than Start date");
                    return false;
                }
            }

            vm.existall = function () {
                if (vm.employeedocumentinfo.id == null) {
                    vm.existall = false;
                    return;
                }
            };

            vm.changeVariable = function () {
                vm.changeFlag = true;
            };

            vm.cancel = function () {
                if (vm.loginStatus == 'EMP')
                    $modalInstance.dismiss("tenant.employeedocumentview");
                //  $state.go("tenant.employeedocumentview")
                else
                    // $state.go("tenant.employeedocumentinfo")
                    $modalInstance.dismiss("tenant.employeedocumentinfo");
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.refemployeecode = [];

            function fillDropDownPersonalinformation() {
                personalinformationService.getEmployeeWithSkillSetForCombobox({}).success(function (result) {
                    vm.refemployeecode = result;

                    if (vm.currentEmployeeRefId != null && vm.currentEmployeeRefId > 0) {
                        vm.tempemp = [];
                        angular.forEach(vm.refemployeecode, function (value, key) {
                            if (parseInt(value.employeeRefId) == vm.currentEmployeeRefId) {
                                vm.tempemp.push(value);
                            }
                        });

                        vm.refemployeecode = vm.tempemp;

                        if (vm.refemployeecode.length == 1)
                            vm.employeedocumentinfo.employeeRefId = vm.refemployeecode[0].employeeRefId;
                    }
                });
            }
            fillDropDownPersonalinformation();

            vm.refdocumentinfo = [];

            function fillDropDownDocumentInfo() {
                employeedocumentinfoService.getDocumentInfo({}).success(function (result) {
                    vm.refdocumentinfo = result;
                });
            }

            vm.getDefaultAlertDays = function (data) {
                vm.employeedocumentinfo.defaultAlertDays = data.defaultAlertDays;
                vm.employeedocumentinfo.lifeTimeFlag = data.lifeTimeFlag;
                vm.employeedocumentinfo.defaultExpiryDays = data.defaultExpiryDays;
            }

            vm.setEndDate = function () {
                if (vm.employeedocumentinfo.defaultExpiryDays > 0 && vm.employeedocumentinfo.endDate == null) {
                    var ed = moment(vm.employeedocumentinfo.startDate).add('days', vm.employeedocumentinfo.defaultExpiryDays);
                    vm.employeedocumentinfo.endDate = moment(ed).format($scope.format);
                }
            }


            vm.attachFile = function (myObject) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/employeedocumentinfo/documentAttach.cshtml',
                    controller: 'tenant.views.hr.master.employeedocumentinfo.documentAttach as vm',
                    backdrop: 'static',
                    resolve: {
                        employeedocumentinfoId: function () {
                            if (myObject == null) {
                                myObject = vm.employeedocumentinfo.employeeRefId;
                            }
                            else
                                return myObject;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    if (result != null) {
                        vm.employeedocumentinfo.fileName = result;
                    }
                    else {
                        employeedocumentinfoService.getEmployeeDocumentInfoForEdit({
                            Id: myObject
                        }).success(function (result) {
                            vm.employeedocumentinfo.fileName = result.employeedocumentinfo.fileName;
                        });
                    }
                });
            };

            //vm.showCheck = function () {

            //    $scope.currentdate = new Date();
            //    var content = vm.employeedocumentinfo.startDate;

            //    if (vm.employeedocumentinfo.startDate == null || vm.employeedocumentinfo.endDate == null)
            //        $('#islifetime').show();
            //};

            vm.hideCheck = function () {

                var content = vm.employeedocumentinfo.startDate;

                if (vm.employeedocumentinfo.startDate != null || vm.employeedocumentinfo.endDate != null) {
                    $('#islifetime').hide();
                    vm.employeedocumentinfo.lifeTimeFlag = false;
                }

            };


            function init() {
                fillDropDownDocumentInfo();
                employeedocumentinfoService.getEmployeeDocumentInfoForEdit({
                    Id: employeedocumentinfoId
                }).success(function (result) {
                    vm.employeedocumentinfo = result.employeeDocumentInfo;
                    //vm.showCheck();

                    if (vm.employeedocumentinfo.id == null) {
                        vm.uilimit = 20;
                        $scope.dt = new Date();
                        vm.employeedocumentinfo.startDate = $scope.dt;
                        vm.employeedocumentinfo.endDate = $scope.dt;
                        vm.employeedocumentinfo.defaultAlertDays = "";
                        vm.importpath = "";
                    }
                    else {
                        vm.uilimit = null;
                        $scope.stdt = new Date(vm.employeedocumentinfo.startDate);
                        vm.employeedocumentinfo.startDate = $scope.stdt;
                        $scope.enddt = new Date(vm.employeedocumentinfo.endDate);
                        vm.employeedocumentinfo.endDate = $scope.enddt;

                        if (vm.employeedocumentinfo.lifeTimeFlag == true) {
                            vm.employeedocumentinfo.startDate = "";
                            vm.employeedocumentinfo.endDate = "";
                            vm.employeedocumentinfo.defaultAlertDays = "";

                        }


                        if (vm.employeedocumentinfo.startDate != null && vm.employeedocumentinfo.startDate != "")
                            $("#islifetime").hide();


                        vm.importpath = abp.appPath + 'PersonalInformation/DownloadPersonalDocumentTemplate?employeeRefId=' + vm.employeedocumentinfo.employeeRefId
                            + "&documentInfoRefId=" + vm.employeedocumentinfo.documentInfoRefId;


                        //if (vm.employeedocumentinfo.fileName == null) {
                        //    $("#filebrowse").show();
                        //    //$("#viewfile").hide();
                        //}
                        //else {
                        //    $("#filebrowse").hide();
                        //   // $("#viewfile").show();
                        //}
                    }
                });
            }

            init();


        }
    ]);
})();

