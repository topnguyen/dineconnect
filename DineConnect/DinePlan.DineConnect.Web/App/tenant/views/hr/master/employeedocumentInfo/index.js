﻿
(function () {
    appModule.controller('tenant.views.hr.master.employeedocumentinfo.index', [
        '$scope', '$uibModal', '$stateParams', 'uiGridConstants', 'abp.services.app.employeeDocumentInfo', '$http', 'appSession',
        function ($scope, $modal, $stateParams, uiGridConstants, employeedocumentinfoService, $http, appSession) {
            var vm = this;
            vm.changeFlag = false;
			/* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.documentInfoRefId = null;
            vm.dateFilterApplied = false;

            vm.activeEmployeeOnly = true;

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            var sd = moment().subtract(6, 'day');
            var td = moment();

            vm.dateRangeModel = {
                startDate: sd,
                endDate: td
            };

            var todayAsString = moment().format($scope.format);
            var weekAgoAsString = moment().subtract(7, 'day').format($scope.format);

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: vm.dateRangeModel.startDate,
                endDate: vm.dateRangeModel.endDate
            });

            vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
            vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;


            vm.dateSetup = function () {
                var sd = moment().subtract(6, 'day');
                var td = moment();
                vm.dateRangeModel = {
                    startDate: sd,
                    endDate: td
                };
            };

            vm.loginStatus = $stateParams.loginStatus;
            vm.currentEmployeeRefId = null;
            vm.clientflag = false;

            if (vm.loginStatus == 'EMP') {
                if (vm.userclientrefid == 2641) {
                    vm.clientflag = true;
                    vm.currentEmployeeRefId = 129;
                    vm.permissions = {
                        create: false, // abp.auth.hasPermission('Pages.Tenant.Hr.Employee.EmployeeDocumentInfo.Create'),
                        edit: false, //abp.auth.hasPermission('Pages.Tenant.Hr.Employee.EmployeeDocumentInfo.Edit'),
                        'delete': false // abp.auth.hasPermission('Pages.Tenant.Hr.Employee.EmployeeDocumentInfo.Delete')
                    };
                }
                else {
                    if (appSession.useremployeerefid == null || appSession.useremployeerefid == 0) {
                        abp.notify.warn(app.localize('EmployeeCodeNotAssignedToThisUser', appSession.user.userName));
                        abp.message.warn(app.localize('EmployeeCodeNotAssignedToThisUser', appSession.user.userName));
                        vm.errorFlag = true;
                    }
                    vm.currentEmployeeRefId = appSession.useremployeerefid;
                    vm.permissions = {
                        create: abp.auth.hasPermission('Pages.Tenant.Hr.Employee.EmployeeDocumentInfo.Create'),
                        edit: abp.auth.hasPermission('Pages.Tenant.Hr.Employee.EmployeeDocumentInfo.Edit'),
                        'delete': abp.auth.hasPermission('Pages.Tenant.Hr.Employee.EmployeeDocumentInfo.Delete')
                    };
                }
            }
            else {
                vm.permissions = {
                    create: abp.auth.hasPermission('Pages.Tenant.Hr.Master.EmployeeDocumentInfo.Create'),
                    edit: abp.auth.hasPermission('Pages.Tenant.Hr.Master.EmployeeDocumentInfo.Edit'),
                    'delete': abp.auth.hasPermission('Pages.Tenant.Hr.Master.EmployeeDocumentInfo.Delete')
                };
            }

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.downloadDocument = function (employeerow) {
                fileDownload(employeerow);

                // fileDownload(employeerow.id);
            };


            vm.userGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
					{
						name: app.localize('Actions'),
						enableSorting: false,
						width: 60,

						cellTemplate:
                             '<div ng-if=\"grid.appScope.permissions.edit\" class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.editEmployeeDocumentInfo(row.entity)"><i class="fa fa-edit"></i></button>' +
                                '</div>'
					},
                    {
                        name: app.localize('Delete'),
                        enableSorting: false,
                        width: 60,

                        cellTemplate:
                            '<div ng-if=\"grid.appScope.permissions.delete\" class=\"ui-grid-cell-contents text-center\">' +
                            '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.deleteEmployeeDocumentInfo(row.entity)"><i class="fa fa-trash"></i></button>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Employee'),
                        field: 'employeeRefName',
                        minWidth: 230,
                        cellClass: function (grid, row) {
                            if (row.entity.documentAlertStatus == 'Y')
                                return 'ui-browncolor';
                            else if (row.entity.documentAlertStatus == 'R')
                                return 'ui-redcolor';
                        },
                    },
                    {
                        name: app.localize('DocumentName'),
                        field: 'documentInfoRefName',
                        cellClass: function (grid, row) {
                            if (row.entity.documentAlertStatus == 'Y')
                                return 'ui-browncolor';
                            else if (row.entity.documentAlertStatus == 'R')
                                return 'ui-redcolor';
                        },
                    },
                    {
                        name: app.localize('IsLifeTime'),
                        field: 'lifeTimeFlag',
                        cellClass: function (grid, row) {
                            if (row.entity.documentAlertStatus == 'Y')
                                return 'ui-browncolor';
                            else if (row.entity.documentAlertStatus == 'R')
                                return 'ui-redcolor';
                        },
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.lifeTimeFlag" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.lifeTimeFlag" class="label label-default">' + app.localize('No') + '</span>' +
                            '</div>',
                        minWidth: 80
                    },
                    {
                        name: app.localize('ValidityFrom'),
                        field: 'startDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                        cellClass: function (grid, row) {
                            if (row.entity.documentAlertStatus == 'Y')
                                return 'ui-browncolor';
                            else if (row.entity.documentAlertStatus == 'R')
                                return 'ui-redcolor';
                        },
                    },
                    {
                        name: app.localize('ValidityTo'),
                        field: 'endDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                        cellClass: function (grid, row) {
                            if (row.entity.documentAlertStatus == 'Y')
                                return 'ui-browncolor';
                            else if (row.entity.documentAlertStatus == 'R')
                                return 'ui-redcolor';
                        },
                    },
                    {
                        name: app.localize('Status'),
                        field: 'status',
                        cellClass: function (grid, row) {
                            if (row.entity.documentAlertStatus == 'Y')
                                return 'ui-browncolor';
                            else if (row.entity.documentAlertStatus == 'R')
                                return 'ui-redcolor';
                            else
                                return 'ui-greencolor';
                        },
                    },
                    {
                        name: app.localize('Days'),
                        field: 'noOfDays',
                        width: 60,
                        cellClass: function (grid, row) {
                            if (row.entity.documentAlertStatus == 'Y')
                                return 'ui-browncolor';
                            else if (row.entity.documentAlertStatus == 'R')
                                return 'ui-redcolor';
                            else
                                return 'ui-greencolor';
                        },
                    },
                    {
                        name: app.localize('AlertFrom'),
                        field: 'alertStartingDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                        cellClass: function (grid, row) {
                            if (row.entity.documentAlertStatus == 'Y')
                                return 'ui-browncolor';
                            else if (row.entity.documentAlertStatus == 'R')
                                return 'ui-redcolor';
                        },
                    },
                     {
                         name: app.localize('FileName'),
                         field: 'fileName',
                         minWidth : 250,
                         cellClass: function (grid, row) {
                             if (row.entity.documentAlertStatus == 'Y')
                                 return 'ui-browncolor';
                             else if (row.entity.documentAlertStatus == 'R')
                                 return 'ui-redcolor';
                         },
                     },
                   //{
                   //     name: app.localize('View'),
                   //     enableSorting: false,
                   //     width: 60,
                   //     cellTemplate:
                   //         '<div class=\"ui-grid-cell-contents text-center\">' +
                   //             ' <a> <button class="btn btn-default btn-xs" ng-click="grid.appScope.downloadDocument(row.entity)"><i class="fa fa-eye"></i></button><a>' +
                   //             '</div>'
                   // },
                    //{
                    //    name: app.localize('Email'),
                    //    enableSorting: false,
                    //    width: 60,
                    //    cellTemplate:
                    //        '<div class=\"ui-grid-cell-contents text-center\">' +
                    //        ' <a> <button class="btn btn-default btn-xs"  ng-click="grid.appScope.sendEmail(row.entity)"><i class="fa fa-envelope"></i></button><a>' +
                    //        '</div>'
                    
                    //},
                    {
						name: app.localize('Download'),
						enableSorting: false,
						width: 60,
						cellTemplate:
						'<div class=\"ui-grid-cell-contents text-center\">' +
						' <a> <button class="btn btn-default btn-xs" ng-click="grid.appScope.directDownload(row.entity)"><i class="fa fa-download"></i></button><a>' +
						'</div>'
					},
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };


            vm.clear = function () {
                vm.filterText = null;
                vm.documentInfoRefId = null;
                vm.dateFilterApplied = false;
            };

            vm.refdocumentinfo = [];

            function fillDropDownDocumentInfo() {
                employeedocumentinfoService.getDocumentInfo({}).success(function (result) {
                    vm.refdocumentinfo = result;
                });
            }
            fillDropDownDocumentInfo();

            vm.filterFlag = false;
           
            vm.getAll = function () {
                vm.loading = true;
                var startDate = null;
                var endDate = null;

                if (vm.dateFilterApplied) {
                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }

                employeedocumentinfoService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    employeeRefId: vm.currentEmployeeRefId,
                    documentInfoRefId: vm.documentInfoRefId,
                    startDate: startDate,
                    endDate: endDate,
                    alertPendingFlag: vm.showPendingFlag,
                    alertExpiredFlag: vm.showExpiredFlag,
                    activeEmployeeOnly : vm.activeEmployeeOnly
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editEmployeeDocumentInfo = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createEmployeeDocumentInfo = function () {
                openCreateOrEditModal(null);
            };

            vm.deleteEmployeeDocumentInfo = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteEmployeeDocumentInfoWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            employeedocumentinfoService.deleteEmployeeDocumentInfo({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                if (vm.loginStatus == 'EMP') {
                    var modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/hr/master/employeedocumentinfo/empDocumentInfo.cshtml',
                        controller: 'tenant.views.hr.master.employeedocumentinfo.empDocumentInfo as vm',
                        backdrop: 'static',
                        keyboard: false,
                        resolve: {
                            employeedocumentinfoId: function () {
                                return objId;
                            }
                        }
                    });
                }
                else {
                    var modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/hr/master/employeedocumentinfo/employeeDocumentInfo.cshtml',
                        controller: 'tenant.views.hr.master.employeedocumentinfo.employeeDocumentInfo as vm',
                        backdrop: 'static',
                        keyboard: false,
                        resolve: {
                            employeedocumentinfoId: function () {
                                return objId;
                            }

                        }
                    });
                }

                

                modalInstance.result.then(function (result) {
                    vm.getAll();
                }).finally(function (result) {
                    vm.getAll();
                });
            }

            function fileDownload(employeerow) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/employeedocumentinfo/fileDownload.cshtml',
                    controller: 'tenant.views.hr.master.employeedocumentinfo.fileDownload as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        employeedocumentinfoId: function () {
                            return employeerow.id;
                        }

                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                }).finally(function (result) {
                    vm.getAll();
                });

                //// Direct Download Button in Index

                //var savePerson = null;

                ////Create a new person
                //var newPerson = {
                //    employeeRefId: employeerow.employeeRefId,
                //    documentInfoRefId: employeerow.documentInfoRefId
                //};

                //abp.ajax({
                //    url: '/PersonalInformation/DownloadPersonalDocumentTemplate',
                //    data: JSON.stringify(newPerson)
                //}).done(function (result) {
                //    $(result).insertBefore(savePerson);
                //});

                //var savePerson = function (person) {
                //    return abp.ajax({
                //        url: '/PersonalInformation/DownloadPersonalDocumentTemplate',
                //        data: JSON.stringify(person)
                //        });
                //};

                //Create a new person
                //var newPerson = {
                //    employeeRefId: employeerow.employeeRefId,
                //    documentInfoRefId: employeerow.documentInfoRefId
                //};

                //savePerson(newPerson).success(function (data) {
                //    abp.notify.success('created new person with id = ' + data.personId);
                //    abp.notify.success(app.localize('Success'));
                //}).finally(function (result) {
                //    vm.getAll();
                //});
            }


            vm.exportToExcel = function () {
                vm.loading = true;
                var startDate = null;
                var endDate = null;

                if (vm.dateFilterApplied) {
                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }

                employeedocumentinfoService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    employeeRefId: vm.currentEmployeeRefId,
                    documentInfoRefId: vm.documentInfoRefId,
                    startDate: startDate,
                    endDate: endDate,
                    alertPendingFlag: vm.showPendingFlag,
                    alertExpiredFlag: vm.showExpiredFlag,
                    activeEmployeeOnly: vm.activeEmployeeOnly
                }).success(function (result) {
                    app.downloadTempFile(result);
                }).finally(function () {
                    vm.loading = false;
                    });

             
            };

            vm.import = function () {
                importModal();
						};

			vm.directDownload = function (item) {
				vm.importpath = abp.appPath + 'PersonalInformation/DownloadPersonalDocumentTemplate?employeeRefId=' + item.employeeRefId + "&documentInfoRefId=" + item.documentInfoRefId;
				window.open(vm.importpath);
            }

            vm.sendEmail = function (item) {
                vm.loading = true;
                employeedocumentinfoService.sendEmailDocumentInfo({
                    id: item.id
                }).success(function (result) {
                    if (result.successFlag) {
                        abp.notify.info(app.localize('EmailSentToUser', item.employeeRefName));
                    }
                    else {
                        abp.notify.warn(result.errorMessage);
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            function importModal() {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/employeedocumentInfo/empdocimportModal.cshtml',
                    controller: 'tenant.views.hr.master.employeeDocumentInfo.empdocimportModal as vm',
                    backdrop: 'static'
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                    vm.loading = false;
                });
            }

            //if (vm.clientflag == false) {
            //     //{
            //        //    name: app.localize('Employee'),
            //        //    field: 'employeeRefName',
            //        //    minWidth: 230,
            //        //    cellClass: function (grid, row) {
            //        //        if (row.entity.documentAlertStatus == 'Y')
            //        //            return 'ui-browncolor';
            //        //        else if (row.entity.documentAlertStatus == 'R')
            //        //            return 'ui-redcolor';
            //        //    },
            //        //},

            //    vm.columnprice =
            //        {
            //            name: app.localize('Employee'),
            //            field: 'employeeRefName',
            //            minWidth: 230,
            //            cellClass: function (grid, row) {
            //                if (row.entity.documentAlertStatus == 'Y')
            //                    return 'ui-browncolor';
            //                else if (row.entity.documentAlertStatus == 'R')
            //                    return 'ui-redcolor';
            //            },
            //        }

            //    //vm.userGridOptions.columnDefs.splice(1, 0, vm.columnprice);
            //    vm.userGridOptions.columnDefs.splice(0, 1, vm.columnprice);
            //}

            vm.getAll();
        }]);
})();

