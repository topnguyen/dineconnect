﻿
(function () {
    appModule.controller('tenant.views.hr.master.employeedocumentinfo.documentAttach', [
        '$scope', 'appSession', '$uibModalInstance', 'FileUploader', 'abp.services.app.employeeDocumentInfo', 'employeedocumentinfoId',
        function ($scope, appSession, $uibModalInstance, fileUploader, employeedocumentinfoService, employeedocumentinfoId) {

            var vm = this;
            vm.employeedocumentinfo = null;

            vm.uploader = new fileUploader({
                url: abp.appPath + 'PersonalInformation/ChangeDocumentInfo',
                formData: [{ id: employeedocumentinfoId }],
                    queueLimit: 1,
                    filters: [{
                        name: 'imageFilter',
                        fn: function (item, options) {
                            //File type check
                            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                            if ('|jpg|jpeg|pdf|'.indexOf(type) === -1) {
                                abp.message.warn(app.localize('EmployeeDocument_Warn_FileType'));
                                return false;
                            }
                          

                            //File size check
                            if (item.size > 3072000) //3000KB
                            {
                                abp.message.warn(app.localize('EmployeeDocument_Warn_SizeLimit'));
                                return false;
                            }

                        return true;
                    }
                    }]
            });

            vm.testdata = "Raja";
            vm.empid = 3;

            vm.save = function () {
                vm.testdata = "NSS";
                vm.empid = 2;
                if (vm.uploader.queue.length == 0) {
                    abp.notify.warn(app.localize('YouAreNotSelectAFile'));
                    return;
                }
                vm.uploader.uploadAll();
            };

            vm.uploader.onBeforeUploadItem = function (fileitem) {
                fileitem.formData.push({ data: vm.testdata });
                fileitem.formData.push({ empid: vm.empid });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.uploader.onErrorItem = function (fileItem, response, status, headers) {
                if (response.success) {
                    $uibModalInstance.close(fileItem);
                } else {
                    abp.message.error(response);
                    abp.notify.error(response);
                    vm.loading = false;
                    vm.saving = false;
                    return;
                }
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {

                if (response.success) {
                        $uibModalInstance.close(fileItem);
                } else {
                    abp.message.error(response.error.message);
                    $uibModalInstance.close();
                    return;
                }
            };
        }
    ]);
})();