﻿
(function () {
    appModule.controller('tenant.views.hr.master.employeedocumentinfo.mobileindex', [
        '$scope', '$uibModal', '$stateParams', 'uiGridConstants', 'abp.services.app.employeeDocumentInfo', '$http', 'appSession',
        function ($scope, $modal, $stateParams, uiGridConstants, employeedocumentinfoService, $http, appSession) {
            var vm = this;
            vm.changeFlag = false;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.loginStatus = $stateParams.loginStatus;
            vm.currentEmployeeRefId = null;

            if (vm.loginStatus == 'EMP') {
                if (appSession.useremployeerefid == null || appSession.useremployeerefid == 0) {
                    abp.notify.warn(app.localize('EmployeeCodeNotAssignedToThisUser', appSession.user.userName));
                    abp.message.warn(app.localize('EmployeeCodeNotAssignedToThisUser', appSession.user.userName));
                    vm.errorFlag = true;
                }
                vm.currentEmployeeRefId = appSession.useremployeerefid;
                vm.permissions = {
                    create: abp.auth.hasPermission('Pages.Tenant.Hr.Employee.EmployeeDocumentInfo.Create'),
                    edit: abp.auth.hasPermission('Pages.Tenant.Hr.Employee.EmployeeDocumentInfo.Edit'),
                    'delete': abp.auth.hasPermission('Pages.Tenant.Hr.Employee.EmployeeDocumentInfo.Delete')
                };
            }
            else {
                vm.permissions = {
                    create: abp.auth.hasPermission('Pages.Tenant.Hr.Master.EmployeeDocumentInfo.Create'),
                    edit: abp.auth.hasPermission('Pages.Tenant.Hr.Master.EmployeeDocumentInfo.Edit'),
                    'delete': abp.auth.hasPermission('Pages.Tenant.Hr.Master.EmployeeDocumentInfo.Delete')
                };
            }

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.downloadDocument = function (employeerow) {
                fileDownload(employeerow);

               // fileDownload(employeerow.id);
            }


            vm.userGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
					{
						name: app.localize('Actions'),
						enableSorting: false,
						width: 60,

						cellTemplate:
                             '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.editEmployeeDocumentInfo(row.entity)"><i class="fa fa-edit"></i></button>' +
                                '</div>'
					},
                    
                    {
                        name: app.localize('Employee'),
                        field: 'employeeRefName',
                        minWidth : 230
                    },
                    {
                        name: app.localize('DocumentName'),
                        field: 'documentInfoRefName'
                    },
                    {
                        name: app.localize('IsLifeTime'),
                        field: 'lifeTimeFlag'
                    },
                    {
                        name: app.localize('ValidityFrom'),
                        field: 'startDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                    },
                    {
                        name: app.localize('ValidityTo'),
                        field: 'endDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                    },
                    {
                        name: app.localize('AlertFrom'),
                        field: 'alertStartingDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD\'',
                        cellClass: function (grid, row) {
                            //if (row.entity.alertStartingDate <= new Date())
                            //if (row.entity.id > 0)
                            if (row.entity.documentAlertStatus == 'Y')
                                return 'ui-yellow ui-ralign';
                            else if (row.entity.documentAlertStatus == 'R')
                                return 'ui-red ui-ralign';
                            else
                                return 'ui-ralign';
                        },
                    },
                     {
                         name: app.localize('FileName'),
                         field: 'fileName'
                     },
                    {
                        name: app.localize('View'),
                        enableSorting: false,
                        width: 60,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                                ' <a> <button class="btn btn-default btn-xs" ng-click="grid.appScope.downloadDocument(row.entity)"><i class="fa fa-download"></i></button><a>' +
                                '</div>'
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

         

            vm.getAll = function () {
                vm.loading = true;
                employeedocumentinfoService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    employeeRefId: vm.currentEmployeeRefId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                    
                });
            };

            vm.editEmployeeDocumentInfo = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createEmployeeDocumentInfo = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteEmployeeDocumentInfo = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteEmployeeDocumentInfoWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            employeedocumentinfoService.deleteEmployeeDocumentInfo({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                if (vm.loginStatus == 'EMP') {
                    var modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/hr/master/employeedocumentinfo/empDocumentInfo.cshtml',
                        controller: 'tenant.views.hr.master.employeedocumentinfo.empDocumentInfo as vm',
                        backdrop: 'static',
                        keyboard: false,
                        resolve: {
                            employeedocumentinfoId: function () {
                                return objId;
                            }

                        }
                    });
                }
                else {
                    var modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/hr/master/employeedocumentinfo/employeeDocumentInfo.cshtml',
                        controller: 'tenant.views.hr.master.employeedocumentinfo.employeeDocumentInfo as vm',
                        backdrop: 'static',
                        keyboard: false,
                        resolve: {
                            employeedocumentinfoId: function () {
                                return objId;
                            }

                        }
                    });
                }

                

                modalInstance.result.then(function (result) {
                    vm.getAll();
                }).finally(function (result) {
                    vm.getAll();
                });
            }

            function fileDownload(employeerow) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/employeedocumentinfo/fileDownload.cshtml',
                    controller: 'tenant.views.hr.master.employeedocumentinfo.fileDownload as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        employeedocumentinfoId: function () {
                            return employeerow.id;
                        }

                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                }).finally(function (result) {
                    vm.getAll();
                });

                //// Direct Download Button in Index

                //var savePerson = null;

                ////Create a new person
                //var newPerson = {
                //    employeeRefId: employeerow.employeeRefId,
                //    documentInfoRefId: employeerow.documentInfoRefId
                //};

                //abp.ajax({
                //    url: '/PersonalInformation/DownloadPersonalDocumentTemplate',
                //    data: JSON.stringify(newPerson)
                //}).done(function (result) {
                //    $(result).insertBefore(savePerson);
                //});

                //var savePerson = function (person) {
                //    return abp.ajax({
                //        url: '/PersonalInformation/DownloadPersonalDocumentTemplate',
                //        data: JSON.stringify(person)
                //        });
                //};

                //Create a new person
                //var newPerson = {
                //    employeeRefId: employeerow.employeeRefId,
                //    documentInfoRefId: employeerow.documentInfoRefId
                //};

                //savePerson(newPerson).success(function (data) {
                //    abp.notify.success('created new person with id = ' + data.personId);
                //    abp.notify.success(app.localize('Success'));
                //}).finally(function (result) {
                //    vm.getAll();
                //});

            }


            vm.exportToExcel = function () {
                employeedocumentinfoService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.import = function () {
                importModal();
            };

            function importModal() {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/employeedocumentInfo/empdocimportModal.cshtml',
                    controller: 'tenant.views.hr.master.employeeDocumentInfo.empdocimportModal as vm',
                    backdrop: 'static'
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                    vm.loading = false;
                }); 
            }

            vm.getAll();
        }]);
})();

