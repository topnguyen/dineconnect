﻿(function () {
    appModule.controller('tenant.views.hr.master.leaverequest.leaveRequestPrint', [
          '$scope', '$stateParams', 'abp.services.app.leaveRequest',
        function ($scope, $stateParams,  leaverequestService)
       {

            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.currentUserId = abp.session.userId;

            vm.leaverequest = null;
            
            vm.leaverequestId = $stateParams.printid;

            vm.getData = function () {
                leaverequestService.getLeaveRequestForPrint({
                    Id: vm.leaverequestId
                }).success(function (result) {
                    vm.leaverequest= result.leaveRequest;
                });
            }
            vm.getData();


            //vm.printDiv = function () {

            //    var divToPrint = document.getElementById('DivIdToPrint');

            //    var newWin = window.open('', 'Print-Window');

            //    newWin.document.open();

            //    newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');

            //    newWin.document.close();

            //    setTimeout(function () { newWin.close(); }, 10);

            //}

            //vm.print = function () {
            //    $("#print").click(function () {
            //        var contents = $("#DivIdToPrint").html();
            //        var frame1 = $('<iframe />');
            //        frame1[0].name = "frame1";
            //        frame1.css({ "position": "absolute", "top": "-1000000px" });
            //        $("body").append(frame1);
            //        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
            //        frameDoc.document.open();
            //        //Create a new HTML document.
            //        frameDoc.document.write('<html><head><title>DIV Contents</title>');
            //        frameDoc.document.write('</head><body>');
            //        //Append the external CSS file.
            //        frameDoc.document.write('<link href="style.css" rel="stylesheet" type="text/css" />');
            //        //Append the DIV contents.
            //        frameDoc.document.write(contents);
            //        frameDoc.document.write('</body></html>');
            //        frameDoc.document.close();
            //        setTimeout(function () {
            //            window.frames["frame1"].focus();
            //            window.frames["frame1"].print();
            //            frame1.remove();
            //        }, 500);
            //    });
            //}

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

        }]);
})();

