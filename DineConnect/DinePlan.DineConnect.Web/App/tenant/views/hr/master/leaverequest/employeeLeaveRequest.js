﻿(function () {
    appModule.controller('tenant.views.hr.master.leaverequest.employeeLeaveRequest', [
        '$scope', '$state', '$stateParams', 'abp.services.app.leaveRequest', 'abp.services.app.leaveType', 'abp.services.app.personalInformation', 'appSession',
        function ($scope, $state, $stateParams, leaverequestService, leavetypeService, personalinformationService, appSession) {
            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            vm.leaverequest = null;

            vm.forenoon = null;
            vm.afternoon = null;

            $scope.existall = true;
            vm.leaverequestId = $stateParams.id;

            vm.loginStatus = $stateParams.loginStatus;
            vm.currentEmployeeRefId = null;

            if (vm.loginStatus == 'EMP') {
                if (appSession.useremployeerefid == null || appSession.useremployeerefid == 0) {
                    abp.notify.warn(app.localize('EmployeeCodeNotAssignedToThisUser', appSession.user.userName));
                    abp.message.warn(app.localize('EmployeeCodeNotAssignedToThisUser', appSession.user.userName));
                    vm.errorFlag = true;
                }
                vm.currentEmployeeRefId = appSession.useremployeerefid;
            }

            vm.numberofdays = 0;
            vm.leaveRequestPortions = [];

            //date start
            //  Date Range Options
            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            //  Date Angular Js 
            $scope.today = function () {
                $scope.dt = moment(new Date()).format("DD-MMM-YYYY");

            };
            $scope.today();

            $scope.clear = function () {
                $scope.dt = null;
            };

            $scope.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            $scope.dateOptions = {
                dateDisabled: disabled,
                formatYear: 'yy',
                maxDate: new Date(2020, 5, 22),
                minDate: new Date(),
                startingDay: 1
            };

            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                    mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }

            $scope.toggleMin = function () {
                $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
                $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
            };

            //$scope.toggleMin();

            $scope.open1 = function () {
                $scope.popup1.opened = true;
            };

            $scope.open2 = function () {
                $scope.popup2.opened = true;
            };

            $scope.open3 = function () {
                $scope.popup3.opened = true;
            };

            $scope.setDate = function (year, month, day) {
                $scope.dt = new Date(year, month, day);
            };

            $scope.formats = ['yyyy-MM-dd'];
            $scope.format = $scope.formats[0];
            //$scope.altInputFormats = ['M!/d!/yyyy'];
            $scope.altInputFormats = ['dd-MMMM-yyyy'];

            $('input[name="stDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: $scope.maxDate
                //minDate:'2017-05-10', //$scope.minDate,
                //maxDate: $scope.maxDate
            });

            $('input[name="endDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: $scope.maxDate
            });


            $scope.popup1 = {
                opened: false
            };

            $scope.popup2 = {
                opened: false
            };

            $scope.popup3 = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            $scope.events = [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];

            function getDayClass(data) {
                var date = data.date,
                    mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            }

            //date end



            vm.save = function () {
                if ($scope.existall == false)
                    return;

                if (vm.leaverequest.totalNumberOfDays <= 0) {
                    vm.saving = false;
                    return;
                }
                if (vm.leaverequest.halfDayFlag == true) {
                    if (document.getElementById('forenoon').checked == true)
                        vm.leaverequest.halfDayTimeSpecification = "FH";
                    else
                        vm.leaverequest.halfDayTimeSpecification = "SH";
                }

                vm.leaverequest.dateOfApply = moment(vm.leaverequest.dateOfApply).format('YYYY-MM-DD');
                vm.leaverequest.leaveFrom = moment(vm.leaverequest.leaveFrom).format('YYYY-MM-DD');
                vm.leaverequest.leaveTo = moment(vm.leaverequest.leaveTo).format('YYYY-MM-DD');

                if (CompareDate() == false) {
                    vm.saving = false;
                    return;
                }

                vm.saving = true;
                vm.leaverequest.leaveStatus = "Pending";

                leaverequestService.createOrUpdateLeaveRequest({
                    leaveRequest: vm.leaverequest
                }).success(function () {

                    abp.notify.info('\' LeaveRequest \'' + app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };


            function CompareDate() {
                var startDate = $('#stDate').val();
                var endDate = $('#endDate').val();

                if (vm.leaverequest.halfDayFlag == true) {
                    endDate = $("#stDate").val();
                    vm.leaverequest.leaveTo = vm.leaverequest.leaveFrom;
                    return true;
                }

                if (startDate > endDate) {
                    abp.notify.warn(app.localize('ReqDateErr'));
                    abp.message.info(app.localize('ReqDateErr'));

                    //alert("End date can be greater than Start date");
                    return false;
                }
            }

            //$scope.validateDate = function () {
            //    // get dates from input fields
            //    var startDate = $("#stDate").val();
            //    var endDate = $("#endDate").val();

            //    if (vm.leaverequest.halfDayFlag == true) {
            //        endDate = $("#stDate").val();
            //        vm.leaverequest.leaveTo = vm.leaverequest.leaveFrom;
            //        vm.leaverequest.totalNumberOfDays = 0.5;
            //        vm.getExactLeaveDays();
            //    }

            //    var sdate = startDate.split("-");
            //    var edate = endDate.split("-");

            //    if (moment(startDate) > moment(endDate)) {
            //        abp.notify.warn(app.localize('SelectCorrectDate'));
            //        return;
            //    }

            //    var future = moment(startDate);
            //    var start = moment(endDate);
            //    var d = start.diff(future, 'days') + 1;

            //    if (d <= 0)
            //    {
            //        abp.notify.warn(app.localize('SelectCorrectDate'));
            //        return;
            //    }



            //    if (sdate[1] == edate[1]) {
            //        if (vm.leaverequest.halfDayFlag == true) {
            //            diffd = .5;
            //        }
            //    }
            //    if (vm.leaverequest.leaveTo != null) {
            //        if (vm.leaverequest.halfDayFlag == true) {
            //            vm.leaverequest.totalNumberOfDays = diffd;
            //            vm.getLtNumberOfDays();
            //        }
            //        else {
            //            vm.leaverequest.totalNumberOfDays = d;
            //            vm.getLtNumberOfDays();
            //        }
            //    }
            //}

            $scope.validateDate = function () {
                // get dates from input fields
                var startDate = $("#stDate").val();
                var endDate = $("#endDate").val();

                if (startDate == null || startDate == '')
                    return;

                if (endDate == null || endDate == '')
                    endDate = startDate;

                if (vm.leaverequest.halfDayFlag == true) {
                    endDate = $("#stDate").val();
                    vm.leaverequest.leaveTo = vm.leaverequest.leaveFrom;
                    vm.leaverequest.totalNumberOfDays = 0.5;
                    vm.getExactLeaveDays();
                    return;
                }

                var sdate = startDate.split("-");
                var edate = endDate.split("-");

                var future = moment(startDate);
                var start = moment(endDate);
                var d = start.diff(future, 'days') + 1;

                if (moment(startDate) > moment(endDate)) {
                    abp.notify.warn(app.localize('SelectCorrectDate'));
                    return;
                }

                if (d <= 0) {
                    abp.notify.warn(app.localize('SelectCorrectDate'));
                    return;
                }

                if (sdate[1] == edate[1]) {
                    if (vm.leaverequest.halfDayFlag == true) {
                        diffd = .5;
                    }
                }

                if (vm.leaverequest.leaveTo != null) {
                    if (vm.leaverequest.halfDayFlag == true) {
                        vm.leaverequest.totalNumberOfDays = diffd;

                    }
                    else {
                        vm.leaverequest.totalNumberOfDays = d;
                    }
                    vm.getExactLeaveDays();
                }

            };

            vm.existall = function () {
                if (vm.leaverequest.id == null) {
                    vm.existall = false;
                    return;
                }

                leaverequestService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.leaverequest.id,
                    operation: 'SEARCH'
                }).success(function (result) {
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.leaverequest.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.leaverequest.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                if (vm.loginStatus == 'EMP')
                    $state.go("tenant.leaverequest") //employeeleaverequest
                else
                    $state.go("tenant.leaverequest")
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.displaydetails = function () {
                leaverequestService.getAll({
                    Id: vm.leaverequestId
                }).success(function (result) {
                    vm.leaveRequest = result.items;
                    // vm.Leaaverequest.length = result.totalCount;
                    resetTableValues();
                    fillDropDownEmployeeHandOver();
                    vm.sno = 0;
                    if (result.totalCount > 0) {
                        angular.forEach(vm.leaveRequest, function (value, key) {
                            if (vm.leaverequest.employeeRefId == result.items[key].employeeRefId) {
                                vm.sno = vm.sno + 1;
                                vm.leaveRequestPortions.push({
                                    'sno': vm.sno,
                                    'leaveTypeRefName': value.leaveTypeRefName,
                                    'dateOfApply': moment(value.dateOfApply).format("YYYY-MM-DD"),
                                    'leaveFrom': moment(value.leaveFrom).format("YYYY-MM-DD"),
                                    'leaveTo': moment(value.leaveTo).format("YYYY-MM-DD"),
                                    'totalNumberOfDays': value.totalNumberOfDays,
                                })
                                vm.numberofdays = vm.numberofdays + result.items[key].totalNumberOfDays;
                            }
                        });
                    }
                }).finally(function () {
                    vm.loading = false;
                });

            }

            vm.displayleavetypedetails = function () {
                leaverequestService.getAll({
                    Id: vm.leaverequestId
                }).success(function (result) {
                    vm.leaveRequest = result.items;
                    // vm.Leaaverequest.length = result.totalCount;
                    resetTableValues();
                    vm.sno = 0;
                    if (result.totalCount > 0) {
                        angular.forEach(vm.leaveRequest, function (value, key) {
                            if (vm.leaverequest.employeeRefId == result.items[key].employeeRefId) {
                                if (vm.leaverequest.leaveTypeRefCode == result.items[key].leaveTypeRefCode) {
                                    vm.numberofdays = vm.numberofdays + result.items[key].totalNumberOfDays;
                                    vm.sno = vm.sno + 1;
                                    vm.leaveRequestPortions.push({
                                        'sno': vm.sno,
                                        'leaveTypeRefName': value.leaveTypeRefName,
                                        'dateOfApply': moment(value.dateOfApply).format("YYYY-MM-DD"),
                                        'leaveFrom': moment(value.leaveFrom).format("YYYY-MM-DD"),
                                        'leaveTo': moment(value.leaveTo).format("YYYY-MM-DD"),
                                        'totalNumberOfDays': value.totalNumberOfDays,
                                    })
                                }
                            }
                            key = key + 1;
                        });
                    }
                }).finally(function () {
                    vm.loading = false;
                });

            }

            vm.ltnumberofdays = [];
            //vm.getLtNumberOfDays = function () {
            //    leavetypeService.getAll({
            //        Id: vm.leaverequest.leaveTypeRefCode
            //    }).success(function (leavetyperesult) {
            //        vm.ltnumberofdays = leavetyperesult.items;
            //        if (leavetyperesult.totalCount > 0) {
            //            angular.forEach(vm.ltnumberofdays, function (value, key) {
            //                if (value.id == parseInt(vm.leaverequest.leaveTypeRefCode)) {
            //                    if ((vm.numberofdays+vm.leaverequest.totalNumberOfDays) >= value.numberOfLeaveAllowed)
            //                        abp.notify.warn(app.localize('ExceedingErr'));
            //                             abp.message.info(app.localize('ExceedingErr'));
            //                }
            //            });
            //        }
            //    }).finally(function () {

            //    });
            //        }

            vm.getLtNumberOfDays = function () {
                vm.acYear = 0;
                if (vm.leaverequest.leaveFrom == null || vm.leaverequest.leaveFrom == '') {
                    vm.acYear = parseInt(moment().format('YYYY'));
                }
                else {
                    vm.acYear = parseInt(moment(vm.leaverequest.leaveFrom).format('YYYY'));
                }

                if (vm.acYear == 0) {
                    return;
                }

                vm.loading = true;

                yearwiseleaveallowedforemployeeService.getEmployeeWiseAcYearWiseLeave({
                    employeeRefId: vm.leaverequest.employeeRefId,
                    acYear: vm.acYear
                }).success(function (leavetyperesult) {
                    vm.ltnumberofdays = leavetyperesult;
                    if (leavetyperesult.length > 0) {
                        angular.forEach(vm.ltnumberofdays, function (value, key) {
                            if (value.leaveTypeRefCode == parseInt(vm.leaverequest.leaveTypeRefCode)) {
                                if ((vm.numberofdays + vm.leaverequest.totalNumberOfDays) >= value.numberOfLeaveAllowed) {
                                    abp.notify.error(app.localize('ExceedingErr'));
                                    abp.notify.error(app.localize('Allowed') + ' - ' + value.leaveTypeRefName + ': ' + value.numberOfLeaveAllowed);
                                    abp.notify.error(app.localize('AlreadyTaken') + ' - ' + value.leaveTypeRefName + ': ' + vm.numberofdays);
                                    abp.notify.error(app.localize('Pending') + ' - ' + value.leaveTypeRefName + ': ' + (value.numberOfLeaveAllowed - vm.numberofdays));
                                    abp.message.info(app.localize('ExceedingErr'));
                                    vm.exceedError = true;
                                    var errorMessaage = app.localize('Allowed') + ' - ' + value.leaveTypeRefName + ': ' + value.numberOfLeaveAllowed;
                                    errorMessaage = errorMessaage + ' ' + app.localize('AlreadyTaken') + ': ' + vm.numberofdays;
                                    errorMessaage = errorMessaage + ' ' + app.localize('Pending') + ': ' + (value.numberOfLeaveAllowed - vm.numberofdays);
                                    abp.message.error(errorMessaage);
                                }
                            }
                        });
                    }
                    else {
                        abp.notify.error(app.localize('ExceedingErr'));
                        abp.notify.warn(app.localize('KindlyContactHr'));
                        vm.exceedError = true;
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            function resetTableValues() {
                vm.leaveRequestPortions = [];
                vm.leaveRequestPortions.leaveTypeRefName = '';
                vm.leaveRequestPortions.dateOfApply = '';
                vm.leaveRequestPortions.leaveFrom = '';
                vm.leaveRequestPortions.leaveTo = '';
                vm.leaveRequestPortions.totalNumberOfDays = '';
                vm.sno = 0;
                vm.numberofdays = 0;
            }

            vm.refemployeecode = [];

            function fillDropDownEmployee() {
                personalinformationService.getActiveEmployeeForCombobox({}).success(function (result) {
                    vm.refemployeecode = result.items;

                    if (vm.currentEmployeeRefId != null && vm.currentEmployeeRefId > 0) {
                        vm.tempemp = [];
                        angular.forEach(vm.refemployeecode, function (value, key) {
                            if (parseInt(value.value) == vm.currentEmployeeRefId) {
                                vm.tempemp.push(value);
                            }
                        });

                        vm.refemployeecode = vm.tempemp;

                        if (vm.refemployeecode.length == 1)
                            vm.leaverequest.employeeRefId = vm.refemployeecode[0].value;
                    }
                });
            }

            vm.refhandoveremployeecode = [];

            function fillDropDownEmployeeHandOver() {
                vm.refhandoveremployeecode = [];
                personalinformationService.getEmployeeForCombobox({}).success(function (result) {
                    vm.refempcode = result.items;
                    angular.forEach(vm.refempcode, function (value, key) {
                        if (value.value != vm.leaverequest.employeeRefId) {
                            vm.refhandoveremployeecode.push(value);
                        }
                    });
                });
            }

            vm.refleavetype = [];

            function fillDropDownLeaveType() {
                leaverequestService.getLeaveTypeForCombobox({}).success(function (result) {
                    vm.refleavetype = result.items;
                });
            }

            function init() {
                abp.ui.setBusy('#leaverequestForm');
                fillDropDownEmployee();
                fillDropDownLeaveType();
                fillDropDownEmployeeHandOver();
                leaverequestService.getLeaveRequestForEdit({
                    Id: vm.leaverequestId
                }).success(function (result) {
                    vm.leaverequest = result.leaveRequest;


                    if (vm.leaverequest.id == null) {
                        vm.leaverequest.employeeRefId = vm.currentEmployeeRefId;
                        vm.displaydetails();
                        vm.displayleavetypedetails();
                        vm.leaverequest.totalNumberOfDays = "";
                        $scope.dt = moment(new Date()).format("YYYY-MM-DD");
                        vm.leaverequest.leaveFrom = null;
                        vm.leaverequest.leaveTo = null;
                        vm.leaverequest.dateOfApply = $scope.dt;
                        $scope.minDate = moment().add(-30, 'days');
                        $scope.maxDate = moment().add(90, 'days');
                    }

                    else {
                        $scope.leaveFrom = new Date(vm.leaverequest.leaveFrom);
                        vm.leaverequest.leaveFrom = $scope.leaveFrom;

                        $scope.leaveTo = new Date(vm.leaverequest.leaveTo);
                        vm.leaverequest.leaveTo = $scope.leaveTo;

                        $scope.minDate = moment(vm.leaverequest.leaveFrom).add(-30, 'days');
                        $scope.maxDate = moment().add(90, 'days');

                        $scope.dateOfApply = new Date(vm.leaverequest.dateOfApply);
                        vm.leaverequest.dateOfApply = moment($scope.dateOfApply).format('YYYY-MM-DD');


                        if (vm.leaverequest.halfDayFlag == true) {
                            if (vm.leaverequest.halfDayTimeSpecification = "FH")
                                document.getElementById('forenoon').checked == true;
                            else
                                document.getElementById('afternoon').checked == true;
                        }

                        vm.displaydetails();
                        vm.displayleavetypedetails();
                    }


                });
                abp.ui.clearBusy('#leaverequestForm');
            }
            init();

            vm.getExactLeaveDays = function () {
                if (vm.leaverequest.employeeRefId == 0 || vm.leaverequest.employeeRefId == null) {
                    abp.notify.error(app.localize('Employee') + " ?");
                    return;
                }

                var leaveFrom = moment(vm.leaverequest.leaveFrom).format('YYYY-MM-DD');
                if (moment(leaveFrom).isValid() == false) {
                    vm.leaverequest.totalNumberOfDays = null;
                    return;
                }

                var leaveTo = moment(vm.leaverequest.leaveTo).format('YYYY-MM-DD');
                if (moment(leaveTo).isValid() == false) {
                    vm.leaverequest.totalNumberOfDays = null;
                    return;
                }
                vm.loading = true;
                leaverequestService.getNumberOfLeaveDays({
                    leaveFrom: leaveFrom,
                    leaveTo: leaveTo,
                    employeeRefId: vm.leaverequest.employeeRefId
                }).success(function (result) {
                    vm.leaverequest.totalNumberOfDays = result;
                    if (vm.leaverequest.totalNumberOfDays == 1 && vm.leaverequest.halfDayFlag) {
                        vm.leaverequest.totalNumberOfDays = 0.5;
                    }
                    vm.loading = false;
                    vm.getLtNumberOfDays();
                }).finally(function () {
                    vm.loading = false;
                    vm.displayleavetypedetails();
                });
            }
        }
    ]);
})();

