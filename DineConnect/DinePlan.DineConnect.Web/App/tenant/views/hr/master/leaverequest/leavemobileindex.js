﻿(function () {
    appModule.controller('tenant.views.hr.master.leaverequest.leavemobileindex', [
        '$scope', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.leaveRequest', 'appSession',
        function ($scope, $state, $stateParams, $modal, uiGridConstants, leaverequestService, appSession) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.errorFlag = false;
            vm.currentEmployeeRefId = null;
            vm.loginStatus = $stateParams.loginStatus;

            if (vm.loginStatus == 'EMP') {
                if (appSession.useremployeerefid == null || appSession.useremployeerefid == 0) {
                    abp.notify.warn(app.localize('EmployeeCodeNotAssignedToThisUser', appSession.user.userName));
                    abp.message.warn(app.localize('EmployeeCodeNotAssignedToThisUser', appSession.user.userName));
                    vm.permissions.create = false;
                    vm.permissions.delete = false;
                    vm.permissions.edit = false;
                    vm.errorFlag = true;
                }
                vm.currentEmployeeRefId = appSession.useremployeerefid;

                vm.permissions = {
                    create: abp.auth.hasPermission('Pages.Tenant.Hr.Employee.EmployeeLeaveRequest.Create'),
                    edit: abp.auth.hasPermission('Pages.Tenant.Hr.Employee.EmployeeLeaveRequest.Edit'),
                    'delete': abp.auth.hasPermission('Pages.Tenant.Hr.Employee.EmployeeLeaveRequest.Delete'),
                };
            }
            else {
                vm.permissions = {
                    create: abp.auth.hasPermission('Pages.Tenant.Hr.Master.LeaveRequest.Create'),
                    edit: abp.auth.hasPermission('Pages.Tenant.Hr.Master.LeaveRequest.Edit'),
                    'delete': abp.auth.hasPermission('Pages.Tenant.Hr.Master.LeaveRequest.Delete'),
                };
            }

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    //{
                    //	name: app.localize('Actions'),
                    //	enableSorting: false,
                    //	width: 120,

                    //	cellTemplate:
                    //	   "<div class=\"ui-grid-cell-contents\">" +
                    //		   "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                    //		   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                    //		   "    <ul uib-dropdown-menu>" +
                    //		   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editLeaveRequest(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                    //		   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteLeaveRequest(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                    //           "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.printLeaveRequest(row.entity)\">" + app.localize("Print") + "</a></li>" +

                    //		   "    </ul>" +
                    //		   "  </div>" +
                    //		   "</div>"
                    //},
                    //{
                    //    name: app.localize('EmployeeName'),
                    //    field: 'employeeRefName'
                    //},
                    // {
                    //     name: app.localize('LeaveType'),
                    //     field: 'leaveTypeRefName'
                    // },
                    {
                        name: app.localize('Edit'),
                        width: 50,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.editLeaveRequest(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>' +
                            '</div>'
                    },
                    //{
                    //    name: app.localize('ApplyDate'),
                    //    field: 'dateOfApply',
                    //    cellFilter: 'momentFormat: \'DD-MMM-YYYY\'',
                    //},
                    {
                        name: app.localize('LeaveFrom'),
                        field: 'leaveFrom',
                        cellFilter: 'momentFormat: \'DD-MMM-YYYY\'',
                        cellClass: function (grid, row) {
                            if (row.entity.leaveStatus == app.localize('Pending'))
                                return 'ui-pending';
                            else if (row.entity.leaveStatus == app.localize('Rejected'))
                                return 'ui-rejected';
                            else if (row.entity.leaveStatus == app.localize('Approved'))
                                return 'ui-approved';
                        },
                    },
                    {
                        name: app.localize('LeaveTo'),
                        field: 'leaveTo', cellFilter: 'momentFormat: \'DD-MMM-YYYY\'',
                        cellClass: function (grid, row) {
                            if (row.entity.leaveStatus == app.localize('Pending'))
                                return 'ui-pending';
                            else if (row.entity.leaveStatus == app.localize('Rejected'))
                                return 'ui-rejected';
                            else if (row.entity.leaveStatus == app.localize('Approved'))
                                return 'ui-approved';
                        },
                    },
                    //{
                    //    name: app.localize('TotalNo.Of.Days'),
                    //    field: 'totalNumberOfDays'
                    //},
                    {
                        name: app.localize('LeaveStatus'),
                        field: 'leaveStatus',
                        cellTemplate: '<div class="ui-grid-cell-contents">' +
                            '  <span> {{row.entity.leaveStatus}} </span>' +
                            '  <br ng-if="row.entity.fileName!=null" >' +
                            '  <span ng-if="row.entity.fileName!=null" >' +
                            '     <a href="' + abp.appPath + 'PersonalInformation/DownloadLeaveDocumentInfo?leaveRequestRefId={{row.entity.id}}" >' + app.localize("View") + '&nbsp;' + app.localize("Document") +
                            '     </a>' +
                            ' </span> ' +
                            '</div>',
                        cellClass: function (grid, row) {
                            if (row.entity.leaveStatus == app.localize('Pending'))
                                return 'ui-pending';
                            else if (row.entity.leaveStatus == app.localize('Rejected'))
                                return 'ui-rejected';
                            else if (row.entity.leaveStatus == app.localize('Approved'))
                                return 'ui-approved';
                        },
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.printLeaveRequest = function (myObject) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/leaverequest/leaveRequestPrint.cshtml',
                    controller: 'tenant.views.hr.master.leaverequest.leaveRequestPrint as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        leaverequestId: function () {
                            return myObject.id;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
                //$state.go("tenant.leaverequestprint", {
                //    printid: myObject.id
                //});
            };

            vm.getAll = function () {

                if (vm.errorFlag == true)
                    return;

                vm.loading = true;
                leaverequestService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    employeeRefId: vm.currentEmployeeRefId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editLeaveRequest = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createLeaveRequest = function () {
                openCreateOrEditModal(null);
            };

            vm.deleteLeaveRequest = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteLeaveRequestWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            leaverequestService.deleteLeaveRequest({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                abp.message.warn(app.localize('ThoseDaysConvertedAsIdle'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                if (vm.loginStatus == 'HR' || vm.loginStatus == '') {
                    $state.go("tenant.leaverequestdetail", {
                        id: objId,
                        loginStatus: vm.loginStatus
                    });
                }
                else {
                    $state.go("tenant.leaverequestdetail", {
                        id: objId,
                        loginStatus: vm.loginStatus
                    });
                }
            }

            vm.exportToExcel = function () {
                leaverequestService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getAll();
        }]);
})();

