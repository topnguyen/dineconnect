﻿(function () {
    appModule.controller('tenant.views.hr.master.leaverequest.createOrEditModal', [
        '$scope', '$state', '$stateParams', 'abp.services.app.leaveRequest', 'abp.services.app.leaveType',
        'abp.services.app.personalInformation', 'FileUploader', 'appSession', 'abp.services.app.yearWiseLeaveAllowedForEmployee', 'deviceDetector',
        function ($scope, $state, $stateParams, leaverequestService, leavetypeService, personalinformationService, fileUploader, appSession, yearwiseleaveallowedforemployeeService, deviceDetector) {
            var vm = this;
            /* eslint-disable */

            vm.permissions = {
                approve: abp.auth.hasPermission('Pages.Tenant.Hr.Master.LeaveRequest.Approve'),
            };
            vm.devicedata = deviceDetector;
            vm.mobileFlag = false;

            if (vm.devicedata.device == 'unknown' || vm.devicedata.device == 'chrome-book' || vm.devicedata.device == 'ps4') {
                vm.mobileFlag = false;
                //abp.notify.info('DeskTop');
            }
            else {
                vm.mobileFlag = true;
                //abp.notify.info('Mobile');
            }
            vm.saving = false;
            vm.leaverequest = null;
            vm.changeFlag = false;
            vm.forenoon = null;
            vm.afternoon = null;
            //vm.isDocumentRequired = false;
            vm.leaverequestId = $stateParams.id;
            vm.importpath = "";
            vm.loginStatus = $stateParams.loginStatus;
            vm.currentEmployeeRefId = null;
            vm.uilimit = 20;

            if (vm.loginStatus == 'EMP') {
                if (appSession.useremployeerefid == null || appSession.useremployeerefid == 0) {
                    abp.notify.warn(app.localize('EmployeeCodeNotAssignedToThisUser', appSession.user.userName));
                    abp.message.warn(app.localize('EmployeeCodeNotAssignedToThisUser', appSession.user.userName));
                    vm.errorFlag = true;
                }
                vm.currentEmployeeRefId = appSession.useremployeerefid;
                $scope.minDate = moment().add(-120, 'days');
                $scope.maxDate = moment().add(70, 'days');
            }
            else {
                $scope.minDate = moment().add(-120, 'days');
                $scope.maxDate = moment().add(70, 'days');
            }

            vm.numberofdays = 0;
            vm.leaveRequestPortions = [];
            //File Uploader
            vm.changeVariable = function () {
                vm.changeFlag = true;
            };

            vm.uploader = new fileUploader({
                url: abp.appPath + 'PersonalInformation/ChangeLeaveDocumentInfo',
                formData: [{ id: 1 }],
                queueLimit: 1,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        //File type check
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        if ('|jpg|jpeg|pdf|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('LeaveDocument_Warn_FileType'));
                            return false;
                        }
                        //File size check
                        if (item.size > 5242880) //3000KB
                        {
                            abp.message.warn(app.localize('LeaveDocument_Warn_SizeLimit'));
                            return false;
                        }
                        return true;
                    }
                }]
            });

            vm.uploader.onBeforeUploadItem = function (fileitem) {
                fileitem.formData.push({ leaveRequestRefId: vm.leaverequest.id });
                //fileitem.formData.push({ documentInfoRefId: vm.clientdocumentinfo.documentInfoRefId })
            };

            vm.uploader.onErrorItem = function (fileItem, response, status, headers) {
                if (response.success) {

                } else {
                    abp.message.error(response);
                    abp.notify.error(response);
                    vm.loading = false;
                    vm.saving = false;
                    return;
                }
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                //vm.cancel();
            };

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            //date start
            //  Date Range Options
            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            //  Date Angular Js 
            $scope.today = function () {
                $scope.dt = moment(new Date()).format("DD-MMM-YYYY");

            };
            $scope.today();

            $scope.clear = function () {
                $scope.dt = null;
            };

            $scope.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            $scope.dateOptions = {
                dateDisabled: disabled,
                formatYear: 'yy',
                maxDate: new Date(2020, 5, 22),
                minDate: new Date(),
                startingDay: 1
            };

            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                    mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }

            $scope.toggleMin = function () {
                $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
                $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
            };

            $scope.toggleMin();

            $scope.open1 = function () {
                $scope.popup1.opened = true;
            };

            $scope.open2 = function () {
                $scope.popup2.opened = true;
            };

            $scope.open3 = function () {
                $scope.popup3.opened = true;
            };

            $scope.setDate = function (year, month, day) {
                $scope.dt = new Date(year, month, day);
            };

            $scope.formats = ['yyyy-MM-dd'];
            $scope.format = $scope.formats[0];
            //$scope.altInputFormats = ['M!/d!/yyyy'];
            $scope.altInputFormats = ['dd-MMMM-yyyy'];



            $('input[name="stDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: $scope.maxDate,
            });

            $('input[name="endDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: $scope.maxDate
            });


            $scope.popup1 = {
                opened: false
            };

            $scope.popup2 = {
                opened: false
            };

            $scope.popup3 = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            $scope.events = [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];

            function getDayClass(data) {
                var date = data.date,
                    mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            }

            //date end



            vm.save = function (argOption) {

                //if (vm.currentEmployeeRefId == 0 || vm.currentEmployeeRefId == null) {
                //    abp.notify.error(app.localize('EmployeeCodeNotAssignedToThisUser', appSession.user.userName));
                //    return;
                //}
                //else {
                //    vm.leaverequest.employeeRefId = vm.currentEmployeeRefId;
                //}

                if (vm.leaverequest.totalNumberOfDays <= 0) {
                    vm.saving = false;
                    return;
                }

                if (vm.leaverequest.leaveReason == null || vm.leaverequest.leaveReason == '') {
                    abp.notify.warn(app.localize('Reason') + ' ? ');
                    return;
                }

                if (vm.leaverequest.halfDayFlag == true) {
                    if (document.getElementById('forenoon').checked == true)
                        vm.leaverequest.halfDayTimeSpecification = "FH";
                    else
                        vm.leaverequest.halfDayTimeSpecification = "SH";
                }

                vm.leaverequest.dateOfApply = moment(vm.leaverequest.dateOfApply).format('YYYY-MM-DD');
                vm.leaverequest.leaveFrom = moment(vm.leaverequest.leaveFrom).format('YYYY-MM-DD');
                vm.leaverequest.leaveTo = moment(vm.leaverequest.leaveTo).format('YYYY-MM-DD');

                if (CompareDate() == false) {
                    vm.saving = false;
                    return;
                }

                vm.acYear = parseInt(moment(vm.leaverequest.leaveFrom, 'YYYY-MM-DD').format('YYYY'));
                vm.getLtNumberOfDays();
                if (vm.exceedError) {
                    abp.notify.error(app.localize('ExceedingErr'));
                    vm.saving = false;
                    return;
                }

                vm.saving = true;
                vm.loading = true;
                if (vm.leaverequest.leaveStatus == null)
                    vm.leaverequest.leaveStatus = "Pending";

                vm.leaverequest.leaveReason = vm.leaverequest.leaveReason.toUpperCase();

                leaverequestService.createOrUpdateLeaveRequest({
                    leaveRequest: vm.leaverequest,
                    authorizationId: argOption
                }).success(function (result) {
                    if (argOption == 1) {
                        abp.notify.info(app.localize('LeaveRequest') + ' ' + app.localize('SavedSuccessfully'));

                        vm.leaverequest.id = result.id;
                        leaverequestService.sendMailForLeaveRequest({
                            id: result.id
                        }).success(function (resSentMail) {

                            abp.notify.info(app.localize('LeaveRequest') + ' ' + app.localize('EmailSentToUser', resSentMail.emailRecieverName + '(' + resSentMail.emailRecieverId + ')'));
                        })
                        if (vm.uploader.queue.length == 0 && vm.leaverequest.isDocumentRequired == true) {
                            abp.notify.warn(app.localize('YouAreNotSelectAFile'));
                            return;
                        }

                        //vm.documentinfoid = result.id;
                        vm.uploader.uploadAll();
                        vm.loading = false;
                    }
                    else if (argOption == 2) {
                        abp.notify.info(app.localize('LeaveApproval') + ' ' + app.localize('Done'));

                        leaverequestService.sendMailForApproveLeaveRequest({
                            leaveRequest: vm.leaverequest,
                            authorizationId: argOption
                        }).success(function (resSentMail) {

                            abp.notify.info(app.localize('LeaveApproval') + ' ' + app.localize('EmailSentToUser', resSentMail.emailRecieverName + '(' + resSentMail.emailRecieverId + ')'));
                        })
                    }
                    else if (argOption == 3) {
                        abp.notify.info(app.localize('LeaveRequest') + ' ' + app.localize('Rejected'));

                        leaverequestService.sendMailForRejectLeaveRequest({
                            leaveRequest: vm.leaverequest,
                            authorizationId: argOption
                        }).success(function (resSentMail) {

                            abp.notify.info(app.localize('LeaveRejected') + ' ' + app.localize('EmailSentToUser', resSentMail.emailRecieverName + '(' + resSentMail.emailRecieverId + ')'));
                        })
                    }
                }).finally(function () {
                    vm.loading = false;
                    if (vm.leaverequest.isDocumentRequired == true) {
                        if (vm.uploader.queue.length == 0) {
                            vm.cancel();
                        }
                    }
                    else {
                        vm.cancel();
                    }
                    vm.saving = false;
                });
            };


            function CompareDate() {
                var startDate = $('#stDate').val();
                var endDate = $('#endDate').val();

                if (vm.leaverequest.halfDayFlag == true) {
                    endDate = $("#stDate").val();
                    vm.leaverequest.leaveTo = vm.leaverequest.leaveFrom;
                    return true;
                }

                if (startDate > endDate) {
                    abp.notify.warn(app.localize('ReqDateErr'));
                    abp.message.info(app.localize('ReqDateErr'));
                    return false;
                }
            }

            $scope.validateDate = function () {
                // get dates from input fields
                var startDate = $("#stDate").val();
                var endDate = $("#endDate").val();

                if (startDate == null || startDate == '')
                    return;

                if (endDate == null || endDate == '')
                    endDate = startDate;

                if (vm.leaverequest.halfDayFlag == true) {
                    endDate = $("#stDate").val();
                    vm.leaverequest.leaveTo = vm.leaverequest.leaveFrom;
                    vm.leaverequest.totalNumberOfDays = 0.5;
                    vm.getExactLeaveDays();
                    return;
                }

                var sdate = startDate.split("-");
                var edate = endDate.split("-");

                var future = moment(startDate);
                var start = moment(endDate);
                var d = start.diff(future, 'days') + 1;

                if (moment(startDate) > moment(endDate)) {
                    abp.notify.warn(app.localize('SelectCorrectDate'));
                    return;
                }

                //if (loginStatus == "EMP") {
                //    if (moment(startDate) < moment()) {
                //        abp.notify.warn(app.localize('SelectCorrectDate'));
                //        return;
                //    }
                //}

                if (d <= 0) {
                    abp.notify.warn(app.localize('SelectCorrectDate'));
                    return;
                }

                if (sdate[1] == edate[1]) {
                    if (vm.leaverequest.halfDayFlag == true) {
                        diffd = .5;
                    }
                }

                if (vm.leaverequest.leaveTo != null) {
                    if (vm.leaverequest.halfDayFlag == true) {
                        vm.leaverequest.totalNumberOfDays = diffd;

                    }
                    else {
                        vm.leaverequest.totalNumberOfDays = d;
                    }
                    vm.getExactLeaveDays();
                }

            };

            vm.getExactLeaveDays = function () {
                if (vm.leaverequest.employeeRefId == 0 || vm.leaverequest.employeeRefId == null) {
                    abp.notify.error(app.localize('Employee') + " ?");
                    return;
                }

                var leaveFrom = moment(vm.leaverequest.leaveFrom).format('YYYY-MM-DD');
                if (moment(leaveFrom).isValid() == false) {
                    vm.leaverequest.totalNumberOfDays = null;
                    return;
                }

                var leaveTo = moment(vm.leaverequest.leaveTo).format('YYYY-MM-DD');
                if (moment(leaveTo).isValid() == false) {
                    vm.leaverequest.totalNumberOfDays = null;
                    return;
                }
                vm.loading = true;
                leaverequestService.getNumberOfLeaveDays({
                    leaveFrom: leaveFrom,
                    leaveTo: leaveTo,
                    employeeRefId: vm.leaverequest.employeeRefId
                }).success(function (result) {
                    vm.leaverequest.totalNumberOfDays = result;
                    if (vm.leaverequest.totalNumberOfDays == 1 && vm.leaverequest.halfDayFlag) {
                        vm.leaverequest.totalNumberOfDays = 0.5;
                    }
                    vm.loading = false;
                    vm.getLtNumberOfDays();
                }).finally(function () {
                    vm.loading = false;
                    vm.displayleavetypedetails();
                });
            }

            vm.cancel = function () {
                if (vm.loginStatus == 'EMP')
                    $state.go("tenant.employeeleaverequest")
                else
                    $state.go("tenant.leaverequest")
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.fillLeaveTypes = function (data) {
                vm.leaverequest.gender = data.gender;
                vm.leaverequest.leaveTypeRefCode = null;

                if (vm.leaverequest.gender == "Male") {
                    fillDropDownLeaveTypeForMale();
                }
                else {
                    fillDropDownLeaveTypeForFemale();
                }
            }



            vm.displayleavetypedetails = function () {
                var acYear = moment().format('YYYY');
                if (vm.leaverequest.leaveFrom != null)
                    acYear = moment(vm.leaverequest.leaveFrom).format('YYYY');

                leaverequestService.getAll({
                    employeeRefId: vm.leaverequest.employeeRefId,
                    acYear: acYear
                }).success(function (result) {
                    vm.leaveRequest = result.items;
                    // vm.Leaaverequest.length = result.totalCount;
                    resetTableValues();
                    vm.sno = 0;
                    vm.numberofdays = 0;
                    if (result.totalCount > 0) {
                        angular.forEach(vm.leaveRequest, function (value, key) {
                            if (vm.leaverequest.employeeRefId == result.items[key].employeeRefId) {
                                if (vm.leaverequest.leaveTypeRefCode == result.items[key].leaveTypeRefCode) {
                                    if (vm.leaverequest.id != null && vm.leaverequest.id == value.id) {

                                    }
                                    else {
                                        vm.numberofdays = vm.numberofdays + result.items[key].totalNumberOfDays;
                                        vm.sno = vm.sno + 1;
                                        vm.leaveRequestPortions.push({
                                            'sno': vm.sno,
                                            'leaveTypeRefName': value.leaveTypeRefName,
                                            'dateOfApply': moment(value.dateOfApply).format("YYYY-MMM-DD"),
                                            'leaveFrom': moment(value.leaveFrom).format("YYYY-MMM-DD"),
                                            'leaveTo': moment(value.leaveTo).format("YYYY-MMM-DD"),
                                            'totalNumberOfDays': value.totalNumberOfDays,
                                            'leaveReason': value.leaveReason
                                        });
                                    }
                                }
                            }
                            key = key + 1;
                        });
                    }
                }).finally(function () {
                    vm.loading = false;
                    vm.getLtNumberOfDays();
                });

            }

            vm.getIsDocumentRequired = function (selectedLeaveType) {
                if (!vm.isUndefinedOrNull(selectedLeaveType)) {
                    vm.leaverequest.isDocumentRequired = selectedLeaveType.isSupportingDocumentsRequired;
                }
            };

            vm.exceedError = false;
            vm.ltnumberofdays = [];

            vm.getLtNumberOfDays = function () {
                vm.acYear = 0;
                if (vm.leaverequest.leaveFrom == null || vm.leaverequest.leaveFrom == '') {
                    vm.acYear = parseInt(moment().format('YYYY'));
                }
                else {
                    vm.acYear = parseInt(moment(vm.leaverequest.leaveFrom).format('YYYY'));
                }

                if (vm.acYear == 0) {
                    return;
                }
                if (vm.leaverequest == null) {
                    return;
                }
                if (vm.leaverequest.employeeRefId == null) {
                    return;
                }
                vm.loading = true;
                yearwiseleaveallowedforemployeeService.getEmployeeWiseAcYearWiseLeave({
                    employeeRefId: vm.leaverequest.employeeRefId,
                    acYear: vm.acYear
                }).success(function (leavetyperesult) {
                    vm.ltnumberofdays = leavetyperesult;
                    if (leavetyperesult.length > 0) {
                        angular.forEach(vm.ltnumberofdays, function (value, key) {
                            if (value.leaveTypeRefCode == parseInt(vm.leaverequest.leaveTypeRefCode)) {
                                if ((vm.numberofdays + vm.leaverequest.totalNumberOfDays) > value.numberOfLeaveAllowed) {
                                    abp.notify.error(app.localize('ExceedingErr'));
                                    abp.notify.error(app.localize('Allowed') + ' - ' + value.leaveTypeRefName + ': ' + value.numberOfLeaveAllowed);
                                    abp.notify.error(app.localize('AlreadyTaken') + ' - ' + value.leaveTypeRefName + ': ' + vm.numberofdays);
                                    abp.notify.error(app.localize('Pending') + ' - ' + value.leaveTypeRefName + ': ' + (value.numberOfLeaveAllowed - vm.numberofdays));
                                    //abp.message.info(app.localize('ExceedingErr'));
                                    vm.exceedError = true;
                                    var errorMessaage = app.localize('Allowed') + ' - ' + value.leaveTypeRefName + ': ' + value.numberOfLeaveAllowed;
                                    errorMessaage = errorMessaage + ' ' + app.localize('AlreadyTaken') + ': ' + vm.numberofdays;
                                    errorMessaage = errorMessaage + ' ' + app.localize('Pending') + ': ' + (value.numberOfLeaveAllowed - vm.numberofdays);
                                    //abp.message.error(errorMessaage);
                                }
                                else {
                                    vm.exceedError = false;
                                }
                            }
                            else {
                                vm.exceedError = false;
                            }
                        });
                    }
                    else {
                        abp.notify.error(app.localize('ExceedingErr'));
                        abp.notify.warn(app.localize('KindlyContactHr'));
                        vm.exceedError = true;
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            }

            function resetTableValues() {
                vm.leaveRequestPortions = [];
                vm.leaveRequestPortions.leaveTypeRefName = '';
                vm.leaveRequestPortions.dateOfApply = '';
                vm.leaveRequestPortions.leaveFrom = '';
                vm.leaveRequestPortions.leaveTo = '';
                vm.leaveRequestPortions.totalNumberOfDays = '';
                vm.sno = 0;
                vm.numberofdays = 0;
            }


            vm.refhandoveremployeecode = [];

            function fillDropDownEmployeeHandOver() {
                vm.refhandoveremployeecode = [];
                personalinformationService.getEmployeeForCombobox({}).success(function (result) {
                    vm.refempcode = result.items;
                    angular.forEach(vm.refempcode, function (value, key) {
                        if (value.value != vm.leaverequest.employeeRefId) {
                            vm.refhandoveremployeecode.push(value);
                        }
                    });
                });
            }

            vm.refleavetype = [];

            function fillDropDownLeaveTypeForMale() {
                vm.refleavetype = [];
                vm.loading = true;
                leaverequestService.getLeaveTypesForMale({}).success(function (result) {
                    vm.refleavetype = result;
                    vm.loading = false;
                });
            }

            function fillDropDownLeaveTypeForFemale() {
                vm.refleavetype = [];
                vm.loading = true;
                leaverequestService.getLeaveTypesForFemale({}).success(function (result) {
                    vm.refleavetype = result;
                    vm.loading = false;
                });
            }

            function init() {
                vm.loading = true;
                fillDropDownEmployeeHandOver();
                leaverequestService.getLeaveRequestForEdit({
                    Id: vm.leaverequestId
                }).success(function (result) {
                    vm.leaverequest = result.leaveRequest;
                    if (vm.leaverequest.id == null) {
                        vm.uilimit = 20;
                        vm.leaverequest.employeeRefId = vm.currentEmployeeRefId;
                        if (vm.currentEmployeeRefId != null) {
                            vm.fillLeaveTypes(vm.refemployeecode[0]);
                            vm.displayleavetypedetails();
                        }
                        vm.leaverequest.totalNumberOfDays = "";
                        $scope.dt = moment(new Date()).format("YYYY-MM-DD");
                        vm.leaverequest.leaveFrom = null;
                        vm.leaverequest.leaveTo = null;
                        vm.leaverequest.dateOfApply = $scope.dt;

                    }
                    else {
                        vm.uilimit = null;
                        if (vm.leaverequest.gender == "Male") {
                            fillDropDownLeaveTypeForMale();
                        }
                        else {
                            fillDropDownLeaveTypeForFemale();
                        }


                        $scope.leaveFrom = new Date(vm.leaverequest.leaveFrom);
                        vm.leaverequest.leaveFrom = $scope.leaveFrom;

                        $scope.leaveTo = new Date(vm.leaverequest.leaveTo);
                        vm.leaverequest.leaveTo = $scope.leaveTo;

                        $scope.dateOfApply = new Date(vm.leaverequest.dateOfApply);
                        vm.leaverequest.dateOfApply = moment($scope.dateOfApply).format('YYYY-MM-DD');

                        $scope.minDate = moment(vm.leaverequest.leaveFrom).add(-120, 'days');
                        $scope.maxDate = moment().add(90, 'days');

                        if (vm.leaverequest.halfDayFlag == true) {
                            if (vm.leaverequest.halfDayTimeSpecification == "FH")
                                document.getElementById('forenoon').checked = true;
                            else
                                document.getElementById('afternoon').checked = true;
                        }

                        if (!vm.isUndefinedOrNull(vm.leaverequest.fileName)) {
                            vm.leaverequest.isDocumentRequired = true;
                            vm.importpath = abp.appPath + 'PersonalInformation/DownloadLeaveDocumentInfo?leaveRequestRefId=' + vm.leaverequest.id;
                        }
                        else {
                            vm.importpath = "";
                        }



                    }
                    vm.displayleavetypedetails();

                });
                abp.ui.clearBusy('#leaverequestForm');
            }

            vm.refemployeecode = [];
            function fillDropDownEmployee() {
                vm.loading = true;
                if (vm.currentEmployeeRefId == null) {
                    personalinformationService.getEmployeeWithSkillSetForCombobox({}).success(function (result) {
                        vm.refemployeecode = result;
                        init();
                    });
                }
                else {
                    personalinformationService.getParticularEmployeeWithSkillSetForCombobox({
                        id: vm.currentEmployeeRefId
                    }).success(function (result) {
                        vm.refemployeecode = result;
                        init();
                    });
                }
            }
            fillDropDownEmployee();



        }
    ]);
})();

//vm.displaydetails = function () {
//    leaverequestService.getAll({
//        Id: vm.leaverequestId
//    }).success(function (result) {
//        vm.leaveRequest = result.items;
//        resetTableValues();
//        fillDropDownEmployeeHandOver();
//        vm.sno = 0;
//        if (result.totalCount > 0) {
//            angular.forEach(vm.leaveRequest, function (value, key) {
//                if (vm.leaverequest.employeeRefId == result.items[key].employeeRefId) {
//                    vm.sno = vm.sno + 1;
//                    vm.leaveRequestPortions.push({
//                        'sno': vm.sno,
//                        'leaveTypeRefName': value.leaveTypeRefName,
//                        'dateOfApply': moment(value.dateOfApply).format("YYYY-MMM-DD"),
//                        'leaveFrom': moment(value.leaveFrom).format("YYYY-MMM-DD"),
//                        'leaveTo': moment(value.leaveTo).format("YYYY-MMM-DD"),
//                        'totalNumberOfDays': value.totalNumberOfDays,
//                        'leaveReason': value.leaveReason
//                    })
//                }
//            });
//        }
//    }).finally(function () {
//        vm.loading = false;
//    });

//}