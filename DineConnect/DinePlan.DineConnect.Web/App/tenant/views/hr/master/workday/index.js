﻿
(function () {
    appModule.controller('tenant.views.hr.master.workday.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.workDay',
        function ($scope, $modal, uiGridConstants, workdayService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Hr.Master.WorkDay.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Hr.Master.WorkDay.Edit'),
                //'delete': abp.auth.hasPermission('Pages.Tenant.Hr.Master.WorkDay.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    //{
                    //	name: app.localize('Actions'),
                    //	enableSorting: false,
                    //	width: 120,

                    //	cellTemplate:
                    //	   "<div class=\"ui-grid-cell-contents\">" +
                    //		   "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                    //		   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                    //		   "    <ul uib-dropdown-menu>" +
                    //		   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editWorkDay(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                    //		   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteWorkDay(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                    //		   "    </ul>" +
                    //		   "  </div>" +
                    //		   "</div>"
                    //               },
                    {
                        name: app.localize('Edit'),
                        enableSorting: false,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-if="grid.appScope.permissions.edit"  ng-click="grid.appScope.editWorkDay(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>' +
                            '  <br>' +
                            '</div>',
                        width: 50,
                    },
                    {
                        name: app.localize('DayOfWeekRefName'),
                        field: 'dayOfWeekRefName'
                    },
                    {
                        name: app.localize('WorkHours'),
                        field: 'noOfHourWorks',
                        cellFilter: 'fractionFilter',
                    },
                    {
                        name: app.localize('RestHours'),
                        field: 'restHours',
                        cellFilter: 'fractionFilter',
                    },
                    {
                        name: app.localize('NoOfWorkDays'),
                        field: 'noOfWorkDays',
                        cellFilter: 'fractionFilter',
                    },
                    {
                        name: app.localize('OtRatio'),
                        field: 'otRatio',
                        cellFilter: 'fractionFilter',
                    },
                    {
                        name: app.localize('Maximum') + ' ' + app.localize('OT') + ' ' + app.localize('Allowed') + ' ' + app.localize('Hours'),
                        field: 'maximumOTAllowedHours',
                        cellFilter: 'fractionFilter',
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'lll\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };



            vm.getAll = function () {
                vm.loading = true;
                workdayService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editWorkDay = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createWorkDay = function () {
                openCreateOrEditModal(null);
            };

            vm.deleteWorkDay = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteWorkDayWarning', myObject.dayOfWeekRefName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            workdayService.deleteWorkDay({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/workday/createOrEditModal.cshtml',
                    controller: 'tenant.views.hr.master.workday.createOrEditModal as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        workdayId: function () {
                            return objId;
                        },
                        employeeRefId: function () {
                            return null;
                        },
                        employeeRefCode: function () {
                            return null;
                        },
                        employeeRefName: function () {
                            return null;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.exportToExcel = function () {
                workdayService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.getAll();

        }])

        .filter('fractionFilter', function () {
            return function (value) {
                //return value.toFixed(2);
                return value;
            };
        });
})();

