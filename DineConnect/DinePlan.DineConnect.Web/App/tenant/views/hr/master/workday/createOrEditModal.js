﻿(function () {
    appModule.controller('tenant.views.hr.master.workday.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.workDay', 'workdayId', 'employeeRefId', 'employeeRefCode', 'employeeRefName',
        function ($scope, $modalInstance, workdayService, workdayId, employeeRefId, employeeRefCode, employeeRefName) {
            /* eslint-disable */
            var vm = this;

            vm.saving = false;
            vm.workday = null;

            vm.save = function () {
                if (vm.existAllElements() == false)
                    return;
                if (vm.workday.noOfHourWorks == 0) {
                    if (vm.workday.noOfWorkDays != 0) {
                        abp.notify.error('Work Day Should Be Zero if the Work Hours is Zero');
                        return;
                    }
                }
                vm.saving = true;
                vm.additionSuccessFlag = false;
                workdayService.createOrUpdateWorkDay({
                    workDay: vm.workday
                }).success(function () {
                    abp.notify.info(app.localize('WorkDay') + ' ' + app.localize('SavedSuccessfully'));
                    $modalInstance.close(true);
                    vm.additionSuccessFlag = true;
                }).finally(function () {
                    if (vm.additionSuccessFlag == false) {

                    }
                    vm.saving = false;
                });
            };

            vm.existAllElements = function () {
                if (vm.workday.noOfHourWorks + vm.workday.restHours > 24) {
                    abp.notify.error(app.localize('TotalWorkHourIncludingRestHoursShouldBeLessThanOrEqual24Hours'));
                    abp.message.error(app.localize('TotalWorkHourIncludingRestHoursShouldBeLessThanOrEqual24Hours'));
                    return false;
                }

                return true;
            }


            vm.cancel = function () {
                $modalInstance.dismiss(null);
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            function init() {
                workdayService.getWorkDayForEdit({
                    Id: workdayId
                }).success(function (result) {
                    vm.workday = result.workDay;
                    vm.workday.employeeRefId = employeeRefId;
                    vm.workday.employeeRefCode = employeeRefCode;
                    vm.workday.employeeRefName = employeeRefName;
                });
            }
            init();
        }
    ]);
})();

