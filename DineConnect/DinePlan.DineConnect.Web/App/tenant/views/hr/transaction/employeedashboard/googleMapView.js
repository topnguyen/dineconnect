﻿(function () {
    appModule.controller('tenant.views.employeedashboard.googleMapView', [
        '$scope', '$uibModalInstance', 'lat','lon',
        function ($scope, $uibModalInstance,lat,lon) {
            var vm = this;
            vm.lat = lat;
            vm.lon = lon;
            vm.viewMap = function () {
                //alert(lat);
                //var lat = 10.8535798;
                //var lon = 77.0179714;
                var myLatlng = new google.maps.LatLng(lat, lon) // This is used to center the map to show our markers
                var mapOptions = {
                    center: myLatlng,
                    zoom: 17,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    marker: true
                };
                var map = new google.maps.Map(document.getElementById("map_div"), mapOptions);
                var marker = new google.maps.Marker({
                    position: myLatlng, 
                    title: 'hi'
                });
                marker.setMap(map);
            }

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
            
        }
    ]);
})();