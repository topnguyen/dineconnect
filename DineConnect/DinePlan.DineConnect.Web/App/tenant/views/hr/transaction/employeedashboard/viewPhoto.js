﻿(function () {
    appModule.controller('tenant.views.employeedashboard.viewPhoto', [
        '$scope', '$uibModalInstance','imageId',
        function ($scope, $uibModalInstance,imageId) {
            var vm = this;    
            vm.imageId = imageId;
            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
            
        }
    ]);
})();