﻿(function () {
    appModule.controller('tenant.views.employeedashboard.employeeDashboard', [
        '$scope', '$state', '$uibModal', '$stateParams', 'abp.services.app.timeSheetMaster', 'appSession', 'abp.services.app.personalInformation', 'abp.services.app.employeeDocumentInfo',
        function ($scope, $state, $modal, $stateParams, timesheetmasterService, appSession, personalinformationService, employeedocumentinfoService) {

            /* eslint-disable */
            var vm = this;
            vm.tenantName = appSession.tenant.tenancyName;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.uilimit = null;

            vm.refemployeecode = [];
            function fillDropDownEmployee() {
                personalinformationService.getEmployeeWithSkillSetForCombobox({}).success(function (result) {
                    vm.refemployeecode = result;
                });
            }

            fillDropDownEmployee();

            vm.employeeDutyChartDetails = [];
            vm.monthWisePendingList = [];
            vm.hrOperationHealthHtml = null;
            vm.incentiveConsolidated = [];
            vm.gpsDayWiseList = [];
            vm.employeeSkills = null;


            vm.printTime = moment().format('MMM Do YY, h:mm:ss a');
            vm.employeeCode = '';
            vm.companyCode = '';

            vm.firsttimeflag = true;

            vm.permissions = {
                otherEmployeeDashBoard: true, // abp.auth.hasPermission('Pages.Tenant.OtherEmployeeDashboard'),
                showSalaryTags: abp.auth.hasPermission('Pages.Tenant.EmployeeDashboard.ShowSalaryInEmployeeDashBoard'),
                generateSalary: true, // abp.auth.hasPermission('Pages.Tenant.Hr.Master.SalaryInfo.GenerateSalary')
            };

            vm.manualincentivespermissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.ManualIncentive.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.ManualIncentive.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.ManualIncentive.Delete')
            };

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.printmailflag = true;
            vm.loginStatus = $stateParams.loginStatus;
            vm.currentEmployeeRefId = null;

            vm.errorFlag = false;

            if (vm.loginStatus == 'EMP') {
                if (appSession.useremployeerefid == null || appSession.useremployeerefid == 0) {
                    abp.notify.error(app.localize('EmployeeCodeNotAssignedToThisUser', appSession.user.userName));
                    //abp.message.error(app.localize('EmployeeCodeNotAssignedToThisUser', appSession.user.userName));
                    vm.errorFlag = true;
                }
                vm.currentEmployeeRefId = appSession.useremployeerefid;
            }

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().subtract(3, 'day').format($scope.format);
            var toDateAsString = moment().format($scope.format);

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            var sd = moment().startOf('month');//moment().subtract(10, 'day');
            var td = moment(); //moment().subtract(1, 'day');

            var diff = sd.diff(td, 'days')
            if (Math.abs(diff) < 4) {
                sd = moment(moment().subtract(5, 'day')).startOf('month');
            }

            vm.dateRangeModel = {
                startDate: sd,
                endDate: td
            };
            vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
            vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;
            vm.salaryMonth = moment(vm.dateRangeModel.startDate).format('MMM-YYYY');

            vm.noofTimes = 2;

            vm.displayErrorFlag = false;

            vm.getLocation = function (arglat, arglon) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/transaction/employeedashboard/googleMapView.cshtml',
                    controller: 'tenant.views.employeedashboard.googleMapView as vm',
                    backdrop: 'static',
                    resolve: {
                        lat: arglat,
                        lon: arglon
                    }
                });
            }

            vm.getImage = function (argImageId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/transaction/employeedashboard/viewPhoto.cshtml',
                    controller: 'tenant.views.employeedashboard.viewPhoto as vm',
                    backdrop: 'static',
                    resolve: {
                        imageId: argImageId

                    }
                });
                modalInstance.result.then(function () {
                    //on ok button press 
                }, function () {
                    //on cancel button press
                    console.log("Modal Closed");
                });
            }

            vm.getInputData = function () {

                //var noofTimes = prompt("Number of Months ?", "3");

                //if (isNaN(noofTimes) == true) {
                //    vm.noofTimes = 3;
                //}
                //else {
                //    vm.noofTimes = noofTimes;
                //}

                //vm.showTimeSheetData();

            }

           

            vm.jobStatusChart = function () {
                
                vm.gpsDayWiseList = [];
                vm.getGpsAttendance();


                if (vm.firsttimeflag) {
                    if (vm.isUndefinedOrNull(vm.dateRangeModel.startDate)) {
                        vm.dateRangeModel = {
                            startDate: sd,
                            endDate: td
                        };
                        vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
                        vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;
                    }
                    var startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    var endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }
                else {
                    if (vm.isUndefinedOrNull(vm.dateRangeModel.startDate)) {
                        vm.dateRangeModel = {
                            startDate: sd,
                            endDate: td
                        };
                        vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
                        vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;
                    }
                    var startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    var endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }

                if (vm.currentEmployeeRefId == 0 || vm.currentEmployeeRefId == null) {
                    abp.notify.error(app.localize('EmployeeCodeNotAssignedToThisUser', appSession.user.userName));
                    return;
                }

                vm.salaryMonth = moment(vm.dateRangeModel.startDate).format('MMM-YYYY');

                var month1 = moment(vm.dateRangeModel.endDate).format('MMM-YYYY');
                var month2 = moment(vm.dateRangeModel.endDate).subtract(2, 'month').format('MMM-YYYY');

                vm.alertHrMonthPending = app.localize('AlertHrMonthPending', month1, month2);
                if (vm.personalinformation != null)
                    vm.personalinformation.profilePictureId = null;
                vm.personalinformation = null;
                vm.personalinfo();


                vm.employeeDutyChartDetails = [];
                vm.monthWisePendingList = [];
                vm.hrOperationHealthHtml = null;
                vm.daywiseStatus = [];
                vm.daysvshourwisestatus = [];
                vm.loadingCount++;
                vm.loading = true;
                timesheetmasterService.getEmployeeDashBoardDutyChartData({
                    startDate: startDate,
                    endDate: endDate,
                    employeeRefId: vm.currentEmployeeRefId,
                    isCalendarEventsRequired: true
                }).success(function (result) {
                    vm.remarksFlag = result.remarksFlag;
                    vm.remarks = result.remarks;
                    vm.hrOperationHealthHtml = result.hrOperationHealthHtml;
                    vm.daysvshourwisestatus = result.daysVsHoursWorkStatus;

                    $scope.events.slice(0, $scope.events.length);
                    $scope.events = [];
                    //var displayEventTime = false;
                    angular.forEach(result.calendarEvents, function (value) {
                        var i = 1;
                        //  displayEventTime = !displayEventTime;
                        $scope.events.push({
                            title: value.title,
                            description: value.description,
                            workDate: new Date(value.startAt),
                            start: value.startAt, // new Date(parseInt(value.StartAt.substr(6))),
                            end: value.endAt, //new Date(parseInt(value.EndAt.substr(6))),
                            allDay: value.isFullDay,
                            stick: true,
                            //color: 'red',
                            // w: value.backGroundColor == null ? '#378006' : value.backGroundColor,
                            workStatus: value.workStatus,
                            projectCostCentreRefName: value.projectCostCentreRefName,
                            tsCurrentStatus: value.tsCurrentStatus,
                            tsNgClass: value.tsNgClass,
                            tsInfo: value.tsInfo,
                            //color: value.color == null ? "" : value.color,
                            backgroundColor: '#378006',

                            //displayEventTime: displayEventTime
                        });

                    });
                    $scope.eventSources = [];
                    $scope.eventSources = [$scope.events];
                    $scope.calendarDefaultDate = moment(startDate, $scope.format); // moment(moment().subtract(35, 'day')).startOf('month');
                    $scope.uiConfig.calendar.defaultDate = $scope.calendarDefaultDate;
                    $scope.uiConfig.calendar.events = $scope.events;

                    if (vm.hrOperationHealthHtml != null && vm.hrOperationHealthHtml != '') {
                        vm.displayErrorFlag = true;
                    }
                    else {
                        vm.displayErrorFlag = false;
                    }

                    if (vm.remarksFlag) {
                        abp.notify.warn(vm.remarks);
                        abp.message.warn(vm.remarks);
                    }

                    vm.jobstatusheader = result.header;
                    if (vm.firsttimeflag) {
                        startDate = result.startDate;
                        endDate = result.endDate;

                    }
                    vm.employeeDutyChartDetails = angular.copy(result.dutyList);
                    vm.incentiveConsolidatedList = angular.copy(result.incentiveConsolidatedList);
                    vm.overAllIncentiveAmount = result.overAllIncentiveAmount;
                    vm.jobstatusheader = result.header;
                    vm.monthWisePendingList = result.monthWisePendingList;
                    vm.daywiseStatus = result.pieChartData;
                    vm.printTime = moment().format('MMMM Do YYYY, h:mm:ss a');
                    vm.employeeRefCode = result.employeeRefCode;
                    vm.companyRefCode = result.companyRefCode;

                    Highcharts.chart('jobstatus_stats', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{point.percentage:.1f} %'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.y} <br/>{point.percentage:.1f} %',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            name: app.localize('JobStatus'),
                            colorByPoint: true,
                            data: $.parseJSON(JSON.stringify(vm.daywiseStatus))
                        }]
                    });

                    Highcharts.chart('hourwise_jobstatus_stats', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{point.percentage:.1f} %'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.y} <br/>{point.percentage:.1f} %',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            name: app.localize('HourWise') + app.localize('JobStatus'),
                            colorByPoint: true,
                            data: $.parseJSON(JSON.stringify(result.timeWiseWorkStatus))
                        }]
                    });


                }).finally(function () {
                    vm.loading = false;
                    vm.loadingCount--;
                    if (vm.firsttimeflag == true) {
                        vm.firsttimeflag = false;
                        vm.uilimit = 10;
                    }
                });
            };

            vm.myFunction = function () {
                document.getElementById("divprintemail").style.visibility = "hidden";
                document.getElementById("divSendMailToAll").style.visibility = "hidden";
                document.getElementById("divMailFormat").style.visibility = "hidden";
                //document.getElementById("divCalendar").style.visibility = "hidden";
                $("#divCalendar").remove();
                vm.printmailflag = false;
                myPrint();
            }


            function myPrint() {
                window.print();
                vm.printmailflag = true;
                document.getElementById("divprintemail").style.visibility = "visible";
                document.getElementById("divSendMailToAll").style.visibility = "visible";
                document.getElementById("divMailFormat").style.visibility = "visible";
                //document.getElementById("divCalendar").style.visibility = "visible";
            }

            vm.mailList = [];

            vm.sendMail = function (mailFlag) {
                if (vm.firsttimeflag) {
                    if (vm.isUndefinedOrNull(vm.dateRangeModel.startDate)) {
                        vm.dateRangeModel = {
                            startDate: sd,
                            endDate: td
                        };
                        vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
                        vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;
                    }
                    var startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    var endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }
                else {
                    if (vm.isUndefinedOrNull(vm.dateRangeModel.startDate)) {
                        vm.dateRangeModel = {
                            startDate: sd,
                            endDate: td
                        };
                        vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
                        vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;
                    }
                    var startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    var endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }

                vm.loading = true;
                vm.mailList = [];
                timesheetmasterService.sendEmployeeStatusEmail(
                    {
                        startDate: startDate,
                        endDate: endDate,
                        employeeRefId: vm.currentEmployeeRefId,
                        mailSendFlag: mailFlag
                    }).success(function (result) {
                        vm.mailList = result;
                    }).finally(function () {
                        vm.loading = false;
                    });
            }

            vm.toAllEmployeeSendMail = function (mailFlag) {
                if (mailFlag) {
                    var dayofyear = moment().dayOfYear();
                    var entered = prompt("Please enter Day Of the Current Year for Send Mail To All Employee");
                    if (entered != dayofyear) {
                        abp.notify.error(app.localize('Wrong'));
                        return;
                    }

                    var password = prompt("Please enter Password for Send Mail To All Employee");
                    if (password != 'qwerty123#') {
                        abp.notify.error(app.localize('Wrong'));
                        return;
                    }
                }

                if (vm.firsttimeflag) {
                    if (vm.isUndefinedOrNull(vm.dateRangeModel.startDate)) {
                        vm.dateRangeModel = {
                            startDate: sd,
                            endDate: td
                        };
                        vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
                        vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;
                    }
                    var startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    var endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }
                else {
                    if (vm.isUndefinedOrNull(vm.dateRangeModel.startDate)) {
                        vm.dateRangeModel = {
                            startDate: sd,
                            endDate: td
                        };
                        vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
                        vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;
                    }
                    var startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    var endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }

                vm.loading = true;
                timesheetmasterService.toAllEmployeeSendStatusEmail(
                    {
                        startDate: startDate,
                        endDate: endDate,
                        employeeRefId: vm.currentEmployeeRefId,
                        mailSendFlag: mailFlag
                    }).success(function (result) {
                        vm.mailList = result;
                    }).finally(function () {
                        vm.loading = false;
                    });
            }

            vm.addManualIncentives = function (argEmployeeRefId, argIncentiveDate, argtimeSheetMasters) {
                var argNdtProcedureRefId = null;
                var argProjectCostCentreRefId = null;
                if (argtimeSheetMasters != null) {
                    if (argtimeSheetMasters.length == 1) {
                        argProjectCostCentreRefId = argtimeSheetMasters[0].projectCostCentreRefId;
                        argNdtProcedureRefId = argtimeSheetMasters[0].ndtProcedureRefId;
                    }
                }

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/transaction/manualincentive/manualIncentive.cshtml',
                    controller: 'tenant.views.hr.transaction.manualincentive.manualIncentive as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        manualincentiveId: function () {
                            return null;
                        },
                        callfromIndex: function () {
                            return false;
                        },
                        argEmployeeRefId: function () {
                            return argEmployeeRefId;
                        },
                        argIncentiveDate: function () {
                            return argIncentiveDate;
                        },
                        argNdtProcedureRefId: function () {
                            return argNdtProcedureRefId;
                        },
                        argProjectCostCentreRefId: function () {
                            return argProjectCostCentreRefId;
                        },
                    }
                });

                modalInstance.result.then(function (result) {
                    if (result != null && result.length > 0) {
                        angular.forEach(result, function (value, key) {
                            if (value.employeeRefId == vm.currentEmployeeRefId) {
                                vm.employeeDutyChartDetails.some(function (dutyvalue, key) {
                                    if (dutyvalue.dutyChartDate == argIncentiveDate) {
                                        dutyvalue.incentiveList.push(value);
                                        return true;
                                    }
                                });
                            }
                        });

                    }
                });
            }

            vm.getMapsData = function (data) {
                var lat = 1.3031948;   //document.getElementById('txtlat').value;
                var lon = 103.6549829; //document.getElementById('txtlon').value;
                var myLatlng = new google.maps.LatLng(lat, lon) // This is used to center the map to show our markers
                var mapOptions = {
                    center: myLatlng,
                    zoom: 18,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    marker: true
                };
                var map = new google.maps.Map(document.getElementById("map_canvas-" + idx), mapOptions);
                var marker = new google.maps.Marker({
                    position: myLatlng
                });
                marker.setMap(map);
                data.showMapflag = true;
            };

            //  Employee Docuemnt Info
            vm.employeeDocumentInfos = [];
            vm.existEmployeeRefId = null;
            vm.getAllEmployeeDocumentInfo = function () {
                if (vm.existEmployeeRefId == vm.currentEmployeeRefId)
                    return;

                vm.loading = true;
                vm.loadingCount++;
                var startDate = null;
                var endDate = null;

                employeedocumentinfoService.getAll({
                    employeeRefId: vm.currentEmployeeRefId,
                    documentInfoRefId: null,
                    startDate: startDate,
                    endDate: endDate,
                    //alertPendingFlag: true,
                    //alertExpiredFlag: true,
                    activeEmployeeOnly: true,
                    sorting: ''
                }).success(function (result) {
                    vm.employeeDocumentInfos = result.items;
                    vm.existEmployeeRefId = vm.currentEmployeeRefId;
                }).finally(function () {
                    vm.loading = false;
                    vm.loadingCount--;
                });
            };
            vm.getAllEmployeeDocumentInfo();

            vm.directDownloadEmployeeDocument = function (item) {
                vm.importpath = abp.appPath + 'PersonalInformation/DownloadPersonalDocumentTemplate?employeeRefId=' + item.employeeRefId + "&documentInfoRefId=" + item.documentInfoRefId;
                window.open(vm.importpath);
            }

            //-----------------------------

            //  Salary Information
            vm.salaryDetails = [];
            vm.getSalaryForTheEmployee = function (argEmployeeRefId) {
                if (vm.isUndefinedOrNull(vm.dateRangeModel.startDate)) {
                    vm.dateRangeModel = {
                        startDate: sd,
                        endDate: td
                    };
                    vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
                    vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;
                }
                var startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                var endDate = moment(vm.dateRangeModel.endDate).format($scope.format);

                vm.loading = true;
                vm.salaryDetails = [];
                timesheetmasterService.getSalaryForTheGivenMonth({
                    monthYear: startDate,
                    employeeRefId: argEmployeeRefId
                }).success(function (result) {
                    if (result.successFlag) {
                        vm.salaryPayable = result;
                        vm.showSalaryModal();
                    }
                }).finally(function () {
                    vm.loading = false;
                });

            }

            var salaryModalInstance = null;

            vm.showSalaryModal = function () {
                vm.loading = true;               

                salaryModalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/salaryinfo/salaryModal.cshtml',
                    controller: 'tenant.views.hr.master.salaryinfo.salaryModal as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        salaryPayable: function () {
                            return vm.salaryPayable;
                        },
                        employeeSkillSetList: function () {
                            return vm.employeeSkillSetList;
                        }
                    }
                });

                vm.loading = false;

                salaryModalInstance.result.then(function (result) {
                    vm.loading = false;
                });
            };
            //





            vm.getGpsAttendance = function () {
                if (vm.firsttimeflag) {
                    if (vm.isUndefinedOrNull(vm.dateRangeModel.startDate)) {
                        vm.dateRangeModel = {
                            startDate: sd,
                            endDate: td
                        };
                        vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
                        vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;
                    }
                    var startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    var endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }
                else {
                    if (vm.isUndefinedOrNull(vm.dateRangeModel.startDate)) {
                        vm.dateRangeModel = {
                            startDate: sd,
                            endDate: td
                        };
                        vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
                        vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;
                    }
                    var startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    var endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }

                vm.gpsloading = true;
                vm.gpsDayWiseList = [];
                timesheetmasterService.getGpsEmployeeAttendanceDateWise({
                    startDate: startDate,
                    endDate: endDate,
                    employeeRefId: vm.currentEmployeeRefId
                }).success(function (result) {
                    vm.gpsDayWiseList = result;
                    vm.gpsloading = false;
                });


            };

            vm.personalinfo = function () {
                vm.loadingEmployeeFlag = true;
                vm.personalinformation = null;
                personalinformationService.getEmployeeDetailsBasedOnEmployeeRefId({
                    employeeRefId: vm.currentEmployeeRefId
                }).success(function (result) {
                    vm.personalinformation = result;
                }).finally(function () {
                    vm.loadingEmployeeFlag = false;
                });
            };

            vm.getSkillSet = function () {
                personalinformationService.getSkillSetListOfGivenEmployee({
                    employeeRefId: vm.currentEmployeeRefId
                }).success(function (result) {
                    vm.employeeSkills = result;
                }).finally(function () {
                    vm.loadingEmployeeFlag = false;
                });
            }

           
            // Calendar ERP
            // full calendar

            ////-----------------------------

            vm.directTimeSheetDownload = function (tsMasterRefId) {
                vm.importpath = abp.appPath + 'PersonalInformation/DownloadTimeSheetData?tsMasterId=' + tsMasterRefId + "&tenantName=" + vm.tenantName;
                window.open(vm.importpath);
            }

            $scope.SelectedEvent = null;
            var isFirstTime = true;

            $scope.events = [];
            $scope.eventSources = [$scope.events];


            //configure calendar

            //$scope.calendarDefaultDate = moment(moment().subtract(35, 'day')).startOf('month');

            $scope.uiConfig = {
                calendar: {
                    height: 600,
                    defaultDate: $scope.calendarDefaultDate,
                    editable: false,
                    displayEventTime: true,
                    header: {
                        left: 'month agendaWeek agendaDay', //basicWeek basicDay 
                        center: 'title',
                        right: 'today prev,next'
                    },
                    next: function (next) {
                        var amoment = $('#calendar').fullCalendar('getDate');
                        alert("The current date of the calendar is " + amoment.format());
                    },
                    eventClick: function (event) {
                        $scope.SelectedEvent = event;
                    },
                    eventAfterAllRender: function () {
                        //if ($scope.events.length > 0 && isFirstTime) {
                        //    //Focus first event
                        //    uiCalendarConfig.calendars.myCalendar.fullCalendar('gotoDate', $scope.events[0].start);
                        //    isFirstTime = false;
                        //}
                    },

                    eventRender: function (eventObj, $el) {
                        var a = $el;

                        $el.popover({
                            title: eventObj.title,
                            content: eventObj.description,
                            trigger: 'hover',
                            placement: 'top',
                            container: 'body'
                        });


                        if (eventObj.workStatus == "Idle") {
                            $el.css('background-color', '#ADFF2F'); // , red , FF7F7F , 8B3626
                            $el.css('color', '#000000');
                        } else if (eventObj.workStatus == "Billable") {
                            if (eventObj.tsNgClass == 'InvoiceDone') {
                                $el.css('background-color', '#4CA64C'); //green
                            }
                            else if (eventObj.tsNgClass == 'PendingForInvoice') {
                                $el.css('background-color', '#007277'); // Mediumpurple , FFFFFF , C0FF3E , FF8181
                                $el.css('color', '#FFFFFF'); // Mediumpurple
                            }
                            else if (eventObj.tsNgClass == 'PinkPending') {
                                $el.css('background-color', '#FF748C'); // Mediumpurple , FF94A5
                                $el.css('color', '#000000'); // Mediumpurple
                            }
                            else if (eventObj.tsNgClass == 'YellowPending') {
                                $el.css('background-color', '#ffea00'); // Mediumpurple 999900
                                $el.css('color', '#000000');
                            }
                            else if (eventObj.tsNgClass == 'Unknown') {
                                $el.css('background-color', '#5A83FF'); // Mediumpurple 7F7F7F, FF7F24, FF984F , 4876FF
                            }
                            //  eventElement.css('color', 'Red');
                        } else if (eventObj.workStatus == "WeekOff") {
                            $el.css('background-color', '#998100');
                        } else if (eventObj.workStatus == "Leave") {
                            $el.css('background-color', '#00BFFF');
                        } else if (eventObj.workStatus == "Office Job") {
                            $el.css('background-color', '#9999FF'); // bLUE
                        } else if (eventObj.workStatus == "Opr-Not Billable") {
                            $el.css('background-color', '#B266B2'); // Purple
                        } else if (eventObj.workStatus == "Unknown") {
                            eventElement.css('background-color', '#4876FF'); // 7F7F7F , 4876FF
                        } else if (eventObj.workStatus == "PH") {
                            $el.css('background-color', '#FFFFFF');
                            $el.css('color', '#000000');
                        } else if (eventObj.workStatus == "Absent") {
                            $el.css('background-color', 'Black');
                        }


                    }

                }
            };
        }
    ]);
})();



