﻿
(function () {
    appModule.controller('tenant.views.hr.transaction.biometricexcludedentry.bioMetricExcludedEntry', [
        '$scope', '$state', '$stateParams', '$uibModalInstance', 'abp.services.app.bioMetricExcludedEntry', 'biometricexcludedentryId', 'abp.services.app.projectCostCentre', 'abp.services.app.personalInformation', 'employeeRefId', 'excludeddate',
        function ($scope, $state, $stateParams, $modalInstance, biometricexcludedentryService, biometricexcludedentryId, projectcostcentreService, personalinformationService, argemployeeRefId, argexcludeddate) {
            var vm = this;
            
            vm.saving = false;
            vm.biometricexcludedentry = null;
            $scope.existall = true;


            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.argemployeerefid = argemployeeRefId;
            vm.argexcludeddate = argexcludeddate;

            
            vm.currentUserId = abp.session.userId;

            //date start
            //  Date Range Options
			var todayAsString = moment().format('YYYY-MM-DD');
			vm.dateRangeOptions = app.createDateRangePickerOptions();
			vm.dateRangeModel = {
			    startDate: todayAsString,
			    endDate: todayAsString
			};

            //  Date Angular Js 
			$scope.today = function () {
			    $scope.dt = moment(new Date()).format("DD-MMM-YYYY");

			};
			$scope.today();

			$scope.clear = function () {
			    $scope.dt = null;
			};

			$scope.inlineOptions = {
			    customClass: getDayClass,
			    minDate: new Date(),
			    showWeeks: true
			};

			$scope.dateOptions = {
			    dateDisabled: disabled,
			    formatYear: 'yy',
			    maxDate: new Date(2020, 5, 22),
			    minDate: new Date(),
			    startingDay: 1,
                showWeeks:false
			};

            // Disable weekend selection
			function disabled(data) {
			    var date = data.date,
                  mode = data.mode;
			    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
			}

			$scope.toggleMin = function () {
			    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
			    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
			};

			$scope.toggleMin();

			$scope.open1 = function () {
			    $scope.popup1.opened = true;
			};

			$scope.open2 = function () {
			    $scope.popup2.opened = true;
			};

			$scope.open3 = function () {
			    $scope.popup3.opened = true;
			};

			$scope.setDate = function (year, month, day) {
			    $scope.dt = new Date(year, month, day);
			};

			$scope.formats = ['yyyy-MM-dd'];
			$scope.format = $scope.formats[0];
            //$scope.altInputFormats = ['M!/d!/yyyy'];
			$scope.altInputFormats = ['dd-MMMM-yyyy'];

			$scope.minDate = moment().add(-45, 'days');
			$scope.maxDate = moment().add(90, 'days');
			
			$('input[name="stDate"]').daterangepicker({
			    locale: {
			        format: $scope.format
			    },
			    singleDatePicker: true,
			    showDropdowns: true,
                startDate: moment(vm.argexcludeddate),
			    minDate: $scope.minDate,
			    maxDate: $scope.maxDate
			});

			$('input[name="endDate"]').daterangepicker({
			    locale: {
			        format: $scope.format
			    },
			    singleDatePicker: true,
			    showDropdowns: true,
                startDate: moment(vm.argexcludeddate),
			    minDate: $scope.minDate,
			    maxDate: $scope.maxDate
			});


			$scope.popup1 = {
			    opened: false
			};

			$scope.popup2 = {
			    opened: false
			};

			$scope.popup3 = {
			    opened: false
			};

			var tomorrow = new Date();
			tomorrow.setDate(tomorrow.getDate() + 1);
			var afterTomorrow = new Date();
			afterTomorrow.setDate(tomorrow.getDate() + 1);
			$scope.events = [
              {
                  date: tomorrow,
                  status: 'full'
              },
              {
                  date: afterTomorrow,
                  status: 'partially'
              }
			];

			function getDayClass(data) {
			    var date = data.date,
                  mode = data.mode;
			    if (mode === 'day') {
			        var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

			        for (var i = 0; i < $scope.events.length; i++) {
			            var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

			            if (dayToCheck === currentDay) {
			                return $scope.events[i].status;
			            }
			        }
			    }

			    return '';
			}

            //date end

			
            vm.save = function () {
			   if ($scope.existall == false)
                    return;
                vm.saving = true;

                vm.biometricexcludedentry.excludeFromDate = moment(vm.argexcludeddate).format('YYYY-MMM-DD');
                vm.biometricexcludedentry.excludeToDate = moment(vm.argexcludeddate).format('YYYY-MMM-DD');

                biometricexcludedentryService.createOrUpdateBioMetricExcludedEntry({
                    bioMetricExcludedEntry: vm.biometricexcludedentry
                }).success(function () {
                    abp.notify.info('\' BioMetricExcludedEntry \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close(true);
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.biometricexcludedentry.id == null) {
			         vm.existall = false;
			         return;
			     }
				 
                biometricexcludedentryService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.biometricexcludedentry.id,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.biometricexcludedentry.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.biometricexcludedentry.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss(false);
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
			 vm.refemployeecode = [];

			 function fillDropDownEmployee() {
			     personalinformationService.getEmployeeForCombobox({}).success(function (result) {
			         vm.refemployeecode = result.items;
			     });
			 }

			 vm.refprojcostcentre = [];

			 function fillDropDownProjectCostCentre() {
			     projectcostcentreService.getProjectCostCentreForCombobox({}).success(function (result) {
			         vm.refprojcostcentre = result.items;
			     });
			 }


	        function init() {
				fillDropDownEmployee();
				fillDropDownProjectCostCentre();
                biometricexcludedentryService.getBioMetricExcludedEntryForEdit({
                    Id: biometricexcludedentryId
                }).success(function (result) {
                    vm.biometricexcludedentry = result.bioMetricExcludedEntry;

                    if (!vm.isUndefinedOrNull(vm.argemployeerefid)) {
                        vm.biometricexcludedentry.excludeFromDate = moment(vm.argexcludeddate).format('YYYY-MMM-DD');
                        vm.biometricexcludedentry.excludeToDate = moment(vm.argexcludeddate).format('YYYY-MMM-DD');
                        vm.biometricexcludedentry.employeeRefId = vm.argemployeerefid;
                    }

                });
            }
            init();
        }
    ]);
})();

