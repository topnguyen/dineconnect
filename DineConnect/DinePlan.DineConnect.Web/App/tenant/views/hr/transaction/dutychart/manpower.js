﻿(function () {
    appModule.controller('tenant.views.hr.transaction.dutychart.manpower', [
        '$scope', '$state', '$uibModal', 'abp.services.app.dutyChart',
         'appSession', 'abp.services.app.employeeDocumentInfo', 'abp.services.app.personalInformation',
        function ($scope, $state, $modal,  dutychartService,
             appSession, employeedocumentinfoService, personalinformationService) {
            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            
            vm.loading = true;
            vm.loadingCount = 0;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.tenantName = appSession.tenant.name;
            vm.defaultLocationRefId = appSession.location.id;

            $scope.minDate = moment().subtract(300, 'days');
            vm.maxDate = moment().add(10, 'd');
            vm.startdate = moment().add(1, 'd');
            vm.dateWiseStatusList = [];

            vm.getMaxDateInfo = function () {
                vm.loading = true;
                vm.dateWiseStatusList = [];
                vm.loadingCount++;
                dutychartService.getDutyChartMaximumDate({

                }).success(function (result) {
                    // Based On Settings - Open
                    vm.maxDate = moment(result.maxDate);

                    vm.maxDate =  moment().add(10, 'd');
                    if (vm.startdate > vm.maxDate)
                        vm.startdate = vm.maxDate;
                    vm.dateWiseStatusList = result.dateWiseStatusList;

                    var errorDayCloseFlag = false;
                    var noofDaysMaximumWithoutDayClosed = 2;
                    angular.forEach(vm.dateWiseStatusList, function (value, key) {
                        if (key < noofDaysMaximumWithoutDayClosed) {

                        }
                        else {
                            if (value.status != app.localize('DayClosed')) {
                                errorDayCloseFlag = true;
                                var errorDate = moment(value.dutychartDate).format('YYYY-MMM-DD ddd');
                                abp.notify.error(app.localize('PlsDayClose', errorDate));
                            }
                        }
                    });

                    if (!errorDayCloseFlag) {
                        if (vm.maxDate <= moment())
                            vm.maxDate = vm.startdate.add(10, 'd');
                    }
                    

                }).finally(function () {
                    $('input[name="chartDate"]').daterangepicker({
                        locale: {
                            format: 'YYYY-MMM-DD dddd'
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.startdate,
                        minDate: $scope.minDate,
                        maxDate: vm.maxDate
                    });
                    vm.loading = false;
                    vm.loadingCount--;
                });
            };

            vm.getMaxDateInfo();

            vm.refInchargeEmployeeList = [];
            vm.getInchargeList = function () {
                personalinformationService.getSimpleInchargeEmployeeDetails({}).success(function (result) {
                    vm.refInchargeEmployeeList = result.items;
                    vm.refInchargeEmployeeList.unshift({ id: 0, employeeCode: 0, employeeName: app.localize('All').toUpperCase() + ' ' + app.localize('Manager').toUpperCase() });
                    vm.inchargeFilter = 0;
                });
            }
            vm.getInchargeList();

            vm.refSupervisorEmployeeList = [];
            vm.getSupervisorList = function () {
                personalinformationService.getSimpleSupervisorEmployeeDetails({}).success(function (result) {
                    vm.refSupervisorEmployeeList = result.items;
                    vm.refSupervisorEmployeeList.unshift({ id: 0, employeeCode: 0, employeeName: ('All').toUpperCase() + ' ' + app.localize('Reporting').toUpperCase() + ' ' + app.localize('Supervisor').toUpperCase() });
                    vm.supervisorFilter = 0;
                });
            }
            vm.getSupervisorList();

            vm.selectedDate = moment().format("YYYY-MMM-DD");

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.DutyChart.Create'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.DutyChart.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.output = null;
            vm.skillWiseCount = null;

            vm.getAll = function () {
                //vm.loading = true;
                //dutychartService.getEmployeeConsolidatedInfo({
                //}).success(function (result) {
                //    vm.output = result;
                //    vm.employeecount = result.employeeCount;
                //    vm.skillWiseCount = result.skillWiseCount;
                //}).finally(function () {
                //    vm.loading = false;
                //});
            };

            vm.fillNewEmployees = function (argDate) {
                vm.loading = true;
                dutychartService.includeAllActiveEmployees({
                    locationRefId : vm.defaultLocationRefId
                }).success(function (result) {
                    vm.createDutyChart(argDate);
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.createDutyChart = function (argDate) {
                var selDate = moment(argDate, 'YYYY-MMM-DD dddd');
                var last5days = moment().add(-10, 'days');

                if (selDate > last5days) {
                    if (vm.hrErrorExists == true) {
                        abp.notify.error(app.localize('HrOperationErrorExists'));
                        abp.message.error(app.localize('HrOperationErrorExists'));
                    }
                }
                openCreateOrEditModal(null, argDate);
            };

            function openCreateOrEditModal(objId, argDate) {
                $state.go("tenant.dutychartdetail", {
                    id: objId,
                    dutychartdate: argDate,
                    hrErrorExists: vm.hrErrorExists
                });
            }

            vm.getAll();
            vm.hrErrorExists = false;
            vm.hrOperationHealthText = '';
            vm.getHrOperationHealthList = function () {
                vm.loading = true;
                vm.hrOperationHealthText = '';
                dutychartService.hrOperationHealthEdp({
                    daysTill: 30
                }).success(function (result) {
                    vm.hrOperationHealthText = result.htmlMessage;
                    vm.loading = false;
                    if (vm.hrOperationHealthText != '' && vm.hrOperationHealthText != null) {
                        document.getElementById("hrOperationError").innerHTML = vm.hrOperationHealthText;
                        vm.hrErrorExists = true;
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.getHrOperationHealthList();

            vm.hrAlertExists = false;
            vm.hrAlertText = '';
            vm.getHrAlertList = function () {
                vm.loading = true;
                vm.hrAlertText = '';
                dutychartService.hrOperationAlert({
                    daysTill: 10
                }).success(function (result) {
                    vm.hrAlertText = result.htmlMessage;
                    vm.loading = false;
                    if (vm.hrAlertText != '' && vm.hrAlertText != null) {
                        document.getElementById("hrAlert").innerHTML = vm.hrAlertText;
                        vm.hrAlertExists = true;
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.getHrAlertList();

            //vm.hrOfficeJobAlertExists = false;
            //vm.hrOfficeJobAlertText = '';
            //vm.getHrOfficeJobAlertList = function () {
            //    vm.loading = true;
            //    vm.hrAlertText = '';
            //    dutychartService.hrOfficeJobAlert({
            //        daysTill: 10
            //    }).success(function (result) {
            //        vm.hrOfficeJobAlertText = result.htmlMessage;
            //        vm.loading = false;
            //        if (vm.hrOfficeJobAlertText != '' && vm.hrOfficeJobAlertText != null) {
            //            document.getElementById("hrOfficeJobAlert").innerHTML = vm.hrOfficeJobAlertText;
            //            vm.hrOfficeJobAlertExists = true;
            //        }
            //    }).finally(function () {
            //        vm.loading = false;
            //    });
            //}

            //vm.getHrOfficeJobAlertList();

            vm.refresh = function () {
                vm.getHrOperationHealthList();
                vm.getHrAlertList();
                //vm.getHrOfficeJobAlertList();
            }

            vm.directDownload = function (argData) {
                vm.importpath = abp.appPath + 'PersonalInformation/ReportDownload?fileName=' + argData.fileName + "&fileExtenstionType=" + argData.fileExtenstionType;
                window.open(vm.importpath);
            }

            vm.dayWiseDutyChartReport = function (argDate, exportType) {
                vm.loading = true;
                dutychartService.getExcelOfDutyChartForGivenDate({
                    dutyChartDate: vm.selectedDate,
                    tenantName: vm.tenantName,
                    exportType: exportType
                }).success(function (result) {
                    vm.directDownload(result);
                    vm.loading = false;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.daywiseConsolidateDutyChart = function (argDate, exportType) {                
                vm.loading = true;
                var superVisortEmployeeRefId = null;
                var inchargeEmployeeRefId = null;
                if (vm.supervisorFilter != 0 && vm.supervisorFilter > 0) {
                    superVisortEmployeeRefId = vm.supervisorFilter;
                }

                if (vm.inchargeFilter != 0 && vm.inchargeFilter > 0) {
                    inchargeEmployeeRefId = vm.inchargeFilter;
                }

                dutychartService.getConsolidatedExcelOfDutyChartForGivenDate({
                    dutyChartDate: vm.selectedDate,
                    tenantName: vm.tenantName,
                    exportType: exportType,
                    superVisortEmployeeRefId: superVisortEmployeeRefId,
                    inchargeEmployeeRefId: inchargeEmployeeRefId
                }).success(function (result) {
                    vm.directDownload(result);
                    vm.loading = false;
                }).finally(function () {
                    vm.loading = false;
                });
            }


            vm.maildayWiseDutyChartReport = function (argDate, exportType) {
                vm.loading = true;
                dutychartService.sendEmailDutyChartSlip({
                    dutyChartDate: vm.selectedDate,
                    tenantName: vm.tenantName,
                    exportType: exportType
                }).success(function (result) {
                    if (result.successFlag) {
                        abp.notify.info(result.successMessage);
                    }
                    else {
                        abp.notify.warn(result.errorMessage);
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            //  Employee Docuemnt Info
            vm.employeeDocumentInfos = [];
            vm.existEmployeeRefId = null;
            vm.getAllEmployeeDocumentInfo = function () {
                vm.loading = true;
                vm.loadingCount++;
                var startDate = null;
                var endDate = null;

                employeedocumentinfoService.getAll({
                    //employeeRefId: vm.currentEmployeeRefId,
                    documentInfoRefId: null,
                    startDate: startDate,
                    endDate: endDate,
                    alertPendingFlag: true,
                    alertExpiredFlag: true,
                    activeEmployeeOnly: true,
                    sorting: 'documentInfoRefId'
                }).success(function (result) {
                    vm.employeeDocumentInfos = result.items;
                    vm.existEmployeeRefId = vm.currentEmployeeRefId;
                }).finally(function () {
                    vm.loading = false;
                    vm.loadingCount--;
                });
            };
            vm.getAllEmployeeDocumentInfo();

            //-----------------------------

        }]);
})();

