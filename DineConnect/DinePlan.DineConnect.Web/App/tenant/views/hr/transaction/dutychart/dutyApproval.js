﻿(function () {
    appModule.controller('tenant.views.hr.transaction.dutychart.dutyApproval', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.dutyChart', 'uiGridConstants', 'appSession', 'abp.services.app.employeeDocumentInfo', 'abp.services.app.personalInformation',
        function ($scope, $state, $stateParams, $modal, dutychartService, uiGridConstants, appSession,
            employeedocumentinfoService, personalinformationService) {

            /* eslint-disable */
            var vm = this;
            vm.loading = true;
            vm.saving = false;
            vm.dutyChartMaster = null;
            vm.dutychartdetail = null;
            $scope.existall = true;
            vm.defaultLocationRefId = appSession.location.id;
            vm.loadingCount = 0;
            vm.advancedFiltersAreShown = true;
            vm.sno = 0;
            vm.dutychartId = $stateParams.id;
            vm.dutyChartDate = $stateParams.dutychartdate;
            vm.displayDate = moment(vm.dutyChartDate).format('YYYY-MMM-DD ddd');
            vm.currentUserId = abp.session.userId;
            vm.companyWiseadvancedFiltersAreShown = true;

            if ($stateParams.hrErrorExists == "true" || $stateParams.hrErrorExists == true)
                vm.hrErrorExists = true;
            else
                vm.hrErrorExists = false;

            vm.changesMade = false;
            vm.dayClosedAllowFlag = false;
            vm.legend = true;
            vm.verifyFlag = false;

            vm.filterFlagStatuses = [];
            vm.filterFlagStatuses.unshift({ value: "ShowAll", displayText: app.localize('ShowAll') });
            vm.filterFlagStatuses.unshift({ value: "Billable", displayText: app.localize('Billable') });
            vm.filterFlagStatuses.unshift({ value: "NotBillable", displayText: app.localize('NotBillable') });
            vm.filterFlagStatus = 'ShowAll';

            vm.mailList = [];
            vm.eitherOr = true;

            //----------------------------------------------------------------------------------------------------------------------
            //          Date Function Start
            //----------------------------------------------------------------------------------------------------------------------
            //  Date Range Options
            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            //  Date Angular Js 
            $scope.today = function () {
                $scope.dt = moment(new Date()).format("DD-MMM-YYYY");
            };
            $scope.today();

            $scope.clear = function () {
                $scope.dt = null;
            };

            $scope.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            $scope.dateOptions = {
                dateDisabled: disabled,
                formatYear: 'yy',
                maxDate: new Date().setDate(new Date().getDate() + 7),
                minDate: new Date(),
                startingDay: 1,
                showWeeks: false
            };

            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                    mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }

            $scope.toggleMin = function () {
                $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
                $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
            };

            $scope.toggleMin();

            $scope.open1 = function () {
                $scope.popup1.opened = true;
            };

            $scope.open2 = function () {
                $scope.popup2.opened = true;
            };

            $scope.setDate = function (year, month, day) {
                $scope.dt = new Date(year, month, day);
            };

            $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];
            $scope.altInputFormats = ['M!/d!/yyyy'];

            $scope.popup1 = {
                opened: false
            };

            $scope.popup2 = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            $scope.events = [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];

            function getDayClass(data) {
                var date = data.date,
                    mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            }


            //----------------------------------------------------------------------------------------------------------------------
            //          Date Function
            //----------------------------------------------------------------------------------------------------------------------


            var requestParams = {
                skipCount: 0,
                maxResultCount: 1000, // app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.dutychartdetaildata = [];
            vm.tempdetaildata = [];
            vm.errorMessage = '';

            vm.billableCount = 0;
            vm.idleCount = 0;
            vm.otherJobCount = 0;
            vm.leaveCount = 0;
            vm.absentCount = 0;

            vm.managerWiseJobStatus = [];

            vm.showLockAndAlertEdp = function () {

                vm.checkLeaveConfirmation();

                vm.hrErrorExists = false;
                vm.hrOperationHealthText = '';
                vm.getHrOperationHealthList = function () {
                    vm.loadingCount++;
                    vm.hrOperationHealthText = '';

                    var inchargeEmployeeRefId = null;
                    if (vm.inchargeFilter != 0 && vm.inchargeFilter > 0) {
                        inchargeEmployeeRefId = vm.inchargeFilter;
                    }

                    dutychartService.hrOperationHealthEdp({
                        startDate: vm.dutyChartDate,
                        endDate: vm.dutyChartDate,
                        inchargeEmployeeRefId: inchargeEmployeeRefId
                    }).success(function (result) {
                        vm.hrOperationHealthText = result.htmlMessage;
                        if (vm.hrOperationHealthText != '' && vm.hrOperationHealthText != null) {
                            document.getElementById("hrOperationError").innerHTML = vm.hrOperationHealthText;
                            vm.hrErrorExists = true;
                        }
                    }).finally(function () {
                        vm.loadingCount--;
                    });
                }

                vm.getHrOperationHealthList();


                vm.hrAlertExists = false;
                vm.hrAlertText = '';
                vm.getHrAlertList = function () {
                    var inchargeEmployeeRefId = null;
                    if (vm.inchargeFilter != 0 && vm.inchargeFilter > 0) {
                        inchargeEmployeeRefId = vm.inchargeFilter;
                    }
                    vm.loadingCount++;
                    vm.hrAlertText = '';
                    dutychartService.hrOperationAlert({
                        startDate: vm.dutyChartDate,
                        endDate: vm.dutyChartDate,
                        inchargeEmployeeRefId: inchargeEmployeeRefId
                    }).success(function (result) {
                        vm.hrAlertText = result.htmlMessage;
                        if (vm.hrAlertText != '' && vm.hrAlertText != null) {
                            document.getElementById("hrAlert").innerHTML = vm.hrAlertText;
                            vm.hrAlertExists = true;
                        }
                    }).finally(function () {
                        vm.loadingCount--;
                    });
                }

                vm.getHrAlertList();

                //vm.hrOfficeJobAlertExists = false;
                //vm.hrOfficeJobAlertText = '';
                //vm.getHrOfficeJobAlertList = function () {
                //    var inchargeEmployeeRefId = null;
                //    if (vm.inchargeFilter != 0 && vm.inchargeFilter > 0) {
                //        inchargeEmployeeRefId = vm.inchargeFilter;
                //    }
                //    vm.hrAlertText = '';
                //    vm.loadingCount++;
                //    dutychartService.hrOfficeJobAlert({
                //        startDate: vm.dutyChartDate,
                //        endDate: vm.dutyChartDate,
                //        inchargeEmployeeRefId: inchargeEmployeeRefId
                //    }).success(function (result) {
                //        vm.hrOfficeJobAlertText = result.htmlMessage;
                //        if (vm.hrOfficeJobAlertText != '' && vm.hrOfficeJobAlertText != null) {
                //            document.getElementById("hrOfficeJobAlert").innerHTML = vm.hrOfficeJobAlertText;
                //            vm.hrOfficeJobAlertExists = true;
                //        }
                //    }).finally(function () {
                //        vm.loadingCount--;
                //    });
                //}

                //vm.getHrOfficeJobAlertList();

                vm.errorMessage = '';
                var i = 0;
                angular.forEach(nonFilteredData, function (mastervalue, key) {
                    angular.forEach(mastervalue.dutyDetails, function (value, key) {
                        if (value.allottedStatus == app.localize('Job') && (value.stationRefId == 0 || value.stationRefId == null || value.stationRefId == '' || value.shiftRefId == 0 || value.shiftRefId == null || value.shiftRefId == '')) {
                            if (vm.errorMessage.length == 0) {
                                vm.errorMessage = app.localize('JobButNotAssigedShift') + " <BR>";
                                abp.notify.info(app.localize('JobButNotAssigedShift'));
                            }

                            vm.errorMessage = vm.errorMessage + ' ' + value.employeeRefName + " <BR> ";

                            if (i < 4) {
                                abp.notify.warn(value.employeeRefName);
                                i++;
                            }
                        }
                    });
                });
                if (vm.errorMessage.length > 0) {
                    document.getElementById("p1errorMessage").innerHTML = vm.errorMessage;
                }


            };

            vm.dutyDashboard = function (data) {

                vm.billableCount = 0;
                vm.idleCount = 0;
                vm.otherJobCount = 0;
                vm.leaveCount = 0;
                vm.absentCount = 0;
                vm.dashBoardShowFlag = false;

                vm.managerWiseJobStatus = [];

                var arrtempStatusList = [];
                var arrInchargeList = [];

                angular.forEach(data, function (mastervalue, key) {
                    var billableFlag = false;

                    angular.forEach(mastervalue.dutyDetails, function (value, key) {
                        if (value.isBillable == true)
                            billableFlag = true;
                    });

                    var currentStatus = '';
                    if (billableFlag == true) {
                        vm.billableCount++;
                        currentStatus = app.localize('Billable');
                    }
                    else if (mastervalue.allottedStatus == app.localize('Idle')) {
                        vm.idleCount++;
                        currentStatus = app.localize('Idle');
                    }
                    else if (mastervalue.allottedStatus == app.localize('Absent')) {
                        vm.absentCount++;
                        currentStatus = app.localize('Absent');
                    }
                    else if (mastervalue.allottedStatus == app.localize('Leave')) {
                        vm.leaveCount++;
                        currentStatus = app.localize('Leave');
                    }
                    else {
                        vm.otherJobCount++;
                        currentStatus = app.localize('Other');
                    }
                    arrtempStatusList.push({
                        'inchargeEmployeeRefId': mastervalue.inchargeEmployeeRefId,
                        'inchargeEmployeeRefName': mastervalue.inchargeEmployeeRefName,
                        'status': currentStatus
                    });

                    var idx = arrInchargeList.indexOf(mastervalue.inchargeEmployeeRefId);
                    if (idx < 0)
                        arrInchargeList.push(mastervalue.inchargeEmployeeRefId);
                });

                var arrEmpList = [];

                angular.forEach(arrInchargeList, function (inchargeValue, key) {

                    arrEmpList = _.filter(arrtempStatusList, function (text) {
                        if (text == null)
                            return false;
                        return (text.inchargeEmployeeRefId && text.inchargeEmployeeRefId == inchargeValue);
                    });

                    //var arrEmpList = arrtempStatusList.filter(t => { return t.inchargeEmployeeRefId == inchargeValue });

                    var inchargeDataCount = [];

                    angular.forEach(arrEmpList, function (value, key) {
                        if (key == 0) {
                            inchargeDataCount = {
                                'inchargeEmployeeRefId': value.inchargeEmployeeRefId,
                                'inchargeEmployeeRefName': value.inchargeEmployeeRefName,
                                'billableCount': 0,
                                'idleCount': 0,
                                'leaveCount': 0,
                                'absentCount': 0,
                                'otherJobCount': 0
                            };
                        }

                        if (value.status == app.localize('Billable')) {
                            inchargeDataCount.billableCount++;
                        }
                        else if (value.status == app.localize('Idle')) {
                            inchargeDataCount.idleCount++;
                        }
                        else if (value.status == app.localize('Leave')) {
                            inchargeDataCount.leaveCount++;
                        }
                        else if (value.status == app.localize('Absent')) {
                            inchargeDataCount.absentCount++;
                        }
                        else if (value.status == app.localize('Other')) {
                            inchargeDataCount.otherJobCount++;
                        }
                    });

                    vm.managerWiseJobStatus.push(inchargeDataCount);

                });

                vm.totalCount = vm.leaveCount + vm.billableCount + vm.absentCount + vm.idleCount + vm.otherJobCount;

                if (vm.totalCount > 0) {

                    var billablePercentage = 0;
                    if (vm.totalCount > 0 && vm.billableCount > 0)
                        billablePercentage = parseFloat((vm.billableCount / vm.totalCount * 100).toFixed(2));

                    var idlePercentage = 0;
                    if (vm.totalCount > 0 && vm.idleCount > 0)
                        idlePercentage = parseFloat((vm.idleCount / vm.totalCount * 100).toFixed(2));

                    var otherJobPercentage = 0;
                    if (vm.totalCount > 0 && vm.otherJobCount > 0)
                        otherJobPercentage = parseFloat((vm.otherJobCount / vm.totalCount * 100).toFixed(2));

                    var leavePercentage = 0;
                    if (vm.totalCount > 0 && vm.leaveCount > 0)
                        leavePercentage = parseFloat((vm.leaveCount / vm.totalCount * 100).toFixed(2));

                    var absentPercentage = 0;
                    if (vm.totalCount > 0 && vm.absentCount > 0)
                        absentPercentage = parseFloat((vm.absentCount / vm.totalCount * 100).toFixed(2));

                    //var colors = ['green', 'purple', 'yellow', 'blue-chambray', 'red-flamingo'];
                    var colors = ['green', 'purple', 'yellow', 'black', 'red'];

                    Highcharts.chart('piecontainer', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: 0,
                            plotShadow: false
                        },
                        title: {
                            text: '', // 'Filter Criteria Based Job Status',
                            align: 'center',
                            verticalAlign: 'top',
                            y: 40
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                        },
                        plotOptions: {
                            pie: {
                                colors: colors,
                                dataLabels: {
                                    enabled: true,
                                    distance: -50,
                                    style: {
                                        fontWeight: 'bold',
                                        color: 'white'
                                    },
                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',

                                },
                                startAngle: -90,
                                endAngle: 90,
                                center: ['50%', '75%'],
                                size: '110%'
                            }
                        },
                        series: [{
                            type: 'pie',
                            colors: colors,
                            name: 'Job Status Share',
                            innerSize: '40%',
                            data: [
                                ['Billable', billablePercentage],
                                ['Other Job', otherJobPercentage],
                                ['Idle', idlePercentage],
                                ['Absent', absentPercentage],
                                ['Leave', leavePercentage]
                            ]
                        }]
                    });
                }

                vm.dashBoardShowFlag = true;
            }

            vm.companyWiseJobStatus = [];
            vm.companyWisebillableCount = 0;
            vm.companyWiseidleCount = 0;
            vm.companyWiseotherJobCount = 0;
            vm.companyWiseleaveCount = 0;
            vm.companyWiseabsentCount = 0;

            vm.companyWiseDashboard = function () {

                vm.companyWiseJobStatus = [];
                vm.companyWiseJobStatus = [];
                vm.companyWisebillableCount = 0;
                vm.companyWiseotherJobCount = 0;
                vm.companyWiseidleCount = 0;

                vm.companyWiseleaveCount = 0;
                vm.companyWiseabsentCount = 0;


                var arrtempStatusList = [];
                var arrCompanyList = [];

                angular.forEach(nonFilteredData, function (companyvalue, key) {
                    var billableFlag = false;

                    angular.forEach(companyvalue.dutyDetails, function (value, key) {
                        if (value.isBillable == true)
                            billableFlag = true;
                    });

                    var currentStatus = '';
                    if (billableFlag == true) {
                        vm.companyWisebillableCount++;
                        currentStatus = app.localize('Billable');
                    }
                    else if (companyvalue.allottedStatus == app.localize('Idle')) {
                        vm.companyWiseidleCount++;
                        currentStatus = app.localize('Idle');
                    }
                    else if (companyvalue.allottedStatus == app.localize('Absent')) {
                        vm.companyWiseabsentCount++;
                        currentStatus = app.localize('Absent');
                    }
                    else if (companyvalue.allottedStatus == app.localize('Leave')) {
                        vm.companyWiseleaveCount++;
                        currentStatus = app.localize('Leave');
                    }
                    else {
                        vm.companyWiseotherJobCount++;
                        currentStatus = app.localize('Other');
                    }
                    arrtempStatusList.push({
                        'companyRefId': companyvalue.companyRefId,
                        'companyRefCode': companyvalue.companyRefCode,
                        'status': currentStatus
                    });

                    var idx = arrCompanyList.indexOf(companyvalue.companyRefId);
                    if (idx < 0)
                        arrCompanyList.push(companyvalue.companyRefId);
                });

                var arrEmpList = [];

                angular.forEach(arrCompanyList, function (companyvalue, key) {

                    arrEmpList = _.filter(arrtempStatusList, function (text) {
                        if (text == null)
                            return false;
                        return (text.companyRefId && text.companyRefId == companyvalue);
                    });

                    //var arrEmpList = arrtempStatusList.filter(t => { return t.inchargeEmployeeRefId == inchargeValue });

                    var companyDataCount = [];

                    angular.forEach(arrEmpList, function (value, key) {
                        if (key == 0) {
                            companyDataCount = {
                                'companyRefId': value.companyRefId,
                                'companyRefCode': value.companyRefCode,
                                'billableCount': 0,
                                'idleCount': 0,
                                'leaveCount': 0,
                                'absentCount': 0,
                                'otherJobCount': 0,
                                'billablePercentage': 0,
                                'idlePercentage': 0,
                                'leavePercentage': 0,
                                'absentPercentage': 0,
                                'otherJobPercentage': 0
                            };
                        }

                        if (value.status == app.localize('Billable')) {
                            companyDataCount.billableCount++;
                        }
                        else if (value.status == app.localize('Idle')) {
                            companyDataCount.idleCount++;
                        }
                        else if (value.status == app.localize('Leave')) {
                            companyDataCount.leaveCount++;
                        }
                        else if (value.status == app.localize('Absent')) {
                            companyDataCount.absentCount++;
                        }
                        else if (value.status == app.localize('Other')) {
                            companyDataCount.otherJobCount++;
                        }
                    });

                    var temptotalCount = companyDataCount.billableCount + companyDataCount.idleCount + companyDataCount.leaveCount + companyDataCount.absentCount + companyDataCount.otherJobCount;

                    if (temptotalCount > 0 && companyDataCount.billableCount > 0)
                        companyDataCount.billablePercentage = parseFloat((companyDataCount.billableCount / temptotalCount * 100).toFixed(2));

                    if (temptotalCount > 0 && companyDataCount.idleCount > 0)
                        companyDataCount.idlePercentage = parseFloat((companyDataCount.idleCount / temptotalCount * 100).toFixed(2));

                    if (temptotalCount > 0 && companyDataCount.otherJobCount > 0)
                        companyDataCount.otherJobPercentage = parseFloat((companyDataCount.otherJobCount / temptotalCount * 100).toFixed(2));

                    if (temptotalCount > 0 && companyDataCount.leaveCount > 0)
                        companyDataCount.leavePercentage = parseFloat((companyDataCount.leaveCount / temptotalCount * 100).toFixed(2));

                    if (temptotalCount > 0 && companyDataCount.absentCount > 0)
                        companyDataCount.absentPercentage = parseFloat((companyDataCount.absentCount / temptotalCount * 100).toFixed(2));

                    vm.companyWiseJobStatus.push(companyDataCount);

                });

                vm.companyWisetotalCount = vm.companyWiseleaveCount + vm.companyWisebillableCount + vm.companyWiseabsentCount + vm.companyWiseidleCount + vm.companyWiseotherJobCount;

                if (vm.companyWisetotalCount > 0) {

                    vm.billablePercentage = 0;
                    if (vm.companyWisetotalCount > 0 && vm.companyWisebillableCount > 0)
                        vm.billablePercentage = parseFloat((vm.companyWisebillableCount / vm.companyWisetotalCount * 100).toFixed(2));

                    vm.idlePercentage = 0;
                    if (vm.companyWisetotalCount > 0 && vm.companyWiseidleCount > 0)
                        vm.idlePercentage = parseFloat((vm.companyWiseidleCount / vm.companyWisetotalCount * 100).toFixed(2));

                    vm.otherJobPercentage = 0;
                    if (vm.companyWisetotalCount > 0 && vm.companyWiseotherJobCount > 0)
                        vm.otherJobPercentage = parseFloat((vm.companyWiseotherJobCount / vm.companyWisetotalCount * 100).toFixed(2));

                    vm.leavePercentage = 0;
                    if (vm.companyWisetotalCount > 0 && vm.companyWiseleaveCount > 0)
                        vm.leavePercentage = parseFloat((vm.companyWiseleaveCount / vm.companyWisetotalCount * 100).toFixed(2));

                    vm.absentPercentage = 0;
                    if (vm.companyWisetotalCount > 0 && vm.companyWiseabsentCount > 0)
                        vm.absentPercentage = parseFloat((vm.companyWiseabsentCount / vm.companyWisetotalCount * 100).toFixed(2));

                    var colors = ['green', 'purple', 'yellow', 'black', 'red'];

                    //Highcharts.chart('companypiecontainer', {
                    //    chart: {
                    //        plotBackgroundColor: null,
                    //        plotBorderWidth: 0,
                    //        plotShadow: false
                    //    },
                    //    title: {
                    //        text: 'Company Wise Status ' + vm.displayDate,
                    //        align: 'center',
                    //        verticalAlign: 'top',
                    //        y: 40
                    //    },
                    //    tooltip: {
                    //        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    //    },
                    //    plotOptions: {
                    //        pie: {
                    //            colors: colors,
                    //            dataLabels: {
                    //                enabled: true,
                    //                distance: -50,
                    //                style: {
                    //                    fontWeight: 'bold',
                    //                    color: 'white'
                    //                },
                    //                format: '<b>{point.name}</b>: {point.percentage:.1f} %',

                    //            },
                    //            startAngle: -90,
                    //            endAngle: 90,
                    //            center: ['50%', '75%'],
                    //            size: '110%'
                    //        }
                    //    },
                    //    series: [{
                    //        type: 'pie',
                    //        colors: colors,
                    //        name: 'OverAll Job Status Share',
                    //        innerSize: '40%',
                    //        data: [
                    //            ['Billable', vm.billablePercentage],
                    //            ['Other Job', vm.otherJobPercentage],
                    //            ['Idle', vm.idlePercentage],                              
                    //            ['Leave', vm.leavePercentage],
                    //            ['Absent', vm.absentPercentage],
                    //        ]
                    //    }]
                    //});
                }
            }

            vm.save = function (argOption) {
                if ($scope.existall == false)
                    return;
                vm.saving = true;
                vm.dutychartdetaildata = [];
                vm.errorMessage = '';
                var i = 0;
                angular.forEach(nonFilteredData, function (mastervalue, key) {

                    angular.forEach(mastervalue.dutyDetails, function (value, key) {
                        if (value.allottedStatus == app.localize('Job') && (value.stationRefId == 0 || value.stationRefId == null || value.stationRefId == '' || value.shiftRefId == 0 || value.shiftRefId == null || value.shiftRefId == '')) {
                            if (vm.errorMessage.length == 0) {
                                vm.errorMessage = app.localize('JobButNotAssigedShift') + " <BR>";
                                abp.notify.info(app.localize('JobButNotAssigedShift'));
                            }

                            vm.errorMessage = vm.errorMessage + '  ' + value.employeeRefName + " <BR> ";

                            if (i < 4) {
                                abp.notify.warn(value.employeeRefName);
                                i++;
                            }
                        }
                        vm.dutychartdetaildata.push({
                            'userSerialNumber': value.userSerialNumber,
                            'dutyChartRefId': value.dutyChartRefId,
                            'sno': value.sno,
                            'dutyChartDate': value.dutyChartDate,
                            'companyRefId': value.companyRefId,
                            'companyRefCode': value.companyRefCode,
                            'employeeRefId': value.employeeRefId,
                            'employeeRefCode': value.employeeRefCode,
                            'employeeRefName': value.employeeRefName,
                            'allottedStatus': value.allottedStatus,
                            'locationRefId': value.locationRefId,
                            'locationRefName': value.locationRefName,
                            'dcHeadRefId': value.dcHeadRefId,
                            'dcHeadRefName': value.dcHeadRefName,
                            'dcGroupRefId': value.dcGroupRefId,
                            'dcGroupRefName': value.dcGroupRefName,
                            'stationRefId': value.stationRefId,
                            'stationRefName': value.stationRefName,
                            'shiftRefId': value.shiftRefId,
                            'shiftRefName': value.shiftRefName,
                            'timeDescription': value.timeDescription,
                            'timeIn': value.timeIn,
                            'timeOut': value.timeOut,
                            'nightFlag': value.nightFlag,
                            'id': value.id,
                            'remarks': value.remarks,
                            'skillRefId': value.skillRefId,
                            'skillRefCode': value.skillRefCode,
                            'isBillable': value.isBillable,
                            'halfDayFlag': value.halfDayFlag,
                            'supervisorEmployeeRefId': value.supervisorEmployeeRefId,
                            'inchargeEmployeeRefId': value.inchargeEmployeeRefId,
                            'supervisorEmployeeRefName': value.supervisorEmployeeRefName,
                            'inchargeEmployeeRefName': value.inchargeEmployeeRefName,
                        });
                    });

                });

                if (vm.errorMessage.length > 0) {
                    document.getElementById("p1errorMessage").innerHTML = vm.errorMessage;
                    // return;
                }

                vm.savestatus = "";

                if (argOption == 1) {
                    vm.dutyChartMaster.completedTime = null;
                    if (vm.savestatus == "")
                        vm.savestatus = app.localize('Draft');
                }

                vm.dutyChartMaster.completedTime = new Date();
                if (vm.savestatus == "")
                    vm.savestatus = app.localize('Finished');

                if (vm.errorMessage.length == 0) {
                    vm.savestatus = app.localize('Finished');
                }
                vm.dutyChartMaster.approvedPersonId = vm.currentUserId;

                if (vm.errorMessage.length == 0) {
                    vm.showLockAndAlertEdp();
                    vm.loading = false;
                    vm.saving = false;
                    //vm.dutyChartMaster.id = result.id;
                    vm.verifyFlag = true;
                }
                else {
                    abp.notify.info(app.localize('DutyChart') + ' ' + app.localize('SavedSuccessfully'));
                    vm.loading = false;
                    vm.saving = false;
                    vm.cancel();
                    return;
                }

                //dutychartService.createOrUpdateDutyChart({
                //    dutyChart: vm.dutyChartMaster,
                //    dutyChartDetail: vm.dutychartdetaildata,
                //    saveStatus: vm.savestatus
                //}).success(function (result) {
                //    if (argOption == 1) {
                //        abp.notify.info(app.localize('DutyChart') + ' ' + app.localize('SavedSuccessfully'));
                //    }
                //    else {

                //        if (vm.errorMessage.length == 0) {
                //            vm.showLockAndAlertEdp();
                //            vm.dutyChartMaster.id = result.id;
                //            vm.verifyFlag = true;
                //            //vm.SendStatusMailToAllEmployee(false);
                //        }
                //        else {
                //            abp.notify.info(app.localize('DutyChart') + ' ' + app.localize('SavedSuccessfully'));
                //            vm.saving = false;
                //            vm.cancel();
                //            return;
                //        }
                //    }
                //}).finally(function () {
                //    vm.saving = false;
                //});

            };

            vm.existall = function () {
                if (vm.dutyChartMaster.Id == null) {
                    vm.existall = false;
                    return;
                }
            };

            vm.cancel = function () {
                $state.go("tenant.manpower");
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.requestParams = {
                locations: '',
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.tempdetaildata = [];

            vm.refInchargeEmployeeList = [];
            vm.getInchargeList = function () {
                personalinformationService.getSimpleInchargeEmployeeDetails({}).success(function (result) {
                    vm.refInchargeEmployeeList = result.items;
                    vm.refInchargeEmployeeList.unshift({ id: 0, employeeCode: 0, employeeName: app.localize('All').toUpperCase() + ' ' + app.localize('Manager').toUpperCase() });
                    vm.inchargeFilter = 0;
                });
            }
            vm.getInchargeList();

            vm.refSupervisorEmployeeList = [];
            vm.getSupervisorList = function () {
                personalinformationService.getSimpleSupervisorEmployeeDetails({}).success(function (result) {
                    vm.refSupervisorEmployeeList = result.items;
                    vm.refSupervisorEmployeeList.unshift({ id: 0, employeeCode: 0, employeeName: ('All').toUpperCase() + ' ' + app.localize('Reporting').toUpperCase() + ' ' + app.localize('Supervisor').toUpperCase() });
                    vm.supervisorFilter = 0;
                });
            }
            vm.getSupervisorList();

            vm.exportToExcel = function () {
                dutychartService.getAllToExcel({
                    dutyChartDate: vm.dutyChartDate
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.tsexportToExcel = function () {
                dutychartService.clientTsToExcel({
                    dutyChartDate: vm.dutyChartDate
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.invoicetsexportToExcel = function () {
                dutychartService.invoiceClientTsToExcel({
                    dutyChartDate: vm.dutyChartDate
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            //  Include Edition in Grid Details

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            var nonFilteredData = [];
            vm.filterText = $stateParams.filterText || '';
            vm.companyFilterText = '';
            vm.skillFilterText = '';
            vm.workStatusFilterText = '';
            vm.smartFilterText = '';

            vm.getLocationValue = function (item) {
                return parseInt(item.value);
            };

            vm.editEntry = function (item) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/transaction/dutychart/editDcRequest.cshtml',
                    controller: 'tenant.views.hr.transaction.dutychart.editDcRequest as vm',
                    resolve: {
                        allItems: function () {
                            return vm.gridOptions.data;
                        },
                        initialText: function () {
                            return item;
                        },
                        masterData: function () {
                            return vm.dutyChartMaster;
                        },
                        publicHolidayFlag: function () {
                            return vm.publicHolidayFlag;
                        },
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.changesMade = result;

                }).finally(function () {
                    if (vm.changesMade) {
                        init();
                    }
                });

            };

            vm.advancesearch = true;


            vm.clearFilter = function () {
                vm.supervisorFilter = 0;
                vm.inchargeFilter = 0;
                vm.filterText = $stateParams.filterText || '';
                vm.companyFilterText = '';
                vm.skillFilterText = '';
                vm.workStatusFilterText = '';
                vm.smartFilterText = '';
                vm.filterFlagStatus = 'ShowAll';
                vm.applyAllFilters();
            }

            vm.applyAllFilters = function () {
                if (vm.supervisorFilter == 0 && vm.inchargeFilter == 0 && !vm.filterText && !vm.companyFilterText && !vm.skillFilterText && !vm.workStatusFilterText && !vm.smartFilterText && vm.filterFlagStatus == 'ShowAll') {
                    vm.gridOptions.data = nonFilteredData;
                    vm.dutyDashboard(vm.gridOptions.data);
                    return;
                }
                toBeFilteredData = nonFilteredData;

                //  vm.filterFlagStatus
                vm.gridOptions.data = nonFilteredData;
                if (vm.filterFlagStatus != 'ShowAll') {
                    var filterText = true;
                    if (vm.filterFlagStatus == 'NotBillable') {
                        filterText = false;
                    }
                    vm.gridOptions.data = _.filter(toBeFilteredData, function (text) {
                        if (text == null)
                            return false;
                        if (vm.targetValueFilter == 'EMPTY' && text.targetValue) {
                            return false;
                        }
                        return (text.defaultBillableFlag == filterText);
                    });
                    toBeFilteredData = vm.gridOptions.data;
                }

                //  Company Filter

                if (vm.companyFilterText.length > 0) {
                    var filterText = vm.companyFilterText.trim().toLocaleLowerCase();
                    vm.gridOptions.data = _.filter(toBeFilteredData, function (text) {
                        if (text == null)
                            return false;
                        if (vm.targetValueFilter == 'EMPTY' && text.targetValue) {
                            return false;
                        }
                        return (text.companyRefCode && text.companyRefCode.toLocaleLowerCase().indexOf(filterText) >= 0);
                    });
                    toBeFilteredData = vm.gridOptions.data;
                }

                //  Skill Filter

                if (vm.skillFilterText.length > 0) {
                    var filterText = vm.skillFilterText.trim().toLocaleLowerCase();
                    vm.gridOptions.data = _.filter(toBeFilteredData, function (text) {
                        if (text == null)
                            return false;
                        if (vm.targetValueFilter == 'EMPTY' && text.targetValue) {
                            return false;
                        }
                        return (text.skillRefCode && text.skillRefCode.toLocaleLowerCase().indexOf(filterText) >= 0);
                    });
                    toBeFilteredData = vm.gridOptions.data;
                }

                //  Employee Filter

                if (vm.filterText.length > 0) {
                    var filterText = vm.filterText.trim().toLocaleLowerCase();
                    vm.gridOptions.data = _.filter(toBeFilteredData, function (text) {
                        if (text == null)
                            return false;
                        return (text.employeeRefName && text.employeeRefName.toLocaleLowerCase().indexOf(filterText) >= 0);
                    });
                    toBeFilteredData = vm.gridOptions.data;
                }

                //  Incharge Filter
                if (vm.eitherOr == false) {
                    if (vm.inchargeFilter != null && vm.inchargeFilter > 0) {
                        var filterText = vm.inchargeFilter;
                        vm.gridOptions.data = _.filter(toBeFilteredData, function (text) {
                            if (text == null)
                                return false;
                            return (text.inchargeEmployeeRefId && text.inchargeEmployeeRefId == filterText);
                        });
                        toBeFilteredData = vm.gridOptions.data;
                    }

                    //  Supervisor Filter
                    if (vm.supervisorFilter != null && vm.supervisorFilter > 0) {
                        var filterText = vm.supervisorFilter;
                        vm.gridOptions.data = _.filter(toBeFilteredData, function (text) {
                            if (text == null)
                                return false;
                            return (text.supervisorEmployeeRefId && text.supervisorEmployeeRefId == filterText);
                        });
                        toBeFilteredData = vm.gridOptions.data;
                    }
                }
                else {
                    if (vm.inchargeFilter > 0 || vm.supervisorFilter > 0) {
                        var supervisorIncharge = vm.supervisorFilter;
                        var managerIncharge = vm.inchargeFilter;
                        vm.gridOptions.data = _.filter(toBeFilteredData, function (text) {
                            if (text == null)
                                return false;
                            return (text.inchargeEmployeeRefId == managerIncharge || text.supervisorEmployeeRefId == supervisorIncharge);
                        });
                        toBeFilteredData = vm.gridOptions.data;
                    }
                }


                //  Work Status Filter

                if (vm.workStatusFilterText.length > 0) {
                    var filterText = vm.workStatusFilterText.trim().toLocaleLowerCase();
                    vm.gridOptions.data = _.filter(toBeFilteredData, function (text) {
                        if (text == null)
                            return false;
                        if (vm.targetValueFilter == 'EMPTY' && text.targetValue) {
                            return false;
                        }
                        return (text.allottedStatus && text.allottedStatus.toLocaleLowerCase().indexOf(filterText) >= 0);
                    });
                    toBeFilteredData = vm.gridOptions.data;
                }

                if (vm.smartFilterText.length > 0) {
                    var filterText = vm.smartFilterText.trim().toLocaleLowerCase();

                    vm.gridOptions.data = _.filter(toBeFilteredData, function (text) {
                        if (vm.targetValueFilter == 'EMPTY' && text.targetValue) {
                            return false;
                        }

                        var searchstring = text.employeeRefName + text.skillRefCode + text.locationRemarks + text.allottedStatus + text.timeDescription + text.companyRefCode;

                        if (vm.advancesearch == true) {
                            return (searchstring && searchstring.toLocaleLowerCase().indexOf(filterText) >= 0);
                        }
                        else {
                            return (text.employeeRefName && text.employeeRefName.toLocaleLowerCase().indexOf(filterText) >= 0);
                        }
                    });
                }

                vm.dutyDashboard(vm.gridOptions.data);

            };



            vm.leaveConfirmationFlag = false;
            vm.leaveFinishedGivenDateList = [];
            vm.checkLeaveConfirmation = function () {
                if (vm.dutyChartMaster == null)
                    return;
                vm.loading = true;
                dutychartService.checkLeaveContinuation({
                    leaveToDate: vm.dutyChartMaster.dutyChartDate
                }).success(function (result) {
                    vm.leaveFinishedGivenDateList = result;
                    if (result.length == 0)
                        vm.leaveConfirmationFlag = true;

                    vm.loading = false;
                });
            };

            //vm.checkLeaveConfirmation();

            vm.dutyChartDayClosed = function (dutyChartId) {
                if (vm.supervisorFilter != 0 && vm.supervisorFilter > 0) {
                    abp.notify.error(app.localize('Supervisor') + ' ' + app.localize('FilterShouldBeEmpty'));
                    abp.message.error(app.localize('Supervisor') + ' ' + app.localize('FilterShouldBeEmpty'));
                    abp.notify.error(app.localize('DayCloseDenied'));
                    $("#uisupervisor").focus();
                    return;
                }

                if (vm.inchargeFilter != 0 && vm.inchargeFilter > 0) {
                    abp.notify.error(app.localize('Incharge') + ' ' + app.localize('FilterShouldBeEmpty'));
                    abp.message.error(app.localize('Incharge') + ' ' + app.localize('FilterShouldBeEmpty'));
                    abp.notify.error(app.localize('DayCloseDenied'));
                    $("#uiincharge").focus();
                    return;
                }

                if (vm.hrErrorExists == true) {
                    abp.notify.error(app.localize('HrOperationErrorExists'));
                    abp.message.error(app.localize('HrOperationErrorExists'));
                    abp.notify.error(app.localize('DayCloseDenied'));
                    return;
                }

                if (vm.hrAlertExists == true) {
                    abp.notify.error(app.localize('HrIdleErrorExists'));
                    abp.message.error(app.localize('HrIdleErrorExists'));
                    abp.notify.error(app.localize('DayCloseDenied'));
                    return;
                }

                if (vm.hrOfficeJobAlertExists) {
                    abp.notify.error(app.localize('HrOfficeJobErrorExists'));
                    abp.message.error(app.localize('HrOfficeJobErrorExists'));
                    abp.notify.error(app.localize('DayCloseDenied'));
                    return;
                }


                if (vm.leaveConfirmationFlag == false) {
                    abp.notify.error(app.localize('PlsConfirmLeaveContinuation'));
                    abp.message.error(app.localize('PlsConfirmLeaveContinuation'));
                    abp.notify.error(app.localize('DayCloseDenied'));
                    return;
                }

                vm.loading = true;
                //var dayCloseDate = moment(vm.dutyChartMaster.dutyChartDate).subtract()
                dutychartService.verifyDayClose({
                    dateId: vm.dutyChartMaster.dutyChartDate,
                    noOfDays: 7
                }).success(function (result) {
                    vm.loading = false;
                    if (result.successFlag == false) {
                        angular.forEach(result.errorMessageList, function (errorvalue, key) {
                            abp.notify.error(errorvalue);
                        });
                        return;
                    }
                    else {
                        //vm.SendStatusMailToAllEmployee(true);

                        dutychartService.dutyChartDayClose({
                            id: vm.dutyChartMaster.id,
                            locationRefId : vm.defaultLocationRefId
                        }).success(function (result) {
                            abp.notify.info(moment(result.dateId).format('YYYY-MMM-DD') + ' ' + app.localize('DutyChartClosedSuccessfully'));
                            abp.message.info(moment(result.dateId).format('YYYY-MMM-DD') + ' ' + app.localize('DutyChartClosedSuccessfully'));
                            vm.cancel();
                        });
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.legendFill = function () {
                vm.gridOptions = {
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: 10000, //app.consts.grid.defaultPageSize,
                    appScopeProvider: vm,
                    columnDefs: [
                        {
                            name: app.localize('Edit'),
                            width: 50,
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button ng-click="grid.appScope.editEntry(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>' +
                                '</div>'
                        },
                        //{
                        //    name: app.localize('Company'),
                        //    field: 'companyRefCode',
                        //    width: 60
                        //},
                        {
                            name: app.localize('Location'),
                            field: 'locationRefName',
                            width: 60
                        },
                        {
                            name: app.localize('employeeRefName'),
                            field: 'employeeRefName',
                            minWidth: 350,
                            cellClass: function (grid, row) {
                                if (vm.legend) {
                                    if (row.entity.allottedStatus == app.localize('Job'))
                                        return 'ui-billable';
                                    if (row.entity.allottedStatus == app.localize('Idle'))
                                        return 'ui-idle';
                                    if (row.entity.allottedStatus == app.localize('Leave'))
                                        return 'ui-leave';
                                    if (row.entity.allottedStatus == app.localize('Absent'))
                                        return 'ui-absent';
                                }
                            }
                        },
                        {
                            name: app.localize('SkillRefCode'),
                            field: 'multipleSkillRemarks',
                            maxWidth: 120,
                            cellClass: function (grid, row) {
                                if (vm.legend) {
                                    if (row.entity.allottedStatus == app.localize('Job'))
                                        return 'ui-billable';
                                    if (row.entity.allottedStatus == app.localize('Idle'))
                                        return 'ui-idle';
                                    if (row.entity.allottedStatus == app.localize('Leave'))
                                        return 'ui-leave';
                                    if (row.entity.allottedStatus == app.localize('Absent'))
                                        return 'ui-absent';
                                }
                            }
                        },
                        {
                            name: app.localize('WorkStatus'),
                            field: 'allottedStatus',
                            width: 80,
                            cellClass: function (grid, row) {
                                if (vm.legend) {
                                    if (row.entity.allottedStatus == app.localize('Job'))
                                        return 'ui-billable';
                                    if (row.entity.allottedStatus == app.localize('Idle'))
                                        return 'ui-idle';
                                    if (row.entity.allottedStatus == app.localize('Leave'))
                                        return 'ui-leave';
                                    if (row.entity.allottedStatus == app.localize('Absent'))
                                        return 'ui-absent';
                                }
                            }
                        },
                        {
                            name: app.localize('Station'),
                            field: 'stationRefName',
                            cellClass: function (grid, row) {
                                if (vm.legend) {
                                    if (row.entity.allottedStatus == app.localize('Job'))
                                        return 'ui-billable';
                                    if (row.entity.allottedStatus == app.localize('Idle'))
                                        return 'ui-idle';
                                    if (row.entity.allottedStatus == app.localize('Leave'))
                                        return 'ui-leave';
                                    if (row.entity.allottedStatus == app.localize('Absent'))
                                        return 'ui-absent';
                                }
                            }
                        },
                        {
                            name: app.localize('Shift'),
                            field: 'shiftRefName',
                            cellClass: function (grid, row) {
                                if (vm.legend) {
                                    if (row.entity.allottedStatus == app.localize('Job'))
                                        return 'ui-billable';
                                    if (row.entity.allottedStatus == app.localize('Idle'))
                                        return 'ui-idle';
                                    if (row.entity.allottedStatus == app.localize('Leave'))
                                        return 'ui-leave';
                                    if (row.entity.allottedStatus == app.localize('Absent'))
                                        return 'ui-absent';
                                }
                            }
                        },
                        //{
                        //    name: app.localize('Billable'),
                        //    field: 'isBillable',
                        //    width: 60,
                        //    cellTemplate:
                        //        '<div class=\"ui-grid-cell-contents\">' +
                        //        '  <span ng-show="row.entity.isBillable" class="label label-success">' + app.localize('Yes') + '</span>' +
                        //        '  <span ng-show="!row.entity.isBillable" class="label label-default">' + app.localize('No') + '</span>' +
                        //        '</div>',
                        //    cellClass: function (grid, row) {
                        //        if (vm.legend) {
                        //            if (row.entity.isBillable == true)
                        //                return 'ui-billable';
                        //            if (row.entity.allottedStatus == app.localize('Idle'))
                        //                return 'ui-idle';
                        //            if (row.entity.allottedStatus == app.localize('Leave'))
                        //                return 'ui-leave';
                        //            if (row.entity.allottedStatus == app.localize('Absent'))
                        //                return 'ui-absent';
                        //        }
                        //    }
                        //},
                        //{
                        //    name: app.localize('Location'),
                        //    field: 'locationRemarks',
                        //    cellClass: function (grid, row) {
                        //        if (vm.legend) {
                        //            if (row.entity.isBillable == true)
                        //                return 'ui-billable';
                        //            if (row.entity.allottedStatus == app.localize('Idle'))
                        //                return 'ui-idle';
                        //            if (row.entity.allottedStatus == app.localize('Leave'))
                        //                return 'ui-leave';
                        //            if (row.entity.allottedStatus == app.localize('Absent'))
                        //                return 'ui-absent';
                        //        }
                        //    }
                        //},
                        {
                            name: app.localize('Supervisor') + "/" + app.localize('Manager'),
                            field: 'inchargeEmployeeRefId',
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents\">' +
                                '  <span class= "label label-danger" ng-if="row.entity.supervisorEmployeeRefId!=row.entity.inchargeEmployeeRefId">{{row.entity.supervisorEmployeeRefName}} </span> &nbsp;   ' +
                                '  <span class= "label label-primary" >&nbsp; {{row.entity.inchargeEmployeeRefName}}</span>' +
                                '</div>', /*class= "label label-primary"*/
                            cellClass: function (grid, row) {
                                if (vm.legend) {
                                    if (row.entity.allottedStatus == app.localize('Job'))
                                        return 'ui-billable';
                                    if (row.entity.allottedStatus == app.localize('Idle'))
                                        return 'ui-idle';
                                    if (row.entity.allottedStatus == app.localize('Leave'))
                                        return 'ui-leave';
                                    if (row.entity.allottedStatus == app.localize('Absent'))
                                        return 'ui-absent';
                                }
                            }
                        },
                        {
                            name: app.localize('timeDescription'),
                            field: 'multipleTimeDescriptionRemarks',
                            minWidth: 80,
                            cellClass: function (grid, row) {
                                if (vm.legend) {
                                    if (row.entity.allottedStatus == app.localize('Job'))
                                        return 'ui-billable';
                                    if (row.entity.allottedStatus == app.localize('Idle'))
                                        return 'ui-idle';
                                    if (row.entity.allottedStatus == app.localize('Leave'))
                                        return 'ui-leave';
                                    if (row.entity.allottedStatus == app.localize('Absent'))
                                        return 'ui-absent';
                                }
                            }
                        },
                        {
                            name: app.localize('AttendanceExcluded'),
                            width: 80,
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button ng-click="grid.appScope.manualAttendance(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('AttendanceExcluded') + '"><i class="fa fa-hand-pointer-o"></i></button>' +
                                '</div>'
                        },
                    ],
                    data: []
                };
                vm.applyAllFilters();
            }

            vm.legendFill();


            vm.SendStatusMailToAllEmployee = function (mailFlag) {
                if (vm.hrErrorExists == true) {
                    abp.notify.error(app.localize('HrOperationErrorExists'));
                    abp.message.error(app.localize('HrOperationErrorExists'));
                    abp.notify.error(app.localize('DayCloseDenied'));
                    return;
                }

                if (vm.hrAlertExists == true) {
                    abp.notify.error(app.localize('HrIdleErrorExists'));
                    abp.notify.error(app.localize('DayCloseDenied'));
                    return;
                }

                if (vm.hrOfficeJobAlertExists) {
                    abp.notify.error(app.localize('HrOfficeJobErrorExists'));
                    abp.notify.error(app.localize('DayCloseDenied'));
                    return;
                }

                //vm.loading = true;
                //dutychartService.sendDayStatusEmailToAllEmployee(
                //    {
                //        dutyChartDate: vm.dutyChartDate,
                //        jobStatus: 'Job',
                //        mailSendFlag: mailFlag
                //    }).success(function (result) {
                //        vm.mailList = result;
                //    }).finally(function () {
                //        vm.loading = false;
                //    });
            };





            vm.manualAttendance = function (data) {
                var dt = moment(data.dutyChartDate);

                openManualAttendanceModal(data.employeeRefId, dt);
            };

            function openManualAttendanceModal(employeeRefId, executiondate) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/transaction/biometricexcludedentry/bioMetricExcludedEntry.cshtml',
                    controller: 'tenant.views.hr.transaction.biometricexcludedentry.bioMetricExcludedEntry as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        biometricexcludedentryId: function () {
                            return null;
                        },
                        employeeRefId: function () {
                            return employeeRefId;
                        },
                        excludeddate: function () {
                            return executiondate;
                        }
                    }
                });

                modalInstance.result.then(function (result) {

                    if (result = true)
                        vm.showLockAndAlertEdp();
                });
            };


            // Excel Export 
            vm.daywiseConsolidateDutyChart = function (argDate, exportType) {
                vm.loading = true;
                var superVisortEmployeeRefId = null;
                if (vm.supervisorFilter != 0 && vm.supervisorFilter > 0) {
                    superVisortEmployeeRefId = vm.supervisorFilter;
                }

                var inchargeEmployeeRefId = null;
                if (vm.inchargeFilter != 0 && vm.inchargeFilter > 0) {
                    inchargeEmployeeRefId = vm.inchargeFilter;
                }

                dutychartService.getConsolidatedExcelOfDutyChartForGivenDate({
                    dutyChartDate: vm.dutyChartDate,
                    tenantName: vm.tenantName,
                    exportType: exportType,
                    superVisortEmployeeRefId: superVisortEmployeeRefId,
                    inchargeEmployeeRefId: inchargeEmployeeRefId
                }).success(function (result) {
                    vm.directDownload(result);
                    vm.loading = false;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.directDownload = function (argData) {
                vm.importpath = abp.appPath + 'PersonalInformation/ReportDownload?fileName=' + argData.fileName + "&fileExtenstionType=" + argData.fileExtenstionType;
                window.open(vm.importpath);
            }

            vm.filterDatabasedOnManager = function (data, argOption) {
                if (argOption == 'total') {
                    vm.inchargeFilter = data.inchargeEmployeeRefId;
                }
                else if (argOption == 'leave') {

                }
            }

            vm.publicHolidayFlag = false;
            function init() {
                vm.loading = true;
                dutychartService.getDutyChartForEdit({
                    Id: vm.dutychartId,
                    locationRefId: vm.defaultLocationRefId,
                    dutyChartDate: vm.dutyChartDate
                }).success(function (result) {

                    vm.dutyChartMaster = result.dutyChart;
                    vm.publicHolidayFlag = result.dutyChart.publicHolidayFlag;

                    if (vm.dutyChartMaster.status == app.localize('Finished'))
                        vm.dayClosedAllowFlag = true;

                    if (vm.dutyChartMaster.dutyChartDate == "0001-01-01T00:00:00") {
                        $scope.dt = new Date();
                    }
                    else {
                        $scope.dt = new Date(vm.dutyChartMaster.dutyChartDate);
                    }

                    if (result.dutyChartDetail.length >= 0) {
                        vm.tempdetaildata = [];
                        vm.dutychartdetail = result.dutyChartDetail;
                        vm.sno = 0;

                        angular.forEach(result.dutyChartDetail, function (value, key) {
                            vm.tempdetaildata.push({
                                'dutyChartRefId': value.dutyChartRefId,
                                'dutyChartDate': value.dutyChartDate,
                                'companyRefId': value.companyRefId,
                                'companyRefCode': value.companyRefCode,
                                'userSerialNumber': value.userSerialNumber,
                                'employeeRefId': value.employeeRefId,
                                'employeeRefCode': value.employeeRefCode,
                                'employeeRefName': value.employeeRefName,
                                'allottedStatus': value.allottedStatus,
                                'locationRefId': value.locationRefId,
                                'locationRefName': value.locationRefName,
                                'dcHeadRefId': value.dcHeadRefId,
                                'dcHeadRefName': value.dcHeadRefName,
                                'dcGroupRefId': value.dcGroupRefId,
                                'dcGroupRefName': value.dcGroupRefName,
                                'stationRefId': value.stationRefId,
                                'stationRefName': value.stationRefName,
                                'shiftRefId': value.shiftRefId,
                                'shiftRefName': value.shiftRefName,
                                'timeDescription': value.timeDescription,
                                'timeIn': value.timeIn,
                                'timeOut': value.timeOut,
                                'nightFlag': value.nightFlag,
                                'remarks': value.remarks,
                                'completedStatus': value.completedStatus,
                                'skillRefId': value.skillRefId,
                                'skillRefCode': value.skillRefCode,
                                'id': value.id,
                                'profilePictureId': value.profilePictureId,
                                'numberOfLocations': value.numberOfLocations,
                                'locationRemarks': value.locationRemarks,
                                'requestRemarks': value.requestRemarks,
                                'multipleSkillRemarks': value.multipleSkillRemarks,
                                'multipleTimeDescriptionRemarks': value.multipleTimeDescriptionRemarks,
                                'dutyDetails': value.dutyDetails,
                                'skills': value.skills,
                                'isSetAsDefault': false,
                                'halfDayFlag': value.halfDayFlag,
                                'supervisorEmployeeRefId': value.supervisorEmployeeRefId,
                                'inchargeEmployeeRefId': value.inchargeEmployeeRefId,
                                'supervisorEmployeeRefName': value.supervisorEmployeeRefName,
                                'inchargeEmployeeRefName': value.inchargeEmployeeRefName,
                            });
                        });

                        nonFilteredData = vm.tempdetaildata;
                        vm.companyWiseDashboard();
                        vm.applyAllFilters();
                        vm.showLockAndAlertEdp();
                    }
                    else {

                    }
                }).finally(function () {
                    vm.saving = false;
                    vm.loading = false;
                });;
            }

            //  Employee Docuemnt Info
            vm.employeeDocumentInfos = [];
            vm.existEmployeeRefId = null;
            vm.getAllEmployeeDocumentInfo = function () {
                vm.loading = true;
                vm.loadingCount++;
                var startDate = null;
                var endDate = null;

                employeedocumentinfoService.getAll({
                    //employeeRefId: vm.currentEmployeeRefId,
                    documentInfoRefId: null,
                    startDate: startDate,
                    endDate: endDate,
                    alertPendingFlag: true,
                    alertExpiredFlag: true,
                    activeEmployeeOnly: true,
                    sorting: 'documentInfoRefId'
                }).success(function (result) {
                    vm.employeeDocumentInfos = result.items;
                    vm.existEmployeeRefId = vm.currentEmployeeRefId;
                }).finally(function () {
                    vm.loading = false;
                    vm.loadingCount--;
                });
            };


            //-----------------------------

            init();
            

        }
    ]);
})();



