﻿(function () {
    appModule.controller('tenant.views.hr.transaction.dutychart.salaryCost', [
        '$scope', '$state', '$uibModal', 'abp.services.app.dutyChart',
        'abp.services.app.timeSheetMaster', 'appSession', 'abp.services.app.personalInformation',
        function ($scope, $state, $modal,  dutychartService,
            timesheetmasterService, appSession, personalinformationService) {
            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.costCentreWiseadvancedFiltersAreShown = true;
            vm.loading = true;
            vm.loadingCount = 0;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.tenantName = appSession.tenant.name;
            vm.locationRefId = appSession.location.id;


            $scope.minDate = moment().subtract(300, 'days');
            vm.maxDate = moment().add(2, 'd');
            vm.startdate = moment().add(1, 'd');
            vm.dateWiseStatusList = [];

            vm.getMaxDateInfo = function () {
                vm.loading = true;
                vm.dateWiseStatusList = [];
                vm.loadingCount++;
                dutychartService.getDutyChartMaximumDate({

                }).success(function (result) {
                    vm.maxDate = moment(result.maxDate);
                    if (vm.startdate > vm.maxDate)
                        vm.startdate = vm.maxDate;
                    vm.dateWiseStatusList = result.dateWiseStatusList;

                    var errorDayCloseFlag = false;
                    var noofDaysMaximumWithoutDayClosed = 2;
                    angular.forEach(vm.dateWiseStatusList, function (value, key) {
                        if (key < noofDaysMaximumWithoutDayClosed) {

                        }
                        else {
                            if (value.status != app.localize('DayClosed')) {
                                errorDayCloseFlag = true;
                                var errorDate = moment(value.dutychartDate).format('YYYY-MMM-DD ddd');
                                abp.notify.error(app.localize('PlsDayClose', errorDate));
                            }
                        }
                    });

                    if (!errorDayCloseFlag) {
                        if (vm.maxDate <= moment())
                            vm.maxDate = vm.startdate.add(1, 'd');
                        if (vm.startdate > vm.maxDate)
                            vm.startdate = vm.maxDate;
                    }
                    

                }).finally(function () {
                    $('input[name="chartDate"]').daterangepicker({
                        locale: {
                            format: 'YYYY-MMM-DD dddd'
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        startDate: vm.startdate,
                        minDate: $scope.minDate,
                        maxDate: vm.maxDate
                    });
                    vm.loading = false;
                    vm.loadingCount--;
                });
            };

            vm.getMaxDateInfo();

            vm.refInchargeEmployeeList = [];
            vm.getInchargeList = function () {
                personalinformationService.getSimpleInchargeEmployeeDetails({}).success(function (result) {
                    vm.refInchargeEmployeeList = result.items;
                    vm.refInchargeEmployeeList.unshift({ id: 0, employeeCode: 0, employeeName: app.localize('All').toUpperCase() + ' ' + app.localize('Manager').toUpperCase() });
                    vm.inchargeFilter = 0;
                });
            }
            vm.getInchargeList();

            vm.refSupervisorEmployeeList = [];
            vm.getSupervisorList = function () {
                personalinformationService.getSimpleSupervisorEmployeeDetails({}).success(function (result) {
                    vm.refSupervisorEmployeeList = result.items;
                    vm.refSupervisorEmployeeList.unshift({ id: 0, employeeCode: 0, employeeName: ('All').toUpperCase() + ' ' + app.localize('Reporting').toUpperCase() + ' ' + app.localize('Supervisor').toUpperCase() });
                    vm.supervisorFilter = 0;
                });
            }
            vm.getSupervisorList();

            vm.selectedDate = moment().format("YYYY-MMM-DD");

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.DutyChart.Create'),
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.output = null;
            vm.skillWiseCount = null;

            vm.salaryCost = null;
            vm.getEmployeeCost = function (argDate) {
                vm.loading = true;
                vm.salaryCost = null;
                vm.dashBoardShowFlag = false;
                vm.idlePercentage = 0;
                vm.advancedFiltersAreShown = false;
                vm.costCentreWiseadvancedFiltersAreShown = false;
                timesheetmasterService.getParticularDaySalary({
                    dateId: argDate,
                    supervisorFilter: vm.supervisorFilter,
                    inchargeFilter: vm.inchargeFilter,
                    locationRefId : vm.locationRefId
                }).success(function (result) {
                    vm.salaryCost = result;
                    if (vm.salaryCost.idleSalaryAmount == 0 || vm.salaryCost.totalPayable == 0) {
                        vm.idlePercentage = 0;
                    }
                    else {
                        vm.idlePercentage = vm.salaryCost.idleSalaryAmount / vm.salaryCost.totalPayable * 100;
                    }

                    vm.advancedFiltersAreShown = true;
                    vm.costCentreWiseadvancedFiltersAreShown = true;
                    vm.dashBoardShowFlag = true;

                    Highcharts.chart('pieGraph', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie',
                            //options3d: {
                            //	enabled: true,
                            //	alpha: 45
                            //}
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{point.percentage:.1f} %'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.y} <br/>{point.percentage:.1f} %',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            name: app.localize('JobStatus'),
                            colorByPoint: true,
                            data: $.parseJSON(JSON.stringify(result.costCentrePieGraph))
                        }]
                    });

                    Highcharts.chart('pieInchargeGraph', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie',
                            //options3d: {
                            //	enabled: true,
                            //	alpha: 45
                            //}
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{point.percentage:.1f} %'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.y} <br/>{point.percentage:.1f} %',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            name: app.localize('CostCentre') ,
                            colorByPoint: true,
                            data: $.parseJSON(JSON.stringify(result.inchargePieGraph))
                        }]
                    });

                    Highcharts.setOptions({
                        colors: ['#CB2326', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#CB2326', '#6AF9C4', '#333']
                    });

                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.dayWiseDutyChartReport = function (argDate, exportType) {
                vm.loading = true;
                dutychartService.getExcelOfDutyChartForGivenDate({
                    dutyChartDate: vm.selectedDate,
                    tenantName: vm.tenantName,
                    exportType: exportType
                }).success(function (result) {
                    vm.directDownload(result);
                    vm.loading = false;
                }).finally(function () {
                    vm.loading = false;
                });
            };


            var salaryModalInstance = null;

            vm.showSalaryModal = function (argSalaryPayable) {
                vm.loading = true;

                salaryModalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/salaryinfo/salaryModal.cshtml',
                    controller: 'tenant.views.hr.master.salaryinfo.salaryModal as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        salaryPayable: function () {
                            return argSalaryPayable;
                        },
                        employeeSkillSetList: function () {
                            return vm.employeeSkillSetList;
                        }
                    }
                });

                vm.loading = false;

                salaryModalInstance.result.then(function (result) {
                    vm.loading = false;
                });
            };
            //

        }]);
})();

