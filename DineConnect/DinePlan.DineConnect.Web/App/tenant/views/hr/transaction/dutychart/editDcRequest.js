﻿(function () {
    appModule.controller('tenant.views.hr.transaction.dutychart.editDcRequest', [
        '$scope', '$uibModalInstance', 'allItems', 'initialText',
        'abp.services.app.dutyChart', 'abp.services.app.personalInformation', 'masterData', 'publicHolidayFlag', 'abp.services.app.dcHeadMaster', 'abp.services.app.dcGroupMaster', 'abp.services.app.dcStationMaster', 'abp.services.app.dcShiftMaster',
        function ($scope, $modalInstance, allItems, initialText,
            dutychartService, personalinformationService, masterData, publicHolidayFlag, dcheadmasterService, dcgroupmasterService, dcstationmasterService, dcshiftmasterService) {

            /* eslint-disable */
            var vm = this;
            vm.loading = false;
            vm.dutyeditflag = false;
            vm.dutyChartMaster = masterData;
            vm.currentUserId = abp.session.userId;
            vm.lastdata = [];
            vm.changesMade = false;
            vm.publicHolidayFlag = publicHolidayFlag;
            vm.lockFlag = true;

            vm.allItems = allItems;
            vm.currentText = initialText;
            vm.editingText = angular.extend({}, vm.currentText);


            //var a = vm.editingText.dutyDetails[0];
            //vm.editingText.dutyDetails.push(a);

            //  Date Setup 
            //$scope.minDate = moment().add(1,'days');
            //$scope.maxDate = moment().add(60,'days');

            //$scope.formats = ['DD-MMM-YY ddd', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            //$scope.format = $scope.formats[0];

            //date start
            //  Date Range Options
            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            //  Date Angular Js 
            $scope.today = function () {
                $scope.dt = moment(new Date()).format("DD-MMM-YYYY");

            };
            $scope.today();

            $scope.clear = function () {
                $scope.dt = null;
            };

            $scope.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            $scope.dateOptions = {
                dateDisabled: disabled,
                formatYear: 'yy',
                maxDate: new Date(2020, 5, 22),
                minDate: new Date(),
                startingDay: 1,
                showWeeks: false,
            };

            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                    mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }

            $scope.toggleMin = function () {
                $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
                $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
            };

            $scope.toggleMin();

            $scope.open1 = function () {
                $scope.popup1.opened = true;
            };

            $scope.open2 = function () {
                $scope.popup2.opened = true;
            };

            $scope.open3 = function () {
                $scope.popup3.opened = true;
            };

            $scope.setDate = function (year, month, day) {
                $scope.dt = new Date(year, month, day);
            };

            $scope.formats = ['yyyy-MM-dd'];
            $scope.format = $scope.formats[0];
            //$scope.altInputFormats = ['M!/d!/yyyy'];
            $scope.altInputFormats = ['dd-MMMM-yyyy'];

            $scope.minDate = moment().add(-45, 'days');
            $scope.maxDate = moment().add(90, 'days');

            $('input[name="endDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: $scope.maxDate
            });


            $scope.popup1 = {
                opened: false
            };

            $scope.popup2 = {
                opened: false
            };

            $scope.popup3 = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            $scope.events = [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];

            function getDayClass(data) {
                var date = data.date,
                    mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            }

            //date end

            $('input[name="poDate"]').daterangepicker({
                locale: {
                    format: $scope.formats[1]
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment()
            });


            vm.setEmployeeSkill = function () {
                vm.employeeSkills = '';
                angular.forEach(vm.editingText.skills, function (value, key) {
                    vm.employeeSkills = vm.employeeSkills + value.skillCode + ","
                });
                if (vm.employeeSkills.length > 0)
                    vm.employeeSkills = vm.employeeSkills.substring(0, vm.employeeSkills.length - 1);
            }

            angular.copy(vm.editingText, vm.lastdata);
            vm.setEmployeeSkill();

            vm.currentIndex = 0;
            vm.saving = false;

            //Calculating index of currentText in allItems
            for (var i = 0; i < allItems.length; i++) {
                if (vm.allItems[i] == vm.currentText) {
                    vm.currentIndex = i;
                    break;
                }
            }


            //vm.fillDropDownDcGroupMaster = function (data) {
            //    vm.loadingCount++;
            //    dcgroupmasterService.getGroupNames({
            //        dcHeadRefId: data.dcHeadRefId
            //    }).success(function (result) {
            //        vm.refdcgroupmaster = result.items;
            //    }).finally(function () {
            //        vm.loadingCount--;
            //        vm.fillDropDownDcStationMaster(data);
            //    });
            //}
            vm.changeDuty = function (data) {
                vm.fillDropDownDcGroupMasterBasedOnSkill(data, false);
            }

            vm.refdcgroupmaster = [];

            vm.fillDropDownDcGroupMasterBasedOnSkill = function (data, argEditFlag) {
                vm.loadingCount++;
                dcgroupmasterService.getGroupNamesBasedOnSkill({
                    skillRefId: data.skillRefId
                }).success(function (result) {
                    vm.refdcgroupmaster = result.items;
                    if (argEditFlag) {
                        if (vm.refdcgroupmaster.length == 1) {
                            data.dcGroupRefId = vm.refdcgroupmaster[0].id;
                            data.dcGroupRefName = vm.refdcgroupmaster[0].groupName;
                        }
                        else {
                            data.dcGroupRefId = null;
                            data.dcGroupRefName = '';
                        }
                    }
                }).finally(function () {
                    vm.loadingCount--;
                    vm.fillDropDownDcStationMaster(data, argEditFlag);
                });
            }



            vm.refdcstationmaster = [];
            vm.fillDropDownDcStationMaster = function (data, argEditFlag) {
                vm.loadingCount++;
                dcstationmasterService.getStationNames({
                    locationRefId: data.locationRefId,
                    dcGroupRefId: data.dcGroupRefId
                }).success(function (result) {
                    vm.refdcstationmaster = result.items;
                    if (argEditFlag == true) {
                        if (vm.refdcstationmaster.length == 1) {
                            data.stationRefId = vm.refdcstationmaster[0].id;
                            data.stationRefName = vm.refdcstationmaster[0].stationName;
                        }
                        else {
                            data.stationRefId = null;
                            data.stationRefName = '';
                        }
                    }
                }).finally(function () {
                    vm.loadingCount--;
                    vm.fillDropDownDcShiftMaster(data, argEditFlag);
                });
            }

            vm.refdcshiftmaster = [];
            vm.fillDropDownDcShiftMaster = function (data, argEditFlag) {
                vm.loadingCount++;
                dcshiftmasterService.getShiftNames({
                    locationRefId: data.locationRefId,
                    stationRefId: data.stationRefId
                }).success(function (result) {
                    vm.refdcshiftmaster = result.items;
                    if (argEditFlag == true) {
                        if (vm.refdcshiftmaster.length == 1) {
                            data.shiftRefId = vm.refdcshiftmaster[0].id;
                            data.shiftRefName = vm.refdcshiftmaster[0].dutyDesc;
                            data.timeDescription = vm.refdcshiftmaster[0].timeDescription;
                        }
                        else {
                            data.shiftRefId = null;
                            data.shiftRefName = '';
                            data.timeDescription = '';
                        }
                    }
                }).finally(function () {
                    vm.loadingCount--;
                });
            }

            vm.getShiftValue = function (item,data) {
                data.shiftRefId = item.id;
                data.shiftRefName = item.dutyDesc;
                data.timeDescription = item.timeDescription;
            };



            function save(close) {
                function executeAfterAction() {
                    if (close) {
                        //                        $modalInstance.close();
                    } else {
                        //Go to next
                        vm.currentIndex++;
                        if (vm.allItems.length <= vm.currentIndex) {
                            $modalInstance.close(vm.changesMade);
                            return;
                        }

                        vm.currentText = vm.allItems[vm.currentIndex];
                        vm.editingText = angular.extend({}, vm.currentText);
                        angular.copy(vm.editingText, vm.lastdata);
                        vm.setEmployeeSkill();
                    }
                }


                if (vm.existDetailElements(vm.editingText) == false)
                    return;

                $('input[name="poDate"]').daterangepicker({
                    locale: {
                        format: $scope.format
                    },
                    singleDatePicker: true,
                    showDropdowns: true,
                    startDate: moment(),
                    minDate: $scope.minDate,
                    maxDate: $scope.maxDate.format($scope.format)
                });



                if (JSON.stringify(vm.lastdata) === JSON.stringify(vm.editingText)) {
                    executeAfterAction();
                    return;
                }

                if (angular.equals(vm.lastdata.dutyDetails, vm.editingText.dutyDetails) &&
                    angular.equals(vm.lastdata, vm.editingText)) {
                    executeAfterAction();
                    return;
                    vm.loading = false;
                }

                vm.editingText.isBillable = vm.editingText.dutyDetails[0].isBillable;

                vm.currentText = vm.editingText;

                vm.save(vm.editingText, close);

                vm.reftimedescription = [];
                vm.pushCustomTimeIds();

                executeAfterAction();
            };

            vm.existDetailElements = function (dataToVerify) {
                var datadetails = dataToVerify.dutyDetails;
                if (datadetails.length == 0) {
                    abp.notify.warn(app.localize('RecordNotFound'));
                    return false;
                }

                vm.existflag = true;
                angular.forEach(datadetails, function (value, key) {

                    if (value.allottedStatus == null || value.allottedStatus == '') {
                        abp.notify.warn(app.localize('JobStatusErr'));
                        vm.existflag = false;
                        return false;
                    }

                    if (app.localize(value.allottedStatus) == app.localize('Job')) {
                        if (value.stationRefId == null || value.stationRefId == 0 || value.stationRefId == '') {
                            abp.notify.warn(app.localize('Station') + ' ' + ' ?');
                            vm.existflag = false;
                            return false;
                        }

                        if (value.shiftRefId == null || value.shiftRefId == 0 || value.shiftRefId == '') {
                            abp.notify.warn(app.localize('Shift') + ' ' + ' ?');
                            vm.existflag = false;
                            return false;
                        }
                        if (vm.checkSkillSet(value) == false) {
                            vm.existflag = false;
                            abp.notify.warn(app.localize('SkillCodeErr'));
                            return false;
                        }
                    }
                    else {
                        if (value.stationRefId != null && value.stationRefId != '') {
                            abp.notify.warn(app.localize(value.allottedStatus) + ' ' + app.localize('StationNotRequiredErr'));
                            vm.existflag = false;
                            vm.changeallottedStatus(value);
                            return false;
                        }
                    }

                    if (value.allottedStatus == app.localize('Job') && (value.timeDescription == null || value.timeDescription == '')) {
                        abp.notify.warn(app.localize('TimeRequiredErr'));
                        vm.existflag = false;
                        return false;
                    }



                });


                if (vm.existflag == false)
                    return false;
                else
                    return true;
            }

            vm.saveAndNext = function () {
                save(false);
            }

            vm.saveAndClose = function () {
                save(true);
            }

            vm.previous = function () {
                if (vm.currentIndex <= 0) {
                    return;
                }

                vm.currentIndex--;
                vm.currentText = vm.allItems[vm.currentIndex];
                vm.editingText = angular.extend({}, vm.currentText);
                vm.setEmployeeSkill();
            };


            vm.addPortion = function (data) {
                if (vm.existDetailElements(vm.editingText) == false)
                    return;

                var prev = data[data.length - 1];
                var usno = data.length + 1;
                data.push({
                    'id': 0,
                    'userSerialNumber': usno,
                    'dutyChartRefId': prev.dutyChartRefId,
                    'sno': prev.sno,
                    'dutyChartDate': prev.dutyChartDate,
                    'companyRefId': prev.companyRefId,
                    'companyRefCode': prev.companyRefCode,
                    'employeeRefId': prev.employeeRefId,
                    'employeeRefCode': prev.employeeRefCode,
                    'employeeRefName': prev.employeeRefName,
                    'allottedStatus': app.localize('Job'),
                    'locationRefId': value.locationRefId,
                    'locationRefName': value.locationRefName,
                    'dcHeadRefId': value.dcHeadRefId,
                    'dcHeadRefName': value.dcHeadRefName,
                    'dcGroupRefId': value.dcGroupRefId,
                    'dcGroupRefName': value.dcGroupRefName,
                    'stationRefId': value.stationRefId,
                    'stationRefName': value.stationRefName,
                    'shiftRefId': value.shiftRefId,
                    'shiftRefName': value.shiftRefName,
                    'timeDescription': null,
                    'timeIn': null,
                    'timeOut': null,
                    'nightFlag': prev.nightFlag,
                    'remarks': prev.remarks,
                    'skillRefId': prev.skillRefId,
                    'skillRefCode': prev.skillSetRefCode,
                    'isSetAsDefault': false,
                    'isBillable': true,
                    'halfDayFlag': data.halfDayFlag,
                });
            }

            vm.removePortion = function (data, productIndex, idtobedeleted) {
                vm.loading = true;
                dutychartService.removeDutyChart({
                    dutyChartDate: vm.editingText.dutyChartDate,
                    employeeRefId: vm.editingText.employeeRefId,
                    id: idtobedeleted
                }).success(function (result) {
                    if (result == true) {
                        data.splice(productIndex, 1);
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            };


            vm.targetValueKeyPress = function ($event) {
                if ($event.keyCode == 13) {
                    vm.saveAndNext();
                } else if ($event.keyCode == 37) {
                    vm.previous();
                } else if ($event.keyCode == 39) {
                    vm.saveAndNext();
                }
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };


            vm.getComboValueString = function (item) {
                return item.value;
            };

            vm.refskillcode = [];
            function fillDropDownSkillCode() {
                personalinformationService.getSkillSetForCombobox({}).success(function (result) {
                    vm.refskillcode = result.items;
                });
            }

            vm.checkNightFlag = function (selectedTime, data) {
                data.nightFlag = selectedTime.nightFlag;
            }

            vm.refjobstatus = [];
            vm.refjobstatus.unshift({ value: "Job", displayText: app.localize('Job') });
            vm.refjobstatus.unshift({ value: "Idle", displayText: app.localize('Idle') });
            vm.refjobstatus.unshift({ value: "Leave", displayText: app.localize('Leave') });
            vm.refjobstatus.unshift({ value: "WeekOff", displayText: app.localize('WeekOff') });
            vm.refjobstatus.unshift({ value: "Absent", displayText: app.localize('Absent') });
            vm.refjobstatus.unshift({ value: "Course", displayText: app.localize('Course') });

            if (vm.publicHolidayFlag)
                vm.refjobstatus.unshift({ value: "PH", displayText: app.localize('PH') });


            vm.getTimeIds = function (item) {
                return parseInt(item.id);
            };

            $scope.getHours = function (mts) {
                var hrs = Math.floor(mts / 60);
                return hrs.toString();
            }

            $scope.getMinutes = function (mts) {
                var minutes = mts % 60;
                return minutes.toString();
            }

            function init() {
                fillDropDownSkillCode();
            }

            init();

            //  Save

            vm.save = function (data, close) {
                if ($scope.existall == false)
                    return;
                vm.changesMade = true;
                vm.saving = true;
                vm.loading = true;

                vm.dutychartdetaildata = [];

                var errorFlag = false;
                angular.forEach(data.dutyDetails, function (value, key) {
                    vm.dutychartdetaildata.push({
                        'id': value.id,
                        'userSerialNumber': value.userSerialNumber,
                        'dutyChartRefId': value.dutyChartRefId,
                        'sno': value.sno,
                        'dutyChartDate': value.dutyChartDate,
                        'companyRefId': value.companyRefId,
                        'companyRefCode': value.companyRefCode,
                        'employeeRefId': value.employeeRefId,
                        'employeeRefCode': value.employeeRefCode,
                        'employeeRefName': value.employeeRefName,
                        'allottedStatus': value.allottedStatus,
                        'locationRefId': value.locationRefId,
                        'locationRefName': value.locationRefName,
                        'dcHeadRefId': value.dcHeadRefId,
                        'dcHeadRefName': value.dcHeadRefName,
                        'dcGroupRefId': value.dcGroupRefId,
                        'dcGroupRefName': value.dcGroupRefName,
                        'stationRefId': value.stationRefId,
                        'stationRefName': value.stationRefName,
                        'shiftRefId': value.shiftRefId,
                        'shiftRefName': value.shiftRefName,
                        'timeDescription': value.timeDescription,
                        'timeIn': value.timeIn,
                        'timeOut': value.timeOut,
                        'nightFlag': value.nightFlag,
                        'remarks': value.remarks,
                        'skillRefId': value.skillRefId,
                        'skillRefCode': value.skillRefCode,
                        'isSetAsDefault': value.isSetAsDefault,
                        'isBillable': value.isBillable,
                        'halfDayFlag': data.halfDayFlag,
                    });
                });

                if (errorFlag) {
                    vm.dutychartdetaildata = [];
                    return;
                }

                vm.savestatus = "";

                vm.dutyChartMaster.completedTime = new Date();
                if (vm.savestatus == "")
                    vm.savestatus = "SAVE";

                vm.dutyChartMaster.approvedPersonId = vm.currentUserId;

                var employeeRefId = data.employeeRefId;
                var dutyChartDate = data.dutyChartDate;
                var skillSetRefCode = data.dutyDetails[0].skillRefCode;

                dutychartService.createOrUpdateDutyChart({
                    dutyChart: vm.dutyChartMaster,
                    dutyChartDetail: vm.dutychartdetaildata,
                    saveStatus: vm.savestatus
                }).success(function (result) {
                    abp.notify.info(data.employeeRefName + '  ' + app.localize('SavedSuccessfully'));
                }).finally(function () {
                    vm.saving = false;
                    vm.loading = false;
                    if (close) {
                        $modalInstance.close(vm.changesMade);
                    }
                });
            };

            vm.changeallottedStatus = function (data) {

                if (data.allottedStatus == app.localize('Job')) {
                    data.isBillable = true;
                }

                if (data.allottedStatus != app.localize('Job')) {
                    data.isBillable = false;
                    data.stationRefId = null;
                    data.shiftRefId = null;
                    data.timeDescription = "";
                    data.isSetAsDefault = false;
                };
            };

            vm.checkSkillSet = function (data) {
                var skillExistFlag = false;
                if (data.skillRefCode == '') {
                    return true;
                }
                angular.forEach(vm.editingText.skills, function (value, key) {
                    if (data.skillRefCode == value.skillCode) {
                        skillExistFlag = true;
                    }
                });

                if (skillExistFlag == false) {
                    abp.message.confirm(
                        app.localize('SelectedSkillNotMatchWithEmployeeSkillSet', vm.editingText.employeeRefName, data.skillRefCode),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                return true;
                            }
                            else {
                                data.skillRefCode = "";
                                return false;
                            }
                        }
                    );
                }
            };

            vm.refdcheadmaster = [];

            function fillDropDownDcHeadMaster(data) {
                vm.loadingCount++;
                dcheadmasterService.getHeadNames({}).success(function (result) {
                    vm.refdcheadmaster = result.items;
                }).finally(function () {
                    vm.loadingCount--;
                    vm.fillDropDownDcGroupMaster(data);
                });
            }
            fillDropDownDcHeadMaster(vm.editingText);


            vm.checkBilledSkillSet = function (data) {
                var skillExistFlag = false;
                if (data.skillRefCode == '') {
                    return true;
                }

                angular.forEach(vm.editingText.skills, function (value, key) {
                    if (data.skillRefCode == value.skillCode) {
                        skillExistFlag = true;
                    }
                });

                if (skillExistFlag == false) {
                    abp.message.confirm(
                        app.localize('SelectedSkillNotMatchWithEmployeeSkillSet', vm.editingText.employeeRefName, data.skillRefCode),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                //employeedefaultworkstationService.deleteEmployeeDefaultWorkStation({
                                //    id: myObject.id
                                //}).success(function () {
                                //    vm.getAll();
                                //    abp.notify.success(app.localize('SuccessfullyDeleted'));
                                //});
                                return true;
                            }
                            else {
                                data.skillRefCode = "";
                                return false;
                            }
                        }
                    );
                }
            };

        }
    ]);
})();