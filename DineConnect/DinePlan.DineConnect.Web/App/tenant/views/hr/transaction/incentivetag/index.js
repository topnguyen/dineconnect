﻿(function () {
    appModule.controller('tenant.views.hr.transaction.incentivetag.index', [
        '$scope', '$state', '$stateParams',  '$uibModal', 'uiGridConstants', 'abp.services.app.incentiveTag',
        function ($scope, $state, $stateParams, $modal, uiGridConstants, incentivetagService) {
            var vm = this;
                        /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.IncentiveTag.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.IncentiveTag.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.IncentiveTag.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

        vm.userGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
					{
						name: app.localize('Actions'),
						enableSorting: false,
						width: 120,
						cellTemplate:
						   "<div class=\"ui-grid-cell-contents\">" +
							   "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
							   "    <ul uib-dropdown-menu>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editIncentiveTag(row.entity)\">" + app.localize("Edit") + "</a></li>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteIncentiveTag(row.entity)\">" + app.localize("Delete") + "</a></li>" +
							   "    </ul>" +
							   "  </div>" +
							   "</div>"
                    },                    
                    {
                        name: app.localize('PayMode'),
                        field: 'allowanceOrDeductionRefId',
                        minWidth: 80,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.allowanceOrDeductionRefId==1" class="label label-success">' + app.localize('Allowance') + '</span>' +
                            '  <span ng-show="row.entity.allowanceOrDeductionRefId==2" class="label label-danger">' + app.localize('Deduction') + '</span>' +
                            '  <span ng-show="row.entity.allowanceOrDeductionRefId==3" class="label label-primary">' + app.localize('Stat') + ' ' + app.localize('Allow') + '</span>' +
                            '  <span ng-show="row.entity.allowanceOrDeductionRefId==4" class="label label-warning">' + app.localize('Stat') + ' ' + app.localize('Deduct') + '</span>' +
                            '</div>',
                    },
                    {
                        name: app.localize('Duration'),
                        field: 'incentivePayModeId',
                        minWidth: 80,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.incentivePayModeId==1" class="label label-danger">' + app.localize('Daily') + '</span>' +
                            '  <span ng-show="row.entity.incentivePayModeId==2" class="label label-info">' + app.localize('Monthly') + '</span>' +
                            '</div>',
                    },
                    {
                        name: app.localize('IncentiveTagCode'),
						field: 'incentiveTagCode',
						minWidth : 250
                    },
      //              {
      //                  name: app.localize('IncentiveTagName'),
						//field: 'incentiveTagName',
						//minWidth: 250
      //              },
                    {
                        name: app.localize('IncentiveEntryMode'),
                        field: 'incentiveEntryMode',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.incentiveEntryMode==0" class="label label-success">' + app.localize('Auto') + '</span>' +
                            '  <span ng-show="row.entity.incentiveEntryMode==1" class="label label-danger">' + app.localize('Manual') + '</span>' +
                            '  <span ng-show="row.entity.incentiveEntryMode==2" class="label label-danger">' + app.localize('Both') + '</span>' +
                            '</div>',
                    },
                    {
                        name: app.localize('HoursBased'),
						field: 'hoursBased',
						cellTemplate:
						'<div class=\"ui-grid-cell-contents\">' +
						'  <span ng-show="row.entity.hoursBased" class="label label-success">' + app.localize('Yes') + '</span>' +
						'  <span ng-show="!row.entity.hoursBased" class="label label-danger">' + app.localize('No') + '</span>' +
						'</div>',
                    },
					{
						name: app.localize('FullWorkHours'),
						field: 'fullWorkHours',
						cellTemplate:
						'<div class=\"ui-grid-cell-contents\">' +
						'  <span ng-show="row.entity.fullWorkHours" class="label label-success">' + app.localize('Yes') + '</span>' +
						'  <span ng-show="!row.entity.fullWorkHours" class="label label-danger">' + app.localize('No') + '</span>' +
						'</div>',
                    },
                    {
                        name: app.localize('CanBeAssignedToAllEmployees'),
                        field: 'canBeAssignedToAllEmployees',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.canBeAssignedToAllEmployees" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.canBeAssignedToAllEmployees" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                    },
     //               {
     //                   name: app.localize('MinimumHours'),
					//	field: 'minimumHours',
					//},
					//{
					//	name: app.localize('MinimumOtHours'),
					//	field: 'minimumOtHours'
					//},
     //               {
     //                   name: app.localize('IncludeInConsolidationPerDay'),
					//	field: 'includeInConsolidationPerDay',
					//	cellTemplate:
					//	'<div class=\"ui-grid-cell-contents\">' +
					//	'  <span ng-show="row.entity.includeInConsolidationPerDay" class="label label-success">' + app.localize('Yes') + '</span>' +
					//	'  <span ng-show="!row.entity.includeInConsolidationPerDay" class="label label-danger">' + app.localize('No') + '</span>' +
					//	'</div>',
     //               },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'LLL\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

         

            vm.getAll = function () {
                vm.loading = true;
                incentivetagService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editIncentiveTag = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createIncentiveTag = function () {
                openCreateOrEditModal(null);
            };

            vm.deleteIncentiveTag = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteIncentiveTagWarning', myObject.incentiveTagCode),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            incentivetagService.deleteIncentiveTag({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.incentivetagedit", {
                    id: objId,
                });
            }

            vm.exportToExcel = function () {
                incentivetagService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getAll();
        }]);
})();

