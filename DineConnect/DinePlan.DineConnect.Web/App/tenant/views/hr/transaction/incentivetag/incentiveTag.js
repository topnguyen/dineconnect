﻿(function () {
    appModule.controller('tenant.views.hr.transaction.incentivetag.incentiveTag', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.incentiveTag', 'abp.services.app.salaryInfo', 'abp.services.app.personalInformation', 'abp.services.app.material', 'abp.services.app.employeeDocumentInfo',
        function ($scope, $state, $stateParams, $modal, incentivetagService, salaryinfoService, personalinformationService, materialService, employeedocumentinfoService) {
            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            vm.incentivetag = null;
            $scope.existall = true;

            vm.incentivetagId = $stateParams.id;

            vm.permissions = {
                bulkUpdateForAllEligibleEmployee: abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.IncentiveTag.BulkUpdateForAllEligibleEmployee'),
            };

            vm.save = function () {
                if ($scope.existall === false)
                    return;

                if (vm.incentivetag.incentiveCategoryRefId == null || vm.incentivetag.incentiveCategoryRefId == 0) {
                    abp.notify.error(app.localize('Category'));
                    return;
                }

                vm.saving = true;

                // vm.incentivetag.incentiveTagCode = vm.incentivetag.incentiveTagCode.toUpperCase();

                vm.incentivetag.incentiveTagName = vm.incentivetag.incentiveTagName.toUpperCase();

                vm.incentiveVsEntityList = [];

                vm.errorFlag = false;
                angular.forEach(vm.detailPortions, function (value, key) {

                    if (vm.isUndefinedOrNull(value.entityType)) {
                        vm.errorFlag = true;
                        abp.notify.error(app.localize('Entity') + ' ' + app.localize('Required'));
                        return;
                    }


                    if (vm.isUndefinedOrNull(value.entityRefId) || value.entityRefId == 0 || value.entityRefId == null) {
                        vm.errorFlag = true;
                        abp.notify.error(app.localize('EntityReference') + '?' + "For" + data.entityType);
                        return;
                    }

                    var lastelement = value;
                    vm.incentiveVsEntityList.push({
                        'sno': value.sno,
                        'id': value.id,
                        'entityType': value.entityType,
                        'incentiveRefId': value.incentiveRefId,
                        'entityRefId': value.entityRefId,
                        'excludeFlag': value.excludeFlag,
                        'eitherOr_ForSameEntity': value.eitherOr_ForSameEntity
                    });
                });

                if (vm.errorflag == true) {
                    vm.saving = false;
                    return;
                }

                incentivetagService.createOrUpdateIncentiveTag({
                    incentiveTag: vm.incentivetag,
                    incentiveVsEntityList: vm.incentiveVsEntityList
                }).success(function () {
                    abp.notify.info(app.localize('IncentiveTag') + ' ' + app.localize('SavedSuccessfully'));
                    vm.cancel(0);
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {
                if (vm.incentivetag.incentiveTagCode == null) {
                    vm.existall = false;
                    return;
                }

            };

            vm.cancel = function (argOption) {
                if (argOption == 1) {
                    abp.message.confirm(
                        app.localize('CancelEntry?'),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                $state.go('tenant.incentivetag', {
                                    passwordRequired: false
                                });
                            }
                        }
                    );
                }
                else {
                    $state.go('tenant.incentivetag', {
                        passwordRequired: false
                    });
                }
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.employeeRemoveRow = function (data) {
                if (data.relatedWithSalaryOrIncentive.toUpperCase() == 'SALARY') {
                    abp.message.confirm(
                        app.localize('DeleteSalaryTagWarning', data.employeeName),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                salaryinfoService.deleteEmployeeParticularSalaryTag({
                                    employeeRefId: data.id,
                                    salaryRefId: vm.incentivetag.id
                                }).success(function () {
                                    init();
                                    abp.notify.success(app.localize(data.employeeName + ' ' + 'SuccessfullyDeleted'));
                                });
                            }
                        }
                    );
                }
                else if (data.incentiveCanBeDeleted) {
                    abp.message.confirm(
                        app.localize('DeleteIncentiveTagWarning', data.employeeName),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                incentivetagService.deleteEmployeeParticularIncentive({
                                    employeeRefId: data.id,
                                    incentiveRefId: vm.incentivetag.id
                                }).success(function () {
                                    init();
                                    abp.notify.success(app.localize(data.employeeName + ' ' + 'SuccessfullyDeleted'));
                                });
                            }
                        }
                    );
                }
                else {
                    abp.notify.error('You Can Not Remove This Incentive');
                    return;
                }
            }

            function init() {
                incentivetagService.getIncentiveTagForEdit({
                    id: vm.incentivetagId
                }).success(function (result) {
                    vm.incentivetag = result.incentiveTag;

                    if (vm.incentivetag.id == null) {
                        vm.incentivetag.activeStatus = true;
                        vm.addPortion();
                    }
                    else {
                        vm.employeedetailPortions = result.employeeList;
                        vm.detailPortions = [];
                        vm.sno = 0;
                        angular.forEach(result.incentiveVsEntityList, function (value, key) {
                            value.referencelist = [];
                            vm.fillEntityReferences(value);
                            vm.sno = vm.sno + 1;
                            vm.detailPortions.push({
                                'sno': vm.sno,
                                'incentiveRefId': value.incentiveRefId,
                                'entityType': value.entityType,
                                'entityRefId': value.entityRefId,
                                'entityRefName': value.entityRefName,
                                'excludeFlag': value.excludeFlag,
                                'eitherOr_ForSameEntity': value.eitherOr_ForSameEntity,
                                'remarks': value.remarks,
                                'referencelist': value.referencelist,
                                'editFlag': false
                            });


                        });
                    }
                });
            }

            init();


            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };


            //  Fill Entity Details

            //  Add Details

            vm.fillEntityReferences = function (data) {
                data.referencelist = [];

                var argOption = data.entityType;
             
                if (argOption == 5)
                    data.referencelist = vm.refskillsets;
                else if (argOption == 6)    //   Materials 
                    data.referencelist = vm.refmaterials;
                else if (argOption == 10)   // Document info
                    data.referencelist = vm.refemployeedocumentinfo;
                else if (argOption == 11)   // Gender List
                    data.referencelist = vm.genderlist;
                else if (argOption == 12)   //  Resident Status
                    data.referencelist = vm.refresidentstatus;
                else if (argOption == 13)// Race
                    data.referencelist = vm.refraces;
                else if (argOption == 14)// Race
                    data.referencelist = vm.refreligions;
            }

            vm.genderlist = [];
            vm.genderlist.unshift({ value: "1", displayText: app.localize('Male') });
            vm.genderlist.unshift({ value: "2", displayText: app.localize('Female') });

            vm.refmaterials = [];
            //vm.getMaterials = function () {
            //    vm.loadingCount++;
            //    materialService.getMaterialForCombobox({}).success(function (result) {
            //        vm.refmaterials = result.items;
            //    }).finally(function () {
            //        vm.loadingCount--;
            //    });
            //}
            //vm.getMaterials();

            vm.refskillsets = [];
            vm.getSkillSets = function () {
                vm.loadingCount++;
                personalinformationService.getSkillSetForCombobox({
                }).success(function (result) {
                    vm.refskillsets = result.items;
                }).finally(function () {
                    vm.loadingCount--;
                });
            }
            vm.getSkillSets();

            vm.refemployeedocumentinfo = [];
            vm.getEmployeeDocumentInfo = function () {
                vm.loadingCount++;
                employeedocumentinfoService.getDocumentInfoForCombobox({}).success(function (result) {
                    vm.refemployeedocumentinfo = result.items;
                }).finally(function () {
                    vm.loadingCount--;
                });
            }
            vm.getEmployeeDocumentInfo();

            vm.refcategory = [];
            function fillDropDownCategory() {
                incentivetagService.getIncentiveCategoryCodes({}).success(function (result) {
                    vm.refcategory = result.items;
                });
            }
            fillDropDownCategory();

            vm.refsalarytags = [];
            function fillDropDownSalaryTags() {
                salaryinfoService.getSalaryTagsForCombobox({}).success(function (result) {
                    vm.refsalarytags = result.items;
                });
            }
            fillDropDownSalaryTags();


            vm.refentities = [];
            function fillDropDownEntities() {
                incentivetagService.getIncetivEntityTypeForCombobox({}).success(function (result) {
                    vm.refentities = result.items;
                });
            }
            fillDropDownEntities();

            vm.refspecialformulaTags = [];
            function fillDropDownSpecialFormulaTags() {
                salaryinfoService.getSpecialFormulaTagsForCombobox({}).success(function (result) {
                    vm.refspecialformulaTags = result.items;
                });
            }
            fillDropDownSpecialFormulaTags();

            vm.refincentivegenerationmode = [];
            function fillDropDownIncentiveGenerationModes() {
                incentivetagService.getIncentiveGenerationModeForCombobox({}).success(function (result) {
                    vm.refincentivegenerationmode = result.items;
                });
            }
            fillDropDownIncentiveGenerationModes();

            vm.refincentivePayMode = [];
            function fillDropDownIncentivePayModes() {
                incentivetagService.getIncentivePayModeForCombobox({}).success(function (result) {
                    vm.refincentivePayMode = result.items;
                });
            }
            fillDropDownIncentivePayModes();

            vm.refAllowanceOrDeductionMode = [];
            function fillDropDownrefAllowanceOrDeductionModes() {
                incentivetagService.getAllowanceOrDeductionModeForCombobox({}).success(function (result) {
                    vm.refAllowanceOrDeductionMode = result.items;
                });
            }
            fillDropDownrefAllowanceOrDeductionModes();

            vm.refresidentstatus = [];

            vm.fillResidentStatus = function () {
                personalinformationService.getResidentStatusList({
                }).success(function (result) {
                    vm.refresidentstatus = result.items;
                }).finally(function () {

                });
            }

            vm.fillResidentStatus();

            vm.refraces = [];

            vm.fillRaces = function () {
                personalinformationService.getRacesStatusList({
                }).success(function (result) {
                    vm.refraces = result.items;
                }).finally(function () {

                });
            }

            vm.fillRaces();

            vm.refreligions = [];

            vm.fillReligions = function () {
                personalinformationService.getReligionList({
                }).success(function (result) {
                    vm.refreligions = result.items;
                }).finally(function () {

                });
            }

            vm.fillReligions();

            //  End Fill Entity Details

            vm.detailPortions = [];
            vm.sno = 0;

            vm.addPortion = function (index) {
                var errorFlag = false;
                if (vm.sno > 0) {
                    var value = vm.detailPortions[vm.detailPortions.length - 1];
                    //value = value[0];

                    if (vm.isUndefinedOrNull(value.entityType)) {
                        errorFlag = true;
                        abp.notify.error(app.localize('Entity') + ' ' + app.localize('Required'));
                        return;
                    }


                    if (vm.isUndefinedOrNull(value.entityRefId) || value.entityRefId == 0) {
                        errorFlag = true;
                        abp.notify.error(app.localize('EntityReference') + '?');
                        return;
                    }

                    value.editFlag = false;
                    var lastelement = value;
                }

                vm.sno = vm.sno + 1;
                vm.detailPortions.push({
                    'sno': vm.sno,
                    'editFlag': true,
                    'incentiveRefId': vm.incentivetag.id,
                    'entityType': null,
                    'entityRefName': null,
                    'entityRefId': null,
                    'excludeFlag': false,
                    'eitherOr_ForSameEntity': false,
                    'referencelist': []
                });

                if (index > 0)
                    $scope.$broadcast('uiselect-' + index);
            }

            vm.removeRow = function (productIndex) {
                if (vm.detailPortions.length > 1) {
                    vm.detailPortions.splice(productIndex, 1);
                    vm.sno = vm.sno - 1;
                }
                else {
                    vm.detailPortions.splice(productIndex, 1);
                    vm.sno = 0;
                    vm.addAssetPortion();
                    return;
                }
            }

            $scope.assetFillfunc = function (data, val) {
                data.entityRefId = null;
                data.entityRefName = val.displayText;
            };

            vm.showToolTipExclude = function () {
                abp.notify.error(app.localize('ExcludeToolTip'));
            }

            vm.showToolTipEitherOr_ForSameEntity = function () {
                abp.notify.error(app.localize('EitherOr_ForSameEntityToolTip'));
            }

            vm.employeeBulkUpdatedList = [];
            vm.onetimeInsertForAll = function (argIncentiveRefId) {
                vm.saving = true;
                vm.loading = true;
                vm.employeeBulkUpdatedList = [];
                salaryinfoService.bulkUpdateForAllEligibleEmployeeForThisIncentive({
                    id: argIncentiveRefId
                }).success(function (result) {
                    abp.notify.info(app.localize('BulkUpdateForEligibleEmployees') + ' -  ' + app.localize('IncentiveTag') + ' ' + vm.incentiveTagCode + ' ' + app.localize('SavedSuccessfully'));
                    abp.notify.info(result.length + ' ' + app.localize('Employees') + ' ' + vm.incentiveTagCode + ' ' + app.localize('SavedSuccessfully'));
                    vm.employeeBulkUpdatedList = result;

                }).finally(function () {
                    vm.saving = false;
                    vm.loading = false;
                });
            }

            $scope.entityAlreadyLinked = function (data, val, index) {
                var forLoopFlag = true;

                angular.forEach(vm.detailPortions, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(data.entityType, value.entityType) && angular.equals(val.value, value.entityRefId) && key != index) {
                            abp.notify.error(app.localize('AlreadyExist', value.entityRefName));
                            forLoopFlag = false;
                        }
                    }
                });


                if (!forLoopFlag) {
                    data.entityRefId = null;
                    data.entityRefName = null;
                    return;
                }

                data.entityRefName = val.displayText;
                //if (val.clientRefId != null) {
                //    abp.notify.error(val.assetTagCode + " - " + val.remarks);

                //    data.assetRefId = null;
                //    data.assetTagCode = null;
                //    return;
                //}


            };


            //  End Details

        }
    ]);
})();

