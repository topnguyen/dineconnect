﻿
(function () {
    appModule.controller('tenant.views.hr.transaction.manualincentive.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.manualIncentive', 'abp.services.app.incentiveTag',
        function ($scope, $modal, uiGridConstants, manualincentiveService, incentivetagService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.ManualIncentive.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.ManualIncentive.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.ManualIncentive.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null,
                incentiveTags: []
            };
            vm.incentiveTags = [];


            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editManualIncentive(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteManualIncentive(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('IncentiveDate'),
                        field: 'incentiveDate',
                        cellFilter: 'momentFormat: \'DD-MMM-YYYY ddd\'',
                        maxWidth: 150
                    },
                    {
                        name: app.localize('IncentiveTagRefCode'),
                        field: 'incentiveTagRefCode'
                    },
                    {
                        name: app.localize('EmployeeRefName'),
                        field: 'employeeRefName'
                    },
                    {
                        name: app.localize('Amount'),
                        field: 'incentiveAmount',
                        cellClass: 'ui-ralign',
                        maxWidth: 80
                    },
                    {
                        name: app.localize('Remarks'),
                        field: 'remarks'
                    },
                   
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'LLL\'',
                        minWidth: 100
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            var sd = moment().subtract(6, 'day');
            var td = moment();

            vm.dateRangeModel = {
                startDate: sd,
                endDate: td
            };

            var todayAsString = moment().format($scope.format);
            var weekAgoAsString = moment().subtract(7, 'day').format($scope.format);

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: vm.dateRangeModel.startDate,
                endDate: vm.dateRangeModel.endDate
            });

            vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
            vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;


            vm.dateSetup = function () {
                var sd = moment().subtract(6, 'day');
                var td = moment();
                vm.dateRangeModel = {
                    startDate: sd,
                    endDate: td
                };
            }

            vm.refincentivetags = [];

            function fillDropDownIncentiveTags() {
                vm.loadingCount++;
                incentivetagService.getIncentiveTagCodes({}).success(function (result) {
                    vm.refincentivetags = result.items;
                }).finally(function () {
                    vm.loadingCount--;
                    init();
                });
            }

            fillDropDownIncentiveTags();


            vm.getAll = function () {
                vm.loading = true;
                var startDate = null;
                var endDate = null;

                if (vm.dateFilterApplied) {
                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }

                manualincentiveService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    employeeRefName: vm.filterText,
                    startDate: startDate,
                    endDate: endDate,
                    incentiveTags: vm.incentiveTags
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editManualIncentive = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createManualIncentive = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteManualIncentive = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteManualIncentiveWarning', myObject.employeeRefName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            manualincentiveService.deleteManualIncentive({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/transaction/manualincentive/manualIncentive.cshtml',
                    controller: 'tenant.views.hr.transaction.manualincentive.manualIncentive as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        manualincentiveId: function () {
                            return objId;
                        },
                        callfromIndex: function () {
                            return true;
                        },
                        argEmployeeRefId: function () {
                            return null;
                        },
                        argIncentiveDate: function () {
                            return null;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.exportToExcel = function () {
                manualincentiveService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getAll();
        }]);
})();

