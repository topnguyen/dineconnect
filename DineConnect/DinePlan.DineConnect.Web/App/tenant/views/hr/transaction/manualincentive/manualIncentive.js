﻿(function () {
    appModule.controller('tenant.views.hr.transaction.manualincentive.manualIncentive', [
        '$scope', '$uibModalInstance', 'abp.services.app.manualIncentive', 'manualincentiveId', 'abp.services.app.personalInformation',
        'abp.services.app.incentiveTag', 'callfromIndex', 'argEmployeeRefId', 'argIncentiveDate', 
        function ($scope, $modalInstance, manualincentiveService, manualincentiveId, personalinformationService,  incentivetagService, callfromIndex, argEmployeeRefId, argIncentiveDate) {
            var vm = this;
            /* eslint-disable */
            vm.loadingCount = 0;
            vm.saving = false;
            vm.manualincentive = null;
            vm.employeelist = [];
            if (callfromIndex == false || callfromIndex == "false")
                vm.callfromIndex = false;
            else
                vm.callfromIndex = true;

            vm.uilimit = 30;

            $scope.minDate = moment().add(-60, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];
            $scope.altInputFormats = ['dd-MMMM-yyyy'];

            $scope.open1 = function () {
                $scope.popup1.opened = true;
            };

            $scope.popup1 = {
                opened: false
            };

            $('input[name="poDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment(),
                minDate: $scope.minDate,
                maxDate: moment()
            });

            vm.save = function () {
                if (vm.manualincentive.incentiveTagRefId == null || vm.manualincentive.incentiveTagRefId == 0) {
                    abp.notify.error(app.localize('IncentiveTag') + ' ?');
                    return;
                }

                if (vm.incenitveBasedOnSalaryTags == false) {
                    if (vm.manualincentive.incentiveAmount == null || vm.manualincentive.incentiveAmount == 0) {
                        abp.notify.error(app.localize('Amount') + ' ?');
                        return;
                    }
                }

                if (vm.employeelist.length == 0) {
                    abp.notify.error(app.localize('Employee') + ' ?');
                    return;
                }

                if (vm.overTimeBasedIncentiveFlag == true) {
                    if (vm.manualincentive.manualOtHours == 0 || vm.manualincentive.manualOtHours == null) {
                        abp.notify.error(app.localize('ManualOtHours') + ' ?');
                        return;
                    }
                }

                $scope.dt = $scope.convert(vm.manualincentive.incentiveDate);
                vm.manualincentive.incentiveDate = $scope.dt;

                vm.saving = true;
                vm.loadingCount++;
                manualincentiveService.createOrUpdateManualIncentive({
                    manualIncentive: vm.manualincentive,
                    employeeList: vm.employeelist
                }).success(function (result) {
                    abp.notify.info(app.localize('ManualIncentive') + ' ' + app.localize('SavedSuccessfully'));
                    $modalInstance.close(result);
                }).finally(function () {
                    vm.saving = false;
                    vm.loadingCount--;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss(null);
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.incenitveBasedOnSalaryTags = false;
            vm.projectCostCentreMandatory = false;
            vm.overTimeBasedIncentiveFlag = false;

            vm.getIncentiveTagValue = function (item) {
                vm.incenitveBasedOnSalaryTags = item.incenitveBasedOnSalaryTags;
                vm.projectCostCentreMandatory = item.projectCostCentreMandatory;
                vm.overTimeBasedIncentiveFlag = item.overTimeBasedIncentive;
                if (vm.callfromIndex == false) {
                    if (argProjectCostCentreRefId != null) {
                        vm.manualincentive.projectCostCentreRefId = argProjectCostCentreRefId;
                        vm.refcostcenter.some(function (costcentrevalue, key) {
                            if (costcentrevalue.value == argProjectCostCentreRefId) {
                                vm.manualincentive.projectCostCentreRefCode = costcentrevalue.displayText;
                                vm.getClientInfoBasedOnCostCenter();
                                return true;
                            }

                        });

                    }

                };

                function init() {
                    manualincentiveService.getManualIncentiveForEdit({
                        Id: manualincentiveId
                    }).success(function (result) {
                        vm.manualincentive = result.manualIncentive;

                        if (vm.manualincentive.id == null) {
                            $scope.dt = $scope.convert(moment().startOf('day'));
                            vm.manualincentive.incentiveDate = $scope.dt;
                            vm.uilimit = 30;
                            if (vm.callfromIndex == false) {
                                vm.refemployeecode.some(function (empvalue, key) {
                                    if (empvalue.employeeRefId == argEmployeeRefId) {
                                        vm.employeelist.push(empvalue);
                                        return true;
                                    }
                                });
                                $scope.dt = $scope.convert(new Date(argIncentiveDate));
                                vm.manualincentive.incentiveDate = $scope.dt;
                            }
                        }
                        else {
                            vm.uilimit = null;
                            $scope.dt = $scope.convert(new Date(vm.manualincentive.incentiveDate));
                            vm.manualincentive.incentiveDate = $scope.dt;

                            vm.refemployeecode.some(function (empvalue, key) {
                                if (empvalue.employeeRefId == vm.manualincentive.employeeRefId)
                                    vm.employeelist.push(empvalue);
                            });

                            vm.refincentivetags.some(function (value, key) {
                                if (value.id == vm.manualincentive.incentiveTagRefId) {
                                    vm.getIncentiveTagValue(value);
                                    return true;
                                }
                            });

                        }
                    });
                }

                vm.refincentivetags = [];

                function fillDropDownIncentiveTags() {
                    vm.loadingCount++;
                    incentivetagService.getNonAutomaticIncentiveTagCodes({}).success(function (result) {
                        vm.refincentivetags = result.items;
                    }).finally(function () {
                        vm.loadingCount--;
                        init();
                    });
                }


                vm.refemployeecode = [];
                vm.filteredemployeecode = [];

                function fillDropDownEmployee() {
                    vm.loadingCount++;
                    personalinformationService.getIncentiveAllowedEmployeeWithSkillSetForCombobox({}).success(function (result) {
                        vm.refemployeecode = result;
                        angular.copy(vm.refemployeecode, vm.filteredemployeecode);
                    }).finally(function () {
                        vm.loadingCount--;
                        fillDropDownIncentiveTags();
                    });
                }

                fillDropDownEmployee();

                $scope.convert = function (thedate) {
                    var tempstr = thedate.toString();
                    var newstr = tempstr.toString().replace(/GMT.*/g, "");
                    newstr = newstr + " UTC";
                    return new Date(newstr);
                };

                vm.filterEmployeeList = function () {
                    vm.loading = true;
                    vm.filteredemployeecode = [];

                    if (vm.showAllEmployee) {
                        angular.forEach(vm.refemployeecode, function (empvalue, key) {
                            var skillExistFlag = false;
                            empvalue.employeeKnownSkillSetList.some(function (skillvalue, key) {
                                if (skillvalue.isBillable == true) {
                                    skillExistFlag = true;
                                    return true;
                                }
                            });

                            if (skillExistFlag == true)
                                vm.filteredemployeecode.push(empvalue);

                            skillExistFlag = false;

                        });
                    }
                    else {
                        angular.forEach(vm.refemployeecode, function (empvalue, key) {
                            var skillExistFlag = false;

                            empvalue.employeeKnownSkillSetList.some(function (skillvalue, key) {

                            });

                            if (skillExistFlag == true)
                                vm.filteredemployeecode.push(empvalue);

                            skillExistFlag = false;

                        });

                        vm.loading = false;
                    }
                }
            }
        }
    ]);
})();

