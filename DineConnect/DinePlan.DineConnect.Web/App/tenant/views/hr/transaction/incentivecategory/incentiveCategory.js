﻿
(function () {
    appModule.controller('tenant.views.hr.transaction.incentivecategory.incentiveCategory', [
        '$scope', '$uibModalInstance', 'abp.services.app.incentiveTag', 'incentivecategoryId',
        function ($scope, $modalInstance, incentivetagService, incentivecategoryId) {
            var vm = this;
            
            vm.saving = false;
            vm.incentivecategory = null;

			vm.save = function () {
				if (vm.existall() == false)
                    return;
                vm.saving = true;
				
                if (vm.incentivecategory.overSeasBasedAllowanceOnly == false)
                    vm.incentivecategory.excludedOverseasList = '';

                vm.incentivecategory.incentiveCategoryCode = vm.incentivecategory.incentiveCategoryCode.toUpperCase();

                vm.incentivecategory.incentiveCategoryName = vm.incentivecategory.incentiveCategoryName.toUpperCase();

                if (vm.incentivecategory.excludedOverseasList.length>0)
                    vm.incentivecategory.excludedOverseasList = vm.incentivecategory.excludedOverseasList.toUpperCase();

                incentivetagService.createOrUpdateIncentiveCategory({
                    incentiveCategory: vm.incentivecategory
                }).success(function () {
                    abp.notify.info(app.localize('IncentiveCategory') + ' ' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.incentivecategory.incentiveCategoryCode == null ) {
			         return false;
			     }

					 if (vm.incentivecategory.incentiveCategoryName == null) {
						 return false;
					 }

            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			
	        function init() {
                incentivetagService.getIncentiveCategoryForEdit({
                    id: incentivecategoryId
                }).success(function (result) {
                    vm.incentivecategory = result.incentiveCategory;
                });
						}

            init();
        }
    ]);
})();

