﻿(function () {
    appModule.controller('tenant.views.connect.locationmenu.item', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.menuItem', 'abp.services.app.commonLookup', 'uiGridConstants',
        function ($scope, $state, $stateParams, $modal, menuService, commonService, uiGridConstants) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.locationId = parseInt($stateParams.location) || parseInt(0);
            vm.currentLocation = "";

            vm.filterLeftMenuText = $stateParams.filterText || '';
            vm.filterRightMenuText = $stateParams.filterText || '';

            vm.getLocationValue = function (item) {
                return parseInt(item.value);
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.loading = false;

            function openCreateOrEditModal(objId) {
                if (objId == 0) {
                    abp.notify.info(app.localize('NoLocation'));
                    return;
                }
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/connect/locationmenu/itemModal.cshtml',
                    controller: 'tenant.views.connect.locationmenu.itemModal as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        locationId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.loadMenu();
                });
            }
            vm.importItems = function () {
                openCreateOrEditModal(vm.locationId);
            };

            vm.exportItems = function () {
                if (vm.locationId == 0) {
                    abp.notify.info(app.localize('NoLocation'));
                    return;
                }

                vm.loading = true;
                menuService.getMenuItemsTemplate(vm.locationId, true)
                    .success(function (result) {
                        vm.loading = false;
                        app.downloadTempFile(result);
                    });
            };

            vm.gridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize('Id'),
                        field: 'id'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('Ops'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.createLocationToMenuItem(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-arrow-circle-right"></i></button>' +
                            '</div>',
                        enableSorting: false
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }

                        vm.loadLeftMenu();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.loadLeftMenu();
                    });
                },
                data: []
            };

            vm.gridLocationOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize('Ops'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.deleteLocationToMenuItem(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-arrow-circle-left"></i></button>' +
                            '</div>',
                        enableSorting: false
                    },
                    {
                        name: app.localize('Id'),
                        field: 'id'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    }
                ],
                data: []
            };

            vm.createLocationToMenuItem = function (model) {
                vm.loading = true;
                menuService.createMenuItemToLocation({
                    locationId: vm.locationId,
                    menuItemId: model.id
                }).success(function (result) {
                    vm.loadMenu();
                }).finally(function () {
                });
                vm.loading = false;
            };

            vm.deleteLocationToMenuItem = function (model) {
                vm.loading = true;

                menuService.deleteMenuItemFromLocation({
                    locationId: vm.locationId,
                    menuItemId: model.id
                }).success(function (result) {
                    vm.loadMenu();
                }).finally(function () {
                });
                vm.loading = false;
            };
            vm.openLocation = function () {
                vm.locationGroup = app.createLocationInputForCreateSingleLocation();

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.locationId = result.locations[0].id;
                        vm.currentLocation = result.locations[0].name;
                        vm.loadMenu();
                    }
                });
            };

            vm.loadLeftMenu = function () {
                if (vm.locationId > 0) {
                    vm.loading = true;
                    menuService.getAll({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterLeftMenuText
                    }).success(function (result) {
                        vm.gridOptions.totalItems = result.totalCount;
                        vm.gridOptions.data = result.items;
                    }).finally(function () {
                        vm.loading = false;
                    });
                }

                //else {
                //    abp.notify.warn('Please chose loaction');
                //}
            };

            vm.loadRightMenu = function () {
                if (vm.locationId > 0) {
                    vm.loading = true;
                    menuService.getLocationMenuItems({
                        locationId: vm.locationId,
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterRightMenuText
                    }).success(function (result) {
                        vm.gridLocationOptions.totalItems = result.totalCount;
                        vm.gridLocationOptions.data = result.items;
                    }).finally(function () {
                        vm.loading = false;
                    });
                }
                //else
                //    abp.notify.warn('Please chose loaction');
            };

            vm.loadMenu = function () {
                vm.loadLeftMenu();
                vm.loadRightMenu();
            };
            vm.loadMenu();
        }
    ]);
})();