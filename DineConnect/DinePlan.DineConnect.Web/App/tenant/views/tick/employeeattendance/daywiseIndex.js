﻿
(function () {
    appModule.controller('tenant.views.tick.master.employeeattendance.daywiseindex', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.attendanceLog', 'abp.services.app.personalInformation', 'abp.services.app.dailyExecution',
        function ($scope, $modal, uiGridConstants, attendancelogService, personalinformationService, dailyexecutionService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.employeeName = null;
            vm.manualTsNumber = null;
            vm.projectCostCentreRefCode = null;
            vm.employeeRefId = null;
            vm.fingerviewFlag = true;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Hr.Master.AttendanceLog.Create'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Hr.Master.AttendanceLog.Delete'),
                'import': abp.auth.hasPermission('Pages.Tenant.Hr.Master.AttendanceLog.Import'),
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('BioMetricCode'),
                        field: 'bioMetricCode',
                        width: 80
                    },
                    {
                        name: app.localize('EmployeeCode'),
                        field: 'employeeRefCode',
                        width: 150
                    },
                    {
                        name: app.localize('Name'),
                        field: 'employeeRefName'
                    },
                    {
                        name: app.localize('Date'),
                        field: 'workDate',
                        cellFilter: 'momentFormat: \'DD-MMM-YYYY dddd \'',
                    },
                    {
                        name: app.localize('CheckTime'),
                        field: 'timeList',
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            //  Set DateRange as 15 Days Before while loading
            var sd = moment().subtract(3, 'day');
            var td = moment();

            vm.dateRangeModel = {
                startDate: sd,
                endDate: td
            };

            //////var todayAsString = moment().format($scope.format);
            //////var weekAgoAsString = moment().subtract(7, 'day').format($scope.format);

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: vm.dateRangeModel.startDate,
                endDate: vm.dateRangeModel.endDate
            });

            vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
            vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;

            vm.getAll = function () {
                vm.fingerviewFlag = true;
                vm.loading = true;

                if (vm.employeeRefId == 0 || vm.employeeRefId == null) {
                    abp.notify.error(app.localize('Employee') + ' ? ');
                    //return;
                }

                if (vm.isUndefinedOrNull(vm.dateRangeModel.startDate)) {
                    vm.dateRangeModel = {
                        startDate: sd,
                        endDate: td
                    };
                    vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
                    vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;
                }
                var startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                var endDate = moment(vm.dateRangeModel.endDate).format($scope.format);

                attendancelogService.getGpsEmployeeAttendanceDateWise({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    employeeRefId: vm.employeeRefId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.length;
                    vm.userGridOptions.data = result;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editAttendanceLog = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createAttendanceLog = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteAttendanceLog = function (myObject) {
                var dt = moment(myObject.checkTime).format('YYYY-MMM-DD HH:mm');
                abp.message.confirm(
                    app.localize('DeleteAttendanceLogWarning', myObject.id + ' ' + myObject.employeeRefName + ' ' + dt),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            attendancelogService.deleteAttendanceLog({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/attendancelog/attendanceLog.cshtml',
                    controller: 'tenant.views.hr.master.attendancelog.attendanceLog as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        attendancelogId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }


            vm.exportSalaryToExcel = function () {
                attendancelogService.getOTLogToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    employeeRefId: vm.employeeRefId
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.exportToExcel = function () {
                attendancelogService.getAttendanceLogToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    employeeRefId: vm.employeeRefId
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.import = function () {
                importModal();
            };
       

            vm.refemployeecode = [];

            function fillDropDownEmployee() {
                personalinformationService.getEmployeeForCombobox({}).success(function (result) {
                    vm.refemployeecode = result.items;
                });
            }

            function importModal() {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/attendancelog/importAttLog.cshtml',
                    controller: 'tenant.views.hr.master.attendancelog.importAttLog as vm',
                    backdrop: 'static'
                });

                modalInstance.result.then(function (result) {
                    vm.resultfromBioImport = result;
                    vm.bioimportRemarks = '';

                    angular.forEach(vm.resultfromBioImport, function (value, key) {
                        vm.bioimportRemarks = vm.bioimportRemarks + value.stringId + ", "
                    });

                    if (vm.bioimportRemarks != '') {
                        vm.bioimportRemarks = app.localize('BioMetricIdsNotMatchedWithEmployeeRecords', vm.bioimportRemarks);
                    }

                    vm.getAll();
                    vm.loading = false;
                });
            }
            

            fillDropDownEmployee();
            vm.getAll();

            vm.getFullDetail = function () {
                vm.fingerviewFlag = false;
                vm.loading = true;
                dailyexecutionService.getFullInfoOnAttendance({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    employeeRefId: vm.employeeRefId
                }).success(function (result) {
                    vm.employeeExecutionDetails = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

        }]);
})();

