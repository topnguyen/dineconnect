﻿
(function () {
    appModule.controller('tenant.views.tick.master.employeeattendance.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.attendanceLog', 'abp.services.app.personalInformation', /*'abp.services.app.dailyExecution',*/
        function ($scope, $modal, uiGridConstants, attendancelogService, personalinformationService/*,dailyexecutionService*/) {
            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.employeeName = null;
            vm.manualTsNumber = null;
            vm.projectCostCentreRefCode = null;
            vm.employeeRefId = null;
            vm.fingerviewFlag = true;
            vm.resultfromBioImport = null;
            //// Angular basic calendar

            //var isAllowToClose = false;

            //$(".addtask").on('click', function () {
            //    alert("task added!");
            //});

            //$(document).ready(function () {
            //    $('#scheduler').fullCalendar({
            //        themeSystem: 'bootstrap3',
            //        eventRender: function (event, element, view) {
            //            var theDate = event.start
            //            var endDate = event.dowend;
            //            var startDate = event.dowstart;
            //            var excludedDate = event.excludedDate;
            //            var excludedDates = event.excludedDates;
            //            //console.log(event);
            //            if (theDate >= endDate) {
            //                return false;
            //            }

            //            if (theDate <= startDate) {
            //                return false;
            //            }

            //            var excludedTomorrrow = new Date(excludedDate);

            //            if (theDate > excludedDate && theDate < excludedTomorrrow.setDate(excludedTomorrrow.getDate() + 1)) {
            //                return false;
            //            }

            //        },
            //        editable: true,
            //        droppable: true, // this allows things to be dropped onto the calendar
            //        businessHours: {
            //            // days of week. an array of zero-based day of week integers (0=Sunday)
            //            dow: [1, 2, 3, 4, 5], // Monday - Friday

            //            start: '09:00', // a start time (09am in this example)
            //            end: '18:00', // an end time (6pm in this example)
            //        },
            //        scrollTime: '8:00',  //scroll pane initially scrolled down at...
            //        defaultView: 'agendaWeek',
            //        locale: 'en-US',
            //        dragOpacity: 0.4,
            //        header: {
            //            left: 'prev,next today',
            //            center: 'title',
            //            right: 'month,agendaWeek,agendaDay'
            //        },
            //        defaultDate: '2017-08-29T16:00:00',
            //        allDaySlot: false,
            //        eventClick: function (event, jsEvent, view) {
            //            $('#modalTitle').html(event.title);
            //            $('#modalBody').html("id:" + event.id + "<br/>" + event.description);
            //            $('#fullCalModal').modal();
            //        },
            //        drop: function (date, jsEvent, ui, resourceId) {
            //            //console.log("this", this);
            //            var evtName = $(this).data('event').title;
            //            var evtID = $(this).attr('id').toString();

            //        },
            //        //The following constraints prevents the user from adding/updating/deleting events that are before the current date
            //        //The end date is required.  So, you can't add events over a year away from the current date
            //        eventConstraint: {
            //            start: moment().startOf('day'),
            //            end: moment(moment().startOf('day'), 'MM-DD-YYY').add(365, 'days')
            //        },
            //        selectConstraint: {
            //            start: moment().startOf('day'),
            //            end: moment(moment().startOf('day'), 'MM-DD-YYY').add(365, 'days')
            //        },
            //        eventReceive: function (event, delta, revertFunc) {
            //            console.log("event", event);
            //            console.log("event.title", event.title);
            //            console.log("event.color", event.color);
            //            console.log(event.start.format());
            //            console.log(event._id);
            //            var defaultDuration = moment.duration($('#scheduler').fullCalendar('option', 'defaultTimedEventDuration')); // get the default and convert it to proper type
            //            var end = event.end || event.start.clone().add(defaultDuration); // If there is no end, compute it
            //            console.log('end is ' + end.format());

            //            //alert(event.title + " was dropped on " + event.start.format());
            //        },
            //        events: [{
            //            id: 1,
            //            title: "Washdishing",
            //            start: '10:00',
            //            end: '13:00',
            //            dow: [1, 4],
            //            dowstart: new Date('2017/7/14'),
            //            dowend: new Date('2017/9/29'),
            //            excludedDate: new Date('2017/8/14'),
            //            excludedDates: [new Date('2017/8/14'), new Date('2017/8/24')]
            //        },
            //        {
            //            id: 10,
            //            title: "Front Desk",
            //            description: "Attend customers on front desk",
            //            start: '09:00',
            //            end: '11:00',
            //            dow: [2, 5],
            //            dowstart: new Date('2017/8/10'),
            //            dowend: new Date('2017/9/20'),
            //            color: '#f8a4a5'
            //        },
            //        {
            //            id: 3,
            //            title: "Overnight working",
            //            start: '2017-08-29T23:00:00',
            //            end: '2017-08-30T02:00:00',
            //            color: 'red'
            //        }
            //        ]
            //    }

            //    )

            //});


            //    //end


            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Hr.Master.AttendanceLog.Create'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Hr.Master.AttendanceLog.Delete'),
                'import': abp.auth.hasPermission('Pages.Tenant.Hr.Master.AttendanceLog.Import'),
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
					{
						name: app.localize('Actions'),
						enableSorting: false,
						width: 120,

						cellTemplate:
						   "<div class=\"ui-grid-cell-contents\">" +
							   "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
							   "    <ul uib-dropdown-menu>" +
							   //"      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editAttendanceLog(row.entity)\">" + app.localize("Edit") + "</a></li>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteAttendanceLog(row.entity)\">" + app.localize("Delete") + "</a></li>" +
							   "    </ul>" +
							   "  </div>" +
							   "</div>"
					},
                    {
                        name: app.localize('BioMetricCode'),
                        field: 'bioMetricCode',
                        width : 80
                    },
                    {
                        name: app.localize('EmployeeCode'),
                        field: 'employeeRefCode',
                        width: 150
                    },
                    {
                        name: app.localize('Name'),
                        field: 'employeeRefName'
                    },
                    {
                        name: app.localize('CheckTime'),
                        field: 'checkTime',
                        cellFilter: 'momentFormat: \'LLL\'',
					},
					{
						name: app.localize('Location'),
						field: 'attendanceMachineLocationRefName',
						maxWidth: 100
                    },
                    {
                        name: app.localize('CheckType'),
                        field: 'checkTypeCode'
                    },
                    {
                        name: app.localize('Status'),
                        field: 'attendanceStatus',
										},
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            //  Set DateRange as 15 Days Before while loading
            var sd = moment().subtract(3, 'day');
            var td = moment();

            vm.dateRangeModel = {
                startDate: sd,
                endDate: td
            };

            //////var todayAsString = moment().format($scope.format);
            //////var weekAgoAsString = moment().subtract(7, 'day').format($scope.format);

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: vm.dateRangeModel.startDate,
                endDate: vm.dateRangeModel.endDate
            });

            vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
            vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;

            vm.getAll = function () {
                vm.fingerviewFlag = true;
                vm.loading = true;

                if (vm.isUndefinedOrNull(vm.dateRangeModel.startDate)) {
                    vm.dateRangeModel = {
                        startDate: sd,
                        endDate: td
                    };
                    vm.dateRangeOptions.startDate = vm.dateRangeModel.startDate;
                    vm.dateRangeOptions.endDate = vm.dateRangeModel.endDate;
                }
                var startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                var endDate = moment(vm.dateRangeModel.endDate).format($scope.format);

                attendancelogService.getAllForIndex({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    employeeRefId: vm.employeeRefId
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                    });

                personalinformationService.getGpsAttendanceList({
                    employeeRefId: vm.employeeRefId,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format)
                }).success(function (result) {
                    vm.loading = false;
                    vm.gpslist = result;
                });
            };

            vm.editAttendanceLog = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createAttendanceLog = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteAttendanceLog = function (myObject) {
                var dt = moment(myObject.checkTime).format('YYYY-MMM-DD HH:mm');
                abp.message.confirm(
                    app.localize('DeleteAttendanceLogWarning', myObject.id + ' ' + myObject.employeeRefName + ' ' + dt),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            attendancelogService.deleteAttendanceLog({
                                id: myObject.id,
                                attMode: myObject.attendanceStatus,
                                employeeRefId : myObject.employeeRefId
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/hr/master/attendancelog/attendanceLog.cshtml',
                    controller: 'tenant.views.hr.master.attendancelog.attendanceLog as vm',
                    backdrop: 'static',
					keyboard: false,
                    resolve: {
                        attendancelogId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.exportToExcel = function () {
                attendancelogService.getAttendanceLogToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.import = function () {
                importModal();
            };
            vm.importmanual = function () {
                importmanualModal();
            };
            vm.refemployeecode = [];

            function fillDropDownEmployee() {
                personalinformationService.getEmployeeForCombobox({}).success(function (result) {
                    vm.refemployeecode = result.items;
                });
            }

            function importModal() {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/tick/employeeattendance/importAttLog.cshtml',
                    controller: 'tenant.views.tick.master.employeeattendance.importAttLog as vm',
                    backdrop: 'static'
                });

                modalInstance.result.then(function (result) {
                    vm.resultfromBioImport = result;
                    vm.bioimportRemarks = '';

                    angular.forEach(vm.resultfromBioImport, function (value, key) {
                        vm.bioimportRemarks = vm.bioimportRemarks + value.stringId + ", "
                    });

                    if (vm.bioimportRemarks != '')
                    {
                        vm.bioimportRemarks = app.localize('BioMetricIdsNotMatchedWithEmployeeRecords', vm.bioimportRemarks);
                    }

                    vm.getAll();
                    vm.loading = false;
                });
            }

            function importmanualModal() {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/tick/employeeattendance/importAttLog.cshtml',
                    controller: 'tenant.views.tick.master.employeeattendance.importAttLog as vm',
                    backdrop: 'static'
                });

                modalInstance.result.then(function (result) {
                    vm.resultfromBioImport = result;
                    vm.bioimportRemarks = '';

                    angular.forEach(vm.resultfromBioImport, function (value, key) {
                        vm.bioimportRemarks = vm.bioimportRemarks + value.stringId + ", "
                    });

                    if (vm.bioimportRemarks != '') {
                        vm.bioimportRemarks = app.localize('BioMetricIdsNotMatchedWithEmployeeRecords', vm.bioimportRemarks);
                    }

                    vm.getAll();
                    vm.loading = false;
                });
            }

            fillDropDownEmployee();
            vm.getAll();

            //vm.getFullDetail = function ()
            //{
            //    vm.fingerviewFlag = false;
            //    vm.loading = true;
            //    dailyexecutionService.getFullInfoOnAttendance({
            //        skipCount: requestParams.skipCount,
            //        maxResultCount: requestParams.maxResultCount,
            //        sorting: requestParams.sorting,
            //        filter: vm.filterText,
            //        startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
            //        endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
            //        employeeRefId: vm.employeeRefId
            //    }).success(function (result) {
            //        vm.employeeExecutionDetails = result.items;
            //    }).finally(function () {
            //        vm.loading = false;
            //    });
            //}

        }]);
})();

