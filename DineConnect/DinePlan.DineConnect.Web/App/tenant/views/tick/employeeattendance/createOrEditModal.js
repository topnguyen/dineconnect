﻿
(function () {
    appModule.controller('tenant.views.tick.master.employeeattendance.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.employeeAttendance', 'employeeattendanceId', 'abp.services.app.commonLookup',
        function ($scope, $modalInstance, employeeattendanceService, employeeattendanceId, commonLookupService) {
            var vm = this;
            
			alert('create');

            vm.saving = false;
            vm.employeeattendance = null;
			$scope.existall = true;

			
            vm.save = function () {
			   if ($scope.existall == false)
                    return;
                vm.saving = true;
				
                employeeattendanceService.createOrUpdateEmployeeAttendance({
                    employeeAttendance: vm.employeeattendance
                }).success(function () {
                    abp.notify.info('\' EmployeeAttendance \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.employeeattendance.name == null) {
			         vm.existall = false;
			         return;
			     }
				 
                employeeattendanceService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'name',
                    filter: vm.employeeattendance.name,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.employeeattendance.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.employeeattendance.name + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
			vm.reflocations = [];

			 function fillDropDownLocations() {
			     	commonLookupService.getLocationsForCombobox({}).success(function (result) {
                    vm.reflocations = result.items;
                   // vm.reflocations.unshift({ value: "0", displayText: app.localize('NotAssigned') });
                });
			 }

	        function init() {
				alert('init');
				fillDropDownLocations();

                employeeattendanceService.getEmployeeAttendanceForEdit({
                    Id: employeeattendanceId
                }).success(function (result) {
                    vm.employeeattendance = result.employeeAttendance;
                });
            }
            init();
        }
    ]);
})();

