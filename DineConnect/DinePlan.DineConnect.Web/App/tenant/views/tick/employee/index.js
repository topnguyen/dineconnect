﻿(function() {
    appModule.controller("tenant.views.tick.employee.index", [
        "$scope", "$state", "$uibModal", "uiGridConstants", "abp.services.app.employeeInfo",
        function($scope, $state, $modal, uiGridConstants, employeeinfoService) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function() {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission("Pages.Tenant.Tick.Employee.Create"),
                edit: abp.auth.hasPermission("Pages.Tenant.Tick.Employee.Delete"),
                'delete': abp.auth.hasPermission("Pages.Tenant.Tick.Employee.Edit")
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Actions"),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                                "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                                "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                                "    <ul uib-dropdown-menu>" +
                                "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editEmployeeInfo(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                                "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteEmployeeInfo(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                                "    </ul>" +
                                "  </div>" +
                                "</div>"
                    },
                      {
                          name: app.localize("Code"),
                          field: "employeeCode"
                      },
                    {
                        name: app.localize("Name"),
                        field: "employeeName"
                    },
                    {
                        name: app.localize("Card"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                                "  <button class=\"btn btn-default btn-xs\" ng-if=\"row.entity.cardRefId!=null\" ng-click=\"grid.appScope.showDetails(row.entity)\"><i class=\"fa fa-search\"></i></button>" +
                                "</div>"
                    },
                ],
                onRegisterApi: function(gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function(pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };


            vm.getAll = function() {
                vm.loading = true;
                employeeinfoService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function(result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function() {
                    vm.loading = false;
                });
            };

            vm.editEmployeeInfo = function(myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createEmployeeInfo = function() {
                openCreateOrEditModal(null);
            };

            vm.inactiveEmployeeInfo = function(myObj) {
                openInactiveModal(myObj.id);
            };

            vm.deleteEmployeeInfo = function(myObject) {
                abp.message.confirm(
                    app.localize("DeleteEmployeeInfoWarning", myObject.id),
                    function(isConfirmed) {
                        if (isConfirmed) {
                            employeeinfoService.deleteEmployeeInfo({
                                id: myObject.id
                            }).success(function() {
                                vm.getAll();
                                abp.notify.success(app.localize("SuccessfullyDeleted"));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {

                $state.go("tenant.employeeedit", {
                    id: objId,
                });

            }


            function openInactiveModal(objId) {

                $state.go("tenant.employeeinactive", {
                    id: objId,
                });
            }

            vm.exportToExcel = function() {
                employeeinfoService.getAllToExcel({})
                    .success(function(result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getAll();

            vm.showDetails = function(row) {
                var modalInstance1 = $modal.open({
                    templateUrl: "~/App/host/views/swipe/transaction/membercard/drillDownLevel.cshtml",
                    controller: "host.views.swipe.transaction.membercard.drillDownLevel as vm",
                    backdrop: "static",
                    resolve: {
                        cardNumber: function() {
                            return "";
                        },
                        cardRefId: function() {
                            return row.cardRefId;
                        }

                    }
                });

                modalInstance1.result.then(function(result) {
                    //vm.getAll();
                });
            };
        }
    ]);
})();