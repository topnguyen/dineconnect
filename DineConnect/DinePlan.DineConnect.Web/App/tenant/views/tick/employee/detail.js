﻿
(function () {
    appModule.controller('tenant.views.tick.employee.createOrEditModal', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.memberCard', 'abp.services.app.employeeInfo', 'abp.services.app.commonLookup', 'abp.services.app.user', 'abp.services.app.employeeDepartment',
        function ($scope, $state, $stateParams, $modal, membercardService, employeeinfoService, commonLookupService, userService, employeeDepartmentService) {
            var vm = this;


            vm.saving = false;
            vm.employeeinfo = null;
            vm.membercard = null;
            $scope.existall = true;
            vm.canIssueCardDirectly = false;

            var employeeinfoId = $stateParams.id;
            vm.selectedUser = null;
            vm.save = function () {

                if ($scope.existall == false)
                    return;

                vm.saving = true;

                if (vm.employeeinfo.employeeName === "")
                    vm.employeeinfo.employeeName = vm.employeeinfo.employeeName.toUpperCase();

                if (vm.employeeinfo.employeeCode === "")
                    vm.employeeinfo.employeeCode = vm.employeeinfo.employeeCode.toUpperCase();

                employeeinfoService.createOrUpdateEmployeeInfo({
                    employeeInfo: vm.employeeinfo
                }).success(function () {
                    abp.notify.info('\' EmployeeInfo \'' + app.localize('SavedSuccessfully'));

                    $state.go('tenant.employee');
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {
                if (vm.employeeinfo.id == null) {
                    vm.existall = false;
                    return;
                }
                employeeinfoService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.employeeinfo.id,
                    operation: 'SEARCH'
                }).success(function (result) {
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.employeeinfo.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.employeeinfo.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $state.go('tenant.employee');
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };


            function init() {

                employeeinfoService.getEmployeeInfoForEdit({
                    Id: employeeinfoId
                }).success(function (result) {

                    vm.employeeinfo = result.employeeInfo;

                    userService.getUsers({}).success(function (result) {
                        vm.users = result.items;
                        vm.selectedUser = vm.users.filter(function (item) {
                            return item.id === vm.employeeinfo.userId;
                        })[0];
                    });

                    membercardService.getMemberCardForEdit({
                        Id: null
                    }).success(function (result) {
                        vm.membercard = result.memberCard;
                        if (vm.membercard.id == null)
                            vm.membercard.isPrimaryCard = true;

                        if (vm.membercard.id != null) {
                            vm.uilimit = null;
                        }
                        else {
                            vm.uilimit = 20;
                        }
                    }).finally(function () {
                    });
                });
                commonLookupService.getEmployeeDepartments({}).success(function (result) {
                    vm.departments = result;
                    vm.departments.unshift({ value: "0", displayText: app.localize("NotAssigned") });
                });
            }

            init();


            vm.refCards = [];
            vm.fillCards = function () {
                vm.refCards = [];
                vm.loading = true;
                vm.loadingCount++;
                membercardService.getSwipeCardDetails({
                }).success(function (result) {
                    vm.loading = false;
                    vm.refCards = result.items;
                }).finally(function () {
                    vm.loading = false;
                    vm.loadingCount--;
                });
            }

            vm.fillCards();

            vm.addCard = function () {
                if (vm.employeeinfo.id == null) {
                    return;
                }

                console.log(membercardService);
                vm.membercard.employeeRefId = vm.employeeinfo.id;
                membercardService.attachEmployeeCard({
                    memberCard: vm.membercard,
                    topUpAmount: vm.topUpAmount,
                    refundAmount: vm.refundAmount,
                    depositCollectedAmount: vm.depositAmountToBeCollected,
                    depositRefundedAmount: vm.depositAmountToBeRefunded
                }).success(function (result) {
                    vm.fillCards();
                    init();
                    abp.notify.info(app.localize('MemberCard') + ' ' + app.localize('SavedSuccessfully'));
                }).finally(function () {
                    vm.saving = false;
                });
            }

            vm.cardTransactionDataShown = function (argOption) {
                var modalInstance1 = $modal.open({
                    templateUrl: '~/App/host/views/swipe/transaction/membercard/drillDownLevel.cshtml',
                    controller: 'host.views.swipe.transaction.membercard.drillDownLevel as vm',
                    backdrop: 'static',
                    resolve: {
                        cardNumber: function () {
                            return '';
                        },
                        cardRefId: function () {
                            return argOption;
                        }

                    }
                });

                modalInstance1.result.then(function (result) {
                    //vm.getAll();
                });
            }


            vm.selectMemberBasedOnCardNumber = function () {
                if (vm.membercard.cardNumber == '' || vm.membercard.cardNumber == null) {
                    vm.membercard.cardRefId = null;
                    return;
                }
                vm.topUpAmount = null;
                vm.refundAmount = null;

                if (vm.cardnumberblurneeded == false)
                    return;

                vm.memberCardList = [];
                vm.uilimit = null;
                vm.selectedCard = null;
                vm.refCards.some(function (refdata, refkey) {
                    if (refdata.cardNumber == vm.membercard.cardNumber) {
                        vm.selectedCard = refdata;
                        return true;
                    }
                });

                vm.cardnumberblurneeded = false;
                if (vm.selectedCard == null)       //  No More Records Found In Card , Means New Card Issue
                {
                    if (vm.canIssueCardDirectly == true) {
                        vm.membercard.cardRefId = null;
                        abp.notify.success(app.localize('NewCardToBeIssued'));
                        return;
                    }
                    else {
                        vm.membercard.cardRefId = null;
                        $("#cardnumber").focus();
                        abp.notify.error(app.localize('CardNumberDoesNotExist', vm.membercard.cardNumber));
                        vm.membercard.cardNumber = null;
                        return;
                    }
                }



                if (vm.selectedCard.activeStatus == true) {
                    abp.notify.error(app.localize('ThisCardAlreadyIssued'));
                    return;
                }

                vm.membercard.cardRefId = vm.selectedCard.id;
                vm.membercard.cardTypeRefId = vm.selectedCard.cardTypeRefId;
                vm.membercard.canIssueWithOutMember = vm.selectedCard.canIssueWithOutMember;
                vm.membercard.isEmployeeCard = vm.selectedCard.isEmployeeCard;

                if (vm.membercard.isEmployeeCard == false) {
                    abp.notify.error(app.localize('ThisCardTypeIsNotForEmployee'));
                    return;
                }
                else {
                    vm.depositAmountToBeCollected = vm.selectedCard.depositAmount;
                    if (vm.depositAmountToBeCollected > 0) {
                        abp.notify.info(app.localize('Deposit') + ' ' + vm.selectedCard.depositAmount);
                    }
                }
            }


        }


    ]);
})();

