﻿(function () {
    appModule.controller("tenant.views.tick.report.timeAttendanceReport", [
        "$scope",  "$uibModal", "uiGridConstants", "abp.services.app.employeeInfo", "appSession",
        function ($scope, $modal, uiGridConstants, employeeinfoService, appSession) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });

            vm.filterText = "";
            vm.loading = false;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;

            // #region Builder

            $scope.builder =
                app.createBuilder(
                    {
                       condition: "AND",
                        rules: [
                            {
                                empty: true
                            }
                        ]
                    },
                    angular.fromJson
                        (
                            [
                                {
                                    "id": "Location",
                                    "label": app.localize("Location"),
                                    "type": "integer",
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                },
                                {
                                    "id": "Name",
                                    "label": app.localize("EmployeeName"),
                                    "type": "string"
                                },
                                {
                                    "id": "EmployeeId",
                                    "label": app.localize("EmployeeId"),
                                    "type": "integer",
                                    "operators": [
                                        "equal",
                                        "less",
                                        "less_or_equal",
                                        "greater",
                                        "greater_or_equal"
                                    ],
                                },
                                {
                                    "id": "Position",
                                    "label": app.localize("Position"),
                                    "type": "string"
                                }
                            ]
                        )
                );


                // #endregion

            $scope.formats = ["YYYY-MMM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];

            vm.requestParams = {
                sorting: null,
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
            };

            var todayAsString = moment().format("YYYY-MM-DD");
            var fromdayAsString = moment().subtract(7, "days").calendar();

            $("input[name=\"reportDate\"]").daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };


            vm.exportToExcel = function (obj) {
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        employeeinfoService.getExcelOfTimeAttendanceReport(vm.getCurrentReportInput(myConfirmation, obj))
                            .success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                abp.ui.clearBusy("#MyLoginForm");
                                vm.disableExport = false;
                            });
                    });
            };

            vm.clear = function () {
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };
                $scope.builder.builder.reset();
                vm.getReport();
            }

            vm.getReport = function () {
                vm.loading = true;
                employeeinfoService.timeAttendanceReport(vm.getCurrentReportInput()).success(function (result) {
                    console.log(result);
                    vm.output = result;
                    vm.reportGridOptions.totalItems = result.timeAttendances.length;
                    vm.reportGridOptions.data = result.timeAttendances;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.reportGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'highlight-row\': row.entity.summaryWorkingHours > 12 }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Location'),
                        field: 'locationId'
                    },
                    {
                        name: app.localize('LocationName'),
                        field: 'locationName'
                    },
                    {
                        name: app.localize('Employee'),
                        field: 'employeeCode'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'employeeName'
                    },
                    {
                        name: app.localize('Designation'),
                        field: 'position'
                    },
                    {
                        name: app.localize('ClockIn'),
                        field: 'clockIn',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\''
                    },
                    {
                        name: app.localize('ClockOut'),
                        field: 'clockOut',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\''

                    },
                    {
                        name: app.localize('WorkingHours'),
                        field: 'workingHours'
                    },
                    {
                        name: app.localize('SummaryWorkingHours'),
                        field: 'summaryWorkingHours'
                    }

                ],
                rowClassRules: {
                    "bg-warning": "1 > 0",
                },
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getReport();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;

                        vm.getReport();
                    });
                },
                data: []
            };
            
            vm.getCurrentReportInput = function (confirmation, obj) {
                console.log($scope.builder);

                return {
                    startDate: moment(vm.dateRangeModel.startDate).lang("en").format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).lang("en").format($scope.format),
                    locationGroup: vm.locationGroup,
                    filterEmployeeName: vm.filterText,
                    runInBackground: confirmation,
                    exportOutputType: obj,
                    skipCount: vm.requestParams.skipCount,
                    maxResultCount: vm.requestParams.maxResultCount,
                    sorting: vm.requestParams.sorting,
                    dynamicFilter: angular.toJson($scope.builder.builder.getRules())
                };
            };

            

            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForSearch();
            };

            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                const modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            };
        }
    ]);
})();