﻿
(function () {
    appModule.controller('tenant.views.tick.department.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.employeeDepartment', 'departmentId',
        function ($scope, $modalInstance, departmentService, departmentId) {
            var vm = this;

            vm.saving = false;
            vm.department = null;
            $scope.existall = true;


            vm.save = function () {
                if ($scope.existall === false)
                    return;

                vm.saving = true;
                vm.department.name = vm.department.name.toUpperCase();


                departmentService.createOrUpdateEmployeeDepartment({
                    department: vm.department,
                }).success(function () {
                    abp.notify.info('\' Department \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {

                if (vm.department.id == null) {
                    vm.existall = false;
                    return;
                }

                departmentService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.department.id,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.department.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.department.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };



            function init() {
                departmentService.getEmployeeDepartmentForEdit({
                    Id: departmentId
                }).success(function (result) {
                    vm.department = result.department;
                });
            }

            init();
        }
    ]);
})();

