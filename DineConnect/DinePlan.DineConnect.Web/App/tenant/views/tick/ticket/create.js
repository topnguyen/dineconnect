﻿(function () {
    appModule.controller('tenant.views.tick.ticket.create', [
        '$scope', '$state', '$stateParams', 'FileUploader', 'abp.services.app.ticketMessage', 'abp.services.app.employeeDepartment', 'abp.services.app.ticketMessageStatus', 'abp.services.app.location',
        function ($scope, $state, $stateParams, fileUploader, ticketMessageService, departmentService, ticketStatusesService, locationService) {
            var vm = this;

            vm.saving = false;
            vm.ticketMessage = {};
            vm.editMode = false;
            vm.files = [];
            vm.selectedDepartment = null;


            departmentService.getAll({}).then(function (result) {
                vm.departments = result.data.items;
            });

            ticketStatusesService.getAll().then(function (result) {
                vm.statuses = result.data.items;

                if (!vm.editMode) {
                    vm.selectedStatus = vm.statuses[0];
                }
            });

            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadDocument',
            });


            vm.save = function () {
                vm.saving = true;
                if (vm.uploader.queue.length > 0)
                    vm.uploader.uploadAll();
                else {
                    vm.uploader.onCompleteAll();
                }
            };

            vm.uploader.onCompleteAll = function () {
                // vm.saving = false;

                vm.ticketMessage.files = JSON.stringify(vm.files);
                vm.ticketMessage.messageStatusId = vm.selectedStatus.id;
                vm.ticketMessage.departmentId = vm.selectedDepartment.id;
                vm.ticketMessage.locationId = vm.selectedLocation.id;
                //files
                ticketMessageService.createTicketMessage({
                    ticketMessage: vm.ticketMessage
                }).then(function (result) {
                    abp.notify.info(app.localize('SavedSuccessfully'));

                    $state.go('tenant.tickticket');

                    //$uibModalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                vm.files.push(response.result);
            };



            vm.cancel = function () {
                $state.go('tenant.tickticket');
            };

            vm.init = function () {

                locationService.getAll({}).success(function(result) {
                    vm.locations = result.items;
                });



            };

            vm.init();
        }
    ]);
})();