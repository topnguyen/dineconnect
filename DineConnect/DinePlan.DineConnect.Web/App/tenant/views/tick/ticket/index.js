﻿(function () {
    appModule.controller('tenant.views.tick.ticket.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.ticketMessage', 'abp.services.app.ticketMessageStatus', 'abp.services.app.location',
        function ($scope, $state, $uibModal, uiGridConstants, ticketMessageService, ticketStatusesService, locationService) {

            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;


            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.date = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.filter = {
                locations: [],
                users: []
            };
            // vm.dateRangeModel.setEndDate(moment().startOf('month').format('YYYY-MM-DD'));
            //  vm.dateRangeModel.setEndDate(moment().endOf('month').format('YYYY-MM-DD'));



            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };


            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive,' +
                    '\'ui-grid-row-bg-red\':  grid.appScope.durationInHour(row.entity) >= 2,' +
                    '\'ui-grid-row-bg-green\': row.entity.messageStatus.title==\'Solved\'' +
                    '}" ui-grid-cell></div>',


                columnDefs: [
					{
					    name: app.localize('Actions'),
					    enableSorting: false,
					    width: 120,

					    cellTemplate:
						   "<div class=\"ui-grid-cell-contents\">" +
							    "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
							   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
							   "    <ul uib-dropdown-menu>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editUnit(row.entity)\">" + app.localize("Edit") + "</a></li>" +
							   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteUnit(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                               "      <li><a ng-click=\"grid.appScope.editTicket(row.entity)\">" + app.localize("Show(Reply,Edit)") + "</a></li>" +
							   "    </ul>" +
							   "  </div>" +
							   "</div>"
					},
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    },
                     {
                         name: app.localize('LastModificationTime'),
                         field: 'lastModificationTime',
                         cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                         minWidth: 100
                     },
                      {
                          name: app.localize('Location'),
                          field: 'location.name'
                      },
                     {
                         name: app.localize('Department'),
                         field: 'department.name'
                     },

                    {
                        name: app.localize('Status'),
                        field: 'messageStatus.title'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.durationInHour = function (entity) {

                if (entity.lastModificationTime != null) {

                    var startDate = moment(entity.lastModificationTime);
                    var toDate = moment();
                    var diffHours = toDate.diff(startDate, 'hours');

                    return diffHours;
                    // var minuteDiff = toDate.diff(fromDate, 'minutes');
                }

                return 0;
            };


            vm.getInputFilter = function () {
                return {
                    startDate: vm.date.startDate,
                    endDate: vm.date.endDate,
                    //locations: vm.filter.locations.length > 0 ? vm.filter.locations.map(a=>a.id) : null,
                    //users: vm.filter.users.length > 0 ? vm.filter.users.map(a=>a.id) : null,
                    //ticketMessageStatusId: vm.filter.status != null ? vm.filter.status.id : null
                };
            }


            vm.getAll = function () {
                vm.loading = true;

                ticketMessageService.getAll({
                    filter: vm.getInputFilter(),
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.exportToExcel = function () {
                ticketMessageService.getAllToExcel({
                    filter: vm.getInputFilter(),
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting
                })
                    .success(function (result) {
                        app.downloadTempFile(result);

                    });
            };


            vm.init = function () {

                vm.getTicketStatuses();
                vm.getLocations();
                vm.getAllCreationUser();
                vm.getAll();

            };

            vm.getAllCreationUser = function () {
                ticketMessageService.getAllCreationUser({}).success(function (result) {
                    vm.users = result.items;
                });
            };

            vm.getLocations = function () {
                locationService.getAll({}).success(function (result) {
                    vm.locations = result.items;
                });
            };

            vm.getTicketStatuses = function () {
                ticketStatusesService.getAll().then(function (result) {
                    vm.statuses = result.data.items;
                });
            };

            function openDetail(objId) {
                $state.go("tenant.tickticketdetail", {
                    id: objId
                });
            }

            vm.editTicket = function (myObj) {
                openDetail(myObj.id);
            };


            vm.openCreateTicketMessage = function () {
                $state.go('tenant.tickticketcreate');
            };

            vm.init();

        }
    ]);
})();