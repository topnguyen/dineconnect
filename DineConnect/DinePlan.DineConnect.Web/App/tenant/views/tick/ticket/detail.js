﻿(function () {
    appModule.controller('tenant.views.tick.ticket.detail', [
        '$scope', '$state', '$stateParams', 'FileUploader', 'abp.services.app.ticketMessage', 'abp.services.app.ticketMessageStatus', 'abp.services.app.employeeDepartment',
        function ($scope, $state, $stateParams, fileUploader, ticketMessageService, ticketStatusesService, departmentService) {
            var vm = this;


            vm.tid = $stateParams.id;

            vm.messageReply = {};
            vm.messageReply.TicketMessageId = vm.tid;

            vm.replyFiles = [];

            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadDocument'
            });


            vm.edit = function (myObject) {
                vm.editMode = true;
                vm.currentEditReply = myObject;

                vm.messageReply.message = myObject.message;

                if (myObject.ticketMessageStatus != null) {

                    $("#changeStatus").parent().addClass('checked');

                    vm.changeStatus = true;
                    vm.selectedStatus = myObject.ticketMessageStatus;
                }

                if (myObject.employeeDepartment != null) {

                    $("#changeDepartment").parent().addClass('checked');

                    vm.changeDepartment = true;
                    vm.selectedDepartment = myObject.employeeDepartment;
                }

            };


            vm.save = function () {
                vm.saving = true;

                ticketMessageService.deleteReply({
                    id: vm.currentEditReply.id
                }).success(function () {
                    vm.editMode = false;
                    vm.add();
                });
            };


            vm.cancel = function () {
                vm.editMode = false;

                vm.clearReplyForm();
            };

            vm.add = function () {
                vm.saving = true;

                if (vm.uploader.queue.length > 0)
                    vm.uploader.uploadAll();
                else {
                    vm.uploader.onCompleteAll();
                }
            };

            vm.remove = function (myObject) {

                abp.message.confirm(
                    app.localize('DeleteReplyWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            vm.clearReplyForm();

                            ticketMessageService.deleteReply({
                                id: myObject.id
                            }).success(function () {

                                vm.init();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.clearReplyForm = function () {
                vm.changeStatus = false;
                vm.changeDepartment = false;
                vm.messageReply.message = null;
                vm.uploader.clearQueue();
                vm.replyFiles = [];
                $("#changeStatus").parent().removeClass('checked');
                $("#changeDepartment").parent().removeClass('checked');
            };



            vm.uploader.onCompleteAll = function () {
                

                vm.messageReply.files = JSON.stringify(vm.replyFiles);

                if (vm.changeStatus) {
                    vm.currentStatus = vm.selectedStatus;
                }

                vm.messageReply.ticketMessageStatusId = vm.changeStatus ? vm.selectedStatus.id : null;
                vm.messageReply.employeeDepartmentId = vm.changeDepartment ? vm.selectedDepartment.id : null;

                ticketMessageService.addReply({
                    ticketMessageReply: vm.messageReply
                }).success(function () {

                    vm.uploader.clearQueue();

                    abp.notify.info(app.localize('SavedSuccessfully'));

                    vm.init();

                }).finally(function () {
                    vm.clearReplyForm();
                    vm.saving = false;
                });

            };


            vm.close = function () {
                $state.go('tenant.tickticket');
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                vm.replyFiles.push(response.result);
            };

            vm.saveDocument = function (file) {
                location.href = abp.appPath + 'FileUpload/DownloadDocument?fileName=' + file.fileName + '&fileSystemName=' + file.fileSystemName;
            };

            vm.getReplies =  function() {
                ticketMessageService.getTicketReplies({
                    ticketMessageId: vm.tid
                }).success(function (result) {
                    vm.replies = result.items;

                    vm.replies.forEach(function (item, i, arr) {
                        item.files = JSON.parse(item.files);
                    });
                });
            };

            vm.getStatuses = function() {
                ticketStatusesService.getAll().then(function(result) {
                    vm.statuses = result.data.items;

                    vm.currentStatus = vm.statuses.filter(function(item) {
                        return item.id === vm.ticketMessage.messageStatusId;
                    })[0];


                    vm.selectedStatus = vm.currentStatus;


                });
            };

            vm.getDepartments = function() {
                departmentService.getAll({}).then(function(result) {
                    vm.departments = result.data.items;


                    vm.selectedDepartment = vm.departments.filter(function(item) {
                        return item.id === vm.ticketMessage.departmentId;
                    })[0];
                });
            };

            vm.init = function() {

                ticketMessageService.getTicketMessageForEdit({
                    id: vm.tid
                }).success(function (result) {

                    vm.ticketMessage = result.ticketMessage;

                    vm.files = JSON.parse(vm.ticketMessage.files);

                    vm.getStatuses();

                    vm.getDepartments();

                    vm.getReplies();

                });
            }

            vm.init();
        }
    ]);
})();