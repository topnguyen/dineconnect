﻿(function () {
    appModule.controller('tenant.views.cater.quotecosting.catercostingMaterialDetailDrillDown', [
        '$scope', '$uibModalInstance', 'menuMappedSubList', 'materialRefName', 'quantity', 'unitRefName' ,
        function ($scope, $modalInstance, menuMappedSubList, materialRefName, quantity, unitRefName) {
            /* eslint-disable */
            var vm = this;
            vm.materialRefName = materialRefName;
            vm.quantity = quantity;
            vm.unitRefName = unitRefName;
            vm.detailData = menuMappedSubList;
            vm.recipeExistFlag = false;

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

        }
    ]);
})();