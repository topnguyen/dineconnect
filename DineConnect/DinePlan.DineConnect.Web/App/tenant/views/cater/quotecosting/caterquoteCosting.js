﻿(function () {
    appModule.controller('tenant.views.cater.quotecosting.caterquoteCosting', [
        '$scope', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.quotationHeader', 'appSession', 'abp.services.app.caterCustomer',
        function ($scope, $state, $stateParams, $modal, uiGridConstants, quotationService, appSession, customerService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.currentUserId = abp.session.userId;
            vm.quotationId = $stateParams.quotationId;
            vm.defaultLocationRefId = appSession.location.id;
            vm.firstFlag = true;
            vm.filterText = null;
            vm.caterCustomerId = null;
            vm.caterCustomerName = '';

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Cater.QuotationHeader.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Cater.QuotationHeader.Edit'),
                'delete': false
            };

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            var endDateAsString = moment().add(60, 'days').format('YYYY-MM-DD');
            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: endDateAsString
            });


            vm.clear = function () {
                vm.caterCustomerId = null;
                vm.caterCustomerName = '';
                vm.filterText = null;
                vm.getAll();
            };

            vm.dateRangeChange = function () {
                if (vm.firstFlag == false)
                    vm.getAll();
            }

            vm.openCustomerLookup = function () {
                vm.lgroup = {
                    locations: [],
                    groups: [],
                    locationTags: [],
                    group: false,
                    single: true,
                    locationTag: false,
                    service: customerService.getAllCaterCustomer
                };

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/lookup/select.cshtml",
                    controller: "tenant.views.common.lookup.select as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.lgroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.caterCustomerId = result.locations[0].id;
                        vm.caterCustomerName = result.locations[0].name;
                        vm.getAll();
                        //vm.onClientChanged();
                    }
                });
            };


            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeOptions.ranges[app.localize('Tomorrow')] = [moment().add(1, 'days').startOf('day'), moment().add(1, 'days').endOf('day')];
            vm.dateRangeOptions.ranges[app.localize('Next7Days')] = [moment().startOf('day'), moment().add(6, 'days').endOf('day')];
            vm.dateRangeOptions.ranges[app.localize('Next30Days')] = [moment().startOf('day'), moment().add(29, 'days').endOf('day')];
            vm.dateRangeOptions.ranges[app.localize('NextMonth')] = [moment().add(1, 'month').startOf('month'), moment().add(1, 'month').endOf('month')];

            vm.dateRangeOptions.max = moment().add(360, 'days').format('YYYY-MM-DD');
            vm.dateRangeOptions.maxDate = moment().add(360, 'days').format('YYYY-MM-DD');


            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: endDateAsString
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.caterCustomerId = null;

            vm.refQuotationHeaders = [];

            vm.totalQuoteAmount = 0;
            vm.getTotalQuoteAmount = function () {
                vm.totalQuoteAmount = 0;
                angular.forEach(vm.refQuotationHeaders, function (item) {
                    vm.totalQuoteAmount += (item.totalAmount * item.noOfPax);
                });
            };

            vm.getAll = function () {
                vm.loading = true;

                if (vm.dateRangeModel.startDate == null) {
                    vm.dateRangeModel = {
                        startDate: todayAsString,
                        endDate: endDateAsString
                    };
                }
                vm.costingData = [];
                quotationService.getQuotationHeader({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    caterCustomerId: vm.caterCustomerId,
                    PoStatusRequired : true
                }).success(function (result) {
                    //vm.userGridOptions.totalItems = result.totalCount;
                    //vm.userGridOptions.data = result.items;
                    vm.refQuotationHeaders = result.items;
                    vm.getTotalQuoteAmount();
                    if (result.items.length == 1) {
                        vm.cosingForAll();
                    }
                    vm.firstFlag = false;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editQuotation = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createQuotation = function () {
                openCreateOrEditModal(null);
            };

            vm.deleteQuotation = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteQuotationWarning', myObject.cardNo),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            quotationService.deleteCaterQuotationHeader({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                $state.go('tenant.detailquotation', {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                quotationService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.getAll();

            vm.cosingForAll = function () {
                angular.forEach(vm.refQuotationHeaders, function (item) {
                    item.doesCostingRequired = true;
                });
                vm.getCosting();
            };

            vm.overAllFlag = false;

            vm.viewLinkedMenu = function (data) {
                var modalInstance1 = $modal.open({
                    templateUrl: '~/App/tenant/views/cater/quotecosting/catercostingMaterialDetailDrillDown.cshtml',
                    controller: 'tenant.views.cater.quotecosting.catercostingMaterialDetailDrillDown as vm',
                    backdrop: 'static',
                    resolve: {
                        menuMappedSubList: function () {
                            return data.menuMappedSubList;
                        },
                        materialRefName: function () {
                            return data.materialRefName;
                        },
                        quantity: function () {
                            return data.totalQty;
                        },
                        unitRefName: function () {
                            return data.uom
                        }
                    }
                });

                modalInstance1.result.then(function (result) {

                });
            }


            vm.costingData = [];
            vm.getCosting = function () {
                var argCaterRefIdList = [];
                angular.forEach(vm.refQuotationHeaders, function (item) {
                    if (item.doesCostingRequired == true)
                        argCaterRefIdList.push(item.id);
                });
                vm.costingData = [];

                if (argCaterRefIdList.length > 0) {
                    vm.loading = true;
                    vm.loadingCount++;
                    quotationService.getMaterialsRequiredForGivenQuotation({
                        quotationRefIdList: argCaterRefIdList,
                        LocationRefId: vm.defaultLocationRefId
                    }).success(function (result) {
                        vm.costingData = result;
                        if (vm.costingData != null) {
                            if (vm.costingData.quotationWiseCostingList.length == 1)
                                vm.overAllFlag = false;
                            else 
                                vm.overAllFlag = true;
                        }
                    }).finally(function () {
                        vm.loading = false;
                        vm.loadingCount--;
                    });
                }
            };



        }]);
})();



            //vm.userGridOptions = {
            //    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
            //    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
            //    paginationPageSizes: app.consts.grid.defaultPageSizes,
            //    paginationPageSize: app.consts.grid.defaultPageSize,
            //    useExternalPagination: true,
            //    useExternalSorting: true,
            //    appScopeProvider: vm,
            //    rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
            //    columnDefs: [
            //        //{
            //        //    name: app.localize('Actions'),
            //        //    enableSorting: false,
            //        //    width: 150,
            //        //    cellTemplate:
            //        //        "<div class=\"ui-grid-cell-contents\">" +
            //        //        "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
            //        //        "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
            //        //        "    <ul uib-dropdown-menu>" +
            //        //        "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editQuotation(row.entity)\">" + app.localize("Edit") + "</a></li>" +
            //        //        "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteQuotation(row.entity)\">" + app.localize("Delete") + "</a></li>" +
            //        //        "    </ul>" +
            //        //        "  </div>" +
            //        //        "</div>"
            //        //},
            //        {
            //            name: app.localize('Id'),
            //            field: 'id'
            //        },
            //        {
            //            name: app.localize('CustomerName'),
            //            field: 'caterCustomer.customerName',
            //            cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.caterCustomerCustomerName}}</div>'
            //        },
            //        {
            //            name: app.localize('EventDate'),
            //            field: 'eventDateTime',
            //            cellFilter: 'momentFormat: \'YYYY-MM-DD\''
            //        },
            //        {
            //            name: app.localize('EventTime'),
            //            field: 'eventDateTime',
            //            cellFilter: 'momentFormat: \'HH:mm\''
            //        },
            //        {
            //            name: app.localize('OrderChannel'),
            //            field: 'orderChannel',
            //            cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.orderChannelString}}</div>'
            //        },
            //        {
            //            name: app.localize('Costing') ,
            //            field: 'id',
            //            enableSorting: false,
            //            cellTemplate:
            //                '<div class=\"ui-grid-cell-contents text-center\">' +
            //                '  <button ng-click="grid.appScope.getCosting(row.entity.id)" class="btn btn-default btn-xs" title="' + app.localize('Costing') + '"><i class="fa fa-dollor"></i></button>' +
            //                '</div>',
            //            width: 50
            //        },
            //    ],
            //    onRegisterApi: function (gridApi) {
            //        $scope.gridApi = gridApi;
            //        $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
            //            if (!sortColumns.length || !sortColumns[0].field) {
            //                requestParams.sorting = null;
            //            } else {
            //                requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
            //            }

            //            vm.getAll();
            //        });
            //        gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
            //            requestParams.skipCount = (pageNumber - 1) * pageSize;
            //            requestParams.maxResultCount = pageSize;

            //            vm.getAll();
            //        });
            //    },
            //    data: []
            //};
