﻿
(function () {
    appModule.controller('tenant.views.cater.caterAutoPo.caterdetailViewInGrid', [
        '$scope', '$uibModalInstance', 'uiGridConstants', 'abp.services.app.quotationHeader', 'caterHeaderRefId', 'appSession',
        function ($scope, $modalInstance, uiGridConstants, quotationService, caterHeaderRefId, appSession) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.defaultLocationId = appSession.user.locationRefId;
            vm.quotationId = caterHeaderRefId;

            vm.porefnumber = null;
            vm.podate = null;
            vm.manualCompleteAllowed = false;


            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.gridOptions = {
                expandableRowTemplate: '<div ui-grid="row.entity.subGridOptions"></div>',
                //expandableRowScope: {
                //    subGridVariable: 'subGridScopeVariable'
                //},
                columnDefs: [
                    {
                        name: app.localize('Id'),
                        field: 'id'
                    }, {
                        name: app.localize('ItemName'),
                        field: 'groupName'
                    },
                    {
                        name: app.localize('Qty'),
                        field: 'quantity',
                        cellClass: "ui-ralign"
                    },
                    {
                        name: app.localize('Price'),
                        field: 'price',
                        cellClass: "ui-ralign",
                        cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >{{row.entity.price | number: 2}}</div>'
                    },
                    {
                        name: app.localize('Amount'),
                        field: 'amount',
                        cellClass: "ui-ralign",
                        cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >{{row.entity.amount | number: 2}}</div>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;

                    gridApi.expandable.on.rowExpandedStateChanged($scope, function (row) {
                        row.expandedRowHeight = row.entity.subDetails.length * 30 + 50;
                    });
                },
                data: []
            };

            vm.setPoAsCompletion = function (data) 
            {
                vm.loading = true;
                purchaseorderService.setCompletePurchaseOrder({
                    poId: poId,
                    poRemarks : 'Manual Completion'
                }).success(function (result) {
                    vm.cancel();
                }).finally(function () {
                    vm.loading = false;
                });
            };
            
            vm.caterQuoteDetails = [];
            vm.getAll = function() {
                vm.loading = true;
                quotationService.getQuotationHeaderForEdit({ id: vm.quotationId })
                    .success(function (result) {
                        vm.loading = false;
                        vm.quotation = result;

                        vm.onClientChanged();
                        if (!vm.quotationId) {
                            vm.quotation.eventDateTime = new Date();
                            vm.quotation.locationRefId = vm.defaultLocationId;
                        }
                        vm.eventTime = vm.quotation.eventDateTime;
                    });

                if (vm.quotationId)
                    quotationService.getQuotationDetails(vm.quotationId)
                        .success(function (result) {
                            vm.loading = false;

                            vm.quoteDetails = result;
                            var data = result;

                            angular.forEach(data, function (item) {
                                if (item.subDetails.length > 0) {
                                    item.subGridOptions = {
                                        columnDefs: [
                                            { name: 'Tag Name', field: 'menuItemName' },
                                            { name: 'Alias Code', field: 'aliasCode' },
                                            {
                                                name: 'Price', field: 'price', cellClass: "ui-ralign",
                                                cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >{{row.entity.price | number: 2}}</div>'
                                            },
                                            { name: 'Pax', field: 'quantity', cellClass: "ui-ralign" }
                                        ],
                                        data: item.subDetails
                                    };
                                } else {
                                    item.subGridOptions = { disableRowExpandable: true, columnDefs: [], data: [] };
                                }
                            });
                            vm.gridOptions.data = data;
                        });
            };

            vm.getAll();

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            
        }]);
})();