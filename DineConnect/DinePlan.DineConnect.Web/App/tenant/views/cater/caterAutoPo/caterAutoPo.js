﻿(function () {
    appModule.controller('tenant.views.cater.caterAutoPo.caterAutoPo', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.quotationHeader', 'appSession', 'abp.services.app.location', 'abp.services.app.dayClose', 'NgTableParams', '$rootScope', 'abp.services.app.purchaseOrderExtended',
        function ($scope, $state, $modal, uiGridConstants, quotationService, appSession, locationService, daycloseService, ngTableParams, $rootScope, purchaseorderExtendedService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = true;
            vm.filterText = null;
            $rootScope.settings.layout.pageSidebarClosed = true;
            vm.currentUserId = abp.session.userId;
            vm.messagenotificationflag = abp.setting.getBoolean("App.House.MessageNotification");
            vm.softmessagenotificationflag = abp.setting.getBoolean("App.House.SoftMessageNotification");
            vm.showOnlyDeliveryDateExpectedAsOnDate = false;
            vm.enterOnHandFilter = -1;
            vm.omitQueueInOrder = true;

            vm.saving = false;

            vm.defaultLocationId = appSession.user.locationRefId;

            if (vm.defaultLocationId == 0 || vm.defaultLocationId == null) {
                abp.message.warn(app.localize('LocationUserNotAssigned'), app.localize('UserLocationNotAuthorized'));
                return;
            }

            vm.permissions = {
                autoPoForCater: true, // abp.auth.hasPermission('Pages.Tenant.House.Transaction.InterTransferApproval.Create'),
            };

            //  Location Link Start
            vm.locationGroup = app.createLocationInputForCreateSingleLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                    vm.getAll();
                });
            };

            //  Location Link End
            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.deliveryDateExpected = moment().format($scope.format);
            $('input[name="dueDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: vm.deliveryDateExpected,
                minDate: $scope.minDate,
                maxDate: moment().add(90, 'days').format('YYYY-MM-DD'), //moment()
            });


            var requestParams = {
                skipCount: 0,
                maxResultCount: 10, // app.consts.grid.defaultPageSize,
                sorting: null
            };

            $scope.getLValue = function (column) {
                return app.localize(column);
            };

            vm.advancedFiltersAreShown = false;
            vm.dateFilterApplied = false;

            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            var next7DayAsString = moment().add(7,'days').format('YYYY-MM-DD');
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: next7DayAsString,
                maxDate: moment().add(90, 'days').format('YYYY-MM-DD'), //moment()
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeOptions.ranges[app.localize('Tomorrow')] = [moment().add(1, 'days').startOf('day'), moment().add(1, 'days').endOf('day')];
            vm.dateRangeOptions.ranges[app.localize('Next7Days')] = [moment().startOf('day'), moment().add(6, 'days').endOf('day')];
            vm.dateRangeOptions.ranges[app.localize('Next30Days')] = [moment().startOf('day'), moment().add(29, 'days').endOf('day')];
            vm.dateRangeOptions.ranges[app.localize('NextMonth')] = [moment().add(1, 'month').startOf('month'), moment().add(1, 'month').endOf('month')];

            vm.dateRangeOptions.max = moment().add(90, 'days').format('YYYY-MM-DD');
            vm.dateRangeOptions.maxDate = moment().add(90, 'days').format('YYYY-MM-DD');

            vm.setDate = function () {
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: next7DayAsString
                };
            }

            vm.setDate();

            vm.clear = function () {
                vm.filterText = null;
                vm.getAll();
            }

            vm.tableParams = new ngTableParams({}, { dataset: [], counts: [] });
            vm.tableParamsMaterial = new ngTableParams({}, { dataset: [], counts: [] });
         
            vm.availableLocations = [];
            vm.uniqueList = [];
            vm.smartLocationSelection = function () {

                var locationIds = [];
                angular.forEach(vm.requestList, function (value, key) {
                    if (value.doesThisRequestToBeConvertedAsPO)
                        locationIds.push(value.locationRefId);
                });

                vm.uniqueList = [];
                vm.uniqueList = locationIds.filter(function (itm, i, a) {
                    return i == locationIds.indexOf(itm);
                });
                //  If More than one Location Available 
                if (vm.uniqueList.length == 1) {
                    vm.locationRefId = vm.uniqueList[0];
                    vm.shippingLocationId = vm.uniqueList[0];
                }
                else {
                    vm.locationRefId = vm.defaultLocationId;
                    vm.shippingLocationId = vm.defaultLocationId;
                }
                vm.fillSmartLocations();
            }

            vm.fillSmartLocations = function () {
                vm.availableLocations = [];

                var existFlag = false;
                vm.uniqueList.some(function (data, key) {
                    if (data == vm.defaultLocationId) {
                        existFlag = true;
                        return true;
                    }
                });
                if (existFlag == false)
                    vm.uniqueList.push(vm.defaultLocationId);

                angular.forEach(vm.uniqueList, function (value, key) {
                    vm.reflocations.some(function (data, key) {
                        if (data.id == value) {
                            vm.availableLocations.push(data);
                            return true;
                        }
                    });
                });
            }


            vm.requestList = [];

            vm.getAll = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;
                var currentStatus = 'Pending';
                currentStatus = '';

                if (vm.dateFilterApplied) {
                    if (vm.dateRangeModel.startDate == null) {
                        vm.setDate();
                    }

                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }

                quotationService.getQuotationHeader({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    requestStatus: '',
                    defaultLocationRefId: vm.defaultLocationId,
                    startDate: startDate,
                    endDate: endDate,
                    currentStatus: currentStatus,
                    showOnlyDeliveryDateExpectedAsOnDate: vm.showOnlyDeliveryDateExpectedAsOnDate,
                    locationGroup: vm.locationGroup,
                    omitAlreadyPoLinkedRequest: true,
                    PoStatusRequired : true
                }).success(function (result) {
                    vm.tableParams = new ngTableParams({}, { dataset: result.items, counts: [] });
                    vm.requestList = result.items;
                    var minDeliveryDate = null;
                    if (vm.requestList.length > 0)
                        minDeliveryDate = vm.requestList[0].deliveryDateRequested;
                    angular.forEach(vm.requestList, function (value, key) {
                        if (minDeliveryDate > value.deliveryDateRequested) {
                            minDeliveryDate = value.deliveryDateRequested;
                        }
                    });
                    vm.deliveryDateExpected = minDeliveryDate;
                    vm.materialPoList = [];
                    vm.smartLocationSelection();
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.showPoStatusRemarks = function (data) {
                var caterRequiredMaterialListDtos = data.caterRequiredMaterialListDtos;
                
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/cater/caterAutoPo/caterPoPipeline.cshtml',
                    controller: 'tenant.views.cater.caterAutoPo.caterPoPipeline as vm',
                    backdrop: 'static',
                    resolve: {
                        data: function () {
                            return caterRequiredMaterialListDtos;
                        },
                        caterOrderRefId: function () {
                            return data.id;
                        },
                        customerName: function () {
                            return data.caterCustomerCustomerName;
                        },
                    }
                });

                modalInstance.result.then(function (result) {
                    var a = result;
                });
                vm.loading = false;
            }

            vm.materialPoList = [];
            vm.getMaterialPoList = function () {
                vm.materialPoList = [];
                vm.poconvertedRequestList = [];
                angular.forEach(vm.requestList, function (value, key) {
                    if (value.doesThisRequestToBeConvertedAsPO) {
                        vm.poconvertedRequestList.push(value);
                    }
                });

                if (vm.poconvertedRequestList.length == 0) {
                    abp.notify.warn('No More PO Needed Request Found');
                    return;
                }
                vm.saving = true;
                vm.loading = true;

                quotationService.getPoRequestBasedOnQuoteSelection({
                    requestBasedOnRequestList: true,
                    requestList: vm.poconvertedRequestList,
                    defaultLocationRefId: vm.defaultLocationId,
                    omitQueueInOrder: vm.omitQueueInOrder
                }).success(function (dataresult) {
                    vm.materialPoList = dataresult;
                    angular.forEach(vm.materialPoList, function (value, key) {
                        value.omitQueueInOrder = vm.omitQueueInOrder;
                    });
                    vm.tableParamsMaterial = new ngTableParams({}, { dataset: vm.materialPoList, counts: [] });
                }).finally(function () {
                    vm.saving = false;
                    vm.loading = false;
                });
            }


            vm.viewPoDetail = function (purchaseOrderId) {
                if (purchaseOrderId != null)
                    openPoView(purchaseOrderId);
            };

            function openPoView(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/purchaseorder/podetailViewInGrid.cshtml',
                    controller: 'tenant.views.house.transaction.purchaseorder.podetailViewInGrid as vm',
                    backdrop: 'static',
                    resolve: {
                        poId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {

                }).finally(function (result) {
                });
            }

            vm.viewCaterDetail = function (argCaterHeaderId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/cater/caterAutoPo/caterdetailViewInGrid.cshtml',
                    controller: 'tenant.views.cater.caterAutoPo.caterdetailViewInGrid as vm',
                    backdrop: 'static',
                    resolve: {
                        caterHeaderRefId: function () {
                            return argCaterHeaderId;
                        }
                    }
                });
            }

            vm.reflocations = [];
            function fillDropDownLocations() {
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.reflocations = $.parseJSON(JSON.stringify(result.items));
                    if (vm.reflocations.length == 1) {
                        vm.shippingLocationId = vm.reflocations[0].id;
                        vm.selectedLocationName = vm.reflocations[0].code;
                    }
                }).finally(function (result) {
                });
            }
            fillDropDownLocations();

            vm.showSubDetailsForRequiredQuantity = function (data) {

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/cater/caterAutoPo/showCaterOrderMaterialDetails.cshtml',
                    controller: 'tenant.views.cater.caterAutoPo.showCaterOrderMaterialDetails as vm',
                    backdrop: 'static',
                    resolve: {
                        data: function () {
                            return data
                        },
                    }
                });

                modalInstance.result.then(function (result) {
                    var a = result;
                });
                vm.loading = false;
            }

            vm.showPipelineOrders = function (data) {
                if (data.materialRefId == 0 || data.materialRefId == null)
                    return;

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/intertransfer/approval/showOrderPipeline.cshtml',
                    controller: 'tenant.views.house.transaction.intertransfer.approval.showOrderPipeline as vm',
                    backdrop: 'static',
                    resolve: {
                        data: function () {
                            return data
                        },
                        materialRefId: function () {
                            return data.materialRefId;
                        },
                        materialRefName: function () {
                            return data.materialRefName;
                        },
                        uom: function () {
                            return data.uom;
                        },
                        alreadyOrderedQuantity: function () {
                            return data.alreadyOrderedQuantity;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    var a = result;
                });
                vm.loading = false;
            }

            vm.changeSupplier = function (data) {

                if (data.materialRefId == 0 || data.materialRefId == null)
                    return;

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/intertransfer/approval/changeDefaultSupplier.cshtml',
                    controller: 'tenant.views.house.transaction.intertransfer.approval.changeDefaultSupplier as vm',
                    backdrop: 'static',
                    resolve: {
                        data: function () {
                            return data
                        },
                        materialRefId: function () {
                            return data.materialRefId;
                        },
                        materialRefName: function () {
                            return data.materialRefName;
                        },
                        uom: function () {
                            return data.uom;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    var a = result;
                });
                vm.loading = false;
            }

            vm.previousPriceOpen = function (data) {
                if (data.materialRefId == 0 || data.materialRefId == null)
                    return;

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/purchaseorder/previousPriceModal.cshtml',
                    controller: 'tenant.views.house.transaction.purchaseorder.previousPriceModal as vm',
                    backdrop: 'static',
                    resolve: {
                        materialRefId: function () {
                            return data.materialRefId;
                        },
                        materialRefName: function () {
                            return data.materialRefName;
                        },
                        uom: function () {
                            return data.uom;
                        }
                    }
                });

                modalInstance.result.then(function (result) {

                });
                vm.loading = false;
            }

            //vm.consolidatedExcel = function () {
            //    vm.poconvertedRequestList = [];
            //    angular.forEach(vm.requestList, function (value, key) {
            //        if (value.doesThisRequestToBeConvertedAsPO) {
            //            vm.poconvertedRequestList.push(value);
            //        }
            //    });

            //    if (vm.poconvertedRequestList.length == 0) {
            //        abp.notify.warn('No More PO Needed Request Found');
            //        return;
            //    }
            //    vm.saving = true;
            //    vm.loading = true;

            //    intertransferService.getConsolidatedPendingBasedGivenRequestList({
            //        interTransferViewDtos: vm.poconvertedRequestList
            //    }).success(function (dataresult) {
            //        intertransferService.exportConsolidatedToExcel({
            //            exportData: dataresult
            //        }).success(function (result) {
            //            app.downloadTempFile(result);
            //        }).finally(function () {
            //            vm.saving = false;
            //            vm.loading = false;
            //        });
            //    }).finally(function () {

            //    });

            //};

            vm.approveTransfer = function (myObj) {
                var currentStatus = myObj.currentStatus;

                if (currentStatus == app.localize('Received')) {
                    abp.message.error(app.localize('ApproveNotAllowedForReceived'));
                    return;
                }

                if (currentStatus == app.localize('PartiallyReceived')) {
                    abp.message.error(app.localize('ApproveNotAllowedForPartialReceived'));
                    return;
                }

                if (currentStatus == app.localize('Completed')) {
                    abp.message.error(app.localize('ApproveNotAllowedForCompleted'));
                    return;
                }

                if (currentStatus == app.localize('Approved')) {
                    abp.message.warn(app.localize('AlreadyApproved'));
                    return;
                }

                if (currentStatus == app.localize('Cancelled')) {
                    abp.message.error(app.localize('ApproveNotAllowedForCancel'));
                    return;
                }
                else
                    openCreateOrEditModal(myObj);

            }

            vm.createApproval = function () {
                openCreateOrEditModal(null);
            };

            function openCreateOrEditModal(objId) {
                $state.go("tenant.intertransferapprovaldetail", {
                    id: objId.id, requestLocationRefName: objId.requestLocationRefName,
                    locationRefName: objId.locationRefName
                });
            }

            vm.getAll();

            vm.checkCloseDay = function () {
                daycloseService.getDayCloseStatus({
                    id: vm.defaultLocationId
                }).success(function (result) {
                    if (result.id == 0) {
                        if (vm.softmessagenotificationflag)
                            abp.notify.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));

                        if (vm.messagenotificationflag)
                            abp.message.warn(app.localize("LastExecutionDate", moment(result.accountDate).format($scope.format)));
                    }

                }).finally(function () {
                });
            }

            vm.checkCloseDay();

            //  Auto PO Details
            vm.autoPoDetails = [];
            vm.fillAutoPoDetails = function () {

                vm.requestMaterialsToBeOrdered = [];
                angular.forEach(vm.materialPoList, function (value, key) {
                    if (value.poOrderQuantity > 0)
                        vm.requestMaterialsToBeOrdered.push(value);
                    value.omitQueueInOrder
                });

                if (vm.requestMaterialsToBeOrdered.length == 0) {
                    abp.notify.warn('No More Materials To Be Ordered');
                    return;
                }

                vm.autoPoloading = true;
                vm.autoPoDetails = [];
                vm.loading = true;

                purchaseorderExtendedService.getAutoPoBasedOnCaterRequest({
                    locationRefId: vm.locationRefId,
                    shippingLocationId: vm.shippingLocationId,
                    deliveryDateExpected: vm.deliveryDateExpected,
                    requestMaterialsToBeOrdered: vm.requestMaterialsToBeOrdered
                }).success(function (result) {
                    vm.autoPoDetails = result;
                    angular.forEach(vm.autoPoDetails, function (value, key) {
                        angular.forEach(value.autoPoDto.purchaseOrderDetail, function (poDetailvalue, poKey) {
                            if (poDetailvalue.unitRefId == 0 || poDetailvalue.unitRefId == null) {
                                abp.notify.error('Unit Reference Not Exists' + poDetailvalue.materialRefName);
                            }
                        });
                    });
                    if (vm.autoPoDetails.length == 0) {
                        vm.autoPoFlag = false;
                        vm.poshowflag = true;
                        abp.notify.warn(app.localize('AutoPoMaterialNotExist'));
                        abp.message.warn(app.localize('AutoPoMaterialNotExist'));
                    }
                    else {
                        angular.forEach(vm.autoPoDetails, function (value, key) {
                            value.visibleFlag = true;
                        });
                    }
                }).finally(function () {
                    vm.autoPoloading = false;
                    vm.loading = false;
                });
            }

            vm.redirectToPurchaseOrderForm = function () {
                var autoTransferPoDetails = JSON.stringify(vm.autoPoDetails);
                $state.go('tenant.purchaseorderdetail', {
                    id: null,
                    autoPoFlag: true,
                    autoPoStatus : 'CATER', // 'TRQ',
                    autoCaterPoDetails: autoTransferPoDetails,
                    autoTransferPoDetails: [],
                });
            }

            vm.deselectAllRequest = function () {
                angular.forEach(vm.requestList, function (value, key) {
                    value.doesThisRequestToBeConvertedAsPO = false;
                });
                vm.materialPoList = [];
            }

            vm.selectAllRequest = function () {
                angular.forEach(vm.requestList, function (value, key) {
                    value.doesThisRequestToBeConvertedAsPO = true;
                });
                vm.smartLocationSelection();
                vm.getMaterialPoList();
            }

            vm.showPoList = function () {

            }

            vm.viewInterTransferDetail = function (myObject) {
                vm.loading = true;
                openView(myObject.id);
            };

            function openView(objId) {
                vm.loading = true;
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/intertransfer/request/requestViewInGrid.cshtml',
                    controller: 'tenant.views.house.transaction.intertransfer.requestViewInGrid as vm',
                    backdrop: 'static',
                    resolve: {
                        transferId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    // vm.getAll();
                }).finally(function (result) {
                    //  vm.getAll();
                });
                vm.loading = false;
            }


        }]);
})();

