﻿(function () {
    appModule.controller('tenant.views.cater.caterAutoPo.showCaterOrderMaterialDetails', [
        '$scope', '$uibModalInstance', 'data', 
        function ($scope, $modalInstance, data) {
            var vm = this;

            vm.loading = false;

            vm.data = data;
            vm.norecordsfound = false;

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

        }
    ]);
})();