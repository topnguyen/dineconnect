﻿(function () {
    appModule.controller('tenant.views.cater.caterAutoPo.caterPoPipeline', [
        '$scope', '$uibModal', '$uibModalInstance', 'data', 'caterOrderRefId', 'customerName',  
        function ($scope, $modal, $modalInstance, data, caterOrderRefId, customerName) {
            var vm = this;

            vm.loading = false;
            vm.caterOrderRefId = caterOrderRefId;
            vm.customerName = customerName;

            vm.data = data;
            vm.norecordsfound = false;

            vm.cancel = function () {
                $modalInstance.dismiss();
            };


            vm.viewPoDetail = function (purchaseOrderId) {
                if (purchaseOrderId != null)
                    openPoView(purchaseOrderId);
            };

            function openPoView(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/house/transaction/purchaseorder/podetailViewInGrid.cshtml',
                    controller: 'tenant.views.house.transaction.purchaseorder.podetailViewInGrid as vm',
                    backdrop: 'static',
                    resolve: {
                        poId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {

                }).finally(function (result) {
                });
            }

        }
    ]);
})();