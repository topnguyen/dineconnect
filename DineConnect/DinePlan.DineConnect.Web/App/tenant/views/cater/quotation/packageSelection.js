﻿(function () {
    appModule.controller('tenant.views.cater.quotation.packageselection', [
        '$scope', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.quotationHeader',
        function ($scope, $state, $stateParams, $modal, uiGridConstants, quotationService) {
            var vm = this;
            /* eslint-disable */
            vm.quotationId = $stateParams.quotationId;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = '';
            vm.packages = [];
            vm.package = null;
            vm.showPackage = false;
            vm.showCategories = false;
            vm.showCustomizarion = false;
            vm.categories = [];

            vm.getAll = function () {
                vm.loading = true;

                quotationService.getMenuItemCombos(vm.quotationId, vm.filterText)
                    .success(function (result) {
                        vm.categories = result;
                        vm.packages = [];
                        angular.forEach(vm.categories, function (p) {
                            angular.forEach(p.menuItems, function (item) {
                                vm.packages.push(item);
                            });
                        });
                    }).finally(function () {
                        vm.loading = false;
                    });
            };

            vm.getAll();

            vm.openPackage = function (item, isSelectbtn) {
                vm.showCategories = false;
                if (isSelectbtn)
                    item.isSelected = !item.isSelected;
                if (!item.isSelected)
                {
                    item.isRemoved = true;
                }                  
                else
                {
                    item.isRemoved = false;
                }
                if (item.productComboGroups.length !== 0 && item.isSelected) {
                    vm.showCategories = true;
                    vm.package = item;
                    vm.menuClass = 'selected-header';
                    vm.customizationClass = '';
                    $(".selection-header a").removeClass('selected-header');

                    vm.selectGroup(vm.package.productComboGroups[0]);
                }
            };

            vm.selectGroup = function (item, event) {
                if (event) {
                    $("a.product-group").removeClass('active');
                    $(event.currentTarget).addClass('active');
                }
                vm.menuItems = item.comboItems;
                vm.showCustomizarion = true;
            };

            vm.backToPackage = function () {
                vm.showCategories = false;
                vm.showCustomizarion = false;
                vm.package = null;
                $(".selection-header a").removeClass('selected-header');
                $(".selection-header .a-middle").addClass('selected-header');
                vm.menuClass = '';
            };

            vm.backToMenuSelection = function () {
                vm.showCustomizarion = false;
                vm.category = null;
                vm.customizationClass = '';
            };

            vm.selectItem = function (item) {
                item.isSelected = !item.isSelected;
                if (!item.isSelected) {
                    item.isRemoved = true;
                }
                else {
                    item.isRemoved = false;
                }


            };

            vm.cancel = function () {
                $state.go('tenant.detailquotation', {
                    id: vm.quotationId
                });
            };

            vm.save = function () {
                vm.saving = true;
                var input = [];

                console.log(vm.categories);
                angular.forEach(vm.categories, function (p) {
                    angular.forEach(p.menuItems, function (item) {
                        if (item.isSelected || item.isRemoved) {
                            input.push(item);
                        }
                    });
                });


                quotationService.addOrUpdateQuoteDetails(input)
                    .success(function (result) {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        vm.cancel();
                    }).finally(function () {
                        vm.saving = false;
                    });
            };

            vm.getTotal = function () {
                var total = 0;
                angular.forEach(vm.packages, function (p) {
                    if (p.isSelected) {
                        total = p.menuItem.portions[0].price * p.menuItem.quantity + total;
                    }
                });

                return total;
            };

            vm.getTax = function () {
                var total = vm.getTotal();
                return total * 0.07;
            };

            vm.getGrandTotal = function () {
                var total = vm.getTotal();
                return total * 1.07;
            };

            vm.menuItemsByCategory = [];
            vm.openMenuByCategory = function (item) {
                vm.showPackage = true;
                vm.menuItemsByCategory = item;
            };

            vm.backToCategories = function () {
                vm.showPackage = false;
                vm.menuItemsByCategory = null;
                vm.backToPackage();
            };


            vm.getSelectedClass = function (item) {
                if (item.isSelected) {
                    return 'item-selected';
                } else return '';
            };
        }]);
})();