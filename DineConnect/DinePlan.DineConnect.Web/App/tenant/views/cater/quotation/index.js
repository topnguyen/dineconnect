﻿(function () {
    appModule.controller('tenant.views.cater.quotation.index', [
        '$scope', '$state', '$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.quotationHeader',
        function ($scope, $state, $stateParams, $modal, uiGridConstants, quotationService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.cardTypeId = $stateParams.cardTypeId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Cater.QuotationHeader.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Cater.QuotationHeader.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Cater.QuotationHeader.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 150,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editQuotation(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteQuotation(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Id'),
                        field: 'id'
                    },
                    {
                        name: app.localize('CustomerName'),
                        field: 'caterCustomer.customerName',
                        cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.caterCustomerCustomerName}}</div>'
                    },
                    {
                        name: app.localize('EventDate'),
                        field: 'eventDateTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD\''
                    },
                    {
                        name: app.localize('EventTime'),
                        field: 'eventDateTime',
                        cellFilter: 'momentFormat: \'HH:mm\''
                    },
                    {
                        name: app.localize('OrderChannel'),
                        field: 'orderChannel',
                        cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.orderChannelString}}</div>'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm\''
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;

                quotationService.getQuotationHeader({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editQuotation = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createQuotation = function () {
                openCreateOrEditModal(null);
            };

            vm.deleteQuotation = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteQuotationWarning', myObject.cardNo),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            quotationService.deleteCaterQuotationHeader({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                $state.go('tenant.detailquotation', {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                quotationService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.getAll();
        }]);
})();