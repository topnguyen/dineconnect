﻿(function () {
    appModule.controller('tenant.views.cater.quotation.detail',
        ['$scope', '$window', '$state', '$stateParams', '$uibModal', 'abp.services.app.quotationHeader', 'abp.services.app.caterCustomer', 'abp.services.app.location', 'appSession',
            function ($scope, $window, $state, $stateParams, $modal, quotationService, customerService, locationService, appSession) {
                var vm = this;
                /* eslint-disable */
                vm.saving = false;
                vm.quotation = null;
                vm.quotationId = $stateParams.id;
                vm.orderChannels = [];
                vm.eventAddresses = [];
                vm.eventTime = new Date();
                vm.quoteDetails = [];

                $('input[name="EventDate"]').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('input[name="EventTime"]').datetimepicker({
                    format: 'LT'
                });

                vm.permissions = {
                    canGetOrderForOtherLocation: abp.auth.hasPermission
                        ('Pages.Tenant.Cater.QuotationHeader.Setting.GetCaterOrderForOtherLocation'),
                };

                vm.currentUserId = abp.session.userId;
                vm.defaultLocationId = appSession.user.locationRefId;

                vm.allowedLocation = [];
                vm.fillUserAllowedLocation = function () {
                    locationService.getLocationBasedOnUser({
                        userId: vm.currentUserId
                    }).success(function (result) {
                        vm.allowedLocation = result.items;
                    }).finally(function () {

                    });
                }

                vm.fillUserAllowedLocation();

                vm.save = function () {
                    vm.saving = true;
                    var date = moment($('input[name="EventDate"]')[0].value, 'DD/MM/YYYY');
                    var time = moment($('input[name="EventTime"]')[0].value, 'HH:mm');

                    vm.quotation.eventDateTime = date.add(date.utcOffset(), 'm').utc().format();
                    vm.quotation.eventTime = time.add(time.utcOffset(), 'm').utc().format();

                    quotationService.createOrUpdateQuotationHeader(vm.quotation)
                        .success(function (result) {
                            abp.notify.info(app.localize('SavedSuccessfully'));
                            if (vm.quotation.doesPOCreatedForThisCateringOrder) {
                                abp.notify.error('Already PO Generated based on this Catering Order. So please be contact the Purchase department immediately, if any changes in Menu / Quantity');
                                abp.message.info('Already PO Generated based on this Catering Order. So please be contact the Purchase department immediately, if any changes in Menu / Quantity');
                            }
                            if (vm.quotationId)
                                vm.cancel();
                            else {
                                vm.quotationId = result;
                                $state.go('tenant.detailquotation', {
                                    id: vm.quotationId
                                });
                            }
                        }).finally(function () {
                            vm.saving = false;
                        });
                };

                vm.cancel = function () {
                    $state.go('tenant.quotation');
                };

                function init() {
                    vm.loading = true;
                    quotationService.getQuotationHeaderForEdit({ id: vm.quotationId })
                        .success(function (result) {
                           
                            vm.quotation = result;

                            vm.onClientChanged();
                            if (!vm.quotationId) {
                                vm.quotation.eventDateTime = new Date();
                                vm.quotation.locationRefId = vm.defaultLocationId;
                            }
                            vm.eventTime = vm.quotation.eventDateTime;
                            vm.loading = false;
                        });

                    customerService.getOrderChannels()
                        .success(function (result) {
                            vm.orderChannels = result.items;
                        });
                    if (vm.quotationId)
                        quotationService.getQuotationDetails(vm.quotationId)
                            .success(function (result) {
                                

                                vm.quoteDetails = result;
                                var data = result;
                                var ser = 1;
                                angular.forEach(data, function (item) {
                                    item.serial = ser++;
                                    if (item.subDetails.length > 0) {
                                        item.subGridOptions = {
                                            columnDefs: [
                                                { name: 'Tag Name', field: 'menuItemName' },
                                                { name: 'Alias Code', field: 'aliasCode' },
                                                {
                                                    name: 'Price', field: 'price', cellClass: "ui-ralign",
                                                    cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >{{row.entity.price | number: 2}}</div>'
                                                },
                                                { name: 'Pax', field: 'quantity', cellClass: "ui-ralign" }
                                            ],
                                            data: item.subDetails
                                        };
                                    } else {
                                        item.subGridOptions = { disableRowExpandable: true, columnDefs: [], data: [] };
                                    }
                                });

                                vm.gridOptions.data = data;
                                vm.loading = false;
                            });
                }

                vm.openLocation = function () {
                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };

                vm.openCustomer = function (isAddTab) {
                    var url = $state.href('tenant.detailcaterCustomer', {
                        id: vm.quotation.caterCustomerId,
                        addressTab: isAddTab
                    });
                    $window.open(url, '_blank');
                };

                vm.onClientChanged = function () {
                    customerService.getCaterCustomerForEdit({ id: vm.quotation.caterCustomerId })
                        .success(function (result) {
                            vm.eventAddresses = result.caterCustomerAddresses;
                        });
                };

                vm.getEditionValue = function (item) {
                    return +item.value;
                };

                vm.openCustomerLookup = function () {
                    vm.lgroup = {
                        locations: [],
                        groups: [],
                        locationTags: [],
                        group: false,
                        single: true,
                        locationTag: false,
                        service: customerService.getAllCaterCustomer
                    };

                    var modalInstance = $modal.open({
                        templateUrl: "~/App/common/views/lookup/select.cshtml",
                        controller: "tenant.views.common.lookup.select as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.lgroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        if (result.locations.length > 0) {
                            vm.quotation.caterCustomerId = result.locations[0].id;
                            vm.quotation.caterCustomerName = result.locations[0].name;

                            vm.onClientChanged();
                        }
                    });
                };

                vm.addOrder = function () {
                    $state.go('tenant.packageselection', {
                        quotationId: vm.quotationId
                    });
                };

                vm.gridOptions = {
                    expandableRowTemplate: '<div ui-grid="row.entity.subGridOptions"></div>',
                    //expandableRowScope: {
                    //    subGridVariable: 'subGridScopeVariable'
                    //},
                    columnDefs: [

                        {
                            name: '',
                            field: 'serial',
                        },
                        {
                            name: app.localize('ItemName'),
                            field: 'groupName'
                        },
                        {
                            name: app.localize('Qty'),
                            field: 'quantity',
                            cellClass: "ui-ralign"
                        },
                        {
                            name: app.localize('Price'),
                            field: 'price',
                            cellClass: "ui-ralign",
                            cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >{{row.entity.price | number: 2}}</div>'
                        },
                        {
                            name: app.localize('Amount'),
                            field: 'amount',
                            cellClass: "ui-ralign",
                            cellTemplate: '<div class="ui-grid-cell-contents ui-ralign" >{{row.entity.amount | number: 2}}</div>'
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;

                        gridApi.expandable.on.rowExpandedStateChanged($scope, function (row) {
                            row.expandedRowHeight = row.entity.subDetails.length * 30 + 50;
                        });
                    },
                    data: []
                };

                init();
            }
        ]);
})();