﻿(function () {
    appModule.controller('tenant.views.cater.customer.detail',
        ['$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.caterCustomer',
            function ($scope, $state, $stateParams, $modal, caterCustomerService) {
                var vm = this;

                vm.saving = false;
                vm.caterCustomer = null;
                vm.caterCustomerId = $stateParams.id;
                vm.addressTab = $stateParams.addressTab;
                vm.orderChannels = [];

                vm.address = { caterCustomerId: vm.caterCustomerId };

                vm.save = function () {
                    vm.saving = true;

                    caterCustomerService.createOrUpdateCaterCustomer(vm.caterCustomer)
                        .success(function (result) {
                            abp.notify.info(app.localize('SavedSuccessfully'));

                            vm.cancel();
                        }).finally(function () {
                            vm.saving = false;
                        });
                };

                vm.cancel = function () {
                    $state.go('tenant.caterCustomer');
                };

                function init() {
                    vm.loading = true;
                    caterCustomerService.getCaterCustomerForEdit({ id: vm.caterCustomerId })
                        .success(function (result) {
                            vm.loading = false;
                            vm.caterCustomer = result;
                        });

                    caterCustomerService.getOrderChannels()
                        .success(function (result) {
                            vm.orderChannels = result.items;
                        });
                }

                init();

                vm.openLocation = function () {
                    var modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };

                var addressIndex = -1;
                vm.createAddress = function () {
                    if (addressIndex >= 0) {
                        vm.caterCustomer.caterCustomerAddresses[addressIndex] = vm.address;
                    }
                    else
                        vm.caterCustomer.caterCustomerAddresses.push(vm.address);
                    vm.address = { caterCustomerId: vm.caterCustomerId };
                    addressIndex = -1;
                };

                vm.removeAddress = function (index) {
                    vm.caterCustomer.caterCustomerAddresses.splice(index, 1);
                };

                vm.editAddress = function (index, address) {
                    addressIndex = index;
                    vm.address = address;
                };

                vm.getEditionValue = function (item) {
                    return +item.value;
                };
            }
        ]);
})();