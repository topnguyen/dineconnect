﻿(function () {
    appModule.controller('tenant.views.template.detail', [
        '$scope', '$state', '$stateParams', 'abp.services.app.emailTemplate',
        function ($scope, $state,$stateParams, appService) {
            var vm = this;
            vm.tid = $stateParams.id;

            vm.save = function () {
                vm.saving = true;
                appService.createOrUpdateTemplate({
                    template: vm.template
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $state.go('tenant.templates');
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $state.go('tenant.templates');
            };

            function init() {

                appService.getTemplateForEdit({
                    id: vm.tid
                }).success(function (result) {
                    vm.template = result.template;

                });
            }
            init();
        }
    ]);
})();