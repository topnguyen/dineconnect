﻿(function () {
    appModule.controller('tenant.views.engage.reservation.reservation', [
        '$scope', "$uibModal", '$state', '$stateParams', 'abp.services.app.reservation', 'abp.services.app.commonLookup', 'hotkeys',
        "abp.services.app.member", 'abp.services.app.location', 'appSession',
        function ($scope, $modal, $state, $stateParams, reservationService, commonLookupService, hotKeys, memberService, locationService, appSession) {
            var vm = this;
            
            vm.saving = false;
            vm.reservation = null;
			$scope.existall = true;

			vm.remainderFlag = true;

			$scope.setMyFocus = function (str) {
			    var element = $window.document.getElementById(str);
			    element.focus();
			};

			vm.reservationId = $stateParams.reservationId;

			vm.memberList = [];
			vm.locationList = [];
			vm.selectedPax = "";

			vm.selectedMember = [];

			vm.memberDetail = app.localize("SelectMember");
			vm.memberAddress = "";

		
			vm.selectedLocation = [];

			$scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
			$scope.maxDate = moment().add(60, 'days');  //  +60 or +90 based on Sysadmin Table

			$scope.formats = ['YYYY-MMM-DD h:mm A', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
			$scope.format = $scope.formats[0];
          

			vm.locationDetail = app.localize('Select') + " " + app.localize('Location');
			vm.locationAddress = '';
			vm.locationCode = 0;

			vm.choseLocation = function () {
			    vm.choseMember();
			};

			function callMemberSelection(result) {
			    var otherInstance = $modal.open({
			        templateUrl: "~/App/tenant/views/wheel/order/memberSearch.cshtml",
			        controller: "tenant.views.wheel.order.memberSearch as vm",
			        backdrop: "static",
			        keyboard: false,
			        resolve: {
			            member: function () {
			                return result;
			            }
			        }
			    });
			    otherInstance.result.then(function (result) {
			        if (result != null) {

			            vm.selectedMember = result;
			            vm.memberDetail = vm.selectedMember.memberCode + "/" + vm.selectedMember.name;
			            vm.memberAddress = vm.selectedMember.address;
			            vm.locationCode = vm.selectedMember.locationRefId;
			            if (vm.locationCode != null) {
			                locationService.getLocationById({
			                    id: vm.locationCode
			                }).success(function (result) {
			                    vm.selectedLocation = result;
			                    vm.locationDetail = result.name;
			                }).finally(function () {
			                });
			            }

			        }

			        hotKeys.add({
			            combo: "enter",
			            allowIn: ["INPUT"],
			            callback: function (event, hotkey) {
			                vm.addItemToOrder();
			            }
			        });

			        vm.getmemberDetails();
			    });
			}
			vm.choseMember = function () {

			    vm.selectedMember = [];
			    hotKeys.del("enter");
			    hotKeys.del("*");
			    var modalInstance = $modal.open({
			        templateUrl: "~/App/tenant/views/wheel/order/selectMember.cshtml",
			        controller: "tenant.views.wheel.order.selectMember as vm",
			        backdrop: "static",
			        keyboard: false
			    });

			    modalInstance.result.then(function (result) {

			        callMemberSelection(result);

			    });

			};

			var requestParams = {
			    skipCount: 0,
			    maxResultCount: app.consts.grid.defaultPageSize,
			    sorting: null
			};

			vm.getmemberDetails = function () {
			    vm.loading = true;
			    memberService.getAll({
			        skipCount: requestParams.skipCount,
			        maxResultCount: requestParams.maxResultCount,
			        sorting: requestParams.sorting,
			        filter: vm.filterText
			    }).success(function (result) {
			        vm.memberList = result.items;
			    }).finally(function () {
			        vm.loading = false;
			    });
			};


			$scope.convert = function (thedate) {
			    var tempstr = thedate.toString();
			    var newstr = tempstr.toString().replace(/GMT.*/g, "");
			    newstr = newstr + " UTC";
			    return new Date(newstr);
			};


			
            vm.save = function () {
			    if ($scope.existall == false)
			        return;

			

			    if (vm.selectedMember == null || vm.selectedMember == "") {
			        abp.notify.warn(app.localize('Select') + " " + app.localize("Member"));
			        return;
			    }

			    if (vm.selectedLocation == null || vm.selectedLocation == "") {
			        abp.notify.warn(app.localize('LocationErr'));
			        return;
			    }

			    if (vm.isUndefinedOrNull(vm.dateselected)) {
			        abp.notify.warn(app.localize('Select') + " " + app.localize("Date"));
			        return;
			    }

			    if (vm.selectedPax == "" || vm.selectedPax == 0)
			    {
			        abp.notify.warn(app.localize('Pax') + " " + app.localize('Required'));
			        return;
			    }

			    var remainderTimes = "";
			    angular.forEach(vm.remainderTimes, function (value, key) {
			        remainderTimes = remainderTimes +  value;
			    });


			    var remainderMediums = "";
			    var errorFlag = false;

			    angular.forEach(vm.remainderMediums, function (value, key) {
			        remainderMediums = remainderMediums + value;
			        if (value == 0) {   //  Email Verification
			            var email = vm.selectedMember.emailId;
			            //if (email.indexOf("@") < 0)
			            //{
			            //    errorFlag = true;
			            //}
			            if (email == null || email == '' || email.indexOf("@") < 0)
			            {
			                abp.notify.warn(app.localize('MemberEmailWrong', email));
			                errorFlag = true;
			            }
			        }
			        if (value == 1) {   //  Phone Verification
			            var phone = vm.selectedMember.memberCode;
			            var isnum = /^\d+$/.test(phone);
			            if (isnum == false) {
			                errorFlag = true;
			                abp.notify.warn(app.localize('MemberPhoneWrong',phone));
			            }
			         
			        }
			    });

			    if (errorFlag)
			    {
			        return;
			    }


			    if (vm.remainderFlag)
			    {
			        if (remainderMediums == "") {
			            abp.notify.warn(app.localize('Remainder') + " " + app.localize("Required"));
			            return;
			        }

			        if (remainderTimes == "")
			        {
			            abp.notify.warn(app.localize('RemainderTime') + " " + app.localize("Required"));
			            return;
			        }

			    }
                
			    vm.saving = true;


                
                vm.reservation.connectMemberId = vm.selectedMember.id;
                vm.reservation.locationRefId = vm.selectedLocation.id;
                vm.reservation.reservationDate = $scope.convert(vm.dateselected); // moment(vm.reservationDate, $scope.format);
                vm.reservation.pax = vm.selectedPax;
                vm.reservation.remainderRequired = vm.remainderFlag;
                vm.reservation.remainderTimes = remainderTimes;
                vm.reservation.remainderMediums = remainderMediums;
                vm.reservation.remarks = vm.selectedNotes;
                vm.reservation.status = app.localize('Pending');

                reservationService.createOrUpdateReservation({
                    reservation: vm.reservation
                }).success(function () {
                    abp.notify.info('\' Reservation \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                    vm.cancel();

                });
            };

            vm.clear = function () {
                abp.message.confirm(
                    app.localize("ClearItemsWarning"),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            clearAll();
                        }
                    }
                );
            };

            function clearAll() {
                vm.selectedMember = [];

                vm.memberDetail = app.localize("SelectMember");
                vm.memberAddress = "";

                vm.selectedLocation = [];

                vm.locationDetail = app.localize('Select') + " " + app.localize('Location');
                vm.locationAddress = '';
                vm.locationCode = 0;

                vm.selectedTime = null;
                vm.selectedPax = "";
                vm.selectedNotes = "";

                vm.noofQuantities = 0;
                vm.existreservationDetails = [];
            }

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };


            vm.cancel = function () {
                $state.go("tenant.reservation");
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
			 vm.locations = [];
			 vm.getLocations = function () {
			     locationService.getAll({
			         skipCount: 0,
			         maxResultCount: app.consts.grid.defaultPageSize,
			         sorting: null
			     }).success(function (result) {
			         vm.locations = $.parseJSON(JSON.stringify(result.items));
			     }).finally(function (result) {

			     });
			 };



			 hotKeys.add({
			     combo: "*",
			     allowIn: ["INPUT"],
			     description: app.localize("SelectMember"),
			     callback: function (event, hotkey) {
			         vm.choseMember();
			     }
			 });

			 vm.refremainderMediums = [];
			 function fillDropDownRemainderMediums() {
			     reservationService.getRemainderMediumsForCombobox({}).success(function (result) {
			         vm.refremainderMediums = result.items;
			     });
			 }

			 vm.refremaindertimes = [];
			 function fillDropDownRemainderTimes() {
			     reservationService.getRemainderTimesForCombobox({}).success(function (result) {
			         vm.refremaindertimes = result.items;
			     });
			 }


			 function init() {
	            vm.getLocations();
	            vm.getmemberDetails();
	            fillDropDownRemainderMediums();
	            fillDropDownRemainderTimes();

                reservationService.getReservationForEdit({
                    Id: vm.reservationId
                }).success(function (result) {
                    vm.reservation = result.reservation;

                    vm.reservationDate = moment().format($scope.format);

             
                });
            }

			 init();

			 $scope.setNow = function (thedate)  {

			     var dateSel = thedate;

			     var monthNames = ["January", "February", "March", "April", "May", "June",
                     "July", "August", "September", "October", "November", "December"
			     ];

			     year = dateSel.getFullYear();
			     month = dateSel.getMonth().toString().length === 1 ? '0' + (dateSel.getMonth() + 1).toString() : dateSel.getMonth() + 1;
			     month = monthNames[month-1];
			     date = dateSel.getDate().toString().length === 1 ? '0' + (dateSel.getDate()).toString() : dateSel.getDate();
			     hours = dateSel.getHours().toString().length === 1 ? '0' + dateSel.getHours().toString() : dateSel.getHours();
			     minutes = dateSel.getMinutes().toString().length === 1 ? '0' + dateSel.getMinutes().toString() : dateSel.getMinutes();
			     seconds = dateSel.getSeconds().toString().length === 1 ? '0' + dateSel.getSeconds().toString() : dateSel.getSeconds();

			     //var AM_PM ;
			     //if (hours < 12) {
			     //    AM_PM = "AM";
			     //} else {
			     //    AM_PM = "PM";
			     //}

			     formattedDateTime = year + '-' + month + '-' + date + " " + hours + ':' + minutes + ':' + seconds; //+ ' ' //+ AM_PM;

			     return formattedDateTime;
			 };

			 $scope.beforeRender = function ($view, $dates, $leftDate, $upDate, $rightDate) {
			     var activeDate = moment().subtract(1, $view).add(1, 'minute');
			     var maxDate = moment().subtract(-90, 'days');

			     $dates.filter(function(date) {
			         return date.localDateValue() <= activeDate.valueOf()

			     }).forEach(function(date) {
			         date.selectable = false;
			     });
			 };



			 vm.dateChange = function () {
			     if (vm.dateselected) {
			         var resDate = $scope.convert(vm.dateselected);

			         var selDate = $scope.setNow(vm.dateselected);

			         vm.loading = true;

			         var startDate = moment(resDate);
			         var endDate = resDate;

			         var locationname = vm.selectedLocation.name;


			         reservationService.getAll({
			             skipCount: requestParams.skipCount,
			             maxResultCount: requestParams.maxResultCount,
			             sorting: 'ReservationDate Asc',
			             locationRefName: vm.locationRefName,
			             startDate: startDate,
			             endDate: endDate,
			             dateFilterForAlert : true
			         }).success(function (result) {
			             vm.existreservationDetails = result.items;
			             $('input[type="datetime"').val(selDate);

			         }).finally(function () {
			             vm.loading = false;
			         });
			     }
			 }

        }
    ]);
})();

