﻿(function () {
    appModule.controller('tenant.views.engage.reservation.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.reservation',
        function ($scope, $state, $modal, uiGridConstants, reservationService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.isDeleted = false;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Engage.Reservation.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Engage.Reservation.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Engage.Reservation.Delete')
            };

            vm.dateFilterApplied = false;

            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.maxDate = moment().add(60, 'days');  //  +60 or +90 based on Sysadmin Table

            var todayAsString = moment().format($scope.format);
            var fromdayAsString = moment().subtract(7, 'days').calendar();

            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString,
                maxDate: $scope.maxDate
            });

            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeOptions.max = $scope.maxDate;
            vm.dateRangeOptions.maxDate = $scope.maxDate;

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editReservation(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"!grid.appScope.isDeleted && grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteReservation(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.isDeleted && grid.appScope.permissions.edit\" ng-click=\"grid.appScope.activateItem(row.entity)\">" + app.localize("Revert") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Date'),
                        field: 'reservationDate',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm\'',
                    },
                    {
                        name: app.localize('Location'),
                        field: 'locationRefName'
                    },
                    {
                        name: app.localize('Code'),
                        field: 'memberCode'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'memberName'
                    },
                    {
                        name: app.localize('Pax'),
                        field: 'pax',
                        cellClass: 'ui-ralign'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.editReservation = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createReservation = function () {
                openCreateOrEditModal(null);
            };

            vm.deleteReservation = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteReservationWarning', myObject.code),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            reservationService.deleteReservation({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                //var modalInstance = $modal.open({
                //    templateUrl: '~/App/tenant/views/engage/reservation/reservation/createOrEditModal.cshtml',
                //    controller: 'tenant.views.engage.reservation.createOrEditModal as vm',
                //    backdrop: 'static',
                //	keyboard: false,
                //    resolve: {
                //        reservationId: function () {
                //            return objId;
                //        }
                //    }
                //});

                //modalInstance.result.then(function (result) {
                //    vm.getAll();
                //});

                $state.go("tenant.reservationdetail", {
                    reservationId: objId,
                });
            }

            vm.getAll = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;

                if (vm.dateFilterApplied) {
                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }

                reservationService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    memberCode: vm.memberCode,
                    locationRefName: vm.locationRefName,
                    startDate: startDate,
                    endDate: endDate,
                    isDeleted: vm.isDeleted
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.exportToExcel = function () {
                vm.loading = true;

                var startDate = null;
                var endDate = null;

                if (vm.dateFilterApplied) {
                    startDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    endDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }

                reservationService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    memberCode: vm.memberCode,
                    locationRefName: vm.locationRefName,
                    startDate: startDate,
                    endDate: endDate
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    }).finally(function () {
                        vm.loading = false;
                    });
            };

            vm.activateItem = function (myObject) {
                reservationService.activateItem({
                    id: myObject.id
                }).success(function (result) {
                    abp.notify.success(app.localize("Successfully"));
                    vm.getAll();
                });
            };

            vm.getAll();
        }]);
})();