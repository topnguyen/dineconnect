﻿(function () {
    appModule.controller("tenant.views.engage.pointcampaign",
        [
            "$scope",
            "$state",
            "$stateParams",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.pointCampaign",
            "abp.services.app.membershipTier",
            function ($scope, $state, $stateParams, $modal, uiGridConstants, pointCampaignService, membershipTierService) {

                var vm = this;

                let func = fnc($scope, $state, $modal, uiGridConstants,
                    app, vm, abp, pointCampaignService, membershipTierService);
                func.actions.init(App);
                func.actions.loadMembershipTierList();

                func.actions.loadModifierTypeList().then(function () {
                    func.actions.setWorkMode($stateParams).then(function () {
                        if (func.data.workMode == func.constants.workMode.Edit) {
                            func.actions.getPointCampaignForEdit();
                        }
                    });
                });

                vm.a = func.actions;
                vm.d = func.data;
                vm.s = func.constants;
                vm.l = func.lists;
                vm.e = func.uiEvents;
            }
        ]);

    let fnc = function ($scope, $state, $modal, uiGridConstants, app, vm, abp, pointCampaignService, membershipTierService) {

        let act = {
            goBackToList: function () {
                $state.go("tenant.loyaltyProgram", {});
            },
            findModifierType: function (typeCode) {
                let modType = f.lists.modifierTypes.find(function (t) { return t.code == typeCode });
                // console.log(modType);
                if (modType) {
                    return modType;
                } else {
                    return null;
                }
            },
            validatePointCampaign: function () {
                let checkNum = function (val) {
                    try {
                        let number = parseFloat(val);
                        return !isNaN(number);
                    } catch (e) {
                        return false;
                    }

                };

                if (!f.data.pointCampaign.name) {
                    abp.notify.error(app.localize('NameIsRequired'));
                    return false;
                }

                if (!f.data.pointCampaign.startDate) {
                    abp.notify.error(`${app.localize('InvalidInput')}: ${app.localize('StartDate')}`);
                    return false;
                }

                if (!f.data.pointCampaign.endDate && !f.data.neverExpires) {
                    abp.notify.error(`${app.localize('InvalidInput')}: ${app.localize('EndDate')}`);
                    return false;
                }

                if (!(f.data.pointCampaign.requiredAmount &&
                    checkNum(f.data.pointCampaign.requiredAmount))) {
                    abp.notify.error(`${app.localize('InvalidInput')}: ${app.localize('RequiredAmount')}`);
                    return false;
                }

                if (!(f.data.pointCampaign.offeredPoints &&
                    checkNum(f.data.pointCampaign.offeredPoints))) {
                    abp.notify.error(`${app.localize('InvalidInput')}: ${app.localize('OfferedPoints')}`);
                    return false;
                }

                let Break = {};
                try {
                    f.data.pointCampaign.modifiers.forEach(function (m) {
                        let mod = DataDefinition.PointCampaignModifier(m);

                        if (!checkNum(mod.modifyingFactor) || mod.modifyingFactor <=0) {
                            abp.notify.error(`${app.localize('InvalidInput')}: ${app.localize('Modifier')} : ${app.localize('ModifyingFactor')}`);
                            throw Break;
                        }

                        if (mod.modifierTypeCode == DataDefinition.ModifierTypeCode.TMPRD) {
                            if (!mod.startDate || !mod.endDate) {
                                abp.notify.error(`${app.localize('InvalidInput')}: ${app.localize('Modifier')} : ${app.localize('StartOrEndDate')}`);
                                throw Break;
                            }
                        } else if (mod.modifierTypeCode == DataDefinition.ModifierTypeCode.MTIER) {
                            if (!mod.membershipTier) {
                                abp.notify.error(`${app.localize('InvalidInput')}: ${app.localize('Modifier')} : ${app.localize('MembershipTier')}`);
                                throw Break;
                            }
                        }
                    })
                } catch (e) {
                    return false;
                }

                //abp.notify.success('Good');
                //return false;
                return true;
            }
        }

        let f = {
            data: {
                currencyCode: abp.features.getValue('DinePlan.DineConnect.Connect.Currency'),
                pointCampaign: DataDefinition.PointCampaign(),
                neverExpires: true,
                modifiers: [],
                workMode: ''
            },
            lists: {
                modifierTypes: null,
                membershipTiers: null
            },
            constants: {
                modifierTypeCode: DataDefinition.ModifierTypeCode,
                workMode: DataDefinition.WorkMode
            },
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                loadModifierTypeList: function () {
                    return new Promise(function (resolve, reject) {
                        pointCampaignService.getModifierTypes()
                            .success(function (result) {
                                f.lists.modifierTypes = result;
                                resolve();
                            });
                    });
                },
                loadMembershipTierList: function () {
                    membershipTierService.getAll({
                        skipCount: 0,
                        maxResultCount: app.consts.grid.defaultPageSize,
                        sorting: 'id',
                        isDeleted: false
                    }).success(function (result) {
                        f.lists.membershipTiers = result.items;
                        // console.log(result.items);
                    });
                },
                goBackToList: function () {
                    act.goBackToList()
                },
                addModifier: function (typeCode) {
                    if (f.actions.checkCanAddOnlyOnce(typeCode)) {
                        let mtype = f.lists.modifierTypes
                            .find(function (t) { return t.code === typeCode });
                        abp.notify.warn(app.localize('PointModAddOnlyOnce', mtype.name));
                        return;
                    }

                    let dd = DataDefinition;
                    let mod = dd.PointCampaignModifier();
                    let modType = act.findModifierType(typeCode);
                    mod.modifierTypeId = modType.id;
                    mod.modifierTypeCode = modType.code;
                    mod.campaignId = f.data.pointCampaign.id;
                    if (typeCode === f.constants.modifierTypeCode.TMPRD) {
                        mod.startDate = new Date();
                        mod.endDate = new Date();
                    }
                    f.data.pointCampaign.modifiers.push(mod);
                },
                removeModifier: function (modifier) {
                    let index = f.data.pointCampaign.modifiers.indexOf(modifier);
                    f.data.pointCampaign.modifiers.splice(index, 1);
                },
                setWorkMode: function (stateParams) {
                    return new Promise(function (resolve, reject) {
                        if (stateParams.id) {
                            f.data.workMode = f.constants.workMode.Edit;
                            f.data.pointCampaign.id = stateParams.id;
                        } else {
                            f.data.workMode = f.constants.workMode.Add;
                        }
                        resolve();
                    });
                },
                savePointCampaign: function () {

                    if (!act.validatePointCampaign()) return;

                    if (f.data.neverExpires) {
                        f.data.pointCampaign.endDate = null;
                    } else {
                        // f.data.pointCampaign.endDate = f.data.pointCampaign.endDateTmp;
                    }

                    pointCampaignService.createOrUpdatePointCampaign({ pointCampaign: f.data.pointCampaign })
                        .success(function (result) {
                            if (f.data.workMode == f.constants.workMode.Edit) {
                                abp.notify.success(app.localize('SavedSuccessfully'));
                            } else {
                                act.goBackToList();
                            }
                        });
                },
                checkCanAddOnlyOnce: function (modifierTypeCode) {
                    if (
                        modifierTypeCode === f.constants.modifierTypeCode.BDMTH ||
                        modifierTypeCode === f.constants.modifierTypeCode.TMPRD) {
                        let found = f.data.pointCampaign.modifiers.some(function (t) {
                            return t.modifierTypeCode === modifierTypeCode;
                        })
                        return found;
                    }
                    return false;
                },
                getPointCampaignForEdit: function () {
                    pointCampaignService.getPointCampaignForUpdate({id: f.data.pointCampaign.id})
                        .success(function (result) {
                            if (!result.pointCampaign.endDate) f.data.neverExpires = true;
                            f.data.pointCampaign = DataDefinition
                                .PointCampaign(result.pointCampaign, f.lists.modifierTypes, f.lists.membershipTiers);
                            // console.log(f.data.pointCampaign);
                        });
                }
            },
            uiEvents: {
                membershipTierSelected: function (mdf) {
                    mdf.membershipTierId = mdf.membershipTier.id; 
                }
            }
        };

        return f;
    };


    let DataDefinition = {
        WorkMode: {
            Add: 'ADD',
            Edit: 'EDIT'
        },
        PointCampaign: function (data, modTypes,tiers) {
            let getTrueDate = function (val) {
                if (val) {
                    if (val.indexOf('T') >= 0) {
                        return new Date(val.split('T')[0]);
                    } else {
                        if (isDate(val)) {
                            return val;
                        } else {
                            return new Date();
                        }
                    }
                } else {
                    return null;
                }
            };

            let getModifiers = function (modifiers) {
                let output = [];
                modifiers.forEach(function (m) {
                    m.startDate = getTrueDate(m.startDate);
                    m.endDate = getTrueDate(m.endDate);
                    if (modTypes && modTypes.length > 0) {
                        let modType = modTypes.find(t=>t.id === m.modifierTypeId)
                        m.modifierTypeCode = modType.code;
                    }
                    m.membershipTier = tiers.find(t => t.id === m.membershipTierId);
                    output.push(DataDefinition.PointCampaignModifier(m));
                });
                return output;
            };

            return {
                id: data? data.id:0,
                name: data ? data.name :null,
                description: data ? data.description :null,
                offeredPoints: data ? data.offeredPoints :1,
                requiredAmount: data ? data.requiredAmount : 3.0,
                startDate: data ? getTrueDate(data.startDate):new Date(),
                endDate: data ? getTrueDate(data.endDate) : null,
                endDateTmp: data ? getTrueDate(data.endDate):new Date(),
                modifiers: data ? getModifiers(data.modifiers) :[]
            }
        },
        PointCampaignModifier: function (data) {
            return {
                id: data ? data.id : 0,
                campaignId: data? data.campaignId: 0,
                modifierTypeId: data ? data.modifierTypeId : 0,
                modifierTypeCode: data ? data.modifierTypeCode : null,
                modifyingFactor: data ? data.modifyingFactor: 2.0,
                startDate: data? data.startDate:null,
                endDate: data ? data.endDate : null,
                membershipTierId: data ? data.membershipTierId : null,
                membershipTier: data ? data.membershipTier : null,
                randomId: Math.floor(Math.random() * 1000) + 1
            }
        },
        PointCampaignModifierType: function (data) {
            return {
                id: data ? data.id :0,
                code: data ? data.code :null,
                name: data ? data.name :null
            }
        },
        ModifierTypeCode: {
            BDMTH: 'BDMTH',
            TMPRD: 'TMPRD',
            MTIER: 'MTIER'
        }
    };

}());
