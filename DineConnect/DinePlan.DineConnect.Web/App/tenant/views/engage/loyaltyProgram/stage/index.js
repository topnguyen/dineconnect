﻿
(function () {
    appModule.controller("tenant.views.engage.loyaltyProgram.stage.index", [
        "$scope", "$state", "$uibModal", "uiGridConstants", "abp.services.app.stage", "abp.services.app.connectLookup", 
        function ($scope, $state, $modal, uiGridConstants, stageService, lookService) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission("Pages.Tenant.Engage.Stage.Create"),
                edit: abp.auth.hasPermission("Pages.Tenant.Engage.Stage.Edit"),
                delete: abp.auth.hasPermission("Pages.Tenant.Engage.Stage.Delete")
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

           

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: 'Display',
                        enableSorting: false,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.stageDetails(row.entity)"><i class="fa fa-search"></i></button>' +
                            '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.editStage(row.entity)"><i class="fa fa-pencil"></i></button>' +
                            '</div>'
                    },
                    {
                        name: app.localize("Name"),
                        field: "name"
                    },
                    {
                        name: app.localize("PointsExpiryAllowed"),
                        field: "pointsExpiryAllowed"
                    }, {
                        name: app.localize("RedemptionValue"),
                        field: "redemptionValue"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };


            vm.getAll = function () {
                vm.loading = true;
                stageService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.createStage = function () {
                createOrEdit(null);
            };

            vm.editStage = function (myObj) {
                createOrEdit(myObj.id);
            };

            function createOrEdit(objId) {
                $state.go("tenant.createoreditstage", {
                    id: objId
                });
            }

            vm.getAll();
        }
    ]);
})();