﻿(function () {
    appModule.controller('tenant.views.engage.loyaltyProgram.stage.createedit', [
        function ($scope, $modal, $state, $stateParams, stageService, programService, lookService) {
            var vm = this;
            vm.saving = false;
            vm.stage = new Object();
            vm.stage.Id = $stateParams.id;
            vm.stage.pointAccumulations = [];
            vm.showRules = false;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            $scope.existall = true;
            $scope.minDate = moment();
            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.isExpiryChecked = function () {
                return $("#Expiry").is(':checked');
            }

            vm.isPointExpiryChecked = function () {
                return $("#PointsExpiryAllowed").is(':checked');
            }

            $scope.builder = 
                app.createBuilder(
                    {
                        condition: "AND",
                        rules: [
                            {
                                id: "NumberOfVisit",
                                operator: "greater_or_equal",
                                value: 0
                            }
                        ]
                    },
                    angular.fromJson
                    (
                        [
                            {
                                "id": "NumberOfVisit",
                                "label": "NumberOfVisit",
                                "operators": [
                                    "equal",
                                    "less",
                                    "less_or_equal",
                                    "greater",
                                    "greater_or_equal"
                                ],
                                "type": "double",
                                "validation": {
                                    "min": 0,
                                    "step": 0.1
                                }
                            }
                            
                        ]
                    )
                );
            $scope.beforeRender = function ($view, $dates, $leftDate, $upDate, $rightDate) {
                var activeDate = moment().subtract(1, $view).add(1, 'minute');

                $dates.filter(function (date) {
                    return date.localDateValue() <= activeDate.valueOf();

                }).forEach(function (date) {
                    date.selectable = false;
                });
            };

            function init() {
                if (vm.stage.Id) {
                    vm.saving = true;
                    stageService.getStage({
                        id: vm.stage.Id
                    }).success(function (result) {
                        console.log(result);

                        vm.stage.name = result.name;
                        vm.stage.loyaltyProgramId = result.loyaltyProgramId;
                        vm.stage.condition = result.condition;
                        vm.stage.conditionDayValue = result.conditionDayValue;
                        vm.stage.minRedemptionPoints = result.minRedemptionPoints;
                        vm.stage.pointsExpiryAllowed = result.pointsExpiryAllowed;
                        vm.stage.pointExpiryDays = result.pointExpiryDays;
                        vm.stage.redemptionValue = result.redemptionValue;
                        vm.stage.previousStageId = result.previousStageId;

                        vm.stage.pointAccumulations = result.pointAccumulations;
                    }).finally(function () {
                        vm.loadPreviousStages();
                        vm.saving = false;
                    });
                }
                lookService.getPointAccumulationTypes({}).success(function (result) {
                    vm.pointAccTypes = result.items;
                    vm.pointAccTypes.unshift({ value: "0", displayText: app.localize('NotAssigned') });
                });

                programService.getForCombobox({}).success(function (result) {
                    vm.loyaltyPrograms = result.items;
                });
            }
            init();

            vm.openRules = function () {
                vm.showRules = !vm.showRules;
            }

            vm.loadPreviousStages = function () {
                stageService.getForCombobox({ loyaltyProgramId: vm.stage.loyaltyProgramId, stageId: vm.stage.Id }).success(function (result) {
                    vm.previousStages = result.items;
                });
            }

            vm.getComboboxValue = function (item) {
                return parseInt(item.value);
            };

            $scope.getPointAccText = function (id) {
                var result = "";
                vm.pointAccTypes.forEach(function (entry) {
                    if (entry.value == id) {
                        result = entry.displayText;
                    }
                });
                return result;
            };

            vm.save = function () {
                vm.saving = true;

                vm.stage.pointAccumulations.push({
                    'point': vm.stage.pointAccumulations.point,
                    'roundOff': vm.stage.pointAccumulations.roundOff,
                    'pAccTypeId': vm.stage.pointAccumulations.pAccTypeId
                });

                stageService.createOrUpdateStage({
                    id: vm.stage.Id,
                    loyaltyProgramId: vm.stage.loyaltyProgramId,
                    name: vm.stage.name,
                    condition: angular.toJson($scope.builder.builder.getRules()),
                    conditionDayValue: vm.stage.conditionDayValue,
                    pointsExpiryAllowed: vm.stage.pointsExpiryAllowed,
                    minRedemptionPoints: vm.stage.minRedemptionPoints,
                    redemptionValue: vm.stage.redemptionValue,
                    pointExpiryDays: vm.stage.pointExpiryDays,
                    previousStageId: vm.stage.previousStageId,
                    pointAccumulations: vm.stage.pointAccumulations
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $state.go('tenant.stage');
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $state.go('tenant.stage');
            };
        }
    ]);
})();
