﻿(function () {
    appModule.controller('tenant.views.engage.loyaltyProgram.createedit', [
        '$scope', '$uibModal', '$state', '$stateParams', 'abp.services.app.loyaltyProgram', "abp.services.app.connectLookup",
        function ($scope, $modal, $state, $stateParams, programService, lookService) {
            var vm = this;
            vm.saving = false;
            vm.program = new Object();
            vm.program.Id = $stateParams.id;
            vm.program.stages = [];
            vm.showRules = false;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            $scope.existall = true;
            $scope.minDate = moment();
            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.isExpiryChecked = function () {
                return $("#Expiry").is(':checked');
            }

            vm.isPointExpiryChecked = function () {
                return $("#PointsExpiryAllowed").is(':checked');
            }

            $scope.beforeRender = function ($view, $dates, $leftDate, $upDate, $rightDate) {
                var activeDate = moment().subtract(1, $view).add(1, 'minute');

                $dates.filter(function (date) {
                    return date.localDateValue() <= activeDate.valueOf();

                }).forEach(function (date) {
                    date.selectable = false;
                });
            };

            function init() {
                if (vm.program.Id) {
                    vm.saving = true;
                    programService.getProgram({
                        id: vm.program.Id
                    }).success(function (result) {
                        console.log(result);

                        vm.program.name = result.name;
                        vm.program.expiry = result.expiry;
                        vm.program.expiryDays = result.expiryDays;
                        vm.program.startDate = result.startDate;

                        vm.program.stages = result.stages;
                    }).finally(function () {
                        vm.saving = false;
                    });
                }
                lookService.getPointAccumulationTypes({}).success(function (result) {
                    vm.pointAccTypes = result.items;
                    vm.pointAccTypes.unshift({ value: "0", displayText: app.localize('NotAssigned') });
                });
            }
            init();

            vm.openRules = function () {
                vm.showRules = !vm.showRules;
            }

            vm.getPointAccValue = function (item) {
                return parseInt(item.value);
            };

            $scope.getPointAccText = function (id) {
                var result = "";
                vm.pointAccTypes.forEach(function (entry) {
                    if (entry.value == id) {
                        result = entry.displayText;
                    }
                });
                return result;
            };

            vm.addStage = function () {
                vm.program.stages.push({
                    'name': vm.program.stages.name,
                    'minRedemptionPoints': vm.program.stages.minRedemptionPoints,
                    'redemptionValue': vm.program.stages.redemptionValue,
                    'condition': angular.toJson($scope.builder.builder.getRules()),
                    'pointsExpiryAllowed': vm.program.stages.pointsExpiryAllowed,
                    'pointExpiryDays': vm.program.stages.pointExpiryDays,
                    'conditionDayValue': vm.program.stages.conditionDayValue
                });

                vm.program.stages[vm.program.stages.length - 1].pointAccumulations = [];

                vm.program.stages[vm.program.stages.length - 1].pointAccumulations.push({
                    'pAccTypeId': vm.program.stages.pointAccumulations.pAccTypeId,
                    'point': vm.program.stages.pointAccumulations.point,
                    'roundOff': vm.program.stages.pointAccumulations.roundOff
                });
            }

            vm.removeStage = function (stageIndex) {
                vm.program.stages.splice(stageIndex, 1);
            }

            vm.save = function () {
                vm.saving = true;

                programService.createOrUpdateProgram({
                    id: vm.program.Id,
                    name: vm.program.name,
                    expiry: vm.program.expiry,
                    expiryDays: vm.program.expiryDays,
                    startDate: vm.program.startDate
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $state.go('tenant.loyaltyProgram');
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $state.go('tenant.loyaltyProgram');
            };
        }
    ]);
})();
