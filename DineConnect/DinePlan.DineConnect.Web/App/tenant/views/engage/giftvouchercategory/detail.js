﻿
(function () {
    appModule.controller('tenant.views.engage.giftvouchercategory.detail', [
        '$scope', '$state', '$stateParams', "$uibModal", 'abp.services.app.giftVoucherCategory',
        function ($scope, $state, $stateParams, $modal, giftvouchercategoryService) {
            var vm = this;

            vm.saving = false;
            vm.giftvouchercategory = null;
            vm.giftvouchercategoryId = $stateParams.id;

            vm.save = function () {
                vm.saving = true;
                vm.giftvouchercategory.code = vm.giftvouchercategory.code.toUpperCase();
                vm.giftvouchercategory.name = vm.giftvouchercategory.name.toUpperCase();
                giftvouchercategoryService.createOrUpdateGiftVoucherCategory({
                    giftVoucherCategory: vm.giftvouchercategory
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.giftvouchercategory');
            };

            function init() {
                giftvouchercategoryService.getGiftVoucherCategoryForEdit({
                    id: vm.giftvouchercategoryId
                }).success(function (result) {
                    vm.giftvouchercategory = result.giftVoucherCategory;
                });
            }
            init();
        }
    ]);
})();

