﻿(function () {
    appModule.controller('tenant.views.engage.giftvouchertype.import', [
        '$scope', 'appSession', '$stateParams', 'uiGridConstants', 'abp.services.app.giftVoucherType', '$uibModalInstance', 'FileUploader',
        function ($scope, appSession, $stateParams, uiGridConstants, giftvouchertypeService, $uibModalInstance, fileUploader) {
            var vm = this;

            vm.giftvouchertypeId = $stateParams.id; 

            vm.uploader = new fileUploader({
                url: abp.appPath + 'Import/ImportGiftVoucher',
                formData: [{ id: vm.giftvouchertypeId == "" ? 0 : vm.giftvouchertypeId }],
                queueLimit: 1,
            });

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.error != null) {
                    abp.message.warn(response.error.message);
                    $uibModalInstance.close();
                    vm.loading = false;
                    return;
                }
                if (response.result.invalidGiftVouchers.totalCount != 0 || response.result.exceedLimitationGiftVouchers.totalCount != 0) {

                    vm.error = true;
                    vm.noError = false;
                    vm.voucherCodes.data = response.result.invalidGiftVouchers.items.concat(response.result.exceedLimitationGiftVouchers.items);
                    vm.duplicateRecord = response.result.invalidGiftVouchers.totalCount;
                    vm.digitExceedLimitation = response.result.exceedLimitationGiftVouchers.totalCount;
                }
                else {
                    $uibModalInstance.close(response.result);
                    abp.notify.info(app.localize("FINISHED"));
                }
                vm.loading = false;
            };

            vm.voucherCodes = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('VoucherCode'),
                        field: 'voucher'
                    }
                ],
                data: []
            };

            vm.importpath = abp.appPath + 'Import/ImportGiftVoucherTemplate';

            vm.save = function () {
                vm.loading = true;
                vm.uploader.uploadAll();
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            init = function () {
                vm.error = false;
                vm.noError = true;
            }

            vm.exportErrorToExcel = function () {
                vm.loading = true;
                var data = vm.voucherCodes.data;
                giftvouchertypeService.exprotErrorToExcel(data)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
                vm.loading = false;
            }

            init();
        }
    ]);
})();
