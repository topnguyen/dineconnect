﻿
(function () {
    appModule.controller('tenant.views.engage.voucher.index', [
        '$scope', '$state','$stateParams', '$uibModal', 'uiGridConstants', 'abp.services.app.giftVoucherType',
        function ($scope, $state, $stateParams, $modal, uiGridConstants, giftvouchertypeService) {

            var vm = this;
            vm.voucherTypeId = $stateParams.voucherTypeId;
            vm.voucherId = $stateParams.voucherId; 

            vm.cancel = function () {
                $state.go('tenant.detailgiftvouchertype', {
                    id: vm.voucherTypeId
                });
            };

            vm.save = function (argOption) {
                vm.saving = true;
                giftvouchertypeService.updateGiftVoucher({
                    giftVoucher: vm.giftvoucher,
                }).success(function (result) {
                    if (result.isSuccess) {
                        console.log(result);
                        abp.notify.info('\' GiftVoucherType \'' + app.localize('SavedSuccessfully'));
                    }
                    else {
                        abp.message.warn(result.message);
                    }
                }).finally(function () {
                    vm.saving = false;
                });
            };

            init = function() {
                giftvouchertypeService.getGiftVoucherForEdit({
                    id: vm.voucherId
                }).success(function (result) {
                    console.log(result);
                    vm.giftvoucher = result.giftVoucher;
                });
            }

            init();
        }]);
})();

