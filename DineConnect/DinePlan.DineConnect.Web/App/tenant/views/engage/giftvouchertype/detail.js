﻿(function () {
    appModule.controller('tenant.views.engage.giftvouchertype.detail', [
        '$scope', 'uiGridConstants', '$state', '$stateParams', '$uibModal', 'abp.services.app.giftVoucherType', 'abp.services.app.engageLook', 'abp.services.app.location', 'abp.services.app.giftVoucherCategory',
        function ($scope, uiGridConstants, $state, $stateParams, $modal, giftvouchertypeService, engagelook, locationService, giftvouchercategoryService) {
            var vm = this;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Engage.GiftVoucherType.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Engage.GiftVoucherType.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Engage.GiftVoucherType.Delete')
            };

            vm.saving = false;
            vm.giftvouchertype = null;
            vm.giftvouchertypeId = $stateParams.id; 
            vm.uilimit = 20;
            
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            $scope.beforeRender = function ($view, $dates, $leftDate, $upDate, $rightDate) {
                var activeDate = moment().subtract(1, $view).add(1, 'minute');
                var maxDate = moment().subtract(-90, 'days');

                $dates.filter(function (date) {
                    return date.localDateValue() <= activeDate.valueOf();

                }).forEach(function (date) {
                    date.selectable = false;
                });
            };

            vm.requestlocations = '';

            $scope.existall = true;
            $scope.minDate = moment().add(0, 'days');  // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            
            vm.voucherTypes = [];

            function fillFieldTypes() {
                engagelook.getVoucherTypes({}).success(function (result) {
                    vm.voucherTypes = result.items;
                });
            }

            vm.save = function (argOption) {
                vm.saving = true;
                giftvouchertypeService.createOrUpdateGiftVoucherType({
                    giftVoucherType: vm.giftvouchertype,
                    locationGroup: vm.locationGroup,
                    giftVoucherItems: vm.userGridOptions.data
                }).success(function (result) {
                    vm.giftvouchertype.id = result.id;
                    vm.giftvouchertypeId = result.id;
                    $stateParams.id = result.id;
                    console.log(result);
                    abp.notify.info('\' GiftVoucherType \'' + app.localize('SavedSuccessfully'));
                    if (argOption == 1)
                        $state.go('tenant.giftvouchertype');
                    else
                        init();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            saveGiftVoucherToDb = function (giftVoucherItems) {
                vm.saving = true;
                giftvouchertypeService.importGiftVouchers({
                    giftVoucherTypeId: vm.giftvouchertype.id,
                    giftVoucherItems: giftVoucherItems
                }).success(function (result) {
                    console.log(result);
                    abp.notify.info('\' GiftVoucherType \'' + app.localize('SavedSuccessfully'));
                    init();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.giftvouchertype');
            };

            vm.intiailizeOpenLocation = function() {
                vm.locationGroup = app.createLocationInputForCreate();
            };

            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }   
                    }
                });
                modalInstance.result.then(function (result) {
                    vm.locationGroup = result;
                });
            }

            vm.importFromExcel = function () {
                var modalInstance = $modal.open({
                    data: [
                        id = vm.giftvouchertypeId
                    ],
                    templateUrl: "~/App/tenant/views/engage/giftvouchertype/import.cshtml",
                    controller: "tenant.views.engage.giftvouchertype.import as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        
                    }
                });
                modalInstance.result.then(function (result) {
                    saveGiftVoucherToDb(result.validGiftVouchers.items);
                });
            }

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: " ",
                        width: 60,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '   <input class="row-checkbox" type="checkbox" ng-model="fieldView" ng-checked="checked" ng-init="checked=row.entity.required" ng-disabled="checked" ng-click="grid.appScope.view(fieldView,row.entity);getExternalScopes().showMe(row.entity.required)">' +
                            '</div>'
                    },
                    {
                        name: app.localize('Actions'),
                        enableSorting: true,
                        width: 120,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.edit" ng-click="grid.appScope.editGiftVoucher(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.delete" ng-click="grid.appScope.deleteGiftVoucher(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.isDeleted && grid.appScope.permissions.edit" ng-click="grid.appScope.activateItem(row.entity)">' + app.localize('Activate') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
					{
					    name: app.localize('Voucher'),
					    field: 'voucher'
                    },
                    {
                        name: app.localize('CreateDate&Time'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'DD/MM/YYYY HH:mm\''
                    },
                    {
                        name: app.localize('Claimed'),
                        field: 'claimed',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.claimed" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.claimed" class="label label-default">' + app.localize('No') + '</span>' +
                            '</div>'
                    }

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                giftvouchertypeService.getVouchers({
                    isDeleted: vm.isDeleted,
                    voucherType: vm.giftvouchertypeId,
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    operation: 'SEARCH'
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.validGiftVouchers.totalCount;
                    vm.userGridOptions.data = result.validGiftVouchers.items;
                    vm.dashBoard = result.dashBoardDto;
                    $('.row-checkbox').each(function (idx, item) {
                        if (item.checked) {
                            $(item).click();
                        }
                    });
                    vm.isSelectAll = false;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.generate = function () {
                vm.loading = true;
                giftvouchertypeService.generateVoucherForType({
                    giftVoucherType: vm.giftvouchertype,
                    locationGroup: vm.locationGroup
                    })
                    .success(function (result) {
                        console.log("hello" + result);
                        if (result == '') {
                            abp.notify.success(app.localize('Done'));
                            vm.getAll();
                        } else {
                            abp.notify.error(result);
                        }
                    }).finally(function () {
                        vm.loading = false;
                    });
            };

            vm.deleteGiftVoucher = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteGiftVoucherWarning', myObject.voucher),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            giftvouchertypeService.deleteGiftVoucher({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.exportToExcel = function () {
                giftvouchertypeService.getAllGiftVoucherToExcel({
                    voucherType: vm.giftvouchertypeId
                }).success(function (result) {
                        app.downloadTempFile(result);
                    });
            }

            vm.editGiftVoucher = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.selectAll = function () {
                $('.row-checkbox').each(function (idx, item) {
                    if (vm.isSelectAll) {
                        if (!(item.checked)) {
                            $(item).click();
                        }
                    } else {
                        if (item.checked) {
                            $(item).click();
                        }
                    }
                });
            }

            vm.deleteAll = function () {
                abp.message.confirm(
                    app.localize('DeleteSelectedGiftVouchersWarning'),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            var ids = [];
                            $('.row-checkbox').each(function (idx, item) {
                                if ((item.checked)) {
                                    var id = vm.userGridOptions.data[idx].id;
                                    ids.push(id);
                                }
                            });
                            if (ids.length != 0) {
                                giftvouchertypeService.deleteMultiGiftVoucher(ids).success(function () {
                                    vm.getAll();
                                    abp.notify.success(app.localize('SuccessfullyDeleted'));
                                });
                            }
                        }
                    }
                );
            }

            vm.activeAll = function () {
                var ids = [];
                $('.row-checkbox').each(function (idx, item) {
                    if ((item.checked)) {
                        var id = vm.userGridOptions.data[idx].id;
                        ids.push(id);
                    }
                });
                if (ids.length != 0) {
                    giftvouchertypeService.activateMultiGiftVoucher(ids).success(function () {
                        vm.getAll();
                        abp.notify.success(app.localize('Successfully'));
                    });
                }
            }

            function openCreateOrEditModal(objId) {
                console.log(objId);

                $state.go("tenant.detailgiftvoucher", {
                    voucherTypeId: vm.giftvouchertypeId,
                    voucherId: objId
                });
            }

            vm.activateItem = function (myObject) {
                giftvouchertypeService.activateGiftVoucher({
                    id: myObject.id
                }).success(function (result) {
                    abp.notify.success(app.localize("Successfully"));
                    vm.getAll();
                });
            };

            vm.vouchercategorys = [];
            vm.getVoucherCategories = function () {
                giftvouchercategoryService.getVoucherCategorys({}).success(function (result) {
                    vm.vouchercategorys = result.items;
                }).finally(function (result) {
                });
            };
            vm.getVoucherCategories();

            function init() {
                fillFieldTypes();
                giftvouchertypeService.getGiftVoucherTypeForEdit({
                    Id: vm.giftvouchertypeId
                }).success(function (result) {
                    if (vm.giftvouchertypeId > 0) {
                        vm.getAll();
                    }
                    vm.locationGroup = result.locationGroup;
                    vm.giftvouchertype = result.giftVoucherType;
                    if (vm.giftvouchertype.id != null) {
                        vm.showvoucherstab = true;
                        vm.uilimit = null;
                    }
                    else {
                        vm.showvoucherstab = false;
                        vm.uilimit = 20;
                    }
                    vm.requestlocations = result.locations;
                    vm.giftvouchertype.validTill = moment(vm.giftvouchertype.validTill).format($scope.format);
                });
            }

            init();
        }
    ]);
})();

