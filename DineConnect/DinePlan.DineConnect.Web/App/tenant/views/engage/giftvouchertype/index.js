﻿(function () {
    appModule.controller('tenant.views.engage.giftvouchertype.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.giftVoucherType',
        function ($scope, $state, $modal, uiGridConstants, giftvouchertypeService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            vm.isDeleted = false;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Engage.GiftVoucherType.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Engage.GiftVoucherType.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Engage.GiftVoucherType.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: 'Display',
                        enableSorting: false,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button class="btn btn-default btn-xs" ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.edit"  ng-click="grid.appScope.showVoucherDetails(row.entity)"><i class="fa fa-search"></i></button>' +
                            '  <button class="btn btn-default btn-xs" ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.edit" ng-click="grid.appScope.editGiftVoucherType(row.entity)"><i class="fa fa-pencil"></i></button>' +
                            '  <button class="btn btn-default btn-xs" ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.delete" ng-click="grid.appScope.deleteGiftVoucherType(row.entity)"><i class="fa fa-trash"></i></button>' +
                            '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.isDeleted && grid.appScope.permissions.edit" ng-click="grid.appScope.activateItem(row.entity)" title="Revert"><i class="fa fa-undo"></i></button>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('Validity'),
                        field: 'validTill',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD\''
                    },
                    {
                        name: app.localize('VoucherCount'),
                        field: 'voucherCount'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\''
                    },
                    {
                        name: app.localize('Generate'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.generateVoucher(row.entity)" ng-if="!row.entity.autoAttach" class="btn btn-default btn-xs" title="' + app.localize('Generate') + '"><i class="fa fa-dashcube"></i></button>' +
                            '</div>'
                    }

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                giftvouchertypeService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    isDeleted: vm.isDeleted,
                    locationGroup: vm.locationGroup
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.showVoucherDetails = function (objId) {
                $state.go("tenant.searchVouchers", {
                    id: objId.id
                });
            };

            vm.generateVoucher = function (myObj) {
                vm.loading = true;
                myObj.isUpdateFlag = true;
                giftvouchertypeService.generateVoucherForType({
                        giftVoucherType: myObj
                    }).success(function (result) {
                        console.log("hello" + result);
                        if (result == '') {
                            abp.notify.success(app.localize('Done'));
                        } else {
                            abp.notify.error(result);
                        }
                    }).finally(function () {
                        vm.loading = false;
                    });
            };

            vm.editGiftVoucherType = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createGiftVoucherType = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteGiftVoucherType = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteGiftVoucherTypeWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            giftvouchertypeService.deleteGiftVoucherType({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                console.log(objId);

                $state.go("tenant.detailgiftvouchertype", {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                giftvouchertypeService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            vm.intiailizeOpenLocation = function () {
                vm.locationGroup = app.createLocationInputForSearch();
            };
            vm.intiailizeOpenLocation();

            vm.openLocation = function () {
                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {

                    if (result.locations.length > 0) {
                        vm.currentLocation = result.locations[0];
                    }
                    else if (result.groups.length > 0) {
                        vm.currentLocation = result.groups[0];
                    }
                    else if (result.locationTags.length > 0) {
                        vm.currentLocation = result.locationTags[0];
                    }
                    else {
                        vm.currentLocation = null;
                    }
                });
            };
            vm.clear = function () {
                vm.currentLocation = null;
                vm.filterText = null;
                vm.categoryInput = null;
                vm.menuTypeInput = 0;
                vm.categoryName = null;
                vm.intiailizeOpenLocation();


                vm.getAll();
            };
            vm.activateItem = function (myObject) {
                giftvouchertypeService.activateItem({
                    id: myObject.id
                }).success(function (result) {
                    abp.notify.success(app.localize("Successfully"));
                    vm.getAll();
                });
            };

            vm.getAll();
        }]);
})();