﻿(function () {

    appModule.controller("tenant.views.engage.voucherSelector",
        [
            "$scope",
            "$state",
            "$uibModalInstance",
            "$uibModal",
            "uiGridConstants",
            "existings",
            "multiSelect",
            "abp.services.app.giftVoucherType",
            function ($scope, $state, $modalInstance, $modal,
                uiGridConstants, existings, multiSelect, giftVoucherTypeService) {

                var vm = this;

                let func = fnc($scope, $state, $modalInstance, $modal, uiGridConstants, giftVoucherTypeService, app, vm);
                func.actions.init(App);

                func.data.existingSelections = existings;
                func.data.multiSelect = (multiSelect) ? multiSelect : false;

                vm.a = func.actions;
                vm.d = func.data;
                vm.l = func.lists;

                func.controls.gridOptions = gridUtil($scope, app, vm, uiGridConstants).gridOptions;
                vm.grid = func.controls.gridOptions;

                func.actions.getItems();
            }
        ]);


    let fnc = function ($scope, $state, $modalInstance, $modal, uiGridConstants,
        giftVoucherTypeService, app, vm) {
        let act = {
            xxx: function () {

            }
        }


        var requestParams = {
            skipCount: 0,
            maxResultCount: app.consts.grid.defaultPageSize,
            sorting: 'id'
        };


        let f = {
            data: {
                filterText: '',
                existingSelections: null,
                multiSelect: false,
                ui: {
                    loadings: {
                        modalBody: false
                    }
                }
            },
            controls: {
                gridOptions: function () {
                    try {
                        return gridUtil($scope, app, vm, uiGridConstants).gridOptions;
                    } catch (e) {
                        return null;    
                    } 
                }()
            },
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                getItems: function () {
                    f.data.ui.loadings.modalBody = true;
                    giftVoucherTypeService.getAll({
                        skipCount: 0,
                        maxResultCount: app.consts.grid.defaultPageSize,
                        sorting: 'id',
                        filter: f.data.filterText,
                        isDeleted: false
                    })
                        .success(function (result) {
                            f.controls.gridOptions.totalItems = result.totalCount;
                            f.controls.gridOptions.data = result.items;
                        })
                        .finally(function () {
                            f.data.ui.loadings.modalBody = false;
                        });
                },
                modalCancel: function () {
                    $modalInstance.dismiss('cancel');
                },
                modalOk: function () {
                    $modalInstance.close(vm.l.itemList.items);
                }
            },
            lists: {
                itemList: new ObjectList()
            }
        };

        return f;
    };


    let gridUtil = function ($scope, app, vm, uiGridConstants) {
        let rowTemplate = `
        <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" 
            class="ui-grid-cell" 
            ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  
            ui-grid-cell>
        </div>
            `;
        let columnDefTemplates = [];
        return {
            gridOptions: {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: rowTemplate,
                multiSelect: vm.d.multiSelect,
                columnDefs: [
                    {
                        name: app.localize('Name'),
                        width: 250,
                        field: 'name'
                    },
                    {
                        name: app.localize('Valid Until'),
                        width: 250,
                        field: 'validTill'
                    }
                ],
                rowHeight: 30,
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' +
                                sortColumns[0].sort.direction;
                        }

                        vm.a.getItems();
                    });
                    gridApi.pagination.on.paginationChanged($scope,
                        function (pageNumber, pageSize) {
                            requestParams.skipCount = (pageNumber - 1) * pageSize;
                            requestParams.maxResultCount = pageSize;

                            vm.a.getItems();
                        });

                    gridApi.selection.on.rowSelectionChanged($scope,
                        function (row) {
                            if (row.isSelected) {
                                vm.l.itemList.add(row.entity);
                            } else {
                                vm.l.itemList.remove(row.entity.id);
                            }
                        });

                },
                data: []
            }
        };
    };

    function ObjectList() {
        return {
            add: function (myObj) {
                if (myObj != null) {
                    var myIndex = -1;

                    if (this.items != null) {
                        this.items.some(function (value, key) {
                            if (value.id == myObj.id) {
                                myIndex = value;
                                return true;
                            }
                        });
                    }

                    if (myIndex === -1) {
                        this.items.push(myObj);
                    } else {
                        //if (vm.autoSelection == false)
                        //    abp.notify.error(myObj.name + "  " + app.localize("Available"));
                    }
                }
            },
            remove: function (id) {
                for (let i = 0; i < this.items.length; i++) {
                    const obj = this.items[i];

                    if (obj.id == id) {
                        this.items.splice(i, 1);
                    }
                }
            },
            items: []
        }
    }

    let DataDefinition = {
        ObjectList: function (ol) {
            return {
                getObjectList: function () { return ol; },
                add: function (item) { ol.add(item); },
                remove: function (item) { ol.remove(item); },
                items: ol.items
            };
        }
    };
}());