﻿(function () {
    appModule.controller("tenant.views.engage.member.index", [
        "$scope", "$state", "$uibModal", "uiGridConstants", "abp.services.app.member",
        function ($scope, $state, $modal, uiGridConstants, memberService) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission("Pages.Tenant.Engage.Member.Create"),
                edit: abp.auth.hasPermission("Pages.Tenant.Engage.Member.Edit"),
                'delete': abp.auth.hasPermission("Pages.Tenant.Engage.Member.Delete")
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: 'Display',
                        enableSorting: false,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.memberDetails(row.entity)"><i class="fa fa-search"></i></button>' +
                            '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.editMember(row.entity)"><i class="fa fa-pencil"></i></button>' +
                            '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.editPoints(row.entity)"><i class="fa fa-bookmark-o"></i></button>' +
                            '</div>'
                    },
                    {
                        name: app.localize("MemberCode"),
                        field: "memberCode"
                    },
                    {
                        name: app.localize("Name"),
                        field: "name"
                    },
                    {
                        name: app.localize("City"),
                        field: "city"
                    }, {
                        name: app.localize("TotalPoints"),
                        field: "totalPoints"
                    }, {
                        name: app.localize("lastVisitDate"),
                        field: "lastVisitDate",
                        cellFilter: "momentFormat: 'YYYY-MM-DD HH:MM'"
                    },
                    {
                        name: app.localize("CreatedDate"),
                        field: "creationTime",
                        cellFilter: "momentFormat: 'YYYY-MM-DD'"
                    },
                    {
                        name: app.localize('Orders'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.getOrders(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-arrow-circle-right"></i></button>' +
                            '</div>',
                        enableSorting: false
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                memberService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editMember = function (myObj) {
                createOrEdit(myObj.id);
            };

            vm.editPoints = function (myObj) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/engage/member/editPoints.cshtml',
                    controller: 'tenant.views.engage.member.editPoints as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        memberId: function () {
                            return myObj.id;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            };

            vm.openMemberAccountList = function (obj) {
                $state.go('tenant.memberaccount', {mid:encodeURIComponent('MID:' + obj.id)});
            };

            vm.createMember = function () {
                createOrEdit(null);
            };
            vm.memberDetails = function (myObj) {
                memberDetails(myObj.id);
            };
            vm.deleteMember = function (myObject) {
                abp.message.confirm(
                    app.localize("DeleteMemberWarning", myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            memberService.deleteMember({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize("SuccessfullyDeleted"));
                            });
                        }
                    }
                );
            };

            vm.getOrders = function (model) {
                $modal.open({
                    templateUrl: '~/App/tenant/views/engage/member/orders.cshtml',
                    controller: 'tenant.views.connect.member.orders as vm',
                    backdrop: 'static',
                    resolve: {
                        member: function () {
                            return model;
                        }
                    }
                });
            };

            function createOrEdit(objId) {
                $state.go("tenant.createoreditmember", {
                    id: objId
                });
            }

            function memberDetails(objId) {
                $state.go("tenant.memberdetails", {
                    id: objId
                });
            }

            vm.exportToExcel = function () {
                abp.ui.setBusy('#MyLoginForm');

                memberService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        abp.ui.clearBusy('#MyLoginForm');
                    });
            };

            vm.getAll();
        }
    ]);
})();