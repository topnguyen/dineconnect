﻿
(function () {

    appModule.controller("tenant.views.engage.accounttrans",
        [
            "$scope",
            "$state",
            "$stateParams",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.memberAccount",
            function ($scope, $state, $stateParams, $modal, uiGridConstants, memberAccountService) {

                var vm = this;

                let func = fnc($scope, $state, $modal, uiGridConstants, app, vm, memberAccountService);
                func.actions.init(App);

                gridUtil($scope, app, vm, uiGridConstants, func.controls.gridAcctTrans);

                vm.d = func.data;
                vm.c = func.controls;
                vm.a = func.actions;

                if ($stateParams.id) {
                    func.data.accountId = $stateParams.id;
                }

                func.actions.getAccount();
                func.actions.getAccountTransactions();

            }
        ]);


    let fnc = function ($scope, $state, $modal, uiGridConstants, app, vm, memberAccountService) {

        let act = {
            xxx: function () {

            }
        }

        let f = {
            data: {
                accountTransactions: [],
                gridAcctTransLoading: false,
                filterText: '',
                isDeleted: false,
                accountId: 0,
                account: null
            },
            controls: {
                gridAcctTrans: DataDefinition.GridOptions()
            },
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                getAccountTransactions: function () {
                    f.data.gridAcctTransLoading = true;
                    memberAccountService.getTransactions({
                        skipCount: 0,
                        maxResultCount: app.consts.grid.defaultPageSize,
                        sorting: 'id',
                        filter: f.data.filterText,
                        isDeleted: f.data.isDeleted,
                        accountId: f.data.accountId
                    })
                        .success(function (result) {
                            f.controls.gridAcctTrans.data = result.items;
                        })
                        .finally(function () {
                            f.data.gridAcctTransLoading = false;
                        });
                },
                getAccount: function () {
                    memberAccountService.getMemberAccountForUpdate({ id: f.data.accountId })
                        .success(function (result) {
                            f.data.account = result.memberAccount;
                        });
                },
                goBackToAccountList: function () {
                    $state.go('tenant.memberaccount', {});
                }
            }
        };

        return f;
    };

    let DataDefinition = {
        GridOptions: function() {
            return {
                enableHorizontalScrollbar: false,
                enableVerticalScrollbar: false,
                paginationPageSizes: 0,
                paginationPageSize: 0,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: null,
                rowTemplate: '',
                columnDefs: [],
                onRegisterApi: {},
                data: []
            }
        }
    };

    let gridUtil = function ($scope, app, vm, uiGridConstants, uiGridOptions) {
        let rowTemplate = `
        <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" 
            class="ui-grid-cell" 
            ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  
            ui-grid-cell>
        </div>
            `;

        uiGridOptions.enableHorizontalScrollbar= uiGridConstants.scrollbars.WHEN_NEEDED;
        uiGridOptions.enableVerticalScrollbar= uiGridConstants.scrollbars.WHEN_NEEDED;
        uiGridOptions.paginationPageSizes = app.consts.grid.defaultPageSizes;
        uiGridOptions.paginationPageSize = app.consts.grid.defaultPageSize;
        uiGridOptions.useExternalPagination = true;
        uiGridOptions.useExternalSorting = true;
        uiGridOptions.appScopeProvider = vm;
        uiGridOptions.rowTemplate = rowTemplate;
        uiGridOptions.columnDefs = [
            {
                name: app.localize('TransactionDate'),
                field: 'creationTime',
                width:200
            },
            {
                name: app.localize('Description'),
                field: 'description'
            },
            {
                name: app.localize('Reference'),
                field: 'reference',
                width: 180
            },
            {
                name: app.localize('Amount'),
                field: 'transactionalUnitCount',
                width: 120
            }
        ];
        uiGridOptions.onRegisterApi = function (gridApi) {
            $scope.gridApi = gridApi;
            $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                if (!sortColumns.length || !sortColumns[0].field) {
                    requestParams.sorting = null;
                } else {
                    requestParams.sorting = sortColumns[0].field + ' ' +
                        sortColumns[0].sort.direction;
                }

                vm.a.getAccountTransactions();
            });
            gridApi.pagination.on.paginationChanged($scope,
                function (pageNumber, pageSize) {
                    requestParams.skipCount = (pageNumber - 1) * pageSize;
                    requestParams.maxResultCount = pageSize;

                    vm.a.getAccountTransactions();
                });
        };
        uiGridOptions.data = [];

        return uiGridOptions;
    };

}());
