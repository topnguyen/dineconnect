﻿(function () {
    appModule.controller('tenant.views.connect.member.orders', [
        '$scope', '$uibModalInstance', 'member', 'abp.services.app.member',
        function ($scope, $modalInstance, member, memberService) {
            var vm = this;
            vm.member = member;
            vm.saving = false;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

           
            vm.close = function () {
                $modalInstance.dismiss();
            };
         
			
            function init() {
                memberService.getOrders({
                    id: vm.member.id
            }).success(function (result) {
                    vm.memberOrder = result.orders;
                    vm.memberCancelOrder = result.cancel;
                    });
            }
            init();
        }
    ]);
})();

