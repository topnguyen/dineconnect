﻿(function () {

    appModule.controller("tenant.views.engage.membershiptiers",
        [
            "$scope",
            "$state",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.membershipTier",
            function ($scope, $state, $modal, uiGridConstants, membershipTierService) {

                var vm = this;

                let func = fnc($scope, $state, $modal, uiGridConstants, app, vm, membershipTierService);
                func.actions.init(App);

                func.controlls.gridTier = gridUtil($scope, app, vm, uiGridConstants).gridOptions;

                vm.a = func.actions;
                vm.c = func.controlls;
                vm.d = func.data;

                func.data.permissions = func.actions.readPermissions();
                func.actions.getMembershipTiers();
            }
        ]);


    let fnc = function ($scope, $state, $modal, uiGridConstants, app, vm, membershipTierService) {

        let act = {
            xxx: function () {

            }
        }

        let f = {
            data: {
                gridTierloading: false,
                isDeleted: false,
                filterText: '',
                permissions: null
            },
            controlls: {
                gridTier: DataDefinition.GridOptions()
            },
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                openCreatePage: function () {
                    $state.go('tenant.membershiptier');
                },
                openEditPage: function (obj) {
                    $state.go('tenant.membershiptier', {id:obj.id});
                },
                deleteMembershipTier: function (obj) {
                    abp.message.confirm(
                        app.localize('DeleteLocationWarning', obj.name),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                membershipTierService.deleteMembershipTier({ id: obj.id })
                                    .success(function () {
                                        abp.notify.success(app.localize('SuccessfullyDeleted'));
                                        f.actions.getMembershipTiers();
                                    });
                            }
                        }
                    )
                },
                readPermissions: function () {
                    return {
                        create: abp.auth.hasPermission('Pages.Tenant.Engage.MembershipTier.Create'),
                        edit: abp.auth.hasPermission('Pages.Tenant.Engage.MembershipTier.Edit'),
                        delete: abp.auth.hasPermission('Pages.Tenant.Engage.MembershipTier.Delete')
                    }
                },
                getMembershipTiers: function () {
                    f.data.gridTierloading = true;

                    membershipTierService.getAll({
                        skipCount: 0,
                        maxResultCount: app.consts.grid.defaultPageSize,
                        sorting: 'id',
                        filter: f.data.filterText,
                        isDeleted: f.data.isDeleted
                    })
                        .success(function (result) {
                            f.controlls.gridTier.data = result.items;
                        })
                        .finally(function () {
                            f.data.gridTierloading = false;
                        });
                }
            }
        };

        return f;
    };


    let DataDefinition = {
        GridOptions: function () {
            return {
                enableHorizontalScrollbar: false,
                enableVerticalScrollbar: false,
                paginationPageSizes: 0,
                paginationPageSize: 0,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: null,
                rowTemplate: '',
                columnDefs: [],
                onRegisterApi: {},
                data: []
            }
        }
    };


    let gridUtil = function ($scope, app, vm, uiGridConstants) {
        let rowTemplate = `
        <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" 
            class="ui-grid-cell" 
            ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  
            ui-grid-cell>
        </div>
            `;
        let columnDefTemplates = [
            `
        <div class="ui-grid-cell-contents">
            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>
                <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" 
                    aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-cog"></i> ${app.localize("Actions")} <span class="caret"></span>
                </button>
                <ul uib-dropdown-menu>
                        <li><a ng-if="!grid.appScope.d.isDeleted && grid.appScope.d.permissions.edit"
                            ng-click="grid.appScope.a.openEditPage(row.entity)">${app.localize("Edit")}</a></li>
                        <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.d.permissions.delete"
                            ng-click="grid.appScope.a.deleteMembershipTier(row.entity)">${app.localize("Delete")}</a></li>
                </ul>
            </div>
        </div>`
        ];
        return {
            gridOptions: {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: rowTemplate,
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate: columnDefTemplates[0]
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('Description'),
                        field: 'description'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' +
                                sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope,
                        function (pageNumber, pageSize) {
                            requestParams.skipCount = (pageNumber - 1) * pageSize;
                            requestParams.maxResultCount = pageSize;

                            vm.getAll();
                        });
                },
                data: []
            }
        };
    };

    

}());
