﻿(function () {

    appModule.controller("tenant.views.engage.membershiptier",
        [
            "$scope",
            "$state",
            "$stateParams",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.membershipTier",
            function ($scope, $state, $stateParams, $modal, uiGridConstants, membershipTierService) {

                var vm = this;

                let func = fnc($scope, $state, $modal, uiGridConstants, membershipTierService, app, vm);
                func.actions.init(App);

                vm.a = func.actions;
                vm.d = func.data;
                vm.s = func.constants;

                func.actions.setWorkMode($stateParams).then(function () {
                    if (func.data.workMode == func.constants.workMode.Edit) {
                        func.actions.getMembershipTierForEdit();
                    }
                });
                
            }
        ]);


    let fnc = function ($scope, $state, $modal, uiGridConstants, membershipTierService, app, vm) {

        let dd = DataDefinition;

        let act = {
            goBackToList: function () {
                $state.go("tenant.membershiptiers", {});
            },
            validateMembershipTier: function () {
                let checkNum = function (val) {
                    try {
                        let number = parseFloat(val);
                        return !isNaN(number);
                    } catch (e) {
                        return false;
                    }

                };

                if (!f.data.membershipTier.name) {
                    abp.notify.error(app.localize('NameIsRequired'));
                    return false;
                }

                if (!checkNum(f.data.membershipTier.price)) {
                    abp.notify.error(`${app.localize('InvalidInput')}: ${app.localize('Price')}`);
                    return false;
                }

                if (!checkNum(f.data.membershipTier.rewardingPoints)) {
                    abp.notify.error(`${app.localize('InvalidInput')}: ${app.localize('RewardingPoints')}`);
                    return false;
                }

                if (!checkNum(f.data.membershipTier.rewardingVoucherId) ||
                    f.data.membershipTier.rewardingVoucherId <= 0) {
                    abp.notify.error(`${app.localize('InvalidInput')}: ${app.localize('RewardingVouchers')}`);
                    return false;
                }

                if (!checkNum(f.data.membershipTier.rewardingVoucherCount)) {
                    abp.notify.error(`${app.localize('InvalidInput')}: ${app.localize('RewardingVoucherCount')}`);
                    return false;
                }

                if (!checkNum(f.data.membershipTier.cashPaymentDiscount)) {
                    abp.notify.error(`${app.localize('InvalidInput')}: ${app.localize('CashPaymentDiscount')}`);
                    return false;
                }

                if (!checkNum(f.data.membershipTier.creditCardPaymentDiscount)) {
                    abp.notify.error(`${app.localize('InvalidInput')}: ${app.localize('CreditCardPaymentDiscount')}`);
                    return false;
                }

                //abp.notify.success('Good');
                //return false;
                return true;
            }
        }

        let f = {
            data: {
                membershipTier: dd.MembershipTier(),
                workMode: ''
            },
            constants: {
                workMode: DataDefinition.WorkMode
            },
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                setWorkMode: function (stateParams) {
                    return new Promise(function (resolve, reject) {
                        if (stateParams.id) {
                            f.data.workMode = DataDefinition.WorkMode.Edit;
                            f.data.membershipTier.id = stateParams.id;
                        } else {
                            f.data.workMode = DataDefinition.WorkMode.Add;
                        }
                        resolve();
                    });
                },
                goBackToList: function () {
                    act.goBackToList()
                },
                getMembershipTierForEdit: function () {
                    membershipTierService.getMembershipTierForUpdate({id: f.data.membershipTier.id})
                        .success(function (result) {
                            f.data.membershipTier = dd.MembershipTier(result.membershipTier);
                        });
                },
                saveMembershipTier: function () {
                    if (!act.validateMembershipTier()) return;
                    membershipTierService.createOrUpdateMembershipTier({
                        MembershipTier: f.data.membershipTier
                    }).success(function () {
                        abp.notify.success(app.localize('SavedSuccessfully'));
                    })
                },
                openModal: function () {
                    let modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/engage/giftvouchertype/voucherSelector.cshtml',
                        controller: 'tenant.views.engage.voucherSelector as vm',
                        backdrop: 'static',
                        resolve: {
                            existings: function () {
                                return null;
                            },
                            multiSelect: false
                        }
                    })

                    modalInstance.result.then(function (result) {
                        if (result && result.length > 0) {
                            f.data.membershipTier.rewardingVoucher = result[0];
                            f.data.membershipTier.rewardingVoucherId = result[0].id;
                        }

                    });
                }
            }
        };

        return f;
    };

    let DataDefinition = {
        WorkMode: {
            Add: 'ADD',
            Edit: 'EDIT'
        },
        MembershipTier: function (data) {
            return {
                id: data? data.id:null,
                name: data ? data.name : null,
                tierLevel: data?data.tierLevel: 0,
                description: data ? data.description : null,
                price: data ? data.price : 0.0,
                amountToEarnOnePoint: data ? data.amountToEarnOnePoint : 0.0,
                rewardingPoints: data ? data.rewardingPoints : 0.0,
                rewardingVoucher: data? data.rewardingVoucher: null,
                rewardingVoucherId: data ? data.rewardingVoucherId : 0,
                rewardingVoucherCount: data ? data.rewardingVoucherCount : 0,
                cashPaymentDiscount: data ? data.cashPaymentDiscount : 0.0,
                creditCardPaymentDiscount: data ? data.creditCardPaymentDiscount : 0.0,
                textOfBenefits: data ? data.textOfBenefits : null,
                textOfConditions: data ? data.textOfConditions : null
            };
        }
    }
}());
