﻿(function () {
    appModule.controller('tenant.views.engage.member.createedit', [
        '$scope', '$uibModal', '$state', '$stateParams',
        'uiGridConstants', 'abp.services.app.member',
        'abp.services.app.engageLook', 'appSession', 'abp.services.app.location',
        'abp.services.app.giftVoucherType', 'abp.services.app.ticket',
        'abp.services.app.memberContactDetail',
        'abp.services.app.memberAddress',
        'abp.services.app.membershipTier',
        function ($scope, $modal, $state, $stateParams, uiGridConstants, memberService, engagelook,
            appSession, locationService, voucherService, ticketService, memberContactDetailService,
            memberAddressService, membershipTierService
        ) {
            var vm = this;
            vm.saving = false;
            vm.isDeleted = false;

            let func = fnc($scope, $modal, $state, $stateParams, uiGridConstants,
                memberService, engagelook, appSession, locationService,
                voucherService, ticketService, memberContactDetailService,
                memberAddressService, membershipTierService, app, vm);

            func.actions.init(App);

            vm.contactGridOptions = func.controls.memberContactGrid;
            vm.addressGridOptions = func.controls.memberAddressGrid;

            vm.d = func.data;
            vm.e = func.uiEvents;
            vm.c = func.constants;
            vm.a = func.actions;
            
            vm.member = null;
            vm.memberContactPortion = [];
            vm.memberAddressPortion = [];
            vm.memberId = $stateParams.id;
            func.data.memberId = vm.memberId;

            func.actions.setWorkMode();
            vm.permissions = func.actions.readPermissions(abp);

            vm.defaultLocationId = appSession.user.locationRefId;
            vm.requestlocations = [];
            vm.locationRefId = null;
            vm.locations = [];

            $scope.existall = true;
            $scope.minDate = moment().add(-1 * 365 * 100, 'days'); // -1 or -2 based on Sysadmin Table
            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];
            vm.fakeId = 1;
            vm.fakeIdContactDetail = 1;
            $('input[name="BirthDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                minDate: $scope.minDate,
                maxDate: moment(),
                autoUpdateInput: false
            });

            vm.save = func.actions.saveMember;
            vm.cancel = func.actions.goBackToList;
            vm.voucherUnique = "";
            vm.addVoucher = func.actions.addVoucher;
            vm.deleteVoucher = func.actions.deleteVoucher;
            vm.emailVoucher = func.actions.emailVoucher;
            vm.addContactDetails = func.actions.addContactDetails;
            vm.editContactDetails = func.actions.editContactDetails
            vm.removeContactDetails = func.actions.removeContactDetails;
            vm.addMemberAddress = func.actions.addMemberAddress;
            vm.checkIsPrimary = func.actions.checkIsPrimary;
            vm.editMemberAddress = func.actions.editMemberAddress;
            vm.removeMemberAddress = func.actions.removeMemberAddress;
            vm.onContactTypeChangeEvent = func.actions.onContactTypeChangeEvent;
            vm.getLocations = func.actions.getLocations;
            vm.openTicket = func.actions.openTicket;
            vm.addContactShown = func.data.addContactShown;

            func.actions.getMembershipTiers().then(function () {
                vm.getLocations();
            });
            
            func.actions.getContactDetailsType();
            func.actions.loadMemberContactDetailTypes();

            if (func.data.workMode === DataDefinition.WorkMode.Edit) {
                func.actions.memberAddress.getAll();
            }
            setTimeout(function () {
                func.uiEvents.selectTab(1);
            }, 1000);
            
        }
    ]);


    let gridUtil = function ($scope, app, vm, uiGridConstants) {
        let rowTemplate = `
        <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" 
            class="ui-grid-cell" 
            ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  
            ui-grid-cell>
        </div>
            `;
        let columnDefTemplates = [
            `
        <div class="ui-grid-cell-contents">
            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>
                <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" 
                    aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-cog"></i> ${app.localize("Actions")} <span class="caret"></span>
                </button>
            <ul uib-dropdown-menu>
                    <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.edit"
                        ng-click="grid.appScope.a.editMemberContactDetail(row.entity)">${app.localize("Edit")}</a></li>
                    <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.delete"
                        ng-click="grid.appScope.a.deleteMemberContactDetail(row.entity.id)">${app.localize("Delete")}</a></li>
            </ul>
            </div>
        </div>`
        ];
        return {
            gridOptions: {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: rowTemplate,
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate: columnDefTemplates[0]
                    },
                    {
                        name: app.localize('Type'),
                        field: 'contactDetailTypeName',
                        width: 120
                    },
                    {
                        name: app.localize('Detail'),
                        field: 'detail'
                    },
                    {
                        name: app.localize('Communicate'),
                        field: 'acceptToCommunicate',
                        width: 120
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' +
                                sortColumns[0].sort.direction;
                        }
                        vm.a.getMemberContactDetials();
                    });
                    gridApi.pagination.on.paginationChanged($scope,
                        function (pageNumber, pageSize) {
                            requestParams.skipCount = (pageNumber - 1) * pageSize;
                            requestParams.maxResultCount = pageSize;

                            vm.a.getMemberContactDetials();
                        });
                },
                data: []
            }
        };
    };
    let gridAddress = function ($scope, app, vm, uiGridConstants) {
        let grid = gridUtil($scope, app, vm, uiGridConstants);

        let actionColTemplate = `
        <div class="ui-grid-cell-contents">
            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>
                <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" 
                    aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-cog"></i> ${app.localize("Actions")} <span class="caret"></span>
                </button>
            <ul uib-dropdown-menu>
                    <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.edit"
                        ng-click="grid.appScope.a.memberAddress.edit(row.entity)">${app.localize("Edit")}</a></li>
                    <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.delete"
                        ng-click="grid.appScope.a.memberAddress.delete(row.entity)">${app.localize("Delete")}</a></li>
            </ul>
            </div>
        </div>`;

        grid.gridOptions.columnDefs = [
            {
                name: app.localize('Actions'),
                enableSorting: false,
                width: 120,
                cellTemplate: actionColTemplate
            },
            {
                name: app.localize('Address'),
                field: 'address',
                width: 230
            },
            {
                name: app.localize('Pri.'),
                enableSorting: false,
                width: 70,
                cellTemplate: `<div style='width:100%;text-align:center'><span>{{row.entity.isPrimary?'✔️':''}}</span></div>`
            }
        ];
        grid.gridOptions.onRegisterApi = function (gridApi) {
            $scope.gridApi2 = gridApi;
            $scope.gridApi2.core.on.sortChanged($scope, function (grid, sortColumns) {
                if (!sortColumns.length || !sortColumns[0].field) {
                    requestParams.sorting = null;
                } else {
                    requestParams.sorting = sortColumns[0].field + ' ' +
                        sortColumns[0].sort.direction;
                }
                vm.a.getMemberContactDetials();
            });
            gridApi.pagination.on.paginationChanged($scope,
                function (pageNumber, pageSize) {
                    requestParams.skipCount = (pageNumber - 1) * pageSize;
                    requestParams.maxResultCount = pageSize;

                    vm.a.getMemberContactDetials();
                });
        };
        grid.gridOptions.enablePaginationControls = false;
        return grid;
    };

    let fnc = function ($scope, $modal, $state, $stateParams, uiGridConstants,
        memberService, engagelook, appSession, locationService,
        voucherService, ticketService, memberContactDetailService,
        memberAddressService, membershipTierService, app, vm) {

        let dd = DataDefinition;

        let act = {
            validateEmail: function (email) {
                const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(String(email).toLowerCase());
            },
            showHideAddContact: function () {
                f.data.addContactShown.value = (f.data.addContactShown.value == true) ? false : true;
            },
            validateMemberContactDetail: function (memberContactDetail) {
                let mc = dd.MemberContact(memberContactDetail);
                if (memberContactDetail == null) {
                    abp.notify.error(app.localize('ContactTypeInfo'));
                    return DataDefinition.MemberContactDetailInvalidation.IsNull;
                }

                if (!mc.contactDetailType) {
                    abp.notify.error(app.localize('ContactTypeInfo'));
                    return DataDefinition.MemberContactDetailInvalidation.ContactDetailTypeIsNull;
                }

                if (mc.contactDetailType === dd.ContactDetailType.email.value) {
                    let isValidEmail = this.validateEmail(mc.detail);
                    if(!isValidEmail) {
                        abp.notify.error(app.localize('InvalidEmailAddress'));
                        return DataDefinition.MemberContactDetailInvalidation.InvalidEmail;
                    }
                }

                return DataDefinition.MemberContactDetailInvalidation.IsValid;
            },
            findMemberContactDetailType: function (value) {
                return f.data.contactDetailTypes.find(function (t) { return t.value === value;})
            }
        }
        
        let f = {
            data: {
                addContactShown: { value: false },
                memberContacts: [],
                saving: false,
                contactDetailTypes: [],
                memberContact: dd.MemberContact(),
                ui: {
                    selectedContactDetailType: dd.TextValuePair(),
                    addressInputDisabled: true,
                    addAddressBntBuzy: false,
                    addContactBntBuzy: false
                },
                memberContactDetailIsNumber: false,
                memberId: 0,
                workMode: null,
                memberContactDetailLoading: false,
                memberAddressLoading: false,
                memberAddress: dd.MemberAddress(),
                membershipTiers: null,
                selectedTab: 1
            },
            constants: {
                workMode: {
                    add: dd.WorkMode.Add,
                    edit: dd.WorkMode.Edit
                }
            },
            controls: {
                memberContactGrid: gridUtil($scope, app, vm, uiGridConstants).gridOptions,
                memberAddressGrid: gridAddress($scope, app, vm, uiGridConstants).gridOptions
            },
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                getMembershipTiers: function () {
                    return new Promise(function (resolve, reject) {
                        membershipTierService.getAll({}).success(function (result) {
                            f.data.membershipTiers = result.items;
                            resolve();
                        });
                    });
                },
                saveMember: function () {
                    vm.contactdetails = [];
                    angular.forEach(vm.memberContactDetail, function (tsvalue, key) {

                        vm.contactdetails.push({
                            'id': tsvalue.id,
                            'connectMemberId': tsvalue.connectMemberId,
                            'contactDetailType': tsvalue.contactDetailType,
                            'contactDetailTypeName': tsvalue.contactDetailTypeName,
                            'detail': tsvalue.detail,
                            'acceptToCommunicate': tsvalue.acceptToCommunicate
                        });
                    });
                    vm.addressDetails = [];
                    angular.forEach(vm.memberAddress, function (tsvalue, key) {

                        vm.addressDetails.push({
                            'id': tsvalue.id,
                            'connectMemberId': tsvalue.connectMemberId,
                            'tag': tsvalue.tag,
                            'address': tsvalue.address,
                            'locality': tsvalue.locality,
                            'city': tsvalue.city,
                            'postalCode': tsvalue.postalCode,
                            'state': tsvalue.state,
                            'country': tsvalue.country,
                            'isPrimary': tsvalue.isPrimary
                        });
                    });
                    vm.saving = true;

                    // let locationId = (vm.locations && vm.locations.length>0)? vm.locations[0]:0;
                    vm.member.membershipTierId = vm.member.membershipTier.id
                    memberService.createOrUpdateMember({
                        member: vm.member,
                        memberContactDetail: vm.contactdetails,
                        memberAddress: vm.addressDetails
                    }).success(function (result) {
                        if (result.successFlag) {
                            abp.notify.info(app.localize('SavedSuccessfully'));
                            // $state.go('tenant.member');
                            f.actions.setOpenMemberId(result.id);
                            f.data.workMode = dd.WorkMode.Edit;
                        }
                        else {
                            angular.forEach(result.errorMessageList, function (value, key) {
                                abp.notify.warn(value);
                            });
                        }
                    }).finally(function () {
                        vm.saving = false;
                    });
                },
                goBackToList: function () {
                    $state.go('tenant.member');
                },
                addVoucher: function () {
                    vm.saving = true;
                    voucherService.linkMemberToVoucher({
                        voucher: vm.voucherUnique,
                        memberId: vm.memberId
                    }).success(function () {
                        voucherService.memberVouchers({
                            voucher: vm.voucherUnique,
                            memberId: vm.memberId
                        }).success(function (result) {
                            vm.member.vouchers = result;
                        });
                    }).finally(function () {
                        vm.saving = false;
                        vm.voucherUnique = "";
                    });
                },
                deleteVoucher: function (vouch) {
                    vm.saving = true;
                    voucherService.unLinkMemberFromVoucher({
                        voucher: vouch.voucher,
                        memberId: vm.memberId
                    }).success(function () {
                        voucherService.memberVouchers({
                            voucher: vm.vouch,
                            memberId: vm.memberId
                        }).success(function (result) {
                            vm.member.vouchers = result;
                        });

                    }).finally(function () {
                        vm.saving = false;
                    });
                },
                emailVoucher: function (vouch) {
                    vm.saving = true;
                    voucherService.emailVoucher({
                        voucher: vouch.voucher,
                        memberId: vm.memberId
                    }).success(function () {
                        abp.notify.success(app.localize("MailSent"));
                    }).finally(function () {
                        vm.saving = false;
                    });
                },
                addContactDetails: function (data) {
                    if (data == null) {
                        abp.notify.error(app.localize('ContactTypeInfo'));
                        return;
                    }
                    if (data.contactDetailType == undefined) {
                        abp.notify.error(app.localize('ContactTypeInfo'));
                        return;
                    }

                    if (data.contactDetailType == '1') {
                        data.contactDetailTypeName = "HandPhone";
                    } else if (data.contactDetailType == '2') {
                        data.contactDetailTypeName = "Phone";
                    } else if (data.contactDetailType == '3') {
                        data.contactDetailTypeName = "Fax";
                    } else if (data.contactDetailType == '4') {
                        data.contactDetailTypeName = "Email";
                        var isValidEmail = act.validateEmail(data.detail);
                        if (!isValidEmail) {
                            abp.notify.error(app.localize('InvalidEmailAddress'));
                            return;
                        }
                    } else if (data.contactDetailType == '5') {
                        data.contactDetailTypeName = "Instagram";
                    } else if (data.contactDetailType == '6') {
                        data.contactDetailTypeName = "LinkedIn";
                    } else
                        data.contactDetailTypeName = "FaceBook";

                    if (data.detail == undefined) {
                        abp.notify.error(app.localize('DetailInfo'));
                        return;
                    }

                    if (data.id == null && data.fakeIdContactDetail == null) {
                        data.fakeIdContactDetail = vm.fakeIdContactDetail++;
                        vm.memberContactDetail.push(data);
                    } else if (data.id == null && data.fakeIdContactDetail > 0) {
                        // TODO: when not saved to db
                        vm.memberContactDetail.some(function (tsValue, key) {
                            if (tsValue.fakeIdContactDetail == data.fakeIdContactDetail) {
                                vm.memberContactDetail[key].contactDetailType = data.contactDetailType;
                                vm.memberContactDetail[key].contactDetailTypeName = data.contactDetailTypeName,
                                    vm.memberContactDetail[key].detail = data.detail;
                                vm.memberContactDetail[key].acceptToCommunicate = data.acceptToCommunicate;
                                vm.memberContactPortion = null;
                                return;
                            }
                        });
                    }
                    else {
                        vm.memberContactDetail.some(function (tsValue, key) {
                            if (tsValue.id == data.id) {
                                vm.memberContactDetail[key].contactDetailType = data.contactDetailType;
                                vm.memberContactDetail[key].contactDetailTypeName = data.contactDetailTypeName,
                                    vm.memberContactDetail[key].detail = data.detail;
                                vm.memberContactDetail[key].acceptToCommunicate = data.acceptToCommunicate;
                                vm.memberContactPortion = null;
                                return;
                            }
                        });
                    }

                    vm.memberContactPortion = null;

                },
                editContactDetails: function (data) {
                    var onlyNumber = false;
                    if (data.contactDetailType == 1 || data.contactDetailType == 2 || data.contactDetailType == 3) {
                        onlyNumber = true;
                    }
                    vm.memberContactPortion = {
                        id: data.id,
                        connectMemberId: data.connectMemberId,
                        contactDetailType: data.contactDetailType,
                        contactDetailTypeName: data.contactDetailTypeName,
                        detail: data.detail,
                        acceptToCommunicate: data.acceptToCommunicate,
                        onlyNumber: onlyNumber,
                        fakeIdContactDetail: data.fakeIdContactDetail
                    };

                },
                removeContactDetails: function (productIndex) {
                    if (vm.memberContactDetail.length > 1) {
                        vm.memberContactDetail.splice(productIndex, 1);
                    }
                    else {
                        vm.memberContactDetail.splice(productIndex, 1);
                        return;
                    }
                },
                addMemberAddress: function (data) {
                    if (data == null) {
                        abp.notify.error(app.localize('Tag') + " " + app.localize('Is') + " " + app.localize('Mandatory'));
                        return;
                    }
                    if (vm.checkIsPrimary(data)) {
                        abp.notify.error(app.localize('isPrimaryAddressErr'));
                        return;
                    }
                    if (data.id == null && data.fakeId == null) {
                        data.fakeId = vm.fakeId++;
                        vm.memberAddress.push(data);
                    } else if (data.id == null && data.fakeId > 0) {
                        // TODO: when not saved to db
                        vm.memberAddress.some(function (tsValue, key) {
                            if (tsValue.fakeId == data.fakeId) {
                                vm.memberAddress[key].tag = data.tag;
                                vm.memberAddress[key].address = data.address,
                                    vm.memberAddress[key].locality = data.locality;
                                vm.memberAddress[key].city = data.city;
                                vm.memberAddress[key].postalCode = data.postalCode;
                                vm.memberAddress[key].state = data.state,
                                    vm.memberAddress[key].country = data.country;
                                vm.memberAddress[key].isPrimary = data.isPrimary;
                                vm.memberAddressPortion = null;
                                vm.memberAddress[key].fakeId = data.fakeId;
                                return;
                            }
                        });

                    }
                    else {
                        vm.memberAddress.some(function (tsValue, key) {
                            if (tsValue.id == data.id) {
                                vm.memberAddress[key].tag = data.tag;
                                vm.memberAddress[key].address = data.address,
                                    vm.memberAddress[key].locality = data.locality;
                                vm.memberAddress[key].city = data.city;
                                vm.memberAddress[key].postalCode = data.postalCode;
                                vm.memberAddress[key].state = data.state,
                                    vm.memberAddress[key].country = data.country;
                                vm.memberAddress[key].isPrimary = data.isPrimary;
                                vm.memberAddressPortion = null;
                                return;
                            }
                        });
                    }
                    vm.memberAddressPortion = null;

                },
                checkIsPrimary: function (data) {

                    var countItem = [];
                    if (data.id > 0 && data.fakeId == undefined) {
                        countItem = vm.memberAddress.filter(x => x.isPrimary == true && x.id !== data.id)
                    } else if (data.fakeId && (data.id == 0 || data.id == undefined)) {
                        countItem = vm.memberAddress.filter(x => x.isPrimary == true && x.fakeId !== data.fakeId)
                    } else if (data.fakeId && data.id) {
                        countItem = vm.memberAddress.filter(x => x.isPrimary == true && (x.id !== data.id || x.fakeId !== data.fakeId))
                    } else {
                        countItem = vm.memberAddress.filter(x => x.isPrimary == true)
                    }
                    if (countItem.length > 0 && data.isPrimary == true) {
                        return true;
                    } else {
                        return false;
                    }
                },
                editMemberAddress: function (data) {
                    vm.memberAddressPortion = {
                        id: data.id,
                        tag: data.tag,
                        address: data.address,
                        locality: data.locality,
                        city: data.city,
                        postalCode: data.postalCode,
                        state: data.state,
                        country: data.country,
                        isPrimary: data.isPrimary,
                        fakeId: data.fakeId
                    };
                },
                removeMemberAddress: function (productIndex) {
                    if (vm.memberAddress.length > 1) {
                        vm.memberAddress.splice(productIndex, 1);
                    }
                    else {
                        vm.memberAddress.splice(productIndex, 1);
                        return;
                    }
                },
                onContactTypeChangeEvent: function (data) {
                    var onlyNumber = false;
                    if (data.contactDetailType == 1 || data.contactDetailType == 2 || data.contactDetailType == 3) {
                        onlyNumber = true;
                    }
                    vm.memberContactPortion = {
                        id: data.id,
                        connectMemberId: data.connectMemberId,
                        contactDetailType: data.contactDetailType,
                        contactDetailTypeName: data.contactDetailTypeName,
                        detail: null,
                        acceptToCommunicate: false,
                        onlyNumber: onlyNumber
                    };
                },
                getLocations: function () {
                    locationService.getAll({
                        skipCount: 0,
                        maxResultCount: app.consts.grid.defaultPageSize,
                        sorting: null
                    }).success(function (result) {
                        vm.locations = $.parseJSON(JSON.stringify(result.items));
                    }).finally(function (result) {
                        memberService.getMemberForEdit({
                            id: vm.memberId
                        }).success(function (result) {
                            vm.member = dd.Member(result.member,f.data.membershipTiers);
                            vm.memberContactDetail = result.memberContactDetail;
                            f.controls.memberContactGrid.data = result.memberContactDetail;
                            vm.memberAddress = result.memberAddress;
                            console.log(vm.memberAddress);
                            if (vm.member.id == null) {
                                angular.forEach(vm.location, function (value, key) {
                                    if (value.id == vm.defaultLocationId) {
                                        vm.requestlocations = value;
                                    }
                                });
                            }

                            $('input[name="BirthDate"]').daterangepicker({
                                locale: {
                                    format: $scope.format
                                },
                                singleDatePicker: true,
                                showDropdowns: true,
                                minDate: $scope.minDate,
                                maxDate: moment(),
                                autoUpdateInput: false
                            });

                        });
                    });
                },
                openTicket: function (vouch) {
                    vm.saving = true;
                    ticketService.getTicketIdOnLocation({
                        location: vouch.locationId,
                        ticketNumber: vouch.ticketNumber
                    }).success(function (result) {
                        console.log(result);
                        $modal.open({
                            templateUrl: '~/App/tenant/views/connect/report/tickets/ticket/ticketModal.cshtml',
                            controller: 'tenant.views.connect.report.ticketModal as vm',
                            backdrop: 'static',
                            resolve: {
                                ticket: function () {
                                    return result;
                                }
                            }
                        });

                    }).finally(function () {
                        vm.saving = false;
                    });
                },
                getContactDetailsType: function () {
                    vm.saving = true;
                    memberService.getContactDetailsTypes()
                        .success(function (result) {
                            vm.contactDetailTypes = result.items;
                        }).finally(function (result) {
                            vm.saving = false;
                        });
                },
                saveMemberContactDetails: function (memberContact) {
                    let mc = dd.MemberContact(memberContact);

                    let validation = act.validateMemberContactDetail(mc);
                    console.log(validation);
                    if (validation !== DataDefinition.MemberContactDetailInvalidation.IsValid) return;

                    f.data.saving = false;
                    f.data.memberContact.connectMemberId = f.data.memberId;
                    memberContactDetailService.createOrUpdateMemberContactDetail(
                        {
                            MemberContactDetail: f.data.memberContact
                        }
                    ).success(function () {
                        f.actions.resetMemberContactDetailControls();
                        act.showHideAddContact();
                        f.actions.getMemberContactDetials();
                    }).finally(function () {
                        f.data.saving = false;
                    });
                },
                loadMemberContactDetailTypes: function () {
                    f.data.saving = true;
                    memberService.getContactDetailsTypes()
                        .success(function (result) {
                            f.data.contactDetailTypes =
                                f.data.contactDetailTypes.slice(0, f.data.contactDetailTypes.length);
                            result.items.forEach(function (t) {
                                let item = dd.TextValuePair(t.displayText, parseInt(t.value));
                                f.data.contactDetailTypes.push(item);
                            });

                        }).finally(function (result) {
                            f.data.saving = false;
                        });
                },
                setWorkMode: function () {
                    f.data.memberId = $stateParams.id;
                    if (f.data.memberId) {
                        f.data.workMode = dd.WorkMode.Edit;
                    } else {
                        f.data.workMode = dd.WorkMode.Add;
                    }
                },
                getMemberContactDetials: function () {
                    f.data.memberContactDetailLoading = true;
                    memberContactDetailService.getAll(
                        {
                            skipCount: 0,
                            maxResultCount: app.consts.grid.defaultPageSize,
                            sorting: 'id',
                            isDeleted: false,
                            contactMemberId: f.data.memberId
                        }                        
                    ).success(function (result) {
                        f.controls.memberContactGrid.data = result.items;
                        f.controls.memberContactGrid.totalItems = result.totalCount;
                    }).finally(function() {
                        f.data.memberContactDetailLoading = false;
                    });
                },
                resetMemberContactDetailControls: function () {
                    f.data.memberContact.id = 0;
                    f.data.ui.selectedContactDetailType = dd.TextValuePair();
                    f.data.memberContact.detail = '';
                    f.data.memberContact.acceptToCommunicate = false;
                },
                readPermissions: function (abp) {
                    return {
                        member: abp.auth.hasPermission('Pages.Tenant.Engage.Member'),
                        create: abp.auth.hasPermission('Pages.Tenant.Play.Display.Create'),
                        edit: abp.auth.hasPermission('Pages.Tenant.Engage.Member.Edit'),
                        delete: abp.auth.hasPermission('Pages.Tenant.Engage.Member.Delete')
                    };
                },
                editMemberContactDetail: function (memberContactDetail) {
                    act.showHideAddContact();
                    let mc = dd.MemberContact(memberContactDetail);
                    f.data.memberContact.id = mc.id;
                    f.data.ui.selectedContactDetailType =
                        act.findMemberContactDetailType(mc.contactDetailType);
                    f.data.memberContact.detail = mc.detail;
                    f.data.memberContact.acceptToCommunicate = mc.acceptToCommunicate;
                    f.uiEvents.contactDetailTypeChange();
                },
                deleteMemberContactDetail: function (id) {
                    memberContactDetailService.deleteMemberContactDetail({
                        id: id
                    }).success(function () {
                        f.actions.getMemberContactDetials();
                    });
                },
                memberAddress: {
                    getAll: function () {
                        f.data.memberAddressLoading = true;
                        memberAddressService.getAll({
                            skipCount: 0,
                            maxResultCount: app.consts.grid.defaultPageSize,
                            sorting: 'id',
                            connectMemberId: f.data.memberId
                        })
                            .success(function (result) {
                                f.controls.memberAddressGrid.data = result.items;
                                f.controls.memberAddressGrid.totalItems = result.totalCount;
                                $scope.gridApi2.core.handleWindowResize();
                            })
                            .finally(function () {
                                f.data.memberAddressLoading = false;
                            });
                    },
                    edit: function (address) {
                        let adr = dd.MemberAddress(address);
                        f.data.memberAddress.id = adr.id;
                        f.data.memberAddress.tag = adr.tag;
                        f.data.memberAddress.address = adr.address;
                        f.data.memberAddress.locality = adr.locality;
                        f.data.memberAddress.city = adr.city;
                        f.data.memberAddress.postalCode = adr.postalCode;
                        f.data.memberAddress.state = adr.state;
                        f.data.memberAddress.country = adr.country;
                        f.data.memberAddress.isPrimary = adr.isPrimary;
                        f.data.ui.addressInputDisabled = false;
                    },
                    save: function () {
                        f.data.ui.addAddressBntBuzy = true;
                        f.data.memberAddress.connectMemberId = f.data.memberId;
                        memberAddressService.createOrUpdateMemberAddress({
                            MemberAddress: f.data.memberAddress
                        })
                            .success(function () {
                                f.actions.memberAddress.getAll();
                                f.data.memberAddress = dd.MemberAddress();
                                f.data.ui.addressInputDisabled = true;
                            }).finally(function () {
                                f.data.ui.addAddressBntBuzy = false;
                            });
                    },
                    delete: function (obj) {
                        memberAddressService.deleteMemberAddress({
                            id: obj.id
                        })
                            .success(function () {
                                f.actions.memberAddress.getAll();
                            });
                    }
                },
                setOpenMemberId: function (memberId) {
                    vm.memberId = memberId;
                    f.data.memberId = memberId;
                }
            },
            uiEvents: {
                addBtnClick: function () {
                    f.actions.saveMemberContactDetails(f.data.memberContact);
                },
                contactDetailTypeChange: function () {
                    f.data.memberContact.contactDetailType = f.data.ui.selectedContactDetailType.value;
                    f.data.memberContactDetailIsNumber =
                        [1, 2, 3].indexOf(f.data.memberContact.contactDetailType) >= 0;
                },
                newContactBtnClick: function () {
                    act.showHideAddContact();
                },
                cancelContactBtnClick: function () {
                    act.showHideAddContact();
                },
                addressAddBtnClick: function () {
                    if (f.data.ui.addressInputDisabled == false) {
                        f.actions.memberAddress.save();
                    } else {
                        f.data.ui.addressInputDisabled = (f.data.ui.addressInputDisabled != true);
                    }
                },
                selectTab: function (selection) {
                    f.data.selectedTab = selection;
                    if (selection == 1) {
                        $('#tab1').css({ visibility: 'visible', position:'relative' });
                        $('#tab2').css({ visibility: 'hidden', position: 'absolute' });
                        $('#tabBtn1').attr('style', 'border-bottom:solid 5px #36c6d3 !important');
                        $('#tabBtn2').attr('style', 'border-bottom:solid 5px #fff !important');
                    } else {
                        $('#tab1').css({ visibility: 'hidden', position: 'absolute' });
                        $('#tab2').css({ visibility: 'visible', position: 'relative' });
                        $('#tabBtn2').attr('style', 'border-bottom:solid 5px #36c6d3 !important');
                        $('#tabBtn1').attr('style', 'border-bottom:solid 5px #fff !important');
                    }
                }
            }
        };

        return f;
    };

    let DataDefinition = {
        Member: function (data,membershipTiers) {
            let getTrueDate = function (val) {
                if (val) {
                    if (val.indexOf('T') >= 0) {
                        return new Date(val.split('T')[0]);
                    } else {
                        if (isDate(val)) {
                            return val;
                        } else {
                            return new Date();
                        }
                    }
                } else {
                    return null;
                }
            };

            let getMembershipTier = function (id) {
                if (!(membershipTiers && Array.isArray(membershipTiers))) return null;
                let found = membershipTiers.find(function (t) { return t.id == id });
                return found;
            };

            return {
                address: data? data.address:null,
                birthDay: data ? getTrueDate(data.birthDay): null,
                city: data ? data.city: null,
                country: data ? data.country:null,
                customData: data ? data.customData:null,
                defaultAddressRefId: data ? data.defaultAddressRefId:null,
                editAddressRefId: data ? data.editAddressRefId: 0,
                emailId: data ? data.emailId:null,
                id: data ? data.id:0,
                isMemberCodeAutomaticGenerateFlag: data ? data.isMemberCodeAutomaticGenerateFlag:false,
                lastName: data ? data.lastName:'',
                lastVisitDate: data ? getTrueDate( data.lastVisitDate):null,
                locality: data ? data.locality:null,
                locationRefId: data ? data.locationRefId:null,
                memberCode: data ? data.memberCode:'',
                memberPoint: data ? data.memberPoint:null,
                name: data ? data.name:'',
                phoneNumber: data ? data.phoneNumber:null,
                postalCode: data ? data.postalCode:null,
                state: data ? data.state:null,
                tenantId: data ? data.tenantId:5,
                totalPoints: data ? data.totalPoints:0,
                userId: data ? data.userId:null,
                userName: data ? data.userName:null,
                vouchers: data ? data.vouchers : [],
                membershipTierId: data? data.membershipTierId:0,
                membershipTier: data ? getMembershipTier(data.membershipTierId) : null
            };
        },
        MemberContact: function (data) {
            return {
                id: data? data.id:0,
                connectMemberId: data? data.connectMemberId:0,
                contactDetailType: data? data.contactDetailType:0,
                contactDetailTypeText: data ? data.contactDetailTypeText : null,
                detail: data? data.detail: null,
                acceptToCommunicate: data ? data.acceptToCommunicate : false
            };
        },
        MemberAddress: function (data) {
            return {
                id: data? data.id:null,
                connectMemberId: data ? data.connectMemberId :0,
                tag: data ? data.tag :null,
                isPrimary: data ? data.isPrimary :false,
                locationId: data ? data.locationId :0,
                address: data ? data.address :null,
                locality: data ? data.locality :null,
                city: data ? data.city :null,
                state: data ? data.state :null,
                country: data ? data.country :null,
                postalCode: data ? data.postalCode :null
            };
        },
        ContactDetailType: {
            handPhone: { text:'HandPhone', value: 1},
            phone: { text: 'Phone', value: 2 },
            fax: { text: 'Fax', value: 3 },
            email: { text: 'Email', value: 4 },
            instagram: { text: 'Instagram', value: 5 },
            linkedIn: { text: 'LinkedIn', value: 6 },
            faceBook: { text: 'FaceBook', value: 7 }
        },
        TextValuePair: function(text, value){
            return { text: text? text:null, value: value? value: null };
        },
        WorkMode: {
            Add: 'ADD',
            Edit: 'EDIT'
        },
        MemberContactDetailInvalidation: {
            IsValid: 0,
            IsNull: 1,
            ContactDetailTypeIsNull: 2,
            InvalidEmail: 3
        }
    }

    appModule.directive('numbersOnly', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attr, ngModelCtrl) {
                function fromUser(text) {
                    if (text) {
                        var transformedInput = text.replace(/[^0-9 ]/g, '');

                        if (transformedInput !== text) {
                            ngModelCtrl.$setViewValue(transformedInput);
                            ngModelCtrl.$render();
                        }
                        return transformedInput;
                    }
                    return undefined;
                }
                ngModelCtrl.$parsers.push(fromUser);
            }
        };
    });
})();
