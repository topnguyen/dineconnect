﻿(function () {

    appModule.controller("tenant.views.engage.accounts",
        [
            "$scope",
            "$state",
            "$stateParams",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.memberAccount",
            function ($scope, $state, $stateParams, $modal, uiGridConstants, memberAccountService) {

                var vm = this;

                let func = fnc($scope, $state, $modal, uiGridConstants,
                    app, vm, memberAccountService);

                func.actions.init(App);

                gridUtil($scope, app, vm, uiGridConstants, func.controls.gridOptionAccount);

                func.data.permissions = func.actions.readPermissions();

                vm.a = func.actions;
                vm.d = func.data;
                vm.c = func.controls;

                if ($stateParams.mid) {
                    func.data.filterText = decodeURIComponent($stateParams.mid);
                }

                func.actions.getMemberAccounts();
            }
        ]);

    let fnc = function ($scope, $state, $modal, uiGridConstants,
        app, vm, memberAccountService) {

        let act = {
            xxx: function () {

            }
        }

        let f = {
            data: {
                gridAcctLoading: false,
                filterText: '',
                isDeleted: false,
                memberAccounts: [],
                permissions: null
            },
            controls: {
                gridOptionAccount: DataDefinition.GridOptions()
            },
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                readPermissions: function () {
                    return {
                        create: abp.auth.hasPermission('Pages.Tenant.Engage.Member.Account.Create'),
                        edit: abp.auth.hasPermission('Pages.Tenant.Engage.Member.Account.Edit'),
                        delete: abp.auth.hasPermission('Pages.Tenant.Engage.Member.Account.Delete')
                    }
                },
                getMemberAccounts: function () {
                    f.data.gridAcctLoading = true;
                    memberAccountService.getAll({
                        skipCount: 0,
                        maxResultCount: app.consts.grid.defaultPageSize,
                        sorting: 'id',
                        filter: f.data.filterText,
                        isDeleted: f.data.isDeleted
                    }).success(function (result) {
                        f.controls.gridOptionAccount.data = result.items;
                    }).finally(function () {
                        f.data.gridAcctLoading = false;
                    });
                },
                openAccountTrans: function (obj) {
                    $state.go('tenant.memberaccounttrans', {id:obj.id});
                }
            }
        };

        return f;
    };


    let DataDefinition = {
        GridOptions: function () {
            return {
                enableHorizontalScrollbar: false,
                enableVerticalScrollbar: false,
                paginationPageSizes: 0,
                paginationPageSize: 0,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: null,
                rowTemplate: '',
                columnDefs: [],
                onRegisterApi: {},
                data: []
            }
        } 
    };


    let gridUtil = function ($scope, app, vm, uiGridConstants, uiGridOptions) {
        let rowTemplate = `
        <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" 
            class="ui-grid-cell" 
            ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  
            ui-grid-cell>
        </div>
            `;
        let columnDefTemplates = [
            `
        <div class="ui-grid-cell-contents">
            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>
                <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" 
                    aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-cog"></i> ${app.localize("Actions")} <span class="caret"></span>
                </button>
            <ul uib-dropdown-menu>
                    <li><a ng-if="!grid.appScope.d.isDeleted && grid.appScope.d.permissions.edit"
                        ng-click="grid.appScope.a.openAccountTrans(row.entity)">${app.localize("View")}</a></li>
            </ul>
            </div>
        </div>`
        ];


        uiGridOptions.enableHorizontalScrollbar= uiGridConstants.scrollbars.WHEN_NEEDED;
        uiGridOptions.enableVerticalScrollbar= uiGridConstants.scrollbars.WHEN_NEEDED;
        uiGridOptions.paginationPageSizes= app.consts.grid.defaultPageSizes;
        uiGridOptions.paginationPageSize= app.consts.grid.defaultPageSize;
        uiGridOptions.useExternalPagination= true;
        uiGridOptions.useExternalSorting= true;
        uiGridOptions.appScopeProvider= vm;
        uiGridOptions.rowTemplate= rowTemplate;
        uiGridOptions.columnDefs = [
            {
                name: app.localize('Actions'),
                enableSorting: false,
                width: 120,
                cellTemplate: columnDefTemplates[0]
            },
            {
                name: app.localize('Name'),
                field: 'name'
            },
            {
                name: app.localize('Number'),
                field: 'number'
            },
            {
                name: app.localize('Balance'),
                field: 'balance'
            }
        ];
        uiGridOptions.onRegisterApi = function (gridApi) {
            $scope.gridApi = gridApi;
            $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                if (!sortColumns.length || !sortColumns[0].field) {
                    requestParams.sorting = null;
                } else {
                    requestParams.sorting = sortColumns[0].field + ' ' +
                        sortColumns[0].sort.direction;
                }

                vm.a.getMemberAccounts();
            });
            gridApi.pagination.on.paginationChanged($scope,
                function (pageNumber, pageSize) {
                    requestParams.skipCount = (pageNumber - 1) * pageSize;
                    requestParams.maxResultCount = pageSize;

                    vm.a.getMemberAccounts();
                });
        }
        uiGridOptions.data = [];
    };


}());
