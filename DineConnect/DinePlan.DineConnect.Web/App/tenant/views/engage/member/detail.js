﻿(function () {
    appModule.controller("tenant.views.engage.member.detail", [
        "$scope", "$state", "$stateParams", "uiGridConstants", "abp.services.app.member",
        function ($scope, $state, $stateParams, uiGridConstants, memberService) {
            var vm = this;

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });

            vm.memberId = $stateParams.id;
            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                'delete': abp.auth.hasPermission('Pages.Tenant.Engage.MemberPoint.Delete')
            };
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Operation"),
                        enableSorting: false,
                        cellTemplate:
                            "<div  ng-if=\"grid.appScope.permissions.delete\" class=\"ui-grid-cell-contents text-center\">" +
                            "  <button class=\"btn btn-default btn-xs\" ng-click=\"grid.appScope.deletePoints(row.entity)\"><i class=\"fa fa-trash-o\"></i></button>" +
                            "</div>"
                    },
                    {
                        name: app.localize("Id"),
                        field: "id"
                    },
                    {
                        name: app.localize("Ticket"),
                        field: "ticketNumber"
                    },
                    {
                        name: app.localize("TicketTotal"),
                        field: "ticketTotal",
                        cellClass: "ui-ralign"
                    },
                    {
                        name: app.localize("TicketDate"),
                        field: "ticketDate",
                        cellFilter: "momentFormat: 'YYYY-MM-DD HH:mm:ss'",
                    },
                    {
                        name: app.localize("Accumulation"),
                        field: "isPointAccumulation",
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <span ng-show=\"row.entity.isPointAccumulation\" class=\"label label-success\">" + app.localize("Yes") + "</span>" +
                            "  <span ng-show=\"!row.entity.isPointAccumulation\" class=\"label label-default\">" + app.localize("No") + "</span>" +
                            "</div>",
                        minWidth: 80
                    },
                    {
                        name: app.localize("Redemption"),
                        field: "isPointRedemption",
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <span ng-show=\"row.entity.isPointRedemption\" class=\"label label-success\">" + app.localize("Yes") + "</span>" +
                            "  <span ng-show=\"!row.entity.isPointRedemption\" class=\"label label-default\">" + app.localize("No") + "</span>" +
                            "</div>",
                        minWidth: 80
                    },
                    {
                        name: app.localize("Point"),
                        field: "totalPoints"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.deletePoints = function (myObject) {
                abp.message.confirm(
                    app.localize("DeletePointsWarning", myObject.ticketNumber),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            memberService.deleteMemberPoint({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize("SuccessfullyDeleted"));
                            });
                        }
                    }
                );
            };

            vm.getAll = function () {
                vm.loading = true;
                memberService.getAllMemberPoints({
                    memberId: vm.memberId,
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.members.totalCount;
                    vm.userGridOptions.data = result.members.items;
                    vm.dashBoard = result.dashBoardDto;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.exportToExcel = function () {
                abp.ui.setBusy("#MyLoginForm");

                memberService.getMemberOrderExport({
                    memberId: vm.memberId
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        abp.ui.clearBusy("#MyLoginForm");
                    });
            };

            vm.getAll();
        }
    ]);
})();