﻿(function() {
    appModule.controller("tenant.views.engage.member.editPoints",
        [
            "$scope", "$uibModalInstance", "abp.services.app.member", "memberId",
            function($scope, $modalInstance, memberService, memberId) {
                var vm = this;

                vm.saving = false;
                vm.member = null;
                vm.save = function() {
                    vm.saving = true;
                    memberService.updateMemberPoints({
                        memberId: memberId,
                        points: vm.member.totalPoints
                    }).success(function() {
                        abp.notify.info(app.localize("SavedSuccessfully"));
                        $modalInstance.close();
                    }).finally(function() {
                        vm.saving = false;
                        $modalInstance.close();
                    });
                };
                vm.cancel = function() {
                    $modalInstance.dismiss();
                };

                function init() {
                    memberService.getMemberForEdit({
                        id: memberId
                    }).success(function(result) {
                        vm.member = result.member;
                    });
                }

                init();
            }
        ]);
})();