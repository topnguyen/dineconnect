﻿(function () {

    appModule.controller("tenant.views.engage.serviceinfo.templates",
        [
            "$scope",
            "$state",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.memberServiceInfoTemplate",
            function ($scope, $state, $modal, uiGridConstants, memberServiceInfoTemplateService) {

                var vm = this;

                let func = fnc($scope, $state, $modal, uiGridConstants, memberServiceInfoTemplateService, app, vm);
                func.actions.init(App);

                vm.a = func.actions;
                vm.c = func.controls;
                vm.d = func.data;

                func.data.permissions = func.actions.readPermissions();

                let gridUtl = gridUtil($scope, app, vm, uiGridConstants);
                gridUtl.setPageFunctions(func);

                func.controls.templateGrid = gridUtl.gridOptions;
                func.actions.getTemplates();

            }
        ]);


    let fnc = function ($scope, $state, $modal, uiGridConstants, memberServiceInfoTemplateService, app, vm) {
        if (!vm) return null;
        let act = {
            xxx: function () {

            }
        }

        let dd = DataDefinition;

        let f = {
            data: {
                ui: {
                    templateGridLoading: false,
                    filterText: ''
                },
                permissions: {},
                isDeleted: false
            },
            controls: {
                templateGrid: dd.GridOptions()
            },
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                createTemplate: function () {
                    $state.go("tenant.serviceinfotemplatebuilder", {})
                },
                getTemplates: function () {
                    f.data.ui.templateGridLoading = true;
                    memberServiceInfoTemplateService.getAll({
                        skipCount: 0,
                        maxResultCount: app.consts.grid.defaultPageSize,
                        sorting: 'id',
                        filter: f.data.ui.filterText,
                        isDeleted: false
                    }).success(function (result) {
                            f.controls.templateGrid.data = result.items;
                        }
                    ).finally(function () {
                            f.data.ui.templateGridLoading = false;
                        }
                    );
                },
                readPermissions: function () {
                    return {
                        create: abp.auth.hasPermission('Pages.Tenant.Engage.ServiceInfo.Template.Create'),
                        edit: abp.auth.hasPermission('Pages.Tenant.Engage.ServiceInfo.Template.Edit'),
                        delete: abp.auth.hasPermission('Pages.Tenant.Engage.ServiceInfo.Template.Delete')
                    }
                },
                editTemplate: function (obj) {
                    $state.go('tenant.serviceinfotemplatebuilder', { id: obj.id });
                },
                deleteTemplate: function (obj) {

                    abp.message.confirm(
                        app.localize('DeleteLocationWarning', obj.name),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                memberServiceInfoTemplateService.deleteMemberServiceInfoTemplate(
                                    { id: obj.id }
                                )
                                    .success(function () {
                                        f.actions.getTemplates();
                                    })
                                    .finally(function () { });
                            }
                        }
                    )
                }
            }
        };

        return f;
    };

    let gridUtil = function ($scope, app, vm, uiGridConstants) {
        let pgFnc = fnc();

        let rowTemplate = `
        <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" 
            class="ui-grid-cell" 
            ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  
            ui-grid-cell>
        </div>
            `;
        let columnDefTemplates = [
            `
        <div class="ui-grid-cell-contents">
            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>
                <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" 
                    aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-cog"></i> ${app.localize("Actions")} <span class="caret"></span>
                </button>
            <ul uib-dropdown-menu>
                    <li><a ng-if="!grid.appScope.d.isDeleted && grid.appScope.d.permissions.edit"
                        ng-click="grid.appScope.a.editTemplate(row.entity)">${app.localize("Edit")}</a></li>
                    <li><a ng-if="!grid.appScope.d.isDeleted && grid.appScope.d.permissions.delete"
                        ng-click="grid.appScope.a.deleteTemplate(row.entity)">${app.localize("Delete")}</a></li>
            </ul>
            </div>
        </div>`
        ];

        let gridOptObj = {
            gridOptions: {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: rowTemplate,
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate: columnDefTemplates[0]
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('Service Info. Type'),
                        field: 'serviceInfoType.description'
                    }
                ],
                onRegisterApi: function () {},
                data: []
            },
            setPageFunctions: function () { }
        };

        gridOptObj.setPageFunctions = function (f) { pgFnc = f };
        gridOptObj.gridOptions.onRegisterApi = function (gridApi) {
            $scope.gridApi = gridApi;
            $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                if (!sortColumns.length || !sortColumns[0].field) {
                    requestParams.sorting = null;
                } else {
                    requestParams.sorting = sortColumns[0].field + ' ' +
                        sortColumns[0].sort.direction;
                }

                pgFnc.actions.getTemplates();
            });
            gridApi.pagination.on.paginationChanged($scope,
                function (pageNumber, pageSize) {
                    requestParams.skipCount = (pageNumber - 1) * pageSize;
                    requestParams.maxResultCount = pageSize;

                    pgFnc.actions.getTemplates();
                });
        };

        return gridOptObj;
    };

    let DataDefinition = {
        WorkMode: {
            Add: 'ADD',
            Edit: 'EDIT'
        },
        GridOptions: function () {
            return {
                enableHorizontalScrollbar: false,
                enableVerticalScrollbar: false,
                paginationPageSizes: 0,
                paginationPageSize: 0,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: null,
                rowTemplate: '',
                columnDefs: [],
                onRegisterApi: {},
                data: []
            }
        } 
    };

}());
