
(function () {

    appModule.controller("tenant.views.engage.serviceinfo.templatebuilder2",
        [
            "$scope",
            "$state",
            "$stateParams",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.memberServiceInfoTemplate",
            function ($scope, $state, $stateParams, $modal, uiGridConstants, memberServiceInfoTemplateService) {

                var vm = this;

                let func = fnc($scope, $state, $modal, uiGridConstants, memberServiceInfoTemplateService, app, vm, abp);
                func.actions.init(App);

                vm.a = func.actions;
                vm.d = func.data;
                vm.e = func.events;

                func.actions.setWorkMode($stateParams);
                func.actions.initEditor();
                func.actions.getServiceInfoTypes().then(function () {
                    if (func.data.workMode == DataDefinition.WorkMode.Edit) {
                        func.actions.openTemplateForEdit();
                    }
                });

            }
        ]);


    let fnc = function ($scope, $state, $modal, uiGridConstants, memberServiceInfoTemplateService, app, vm, abp) {

        let dd = DataDefinition;

        let act = {
            findServiceInfoType: function (id) {
                let found =
                    f.data.serviceInfoTypes.find(function (t) { return t.id == id });
                return found;
            },
            validateTemplateInput: function () {
                if (!f.data.template.name || f.data.template.name == "") {
                    abp.notify.error(app.localize("NameIsRequired"));
                    return false;
                }
                if (f.data.template.serviceInfoTypeId == 0) {
                    abp.notify.error(app.localize("ServiceInfoTemplateTypeNotSelected"));
                    return false;
                }
                return true;
            }
        }

        let f = {
            data: {
                template: dd.MemberServiceInfoTemplate(),
                serviceInfoTypes: [],
                ui: {
                    selectedServiceInfoType: null,
                    saving: false,
                    editorLoading: false
                },
                templateId: 0,
                workMode: '',
                uploadUrl: abp.appPath + 'FileUpload/UploadFile'
            },
            constants: {
                workMode: {
                    add: dd.WorkMode.Add,
                    edit: dd.WorkMode.Edit
                }
            },
            controls: {
                editor: null
            },
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                initEditor: function () {
                    f.controls.editor = grapes(f.data.templateId,f);
                },
                saveTemplate: function () {
                    if (!act.validateTemplateInput()) return;
                    let save = function () {
                        f.data.ui.saving = true;
                        f.data.template.id = f.data.templateId;
                        f.controls.editor.setTemplateId(f.data.template.id);
                        f.controls.editor.setTemplateName(f.data.template.name);
                        f.controls.editor.setServiceInfoTypeId(f.data.template.serviceInfoTypeId);
                        f.controls.editor.setIsActive(f.data.template.isActive);
                        f.controls.editor.setStoredAsPlainText(f.data.template.storedAsPlainText);
                        f.controls.editor.setContentInText(f.actions.convertToPlain(f.controls.editor.getHtml()));
                        f.controls.editor.store(function (result) {
                            f.data.ui.saving = false;
                            abp.notify.success(app.localize('SuccessfullySaved'));
                            if (f.data.workMode == dd.WorkMode.Add) {
                                f.actions.setWorkMode(result.result)
                                f.actions.initEditor();
                            }
                            $scope.$apply();
                        });
                    };

                    if (f.data.template.isActive) {
                        f.actions.checkActiveTemplate().then(function (result) {
                            if (result && result.id != f.data.templateId) {
                                abp.message.confirm(
                                    app.localize('ActiveTemplateExists', result.name),
                                    function (isConfirmed) {
                                        if (isConfirmed) {
                                            // alert('Saved!');
                                            save();
                                        }
                                    }
                                )
                            } else {
                                save();
                            }
                        });
                    } else {
                        save();
                    }

                },
                uploadAssets: function () {
                    f.controls.editor.upload();
                },
                checkActiveTemplate: function () {
                    return new Promise(function (resolve, reject) {
                        memberServiceInfoTemplateService.getActiveTemplate({
                            id: f.data.template.serviceInfoTypeId
                        })
                            .success(function (result) {
                                resolve(result);
                            });
                    });

                },
                getServiceInfoTypes: function () {
                    return new Promise(function (resolve, reject) {
                        memberServiceInfoTemplateService.getMemberServiceInfoTypes({})
                            .success(function (result) {
                                f.data.serviceInfoTypes = result.items;
                                resolve();
                            });
                    })
                },
                loadTemplate: function () {
                    f.controls.editor.setTemplateId(f.data.template.id);
                    f.data.ui.editorLoading = true;
                    f.controls.editor.load(function () {
                        f.data.ui.editorLoading = false;
                        $scope.$apply();
                    });

                },
                setWorkMode: function (stateParams) {
                    f.data.templateId = stateParams.id;
                    if (f.data.templateId) {
                        f.data.workMode = dd.WorkMode.Edit;
                        f.data.template.id = f.data.templateId;
                    } else {
                        f.data.workMode = dd.WorkMode.Add;
                    }
                },
                openTemplateForEdit: function () {
                    memberServiceInfoTemplateService.getMemberServiceInfoTemplateForUpdate(
                        { id: f.data.templateId }
                    )
                        .success(function (result) {
                            f.data.template = result.memberServiceInfoTemplate;
                            f.data.ui.selectedServiceInfoType = act.findServiceInfoType(f.data.template.serviceInfoTypeId);
                            f.actions.loadTemplate();
                        })
                        .finally(function () { });
                },
                goBackToList: function () {
                    $state.go('tenant.serviceinfotemplates', {})
                },
                convertToPlain: function (html) {
                    var tempDivElement = document.getElementById("convertToPlainText");
                    tempDivElement.innerHTML = html;
                    let text = tempDivElement.textContent || tempDivElement.innerText || "";
                    return text;
                }
            },
            events: {
                serviceInfoTypeSelected: function () {
                    f.data.template.serviceInfoTypeId = f.data.ui.selectedServiceInfoType.id;
                }
            }
        };

        return f;
    };

    let DataDefinition = {
        MemberServiceInfoTemplate: function (data) {
            return {
                id: data ? data.id : 0,
                name: data ? data.name : null,
                serviceInfoTypeId: data ? data.serviceInfoTypeId : 0,
                template: data ? data.template : null,
                isActive: data? data.isActive: false,
                storedAsPlainText: data? data.storedAsPlainText: false
            };
        },
        WorkMode: {
            Add: 'ADD',
            Edit: 'EDIT'
        }
    };

    let grapes = function (templateId,func) {

        let loadUrl = `/MemberServiceInfo/LoadTemplate2?id=`;
        let getLoadUrl = function () {
            return loadUrl + templateId;
        };

        let storageManager = {
            id: '',
            type: 'remote',        
            autosave: false,       
            autoload: false,       
            stepsBeforeSave: 1,    
            storeComponents: true, 
            storeStyles: true,     
            storeHtml: true,       
            storeCss: true,        
            urlStore: '/MemberServiceInfo/StoreTemplate',
            urlLoad: getLoadUrl(),
            params: {
                templateName: '',
                serviceInfoTypeId: 0,
                isActive: false,
                storedAsPlainText: false,
                contentInText: ''
            },
            headers: {},
        };

        let editor = grapesjs.init({
            height: '800px',
            showOffsets: 1,
            noticeOnUnload: 0,
            storageManager: storageManager,
            container: '#gjs',
            fromElement: true,

            plugins: ['gjs-preset-webpage'],
            pluginsOpts: {
                'gjs-preset-webpage': {}
            },
            assetManager: {
                upload: func.data.uploadUrl,
                uploadName: 'files',
                autoAdd: 0
            }
        });

        editor.setTemplateId = function (id) {
            storageManager.params.templateId = id;
            templateId = id;
            //console.log(editor);
            storageManager.urlLoad = loadUrl + id;
            //editor.StorageManager = storageManager;
            //editor.StorageManager.getConfig().urlLoad = loadUrl + id;
        };

        editor.on('asset:upload:response', function (result) {
            console.log(result.result.fileSystemName);
            editor.AssetManager.add(result.result.fileSystemName)
        });

        editor.setTemplateName = function (name) { storageManager.params.templateName = name; };
        editor.setServiceInfoTypeId = function (id) { storageManager.params.serviceInfoTypeId = id; }
        editor.setIsActive = function (value) { storageManager.params.isActive = value; }
        editor.setStoredAsPlainText = function (value) { storageManager.params.storedAsPlainText = value; }
        editor.setContentInText = function (value) { storageManager.params.contentInText = value; }
        return editor;
    }
}());