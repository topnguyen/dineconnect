﻿(function () {
    appModule.controller('common.views.syncer.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.sync', "abp.services.app.tenantSettings", '$sce',
        function ($scope, $modal, uiGridConstants, syncService, tenantSettingsService, $sce) {
            var vm = this;

            vm.syncCount = null;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            $scope.beforeRender = function ($view, $dates, $leftDate, $upDate, $rightDate) {
                var activeDate = moment().subtract(1, $view).add(1, 'minute');
                $dates.filter(function (date) {
                    return date.localDateValue() >= activeDate.valueOf();
                }).forEach(function (date) {
                    date.selectable = false;
                });
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            $scope.beforeRenderPull = function ($view, $dates, $leftDate, $upDate, $rightDate) {
                var activeDate = moment().subtract(1, $view).add(1, 'minute');
                $dates.filter(function (date) {
                    return date.localDateValue() >= activeDate.valueOf();
                }).forEach(function (date) {
                    date.selectable = false;
                });
            };
            vm.loading = false;
            vm.status = null;
            vm.location = 0;

            vm.statuses = [];
            vm.getStatuses = function () {
                syncService.getSyncStatusDtos()
                    .success(function (result) {
                        vm.statuses = result;
                    });
            };

            vm.statusItem = "";
            vm.getEditionValue = function (value) {
                var status = -1;
                vm.statuses.some(function (item, key) {
                    if (item.status === value) {
                        status = item;
                        return true;
                    }
                });
                //var status = vm.statuses.findIndex(x => x.status === vm.status);
                if (status !== -1)
                    vm.statusItem = angular.toJson(status);
                //vm.getAll()
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Location'),
                        field: 'locationName'
                    },
                    {
                        name: app.localize('LastPush'),
                        field: 'lastPush',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    },
                    {
                        name: app.localize('LastPull'),
                        field: 'lastPull',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 100
                    },
                    {
                        name: app.localize('Details'),
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-click="grid.appScope.openModel(row.entity)" class="btn btn-default btn-xs" title="' + app.localize('Details') + '"><i class="fa fa-dashcube"></i></button>' +
                            '</div>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                syncService.getAllLastSyncs({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    locationId: vm.location,
                    status: vm.statusItem
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.openModel = function (model) {
                syncService.getLocationSyncer({
                    locationId: model.locationId
                }).success(function (result) {
                    $modal.open({
                        templateUrl: '~/App/tenant/views/syncer/openModel.cshtml',
                        controller: 'common.views.syncer.opemModel as vm',
                        backdrop: 'static',
                        keyboard: false,
                        resolve: {
                            syncer: function () {
                                return result;
                            }
                        }
                    });
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.clear = function () {
                vm.location = 0;
                vm.currentLocation = null;
                vm.lastPush = "";
                vm.lastPull = "";
                vm.status = "";
                vm.statusItem = "";

                vm.getAll();
            };

            vm.exportToExcel = function () {
                syncService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.openLocation = function () {
                
                vm.locationGroup = app.createLocationInputForCreateSingleLocation();

                var modalInstance = $modal.open({
                    templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                    controller: "tenant.views.connect.location.select.location as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        location: function () {
                            return vm.locationGroup;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    if (result.locations.length > 0) {
                        vm.currentLocation = result.locations[0];
                        vm.location = result.locations[0].id;
                    } else {
                        vm.location = 0;
                        vm.currentLocation = null;
                    }
                });
            };

            vm.getListByStatus = function (status) {
                vm.statusItem = angular.toJson(status);
                vm.getAll();
            };

            vm.getStatuses();
            vm.getAll();
        }]);
})();