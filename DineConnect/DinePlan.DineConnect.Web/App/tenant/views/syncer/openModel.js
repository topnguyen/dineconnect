﻿(function () {
    appModule.controller('common.views.syncer.opemModel', [
        '$scope', '$uibModalInstance', 'syncer',
        function ($scope, $modalInstance, syncer) {
            var vm = this;
            vm.syncer = syncer;

            vm.close = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();