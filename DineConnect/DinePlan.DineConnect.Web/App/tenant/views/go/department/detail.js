﻿
(function () {
    appModule.controller('tenant.views.go.departments.detail', [
        '$scope', 'uiGridConstants', '$state', '$stateParams', 'abp.services.app.dineGoDepartment', 'abp.services.app.department', 'abp.services.app.dineGoCharge',
        function ($scope, uiGridConstants, $state, $stateParams, dinegodepartmentService, departmentService, dinegochargeService) {
            var vm = this;

            vm.saving = false;
            vm.dinegodepartment = null;
            vm.dinegodepartmentdetail = null;
            vm.dinegodepartmentId = $stateParams.id;
            vm.uilimit = 20;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };

            vm.save = function () {
                if (vm.isUndefinedOrNull(vm.dinegodepartment.label)) {
                    abp.notify.error('Name is mandatory');
                    return;
                }
                if (vm.isUndefinedOrNull(vm.dinegodepartment.departmentId)) {
                    abp.notify.error('Department is mandatory');
                    return;
                }
                vm.saving = true;
                vm.dinegodepartment.dineGoCharges = vm.dineGoCharges;
                dinegodepartmentService.createOrUpdateDineGoDepartment({
                    dineGoDepartment: vm.dinegodepartment,
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $state.go('tenant.dinegodepartment');
            };

            function getDepartment() {
                vm.saving = true;
                departmentService.getDepartmentsForCombobox({
                }).success(function (result) {
                    vm.departments = result.items;
                }).finally(function (result) {
                    vm.saving = false;
                });
            }
            getDepartment();

            function getCharges() {
                vm.saving = true;
                dinegochargeService.getChargesListDtos({
                }).success(function (result) {
                    vm.charges = result;
                }).finally(function (result) {
                    vm.saving = false;
                });
            }
            getCharges();

            vm.dineGoDepartmentDetailOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Charge"),
                        field: "label"
                    },
                    {
                        name: app.localize("Delete"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.removePortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Delete") + "\"><i class=\"fa fa-trash\"></i></button>" +
                            "</div>"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        init();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        init();
                    });
                },
                data: []
            };


            vm.removePortion = function (myObject) {
                for (var i = 0; i < vm.dineGoCharges.length; i++) {
                    var obj = vm.dineGoCharges[i];

                    if (obj.id == myObject.id) {
                        vm.dineGoCharges.splice(i, 1);
                    }
                }
            };
            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };

            vm.savePortion = function () {
                if (vm.dinegodepartmentdetail==null) {
                    abp.notify.error('Charge is mandatory');
                    return;
                }
                var successFlag = false;
                vm.dineGoCharges.some(function (value, key) {
                    if (value.id == vm.dinegodepartmentdetail.id) {
                        abp.notify.error('Record Already Exists');
                        successFlag = true;
                        return successFlag;
                    }
                });
                if (!successFlag) {
                    vm.dineGoCharges.push(vm.dinegodepartmentdetail);
                    vm.dineGoDepartmentDetailOptions.data = vm.dineGoCharges;
                    vm.dineGoDepartmentDetailOptions.totalItems = vm.dineGoCharges.length;
                }
                vm.dinegodepartmentdetail = null;
            }

            function init() {
                dinegodepartmentService.getDineGoDepartmentForEdit({
                    id: vm.dinegodepartmentId
                }).success(function (result) {
                    vm.dinegodepartment = result.dineGoDepartment;
                    vm.dineGoCharges = vm.dinegodepartment.dineGoCharges;
                    vm.dineGoDepartmentDetailOptions.data = vm.dineGoCharges;
                    vm.dineGoDepartmentDetailOptions.totalItems = vm.dineGoCharges.length;

                    if (vm.dinegodepartment.id == null) {
                        vm.uilimit = 20;
                    }
                    else {
                        vm.uilimit = null;
                    }
                });
            }
            init();
        }
    ]);
})();

