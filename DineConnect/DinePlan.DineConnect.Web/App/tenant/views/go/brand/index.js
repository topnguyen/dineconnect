﻿
(function () {
    appModule.controller('tenant.views.go.brand.index', [
        '$scope', '$state' ,'uiGridConstants', 'abp.services.app.dineGoBrand',
        function ($scope, $state, uiGridConstants, dinegobrandService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Go.DineGoBrand.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Go.DineGoBrand.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Go.DineGoBrand.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.gridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.editDineGoBrand(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.permissions.delete" ng-click="grid.appScope.deleteDineGoBrand(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Label'),
                        field: 'label'
                    },
                    {
                        name: app.localize('Location'),
                        field: 'locationName'
                    },
                    {
                        name: app.localize('ScreenMenu'),
                        field: 'screenMenuName'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };



            vm.getAll = function () {
                vm.loading = true;
                dinegobrandService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.gridOptions.totalItems = result.totalCount;
                    vm.gridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            function openDineGoBrand(objId) {
                $state.go("tenant.detaildinegobrand", {
                    id: objId
                });
            }
            vm.editDineGoBrand = function (myObj) {
                openDineGoBrand(myObj.id);
            };

            vm.createDineGoBrand = function () {
                openDineGoBrand(null);
            };
            vm.deleteDineGoBrand = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteDineGoBrandWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            dinegobrandService.deleteDineGoBrand({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.exportToExcel = function () {
                dinegobrandService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    app.downloadTempFile(result);
                });
            };
            vm.clear = function () {
                vm.filterText = null;
                vm.getAll();
            };

            vm.getAll();
        }]);
})();

