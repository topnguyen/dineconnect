﻿
(function () {
    appModule.controller('tenant.views.go.brand.detail', [
        '$scope', '$state', '$stateParams', 'uiGridConstants', 'abp.services.app.dineGoBrand', 'abp.services.app.location',
        function ($scope, $state, $stateParams, uiGridConstants, dinegobrandService, locationService) {
            var vm = this;

            vm.saving = false;
            vm.dinegobrand = null;
            vm.dinegobranddetail = null;
            vm.dinegobrandId = $stateParams.id;
            vm.uilimit = 20;
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };

            vm.save = function () {
                if (vm.isUndefinedOrNull(vm.dinegobrand.label)) {
                    abp.notify.error('Name is mandatory');
                    return;
                }
                if (vm.isUndefinedOrNull(vm.dinegobrand.locationId)) {
                    abp.notify.error('Location is mandatory');
                    return;
                }
                if (vm.isUndefinedOrNull(vm.dinegobrand.screenMenuId)) {
                    abp.notify.error('Screen Menu is mandatory');
                    return;
                }
                 vm.saving = true;
                vm.dinegobrand.dineGoDepartments = vm.dineGoDepartments;
                dinegobrandService.createOrUpdateDineGoBrand({
                    dineGoBrand: vm.dinegobrand
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.dinegobrand');
            };

            function getScreenMenus() {
                vm.saving = true;
                dinegobrandService.getScreenMenuForCombobox({
                    futureData: false
                }).success(function (result) {
                    vm.screenmenus = result.items;
                }).finally(function (result) {
                    vm.saving = false;
                });
            }
            getScreenMenus();


            vm.reflocation = [];

            function fillDropDownLocation() {
                vm.saving = true;
                locationService.getLocationForCombobox({}).success(function (result) {
                    vm.reflocation = result.items;
                }).finally(function (result) {
                    vm.saving = false;
                });
            }
            fillDropDownLocation();

            function getDepartments() {
                vm.saving = true;
                dinegobrandService.getDineGodepartmentForCombobox({
                }).success(function (result) {
                    vm.departments = result;
                }).finally(function (result) {
                    vm.saving = false;
                });
            }
            getDepartments();

            vm.dinegobrandDetailOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Department"),
                        field: "label"
                    },
                    {
                        name: app.localize("Delete"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.removePortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Delete") + "\"><i class=\"fa fa-trash\"></i></button>" +
                            "</div>"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        init();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        init();
                    });
                },
                data: []
            };
           
            vm.removePortion = function (myObject) {
                for (var i = 0; i < vm.dineGoDepartments.length; i++) {
                    var obj = vm.dineGoDepartments[i];

                    if (obj.id == myObject.id) {
                        vm.dineGoDepartments.splice(i, 1);
                    }
                }
            };

           

            vm.savePortion = function () {
                if (vm.dinegobranddetail == null) {
                    abp.notify.error('Department is mandatory');
                    return;
                }
                var successFlag = false;
                vm.dineGoDepartments.some(function (value, key) {
                    if (value.id == vm.dinegobranddetail.id) {
                        abp.notify.error('Record Already Exists');
                        successFlag = true;
                        return successFlag;
                    }
                });
                if (!successFlag) {
                    vm.dineGoDepartments.push(vm.dinegobranddetail);
                    vm.dinegobrandDetailOptions.data = vm.dineGoDepartments;
                    vm.dinegobrandDetailOptions.totalItems = vm.dineGoDepartments.length;
                }
                vm.dinegobranddetail = null;
            }

            function init() {
                dinegobrandService.getDineGoBrandForEdit({
                    id: vm.dinegobrandId
                }).success(function (result) {
                    vm.dinegobrand = result.dineGoBrand;
                    vm.dineGoDepartments = vm.dinegobrand.dineGoDepartments;
                    vm.dinegobrandDetailOptions.data = vm.dineGoDepartments;
                    vm.dinegobrandDetailOptions.totalItems = vm.dineGoDepartments.length;
                    if (vm.dinegobrand.id == null)
                    {
                        vm.uilimit = 20;
                    }
                    else
                    {
                        vm.uilimit = null;
                    }
                });
            }
            init();
        }
    ]);
})();

