﻿
(function () {
    appModule.controller('tenant.views.go.charge.index', [
        '$scope', '$state', 'uiGridConstants', 'abp.services.app.dineGoCharge',
        function ($scope, $state, uiGridConstants, dinegochargeService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Go.DineGoCharge.Create'),
                edit: abp.auth.hasPermission('Pages.Tenant.Go.DineGoCharge.Edit'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Go.DineGoCharge.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.editDineGoCharge(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.permissions.delete" ng-click="grid.appScope.deleteDineGoCharge(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Label'),
                        field: 'label'
                    },
                    {
                        name: app.localize('TransactionTypeName'),
                        field: 'transactionTypeName'
                    },
                    {
                        name: app.localize('IsPercent'),
                        field: 'isPercent',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.isPercent" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.isPercent" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                    },
                    {
                        name: app.localize('ChargeValue'),
                        field: 'chargeValue'
                    },
                    {
                        name: app.localize('TaxIncluded'),
                        field: 'taxIncluded',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.taxIncluded" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.taxIncluded" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                dinegochargeService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            function openDineGoCharge(objId) {
                $state.go("tenant.detaildinegopaymenttypecharge", {
                    id: objId
                });
            }
            vm.editDineGoCharge = function (myObj) {
                openDineGoCharge(myObj.id);
            };

            vm.createDineGoCharge = function () {
                openDineGoCharge(null);
            };
            vm.deleteDineGoCharge = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteDineGoChargeWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            dinegochargeService.deleteDineGoCharge({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };
            vm.exportToExcel = function () {
                dinegochargeService.getAllToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    app.downloadTempFile(result);
                });
            };

            vm.clear = function () {
                vm.filterText = null;
                vm.getAll();
            };

            vm.getAll();
        }]);
})();

