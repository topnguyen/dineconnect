﻿
(function () {
    appModule.controller('tenant.views.go.charge.detail', [
        '$scope', '$state', '$stateParams', 'abp.services.app.dineGoCharge', 'abp.services.app.connectLookup',
        function ($scope, $state, $stateParams, dinegochargeService, connectLookupService) {
            var vm = this;

            vm.saving = false;
            vm.dinegocharge = null;
            vm.dinegochargeId = $stateParams.id;
            vm.uilimit = 20;

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };

            vm.save = function () {
                if (vm.isUndefinedOrNull(vm.dinegocharge.label)) {
                    abp.notify.error('Name is mandatory');
                    return;
                }
                if (vm.isUndefinedOrNull(vm.dinegocharge.transactionTypeId)) {
                    abp.notify.error('Transaction Type is mandatory');
                    return;
                }
                if (vm.isUndefinedOrNull(vm.dinegocharge.chargeValue)) {
                    abp.notify.error('Charge is mandatory');
                    return;
                }
                vm.saving = true;
                dinegochargeService.createOrUpdateDineGoCharge({
                    dineGoCharge: vm.dinegocharge
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.dinegopaymenttypecharge');
            };

            function getTransactionType() {
                connectLookupService.getTransactionTypesForCombobox({}).success(function (result) {
                    vm.transactionTypes = result.items;
                });
            }
            getTransactionType();

            function init() {
                dinegochargeService.getDineGoChargeForEdit({
                    id: vm.dinegochargeId
                }).success(function (result) {
                    vm.dinegocharge = result.dineGoCharge;
                    if (vm.dinegocharge.id == null) {
                        vm.uilimit = 20;
                    }
                    else {
                        vm.uilimit = null;
                    }
                });
            }
            init();
        }
    ]);
})();

