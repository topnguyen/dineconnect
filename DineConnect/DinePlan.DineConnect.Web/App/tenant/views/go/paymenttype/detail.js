﻿(function () {
    appModule.controller('tenant.views.go.paymenttype.detail', [
        '$scope',  '$state', '$stateParams', 'abp.services.app.dineGoPaymentType', 'abp.services.app.connectLookup',
        function ($scope,  $state, $stateParams, dineGoPaymentType, connectLookupService) {
            var vm = this;
            vm.saving = false;
            vm.uilimit = 20;
            vm.dineGoPaymentTypeId = $stateParams.id;

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };

            vm.save = function (argOption) {
                if (vm.isUndefinedOrNull(vm.dineGoPaymentType.label)) {
                    abp.notify.error('Name is mandatory');
                    return;
                }
                if (vm.isUndefinedOrNull(vm.dineGoPaymentType.paymentTypeId)) {
                    abp.notify.error('Payment Type is mandatory');
                    return;
                }
                vm.saving = true;
                dineGoPaymentType.createOrUpdateDineGoPaymentType({
                    dineGoPaymentType: vm.dineGoPaymentType
                }).success(function (result) {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.dineGoPaymentType.id = result.id;
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.dinegopaymenttype');
            };

            function getAllPaymentTypes() {
                vm.saving = true;
                connectLookupService.getPaymentTypes({
                }).success(function (result) {
                    vm.payments = result.items;
                }).finally(function (result) {
                    vm.saving = false;
                });
            }
            getAllPaymentTypes();

            function init() {
                vm.saving = true;
                dineGoPaymentType.getDineGoPaymentTypeForEdit({
                    id: vm.dineGoPaymentTypeId
                }).success(function (result) {
                    vm.dineGoPaymentType = result.dineGoPaymentType;
                    if (vm.dineGoPaymentType.id == null) {
                        vm.uilimit = 20;
                    }
                    else {
                        vm.uilimit = null;
                    }
                }).finally(function () {
                    vm.saving = false;
                });
            }

            init();
        }
    ]);
})();