﻿
(function () {
    appModule.controller('tenant.views.go.device.detail', [
        '$scope', '$state', '$stateParams', 'uiGridConstants', 'abp.services.app.dineGoDevice', 'FileUploader',
        function ($scope, $state, $stateParams, uiGridConstants, dinegodeviceService, fileUploader) {
            var vm = this;

            vm.saving = false;
            vm.dinegodevice = null;
            vm.dinegodevicebranddetail = null;
            vm.dinegodevicepaymenttypedetail = null;
            vm.dinegodeviceId = $stateParams.id;
            vm.uilimit = 50;
            vm.fileTag = '';
            vm.isUpload = false;

            vm.dineGoBrands = [];
            vm.dineGoPaymentTypes = [];

            vm.idleContents = [];
            vm.bannerContents = [];
            vm.logo = [];
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val === '';
            };

            vm.save = function () {
                if (vm.isUndefinedOrNull(vm.dinegodevice.name)) {
                    abp.notify.error('Name is mandatory');
                    return;
                }
                if (vm.isUndefinedOrNull(vm.dinegodevice.idleTime)) {
                    abp.notify.error('Idle Time is mandatory');
                    return;
                }
                vm.saving = true;
                vm.dinegodevice.dineGoBrands = vm.dineGoBrands;
                vm.dinegodevice.dineGoPaymentTypes = vm.dineGoPaymentTypes;
                vm.deviceThemeSetting.idleContents = angular.toJson(vm.idleContents);
                vm.deviceThemeSetting.bannerContents = angular.toJson(vm.bannerContents);
                vm.deviceThemeSetting.logo = angular.toJson(vm.logo);
                vm.deviceThemeSetting.backgroundColor = vm.mainButtonColor;

                dinegodeviceService.createOrUpdateDineGoDevice({
                    dineGoDevice: vm.dinegodevice,
                    deviceThemeSettings: vm.deviceThemeSetting
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.cancel();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.cancel = function () {
                $state.go('tenant.dinegodevice');
            };

            function getBrands() {
                vm.saving = true;
                dinegodeviceService.getDineGoBrandForCombobox({
                }).success(function (result) {
                    vm.brands = result;
                }).finally(function (result) {
                    vm.saving = false;
                });
            }
            getBrands();
            function getPaymentTypes() {
                vm.saving = true;
                dinegodeviceService.getDineGoPaymentTypeForCombobox({
                }).success(function (result) {
                    vm.paymentTypes = result;
                }).finally(function (result) {
                    vm.saving = false;
                });
            }
            getPaymentTypes();

            vm.setBrandGrid = function () {
                vm.dineGoDeviceBrandDetailOptions = {
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                    columnDefs: [
                        {
                            name: app.localize("Brand"),
                            field: "label"
                        },
                        {
                            name: app.localize("Delete"),
                            cellTemplate:
                                "<div class=\"ui-grid-cell-contents text-center\">" +
                                "  <button ng-click=\"grid.appScope.removeBrandPortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Delete") + "\"><i class=\"fa fa-trash\"></i></button>" +
                                "</div>"
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridbrandApi = gridApi;
                        $scope.gridbrandApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                            if (!sortColumns.length || !sortColumns[0].field) {
                                requestParams.sorting = null;
                            } else {
                                requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                            }
                            init();
                        });
                        gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                            requestParams.skipCount = (pageNumber - 1) * pageSize;
                            requestParams.maxResultCount = pageSize;
                            init();
                        });
                    },
                    data: vm.dineGoBrands
                };
            }
            vm.setBrandGrid();

            vm.removeBrandPortion = function (myObject) {
                for (var i = 0; i < vm.dineGoBrands.length; i++) {
                    var obj = vm.dineGoBrands[i];

                    if (obj.id == myObject.id) {
                        vm.dineGoBrands.splice(i, 1);
                    }
                }
            };


            vm.saveBrandPortion = function () {
                if (vm.dinegodevicebranddetail == null) {
                    abp.notify.error('Brand is mandatory');
                    return;
                }
                var successFlag = false;
                vm.dineGoBrands.some(function (value, key) {
                    if (value.id == vm.dinegodevicebranddetail.id) {
                        abp.notify.error('Record Already Exists');
                        successFlag = true;
                        return successFlag;
                    }
                });
                if (!successFlag) {
                    vm.dineGoBrands.push(vm.dinegodevicebranddetail);
                    vm.dineGoDeviceBrandDetailOptions.data = vm.dineGoBrands;
                    vm.dineGoDeviceBrandDetailOptions.totalItems = vm.dineGoBrands.length;
                }
                vm.dinegodevicebranddetail = null;
            };

            vm.setPaymentGrid = function () {
                vm.dineGoDevicePaymentTypeDetailOptions = {
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                    columnDefs: [
                        {
                            name: app.localize("PaymentType"),
                            field: "label"
                        },
                        {
                            name: app.localize("Delete"),
                            cellTemplate:
                                "<div class=\"ui-grid-cell-contents text-center\">" +
                                "  <button ng-click=\"grid.appScope.removePaymentTypePortion(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Delete") + "\"><i class=\"fa fa-trash\"></i></button>" +
                                "</div>"
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridpaymenttypeApi = gridApi;
                        $scope.gridpaymenttypeApi.core.on.sortChanged($scope,
                            function (grid, sortColumns) {
                                if (!sortColumns.length || !sortColumns[0].field) {
                                    requestParams.sorting = null;
                                }
                                else {
                                    requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                                }
                                init();
                            });
                        gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                            requestParams.skipCount = (pageNumber - 1) * pageSize;
                            requestParams.maxResultCount = pageSize;
                            init();
                        });
                    },
                    data: vm.dineGoPaymentTypes
                };
            }
            vm.setPaymentGrid();

            vm.removePaymentTypePortion = function (myObject) {
                for (var i = 0; i < vm.dineGoPaymentTypes.length; i++) {
                    var obj = vm.dineGoPaymentTypes[i];

                    if (obj.id == myObject.id) {
                        vm.dineGoPaymentTypes.splice(i, 1);
                    }
                }
            };


            vm.savePaymentTypePortion = function () {
                if (vm.dinegodevicepaymenttypedetail == null) {
                    abp.notify.error('Payment Type is mandatory');
                    return;
                }
                var successFlag = false;
                vm.dineGoPaymentTypes.some(function (value, key) {
                    if (value.id == vm.dinegodevicepaymenttypedetail.id) {
                        abp.notify.error('Record Already Exists');
                        successFlag = true;
                        return successFlag;
                    }
                });
                if (!successFlag) {
                    vm.dineGoPaymentTypes.push(vm.dinegodevicepaymenttypedetail);
                    vm.dineGoDevicePaymentTypeDetailOptions.data = vm.dineGoPaymentTypes;
                    vm.dineGoDevicePaymentTypeDetailOptions.totalItems = vm.dineGoPaymentTypes.length;
                }
                vm.dinegodevicepaymenttypedetail = null;
            };


            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadFile'
            });

            vm.uploader.filters.push({
                name: 'imageFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    return true;
                }
            });

            vm.uploader.onBeforeUploadItem = function (fileitem) {
                fileitem.formData.push({ fileTag: vm.fileTag });
            };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response !== null) {
                    if (response.result.fileTag == 'Idle Content') {
                        if (response.result !== null) {
                            if (response.result.fileName !== null) {
                                vm.idleContents.push(response.result);
                                vm.refreshImage = true;
                                if (!vm.isUpload) {
                                    vm.save();
                                }
                            }
                        }
                    }
                    else if (response.result.fileTag == 'Banner Content') {
                        if (response.result !== null) {
                            if (response.result.fileName !== null) {
                                vm.bannerContents.push(response.result);
                                vm.refreshImage = true;
                                if (!vm.isUpload) {
                                    vm.save();
                                }
                            }
                        }
                    }
                    else if (response.result.fileTag == 'Logo') {
                        vm.logo = [];
                        if (response.result !== null) {
                            if (response.result.fileName !== null) {
                                vm.logo.push(response.result);
                                vm.refreshImage = true;
                                if (!vm.isUpload) {
                                    vm.save();
                                }
                            }
                        }
                    }
                    $("#idleFile").val(null);
                    $("#bannerFile").val(null);
                    $("#logoFile").val(null);
                }
            };

            vm.downloader = function (file) {
                location.href = abp.appPath + 'FileUpload/DownloadFile?fileName=' + file.fileName + '&url=' + file.fileSystemName;
            };

            vm.removeFile = function (findex) {
                if (vm.fileTag == 'Idle Content') {
                    vm.idleContents.splice(findex, 1);
                }
                else if (vm.fileTag == 'Banner Content') {
                    vm.bannerContents.splice(findex, 1);
                }
                else if (vm.fileTag == 'Logo') {
                    vm.logo.splice(findex, 1);
                }

                vm.refreshImage = true;
            };

            vm.uploadFile = function (isuload) {
                vm.isUpload = isuload;
                var file = vm.uploader.queue[vm.uploader.queue.length - 1];
                file.upload();
            };



            function init() {
                vm.loading = true;
                dinegodeviceService.getDineGoDeviceForEdit({
                    id: vm.dinegodeviceId
                }).success(function (result) {
                    vm.dinegodevice = result.dineGoDevice;
                    vm.deviceThemeSetting = result.deviceThemeSettings;

                    vm.dineGoBrands = vm.dinegodevice.dineGoBrands;
                    vm.dineGoPaymentTypes = vm.dinegodevice.dineGoPaymentTypes;

                    vm.dineGoDeviceBrandDetailOptions.data = vm.dinegodevice.dineGoBrands;
                    vm.dineGoDeviceBrandDetailOptions.totalItems = vm.dinegodevice.dineGoBrands.length;

                    vm.dineGoDevicePaymentTypeDetailOptions.data = vm.dineGoPaymentTypes;
                    vm.dineGoDevicePaymentTypeDetailOptions.totalItems = vm.dineGoPaymentTypes.length;
                    if (vm.dinegodevice.id == null) {
                        vm.uilimit = 50;
                    }
                    else {
                        vm.uilimit = null;
                        vm.idleContents = vm.deviceThemeSetting.idleContents ? angular.fromJson(vm.deviceThemeSetting.idleContents) : [];
                        vm.bannerContents = vm.deviceThemeSetting.bannerContents ? angular.fromJson(vm.deviceThemeSetting.bannerContents) : [];
                        vm.logo = vm.deviceThemeSetting.logo ? angular.fromJson(vm.deviceThemeSetting.logo) : [];
                        if (vm.deviceThemeSetting.backgroundColor != null) {
                            vm.mainButtonColor = vm.deviceThemeSetting.backgroundColor;
                        }
                    }

                }).finally(function (result) {
                    vm.loading = false;
                });
            }
            init();
        }
    ]);
})();

