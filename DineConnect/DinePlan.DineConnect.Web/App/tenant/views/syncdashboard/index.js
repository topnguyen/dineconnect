﻿(function () {
    appModule.controller("common.views.syncdashboard.index",
        [
            "$scope"
            , "$uibModal"
            , "uiGridConstants"
            , "abp.services.app.tenantSettings"
            , "abp.services.app.commonLookup"
            , "abp.services.app.syncDashboard",
            function ($scope
                , $modal
                , uiGridConstants
                , tenantSettingsService
                , commonLookup
                , syncDashboardService
            ) {
                var vm = this;
                vm.loading = false;
                vm.advancedFiltersAreShown = false;
                $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
                $scope.format = $scope.formats[0];
                $scope.settings = {
                    dropdownToggleState: false,
                    time: {
                        fromHour: "06",
                        fromMinute: "00",
                        toHour: "23",
                        toMinute: "59   "
                    },
                    noRange: false,
                    format: 24,
                    noValidation: true
                };
                vm.unsynced = false;
                vm.noFilter = false;
                vm.advancedFiltersAreShown = false;
                vm.totalTicketAmount = "";
                vm.totalTickets = "";
                vm.totalUnsynced = "";
                vm.totalItems = "";

                vm.totalFail = "";
                vm.totalPass = "";
                vm.totalTran = "";


                vm.requestParams = {
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: null,
                    userId: abp.session.userId,
                };
                $scope.settings = {
                    dropdownToggleState: false,
                    time: {
                        fromHour: "06",
                        fromMinute: "30",
                        toHour: "23",
                        toMinute: "30"
                    },
                    noRange: false,
                    format: 24,
                    noValidation: true
                };
                var todayAsString = moment().format("YYYY-MM-DD");
                vm.dateRangeOptions = app.createDateRangePickerOptions();
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };

                $scope.$on("$viewContentLoaded",
                    function () {
                        App.initAjax();
                    });

                vm.getEditionValue = function (item) {
                    return item.displayText;
                };

                vm.decimals = null;
                vm.getDecimals = function () {
                    tenantSettingsService.getAllSettings()
                        .success(function (result) {
                            vm.decimals = result.connect.decimals;
                        }).finally(function () {
                        });
                };
                vm.locationsCode = [];
                vm.$onInit = function () {
                    localStorage.clear();
                    vm.getLocations();
                };
                vm.$viewContentLoaded = function () {
                };

                // #region Grid
                vm.gridOptions = {
                    showColumnFooter: true,
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    columnDefs: [

                        {
                            name: app.localize("Location") + app.localize("Name"),
                            field: "location.name"
                        },
                        {
                            name: app.localize("Ticket") + app.localize("No"),
                            field: "ticketItem.ticket.ticketNumber"
                        },
                        {
                            name: app.localize("Amount"),
                            field: "ticketItem.ticket.totalAmount"
                        },
                        {
                            name: app.localize("Date"),
                            field: "ticketItem.ticket.ticketCreatedTime",
                            cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\''
                        },
                        {
                            name: app.localize("Log"),
                            enableSorting: false,
                            width: 150,
                            cellTemplate:
                                "<div class=\"ui-grid-cell-contents text-center\">" +
                                "  <button class=\"btn btn-default btn-xs\" ng-click=\"grid.appScope.showDetails(row.entity)\">Click</button>" +
                                "</div>"
                        },
                        {
                            name: app.localize("Operations"),
                            field: "operations"
                        },
                        {
                            name: 'Re-Process',
                            enableSorting: false,
                            width: 200,
                            headerCellTemplate: '<span></span>',
                            cellTemplate:
                                '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-primary btn-xs" ng-click="grid.appScope.reProcess(row.entity)">Re-Process</button>' +
                                '</div>'
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope,
                            function (grid, sortColumns) {
                                if (!sortColumns.length || !sortColumns[0].field) {
                                    vm.requestParams.sorting = null;
                                } else {
                                    vm.requestParams.sorting =
                                        sortColumns[0].field + " " + sortColumns[0].sort.direction;
                                }
                                vm.getAll();
                            });
                        gridApi.pagination.on.paginationChanged($scope,
                            function (pageNumber, pageSize) {
                                vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                                vm.requestParams.maxResultCount = pageSize;
                                vm.getAll();
                            });
                    },
                    data: []
                };
                vm.disableExport = false;

                vm.toggleFooter = function () {
                    $scope.gridOptions.showGridFooter = !$scope.gridOptions.showGridFooter;
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                };

                vm.toggleColumnFooter = function () {
                    $scope.gridOptions.showColumnFooter = !$scope.gridOptions.showColumnFooter;
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                };

                // #endregion
                // #region Location

                vm.intiailizeOpenLocation = function () {
                    vm.locationGroup = app.createLocationInputForSearch();
                };
                vm.intiailizeOpenLocation();

                vm.openLocation = function () {
                    const modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return vm.locationGroup;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        vm.locationGroup = result;
                    });
                };
                vm.showDetails = function (postdata) {
                    $modal.open({
                        templateUrl: "~/App/tenant/views/syncdashboard/ticketModal.cshtml",
                        controller: "tenant.views.syncdashboard.ticketModal as vm",
                        backdrop: "static",
                        resolve: {
                            postId: function () {
                                return postdata.id;
                            }
                        }
                    });
                };
                vm.reProcess = function (postdata) {
                    abp.message.confirm("",
                        app.localize("DoYouWantToReProcess"),
                        function (isConfirmed) {
                            if (isConfirmed == true) {
                                abp.notify.info(app.localize("Re-Processing"));
                                syncDashboardService.reProcess(postdata.id)
                                    .success(function () {
                                        vm.loading = true;
                                        abp.ui.clearBusy("#MyLoginForm");
                                        vm.getReprocess(postdata.id);
                                        vm.getAll();
                                        vm.getTotal();
                                    });
                            }
                            vm.loading = false;
                            abp.ui.clearBusy();
                        });
                };
                // #endregion
                // #region Exports
                vm.getCurrentTicketInput = function () {
                    return {
                        startDate: moment(vm.dateRangeModel.startDate).lang("en").format($scope.format),
                        endDate: moment(vm.dateRangeModel.endDate).lang("en").format($scope.format),
                        locationGroup: vm.locationGroup,
                        processed: vm.unsynced == false ? null : false,
                        noFilter: vm.noFilter,
                        skipCount: vm.requestParams.skipCount,
                        maxResultCount: vm.requestParams.maxResultCount,
                        sorting: vm.requestParams.sorting,
                        userId: vm.requestParams.userId,
                        startHour: $scope.settings.time.fromHour,
                        endHour: $scope.settings.time.toHour,
                        startMinute: $scope.settings.time.fromMinute,
                        endMinute: $scope.settings.time.toMinute,
                        dynamicFilter: angular.toJson($scope.builder.builder.getRules()),
                    };
                };
                vm.exportToExcel = function (obj) {
                    abp.ui.setBusy("#MyLoginForm");
                    syncDashboardService.getAllToExcel(vm.getCurrentTicketInput())
                        .success(function (result) {
                            if (result != null) {
                                app.downloadTempFile(result);
                            }
                        }).finally(function () {
                            abp.ui.clearBusy("#MyLoginForm");
                            vm.disableExport = false;
                        });
                };

                vm.clear = function () {
                    vm.dateRangeModel = {
                        startDate: todayAsString,
                        endDate: todayAsString
                    };
                    $scope.builder.builder.reset();
                    vm.getAll();
                }

                vm.getTotal = function () {
                    syncDashboardService.getTotalTicket(vm.getCurrentTicketInput()).success(function (result) {
                        vm.totalTicketAmount = result.totalAmount;
                        vm.totalTickets = result.totalTicket;
                        vm.totalSynced = result.syncedTicket;
                    }).finally(function (result) {
                    });
                };

                vm.getReprocess = function (postdata) {
                    $modal.open({
                        templateUrl: "~/App/tenant/views/syncdashboard/reprocessModal.cshtml",
                        controller: "tenant.views.syncdashboard.reprocessModal as vm",
                        backdrop: "static",
                        resolve: {
                            input: function () {
                                return postdata;
                            }
                        }
                    });

                };

                vm.process = function () {
                    abp.ui.setBusy();
                    vm.requestParams.idReProcess = 0;
                    vm.requestParams.isReProcessed = false;
                    syncDashboardService.process(vm.getCurrentTicketInput())
                        .success(function (result) {
                            vm.getAll();
                            vm.getTotal();

                        }).finally(function () {
                            abp.ui.clearBusy();
                        });
                }
                // #endregion

                vm.getAll = function () {
                    vm.loading = true;
                    syncDashboardService.getAll(vm.getCurrentTicketInput())
                        .success(function (result) {
                            vm.getTotal();
                            console.log("data", result);
                            vm.gridOptions.totalItems = result.items.totalCount;
                            vm.gridOptions.data = result.items;
                        }).finally(function () {
                            vm.loading = false;
                        });
                };
                vm.locations = [];
                vm.getLocations = function () {
                    commonLookup.getLocationForCombobox({
                    }).success(function (result) {
                        vm.locations = result.items;
                        console.log("loc", vm.locations);
                    }).finally(function (result) {
                    });
                };
                $scope.builder =
                    app.createBuilder(
                        {
                            condition: "AND",
                            rules: []
                        },
                        angular.fromJson
                            (
                                [
                                    {
                                        "id": "Location.Name",
                                        "label": app.localize("Location"),
                                        "type": "string",
                                        "operators": [
                                            "equal",
                                            "not_equal",
                                        ],
                                        plugin: 'selectize',
                                        plugin_config: {
                                            valueField: 'value',
                                            labelField: 'displayText',
                                            searchField: 'displayText',
                                            sortField: 'displayText',
                                            create: true,
                                            maxItems: 1,
                                            plugins: ['remove_button'],
                                            onInitialize: function () {
                                                var that = this;
                                                if (vm.locations.length > 0) {
                                                    vm.locations.forEach(function (item) {
                                                        item.value = item.displayText;
                                                        that.addOption(item);
                                                    });
                                                } else {
                                                    commonLookup.getLocationForCombobox({
                                                    }).success(function (result) {
                                                        vm.locations = result.items;
                                                        vm.locations.forEach(function (item) {
                                                            item.value = item.displayText;
                                                            that.addOption(item);
                                                        });
                                                    }).finally(function (result) {
                                                    });
                                                }
                                            }
                                        },
                                        valueSetter: function (rule, value) {
                                            rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                                        }
                                    },
                                    {
                                        "id": "TicketItem.Ticket.TicketNumber",
                                        "label": app.localize("Ticket") + app.localize("No"),
                                        "type": "string"
                                    },
                                    {
                                        "id": "TicketItem.Ticket.TotalAmount",
                                        "label": app.localize("Amount"),
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "type": "integer"
                                    },
                                    {
                                        "id": "TicketItem.Ticket.TicketCreatedTime",
                                        "label": app.localize("Date"),
                                        "type": "datetime",
                                        "operators": [
                                            "equal",
                                            "less",
                                            "less_or_equal",
                                            "greater",
                                            "greater_or_equal"
                                        ],
                                        "placeholder": 'yyyy-MM-dd',
                                    }
                                    ,
                                    {
                                        "id": "Operations",
                                        "label": app.localize("Operations"),
                                        "type": "string"
                                    }
                                ]
                            )
                    );
            }
        ]);
})();