﻿(function () {
    appModule.controller('tenant.views.syncdashboard.ticketModal',
        [
            '$scope'
            , '$uibModalInstance'
            , 'postId'
            , "abp.services.app.post",
            function ($scope
                , $modalInstance
                , postId
                , postService) {
                var vm = this;
                vm.credit = false;
                vm.saving = false;
                vm.contents = null;
                vm.close = function () {
                    $modalInstance.dismiss();
                };
                vm.refreshPage = function () {
                    $window.location.reload();
                };
                vm.init = function () {
                    abp.ui.setBusy();
                    postService.pullContent(postId)
                    .success(function (result) {
                        console.log("data-detail", result);
                        vm.data = result;
                        vm.ticket = result.ticketItem.ticket;
                        vm.error = result.preErrorValues;
                        console.log("data-errors", vm.error );
                    }).finally(function () {
                        abp.ui.clearBusy();
                    });
                };
                vm.save = function () {
                    vm.saving = true;
                    postService.updateContents({
                        id: vm.data.id,
                        contents: vm.data.contents
                    }).success(function () {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        $modalInstance.dismiss();
                        vm.refreshPage();
                    }).finally(function () {
                        vm.saving = false;
                    });
                };
                vm.init();
            }
        ]);
})();