﻿(function () {
    appModule.controller('tenant.views.syncdashboard.reprocessModal',
        [
            '$scope'
            , '$uibModalInstance'
            ,'input'
            , "abp.services.app.syncDashboard",
            function ($scope
                , $modalInstance
                ,input
                , syncDashboardService) {
                var vm = this;
                vm.credit = false;
                vm.contents = null;
                vm.totalFail = "";
                vm.totalPass ="";
                vm.totalTran = "";
                vm.close = function () {
                    $modalInstance.dismiss();
                };
                vm.refreshPage = function () {
                    $window.location.reload();
                };
                vm.init = function () {
                    abp.ui.setBusy();
                    syncDashboardService.getReProcess(input).success(function (result) {
                        vm.totalFail = result.failed;
                        vm.totalPass = result.passed;
                        vm.totalTran = result.totalTransaction;
                    }).finally(function () {
                        abp.ui.clearBusy();
                    });                   
                };
                vm.init();
            }
        ]);
})();