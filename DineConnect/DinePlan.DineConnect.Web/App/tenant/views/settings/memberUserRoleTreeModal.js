﻿(function () {
    appModule.controller('tenant.views.settings.memberUserRoleTreeModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.role', 'setting',
        function ($scope, $uibModalInstance, roleService, setting) {
            var vm = this;
            vm.setting = setting;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };


            vm.userRole = {

                $tree: null,
                selectedOu: {
                    id: null,
                    code: null,
                    name: null,
                    set: function (ouInuserRole) {
                        if (!ouInuserRole) {
                            vm.userRole.selectedOu.id = null;
                            vm.userRole.selectedOu.code = null;
                            vm.userRole.selectedOu.name = null;
                        } else {
                            vm.userRole.selectedOu.id = ouInuserRole.id;
                            vm.userRole.selectedOu.code = ouInuserRole.original.code;
                            vm.userRole.selectedOu.name = ouInuserRole.original.name;
                        }

                    }
                },



                getTreeDataFromServer: function (callback) {
                    roleService.getRoles({}).then(function (result) {
                        var treeData = _.map(result.data.items, function (item) {
                            return {
                                id: item.id,
                                parent: item.parentId ? item.parentId : '#',
                                code: item.displayName,
                                name: item.name,
                                text: item.name,
                                state: {
                                    opened: true,
                                    selected: item.id == vm.setting.engage.memberUserRoleID ? true : false
                                }
                            };
                        });

                        callback(treeData);
                    });
                },

                init: function () {
                    vm.userRole.getTreeDataFromServer(function (treeData) {
                        vm.userRole.$tree = $('#userRoleEditTree');

                        vm.userRole.$tree
                            .on('changed.jstree', function (e, data) {
                                if (data.selected.length != 1) {
                                    vm.userRole.selectedOu.set(null);
                                } else {
                                    var selectedNode = data.instance.get_node(data.selected[0]);
                                    vm.userRole.selectedOu.set(selectedNode);
                                }
                            })
                            .jstree({
                                'core': {
                                    data: treeData,
                                    multiple: false,
                                },
                                "types": {
                                    "default": {
                                        "icon": "fa fa-folder tree-item-icon-color icon-lg"
                                    },
                                    "file": {
                                        "icon": "fa fa-file tree-item-icon-color icon-lg"
                                    }
                                },
                                'checkbox': {
                                    keep_selected_style: false,
                                    three_state: false,
                                    cascade: ''
                                },
                                plugins: ['checkbox', 'types']
                            });
                    });
                },

                reload: function () {
                    vm.userRole.getTreeDataFromServer(function (treeData) {
                        vm.userRole.$tree.jstree(true).settings.core.data = treeData;
                        vm.userRole.$tree.jstree('refresh');
                    });
                }
            };

            vm.userRole.init();

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
            vm.save = function () {
                $uibModalInstance.close(vm.userRole);
            }

        }]);
})();