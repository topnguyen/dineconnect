﻿(function () {
    appModule.controller("tenant.views.settings.index", [
        "$scope", "$http", "$uibModal", "abp.services.app.tenantSettings", "abp.services.app.ticket", "abp.services.app.setup", 
        "abp.services.app.connectLookup", 'abp.services.app.material', 'abp.services.app.urbanPiperIntegrate', 'abp.services.app.seed', 'abp.services.app.deliveryHero', 'abp.services.app.externalRun', 'abp.services.app.user',
        function ($scope, $http, $modal, tenantSettingsService, ticketService, setupService, 
            lookService, materialService, urbanPiperIntegrateService, seedService, deliveryHeroAppService, externalService, userService) {
            var vm = this;

            /* eslint-disable */

            vm.passwordPolicy = null;
            vm.getPasswordPolicyRule = function () {
                vm.passwordPolicy = null;
                userService.getPasswordPolicyForSetting({
                    passwordPolicy : vm.settings.passwordPolicy
                }).success(function (result) {
                    vm.passwordPolicy = result;
                });
            }

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });

            vm.refaveragepricetags = [];
            function fillDropDownAveragePriceTagType() {
                materialService.getMaterialAveragePriceTagForCombobox({}).success(function (result) {
                    vm.refaveragepricetags = result.items;
                });
            }
            fillDropDownAveragePriceTagType();

            vm.isMultiTenancyEnabled = abp.multiTenancy.isEnabled;
            vm.isEngageEnabled = abp.features.isEnabled("DinePlan.DineConnect.Engage");
            vm.isHouseEnabled = abp.features.isEnabled("DinePlan.DineConnect.House");
            vm.isConnectEnabled = abp.features.isEnabled("DinePlan.DineConnect.Connect");
            vm.isCompactEnabled = abp.features.isEnabled("DinePlan.DineConnect.Touch");
            vm.isSwipeEnabled = abp.features.isEnabled("DinePlan.DineConnect.Swipe");
            vm.isTickEnabled = abp.features.isEnabled("DinePlan.DineConnect.Tick");
            vm.urbanPiperEnabled = abp.features.isEnabled("DinePlan.DineConnect.Connect.UrbanPiper");
            vm.deliveryHeroEnabled = abp.features.isEnabled("DinePlan.DineConnect.Connect.DeliveryHero");
            vm.mycustomer = abp.features.getValue("DinePlan.DineConnect.Connect.Customer");

            vm.currentLanguage = abp.localization.currentLanguage;
          

            vm.loading = false;
            vm.settings = null;

            vm.getSettings = function () {
                vm.loading = true;
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        if (result.connect.schedules !== "") {
                            vm.schedules = angular.fromJson(result.connect.schedules);
                        }

                        if (vm.schedules == null) {
                            vm.schedules = [];
                        }
                        if (vm.schedules === "") {
                            vm.schedules = [];
                        }

                        if (result.connect.weekDay !== "") {
                            vm.refweekday = angular.fromJson(result.connect.weekDay);
                        }
                        if (vm.refweekday == null) {
                            vm.refweekday = [];
                        }
                        if (vm.refweekday === "") {
                            vm.refweekday = [];
                        }

                        if (result.connect.weekEnd !== "") {
                            vm.refweekend = angular.fromJson(result.connect.weekEnd);
                        }

                        if (vm.refweekend == null) {
                            vm.refweekend = [];
                        }
                        if (vm.refweekend === "") {
                            vm.refweekend = [];
                        }
                        if (result.connect.lastSyncStatuses !== "") {
                            vm.lastSyncStatuses = angular.fromJson(result.connect.lastSyncStatuses);
                        }

                        if (vm.lastSyncStatuses == null) {
                            vm.lastSyncStatuses = [];
                        }
                        if (vm.lastSyncStatuses === "") {
                            vm.lastSyncStatuses = [];
                        }
                        vm.settings = result;
                        vm.getPasswordPolicyRule();
                    }).finally(function () {
                        vm.loading = false;
                    });
            };

            vm.syncAll = function () {
                vm.loading = true;
                abp.ui.setBusy("#MyLoginForm");
                setupService.syncConnect(
                    vm.settings.general
                ).success(function () {
                    abp.notify.info(app.localize("Done"));
                    abp.message.info(app.localize("Done"));
                    abp.ui.clearBusy("#MyLoginForm");
                }).finally(function () {
                    vm.loading = false;
                    abp.ui.clearBusy("#MyLoginForm");
                });
            };
            vm.executeApp = function(path) {
                vm.loading = true;
                abp.ui.setBusy("#MyLoginForm");
                externalService.runApp(path
                ).success(function () {
                    abp.message.info(app.localize("Done"));
                    abp.ui.clearBusy("#MyLoginForm");
                }).finally(function () {
                    vm.loading = false;
                    abp.ui.clearBusy("#MyLoginForm");
                });
                
            }
            vm.seedData = function () {
                vm.loading = true;
                abp.ui.setBusy("#MyLoginForm");
                seedService.seedSampleData(
                ).success(function () {
                    abp.notify.info(app.localize("Done"));
                    abp.message.info(app.localize("Done"));
                    abp.ui.clearBusy("#MyLoginForm");
                }).finally(function () {
                    vm.loading = false;
                    abp.ui.clearBusy("#MyLoginForm");
                });
            };

            vm.saveAll = function () {
                console.log(vm.settings);

                if (vm.settings !== null && vm.settings.house !== null && vm.settings.house.defaultAveragePriceTagRefId != null && vm.settings.house.defaultAveragePriceTagRefId == 2 && (vm.settings.house.noOfMonthAvgTaken == 0 || vm.settings.house.noOfMonthAvgTaken == '')) {
                    abp.notify.error(app.localize('Number') + ' ' + app.localize('Of') + ' ' + app.localize('Months') + " ?");
                    vm.saving = false;
                    vm.loading = false;
                    return;
                }

                vm.settings.connect.lastSyncStatuses = angular.toJson(vm.lastSyncStatuses);
                vm.settings.connect.schedules = angular.toJson(vm.schedules);
                vm.settings.connect.weekDay = angular.toJson(vm.refweekday);
                vm.settings.connect.weekEnd = angular.toJson(vm.refweekend);

                tenantSettingsService.updateAllSettings(
                    vm.settings
                ).success(function () {
                    abp.notify.info(app.localize("SavedSuccessfully"));
                    location.href = abp.appPath + "Localization/ChangeCulture?cultureName=" + vm.currentLanguage.name + "&returnUrl=" + window.location.href;
                });
            };

            vm.updateMemberAndPoints = function () {
                abp.ui.setBusy("#MyLoginForm");
                ticketService.updateMembersAndPoints(
                ).success(function () {
                    abp.notify.info(app.localize("Done"));
                    abp.ui.clearBusy("#MyLoginForm");
                });
            };
            vm.getSettings();

            vm.scname = "";
            vm.schedules = [];
            $scope.settings = {
                dropdownToggleState: false,
                time: {
                    fromHour: "06",
                    fromMinute: "30",
                    toHour: "23",
                    toMinute: "30"
                },
                noRange: false,
                format: 24,
                noValidation: true
            };

            vm.clearSales = function () {
                vm.confirmPassword = {
                    confirmed: false,
                    confirmDesc: "Once done, it can not be reverted"
                }

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/password/password.cshtml",
                    controller: "tenant.views.common.password as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        password: function () {
                            return vm.confirmPassword;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    console.log(result);
                    if (result.confirmed) {
                        abp.ui.setBusy("#MyLoginForm");
                        setupService.clearSales(
                        ).success(function () {
                            abp.notify.info(app.localize("Done"));
                            abp.ui.clearBusy("#MyLoginForm");
                        });
                    } else {
                        abp.notify.error(app.localize("WrongPassword"));
                    }
                });
            }

            vm.clearHouseTransaction = function () {
                vm.confirmPassword = {
                    confirmed: false,
                    confirmDesc: "Once done, it can not be reverted"
                }

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/password/password.cshtml",
                    controller: "tenant.views.common.password as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        password: function () {
                            return vm.confirmPassword;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    console.log(result);
                    if (result.confirmed) {
                        abp.ui.setBusy("#MyLoginForm");
                        setupService.clearInventoryTransaction(
                        ).success(function () {
                            abp.notify.info(app.localize("Done"));
                            abp.ui.clearBusy("#MyLoginForm");
                        });
                    } else {
                        abp.notify.error(app.localize("WrongPassword"));
                    }
                });
            }

            vm.clearMenu = function () {
                vm.confirmPassword = {
                    confirmed: false,
                    confirmDesc: "Once done, it can not be reverted"
                }

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/password/password.cshtml",
                    controller: "tenant.views.common.password as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        password: function () {
                            return vm.confirmPassword;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    console.log(result);
                    if (result.confirmed) {
                        abp.ui.setBusy("#MyLoginForm");
                        setupService.clearMenu(
                        ).success(function () {
                            abp.notify.info(app.localize("Done"));
                            abp.ui.clearBusy("#MyLoginForm");
                        });
                    } else {
                        abp.notify.error(app.localize("WrongPassword"));
                    }
                });
            }

            vm.tenantOrderUpdates = function () {
                vm.confirmPassword = {
                    confirmed: false,
                    confirmDesc: "Once done, it can not be reverted"
                }

                var modalInstance = $modal.open({
                    templateUrl: "~/App/common/views/password/password.cshtml",
                    controller: "tenant.views.common.password as vm",
                    backdrop: "static",
                    keyboard: false,
                    resolve: {
                        password: function () {
                            return vm.confirmPassword;
                        }
                    }
                });
                modalInstance.result.then(function (result) {
                    console.log(result);
                    if (result.confirmed) {
                        abp.ui.setBusy("#MyLoginForm");
                        setupService.updateMenuAndOrders(
                        ).success(function () {
                            abp.notify.info(app.localize("Done"));
                            abp.ui.clearBusy("#MyLoginForm");
                        });
                    } else {
                        abp.notify.error(app.localize("WrongPassword"));
                    }
                });
            }

            vm.addSchedule = function () {
                if (!vm.scname || 0 === vm.scname.length) {
                    return;
                }
                if ($scope.settings.time.fromHour > $scope.settings.time.toHour
                    || ($scope.settings.time.fromHour === $scope.settings.time.toHour && $scope.settings.time.fromMinute >= $scope.settings.time.toMinute)) {
                    abp.notify.error(app.localize("WrongDateRange"));
                    return;
                }

                vm.schedules.push({
                    'startHour': $scope.settings.time.fromHour,
                    'endHour': $scope.settings.time.toHour,
                    'startMinute': $scope.settings.time.fromMinute,
                    'endMinute': $scope.settings.time.toMinute,
                    'name': vm.scname
                });
                vm.scname = "";
            };
            vm.removeSchedule = function (productIndex) {
                vm.schedules.splice(productIndex, 1);
            };
            vm.refweekday = [];
            vm.refweekend = [];

            vm.refdays = [];

            function fillDays() {
                vm.loading = true;
                lookService.getDays({}).success(function (result) {
                    vm.refdays = result.items;
                }).finally(function () {
                });
            }

            fillDays();

            $scope.lastSyncSettings = {
                dropdownToggleState: false,
                time: {
                    fromHour: "00",
                    fromMinute: "05",
                    toHour: "00",
                    toMinute: "59"
                },
                noRange: false,
                format: 24,
                noValidation: false
            };

            vm.status = "";
            vm.lastSyncStatuses = [];
            vm.addStatus = function () {
                if (!vm.status || 0 === vm.status.length) {
                    return;
                }

                vm.lastSyncStatuses.push({
                    'startHour': $scope.lastSyncSettings.time.fromHour,
                    'endHour': $scope.lastSyncSettings.time.toHour,
                    'startMinute': $scope.lastSyncSettings.time.fromMinute,
                    'endMinute': $scope.lastSyncSettings.time.toMinute,
                    'status': vm.status
                });
                vm.status = "";
            };
            vm.removeStatus = function (index) {
                vm.lastSyncStatuses.splice(index, 1);
            };

            vm.integrateMenuItems = function () {
                vm.loading = true;

                urbanPiperIntegrateService.pushMenuItemsBackground({
                    userName: vm.settings.connect.urbanPiperUserName,
                    apiKey: vm.settings.connect.urbanPiperApiKey,
                    flushAll: true
                }).success(function () {
                    abp.notify.info(app.localize("Done"));
                    //abp.ui.clearBusy("#MyLoginForm");
                }).finally(function () {
                    vm.loading = false;
                });
            };


            vm.integratePushLocations = function () {
                vm.loading = true;
                console.log("Push");
                urbanPiperIntegrateService.pushStores().success(function () {
                    abp.notify.info(app.localize("Done"));
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.integrateDeliveryHero = function () {
                vm.loading = true;
                deliveryHeroAppService.push({
                    location: 'D101',
                    flushAll: true
                }).success(function () {
                    abp.notify.info(app.localize("Done"));
                    //abp.ui.clearBusy("#MyLoginForm");
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.openMemberUserRole = function () {
                openCreateMemberUserRole(null);
            };

            function openCreateMemberUserRole() {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/settings/memberUserRoleTreeModal.cshtml',
                    controller: 'tenant.views.settings.memberUserRoleTreeModal as vm',
                    backdrop: 'static',
                    resolve: {
                        setting: function () {
                            return vm.settings;
                        }
                    }
                });
                modalInstance.result
                    .then(function (result) {
                        vm.settings.engage.memberUserRoleID = result.selectedOu.id;
                        vm.settings.engage.memberUserRoleName = result.selectedOu.code;
                    }).finally(function () {
                    });
            }
        }
    ]);
})();