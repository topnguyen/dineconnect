﻿(function () {
    appModule.controller("tenant.views.play.layout.detail",
        [
            "$scope",
            "$sce",
            "$state",
            "$stateParams",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.layout",
            "abp.services.app.resolution",
            function ($scope, $sce, $state, $stateParams, $modal, uiGridConstants, layoutService, resolutionService) {

                var vm = this;

                vm.loading = false;
                vm.layoutId = $stateParams.id;

                if (vm.layoutId && vm.layoutId > 0) {
                    vm.workMode = 'EDIT';
                } else {
                    vm.workMode = 'ADD';
                }

                var requestParams = {
                    skipCount: 0,
                    maxResultCount: app.consts.grid.defaultPageSize,
                    sorting: 'id'
                };

                vm.acceptChange = null;
                vm.currentUserId = abp.session.userId;
                vm.isDeleted = false;
                vm.filterText = null;


                $scope.$on("$viewContentLoaded", function () {
                    App.initAjax();
                });

                let goBacktoList = function () {
                    $state.go('tenant.dineplaylayouts', {});
                };

                vm.cancel = goBacktoList;

                vm.templates = moduleData.templates($sce);
                vm.selectedTemplate = null;

                vm.createOrUpdateLayout = function () {
                    vm.layout.id = vm.layoutId;
                    vm.layout.templateId = vm.selectedTemplate.value;
                    vm.layout.resolutionId = vm.layout.resolution.id;
                    vm.loading = true;
                    layoutService.createOrUpdateLayout({ layout: vm.layout })
                        .success(function (result) {
                            goBacktoList();
                        }).finally(function () {
                            vm.loading = false;
                        });
                };

                vm.layout = moduleData.getEmptyLayoutObject();

                vm.resolutions = resolutions;

                let getResolutions = function () {
                    resolutionService.getAll({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterText,
                        location: vm.location,
                        acceptChange: vm.acceptChange,
                        isDeleted: vm.isDeleted
                    }).success(function (result) {
                        vm.resolutions = result.items;
                        if (vm.layoutId && vm.layoutId > 0) {
                            vm.getLayoutForEdit();
                        }
                    }).finally(function () {
                        vm.loading = false;
                    });
                };


                vm.getLayoutForEdit = function () {
                    vm.loading = true;
                    layoutService.getLayoutForEdit({
                        id: vm.layoutId
                    })
                        .success(function (result) {
                            populateLayoutDataToControls(result);
                        }).finally(function () {
                            vm.loading = false;
                        });
                };

                let getTemplateById = function (id) {
                    for (let i = 0; i < vm.templates.length; i++) {
                        if (vm.templates[i].value == id) {
                            vm.selectedTemplate = vm.templates[i];
                            break;
                        }
                    }
                }

                let getResolutionById = function (id) {
                    for (let i = 0; i < vm.resolutions.length; i++) {
                        if (vm.resolutions[i].id == id) {
                            vm.layout.resolution = vm.resolutions[i];
                            vm.resolutionChanged();
                            break;
                        }
                    }
                }

                let populateLayoutDataToControls = function (data) {
                    vm.layout.id = data.layout.id;
                    vm.layout.name = data.layout.name;
                    vm.layout.resolutionId = data.layout.resolutionId;
                    vm.layout.description = data.layout.description;
                    vm.layout.templateId = data.layout.templateId;
                    getTemplateById(data.layout.templateId);
                    getResolutionById(data.layout.resolutionId);

                    vm.templateChanged();
                };

                getResolutions();
                vm.resolutionChanged = function () {
                    vm.templateSelection.check();

                    if (!vm.templateSelection.selected) return;

                    let width = 200 / vm.layout.resolution.height * vm.layout.resolution.width;
                    $('.template-container').css('width', width + 'px');

                    let regionResos = JSON.parse(vm.selectedTemplate.regionResolutions);
                    for (var i = 0; i < regionResos.length; i++) {
                        let w = Math.ceil(vm.layout.resolution.width * regionResos[i][0]);
                        let h = Math.ceil(vm.layout.resolution.height * regionResos[i][1]);
                        $(`#t${vm.selectedTemplate.value}r${i + 1}`).html(`${w} x ${h}`)
                    }
                }

                vm.templateChanged = function () {
                    vm.templateSelection.check();
                    setTimeout(function () {
                        vm.resolutionChanged();
                    }, 1000);
                }

                vm.canSave = function () {
                    if (!vm.layout.name || vm.layout.name === '') return false;
                    if (!vm.layout.resolution) return false;
                    if (!vm.selectedTemplate) return false;
                    return true;
                };

                vm.getLayoutTemplates = function () {
                    layoutService.getLayoutTemplates()
                        .success(
                            function (result) {
                                templateHtmls = result;
                                //console.log(result);
                            }
                        );
                };

                vm.getLayoutTemplates();

                vm.templateSelection = {
                    selected: false,
                    message: '',
                    check: function () {
                        this.message = '';
                        this.selected = true;
                        if (!vm.selectedTemplate) {
                            this.selected = false;
                            this.message = 'Please select template.';
                        } else {
                            if (!vm.layout.resolution.id) {
                                this.selected = false;
                                this.message = 'Please select resolution.';
                            }
                        }
                    }
                };

                vm.templateSelection.check();

            }
        ]);

    let templateHtmls = [
        `
            <div class="row region-height-100">
                <div class="col-xs-12 region-height-100">
                    <div class="template-region region-height-100">
                        <<t1r1>>
                    </div>
                </div>
            </div>|[[1,1]]
        `,
        `
            <div class="row region-height-50">
                <div class="col-xs-12 region-height-100">
                    <div class="template-region region-height-100">
                        <<t2r1>>
                    </div>
                </div>
            </div>
            <div class="row region-height-50">
                <div class="col-xs-12 region-height-100">
                    <div class="template-region region-height-100">
                        <<t2r2>>
                    </div>
                </div>
            </div>|[[1,0.5],[1,0.5]]
        `,
        `
            <div class="row region-height-100">
                <div class="col-xs-6 region-height-100 region-padding-0">
                    <div class="template-region region-height-100">
                        <<t3r1>>
                    </div>
                </div>
                <div class="col-xs-6 region-height-100 region-padding-0">
                    <div class="template-region region-height-100">
                        <<t3r2>>
                    </div>
                </div>
            </div>|[[0.5,1],[0.5,1]]
        `,
        `
            <div class="row region-height-33">
                <div class="col-xs-12 region-height-100 region-padding-0">
                    <div class="template-region region-height-100">
                        <<t4r1>>
                    </div>
                </div>
            </div>
            <div class="row region-height-33">
                <div class="col-xs-12 region-height-100 region-padding-0">
                    <div class="template-region region-height-100">
                        <<t4r2>>
                    </div>
                </div>
            </div>
            <div class="row region-height-33">
                <div class="col-xs-12 region-height-100 region-padding-0">
                    <div class="template-region region-height-100">
                        <<t4r3>>
                    </div>
                </div>
            </div>|[[1,0.3333333333333333],[1,0.3333333333333333],[1,0.3333333333333333]]
        `,
        `
            <div class="row region-height-50">
                <div class="col-xs-6 region-height-100 region-padding-0">
                    <div class="template-region region-height-100">
                        <<t5r1>>
                    </div>
                </div>
                <div class="col-xs-6 region-height-100 region-padding-0">
                    <div class="template-region region-height-100">
                        <<t5r2>>
                    </div>
                </div>
            </div>
            <div class="row region-height-50">
                <div class="col-xs-12 region-height-100 region-padding-0">
                    <div class="template-region region-height-100">
                        <<t5r3>>
                    </div>
                </div>
            </div>|[[0.5,0.5],[0.5,0.5],[1,0.5]]
        `,
        `
            <div class="row region-height-50">
                <div class="col-xs-12 region-height-100 region-padding-0">
                    <div class="template-region region-height-100">
                        <<t6r1>>
                    </div>
                </div>
            </div>
            <div class="row region-height-50">
                <div class="col-xs-6 region-height-100 region-padding-0">
                    <div class="template-region region-height-100">
                        <<t6r2>>
                    </div>
                </div>
                <div class="col-xs-6 region-height-100 region-padding-0">
                    <div class="template-region region-height-100">
                        <<t6r3>>
                    </div>
                </div>
            </div>|[[1,0.5],[0.5,0.5],[0.5,0.5]]
        `,
        `
            <div class="row region-height-100">
                <div class="col-xs-4 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t7r1>>
			        </div>
                </div>
                <div class="col-xs-4 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t7r2>>
			        </div>
                </div>
                <div class="col-xs-4 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t7r3>>
			        </div>
                </div>
            </div>|[[0.3333333333333333,1],[0.3333333333333333,1],[0.3333333333333333,1]]
        `,
        `
            <div class="row region-height-100">
                <div class="col-xs-9 region-height-100 region-padding-0">
                    <div class="row region-height-50">
                        <div class="col-xs-12 region-height-100 region-padding-0">
			                <div class="template-region region-height-100">
			                    <<t8r1>>
			                </div>
                        </div>
                    </div>
                    <div class="row region-height-50">
                        <div class="col-xs-12 region-height-100 region-padding-0">
			                <div class="template-region region-height-100">
			                    <<t8r2>>
			                </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-3 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t8r3>>
			        </div>
                </div>
            </div>|[[0.75,0.5],[0.75,0.5],[0.25,1]]
        `,
        `
            <div class="row region-height-100">
                <div class="col-xs-3 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t9r1>>
			        </div>
                </div>
                <div class="col-xs-9 region-height-100 region-padding-0">
                    <div class="row region-height-50">
                        <div class="col-xs-12 region-height-100 region-padding-0">
			                <div class="template-region region-height-100">
			                    <<t9r2>>
			                </div>
                        </div>
                    </div>
                    <div class="row region-height-50">
                        <div class="col-xs-12 region-height-100 region-padding-0">
			                <div class="template-region region-height-100">
			                    <<t9r3>>
			                </div>
                        </div>
                    </div>
                </div>
            </div>|[[0.25,1],[0.75,0.5],[0.75,0.5]]
        `,
        `
            <div class="row region-height-50">
                <div class="col-xs-6 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t10r1>>
			        </div>
                </div>
                <div class="col-xs-6 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t10r2>>
			        </div>
                </div>
            </div>
            <div class="row region-height-50">
                <div class="col-xs-6 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t10r3>>
			        </div>
                </div>
                <div class="col-xs-6 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t10r4>>
			        </div>
                </div>
            </div>|[[0.5,0.5],[0.5,0.5],[0.5,0.5],[0.5,0.5]]
        `,
        `
            <div class="row region-height-25">
                <div class="col-xs-12 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t11r1>>
			        </div>
                </div>
            </div>
            <div class="row region-height-25">
                <div class="col-xs-12 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t11r2>>
			        </div>
                </div>
            </div>
            <div class="row region-height-25">
                <div class="col-xs-12 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t11r3>>
			        </div>
                </div>
            </div>
            <div class="row region-height-25">
                <div class="col-xs-12 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t11r4>>
			        </div>
                </div>
            </div>|[[1,0.25],[1,0.25],[1,0.25],[1,0.25]]
        `,
        `
            <div class="row region-height-100">
                <div class="col-xs-3 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
    			        <<t12r1>>
			        </div>
                </div>
                <div class="col-xs-3 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
    			        <<t12r2>>
			        </div>
                </div>
                <div class="col-xs-3 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
    			        <<t12r3>>
			        </div>
                </div>
                <div class="col-xs-3 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
    			        <<t12r4>>
			        </div>
                </div>
            </div>|[[0.25,1],[0.25,1],[0.25,1],[0.25,1]]
        `,
        `
            <div class="row region-height-33">
                <div class="col-xs-6 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t13r1>>
			        </div>
                </div>
                <div class="col-xs-6 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t13r2>>
			        </div>
                </div>
            </div>   
            <div class="row region-height-33">
                <div class="col-xs-6 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t13r3>>
			        </div>
                </div>
                <div class="col-xs-6 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t13r4>>
			        </div>
                </div>
            </div> 
            <div class="row region-height-33">
                <div class="col-xs-12 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t13r5>>
			        </div>
                </div>
            </div>|[[0.5,0.333333333333333],[0.5,0.333333333333333],[0.5,0.333333333333333],[0.5,0.333333333333333],[1,0.333333333333333]]
        `,
        `
            <div class="row region-height-33">
                <div class="col-xs-6 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t14r1>>
			        </div>
                </div>
                <div class="col-xs-6 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t14r2>>
			        </div>
                </div>
            </div>
            <div class="row region-height-33">
                <div class="col-xs-6 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t14r3>>
			        </div>
                </div>
                <div class="col-xs-6 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t14r4>>
			        </div>
                </div>
            </div>
            <div class="row region-height-33">
                <div class="col-xs-6 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t14r5>>
			        </div>
                </div>
                <div class="col-xs-6 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t14r6>>
			        </div>
                </div>
            </div>|[[0.5,0.333333333333333],[0.5,0.333333333333333],[0.5,0.333333333333333],[0.5,0.333333333333333],[0.5,0.333333333333333],[0.5,0.333333333333333]]
        `,
        `
            <div class="row region-height-33">
                <div class="col-xs-12 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t15r1>>
			        </div>
                </div>
            </div>
            <div class="row region-height-33">
                <div class="col-xs-12 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t15r2>>
			        </div>
                </div>
            </div>
            <div class="row region-height-33">
                <div class="col-xs-4 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t15r3>>
			        </div>
                </div>
                <div class="col-xs-4 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t15r4>>
			        </div>
                </div>
                <div class="col-xs-4 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
			            <<t15r5>>
			        </div>
                </div>
            </div>|[[1,0.333333333333333],[1,0.333333333333333],[0.333333333333333,0.333333333333333],[0.333333333333333,0.333333333333333],[0.333333333333333,0.333333333333333]]
        `,
        `
            <div class="row region-height-33">
                <div class="col-xs-12 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
    			        <<t16r1>>
			        </div>
                </div>
            </div>
            <div class="row region-height-33">
                <div class="col-xs-6 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
    			        <<t16r2>>
			        </div>
                </div>
                <div class="col-xs-6 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
    			        <<t16r3>>
			        </div>
                </div>
            </div>
            <div class="row region-height-33">
                <div class="col-xs-6 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
    			        <<t16r4>>
			        </div>
                </div>
                <div class="col-xs-6 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
    			        <<t16r5>>
			        </div>
                </div>
            </div>|[[1,0.333333333333333],[0.5,0.333333333333333],[0.5,0.333333333333333],[0.5,0.333333333333333],[0.5,0.333333333333333]]
        `,
        `
            <div class="row region-height-33">
                <div class="col-xs-4 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
    			        <<t17r1>>
			        </div>
                </div>
                <div class="col-xs-4 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
    			        <<t17r2>>
			        </div>
                </div>
                <div class="col-xs-4 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
    			        <<t17r3>>
			        </div>
                </div>
            </div>
            <div class="row region-height-33">
                <div class="col-xs-12 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
    			        <<t17r4>>
			        </div>
                </div>
            </div>
            <div class="row region-height-33">
                <div class="col-xs-12 region-height-100 region-padding-0">
			        <div class="template-region region-height-100">
    			        <<t17r5>>
			        </div>
                </div>
            </div>|[[0.333333333333333,0.333333333333333],[0.333333333333333,0.333333333333333],[0.333333333333333,0.333333333333333],[1,0.333333333333333],[1,0.333333333333333]]
        `
    ];

    let resolutions = [
        { id: 1, name: '1366 x 768', width: 1366, height: 768, enabled: true },
        { id: 2, name: '1360 x 768', width: 1360, height: 768, enabled: true },
        { id: 3, name: '1280 x 768', width: 1280, height: 768, enabled: true },
    ];

    let moduleData = {
        templates: function ($sce) {
            let templateCounts = 17;
            let templates = [];

            let replaceTemplate = function (id, template, regionResl) {;
                let replaced = template;
                for (let i = 1; i <= 6; i++) {
                    let replacement = `
                        <table style="height:100%;width:100%;">
                            <tr>
                                <td style="vertical-align:middle;text-align:center;">
                                    ${i}</br>
                                    <span id="t${id}r${i}" style='font-size:12px;'>W x H</span>
                                </td>
                            </tr>
                        </table>
                    `;
                    replaced = replaced.replaceAll(`<<t${id}r${i}>>`, replacement);
                }
                return replaced;
            };

            for (let i = 0; i < templateCounts; i++) {
                let tHtmls = templateHtmls[i].split('|');
                let templateHtml = replaceTemplate(i + 1, tHtmls[0]);
                templates.push({
                    value: i + 1,
                    text: `Template ${i + 1}`,
                    html: $sce.trustAsHtml(templateHtml),
                    regionResolutions: tHtmls[1]
                });
            }
            return templates;
        },
        getEmptyLayoutObject: function () {
            return {
                name: "",
                resolution: {},
                resolutionId: 0,
                description: '',
                templateId: -1
            };
        }
    }

}());
