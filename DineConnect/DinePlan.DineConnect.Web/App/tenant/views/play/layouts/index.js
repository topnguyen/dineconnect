﻿
(function () {
    appModule.controller("tenant.views.play.layouts",
        [
            "$scope",
            "$sce",
            "$state",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.layout",
            function ($scope, $sce, $state, $modal, uiGridConstants, layoutService) {

                var vm = this;

                let func = fnc($scope, $state, $modal, uiGridConstants, layoutService, app, vm, $sce);

                func.actions.init(App);

                vm.renderThumbnail = func.actions.renderThumbnail;

                vm.loading = false;
                vm.acceptChange = null;
                vm.currentUserId = abp.session.userId;
                vm.isDeleted = false;
                vm.filterText = null;

                vm.permissions = func.actions.readPermissions(abp);

                let grdUt = gridUtil($scope, app, vm, uiGridConstants);

                vm.userGridOptions = grdUt.gridOptions;

                vm.getAll = func.actions.getAll;
                vm.getAll();

                vm.createLayout = func.actions.createLayout;
                vm.editLayout = func.actions.editLayout;
                vm.deleteLayout = func.actions.deleteLayout;
                vm.manageContent = func.actions.manageContent;
                vm.preview = func.actions.preview;

                
            }
        ]);

    let gridUtil = function ($scope, app, vm, uiGridConstants) {
        let rowTemplate = `
        <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" 
            class="ui-grid-cell" 
            ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  
            ui-grid-cell>
        </div>
            `;
        let columnDefTemplates = [
            `
        <div class="ui-grid-cell-contents">
            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>
                <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" 
                    aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-cog"></i> ${app.localize("Actions")} <span class="caret"></span>
                </button>
                <ul uib-dropdown-menu>
                    <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.edit"
                        ng-click="grid.appScope.manageContent(row.entity)">${app.localize("Manage Content")}</a></li>
                    <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.edit"
                        ng-click="grid.appScope.preview(row.entity)">${app.localize("Preview")}</a></li>
                    <li><a ng-if="grid.appScope.permissions.edit"
                        ng-click="grid.appScope.editLayout(row.entity)">${app.localize("Edit")}</a></li>
                    <li><a ng-if="grid.appScope.permissions.delete"
                        ng-click="grid.appScope.deleteLayout(row.entity)">${app.localize("Delete")}</a></li>
                </ul>
            </div>
        </div>`
        ];
        return {
            gridOptions: {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: rowTemplate,
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate: columnDefTemplates[0]
                    },
                    {
                        name: app.localize('Name'),
                        width: 250,
                        field: 'name'
                    },
                    {
                        name: app.localize('Description'),
                        field: 'description'
                    },
                    {
                        name: app.localize('Template'),
                        width: 135,
                        cellTemplate: `<div style="width:130px;height:130px;background-color:#5ea7d2;margin:2px;" 
                            id="gridL{{row.entity.id}}T{{row.entity.templateId}}">
                            <div class="template-container"></div></div>
                        `
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' +
                                sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope,
                        function (pageNumber, pageSize) {
                            requestParams.skipCount = (pageNumber - 1) * pageSize;
                            requestParams.maxResultCount = pageSize;

                            vm.getAll();
                        });
                },
                data: []
            }
        };
    };
    let fnc = function ($scope, $state, $modal, uiGridConstants, layoutService, app, vm, $sce) {

        let act = {
            openLayout: function (objId) {
                $state.go("tenant.dineplaylayoutsdetail", {
                    id: objId
                });
            },
            openLayoutContent: function (objId) {
                $state.go("tenant.dineplaylayoutscontent", {
                    id: objId
                });
            },
            openLayoutPreview: function (objId) {
                $state.go("tenant.dineplaylayoutspreview", {
                    id: objId,
                    src: 'L'
                });
            }
        }

        var requestParams = {
            skipCount: 0,
            maxResultCount: app.consts.grid.defaultPageSize,
            sorting: 'id'
        };

        let f = {
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                readPermissions: function (abp) {
                    return {
                        create: abp.auth.hasPermission('Pages.Tenant.Play.Layout.Create'),
                        edit: abp.auth.hasPermission('Pages.Tenant.Play.Layout.Edit'),
                        delete: abp.auth.hasPermission('Pages.Tenant.Play.Layout.Delete')
                    };
                },
                getAll: function () {
                    vm.loading = true;
                    layoutService.getAll({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterText,
                        location: vm.location,
                        acceptChange: vm.acceptChange,
                        isDeleted: vm.isDeleted
                    }).success(function (result) {
                        vm.userGridOptions.totalItems = result.totalCount;
                        vm.userGridOptions.data = result.items;
                        f.actions.showTemplateInGrid();
                    }).finally(function () {
                        vm.loading = false;
                    });
                },
                createLayout: function () {
                    act.openLayout(null);
                },
                editLayout: function (obj) {
                    act.openLayout(obj.id);
                },
                deleteLayout: function (obj) {
                    abp.message.confirm(
                        app.localize('DeleteLayoutWarning', obj.name),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                layoutService.deleteLayout({ id: obj.id })
                                    .success(function(result) {
                                        if (result.success) {
                                            vm.getAll();
                                            // abp.notify.success(app.localize('SuccessfullyDeleted'));
                                        } else {
                                            let scheduledEventName = '';
                                            let counter = 1;
                                            try {
                                                result.usedByNames.forEach(function(n) {
                                                    if (counter > 3) {
                                                        scheduledEventName += (scheduledEventName != '' ? '\n' : '') +
                                                            `and more (${result.usedByNames.length - 3}).`;
                                                        throw {};
                                                    } else {
                                                        scheduledEventName += (scheduledEventName != '' ? '\n' : '') +
                                                            '📅' +
                                                            n;
                                                    }
                                                    counter++;
                                                });
                                            } catch (err) {

                                            }
                                            abp.message.warn(
                                                app.localize('LayoutIsInUse', obj.name) + '\n' + scheduledEventName
                                            );

                                        }
                                    }).finally(function () {
                                        vm.loading = false;
                                    });
                            }
                        }
                    )
                },
                manageContent: function (obj) {
                    act.openLayoutContent(obj.id);
                },
                preview: function (obj) {
                    act.openLayoutPreview(obj.id);
                },
                showTemplateInGrid: function () {
                    setTimeout(function () {
                        for (var i = 0; i < vm.userGridOptions.data.length; i++) {
                            let r = vm.userGridOptions.data[i];
                            let html = r.template.html;
                            for (var j = 1; j <= 6; j++) {
                                html = html.replaceAll(`<<t${r.templateId}r${j}>>`,'')
                            }
                            $(`#gridL${r.id}T${r.templateId}`).html(html);
                        }
                    },1000);

                }
            }
        };

        return f;
    };
}());
