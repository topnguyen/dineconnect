﻿(function () {
    appModule.controller("tenant.views.play.layout.preview",
        [
            "$scope",
            "$sce",
            "$state",
            "$stateParams",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.layout",
            function ($scope, $sce, $state, $stateParams, $modal, uiGridConstants, layoutService) {

                var vm = this;

                vm.loading = false;

                vm.layoutId = $stateParams.id;
                vm.src = $stateParams.src;
                vm.appPath = abp.appPath;
                vm.layout = null;
                vm.resolution = null;
                vm.layoutContentTemplate = null;

                let func = fnc($scope, $state, $modal, uiGridConstants, layoutService,
                    app, vm, $sce, abp);
                func.actions.init(App);

                func.actions.getLayoutContentTemplate();
                func.actions.getLayout();
                vm.layoutWidth = 100;
                vm.layoutHeight = 500;

                vm.play = func.actions.play;
                vm.cancel = func.actions.goBackToList;
            }
        ]);



    let fnc = function ($scope, $state, $modal, uiGridConstants, layoutService, app, vm, $sce, abp) {

        let act = {
            goBackToList: function () {
                if (vm.src === 'C') {
                    $state.go('tenant.dineplaylayoutscontent', {id: vm.layoutId})
                } else {
                    $state.go('tenant.dineplaylayouts', {});
                }
            },
            getUsableTemplate: function (objTemplate) {
                let replaceTemplate = function (id, template) {
                    let fileInputHtml = `
                        <div class="previewPanel" id="prvwR<<R>>" style="width:100%;height:100%;"></div>
                    `;

                    let replaced = template;
                    fileInputHtml = fileInputHtml.replaceAll('<<T>>', id);

                    for (let i = 1; i <= 6; i++) {
                        let replacedFileInputHtml;
                        replacedFileInputHtml = fileInputHtml.replaceAll('<<R>>', i);
                        replaced = replaced.replaceAll(`<<t${id}r${i}>>`, replacedFileInputHtml);
                    }
                    return replaced;
                };

                let templateHtml = replaceTemplate(objTemplate.id, objTemplate.html);
                objTemplate.html = $sce.trustAsHtml(templateHtml);
                return objTemplate;
            },
            getFileUrl: function (file) {
                return abp.appPath + 'FileUpload/DownloadFile?fileName=' +
                    file.fileName + '&url=' + file.systemFileName;
            },
            getLayoutContents: function () {
                layoutService.getContents({ id: vm.layoutId })
                    .success(function (result) {
                        vm.contents = [];
                        for (let i = 0; i < result.length; i++) {
                            let content = {
                                id: result[i].id,
                                regionNo: result[i].regionNo,
                                fileName: result[i].fileName,
                                fileType: result[i].fileType,
                                stretched: result[i].stretched,
                                layoutId: result[i].layoutId,
                                displayLength: result[i].displayLength,
                                systemFileName: result[i].systemFileName,
                                file: null,
                                fileToUpload: null,
                                hasFile: result[i].hasFile,
                                fileUrl: act.getFileUrl({
                                    fileName: result[i].systemFileName,
                                    systemFileName: result[i].systemFileName
                                })
                            };
                            vm.contents.push(content);
                            if (content.fileType.indexOf('video') >= 0) {
                                let videoTag = `
                                    <video id="player${content.regionNo}" 
                                        style="width:100%;height:100%;object-fit:cover;" loop>
                                        <source src="${content.fileUrl}" type="${content.fileType}">
                                    </video>
                                `;
                                $("#prvwR" + result[i].regionNo).html(videoTag);
                            } else {
                                let videoTag = `
                                    <img src="${content.fileUrl}"  style="width:100%;height:100%;object-fit:cover;"/>
                                `;
                                $("#prvwR" + result[i].regionNo).html(videoTag);
                            }
                            
                        }
                    })
                    .finally(function () { });
            },
            setPreviewWidth: function () {
                vm.layoutWidth =
                    vm.layoutHeight /
                    vm.layout.resolution.height *
                    vm.layout.resolution.width;
                $('.template-container').css('width', vm.layoutWidth + 'px');
            }
        }

        let f = {
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                goBackToList: function () {
                    act.goBackToList()
                },
                initLayoutPreview: function () {

                },
                getLayoutContentTemplate: function () {
                    layoutService.getLayoutContentTemplate(vm.layoutId)
                        .success(function (result) {
                            vm.layoutContentTemplate = act.getUsableTemplate(result);
                            vm.contents = act.getLayoutContents();
                        })
                        .finally(function () {
                            vm.loading = false;
                        });
                },
                play: function () {
                    for (var i = 0; i < vm.contents.length; i++) {
                        if (vm.contents[i].fileType.indexOf('video') >= 0) {
                            document.getElementById('player' + (i + 1)).play();
                        }
                    }
                },
                getLayout: function () {
                    vm.loading = true;
                    layoutService.getLayout({ id: vm.layoutId })
                        .success(function (result) {
                            vm.layout = result;
                            act.setPreviewWidth();
                        })
                        .finally(function () {
                            vm.loading = false;
                        });
                }
            }
        };

        return f;
    };
}());
