﻿(function () {

    appModule.controller("tenant.views.play.layout.selector",
        [
            "$scope",
            "$state",
            "$uibModal",
            "$uibModalInstance",
            "uiGridConstants",
            "abp.services.app.layout",
            "multiSelect",
            function ($scope, $state, $modal, $modalInstance, uiGridConstants, layoutService, multiSelect) {

                var vm = this;

                vm.loading = false;
                vm.isDeleted = false;
                vm.filterText = '';

                let func = fnc($scope, $state, $modal, $modalInstance, uiGridConstants, layoutService, app, vm);
                func.actions.init(App);

                vm.select = func.actions.modalOk
                vm.cancel = func.actions.modalClose;

                vm.multiSelect = (multiSelect) ? multiSelect : false;
                vm.userGridOptions = gridUtil($scope, app, vm, uiGridConstants).gridOptions;
                vm.lists = func.lists;
                vm.getAll = func.actions.getLayouts;

                vm.getAll();
            }
        ]);


    function ObjectList() {
        return {
            add: function (myObj) {
                if (myObj != null) {
                    var myIndex = -1;

                    if (this.items != null) {
                        this.items.some(function (value, key) {
                            if (value.id == myObj.id) {
                                myIndex = value;
                                return true;
                            }
                        });
                    }

                    if (myIndex === -1) {
                        this.items.push(myObj);
                    } else {
                        //if (vm.autoSelection == false)
                        //    abp.notify.error(myObj.name + "  " + app.localize("Available"));
                    }
                }
            },
            remove: function (id) {
                for (let i = 0; i < this.items.length; i++) {
                    const obj = this.items[i];

                    if (obj.id == id) {
                        this.items.splice(i, 1);
                    }
                }
            },
            items: []
        }
    }

    let gridUtil = function ($scope, app, vm, uiGridConstants) {
        let rowTemplate = `
        <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" 
            class="ui-grid-cell" 
            ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  
            ui-grid-cell>
        </div>
            `;
        let columnDefTemplates = [];
        return {
            gridOptions: {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: rowTemplate,
                multiSelect: vm.multiSelect,
                columnDefs: [
                    {
                        name: app.localize('Code'),
                        field: 'id'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' +
                                sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope,
                        function (pageNumber, pageSize) {
                            requestParams.skipCount = (pageNumber - 1) * pageSize;
                            requestParams.maxResultCount = pageSize;

                            vm.getAll();
                        });

                    gridApi.selection.on.rowSelectionChanged($scope,
                        function (row) {
                            if (row.isSelected) {
                                vm.lists.layoutList.add(row.entity);
                            } else {
                                vm.lists.layoutList.remove(row.entity.id);
                            }
                        });

                },
                data: []
            }
        };
    };

    let fnc = function ($scope, $state, $modal, $modalInstance, uiGridConstants, layoutService, app, vm) {

        let act = {
            xxx: function () {

            }
        }


        var requestParams = {
            skipCount: 0,
            maxResultCount: app.consts.grid.defaultPageSize,
            sorting: 'id'
        };

        let f = {
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                modalClose: function () {
                    $modalInstance.dismiss('cancel');
                },
                modalOk: function () {
                    $modalInstance.close(vm.lists.layoutList.items);
                },
                getLayouts: function () {
                    vm.loading = true;
                    layoutService.getAll({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterText,
                        isDeleted: vm.isDeleted
                    }).success(function (result) {
                        vm.userGridOptions.totalItems = result.totalCount;
                        vm.userGridOptions.data = result.items;
                    }).finally(function () {
                        vm.loading = false;
                    })
                }
            },
            lists: {
                layoutList: new ObjectList()
            }
        };

        return f;
    };


}());
