﻿(function () {
    appModule.controller("tenant.views.play.layout.content",
        [
            "$scope",
            "$sce",
            "$state",
            "$stateParams",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.layout",
            "FileUploader",
            function ($scope, $sce, $state, $stateParams, $modal,
                uiGridConstants, layoutService, fileUploader) {

                var vm = this;

                let func = fnc($scope, $state, $modal, uiGridConstants,
                    layoutService, app, vm, abp, $sce, fileUploader);
                func.actions.init(App);

                vm.layoutId = $stateParams.id;

                vm.loading = false;
                vm.selectedRegion = null;
                vm.save = func.actions.saveContentLayout;
                vm.cancel = func.actions.goBackToList;
                vm.removeFile = func.actions.removeFile;
                vm.onFileControlInput = func.actions.onFileControlInput
                vm.getFileUploadIndication = func.actions.getFileUploadIndication;
                vm.goToPreview = func.actions.goToPreview;

                func.actions.getLayoutContentTemplate();
            }
        ]);

    let fnc = function ($scope, $state, $modal, uiGridConstants, layoutService, app, vm, abp, $sce, fileUploader) {
        let saveMarker1 = 0;
        let saveMarker2 = 0;
        let act = {
            goBackToList: function () {
                $state.go("tenant.dineplaylayouts", {});
            },
            getUsableTemplate: function (objTemplate) {
                let replaceTemplate = function (id, template) {
                    let fileInputHtml = `
                        <img id="imgt<<T>>r<<R>>" />
                        <button class="btn btn-default" type="submit" onclick="browseFile('t<<T>>r<<R>>');">
                            <i class="fa fa-paperclip" aria-hidden="true" style="font-size:25px;"></i>
                            <span>Browse File (<<R>>)</span>
                        </button>
                        
                    `;
                    let replaced = template;
                    fileInputHtml = fileInputHtml.replaceAll('<<T>>', id);

                    for (let i = 1; i <= 6; i++) {
                        let replacedFileInputHtml = fileInputHtml.replaceAll('<<R>>', i);
                        replaced = replaced.replaceAll(`<<t${id}r${i}>>`, replacedFileInputHtml);
                    }
                    return replaced;
                };

                let templateHtml = replaceTemplate(objTemplate.id, objTemplate.html);
                objTemplate.html = $sce.trustAsHtml(templateHtml);
                return objTemplate;
            },
            getDefaultContentObjects: function (contentCount) {
                let contents = [];
                for (var i = 0; i < contentCount; i++) {
                    let content = {
                        id:null,
                        regionNo: i + 1,
                        fileName: null,
                        fileType: '-',
                        stretched: false,
                        displayLength: 0,
                        systemFileName: null,
                        file: null,
                        fileToUpload: null,
                        hasFile: false,
                        updateCode:null
                    };
                    contents.push(content);
                }
                return contents;
            },
            getUploader: function () {
                return new fileUploader({
                    url: abp.appPath + 'FileUpload/UploadFile'
                });
            },
            addUploaderFilter: function (uploader) {
                uploader.filters.push({
                    name: 'imageFilter',
                    fn: function (item /*{File|FileLikeObject}*/, options) {
                        return true;
                    }
                });
            },
            getLayoutContents: function () {
                layoutService.getContents({id: vm.layoutId})
                    .success(function (result) {
                        vm.contents = [];
                        for (let i = 0; i < result.length; i++) {
                            let content = {
                                id: result[i].id,
                                regionNo: result[i].regionNo,
                                fileName: result[i].fileName,
                                fileType: result[i].fileType,
                                stretched: result[i].stretched,
                                layoutId: result[i].layoutId,
                                displayLength: result[i].displayLength,
                                systemFileName: result[i].systemFileName,
                                file: null,
                                fileToUpload: null,
                                hasFile: result[i].hasFile,
                                uploader: act.getUploader(),
                                updateCode: null
                            };
                            content.uploader.uploaderId = i;
                            content.uploader.onAfterAddingFile = f.actions.fileUploader.onAfterAddingFile;
                            content.uploader.onSuccessItem = f.actions.fileUploader.onSuccessItem;
                            act.addUploaderFilter(content.uploader);
                            vm.contents.push(content);
                        }
                    })
                    .finally(function () { });
            }
        }

        let f = {
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                goBackToList: function () {
                    act.goBackToList()
                },
                goToPreview: function (objId) {
                    $state.go("tenant.dineplaylayoutspreview", {
                        id: objId,
                        src:'C'
                    });
                },
                initContentLayout: function () {

                },
                saveContentLayout: function (option) {
                    if (option === 'UL') {
                        saveMarker1 = 0;
                        saveMarker2 = 0;
                        for (var i = 0; i < vm.contents.length; i++) {
                            if (vm.contents[i].fileToUpload) {
                                saveMarker1++;
                                vm.contents[i].fileToUpload.upload();
                            }
                        }
                    } else if (option === 'SV') {
                        let params = [];
                        for (var i = 0; i < vm.contents.length; i++) {
                            params.push({
                                id: vm.contents[i].id,
                                fileName: vm.contents[i].fileName,
                                systemFileName: vm.contents[i].systemFileName,
                                LayoutId: vm.contents[i].layoutId,
                                regionNo: vm.contents[i].regionNo,
                                fileType: vm.contents[i].fileType,
                                displayLength: vm.contents[i].displayLength,
                                stretched: vm.contents[i].stretched,
                                hasFile: vm.contents[i].hasFile
                            });
                        }
                        vm.loading = true;
                        layoutService.createOrUpdateLayoutContent({
                            LayoutContents: params
                        }).success(function () {
                            abp.notify.info(app.localize('SavedSuccessfully'));
                        }).finally(function () {
                            vm.loading = false;
                        });
                    }
                },
                goBackToList: function () {
                    act.goBackToList();
                },
                getLayoutContentTemplate: function () {
                    layoutService.getLayoutContentTemplate(vm.layoutId)
                        .success(function (result) {
                            vm.layoutContentTemplate = act.getUsableTemplate(result);
                            //vm.contents = act.getDefaultContentObjects(vm.layoutContentTemplate.contentCount);
                            vm.contents = act.getLayoutContents();
                        })
                        .finally(function () {
                            vm.loading = false;
                        });
                },
                onFileControlInput: function (region) {
                    vm.selectedRegion = region;
                },
                fileUploader: {
                    onAfterAddingFile: function (item) {
                        // console.log("item");
                        // console.log(item);
                        // console.log(this.uploaderId);

                        if (item.file.size > 10000000) {
                            abp.notify.error(app.localize('Size is Big'));
                            if (vm.uploader) vm.uploader.clearQueue();
                            return;
                        }
                        console.log('FileSize');
                        console.log(item.file.size);
                        console.log(item.file.type);
                        console.log(!(item.file.type.indexOf('/avi') >= 0 || item.file.type.indexOf('/mp4') >= 0));

                        if (!item.file.type.startsWith('image') &&
                            !item.file.type.startsWith('video')) {
                            abp.notify.error(app.localize('FileTypeNotSupported'));
                            if (vm.uploader) vm.uploader.clearQueue();
                            return;
                        }

                        if (!(item.file.type.indexOf('/avi') >= 0 ||
                            item.file.type.indexOf('/mp4') >= 0 ||
                            item.file.type.indexOf('/jpg') >= 0 ||
                            item.file.type.indexOf('/jpeg') >= 0 ||
                            item.file.type.indexOf('/png') >= 0)
                        ) {
                            abp.notify.error(app.localize('FileTypeNotSupported'));
                            if (vm.uploader) vm.uploader.clearQueue();
                            return;
                        }

                        vm.selectedRegion.fileName = item.file.name;
                        vm.selectedRegion.fileToUpload = item;
                        vm.selectedRegion.fileType = item.file.type;
                        
                        if (vm.selectedRegion.hasFile) {
                            vm.selectedRegion.updateCode = "UPD";
                        } else if (!vm.selectedRegion.hasFile) {
                            vm.selectedRegion.updateCode = "NEW";
                        }
                        vm.selectedRegion.hasFile = true;
                        //item.upload();
                    },
                    onSuccessItem: function (fileItem, response, status, headers) {
                        /*
                        if (vm.replyFiles == null) {
                            vm.replyFiles = [];
                        }*/
                        if (response != null) {
                            if (response.result != null) {
                                if (response.result.fileName != null) {
                                    vm.contents[this.uploaderId].systemFileName =
                                        response.result.fileName;
                                    saveMarker2++;
                                }
                            }
                        }

                        if (saveMarker1 === saveMarker2) {
                            f.actions.saveContentLayout('SV');
                        }
                        
                    }
                },
                removeFile: function (index) {
                    let content = vm.contents[index];

                    if (content.hasFile) {
                        content.updateCode = 'OLD';
                    }

                    content.fileName= null;
                    content.fileType= null;
                    content.stretched= false;
                    content.displayLength= 0;
                    content.systemFileName= '-';
                    content.file= null;
                    content.fileToUpload= null;
                    content.hasFile= false;
                },
                getFileUploadIndication: function (fileName) {
                    return !fileName ? false : fileName.length <2 ? false : true;
                }
            }
        };

        return f;
    };
}());

let browseFile = function (fileCtrl) {
    $('#' + fileCtrl).click();
};
