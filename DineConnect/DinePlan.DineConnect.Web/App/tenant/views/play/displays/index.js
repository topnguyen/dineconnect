﻿(function () {

    appModule.controller("tenant.views.play.displays",
        [
            "$scope",
            "$state",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.display",
            function ($scope, $state, $modal, uiGridConstants, displayService) {

                var vm = this;

                let func = fnc($scope, $state, $modal, uiGridConstants, displayService, app, vm);
                func.actions.init(App);

                vm.loading = false;
                vm.acceptChange = null;
                vm.currentUserId = abp.session.userId;
                vm.isDeleted = false;
                vm.filterText = null;

                vm.permissions = func.actions.readPermissions(abp);

                let gridUt = gridUtil($scope, app, vm, uiGridConstants);
                vm.userGridOptions = gridUt.gridOptions;
                vm.createDisplay = func.actions.createDisplay;
                vm.editDisplay = func.actions.editDisplay;
                vm.deleteDisplay = func.actions.deleteDisplay;

                vm.getDisplays = func.actions.getDisplays;
                vm.getDisplays();
                vm.filters = func.filters.location;
                vm.openLocationSelectorModal = func.actions.openLocationSelectorModal;
            }
        ]);


    let fnc = function ($scope, $state, $modal, uiGridConstants, displayService, app, vm) {

        let act = {
            xxx: function () {

            }
        }

        var requestParams = {
            skipCount: 0,
            maxResultCount: app.consts.grid.defaultPageSize,
            sorting: 'id'
        };

        let f = {
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                readPermissions: function (abp) {
                    return {
                        create: abp.auth.hasPermission('Pages.Tenant.Play.Display.Create'),
                        edit: abp.auth.hasPermission('Pages.Tenant.Play.Display.Edit'),
                        delete: abp.auth.hasPermission('Pages.Tenant.Play.Display.Delete'),
                        approve: false
                    };
                },
                createDisplay: function () {
                    $state.go('tenant.dineplaydisplaysdetail',{});
                },
                getDisplays: function () {
                    vm.loading = true;

                    displayService.getAll({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterText,
                        location: vm.location,
                        acceptChange: vm.acceptChange,
                        isDeleted: vm.isDeleted,
                        locationId: f.filters.location.id
                    })
                        .success(function (result) {
                            vm.userGridOptions.totalItems = result.totalCount;
                            vm.userGridOptions.data = result.items;
                        })
                        .finally(function () {
                            vm.loading = false;
                        });
                },
                editDisplay: function (obj) {
                    $state.go('tenant.dineplaydisplaysdetail', {id:obj.id});
                },
                deleteDisplay: function (obj) {
                    abp.message.confirm(
                        app.localize('DeleteDisplayWarning', obj.name),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                displayService.deleteDisplay({
                                    id: obj.id
                                }).success(function (result) {
                                    if (result.success) {
                                        vm.getDisplays();
                                        abp.notify.success(app.localize('SuccessfullyDeleted'));
                                    } else {
                                        let eventNames = '';
                                        let counter = 1;

                                        try {
                                            result.usedByNames.forEach(function(n) {
                                                if (counter > 3) {
                                                    eventNames += (eventNames != '' ? '\n' : '') +
                                                        `and more (${result.usedByNames.length - 3}).`;
                                                    throw {};
                                                } else {
                                                    eventNames += (eventNames != '' ? '\n' : '') + '📅' + n;
                                                }
                                                counter++;
                                            });
                                        } catch (err) {
                                            
                                        }
                                        abp.message.warn(
                                            app.localize('DisplayIsInUse', obj.name) + '\n' +  eventNames
                                        )
                                    }
                                });
                            }
                        }
                    );
                },
                openLocationSelectorModal: function () {
                    let searchParam = app.createLocationInputForSearch();
                    searchParam.hideGroupDisplay = true;
                    const modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return searchParam;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        f.filters.location = result.locations[0];
                        f.actions.getDisplays();
                        // console.log(f.filters.location);
                    });
                }
            },
            filters: {
                location: app.createLocationInputForSearch()
            }
        };

        return f;
    };


    let gridUtil = function ($scope, app, vm, uiGridConstants) {
        let rowTemplate = `
        <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" 
            class="ui-grid-cell" 
            ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  
            ui-grid-cell>
        </div>
            `;
        let columnDefTemplates = [
            `
        <div class="ui-grid-cell-contents">
            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>
                <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" 
                    aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-cog"></i> ${app.localize("Actions")} <span class="caret"></span>
                </button>
            <ul uib-dropdown-menu>
                    <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.edit"
                        ng-click="grid.appScope.editDisplay(row.entity)">${app.localize("Edit")}</a></li>
                    <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.approve"
                        ng-click="grid.appScope.editDisplay(row.entity)">${app.localize("Approve")}</a></li>
                    <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.delete"
                        ng-click="grid.appScope.deleteDisplay(row.entity)">${app.localize("Delete")}</a></li>
            </ul>
            </div>
        </div>`
        ];
        return {
            gridOptions: {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: rowTemplate,
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate: columnDefTemplates[0]
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('MacAddress'),
                        field: 'macAddress',
                        width: 150
                    },
                    {
                        name: app.localize('Active'),
                        field: 'active',
                        width: 100
                    },
                    {
                        name: app.localize('Approved'),
                        field: 'approved',
                        width: 110
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' +
                                sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope,
                        function (pageNumber, pageSize) {
                            requestParams.skipCount = (pageNumber - 1) * pageSize;
                            requestParams.maxResultCount = pageSize;

                            vm.getAll();
                        });
                },
                data: []
            }
        };
    };



}());
