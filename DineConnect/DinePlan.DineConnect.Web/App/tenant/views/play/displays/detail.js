﻿(function () {
    appModule.controller("tenant.views.play.display.detail",
        [
            "$scope",
            "$state",
            "$stateParams",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.resolution",
            "abp.services.app.location",
            "abp.services.app.display",
            "abp.services.app.displayGroup",
            function ($scope, $state, $stateParams, $modal, uiGridConstants, resolutionService,
                locationService, displayService, displayGroupService) {

                var vm = this;

                let func = fnc($scope, $state, $modal, resolutionService, app, vm,
                    locationService, displayService, displayGroupService);
                let gridUt = gridUtil($scope, app, vm, uiGridConstants);

                func.actions.init(App);

                vm.loading = false;

                vm.resolutions = [];
                vm.plants = [];
                vm.displayGroups = [];
                vm.selectedPlant = null;
                vm.filterText = null;
                vm.displayId = $stateParams.id;

                if (vm.displayId && vm.displayId > 0) {
                    vm.workMode = 'EDIT';
                } else {
                    vm.workMode = 'ADD';
                }

                vm.display = {};
                vm.selectedDisplayGroups = [];
                vm.selectedResolution = null;
                vm.currentUserId = abp.session.userId;
                vm.rowSelectionFlag = true;

                vm.userGridOptions = gridUt.gridOptions;
                vm.save = func.actions.saveDisplay;
                vm.cancel = func.actions.goBackToList;
                vm.removeDisplayGroup = func.actions.removeDisplayGroup;
                vm.getDisplayGroups = func.actions.getDisplayGroups;
                vm.openDisplayGroupSelectorModal = func.actions.openDisplayGroupSelectorModal;
                vm.openLocationSelectorModal = func.actions.openLocationSelectorModal;
                vm.copyRegistrationKeyToClipBoard = func.actions.copyRegistrationKeyToClipBoard;

                func.actions.getResolutions();

                if (!vm.displayId || vm.displayId <= 0) {
                    // vm.getDisplayGroups();
                }
            }
        ]);

    /* GRID */
    let gridUtil = function ($scope, app, vm, uiGridConstants) {
        let rowTemplate = `
        <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" 
            class="ui-grid-cell" 
            ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  
            ui-grid-cell>
        </div>
            `;
        let columnDefTemplates = [];
        return {
            gridOptions: {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: rowTemplate,
                columnDefs: [
                    {
                        name: app.localize('Code'),
                        field: 'code'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize(" "),
                        maxWidth: 50,
                        cellTemplate: `
                            <div class=\"ui-grid-cell-contents text-center\">
                                <button ng-click=\"grid.appScope.removeDisplayGroup(row.entity)\" 
                                    class=\"btn btn-link btn-xs\" title=\"" + app.localize("") + "\">
                                    <i style=\"color:black;font-size:15px;\" class=\"fa fa-minus-circle\"></i>
                                </button>
                            </div>`
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {

                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' +
                                sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope,
                        function (pageNumber, pageSize) {
                            requestParams.skipCount = (pageNumber - 1) * pageSize;
                            requestParams.maxResultCount = pageSize;
                            vm.getAll();
                        });
                },
                data: []
            }
        };
    };

    function ObjectList() {
        return {
            add: function (myObj) {
                if (myObj != null) {
                    var myIndex = -1;

                    if (this.items != null) {
                        this.items.some(function (value, key) {
                            if (value.id == myObj.id) {
                                myIndex = value;
                                return true;
                            }
                        });
                    }

                    if (myIndex === -1) {
                        this.items.push(myObj);
                    } else {
                        //if (vm.autoSelection == false)
                        //    abp.notify.error(myObj.name + "  " + app.localize("Available"));
                    }
                }
            },
            remove: function (id) {
                for (let i = 0; i < this.items.length; i++) {
                    const obj = this.items[i];

                    if (obj.id == id) {
                        this.items.splice(i, 1);
                    }
                }
            },
            items: []
        }
    };

    let fnc = function ($scope, $state, $modal, resolutionService, app, vm,
        locationService, displayService, displayGroupService) {

        let act = {
            goBackToList: function () {
                $state.go("tenant.dineplaydisplays", {});
            },
            getResolutionById: function (id) {
                for (var i = 0; i < vm.resolutions.length; i++) {
                    if (vm.resolutions[i].id == id) {
                        vm.selectedResolution = vm.resolutions[i];
                    }
                }
            },
            getPlantById: function (id) {
                for (var i = 0; i < vm.plants.length; i++) {
                    if (vm.plants[i].id == id) {
                        vm.selectedPlant = vm.plants[i];
                    }
                }
            }
        }

        var requestParams = {
            skipCount: 0,
            maxResultCount: app.consts.grid.defaultPageSize,
            sorting: 'id'
        };

        let f = {
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                goBackToList: function () {
                    act.goBackToList()
                },
                initDisplay: function () {

                },
                saveDisplay: function () {
                    vm.display.id = vm.displayId;
                    vm.display.resolutionId = vm.selectedResolution.id;
                    vm.display.resolution = vm.selectedResolution;
                    vm.display.locationId = vm.selectedPlant.id;
                    vm.display.location = vm.selectedPlant;
                    vm.display.displayGroups = vm.userGridOptions.data;
                    displayService.createOrUpdateDisplay(
                        {
                            display: vm.display
                        }
                    ).success(
                        function () {
                            act.goBackToList();
                        }
                    );
                    
                },
                getResolutions: function () {
                    resolutionService.getAll({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        isDeleted: false
                    })
                        .success(function (result) {
                            vm.resolutions = result.items;
                            f.actions.getPlants();
                        })
                        .finally(function () { })
                },
                getPlants: function () {
                    vm.loading = true;
                    locationService.getLocationBasedOnUser({
                        userId: vm.currentUserId
                    }).success(function (result) {
                        vm.plants = $.parseJSON(JSON.stringify(result.items));
                        if (vm.displayId && vm.displayId > 0) {
                            f.actions.getDisplayForEdit();
                        }
                    }).finally(function (result) {
                        vm.loading = false;
                    });
                },
                getDisplayGroups: function () {
                    vm.loading = true;
                    displayGroupService.getAll({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        isDeleted: false,
                        filter: vm.filterText
                    })
                        .success(function (result) {
                            vm.userGridOptions.totalItems = result.totalCount;
                            vm.userGridOptions.data = result.items;
                            // console.log(result.items);
                        })
                        .finally(function () {
                            vm.loading = false;
                        });
                },
                addDisplayGroupSelection: function(myObj) {
                    if (myObj != null) {
                        var myIndex = -1;
                        if (vm.selectedDisplayGroups != null) {
                            vm.selectedDisplayGroups.some(function (value, key) {
                                if (value.id == myObj.id) {
                                    myIndex = value;
                                    return true;
                                }
                            });
                        }

                        if (myIndex === -1) {
                            vm.selectedDisplayGroups.push(myObj);
                        } else {
                            if (vm.autoSelection == false)
                                abp.notify.error(myObj.name + "  " + app.localize("Available"));
                        }
                    }
                },
                removeDisplayGroupSelection: function (row) {
                    for (let i = 0; i < vm.selectedDisplayGroups.length; i++) {
                        const obj = vm.selectedDisplayGroups[i];

                        if (obj.id == row.entity.id) {
                            vm.selectedDisplayGroups.splice(i, 1);
                        }
                    }
                },
                getDisplayForEdit: function () {
                    vm.loading = true;
                    displayService.getDisplayForEdit({ id: vm.displayId })
                        .success(function (result) {
                            vm.display.name = result.display.name;
                            vm.display.locationId = result.display.locationId;
                            vm.display.resolutionId = result.display.resolutionId;
                            act.getResolutionById(result.display.resolutionId);
                            act.getPlantById(result.display.locationId);
                            vm.display.macAddress = result.display.macAddress;
                            vm.display.active = result.display.active;
                            vm.userGridOptions.totalItems = result.display.displayGroups.length;
                            vm.userGridOptions.data = result.display.displayGroups;
                            vm.registrationKey = result.display.registrationKey;
                        })
                        .finally(function () {
                            vm.loading = false;
                        });
                },
                openDisplayGroupSelectorModal: function () {
                    let modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/play/displaygroups/selectDisplayGroups.cshtml',
                        controller: 'tenant.views.play.displaygroup.selector as vm',
                        backdrop: 'static',
                        resolve: {
                            existings: function () {
                                return f.actions.getIds(vm.userGridOptions.data);
                            },
                            multiSelect: function () {
                                return true;
                            }
                        }
                    })

                    modalInstance.result.then(function (result) {
                        if (result && result.length > 0) {
                            for (var i = 0; i < result.length; i++) {
                                let found = -1;
                                vm.userGridOptions.data.some(function (value, key) {
                                    if (result[i].id === value.id) {
                                        found = value;
                                        return true;
                                    }
                                });
                                if (found === -1) {
                                    vm.userGridOptions.data.push(result[i]);
                                }
                            }

                        }

                    });
                },
                getIds: function (items) {
                    let ids = [];
                    for (var i = 0; i < items.length; i++) {
                        ids.push(items[i].id);
                    }
                    return ids;
                },
                removeDisplayGroup: function (obj) {
                    let list = new ObjectList();
                    list.items = vm.userGridOptions.data;
                    list.remove(obj.id);
                },
                openLocationSelectorModal: function () {
                    let searchParam = app.createLocationInputForSearch();
                    searchParam.hideGroupDisplay = true;
                    const modalInstance = $modal.open({
                        templateUrl: "~/App/tenant/views/connect/location/select.cshtml",
                        controller: "tenant.views.connect.location.select.location as vm",
                        backdrop: "static",
                        keyboard: false,
                        resolve: {
                            location: function () {
                                return searchParam;
                            }
                        }
                    });
                    modalInstance.result.then(function (result) {
                        act.getPlantById(result.locations[0].id)
                        // console.log(result.locations[0]);
                    });
                },
                copyRegistrationKeyToClipBoard: function () {
                    let text = document.getElementById('txtRegisKey');
                    text.style.display = 'block';
                    text.select();
                    text.setSelectionRange(0, 99999); /* For mobile devices */
                    
                    /* Copy the text inside the text field */
                    document.execCommand("copy");
                    setTimeout(function () {
                        //text.style.display = 'none';
                    }, 100);  
                    text.style.display = 'none';
                    abp.notify.success(app.localize('RegisKeyCoppied'));
                }
            }
        };

        return f;
    };


}());