﻿
(function () {
    let gridUtil = function ($scope, app, vm, uiGridConstants) {
        let rowTemplate = `
        <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" 
            class="ui-grid-cell" 
            ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  
            ui-grid-cell>
        </div>
            `;
        let columnDefTemplates = [
            `
        <div class="ui-grid-cell-contents">
            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>
                <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" 
                    aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-cog"></i> ${app.localize("Actions")} <span class="caret"></span>
                </button>
            <ul uib-dropdown-menu>
                    <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.edit"
                        ng-click="grid.appScope.openDaypartForEdit(row.entity)">${app.localize("Edit")}</a></li>
                    <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.delete"
                        ng-click="grid.appScope.deleteDaypart(row.entity)">${app.localize("Delete")}</a></li>
            </ul>
            </div>
        </div>`
        ];
        return {
            gridOptions: {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: rowTemplate,
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate: columnDefTemplates[0]
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' +
                                sortColumns[0].sort.direction;
                        }

                        vm.getDayparts();
                    });
                    gridApi.pagination.on.paginationChanged($scope,
                        function (pageNumber, pageSize) {
                            requestParams.skipCount = (pageNumber - 1) * pageSize;
                            requestParams.maxResultCount = pageSize;

                            vm.getDayparts();
                        });
                },
                data: []
            }
        };
    };

    appModule.controller("tenant.views.play.dayparting",
        [
            "$scope",
            "$state",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.daypart",
            function ($scope, $state, $modal, uiGridConstants, daypartService) {

                var vm = this;

                vm.loading = false;

                let func = fnc($scope, $state, $modal, uiGridConstants, daypartService, app, vm);
                func.actions.init(App);

                vm.permissions = func.actions.readPermissions(abp);

                vm.acceptChange = null;
                vm.currentUserId = abp.session.userId;
                vm.isDeleted = false;
                vm.filterText = null;

                let grdUt = gridUtil($scope, app, vm, uiGridConstants);

                vm.userGridOptions = grdUt.gridOptions;

                vm.createDaypart = func.actions.createDaypart;
                vm.openDaypartForEdit = func.actions.openDaypartForEdit;
                vm.deleteDaypart = func.actions.deleteDaypart;
                vm.getDayparts = func.actions.getDayparts;
                vm.getDayparts();

            }
        ]);


    let fnc = function ($scope, $state, $modal, uiGridConstants, daypartService, app, vm) {

        let act = {
            xxx: function () {

            }
        }


        var requestParams = {
            skipCount: 0,
            maxResultCount: app.consts.grid.defaultPageSize,
            sorting: 'id'
        };


        let f = {
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                createDaypart: function openCreateOrEditModal() {
                    $state.go("tenant.dineplaydaypartingdetail", {});
                },
                openDaypartForEdit: function (obj) {
                    $state.go("tenant.dineplaydaypartingdetail", {id:obj.id});
                },
                readPermissions: function (abp) {
                    return {
                        create: abp.auth.hasPermission('Pages.Tenant.Play.DayParting.Create'),
                        edit: abp.auth.hasPermission('Pages.Tenant.Play.DayParting.Edit'),
                        delete: abp.auth.hasPermission('Pages.Tenant.Play.DayParting.Delete')
                    };
                },
                getDayparts: function () {
                    vm.loading = true;
                    daypartService.getAll({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterText,
                        isDeleted: vm.isDeleted
                    })
                        .success(function (result) {
                            vm.userGridOptions.totalItems = result.totalCount;
                            vm.userGridOptions.data = result.items;
                        })
                        .finally (function () {
                            vm.loading = false;
                        });
                },
                deleteDaypart: function (obj) {
                    abp.message.confirm(
                        app.localize('DeleteDaypartWarning', obj.name),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                daypartService.deleteDaypart({ id: obj.id })
                                    .success(function (result) {
                                        vm.getDayparts();
                                    });
                            }
                        }
                    )
                }
            }
        };

        return f;
    };


}());
