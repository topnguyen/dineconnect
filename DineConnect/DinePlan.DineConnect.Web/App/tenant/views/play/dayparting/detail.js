﻿
(function () {
    appModule.controller("tenant.views.play.dayparting.detail",
        [
            "$scope",
            "$state",
            "$stateParams",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.daypart",
            function ($scope, $state, $stateParams, $modal, uiGridConstants, daypartService) {

                var vm = this;

                vm.loading = false;

                let func = fnc($scope, $state, $modal, uiGridConstants, app, vm, daypartService);
                func.actions.init(App);

                vm.daypartId = $stateParams.id;

                if (vm.daypartId && vm.daypartId > 0) {
                    vm.workMode = 'EDIT';
                } else {
                    vm.workMode = 'ADD';
                }

                vm.cancel = func.actions.goBackToList;
                vm.save = func.actions.saveDaypart;

                vm.weekdays = weekdays;
                // vm.weekdays2 = ['Sunday','Monday','Tuesday'];

                vm.daypart = {
                    name: '',
                    startTime:null,
                    endTime: null,
                    exceptions: []
                };


                vm.startTime = null;
                vm.endTime = null;

                vm.exceptionList = new ObjectList();
                vm.exceptionList.items = vm.daypart.exceptions;
                vm.addException = func.actions.addException;

                if (vm.workMode == 'EDIT') {
                    func.actions.getDaypartForEdit();
                }

                vm.addException(true,null);
            }
        ]);

    let weekdays = [
        { value: 0, text: app.localize("-"), text2: "-" },
        { value: 1, text: app.localize("Sunday"), text2: "Sunday" },
        { value: 2, text: app.localize("Monday"), text2: "Monday" },
        { value: 3, text: app.localize("Tuesday"), text2: "Tuesday" },
        { value: 4, text: app.localize("Wednesday"), text2: "Wednesday" },
        { value: 5, text: app.localize("Thursday"), text2: "Thursday" },
        { value: 6, text: app.localize("Friday"), text2: "Friday" },
        { value: 7, text: app.localize("Saturday"), text2: "Saturday" },
    ];

    function ObjectList() {
        return {
            add: function (myObj) {
                if (myObj != null) {
                    var myIndex = -1;

                    if (this.items != null) {
                        this.items.some(function (value, key) {
                            if (value.id == myObj.id) {
                                myIndex = value;
                                return true;
                            }
                        });
                    }

                    if (myIndex === -1) {
                        if (!myObj.id) {
                            myObj.id = this.nextId;
                            this.nextId++;
                        }
                        this.items.push(myObj);
                        // console.log(myObj);
                    } else {
                        //if (vm.autoSelection == false)
                        //    abp.notify.error(myObj.name + "  " + app.localize("Available"));
                    }
                }
            },
            remove: function (id) {
                for (let i = 0; i < this.items.length; i++) {
                    const obj = this.items[i];

                    if (obj.id == id) {
                        this.items.splice(i, 1);
                    }
                }
            },
            items: [],
            nextId: 1
        }
    }

    let fnc = function ($scope, $state, $modal, uiGridConstants, app, vm, daypartService) {

        let act = {
            goBackToList: function () {
                $state.go("tenant.dineplaydayparting", {});
            },
            getWeekday: function (day) {
                let wd = vm.weekdays.find( function(e) { return e.text2 === day })
                return wd;
            },
            getExceptionToSave: function (exception) {
                if (!exception.day||exception.day.text==='-') return null;
                let output = {
                    day: exception.day.text2,
                    startHour: exception.startTime.getHours(),
                    startMinute: exception.startTime.getMinutes(),
                    endHour: exception.endTime.getHours(),
                    endMinute: exception.endTime.getMinutes()
                };
                if (exception.sysId) {
                    output.id = exception.sysId;
                } else {
                    output.id = null;
                }
                return output;
            },
            getExceptionForEdit:function (exception) {
                return {
                    day: this.getWeekday(exception.day),
                    startTime: new Date(1970, 0, 1, exception.startHour, exception.startMinute),
                    endTime: new Date(1970, 0, 1, exception.endHour, exception.endMinute),
                    sysId: exception.sysId
                }
            }
        }

        let f = {
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                goBackToList: function () {
                    act.goBackToList()
                },
                saveDaypart: function () {
                     // console.log(vm.daypart);
                    let daypart = {
                        id: vm.daypart.id,
                        name: vm.daypart.name,
                        startHour: vm.daypart.startTime.getHours(),
                        startMinute: vm.daypart.startTime.getMinutes(),
                        endHour: vm.daypart.endTime.getHours(),
                        endMinute: vm.daypart.endTime.getMinutes(),
                    }
                    daypart.exceptions = [];
                    for (var i = 0; i < vm.daypart.exceptions.length; i++) {
                        let exception = act.getExceptionToSave(vm.daypart.exceptions[i]);
                        if (!exception) continue;
                        exception.daypartId = daypart.id;
                        daypart.exceptions.push(exception);
                    }

                    // console.log(daypart);

                    daypartService.createOrUpdateDaypart(
                        { Daypart: daypart }
                    ).success(
                        function (result) {
                            act.goBackToList();
                        }
                    );
                },
                getDaypartForEdit: function () {
                    daypartService.getDaypartForEdit({ id: vm.daypartId })
                        .success(function (result) {
                            // console.log(result.daypart);
                            vm.daypart.id = result.daypart.id;
                            vm.daypart.name = result.daypart.name;
                            vm.daypart.startTime = new Date(1970, 0, 1, result.daypart.startHour, result.daypart.startMinute);
                            vm.daypart.endTime = new Date(1970, 0, 1, result.daypart.endHour, result.daypart.endMinute);
                            vm.daypart.exceptions = [];
                            vm.exceptionList.items = vm.daypart.exceptions;
                            vm.exceptionList.nextId = 1;
                            for (var i = 0; i < result.daypart.exceptions.length; i++) {
                                result.daypart.exceptions[i].sysId = result.daypart.exceptions[i].id;
                                vm.exceptionList.add(act.getExceptionForEdit(result.daypart.exceptions[i]));
                                // vm.daypart.exceptions.push(act.getExceptionForEdit(result.daypart.exceptions[i]));
                            }
                            if (vm.daypart.exceptions.length <= 0) vm.addException(true, null);
                        })
                        .finally(function () {

                        });
                },
                addException: function (add,exception) {
                    let obj = { day: weekdays[0], startTime: null, endTime: null };
                    if (add) {
                        // vm.daypart.exceptions.push(obj);
                        vm.exceptionList.add(obj);
                    } else {
                        // vm.daypart.slice()
                        // console.log('Remove');
                        // console.log(exception);
                        vm.exceptionList.remove(exception.id);
                    }
                    
                }
            }
        };

        return f;
    };



}());