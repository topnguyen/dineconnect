﻿(function () {
    appModule.controller("tenant.views.paly.resolutions.detail",
        [
            "$scope",
            "$state",
            "$stateParams",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.resolution",
            function ($scope, $state, $stateParams, $modal, uiGridConstants, resolutionService) {

                var vm = this;
                let func = fnc($scope, $state, $modal, uiGridConstants, resolutionService,app,vm);

                vm.loading = false;

                vm.resolutionId = $stateParams.id;

                if (vm.resolutionId && vm.resolutionId > 0) {
                    vm.workMode = 'EDIT';
                } else {
                    vm.workMode = 'ADD';
                }

                func.actions.init(App);
                func.actions.initResolution();

                if (vm.workMode == "EDIT") {
                    func.actions.getResolutionForEdit();
                }

                vm.cancel = func.actions.goBackToList;
                vm.save = func.actions.saveResolution;
                
            }
        ]);

    let fnc = function ($scope, $state, $modal, uiGridConstants, resolutionService, app, vm) {

        let act = {
            goBackToList: function () {
                $state.go("tenant.dineplayresolutions", {});
            }
        }

        let f = {
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                goBackToList: function () {
                    act.goBackToList()
                },
                initResolution: function () {
                    vm.resolution = {};
                    vm.resolution.name = '';
                    vm.resolution.width = 0;
                    vm.resolution.height = 0;
                    vm.resolution.enabled = true;
                },
                saveResolution: function () {
                    vm.resolution.id = vm.resolutionId;
                    resolutionService.createOrUpdateResolution(
                        { resolution: vm.resolution }
                    ).success(
                        function () {
                            act.goBackToList();
                        }
                    );
                },
                getResolutionForEdit: function () {
                    resolutionService.getResolutionForEdit({ id: vm.resolutionId })
                        .success(function (result) {
                            vm.resolution.id = result.resolution.id;
                            vm.resolution.name = result.resolution.name;
                            vm.resolution.width = result.resolution.width;
                            vm.resolution.height = result.resolution.height;
                            vm.resolution.enabled = result.resolution.enabled;
                        })
                        .finally();
                }
            }
        };

        return f;
    };

}());