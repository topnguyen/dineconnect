﻿(function () {
    appModule.controller("tenant.views.paly.resolutions",
        [
            "$scope",
            "$state",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.resolution",
            function ($scope,$state, $modal, uiGridConstants, resolutionService) {

                var vm = this;

                let func = fnc($scope, $state, $modal,
                    uiGridConstants, resolutionService, app, vm);

                func.actions.init(App, abp.session.userId);

                vm.permissions = func.actions.readPermission(abp);
                vm.userGridOptions =
                    gridUtil($scope, app, vm, uiGridConstants).gridOptions;

                vm.getAll = func.actions.getAll;
                vm.getAll();

                vm.openCreateOrEdit = func.actions.openCreateOrEdit;
                vm.deleteResolution = func.actions.deleteResolution;
            }
        ]);

    let gridUtil = function ($scope, app, vm, uiGridConstants) {
        let rowTemplate = `
        <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" 
            class="ui-grid-cell" 
            ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  
            ui-grid-cell>
        </div>
            `;
        let columnDefTemplates = [
            `
        <div class="ui-grid-cell-contents">
            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>
                <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" 
                    aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-cog"></i> ${app.localize("Actions")} <span class="caret"></span>
                </button>
            <ul uib-dropdown-menu>
                    <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.edit"
                        ng-click="grid.appScope.openCreateOrEdit(row.entity)">${app.localize("Edit")}</a></li>
                    <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.delete"
                        ng-click="grid.appScope.deleteResolution(row.entity)">${app.localize("Delete")}</a></li>
            </ul>
            </div>
        </div>`
        ];
        return {
            gridOptions: {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: rowTemplate,
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate: columnDefTemplates[0]
                    },
                    {
                        name: app.localize('ID'),
                        field: 'id',
                        width: 100
                    },
                    {
                        name: app.localize('Resolution'),
                        field: 'name'
                    },
                    {
                        name: app.localize('Width'),
                        field: 'width'
                    },
                    {
                        name: app.localize('Height'),
                        field: 'height'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' +
                                sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope,
                        function (pageNumber, pageSize) {
                            requestParams.skipCount = (pageNumber - 1) * pageSize;
                            requestParams.maxResultCount = pageSize;

                            vm.getAll();
                        });
                },
                data: []
            }
        };
    };

    let fnc = function ($scope, $state, $modal, uiGridConstants, resolutionService, app, vm) {

        let act = {
            xxx: function () {

            }
        }

        var requestParams = {
            skipCount: 0,
            maxResultCount: app.consts.grid.defaultPageSize,
            sorting: 'id'
        };

        let f = {
            actions: {
                init: function (App, userId) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });

                    vm.loading = false;
                    vm.acceptChange = null;
                    vm.currentUserId = userId;
                    vm.isDeleted = false;
                    vm.filterText = null;

                },
                readPermission: function (abp) {
                    return {
                        create: abp.auth.hasPermission('Pages.Tenant.Play.Resolution.Create'),
                        edit: abp.auth.hasPermission('Pages.Tenant.Play.Resolution.Edit'),
                        delete: abp.auth.hasPermission('Pages.Tenant.Play.Resolution.Delete')
                    }
                },
                getAll: function () {
                    vm.loading = true;
                    resolutionService.getAll({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterText,
                        location: vm.location,
                        acceptChange: vm.acceptChange,
                        isDeleted: vm.isDeleted
                    }).success(function (result) {
                        vm.userGridOptions.totalItems = result.totalCount;
                        vm.userGridOptions.data = result.items;
                    }).finally(function () {
                        vm.loading = false;
                    });
                },
                openCreateOrEdit: function (obj) {
                    let id = obj ? obj.id : null;
                    $state.go("tenant.dineplayresolutiondetail", { id: id });
                },
                deleteResolution: function (obj) {
                    abp.message.confirm(
                        app.localize('DeleteResolutionWarning', obj.name),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                resolutionService.deleteResolution({ id: obj.id })
                                    .success(function (result) {
                                        if (result.success) {
                                            vm.getAll();
                                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                                        } else {
                                            let displayNames = '';
                                            let counter = 1;
                                            try {
                                                result.usedByNames.forEach(function (n) {
                                                    let owningType = n.indexOf('Display') >= 0 ? 1 : 2;
                                                    if (counter > 3) {
                                                        displayNames += (displayNames != '' ? '\n' : '') +
                                                            `and more (${result.usedByNames.length - 3}).`;
                                                        throw {};
                                                    } else {
                                                        displayNames += (displayNames != '' ? '\n' : '') +
                                                            (owningType == 1 ? '🖥️' : '📐') + n;
                                                    }
                                                    counter++;
                                                });
                                            } catch(err) {}
                                            abp.message.warn(
                                                app.localize('ResolutionIsInUse', obj.name) + '\n' + displayNames
                                            );
                                        }

                                    }).finally(function () {
                                        vm.loading = false;
                                    });
                            }
                        }
                    );
                }
            }
        };

        return f;
    };

}());