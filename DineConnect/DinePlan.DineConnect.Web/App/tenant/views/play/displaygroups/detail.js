﻿
(function () {
    appModule.controller("tenant.views.play.displaygroup.detail",
        [
            "$scope",
            "$state",
            "$stateParams",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.displayGroup",
            function ($scope, $state, $stateParams, $modal, uiGridConstants, displayGroupService) {

                var vm = this;

                let func = fnc($scope, $state, $modal, uiGridConstants, app, vm, displayGroupService);
                let gridUt = gridUtil($scope, app, vm, uiGridConstants);

                func.actions.init(App);

                vm.displayGroupId = $stateParams.id

                if (vm.displayGroupId && vm.displayGroupId > 0) {
                    vm.workMode = 'EDIT';
                } else {
                    vm.workMode = 'ADD';
                }

                vm.loading = false;
                vm.cancel = func.actions.goBackToList;
                vm.save = func.actions.saveDisplayGroup;
                vm.userGridOptions = gridUt.gridOptions; 
                vm.getDisplaysOfGroup = func.actions.getDisplaysOfGroup;
                vm.openDisplaySelectorModal = func.actions.openDisplaySelectorModal;
                vm.removeDisplayGroup = func.actions.removeDisplayGroup;

                if (vm.workMode=='EDIT') {
                    func.actions.getDisplayGroupForEdit();
                    // vm.getDisplaysOfGroup();
                }

            }
        ]);


    /* GRID */
    let gridUtil = function ($scope, app, vm, uiGridConstants) {
        let rowTemplate = `
        <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" 
            class="ui-grid-cell" 
            ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  
            ui-grid-cell>
        </div>
            `;
        let columnDefTemplates = [];
        return {
            gridOptions: {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: rowTemplate,
                columnDefs: [
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('Resolution'),
                        field: 'resolution.name'
                    },
                    ,
                    {
                        name: app.localize(" "),
                        maxWidth: 50,
                        cellTemplate: `
                            <div class=\"ui-grid-cell-contents text-center\">
                                <button ng-click=\"grid.appScope.removeDisplayGroup(row.entity)\" 
                                    class=\"btn btn-link btn-xs\" title=\"" + app.localize("") + "\">
                                    <i style=\"color:black;font-size:15px;\" class=\"fa fa-minus-circle\"></i>
                                </button>
                            </div>`
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' +
                                sortColumns[0].sort.direction;
                        }

                        vm.getDisplaysOfGroup();
                    });
                    gridApi.pagination.on.paginationChanged($scope,
                        function (pageNumber, pageSize) {
                            requestParams.skipCount = (pageNumber - 1) * pageSize;
                            requestParams.maxResultCount = pageSize;

                            vm.getDisplaysOfGroup();
                        });
                },
                data: []
            }
        };
    };


    function ObjectList() {
        return {
            add: function (myObj) {
                if (myObj != null) {
                    var myIndex = -1;

                    if (this.items != null) {
                        this.items.some(function (value, key) {
                            if (value.id == myObj.id) {
                                myIndex = value;
                                return true;
                            }
                        });
                    }

                    if (myIndex === -1) {
                        this.items.push(myObj);
                    } else {
                        //if (vm.autoSelection == false)
                        //    abp.notify.error(myObj.name + "  " + app.localize("Available"));
                    }
                }
            },
            remove: function (id) {
                for (let i = 0; i < this.items.length; i++) {
                    const obj = this.items[i];

                    if (obj.id == id) {
                        this.items.splice(i, 1);
                    }
                }
            },
            items: []
        }
    }


    let fnc = function ($scope, $state, $modal, uiGridConstants, app, vm, displayGroupService) {

        let act = {
            goBackToList: function () {
                $state.go("tenant.dineplaydisplaygroups", {});
            }
        }
        
        let f = {
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                goBackToList: function () {
                    act.goBackToList()
                },
                saveDisplayGroup: function () {
                    vm.displayGroup.displays = vm.userGridOptions.data;
                    displayGroupService.createOrUpdateDisplayGroup(
                        { DisplayGroup: vm.displayGroup }
                    ).success(
                        function () {
                            act.goBackToList();
                        }
                    );
                },
                getDisplayGroupForEdit: function () {
                    vm.loading = true;
                    displayGroupService.getDisplayGroupForEdit({ id:vm.displayGroupId })
                        .success(function (result) {
                            vm.displayGroup = result.displayGroup;
                            vm.userGridOptions.totalItems = result.displayGroup.displays.length;
                            vm.userGridOptions.data = result.displayGroup.displays;
                        })
                        .finally(function () {
                            vm.loading = false;
                        });
                },
                getDisplaysOfGroup: function () {
                    vm.loading = true;
                    displayGroupService.getDisplaysOfGroup({ id: vm.displayGroupId})
                        .success(function (result) {
                            vm.userGridOptions.totalItems = result.totalCount;
                            vm.userGridOptions.data = result.items;
                        })
                        .finally(function () {
                            vm.loading = false;
                        });
                },
                openDisplaySelectorModal: function () {
                    let modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/play/displays/selectDisplays.cshtml',
                        controller: 'tenant.views.play.display.selector as vm',
                        backdrop: 'static',
                        resolve: {
                            existings: function () {
                                return f.actions.getIds(vm.userGridOptions.data);
                            },
                            multiSelect: function () {
                                return true;
                            }
                        }
                    })

                    modalInstance.result.then(function (result) {
                        if (result && result.length > 0) {
                            for (var i = 0; i < result.length; i++) {
                                let found = -1;
                                vm.userGridOptions.data.some(function (value, key) {
                                    if (result[i].id === value.id) {
                                        found = value;
                                        return true;
                                    }
                                });
                                if (found === -1) {
                                    vm.userGridOptions.data.push(result[i]);
                                }
                            }

                        }

                    });

                },
                getIds: function (items) {
                    let ids = [];
                    for (var i = 0; i < items.length; i++) {
                        ids.push(items[i].id);
                    }
                    return ids;
                },
                removeDisplayGroup: function (obj) {
                    let list = new ObjectList();
                    list.items = vm.userGridOptions.data;
                    list.remove(obj.id);
                }
            }
        };

        return f;
    };


}());
