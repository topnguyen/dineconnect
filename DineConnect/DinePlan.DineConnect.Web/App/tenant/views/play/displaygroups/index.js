﻿(function () {
    appModule.controller("tenant.views.paly.displaygroups",
        [
            "$scope",
            "$state",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.displayGroup",
            function ($scope, $state, $modal, uiGridConstants, displayGroupService) {

                var vm = this;

                let func = fnc($scope, $state, $modal, uiGridConstants, displayGroupService, app, vm);
                func.actions.init(App);

                vm.loading = false;
                vm.leftLoading = false;
                vm.acceptChange = null;
                vm.currentUserId = abp.session.userId;
                vm.isDeleted = false;
                vm.displayGroupFilterText = null;
                vm.displaysOfGroupFilterText = null;

                vm.permissions = {
                    manageGroup: true,
                    manageDisplay: true
                };

                vm.exportToExcel = function () {

                };

                vm.displayGroupId = 1;

                let gridUtl = gridUtil($scope, app, vm, uiGridConstants);

                vm.displayGroupGridOptions = gridUtl.displayGroupGridOptions;
                vm.groupDisplaysGridOptions = gridUtl.groupDisplaysGridOptions;
                vm.openDisplayGroup = func.actions.openDisplayGroup;
                vm.openDisplayGroupForEdit = func.actions.openDisplayGroupForEdit;
                vm.getDisplaysOfGroup = func.actions.getDisplaysOfGroup;
                vm.deleteDisplayGroup = func.actions.deleteDisplayGroup;
                vm.displayGroupSelected = func.actions.displayGroupSelected;

                vm.getDisplayGroups = func.actions.getDisplayGroups;
                vm.getDisplayGroups();

            }
        ]
    );


    let fnc = function ($scope, $state, $modal, uiGridConstants, displayGroupService, app, vm) {

        let act = {
            xxx: function () {

            }
        }

        var requestParams = {
            skipCount: 0,
            maxResultCount: app.consts.grid.defaultPageSize,
            sorting: 'id'
        };

        let f = {
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                openDisplayGroup: function () {
                    $state.go('tenant.dineplaydisplaygroupsdetail', {});
                },
                openDisplayGroupForEdit: function () {
                    if (vm.selectedDisplayGroup.id <= 0) return;
                    $state.go('tenant.dineplaydisplaygroupsdetail', { id: vm.selectedDisplayGroup.id});
                },
                getDisplayGroups() {
                    vm.leftLoading = true;
                    displayGroupService.getAll({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        isDeleted: vm.isDeleted,
                        filter: vm.displayGroupFilterText
                    })
                        .success(function (result) {
                            vm.displayGroupGridOptions.totalItems = result.totalCount;
                            vm.displayGroupGridOptions.data = result.items;
                        })
                        .finally(function () {
                            vm.leftLoading = false;
                        });
                },
                getDisplaysOfGroup: function (obj) {
                    if (obj) {
                        vm.selectedDisplayGroup = obj;
                        vm.displaysOfGroupFilterText = '';
                    }

                    vm.rightLoading = true;
                    displayGroupService.getDisplaysOfGroup({
                        dispalyGroupId: vm.selectedDisplayGroup.id,
                        filter: vm.displaysOfGroupFilterText
                    })
                        .success(function (result) {
                            vm.groupDisplaysGridOptions.totalItems = result.totalCount;
                            vm.groupDisplaysGridOptions.data = result.items;
                        })
                        .finally(function () {
                            vm.rightLoading = false;
                        });
                },
                deleteDisplayGroup: function () {
                    if (vm.selectedDisplayGroup.id <= 0) return;
                    abp.message.confirm(
                        app.localize('DeleteDisplayGroupWarning', vm.selectedDisplayGroup.name),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                displayGroupService.deleteDisplayGroup({ id: vm.selectedDisplayGroup.id })
                                    .success(function (result) {
                                        vm.selectedDisplayGroup = null;
                                        f.actions.getDisplayGroups();
                                    })
                            }
                        }
                    );
                },
                displayGroupSelected: function () {
                    return vm.selectedDisplayGroup && vm.selectedDisplayGroup.id > 0;
                }
            }
        };

        return f;
    };


    let gridUtil = function ($scope, app, vm, uiGridConstants) {
        let rowTemplate = `
        <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" 
            class="ui-grid-cell" 
            ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  
            ui-grid-cell>
        </div>
            `;
        let columnDefTemplates = [
            `
        <div class="ui-grid-cell-contents">" +
            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>
                <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" 
                    aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-cog"></i> ${app.localize("Actions")} <span class="caret"></span>
                </button>
            <ul uib-dropdown-menu>
                    <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.edit"
                        ng-click="grid.appScope.editLocation(row.entity)">${app.localize("Edit")}"</a></li>
                    <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.delete"
                        ng-click="grid.appScope.deleteLocation(row.entity)">${app.localize("Delete")}</a></li>
                    <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.edit"
                        ng-click="grid.appScope.locationcontactLink(row.entity)">${app.localize("Contacts")}</a></li>
                    <li><a ng-if="grid.appScope.isDeleted && grid.appScope.permissions.edit"
                        ng-click="grid.appScope.activateItem(row.entity)">${app.localize("Revert")}</a></li>
            </ul>
            </div>
        </div>`
        ];
        return {
            displayGroupGridOptions: {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: rowTemplate,
                columnDefs: [
                    {
                        name: app.localize("Select"),
                        maxWidth: 100,
                        cellTemplate:`
                            <div class=\"ui-grid-cell-contents text-center\">
                                <button ng-click=\"grid.appScope.getDisplaysOfGroup(row.entity)\" 
                                    class=\"btn btn-default btn-xs\" title=\"" + app.localize("Select") + "\">
                                    <i class=\"fa fa-dashcube\"></i>
                                </button>
                            </div>`
                    },
                    {
                        name: app.localize("Code"),
                        field: "code"
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' +
                                sortColumns[0].sort.direction;
                        }

                        vm.getDisplayGroups();
                    });
                    gridApi.pagination.on.paginationChanged($scope,
                        function (pageNumber, pageSize) {
                            requestParams.skipCount = (pageNumber - 1) * pageSize;
                            requestParams.maxResultCount = pageSize;

                            vm.getDisplayGroups();
                        });
                },
                data: []
            },
            groupDisplaysGridOptions: {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: rowTemplate,
                columnDefs: [
                    {
                        name: app.localize("Name"),
                        field: "name"
                    },
                    {
                        name: app.localize("Resolution"),
                        field: "resolution.name"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        vm.getDisplaysOfGroup();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        vm.getDisplaysOfGroup();
                    });
                },
                data: []
            }
        };
    };



}());