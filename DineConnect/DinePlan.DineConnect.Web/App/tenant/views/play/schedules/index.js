﻿(function () {

    appModule.controller("tenant.views.play.schedules",
        [
            "$scope",
            "$state",
            "$stateParams",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.scheduledEvent",
            "abp.services.app.display",
            "abp.services.app.displayGroup",
            function ($scope, $state, $stateParams, $modal, uiGridConstants, scheduledEventService,
                displayService, displayGroupService) {

                var vm = this;

                vm.loading = false;
                vm.isDeleted = false;

                let func = fnc($scope, $state, $modal, uiGridConstants, app, vm, scheduledEventService,
                    displayService, displayGroupService);
                func.actions.init(App);

                vm.calendarMonthViewTitle = func.actions.getMonthViewTitle();
                vm.createEvent = func.actions.createEvent;

                vm.calendarEvents = [];
                vm.calendarEventSource = [vm.calendarEvents];
                vm.selectedEvent = null;
                vm.useCalendarView = true;
                vm.userGridOptions = gridUtil($scope, app, vm, uiGridConstants).gridOptions;

                $('input[name="eventStartDateRangePicker"]').daterangepicker({
                    showDropdowns: true,
                    minYear: 1901,
                    maxYear: parseInt(moment().format('YYYY'), 10),
                    autoApply: true
                });

                vm.scheduledEventDateRange = null;
                vm.scheduledEventDateRangeChanged = func.actions.scheduledEventDateRangeChanged;

                vm.changeCalendarView = func.actions.changeCalendarView;
                vm.calendarNext = func.actions.calendar.next;
                vm.calendarPrev = func.actions.calendar.prev;
                vm.calendarToday = func.actions.calendar.gotoToday;

                vm.displays = [];
                vm.displayGroups = [];
                func.actions.getDisplays();
                func.actions.getDisplayGroups();

                vm.uiConfig = func.uiConfig;

                vm.navSrc = $stateParams.src;

                vm.datePicker = {};
                vm.datePicker.dateOptions = func.uiConfig.datePicker.dateOptions;
                vm.datePicker.gotoDate = func.actions.calendar.gotoDate;
                vm.datePicker.opened = false;
                vm.datePicker.format = 'dd-MMMM-yyyy';
                vm.datePicker.currentDate;
                vm.datePicker.open = func.actions.datePicker.open; 

                vm.eventFilter = {};
                vm.eventFilter.selection = func.lists.eventFilterOptions[0];
                vm.eventFilter.select = func.actions.selectFilterType;
                vm.eventFilter.value = null;

                vm.scheduledEvents = [];
                vm.getScheduledEvents = func.actions.getScheduledEvents;
                vm.getScheduledEvents();
                vm.permissions = func.actions.readPermission(abp);

                vm.openEventForEdit = func.actions.openEventForEdit;
                vm.deleteScheduledEvent = func.actions.deleteScheduledEvent;
                if (vm.navSrc === 'LIS') func.actions.changeCalendarView("customlist");
            }
        ]);


    function ObjectList() {
        return {
            add: function (myObj) {
                if (myObj != null) {
                    var myIndex = -1;

                    if (this.items != null) {
                        this.items.some(function (value, key) {
                            if (value.id == myObj.id) {
                                myIndex = value;
                                return true;
                            }
                        });
                    }

                    if (myIndex === -1) {
                        this.items.push(myObj);
                    } else {
                        //if (vm.autoSelection == false)
                        //    abp.notify.error(myObj.name + "  " + app.localize("Available"));
                    }
                }
            },
            remove: function (id) {
                for (let i = 0; i < this.items.length; i++) {
                    const obj = this.items[i];

                    if (obj.id == id) {
                        this.items.splice(i, 1);
                    }
                }
            },
            items: []
        }
    }


    let gridUtil = function ($scope, app, vm, uiGridConstants) {
        let rowTemplate = `
        <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" 
            class="ui-grid-cell" 
            ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  
            ui-grid-cell>
        </div>
            `;
        let columnDefTemplates = [
            `
        <div class="ui-grid-cell-contents">
            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>
                <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" 
                    aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-cog"></i> ${app.localize("Actions")} <span class="caret"></span>
                </button>
            <ul uib-dropdown-menu>
                    <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.edit"
                        ng-click="grid.appScope.openEventForEdit(row.entity.id,'LIS')">${app.localize("Edit")}</a></li>
                    <li><a ng-if="!grid.appScope.isDeleted && grid.appScope.permissions.delete"
                        ng-click="grid.appScope.deleteScheduledEvent(row.entity)">${app.localize("Delete")}</a></li>
            </ul>
            </div>
        </div>`
        ];
        return {
            gridOptions: {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: rowTemplate,
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate: columnDefTemplates[0]
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('Start Date'),
                        field: 'startTime'
                    },
                    {
                        name: app.localize('Layout'),
                        field: 'layout.name'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' +
                                sortColumns[0].sort.direction;
                        }

                        vm.getScheduledEvents();
                    });
                    gridApi.pagination.on.paginationChanged($scope,
                        function (pageNumber, pageSize) {
                            requestParams.skipCount = (pageNumber - 1) * pageSize;
                            requestParams.maxResultCount = pageSize;

                            vm.getScheduledEvents();
                        });
                },
                data: []
            }
        };
    };


    let fnc = function ($scope, $state, $modal, uiGridConstants, app, vm, scheduledEventService,
        displayService, displayGroupService) {

        let act = {
            xxx: function () {

            }
        }

        // let eventCalendar = $('#calendar');

        let requestParams = {
            scheduledEvents: {
                skipCount: 0,
                maxResultCount: 500,
                sorting: 'id'
            },
            displays: {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: 'id'
            },
            displaysGroups: {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: 'id'
            }
        };
        
        let f = {
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                getScheduledEvents: function () {
                    vm.loading = true;
                    let filterText = '';
                    if (vm.eventFilter.value) {
                        filterText = vm.eventFilter.value.name;
                    }

                    let startDateFrom = null;
                    let startDateTo = null;

                    if (!vm.useCalendarView &&
                        vm.scheduledEventDateRange &&
                        vm.scheduledEventDateRange != '') {
                        let from = vm.scheduledEventDateRange.split('-')[0];
                        let to = vm.scheduledEventDateRange.split('-')[1];
                        startDateFrom = new Date(from.trim());
                        startDateTo = new Date(to.trim());
                    }

                    scheduledEventService.getAll({
                        skipCount: requestParams.scheduledEvents.skipCount,
                        maxResultCount: requestParams.scheduledEvents.maxResultCount,
                        sorting: requestParams.scheduledEvents.sorting,
                        filter: filterText,
                        isDeleted: false,
                        filteringOption: vm.eventFilter.selection.value,
                        startDateFrom: moment(startDateFrom).format('YYYY-MM-DD HH:mm'),
                        startDateTo: moment(startDateTo).format('YYYY-MM-DD HH:mm')
                    })
                    .success(function (result) {
                        vm.calendarEvents.splice(0, vm.calendarEvents.length)
                        result.items.forEach(function (e) {
                            vm.calendarEvents.push(
                                {
                                    id: e.id,
                                    title: e.name,
                                    start: e.startTime,
                                    end: e.endTime
                                }
                            );
                        });
                        vm.userGridOptions.totalItems = result.totalCount;
                        vm.userGridOptions.data = result.items;
                    }).finally(function () {
                        vm.loading = false;
                    });
                },
                createEvent: function () {
                    $state.go("tenant.dineplayscheduleevent", {});
                },
                eventSelected: function (event) {
                    vm.selectedEvent = event;
                    f.actions.openEventForEdit(event.id,'CAL');
                },
                openEventForEdit: function (id,src) {
                    $state.go("tenant.dineplayscheduleevent", { id: id,src:src });
                },
                changeCalendarView: function (view) {
                    if (view === 'customlist') {
                        vm.useCalendarView = false;
                    } else {
                        vm.useCalendarView = true;
                    }
                    vm.scheduledEventDateRange = null;
                    f.uiConfig.calendar.defaultView = view;
                    f.actions.getScheduledEvents();
                },
                getMonthViewTitle: function (calendarDate) {
                    let tDatePart = new Date().toDateString().split(' ');
                    if (calendarDate) tDatePart = calendarDate.toDateString().split(' ');
                    let title = tDatePart[1] + ' ' + tDatePart[3];
                    return title;
                },
                calendar: {
                    next: function () {
                        // eventCalendar.fullCalendar('next');
                        $('#calendar').fullCalendar('next');
                        let tDate = $('#calendar').fullCalendar('getDate')._d;
                        vm.calendarMonthViewTitle = f.actions.getMonthViewTitle(tDate);
                        f.actions.getScheduledEvents();
                        f.actions.getScheduledEvents();
                    },
                    prev: function () {
                        $('#calendar').fullCalendar('prev');
                        let tDate = $('#calendar').fullCalendar('getDate')._d;
                        vm.calendarMonthViewTitle = f.actions.getMonthViewTitle(tDate);
                        f.actions.getScheduledEvents();
                    },
                    gotoToday: function () {
                        $('#calendar').fullCalendar('gotoDate', new Date());
                        let tDate = $('#calendar').fullCalendar('getDate')._d;
                        vm.calendarMonthViewTitle = f.actions.getMonthViewTitle(tDate);
                        f.actions.getScheduledEvents();
                    },
                    gotoDate: function () {
                        $('#calendar').fullCalendar('gotoDate', vm.datePicker.currentDate);
                        let tDate = $('#calendar').fullCalendar('getDate')._d;
                        vm.calendarMonthViewTitle = f.actions.getMonthViewTitle(tDate);
                        f.actions.getScheduledEvents();
                    }
                },
                datePicker: {
                    open: function () {
                        vm.datePicker.opened = true;
                    }
                },
                selectFilterType: function (filterType) {
                    vm.eventFilter.selection = f.lists.eventFilterOptions
                        .find(function (o) { return o.value === filterType });
                    if (filterType == 'DSP') {
                        vm.filterings = vm.displays;
                    } else if (filterType == 'GRP') {
                        vm.filterings = vm.displayGroups;
                    } else if (filterType == 'ALL') {
                        vm.filterings = null;
                    }
                    setTimeout(function () {
                        $('#ddFilter').selectpicker('refresh');
                    }, 500);                    
                },
                getDisplays: function () {
                    displayService.getAll({

                        skipCount: requestParams.displays.skipCount,
                        maxResultCount: requestParams.displays.maxResultCount,
                        sorting: requestParams.displays.sorting,
                        filter: '',
                        isDeleted: false

                    }).success(function (result) {
                        vm.displays = result.items;
                        //console.log(vm.displays);
                        });
                },
                getDisplayGroups: function () {
                    displayGroupService.getAll({
                        skipCount: requestParams.displaysGroups.skipCount,
                        maxResultCount: requestParams.displaysGroups.maxResultCount,
                        sorting: requestParams.displaysGroups.sorting,
                        filter: '',
                        isDeleted: false
                    }).success(function (result) {
                        vm.displayGroups = result.items;
                        //vm.ddg = [];
                        //result.items.forEach(function (g) {
                        //    vm.ddg.push({name:g.name});
                        //});
                        //console.log(vm.displayGroups);
                    });
                },
                scheduledEventDateRangeChanged: function () {
                    f.actions.getScheduledEvents();
                },
                readPermission: function (abp) {
                    return {
                        create: abp.auth.hasPermission('Pages.Tenant.Play.Schedule.Event.Create'),
                        edit: abp.auth.hasPermission('Pages.Tenant.Play.Schedule.Event.Edit'),
                        delete: abp.auth.hasPermission('Pages.Tenant.Play.Schedule.Event.Delete')
                    }
                },
                deleteScheduledEvent: function (obj) {
                    abp.message.confirm(
                        app.localize('DeleteScheduledEventWarning', obj.name),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                scheduledEventService.deleteScheduledEvent({ id: obj.id })
                                    .success(function (result) {
                                        abp.notify.success(app.localize('SuccessfullyDeleted'));
                                        f.actions.goBackToList();
                                    });
                            }
                        }
                    )
                }
            },
            uiConfig: {
                calendar: {
                    header: false,
                    displayEventTime: false,
                    eventClick: function (event) {
                        f.actions.eventSelected(event);
                    },
                    defaultView: 'month'
                },
                datePicker: {
                    dateOptions: {
                        formatYear: 'yy',
                        maxDate: new Date(2030, 5, 22),
                        minDate: new Date(),
                        startingDay: 1
                    }
                }
            },
            lists: {
                eventFilterOptions: [
                    { value: 'ALL', text: app.localize('All')},
                    { value: 'DSP', text: app.localize('Display') },
                    { value: 'GRP', text: app.localize('DisplayGroup') },
                ]
            }
        };

        return f;
    };


}());
