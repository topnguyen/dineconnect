﻿(function () {
    appModule.controller("tenant.views.play.schedule.event",
        [
            "$scope",
            "$state",
            "$stateParams",
            "$uibModal",
            "uiGridConstants",
            "abp.services.app.scheduledEvent",
            "abp.services.app.displayGroup",
            "abp.services.app.display",
            "abp.services.app.daypart",
            "abp.services.app.layout",
            function ($scope, $state, $stateParams, $modal, uiGridConstants,
                scheduledEventService, displayGroupService,
                displayService, daypartService, layoutService
            ) {

                var vm = this;

                vm.loading = false;

                let func = fnc($scope, $state, $modal, uiGridConstants, app, vm,
                    scheduledEventService, displayGroupService, displayService,
                    daypartService, layoutService);
                func.actions.init(App);
                func.actions.initScheduledEvent();

                vm.save = func.actions.saveScheduledEvent;
                vm.cancel = func.actions.goBackToList;

                vm.app = app;

                vm.scheduledEventId = $stateParams.id;
                vm.navSrc = $stateParams.src;
                if (vm.scheduledEventId && vm.scheduledEventId > 0) {
                    vm.workMode = 'EDIT';
                } else {
                    vm.workMode = 'ADD';
                }
                vm.timeSelHours = moduleData.getTimeSelOptions('H');
                vm.timeSelMinutes = moduleData.getTimeSelOptions('M');
                vm.repeatTypes = moduleData.getRepeatTypes();
                vm.daysOfWeek = moduleData.getDaysOfWeek();
                vm.timeUnit = moduleData.getTimeUnit;
                vm.updateSelectedUntilDateTime = func.actions.updateSelectedUntilDateTime;
                vm.updateSelectedStartDateTime = func.actions.updateSelectedStartDateTime;
                vm.updateSelectedEndDateTime = func.actions.updateSelectedEndDateTime;
                vm.validateScheduledEvent = func.actions.validateScheduledEvent;
                vm.displayChanged = func.actions.displayChanged;
                vm.displayGroupChanged = func.actions.displayGroupChanged;
                vm.daypartChanged = func.actions.daypartChanged;
                vm.layoutChanged = func.actions.layoutChanged;
                vm.repeatTypeChanged = func.actions.repeatTypeChanged;
                vm.weekdayChanged = func.actions.weekdayChanged;
                vm.isChild = func.actions.isChild;

                vm.displayGroups = [];
                vm.displays = [];
                vm.dayparts = [];
                vm.layouts = [];

                vm.displayOption = 'DSP'
                let tDate = new Date();
                
                tDate.setTime(tDate.getTime() + (0.25 * 60 * 60 * 1000));
                vm.startDate = new Date(tDate.getFullYear(),tDate.getMonth(),tDate.getDate());
                vm.startHour = tDate.getHours();
                vm.startMinute = tDate.getMinutes();
                tDate.setTime(tDate.getTime() + (1 * 60 * 60 * 1000));
                vm.endDate = new Date(tDate.getFullYear(), tDate.getMonth(), tDate.getDate());
                vm.endHour = tDate.getHours();
                vm.endMinute = tDate.getMinutes();
                vm.untilDate = null;
                vm.selectedDisplayGroup = null;
                vm.selectedDisplay = null;
                vm.selectedDaypart = null;
                vm.selectedLayout = null;
                vm.selectedRepeatType = vm.repeatTypes[0];
                vm.selectedDaysOfWeek = [];
                vm.delete = func.actions.deleteScheduledEvent;
                vm.scheduledEvent.hasChildren = false;
                vm.scheduledEvent.parentId = null;

                func.actions.getDisplayGroups.then(function () {
                    func.actions.getDisplays.then(function () {
                        func.actions.getDayparts.then(function () {

                            func.actions.updateSelectedStartDateTime();
                            func.actions.updateSelectedEndDateTime();

                            func.actions.getLayouts.then(function (result, error) {
                                if (vm.workMode == 'EDIT') {
                                    func.actions.getScheduledEventForEdit();
                                }
                            });
                        });
                    });
                });
                
                vm.openDisplaySelectorModal = func.actions.openDisplaySelectorModal;
                vm.openDisplayGroupSelectorModal = func.actions.openDisplayGroupSelectorModal;
                vm.openDaypartSelectorModal = func.actions.openDaypartSelectorModal;
                vm.openLayoutSelectorModal = func.actions.openLayoutSelectorModal;
                vm.isChildOrParent = func.actions.isChildOrParent;

                vm.scheduledEvent.priority = 1;
            }
        ]);

    let moduleData = {
        getTimeSelOptions: function (option) {
            let hrsMnts = [];
            let ceiling = option.toUpperCase() === 'H' ? 23 : 59;
            for (let i = 0; i <= ceiling; i++) { hrsMnts.push(i) };
            return hrsMnts;
        },
        getRepeatTypes: function () {
            let repeatTypes = [
                { id: 0, timeUnit:'', value:'none',text: 'None' },
                { id: 1, timeUnit:'minute', value:'perminute',text: 'Per Minute' },
                { id: 2, timeUnit:'hour', value:'hourly',text: 'Hourly' },
                { id: 3, timeUnit:'day', value:'daily',text: 'Daily' },
                { id: 4, timeUnit:'week', value:'weekly',text: 'Weekly' },
                { id: 5, timeUnit:'month', value:'monthly',text: 'Monthly' },
                { id: 6, timeUnit:'year', value:'yearly',text: 'Yearly' }
            ];
            return repeatTypes;
        },
        getDaysOfWeek: function () {
            let days = [
                { id: 0, value:'sun', text: 'Sunday' },
                { id: 1, value:'mon', text: 'Monday' },
                { id: 2, value:'tue', text: 'Tuesday' },
                { id: 3, value:'wed', text: 'Wednesday' },
                { id: 4, value:'thu', text: 'Thursday' },
                { id: 5, value:'fri', text: 'Friday' },
                { id: 6, value:'sat', text: 'Saturday' }
            ];
            return days;
        },
        getTimeUnit: function (selRepeatType) {
            let units = ['', 'Minute', 'Hour', 'Day', 'Week', 'Month', 'Year'];
            return units[selRepeatType];
        }
    }


    let fnc = function ($scope, $state, $modal, uiGridConstants, app, vm, scheduledEventService,
        displayGroupService, displayService, daypartService, layoutService) {

        let act = {
            goBackToList: function () {
                $state.go("tenant.dineplayschedules", {src:vm.navSrc});
            },
            getSelectedWeekdaysCsv: function (opt) {
                let weekdays = "";
                let option = opt ? opt : 'value';
                vm.selectedDaysOfWeek.forEach(function (wd) {
                    weekdays += (weekdays == '' ? '' : ',') + (option==='value'? wd.value:wd.text);
                });
                return weekdays;
            },
            findSelectedObject: function (id, list) {
                let found = list.find(function (o) { return o.id === id });
                return found;
            },
            findSelectedRepeatType: function (repeatType) {
                let found = vm.repeatTypes.find(function (t) { return t.value === repeatType });
                return found;
            },
            findSelectedWeekdays: function (selWeekdaysCsv) {
                let allWeekdays = moduleData.getDaysOfWeek();
                let selWeekdays = selWeekdaysCsv.split(',');
                let selWeekdaysObj = [];
                selWeekdays.forEach(function (wd) {
                    let found = allWeekdays.find(function (d) { return d.value === wd });
                    if (found) {
                        selWeekdaysObj.push(found);
                    }
                });
                // console.log("Weekdays");
                // console.log(selWeekdaysObj);
                return selWeekdaysObj;
            }
        }

        let getDisplayGroupsRequestParams = {
            skipCount: 0,
            maxResultCount: app.consts.grid.defaultPageSize,
            sorting: 'name'
        };

        let getDisplaysRequestParams = {
            skipCount: 0,
            maxResultCount: app.consts.grid.defaultPageSize,
            sorting: 'id'
        };

        let getDaypartsRequestParams = {
            skipCount: 0,
            maxResultCount: app.consts.grid.defaultPageSize,
            sorting: 'id'
        };

        let getLayoutsRequestParams = {
            skipCount: 0,
            maxResultCount: app.consts.grid.defaultPageSize,
            sorting: 'id'
        };

        let f = {
            actions: {
                init: function (App) {
                    $scope.$on("$viewContentLoaded", function () {
                        App.initAjax();
                    });
                },
                initScheduledEvent: function () {
                    vm.scheduledEvent = {
                        id: 0,
                        name: null,
                        displayOption: '',
                        displayId: 0,
                        displayGroupId: 0,
                        daypartId: 0,
                        startTime: null,
                        endTime: null,
                        //startHour: 0,
                        //startMinute: 0,
                        //endHour: 0,
                        //endMinute: 0,
                        layoutId: 0,
                        priority: 0,
                        repeatType: 'none',
                        repeatInterval: 0,
                        repeatTimeUnit: null,
                        repeatWeekdays: null,
                        repeatUntil: null
                    }
                },
                goBackToList: function () {
                    act.goBackToList()
                },
                saveScheduledEvent: function (saveAllChildren) {
                    //console.log(JSON.stringify(vm.scheduledEvent));
                    vm.loading = true;
                    vm.scheduledEvent.displayOption = vm.displayOption;
                    vm.scheduledEvent.repeatWeekdays = act.getSelectedWeekdaysCsv();
                    if (!vm.scheduledEvent.repeatType) f.actions.repeatTypeChanged();
                    // console.log(vm.scheduledEvent);
                    scheduledEventService.createOrUpdateScheduledEvent(
                        { ScheduledEvent: vm.scheduledEvent, updateAllChildren: saveAllChildren }
                    ).success(
                        function () {
                            act.goBackToList();
                        }
                    ).finally(function () {
                        vm.loading = false;
                    });
                },
                getScheduledEventForEdit: function () {
                    vm.loading = true;
                    scheduledEventService.getScheduledEventForUpdate({ id: vm.scheduledEventId })
                        .success(function (result) {
                            // console.log(result);

                            vm.scheduledEvent.id = result.scheduledEvent.id;
                            vm.scheduledEvent.name = result.scheduledEvent.name;
                            vm.scheduledEvent.hasChildren = result.scheduledEvent.hasChildren;
                            vm.scheduledEvent.parentId = result.scheduledEvent.parentId;
                            // console.log('IsChild ' + vm.isChild());
                            if (result.scheduledEvent.displayId) {
                                vm.displayOption = 'DSP'
                                vm.selectedDisplay = act.findSelectedObject(result.scheduledEvent.displayId, vm.displays);
                            } else if (result.scheduledEvent.displayGroupId) {
                                vm.displayOption = 'GRP'
                                vm.selectedDisplayGroup = act.findSelectedObject(result.scheduledEvent.displayGroupId, vm.displayGroups);
                            }
                            
                            vm.scheduledEvent.displayId = result.scheduledEvent.displayId;
                            vm.selectedLayout = act.findSelectedObject(result.scheduledEvent.layoutId, vm.layouts);
                            if (!result.scheduledEvent.daypartId) {
                                vm.selectedDaypart = vm.dayparts[0];
                            } else {
                                vm.selectedDaypart = act.findSelectedObject(result.scheduledEvent.daypartId, vm.dayparts);
                            }
                            vm.scheduledEvent.priority = result.scheduledEvent.priority;
                            vm.selectedRepeatType = act.findSelectedRepeatType(result.scheduledEvent.repeatType);
                            vm.scheduledEvent.repeatInterval = result.scheduledEvent.repeatInterval;

                            if (result.scheduledEvent.repeatWeekdays) {
                                vm.selectedDaysOfWeek = act.findSelectedWeekdays(result.scheduledEvent.repeatWeekdays);
                                vm.scheduledEvent.repeatWeekdays = act.getSelectedWeekdaysCsv();
                                setTimeout(function () {
                                    $('.selectpicker').selectpicker('refresh');
                                    $('#weekdaysSelector > div > button > span.filter-option.pull-left').text(act.getSelectedWeekdaysCsv('text'));
                                }, 500);
                            }

                            if (result.scheduledEvent.repeatUntil) {
                                let tDate = result.scheduledEvent.repeatUntil;
                                let tDate2 = new Date(result.scheduledEvent.repeatUntil);
                                vm.untilDate = new Date(tDate.split('T')[0]);
                                vm.untilHour = tDate2.getHours();
                                vm.untilMinute = tDate2.getMinutes();
                            }
                            if (result.scheduledEvent.startTime) {
                                let tDate = result.scheduledEvent.startTime;
                                let tDate2 = new Date(result.scheduledEvent.startTime);
                                vm.startDate = new Date(tDate.split('T')[0]);
                                vm.startHour = tDate2.getHours();
                                vm.startMinute = tDate2.getMinutes();
                            }
                            if (result.scheduledEvent.endTime) {
                                let tDate = result.scheduledEvent.endTime;
                                let tDate2 = new Date(result.scheduledEvent.endTime);
                                vm.endDate = new Date(tDate.split('T')[0]);
                                vm.endHour = tDate2.getHours();
                                vm.endMinute = tDate2.getMinutes();
                            }
                            f.actions.updateSelectedStartDateTime();
                            f.actions.updateSelectedEndDateTime();
                            f.actions.updateSelectedUntilDateTime();
                            if (result.scheduledEvent.displayId) f.actions.displayChanged();
                            if (result.scheduledEvent.displayGroupId) f.actions.displayGroupChanged();
                            if (result.scheduledEvent.daypartId) f.actions.daypartChanged();
                            if (result.scheduledEvent.layoutId) f.actions.layoutChanged();
                            f.actions.repeatTypeChanged();
                        })
                        .finally(function () {
                            vm.loading = false;
                        });
                },
                getDisplayGroups: new Promise(function (resolve, reject) {
                    displayGroupService.getAll({
                        skipCount: getDisplayGroupsRequestParams.skipCount,
                        maxResultCount: getDisplayGroupsRequestParams.maxResultCount,
                        sorting: getDisplayGroupsRequestParams.sorting,
                        filter: vm.filterText,
                        isDeleted: vm.isDeleted
                    })
                        .success(function (result) {
                            vm.displayGroups = result.items;
                            resolve();
                        }).error(function () { reject() })
                }),
                displayGroupChanged: function () {
                    vm.scheduledEvent.displayGroupId = vm.selectedDisplayGroup.id;
                },
                getDisplays: new Promise(function (resolve, reject) {
                    displayService.getAll({
                        skipCount: getDisplaysRequestParams.skipCount,
                        maxResultCount: getDisplaysRequestParams.maxResultCount,
                        sorting: getDisplaysRequestParams.sorting,
                        filter: vm.filterText,
                        isDeleted: vm.isDeleted
                    }).success(function (result) {
                        vm.displays = result.items;
                        resolve();
                    }).error(function () { reject() });
                }),
                weekdayChanged: function () {
                    vm.scheduledEvent.repeatWeekdays = act.getSelectedWeekdaysCsv();
                },
                displayChanged: function () {
                    vm.scheduledEvent.displayId = vm.selectedDisplay.id;
                },
                getDayparts: new Promise(function (resolve, reject) {
                    daypartService.getAll({
                        skipCount: getDaypartsRequestParams.skipCount,
                        maxResultCount: getDaypartsRequestParams.maxResultCount,
                        sorting: getDaypartsRequestParams.sorting,
                        filter: vm.filterText,
                        isDeleted: vm.isDeleted
                    }).success(function (result) {
                        vm.dayparts = [];
                        vm.dayparts.push({ id: 0, name: 'Custom' })
                        result.items.forEach(function (l) {
                            vm.dayparts.push({ id: l.id, name: l.name })
                        })
                        vm.selectedDaypart = vm.dayparts[0];
                        resolve();
                    }).error(function () { reject()});
                }),
                daypartChanged: function () {
                    vm.scheduledEvent.daypartId = vm.selectedDaypart.id;
                    vm.startHour = vm.selectedDaypart.startHour;
                    vm.startMinute = vm.selectedDaypart.startMinute;
                },
                getLayouts: new Promise( function (resolve, reject) {
                    layoutService.getAll({
                        skipCount: getLayoutsRequestParams.skipCount,
                        maxResultCount: getLayoutsRequestParams.maxResultCount,
                        sorting: getLayoutsRequestParams.sorting,
                        filter: vm.filterText,
                        isDeleted: vm.isDeleted
                    }).success(function (result) {
                        vm.layouts = result.items;
                        resolve();
                    });
                }),
                layoutChanged: function () {
                    vm.scheduledEvent.layoutId = vm.selectedLayout.id;
                },
                updateSelectedStartDateTime: function () {
                    try {
                        /*
                        let h = vm.selectedDaypart.id === 0 ? vm.startHour : 1;
                        let m = vm.selectedDaypart.id === 0 ? vm.startMinute : 0;
                        */
                        let h = vm.startHour;
                        let m = vm.startMinute;
                        let tDate = new Date(
                            vm.startDate.getFullYear(),
                            vm.startDate.getMonth(),
                            vm.startDate.getDate(),
                            h,
                            m);
                        vm.scheduledEvent.startTime = moment(tDate).format('YYYY-MM-DD HH:mm');
                    } catch (e) {
                        vm.scheduledEvent.startTime = null;
                    }
                    // console.log(vm.scheduledEvent.startTime);
                    //console.log(tDate.getTimezoneOffset());
                },
                updateSelectedEndDateTime: function () {
                    try {
                        let h = vm.selectedDaypart.id === 0 ? vm.endHour : 1;
                        let m = vm.selectedDaypart.id === 0 ? vm.endMinute : 0;
                        let tDate = new Date(
                            vm.endDate.getFullYear(),
                            vm.endDate.getMonth(),
                            vm.endDate.getDate(),
                            h,
                            m)
                        vm.scheduledEvent.endTime = moment(tDate).format('YYYY-MM-DD HH:mm');
                    } catch (e) {
                        vm.scheduledEvent.endTime = null;
                    }
                    // console.log(vm.scheduledEvent.endTime);
                },
                updateSelectedUntilDateTime: function () {
                    try {
                        let h = vm.untilHour ? vm.untilHour : 0;
                        let m = vm.untilMinute ? vm.untilMinute : 0;
                        let tDate = new Date(
                            vm.untilDate.getFullYear(),
                            vm.untilDate.getMonth(),
                            vm.untilDate.getDate(),
                            h,
                            m);
                        vm.scheduledEvent.repeatUntil = moment(tDate).format('YYYY-MM-DD HH:mm');
                    } catch (e) {
                        vm.scheduledEvent.repeatUntil = null;
                    }
                    // console.log(vm.scheduledEvent.repeatUntil);
                },
                validateScheduledEvent: function () {
                    let today = new Date();

                    if (!vm.scheduledEvent.name ||
                        vm.scheduledEvent.name === '') return app.localize('NameIsRequired');

                    if (vm.displayOption === 'DSP' &&
                        (!vm.scheduledEvent.displayId ||
                            vm.scheduledEvent.displayId < 1 ||
                            vm.scheduledEvent.displayId === '')) return app.localize('DisplayIsRequired');

                    if (vm.displayOption === 'GRP' &&
                        (!vm.scheduledEvent.displayGroupId ||
                            vm.scheduledEvent.displayGroupId < 1 ||
                            vm.scheduledEvent.displayGroupId === '')) return app.localize('DisplayGroupIsRequired');

                    if (!vm.scheduledEvent.startTime) return app.localize('StartTimeIsRequired');
                    if (new Date(vm.scheduledEvent.startTime) < today && vm.workMode == 'ADD') return app.localize('InvalidStartDate');

                    if (vm.scheduledEvent.daypartId === 0) {
                        if (!vm.scheduledEvent.endTime) return app.localize('EndTimeIsRequired');
                        if (new Date(vm.scheduledEvent.endTime) < today) return app.localize('InvalidEndDate');
                        if (new Date(vm.scheduledEvent.endTime) < new Date(vm.scheduledEvent.startTime))
                            return app.localize('EndDateIsEarlierThanStartDate');
                    }

                    if (vm.scheduledEvent.layoutId <= 0) return app.localize('LayoutIsRequired');

                    if (vm.scheduledEvent.repeatType != 'none' &&
                        !vm.scheduledEvent.repeatUntil) return app.localize('RepeatEndDateIsRequired');

                    if (vm.scheduledEvent.repeatType != 'none' &&
                        (vm.scheduledEvent.repeatInterval <= 0)) return app.localize('NumberOfRepetitionIsRequired');

                    if (vm.scheduledEvent.repeatType === 'weekly' &&
                        (!vm.scheduledEvent.repeatWeekdays ||
                            vm.scheduledEvent.repeatWeekdays.length < 3)) return app.localize('DayOfWeekIsRequired');

                    return '';
                },
                repeatTypeChanged: function () {
                    vm.scheduledEvent.repeatType = vm.selectedRepeatType.value;
                    vm.scheduledEvent.repeatTimeUnit = vm.selectedRepeatType.timeUnit;
                    // console.log(vm.scheduledEvent.repeatType);
                },
                deleteScheduledEvent: function (deleteAllChildren) {
                    abp.message.confirm(
                        app.localize('DeleteLocationWarning', vm.scheduledEvent.name),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                scheduledEventService.deleteScheduledEvent(
                                    {
                                        id: vm.scheduledEventId,
                                        deleteAllChildren: deleteAllChildren
                                    })
                                    .success(function (result) {
                                        abp.notify.success(app.localize('SuccessfullyDeleted'));
                                        f.actions.goBackToList();
                                    });
                            }
                        }
                    )
                },
                openDisplaySelectorModal: function () {

                    let modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/play/displays/selectDisplays.cshtml',
                        controller: 'tenant.views.play.display.selector as vm',
                        backdrop: 'static',
                        resolve: {
                            existings: function () {
                                return [];
                            },
                            multiSelect: function () {
                                return false;
                            }
                        }
                    })

                    modalInstance.result.then(function (result) {
                        if (result && result.length > 0) {
                            vm.selectedDisplay = result[0];
                            f.actions.displayChanged();
                        }
                    });

                },
                openDisplayGroupSelectorModal: function () {

                    let modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/play/displaygroups/selectDisplayGroups.cshtml',
                        controller: 'tenant.views.play.displaygroup.selector as vm',
                        backdrop: 'static',
                        resolve: {
                            existings: function () {
                                return [];
                            },
                            multiSelect: function () {
                                return false;
                            }
                        }
                    })

                    modalInstance.result.then(function (result) {
                        if (result && result.length > 0) {
                            for (var i = 0; i < result.length; i++) {
                                vm.selectedDisplayGroup = result[0];
                                f.actions.displayGroupChanged();
                            }

                        }

                    });

                },
                openDaypartSelectorModal: function () {

                    let modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/play/dayparting/selectDaypart.cshtml',
                        controller: 'tenant.views.play.daypart.selector as vm',
                        backdrop: 'static',
                        resolve: {
                            existings: function () {
                                return [];
                            },
                            multiSelect: function () {
                                return false;
                            }
                        }
                    })

                    modalInstance.result.then(function (result) {
                        if (result && result.length > 0) {
                            vm.selectedDaypart = result[0];
                            f.actions.daypartChanged();
                        }

                    });

                },
                openLayoutSelectorModal: function () {

                    let modalInstance = $modal.open({
                        templateUrl: '~/App/tenant/views/play/layouts/selectLayout.cshtml',
                        controller: 'tenant.views.play.layout.selector as vm',
                        backdrop: 'static',
                        resolve: {
                            existings: function () {
                                return [];
                            },
                            multiSelect: function () {
                                return false;
                            }
                        }
                    })

                    modalInstance.result.then(function (result) {
                        if (result && result.length > 0) {
                            vm.selectedLayout = result[0];
                            f.actions.layoutChanged();
                        }

                    });

                },
                isChild: function () {
                    return ((!vm.scheduledEvent.parentId?false:true) && vm.scheduledEvent.parentId >0)
                },
                isChildOrParent: function () {
                    return vm.scheduledEvent.hasChildren || vm.scheduledEvent.parentId
                }
            }
        };

        return f;
    };
}());
