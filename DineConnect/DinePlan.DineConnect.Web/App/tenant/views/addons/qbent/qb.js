﻿

(function () {
    appModule.controller('tenant.views.addons.qbent', [
        '$scope', '$state', '$http', '$uibModal', 'uiGridConstants', 'abp.services.app.quickBooks',
        function ($scope, $state, $http, $modal, uiGridConstants, qbService) {
            var vm = this;
            vm.isConnected = false;
            vm.filterText = null;
            vm.loading = false;


            vm.connect = function () {
                vm.loading = true;
                qbService.enableOrDisableQbEnterprise(true).success(function (result) {
                    vm.isConnected = result;
                }).finally(function () {
                    vm.loading = false;
                });
            }
            vm.disconnect = function () {
                vm.loading = true;
                qbService.enableOrDisableQbEnterprise(false).success(function (result) {
                    vm.isConnected = result;
                }).finally(function () {
                    vm.loading = false;
                });
            }

            

            vm.checkConnected = function () {
                vm.loading = true;
                qbService.checkQbEnterprise({
                }).success(function (result) {
                    vm.isConnected = result;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.checkConnected();

        }]);
})();


