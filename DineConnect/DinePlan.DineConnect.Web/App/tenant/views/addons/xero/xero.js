﻿(function() {
    appModule.controller("tenant.views.addons.xero",
        [
            "$scope", "$state", "$http", "abp.services.app.xero",'abp.services.app.connectLookup',
            function($scope, $state, $http, xeroService,lookUpService) {

                var vm = this;
                vm.xero = [];
                vm.createSeparateInvoice = false;

                vm.loginUrl = "";

                xeroService.isTokenExists().success(function(result) {
                    vm.isConnected = result;
                    vm.refresh();
                }).finally(function() {
                    vm.loading = false;
                });


                if (!vm.isConnected) {
                    xeroService.getLoginUrl().success(function(result) {
                        vm.loginUrl = result;
                    });
                }

                vm.disconnect = function() {
                    xeroService.removeToken().success(function(result) {
                        $state.reload();
                        vm.isConnected = false;
                    }).finally(function() {
                        vm.loading = false;
                    });

                };

                vm.fillItems = function() {
                    xeroService.getItems().success(function(result) {
                        vm.xeroItems = result;
                        vm.fillCustomers();
                    }).finally(function() {
                    });
                };
                vm.fillCustomers = function() {
                    xeroService.getCustomers().success(function(result) {
                        vm.xeroContacts = result;
                        vm.fillAccounts();
                    }).finally(function() {

                    });
                };

                vm.fillAccounts = function() {
                    xeroService.getAccounts().success(function(result) {
                        vm.xeroAccounts = result;
                        vm.fillSetting();
                    }).finally(function() {
                    });
                };

                vm.fillSetting = function() {
                    xeroService.getXeroSetting().success(function(result) {
                        console.log(result);
                        vm.xero = result;
                        vm.createSeparateInvoice = vm.xero.createSeparateInvoice;
                    }).finally(function() {
                    });
                };
                vm.refresh = function() {
                    vm.loading = true;
                    vm.fillItems();
                    vm.loading = false;


                };
                vm.getOptionValue = function(item) {
                    return item.value;
                };
                vm.update = function() {
                    console.log(vm.xero);
                    vm.xero.createSeparateInvoice = vm.createSeparateInvoice;
                    vm.loading = true;
                    xeroService.updateXeroSetting(vm.xero).success(function(result) {
                    }).finally(function() {

                        abp.notify.info(app.localize("SavedSuccessfully"));
                        vm.loading = false;
                    });
                };

                vm.sync = function() {
                    vm.loading = true;
                    xeroService.syncNonSales().success(function(result) {
                    }).finally(function() {

                        abp.notify.info(app.localize("Success"));
                        vm.loading = false;
                    });
                };
                function getAllPaymentTypes() {
                    vm.saving = true;
                    lookUpService.getPaymentTypes({
                    }).success(function (result) {
                        vm.payments = result.items;
                    }).finally(function (result) {
                        vm.saving = false;
                    });
                }
                getAllPaymentTypes();

                 
                function getAllDepartments() {
                    vm.saving = true;
                    lookUpService.getDepartments({
                    }).success(function (result) {
                        vm.departments = result.items;
                    }).finally(function (result) {
                        vm.saving = false;
                    });
                }
                getAllDepartments();


                vm.addPaymentTypePortion = function () {
                    vm.xero.paymentTrackings.push({ 'paymentTypeId': 0, 'paymentAccount': "" });
                };

                vm.removePaymentTypePortion = function (productIndex) {
                    vm.xero.paymentTrackings.splice(productIndex, 1);
                };

                vm.addDepartmentPortion = function () {
                    vm.xero.departmentTrackings.push({ 'departmentId': 0, 'saleItem': "" });
                };

                vm.removeDepartmentPortion = function (productIndex) {
                    vm.xero.departmentTrackings.splice(productIndex, 1);
                };
            }
        ]);
})();