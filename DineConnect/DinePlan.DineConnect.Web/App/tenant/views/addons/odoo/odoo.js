﻿(function() {
    appModule.controller("tenant.views.addons.odoo", [
        "$scope", "$state", "$http", "$window", "$uibModal", "uiGridConstants", "abp.services.app.odoo",
        function($scope, $state, $http, $window, $modal, uiGridConstants, odooService) {
            var vm = this;
            vm.isConnected = false;
            vm.isAccountTypeChoosen = false;
            vm.assetAccountTypeId = 0;
            vm.revenueAccountTypeId = 0;

            vm.filterText = null;
            vm.loading = false;

            vm.connect = function() {
                vm.loading = true;

                odooService.updateConnection({
                    odooUrl: vm.odooUrl,
                    odooDatabase: vm.odooDatabase,
                    odooUsername: vm.odooUsername,
                    odooPassword: vm.odooPassword
                }).success(function(result) {
                    vm.updateOdooResults(result);
                }).finally(function() {
                    vm.loading = false;
                });
            };
            vm.syncSuppliers = function() {
                vm.loading = true;
                odooService.syncSuppliers({

                }).success(function(result) {
                }).finally(function() {
                    vm.loading = false;
                });
            };
            vm.syncMaterials = function () {
                vm.loading = true;
                odooService.syncMaterials({

                }).success(function (result) {
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.syncLocations = function() {
                vm.loading = true;
                odooService.syncLocations({

                }).success(function(result) {
                }).finally(function() {
                    vm.loading = false;
                });
            };

            vm.checkConnected = function() {
                vm.loading = true;

                odooService.getAccountTypes({})
                    .success(function(result) {
                        vm.accountTypes = result;
                    }).finally(function() {
                        odooService.updateConnection({
                        }).success(function(result) {
                            vm.updateOdooResults(result);
                        }).finally(function() {
                            vm.loading = false;
                        });
                    });

                odooService.getAccounts({})
                  .success(function (result) {
                      vm.accounts = result;
                  }).finally(function () {
                      odooService.updateConnection({
                      }).success(function (result) {
                          vm.updateOdooResults(result);
                      }).finally(function () {
                          vm.loading = false;
                      });
                  });
            };
            vm.checkConnected();

            vm.updateOdooResults = function(result) {

                console.log(result);

                vm.isConnected = result.status;
                vm.odooUrl = result.odooUrl;
                vm.odooDatabase = result.odooDatabase;
                vm.odooUsername = result.odooUsername;
                vm.odooPassword = result.odooPassword;

                odooService.getAccountTypes({})
                    .success(function(result) {
                        console.log(result);
                        vm.accountTypes = result;
                    }).finally(function() {
                        vm.loading = false;
                    });

                if (result.assetAccountType > 0) {

                    vm.assetAccountTypeId = result.assetAccountType;
                    vm.revenueAccountTypeId = result.revenueAccountType;
                  
                    vm.isAccountTypeChoosen = true;


                } else {
                    vm.isAccountTypeChoosen = false;
                    if (result.status) {
                        odooService.getAccountTypes({})
                            .success(function(result) {
                                console.log(result);
                                vm.accountTypes = result;
                            }).finally(function() {
                                vm.loading = false;
                            });
                    }
                }

                if (result.invoiceAccountId > 0) {
                    vm.invoiceAccountId = result.invoiceAccountId;
                    vm.taxAccountId = result.taxAccountId;

                } else {
                    if (result.status) {
                        odooService.getAccounts({})
                            .success(function (result) {
                                vm.accounts = result;
                            }).finally(function () {
                                vm.loading = false;
                            });
                    }
                }
            };

            vm.getEditionValue = function(item) {
                return parseInt(item.value);
            };

            vm.updateAccountTypes = function() {
                odooService.updateConnection({
                        odooUrl: vm.odooUrl,
                        odooDatabase: vm.odooDatabase,
                        odooUsername: vm.odooUsername,
                        odooPassword: vm.odooPassword,
                        assetAccountType: vm.assetAccountTypeId,
                        revenueAccountType: vm.revenueAccountTypeId,
                        invoiceAccountId: vm.invoiceAccountId,
                        taxAccountId: vm.taxAccountId
                    })
                    .success(function(result) {
                        vm.isAccountTypeChoosen = true;
                        vm.updateOdooResults(result);
                    }).finally(function() {
                        vm.loading = false;
                    });
            };
        }
    ]);
})();