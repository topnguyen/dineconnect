﻿(function () {
    appModule.controller('tenant.views.addons.zomato', [
        '$scope', '$state', '$http', '$window', '$uibModal', 'uiGridConstants', 'abp.services.app.zomato', 'abp.services.app.connectLookup',
        function ($scope, $state, $http, $window, $modal, uiGridConstants, zomatoService, lookService) {
            var vm = this;
            vm.isConnected = false;
            vm.filterText = null;
            vm.loading = false;
            vm.priceTags = [];
            vm.priceTagId = 0;

            vm.syncMenu = function() {
                vm.loading = true;
                zomatoService.syncMenu({
                
                }).success(function(result) {
                }).finally(function() {
                    vm.loading = false;
                });
            };
            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };
            vm.syncLocations = function() {
                vm.loading = true;
                zomatoService.syncLocations({
                
                }).success(function(result) {
                }).finally(function() {
                    vm.loading = false;
                });
            };

            vm.syncPrice = function() {
                vm.loading = true;
                zomatoService.syncPrice({
                    apiKey: vm.zomatoKey,
                    tagId: vm.priceTagId
                }).success(function (result) {
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.checkConnected = function () {
                vm.loading = true;
                zomatoService.connected({
                    apiKey: vm.zomatoKey,
                    tagId: vm.priceTagId
                }).success(function (result) {
                    vm.isConnected = result.connected;
                    vm.zomatoKey = result.apiKey;
                    vm.priceTagId = result.tagId;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.checkConnected();
           
            function init() {
                vm.saving = true;
                lookService.getPriceTags({}).success(function (result) {
                    vm.priceTags = result.items;
                    vm.priceTags.unshift({ value: "0", displayText: app.localize('NotAssigned') });

                });
                vm.saving = false;

            }
            init();

        }]);
})();


