﻿(function () {
    appModule.controller('tenant.views.addons.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants',
        function ($scope,$state, $modal, uiGridConstants) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;
            

            vm.qbSettingOnline = function (objId) {
                $state.go("tenant.addonqb", {
                    id: objId
                });
            };

            vm.xeroSetting = function (objId) {
                $state.go("tenant.addonxero", {
                    id: objId
                });
            };
           
            vm.qoyod = function (objId) {
                $state.go("tenant.addonqoyod", {
                    id: objId
                });
            };

            vm.zomato = function (objId) {
                $state.go("tenant.addonzomato", {
                    id: objId
                });
            };

            vm.odoo = function (objId) {
                $state.go("tenant.addonodoo", {
                    id: objId
                });
            };

            vm.xero = function (objId) {
                $state.go("tenant.addonxero", {
                    id: objId
                });
            };

            vm.quickbooks = function (objId) {
                $state.go("tenant.addonquickbooks", {
                    id: objId
                });
            };

            vm.zohobooks = function (objId) {
                $state.go("tenant.addonzohobooks", {
                    id: objId
                });
            };

            vm.urbanPiper = function (objId) {
                $state.go("tenant.addonurbanpiper", {
                    id: objId
                });
            };

            vm.grab = function (objId) {
                $state.go("tenant.addongrab", {
                    id: objId
                });
            };
        }]);
})();

