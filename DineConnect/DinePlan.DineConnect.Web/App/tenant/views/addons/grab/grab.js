﻿(function () {
    appModule.controller("tenant.views.addons.grab",
        [
            "$scope", "$state", "$http", "abp.services.app.grab", 'abp.services.app.connectLookup',
            function ($scope, $state, $http, grabService, lookUpService) {
                var vm = this;
                vm.grab = [];
                vm.clientId = "";
                vm.clientSecret = "";
                vm.loginUrl = "";
                vm.production = false;
                vm.isConnected = false;

                grabService.isTokenExists().success(function (result) {
                    vm.isConnected = result;
                    vm.refresh();
                }).finally(function () {
                    vm.loading = false;
                });

                vm.disconnect = function () {
                    grabService.removeToken().success(function (result) {
                        $state.reload();
                        vm.isConnected = false;
                    }).finally(function () {
                        vm.loading = false;
                    });
                };

                vm.connect = function () {
                    console.log("Connecting");
                    grabService.connect(vm.clientId, vm.clientSecret, vm.production).success(function (result) {
                        $state.reload();
                        vm.isConnected = result;
                    }).finally(function () {
                        vm.loading = false;
                    });
                };

                vm.refresh = function () {
                };

                vm.getOptionValue = function (item) {
                    return item.value;
                };

                vm.cancel = function () {
                    $state.go('tenant.addons');
                };

                vm.syncData = function () {
                    grabService.syncToGrabForMenu(vm.merchantID)
                        .success(function (result) {

                            if (result.isSuccessful)
                                abp.notify.info(app.localize("SyncedSuccessfully"));
                            else
                                abp.notify.info("Failed! " + result.statusDescription);
                        }).finally(function () {
                            vm.loading = false;
                        });
                };
            }
        ]);
})();