﻿(function() {
    appModule.controller("tenant.views.addons.urbanpiper",
        [
            "$scope", "$state", "$http", "$window", "$uibModal", "uiGridConstants", "abp.services.app.urbanPiper",
            function($scope, $state, $http, $window, $modal, uiGridConstants, urbanService) {
                var vm = this;


                vm.isConnected = false;
                vm.filterText = null;
                vm.loading = false;

                vm.urbanPiper = {
                    'user': "biz_adm_clients_gwAxdKMvMNOp",
                    'url': "https://staging.urbanpiper.com/",
                    'key': ""
                };

                vm.checkConnected = function() {
                    vm.loading = true;
                    urbanService.getOrUpdateToken(vm.urbanPiper).success(function(result) {
                        vm.urbanPiper = result;
                        abp.notify.info(app.localize('SavedSuccessfully'));

                    }).finally(function() {
                        vm.loading = false;
                    });

                };
                vm.checkConnected();


            }
        ]);
})();