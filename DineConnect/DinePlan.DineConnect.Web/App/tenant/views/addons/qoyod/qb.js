﻿(function () {
    appModule.controller('tenant.views.addons.quoyod', [
        '$scope', '$state', '$http', '$uibModal', 'uiGridConstants', 'abp.services.app.qoyod',
        function ($scope, $state, $http, $modal, uiGridConstants, qbService) {
            var vm = this;
            vm.isConnected = false;
            vm.filterText = null;
            vm.loading = false;
            vm.code = "";
            vm.outputSupplier = null;
            vm.outputCustomer = null;

            
            vm.connect = function () {
                if (vm.code.length > 0) {
                    vm.loading = true;
                    qbService.getOrUpdateToken(vm.code).success(function (result) {
                        vm.isConnected = result.length > 0;
                        vm.code = result;
                    }).finally(function () {
                        vm.loading = false;
                    });
                }
            }


            vm.syncLocations = function () {
                vm.loading = true;
                qbService.syncLocations({
                }).success(function (result) {
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.syncSuppliers = function () {
                vm.loading = true;
                qbService.syncSuppliers({
                }).success(function (result) {
                    console.log(result);
                    vm.outputSupplier = result;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.syncCustomers = function () {
                vm.loading = true;
                qbService.syncCustomers({
                }).success(function (result) {
                    vm.outputCustomer = result;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.syncLocations = function () {
                vm.loading = true;
                qbService.syncLocations({
                }).success(function (result) {
                    vm.outputLocation = result;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.checkConnected = function () {
                vm.loading = true;
                qbService.getOrUpdateToken("").success(function (result) {
                    vm.isConnected = result.length > 0;
                    vm.code = result;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.checkConnected();

        }]);
})();


