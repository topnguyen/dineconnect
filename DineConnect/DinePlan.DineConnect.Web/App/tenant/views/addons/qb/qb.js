﻿(function () {
    appModule.controller('tenant.views.addons.qb', [
        '$scope', '$state', '$http','$window', '$uibModal', 'uiGridConstants', 'abp.services.app.quickBooks', 
        function ($scope, $state, $http, $window, $modal, uiGridConstants, qbService) {
            var vm = this;
            vm.isConnected = false;
            vm.filterText = null;
            vm.loading = false;

            qbService.isTokenExists().success(function (result) {
                vm.isConnected = result;
                vm.refresh();
            }).finally(function () {
                vm.loading = false;
            });


            if (!vm.isConnected) {
                qbService.getLoginUrl().success(function (result) {
                    vm.loginUrl = result;
                });
            }

            vm.getOptionValue = function (item) {
                return item.value;
            };

            vm.fillAccounts = function () {
                qbService.getAccounts().success(function (result) {
                    vm.qbAccounts = result;
                }).finally(function () {
                });
            };

            vm.fillCustomers = function () {
                qbService.getCustomers().success(function (result) {
                    vm.qbCustomers = result;
                }).finally(function () {
                });
            };

            vm.refresh = function () {
                vm.loading = true;   
                vm.fillAccounts();
                vm.fillCustomers();
                vm.loading = false;
            };

            vm.disconnect = function () {
                qbService.removeToken().success(function (result) {
                    $state.reload();
                    vm.isConnected = false;
                }).finally(function () {
                    vm.loading = false;
                });

            };

            /*
            vm.connect = function () {
                qbService.getAuthUrl({}).then(function (result) {
                    $window.location.href = result.data;
                }, function (error) {
                    alert(error.data);
                });
            };


            vm.syncLocations = function () {
                vm.loading = true;
                qbService.syncLocations({
                }).success(function (result) {
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.syncSuppliers = function () {
                vm.loading = true;
                qbService.syncSuppliers({
                }).success(function (result) {
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.checkConnected = function () {
                vm.loading = true;
                qbService.connected({
                }).success(function (result) {
                    vm.isConnected = result;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.checkConnected();
            */

        }]);
})();


