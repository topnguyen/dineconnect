﻿(function () {
    appModule.controller('tenant.views.addons.power', [
        '$scope', '$state', '$http', '$uibModal', 'uiGridConstants', 'abp.services.app.quickBooks',
        function ($scope, $state, $http, $modal, uiGridConstants, qbService) {
            var vm = this;
            vm.isConnected = false;
            vm.filterText = null;
            vm.loading = false;

            vm.connect = function () {
                var post = $http({
                    method: "GET",
                    url: "/Power/Grant"
                });

                post.success(function (data, status) {
                    $window.alert("Hello: " + data.Name + " .\nCurrent Date and Time: " + data.DateTime);
                });

                post.error(function (data, status) {
                    $window.alert(data.Message);
                });
            }

        }]);
})();


