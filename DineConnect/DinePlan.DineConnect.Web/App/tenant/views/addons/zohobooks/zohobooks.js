﻿(function () {
    appModule.controller('tenant.views.addons.zohobooks', [
        '$scope', '$state', '$http', '$window', '$uibModal', 'uiGridConstants', 'abp.services.app.zohoBooks',
        function ($scope, $state, $http, $window, $modal, uiGridConstants, zohoBooksService) {
            var vm = this;
            vm.isConnected = false;
            vm.filterText = null;
            vm.loading = false;

            
            zohoBooksService.isTokenExists().success(function (result) {
                vm.isConnected = result;
                vm.refresh();
            }).finally(function () {
                vm.loading = false;
            });
            

            if (!vm.isConnected) {
                zohoBooksService.getLoginUrl().success(function (result) {
                    vm.loginUrl = result;
                });
            }

            vm.disconnect = function () {
                zohoBooksService.removeToken().success(function (result) {
                    $state.reload();
                    vm.isConnected = false;
                }).finally(function () {
                    vm.loading = false;
                });

            };


            vm.fillAccounts = function () {
                zohoBooksService.getAccounts().success(function (result) {
                    vm.zohoBooksAccounts = result;
                }).finally(function () {
                });
            };


            vm.fillCustomers = function () {
                zohoBooksService.getCustomers().success(function (result) {
                    vm.zohoBooksCustomers = result;
                }).finally(function () {
                });
            };

            vm.getOptionValue = function (item) {
                return item.value;
            };

            vm.refresh = function () {
                vm.loading = true;
                vm.fillAccounts();
                vm.fillCustomers();
                vm.loading = false;
            };


            /*


          

            

           

            /*
            vm.connect = function () {
                qbService.getAuthUrl({}).then(function (result) {
                    $window.location.href = result.data;
                }, function (error) {
                    alert(error.data);
                });
            };


            vm.syncLocations = function () {
                vm.loading = true;
                qbService.syncLocations({
                }).success(function (result) {
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.syncSuppliers = function () {
                vm.loading = true;
                qbService.syncSuppliers({
                }).success(function (result) {
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.checkConnected = function () {
                vm.loading = true;
                qbService.connected({
                }).success(function (result) {
                    vm.isConnected = result;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.checkConnected();
            */

        }]);
})();


