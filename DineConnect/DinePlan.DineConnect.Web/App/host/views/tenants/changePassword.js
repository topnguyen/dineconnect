﻿(function () {
    appModule.controller('host.views.tenants.changePassword', [
        '$scope', '$uibModalInstance', 'abp.services.app.tenant', 'tenant',
        function ($scope, $uibModalInstance, tenantService, tenant) {
            var vm = this;

            vm.saving = false;
           
            vm.tenant = {
                tenantId: tenant,
                adminPassword: ""
            };

            vm.save = function () {
                vm.saving = true;
                tenantService.changePassword(vm.tenant)
                    .success(function () {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                    }).finally(function () {
                        vm.saving = false;
                    });
            };

           
        }
    ]);
})();