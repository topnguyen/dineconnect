﻿(function () {
    appModule.controller('host.views.swipe.swipedashboard', [
        '$scope', 'appSession', '$rootScope', 'abp.services.app.memberCard', 'NgTableParams',
        function ($scope, appSession, $rootScope, membercardService, ngTableParams) {
            var vm = this;

            vm.dateRangeOptions = app.createDateRangePickerOptions();
            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];
            var todayAsString = moment().format($scope.format);
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.numberOfCardsActivated = 0;
            vm.numberOfCardsDorment = 0;
            vm.totalAmountBalance = 0;
            vm.refundableDepositCollected = 0;
            
            vm.currentLocation = appSession.location!=null?appSession.location.name:"";
            vm.currentLocationId = appSession.location!=null?appSession.location.id:0;


            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.currencyText = abp.features.getValue('DinePlan.DineConnect.Connect.Currency');
            vm.paymentChart = function() {
                setupService.getPaymentChart({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    location: vm.currentLocationId
                }).success(function (result) {

                    Highcharts.chart('payment_stats', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{point.percentage:.1f} %'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.y} <br/>{point.percentage:.1f} %',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            name: app.localize('Payment'),
                            colorByPoint: true,
                            data: $.parseJSON(JSON.stringify(result))
                        }]
                    });
                });
            };
           
            vm.transactionChart = function () {

                setupService.getTransactionChart({
                    location: vm.currentLocationId,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format)
                }).success(function (result) {
                    Highcharts.chart('transaction_stats', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{point.percentage:.1f} %'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.y} <br/>{point.percentage:.1f} %',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            name: app.localize('Payment'),
                            colorByPoint: true,
                            data: $.parseJSON(JSON.stringify(result))
                        }]
                    });
                });
            };
            vm.departmentChart = function () {
                setupService.getDepartmentChart({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    location: vm.currentLocationId
                }).success(function (result) {
                    Highcharts.chart('department_stats', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{point.percentage:.1f} %'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.y} <br/>{point.percentage:.1f} %',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            name: app.localize('Department'),
                            colorByPoint: true,
                            data: $.parseJSON(JSON.stringify(result))
                        }]
                    });
                });
            };

            $scope.getLValue = function (column) {
                return app.localize(column).toUpperCase();
            };


            vm.swipeTransactionChart = function () {
                membercardService.getCardTransactionChart({
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                }).success(function (result) {
                    var a = 1;
                    Highcharts.chart('transaction_stats', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: app.localize('Transaction')
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            transactions: $.parseJSON(JSON.stringify(result.transactions)),
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: app.localize('Total')
                            }
                        },
                        tooltip: {
                            headerFormat: '<table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        series: $.parseJSON(JSON.stringify(result.chartOutput))
                    });

                });
            };

            vm.swipeCardTypeList = [];
            vm.getCardDetails = function () {
                vm.loading = true;
                vm.swipeCardTypeList = [];
                membercardService.getSwipeCardDashBoardReport({
                }).success(function (result) {
                    if (result != null) {
                        vm.numberOfCardsActivated = result.numberOfCardsActivated;
                        vm.numberOfCardsDorment = result.numberOfCardsDorment;
                        vm.totalAmountBalance = result.totalAmountBalance;
                        vm.refundableDepositCollected = result.refundableDepositCollected;
                    }
                    
                    vm.swipeCardTypeList = result.swipeCardTypeList;
                    vm.tableParams = new ngTableParams({}, { dataset: result.swipeCardTypeList, counts: [] });
                    console.log(vm.tableParams);
                    vm.loading = false;
                });
            }

            vm.refresh = function () {
                //vm.paymentChart();
                //vm.transactionChart();
                //vm.departmentChart();
                //vm.itemChart();
                //vm.getTicketDetails();
                vm.swipeTransactionChart();
                vm.getCardDetails();
            }

            //vm.refresh();

        }
    ]);
})();