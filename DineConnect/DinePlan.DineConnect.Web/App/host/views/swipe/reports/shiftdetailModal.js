﻿(function () {
    appModule.controller('tenant.views.connect.report.shiftdetailModal', [
        '$scope', '$uibModalInstance', 'ledgerId', 'abp.services.app.memberCard',
        function ($scope, $modalInstance, ledgerId, membercardService) {
            var vm = this;

            vm.close = function () {
                $modalInstance.dismiss();
            };

            vm.init = function () {
                membercardService.getSwipeCardLedgerDetail({
                    id : ledgerId
                }).success(function(result)  {
                    vm.ledger = result;
                    vm.ctime = moment(vm.ledger.ledgerTime).format('LLL');
                });
            };

            vm.init();
        }
    ]);
})();