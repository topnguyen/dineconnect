﻿(function () {
    appModule.controller('host.views.swipe.transaction.swipeshift.swipeShiftReport', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.swipeShift', '$rootScope',
    function ($scope, $modal, uiGridConstants, swipeshiftService,$rootScope) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            $rootScope.settings.layout.pageSidebarClosed = true;
            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];
            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.shiftStatusList = [];
            vm.shiftStatusList.push({ value: 0, displayText: app.localize('Excess') });
            vm.shiftStatusList.push({ value: 1, displayText: app.localize('Shortage') });
            vm.shiftStatusList.push({ value: 2, displayText: app.localize('Equal') });

            vm.shiftStatus = [];

            vm.userGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
					//{
					//	name: app.localize('Actions'),
					//	enableSorting: false,
					//	width: 120,

					//	cellTemplate:
					//	   "<div class=\"ui-grid-cell-contents\">" +
					//		    "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
					//		   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
					//		   "    <ul uib-dropdown-menu>" +
					//		   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editSwipeShift(row.entity)\">" + app.localize("Edit") + "</a></li>" +
					//		   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteSwipeShift(row.entity)\">" + app.localize("Delete") + "</a></li>" +
					//		   "    </ul>" +
					//		   "  </div>" +
					//		   "</div>"
					//},
                    //{
                    //    name: app.localize('UserId'),
                    //    field: 'userId'
                    //},
                    {
                        name: app.localize('UserName'),
                        field: 'userName'
                    },
                    {
                        name: app.localize('StartTime'),
                        field: 'shiftStartTime',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm\'',
                        minWidth: 150
                    },
                    {
                        name: app.localize('EndTime'),
                        field: 'shiftEndTime',
                        cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm\'',
                        minWidth: 150
                    },
                    {
                        name: app.localize('Opening'),
                        field: 'tenderOpenAmount',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter2Digits'
                    },
                    {
                        name: app.localize('CashIn'),
                        field: 'tillShiftCashIn',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter2Digits'
                    },
                    {
                        name: app.localize('Credit'),
                        field: 'tenderCredit',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter2Digits'
                    },
                    {
                        name: app.localize('CashOut') ,
                        field: 'tillShiftCashOut',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter2Digits'
                    },
                    {
                        name: app.localize('Debit'),
                        field: 'tenderDebit',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter2Digits'
                    },
                    {
                        name: app.localize('Closing'),
                        field: 'tenderCloseAmount',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter2Digits'
                    },
                    {
                        name: app.localize('Actual'),
                        field: 'actualTenderClosing',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter2Digits'
                    },
				    {
				        name: app.localize('Status'),
				        field: 'excessShortageStatus',
				        cellClass: function (grid, row) {
				            if (row.entity.excessShortageAmount < 0)
				                return 'ui-red';
				            else
				                return true;
				        },
				        //cellTemplate:                   //'<div class=\"ui-grid-cell-contents\">' +
				   //     '  <span ng-show="row.entity.excessShortageAmount>0" class="label label-success">' + row.entity.excessShortageStatus + '</span>' +
                   //'  <span ng-show="!row.entity.excessShortageStatus<0" " class="label label-danger">' + row.entity.excessShortageStatus + '</span>' +
                   //'  <span ng-show="!row.entity.excessShortageStatus==0" "class="label label-info">' + row.entity.excessShortageStatus + '</span>' +
                   //'</div>',
				    },
                    {
                        name: app.localize('Difference'),
                        field: 'excessShortageAmount',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter2Digits'
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                var alltrans = vm.shiftStatus;
                if (vm.shiftStatus == '' || vm.shiftStatus.length==0) {
                    alltrans = [];
                }

                swipeshiftService.getShiftViewAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    userName: vm.userName,
                    startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                    shiftExcessShortageStatusList: alltrans
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editSwipeShift = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createSwipeShift = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteSwipeShift = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteSwipeShiftWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            swipeshiftService.deleteSwipeShift({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/host/views/swipe/transaction/swipeshift/createOrEditModal.cshtml',
                    controller: 'host.views.swipe.transaction.swipeshift.createOrEditModal as vm',
                    backdrop: 'static',
					keyboard: false,
                    resolve: {
                        swipeshiftId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.exportToExcel = function () {
                vm.loading = true;
                var alltrans = vm.shiftStatus;
                if (vm.shiftStatus == '' || vm.shiftStatus.length==0) {
                    alltrans = [];
                }

								swipeshiftService.getAllToExcel({
									skipCount: requestParams.skipCount,
									maxResultCount: requestParams.maxResultCount,
									sorting: requestParams.sorting,
									filter: vm.filterText,
									userName: vm.userName,
									startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
									endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
									shiftExcessShortageStatusList: alltrans
                }).success(function (result) {
                    app.downloadTempFile(result);
                    vm.loading = false;
                    });
            };


            vm.getAll();
        }])
    
    .filter('fractionFilter2Digits', function () {
        return function (value) {
            return value.toFixed(2);
        };
    })
}) ();

