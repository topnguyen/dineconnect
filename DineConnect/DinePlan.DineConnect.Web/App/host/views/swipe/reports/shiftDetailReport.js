﻿(function () {
    appModule.controller('host.views.swipe.transaction.swipeshift.shiftDetailReport', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.swipeShift', '$rootScope', 'abp.services.app.swipePaymentType', 'abp.services.app.memberCard',
    function ($scope, $modal, uiGridConstants, swipeshiftService,$rootScope,swipepaymenttypeService,membercardService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            $rootScope.settings.layout.pageSidebarClosed = true;
            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.loading = true;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];
            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.shiftStatus = [];

            vm.userGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
                    {
                        name: 'Actions',
                        enableSorting: false,
                        width: 50,
                        headerCellTemplate: '<span></span>',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.showDetails(row.entity)"><i class="fa fa-search"></i></button>' +
                                '</div>'
                    },
                     {
                         name: app.localize('Time'),
                         field: 'ledgerTime',
                         cellFilter: 'momentFormat: \'YYYY-MMM-DD HH:mm\'',
                         minWidth: 150
                     },
                    {
                        name: app.localize('CardNumber'),
                        field: 'cardNumber'
                    },
                    {
                        name: app.localize('Description'),
                        field: 'description',
                        minWidth: 150
                    },
                   
                    {
                        name: app.localize('OpeningBalance'),
                        field: 'openingBalance',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter2Digits'
                    },
                    
                    {
                        name: app.localize('Credit'),
                        field: 'credit',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter2Digits'
                    },
                    {
                        name: app.localize('Debit'),
                        field: 'debit',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter2Digits'
                    },
                    {
                        name: app.localize('ClosingBalance'),
                        field: 'closingBalance',
                        cellClass: 'ui-ralign',
                        cellFilter: 'fractionFilter2Digits'
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.payments = [];
          

            vm.showDetails = function(myObject) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/host/views/swipe/reports/shiftdetailModal.cshtml',
                    controller: 'tenant.views.connect.report.shiftdetailModal as vm',
                    backdrop: 'static',
					keyboard: false,
                    resolve: {
                        ledgerId: function () {
                            return myObject.id;
                        }
                    }
                });
            }

            vm.refPaymentTypes = [];
            vm.getPayments = function () {
                vm.refPaymentTypes = [];
                swipepaymenttypeService.getAll({
                }).success(function (result) {
                    vm.refPaymentTypes = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.getPayments();

            vm.transactions = [];
            vm.selectedTransactions = [];

            vm.getTransactions = function () {
                membercardService.getSwipeTransactionTypeForCombobox({
                }).success(function (result) {
                    vm.transactions = $.parseJSON(JSON.stringify(result.items));
                }).finally(function (result) {
                });
            };

						vm.getTransactions();

						vm.getAll = function () {
							vm.loading = true;
							var alltrans = vm.selectedTransactions;
							if (vm.selectedTransactions == '' || vm.selectedTransactions.length == 0) {
								alltrans = [];
							}

							var allPayment = vm.payments;
							if (vm.payments == '' || vm.payments.length == 0) {
								allPayment = [];
							}


							swipeshiftService.getShiftDetailViewAll({
								skipCount: requestParams.skipCount,
								maxResultCount: requestParams.maxResultCount,
								sorting: requestParams.sorting,
								filter: vm.filterText,
								userName: vm.userName,
								startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
								endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
								transactionList: alltrans,
								paymentTypeList: allPayment,
								shiftExcessShortageStatusList: []
							}).success(function (result) {
								vm.userGridOptions.totalItems = result.totalCount;
								vm.userGridOptions.data = result.items;
							}).finally(function () {
								vm.loading = false;
							});
						};

            vm.exportToExcel = function () {
							vm.loading = true;
							var alltrans = vm.selectedTransactions;
							if (vm.selectedTransactions == '' || vm.selectedTransactions.length == 0) {
								alltrans = [];
							}

							var allPayment = vm.payments;
							if (vm.payments == '' || vm.payments.length == 0) {
								allPayment = [];
							}


							swipeshiftService.getAllToExcelShiftDetail({
							skipCount: requestParams.skipCount,
							maxResultCount: requestParams.maxResultCount,
							sorting: requestParams.sorting,
							filter: vm.filterText,
							userName: vm.userName,
							startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
							endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
							transactionList: alltrans,
							paymentTypeList: allPayment,
							shiftExcessShortageStatusList: []
            }).success(function (result) {
                app.downloadTempFile(result);
                vm.loading = false;
                });
        };

            vm.getAll();
        }])
    
    .filter('fractionFilter2Digits', function () {
        return function (value) {
            return value.toFixed(2);
        };
    })
}) ();

