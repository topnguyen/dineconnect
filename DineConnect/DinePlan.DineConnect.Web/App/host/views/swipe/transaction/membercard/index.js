﻿(function () {
    appModule.controller('host.views.swipe.transaction.membercard.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.memberCard', '$rootScope', 'appSession', 'abp.services.app.swipeShift',
        function ($scope, $state, $modal, uiGridConstants, membercardService, $rootScope, appSession, swipeshiftService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loginStatus = null;
            vm.loading = false;
            vm.filterText = null;

            vm.currentUserName = appSession.user.name;
            vm.currentUserId = appSession.user.id;

            vm.shiftAlreadyOpenflag = false;
            vm.loading = true;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Host.Swipe.Transaction.MemberCard.Create'),
                edit: abp.auth.hasPermission('Pages.Host.Swipe.Transaction.MemberCard.Edit'),
                view: abp.auth.hasPermission('Pages.Host.Swipe.Transaction.MemberCard.Create'),
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };



            vm.userGridOptions = {
						enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
						enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
						paginationPageSizes: app.consts.grid.defaultPageSizes,
						paginationPageSize: app.consts.grid.defaultPageSize,
						useExternalPagination: true,
						useExternalSorting: true,
						appScopeProvider: vm,
						rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
                     {
                         name: app.localize('View') ,
                         enableSorting: false,
                         width: 60,
                         //headerCellTemplate: '<span></span>',
                         cellTemplate:
                             '<div class=\"ui-grid-cell-contents text-center\">' +
                                 '  <button  ng-if= \"row.entity.id\" class="btn btn-default btn-xs" ng-click="grid.appScope.viewMemberDetails(row.entity)"><i class="fa fa-edit"></i></button>' +
                                 '</div>'
                     },
                   
                    {
                        name: app.localize('MemberCode'),
                        field: 'memberCode'
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name'
                    },
                    {
                        name: app.localize('CardNumber'),
                        field: 'cardNumber'
                    },
                    {
                        name: app.localize('Balance'),
                        field: 'balance',
                        cellClass: 'ui-ralign'
                    },
                    {
                        name: app.localize('Transaction'),
                        enableSorting: false,
                        width: 100,
                        //headerCellTemplate: '<span></span>',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.viewCardTransaction(row.entity)"><i class="fa fa-eye"></i></button>' +
                                '</div>'
                    },
                 
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                membercardService.getMemberIndexView({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    isActive: null,
                    //cardNumber : vm.cardNumber
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.exportExcel = function (obj) {

                abp.ui.setBusy("#MyLoginForm");
                membercardService.getAllToExcel({
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                        abp.ui.clearBusy("#MyLoginForm");
                    });
            };


            vm.editMemberCard = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

          

            vm.createMemberCard = function () {
                openCreateOrEditModal(null);
            };

            vm.deactiveMemberCard = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteMemberCardWarning', myObject.id),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            membercardService.deactivateMemberCard({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeactivatedMemberCard'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                if (vm.loginStatus == null || vm.loginStatus.id == null || vm.loginStatus.id == 0)
                {
                    abp.notify.error(app.localize('ShiftShouldOpenBeforeOperations'));
                    $state.go('host.swipeshift');
                    vm.loading = false;
                    return;
                }

                $state.go('host.membercardedit', {
                    id: objId,
                    shiftRefId : vm.loginStatus.id
                });
            }


            vm.print = function (objid) {
                $state.go('host.membercardprint', {
                    id: objid,
                });

                //window.location.href = '/PrintEscPos/Index';
                //vm.currentLanguage = abp.localization.currentLanguage;
                //location.href = abp.appPath + 'Localization/ChangeCulture?cultureName=' + vm.currentLanguage.name + '&returnUrl=/PrintEscPos/Index?transactionId=' + objid ;// + window.location.href;
            }

            vm.viewCardTransaction = function (myObj) {
                if (myObj.cardRefId == 0 || myObj.cardRefId == null)
                {
                    abp.notify.error(app.localize('NoRecordsFound'));
                    return;
                }

                var modalInstance1 = $modal.open({
                    templateUrl: '~/App/host/views/swipe/transaction/membercard/drillDownLevel.cshtml',
                    controller: 'host.views.swipe.transaction.membercard.drillDownLevel as vm',
                    backdrop: 'static',
                    resolve: {
                        cardNumber: function () {
                            return '';
                        },
                        cardRefId: function () {
                            return myObj.cardRefId;
                        }

                    }
                });

                modalInstance1.result.then(function (result) {
                    //vm.getAll();
                });
            };

						vm.viewMemberDetails = function (myObj) {
                var modalInstance1 = $modal.open({
                    templateUrl: '~/App/host/views/swipe/transaction/membercard/viewMember.cshtml',
                    controller: 'host.views.swipe.transaction.membercard.viewMember as vm',
                    backdrop: 'static',
                    resolve: {
                        memberCode: function () {
                            return myObj.memberCode;
                        },
                        memberRefId: function () {
                            return myObj.id;
                        },
                        cardRefId: function () {
                            return myObj.cardRefId;
												},
                    }
                });

                modalInstance1.result.then(function (result) {

                });
            };


            vm.exportToExcel = function () {
                membercardService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };


            vm.getLoginInfo = function () {
                vm.loading = true;
                swipeshiftService.getLoginStatus({
                    userId: vm.currentUserId
                }).success(function (result) {
                    if (result.shiftInOutStatus == 0) {
                        vm.shiftAlreadyOpenflag = false;
                        vm.loginStatus = result;
                        abp.notify.error(app.localize('ShiftShouldOpenBeforeOperations'));
                        $state.go('host.swipeshift');
                        vm.loading = false;
                        return;
                    }
                    else if (result.shiftInOutStatus == 1) {
                        vm.shiftAlreadyOpenflag = true;
                        vm.loginStatus = result;
                        vm.loading = false;
                    }
                    vm.getAll();
                }).finally(function () {
                    
                });
            };

            vm.getLoginInfo();

        }]);
})();

