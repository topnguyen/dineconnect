﻿(function () {
    appModule.controller('host.views.swipe.transaction.membercard.viewMember', [
        '$scope', '$uibModalInstance', '$uibModal', 'memberCode', 'memberRefId', 'abp.services.app.memberCard', 'cardRefId', 
        function ($scope, $modalInstance,$modal, memberCode, memberRefId, membercardService, cardRefId) {

            var vm = this;
            vm.memberDetails = null;
						vm.memberCardList = [];
						vm.cardRefId = cardRefId;

            vm.fillTransaction = function () {
                vm.loading = true;
                vm.memberCardList = [];
                vm.memberDetails = null;
                membercardService.getCardTransactionList(
                {
                    memberCode : memberCode,
                    memberRefId: memberRefId,
										cardRefId: vm.cardRefId,
										transactionDetailNotNeeded : true
                }).success(function (result) {
                    if (result != null) {
											vm.memberDetails = result;
											vm.memberDetails.cardDetails.some(function (refdata, refkey) {
												if (refdata.cardRefId == vm.cardRefId) {
													vm.cardDetail = refdata;
													vm.memberCardList = vm.cardDetail.memberCards;
													return true;
												}


												refdata.memberCards.some(function (memdata, memkey) {
													if (memdata.cardRefId == vm.cardRefId) {
														vm.cardDetail = refdata;
														vm.memberCardList = vm.cardDetail.memberCards;
														vm.detailData = vm.cardDetail.cardTransactionDetails;
														return true;
													}
												});

												if (vm.detailData != null)
													return true;
											});

                    }
                }).finally(function () {
                    vm.loading = false;
                });
            }

            vm.fillTransaction();


            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.cardTransactionDataShown = function (argOption) {
                var modalInstance1 = $modal.open({
                    templateUrl: '~/App/host/views/swipe/transaction/membercard/drillDownLevel.cshtml',
                    controller: 'host.views.swipe.transaction.membercard.drillDownLevel as vm',
                    backdrop: 'static',
                    resolve: {
                        cardNumber: function () {
                            return '';
                        },
                        cardRefId: function () {
                            return argOption;
                        }

                    }
                });

                modalInstance1.result.then(function (result) {
                    //vm.getAll();
                });
            }
        }
    ]);
})();