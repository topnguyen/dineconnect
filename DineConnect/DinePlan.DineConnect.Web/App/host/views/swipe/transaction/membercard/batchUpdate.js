(function () {
    appModule.controller('host.views.swipe.transaction.membercard.batchUpdate', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.memberCard', '$rootScope', 'abp.services.app.swipeMember', 'abp.services.app.swipePaymentType', 'appSession', 'hotkeys', 'abp.services.app.personalInformation', 'abp.services.app.swipeCard',
        function ($scope, $state, $stateParams, $modal, membercardService, $rootScope, swipememberService, swipepaymenttypeService, appSession, hotKeys, employeeinfoService, swipecardService) {
            /* eslint-disable */
            var vm = this;
            vm.shiftRefId = $stateParams.shiftRefId;
            vm.batchStatus = $stateParams.batchStatus;
            vm.cardList = [];
            vm.batchAmount = null;
            vm.totalDeposit = 0;
            vm.totalBatchAmount = 0;
            vm.totalDepositAndBatchAmount = 0;

            if (vm.batchStatus == null || vm.batchStatus.length == 0) {
                abp.notify.error(app.localize('ShouldBeBatchUpdate'));
                $state.go('host.membercardedit', {
                    id: null,
                    shiftRefId: vm.shiftRefId
                });
                return;
            }

            vm.batchStatus = vm.batchStatus.toUpperCase();

            if (vm.batchStatus.toUpperCase() == 'BATCHREFUND') {
                vm.depositNeedToRefundFlag = true;
                vm.isFullRefundFlag = true;
            }

            vm.loadingCount = 0;
            vm.canIssueCardDirectly = abp.setting.getBoolean('App.Swipe.CanIssueCardDirectly');

            vm.swipememberpermissions = {
                create: abp.auth.hasPermission('Pages.Host.Swipe.Master.SwipeMember.Create'),
                edit: abp.auth.hasPermission('Pages.Host.Swipe.Master.SwipeMember.Edit'),
                'delete': abp.auth.hasPermission('Pages.Host.Swipe.Master.SwipeMember.Delete')
            };

            $rootScope.settings.layout.pageSidebarClosed = true;
            vm.refPaymentTypes = [];
            vm.paymentList = [];
            vm.initPaymentList = [];

            vm.getPaymentTypes = function () {
                vm.loading = true;
                vm.refPaymentTypes = [];
                vm.initPaymentList = [];
                vm.loadingCount++;
                swipepaymenttypeService.getAll({

                }).success(function (result) {
                    vm.refPaymentTypes = result.items;
                    vm.initPaymentList
                    angular.forEach(vm.refPaymentTypes, function (value, key) {
                        var newpaymenttype = value;
                        newpaymenttype.amount = '';
                        newpaymenttype.showFlag = true;
                        vm.paymentList.push(newpaymenttype);
                        vm.initPaymentList.push(newpaymenttype);
                    });
                }).finally(function () {
                    vm.loading = false;
                    vm.loadingCount--;
                });
            };

            vm.refSwipeMembers = [];
            vm.fillSwipeMember = function () {
                vm.refSwipeMembers = [];
                vm.loading = true;
                vm.loadingCount++;
                swipememberService.getMemberList({
                    isActive: null
                }).success(function (result) {
                    vm.loading = false;
                    vm.refSwipeMembers = result.items;
                }).finally(function () {
                    vm.loading = false;
                    vm.loadingCount--;
                });
            }

            vm.refCardtypes = [];
            vm.fillCardtypes = function () {
                vm.refCardtypes = [];
                vm.loadingCount++;
                swipecardService.getSwipeCardTypes({}).success(function (result) {
                    vm.refCardtypes = result.items;
                }).finally(function () {
                    vm.loadingCount--;
                });
            }
            vm.fillCardtypes();


            vm.refEmployees = [];
            vm.fillEmployees = function () {
                vm.refEmployees = [];
                vm.loading = true;
                vm.loadingCount++;
                employeeinfoService.getAll({

                }).success(function (result) {
                    vm.loading = false;
                    vm.refEmployees = result.items;
                }).finally(function () {
                    vm.loading = false;
                    vm.loadingCount--;
                });
            }









            vm.refCards = [];
            vm.fillCards = function () {
                vm.refCards = [];
                vm.loading = true;
                vm.loadingCount++;
                membercardService.getSwipeCardDetails({

                }).success(function (result) {
                    vm.loading = false;
                    vm.refCards = result.items;
                }).finally(function () {
                    vm.loading = false;
                    vm.loadingCount--;
                });
            }

            vm.refMemberCards = [];
            vm.fillMemberCards = function () {
                vm.refMemberCards = [];
                vm.loading = true;
                vm.loadingCount++;
                membercardService.getMemberCardListAll({
                    activeMemberCardsFlag: true
                }).success(function (result) {
                    vm.loading = false;
                    vm.refMemberCards = result.items;
                }).finally(function () {
                    vm.loading = false;
                    vm.loadingCount--;
                });
            }

            vm.refreshData = function () {
                vm.getPaymentTypes();
                vm.fillCards();
                vm.fillMemberCards();
                vm.fillSwipeMember();
                vm.fillEmployees();
            }
            //vm.refreshData();
            vm.printFlag = false;

            vm.preInit = function () {
                vm.saving = false;
                vm.membercard = null;
                vm.membercardId = null;
                vm.selectedCard = null;

                vm.topUpAmount = null;
                vm.refundAmount = null;
                vm.supplementaryCardIssueFlag = false;
                vm.onemoreprimarycardFlag = false;
                vm.primaryCardRefId = null;


                vm.topupFlag = false;
                vm.refundFlag = false;
                vm.finalPaymentFlag = false;

                if (vm.batchStatus.toUpperCase() == 'BATCHTOPUP') {
                    vm.topupFlag = true;
                }
                else if (vm.batchStatus.toUpperCase() == 'BATCHREFUND') {
                    vm.refundFlag = true;
                }

                vm.depositNeedToCollectFlag = false;
                vm.depositAmountToBeCollected = 0;
                vm.depositAmountToBeRefunded = 0;
                vm.depositedTotalAmount = 0;

                vm.tryToIssueSupplementaryCardFlag = false;
                vm.onemoreprimarycardFlag == false;
                vm.uilimit = 20;

                vm.refPaymentTypes = [];
                vm.paymentList = [];
                vm.initPaymentList = [];
                vm.memberCardList = [];
                vm.refSwipeMembers = [];
                vm.refCards = [];
                vm.refMemberCards = [];
                vm.refEmployees = [];

                vm.refreshData();
                if (vm.batchAmount == null || vm.batchAmount == 0) {
                    $('#AmounttoProcess').focus();
                }
                else {
                    $('#cardnumber').focus();
                }
            }

            vm.preInit();

            vm.existAllElements = function () {
                var errorFlag = false;
                if (vm.membercard.cardNumber == '' || vm.membercard.cardNumber == null) {
                    abp.notify.error(app.localize('CardNumberErr'));
                    errorFlag = true;
                }

                if (vm.membercard.canIssueWithOutMember == false && vm.membercard.isEmployeeCard == false) {
                    if (vm.membercard.memberCode == '' || vm.membercard.memberCode == null || vm.membercard.memberRefId == null || vm.membercard.memberRefId == '') {
                        abp.notify.error(app.localize('SelectMember'));
                        errorFlag = true;
                    }
                }


                if (vm.topUpAmount < 0) {
                    abp.notify.error(app.localize('TopUpAmountCanNotBeNegative', vm.topUpAmount));
                    abp.notify.info(app.localize('Deposit') + ' ' + app.localize('Amount') + ' ' + vm.depositAmountToBeCollected);
                    $('#price-0').focus();
                    errorFlag = true;
                }

                if (vm.refundAmount < 0) {
                    abp.notify.error(app.localize('RefundAmountCanNotBeNegative', vm.refundAmount));
                    $('#price-0').focus();
                    errorFlag = true;
                    return false;
                }

                if ((vm.topUpAmount == null) && (vm.refundAmount == null)) {
                    //abp.notify.error(app.localize('TopUp') + ' / ' + app.localize('Refund') + '?');
                    //$('#AmounttoProcess').focus();
                    errorFlag = true;
                    return false;
                }

                //#region Check Minimum Topup Amount
                if (vm.topUpAmount > 0 && vm.selectedCard.minimumTopUpAmount) {
                    if (vm.selectedCard.minimumTopUpAmount > vm.topUpAmount) {
                        abp.notify.error(app.localize('TopUpAmountCanNotBeLessThanMinimumTopUpAmount', vm.topUpAmount, vm.selectedCard.minimumTopUpAmount))
                        abp.message.warn(app.localize('TopUpAmountCanNotBeLessThanMinimumTopUpAmount', vm.topUpAmount, vm.selectedCard.minimumTopUpAmount));
                        errorFlag = true;
                        return false;
                    }
                }
                //#endregion

                if (vm.selectedCard.dailyLimit > 0 || vm.selectedCard.weeklyLimit > 0 || vm.selectedCard.monthlyLimit > 0 || vm.selectedCard.yearlyLimit > 0) {
                    if (vm.topUpAmount > 0) {
                        if (vm.selectedCard.isEmployeeCard)
                            abp.notify.error(app.localize('TopupNotAllowedForLimitCardEmployee', vm.depositAmountToBeCollected));
                        else
                            abp.notify.error(app.localize('TopupNotAllowedForLimitCard', vm.depositAmountToBeCollected));

                        abp.notify.info(app.localize('Deposit') + ' ' + app.localize('Amount') + ' ' + vm.depositAmountToBeCollected);
                        errorFlag = true;
                        return false;
                    }
                }



                if (vm.topUpAmount === 0 && vm.topupFlag) {
                    if (vm.supplementaryCardIssueFlag == true) {
                        //Do nothing
                    }
                    else if (vm.depositAmountToBeCollected > 0) {
                        //	Do nothing
                    }
                    else if (vm.selectedCard.dailyLimit > 0 || vm.selectedCard.weeklyLimit > 0 || vm.selectedCard.monthlyLimit > 0 || vm.selectedCard.yearlyLimit > 0) {
                        //	Do nothing
                    }
                    else {
                        abp.notify.error(app.localize('TopUp') + ' ' + '?');
                        $('#AmounttoProcess').focus();
                        errorFlag = true;
                    }
                }


                if (vm.refundFlag == true) {

                    if (vm.refundAmount != null && vm.refundAmount > 0) {
                        if (vm.checkRefundConstraints() == true)
                            errorFlag = true;
                    }

                    if (vm.refundAmount > 0) {
                        if (vm.refundAmount < vm.membercard.balance) {
                            vm.depositAmountToBeRefunded = 0;
                        }
                        else {
                            //if (vm.refundAmount != (vm.depositAmountToBeRefunded + vm.membercard.balance)) {
                            //    if (vm.membercard.employeeRefId == null) {
                            //        abp.notify.error(app.localize('RefundTotalRule'));
                            //        errorFlag = true;
                            //    }
                            //}
                        }
                    }
                }

                if (vm.checkErrorForExtraCard == true)
                    return false;

                //if (value.cardTypeRefId!=vm.membercard.cardTypeRefId)
                //{
                //	abp.notify.error(app.localize('MemberPrimaryCardTypeIsDifferentError'));
                //	vm.tryToIssueSupplementaryCardFlag = false;
                //}



                if (errorFlag)
                    return false;
                else
                    return true;

            };

            vm.checkRefundConstraints = function () {
                if (vm.refundAmount != null && vm.refundAmount > 0) {
                    if (vm.selectedCard.isAutoRechargeCard == true) {
                        if (vm.refundAmount != (vm.depositAmountToBeRefunded)) {
                            abp.notify.error(app.localize('AutoRechargeCardCanRefundOnlyDepositIfAny', vm.depositAmountToBeRefunded));
                            return true;
                        }
                        //	if (vm.depositNeedToRefundFlag == true)
                        //		abp.notify.error(app.localize('RefundCanNotBeGreaterThanBalancePlusDeposit', vm.refundAmount, 0, vm.depositAmountToBeRefunded));
                        //	else
                        //	{
                        //		abp.notify.error(app.localize('RefundCanNotBeGreaterThanBalance', vm.refundAmount, 0));
                        //		abp.notify.error(app.localize('AutoRechargeCardCanRefundOnlyDepositIfAny', vm.depositAmountToBeRefunded));
                        //	}
                        //	return true;
                        //}
                        //else if (vm.refundAmount < vm.depositAmountToBeRefunded) {
                        //	if (vm.depositNeedToRefundFlag == true)
                        //		abp.notify.error(app.localize('RefundShouldEqualToDepositAmount', vm.refundAmount, 0, vm.depositAmountToBeRefunded));
                        //	else {
                        //		abp.notify.error(app.localize('AutoRechargeCardCanRefundOnlyDepositIfAny', vm.depositAmountToBeRefunded));
                        //	}
                        //	return true;
                        //}
                        else
                            return false;
                    }
                    else {
                        if (vm.refundAmount > (vm.membercard.balance + vm.depositAmountToBeRefunded)) {
                            if (vm.depositNeedToRefundFlag == true)
                                abp.notify.error(app.localize('RefundCanNotBeGreaterThanBalancePlusDeposit', vm.refundAmount, vm.membercard.balance, vm.depositAmountToBeRefunded));
                            else
                                abp.notify.error(app.localize('RefundCanNotBeGreaterThanBalance', vm.refundAmount, vm.membercard.balance));

                            return true;
                        }
                        else
                            return false;
                    }
                }
            }

            vm.checkForSupplementary = function () {
                //if (vm.memberCardList.length > 0 && (vm.membercard.cardRefId == 0 || vm.membercard.cardRefId == null)) {
                if (vm.memberCardList.length > 0) {
                    vm.tryToIssueSupplementaryCardFlag = true;
                    angular.forEach(vm.memberCardList, function (value, key) {
                        if (value.cardRefId == vm.membercard.cardRefId) {
                            vm.tryToIssueSupplementaryCardFlag = false;
                        }

                    });

                    if (vm.tryToIssueSupplementaryCardFlag == true && vm.supplementaryCardIssueFlag == false && vm.onemoreprimarycardFlag == false) {
                        //abp.notify.error(app.localize('MemberAlreadyHavePrimaryCard'));
                        abp.notify.error(app.localize('DoYouWantToIssueSupplementaryCard'));
                        abp.notify.error(app.localize('DoYouWantToIssueOneMorePrimaryCard'));
                        return false;
                    }

                    if (vm.primaryCardRefId != null) {

                        return true;




                    }
                    else {
                        if (vm.onemoreprimarycardFlag == true)
                            return true
                        else {
                            abp.notify.error(app.localize('PrimaryCardNumber') + '?');
                            return false;
                        }
                    }
                }
                else {
                    return true;
                }
            };

            vm.cancel = function (argOption) {
                if (argOption == 1) {
                    abp.message.confirm(
                        app.localize('CancelEntry?'),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                $state.go('host.membercardedit', {
                                    id: null,
                                    shiftRefId: vm.shiftRefId
                                });
                            }
                        }
                    );
                }
                else {
                    $state.go('host.membercardedit', {
                        id: null,
                        shiftRefId: vm.shiftRefId
                    });
                }
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            function init() {
                membercardService.getMemberCardForEdit({
                    Id: vm.membercardId
                }).success(function (result) {
                    vm.membercard = result.memberCard;
                    if (vm.membercard.id == null)
                        vm.membercard.isPrimaryCard = true;

                    if (vm.membercard.id != null) {
                        vm.uilimit = null;
                    }
                    else {
                        vm.uilimit = 20;
                    }
                }).finally(function () {
                    $('#AmounttoProcess').focus();
                });
            }






















            init();




            vm.createSwipeMember = function () {
                addorEditMember(null);
            };


            vm.addorEditMember = function (argMemberId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/host/views/swipe/master/swipemember/swipemember.cshtml',
                    controller: 'host.views.swipe.master.swipemember.swipemember as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        swipememberId: function () {
                            return argMemberId == 0 ? null : argMemberId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    if (result != null) {
                        vm.fillSwipeMember();
                        vm.membercard.memberRefId = result.id;
                        vm.membercard.memberCode = result.memberCode;
                        $scope.memberfunc(result);

                    }
                });
            }


            vm.memberCardList = [];

















            $scope.memberfunc = function (val) {
                if (val == null)
                    return;




                if (vm.membercard.memberRefId != null && vm.membercard.memberRefId != '')
                    vm.membercard.memberRefId = val.id;

                vm.membercard.memberCode = val.memberCode;
                vm.membercard.name = val.name;
                vm.membercard.phoneNumber = val.phoneNumber;
                vm.membercard.emailId = val.emailId;

                vm.memberCardList = [];
                var primaryCardCount = 0;
                var alreadyPrimaryCardIdSet = false;
                if (vm.membercard.primaryCardRefId == null) {
                    alreadyPrimaryCardIdSet = false;
                }
                else {
                    alreadyPrimaryCardIdSet = true;
                }
                angular.forEach(vm.refMemberCards, function (value, key) {
                    if (value.memberRefId == vm.membercard.memberRefId) {
                        vm.memberCardList.push(value);
                        if (alreadyPrimaryCardIdSet == false) {
                            if (value.isPrimaryCard == true) {
                                vm.primaryCardRefId = value.cardRefId;
                                primaryCardCount++;
                            }
                        }

                    }
                });
                if (alreadyPrimaryCardIdSet == false) {
                    if (primaryCardCount > 1)
                        vm.primaryCardRefId = null;
                }

                vm.checkForSupplementary();
            };




            $scope.employeefunc = function (val) {


                if (val == null)
                    return;





                if (vm.membercard.employeeRefId != null && vm.membercard.employeeRefId != '')
                    vm.membercard.employeeRefId = val.id;













                vm.membercard.employeeRefName = val.employeeName;
                vm.membercard.memberCode = '';
                vm.membercard.name = '';
                vm.membercard.phoneNumber = '';
                vm.membercard.emailId = '';

                vm.memberCardList = [];
                var primaryCardCount = 0;
                var alreadyPrimaryCardIdSet = false;
                if (vm.membercard.primaryCardRefId == null) {
                    alreadyPrimaryCardIdSet = false;
                }
                else {
                    alreadyPrimaryCardIdSet = true;
                }

                angular.forEach(vm.refMemberCards, function (value, key) {
                    if (value.employeeRefId == vm.membercard.employeeRefId) {
                        vm.memberCardList.push(value);
                        if (alreadyPrimaryCardIdSet == false) {
                            if (value.isPrimaryCard == true) {
                                vm.primaryCardRefId = value.cardRefId;
                                primaryCardCount++;
                            }
                        }
                    }
                });
                if (alreadyPrimaryCardIdSet == false) {
                    if (primaryCardCount > 1)
                        vm.primaryCardRefId = null;
                }

                vm.checkForSupplementary();
            };

            vm.selectMemberBasedOnCardNumber = function () {

                if (vm.membercard == null) {
                    return
                }
                if (vm.membercard.cardNumber == '' || vm.membercard.cardNumber == null) {
                    vm.membercard.cardRefId = null;
                    return;
                }
                vm.topUpAmount = null;
                vm.refundAmount = null;
                if (vm.cardnumberblurneeded == false)
                    return;

                vm.memberCardList = [];
                vm.uilimit = null;
                vm.selectedCard = null;
                vm.refCards.some(function (refdata, refkey) {
                    if (refdata.cardNumber == vm.membercard.cardNumber) {
                        vm.selectedCard = refdata;
                        return true;
                    }
                });

                vm.cardnumberblurneeded = false;
                if (vm.selectedCard == null)       //  No More Records Found In Card , Means New Card Issue
                {
                    if (vm.canIssueCardDirectly == true) {
                        vm.membercard.cardRefId = null;
                        abp.notify.success(app.localize('NewCardToBeIssued'));
                        vm.batchProcess();
                        return;
                    }
                    else {
                        var autoActivationFlag = false;
                        var autoCardTypeRefId = null;
                        vm.refCardtypes.some(function (cardtype, refkey) {
                            if (cardtype.autoActivateRegExpression != null && cardtype.autoActivateRegExpression != '') {
                                var pattern = cardtype.autoActivateRegExpression.replace('\\\\', '\\');
                                var re = new RegExp(pattern);
                                if (vm.membercard.cardNumber.match(re)) {
                                    //alert('Successful match');
                                    autoActivationFlag = true;
                                    autoCardTypeRefId = cardtype.id;
                                    vm.batchProcess();
                                    return
                                }
                            }
                        });

                        if (autoActivationFlag && autoCardTypeRefId != null) {
                            vm.swipecard = {
                                cardNumber: vm.membercard.cardNumber,
                                CardTypeRefId: autoCardTypeRefId
                            };

                            vm.loadingCount++;
                            swipecardService.createOrUpdateSwipeCard({
                                swipeCard: vm.swipecard,
                                autoCreateFlag: true
                            }).success(function (result) {
                                abp.notify.info(app.localize('SwipeCard') + ' ' + app.localize('SavedSuccessfully'));
                                vm.refCards.push(result);
                            }).finally(function () {
                                vm.loadingCount--;
                                vm.cardnumberblurneeded = true;
                                vm.selectMemberBasedOnCardNumber();
                            });
                            $('#price-0').focus();
                            return;
                            //Insert Swipe Card Automatically
                        }
                        if (vm.membercard != null) {
                            $('#cardnumber').focus();
                            vm.membercard.cardRefId = null;
                            abp.notify.error(app.localize('CardNumberDoesNotExist', vm.membercard.cardNumber));
                            vm.membercard.cardNumber = null;
                        }
                        $('#cardnumber').focus();
                        return;
                    }
                }


                if (vm.selectedCard.canIssueWithOutMember == false) {
                    // To be Say only No Member can be used in Batch Update
                    abp.notify.error(app.localize('MemberRequiredCardCanNotBeManipulatedInBatchUpdate', vm.membercard.cardNumber));
                    vm.membercard.cardNumber = "";
                    $('#cardnumber').focus();
                    return;
                }

                vm.membercard.cardRefId = vm.selectedCard.id;
                vm.membercard.cardTypeRefId = vm.selectedCard.cardTypeRefId;
                vm.membercard.canIssueWithOutMember = vm.selectedCard.canIssueWithOutMember;
                vm.membercard.isEmployeeCard = vm.selectedCard.isEmployeeCard;

                if (vm.membercard.canIssueWithOutMember == true) {
                    vm.membercard.memberRefId = null;
                    vm.membercard.memberCode = null;
                    vm.membercard.id = null;
                }
                else {
                    // To be Say only No Member can be used in Batch Update
                    var i = 1;
                }

                vm.membercard.isBalanceGet = false;
                if (vm.refundFlag)
                    vm.getCardBalance();

                vm.uilimit = null;
                var selectedMemberCard = null;

                vm.refMemberCards.some(function (refdata, refkey) {
                    if (refdata.cardNumber == vm.membercard.cardNumber) {
                        selectedMemberCard = refdata;
                        return true;
                    }
                });

                if (selectedMemberCard != null) {
                    vm.depositAmountToBeCollected = 0;
                    vm.depositNeedToCollectFlag = false;
                    vm.membercard.memberRefId = selectedMemberCard.memberRefId;
                    vm.membercard.isPrimaryCard = selectedMemberCard.isPrimaryCard;
                    vm.membercard.primaryCardRefId = selectedMemberCard.primaryCardRefId;
                    vm.membercard.id = selectedMemberCard.id;
                    vm.primaryCardRefId = selectedMemberCard.primaryCardRefId;
                    vm.membercard.depositAmount = selectedMemberCard.depositAmount;
                    vm.membercard.employeeRefId = selectedMemberCard.employeeRefId;
                    vm.membercard.employeeRefName = '';

                    if (selectedMemberCard.employeeRefId != null) {
                        var selectedEmployee;
                        vm.refEmployees.some(function (refdata, refkey) {
                            if (refdata.id == vm.membercard.employeeRefId) {
                                vm.membercard.employeeRefName = refdata.employeeName;
                                selectedEmployee = refdata;
                                return true;
                            }
                        });
                        if (selectedEmployee != null) {
                            $scope.employeefunc(selectedEmployee);
                        }
                    }
                    else {
                        var selectedMember = null;
                        vm.refSwipeMembers.some(function (refdata, refkey) {
                            if (refdata.id == vm.membercard.memberRefId) {
                                selectedMember = refdata;
                                return true;
                            }
                        });

                        if (selectedMember != null) {
                            $scope.memberfunc(selectedMember);
                        }
                    }

                    $('#AmounttoProcess').focus();
                }
                else {
                    vm.membercard.memberRefId = null;
                    vm.membercard.id = null;
                    vm.membercard.isPrimaryCard = true;
                    vm.membercard.primaryCardRefId = null;
                    vm.membercard.depositAmount = 0;
                    vm.membercard.employeeRefId = null;
                    vm.membercard.employeeRefName = '';

                    //About Collecting Deposit
                    if (vm.selectedCard.depositAmount > 0 && vm.topupFlag) {
                        //abp.notify.info(app.localize('DepositNeedToCollect', vm.selectedCard.depositAmount));
                        vm.depositAmountToBeCollected = vm.selectedCard.depositAmount;
                        vm.depositNeedToCollectFlag = true;
                    }
                    else {
                        vm.depositAmountToBeCollected = 0;
                        vm.depositNeedToCollectFlag = false;
                    }
                    $scope.$broadcast('UiSelectDemo1');
                }

                vm.batchProcess();

            }


            vm.getCardBalance = function () {
                if (vm.membercard == null) {
                    return;
                }

                if (vm.membercard.cardRefId == null || vm.membercard.cardRefId == 0) {
                    return;
                }
                vm.loading = true;

                membercardService.apiGetSwipeCardCurrentBalance({
                    id: vm.membercard.cardRefId
                }).success(function (result) {
                    vm.loading = false;
                    //@@@@Pending   '   After Expiry What to do ?
                    if (result.errorMessage != '') {
                        //abp.notify.error(result.errorMessage);
                        if (vm.refundFlag) {
                            vm.membercard.cardNumber = null;
                            $('#cardnumber').focus();
                            vm.loading = false;
                            vm.saving = false;
                            vm.preInit();
                            return;
                        }
                        vm.loading = false;
                        vm.saving = false;
                        //                        return;
                    }
                    vm.membercard.balance = result.balance;
                    vm.membercard.depositBalance = result.depositBalance;
                    vm.membercard.isBalanceGet = true;
                    vm.membercard.isFirstTimeCard = result.isFirstTimeCard;


                    if (vm.refundFlag) {
                        if (vm.depositNeedToRefundFlag) {
                            vm.depositAmountToBeRefunded = result.depositBalance;
                        }
                        if (vm.isFullRefundFlag) {
                            vm.batchAmount = result.balance;
                        }
                        if (vm.batchAmount == null || vm.batchAmount == 0) {
                            vm.batchAmount = result.balance;
                            vm.batchAmount = parseFloat(vm.batchAmount).toFixed(2);
                            vm.refundAmount = vm.batchAmount;
                        }
                        else {
                            vm.batchAmount = parseFloat(vm.batchAmount).toFixed(2);
                            vm.refundAmount = vm.batchAmount;
                        }
                        vm.addCardPortion();

                    }
                    vm.loading = false;
                    vm.saving = false;
                });
            }

            //  Key Board Events

            //$('#cardnumber').on('keydown', function (e) {
            //    var keyCode = e.keyCode || e.which;
            //    if (keyCode === 13) {
            //        vm.selectMemberBasedOnCardNumber();
            //        if ((vm.membercard.memberRefId != null && vm.membercard.memberRefId > 0) || vm.membercard.canIssueWithOutMember == true) {
            //            //  Do Nothing now
            //            e.preventDefault();
            //            $('#cmdTopUp').focus();
            //        }
            //        else {
            //            e.preventDefault();
            //            $scope.$broadcast('UiSelectDemo1');
            //        }
            //    }
            //});

            vm.topupKeyPress = function ($event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    $event.preventDefault();
                    if (vm.membercard.topUpAmount == 0 || vm.membercard.topUpAmount == null || vm.membercard.topUpAmount == '')
                        $('#refund').focus();
                    else
                        $('#cmdsave').focus();
                }
            };

            vm.memberKeyPress = function ($event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    $event.preventDefault();
                    $('#cmdTopUp').focus();
                    //if (vm.membercard.topUpAmount == 0 || vm.membercard.topUpAmount == null || vm.membercard.topUpAmount == '')
                    //    $('#toput').focus();
                    //else
                    //    $('#cmdsave').focus();
                }
            };

            vm.employeeKeyPress = function ($event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    $event.preventDefault();
                    $('#cmdTopUp').focus();
                }
            };

            vm.primaryCardKeyPress = function ($event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    $event.preventDefault();

                    $('#cmdTopUp').focus();

                }
            };


            vm.priceKeyPress = function (objId, $event) {
                //if (vm.membercard.cardNumber == null || vm.membercard.cardNumber == '') {
                //    $event.preventDefault();
                //    abp.notify.error(app.localize('CardNumber') + ' ?');
                //    $('#cardnumber').focus();
                //    return;
                //}


                //if (vm.membercard.canIssueWithOutMember == false && vm.membercard.isEmployeeCard == false) {
                //    if (vm.membercard.memberRefId == null || vm.membercard.memberRefId == '' || vm.membercard.memberRefId == 0) {
                //        $event.preventDefault();
                //        abp.notify.error(app.localize('SelectMember'));
                //        $scope.$broadcast('UiSelectDemo1');
                //        return;
                //    }
                //}

                //if (vm.topupFlag == false && vm.refundFlag == false) {
                //    if (vm.membercard.balance == 0) {
                //        vm.clickTopUpButton();
                //        return;
                //    }
                //    $event.preventDefault();
                //    abp.notify.error(app.localize('TopUp') + '/' + app.localize('Refund') + '?');
                //    $('#cmdTopUp').focus();
                //    return;
                //}

                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    //alert(objId);
                    nextId = objId + 1;
                    while (nextId < vm.paymentList.length) {
                        if (vm.paymentList[nextId].showFlag == false)
                            nextId++;
                        else
                            break;

                        if (nextId >= vm.paymentList.length)
                            break;
                    }
                    if (nextId < vm.paymentList.length)
                        $('#price-' + nextId).focus();
                    else
                        $('#cmdsave').focus();
                }
            };
            //  End Key Board Events

            vm.calculateTotalAmount = function (data) {
                var tempTotalAmt = 0;

                if (data.length == 0 || vm.isUndefinedOrNull(data))
                    return;

                angular.forEach(data, function (value, key) {
                    if ($.isNumeric(value.amount)) {
                        tempTotalAmt = parseFloat(tempTotalAmt) + parseFloat(vm.isUndefinedOrNull(value.amount) ? 0 : parseFloat(value.amount));
                    }
                });

                vm.totalAmount = parseFloat(tempTotalAmt.toFixed(2));

                if (vm.topupFlag == true) {
                    //  vm.topUpAmount = vm.totalAmount - vm.depositAmountToBeCollected;
                    vm.refundAmount = 0;
                }
                else if (vm.refundFlag == true) {
                    vm.refundAmount = vm.totalAmount;
                    //if (vm.depositNeedToRefundFlag == true) {
                    //    vm.depositAmountToBeRefunded = vm.depositedTotalAmount;
                    //    vm.refundAmount = vm.refundAmount + vm.depositAmountToBeRefunded;
                    //}
                    vm.topUpAmount = 0;
                    vm.checkRefundConstraints();
                }






            }

            vm.clickTopUpButton = function () {
                vm.clickCancelPayment();
                if (vm.membercard.cardNumber == null || vm.membercard.cardNumber == '') {
                    abp.notify.error(app.localize('CardNumber') + ' ?');
                    $('#cardnumber').focus();
                    return;
                }

                if (vm.membercard.canIssueWithOutMember == false && vm.membercard.isEmployeeCard == false) {
                    if (vm.membercard.memberRefId == null || vm.membercard.memberRefId == '' || vm.membercard.memberRefId == 0) {
                        abp.notify.error(app.localize('SelectMember'));
                        $scope.$broadcast('UiSelectDemo1');
                        return;
                    }
                }

                //vm.paymentList = vm.initPaymentList;
                vm.topupFlag = true;
                vm.refundFlag = false;
                vm.refundAmount = 0;

                angular.forEach(vm.paymentList, function (value, key) {
                    value.showFlag = value.topupAcceptFlag;
                    if (value.showFlag == false) {
                        value.amount = '';
                    }
                });

                vm.calculateTotalAmount(vm.paymentList);

                $('#price-0').focus();

            }

            vm.clickRefundButton = function () {
                angular.forEach(vm.initPaymentList, function (value, key) {
                    value.amount = '';
                });
                angular.forEach(vm.paymentList, function (value, key) {
                    value.amount = '';
                });

                //if (vm.membercard.isPrimaryCard == false) {
                //    abp.notify.error(app.localize('CanPrimaryCardOnlyRefundGiven'));
                //    abp.notify.error(app.localize('ThisIsSupplementaryCard'));
                //    $('#cardnumber').focus();
                //    return;
                //}
                //if (vm.membercard.canIssueWithOutMember == false && vm.membercard.isEmployeeCard == false) {
                //    if (vm.membercard.memberRefId == null || vm.membercard.memberRefId == '' || vm.membercard.memberRefId == 0) {
                //        abp.notify.error(app.localize('SelectMember'));
                //        $scope.$broadcast('UiSelectDemo1');
                //        return;
                //    }
                //}

                //if (vm.selectedCard != null) {
                //    if (vm.selectedCard.refundAllowed == false) {
                //        abp.notify.error(app.localize('RefundNotAllowedForThisCard', vm.selectedCard.cardNumber));
                //        abp.message.error(app.localize('RefundNotAllowedForThisCard', vm.selectedCard.cardNumber));
                //        return;
                //    }

                //}



                //vm.depositAmountToBeRefunded = 0;

                //vm.loading = true;
                //membercardService.apiGetSwipeCardCurrentDepositBalance({
                //    id: vm.membercard.cardRefId
                //}).success(function (result) {
                //    if (result.errorMessage != '') {
                //        abp.notify.error(result.errorMessage);
                //        vm.loading = false;
                //        return;
                //    }
                //    if (vm.paymentList.length > 0 && (vm.membercard.balance > 0 || vm.membercard.depositBalance > 0)) {
                //        var firstIndex = -1;
                //        angular.forEach(vm.paymentList, function (value, key) {
                //            value.showFlag = value.refundAcceptFlag;
                //            if (value.showFlag == false) {
                //                value.amount = '';
                //            }
                //            else if (firstIndex == -1) {
                //                firstIndex = key;
                //            }
                //        });
                //        if (firstIndex != -1) {

                //            vm.topupFlag = false;
                //            vm.refundFlag = true;

                //            vm.topUpAmount = 0;
                //            vm.depositedTotalAmount = result.totalDeposit;

                //            if (vm.membercard.balance == 0) {
                //                if (vm.depositNeedToRefundFlag == false)
                //                    vm.depositNeedToRefundFlag = true;
                //            }

                //            if (vm.depositNeedToRefundFlag)
                //                vm.depositAmountToBeRefunded = result.totalDeposit;
                //            else
                //                vm.depositAmountToBeRefunded = 0;

                //            if (vm.selectedCard.isAutoRechargeCard == true) {
                //                vm.paymentList[firstIndex].amount = vm.depositAmountToBeRefunded;
                //            }
                //            else {
                //                vm.paymentList[firstIndex].amount = vm.membercard.balance + vm.depositAmountToBeRefunded;
                //            }

                //            vm.calculateTotalAmount(vm.paymentList);

                //            $('#price-0').focus();
                //        }
                //    }
                //    vm.loading = false;
                //});
            }

            vm.clickCancelPayment = function () {
                vm.totalAmount = null;
                vm.topUpAmount = 0;
                vm.refundAmount = 0;
                vm.totalAmount = 0;
                angular.forEach(vm.initPaymentList, function (value, key) {
                    value.amount = '';
                });
                angular.forEach(vm.paymentList, function (value, key) {
                    value.amount = '';
                });
            }

            vm.cardTransactionDataShown = function (argOption) {
                var modalInstance1 = $modal.open({
                    templateUrl: '~/App/host/views/swipe/transaction/membercard/drillDownLevel.cshtml',
                    controller: 'host.views.swipe.transaction.membercard.drillDownLevel as vm',
                    backdrop: 'static',
                    resolve: {
                        cardNumber: function () {
                            return '';
                        },
                        cardRefId: function () {
                            return argOption;
                        }

                    }
                });

                modalInstance1.result.then(function (result) {
                    //vm.getAll();
                });
            }

            vm.viewMemberDetails = function (myObj) {
                var modalInstance1 = $modal.open({
                    templateUrl: '~/App/host/views/swipe/transaction/membercard/viewMember.cshtml',
                    controller: 'host.views.swipe.transaction.membercard.viewMember as vm',
                    backdrop: 'static',
                    resolve: {
                        memberCode: function () {
                            return '';
                        },
                        memberRefId: function () {
                            return myObj;
                        },
                        cardRefId: function () {
                            return vm.membercard.cardRefId;
                        },

                    }

                });


                modalInstance1.result.then(function (result) {
                    //vm.getAll();
                });
            };

            vm.redeem = function () {


                membercardService.getAllSwipeCardsForMember({
                    //memberRefId: vm.membercard.memberRefId
                    employeeRefId: vm.membercard.employeeRefId
                }).success(function (result) {
                    vm.loading = false;
                    vm.a = result;
                });

                //  	membercardService.apiUpdateSwipeCardLedger({
                //  		ledgerTime : moment(new Date()),
                //  		cardNumber: vm.membercard.cardNumber,
                //  		amountDebitFromSwipeCard: 100,
                //  		transactionType: 4,
                //  		description: 'AutoDebit',
                //ticketNumber : 1
                //  	}).success(function (result) {
                //  		vm.loading = false;
                //  		abp.notify.error('Redeemed');
                //  	});
            }

            vm.pageRefresh = function () {
                vm.loadingCount++;
                vm.currentLanguage = abp.localization.currentLanguage;
                location.href = abp.appPath + 'Localization/ChangeCulture?cultureName=' + vm.currentLanguage.name + '&returnUrl=' + window.location.href;
                vm.loadingCount--;
            }

            hotKeys.add({
                combo: 'alt++',
                allowIn: ['BUTTON', 'INPUT'],
                description: app.localize('TopUp'),
                callback: function (event, hotkey) {
                    vm.clickTopUpButton();
                }
            });

            hotKeys.add({
                combo: 'alt+-',
                allowIn: ['BUTTON', 'INPUT'],
                description: app.localize('Refund'),
                callback: function (event, hotkey) {
                    vm.clickRefundButton();
                }
            });


            hotKeys.add({
                combo: 'esc',
                allowIn: ['BUTTON', 'INPUT'],
                description: app.localize('Cancel') + ' ' + app.localize('Payment') + ' ' + app.localize('Entry'),
                callback: function (event, hotkey) {
                    vm.clickCancelPayment();
                }
            });

            hotKeys.add({
                combo: 'alt+*',
                allowIn: ['BUTTON', 'INPUT'],
                description: app.localize('PrintLast'),
                callback: function (event, hotkey) {
                    vm.printLastTransaction();
                }
            });

            vm.clear = function (argOption) {
                if (argOption == 1) {
                    abp.message.confirm(
                        app.localize('ClearEntry?'),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                vm.preInit();
                                init();
                                $('#cardnumber').focus();
                            }
                        }
                    );
                }
                else {
                    vm.preInit();
                    init();
                    $('#cardnumber').focus();
                }
            };

            vm.printLastTransaction = function () {
                $('#printButton').trigger('click');
            }

            vm.topupByApi = function () {
                vm.saving = true;

                vm.swipePaymentTenderList = [];

                angular.forEach(vm.paymentList, function (value, key) {
                    if (value.amount > 0) {
                        vm.swipePaymentTenderList.push({
                            'paymentTypeId': value.id,
                            'tenderedAmount': value.amount,
                            'amount': value.amount,
                            'paymentUserName': vm.currentUserName,
                        });
                    }
                });

                membercardService.apiTopUpCard({
                    cardRefId: vm.membercard.cardRefId,
                    topUpAmount: vm.topUpAmount,
                    swipePaymentTenderList: vm.swipePaymentTenderList
                }).success(function (result) {
                    if (result.errorMessage.length > 0) {
                        abp.notify.error(result.errorMessage);
                        return;
                    }
                    abp.notify.info(app.localize('MemberCard') + ' ' + app.localize('SavedSuccessfully'));
                    vm.preInit();
                    init();
                    $('#cardnumber').focus();
                    //vm.cancel(0);
                }).finally(function () {
                    vm.saving = false;
                });
            }

            vm.checkErrorForExtraCard = function () {
                return true;
                //if (vm.memberCardList.length > 0) {
                //	angular.forEach(vm.memberCardList, function (value, key) {
                //		if (value.cardTypeRefId != vm.membercard.cardTypeRefId) {
                //			abp.notify.error(app.localize('MemberPrimaryCardTypeIsDifferentError'));
                //			return true;
                //		}
                //	});
                //}
                //return false;
            }

            //  End Of All

            //-----
            vm.batchProcess = function () {
                if (vm.topupFlag) {
                    if (vm.batchAmount == null || vm.batchAmount == 0) {
                        abp.notify.error(app.localize('Amount'));
                        $('#AmounttoProcess').focus();
                        return;
                    }

                    vm.topUpAmount = vm.batchAmount;
                    vm.addCardPortion();
                }

            };

            vm.removeCardPortion = function (index) {
                vm.cardList.splice(index, 1);
                vm.totalDeposit = 0;
                vm.totalBatchAmount = 0;

                angular.forEach(vm.cardList, function (value, key) {
                    if (vm.topupFlag) {
                        vm.totalDeposit = parseFloat(vm.totalDeposit) + parseFloat(value.depositCollectedAmount);
                        vm.totalBatchAmount = parseFloat(vm.totalBatchAmount) + parseFloat(value.topUpAmount);
                    }
                    else {
                        vm.totalDeposit = parseFloat(vm.totalDeposit) + parseFloat(value.depositRefundedAmount);
                        vm.totalBatchAmount = parseFloat(vm.totalBatchAmount) + parseFloat(value.refundAmount);
                    }
                });

                var depositAmountToBeCollected = angular.copy(vm.depositAmountToBeCollected);
                var depositAmountToBeRefunded = angular.copy(vm.depositAmountToBeRefunded);

                if (vm.topupFlag) {
                    vm.totalDeposit = parseFloat(vm.totalDeposit); //+ parseFloat(depositAmountToBeCollected);
                    vm.totalBatchAmount = parseFloat(vm.totalBatchAmount);//+ parseFloat(topUpAmount);
                }
                else {
                    vm.totalDeposit = parseFloat(vm.totalDeposit);//+ parseFloat(depositAmountToBeRefunded);
                    vm.totalBatchAmount = parseFloat(vm.totalBatchAmount);// + parseFloat(refundAmount);
                }

                vm.totalDepositAndBatchAmount = parseFloat(vm.totalDeposit) + parseFloat(vm.totalBatchAmount);

                //vm.loading = true;
                //abp.message.confirm(
                //    app.localize('CancelEntry?'),
                //    function (isConfirmed) {
                //        if (isConfirmed) {
                //            vm.cardList.splice(index, 1);
                //            vm.loading = false;
                //        }
                //        else {
                //            vm.loading = false;
                //        }
                //    }
                //);

            };

            vm.cardNumberKeyPress = function ($event) {
                if (vm.cardList.length == 0) {
                    var keyCode = $event.which || $event.keyCode;
                    if (keyCode == 13) {
                        $event.preventDefault();
                        $('#AmounttoProcess').focus();
                    }
                }
            };

            vm.amountKeyPress = function ($event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    $event.preventDefault();
                    $('#cardnumber').focus();
                }
            };

            vm.checkBatchAmount = function () {
                if (vm.batchAmount == null || vm.batchAmount == 0) {
                    $('#AmounttoProcess').focus();
                }
                else {
                    $('#cardnumber').focus();
                }

            }

            vm.addCardPortion = function () {
                vm.saving = true;
                vm.loading = true;

                if (vm.membercard.cardNumber == '' || vm.membercard.cardNumber == null) {
                    vm.saving = false;
                    vm.loading = false;
                    return;
                }
                vm.invalidStatus = false;
                if (vm.membercard.canIssueWithOutMember == false && vm.membercard.isEmployeeCard == false) {
                    if (vm.membercard.memberCode == '' || vm.membercard.memberCode == null || vm.membercard.memberRefId == null || vm.membercard.memberRefId == '') {
                        abp.notify.error(app.localize('SelectMember'));
                        vm.saving = false;
                        vm.loading = false;
                        return;
                    }
                }

                if (vm.checkForSupplementary() == false) {
                    vm.saving = false;
                    vm.loading = false;
                    return;
                }

                if (vm.existAllElements() == false) {
                    vm.saving = false;
                    vm.loading = false;
                    return;
                }

                vm.errorflag = false;
                vm.currentUserName = appSession.user.name;

                vm.membercard.swipePaymentTenderList = [];

                if (vm.errorflag == true) {
                    vm.saving = false;
                    vm.loading = false;
                    return;
                }

                if (vm.onemoreprimarycardFlag == true) {
                    vm.membercard.isPrimaryCard = true;
                }
                else if (vm.supplementaryCardIssueFlag == true) {
                    vm.membercard.isPrimaryCard = false;
                    if (vm.primaryCardRefId == null) {
                        abp.notify.error(app.localize('PrimaryCardNumber') + '?');
                        vm.saving = false;
                        vm.loading = false;
                        return;
                    }
                    vm.membercard.primaryCardRefId = vm.primaryCardRefId;
                }

                vm.membercard.shiftRefId = vm.shiftRefId;

                if (vm.membercard.depositAmount == 0 && vm.depositAmountToBeCollected > 0 && vm.depositNeedToCollectFlag)
                    vm.membercard.depositAmount = vm.depositAmountToBeCollected;

                if (vm.refundFlag == true) {
                    if (vm.refundAmount > 0) {
                        if (vm.refundAmount < vm.membercard.balance) {
                            vm.depositAmountToBeRefunded = 0;
                        }
                        else {
                            //if (vm.refundAmount != (/*vm.depositAmountToBeRefunded + */vm.membercard.balance)) {
                            //    if (vm.membercard.employeeRefId == null) {
                            //        abp.notify.error(app.localize('RefundTotalRule'));
                            //        vm.saving = false;
                            //        vm.loading = false;
                            //        return;
                            //    }
                            //}
                            //else {
                            //    vm.refundAmount = vm.refundAmount - vm.depositAmountToBeRefunded;
                            //}
                        }
                    }
                }

                if (vm.depositAmountToBeRefunded > 0 && vm.depositAmountToBeCollected > 0) {
                    abp.notify.error(app.localize('DepositCollectedOrRefunded'));
                    vm.saving = false;
                    vm.loading = false;
                    return;
                }
                if (vm.topUpAmount > 0 && vm.refundAmount > 0) {
                    abp.notify.error(app.localize('TopUp') + ' / ' + app.localize('Refund') + ' ?');
                    vm.saving = false;
                    vm.loading = false;
                    return;
                }

                if (vm.membercard.isBalanceGet == true) {
                    vm.addMemberCardIntoCardList();
                }
                else {
                    if (vm.membercard == null) {
                        return;
                    }

                    if (vm.membercard.cardRefId == null || vm.membercard.cardRefId == 0) {
                        return;
                    }

                    vm.loading = true;
                    vm.saving = true;
                    membercardService.apiGetSwipeCardCurrentBalance({
                        id: vm.membercard.cardRefId
                    }).success(function (result) {
                        vm.loading = false;
                        //@@@@Pending   '   After Expiry What to do ?
                        if (result.errorMessage != '') {
                            abp.notify.error(result.errorMessage);
                            if (vm.refundFlag) {
                                vm.membercard.cardNumber = null;
                                $('#cardnumber').focus();
                                vm.loading = false;
                                vm.saving = false;
                                preInit();
                                return;
                            }
                            //vm.membercard.balance = result.balance;
                            vm.loading = false;
                            vm.saving = false;
                        }
                        vm.membercard.balance = result.balance;
                        vm.membercard.depositBalance = result.depositBalance;
                        vm.membercard.isBalanceGet = true;
                        vm.membercard.isFirstTimeCard = result.isFirstTimeCard;
                        if (vm.refundFlag) {
                            if (vm.depositNeedToRefundFlag) {
                                vm.depositAmountToBeRefunded = result.depositBalance;
                            }
                            if (vm.isFullRefundFlag) {
                                vm.batchAmount = result.balance;
                            }
                            if (vm.batchAmount == null || vm.batchAmount == 0) {
                                vm.batchAmount = result.balance;
                                vm.batchAmount = parseFloat(vm.batchAmount).toFixed(2);
                                vm.refundAmount = vm.batchAmount;
                            }
                            else {
                                vm.batchAmount = parseFloat(vm.batchAmount).toFixed(2);
                                vm.refundAmount = vm.batchAmount;
                            }
                        }
                        vm.addMemberCardIntoCardList();
                    }).finally(function () {
                        vm.loading = false;
                        vm.saving = false;
                    });

                }


            };

            vm.addMemberCardIntoCardList = function () {

                if (vm.refundFlag) {
                    if (vm.selectedCard != null) {
                        if (vm.selectedCard.refundAllowed == false) {
                            abp.notify.error(app.localize('RefundNotAllowedForThisCard', vm.selectedCard.cardNumber));
                            abp.message.error(app.localize('RefundNotAllowedForThisCard', vm.selectedCard.cardNumber));
                            return;
                        }
                    }
                    if (vm.membercard.refundAllowed == false) {
                        abp.notify.error(app.localize('RefundNotAllowedForThisCard', vm.membercard.cardNumber));
                        abp.message.error(app.localize('RefundNotAllowedForThisCard', vm.membercard.cardNumber));
                        return;
                    }
                    if (vm.membercard.isPrimaryCard == false) {
                        abp.notify.error(app.localize('CanPrimaryCardOnlyRefundGiven'));
                        abp.notify.error(app.localize('ThisIsSupplementaryCard'));
                        $('#cardnumber').focus();
                        return;
                    }
                    if (vm.membercard.canIssueWithOutMember == false && vm.membercard.isEmployeeCard == false) {
                        if (vm.membercard.memberRefId == null || vm.membercard.memberRefId == '' || vm.membercard.memberRefId == 0) {
                            abp.notify.error(app.localize('SelectMember'));
                            $scope.$broadcast('UiSelectDemo1');
                            return;
                        }
                    }
                }
                else if (vm.topupFlag) {
                    if (vm.batchAmount == 0 || vm.batchAmount == null) {
                        abp.notify.error(app.localize('Check') + ' ' + app.localize('TopUp') + ' ' + app.localize('Amount'));
                        abp.message.info(app.localize('Check') + ' ' + app.localize('TopUp') + ' ' + app.localize('Amount'));
                        $('#AmounttoProcess').focus();
                        return;
                    }
                }

                vm.loading = true;
                vm.saving = true;

                var forLoopFlag = true;
                vm.totalDeposit = 0;
                vm.totalBatchAmount = 0;


                angular.forEach(vm.cardList, function (value, key) {
                    if (forLoopFlag) {
                        if (angular.equals(vm.membercard.cardNumber, value.memberCard.cardNumber)) {
                            $('#cardnumber').focus();
                            abp.notify.error(vm.membercard.cardNumber + ' ' + app.localize('CardNumber') + ' ' + app.localize('AlreadyExists'));
                            abp.message.warn(vm.membercard.cardNumber + ' ' + app.localize('CardNumber') + ' ' + app.localize('AlreadyExists'));
                            forLoopFlag = false;
                            vm.membercard.cardNumber = null;
                            $('#cardnumber').focus();
                        }
                    }
                    if (vm.topupFlag) {
                        vm.totalDeposit = parseFloat(vm.totalDeposit) + parseFloat(value.depositCollectedAmount);
                        vm.totalBatchAmount = parseFloat(vm.totalBatchAmount) + parseFloat(value.topUpAmount);
                    }
                    else {
                        vm.totalDeposit = parseFloat(vm.totalDeposit) + parseFloat(value.depositRefundedAmount);
                        vm.totalBatchAmount = parseFloat(vm.totalBatchAmount) + parseFloat(value.refundAmount);
                    }
                });

                if (!forLoopFlag) {
                    vm.saving = false;
                    vm.loading = false;
                    vm.preInit();
                    return;
                }

                var membercard = angular.copy(vm.membercard);
                var topUpAmount = angular.copy(vm.topUpAmount);
                var refundAmount = angular.copy(vm.refundAmount);
                var depositAmountToBeCollected = angular.copy(vm.depositAmountToBeCollected);
                var depositAmountToBeRefunded = angular.copy(vm.depositAmountToBeRefunded);

                if (vm.topupFlag) {
                    vm.totalDeposit = parseFloat(vm.totalDeposit) + parseFloat(depositAmountToBeCollected);
                    vm.totalBatchAmount = parseFloat(vm.totalBatchAmount) + parseFloat(topUpAmount);
                }
                else {
                    vm.totalDeposit = parseFloat(vm.totalDeposit) + parseFloat(depositAmountToBeRefunded);
                    vm.totalBatchAmount = parseFloat(vm.totalBatchAmount) + parseFloat(refundAmount);
                }

                vm.totalDepositAndBatchAmount = parseFloat(vm.totalDeposit) + parseFloat(vm.totalBatchAmount);

                vm.cardList.push({
                    memberCard: membercard,
                    topUpAmount: topUpAmount,
                    refundAmount: refundAmount,
                    depositCollectedAmount: depositAmountToBeCollected,
                    depositRefundedAmount: depositAmountToBeRefunded
                });

                vm.preInit();
                vm.loading = false;
                vm.saving = false;
            }

            vm.setPaymentProcess = function () {
                vm.finalPaymentFlag = true;
                $('#price-0').focus();
            }

            vm.save = function () {

                vm.totalPayment = 0;
                if (vm.cardList.length == 0) {
                    $('#cardnumber').focus();
                    return;
                }
                if (vm.totalDepositAndBatchAmount == 0) {
                    $('#price-0').focus();
                    return;
                }
                var batchPaymentTenderList = [];
                angular.forEach(vm.paymentList, function (value, key) {
                    if (value.amount > 0) {
                        batchPaymentTenderList.push({
                            'paymentTypeId': value.id,
                            'tenderedAmount': value.amount,
                            'amount': value.amount,
                            'paymentUserName': vm.currentUserName,
                        });
                        vm.totalPayment = parseFloat(vm.totalPayment) + parseFloat(value.amount);
                    }
                });
                if (vm.totalPayment == 0) {
                    $('#price-0').focus();
                    return;
                }

                if (vm.totalPayment != vm.totalDepositAndBatchAmount) {
                    $('#price-0').focus();
                    abp.notify.error(app.localize('TotalBatchAmountAndPaymentAmountNotMatched', vm.totalDepositAndBatchAmount, vm.totalPayment));
                    $('#price-0').focus();
                    abp.message.error(app.localize('TotalBatchAmountAndPaymentAmountNotMatched', vm.totalDepositAndBatchAmount, vm.totalPayment));
                    $('#price-0').focus();
                    vm.saving = false;
                    vm.loading = false;
                    return;
                }


                var batchTopUpAmount = 0;
                var batchRefundAmount = 0;
                var batchDepositCollectedAmount = 0;
                var batchDepositRefundedAmount = 0;


                if (vm.topupFlag) {
                    batchTopUpAmount = vm.totalBatchAmount;
                    batchDepositCollectedAmount = vm.totalDeposit;
                }
                else {
                    batchRefundAmount = vm.totalBatchAmount;
                    batchDepositRefundedAmount = vm.totalDeposit;
                }

                vm.saving = true;
                vm.loading = true;

                membercardService.batchUpdateMemberCard({
                    cardList: vm.cardList,
                    batchTopUpAmount: batchTopUpAmount,
                    batchRefundAmount: batchRefundAmount,
                    batchDepositCollectedAmount: batchDepositCollectedAmount,
                    batchDepositRefundedAmount: batchDepositRefundedAmount,
                    batchPaymentTenderList: batchPaymentTenderList
                }).success(function (result) {
                    abp.notify.info(app.localize('MemberCard') + ' ' + app.localize('SavedSuccessfully'));
                    vm.printFlag = true;
                    vm.transactionIdList = '';
                    if (result.length > 0) {
                        angular.forEach(result, function (value, key) {
                            if (vm.transactionIdList == '' && (key + 1) == result.length) {
                                vm.transactionIdList = value.id;
                            }
                            else if (vm.transactionIdList == '' && (key + 1) < result.length) {
                                vm.transactionIdList = vm.transactionIdList + value.id + ',';
                            }
                            else if (key + 1 == result.length) {
                                vm.transactionIdList = vm.transactionIdList + value.id;
                            }
                            else {
                                vm.transactionIdList = vm.transactionIdList + value.id + ',';
                            }
                        });
                    }

                    var a = $('#transactionId').val();

                    $('#transactionId').val(vm.transactionIdList);
                    var b = $('#transactionId').val();
                    $('#printButton').trigger('click');
                    vm.cardList = [];
                    vm.preInit();
                    init();
                    $('#cardnumber').focus();
                    abp.notify.info(app.localize('Batch') + ' ' + app.localize('Update') + ' ' + app.localize('Done') + ' ' + app.localize('Sucessfully'));
                    abp.message.info(app.localize('Batch') + ' ' + app.localize('Update') + ' ' + app.localize('Done') + ' ' + app.localize('Sucessfully'));
                    vm.cancel(0);
                }).finally(function () {
                    vm.saving = false;
                });
            };


            //----- Batch End 
        }
    ]);
})();

