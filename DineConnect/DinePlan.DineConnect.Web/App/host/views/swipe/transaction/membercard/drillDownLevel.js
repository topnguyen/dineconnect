﻿(function () {
    appModule.controller('host.views.swipe.transaction.membercard.drillDownLevel', [
        '$scope', '$uibModalInstance', 'cardNumber', 'cardRefId', 'abp.services.app.memberCard',
        function ($scope, $modalInstance, cardNumber, cardRefId, membercardService) {

            var vm = this;
            vm.memberDetails = null;
            vm.detailData = [];
            vm.cardRefId = cardRefId;
            vm.lastNRecords = 10;

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };


            $scope.formats = ['YYYY-MMM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            var todayAsString = moment().format('YYYY-MM-DD');
            $('input[name="reportDate"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                showDropdowns: true,
                startDate: todayAsString,
                endDate: todayAsString
            });


            vm.dateRangeOptions = app.createDateRangePickerOptions();

            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.fillTransaction = function () {
                vm.detailData = [];
                vm.memberDetails = null;

                if (cardRefId == 0 || cardRefId == null) {
                    abp.notify.error(app.localize('NoRecordsFound'));
                    $modalInstance.dismiss();
                    return;
                }

                var StartDate = null;
                var EndDate = null;
                if (vm.datefilterFlag) {
                    StartDate = moment(vm.dateRangeModel.startDate).format($scope.format);
                    EndDate = moment(vm.dateRangeModel.endDate).format($scope.format);
                }

                if (vm.isUndefinedOrNull(vm.lastNRecords)) {
                    vm.lastNRecords = 10;
                }

                vm.loading = true;

                membercardService.getCardTransactionList(
                    {
                        cardRefId: vm.cardRefId,
                        startDate: StartDate,
                        endDate: EndDate,
                        lastNRecord: vm.lastNRecords
                    }).success(function (result) {
                        if (result != null) {
                            vm.memberDetails = result;

                            vm.memberDetails.cardDetails.some(function (refdata, refkey) {
                                if (refdata.cardRefId == vm.cardRefId) {
                                    vm.cardDetail = refdata;
                                    vm.memberCardList = vm.cardDetail.memberCards;
                                    vm.detailData = vm.cardDetail.cardTransactionDetails;
                                    return true;
                                }

                                refdata.memberCards.some(function (memdata, memkey) {
                                    if (memdata.cardRefId == vm.cardRefId) {
                                        vm.cardDetail = refdata;
                                        vm.memberCardList = vm.cardDetail.memberCards;
                                        vm.detailData = vm.cardDetail.cardTransactionDetails;
                                        return true;
                                    }
                                });

                                if (vm.detailData != null)
                                    return true;

                            });

                        }
                    }).finally(function () {
                        vm.loading = false;
                    });
            }

            vm.fillTransaction();


            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();