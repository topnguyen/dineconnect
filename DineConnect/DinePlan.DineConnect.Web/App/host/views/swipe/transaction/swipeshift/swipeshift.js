﻿(function () {
    appModule.controller('host.views.swipe.transaction.swipeshift.swipeshift', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.swipeShift', 'appSession', 'abp.services.app.swipePaymentType', '$rootScope',
        function ($scope, $state, $stateParams, $modal, swipeshiftService, appSession, swipepaymenttypeService, $rootScope) {
            var vm = this;
            /* eslint-disable */
            vm.saving = false;
            vm.swipeshift = null;
            vm.shiftInOutStatus = null;
            vm.tenderOpenAmount = null;
            vm.tenderCloseAmount = null;
            vm.tenderDifferenceAmount = null;
            vm.openTotalAmount = null;
            vm.shiftTotalCashInAmount = null;
            vm.shiftTotalCashOutAmount = null;

            vm.alertMessageFlag = false;
            vm.alertViewFlag = false;

            vm.permissions = {
                blindShift: abp.auth.hasPermission('Pages.Host.Swipe.Transaction.SwipeShift.BlindShift'),
            };

            $scope.existall = true;


            vm.cancel = function () {
                $state.go('host.swipeshift');
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            vm.currentUserName = appSession.user.name;
            vm.currentUserId = appSession.user.id;

            vm.swipeshiftId = $stateParams.id;
            vm.operationStatus = $stateParams.operationStatus;

            if (vm.operationStatus == app.localize('StartShift'))
                vm.shiftInOutStatus = 0;
            else if (vm.operationStatus == app.localize('StopShift'))
                vm.shiftInOutStatus = 1;
            else {
                abp.notify.error(app.localize('ShiftStatusError'));
                vm.cancel();
                return;
            }



            vm.save = function () {
                vm.saving = true;

                vm.swipeshift.userId = vm.currentUserId;
                vm.swipeShiftTenderList = [];

                if (vm.shiftInOutStatus == 0) {
                    angular.forEach(vm.openPaymentList, function (value, key) {
                        if (value.tenderedAmount > 0) {
                            vm.swipeShiftTenderList.push({
                                'shiftRefId': null,
                                'shiftInOutStatus': vm.shiftInOutStatus,
                                'paymentTypeId': value.id,
                                'tenderedAmount': value.tenderedAmount,
                                'actualAmount': 0,
                            });
                        }
                    });
                    vm.swipeshift.tenderOpenAmount = vm.tenderOpenAmount;
                }
                else if (vm.closePaymentList.length > 0 && vm.shiftInOutStatus == 1) {
                    angular.forEach(vm.closePaymentList, function (value, key) {
                        if (value.tenderedAmount > 0) {
                            vm.swipeShiftTenderList.push({
                                'shiftRefId': vm.swipeshift.id,
                                'shiftInOutStatus': vm.shiftInOutStatus,
                                'paymentTypeId': value.id,
                                'actualAmount': value.actualAmount,
                                'tenderedAmount': value.tenderedAmount,

                            });
                        }
                    });
                    vm.swipeshift.userTenderEntered = vm.tenderCloseAmount;
                    vm.swipeshift.tenderCloseAmount = vm.tenderCloseAmount;
                    vm.swipeshift.excessShortageAmount = vm.tenderDifferenceAmount;
                    if (vm.tenderDifferenceAmount > 0)
                        vm.swipeshift.excessShortageStatus = app.localize('Excess');
                    else if (vm.tenderDifferenceAmount < 0)
                        vm.swipeshift.excessShortageStatus = app.localize('Shortage');
                    else
                        vm.swipeshift.excessShortageStatus = app.localize('Equal');

                    if (vm.tenderDifferenceAmount != 0 && vm.alertViewFlag == false) {
                        if (vm.permissions.blindShift == true) {
                            vm.alertMessage = app.localize("BlindShiftExcessShortageAlertMessage");
                            abp.notify.error(app.localize('Confirm') + "?");
                        }
                        else {
                            vm.alertMessage = app.localize("ShiftExcessShortageAlertMessage", vm.tenderDifferenceAmount, vm.swipeshift.excessShortageStatus);
                            abp.notify.error(app.localize(vm.swipeshift.excessShortageStatus) + ' ' + app.localize('Confirm') + "?");
                        }
                        vm.alertMessageFlag = true;
                        vm.saving = false;
                        return;
                    }
                }
                else {
                    abp.notify.error(app.localize('ShiftStatusError'));
                    vm.saving = false;
                    return;
                }


                if (vm.swipeShiftTenderList.length == 0) {
                    if (vm.shiftInOutStatus == 0)
                        abp.notify.error(app.localize('ShiftOpenTenderInformationMissing'));
                    else
                        abp.notify.error(app.localize('ShiftCloseTenderInformationMissing'));

                    vm.saving = false;
                    return;
                }

                vm.swipeshift.swipeShiftTenderList = vm.swipeShiftTenderList;



                swipeshiftService.createOrUpdateSwipeShift({
                    swipeShift: vm.swipeshift
                }).success(function () {
                    if (vm.shiftInOutStatus == 0) {
                        abp.notify.info(app.localize('StartShift') + ' ' + app.localize('Successfully'));
                        $state.go('host.swipeshift');
                    }
                    else {
                        abp.notify.info(app.localize('StopShift') + ' ' + app.localize('Successfully'));
                        $state.go('host.swipeshift');
                    }
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.refundFlag = true;

            vm.refPaymentTypes = []
            vm.openPaymentList = [];
            vm.closePaymentList = [];
            vm.openTender = null;
            vm.closeTender = null;

            vm.getPaymentTypes = function () {
                vm.loading = true;
                vm.refPaymentTypes = [];

                vm.openPaymentList = [];

                swipepaymenttypeService.getAll({}).success(function (result) {
                    vm.refPaymentTypes = result.items;
                    if (result.items.length == 0) {
                        abp.notify.error(app.localize('SwipePaymentType') + " ?");;
                        $state.go('host.swipepaymenttype');
                        return;
                    }
                    if (vm.shiftInOutStatus == 0) {
                        angular.forEach(vm.refPaymentTypes, function (value, key) {
                            vm.openTender = angular.copy(value);
                            vm.openTender.tenderedAmount = '';
                            if (value.refundAcceptFlag == true)
                                vm.openPaymentList.push(vm.openTender);
                        });
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.txtclosetenderKeyPress = function (objId, $event) {
                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    nextId = objId + 1;
                    if (nextId < vm.closePaymentList.length)
                        $("#txtclosetender-" + nextId).focus();
                    else
                        $("#cmdsave").focus();
                }
            };

            vm.priceKeyPress = function (objId, $event) {
                //if (vm.topupFlag == false && vm.refundFlag == false) {
                //    $event.preventDefault();
                //    abp.notify.error(app.localize('TopUp') + "/" + app.localize('Refund') + "?");
                //    $("#cmdTopUp").focus();

                //    return;
                //}

                var keyCode = $event.which || $event.keyCode;
                if (keyCode == 13) {
                    //alert(objId);
                    nextId = objId + 1;
                    if (nextId < vm.openPaymentList.length)
                        $("#price-" + nextId).focus();
                    else
                        $("#cmdsave").focus();
                }
            };
            //  End Key Board Events

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.calculateTenderOpenAmount = function (data) {
                var tempTotalAmt = 0;

                if (data.length == 0 || vm.isUndefinedOrNull(data))
                    return;

                angular.forEach(data, function (value, key) {
                    if ($.isNumeric(value.tenderedAmount)) {
                        tempTotalAmt = parseFloat(tempTotalAmt) + parseFloat(vm.isUndefinedOrNull(value.tenderedAmount) ? 0 : parseFloat(value.tenderedAmount));
                    }
                });

                vm.tenderOpenAmount = parseFloat(tempTotalAmt.toFixed(2));
            }


            vm.calculateTenderCloseAmount = function (data) {
                var tempTotalAmt = 0;
                var tempDiffAmt = 0;
                var diff = 0;

                if (data.length == 0 || vm.isUndefinedOrNull(data))
                    return;

                angular.forEach(data, function (value, key) {
                    if ($.isNumeric(value.tenderedAmount)) {
                        tempTotalAmt = parseFloat(tempTotalAmt) + parseFloat(vm.isUndefinedOrNull(value.tenderedAmount) ? 0 : parseFloat(value.tenderedAmount));

                        diff = parseFloat(vm.isUndefinedOrNull(value.tenderedAmount) ? 0 : parseFloat(value.tenderedAmount)) - value.actualAmount;

                        value.differenceAmount = diff;

                        tempDiffAmt = parseFloat(tempDiffAmt) + parseFloat(vm.isUndefinedOrNull(diff) ? 0 : parseFloat(diff));
                    }
                    else {
                        value.differenceAmount = null;
                    }

                });

                vm.tenderCloseAmount = parseFloat(tempTotalAmt.toFixed(2));
                vm.tenderDifferenceAmount = vm.tenderCloseAmount - vm.swipeshift.actualTenderClosing;
                vm.tenderDifferenceAmount = parseFloat(vm.tenderDifferenceAmount.toFixed(2));
            }


            function init() {
                vm.loading = true;
                swipeshiftService.getSwipeShiftForEdit({
                    Id: vm.swipeshiftId
                }).success(function (result) {
                    vm.swipeshift = result.swipeShift;
                    if (vm.shiftInOutStatus == 1) {
                        var tempOpenBalanceAmt = 0;
                        var tempCashInAmt = 0;
                        var tempCashOutAmt = 0;
                        var tempCreditAmt = 0;
                        var tempDebitAmt = 0;

                        vm.closePaymentList = result.swipeShift.closePaymentList;
                        angular.forEach(vm.closePaymentList, function (value, key) {
                            value.tenderedAmount = '';
                            if ($.isNumeric(value.openBalance)) {
                                tempOpenBalanceAmt = parseFloat(tempOpenBalanceAmt) + parseFloat(vm.isUndefinedOrNull(value.openBalance) ? 0 : parseFloat(value.openBalance));
                            }
                            if ($.isNumeric(value.tillShiftCashIn)) {
                                tempCashInAmt = parseFloat(tempCashInAmt) + parseFloat(vm.isUndefinedOrNull(value.tillShiftCashIn) ? 0 : parseFloat(value.tillShiftCashIn));
                            }
                            if ($.isNumeric(value.tillShiftCashOut)) {
                                tempCashOutAmt = parseFloat(tempCashOutAmt) + parseFloat(vm.isUndefinedOrNull(value.tillShiftCashOut) ? 0 : parseFloat(value.tillShiftCashOut));
                            }
                            if ($.isNumeric(value.credit)) {
                                tempCreditAmt = parseFloat(tempCreditAmt) + parseFloat(vm.isUndefinedOrNull(value.credit) ? 0 : parseFloat(value.credit));
                            }
                            if ($.isNumeric(value.debit)) {
                                tempDebitAmt = parseFloat(tempDebitAmt) + parseFloat(vm.isUndefinedOrNull(value.debit) ? 0 : parseFloat(value.debit));
                            }
                        });

                        vm.openTotalAmount = parseFloat(tempOpenBalanceAmt.toFixed(2));
                        vm.shiftTotalCashInAmount = parseFloat(tempCashInAmt.toFixed(2));
                        vm.shiftTotalCashOutAmount = parseFloat(tempCashOutAmt.toFixed(2));
                        vm.shiftTotalCreditAmount = parseFloat(tempCreditAmt.toFixed(2));
                        vm.shiftTotalDebitAmount = parseFloat(tempDebitAmt.toFixed(2));

                        vm.tenderOpenAmount = vm.swipeshift.tenderOpenAmount;
                        vm.openPaymentList = result.swipeShift.openPaymentList;
                        vm.calculateTenderCloseAmount(vm.closePaymentList);
                        vm.loading = false;
                    }
                    else {
                        vm.closePaymentList = [];
                        vm.loading = false;
                        vm.getPaymentTypes();
                    }

                });
            }

            init();

        }
    ]);
})();