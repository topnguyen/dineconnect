﻿
(function () {
    appModule.controller('host.views.swipe.transaction.swipeshift.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.swipeShift', 'appSession',
        function ($scope, $state, $modal, uiGridConstants, swipeshiftService, appSession) {
            var vm = this;
            /* eslint-disable */

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.currentUserName = appSession.user.name;
            vm.currentUserId = appSession.user.id;
            vm.currentUserId = abp.session.userId;

            vm.shiftAlreadyOpenflag = null;
            vm.loading = true;

            vm.getLoginInfo = function () {
                vm.loading = true;
                swipeshiftService.getLoginStatus({
                    userId: vm.currentUserId
                }).success(function (result) {
                    if (result.shiftInOutStatus == 0) {
                        vm.shiftAlreadyOpenflag = false;
                        vm.loginStatus = result;
                        vm.operationStatus = app.localize('StartShift');
                    }
                    else if (result.shiftInOutStatus == 1) {
                        vm.shiftAlreadyOpenflag = true;
                        vm.loginStatus = result;
                        vm.operationStatus = app.localize('StopShift');
                    }
                    //vm.userGridOptions.totalItems = result.totalCount;
                    //vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.getLoginInfo();

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Host.Swipe.Transaction.SwipeShift.Create'),
                edit: abp.auth.hasPermission('Pages.Host.Swipe.Transaction.SwipeShift.Edit'),
                createTransaction: abp.auth.hasPermission('Pages.Host.Swipe.Transaction.MemberCard.Create'),
            };

            vm.openCashInOut = function (argOption) {
                var operationStatus = '';
                if (argOption == 2) {
                    operationStatus = 'CashIn';
                }
                else if (argOption == 3) {
                    operationStatus = 'CashOut';
                }

                var modalInstance = $modal.open({
                    //host\views\swipe\transaction\swipeshift
                    templateUrl: '~/App/host/views/swipe/transaction/swipeshift/tillCashInCashOut.cshtml',
                    controller: 'host.views.swipe.transaction.swipeshift.tillCashInCashOut as vm',
                    backdrop: 'static',
                    resolve: {
                        swipeshiftId: function () {
                            return vm.loginStatus.id
                        },
                        operationStatus: function () {
                            return operationStatus;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });

            }

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.editSwipeShift = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createSwipeShift = function () {
                openCreateOrEditModal(null);
            };

            vm.deleteSwipeShift = function (myObject) {
                //abp.message.confirm(
                //    app.localize('DeleteSwipeShiftWarning', myObject.id),
                //    function (isConfirmed) {
                //        if (isConfirmed) {
                //            swipeshiftService.deleteSwipeShift({
                //                id: myObject.id
                //            }).success(function () {
                //                vm.getAll();
                //                abp.notify.success(app.localize('SuccessfullyDeleted'));
                //            });
                //        }
                //    }
                //);
            };

            function openCreateOrEditModal(objId) {

                $state.go('host.swipeshiftedit', {
                    id: objId,
                    operationStatus: vm.operationStatus
                });
            }

            vm.openTransaction = function () {
                $state.go('host.membercardedit', {
                    id: null,
                    shiftRefId: vm.loginStatus.id
                });
            }

        }]);
})();




//vm.userGridOptions = {
//    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
//    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
//    paginationPageSizes: app.consts.grid.defaultPageSizes,
//    paginationPageSize: app.consts.grid.defaultPageSize,
//    useExternalPagination: true,
//    useExternalSorting: true,
//    appScopeProvider: vm,
//    rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
//    columnDefs: [
//        {
//            name: app.localize('Actions'),
//            enableSorting: false,
//            width: 120,

//            cellTemplate:
//               "<div class=\"ui-grid-cell-contents\">" +
//                    "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
//                   "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
//                   "    <ul uib-dropdown-menu>" +
//                   "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editSwipeShift(row.entity)\">" + app.localize("Edit") + "</a></li>" +
//                   "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteSwipeShift(row.entity)\">" + app.localize("Delete") + "</a></li>" +
//                   "    </ul>" +
//                   "  </div>" +
//                   "</div>"
//        },
//        {
//            name: app.localize('Id'),
//            field: 'id'
//        },
//        {
//            name: app.localize('CreationTime'),
//            field: 'creationTime',
//            cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
//            minWidth: 100
//        }
//    ],
//    onRegisterApi: function (gridApi) {
//        $scope.gridApi = gridApi;
//        $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
//            if (!sortColumns.length || !sortColumns[0].field) {
//                requestParams.sorting = null;
//            } else {
//                requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
//            }

//            vm.getAll();
//        });
//        gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
//            requestParams.skipCount = (pageNumber - 1) * pageSize;
//            requestParams.maxResultCount = pageSize;

//            vm.getAll();
//        });
//    },
//    data: []
//};

//vm.getAll = function () {
//    vm.loading = true;
//    swipeshiftService.getAll({
//        skipCount: requestParams.skipCount,
//        maxResultCount: requestParams.maxResultCount,
//        sorting: requestParams.sorting,
//        filter: vm.filterText
//    }).success(function (result) {
//        vm.userGridOptions.totalItems = result.totalCount;
//        vm.userGridOptions.data = result.items;
//    }).finally(function () {
//        vm.loading = false;
//    });
//};

//vm.exportToExcel = function () {
//    swipeshiftService.getAllToExcel({})
//        .success(function (result) {
//            app.downloadTempFile(result);
//        });
//};
