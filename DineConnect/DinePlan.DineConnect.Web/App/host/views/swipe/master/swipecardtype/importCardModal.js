﻿(function () {
    appModule.controller('host.views.swipe.master.swipecardtype.importCard', [
        '$scope', 'appSession', '$uibModalInstance', 'FileUploader', 'abp.services.app.swipeCardType', 'cardTypeRefId',
    function ($scope, appSession, $uibModalInstance, fileUploader, swipecardtypeService, cardTypeRefId) {
            var vm = this;
            vm.loading = false;
            vm.cardTypeRefId = cardTypeRefId;
            vm.uploader = new fileUploader({
                url: abp.appPath + 'Import/ImportCards',
                formData: [
                    {
                        "cardTypeRefId": vm.cardTypeRefId
                    },
                ],
                queueLimit: 1,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        if ('|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ImportTemplate_Warn_FileType'));
                            return false;
                        }
                        return true;
                    }
                }]
            });

            vm.importpath = abp.appPath + 'Import/ImportCardTemplate';

            vm.save = function () {
                if (vm.uploader.queue.length == 0) {
                    abp.notify.warn(app.localize('YouAreNotSelectAFile'));
                    return;
                }
                vm.loading = true;
                vm.uploader.uploadAll();
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.datafromexcel = [];

        vm.uploader.onErrorItem = function (fileItem, response, status, headers) {
            if (response.success) {
                abp.notify.info(app.localize("FINISHED"));
                vm.loading = false;
                $uibModalInstance.close();
            } else {
                abp.message.error(response);
                abp.notify.error(response);
                vm.loading = false;
                vm.saving = false;
                return;
            }
        };

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.error != null) {
                    abp.message.warn(response.error.message);
                    $uibModalInstance.dismiss();
                    vm.loading = false;
                    return;
                }
                vm.allItems = [];
                //vm.datafromexcel = response.result;
                //if (vm.datafromexcel == null) {
                //    if (response.error != null) {
                //        abp.message.warn(response.error.message);
                //        vm.readfromexcelstatus = false;
                //        vm.cancel();
                //        return;
                //    }
                //}

                //if (vm.datafromexcel != null) {
                //    vm.sno = 0;
                //    vm.allItems = [];
                //    angular.forEach(vm.datafromexcel, function (value, key) {
                //        vm.sno = vm.sno + 1;
                //        vm.allItems.push({
                //            'sno': vm.sno,
                //            'materialRefId': value.materialRefId,
                //            'materialName': value.materialName,
                //            'adjustmentQty': value.differenceQuantity,
                //            'adjustmentMode': value.adjustmentStatus,
                //            'clBalance': value.clBalance,
                //            'currentStock': value.currentStock,
                //            'unitRefId': value.unitRefId,
                //            'unitRefName': value.unitRefName,
                //            'defaultUnitId': value.defaultUnitId,
                //            'defaultUnitName' : value.defaultUnitName
                //        })
                //    });
                //}
                //else {
                //    vm.allItems = [];
                //}
                //vm.readfromexcelstatus = false;

                abp.notify.info(app.localize("FINISHED"));
                vm.loading = false;
                $uibModalInstance.close();
                
            };
        }

    ]);
})();
