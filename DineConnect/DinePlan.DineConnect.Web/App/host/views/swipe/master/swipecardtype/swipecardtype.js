﻿(function () {
    appModule.controller('host.views.swipe.master.swipecardtype.swipecardtype', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.swipeCardType', 'abp.services.app.swipeCard',
        function ($scope, $state, $stateParams, $modal, swipecardtypeService, swipecardService) {
            var vm = this;

            vm.saving = false;
            vm.swipecardtype = null;
            $scope.existall = true;
            vm.tabtwo = false;
						vm.swipecardtypeId = $stateParams.id;
						vm.autoActivationFlag = false;

						vm.permissions = {
							lock: abp.auth.hasPermission('Pages.Host.Swipe.Master.SwipeCardType.Lock')
						};

            vm.save = function (argOption) {
                if ($scope.existall == false)
                	return;
                if (vm.swipecardtype.isEmployeeCard == true && vm.swipecardtype.canIssueWithOutMember==true) {
                	abp.notify.error(app.localize('CardAssignedToEmployeeSoCanIssueWithOutMemberError') + ' ?');
                	return;
                }
                if (vm.swipecardtype.isAutoRechargeCard == true) {
                	if (vm.swipecardtype.dailyLimit == 0 && vm.swipecardtype.weeklyLimit ==0 && vm.swipecardtype.monthlyLimit == 0 && vm.swipecardtype.yearlyLimit == 0) {
                		abp.notify.error(app.localize('AutoRechargeCardLimitNeeded') + ' ?');
                		return;
                	}
                }
                else
                {
                	vm.swipecardtype.dailyLimit = 0;
                	vm.swipecardtype.monthlyLimit = 0;
                	vm.swipecardtype.weeklyLimit = 0;
                	vm.swipecardtype.yearlyLimit = 0;
                }

                if (vm.swipecardtype.depositRequired == true) {
                    if (vm.swipecardtype.depositAmount == 0) {
                        abp.notify.error(app.localize('DepositAmount') + ' ?');
                        return;
                    }
                }
                else {
                    vm.swipecardtype.depositAmount = 0;
                }

                vm.saving = true;
                vm.swipecardtype.name = vm.swipecardtype.name.toUpperCase();

                swipecardtypeService.createOrUpdateSwipeCardType({
                    swipeCardType : vm.swipecardtype
                }).success(function (result) {
                    vm.swipecardtype.id = result.id;
                    vm.swipecardtypeId = result.id;
                    abp.notify.info(app.localize('SwipeCardType') + ' ' + app.localize('SavedSuccessfully'));
                    if (argOption == 1)
                        vm.cancel();
                    else
                        init();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.existall = function () {

                if (vm.swipecardtype.id == null) {
                    vm.existall = false;
                    return;
                }

                swipecardtypeService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.swipecardtype.id,
                    operation: 'SEARCH'
                }).success(function (result) {

                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.swipecardtype.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.swipecardtype.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $state.go('host.swipecardtype');
            };

            vm.getComboValue = function (item) {
                return parseInt(item.value);
            };

            function init() {
                vm.loading = true;
                swipecardtypeService.getSwipeCardTypeForEdit({
                    Id: vm.swipecardtypeId
                }).success(function (result) {
                    vm.loading = false;
                    vm.swipecardtype = result.swipeCardType;
                    vm.cardList = result.cardList;
										console.log(result);
										vm.autoActivationFlag = false;
										if (vm.swipecardtype.id == null) {
											vm.swipecardtype.depositRequired = abp.setting.getBoolean("App.Swipe.DepositRequiredDefault");
											vm.swipecardtype.depositAmount = parseFloat(abp.setting.get("App.Swipe.DepositAmountDefault"));
											//vm.swipecardtype.expiryDaysFromIssue = parseInt(abp.setting.get("App.Swipe.ExpiryDaysFromIssueDefault"));
											vm.swipecardtype.refundAllowed = abp.setting.getBoolean("App.Swipe.RefundAllowedDefault");
										}
										else {
											if (vm.swipecardtype.autoActivateRegExpression.length>0)
												vm.autoActivationFlag = true;
										}
                });
            }

            init();

            vm.cardTransactionDataShown = function (argOption) {
                var modalInstance1 = $modal.open({
                    templateUrl: '~/App/host/views/swipe/transaction/membercard/drillDownLevel.cshtml',
                    controller: 'host.views.swipe.transaction.membercard.drillDownLevel as vm',
                    backdrop: 'static',
                    resolve: {
                        cardNumber: function () {
                            return '';
                        },
                        cardRefId: function () {
                            return argOption;
                        }

                    }
                });

                modalInstance1.result.then(function (result) {
                    //vm.getAll();
                });
            }

            vm.import = function () {
                importModal(null);
            };

            function importModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/host/views/swipe/master/swipecardtype/importCardModal.cshtml',
                    controller: 'host.views.swipe.master.swipecardtype.importCard as vm',
                    backdrop: 'static',
                    resolve: {
                        cardTypeRefId: function () {
                            return vm.swipecardtype.id;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    init();
                });
            }


            vm.generateCards = function () {
                
                if (vm.cardStartingNumber == null || vm.cardStartingNumber == 0)
                {
                    abp.notify.error(app.localize('CardStaringNumber') + "?");
                    return;
                }

                if (vm.cardCount == null || vm.cardCount == 0) {
                    abp.notify.error(app.localize('CardCount') + "?");
                    return;
                }

                if (vm.cardNumberOfDigits == null || vm.cardNumberOfDigits == 0) {
                    abp.notify.error(app.localize('CardNumberOfDigits') + "?");
                    return;
                }

                swipecardService.generateSwipeCards({
                    cardTypeRefId: vm.swipecardtype.id,
                    prefix: vm.prefix,
                    suffix: vm.suffix,
                    cardNumberOfDigits: vm.cardNumberOfDigits,
                    cardStartingNumber: vm.cardStartingNumber,
                    cardCount : vm.cardCount
                }).success(function (result) {
                    abp.notify.info(vm.cardCount + ' ' + app.localize('CardsInitiatedSuccessfully'));
                    init();
                    vm.tabtwo = true;
                });
            }
        }
    ]);
})();


