﻿
(function () {
    appModule.controller('host.views.swipe.master.swipecard.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.swipeCard', 'swipecardId', 'abp.services.app.commonLookup',
        function ($scope, $modalInstance, swipecardService, swipecardId, commonLookupService) {
            var vm = this;
            
            vm.saving = false;
            vm.swipecard = null;
			$scope.existall = true;

			vm.permissions = {
			    canOverRide_CardType: abp.auth.hasPermission('Pages.Host.Swipe.Master.SwipeCard.CanOverRide_CardType'),
			};

            vm.save = function () {
			   if ($scope.existall == false)
                    return;

			   if (vm.swipecard.isAutoRechargeCard == true) {
			   	if (vm.swipecard.dailyLimit == 0 && vm.swipecard.weeklyLimit == 0 && vm.swipecard.monthlyLimit == 0 && vm.swipecard.yearlyLimit == 0) {
			   		abp.notify.error(app.localize('AutoRechargeCardLimitNeeded') + ' ?');
			   		return;
			   	}
			   }
			   else {
			   	vm.swipecard.dailyLimit = 0;
			   	vm.swipecard.monthlyLimit = 0;
			   	vm.swipecard.weeklyLimit = 0;
			   	vm.swipecard.yearlyLimit = 0;
			   }

			   //if (vm.swipecard.dailyLimit > 0 || vm.swipecard.weeklyLimit > 0 || vm.swipecard.monthlyLimit || vm.swipecard.yearlyLimit) {
			   //	if (vm.swipecard.refundAllowed == true) {
			   //		abp.notify.error(app.localize('RefundNotAllowedForLimitCard') + ' ?');
			   //		return
			   //	}
			   //}

			   if (vm.swipecard.depositRequired == true)
                {
                    if(vm.swipecard.depositAmount ==0)
                    {
                        abp.notify.error(app.localize('DepositAmount') + ' ?');
                        return;
                    }
                }
                else
                {
                    vm.swipecard.depositAmount = 0;
			   }

			   if (vm.swipecard.balance > 0)
			   {
			       if (vm.swipecard.activeStatus==false)
			       {
			           abp.notify.error(app.localize('BalanceActiveStatusError'));
			           return;
			       }
			   }

			   //if (vm.swipecard.expiryDaysFromIssue == 0 || vm.swipecard.expiryDaysFromIssue == null) {
			   //    abp.notify.error(app.localize('ExpiryDaysFromIssueShouldBeGreaterThanZero'));
			   //    return;
			   //}

                vm.saving = true;
                swipecardService.createOrUpdateSwipeCard({
                    swipeCard: vm.swipecard
                }).success(function () {
                    abp.notify.info('\' SwipeCard \'' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.swipecard.id == null) {
			         vm.existall = false;
			         return;
			     }
				 
                swipecardService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'id',
                    filter: vm.swipecard.id,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.swipecard.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.swipecard.id + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
			 };

			 vm.refCardtypes = [];
			 vm.fillCardtypess = function () {
			     vm.refCardtypes = [];
			     vm.loading = true;
			     swipecardService.getSwipeCardTypes({}).success(function (result) {
			         vm.loading = false;
			         vm.refCardtypes = result.items;
			     });
			 }
			 vm.fillCardtypess();
			
	        function init() {
                swipecardService.getSwipeCardForEdit({
                    Id: swipecardId
                }).success(function (result) {
                    vm.swipecard = result.swipeCard;
                    if (vm.swipecard.id == null) {
                        //vm.swipecard.depositRequired = abp.setting.getBoolean("App.Swipe.DepositRequiredDefault");
                        //vm.swipecard.depositAmount = parseFloat(abp.setting.get("App.Swipe.DepositAmountDefault"));
                        ////vm.swipecard.expiryDaysFromIssue = parseInt( abp.setting.get("App.Swipe.ExpiryDaysFromIssueDefault"));
                        //vm.swipecard.refundAllowed = abp.setting.getBoolean("App.Swipe.RefundAllowedDefault");

                        vm.swipecard.activeStatus = false;
                        vm.swipecard.balance = 0;
                    }
                    });     
	        }

	        init();

	        vm.changeCardType = function (data) {
	            vm.swipecard.depositRequired = data.depositRequired;
	            vm.swipecard.depositAmount = data.depositAmount;
	            vm.swipecard.expiryDaysFromIssue = data.expiryDaysFromIssue;
	            vm.swipecard.refundAllowed = data.refundAllowed;
	            vm.swipecard.canIssueWithOutMember = data.canIssueWithOutMember;
	            vm.swipecard.dailyLimit = data.dailyLimit;
	            vm.swipecard.weeklyLimit = data.weeklyLimit;
	            vm.swipecard.monthlyLimit = data.monthlyLimit;
	            vm.swipecard.yearlyLimit = data.yearlyLimit;
	        }

        }
    ]);
})();

