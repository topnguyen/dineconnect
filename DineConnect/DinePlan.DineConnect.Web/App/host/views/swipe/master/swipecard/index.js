﻿(function () {
    appModule.controller('host.views.swipe.master.swipecard.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.swipeCard',
        function ($scope, $modal, uiGridConstants, swipecardService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Host.Swipe.Master.SwipeCard.Create'),
                edit: abp.auth.hasPermission('Pages.Host.Swipe.Master.SwipeCard.Edit'),
                'delete': abp.auth.hasPermission('Pages.Host.Swipe.Master.SwipeCard.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 60,
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editSwipeCard(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteSwipeCard(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('CardNumber'),
                        field: 'cardNumber'
                    },
                    {
                        name: app.localize('Balance'),
                        field: 'balance',
                        cellClass: 'ui-ralign',
                        width: 120
                    },
                    {
                        name: app.localize('Active'),
                        field: 'activeStatus',
                        width: 100,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.activeStatus" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.activeStatus" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                    },
                    {
                        name: app.localize('DepositRequired'),
                        field: 'depositRequired',
                        width: 100,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.depositRequired" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.depositRequired" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                    },
                    {
                        name: app.localize('DepositAmount'),
                        field: 'depositAmount',
                        cellClass: 'ui-ralign',
                        width: 100,
                    },
                    {
                        name: app.localize('RefundAllowed'),
                        field: 'refundAllowed',
                        width: 100,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.refundAllowed" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.refundAllowed" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                    },
                    {
                        name: app.localize('CanIssueWithOutMember'),
                        field: 'canIssueWithOutMember',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.canIssueWithOutMember" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.canIssueWithOutMember" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                    },

                    {
                        name: app.localize('CanExcludeTaxAndCharges'),
                        field: 'canExcludeTaxAndCharges',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.canExcludeTaxAndCharges" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.canExcludeTaxAndCharges" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                    },
                    {
                        name: app.localize('IsEmployeeCard'),
                        field: 'isEmployeeCard',
                        width: 100,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.isEmployeeCard" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.isEmployeeCard" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                    },

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                swipecardService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editSwipeCard = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createSwipeCard = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteSwipeCard = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteSwipeCardWarning', myObject.cardNumber),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            swipecardService.deleteSwipeCard({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/host/views/swipe/master/swipecard/createOrEditModal.cshtml',
                    controller: 'host.views.swipe.master.swipecard.createOrEditModal as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        swipecardId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.exportToExcel = function () {
                swipecardService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.getAll();
        }]);
})();