﻿
(function () {
    appModule.controller('host.views.swipe.master.swipepaymenttype.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.swipePaymentType', 'swipepaymenttypeId', 'abp.services.app.commonLookup',
        function ($scope, $modalInstance, swipepaymenttypeService, swipepaymenttypeId, commonLookupService) {
            var vm = this;
            
            vm.saving = false;
            vm.swipepaymenttype = null;
			$scope.existall = true;

			
            vm.save = function () {
                if (vm.swipepaymenttype.topupAcceptFlag == false && vm.swipepaymenttype.refundAcceptFlag == false)
                {
                    abp.notify.error(app.localize('AtleastAnyOneShouldSelectTopUpRefund'));
                    return;
                }
                vm.saving = true;
                
			   vm.swipepaymenttype.name = vm.swipepaymenttype.name.toUpperCase();
				
                swipepaymenttypeService.createOrUpdateSwipePaymentType({
                    swipePaymentType: vm.swipepaymenttype
                }).success(function () {
                    abp.notify.info(app.localize('SwipePaymentType') + ' ' + app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
			     if (vm.swipepaymenttype.id == null) {
			         vm.existall = false;
			         return;
			     }
				 
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
	        function init() {

                swipepaymenttypeService.getSwipePaymentTypeForEdit({
                    Id: swipepaymenttypeId
                }).success(function (result) {
                    vm.swipepaymenttype = result.swipePaymentType;
                    if (vm.swipepaymenttype.id == null)
                        vm.swipepaymenttype.sortOrder = "";
                });
            }
            init();
        }
    ]);
})();

