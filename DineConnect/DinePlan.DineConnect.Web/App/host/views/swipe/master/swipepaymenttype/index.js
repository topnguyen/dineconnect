﻿(function () {
    appModule.controller('host.views.swipe.master.swipepaymenttype.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.swipePaymentType',
        function ($scope, $modal, uiGridConstants, swipepaymenttypeService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;
            vm.currentUserId = abp.session.userId;

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Host.Swipe.Master.SwipePaymentType.Create'),
                edit: abp.auth.hasPermission('Pages.Host.Swipe.Master.SwipePaymentType.Edit'),
                'delete': abp.auth.hasPermission('Pages.Host.Swipe.Master.SwipePaymentType.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.userGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,

                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                            "  <div class=\"btn-group dropdown\" uib-dropdown=\"\" dropdown-append-to-body>" +
                            "    <button class=\"btn btn-xs btn-primary blue\" uib-dropdown-toggle=\"\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-cog\"></i> " + app.localize("Actions") + " <span class=\"caret\"></span></button>" +
                            "    <ul uib-dropdown-menu>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.edit\" ng-click=\"grid.appScope.editSwipePaymentType(row.entity)\">" + app.localize("Edit") + "</a></li>" +
                            "      <li><a ng-if=\"grid.appScope.permissions.delete\" ng-click=\"grid.appScope.deleteSwipePaymentType(row.entity)\">" + app.localize("Delete") + "</a></li>" +
                            "    </ul>" +
                            "  </div>" +
                            "</div>"
                    },
                    {
                        name: app.localize('Name'),
                        field: 'name',
                        minWidth: 180
                    },
                    {
                        name: app.localize('AccountCode'),
                        field: 'accountCode',
                        minWidth: 180
                    },
                    {
                        name: app.localize('SortOrder'),
                        field: 'sortOrder',
                    },
                    {
                        name: app.localize('TopupAcceptFlag'),
                        field: 'topupAcceptFlag',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.topupAcceptFlag" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.topupAcceptFlag" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                    },
                    {
                        name: app.localize('RefundAcceptFlag'),
                        field: 'refundAcceptFlag',
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.refundAcceptFlag" class="label label-success">' + app.localize('Yes') + '</span>' +
                            '  <span ng-show="!row.entity.refundAcceptFlag" class="label label-danger">' + app.localize('No') + '</span>' +
                            '</div>',
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAll();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAll();
                    });
                },
                data: []
            };

            vm.getAll = function () {
                vm.loading = true;
                swipepaymenttypeService.getAll({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.userGridOptions.totalItems = result.totalCount;
                    vm.userGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editSwipePaymentType = function (myObj) {
                openCreateOrEditModal(myObj.id);
            };

            vm.createSwipePaymentType = function () {
                openCreateOrEditModal(null);
            };
            vm.deleteSwipePaymentType = function (myObject) {
                abp.message.confirm(
                    app.localize('DeleteSwipePaymentTypeWarning', myObject.name),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            swipepaymenttypeService.deleteSwipePaymentType({
                                id: myObject.id
                            }).success(function () {
                                vm.getAll();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            function openCreateOrEditModal(objId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/host/views/swipe/master/swipepaymenttype/createOrEditModal.cshtml',
                    controller: 'host.views.swipe.master.swipepaymenttype.createOrEditModal as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        swipepaymenttypeId: function () {
                            return objId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getAll();
                });
            }

            vm.exportToExcel = function () {
                swipepaymenttypeService.getAllToExcel({})
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.getAll();
        }]);
})();