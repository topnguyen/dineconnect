﻿(function () {
    appModule.controller('host.views.swipe.master.swipemember.swipemember', [
        '$scope', '$uibModalInstance', 'abp.services.app.swipeMember', 'swipememberId', 
        function ($scope, $modalInstance, swipememberService, swipememberId) {
            var vm = this;
            
            vm.saving = false;
            vm.swipemember = null;
			$scope.existall = true;
			
            vm.save = function () {
			   if ($scope.existall == false)
                    return;
                vm.saving = true;
				
                vm.swipemember.memberCode = vm.swipemember.memberCode.toUpperCase();
                vm.swipemember.name = vm.swipemember.name.toUpperCase();

                swipememberService.createOrUpdateSwipeMember({
                    swipeMember: vm.swipemember
                }).success(function (result) {
                    abp.notify.info(app.localize('SwipeMember')+  ' ' + result.memberCode + ' ' + app.localize('SavedSuccessfully'));
                    $modalInstance.close(result);
                }).finally(function () {
                    vm.saving = false;
                });
            };

			 vm.existall = function ()
            {
                
			     if (vm.swipemember.MemberCode == null) {
			         vm.existall = false;
			         return;
			     }
				 
                swipememberService.getAll({
                    skipCount: 0,
                    maxResultCount: 2,
                    sorting: 'MemberCode',
                    filter: vm.swipemember.MemberCode,
                    operation: 'SEARCH'
                }).success(function (result) {
                    
                    $scope.existall = true;
                    if (result.totalCount > 0 && vm.swipemember.id == null) {
                        vm.saving = false;
                        vm.loading = false;
                        abp.notify.info('\' ' + vm.swipemember.MemberCode + '\' ' + app.localize('NameExist'));
                        $scope.existall = false;
                    }
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss(null);
            };
			
			 vm.getComboValue = function (item) {
                return parseInt(item.value);
            };
			
			vm.reflocations = [];

	        function init() {
                swipememberService.getSwipeMemberForEdit({
                    Id: swipememberId
                }).success(function (result) {
                    vm.swipemember = result.swipeMember;
                });
            }
	        init();


	        vm.memberCodeKeyPress = function ($event) {
	            var keyCode = $event.which || $event.keyCode;
	            if (keyCode == 13) {
	                $event.preventDefault();
	                $("#membername").focus();
	            }
	        };


	        vm.memberAlreadyExists = function()
	        {
	            vm.loading = true;
	            swipememberService.memberAlreadyExists({
                    memberCode : vm.swipemember.memberCode
	            }).success(function (result) {
	                if (result!=null) {
	                    if (vm.swipemember.id != result.id) {
	                        abp.notify.error(app.localize('MemberCodeAlreadyExists_P', vm.swipemember.memberCode));
	                        //abp.message.error(app.localize('MemberCodeAlreadyExists_P', vm.swipemember.memberCode));
	                        vm.swipemember.memberCode = null;
	                        $("#memberCode").focus();
	                    }
	                }
	                vm.loading = false;
	            });
	            
	        }
        }
    ]);
})();

