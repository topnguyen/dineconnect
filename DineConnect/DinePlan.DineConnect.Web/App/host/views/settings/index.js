﻿(function() {
    appModule.controller('host.views.settings.index', [
        '$scope', 'abp.services.app.hostSettings', 'abp.services.app.hostLookup',
        function($scope, hostSettingsService, hostLookup) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.settings = null;

            vm.getSettings = function() {
                vm.loading = true;
                hostSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.settings = result;
                    }).finally(function() {
                        vm.loading = false;
                    });
            };

            vm.saveAll = function() {
                hostSettingsService.updateAllSettings(
                    vm.settings
                ).success(function() {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                });
            };
            vm.getEditionValue = function (item) {
                return parseInt(item.value);
            };
            function init() {
                hostLookup.getShortMessageTypes({}).success(function (result) {
                    vm.messageTypes = result.items;
                    vm.messageTypes.unshift({ value: "0", displayText: app.localize('NotAssigned') });
                });
            }

            init();
            vm.getSettings();
        }
    ]);
})();