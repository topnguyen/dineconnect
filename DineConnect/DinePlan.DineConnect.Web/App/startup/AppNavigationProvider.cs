﻿using Abp.Application.Features;
using Abp.Application.Navigation;
using Abp.Localization;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Web.Navigation;

namespace DinePlan.DineConnect.Web.App.Startup
{
    /// <summary>
    ///     This class defines menus for the application.
    ///     It uses ABP's menu system.
    ///     When you add menu items here, they are automatically appear in angular application.
    ///     See .cshtml and .js files under App/Main/views/layout/header to know how to render menu.
    /// </summary>
    public class AppNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                .AddItem(new MenuItemDefinition(
                        PageNames.App.Host.Tenants,
                        L("Tenants"),
                        url: "host.tenants",
                        icon: "icon-globe",
                        requiredPermissionName: AppPermissions.Pages_Tenants
                    )
                ).AddItem(new MenuItemDefinition(
                        PageNames.App.Host.Editions,
                        L("Editions"),
                        url: "host.editions",
                        icon: "icon-grid",
                        requiredPermissionName: AppPermissions.Pages_Editions
                    )
                ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Dashboard,
                        L("Dashboard"),
                        url: "tenant.dashboard",
                        icon: "icon-home",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Dashboard
                    )
                )

                #region Connect

                .AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Connect,
                        L("Connect"), "fa fa-connectdevelop",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Connect,
                        featureDependency: new SimpleFeatureDependency("DinePlan.DineConnect.Connect")
                    )
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.DineConnect.Location,
                            L("Location"), "fa fa-map")
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Company, L("Company"),
                            "fa fa-sitemap",
                            "tenant.company",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Master_Company))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Location, L("Location"),
                            "fa fa-location-arrow", "tenant.location",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Master_Location))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.LocationGroup, L("LocationGroup"),
                            "fa fa-archive", "tenant.locationgroup",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Master_LocationGroup))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.LocationTag, L("LocationTag"),
                            "fa fa-tags", "tenant.locationtag",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Master_LocationTag))
                    )
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.DineConnect.Master,
                            L("Master"), "fa fa-indent",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Master)
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.PaymentType, L("PaymentType"),
                            "fa fa-credit-card", "tenant.paymenttype",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Master_PaymentType))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Numerator, L("Numerator"),
                            "fa fa-exchange", "tenant.numerator",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Master_Numerator))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.TicketType, L("TicketType"),
                            "fa fa-ticket", "tenant.tickettype",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Master_TicketType))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.ForeignCurrency, L("ForeignCurrency"),
                            "fa fa-server", "tenant.foreigncurrency",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Master_ForeignCurrency))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.PlanReason, L("PlanReason"),
                            "fa fa-external-link", "tenant.planreason",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Master_PlanReason))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.TransactionType, L("TransactionType"),
                            "fa fa-database", "tenant.transactiontype",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Master_TransactionType))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.DepartmentGroup, L("DepartmentGroup"),
                            "fa fa-money", "tenant.departmentgroup",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Master_DepartmentGroup))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Department, L("Department"),
                            "fa fa-adjust", "tenant.department",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Master_Department))
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineConnect.TillAccount,
                            L("TillAccount"), "fa fa-money", "tenant.tillaccount",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Master_TillAccount))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Calculation, L("Calculation"),
                            "fa fa-gg",
                            "tenant.calculation",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Master_Calculation))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.DinePlanTax, L("Tax"), "fa fa-unlink",
                            "tenant.dineplantax",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Master_DinePlanTax))
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineConnect.FutureData,
                            L("FutureData"), "fa fa-indent",
                            "tenant.futuredata",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_FutureData))
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineConnect.Terminal,
                            L("Terminal"), "fa fa-file-code-o",
                            "tenant.terminal",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Terminal))
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineConnect.ImportSetting,
                            L("ImportSetting"), "fa fa-file",
                            "tenant.import",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_ImportSetting))
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineConnect.FileManager,
                            L("FileManager"), "fa fa-folder",
                            "tenant.filemanager",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_FileManager))
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Menu, L("Menu"), "fa fa-bullseye",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Menu)
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.ProductGroup, L("Group"),
                            "fa fa-external-link", "tenant.productgroup",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Menu_ProductGroup))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Category, L("Category"),
                            "fa fa-futbol-o", "tenant.category",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Menu_Category))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.MenuItem, L("MenuItem"), "fa fa-bars",
                            "tenant.menuitem",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Menu_MenuItem))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.ScreenMenu, L("ScreenMenu"),
                            "fa fa-film", "tenant.screenmenu",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Menu_ScreenMenu,
                            featureDependency: new SimpleFeatureDependency("DinePlan.DineConnect.Connect")))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.ComboGroup, L("ComboGroup"),
                            "fa fa-bars",
                            "tenant.combogroup",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Menu_ComboGroup))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.LocationMenuPrice,
                            L("LocationMenuPrice"), "fa fa-building-o", "tenant.locationmenuprice",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Menu_MenuItemPrice))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineTouch.LocationMenuItem,
                            L("LocationMenuItem"), "fa fa-clone", "tenant.locationmenuitem",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Touch_Menu))
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Print, L("Print"), "fa fa-print",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Display)
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineConnect.Printer,
                            L("Printer"), "fa fa-print",
                            "tenant.printer",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Display_Printer))
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineConnect.PrintTemplate,
                            L("Template"), "fa fa-file-code-o",
                            "tenant.printtemplate",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Display_PrintTemplate))
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineConnect.PrintJob,
                            L("Job"), "fa fa-tasks",
                            "tenant.printJob",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Display_PrintJob))
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineConnect.PrintTemplateCondition,
                            L("Condition"), "fa fa-file-code-o",
                            "tenant.printtemplatecondition",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Display_PrintTemplateCondition))
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineConnect.DineDevice,
                            L("Device"),
                            "fa fa-money",
                            "tenant.dinedevice",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Display_DineDevice
                        ))
                    ).AddItem(new MenuItemDefinition(PageNames.App.DineConnect.ProgramSettings, L("ProgramSetting"),
                            "icon-settings",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_ProgramSetting)
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineConnect.ProgramSettingTemplate,
                            L("Template"), "fa fa-cubes",
                            "tenant.programSettingTemplate",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_ProgramSetting_Template))
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineConnect.ProgramSettingPTTOrSetting,
                            L("PTTOrSettings"), "fa fa-cubes",
                            "tenant.PTTOrSetting",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_ProgramSetting_PTTOrSetting))
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Table, L("Table"), "fa fa-clone",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Table)
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.TableGroup, L("Group"),
                            "fa fa-building-o", "tenant.tablegroup",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Table_Group))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.ConnectTable, L("Tables"),
                            "fa fa-table",
                            "tenant.tables", requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Tables))
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Menu, L("Tag"), "fa fa-tag")
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.PriceTag, L("PriceTag"), "fa fa-at",
                            "tenant.pricetag",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Menu_PriceTag))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.OrderTagGroup, L("OrderTag"),
                            "fa fa-bars", "tenant.ordertaggroup",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Menu_OrderTagGroup))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.TicketTagGroup, L("TicketTag"),
                            "fa fa-filter", "tenant.tickettaggroup",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Menu_OrderTagGroup))
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Menu, L("Promotion"), "fa fa-briefcase")
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.PromotionCategory,
                            L("PromotionCategory"),
                            "fa fa-caret-square-o-up",
                            "tenant.promotioncategory",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Menu_PromotionCategory
                        ))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Promotion, L("Promotion"),
                            "fa fa-bookmark", "tenant.promotion",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Menu_Promotion))
                    )
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.DineConnect.User, L("User"), "fa fa-user-md",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_User)
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.DinePlanUserRole, L("Roles"),
                            "fa fa-cogs", "tenant.dineplanuserrole",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_User_DinePlanUserRole))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.DinePlanUser, L("Users"),
                            "fa fa-users", "tenant.dineplanuser",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_User_DinePlanUser))
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Card, L("Card"), "fa fa-credit-card")
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineConnect.ConnectCardTypeCategory,
                            L("Category"),
                            "fa fa-money",
                            "tenant.connectcardtypecategory",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Card_ConnectCardTypeCategory
                        ))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.CardType, L("CardType"),
                            "fa fa-futbol-o", "tenant.connectcardtype",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Card_CardType))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Cards, L("Cards"),
                            "fa fa-credit-card", "tenant.connectcard",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Card_Cards))
                    ).AddItem(new MenuItemDefinition(PageNames.App.DineConnect.FullTax, L("FullTax"), "fa fa-ticket")
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Member, L("FullTaxMember"),
                            "fa fa-user", "tenant.fulltaxmember",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_FullTax_Member))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Member, L("Report"),
                            "fa fa-flag", "tenant.fulltaxreport",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_FullTax_Report))
                    )
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.DineConnect.Report,
                            L("Report"), "fa fa-flag",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report)
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Tickets, L("Ticket"),
                                "fa fa-database",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_Tickets)
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Tickets, L("Ticket"),
                                "fa fa-ticket",
                                "tenant.ticket"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Tickets, L("Sync"),
                                "fa fa-ticket", "tenant.ticketsync"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.HourSummary, L("Hourly"),
                                "fa fa-map-marker", "tenant.hoursummary"))
                        )
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Orders, L("Order"),
                                "fa fa-database",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_Orders)
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Orders, L("Order"),
                                "fa fa-sitemap",
                                "tenant.order"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.OrderTags, L("OrderTag"),
                                "fa fa-sitemap", "tenant.ordertag"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.OrderExchange, L("OrderExchange"),
                                "fa fa-sitemap", "tenant.exchange"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.ReturnProduct, L("OrderReturn"),
                                "fa fa-sitemap", "tenant.returnproduct"))
                        )
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Departments, L("Department"),
                                "fa fa-th-large",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_Departments)
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.DepartmentSalesReport,
                                L("Summary"),
                                "fa fa-caret-square-o-up", "tenant.departmentSalesSummary"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.DepartmentItemSalesReport,
                                L("Item"),
                                "fa fa-ellipsis-v", "tenant.departmentItemSales"))
                        )
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.GroupReport, L("Group"),
                                "fa fa-group",
                                "tenant.groupReport",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_Tickets)
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.GroupReport, L("Summary"),
                                "fa fa-clone", "tenant.groupReport"))
                        )
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.CategoryReport, L("Category"),
                                "fa fa-caret-square-o-up",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_Items)
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.CategoryReport, L("Summary"),
                                "fa fa-clone", "tenant.categoryReport"))
                        )
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Items, L("Item"), "fa fa-database",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_Items)
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.ItemReport, L("Summary"),
                                "fa fa-clone", "tenant.itemreport"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.ItemHourlySalesReport,
                                L("Hourly"),
                                "fa fa-clock-o", "tenant.hourlySalesReport"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.TopBottomItemReport, L("TopDown"),
                                "fa fa-arrow-circle-up", "tenant.topbottomitemreport"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.ItemTagReport, L("ItemTag"),
                                "fa fa-clone", "tenant.itemTagReport"))
                        )
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Promotion, L("Promotion"),
                                "fa fa-database",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_Items)
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Promotion, L("OrderPromotion"),
                                "fa fa-gamepad", "tenant.promotionreport"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Discount, L("TicketPromotion"),
                                "fa fa-ship", "tenant.promotionticketreport"))
                        )
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Summary, L("Sale"),
                                "fa fa-life-ring",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_Summary)
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.DatewiseSale, L("Summary"),
                                "fa fa-map-marker", "tenant.datewisesale"))
                             .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.SaleSummaryNew, L("SummaryNew"),
                                "fa fa-map-marker", "tenant.salesummarynew"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.WorkDaySummary, L("WorkDay"),
                                "fa fa-map-marker", "tenant.workdaysale"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.WeekDaySummary, L("WeekDay"),
                                "fa fa-map-marker", "tenant.weekdaysale"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.WeekEndSummary, L("WeekEnd"),
                                "fa fa-map-marker", "tenant.weekendsale"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.PlanWorkDay, L("PlanWorkDay"),
                                "fa fa-map-marker", "tenant.planworkday"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.CashSummaryReport,
                                L("CashSummaryReport"), "fa fa-ticket", "tenant.cashsummaryreport",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_CashSummaryReport))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.CollectionReport, L("Collection"),
                                "fa fa-clone", "tenant.collectionReport"))
                        )
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.ScheduleReport, L("Schedule"),
                                "fa fa-calendar",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_Schedule)
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.ScheduleReport, L("Item"),
                                "fa fa-clone", "tenant.schedulesReport"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.ScheduleReport, L("Location"),
                                "fa fa-clone", "tenant.locationScheduleReport"))
                        )
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Summary, L("Other"),
                                "fa fa-life-ring",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_Summary)
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.TillTransaction,
                                L("TillTransaction"),
                                "fa fa-money", "tenant.tillaccountreport"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.LogSummary, L("Log"),
                                "fa fa-bug", "tenant.log"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.SaleTarget, L("Summary"),
                                "fa fa-building",
                                "tenant.saleTarget"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Tender, L("Tender"), "fa fa-ship",
                                "tenant.tender"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.PaymentExcessReport,
                                L("PaymentExcess"), "fa fa-credit-card",
                                "tenant.paymentexcess"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.ExchangeReport,
                                L("Exchange"), "fa fa-exchange",
                                "tenant.exchangereport"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.DailyReconciliation,
                                L("DailyReconciliation"), "fa fa-calendar",
                                "tenant.dailyreconciliation"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.InternalSaleMeetingReport,
                                L("InternalSaleMeetingReport"), "fa fa-refresh",
                                "tenant.internalsalemeetingreport"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.MenuEngineering,
                                L("MenuEngineering"),
                                "fa fa-clone", "tenant.menuengineeringreport"))
                        )
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Thailand, L("Thailand"),
                                "fa fa-life-ring",
                                featureDependency: new SimpleFeatureDependency("DinePlan.DineConnect.Feature.Custom"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.ThailandReport, L("TaxReport"),
                                "fa fa-money", "tenant.thailandReport"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.ThailandReportDetails,
                                L("TaxReportDetails"),
                                "fa fa-map-marker", "tenant.thailandReportRetails"))
                        )
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Customize, L("Customize"),
                                "fa fa-ticket",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_Customize)
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.SalesTaxReport,
                                L("SalesTaxReport"),
                                "fa fa-clone", "tenant.salestaxreport"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.NonSalesTaxReport,
                                L("NonSalesTaxReport"),
                                "fa fa-clone", "tenant.nonsalestaxreport"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.SpeedOfServiceReport,
                                L("SpeedOfServiceReport"), "fa fa-ticket", "tenant.speedofservicereport",
                                requiredPermissionName: AppPermissions
                                    .Pages_Tenant_Connect_Report_SpeedOfServiceReport))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Tickets, L("ABBReprint"),
                                "fa fa-ticket", "tenant.abbreprint",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_Tickets))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.CashAuditReport,
                                L("CashAuditReport"),
                                "fa fa-exchange", "tenant.cashaudit",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_CashAudits))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.CreditCardReport, L("CreditCard"),
                                "fa fa-exchange", "tenant.creditcard",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_CreditCard))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.AbnormalEndDay,
                                L("AbnormalEndDay"),
                                "fa fa-exchange", "tenant.abnormalendday"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.FeedbackUnread,
                                L("FeedbackUnread"),
                                "fa fa-envelope", "tenant.feedbackunread"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.ProductMixReport,
                                L("ProductMixReport"),
                                "fa fa-envelope", "tenant.productmixreport"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.CreditSalesReport,
                                L("CreditSales"),
                                "fa fa-exchange", "tenant.creditsales",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_CreditSales))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.FullTaxInvoice,
                                L("FullTaxInvoiceReport"),
                                "fa fa-clone", "tenant.fulltaxinvoicereport"))
                        )
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.TopDownSellReport,
                                L("TopDownSellReport"),
                                "fa fa-ticket",
                                "tenant.topdownsell",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_TopDownSellReport)
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.TopDownSellReportAmountSummary,
                                L("TopDownSellReportByQuantitySummary"),
                                "fa fa-clone", "tenant.topdownsellreportbyquantitysummary",
                                requiredPermissionName: AppPermissions
                                    .Pages_Tenant_Connect_Report_TopDownSellReportByQuantitySummary))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.TopDownSellReportQuantitySummary,
                                L("TopDownSellReportByAmountSummary"),
                                "fa fa-clone", "tenant.topdownsellreportbyamountsummary",
                                requiredPermissionName: AppPermissions
                                    .Pages_Tenant_Connect_Report_TopDownSellReportByAmountSummary))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.TopDownSellReportQuantityPlant,
                                L("TopDownSellReportByQuantityPlant"),
                                "fa fa-clone", "tenant.topdownsellreportbyquantityplant",
                                requiredPermissionName: AppPermissions
                                    .Pages_Tenant_Connect_Report_TopDownSellReportByQuantityPlant))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.TopDownSellReportAmountPlant,
                                L("TopDownSellReportByAmountPlant"),
                                "fa fa-clone", "tenant.topdownsellreportbyamountplant",
                                requiredPermissionName: AppPermissions
                                    .Pages_Tenant_Connect_Report_TopDownSellReportByAmountPlant))
                        )
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.Comparision, L("Comparision"),
                                "fa fa-ticket",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_Comparision)
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.MonthlySales,
                                L("MonthlySales"),
                                "fa fa-clone", "tenant.monthlysales"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.MonthlySalesAdvance,
                                L("MonthlySalesAdvance"),
                                "fa fa-clone", "tenant.monthlysaleadvancereport"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.DailySalesBreak,
                                L("DailySalesBreak"),
                                "fa fa-clone", "tenant.dailysalesbreak"))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.MealTimeSales,
                                L("MealTimeSales"),
                                "fa fa-clone", "tenant.mealtimesale",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_MealTimeReport))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.MonthlyStoreSalesStats,
                                L("MonthlyStoreSalesStats"),
                                "fa fa-clone", "tenant.monthlystoresalesstas"))
                        )
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.SalesByPeriod, L("SalesByPeriod"),
                                "fa fa-ticket",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_SalesByPeriodReport)
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.SalesByPeriod,
                                L("SalesByPeriod"),
                                "fa fa-clone", "tenant.salesbyperiod",
                                requiredPermissionName: AppPermissions
                                    .Pages_Tenant_Connect_Report_SalesByPeriodByReport))
                        )
                        .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.SalesPromotion, L("SalesPromotion"),
                                "fa fa-ticket",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect_Report_SalesPromotionReport)
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.SalesPromotionByPromotion,
                                L("SalesPromotionReportByPromotion"),
                                "fa fa-clone", "tenant.salespromotionbypromotion",
                                requiredPermissionName: AppPermissions
                                    .Pages_Tenant_Connect_Report_SalesPromotionByProReport))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineConnect.SalesPromotionByPlant,
                                L("SalesPromotionReportByPlant"),
                                "fa fa-clone", "tenant.salespromotionbyplant",
                                requiredPermissionName: AppPermissions
                                    .Pages_Tenant_Connect_Report_SalesPromotionByPlantReport))
                        )
                    ))

                #endregion Connect

                #region House

                .AddItem(new MenuItemDefinition(PageNames.App.Tenant.House, L("House"), "fa fa-th",
                        requiredPermissionName: AppPermissions.Pages_Tenant_House,
                        featureDependency: new SimpleFeatureDependency("DinePlan.DineConnect.House"))
                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Dashboard, L("Dashboard"),
                        "fa fa-dashcube", "tenant.housedashboard",
                        requiredPermissionName: AppPermissions.Pages_Tenant_House_Dashboard))
                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.NotificationDashboard,
                        L("NotificationDashboard"),
                        "fa fa-dashcube", "tenant.notificationDashboard",
                        requiredPermissionName: AppPermissions.Pages_Tenant_House_NotificationDashboard))
                    // Master Sub Main Menu
                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Master, L("Master"), "fa fa-indent",
                            requiredPermissionName: AppPermissions.Pages_Tenant_House_Master)
                        .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.ProductionUnit, L("ProductionUnit"),
                            "fa fa-user-secret", "tenant.productionunit",
                            requiredPermissionName: AppPermissions.Pages_Tenant_House_Master_ProductionUnit))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Unit, L("Unit"), "fa fa-magnet",
                            "tenant.unit", requiredPermissionName: AppPermissions.Pages_Tenant_House_Master_Unit))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.InventoryCycleTag,
                            L("InventoryCycleTag"), "fa fa-calendar", "tenant.inventorycycletag",
                            requiredPermissionName: AppPermissions.Pages_Tenant_House_Master_InventoryCycleTag))
                        //Template
                        .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Template, L("Template"),
                              "fa fa-star-half-o", "tenant.housetemplate",
                              requiredPermissionName: AppPermissions.Pages_Tenant_House_Master_Template))
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Master, L("MaterialMenu"),
                            "fa fa-recycle", requiredPermissionName: AppPermissions.Pages_Tenant_House_Master)
                        .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Material, L("Materials"),
                            "fa fa-building", "tenant.material",
                            requiredPermissionName: AppPermissions.Pages_Tenant_House_Master_Material))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.MaterialMenuMapping,
                            L("Mappings"), "fa fa-link", "tenant.materialmenumapping",
                            requiredPermissionName: AppPermissions.Pages_Tenant_House_Master_MaterialMenuMapping))
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Master, L("Stock"),
                            "fa fa-database", requiredPermissionName: AppPermissions.Pages_Tenant_House_Master)
                        .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.MaterialLocationWiseStock,
                            L("LocationWiseStock"), "fa fa-stack-overflow", "tenant.materiallocationwisestock",
                            requiredPermissionName:
                            AppPermissions.Pages_Tenant_House_Master_MaterialLocationWiseStock))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.MaterialLocationWiseStock,
                            L("AllLocationWiseStock"), "fa fa-list-ol", "tenant.allMateriallocationwisestock",
                            requiredPermissionName:
                            AppPermissions.Pages_Tenant_House_Master_Material_AllLocation_WiseStock))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.MaterialLedger, L("Movement"),
                            "fa fa-map", "tenant.materialledger",
                            requiredPermissionName: AppPermissions.Pages_Tenant_House_Master_MaterialLedger))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.ClosingStock,
                            L("ClosingStockEntry"), "fa fa-stack-overflow", "tenant.closingstockentry",
                            requiredPermissionName:
                            AppPermissions.Pages_Tenant_House_Transaction_ClosingStockEntry))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.ManualReason,
                            L("ManualReason"), "fa fa-stack-overflow", "tenant.manualreason",
                            requiredPermissionName:
                            AppPermissions.Pages_Tenant_House_Master_ManualReason))
                    )

                    // Transaction Sub Main Menu
                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Master, L("TransactionMenu"),
                            "fa fa-money", requiredPermissionName: AppPermissions.Pages_Tenant_House_Transaction)
                        .AddItem(
                            new MenuItemDefinition(PageNames.App.DineHouse.Purchase, L("Purchasing"),
                                    "fa fa-shopping-cart",
                                    "tenant.purchase",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_House_Transaction_PurchaseOrder)
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Tax, L("PurchaseTax"),
                                    "fa fa-unlink",
                                    "tenant.tax",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_House_Master_Tax))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Supplier, L("Supplier"),
                                    "fa fa-skyatlas", "tenant.supplier",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_House_Master_Supplier))
                                // PurchaseCategory Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.PurchaseCategory,
                                    L("Category"), "fa fa-object-group", "tenant.purchasecategory",
                                    requiredPermissionName:
                                    AppPermissions.Pages_Tenant_House_Master_PurchaseCategory))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.PurchaseOrder, L("Ordering"),
                                    "fa fa-pencil-square", "tenant.purchaseorder",
                                    requiredPermissionName:
                                    AppPermissions.Pages_Tenant_House_Transaction_PurchaseOrder))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.BulkPurchaseOrder,
                                    L("BulkPurchaseOrder"),
                                    "fa fa-pencil-square", "tenant.createbulkpurchaseorder",
                                    requiredPermissionName:
                                    AppPermissions.Pages_Tenant_House_Transaction_PurchaseOrder_CreateBulkPurchaseOrder)
                                )
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.InwardDirectCredit,
                                    L("Receiving"), "fa fa-arrow-down", "tenant.inwarddirectcredit",
                                    requiredPermissionName:
                                    AppPermissions.Pages_Tenant_House_Transaction_InwardDirectCredit))
                                // Invoice Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Invoice, L("Invoicing"),
                                    "fa fa-book", "tenant.invoice",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_House_Transaction_Invoice))
                                // PurchaseReturn Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.PurchaseReturn,
                                    L("Returning"), "fa fa-arrow-up", "tenant.purchasereturn",
                                    requiredPermissionName:
                                    AppPermissions.Pages_Tenant_House_Transaction_PurchaseReturn))
                        )
                        .AddItem(
                            new MenuItemDefinition(PageNames.App.DineHouse.Purchase, L("Sales"),
                                    "fa fa-shopping-cart",
                                    "tenant.sales",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_House_Sales)
                                // SalesOrder Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.SalesTax, L("SalesTax"),
                                    "fa fa-share-square-o",
                                    "tenant.salestax",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_House_Master_SalesTax))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Customer, L("Customer"),
                                    "fa fa-user-plus", "tenant.customer",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_House_Master_Customer))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.SalesOrder, L("Ordering"),
                                    "fa fa-opera", "tenant.salesorder",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_House_Transaction_SalesOrder))

                                // SalesDeliveryOrder Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.SalesDeliveryOrder,
                                    L("SalesDeliveryOrder"), "fa fa-exchange", "tenant.salesdeliveryorder",
                                    requiredPermissionName:
                                    AppPermissions.Pages_Tenant_House_Transaction_SalesDeliveryOrder))
                                // SalesInvoice Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.SalesInvoice,
                                    L("SalesInvoice"), "fa fa-stack-exchange", "tenant.salesinvoice",
                                    requiredPermissionName:
                                    AppPermissions.Pages_Tenant_House_Transaction_SalesInvoice))
                        )
                        // Production Sub Head Menu
                        .AddItem(
                            new MenuItemDefinition(PageNames.App.DineHouse.MovementMenu, L("Movement"),
                                    "fa fa-arrows",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_House_Movement)
                                // Request Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Request, L("Request"),
                                    "fa fa-arrow-right", "tenant.request",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_House_Transaction_Request))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Issue, L("Issue"),
                                    "fa fa-arrow-left", "tenant.issue",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_House_Transaction_Issue))
                                // Return Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Return, L("Return"),
                                    "glyphicon glyphicon-registration-mark", "tenant.return",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_House_Transaction_Return))
                                // Adjustment Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Adjustment, L("Adjustment"),
                                    "fa fa-adjust", "tenant.adjustment",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_House_Transaction_Adjustment))
                                // MenuItemWastage Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.MenuItemWastage,
                                    L("MenuItemWastage"), "fa fa-bitbucket", "tenant.menuitemwastage",
                                    requiredPermissionName:
                                    AppPermissions.Pages_Tenant_House_Transaction_MenuItemWastage))
                                //ClosingStock Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.ClosingStock,
                                    L("ClosingStock"), "fa fa-stack-overflow", "tenant.closingstockadjustment",
                                    requiredPermissionName:
                                    AppPermissions.Pages_Tenant_House_Transaction_ClosingStock))
                        )
                        .AddItem(
                            new MenuItemDefinition(PageNames.App.DineHouse.ProductionMenu, L("Production"),
                                    "fa fa-hand-lizard-o",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_House_Production)
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Yield, L("Yield"),
                                    "fa fa-random ", "tenant.yield",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_House_Transaction_Yield))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Production, L("Productions"),
                                    "fa fa-bitbucket", "tenant.production",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_House_Transaction_Production))
                        )
                        .AddItem(
                            new MenuItemDefinition(PageNames.App.DineHouse.InterTransfer, L("InterTransfer"),
                                    "fa fa-exchange", "tenant.intertransfer",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_House_Transaction_InterTransfer)
                                // InterTransfer Request Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.InterTransfer, L("Request"),
                                    "fa fa-share", "tenant.intertransferrequest",
                                    requiredPermissionName:
                                    AppPermissions.Pages_Tenant_House_Transaction_InterTransfer))
                                // InterTransfer Approval Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.InterTransferApproval,
                                    L("InterTransferApproval"), "fa fa-reply", "tenant.intertransferapproval",
                                    requiredPermissionName:
                                    AppPermissions.Pages_Tenant_House_Transaction_InterTransferApproval))
                                // InterTransfer Approval Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.InterTransferApproval,
                                    L("RequestBasedPOGeneration"), "fa fa-shopping-cart", "tenant.requestintoAutoPo",
                                    requiredPermissionName:
                                    AppPermissions.Pages_Tenant_House_Transaction_InterTransferApproval))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.InterTransfer, L("Receive"),
                                    "fa fa-level-down", "tenant.intertransferreceived",
                                    requiredPermissionName:
                                    AppPermissions.Pages_Tenant_House_Transaction_InterTransferReceived))
                        )
                    )
                    .AddItem(
                        new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("DayClose"),
                            "fa fa-thumbs-up", "tenant.daycloseindex",
                            requiredPermissionName: AppPermissions.Pages_Tenant_House_Transaction_DayClose)
                    )
                    //.AddItem(
                    //    new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("IncludeSalesIntoClosedLedger"),
                    //        "fa fa-sign-in", "tenant.includemissingsales",
                    //        requiredPermissionName: AppPermissions.Pages_Tenant_House_Transaction_DayClose_MissingSalesIntoDayClose)
                    //)

                    // HouseReport Menu
                    .AddItem(
                        new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("Report"),
                                "fa fa-pencil-square-o ", "tenant.housereport",
                                requiredPermissionName: AppPermissions.Pages_Tenant_House_Master_HouseReport)

                            //Purchase Group Report
                            .AddItem(
                                new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("Invoice"),
                                        "fa fa-pencil-square-o ", "tenant.housereport",
                                        requiredPermissionName:
                                        AppPermissions.Pages_Tenant_House_Master_HouseReport_Invoice)
                                    //Invoice
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("Summary"),
                                        "fa fa-file-text-o", "tenant.invoicesummary"))
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("Analysis"),
                                        "fa fa-file-text-o", "tenant.purchasereciptreport"))
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("Consolidated"),
                                        "fa fa-file-text-o", "tenant.purchasereportconsolidated"))
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("Detail"),
                                        "fa fa-file-text-o", "tenant.purchaseconsolidateddetailreport"))
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("PurchaseTaxDetail"),
                                        "fa fa-text-width", "tenant.purchaseTaxDetailReport"))
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("ExportPurchaseWithCategory"),
                                        "fa fa-file-text-o", "tenant.purchasewithcategoryandPoReference"))
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("PurchaseSupplierWiseExcel"),
                                        "fa fa-file-excel-o", "tenant.purchasereportExcelwithGroupCategoryMaterial"))
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("PurchaseReturn"),
                                        "fa fa-registered", "tenant.purchaseReturnDetailReport"))
                            )
                            .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("InterTransfer"),
                                    "fa fa-pencil-square-o", "tenant.housereport",
                                    requiredPermissionName: AppPermissions
                                        .Pages_Tenant_House_Master_HouseReport_InterTransfer)
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("Detail"),
                                    "fa fa-file-text-o", "tenant.transferreport"))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                    L("ReceivedDetail"),
                                    "fa fa-file-text-o", "tenant.transferreceivedreport"))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("Status"),
                                    "fa fa-file-text-o", "tenant.intertransferStatusReport"))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                    L("RequestConsolidation"),
                                    "fa fa-file-text-o", "tenant.intertransferRequestConsolidationReport"))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                    L("ReceivedSummary"),
                                    "fa fa-file-text-o", "tenant.intertransferReceivedSummary"))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                    L("ReceivedDetailTrack"),
                                    "fa fa-file-text-o", "tenant.intertransferReceivedDetailTrack"))
                            )
                            .AddItem(
                                new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("Material"),
                                        "fa fa-pencil-square-o ", "tenant.housereport",
                                        requiredPermissionName:
                                        AppPermissions.Pages_Tenant_House_Master_HouseReport_Material)
                                    //RateView
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("AveragePrice"),
                                        "fa fa-file-text-o", "tenant.housereportrateview", customData: "RATEVIEW"))
                                    //Supplier RateView
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("SupplierPrice"),
                                        "fa fa-file-text-o", "tenant.housereportsupplierrateview",
                                        customData: "SUPPLIERRATEVIEW"))
                                    //Invoice Category Wise
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("Category"), "fa fa-file-text-o", "tenant.purchasereportcategorywise"))
                                    //  Wastages Report
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("Adjustment"), "fa fa-adjust", "tenant.wastagematerialreport"))
                                    //  Wastages Report
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("PhysicalStock"), "fa fa-stack-exchange", "tenant.physicalstockreport"))
                                    //  Wastages Report
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("DayCloseVariance"), "fa fa-file-text-o", "tenant.daycloseadjustmentreport"))
                                    //Material Wastage Report
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("MaterialWastage"), "fa fa-bitbucket", "tenant.materialwastagereport"))
                                    //Material Wastage Report
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("MenuWastage"), "fa fa-file-text-o", "tenant.menuWastageReport"))
                            )
                            //Product Analysis Report
                            .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("Analytical"),
                                    "fa fa-file-text-o", "tenant.housereport",
                                    requiredPermissionName:
                                    AppPermissions.Pages_Tenant_House_Master_HouseReport_Analytical
                                )
                                //Purchase Projection
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("Projection"),
                                    "fa fa-file-text-o", "tenant.purchaseprojection"))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("Material"),
                                    "fa fa-file-text-o", "tenant.reportdetailproduct", customData: "Analytical"))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("DayClose"),
                                    "fa fa-thumbs-up", "tenant.housedaycloseSalesDifference", customData: "Analytical"))
                            )
                            //  Stock Group Report
                            .AddItem(
                                new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("Stock"),
                                        "fa fa-pencil-square-o ", "tenant.housereport",
                                        requiredPermissionName:
                                        AppPermissions.Pages_Tenant_House_Master_HouseReport_Stock)
                                    //Stock As On Date
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("Detail"),
                                        "fa fa-file-text-o", "tenant.housereportstock", customData: "STOCK"))
                                    //Variance Stock As On Date
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("VarianceStockDetail"),
                                        "fa fa-file-text-o", "tenant.housereportvariancestock",
                                        customData: "VARIANCESTOCK"))
                                    //Closing Stock As On Date
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("ClosingStock"),
                                        "fa fa-file-text-o", "tenant.housereportclosingstock",
                                        customData: "CLOSINGSTOCK"))
                                    //	Manual Closing Stock Taken
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("ManualCloseStockTaken"), "fa fa-thumbs-up",
                                        "tenant.closingstocktakenreport"))
                                    //ClosingVarriance Menu
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("ClosingVariance"), "fa fa-stack-overflow",
                                        "tenant.inventoryclosingvariance"))

                                    //Stock Track Between Two Days
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("Track"),
                                        "fa fa-random", "tenant.housereportmaterialtrack"))
                                    //ConsolidatedClosingStock Diff Menu
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.ClosingStockEntry,
                                        L("ConsolidatedStockDiff"), "fa fa-digg",
                                        "tenant.consolidated-closingstockdiff",
                                        requiredPermissionName:
                                        AppPermissions.Pages_Tenant_House_Transaction_ClosingStockEntry))
                                    //Close Stock Excel with Group Category Material for the given date
                                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                        L("ClosingStockReportWithCost"), "fa fa-file-excel-o",
                                        "tenant.closingstockwithcost"))
                            )
                            //Costing
                            .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("Costing"),
                                    "fa fa-file-text-o", "tenant.housereport",
                                    requiredPermissionName:
                                    AppPermissions.Pages_Tenant_House_Master_HouseReport_Costing)
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                    L("CostVsSales"),
                                    "fa fa-file-text-o", "tenant.housereportcostingmaterial"))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("Category"),
                                    "fa fa-file-text-o", "tenant.housereportcostingcategory"))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport, L("Recipe"),
                                    "fa fa-file-text-o", "tenant.housereportcostingrecipe"))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                    L("ProductionUnit"),
                                    "fa fa-file-text-o", "tenant.productionunitcosting"))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.HouseReport,
                                    L("Menu"),
                                    "fa fa-file-text-o", "tenant.menuwisecostingconsolidateddetail"))
                            )
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.DineHouse.Numbering, L("Numbering"),
                        "fa fa-pencil-square-o", "tenant.numbering",
                        requiredPermissionName: AppPermissions.Pages_Tenant_House_Numbering)
                    )
                )

                #endregion House

                #region Cluster

                .AddItem(new MenuItemDefinition(PageNames.App.Tenant.Cluster, L("Cluster"), "fa fa-cart-arrow-down",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Cluster,
                        featureDependency: new SimpleFeatureDependency("DinePlan.DineConnect.Cluster"))

                    // Master Sub Main Menu

                    .AddItem(new MenuItemDefinition(PageNames.App.DineCluster.Master, L("Location"), "fa fa-indent",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_Master) 
                        .AddItem(new MenuItemDefinition(PageNames.App.DineCluster.DelAggLocationGroup,
                            L("Group"), "fa fa-object-group", "tenant.delagglocationgroup",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocationGroup
                        ))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineCluster.DelAggLocation,
                            L("Location"), "glyphicon glyphicon-map-marker", "tenant.delagglocation",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocation
                        ))
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineCluster.DelAggLocMapping, L("Mapping"), "fa fa-map",
                            "tenant.delagglocmapping",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocMapping
                        ))
                       
                    
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.DineCluster.Master, L("Product"), "fa fa-indent",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_Master) 
                       
                        //DelAggItemGroup
                        .AddItem(new MenuItemDefinition(PageNames.App.DineCluster.DelAggItemGroup,
                            L("DelAggItemGroup"), "fa fa-cubes", "tenant.delaggitemgroup",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_Master_DelAggItemGroup
                        ))
                        //DelAggCategory
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineCluster.DelAggCategory,
                            L("DelAggCategory"), "fa fa-caret-square-o-right", "tenant.virtualdelaggcategory",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_DelAggCategory
                        ))
                        //DelAggItem 
                        .AddItem(new MenuItemDefinition(PageNames.App.DineCluster.DelAggItem,
                            L("DelAggItem"), "glyphicon glyphicon-inbox", "tenant.delaggitem",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_Master_DelAggItem
                        ))
                        //DelAggLocationItem
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineCluster.DelAggLocationItem,
                            L("DelAggLocationItem1"),
                            "fa fa-money",
                            "tenant.delagglocationitem1",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocationItem
                        ))
                        //DelAggLocationItem
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineCluster.DelAggLocationItem,
                            L("DelAggLocationItem"),
                            "fa fa-money",
                            "tenant.delagglocationitem",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocationItem
                        ))
                        //DelAggTax 
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineCluster.DelAggTax,
                            L("DelAggTax"), "glyphicon glyphicon-bookmark", "tenant.delaggtax",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_Master_DelAggTax
                        ))
                        //DelAggCharge 
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineCluster.DelAggCharge,
                            L("Charge"), "fa fa-money", "tenant.delaggcharge",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_DelAggCharge
                        ))
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.DineCluster.Master, L("Modifier"), "fa fa-indent",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_Master) 
                        .AddItem(new MenuItemDefinition(PageNames.App.DineCluster.DelAggVariantGroup,
                            L("DelAggVariantGroup"), "glyphicon glyphicon-th", "tenant.delaggvariantgroup",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_Master_DelAggVariantGroup
                        ))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineCluster.DelAggVariant,
                            L("DelAggVariant"), "fa fa-vimeo-square", "tenant.delaggvariant",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_Master_DelAggVariant
                        ))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineCluster.DelAggModifierGroup,
                            L("DelAggModifierGroup"), "glyphicon glyphicon-th-list", "tenant.delaggmodifiergroup",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_Master_DelAggModifierGroup
                        ))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineCluster.DelAggModifier,
                            L("DelAggModifier"), "fa fa-medium", "tenant.delaggmodifier",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_Master_DelAggModifier
                        ))
                       
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.DineCluster.Master, L("Manage"), "fa fa-indent",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_Master)
                        //DelTimingGroup 
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineCluster.DelTimingGroup,
                            L("DelTimingGroup"), "fa fa-clock-o", "tenant.deltiminggroup",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_DelTimingGroup
                        ))
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineCluster.DelAggLanguages,
                            L("Language"),
                            url: "delaggLanguages",
                            icon: "icon-flag",
                            requiredPermissionName: AppPermissions.Pages_Administration_Languages
                        ))
                        //DelAggImage Menu Item
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineCluster.DelAggImage,
                            L("Image"), "fa fa-picture-o", "tenant.delaggimage",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Cluster_DelAggImage
                        ))
                    )
                )

                #endregion Cluster

                #region Engage

                .AddItem(new MenuItemDefinition(
                            PageNames.App.Tenant.Engage,
                            L("Engage"), "fa fa-chain", requiredPermissionName: AppPermissions.Pages_Tenant_Engage,
                            featureDependency: new SimpleFeatureDependency("DinePlan.DineConnect.Engage")
                        )
                        .AddItem(new MenuItemDefinition(PageNames.App.DineEngage.Membership, L("Membership"),
                                "fa fa-list-alt")
                            .AddItem(new MenuItemDefinition(PageNames.App.DineEngage.Member, L("Members"),
                                "fa fa-users",
                                "tenant.member", requiredPermissionName: AppPermissions.Pages_Tenant_Engage_Member))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineEngage.Account, L("Accounts"),
                                "fa fa-bank",
                                "tenant.memberaccount",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Engage_Member_Account))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineEngage.MembershipTier, L("Tiers"),
                                "fa fa-level-up",
                                "tenant.membershiptiers",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Engage_MembershipTier))
                        )
                        .AddItem(new MenuItemDefinition(PageNames.App.DineEngage.Loyalty, L("Loyalty"), "fa fa-heart-o")
                            .AddItem(new MenuItemDefinition(PageNames.App.DineEngage.LoyaltyProgram, L("PointCampaign"),
                                "fa fa-star-half-o",
                                "tenant.loyaltyProgram",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Engage_LoyaltyProgram))
                            .AddItem(new MenuItemDefinition(
                                PageNames.App.DineEngage.GiftVoucherCategory,
                                L("VoucherCategories"),
                                "fa fa-caret-square-o-up",
                                "tenant.giftvouchercategory",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Engage_GiftVoucherCategory
                            ))
                            .AddItem(new MenuItemDefinition(PageNames.App.DineEngage.GiftVoucher, L("GiftVouchers"),
                                "fa fa-ticket",
                                "tenant.giftvouchertype",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Engage_GiftVoucherType))
                        )
                        .AddItem(
                            new MenuItemDefinition(PageNames.App.DineEngage.ServiceInfo, L("ServiceInfo"),
                                    "fa-envelope")
                                .AddItem(new MenuItemDefinition(PageNames.App.DineEngage.ServiceInfoTemplate,
                                    L("Templates"),
                                    "fa fa-bars", "tenant.serviceinfotemplates",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_Engage_ServiceInfo_Template))
                        )
                    /*
                    .AddItem(new MenuItemDefinition(PageNames.App.DineEngage.GiftVoucher, L("GiftVoucher"),
                            "fa fa-briefcase")
                        .AddItem(new MenuItemDefinition(
                            PageNames.App.DineEngage.GiftVoucherCategory,
                            L("Category"),
                            "fa fa-caret-square-o-up",
                            "tenant.giftvouchercategory",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Engage_GiftVoucherCategory
                        ))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineEngage.GiftVoucherType, L("GiftVoucher"),
                            "fa fa-bookmark", "tenant.giftvouchertype",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Engage_GiftVoucherType))
                    )
    
                    .AddItem(new MenuItemDefinition(PageNames.App.DineEngage.Reservation, L("Reservation"),
                        "fa fa-cutlery", "tenant.reservation",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Engage_Reservation))
                    .AddItem(new MenuItemDefinition(PageNames.App.DineEngage.LoyaltyProgram, L("LoyaltyProgram"),
                            "fa fa-cutlery",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Engage_LoyaltyProgram)
                        .AddItem(new MenuItemDefinition(PageNames.App.DineEngage.LoyaltyProgram, L("LoyaltyPrograms"),
                            "fa fa-cutlery", "tenant.loyaltyProgram",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Engage_LoyaltyProgram))
                        .AddItem(new MenuItemDefinition(PageNames.App.DineEngage.LoyaltyProgramStage,
                            L("ProgramStages"),
                            "fa fa-cutlery", "tenant.stage",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Engage_Stage)))*/
                )

                #endregion Engage

                #region Wheel

                .AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Wheel,
                        L("Wheel"), "fa fa-rocket", requiredPermissionName: AppPermissions.Pages_Tenant_Wheel,
                        featureDependency: new SimpleFeatureDependency("DinePlan.DineConnect.Wheel")
                    ).AddItem(new MenuItemDefinition(PageNames.App.DineWheel.Dashboard, L("Dashboard"), "fa fa-rocket",
                        "tenant.wheeldashboard", requiredPermissionName: AppPermissions.Pages_Tenant_Wheel_Dashboard))
                    .AddItem(new MenuItemDefinition(PageNames.App.DineWheel.WheelOrder, L("Order"), "fa fa-list",
                        "tenant.wheelorder", requiredPermissionName: AppPermissions.Pages_Tenant_Wheel_Order))
                )

                #endregion Wheel

                #region Swipe

                .AddItem(new MenuItemDefinition(
                        PageNames.App.Host.Swipe,
                        L("Swipe"), "fa fa-credit-card", requiredPermissionName: AppPermissions.Pages_Host_Swipe
                        , featureDependency: new SimpleFeatureDependency("DinePlan.DineConnect.Swipe")
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.Swipe.Dashboard, L("Dashboard"),
                        "fa fa-dot-circle-o", "host.swipedashboard",
                        requiredPermissionName: AppPermissions.Pages_Host_Swipe_Dashboard))

                    #region SwipeMaster

                    .AddItem(new MenuItemDefinition(
                            PageNames.App.Swipe.Master, L("Master"), "fa fa-indent",
                            requiredPermissionName: AppPermissions.Pages_Host_Swipe_Master)

                        // SwipePaymentType Menu
                        .AddItem(new MenuItemDefinition(PageNames.App.Swipe.SwipePaymentType, L("PaymentType"),
                            "fa fa-money", "host.swipepaymenttype",
                            requiredPermissionName: AppPermissions.Pages_Host_Swipe_Master_SwipePaymentType))

                        // SwipeCardType Menu
                        .AddItem(new MenuItemDefinition(PageNames.App.Swipe.SwipeCardType, L("SwipeCardType"),
                            "fa fa-futbol-o", "host.swipecardtype",
                            requiredPermissionName: AppPermissions.Pages_Host_Swipe_Master_SwipeCardType))

                        // SwipeCard Menu
                        .AddItem(new MenuItemDefinition(PageNames.App.Swipe.SwipeCard, L("SwipeCard"),
                            "fa fa-credit-card", "host.swipecard",
                            requiredPermissionName: AppPermissions.Pages_Host_Swipe_Master_SwipeCard))

                        // SwipeMember Menu
                        .AddItem(new MenuItemDefinition(PageNames.App.Swipe.SwipeMember, L("SwipeMember"),
                            "fa fa-user-plus", "host.swipemember",
                            requiredPermissionName: AppPermissions.Pages_Host_Swipe_Master_SwipeMember))
                    )

                    #endregion SwipeMaster

                    #region SwipeTrasaction

                    .AddItem(new MenuItemDefinition(PageNames.App.Swipe.Transaction, L("Transaction"), "fa fa-money",
                            requiredPermissionName: AppPermissions.Pages_Host_Swipe_Transaction)

                        // SwipeShift Menu
                        .AddItem(new MenuItemDefinition(PageNames.App.Swipe.SwipeShift, L("SwipeShift"),
                            "fa fa-clock-o ", "host.swipeshift",
                            requiredPermissionName: AppPermissions.Pages_Host_Swipe_Transaction_SwipeShift))

                        // MemberCard Menu
                        .AddItem(new MenuItemDefinition(PageNames.App.Swipe.MemberCard, L("MemberCard"),
                            "fa  fa-exchange", "host.membercardview",
                            requiredPermissionName: AppPermissions.Pages_Host_Swipe_Transaction_MemberCard))
                    )

                    #endregion SwipeTrasaction

                    .AddItem(new MenuItemDefinition(PageNames.App.Swipe.Transaction, L("Report"),
                            "fa fa-pencil-square-o ", requiredPermissionName: AppPermissions.Pages_Host_Swipe_Report)
                        .AddItem(new MenuItemDefinition(PageNames.App.Swipe.SwipeShift, L("SwipeShift"),
                            "fa fa-pencil-square-o ", "host.reportswipeshift"))
                        .AddItem(new MenuItemDefinition(PageNames.App.Swipe.SwipeShift, L("Transaction"),
                            "fa fa-pencil-square-o ", "host.reportswipeshifttransactiondetail"))
                    )
                )

                #endregion Swipe

                #region Tick

                .AddItem(new MenuItemDefinition(
                            PageNames.App.Tenant.Tick,
                            L("Tick"), "fa fa-check-square", requiredPermissionName: AppPermissions.Pages_Tenant_Tick,
                            featureDependency: new SimpleFeatureDependency("DinePlan.DineConnect.Tick")
                        )

                        //.AddItem(new MenuItemDefinition(PageNames.App.DineTick.Dashboard, L("Dashboard"),
                        //    "fa fa-dot-circle-o", "tenant.tickdashboard",
                        //    requiredPermissionName: AppPermissions.Pages_Tenant_Tick_Dashboard))
                        //.AddItem(new MenuItemDefinition(
                        //    PageNames.App.DineTick.Master,
                        //    L("Master"), "fa fa-indent",
                        //    requiredPermissionName: AppPermissions.Pages_Tenant_Tick_Master)
                        //    .AddItem(new MenuItemDefinition(PageNames.App.DineTick.Department, L("Department"),
                        //        "fa fa-ticket", "tenant.tickdepartment",
                        //        requiredPermissionName: AppPermissions.Pages_Tenant_Tick_Employee_Department))
                        //).AddItem(new MenuItemDefinition(
                        //    PageNames.App.DineTick.Employee,
                        //    L("Employee"), "fa fa-street-view",
                        //    requiredPermissionName: AppPermissions.Pages_Tenant_Tick_Employee)
                        //    .AddItem(new MenuItemDefinition(PageNames.App.DineTick.Employee, L("Employee"),
                        //        "fa fa-street-view", "tenant.employee",
                        //        requiredPermissionName: AppPermissions.Pages_Tenant_Tick_Employee))
                        //    .AddItem(new MenuItemDefinition(PageNames.App.DineTick.EmployeeAttendance, L("Attendance"),
                        //        "fa fa-at", "tenant.employeeattendance",
                        //        requiredPermissionName: AppPermissions.Pages_Tenant_Tick_EmployeeAttendance))
                        //)
                        //.AddItem(new MenuItemDefinition(PageNames.App.DineTick.Ticket, L("Ticket"),
                        //    "fa fa-ticket", "tenant.tickticket",
                        //    requiredPermissionName: AppPermissions.Pages_Tenant_Tick_Ticket))
                        //.AddItem(new MenuItemDefinition(PageNames.App.DineTick.EmployeeAttendance, L("Report"),
                        //    "fa fa-edit", "tenant.attendanceReport",
                        //    requiredPermissionName: AppPermissions.Pages_Tenant_Tick_Ticket))

                        //     Employee Dashboard
                        .AddItem(new MenuItemDefinition(PageNames.App.Hr.EmployeeDashboard, L("EmployeeDashboard"),
                            url: "tenant.employeedashboard", icon: "fa fa-user",
                            requiredPermissionName: AppPermissions.Pages_Tenant_EmployeeDashboard))

                        #region Tick_Operation

                        .AddItem(new MenuItemDefinition(PageNames.App.Hr.Operation, L("Operation"), "fa fa-opera",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Transaction_DutyChart)

                                // DutyChart Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.Hr.DutyChart, L("DutyChart"),
                                    "fa fa-industry",
                                    "tenant.manpower",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Transaction_DutyChart))

                                // LeaveRequest Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.Hr.LeaveRequest, L("LeaveRequest"),
                                    "fa fa-share",
                                    "tenant.leaverequest",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Master_LeaveRequest))

                                // ManualIncentive Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.Hr.ManualIncentive, L("ManualIncentive"),
                                    "fa fa-pencil-square", "tenant.manualincentive",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Transaction_ManualIncentive))

                                #region Tick_Operation_Schedule _Master

                                .AddItem(new MenuItemDefinition(PageNames.App.Hr.Master, L("ScheduleMaster"),
                                        "fa fa-edit",
                                        requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Transaction_DutyChart)

                                    // DcHeadMaster Menu
                                    .AddItem(new MenuItemDefinition(PageNames.App.Hr.DcHeadMaster, L("DcHeadMaster"),
                                        "fa fa-header",
                                        "tenant.dcheadmaster",
                                        requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Master_DcHeadMaster))

                                    // DcGroupMaster Menu
                                    .AddItem(new MenuItemDefinition(PageNames.App.Hr.DcGroupMaster, L("DcGroupMaster"),
                                        "fa fa-object-group", "tenant.dcgroupmaster",
                                        requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Master_DcGroupMaster))

                                    // DcStationMaster Menu
                                    .AddItem(new MenuItemDefinition(PageNames.App.Hr.DcStationMaster,
                                        L("DcStationMaster"),
                                        "fa fa-houzz", "tenant.dcstationmaster",
                                        requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Master_DcStationMaster))

                                    // DcShiftMaster Menu
                                    .AddItem(new MenuItemDefinition(PageNames.App.Hr.DcShiftMaster, L("DcShiftMaster"),
                                        "fa fa-hourglass-start", "tenant.dcshiftmaster",
                                        requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Master_DcShiftMaster))
                                )

                                #endregion Tick_Operation_Schedule _Master

                                #region Tick_Operation_Master

                                .AddItem(new MenuItemDefinition(PageNames.App.Hr.Master, L("Master"), "fa fa-indent",
                                        requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Transaction_DutyChart)
                                    // PublicHoliday Menu
                                    .AddItem(new MenuItemDefinition(PageNames.App.Hr.PublicHoliday, L("PublicHoliday"),
                                        "fa fa-calendar", "tenant.publicholiday",
                                        requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Master_PublicHoliday))

                                    // WorkDay Menu
                                    .AddItem(new MenuItemDefinition(PageNames.App.Hr.WorkDay, L("WorkDay"),
                                        "fa fa-clock-o",
                                        "tenant.hrworkday",
                                        requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Master_WorkDay))
                                )

                            #endregion Tick_Operation_Master

                        )

                        #endregion Tick_Operation

                        #region Tick_Employee

                        .AddItem(new MenuItemDefinition(PageNames.App.Hr.Employee, L("Employee"), "fa fa-users",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Master_PersonalInformation)

                                // PersonalInformation Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.Hr.PersonalInformation,
                                    L("PersonalInformation"),
                                    "fa fa-users", "tenant.personalinformation",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Master_PersonalInformation))

                                // SalaryInfo Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.Hr.SalaryInfo, L("SalaryInfo"),
                                    "fa fa-money", "tenant.salaryinfo",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Master_SalaryInfo))
                                .AddItem(new MenuItemDefinition(PageNames.App.DineTick.EmployeeAttendance,
                                    L("Attendance"),
                                    "fa fa-at", "tenant.employeeattendance",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_Tick_EmployeeAttendance))

                                // EmployeeSkillSet Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.Hr.EmployeeSkillSet,
                                    L("EmployeeSkillSet"),
                                    "fa fa-book", "tenant.employeeskillset",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Master_EmployeeSkillSet))

                                // EmployeeDocumentInfo Menu
                                .AddItem(new MenuItemDefinition(PageNames.App.Hr.EmployeeDocumentInfo,
                                    L("EmployeeDocumentInfo"),
                                    "fa fa-suitcase", "tenant.employeedocumentinfo",
                                    requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Master_EmployeeDocumentInfo))

                                #region Tick_Employee_Master

                                .AddItem(new MenuItemDefinition(PageNames.App.Hr.Master, L("Master"), "fa fa-indent",
                                        requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Master)

                                    // IncentiveCategory Menu
                                    .AddItem(new MenuItemDefinition(PageNames.App.Hr.IncentiveCategory,
                                        L("IncentiveCategory"), "fa fa-futbol-o", "tenant.incentivecategory",
                                        requiredPermissionName: AppPermissions
                                            .Pages_Tenant_Hr_Transaction_IncentiveCategory))

                                    // IncentiveTag Menu
                                    .AddItem(new MenuItemDefinition(PageNames.App.Hr.IncentiveTag, L("IncentiveTag"),
                                        "fa fa-dollar", "tenant.incentivetag",
                                        requiredPermissionName: AppPermissions
                                            .Pages_Tenant_Hr_Transaction_IncentiveTag))

                                    // SkillSet Menu
                                    .AddItem(new MenuItemDefinition(PageNames.App.Hr.SkillSet, L("SkillSet"),
                                        "fa fa-skyatlas",
                                        "tenant.skillset",
                                        requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Master_SkillSet))
                                    // DocumentInfo Menu
                                    .AddItem(new MenuItemDefinition(PageNames.App.Hr.DocumentInfo, L("DocumentInfo"),
                                        "fa fa-file-text", "tenant.documentinfo",
                                        requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Master_DocumentInfo))
                                    // LeaveType Menu
                                    .AddItem(new MenuItemDefinition(PageNames.App.Hr.LeaveType, L("LeaveType"),
                                        "fa fa-clone",
                                        "tenant.leavetype",
                                        requiredPermissionName: AppPermissions.Pages_Tenant_Hr_Master_LeaveType))
                                )

                            #endregion Tick_Employee_Master

                        )

                        #endregion Tick_Employee

                        #region Tick_Reports

                        // Reports
                        .AddItem(new MenuItemDefinition(PageNames.App.Hr.Master, L("Report"), "fa fa-flag",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect)
                            //        // Salary Cost Menu
                            .AddItem(new MenuItemDefinition(PageNames.App.Hr.SalaryCost, L("Cost"), "fa fa-money",
                                "tenant.salaryCost",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect))
                            //        // Salary Cost Menu
                            .AddItem(new MenuItemDefinition(PageNames.App.DineTick.TimeAttendanceReport,
                                L("TimeAttendanceReport"), "fa fa-file",
                                "tenant.timeAttendanceReport",
                                requiredPermissionName: AppPermissions.Pages_Tenant_Connect))
                        )

                    #endregion Tick_Reports

                )

                #endregion Tick

                #region Cater

                .AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Cater,
                        L("Cater"), "fa fa-server",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Cater,
                        featureDependency: new SimpleFeatureDependency("DinePlan.DineConnect.Cater")
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.DineCater.Dashboard, L("Dashboard"), "fa fa-rocket",
                        "", requiredPermissionName: AppPermissions.Pages_Tenant_Cater_Dashboard))
                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Cater.Customer,
                        L("Customer"), "fa fa-users",
                        "tenant.caterCustomer",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Cater_Customer))
                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Cater.QuotationHeader,
                        L("Quotation"), "fa fa-plus-square",
                        "tenant.quotation",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Cater_QuotationHeader))
                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Cater.QuotationHeader,
                        L("Costing"), "fa fa-money",
                        "tenant.caterquotecosting",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Cater_QuotationHeader))
                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Cater.QuotationHeader,
                        L("AutoPO"), "fa fa-shopping-cart",
                        "tenant.caterrequiredmaterialautopo",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Cater_QuotationHeader))
                )

                #endregion Cater

                #region Go

                .AddItem(new MenuItemDefinition(PageNames.App.Tenant.Go, L("Go"), "fa fa-rocket",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Go,
                        featureDependency: new SimpleFeatureDependency("DinePlan.DineConnect.Go"))
                    .AddItem(new MenuItemDefinition(PageNames.App.DineGo.DineGoDevice, L("Device"), "fa fa-desktop",
                        "tenant.dinegodevice", requiredPermissionName: AppPermissions.Pages_Tenant_Go_DineGoDevice))
                    .AddItem(new MenuItemDefinition(PageNames.App.DineGo.DineGoBrand, L("Brand"), "fa fa-list",
                        "tenant.dinegobrand", requiredPermissionName: AppPermissions.Pages_Tenant_Go_DineGoBrand))
                    .AddItem(new MenuItemDefinition(PageNames.App.DineGo.DineGoDepartment, L("Department"),
                        "fa fa-adjust",
                        "tenant.dinegodepartment",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Go_DineGoDepartment))
                    .AddItem(new MenuItemDefinition(PageNames.App.DineGo.DineGoPaymentType, L("PaymentType"),
                        "fa fa-credit-card",
                        "tenant.dinegopaymenttype",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Go_DineGoPaymentType))
                    .AddItem(new MenuItemDefinition(PageNames.App.DineGo.DineGoCharge, L("Charges"), "fa fa-money",
                        "tenant.dinegopaymenttypecharge",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Go_DineGoCharge))
                )

                #endregion Go

                #region Play

                .AddItem(new MenuItemDefinition(PageNames.App.Tenant.Play, L("Play"), "fa fa-youtube-play",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Play,
                        featureDependency: new SimpleFeatureDependency("DinePlan.DineConnect.Play"))
                    .AddItem(new MenuItemDefinition(PageNames.App.DinePlay.DinePlayResolutions,
                        L("MenuResolutions"), "fa fa-arrows",
                        "tenant.dineplayresolutions",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Play_Resolution)
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.DinePlay.DinePlayDisplayGroups,
                        L("MenuDisplayGroups"), "fa fa-object-group",
                        "tenant.dineplaydisplaygroups",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Play_DisplayGroup)
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.DinePlay.DinePlayDisplays,
                        L("MenuDisplays"), "fa fa-television  ",
                        "tenant.dineplaydisplays",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Play_Display)
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.DinePlay.DinePlayDayParting,
                        L("Dayparting"), "fa fa-clock-o",
                        "tenant.dineplaydayparting",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Play_DayParting)
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.DinePlay.DinePlayDayLayouts,
                        L("MenuDisplayLayouts"), "fa fa-th-large",
                        "tenant.dineplaylayouts",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Play_Layout)
                    )
                    .AddItem(new MenuItemDefinition(PageNames.App.DinePlay.DinePlayDaySchedules,
                        L("MenuDisplaySchedules"), "fa fa-calendar",
                        "tenant.dineplayschedules",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Play_Schedule)
                    )
                )

                #endregion Play

                #region Administration

                .AddItem(new MenuItemDefinition(
                        PageNames.App.Common.Administration,
                        L("Administration"), "icon-wrench"
                    )
                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Common.OrganizationUnits,
                        L("OrganizationUnits"), "fa fa-user", "tenant.userorganizationunit",
                        requiredPermissionName: AppPermissions.Pages_Administration_OrganizationUnits)
                    )
                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Common.UserLocation,
                        L("UserLocations"), "fa fa-user", "tenant.userlocation",
                        requiredPermissionName: AppPermissions.Pages_Administration_Users)
                    )
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.Tenant.Addons,
                            L("Addons"),
                            url: "tenant.addons",
                            icon: "fa fa-houzz",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Addons
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.Common.Languages,
                            L("Languages"),
                            url: "languages",
                            icon: "icon-flag",
                            requiredPermissionName: AppPermissions.Pages_Administration_Languages
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.Common.DinePlanLanguages,
                            L("DinePlanLanguages"),
                            url: "dineplanLanguages",
                            icon: "icon-flag",
                            requiredPermissionName: AppPermissions.Pages_Administration_Languages
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.Common.AuditLogs,
                            L("AuditLogs"),
                            url: "auditLogs",
                            icon: "icon-lock",
                            requiredPermissionName: AppPermissions.Pages_Administration_AuditLogs
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.Common.SystemLogs,
                            L("SystemLogs"),
                            url: "systemLogs",
                            icon: "icon-lock",
                            requiredPermissionName: AppPermissions.Pages_Administration_AuditLogs
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.Common.TrackerLogs,
                            L("TrackerLogs"),
                            url: "trackerLogs",
                            icon: "icon-lock",
                            requiredPermissionName: AppPermissions.Pages_Administration_AuditLogs
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.Common.ExternalLogs,
                            L("ExternalLogs"),
                            url: "externalLogs",
                            icon: "icon-lock",
                            requiredPermissionName: AppPermissions.Pages_Administration_AuditLogs
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.Common.Templates,
                            L("Templates"),
                            url: "tenant.templates",
                            icon: "fa fa-cubes",
                            requiredPermissionName: AppPermissions.Pages_Administration_Templates
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Common.LastSync,
                        L("LastSync"), "fa fa-database",
                        "tenant.syncer",
                        requiredPermissionName: AppPermissions.Pages_Administration_LastSync)
                    )

                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Common.SyncDashboard,
                        L("SyncDashboard"), "fa fa-refresh",
                        "tenant.syncdashboard",
                        requiredPermissionName: AppPermissions.Pages_Administration_SyncDashboard)
                    )
                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Common.TickMessage,
                        L("TickMessage"), "fa fa-money",
                        "tenant.tickmessage",
                        requiredPermissionName: AppPermissions.Pages_Tenant_TickMessage))
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.Common.Roles,
                            L("Roles"),
                            url: "roles",
                            icon: "icon-briefcase",
                            requiredPermissionName: AppPermissions.Pages_Administration_Roles
                        )
                    ).AddItem(new MenuItemDefinition(
                            PageNames.App.Common.Users,
                            L("Users"),
                            url: "users",
                            icon: "icon-users",
                            requiredPermissionName: AppPermissions.Pages_Administration_Users
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.Common.SMTPSetting,
                            L("SMTPSetting"),
                            url: "smtpsetting",
                            icon: "fa fa-envelope",
                            requiredPermissionName: AppPermissions.Pages_Administration_SMTPSetting
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.Host.Settings,
                            L("Settings"),
                            url: "host.settings",
                            icon: "icon-settings",
                            requiredPermissionName: AppPermissions.Pages_Administration_Host_Settings
                        )
                    ).AddItem(new MenuItemDefinition(
                            PageNames.App.Common.Maintenance,
                            L("Maintenance"),
                            url: "maintenance",
                            icon: "icon-lock",
                            requiredPermissionName: AppPermissions.Pages_Administration_Maintenance
                        )
                    ).AddItem(new MenuItemDefinition(
                            PageNames.App.Tenant.Settings,
                            L("Settings"),
                            url: "tenant.settings",
                            icon: "icon-settings",
                            requiredPermissionName: AppPermissions.Pages_Administration_Tenant_Settings
                        )
                    )
                );

            #endregion Administration
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, DineConnectConsts.LocalizationSourceName);
        }
    }
}