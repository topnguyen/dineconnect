﻿/* 'app' MODULE DEFINITION */
var appModule = angular.module("app", [
    "ui.router",
    "ui.bootstrap",
    "ui.utils",
    "ui.jq",
    "ui.grid",
    "ui.grid.pagination",
    "ui.grid.edit",
    "ui.grid.cellNav",
    "ui.grid.autoResize",
    "ui.bootstrap.datetimepicker",
    'ui.dateTimeInput', 'jcs-autoValidate',
    "oc.lazyLoad",
    "indexed-db",
    "angucomplete-alt",
    "cfp.hotkeys",
    "ngSanitize",
    "ui.select",
    "chart.js",
    "multipleDatePicker",
    "angularFileUpload",
    "daterangepicker",
    "angularMoment",
    "frapontillo.bootstrap-switch",
    "wingify.timePicker",
    "colorpicker",
    "ngTable",
    "angular-sortable-view",
    "abp",
    "ng.deviceDetector",
    "angular-timeline",
    "angular-jquery-querybuilder",
    "summernote",
    "dualmultiselect",
    "ngThumb",
    'ui.calendar',
    'ngTagsInput',
    'ui.grid.selection',
    'ngQuill',
    'ui.grid.treeView',
    'ui.grid.treeBase',
    'ui.grid.grouping',
    'ui.sortable',
    'FileManagerApp',
    'ui.grid.pinning',
    'ui.grid.expandable',
    'formly',
    'formlyBootstrap'
]);

/* LAZY LOAD CONFIG */

/* This application does not define any lazy-load yet but you can use $ocLazyLoad to define and lazy-load js/css files.
 * This code configures $ocLazyLoad plug-in for this application.
 * See it's documents for more information: https://github.com/ocombe/ocLazyLoad
 */

//file manager
appModule.config(['fileManagerConfigProvider', function (config) {
    var defaults = config.$get();
    config.set({
        appName: 'angular-filemanager',
        pickCallback: function (item) {
            var msg = 'Picked %s "%s" for external use'
                .replace('%s', item.type)
                .replace('%s', item.fullPath());
            window.alert(msg);
        },

        allowedActions: angular.extend(defaults.allowedActions, {
            pickFiles: true,
            pickFolders: false
        })
    });
}]);

appModule.config([
    "$ocLazyLoadProvider", function ($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({
            cssFilesInsertBefore: "ng_load_plugins_before", // load the css files before a LINK element with this ID.
            debug: false,
            events: true,
            modules: []
        });
    }
]);

/* eslint-disable */
/*LOCAL DB*/
appModule.config([
    "indexeddbProvider", function (indexeddbProvider) {
        indexeddbProvider.setDbName("DineConnect"); // your database name
        indexeddbProvider.setDbVersion(1); // your database version

        var tables = [
            {
                name: "menus",
                fields: [
                    {
                        name: "locationId",
                        keyPath: true
                    },
                    {
                        name: "items",
                        multiEntry: true
                    }
                ]
            }
        ];
        indexeddbProvider.setDbTables(tables);
    }
]);
/*CHART */
appModule.config([
    "ChartJsProvider", function (chartJsProvider) {
        chartJsProvider.setOptions({ colors: ["#803690", "#00ADF9", "#DCDCDC", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"] });
    }
]);

/* THEME SETTINGS */
App.setAssetsPath(abp.appPath + "metronic/assets/");
appModule.factory("settings", [
    "$rootScope", function ($rootScope) {
        var settings = {
            layout: {
                pageSidebarClosed: false, // sidebar menu state
                pageContentWhite: true, // set page content layout
                pageBodySolid: false, // solid body color state
                pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
            },
            layoutImgPath: App.getAssetsPath() + "admin/layout4/img/",
            layoutCssPath: App.getAssetsPath() + "admin/layout4/css/",
            assetsPath: abp.appPath + "metronic/assets",
            globalPath: abp.appPath + "metronic/assets/global",
            layoutPath: abp.appPath + "metronic/assets/layouts/layout4"
        };

        $rootScope.settings = settings;

        return settings;
    }
]);
/* ROUTE DEFINITIONS */

appModule.config([
    "$stateProvider", "$urlRouterProvider",
    function ($stateProvider, $urlRouterProvider) {
        // Default route (overrided below if user has permission)
        $urlRouterProvider.otherwise("/welcome");

        //Welcome page
        $stateProvider.state("welcome", {
            url: "/welcome",
            templateUrl: "~/App/common/views/welcome/index.cshtml"
        });

        //COMMON routes

        if (abp.auth.hasPermission("Pages.Administration.Roles")) {
            $stateProvider.state("roles", {
                url: "/roles",
                templateUrl: "~/App/common/views/roles/index.cshtml",
                menu: "Administration.Roles"
            });
        }

        if (abp.auth.hasPermission("Pages.Administration.Users")) {
            $stateProvider.state("users", {
                url: "/users?filterText",
                templateUrl: "~/App/common/views/users/index.cshtml",
                menu: "Administration.Users"
            });
        }

        if (abp.auth.hasPermission("Pages.Administration.SMTPSetting")) {
            $stateProvider.state("smtpsetting", {
                url: "/smtpsetting",
                templateUrl: "~/App/common/views/smtpsetting/index.cshtml",
                menu: "Administration.SMTPSetting"
            });
        }
        if (abp.auth.hasPermission("Pages.Administration.Languages")) {
            $stateProvider.state("languages", {
                url: "/languages",
                templateUrl: "~/App/common/views/languages/index.cshtml",
                menu: "Administration.Languages"
            });

            if (abp.auth.hasPermission("Pages.Administration.Languages.ChangeTexts")) {
                $stateProvider.state("languageTexts", {
                    url: "/languages/texts/:languageName?sourceName&baseLanguageName&targetValueFilter&filterText",
                    templateUrl: "~/App/common/views/languages/texts.cshtml",
                    menu: "Administration.Languages"
                });
            }
        }

        if (abp.auth.hasPermission("Pages.Administration.Languages")) {
            $stateProvider.state("dineplanLanguages", {
                url: "/dineplan-languages",
                templateUrl: "~/App/common/views/dineplanLanguages/index.cshtml",
                menu: "Administration.DinePlanLanguages"
            });

            if (abp.auth.hasPermission("Pages.Administration.Languages.ChangeTexts")) {
                $stateProvider.state("dineplanLanguageTexts", {
                    url: "/dineplan-languages/texts/:languageName?filterText",
                    templateUrl: "~/App/common/views/dineplanLanguages/texts.cshtml",
                    menu: "Administration.DinePlanLanguages"
                });
            }
        }

        if (abp.auth.hasPermission("Pages.Administration.Maintenance")) {
            $stateProvider.state("maintenance", {
                url: "/maintenance",
                templateUrl: "~/App/common/views/maintenance/index.cshtml",
                menu: "Administration.Maintenance"
            });
        }

        if (abp.auth.hasPermission("Pages.Administration.AuditLogs")) {
            $stateProvider.state("auditLogs", {
                url: "/auditLogs",
                templateUrl: "~/App/common/views/auditLogs/index.cshtml",
                menu: "Administration.AuditLogs"
            });
        }
        if (abp.auth.hasPermission("Pages.Administration.AuditLogs")) {
            $stateProvider.state("systemLogs", {
                url: "/systemLogs",
                templateUrl: "~/App/common/views/auditLogs/systemlog.cshtml",
                menu: "Administration.AuditLogs"
            });
        }
        if (abp.auth.hasPermission("Pages.Administration.AuditLogs")) {
            $stateProvider.state("trackerLogs", {
                url: "/trackerLogs",
                templateUrl: "~/App/common/views/trackerAuditLogs/index.cshtml",
                menu: "Administration.TrackerLogs"
            });
        }
        if (abp.auth.hasPermission("Pages.Administration.AuditLogs")) {
            $stateProvider.state("externalLogs", {
                url: "/externalLogs",
                templateUrl: "~/App/common/views/externalLogs/index.cshtml",
                menu: "Administration.ExternalLogs"
            });
        }
        if (abp.auth.hasPermission("Pages.Administration.OrganizationUnits")) {
            $stateProvider.state("tenant.userorganizationunit", {
                url: "/organizationUnits",
                templateUrl: "~/App/common/views/organizationUnits/index.cshtml",
                menu: "Administration.OrganizationUnits"
            });
        }

        if (abp.auth.hasPermission("Pages.Administration.Users")) {
            $stateProvider.state("tenant.userlocation", {
                url: "/user-location",
                templateUrl: "~/App/common/views/locationuser/locationuser.cshtml",
                menu: "Administration.UserLocation"
            });
        }

        $stateProvider.state("notifications", {
            url: "/notifications",
            templateUrl: "~/App/common/views/notifications/index.cshtml"
        });

        //HOST routes

        $stateProvider.state("host", {
            'abstract': true,
            url: "/host",
            template: "<div ui-view class=\"fade-in-up\"></div>"
        });

        if (abp.auth.hasPermission("Pages.Tenants")) {
            $urlRouterProvider.otherwise("/host/tenants"); //Entrance page for the host
            $stateProvider.state("host.tenants", {
                url: "/tenants",
                templateUrl: "~/App/host/views/tenants/index.cshtml",
                menu: "Tenants"
            });
        }

        if (abp.auth.hasPermission("Pages.Editions")) {
            $stateProvider.state("host.editions", {
                url: "/editions",
                templateUrl: "~/App/host/views/editions/index.cshtml",
                menu: "Editions"
            });
        }

        if (abp.auth.hasPermission("Pages.Administration.Host.Settings")) {
            $stateProvider.state("host.settings", {
                url: "/settings",
                templateUrl: "~/App/host/views/settings/index.cshtml",
                menu: "Administration.Settings.Host"
            });
        }

        //TENANT routes

        $stateProvider.state("tenant", {
            'abstract': true,
            url: "/tenant",
            template: "<div ui-view class=\"fade-in-up\"></div>"
        });

        if (abp.auth.hasPermission("Pages.Tenant.Dashboard")) {
            $urlRouterProvider.otherwise("/tenant/dashboard"); //Entrance page for a tenant
            $stateProvider.state("tenant.dashboard", {
                url: "/dashboard",
                templateUrl: "~/App/tenant/views/dashboard/index.cshtml",
                menu: "Dashboard.Tenant"
            });
        }

        if (abp.auth.hasPermission("Pages.Administration.Tenant.Settings")) {
            $stateProvider.state("tenant.settings", {
                url: "/settings",
                templateUrl: "~/App/tenant/views/settings/index.cshtml",
                menu: "Administration.Settings.Tenant"
            });
        }
        if (abp.auth.hasPermission("Pages.Administration.CustomReport")) {
            $stateProvider.state("tenant.customreport", {
                url: "/custom-report",
                templateUrl: "~/App/tenant/views/customreport/index.cshtml",
                menu: "Administration.CustomReport"
            });
        }
        if (abp.auth.hasPermission("Pages.Administration.LastSync")) {
            $stateProvider.state("tenant.syncer", {
                url: "/location-syncer",
                templateUrl: "~/App/tenant/views/syncer/index.cshtml",
                menu: "Administration.LastSync"
            });
        }
        if (abp.auth.hasPermission("Pages.Administration.SyncDashboard")) {
            $stateProvider.state("tenant.syncdashboard", {
                url: "/syncdashboard",
                templateUrl: "~/App/tenant/views/syncdashboard/index.cshtml",
                menu: "Administration.SyncDashboard"
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.TickMessage')) {
            $stateProvider.state('tenant.tickmessage', {
                url: '/tick-tickmessage',
                templateUrl: '~/App/tenant/views/tickmessages/index.cshtml',
                menu: 'Administration.TickMessage'
            });

            $stateProvider.state("tenant.detailtickmessage", {
                url: "/tick-detailtickmessage/:id",
                templateUrl: '~/App/tenant/views/tickmessages/tickdetail.cshtml',
                menu: 'Administration.TickMessage'
            });
        }
        if (abp.auth.hasPermission("Pages.Administration.Templates")) {
            $stateProvider.state("tenant.templates", {
                url: "/tenant-templates",
                templateUrl: "~/App/tenant/views/templates/index.cshtml",
                menu: "Administration.Templates"
            });

            $stateProvider.state("tenant.detailtemplate", {
                url: "/connect-detail-template/:id",
                templateUrl: "~/App/tenant/views/templates/detail.cshtml",
                menu: "Administration.Templates"
            });
        }

        //Cater

        if (abp.auth.hasPermission("Pages.Tenant.Cater.Customer")) {
            $stateProvider.state("tenant.caterCustomer", {
                url: "/cater-customer",
                templateUrl: "~/App/tenant/views/cater/customer/index.cshtml",
                menu: "Tenant.Cater.Customer"
            });
            $stateProvider.state("tenant.detailcaterCustomer", {
                url: "/cater-customer-detail/:id,:addressTab",
                templateUrl: "~/App/tenant/views/cater/customer/detail.cshtml",
                menu: "Tenant.Cater.Customer"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Cater.QuotationHeader")) {
            $stateProvider.state("tenant.quotation", {
                url: "/cater-quotation",
                templateUrl: "~/App/tenant/views/cater/quotation/index.cshtml",
                menu: "Tenant.Cater.QuotationHeader"
            });
            $stateProvider.state("tenant.detailquotation", {
                url: "/cater-quotation-detail/:id",
                templateUrl: "~/App/tenant/views/cater/quotation/detail.cshtml",
                menu: "Tenant.Cater.QuotationHeader"
            });

            $stateProvider.state("tenant.packageselection", {
                url: "/cater-package-selection/:quotationId",
                templateUrl: "~/App/tenant/views/cater/quotation/packageSelection.cshtml",
                menu: "Tenant.Cater.QuotationHeader"
            });

            $stateProvider.state("tenant.caterquotecosting", {
                url: "/caterquotecosting",
                templateUrl: "~/App/tenant/views/cater/quotecosting/caterquoteCosting.cshtml",
                menu: "Tenant.Cater.QuotationHeader"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Cater.QuotationHeader.AutoPo")) {
            $stateProvider.state("tenant.caterrequiredmaterialautopo", {
                url: "/caterrequiredmaterialautopo",
                templateUrl: "~/App/tenant/views/cater/caterAutoPo/caterAutoPo.cshtml",
                menu: "Tenant.Cater.QuotationHeader"
            });
        }

        //CONNECT

        if (abp.auth.hasPermission("Pages.Tenant.Connect.ImportSetting")) {
            $stateProvider.state("tenant.import", {
                url: "/import",
                templateUrl: "~/App/tenant/views/connect/importsetting/index.cshtml",
                menu: "Tenant.Connect.ImportSetting"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.FileManager")) {
            $stateProvider.state("tenant.filemanager", {
                url: "/file-manager",
                templateUrl: "~/App/tenant/views/connect/filemanager/index.cshtml",
                menu: "Tenant.Connect.FileManager"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Terminal")) {
            $stateProvider.state("tenant.terminal", {
                url: "/terminal",
                templateUrl: "~/App/tenant/views/connect/terminal/index.cshtml",
                menu: "Tenant.Connect.Terminal"
            });

            $stateProvider.state("tenant.detailterminal", {
                url: "/terminal-detail/:id",
                templateUrl: "~/App/tenant/views/connect/terminal/detail.cshtml",
                menu: "Tenant.Connect.Terminal"
            });
        }

        //if (abp.auth.hasPermission("Pages.Tenant.Connect.Display.ReceiptContent")) {
        //    $stateProvider.state("tenant.receiptcontent", {
        //        url: "/receipt-content",
        //        templateUrl: "~/App/tenant/views/connect/receiptcontent/index.cshtml",
        //        menu: "Tenant.Connect.ReceiptContent"
        //    });
        //}

        if (abp.auth.hasPermission("Pages.Tenant.Connect.ProgramSetting.Settings")) {
            $stateProvider.state("tenant.programSetting", {
                url: "/program-setting",
                templateUrl: "~/App/tenant/views/connect/programsettings/programsetting/index.cshtml",
                menu: "Tenant.Connect.ProgramSetting.Settings"
            });

            $stateProvider.state("tenant.detailprogramSetting", {
                url: "/program-setting-detail/:id",
                templateUrl: "~/App/tenant/views/connect/programsettings/programsetting/detail.cshtml",
                menu: "Tenant.Connect.ProgramSetting.Settings"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.ProgramSetting.Template")) {
            $stateProvider.state("tenant.programSettingTemplate", {
                url: "/program-setting-template",
                templateUrl: "~/App/tenant/views/connect/programsettings/programsettingtemplate/index.cshtml",
                menu: "Tenant.Connect.ProgramSetting.Template"
            });

            $stateProvider.state("tenant.detailprogramSettingTemplate", {
                url: "/program-setting-template-detail/:id",
                templateUrl: "~/App/tenant/views/connect/programsettings/programsettingtemplate/detail.cshtml",
                menu: "Tenant.Connect.ProgramSetting.Template"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.ProgramSetting.PTTOrSetting")) {
            $stateProvider.state("tenant.PTTOrSetting", {
                url: "/ptt-or-setting",
                templateUrl: "~/App/tenant/views/connect/programsettings/pttorsetting/index.cshtml",
                menu: "Tenant.Connect.ProgramSetting.PTTOrSetting"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Display.PrintTemplate")) {
            $stateProvider.state("tenant.printtemplate", {
                url: "/print-template",
                templateUrl: "~/App/tenant/views/connect/printtemplate/index.cshtml",
                menu: "Tenant.Connect.PrintTemplate"
            });

            $stateProvider.state("tenant.detailprinttemplate", {
                url: "/print-template-detail/:id",
                templateUrl: "~/App/tenant/views/connect/printtemplate/detail.cshtml",
                menu: "Tenant.Connect.PrintTemplate"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Display.PrintTemplate")) {
            $stateProvider.state("tenant.printer", {
                url: "/printer",
                templateUrl: "~/App/tenant/views/connect/printer/index.cshtml",
                menu: "Tenant.Connect.Printer"
            });

            $stateProvider.state("tenant.detailprinter", {
                url: "/printer-detail/:id",
                templateUrl: "~/App/tenant/views/connect/printer/detail.cshtml",
                menu: "Tenant.Connect.Printer"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Display.PrintTemplate")) {
            $stateProvider.state("tenant.printJob", {
                url: "/printJob",
                templateUrl: "~/App/tenant/views/connect/printjob/index.cshtml",
                menu: "Tenant.Connect.PrintJob"
            });

            $stateProvider.state("tenant.detailprintJob", {
                url: "/printJob-detail/:id",
                templateUrl: "~/App/tenant/views/connect/printjob/detail.cshtml",
                menu: "Tenant.Connect.PrintJob"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Display.PrintTemplateCondition")) {
            $stateProvider.state("tenant.printtemplatecondition", {
                url: "/print-template-condition",
                templateUrl: "~/App/tenant/views/connect/printtemplatecondition/index.cshtml",
                menu: "Tenant.Connect.PrintTemplateCondition"
            });

            $stateProvider.state("tenant.detailprinttemplatecondition", {
                url: "/print-template-condition-detail/:id",
                templateUrl: "~/App/tenant/views/connect/printtemplatecondition/detail.cshtml",
                menu: "Tenant.Connect.PrintTemplateCondition"
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Connect.Display.DineDevice')) {
            $stateProvider.state('tenant.dinedevice', {
                url: '/connect-prints-dinedevice',
                templateUrl: '~/App/tenant/views/connect/dinedevice/index.cshtml',
                menu: 'Tenant.Connect.DineDevice'
            });
            $stateProvider.state("tenant.detaildinedevice", {
                url: "/connect-prints-dinedevice-detail/:id",
                templateUrl: "~/App/tenant/views/connect/dinedevice/detail.cshtml",
                menu: "Tenant.Connect.DineDevice"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.FullTax.Member")) {
            $stateProvider.state("tenant.fulltaxmember", {
                url: "/fulltax-member",
                templateUrl: "~/App/tenant/views/connect/fulltax/taxmember/index.cshtml",
                menu: "Tenant.Connect.FullTax"
            });

            $stateProvider.state("tenant.detailfulltaxmember", {
                url: "/fulltax-member-detail/:id",
                templateUrl: "~/App/tenant/views/connect/fulltax/taxmember/detail.cshtml",
                menu: "Tenant.Connect.FullTax"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.FullTax.Report")) {
            $stateProvider.state("tenant.fulltaxreport", {
                url: "/fulltax-report",
                templateUrl: "~/App/tenant/views/connect/fulltax/taxmember/index.cshtml",
                menu: "Tenant.Connect.FullTax"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.FutureData")) {
            $stateProvider.state("tenant.futuredata", {
                url: "/connect-future-data",
                templateUrl: "~/App/tenant/views/connect/futuredata/index.cshtml",
                menu: "Tenant.Connect.FutureData"
            });

            $stateProvider.state("tenant.detailfuturedata", {
                url: "/connect-future-data-detail/:id",
                templateUrl: '~/App/tenant/views/connect/futuredata/futuredataDetail.cshtml',
                menu: 'Tenant.Connect.FutureData'
            });

            $stateProvider.state("tenant.menuitemfuturedatadetail", {
                url: "/connect-future-data-menuitem-detail/:id",
                templateUrl: '~/App/tenant/views/connect/futuredata/futuredataForMenuItem.cshtml',
                menu: 'Tenant.Connect.FutureData'
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Master.Location")) {
            $stateProvider.state("tenant.location", {
                url: "/connect-master-location",
                templateUrl: "~/App/tenant/views/connect/location/index.cshtml",
                menu: "Tenant.Connect.Master.Location"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Master.Location")) {
            $stateProvider.state("tenant.createlocation", {
                url: "/connect-detail-location/:id",
                templateUrl: "~/App/tenant/views/connect/location/detail.cshtml",
                menu: "Tenant.Connect.Master.Location"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.Master.LocationTag")) {
            $stateProvider.state("tenant.locationtag", {
                url: "/connect-master-locationtag",
                templateUrl: "~/App/tenant/views/connect/locationTag/index.cshtml",
                menu: "Tenant.Connect.Master.LocationTag"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Master.LocationGroup")) {
            $stateProvider.state("tenant.locationgroup", {
                url: "/connect-master-locationgroup",
                templateUrl: "~/App/tenant/views/connect/locationgroup/index.cshtml",
                menu: "Tenant.Connect.Master.LocationGroup"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.Master.PaymentType")) {
            $stateProvider.state("tenant.paymenttype", {
                url: "/connect-master-paymenttype",
                templateUrl: "~/App/tenant/views/connect/paymenttype/index.cshtml",
                menu: "Tenant.Connect.Master.PaymentType"
            });

            $stateProvider.state("tenant.createpayment", {
                url: "/connect-detail-payment/:id",
                templateUrl: "~/App/tenant/views/connect/paymenttype/create.cshtml",
                menu: "Tenant.Connect.Master.PaymentType"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Master.TicketType")) {
            $stateProvider.state("tenant.tickettype", {
                url: "/connect-master-tickettype",
                templateUrl: "~/App/tenant/views/connect/tickettype/index.cshtml",
                menu: "Tenant.Connect.Master.TicketType"
            });

            $stateProvider.state("tenant.createtickettype", {
                url: "/connect-detail-ticket-type/:id",
                templateUrl: "~/App/tenant/views/connect/tickettype/create.cshtml",
                menu: "Tenant.Connect.Master.TicketType"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Master.Numerator")) {
            $stateProvider.state("tenant.numerator", {
                url: "/connect-master-numerator",
                templateUrl: "~/App/tenant/views/connect/numerator/index.cshtml",
                menu: "Tenant.Connect.Master.Numerator"
            });

            $stateProvider.state("tenant.createnumerator", {
                url: "/connect-detail-numerator/:id",
                templateUrl: "~/App/tenant/views/connect/numerator/create.cshtml",
                menu: "Tenant.Connect.Master.Numerator"
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Connect.Master.PlanReason')) {
            $stateProvider.state('tenant.planreason', {
                url: '/connect-master-planreason',
                templateUrl: '~/App/tenant/views/connect/planreason/index.cshtml',
                menu: 'Tenant.Connect.Master.PlanReason'
            });

            $stateProvider.state("tenant.createplanreason", {
                url: "/connect-detail-planreason/:id",
                templateUrl: "~/App/tenant/views/connect/planreason/createreason.cshtml",
                menu: "Tenant.Connect.Master.PlanReason"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Master.ForeignCurrency")) {
            $stateProvider.state("tenant.foreigncurrency", {
                url: "/connect-master-foreigncurrency",
                templateUrl: "~/App/tenant/views/connect/foreigncurrency/index.cshtml",
                menu: "Tenant.Connect.Master.ForeignCurrency"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Master.TransactionType")) {
            $stateProvider.state("tenant.transactiontype", {
                url: "/connect-master-transactiontype",
                templateUrl: "~/App/tenant/views/connect/transactiontype/index.cshtml",
                menu: "Tenant.Connect.Master.TransactionType"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Master.EntityType")) {
            $stateProvider.state("tenant.entitytype", {
                url: "/connect-master-entitytype",
                templateUrl: "~/App/tenant/views/connect/entitytype/index.cshtml",
                menu: "Tenant.Connect.Master.EntityType"
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Connect.Menu.DeliveryAggregator')) {
            $stateProvider.state('tenant.deliveryAggregators', {
                url: '/connect-menu-deliveryAggregators',
                templateUrl: '~/App/tenant/views/connect/deliveryAggregators/index.cshtml',
                menu: 'Tenant.Connect.Menu.DeliveryAggregators'
            });
            $stateProvider.state("tenant.deliveryAggregatorDetail", {
                url: "/connect-menu-deliveryAggregator-detail/:id",
                templateUrl: "~/App/tenant/views/connect/deliveryAggregators/create.cshtml",
                menu: "Tenant.Connect.Menu.DeliveryAggregators"
            });
            $stateProvider.state("tenant.deliveryAggPlatform", {
                url: "/connect-menu-deliveryAggregator-platform/:id",
                templateUrl: "~/App/tenant/views/connect/deliveryAggregators/platform.cshtml",
                menu: "Tenant.Connect.Menu.DeliveryAggregators"
            });
            $stateProvider.state("tenant.deliveryAggMenuManagement", {
                url: "/connect-menu-deliveryAggregator-menu-manage/:id",
                templateUrl: "~/App/tenant/views/connect/deliveryAggregators/menuManagement.cshtml",
                menu: "Tenant.Connect.Menu.DeliveryAggregators"
            });

            $stateProvider.state("tenant.deliveryHeroManagement", {
                url: "/connect-menu-deliveryAggregator-deliveryHero-manage/:id",
                templateUrl: "~/App/tenant/views/connect/deliveryAggregators/deliveryHeroManagement.cshtml",
                menu: "Tenant.Connect.Menu.DeliveryAggregators"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.Category")) {
            $stateProvider.state("tenant.category", {
                url: "/connect-menu-category",
                templateUrl: "~/App/tenant/views/connect/category/index.cshtml",
                menu: "Tenant.Connect.Menu.Category"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.Category")) {
            $stateProvider.state("tenant.detailcategory", {
                url: "/connect-menu-detailcategory/:id",
                templateUrl: "~/App/tenant/views/connect/category/createOrEdit.cshtml",
                menu: "Tenant.Connect.Menu.Category"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.MenuItem")) {
            $stateProvider.state("tenant.menuitem", {
                url: "/connect-menu-menuitem/:filter?page",
                templateUrl: "~/App/tenant/views/connect/menuitem/index.cshtml",
                menu: "Tenant.Connect.Menu.MenuItem"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ProductGroup")) {
            $stateProvider.state("tenant.productgroup", {
                url: "/connect-menu-productgroup",
                templateUrl: "~/App/tenant/views/connect/productgroup/productgroupindex.cshtml",
                menu: "Tenant.Connect.Menu.ProductGroup"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ProductGroup")) {
            $stateProvider.state("tenant.detailproductgroup", {
                url: "/connect-menu-detailproductgroup/:id",
                templateUrl: "~/App/tenant/views/connect/productgroup/productgroupcreateOrEditModal.cshtml",
                menu: "Tenant.Connect.Menu.ProductGroup"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.MenuItem")) {
            $stateProvider.state("tenant.detailmenuitem", {
                url: "/connect-menu-detailmenuitem/:id?filter?page",
                templateUrl: "~/App/tenant/views/connect/menuitem/detail.cshtml",
                menu: "Tenant.Connect.Menu.MenuItem"
            });

            $stateProvider.state("tenant.upsellinglocationprice", {
                url: "/connect-menu-upsellinglocationprice/:id,:upmenuItemId",
                templateUrl: "~/App/tenant/views/connect/menuitem/locationPriceModal.cshtml",
                menu: "Tenant.Connect.Menu.MenuItem"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ComboGroup")) {
            $stateProvider.state("tenant.combogroup", {
                url: "/connect-combo-group",
                templateUrl: "~/App/tenant/views/connect/combogroup/index.cshtml",
                menu: "Tenant.Connect.Menu.ComboGroup"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ComboGroup")) {
            $stateProvider.state("tenant.detailcombogroup", {
                url: "/connect-combo-group-detail/:id",
                templateUrl: "~/App/tenant/views/connect/combogroup/detail.cshtml",
                menu: "Tenant.Connect.Menu.ComboGroup"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.ScreenMenu")) {
            $stateProvider.state("tenant.screenmenu", {
                url: "/connect-menu-screenmenu",
                templateUrl: "~/App/tenant/views/connect/screenmenu/index.cshtml",
                menu: "Tenant.Connect.Menu.ScreenMenu"
            });
            $stateProvider.state("tenant.detailscreenmenu", {
                url: "/connect-menu-detailscreenmenu/:id,:futureDataId",
                templateUrl: "~/App/tenant/views/connect/screenmenu/detail.cshtml",
                menu: "Tenant.Connect.Menu.ScreenMenu"
            });

            $stateProvider.state("tenant.screencategory", {
                url: "/connect-menu-screenmenu/:menuid?categoryid?itemid?location,:futureDataId",
                templateUrl: "~/App/tenant/views/connect/screenmenu/category/category.cshtml",
                menu: "Tenant.Connect.Menu.ScreenMenu"
            });

            $stateProvider.state("tenant.detailscreencategory", {
                url: "/connect-menu-screencategory/:menuid?categoryid?itemid?location",
                templateUrl: "~/App/tenant/views/connect/screenmenu/category/detailcategory.cshtml",
                menu: "Tenant.Connect.Menu.ScreenMenu"
            });
            $stateProvider.state("tenant.screenmenuitem", {
                url: "/connect-menu-screenmenuitem/:menuid?categoryid?itemid?location,:futureDataId",
                templateUrl: "~/App/tenant/views/connect/screenmenu/menuitem/menuitem.cshtml",
                menu: "Tenant.Connect.Menu.ScreenMenu"
            });
            $stateProvider.state("tenant.createscreenmenuitem", {
                url: "/connect-menu-createscreenmenuitem/:menuid?categoryid?itemid?location",
                templateUrl: "~/App/tenant/views/connect/screenmenu/menuitem/create.cshtml",
                menu: "Tenant.Connect.Menu.ScreenMenu"
            });
            $stateProvider.state("tenant.detailscreenmenuitem", {
                url: "/connect-menu-detailscreenmenuitem/:menuid?categoryid?itemid?location",
                templateUrl: "~/App/tenant/views/connect/screenmenu/menuitem/detail.cshtml",
                menu: "Tenant.Connect.Menu.ScreenMenu"
            });

            $stateProvider.state("tenant.vitualScreenMenu", {
                url: "/connect-menu-virtualscreen/:screenMenuId?categoryid?itemid?location",
                templateUrl: "~/App/tenant/views/connect/screenmenu/virtualScreen.cshtml",
                menu: "Tenant.Connect.Menu.ScreenMenu"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Conect.Table.Group")) {
            $stateProvider.state("tenant.tablegroup", {
                url: "/connect-table-group",
                templateUrl: "~/App/tenant/views/connect/table/group.cshtml",
                menu: "Tenant.Connect.Table.Group"
            });

            $stateProvider.state("tenant.linktable", {
                url: "/connect-link-table/:id",
                templateUrl: "~/App/tenant/views/connect/table/link/linktable.cshtml",
                menu: "Tenant.Connect.Table.Group"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Conect.Tables")) {
            $stateProvider.state("tenant.tables", {
                url: "/connect-tables",
                templateUrl: "~/App/tenant/views/connect/table/tables.cshtml",
                menu: "Tenant.Connect.Table.ConnectTable"
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Connect.Menu.PromotionCategory')) {
            $stateProvider.state('tenant.promotioncategory', {
                url: '/connect-menu-promotioncategory',
                templateUrl: '~/App/tenant/views/connect/promotioncategory/index.cshtml',
                menu: 'Tenant.Connect.Menu.PromotionCategory'
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.PromotionCategory")) {
            $stateProvider.state("tenant.detailpromotioncategory", {
                url: "/connect-menu-detailpromotioncategory/:id",
                templateUrl: "~/App/tenant/views/connect/promotioncategory/detail.cshtml",
                menu: "Tenant.Connect.Menu.PromotionCategory"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.Promotion")) {
            $stateProvider.state("tenant.promotion", {
                url: "/connect-menu-promotion",
                templateUrl: "~/App/tenant/views/connect/promotion/index.cshtml",
                menu: "Tenant.Connect.Menu.Promotion"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.Promotion")) {
            $stateProvider.state("tenant.detailpromotion", {
                url: "/connect-menu-detailpromotion/:id",
                templateUrl: "~/App/tenant/views/connect/promotion/detail.cshtml",
                menu: "Tenant.Connect.Menu.Promotion"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.Promotion")) {
            $stateProvider.state("tenant.plantquota", {
                url: "/connect-plantquota/:id",
                templateUrl: "~/App/tenant/views/connect/promotion/plantquota.cshtml",
                menu: "Tenant.Connect.Menu.Promotion"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.Promotion")) {
            $stateProvider.state("tenant.quotaticket", {
                url: "/connect-quotaticket/:locationId?plant?plantNo?promotionId?quotaId",
                templateUrl: "~/App/tenant/views/connect/promotion/quotadetail.cshtml",
                menu: "Tenant.Connect.Menu.Promotion"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.Promotion")) {
            $stateProvider.state("tenant.quotadetail", {
                url: "/connect-menu-quotadetail/:id",
                templateUrl: "~/App/tenant/views/connect/promotion/quotadetail.cshtml",
                menu: "Tenant.Connect.Menu.Promotion"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Engage.Member.Account")) {
            $stateProvider.state("tenant.memberaccount", {
                url: "/engage-accounts/:mid",
                templateUrl: "~/App/tenant/views/engage/member/accounts.cshtml",
                menu: "Tenant.Engage.Account"
            });

            $stateProvider.state("tenant.memberaccounttrans", {
                url: "/engage-accounttrans/:id",
                templateUrl: "~/App/tenant/views/engage/member/accounttrans.cshtml",
                menu: "Tenant.Engage.Account"
            });
        }

        //if (abp.auth.hasPermission("Pages.Tenant.Engage.LoyaltyProgram")) {
        //    $stateProvider.state("tenant.loyaltyProgram", {
        //        url: "/engage-loyaltyProgram",
        //        templateUrl: "~/App/tenant/views/engage/LoyaltyProgram/index.cshtml",
        //        menu: "Tenant.Engage.LoyaltyProgram"
        //    });
        //}

        if (abp.auth.hasPermission("Pages.Tenant.Engage.LoyaltyProgram")) {
            $stateProvider.state("tenant.loyaltyProgram", {
                url: "/engage-loyaltyProgram",
                templateUrl: "~/App/tenant/views/engage/LoyaltyProgram/pointcampaign/pointcampaigns.cshtml",
                menu: "Tenant.Engage.LoyaltyProgram"
            });
        }

        //if (abp.auth.hasPermission("Pages.Tenant.Engage.LoyaltyProgram")) {
        //    $stateProvider.state("tenant.createoreditloyaltyprogram", {
        //        url: "/engage-createeditloyaltyprogram/:id",
        //        templateUrl: "~/App/tenant/views/engage/loyaltyProgram/createedit.cshtml",
        //        menu: "Tenant.Engage.LoyaltyProgram"
        //    });
        //}

        if (abp.auth.hasPermission("Pages.Tenant.Engage.LoyaltyProgram")) {
            $stateProvider.state("tenant.pointcampaign", {
                url: "/engage-createeditloyaltyprogram/:id",
                templateUrl: "~/App/tenant/views/engage/LoyaltyProgram/pointcampaign/pointcampaign.cshtml",
                menu: "Tenant.Engage.LoyaltyProgram"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Engage.Stage")) {
            $stateProvider.state("tenant.stage", {
                url: "/engage-loyaltyProgram-stage",
                templateUrl: "~/App/tenant/views/engage/loyaltyProgram/stage/index.cshtml",
                menu: "Tenant.Engage.LoyaltyProgram.Stage"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Engage.Stage")) {
            $stateProvider.state("tenant.createoreditstage", {
                url: "/engage-createeditstage/:id",
                templateUrl: "~/App/tenant/views/engage/loyaltyProgram/stage/createedit.cshtml",
                menu: "Tenant.Engage.LoyaltyProgram.Stage"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Engage.MembershipTier")) {
            $stateProvider.state("tenant.membershiptiers", {
                url: "/engage-membership-tiers",
                templateUrl: "~/App/tenant/views/engage/member/tiers.cshtml",
                menu: "Tenant.Engage.MembershipTier"
            });
            if(abp.auth.hasPermission("Pages.Tenant.Engage.MembershipTier.Create") &&
                abp.auth.hasPermission("Pages.Tenant.Engage.MembershipTier.Edit")) {
                $stateProvider.state("tenant.membershiptier", {
                    url: "/engage-membership-tier/:id",
                    templateUrl: "~/App/tenant/views/engage/member/tier.cshtml"
                });
            }
        }

        if (abp.auth.hasPermission("Pages.Tenant.Engage.ServiceInfo.Template")) {
            $stateProvider.state("tenant.serviceinfotemplates", {
                url: "/engage-serviceinfo-templates",
                templateUrl: "~/App/tenant/views/engage/serviceinfo/templates/templates.cshtml",
                menu: "Tenent.Engage.ServiceInfoTemplate"
            });
            $stateProvider.state("tenant.serviceinfotemplatebuilder", {
                url: "/engage-serviceinfo-templatebuilder/:id",
                templateUrl: "~/App/tenant/views/engage/serviceinfo/templates/templatebuilder.cshtml"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.OrderTagGroup")) {
            $stateProvider.state("tenant.detailOrderTag", {
                url: "/connect-tag-detailorderdertag/:id",
                templateUrl: "~/App/tenant/views/connect/ordertaggroup/tagdetail.cshtml",
                menu: "Tenant.Connect.Menu.OrderTagGroup"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Master.EntityType")) {
            $stateProvider.state("tenant.detailentitytype", {
                url: "/connect-master-detailentitytype/:id",
                templateUrl: "~/App/tenant/views/connect/entitytype/detail.cshtml",
                menu: "Tenant.Connect.Master.EntityType"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.Master.Department")) {
            $stateProvider.state("tenant.department", {
                url: "/connect-master-department",
                templateUrl: "~/App/tenant/views/connect/department/index.cshtml",
                menu: "Tenant.Connect.Master.Department"
            });

            $stateProvider.state("tenant.createdepartment", {
                url: "/connect-menu-createdepartment/:id",
                templateUrl: "~/App/tenant/views/connect/department/create.cshtml",
                menu: "Tenant.Connect.Master.Department"
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Connect.Master.DepartmentGroup')) {
            $stateProvider.state('tenant.departmentgroup', {
                url: '/connect-master-departmentgroup',
                templateUrl: '~/App/tenant/views/connect/departmentgroup/index.cshtml',
                menu: 'Tenant.Connect.Master.DepartmentGroup'
            });
            $stateProvider.state("tenant.createdepartmentgroup", {
                url: "/connect-master-createdepartmentgroup/:id",
                templateUrl: "~/App/tenant/views/connect/departmentgroup/create.cshtml",
                menu: "Tenant.Connect.Master.DepartmentGroup"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.Master.DinePlanTax")) {
            $stateProvider.state("tenant.dineplantax", {
                url: "/connect-master-dineplantax",
                templateUrl: "~/App/tenant/views/connect/dineplantax/index.cshtml",
                menu: "Tenant.Connect.Master.DinePlanTax"
            });

            $stateProvider.state("tenant.dineplantaxdetail", {
                url: "/connect-master-dineplantaxdetail/:id,:taxName,:futureDataId",
                templateUrl: "~/App/tenant/views/connect/dineplantax/dineplantaxDetail.cshtml",
                menu: "Tenant.Connect.Master.DinePlanTax",
                params: {
                    reportKey: "STOCK"
                },
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Master.Calculation")) {
            $stateProvider.state("tenant.calculation", {
                url: "/connect-master-calculation",
                templateUrl: "~/App/tenant/views/connect/calculation/index.cshtml",
                menu: "Tenant.Connect.Master.Calculation"
            });

            $stateProvider.state("tenant.detailcalculation", {
                url: "/connect-master-detailcalculation/:id",
                templateUrl: "~/App/tenant/views/connect/calculation/calculation.cshtml",
                menu: "Tenant.Connect.Master.Calculation"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.MenuItemPrice")) {
            $stateProvider.state("tenant.locationmenuprice", {
                url: "/connect-menu-locationmenuprice/:location?filterText",
                templateUrl: "~/App/tenant/views/connect/locationmenu/price.cshtml",
                menu: "Tenant.Connect.Menu.LocationMenuPrice"
            });
        }

        //Touch

        if (abp.auth.hasPermission("Pages.Tenant.Touch.Menu")) {
            $stateProvider.state("tenant.locationmenuitem", {
                url: "/connect-menu-locationmenuitem/:location?filterText",
                templateUrl: "~/App/tenant/views/touch/locationmenu/item.cshtml",
                menu: "Tenant.Touch.LocationMenuItem"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.PriceTag")) {
            $stateProvider.state("tenant.pricetag", {
                url: "/connect-menu-pricetag/:isDisableCreate",
                templateUrl: "~/App/tenant/views/connect/pricetag/index.cshtml",
                menu: "Tenant.Connect.Menu.PriceTag"
            });

            $stateProvider.state("tenant.createpricetag", {
                url: "/connect-menu-createtag/:id",
                templateUrl: "~/App/tenant/views/connect/pricetag/create.cshtml",
                menu: "Tenant.Connect.Menu.PriceTag"
            });

            $stateProvider.state("tenant.detailpricetag", {
                url: "/connect-menu-detailpricetag/:id,:futureDataId",
                templateUrl: "~/App/tenant/views/connect/pricetag/detail.cshtml",
                menu: "Tenant.Connect.Menu.PriceTag"
            });

            $stateProvider.state("tenant.pricetaglocationprice", {
                url: "/connect-menu-pricetaglocationprice/:id,:pricetagDefinitionId",
                templateUrl: "~/App/tenant/views/connect/pricetag/locationPriceModal.cshtml",
                menu: "Tenant.Connect.Menu.PriceTag"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.User.DinePlanUser")) {
            $stateProvider.state("tenant.dineplanuser", {
                url: "/connect-dineplanuser",
                templateUrl: "~/App/tenant/views/connect/user/dineplanuser/index.cshtml",
                menu: "Tenant.Connect.User.DinePlanUser"
            });

            $stateProvider.state("tenant.createdineplanuser", {
                url: "/connect-createdineplanuser/:id",
                templateUrl: "~/App/tenant/views/connect/user/dineplanuser/create.cshtml",
                menu: "Tenant.Connect.User.DinePlanUser"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.User.DinePlanUserRole")) {
            $stateProvider.state("tenant.dineplanuserrole", {
                url: "/connect-user-dineplanuserrole",
                templateUrl: "~/App/tenant/views/connect/user/dineplanuserrole/index.cshtml",
                menu: "Tenant.Connect.User.DinePlanUserRole"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Report")) {
            $stateProvider.state("tenant.ticket", {
                url: "/connect-report-ticket",
                templateUrl: "~/App/tenant/views/connect/report/tickets/ticket/ticket.cshtml",
                menu: "Tenant.Connect.Report.Tickets.Ticket"
            });

            $stateProvider.state("tenant.ticketsync", {
                url: "/connect-report-ticketsync",
                templateUrl: "~/App/tenant/views/connect/report/tickets/ticketsync/ticketsync.cshtml",
                menu: "Tenant.Connect.Report.Tickets.TicketSync"
            });

            $stateProvider.state("tenant.abbreprint", {
                url: "/connect-report-abbreprint",
                templateUrl: "~/App/tenant/views/connect/report/tickets/abbreprint/abbreprint.cshtml",
                menu: "Tenant.Connect.Report.Tickets.ABBReprint"
            });

            $stateProvider.state("tenant.cashaudit", {
                url: "/connect-report-cashaudit",
                templateUrl: "~/App/tenant/views/connect/report/cashaudit/cashaudit.cshtml",
                menu: "Tenant.Connect.Report.CashAudit"
            });

            $stateProvider.state("tenant.creditcard", {
                url: "/connect-report-creditcard",
                templateUrl: "~/App/tenant/views/connect/report/creditcard/creditcard.cshtml",
                menu: "Tenant.Connect.Report.CreditCard"
            });

            $stateProvider.state("tenant.promotionreport", {
                url: "/connect-report-promotion",
                templateUrl: "~/App/tenant/views/connect/report/promotion/order/promotion.cshtml",
                menu: "Tenant.Connect.Report.Tickets.Promotion"
            });

            $stateProvider.state("tenant.promotionticketreport", {
                url: "/connect-report-discount",
                templateUrl: "~/App/tenant/views/connect/report/promotion/ticket/discount.cshtml",
                menu: "Tenant.Connect.Report.Discount"
            });

            $stateProvider.state("tenant.salestaxreport", {
                url: "/connect-report-salestaxreport",
                templateUrl: "~/App/tenant/views/connect/report/customize/salestaxreport/salestaxreport.cshtml",
                menu: "Tenant.Connect.Report.Customize"
            });
            $stateProvider.state("tenant.nonsalestaxreport", {
                url: "/connect-report-nonsalestaxreport",
                templateUrl: "~/App/tenant/views/connect/report/customize/nonsalestaxreport/nonsalestaxreport.cshtml",
                menu: "Tenant.Connect.Report.Customize"
            });

            $stateProvider.state("tenant.dailysalestaxreport", {
                url: "/connect-report-dailysalestaxreport",
                templateUrl: "~/App/tenant/views/connect/report/customize/dailysalestaxreport/dailysalestaxreport.cshtml",
                menu: "Tenant.Connect.Report.Customize"
            });
            $stateProvider.state("tenant.monthlysalestaxreport", {
                url: "/connect-report-monthlysalestaxreport",
                templateUrl: "~/App/tenant/views/connect/report/customize/monthlysalestaxreport/monthlysalestaxreport.cshtml",
                menu: "Tenant.Connect.Report.Customize"
            });          
            $stateProvider.state("tenant.dailynonsalestaxReport", {
                url: "/connect-report-dailynonsalestaxReport",
                templateUrl: "~/App/tenant/views/connect/report/customize/dailynonsalestaxreport/dailynonsalestaxreport.cshtml",
                menu: "Tenant.Connect.Report.Customize"
            });
            $stateProvider.state("tenant.monthlynonsalestaxreport", {
                url: "/connect-report-monthlynonsalestaxreport",
                templateUrl: "~/App/tenant/views/connect/report/customize/monthlynonsalestaxreport/monthlynonsalestaxreport.cshtml",
                menu: "Tenant.Connect.Report.Customize"
            });

            $stateProvider.state("tenant.speedofservicereport", {
                url: "/connect-report-speedofservicereport",
                templateUrl: "~/App/tenant/views/connect/report/speedofservicereport/speedofservicereport.cshtml",
                menu: "Tenant.Connect.Report.SpeedOfServiceReport"
            });

            $stateProvider.state("tenant.monthlysales", {
                url: "/connect-report-monthlysales",
                templateUrl: "~/App/tenant/views/connect/report/cedele/monthlystoresales/monthlystoresales.cshtml",
                menu: "Tenant.Connect.Report.MonthlySales"
            });

            $stateProvider.state("tenant.mealtimesale", {
                url: "/connect-report-mealtimesales",
                templateUrl: "~/App/tenant/views/connect/report/cedele/mealtimesales/mealtimesales.cshtml",
                menu: "Tenant.Connect.Report.MealTimeSales"
            });
            $stateProvider.state("tenant.monthlysaleadvancereport", {
                url: "/connect-report-monthlysaleadvancereport",
                templateUrl: "~/App/tenant/views/connect/report/customize/monthlysaleadvancereport/monthlysaleadvancereport.cshtml",
                menu: "Tenant.Connect.Report.MonthlySalesAdvance"
            });
            $stateProvider.state("tenant.dailysalesbreak", {
                url: "/connect-report-dailysalesbreak",
                templateUrl: "~/App/tenant/views/connect/report/customize/dailysalesbreak/dailysalesbreak.cshtml",
                menu: "Tenant.Connect.Report.DailySalesBreak"
            });
            $stateProvider.state("tenant.salesbyperiod", {
                url: "/connect-report-salesbyperiod",
                templateUrl: "~/App/tenant/views/connect/report/salesbyperiod/salesbyperiod.cshtml",
                menu: "Tenant.Connect.Report.SalesByPeriod"
            });
            $stateProvider.state("tenant.salespromotionbypromotion", {
                url: "/connect-sales-promotion-by-promotion",
                templateUrl: "~/App/tenant/views/connect/report/salespromotion/salesbypromotion/index.cshtml",
                menu: "Tenant.Connect.Report.SalesPromotionReport.SalesPromotionByPromotion"
            });

            $stateProvider.state("tenant.salespromotionbyplant", {
                url: "/connect-sales-promotion-by-plant",
                templateUrl: "~/App/tenant/views/connect/report/salespromotion/salesbyplant/index.cshtml",
                menu: "Tenant.Connect.Report.SalesPromotionReport.SalesPromotionByPlant"
            });
            $stateProvider.state("tenant.monthlystoresalesstas", {
                url: "/connect-report-monthlystoresalesstats",
                templateUrl: "~/App/tenant/views/connect/report/cedele/monthlystoresalesstats/monthlystoresalesstats.cshtml",
                menu: "Tenant.Connect.Report.MonthlyStoreSalesStats"
            });

            $stateProvider.state("tenant.cashsummaryreport", {
                url: "/connect-report-cashsummaryreport",
                templateUrl: "~/App/tenant/views/connect/report/cashsummaryreport/cashsummaryreport.cshtml",
                menu: "Tenant.Connect.Report.CashSummaryReport"
            });

            $stateProvider.state("tenant.fulltaxinvoicereport", {
                url: "/connect-report-full-tax-invoice",
                templateUrl: "~/App/tenant/views/connect/report/fulltaxinvoicereport/fulltaxinvoicereport.cshtml",
                menu: "Tenant.Connect.Report.Customize.FullTaxInvoice"
            });


            $stateProvider.state("tenant.topdownsellreportbyquantitysummary", {
                url: "/connect-top-down-sell-report-by-quantity-summary",
                templateUrl: "~/App/tenant/views/connect/report/topdownsell/topdownsellreportbyquantitysummary/index.cshtml",
                menu: "Tenant.Connect.Report.Customize.TopDownSellReport.QuantitySummary"
            });
          
            $stateProvider.state("tenant.topdownsellreportbyamountsummary", {
                url: "/connect-top-down-sell-report-by-amount-summary",
                templateUrl: "~/App/tenant/views/connect/report/topdownsell/topdownsellreportbyamountsummary/index.cshtml",
                menu: "Tenant.Connect.Report.Customize.TopDownSellReport.AmountSummary"
            });
            $stateProvider.state("tenant.topdownsellreportbyquantityplant", {
                url: "/connect-top-down-sell-report-by-quantity-plant",
                templateUrl: "~/App/tenant/views/connect/report/topdownsell/topdownsellreportbyquantityplant/index.cshtml",
                menu: "Tenant.Connect.Report.Customize.TopDownSellReport.QuantityPlant"
            });

            $stateProvider.state("tenant.topdownsellreportbyamountplant", {
                url: "/connect-top-down-sell-report-by-amount-plant",
                templateUrl: "~/App/tenant/views/connect/report/topdownsell/topdownsellreportbyamountplant/index.cshtml",
                menu: "Tenant.Connect.Report.Customize.TopDownSellReport.AmountPlant"
            });
            $stateProvider.state("tenant.order", {
                url: "/connect-report-order",
                templateUrl: "~/App/tenant/views/connect/report/orders/order/order.cshtml",
                menu: "Tenant.Connect.Report.Orders.Order"
            });

            $stateProvider.state("tenant.ordertag", {
                url: "/connect-report-ordertag",
                templateUrl: "~/App/tenant/views/connect/report/ordertags/ordertag.cshtml",
                menu: "Tenant.Connect.Report.Orders.OrderTag"
            });
            $stateProvider.state("tenant.exchange", {
                url: "/connect-report-exchange",
                templateUrl: "~/App/tenant/views/connect/report/exchange/exchange.cshtml",
                menu: "Tenant.Connect.Report.Orders.Exchange"
            });
            $stateProvider.state("tenant.returnproduct", {
                url: "/connect-report-return-product",
                templateUrl: "~/App/tenant/views/connect/report/returnproduct/returnproduct.cshtml",
                menu: "Tenant.Connect.Report.ReturnProduct.ReturnProduct"
            });
            $stateProvider.state("tenant.categoryReport", {
                url: "/connect-report-category-report",
                templateUrl: "~/App/tenant/views/connect/report/items/category/category.cshtml",
                menu: "Tenant.Connect.Report.Items.Category"
            });

            $stateProvider.state("tenant.groupReport", {
                url: "/connect-report-group-report",
                templateUrl: "~/App/tenant/views/connect/report/items/group/group.cshtml",
                menu: "Tenant.Connect.Report.Items.Group"
            });

            $stateProvider.state("tenant.itemTagReport", {
                url: "/connect-report-itemTag-report",
                templateUrl: "~/App/tenant/views/connect/report/items/itemtag/itemtag.cshtml",
                menu: "Tenant.Connect.Report.Items.ItemTagReport"
            });

            $stateProvider.state("tenant.menuengineeringreport", {
                url: "/connect-report-menuengineering-report",
                templateUrl: "~/App/tenant/views/connect/report/menuengineering/menuengineering.cshtml",
                menu: "Tenant.Connect.Report.Items.MenuEngineering"
            });

            $stateProvider.state("tenant.departmentReport", {
                url: "/connect-report-department-report",
                templateUrl: "~/App/tenant/views/connect/report/items/department/department.cshtml",
                menu: "Tenant.Connect.Report.Items.Department"
            });

            $stateProvider.state("tenant.itemreport", {
                url: "/connect-report-item-report",
                templateUrl: "~/App/tenant/views/connect/report/items/item/item.cshtml",
                menu: "Tenant.Connect.Report.Items.Item"
            });

            $stateProvider.state("tenant.hourlySalesReport", {
                url: "/connect-report-item-hourly-report",
                templateUrl: "~/App/tenant/views/connect/report/items/hourlySales/hourlySales.cshtml",
                menu: "Tenant.Connect.Report.Items.HourlySales"
            });
            $stateProvider.state("tenant.topbottomitemreport", {
                url: "/connect-top-bottom-item-report",
                templateUrl: "~/App/tenant/views/connect/report/items/topbottomitem/topbottomitem.cshtml",
                menu: "Tenant.Connect.Report.Items.TopBottomItem"
            });

            $stateProvider.state("tenant.datewisesale", {
                url: "/connect-report-summary-sale-report",
                templateUrl: "~/App/tenant/views/connect/report/summary/sale/sale.cshtml",
                menu: "Tenant.Connect.Report.Summary.DatewiseSale"
            });
            $stateProvider.state("tenant.salesummarynew", {
                url: "/connect-report-summary-sale-new-report",
                templateUrl: "~/App/tenant/views/connect/report/summary/salesummarynew/sale.cshtml",
                menu: "Tenant.Connect.Report.Summary.SaleNewSummary"
            });
            $stateProvider.state("tenant.salessummaryprint", {
                url: "/connect-report-summary-sale-report-print",
                templateUrl: "~/App/tenant/views/connect/report/summary/sale/salesSummaryPrint.cshtml",
                menu: "Tenant.Connect.Report.Summary.DatewiseSale",
                params: {
                    ticketInput: null
                }
            });

            $stateProvider.state("tenant.log", {
                url: "/connect-report-log",
                templateUrl: "~/App/tenant/views/connect/report/summary/log/log.cshtml",
                menu: "Tenant.Connect.Report.Summary.Log"
            });
            $stateProvider.state("tenant.workdaysale", {
                url: "/connect-report-workday",
                templateUrl: "~/App/tenant/views/connect/report/summary/workday/day.cshtml",
                menu: "Tenant.Connect.Report.Summary.DatewiseSale"
            });
            $stateProvider.state("tenant.planworkday", {
                url: "/connect-report-plan-workday",
                templateUrl: "~/App/tenant/views/connect/report/summary/planworkday/workday.cshtml",
                menu: "Tenant.Connect.Report.PlanWorkDay.Summary"
            });
            $stateProvider.state("tenant.weekdaysale", {
                url: "/connect-report-weekday",
                templateUrl: "~/App/tenant/views/connect/report/summary/weekday/weekday.cshtml",
                menu: "Tenant.Connect.Report.WeekDay.Summary"
            });
            $stateProvider.state("tenant.weekendsale", {
                url: "/connect-report-weekend",
                templateUrl: "~/App/tenant/views/connect/report/summary/weekend/weekend.cshtml",
                menu: "Tenant.Connect.Report.WeekEnd.Summary"
            });
            $stateProvider.state("tenant.hoursummary", {
                url: "/connect-report-hoursummary",
                templateUrl: "~/App/tenant/views/connect/report/hour/summary/summary.cshtml",
                menu: "Tenant.Connect.Report.Summary.DatewiseSale"
            });
            $stateProvider.state("tenant.locationhoursummary", {
                url: "/connect-report-locationhoursummary",
                templateUrl: "~/App/tenant/views/connect/report/hour/locationsummary/locationsummary.cshtml",
                menu: "Tenant.Connect.Report.Summary.DatewiseSale"
            });

            $stateProvider.state("tenant.daysummaryprint", {
                url: "/connect-report-summary-day-repor",
                templateUrl: "~/App/tenant/views/connect/report/summary/workday/print.cshtml",
                menu: "Tenant.Connect.Report.Summary.DatewiseSale",
                params: {
                    periodInput: null
                }
            });

            $stateProvider.state("tenant.dateitemreport", {
                url: "/connect-report-date-item-report",
                templateUrl: "~/App/tenant/views/connect/report/date/item/item.cshtml",
                menu: "Tenant.Connect.Report.Date.DateItemReport"
            });

            $stateProvider.state("tenant.datewisepayment", {
                url: "/connect-report-summary-payment-report",
                templateUrl: "~/App/tenant/views/connect/report/summary/payment/payment.cshtml",
                menu: "Tenant.Connect.Report.Summary.DatewisePayment"
            });

            $stateProvider.state("tenant.datewisetransaction", {
                url: "/connect-report-summary-transaction-report",
                templateUrl: "~/App/tenant/views/connect/report/summary/transaction/transaction.cshtml",
                menu: "Tenant.Connect.Report.Summary.DatewiseTransaction"
            });

            $stateProvider.state("tenant.tillaccountreport", {
                url: "/connect-report-tilltransactionreport",
                templateUrl: "~/App/tenant/views/connect/report/till/tilltransaction.cshtml",
                menu: "Tenant.Connect.Report.TillTransaction"
            });

            $stateProvider.state("tenant.departmentSalesSummary", {
                url: "/connect-report-department-sale-summary",
                templateUrl: "~/App/tenant/views/connect/report/departments/departmentSummary/department.cshtml",
                menu: "Tenant.Connect.Report.Departments"
            });

            $stateProvider.state("tenant.departmentItemSales", {
                url: "/connect-report-department-sale-item",
                templateUrl: "~/App/tenant/views/connect/report/departments/departmentItem/department.cshtml",
                menu: "Tenant.Connect.Report.Departments"
            });

            $stateProvider.state("tenant.thailandReport", {
                url: "/connect-report-thailand-summary",
                templateUrl: "~/App/tenant/views/connect/report/thailand/thaitax/thaitax.cshtml",
                menu: "Tenant.Connect.Report.Thailand.Item"
            });

            $stateProvider.state("tenant.thailandReportRetails", {
                url: "/connect-report-thailand-summary-details",
                templateUrl: "~/App/tenant/views/connect/report/thailand/thaitaxdetail/thaitaxdetail.cshtml",
                menu: "Tenant.Connect.Report.Thailand.Details"
            });
            
            $stateProvider.state("tenant.saleTarget", {
                url: "/connect-report-salestarget-summary",
                templateUrl: "~/App/tenant/views/connect/report/target/saletarget.cshtml",
                menu: "Tenant.Connect.Report.SaleTarget"
            });

            $stateProvider.state("tenant.tender", {
                url: "/connect-report-tender",
                templateUrl: "~/App/tenant/views/connect/report/tender/tender.cshtml",
                menu: "Tenant.Connect.Report.Tender"
            });

            $stateProvider.state("tenant.paymentexcess", {
                url: "/connect-report-payment-excess",
                templateUrl: "~/App/tenant/views/connect/report/paymentexcess/paymentexcess.cshtml",
                menu: "Tenant.Connect.Report.PaymentExcess"
            });

            $stateProvider.state("tenant.exchangereport", {
                url: "/connect-report-exchange-report",
                templateUrl: "~/App/tenant/views/connect/report/exchangereport/exchangereport.cshtml",
                menu: "Tenant.Connect.Report.ExchangeReport"
            });

            $stateProvider.state("tenant.abnormalendday", {
                url: "/connect-report-abnormal-end-day",
                templateUrl: "~/App/tenant/views/connect/report/abnormalendday/abnormalendday.cshtml",
                menu: "Tenant.Connect.Report.AbnormalEndDay"
            });
            $stateProvider.state("tenant.creditsales", {
                url: "/connect-report-creditsales",
                templateUrl: "~/App/tenant/views/connect/report/creditsales/creditsales.cshtml",
                menu: "Tenant.Connect.Report.CreditSales"
            });

            $stateProvider.state("tenant.dailyreconciliation", {
                url: "/connect-report-daily-reconciliation-report",
                templateUrl: "~/App/tenant/views/connect/report/dailyreconciliation/dailyreconciliation.cshtml",
                menu: "Tenant.Connect.Report.DailyReconciliation"
            });

            $stateProvider.state("tenant.internalsalemeetingreport", {
                url: "/connect-report-internal-sale-meeting",
                templateUrl: "~/App/tenant/views/connect/report/internalsalemeetingreport/internalsalemeetingreport.cshtml",
                menu: "Tenant.Connect.Report.DailyReconciliation"
            });

            $stateProvider.state("tenant.feedbackunread", {
                url: "/connect-report-feedback-unread",
                templateUrl: "~/App/tenant/views/connect/report/feedbackunread/index.cshtml",
                menu: "Tenant.Connect.Report.FeedbackUnread"
            });
            $stateProvider.state("tenant.productmixreport", {
                url: "/connect-report-productmix",
                templateUrl: "~/App/tenant/views/connect/report/productmix/productmix.cshtml",
                menu: "Tenant.Connect.Report.ProductMixReport"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Report.Collection")) {
            $stateProvider.state("tenant.collectionReport", {
                url: "/connect-report-collection",
                templateUrl: "~/App/tenant/views/connect/report/collection/collection.cshtml",
                menu: "Tenant.Connect.Report.Collection"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.Report.Collection")) {
            $stateProvider.state("tenant.schedulesReport", {
                url: "/connect-report-schedules-report",
                templateUrl: "~/App/tenant/views/connect/report/items/schedule/schedule.cshtml",
                menu: "Tenant.Connect.Report.Items.Schedule"
            });
            $stateProvider.state("tenant.locationScheduleReport", {
                url: "/connect-report-location-schedule",
                templateUrl: "~/App/tenant/views/connect/report/items/schedule/locationSchedule.cshtml",
                menu: "Tenant.Connect.Report.Items.Schedule"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Conect.Card.CardType")) {
            $stateProvider.state("tenant.connectcardtype", {
                url: "/connect-card-connectcardtype",
                templateUrl: "~/App/tenant/views/connect/connectcardtype/index.cshtml",
                menu: "Tenant.Connect.Card.CardType"
            });

            $stateProvider.state("tenant.connectcardtypedit", {
                url: "/connect-card-connectcardtype/:id",
                templateUrl: "~/App/tenant/views/connect/connectcardtype/connectcardtype.cshtml",
                menu: "Tenant.Connect.Card.CardType"
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Connect.Card.ConnectCardTypeCategory')) {
            $stateProvider.state('tenant.connectcardtypecategory', {
                url: '/connect-master-connectcardtypecategory',
                templateUrl: '~/App/tenant/views/connect/connectcardtypecategory/index.cshtml',
                menu: 'Tenant.Connect.Card.ConnectCardTypeCategory'
            });
            $stateProvider.state("tenant.detailconnectcardtypecategory", {
                url: "/engage-detailconnectcardtypecategory/:id",
                templateUrl: "~/App/tenant/views/connect/connectcardtypecategory/detail.cshtml",
                menu: "Tenant.Connect.Card.ConnectCardTypeCategory"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Conect.Card.Cards")) {
            $stateProvider.state("tenant.connectcard", {
                url: "/connect-card-connectcard",
                templateUrl: "~/App/tenant/views/connect/connectcard/index.cshtml",
                menu: "Tenant.Connect.Card.Cards"
            });

            //$stateProvider.state("tenant.connectcardtypedit", {
            //$stateProvider.state("tenant.connectcardtypedit", {
            //    url: "/connect-card-connectcardtype/:id",
            //    templateUrl: "~/App/tenant/views/connect/connectcardtype/connectcardtype.cshtml",
            //    menu: "Tenant.Connect.Card.CardType"
            //});
        }
        //HOUSE

        if (abp.auth.hasPermission("Pages.Tenant.House.Dashboard")) {
            $stateProvider.state("tenant.housedashboard", {
                url: "/house-dashboard",
                templateUrl: "~/App/tenant/views/house/housedashboard/housedashboard.cshtml",
                menu: "Tenant.House.Dashboard"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.NotificationDashboard")) {
            $stateProvider.state("tenant.notificationDashboard", {
                url: "/house-notificationDashboard",
                templateUrl: "~/App/tenant/views/house/notificationdashboard/notificationDashboard.cshtml",
                menu: "Tenant.House.Transaction.InterTransfer"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.Brand")) {
            $stateProvider.state("tenant.brand", {
                url: "/house-master-brand",
                templateUrl: "~/App/tenant/views/house/master/brand/index.cshtml",
                menu: "Tenant.House.Master.Brand"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.Country")) {
            $stateProvider.state("tenant.country", {
                url: "/house-master-country",
                templateUrl: "~/App/tenant/views/house/master/country/index.cshtml",
                menu: "Tenant.House.Master.Country"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Numbering")) {
            $stateProvider.state("tenant.numbering", {
                url: "/numbering",
                templateUrl: "~/App/tenant/views/house/numbering/index.cshtml",
                menu: "Tenant.House.Numbering"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.House.Numbering")) {
            $stateProvider.state("tenant.createnumbering", {
                url: "/numbering/:id",
                templateUrl: "~/App/tenant/views/house/numbering/create.cshtml",
                menu: "Tenant.House.Numbering"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.State")) {
            $stateProvider.state("tenant.state", {
                url: "/house-master-state",
                templateUrl: "~/App/tenant/views/house/master/state/index.cshtml",
                //templateUrl: '~/App/tenant/views/house/master/state/index.cshtml',
                menu: "Tenant.House.Master.State"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.City")) {
            $stateProvider.state("tenant.city", {
                url: "/house-master-city",
                templateUrl: "~/App/tenant/views/house/master/city/index.cshtml",
                //templateUrl: '~/App/tenant/views/house/master/city/index.cshtml',
                menu: "Tenant.House.Master.City"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.Supplier")) {
            $stateProvider.state("tenant.supplier", {
                url: "/house-master-supplier",
                templateUrl: "~/App/tenant/views/house/master/supplier/index.cshtml",
                menu: "Tenant.House.Master.Supplier"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.Unit")) {
            $stateProvider.state("tenant.unit", {
                url: "/house-master-unit",
                templateUrl: "~/App/tenant/views/house/master/unit/index.cshtml",
                menu: "Tenant.House.Master.Unit"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.UnitConversion")) {
            $stateProvider.state("tenant.unitconversion", {
                url: "/house-master-unitconversion",
                templateUrl: "~/App/tenant/views/house/master/unitconversion/index.cshtml",
                menu: "Tenant.House.Master.UnitConversion"
            });
        }

        //Recipe

        //RecipeGroup
        if (abp.auth.hasPermission("Pages.Tenant.House.Master.RecipeGroup")) {
            $stateProvider.state("tenant.RecipeGroup", {
                url: "/house-master-RecipeGroup",
                templateUrl: "~/App/tenant/views/house/master/RecipeGroup/index.cshtml",
                menu: "Tenant.House.Master.RecipeGroup"
            });
        }
        //recipe

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.Recipe")) {
            $stateProvider.state("tenant.recipe", {
                url: "/house-master-recipe",
                templateUrl: "~/App/tenant/views/house/master/recipe/index.cshtml",
                menu: "Tenant.House.Master.Recipe"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.Recipe")) {
            $stateProvider.state("tenant.recipedetail", {
                url: "/house-master-recipedetail/:id",
                templateUrl: "~/App/tenant/views/house/master/recipe/recipedetail.cshtml",
                menu: "Tenant.House.Master.Recipe"
            });
        }

        //recipeingredient

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.RecipeIngredient")) {
            $stateProvider.state("tenant.recipeingredient", {
                url: "/house-master-recipeingredient",
                templateUrl: "~/App/tenant/views/house/master/recipeingredient/index.cshtml",
                menu: "Tenant.House.Master.RecipeIngredient"
            });
        }

        //Material Master Menu

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.MaterialGroup")) {
            $stateProvider.state("tenant.materialgroup", {
                url: "/house-master-materialgroup",
                templateUrl: "~/App/tenant/views/house/master/materialgroup/index.cshtml",
                menu: "Tenant.House.Master.MaterialGroup"
            });
        }

        //materialgroupcategory
        if (abp.auth.hasPermission("Pages.Tenant.House.Master.MaterialGroupCategory")) {
            $stateProvider.state("tenant.materialgroupcategory", {
                url: "/house-master-materialgroupcategory",
                templateUrl: "~/App/tenant/views/house/master/materialgroupcategory/index.cshtml",
                menu: "Tenant.House.Master.MaterialGroupCategory"
            });
        }

        //material

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.Material")) {
            $stateProvider.state("tenant.material", {
                url: "/house-master-material",
                templateUrl: "~/App/tenant/views/house/master/material/index.cshtml",
                menu: "Tenant.House.Master.Material"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.Material")) {
            $stateProvider.state("tenant.materialdetail", {
                url: "/house-master-materialdetail/:id,:changeUnitFlag,:callingFrom",
                templateUrl: "~/App/tenant/views/house/master/material/materialdetail.cshtml",
                menu: "Tenant.House.Master.Material"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.OrderTagGroup")) {
            $stateProvider.state("tenant.ordertaggroup", {
                url: "/connect-menu-ordertaggroup",
                templateUrl: "~/App/tenant/views/connect/ordertaggroup/index.cshtml",
                menu: "Tenant.Connect.Menu.OrderTagGroup"
            });

            $stateProvider.state("tenant.ordertaggrouplocationprice", {
                url: "/connect-menu-ordertaggrouplocationprice/:id,:ordertagId",
                templateUrl: "~/App/tenant/views/connect/ordertaggroup/locationPriceModal.cshtml",
                menu: "Tenant.Connect.Menu.OrderTagGroup"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Master.TillAccount")) {
            $stateProvider.state("tenant.tillaccount", {
                url: "/connect-master-tillaccount",
                templateUrl: "~/App/tenant/views/connect/tillaccount/index.cshtml",
                menu: "Tenant.Connect.Master.TillAccount"
            });

            $stateProvider.state("tenant.createtillaccount", {
                url: "/connect-create-tillaccount/:id",
                templateUrl: "~/App/tenant/views/connect/tillaccount/create.cshtml",
                menu: "Tenant.Connect.Master.TillAccount"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.TicketTagGroup")) {
            $stateProvider.state("tenant.tickettaggroup", {
                url: "/connect-menu-tickettaggroup",
                templateUrl: "~/App/tenant/views/connect/tickettaggroup/index.cshtml",
                menu: "Tenant.Connect.Menu.TicketTagGroup"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Menu.TicketTagGroup")) {
            $stateProvider.state("tenant.detailtickettaggroup", {
                url: "/connect-tag-detailticketdertag/:id",
                templateUrl: "~/App/tenant/views/connect/tickettaggroup/tagdetail.cshtml",
                menu: "Tenant.Connect.Menu.TicketTagGroup"
            });
        }

        //Template
        if (abp.auth.hasPermission('Pages.Tenant.House.Master.Template')) {
            $stateProvider.state('tenant.housetemplate', {
                url: '/housetemplate',
                templateUrl: '~/App/tenant/views/house/master/template/index.cshtml',
                menu: 'Tenant.House.Master.Template'
            });
        }

        //materiallocationwisestock
        if (abp.auth.hasPermission("Pages.Tenant.House.Master.MaterialLocationWiseStock")) {
            $stateProvider.state("tenant.materiallocationwisestock", {
                url: "/house-master-materiallocationwisestock",
                templateUrl: "~/App/tenant/views/house/master/materiallocationwisestock/index.cshtml",
                menu: "Tenant.House.Master.MaterialLocationWiseStock"
            });
        }

        //All Location Materiallocationwisestock
        if (abp.auth.hasPermission("Pages.Tenant.House.Master.MaterialLocationWiseStock")) {
            $stateProvider.state("tenant.allMateriallocationwisestock", {
                url: "/house-master-allMateriallocationwisestock",
                templateUrl: "~/App/tenant/views/house/master/materiallocationwisestock/allLocationStock.cshtml",
                menu: "Tenant.House.Master.MaterialLocationWiseStock"
            });
        }

        //All Location Materiallocationwisestock Details
        if (abp.auth.hasPermission("Pages.Tenant.House.Master.MaterialLocationWiseStock")) {
            $stateProvider.state("tenant.locationWiseStockNonModal", {
                url: "/house-master-locationWiseStockNonModal/:id",
                templateUrl: "~/App/tenant/views/house/master/materiallocationwisestock/locationWiseStockNonModal.cshtml",
                menu: "Tenant.House.Master.MaterialLocationWiseStock",
                params: {
                    materialStock: ""
                },
            });
        }

        // Tax Identification

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.Tax")) {
            $stateProvider.state("tenant.tax", {
                url: "/house-master-tax",
                templateUrl: "~/App/tenant/views/house/master/tax/index.cshtml",
                menu: "Tenant.House.Master.Tax"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.Tax")) {
            $stateProvider.state("tenant.taxdetail", {
                url: "/house-master-taxdetail/:id",
                templateUrl: "~/App/tenant/views/house/master/tax/taxDetail.cshtml",
                menu: "Tenant.House.Master.Tax"
            });
        }

        //SAlesTax
        if (abp.auth.hasPermission("Pages.Tenant.House.Master.SalesTax")) {
            $stateProvider.state("tenant.salestax", {
                url: "/house-master-salestax",
                templateUrl: "~/App/tenant/views/house/master/salestax/index.cshtml",
                menu: "Tenant.House.Master.SalesTax"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.SalesTax")) {
            $stateProvider.state("tenant.salestaxdetail", {
                url: "/house-master-salestaxdetail/:id",
                templateUrl: "~/App/tenant/views/house/master/salestax/salestaxDetail.cshtml",
                menu: "Tenant.House.Master.SalesTax"
            });
        }
        //Storage LOcation

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.StorageLocation")) {
            $stateProvider.state("tenant.storagelocation", {
                url: "/house-master-storagelocation",
                templateUrl: "~/App/tenant/views/house/master/storagelocation/index.cshtml",
                menu: "Tenant.House.Master.StorageLocation"
            });
        }

        //Contact

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.Contact")) {
            $stateProvider.state("tenant.contact", {
                url: "/house-master-contact",
                templateUrl: "~/App/tenant/views/house/master/contact/index.cshtml",
                menu: "Tenant.House.Master.Contact"
            });
        }

        //Contact Location

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.LocationContact")) {
            $stateProvider.state("tenant.locationcontact", {
                url: "/house-master-locationcontact",
                templateUrl: "~/App/tenant/views/connect/master/locationcontact/index.cshtml",
                menu: "Tenant.House.Master.LocationContact"
            });
        }

        // Supplier Contact

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.SupplierContact")) {
            $stateProvider.state("tenant.suppliercontact", {
                url: "/house-master-suppliercontact",
                templateUrl: "~/App/tenant/views/house/master/suppliercontact/index.cshtml",
                menu: "Tenant.House.Master.SupplierContact"
            });
        }

        //  Manual Reason

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.ManualReason")) {
            $stateProvider.state("tenant.manualreason", {
                url: "/house-master-manualreason",
                templateUrl: "~/App/tenant/views/house/master/manualreason/index.cshtml",
                menu: "Tenant.House.Master.ManualReason"
            });
        }

        //MaterialStockIndividualStore
        if (abp.auth.hasPermission("Pages.Tenant.House.Master.MaterialStockIndividualStore")) {
            $stateProvider.state("tenant.materialstockindividualstore", {
                url: "/house-master-materialstockindividualstore",
                templateUrl: "~/App/tenant/views/house/master/materialstockindividualstore/index.cshtml",
                menu: "Tenant.House.Master.MaterialStockIndividualStore"
            });
        }
        //suppliermaterial

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.SupplierMaterial")) {
            $stateProvider.state("tenant.suppliermaterial", {
                url: "/house-master-suppliermaterial/:id",
                templateUrl: "~/App/tenant/views/house/master/suppliermaterial/createOrEditModal.cshtml",
                menu: "Tenant.House.Master.SupplierMaterial"
            });
        }

        //materialwisebrand

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.MaterialBrandsLink")) {
            $stateProvider.state("tenant.materialbrandslink", {
                url: "/house-master-materialbrandslink",
                templateUrl: "~/App/tenant/views/house/master/materialbrandslink/index.cshtml",
                menu: "Tenant.House.Master.MaterialBrandsLink"
            });
        }

        // InwardDirectCredit

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.InwardDirectCredit")) {
            $stateProvider.state("tenant.inwarddirectcredit", {
                url: "/house-transaction-inwarddirectcredit",
                templateUrl: "~/App/tenant/views/house/transaction/inwarddirectcredit/index.cshtml",
                menu: "Tenant.House.Transaction.InwardDirectCredit"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.InwardDirectCredit")) {
            $stateProvider.state("tenant.inwarddirectcreditmainform", {
                url: "/house-transaction-inwarddirectcreditmainform/:id",
                templateUrl: "~/App/tenant/views/house/transaction/inwarddirectcredit/inwardDcMainForm.cshtml",
                menu: "Tenant.House.Transaction.InwardDirectCredit"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.InwardDirectCredit")) {
            $stateProvider.state("tenant.inwarddcprint", {
                url: "/house-transaction-inwarddcprint/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/inwarddirectcredit/inwardDcPrint.cshtml",
                menu: "Tenant.House.Transaction.InwardDirectCredit"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.InwardDirectCredit")) {
            $stateProvider.state("tenant.inwarddcprint40col", {
                url: "/house-transaction-inwarddcprint40col/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/inwarddirectcredit/revisedinwardDcPrint.cshtml",
                menu: "Tenant.House.Transaction.InwardDirectCredit"
            });
        }

        //  Inter Transfer - Request
        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.InterTransfer")) {
            $stateProvider.state("tenant.intertransferrequest", {
                url: "/house-transaction-intertransferrequest",
                templateUrl: "~/App/tenant/views/house/transaction/intertransfer/request/index.cshtml",
                menu: "Tenant.House.Transaction.InterTransfer"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.InterTransfer")) {
            $stateProvider.state("tenant.intertransferrequestprint", {
                url: "/house-transaction-intertransferrequestprint/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/intertransfer/request/RequestPrint.cshtml",
                menu: "Tenant.House.Transaction.InterTransfer"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.InterTransfer")) {
            $stateProvider.state("tenant.intertransferrequestdetail", {
                url: "/house-transaction-intertransferrequestdetail/:id,:directTransfer",
                templateUrl: "~/App/tenant/views/house/transaction/intertransfer/request/request.cshtml",
                menu: "Tenant.House.Transaction.InterTransfer"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.InterTransfer")) {
            $stateProvider.state("tenant.intertransferrequestsummary", {
                url: "/house-transaction-intertransferrequestsummary",
                templateUrl: "~/App/tenant/views/house/transaction/intertransfer/request/intertransferrequestsummary.cshtml",
                menu: "Tenant.House.Transaction.InterTransfer"
            });
        }

        //  Inter Transfer  -   Transfer Approval

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.InterTransferApproval")) {
            $stateProvider.state("tenant.intertransferapproval", {
                url: "/house-transaction-intertransferapproval",
                templateUrl: "~/App/tenant/views/house/transaction/intertransfer/approval/indexTransfer.cshtml",
                menu: "Tenant.House.Transaction.InterTransferApproval"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.InterTransferApproval")) {
            $stateProvider.state("tenant.requestintoAutoPo", {
                url: "/house-requestintoAutoPo",
                templateUrl: "~/App/tenant/views/house/transaction/intertransfer/approval/requestAutoPo.cshtml",
                menu: "Tenant.House.Transaction.InterTransfer"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.InterTransferApproval")) {
            $stateProvider.state("tenant.intertransferapprovaldetail", {
                url: "/house-transaction-intertransferapprovaldetail/:id,:requestLocationRefName,:locationRefName",
                templateUrl: "~/App/tenant/views/house/transaction/intertransfer/approval/Approval.cshtml",
                menu: "Tenant.House.Transaction.InterTransfer"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.InterTransferApproval")) {
            $stateProvider.state("tenant.intertransferapprovalprint", {
                url: "/house-transaction-intertransferapprovalprint/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/intertransfer/approval/ApprovalPrint.cshtml",
                menu: "Tenant.House.Transaction.InterTransfer"
            });
        }

        //  Inter Transfer  -   Transfer Received

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.InterTransferReceived")) {
            $stateProvider.state("tenant.intertransferreceived", {
                url: "/house-transaction-intertransferreceived",
                templateUrl: "~/App/tenant/views/house/transaction/intertransfer/received/indexReceived.cshtml",
                menu: "Tenant.House.Transaction.InterTransferReceived"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.InterTransferReceived")) {
            $stateProvider.state("tenant.intertransferreceiveddetail", {
                url: "/house-transaction-intertransferreceiveddetail/:id,:requestLocationRefName",
                templateUrl: "~/App/tenant/views/house/transaction/intertransfer/received/Received.cshtml",
                menu: "Tenant.House.Transaction.InterTransferReceived"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.InterTransferReceived")) {
            $stateProvider.state("tenant.intertransferreceivedprint", {
                url: "/house-transaction-intertransferreceivedprint/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/intertransfer/received/ReceivedPrint.cshtml",
                menu: "Tenant.House.Transaction.InterTransfer"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.InterTransferReceived")) {
            $stateProvider.state("tenant.intertransferreceivedsummary", {
                url: "/house-transaction-intertransferreceivedsummary",
                templateUrl: "~/App/tenant/views/house/transaction/intertransfer/received/indexReceived.cshtml",
                menu: "Tenant.House.Transaction.InterTransferReceived"
            });
        }
        //  Inter Transfer  -   Transfer Direct Approval

        //if (abp.auth.hasPermission('Pages.Tenant.House.Transaction.InterTransferDirectApproval')) {
        //    $stateProvider.state('tenant.InterTransferDirectApproval', {
        //        url: '/house-transaction-directapproval',
        //        templateUrl: '~/App/tenant/views/house/transaction/intertransfer/directapproval/index.cshtml',
        //        menu: 'Tenant.House.Transaction.DirectApproval'
        //    });
        //}

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.InterTransferDirectApproval")) {
            $stateProvider.state("tenant.intertransferdirectapprovaldetail", {
                url: "/house-transaction-directapprovaldetail",
                templateUrl: "~/App/tenant/views/house/transaction/intertransfer/directapproval/directapproval.cshtml",
                menu: "Tenant.House.Transaction.DirectApproval"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.InterTransferDirectApproval")) {
            $stateProvider.state("tenant.intertransferapprovalandrequestdetail", {
                url: "/house-transaction-intertransferapprovalandrequestdetail",
                templateUrl: "~/App/tenant/views/house/transaction/intertransfer/directapproval/approvalandreceived.cshtml",
                menu: "Tenant.House.Transaction.DirectApproval"
            });
        }

        //Issue
        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Issue")) {
            $stateProvider.state("tenant.issue", {
                url: "/house-transaction-issue",
                templateUrl: "~/App/tenant/views/house/transaction/issue/index.cshtml",
                menu: "Tenant.House.Transaction.Issue"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Issue")) {
            $stateProvider.state("tenant.issuedetail", {
                url: "/house-transaction-issuedetail/:id",
                templateUrl: "~/App/tenant/views/house/transaction/issue/issueMainForm.cshtml",
                menu: "Tenant.House.Transaction.Issue"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Issue")) {
            $stateProvider.state("tenant.issueprint", {
                url: "/house-transaction-issueprint/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/issue/issuePrintDetail.cshtml",
                menu: "Tenant.House.Transaction.Issue"
            });
        }

        // Request
        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Request")) {
            $stateProvider.state("tenant.request", {
                url: "/house-transaction-request",
                templateUrl: "~/App/tenant/views/house/transaction/request/index.cshtml",
                menu: "Tenant.House.Transaction.Request"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Request")) {
            $stateProvider.state("tenant.requestdetail", {
                url: "/house-transaction-request/:id",
                templateUrl: "~/App/tenant/views/house/transaction/request/requestMainForm.cshtml",
                menu: "Tenant.House.Transaction.Request"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Request")) {
            $stateProvider.state("tenant.requestprint", {
                url: "/house-transaction-requestprint/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/request/requestPrintDetail.cshtml",
                menu: "Tenant.House.Transaction.Request"
            });
        }

        //  Return
        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Return")) {
            $stateProvider.state("tenant.return", {
                url: "/house-transaction-return",
                templateUrl: "~/App/tenant/views/house/transaction/return/index.cshtml",
                menu: "Tenant.House.Transaction.Return"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Return")) {
            $stateProvider.state("tenant.returndetail", {
                url: "/house-transaction-returndetail/:id",
                templateUrl: "~/App/tenant/views/house/transaction/return/returnMainForm.cshtml",
                menu: "Tenant.House.Transaction.Return"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Adjustment")) {
            $stateProvider.state("tenant.adjustment", {
                url: "/house-transaction-adjustment",
                templateUrl: "~/App/tenant/views/house/transaction/adjustment/index.cshtml",
                menu: "Tenant.House.Transaction.Adjustment",
                params: {
                    physicalReportFlag: false,
                },
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Adjustment")) {
            $stateProvider.state("tenant.physicalstockreport", {
                url: "/house-transaction-physicalstockreport",
                templateUrl: "~/App/tenant/views/house/transaction/adjustment/index.cshtml",
                menu: "Tenant.House.Transaction.Adjustment",
                params: {
                    physicalReportFlag: true,
                    filtervalue: 888
                },
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Adjustment")) {
            $stateProvider.state("tenant.daycloseadjustmentreport", {
                url: "/house-transaction-daycloseadjustmentreport",
                templateUrl: "~/App/tenant/views/house/transaction/adjustment/index.cshtml",
                menu: "Tenant.House.Transaction.Adjustment",
                params: {
                    physicalReportFlag: true,
                    filtervalue: 999
                },
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Adjustment")) {
            $stateProvider.state("tenant.adjustmentdetail", {
                url: "/house-transaction-adjustmentdetail/:id,:forceAdjustmentFlag,:closingstockAdjustmentFlag,:dayCloseCallFlag",
                templateUrl: "~/App/tenant/views/house/transaction/adjustment/adjustmentMainForm.cshtml",
                menu: "Tenant.House.Transaction.Adjustment",
                params: {
                    autoadjustmentdetail: ""
                },
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.PurchaseCategory")) {
            $stateProvider.state("tenant.purchasecategory", {
                url: "/house-master-purchasecategory",
                templateUrl: "~/App/tenant/views/house/master/purchasecategory/index.cshtml",
                menu: "Tenant.House.Master.PurchaseCategory"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Adjustment")) {
            $stateProvider.state("tenant.adjustmentprint", {
                url: "/house-transaction-adjustmentprint/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/adjustment/adjustmentPrint.cshtml",
                menu: "Tenant.House.Transaction.Adjustment"
            });
        }

        //Invoice
        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Invoice")) {
            $stateProvider.state("tenant.invoice", {
                url: "/house-transaction-invoice",
                templateUrl: "~/App/tenant/views/house/transaction/invoice/index.cshtml",
                menu: "Tenant.House.Transaction.Invoice"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Invoice")) {
            $stateProvider.state("tenant.invoicesummary", {
                url: "/house-transaction-invoicesummary",
                templateUrl: "~/App/tenant/views/house/transaction/invoice/invoiceSummary.cshtml",
                menu: "Tenant.House.Transaction.Invoice"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.ClosingStockEntry")) {
            $stateProvider.state("tenant.consolidated-closingstockdiff", {
                url: "/house-consolidated-closingstockdiff",
                templateUrl: "~/App/tenant/views/house/master/housereport/consolidatedClosingStockDiff.cshtml",
                menu: "Tenant.House.Transaction.ClosingStock"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.closingstockwithcost", {
                url: "/house-closingstockwithcost",
                templateUrl: "~/App/tenant/views/house/master/housereport/closingStockExcelwithGroupCategoryMaterial.cshtml",
                menu: "Tenant.House.Master.HouseReport"
            });
        }
        

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Invoice")) {
            $stateProvider.state("tenant.invoicedetail", {
                url: "/house-transaction-invoicedetail/:id,:invoiceType",
                templateUrl: "~/App/tenant/views/house/transaction/invoice/invoiceDetail.cshtml",
                menu: "Tenant.House.Transaction.Invoice"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Invoice")) {
            $stateProvider.state("tenant.invoicedetailfromPo", {
                url: "/house-transaction-invoicedetailfromPo/:id,:invoiceType,:poRefId",
                templateUrl: "~/App/tenant/views/house/transaction/invoice/invoicedetailfromPo.cshtml",
                menu: "Tenant.House.Transaction.Invoice"
            });
        }
        

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Invoice")) {
            $stateProvider.state("tenant.invoiceprintdetail", {
                url: "/house-transaction-invoiceprint/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/invoice/invoicePrintDetail.cshtml",
                menu: "Tenant.House.Transaction.Invoice"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Invoice")) {
            $stateProvider.state("tenant.invoiceprintdetail40col", {
                url: "/house-transaction-invoiceprintdetail40col/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/invoice/smartinvoicePrintDetail.cshtml",
                menu: "Tenant.House.Transaction.Invoice"
            });
        }

        //   UserDefaultOrganization
        if (abp.auth.hasPermission("Pages.Tenant.House.Master.UserDefaultOrganization")) {
            $stateProvider.state("tenant.userdefaultorganization", {
                url: "/user-organization",
                templateUrl: "~/App/tenant/views/house/master/userdefaultorganization/index.cshtml",
                menu: "Tenant.House.Master.UserDefaultOrganization"
            });
        }

        //MaterialLedger
        if (abp.auth.hasPermission("Pages.Tenant.House.Master.MaterialLedger")) {
            $stateProvider.state("tenant.materialledger", {
                url: "/house-master-materialledger",
                templateUrl: "~/App/tenant/views/house/master/materialledger/index.cshtml",
                menu: "Tenant.House.Master.MaterialLedger"
            });
        }

        //PurchaseOrder

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.PurchaseOrder")) {
            $stateProvider.state("tenant.purchaseorder", {
                url: "/house-transaction-purchaseorder",
                templateUrl: "~/App/tenant/views/house/transaction/purchaseorder/index.cshtml",
                menu: "Tenant.House.Transaction.PurchaseOrder"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.PurchaseOrder")) {
            $stateProvider.state("tenant.purchaseorderdetail", {
                url: "/house-transaction-purchaseorderdetail/:id,:autoPoFlag,:autoPoStatus",
                templateUrl: "~/App/tenant/views/house/transaction/purchaseorder/purchaseOrder.cshtml",
                menu: "Tenant.House.Transaction.PurchaseOrder",
                params: {
                    autoTransferPoDetails: "",
                    autoCaterPoDetails: ""
                },
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.PurchaseOrder")) {
            $stateProvider.state("tenant.purchaseorderupload", {
                url: "/house-transaction-purchaseorderupload/:id",
                templateUrl: "~/App/tenant/views/house/transaction/purchaseorder/purchaseOrderUpload.cshtml",
                menu: "Tenant.House.Transaction.PurchaseOrder"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.PurchaseOrder")) {
            $stateProvider.state("tenant.purchaseorderprint", {
                url: "/house-transaction-purchaseorderprint/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/purchaseorder/PoPrint.cshtml",
                menu: "Tenant.House.Transaction.PurchaseOrder"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.PurchaseOrder")) {
            $stateProvider.state("tenant.autopogenerate", {
                url: "/house-transaction-autopogenerate",
                templateUrl: "~/App/tenant/views/house/transaction/purchaseorder/AutoPoSetting.cshtml",
                menu: "Tenant.House.Transaction.PurchaseOrder"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.PurchaseOrder")) {
            $stateProvider.state("tenant.createbulkpurchaseorder", {
                url: "/house-transaction-createbulkpurchaseorder",
                templateUrl: "~/App/tenant/views/house/transaction/purchaseorder/createBulkPurchaseOrder.cshtml",
                menu: "Tenant.House.Transaction.PurchaseOrder"
            });
        }

        //ProductRecipesLink
        if (abp.auth.hasPermission("Pages.Tenant.House.Master.ProductRecipesLink")) {
            $stateProvider.state("tenant.productrecipeslink", {
                url: "/house-master-productrecipeslink",
                templateUrl: "~/App/tenant/views/house/master/productrecipeslink/index.cshtml",
                menu: "Tenant.House.Master.ProductRecipesLink"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.ProductRecipesLink")) {
            $stateProvider.state("tenant.productrecipeslinkdetail", {
                url: "/house-master-productrecipeslinkdetail/:id",
                templateUrl: "~/App/tenant/views/house/master/productrecipeslink/productRecipeLink.cshtml",
                menu: "Tenant.House.Master.ProductRecipesLink"
            });
        }

        //HouseReport
        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.housereportstock", {
                url: "/house-master-housereportstock",
                templateUrl: "~/App/tenant/views/house/master/housereport/housereport.cshtml",
                menu: "Tenant.House.Master.HouseReport",
                params: {
                    reportKey: "STOCK",
                },
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.housereportvariancestock", {
                url: "/house-report-housereportvariancestock",
                templateUrl: "~/App/tenant/views/house/master/housereport/housereport.cshtml",
                menu: "Tenant.House.Master.HouseReport",
                params: {
                    reportKey: "VARIANCESTOCK",
                },
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.housereportclosingstock", {
                url: "/house-master-housereportclstock",
                templateUrl: "~/App/tenant/views/house/master/housereport/housereport.cshtml",
                menu: "Tenant.House.Master.HouseReport",
                params: {
                    reportKey: "CLOSESTOCK",
                },
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.housereportrateview", {
                url: "/house-master-housereportreportview",
                templateUrl: "~/App/tenant/views/house/master/housereport/housereport.cshtml",
                menu: "Tenant.House.Master.HouseReport",
                params: {
                    reportKey: "RATEVIEW",
                },
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.housereportsupplierrateview", {
                url: "/house-master-housereportrateview",
                templateUrl: "~/App/tenant/views/house/master/housereport/housereport.cshtml",
                menu: "Tenant.House.Master.HouseReport",
                params: {
                    reportKey: "SUPPLIERRATEVIEW",
                },
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.reportdetailproduct", {
                url: "/house-master-reportdetailproduct",
                templateUrl: "~/App/tenant/views/house/master/housereport/productAnalysis.cshtml",
                menu: "Tenant.House.Master.HouseReport"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.inventoryclosingvariance", {
                url: "/inventoryclosingvariance",
                templateUrl: "~/App/tenant/views/house/master/closingVarriance/varianceClosingStock.cshtml",
                menu: "Tenant.House.Master.HouseReport"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.materialwastagereport", {
                url: "/materialwastagereport",
                templateUrl: "~/App/tenant/views/house/master/housereport/materialWastageReport.cshtml",
                menu: "Tenant.House.Master.HouseReport"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.menuWastageReport", {
                url: "/menuWastageReport",
                templateUrl: "~/App/tenant/views/house/master/housereport/menuWastageReport.cshtml",
                menu: "Tenant.House.Master.HouseReport"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.housereportcostingmaterial", {
                url: "/house-master-housereportcostingmaterial",
                templateUrl: "~/App/tenant/views/house/master/housereport/costingReport.cshtml",
                menu: "Tenant.House.Master.HouseReport",
                params: {
                    materialcostingflag: true,
                    categorycostingflag: false,
                },
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.housereportcostingcategory", {
                url: "/house-master-housereportcostingcategory",
                templateUrl: "~/App/tenant/views/house/master/housereport/costingReport.cshtml",
                menu: "Tenant.House.Master.HouseReport",
                params: {
                    materialcostingflag: false,
                    categorycostingflag: true,
                },
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.housereportlocationwisecostofsales", {
                url: "/house-master-housereportlocationwisecostofsales",
                templateUrl: "~/App/tenant/views/house/master/housereport/locationwiseCostOfSSales.cshtml",
                menu: "Tenant.House.Master.HouseReport",
                params: {
                    materialcostingflag: false,
                    categorycostingflag: false,
                },
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.housereportcostingrecipe", {
                url: "/house-master-housereportcostingrecipe",
                templateUrl: "~/App/tenant/views/house/master/housereport/recipecostingReport.cshtml",
                menu: "Tenant.House.Master.HouseReport",
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.productionunitcosting", {
                url: "/house-report-productionunitcosting",
                templateUrl: "~/App/tenant/views/house/master/housereport/productionunitcostingReport.cshtml",
                menu: "Tenant.House.Master.HouseReport",
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.menuwisecostingconsolidateddetail", {
                url: "/house-report-menuwisecostingconsolidateddetail",
                templateUrl: "~/App/tenant/views/house/master/housereport/menuitemcostingReport.cshtml",
                menu: "Tenant.House.Master.HouseReport",
                params: {
                    reportStatus: "Consolidated",
                },
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.housereportmaterialtrack", {
                url: "/house-master-housereportmaterialtrack",
                templateUrl: "~/App/tenant/views/house/master/housereport/materialTrackReport.cshtml",
                menu: "Tenant.House.Master.HouseReport",
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.housedaycloseSalesDifference", {
                url: "/house-daycloseSalesDifference",
                templateUrl: "~/App/tenant/views/house/master/housereport/daycloseSalesDifference.cshtml",
                menu: "Tenant.House.Master.HouseReport",
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.errorcorrectionprocess", {
                url: "/house-master-errorCorrectionProcess",
                templateUrl: "~/App/tenant/views/house/master/errorcorrection/errorCorrectionProcess.cshtml",
                menu: "Tenant.House.Master.HouseReport",
            });
        }

        // Purchase Projection Report

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.purchaseprojection", {
                url: "/house-master-housereportpurchaseprojection",
                templateUrl: "~/App/tenant/views/house/master/materiallocationwisestock/PoAutoGeneration.cshtml",
                menu: "Tenant.House.Master.HouseReport"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.purchasereciptreport", {
                url: "/house-master-purchasereciptreport",
                templateUrl: "~/App/tenant/views/house/master/housereport/purchaserecipt.cshtml",
                menu: "Tenant.House.Master.HouseReport",
                params: {
                    consolidatedFlag: false,
                },
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.purchasewithcategoryandPoReference", {
                url: "/purchasewithcategoryandPoReference",
                templateUrl: "~/App/tenant/views/house/master/housereport/purchaseexportwithCategory.cshtml",
                menu: "Tenant.House.Master.HouseReport",
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.purchasereportExcelwithGroupCategoryMaterial", {
                url: "/purchasereportExcelwithGroupCategoryMaterial",
                templateUrl: "~/App/tenant/views/house/master/housereport/purchasereportExcelwithGroupCategoryMaterial.cshtml",
                menu: "Tenant.House.Master.HouseReport",
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.purchasereportconsolidated", {
                url: "/house-master-purchasereportconsolidated",
                templateUrl: "~/App/tenant/views/house/master/housereport/purchaserecipt.cshtml",
                menu: "Tenant.House.Master.HouseReport",
                params: {
                    consolidatedFlag: true,
                },
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.purchaseconsolidateddetailreport", {
                url: "/house-master-purchaseconsolidateddetailreport",
                templateUrl: "~/App/tenant/views/house/master/housereport/purchaseConsolidatedVsDetailReport.cshtml",
                menu: "Tenant.House.Master.HouseReport"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            // CRG Reports
            $stateProvider.state("tenant.purchaseTaxDetailReport", {
                url: "/house-report-purchaseTaxDetailReport",
                templateUrl: "~/App/tenant/views/house/master/housereport/purchasetaxDetailReport/purchasetaxDetailReport.cshtml",
                menu: "Tenant.House.Master.HouseReport"
            });

            $stateProvider.state("tenant.purchaseReturnDetailReport", {
                url: "/house-report-purchaseReturnDetailReport",
                templateUrl: "~/App/tenant/views/house/master/housereport/purchasereturndetailreport/purchasereturndetailreport.cshtml",
                menu: "Tenant.House.Master.HouseReport"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.transferreport", {
                url: "/house-master-transferreport",
                templateUrl: "~/App/tenant/views/house/master/housereport/transferreport.cshtml",
                menu: "Tenant.House.Master.HouseReport"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.intertransferRequestConsolidationReport", {
                url: "/house-intertransferRequestConsolidationReport",
                templateUrl: "~/App/tenant/views/house/master/housereport/intertransferRequestConsolidationReport.cshtml",
                menu: "Tenant.House.Master.HouseReport"
            });
        }
        

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.intertransferStatusReport", {
                url: "/intertransferStatusReport",
                templateUrl: "~/App/tenant/views/house/master/housereport/intertransferStatusReport.cshtml",
                menu: "Tenant.House.Master.HouseReport"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.transferreceivedreport", {
                url: "/house-master-transferreceivedreport",
                templateUrl: "~/App/tenant/views/house/master/housereport/transferreceivedreport.cshtml",
                menu: "Tenant.House.Master.HouseReport"
            });
        }


        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.intertransferReceivedSummary", {
                url: "/house-intertransferReceivedSummary",
                templateUrl: "~/App/tenant/views/house/master/housereport/Intertransferreceivedreportexcel/intertransferreceivedreportExcelwithGroupCategoryMaterial.cshtml",
                menu: "Tenant.House.Master.HouseReport"
            });
        }



        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.intertransferReceivedDetailTrack", {
                url: "/house-intertransferReceivedDetailTrack",
                templateUrl: "~/App/tenant/views/house/master/housereport/Intertransferreceivedreportexcel/intertransferReceivedDetailTrack.cshtml",
                menu: "Tenant.House.Master.HouseReport"
            });
        }
        

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.wastagematerialreport", {
                url: "/house-master-wastagematerialreport",
                templateUrl: "~/App/tenant/views/house/master/housereport/wastageReport.cshtml",
                menu: "Tenant.House.Master.HouseReport"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.HouseReport")) {
            $stateProvider.state("tenant.purchasereportcategorywise", {
                url: "/house-master-purchasereportcategorywise",
                templateUrl: "~/App/tenant/views/house/master/housereport/purchasereciptCategoryWise.cshtml",
                menu: "Tenant.House.Master.HouseReport"
            });
        }

        //Company

        if (abp.auth.hasPermission("Pages.Tenant.Connect.Master.Company")) {
            $stateProvider.state("tenant.company", {
                url: "/connect-master-company",
                templateUrl: "~/App/tenant/views/connect/company/index.cshtml",
                menu: "Tenant.Connect.Master.Company"
            });

            $stateProvider.state("tenant.companydetail", {
                url: "/connect-master-company-detail/:id",
                templateUrl: "~/App/tenant/views/connect/company/detail.cshtml",
                menu: "Tenant.Connect.Master.Company"
            });
        }

        //Addons
        if (abp.auth.hasPermission("Pages.Tenant.Addons")) {
            $stateProvider.state("tenant.addons", {
                url: "/addons-index",
                templateUrl: "~/App/tenant/views/addons/index.cshtml",
                menu: "Tenant.Addons.Index"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Addons")) {
            $stateProvider.state("tenant.addonqb", {
                url: "/addons-qb/:id",
                templateUrl: "~/App/tenant/views/addons/qb/qb.cshtml"
            });

            $stateProvider.state("tenant.addonzohobooks", {
                url: "/addons-zohobooks/:id",
                templateUrl: "~/App/tenant/views/addons/zohobooks/zohobooks.cshtml"
            });

            $stateProvider.state("tenant.addonqbenterprise", {
                url: "/addons-qbenter/:id",
                templateUrl: "~/App/tenant/views/addons/qbent/qb.cshtml"
            });
            $stateProvider.state("tenant.addonxero", {
                url: "/addons-xero/:id",
                templateUrl: "~/App/tenant/views/addons/xero/xero.cshtml"
            });

            $stateProvider.state("tenant.addonquickbooks", {
                url: "/addons-quickbooks/:id",
                templateUrl: "~/App/tenant/views/addons/qb/qb.cshtml"
            });

            $stateProvider.state("tenant.addonqoyod", {
                url: "/addons-quoyod/:id",
                templateUrl: "~/App/tenant/views/addons/qoyod/qb.cshtml"
            });

            $stateProvider.state("tenant.addonzomato", {
                url: "/addons-zomato/:id",
                templateUrl: "~/App/tenant/views/addons/zomato/zomato.cshtml"
            });

            $stateProvider.state("tenant.addonodoo", {
                url: "/addons-odoo/:id",
                templateUrl: "~/App/tenant/views/addons/odoo/odoo.cshtml"
            });

            $stateProvider.state("tenant.addonurbanpiper", {
                url: "/addons-urbanpiper/:id",
                templateUrl: "~/App/tenant/views/addons/urbanpiper/urbanpiper.cshtml"
            });

            $stateProvider.state("tenant.addongrab", {
                url: "/addons-addongrab/:id",
                templateUrl: "~/App/tenant/views/addons/grab/grab.cshtml"
            });
        }

        //TICK
        if (abp.auth.hasPermission("Pages.Tenant.Tick.Department")) {
            $stateProvider.state("tenant.tickdepartment", {
                url: "/tick-department",
                templateUrl: "~/App/tenant/views/tick/department/index.cshtml",
                menu: "Tenant.Tick.Department"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Tick.Employee")) {
            $stateProvider.state("tenant.employee", {
                url: "/tick-employee",
                templateUrl: "~/App/tenant/views/tick/employee/index.cshtml",
                menu: "Tenant.Tick.Employee"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Tick.Employee")) {
            $stateProvider.state("tenant.employeeedit", {
                url: "/tick-employeeedit/:id",
                templateUrl: "~/App/tenant/views/tick/employee/detail.cshtml",
                menu: "Tenant.Tick.Employee"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Tick.Employee")) {
            $stateProvider.state("tenant.employeeinactive", {
                url: "/tick-employeeinactive/:id",
                templateUrl: "~/App/tenant/views/tick/employee/inactive.cshtml",
                menu: "Tenant.Tick.Employee"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Tick.Ticket")) {
            $stateProvider.state("tenant.tickticket", {
                url: "/tick-ticket",
                templateUrl: "~/App/tenant/views/tick/ticket/index.cshtml",
                menu: "Tenant.Tick.Ticket"
            });

            $stateProvider.state("tenant.tickticketdetail", {
                url: "/tick-ticket-detail/:id",
                templateUrl: "~/App/tenant/views/tick/ticket/detail.cshtml",
                menu: "Tenant.Tick.Ticket"
            });

            $stateProvider.state("tenant.tickticketcreate", {
                url: "/tick-ticket-create/",
                templateUrl: "~/App/tenant/views/tick/ticket/create.cshtml",
                menu: "Tenant.Tick.Ticket"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Tick.EmployeeAttendance")) {
            $stateProvider.state("tenant.employeeattendance", {
                url: "/tick-master-employeeattendance",
                templateUrl: "~/App/tenant/views/tick/employeeattendance/index.cshtml",
                menu: "Tenant.Tick.EmployeeAttendance"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Tick.EmployeeAttendance")) {
            $stateProvider.state("tenant.timeAttendanceReport", {
                url: "/tick-master-timeAttendanceReport",
                templateUrl: "~/App/tenant/views/tick/report/timeAttendanceReport.cshtml",
                menu: "Tenant.Tick.Report.TimeAttendanceReport"
            });
        }

        //Engage

        if (abp.auth.hasPermission("Pages.Tenant.Engage.GiftVoucherType")) {
            $stateProvider.state("tenant.giftvouchertype", {
                url: "/engage-giftvouchertype",
                templateUrl: "~/App/tenant/views/engage/giftvouchertype/index.cshtml",
                menu: "Tenant.Engage.GiftVoucherType"
            });

            $stateProvider.state("tenant.detailgiftvouchertype", {
                url: "/engage-detailgiftvouchertype/:id",
                templateUrl: "~/App/tenant/views/engage/giftvouchertype/detail.cshtml",
                menu: "Tenant.Engage.GiftVoucherType"
            });

            $stateProvider.state("tenant.searchVouchers", {
                url: "/engage-searchVouchers/:id",
                templateUrl: "~/App/tenant/views/engage/giftvouchertype/searchVouchers.cshtml",
                menu: "Tenant.Engage.GiftVoucherType"
            });

            $stateProvider.state("tenant.detailgiftvoucher", {
                url: "/engage-giftvoucher/:voucherTypeId/:voucherId",
                templateUrl: "~/App/tenant/views/engage/giftvouchertype/voucher.cshtml",
                menu: "Tenant.Engage.GiftVoucherType"
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Engage.Member")) {
            $stateProvider.state("tenant.member", {
                url: "/engage-member",
                templateUrl: "~/App/tenant/views/engage/member/index.cshtml",
                menu: "Tenant.Engage.Member"
            });
            $stateProvider.state("tenant.createoreditmember", {
                url: "/engage-createeditmember/:id",
                templateUrl: "~/App/tenant/views/engage/member/createedit.cshtml",
                menu: "Tenant.Engage.Member"
            });
            $stateProvider.state("tenant.memberdetails", {
                url: "/engage-memberdetails/:id",
                templateUrl: "~/App/tenant/views/engage/member/detail.cshtml",
                menu: "Tenant.Engage.Member"
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Engage.GiftVoucherCategory')) {
            $stateProvider.state('tenant.giftvouchercategory', {
                url: '/engage-giftvouchercategory',
                templateUrl: '~/App/tenant/views/engage/giftvouchercategory/index.cshtml',
                menu: 'Tenant.Engage.GiftVoucherCategory'
            });

            $stateProvider.state("tenant.detailgiftvouchercategory", {
                url: "/engage-detailgiftvouchercategory/:id",
                templateUrl: "~/App/tenant/views/engage/giftvouchercategory/detail.cshtml",
                menu: "Tenant.Engage.GiftVoucherCategory"
            });
        }
        //Wheel

        if (abp.auth.hasPermission("Pages.Tenant.Wheel.Dashboard")) {
            $stateProvider.state("tenant.wheeldashboard", {
                url: "/wheel-order-summary",
                templateUrl: "~/App/tenant/views/wheel/dashboard/index.cshtml",
                menu: "Tenant.Wheel.Dashboard"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Wheel.Order")) {
            $stateProvider.state("tenant.wheelorder", {
                url: "/wheel-order",
                templateUrl: "~/App/tenant/views/wheel/order/index.cshtml",
                menu: "Tenant.Wheel.Order"
            });
        }

        //Template

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.Template")) {
            $stateProvider.state("tenant.template", {
                url: "/house-master-template",
                templateUrl: "~/App/tenant/views/house/master/template/index.cshtml",
                menu: "Tenant.House.Master.Template"
            });
        }
        //Customer
        if (abp.auth.hasPermission("Pages.Tenant.House.Master.Customer")) {
            $stateProvider.state("tenant.customer", {
                url: "/house-master-customer",
                templateUrl: "~/App/tenant/views/house/master/customer/index.cshtml",
                menu: "Tenant.House.Master.Customer"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.CustomerMaterial")) {
            $stateProvider.state("tenant.customermaterial", {
                url: "/house-master-customermaterial",
                templateUrl: "~/App/tenant/views/house/master/customermaterial/index.cshtml",
                menu: "Tenant.House.Master.CustomerMaterial"
            });
        }

        // Customer Tag
        if (abp.auth.hasPermission("Pages.Tenant.House.Master.CustomerTagDefinition")) {
            $stateProvider.state("tenant.customertagdefinition", {
                url: "/house-master-customertagdefinition",
                templateUrl: "~/App/tenant/views/house/master/customertagdefinition/pricetagindex.cshtml",
                menu: "Tenant.House.Master.CustomerTagDefinition"
            });
        }

        // Customer Material Price Based On Customer Tag

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.CustomerTagMaterialPrice")) {
            $stateProvider.state("tenant.customertagmaterialprice", {
                url: "/house-master-customertagmaterialprice",
                templateUrl: "~/App/tenant/views/house/master/customertagmaterialprice/index.cshtml",
                menu: "Tenant.House.Master.CustomerTagMaterialPrice"
            });
        }

        //   Material Menu Mappings

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.MaterialMenuMapping")) {
            $stateProvider.state("tenant.materialmenumapping", {
                url: "/house-master-materialmenumapping/:filter,:menuselection",
                templateUrl: "~/App/tenant/views/house/master/materialmenumapping/index.cshtml",
                menu: "Tenant.House.Master.MaterialMenuMapping"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.MaterialMenuMapping")) {
            $stateProvider.state("tenant.materialmenumappingdetail", {
                url: "/house-master-materialmenumappingdetail/:id,:filter,:menuselection,:callingFrom,:materialRefId",
                templateUrl: "~/App/tenant/views/house/master/materialmenumapping/materialMenuMapping.cshtml",
                menu: "Tenant.House.Master.MaterialMenuMapping"
            });
        }

        //  Yield
        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Yield")) {
            $stateProvider.state("tenant.yield", {
                url: "/house-transaction-yield",
                templateUrl: "~/App/tenant/views/house/transaction/yield/index.cshtml",
                menu: "Tenant.House.Transaction.Yield"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Yield")) {
            $stateProvider.state("tenant.yielddetail", {
                url: "/house-transaction-yield/:id,:editMode",
                templateUrl: "~/App/tenant/views/house/transaction/yield/yield.cshtml",
                menu: "Tenant.House.Transaction.Yield"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Yield")) {
            $stateProvider.state("tenant.yieldprint", {
                url: "/yieldprint/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/yield/yieldPrint.cshtml",
                menu: "Tenant.House.Transaction.Yield"
            });
        }

        //  Production

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Production")) {
            $stateProvider.state("tenant.production", {
                url: "/house-transaction-production",
                templateUrl: "~/App/tenant/views/house/transaction/production/index.cshtml",
                menu: "Tenant.House.Transaction.Production"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Production")) {
            $stateProvider.state("tenant.productiondetail", {
                url: "/house-transaction-productiondetail/:id,:prodmethod",
                templateUrl: "~/App/tenant/views/house/transaction/production/production.cshtml",
                menu: "Tenant.House.Transaction.Production"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.Production")) {
            $stateProvider.state("tenant.productionprint", {
                url: "/house-transaction-productionprint/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/production/productionPrint.cshtml",
                menu: "Tenant.House.Transaction.Production"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.DayClose")) {
            $stateProvider.state("tenant.dayclose", {
                url: "/house-transaction-dayclose",
                templateUrl: "~/App/tenant/views/house/transaction/dayclose/dayclose.cshtml",
                menu: "Tenant.House.Transaction.DayClose"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.DayClose")) {
            $stateProvider.state("tenant.daycloseindex", {
                url: "/house-daycloseindex/:doesRunInBackGround,:backgroundTransactionDate,:remarks",
                templateUrl: "~/App/tenant/views/house/transaction/dayclose/daycloseindex.cshtml",
                menu: "Tenant.House.Transaction.DayClose"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.DayClose")) {
            $stateProvider.state("tenant.includemissingsales", {
                url: "/house-transaction-includeSalesIntoClosedLedger",
                templateUrl: "~/App/tenant/views/house/transaction/dayclose/salesMissingIntoDayClose.cshtml",
                menu: "Tenant.House.Transaction.DayClose"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.SupplierDocument")) {
            $stateProvider.state("tenant.supplierdocument", {
                url: "/house-transaction-supplierdocument",
                templateUrl: "~/App/tenant/views/house/transaction/supplierdocument/index.cshtml",
                menu: "Tenant.House.Transaction.SupplierDocument"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.ProductionUnit")) {
            $stateProvider.state("tenant.productionunit", {
                url: "/house-master-productionunit",
                templateUrl: "~/App/tenant/views/house/master/productionunit/index.cshtml",
                menu: "Tenant.House.Master.ProductionUnit"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Engage.Reservation")) {
            $stateProvider.state("tenant.reservation", {
                url: "/engage-reservation",
                templateUrl: "~/App/tenant/views/engage/reservation/index.cshtml",
                menu: "Tenant.Engage.Reservation"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Engage.Reservation")) {
            $stateProvider.state("tenant.reservationdetail", {
                url: "/engage-reservationdetail/:reservationId",
                templateUrl: "~/App/tenant/views/engage/reservation/reservation.cshtml",
                menu: "Tenant.Engage.Reservation"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.PurchaseReturn")) {
            $stateProvider.state("tenant.purchasereturn", {
                url: "/house-transaction-purchasereturn",
                templateUrl: "~/App/tenant/views/house/transaction/purchasereturn/index.cshtml",
                menu: "Tenant.House.Transaction.PurchaseReturn"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.PurchaseReturn")) {
            $stateProvider.state("tenant.purchasereturndetail", {
                url: "/house-transaction-purchasereturndetail/:id",
                templateUrl: "~/App/tenant/views/house/transaction/purchasereturn/purchasereturn.cshtml",
                menu: "Tenant.House.Transaction.PurchaseReturn"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.PurchaseReturn")) {
            $stateProvider.state("tenant.purchasereturnprint", {
                url: "/house-transaction-purchasereturnprint/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/purchasereturn/PoReturnPrint.cshtml",
                menu: "Tenant.House.Transaction.PurchaseReturn"
            });
        }
        //SalesOrder

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.SalesOrder")) {
            $stateProvider.state("tenant.salesorder", {
                url: "/house-transaction-salesorder",
                templateUrl: "~/App/tenant/views/house/transaction/customersales/salesorder/index.cshtml",
                menu: "Tenant.House.Transaction.SalesOrder"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.SalesOrder")) {
            $stateProvider.state("tenant.salesorderdetail", {
                url: "/house-transaction-salesorderdetail/:id,:autoSoFlag",
                templateUrl: "~/App/tenant/views/house/transaction/customersales/salesorder/salesOrder.cshtml",
                menu: "Tenant.House.Transaction.SalesOrder"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.SalesOrder")) {
            $stateProvider.state("tenant.salesorderprint", {
                url: "/house-transaction-salesorderprint/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/customersales/salesorder/SoPrint.cshtml",
                menu: "Tenant.House.Transaction.SalesOrder"
            });
        }

        // SalesDeliveryOrder

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.SalesDeliveryOrder")) {
            $stateProvider.state("tenant.salesdeliveryorder", {
                url: "/house-transaction-salesdeliveryorder",
                templateUrl: "~/App/tenant/views/house/transaction/customersales/salesdeliveryorder/index.cshtml",
                menu: "Tenant.House.Transaction.SalesDeliveryOrder"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.SalesDeliveryOrder")) {
            $stateProvider.state("tenant.salesdeliveryordermainform", {
                url: "/house-transaction-salesdeliveryordermainform/:id",
                templateUrl: "~/App/tenant/views/house/transaction/customersales/salesdeliveryorder/salesDoMainForm.cshtml",
                menu: "Tenant.House.Transaction.SalesDeliveryOrder"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.SalesDeliveryOrder")) {
            $stateProvider.state("tenant.salesdoprint", {
                url: "/house-transaction-salesdoprint/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/customersales/salesdeliveryorder/salesDoPrint.cshtml",
                menu: "Tenant.House.Transaction.SalesDeliveryOrder"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.SalesDeliveryOrder")) {
            $stateProvider.state("tenant.salesdoprint40col", {
                url: "/house-transaction-salesdoprint40col/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/customersales/salesdeliveryorder/revisedsalesDoPrint.cshtml",
                menu: "Tenant.House.Transaction.SalesDeliveryOrder"
            });
        }

        //SalesInvoice
        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.SalesInvoice")) {
            $stateProvider.state("tenant.salesinvoice", {
                url: "/house-transaction-salesinvoice",
                templateUrl: "~/App/tenant/views/house/transaction/customersales/salesinvoice/index.cshtml",
                menu: "Tenant.House.Transaction.SalesInvoice"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.SalesInvoice")) {
            $stateProvider.state("tenant.salesinvoicesummary", {
                url: "/house-transaction-salesinvoicesummary",
                templateUrl: "~/App/tenant/views/house/transaction/customersales/salesinvoice/salesinvoiceSummary.cshtml",
                menu: "Tenant.House.Transaction.SalesInvoice"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.SalesInvoice")) {
            $stateProvider.state("tenant.salesinvoicedetail", {
                url: "/house-transaction-salesinvoicedetail/:id,:salesinvoiceType",
                templateUrl: "~/App/tenant/views/house/transaction/customersales/salesinvoice/salesinvoiceDetail.cshtml",
                menu: "Tenant.House.Transaction.SalesInvoice"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.SalesInvoice")) {
            $stateProvider.state("tenant.salesinvoiceprintdetail", {
                url: "/house-transaction-salesinvoiceprint/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/customersales/salesinvoice/salesinvoicePrintDetail.cshtml",
                menu: "Tenant.House.Transaction.SalesInvoice"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.SalesInvoice")) {
            $stateProvider.state("tenant.salesinvoiceprintdetail40col", {
                url: "/house-transaction-salesinvoiceprintdetail40col/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/customersales/salesinvoice/smartinvoicePrintDetail.cshtml",
                menu: "Tenant.House.Transaction.SalesInvoice"
            });
        }

        //Closing stock

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.ClosingStock")) {
            $stateProvider.state("tenant.closingstockadjustment", {
                url: "/house-closingstock-adjustment",
                templateUrl: "~/App/tenant/views/house/transaction/adjustment/index.cshtml",
                menu: "Tenant.House.Transaction.ClosingStock",
                params: {
                    physicalReportFlag: false,
                    closingstockAdjustmentFlag: true
                },
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.ClosingStock")) {
            $stateProvider.state("tenant.closingstocktakenreport", {
                url: "/house-transaction-closingstocktakenreport",
                templateUrl: "~/App/tenant/views/house/transaction/closingstock/reportclosingstock.cshtml",
                menu: "Tenant.House.Transaction.ClosingStock"
            });
        }

        //if (abp.auth.hasPermission('Pages.Tenant.House.Transaction.ClosingStock')) {
        //    $stateProvider.state('tenant.closingstock', {
        //        url: '/house-transaction-closingstock',
        //        templateUrl: '~/App/tenant/views/house/transaction/closingstock/index.cshtml',
        //        menu: 'Tenant.House.Transaction.ClosingStock'
        //    });
        //}

        //if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.ClosingStock")) {
        //    $stateProvider.state("tenant.closingstockdetail", {
        //        url: "/house-transaction-closingstockdetail/:id",
        //        templateUrl: "~/App/tenant/views/house/transaction/closingstock/closingstockMainForm.cshtml",
        //        menu: "Tenant.House.Transaction.ClosingStock"
        //    });
        //}

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.ClosingStock")) {
            $stateProvider.state("tenant.closingstockprint", {
                url: "/house-transaction-closingstockprint/:printid",
                templateUrl: "~/App/tenant/views/house/transaction/closingstock/closingstockPrintDetail.cshtml",
                menu: "Tenant.House.Transaction.ClosingStock"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.ClosingStock")) {
            $stateProvider.state("tenant.mobileClosingstock", {
                url: "/house-transaction-mobileClosingstock",
                templateUrl: "~/App/tenant/views/house/transaction/closingstock/mobileIndex.cshtml",
                menu: "Tenant.House.Transaction.ClosingStock"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.ClosingStock")) {
            $stateProvider.state("tenant.mobileClosingstockdetail", {
                url: "/house-transaction-mobileClosingstockdetail/:id",
                templateUrl: "~/App/tenant/views/house/transaction/closingstock/mobileClosingstockMainForm.cshtml",
                menu: "Tenant.House.Transaction.ClosingStock"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.ClosingStock")) {
            $stateProvider.state("tenant.closingstockentry", {
                url: "/house-trn-closingstock",
                templateUrl: "~/App/tenant/views/house/transaction/closingstock/index.cshtml",
                menu: "Tenant.House.Transaction.ClosingStock"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.ClosingStock")) {
            $stateProvider.state("tenant.closingstockdetail", {
                url: "/house-trn-closingstockdetail/:id",
                templateUrl: "~/App/tenant/views/house/transaction/closingstock/closingstockMainForm.cshtml",
                menu: "Tenant.House.Transaction.ClosingStock"
            });
        }
        //menuitemwatage
        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.MenuItemWastage")) {
            $stateProvider.state("tenant.menuitemwastage", {
                url: "/house-transaction-menuitemwastage",
                templateUrl: "~/App/tenant/views/house/transaction/menuitemwastage/index.cshtml",
                menu: "Tenant.House.Transaction.MenuItemWastage"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Transaction.MenuItemWastage")) {
            $stateProvider.state("tenant.menuitemwastagedetail", {
                url: "/house-transaction-menuitemwastagedetail/:id",
                templateUrl: "~/App/tenant/views/house/transaction/menuitemwastage/menuitemwastageMainForm.cshtml",
                menu: "Tenant.House.Transaction.MenuItemWastage"
            });
        }

        // Swipe Dashboard
        if (abp.auth.hasPermission("Pages.Host.Swipe.Dashboard")) {
            $stateProvider.state("host.swipedashboard", {
                url: "/Swipe-dashboard",
                templateUrl: "~/App/host/views/swipe/dashboard/swipedashboard.cshtml",
                menu: "Host.Swipe.Dashboard"
            });
        }

        //Swipe Memeber
        if (abp.auth.hasPermission("Pages.Host.Swipe.Master.SwipeMember")) {
            $stateProvider.state("host.swipemember", {
                url: "/swipe-master-swipemember",
                templateUrl: "~/App/host/views/swipe/master/swipemember/index.cshtml",
                menu: "Host.Swipe.Master.SwipeMember"
            });
        }

        // Member Card
        if (abp.auth.hasPermission("Pages.Host.Swipe.Transaction.MemberCard")) {
            //$urlRouterProvider.otherwise("host.membercardview");
            $stateProvider.state("host.membercardview", {
                url: "/swipe-transaction-membercardview",
                templateUrl: "~/App/host/views/swipe/transaction/membercard/index.cshtml",
                menu: "Host.Swipe.Transaction.MemberCard"
            });
        }

        if (abp.auth.hasPermission("Pages.Host.Swipe.Transaction.MemberCard")) {
            $stateProvider.state("host.membercardedit", {
                url: "/swipe-transaction-membercardedit/:id,:shiftRefId",
                templateUrl: "~/App/host/views/swipe/transaction/membercard/memberCard.cshtml",
                menu: "Host.Swipe.Transaction.MemberCard"
            });
        }

        if (abp.auth.hasPermission("Pages.Host.Swipe.Transaction.MemberCard")) {
            $stateProvider.state("host.membercardbatchupdate", {
                url: "/swipe-transaction-membercardbatchupdate/:id,:shiftRefId,:batchStatus",
                templateUrl: "~/App/host/views/swipe/transaction/membercard/batchUpdate.cshtml",
                menu: "Host.Swipe.Transaction.MemberCard"
            });
        }

        if (abp.auth.hasPermission("Pages.Host.Swipe.Transaction.MemberCard")) {
            $stateProvider.state("host.membercardprint", {
                url: "/swipe-transaction-membercardprint/:id",
                templateUrl: "~/App/host/views/swipe/transaction/membercard/cardPrint.cshtml",
                menu: "Host.Swipe.Transaction.MemberCard"
            });
        }

        if (abp.auth.hasPermission("Pages.Host.Swipe.Master.SwipeCard")) {
            $stateProvider.state("host.swipecard", {
                url: "/swipe-master-swipecard",
                templateUrl: "~/App/host/views/swipe/master/swipecard/index.cshtml",
                menu: "Host.Swipe.Master.SwipeCard"
            });
        }

        if (abp.auth.hasPermission("Pages.Host.Swipe.Master.SwipeCardType")) {
            $stateProvider.state("host.swipecardtype", {
                url: "/swipe-master-swipecardtype",
                templateUrl: "~/App/host/views/swipe/master/swipecardtype/index.cshtml",
                menu: "Host.Swipe.Master.SwipeCardType"
            });
        }

        if (abp.auth.hasPermission("Pages.Host.Swipe.Master.SwipeCardType")) {
            $stateProvider.state("host.swipecardtypeedit", {
                url: "/swipe-master-swipecardtypeedit/:id",
                templateUrl: "~/App/host/views/swipe/master/swipecardtype/swipecardtype.cshtml",
                menu: "Host.Swipe.Master.SwipeCardType"
            });
        }

        if (abp.auth.hasPermission("Pages.Host.Swipe.Master.SwipePaymentType")) {
            $stateProvider.state("host.swipepaymenttype", {
                url: "/swipe-master-swipepaymenttype",
                templateUrl: "~/App/host/views/swipe/master/swipepaymenttype/index.cshtml",
                menu: "Host.Swipe.Master.SwipePaymentType"
            });
        }

        if (abp.auth.hasPermission("Pages.Host.Swipe.Transaction.SwipeShift")) {
            $stateProvider.state("host.swipeshift", {
                url: "/swipe-transaction-swipeshift",
                templateUrl: "~/App/host/views/swipe/transaction/swipeshift/index.cshtml",
                menu: "Host.Swipe.Transaction.SwipeShift"
            });
        }

        if (abp.auth.hasPermission("Pages.Host.Swipe.Transaction.SwipeShift")) {
            $stateProvider.state("host.swipeshiftedit", {
                url: "/swipe-transaction-swipeshiftedit/:id,:operationStatus",
                templateUrl: "~/App/host/views/swipe/transaction/swipeshift/swipeshift.cshtml",
                menu: "Host.Swipe.Transaction.SwipeShift"
            });
        }

        if (abp.auth.hasPermission("Pages.Host.Swipe.Transaction.SwipeShift")) {
            $stateProvider.state("host.reportswipeshift", {
                url: "/host.reportswipeshift",
                templateUrl: "~/App/host/views/swipe/reports/swipeShiftReport.cshtml",
                menu: "Host.Swipe.Transaction.SwipeShift"
            });
        }

        if (abp.auth.hasPermission("Pages.Host.Swipe.Transaction.SwipeShift")) {
            $stateProvider.state("host.reportswipeshifttransactiondetail", {
                url: "/host.reportswipeshifttransactiondetail",
                templateUrl: "~/App/host/views/swipe/reports/shiftDetailReport.cshtml",
                menu: "Host.Swipe.Transaction.SwipeShift"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.InventoryCycleTag")) {
            $stateProvider.state("tenant.inventorycycletag", {
                url: "/house-master-inventorycycletag",
                templateUrl: "~/App/tenant/views/house/master/inventorycycletag/index.cshtml",
                menu: "Tenant.House.Master.InventoryCycleTag"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.House.Master.InventoryCycleTag")) {
            $stateProvider.state("tenant.inventorycycletagedit", {
                url: "/house-master-inventorycycletagedit/:id",
                templateUrl: "~/App/tenant/views/house/master/inventorycycletag/inventoryCycleTag.cshtml",
                menu: "Tenant.House.Master.InventoryCycleTag"
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Book.Master.BookInvoice")) {
            $stateProvider.state("tenant.bookinvoice", {
                url: "/book-master-bookinvoice",
                templateUrl: "~/App/tenant/views/book/master/bookinvoice/index.cshtml",
                menu: "Tenant.Book.Master.BookInvoice"
            });
        }

        //  HR Start

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Master.SkillSet')) {
            $stateProvider.state('tenant.skillset', {
                url: '/hr-master-skillset',
                templateUrl: '~/App/tenant/views/hr/master/skillset/index.cshtml',
                menu: 'Tenant.Hr.Master.SkillSet'
            });
        }

        //PersonalInformation
        if (abp.auth.hasPermission('Pages.Tenant.Hr.Master.PersonalInformation')) {
            $stateProvider.state('tenant.personalinformation', {
                url: '/hr-master-personalinformation',
                templateUrl: '~/App/tenant/views/hr/master/personalinformation/index.cshtml',
                menu: 'Tenant.Hr.Master.PersonalInformation'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Master.PersonalInformation')) {
            $stateProvider.state('tenant.personalinformationdetail', {
                url: '/hr-master-personalinformation/:id,:cloneFlag',
                templateUrl: '~/App/tenant/views/hr/master/personalinformation/employee.cshtml',
                menu: 'Tenant.Hr.Master.PersonalInformation'
            });
        }

        //Employee SkillSet

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Master.EmployeeSkillSet')) {
            $stateProvider.state('tenant.employeeskillset', {
                url: '/hr-master-employeeskillset',
                templateUrl: '~/App/tenant/views/hr/master/employeeskillset/index.cshtml',
                menu: 'Tenant.Hr.Master.EmployeeSkillSet'
            });
        }

        // Document Info

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Master.DocumentInfo')) {
            $stateProvider.state('tenant.documentinfo', {
                url: '/hr-master-documentinfo',
                templateUrl: '~/App/tenant/views/hr/master/documentinfo/index.cshtml',
                menu: 'Tenant.Hr.Master.DocumentInfo'
            });
        }

        //EmployeeDocumentInfo

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Master.EmployeeDocumentInfo') || ('Pages.Tenant.Hr.Employee.EmployeeDocumentView')) {
            $stateProvider.state('tenant.employeedocumentinfo', {
                url: '/hr-master-employeedocumentinfo',
                templateUrl: '~/App/tenant/views/hr/master/employeedocumentinfo/index.cshtml',
                menu: 'Tenant.Hr.Employee.EmployeeDocumentView',
                params: {
                    loginStatus: 'HR',
                },
            });
        }

        //LeaveType

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Master.LeaveType')) {
            $stateProvider.state('tenant.leavetype', {
                url: '/hr-master-leavetype',
                templateUrl: '~/App/tenant/views/hr/master/leavetype/index.cshtml',
                menu: 'Tenant.Hr.Master.LeaveType'
            });
        }

        //LeaveRequest Print
        if (abp.auth.hasPermission('Pages.Tenant.Hr.Master.LeaveRequest')) {
            $stateProvider.state('tenant.leaverequestprint', {
                url: '/hr-master-leaverequestprint/:printid',
                templateUrl: '~/App/tenant/views/hr/transaction/leaverequest/leaveRequestPrint.cshtml',
                menu: 'Tenant.Hr.Master.LeaveRequest'
            });
        }

        //LeaveRequest
        if (abp.auth.hasPermission('Pages.Tenant.Hr.Master.LeaveRequest')) {
            $stateProvider.state('tenant.leaverequest', {
                url: '/hr-master-leaverequest',
                templateUrl: '~/App/tenant/views/hr/master/leaverequest/index.cshtml',
                menu: 'Tenant.Hr.Master.LeaveRequest',
                params: {
                    loginStatus: 'HR',
                },
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Master.LeaveRequest')
            || abp.auth.hasPermission('Pages.Tenant.Hr.Employee.EmployeeLeaveRequest')) {
            $stateProvider.state('tenant.leaverequestdetail', {
                url: '/hr-master-leaverequest/:id',
                templateUrl: '~/App/tenant/views/hr/master/leaverequest/createOrEditModal.cshtml',
                menu: 'Tenant.Hr.Master.LeaveRequest',
                params: {
                    loginStatus: 'HR',
                },
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Master.WorkDay')) {
            $stateProvider.state('tenant.hrworkday', {
                url: '/hrworkday',
                templateUrl: '~/App/tenant/views/hr/master/workday/index.cshtml',
                menu: 'Tenant.Hr.Master.WorkDay'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Master.PublicHoliday')) {
            $stateProvider.state('tenant.publicholiday', {
                url: '/hr-master-publicholiday',
                templateUrl: '~/App/tenant/views/hr/master/publicholiday/index.cshtml',
                menu: 'Tenant.Hr.Master.PublicHoliday'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Employee.EmployeeLeaveRequest')) {
            $stateProvider.state('tenant.employeeleaverequestdetail', {
                url: '/hr-master-employeeleaverequestdetail/:id',
                templateUrl: '~/App/tenant/views/hr/transaction/leaverequest/employeeLeaveRequest.cshtml',
                menu: 'Tenant.Hr.Employee.EmployeeLeaveRequest',
                params: {
                    loginStatus: 'EMP',
                },
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Master.DcHeadMaster')) {
            $stateProvider.state('tenant.dcheadmaster', {
                url: '/hr-master-dcheadmaster',
                templateUrl: '~/App/tenant/views/hr/master/dcheadmaster/index.cshtml',
                menu: 'Tenant.Hr.Master.DcHeadMaster'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Master.DcGroupMaster')) {
            $stateProvider.state('tenant.dcgroupmaster', {
                url: '/hr-master-dcgroupmaster',
                templateUrl: '~/App/tenant/views/hr/master/dcgroupmaster/index.cshtml',
                menu: 'Tenant.Hr.Master.DcGroupMaster'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Master.DcStationMaster')) {
            $stateProvider.state('tenant.dcstationmaster', {
                url: '/hr-master-dcstationmaster',
                templateUrl: '~/App/tenant/views/hr/master/dcstationmaster/index.cshtml',
                menu: 'Tenant.Hr.Master.DcStationMaster'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Master.DcShiftMaster')) {
            $stateProvider.state('tenant.dcshiftmaster', {
                url: '/hr-master-dcshiftmaster',
                templateUrl: '~/App/tenant/views/hr/master/dcshiftmaster/index.cshtml',
                menu: 'Tenant.Hr.Master.DcShiftMaster'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.DutyChart')) {
            $stateProvider.state('tenant.dutychart', {
                url: '/hr-transaction-dutychart',
                templateUrl: '~/App/tenant/views/hr/transaction/dutychart/index.cshtml',
                menu: 'Tenant.Hr.Transaction.DutyChart'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.DutyChart')) {
            $stateProvider.state('tenant.manpower', {
                url: '/hr-transaction-manpower',
                templateUrl: '~/App/tenant/views/hr/transaction/dutychart/manpower.cshtml',
                menu: 'Tenant.Hr.Transaction.DutyChart'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.DutyChart')) {
            $stateProvider.state('tenant.dutychartdetail', {
                url: '/hr-transaction-manpower/:id,:dutychartdate,:hrErrorExists',
                templateUrl: '~/App/tenant/views/hr/transaction/dutychart/dutyApproval.cshtml',
                menu: 'Tenant.Hr.Transaction.DutyChart'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.IncentiveCategory')) {
            $stateProvider.state('tenant.incentivecategory', {
                url: '/hr-transaction-incentivecategory',
                templateUrl: '~/App/tenant/views/hr/transaction/incentivecategory/index.cshtml',
                menu: 'Tenant.Hr.Transaction.IncentiveCategory'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.IncentiveTag')) {
            $stateProvider.state('tenant.incentivetag', {
                url: '/hr-transaction-incentivetag',
                templateUrl: '~/App/tenant/views/hr/transaction/incentivetag/index.cshtml',
                menu: 'Tenant.Hr.Transaction.IncentiveTag'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.IncentiveTag')) {
            $stateProvider.state('tenant.incentivetagedit', {
                url: '/hr-transaction-incentivetagedit/:id',
                templateUrl: '~/App/tenant/views/hr/transaction/incentivetag/incentiveTag.cshtml',
                menu: 'Tenant.Hr.Transaction.IncentiveTag'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.ManualIncentive')) {
            $stateProvider.state('tenant.manualincentive', {
                url: '/hr-transaction-manualincentive',
                templateUrl: '~/App/tenant/views/hr/transaction/manualincentive/index.cshtml',
                menu: 'Tenant.Hr.Transaction.ManualIncentive'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Master.SalaryInfo')) {
            $stateProvider.state('tenant.salaryinfo', {
                url: '/hr-master-salaryinfo/:passwordRequired',
                templateUrl: '~/App/tenant/views/hr/master/salaryinfo/salaryindex.cshtml',
                menu: 'Tenant.Hr.Master.SalaryInfo'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Master.SalaryInfo')) {
            $stateProvider.state('tenant.salaryinfodetail', {
                url: '/hr-salaryinfodetail/:id,:cloneFlag',
                templateUrl: '~/App/tenant/views/hr/master/salaryinfo/salaryInfo.cshtml',
                menu: 'Tenant.Hr.Master.SalaryInfo'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Hr.Transaction.DutyChart')) {
            $stateProvider.state('tenant.salaryCost', {
                url: '/salaryCost',
                templateUrl: '~/App/tenant/views/hr/transaction/dutychart/salaryCost.cshtml',
                menu: 'Tenant.Hr.Master.SalaryInfo'
            });
        }

        //Employee DashBoard
        if (abp.auth.hasPermission('Pages.Tenant.EmployeeDashboard')) {
            //$urlRouterProvider.otherwise("/tenant/employeedashboard"); //Entrance page for a tenant
            $stateProvider.state('tenant.employeedashboard', {
                url: '/employeedashboard',
                templateUrl: '~/App/tenant/views/hr/transaction/employeedashboard/employeeDashboard.cshtml',
                menu: 'Tenant.EmployeeDashboard',
                params: {
                    loginStatus: 'EMP',
                },
            });
        }

        //Go
        if (abp.auth.hasPermission('Pages.Tenant.Go.DineGoBrand')) {
            $stateProvider.state('tenant.dinegobrand', {
                url: '/go-dinegobrand',
                templateUrl: '~/App/tenant/views/go/brand/index.cshtml',
                menu: 'Tenant.Go.DineGoBrand'
            });
            $stateProvider.state('tenant.detaildinegobrand', {
                url: '/go-detaildinegobrand-detail/:id',
                templateUrl: '~/App/tenant/views/go/brand/detail.cshtml',
                menu: 'Tenant.Go.DineGoBrand'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Go.DineGoDevice')) {
            $stateProvider.state('tenant.dinegodevice', {
                url: '/go-dinegodevice',
                templateUrl: '~/App/tenant/views/go/device/index.cshtml',
                menu: 'Tenant.Go.DineGoDevice'
            });
            $stateProvider.state('tenant.detaildinegodevice', {
                url: '/go-dinegodevice-detail/:id',
                templateUrl: '~/App/tenant/views/go/device/detail.cshtml',
                menu: 'Tenant.Go.DineGoDevice'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Go.DineGoDepartment')) {
            $stateProvider.state('tenant.dinegodepartment', {
                url: '/go-dinegodepartment',
                templateUrl: '~/App/tenant/views/go/department/index.cshtml',
                menu: 'Tenant.Go.DineGoDepartment'
            });
            $stateProvider.state('tenant.detaildinegodepartment', {
                url: '/go-dinegodepartment-detail/:id',
                templateUrl: '~/App/tenant/views/go/department/detail.cshtml',
                menu: 'Tenant.Go.DineGoDepartment'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Go.DineGoPaymentType')) {
            $stateProvider.state('tenant.dinegopaymenttype', {
                url: '/go-dinegopaymenttype',
                templateUrl: '~/App/tenant/views/go/paymenttype/index.cshtml',
                menu: 'Tenant.Go.DineGoPaymentType'
            });

            $stateProvider.state("tenant.detaildinegopaymenttype", {
                url: "/go-dinegopaymenttype-detail/:id",
                templateUrl: "~/App/tenant/views/go/paymenttype/detail.cshtml",
                menu: "Tenant.Go.DineGoPaymentType"
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Go.DineGoCharge')) {
            $stateProvider.state('tenant.dinegopaymenttypecharge', {
                url: '/go-dinegopaymenttypecharge',
                templateUrl: '~/App/tenant/views/go/charge/index.cshtml',
                menu: 'Tenant.Go.DineGoCharge'
            });
            $stateProvider.state('tenant.detaildinegopaymenttypecharge', {
                url: '/go-dinegopaymenttypecharge-detail/:id',
                templateUrl: '~/App/tenant/views/go/charge/detail.cshtml',
                menu: 'Tenant.Go.DineGoCharge'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Play.Resolution')) {
            $stateProvider.state('tenant.dineplayresolutions', {
                url: '/play-resolutions/',
                templateUrl: '~/App/tenant/views/play/resolutions/index.cshtml',
                menu: 'Tenant.Play.DinePlaySolutions'
            });

            $stateProvider.state('tenant.dineplayresolutiondetail', {
                url: '/play-resolutions-detail/:id',
                templateUrl: '~/App/tenant/views/play/resolutions/detail.cshtml',
                menu: 'Tenant.Play.DinePlaySolutions'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Play.DisplayGroup')) {
            $stateProvider.state('tenant.dineplaydisplaygroups', {
                url: '/play-displaygroups/',
                templateUrl: '~/App/tenant/views/play/displaygroups/index.cshtml',
                menu: 'Tenant.Play.DinePlayDisplayGroups'
            });
            $stateProvider.state('tenant.dineplaydisplaygroupsdetail', {
                url: '/play-displaygroups-detail/:id',
                templateUrl: '~/App/tenant/views/play/displaygroups/detail.cshtml',
                menu: 'Tenant.Play.DinePlayDisplayGroups'
            });
            $stateProvider.state('tenant.dineplaydisplaygroupsselector', {
                url: '/play-displaygroups-selector/',
                templateUrl: '~/App/tenant/views/play/displaygroups/selectDisplayGroups.cshtml',
                menu: 'Tenant.Play.DinePlayDisplayGroups'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Play.Display')) {
            $stateProvider.state('tenant.dineplaydisplays', {
                url: '/play-displays/',
                templateUrl: '~/App/tenant/views/play/displays/index.cshtml',
                menu: 'Tenant.Play.Tenant.Play.DinePlayDisplays'
            });
            $stateProvider.state('tenant.dineplaydisplaysdetail', {
                url: '/play-displays-detail/:id',
                templateUrl: '~/App/tenant/views/play/displays/detail.cshtml',
                menu: 'Tenant.Play.Tenant.Play.DinePlayDisplays'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Play.DayParting')) {
            $stateProvider.state('tenant.dineplaydayparting', {
                url: '/play-dayparting/',
                templateUrl: '~/App/tenant/views/play/dayparting/index.cshtml',
                menu: 'Tenant.Play.DinePlayDayParting'
            });

            $stateProvider.state('tenant.dineplaydaypartingdetail', {
                url: '/play-dayparting-detail/:id',
                templateUrl: '~/App/tenant/views/play/dayparting/detail.cshtml',
                menu: 'Tenant.Play.DinePlayDayParting'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Play.Layout')) {
            $stateProvider.state('tenant.dineplaylayouts', {
                url: '/play-layouts/',
                templateUrl: '~/App/tenant/views/play/layouts/index.cshtml',
                menu: 'Tenant.Play.DinePlayLayouts'
            });
            $stateProvider.state('tenant.dineplaylayoutsdetail', {
                url: '/play-layouts-detail/:id',
                templateUrl: '~/App/tenant/views/play/layouts/detail.cshtml',
                menu: 'Tenant.Play.DinePlayLayouts'
            });
            $stateProvider.state('tenant.dineplaylayoutscontent', {
                url: '/play-layouts-content/:id',
                templateUrl: '~/App/tenant/views/play/layouts/content.cshtml',
                menu: 'Tenant.Play.DinePlayLayouts'
            });
            $stateProvider.state('tenant.dineplaylayoutspreview', {
                url: '/play-layouts-preview/:id/:src',
                templateUrl: '~/App/tenant/views/play/layouts/preview.cshtml',
                menu: 'Tenant.Play.DinePlayLayouts'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Play.Schedule')) {
            $stateProvider.state('tenant.dineplayschedules', {
                url: '/play-schedules/:src',
                templateUrl: '~/App/tenant/views/play/schedules/index.cshtml',
                menu: 'Tenant.Play.DinePlaySchedules'
            });
            $stateProvider.state('tenant.dineplayscheduleevent', {
                url: '/play-schedules-event/:id/:src',
                templateUrl: '~/App/tenant/views/play/schedules/event.cshtml',
                menu: 'Tenant.Play.DinePlaySchedules'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggLocationGroup')) {
            $stateProvider.state('tenant.delagglocationgroup', {
                url: '/cluster-delagglocationgroup',
                templateUrl: '~/App/tenant/views/cluster/delagglocationgroup/indexdelagglocationgroup.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggLocationGroup'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggLocationGroup')) {
            $stateProvider.state('tenant.delagglocationgroupcreate', {
                url: '/cluster-delagglocationgroupcreate/:id',
                templateUrl: '~/App/tenant/views/cluster/delagglocationgroup/delagglocationgroup.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggLocationGroup'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggLocation')) {
            $stateProvider.state('tenant.delagglocation', {
                url: '/cluster-delagglocation',
                templateUrl: '~/App/tenant/views/cluster/delagglocation/indexdelagglocation.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggLocation'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggLocation')) {
            $stateProvider.state('tenant.delagglocationcreate', {
                url: '/cluster-delagglocationcreate/:id',
                templateUrl: '~/App/tenant/views/cluster/delagglocation/delagglocation.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggLocation'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggLocMapping')) {
            $stateProvider.state('tenant.delagglocmapping', {
                url: '/cluster-delagglocmapping',
                templateUrl: '~/App/tenant/views/cluster/delagglocmapping/indexdelagglocmapping.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggLocMapping'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggLocMapping')) {
            $stateProvider.state('tenant.delagglocmappingcreate', {
                url: '/cluster-delagglocmappingcreate/:id',
                templateUrl: '~/App/tenant/views/cluster/delagglocmapping/delagglocmapping.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggLocMapping'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggLocationItem')) {
            $stateProvider.state('tenant.delagglocationitem', {
                url: '/cluster-delagglocationitem',
                templateUrl: '~/App/tenant/views/cluster/delagglocationitem/indexdelagglocationitem.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggLocationItem'
            });
            $stateProvider.state('tenant.delagglocationitem1', {
                url: '/cluster-delagglocationitem1',
                templateUrl: '~/App/tenant/views/cluster/delagglocationitem1/indexdelagglocationitem.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggLocationItem'
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Cluster.Master.DelAggLocationItem")) {
            $stateProvider.state("tenant.delagglocationitemcreate", {
                url: "/cluster-delagglocationitemcreate/:id",
                templateUrl: '~/App/tenant/views/cluster/delagglocationitem/delagglocationitem.cshtml',
                menu: "Pages.Tenant.Cluster.Master.DelAggLocationItem"
            });

            $stateProvider.state("tenant.delagglocationitemcreate1", {
                url: "/cluster-delagglocationitemcreate1/:id",
                templateUrl: '~/App/tenant/views/cluster/delagglocationitem1/delagglocationitem.cshtml',
                menu: "Pages.Tenant.Cluster.Master.DelAggLocationItem"
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggVariantGroup')) {
            $stateProvider.state('tenant.delaggvariantgroup', {
                url: '/cluster-delaggvariantgroup',
                templateUrl: '~/App/tenant/views/cluster/delaggvariantgroup/indexdelaggvariantgroup.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggVariantGroup'
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Cluster.Master.DelAggVariantGroup")) {
            $stateProvider.state("tenant.delaggvariantgroupdetail", {
                url: "/cluster-delaggvariantgroupdetail/:id",
                templateUrl: '~/App/tenant/views/cluster/delaggvariantgroup/delaggvariantgroup.cshtml',
                menu: "Pages.Tenant.Cluster.Master.DelAggVariantGroup"
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggModifierGroup')) {
            $stateProvider.state('tenant.delaggmodifiergroup', {
                url: '/cluster-delaggmodifiergroup',
                templateUrl: '~/App/tenant/views/cluster/delaggmodifiergroup/indexdelaggmodifiergroup.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggModifierGroup'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggModifierGroup')) {
            $stateProvider.state('tenant.delaggmodifiergroupdetail', {
                url: '/cluster-delaggmodifiergroupdetail/:id',
                templateUrl: '~/App/tenant/views/cluster/delaggmodifiergroup/delaggmodifiergroup.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggModifierGroup'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggVariant')) {
            $stateProvider.state('tenant.delaggvariant', {
                url: '/cluster-delaggvariant',
                templateUrl: '~/App/tenant/views/cluster/delaggvariant/indexdelaggvariant.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggVariant'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggVariant')) {
            $stateProvider.state('tenant.delaggvariantcreate', {
                url: '/cluster-delaggvariantcreate/:id',
                templateUrl: '~/App/tenant/views/cluster/delaggvariant/delaggvariant.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggVariant'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggModifier')) {
            $stateProvider.state('tenant.delaggmodifier', {
                url: '/cluster-delaggmodifier',
                templateUrl: '~/App/tenant/views/cluster/delaggmodifier/indexdelaggmodifier.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggModifier'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggModifier')) {
            $stateProvider.state('tenant.delaggmodifiercreate', {
                url: '/cluster-delaggmodifiercreate/:id',
                templateUrl: '~/App/tenant/views/cluster/delaggmodifier/delaggmodifier.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggModifier'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggItem')) {
            $stateProvider.state('tenant.delaggitem', {
                url: '/cluster-master-delaggitem',
                templateUrl: '~/App/tenant/views/cluster/delaggitem/indexdelaggitem.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggItem'
            });
        }
        if (abp.auth.hasPermission("Pages.Tenant.Cluster.Master.DelAggItem")) {
            $stateProvider.state("tenant.delaggitemdetail", {
                url: "/cluster-master-delaggitemdetail/:id,:delAggCatId,:categoryFlag",
                templateUrl: '~/App/tenant/views/cluster/delaggitem/delaggitem.cshtml',
                menu: "Tenant.Cluster.Master.DelAggItem"
            });
        }
        //DelAggItemGroup routes
        if (abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggItemGroup')) {
            $stateProvider.state('tenant.delaggitemgroup', {
                url: '/cluster-master-delaggitemgroup',
                templateUrl: '~/App/tenant/views/cluster/delaggitemgroup/indexdelaggitemgroup.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggItemGroup'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggItemGroup')) {
            $stateProvider.state('tenant.delaggitemgroupcreate', {
                url: '/cluster-master-delaggitemgroupcreate/:id',
                templateUrl: '~/App/tenant/views/cluster/delaggitemgroup/delaggitemgroup.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggItemGroup'
            });
        }

        //DelAggTax routes
        if (abp.auth.hasPermission('Pages.Tenant.Cluster.Master.DelAggTax')) {
            $stateProvider.state('tenant.delaggtax', {
                url: '/cluster-master-delaggtax',
                templateUrl: '~/App/tenant/views/cluster/delaggtax/indexdelaggtax.cshtml',
                menu: 'Tenant.Cluster.Master.DelAggTax'
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Cluster.Master.DelAggTax")) {
            $stateProvider.state("tenant.delaggtaxdetail", {
                url: "/cluster-master-delaggtaxdetail/:id",
                templateUrl: '~/App/tenant/views/cluster/delaggtax/delaggtax.cshtml',
                menu: "Tenant.Cluster.Master.DelAggTax"
            });
        }
        //DelTimingGroup routes
        if (abp.auth.hasPermission('Pages.Tenant.Cluster.DelTimingGroup')) {
            $stateProvider.state('tenant.deltiminggroup', {
                url: '/cluster-deltiminggroup',
                templateUrl: '~/App/tenant/views/cluster/deltiminggroup/indexdeltiminggroup.cshtml',
                menu: 'Tenant.Cluster.DelTimingGroup'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Cluster.DelTimingGroup')) {
            $stateProvider.state('tenant.deltiminggroupdetail', {
                url: '/cluster-deltiminggroupdetail/:id',
                templateUrl: '~/App/tenant/views/cluster/deltiminggroup/deltiminggroup.cshtml',
                menu: 'Tenant.Cluster.DelTimingGroup'
            });
        }
        //DelAggCategory routes
        if (abp.auth.hasPermission('Pages.Tenant.Cluster.DelAggCategory')) {
            $stateProvider.state('tenant.virtualdelaggcategory', {
                url: '/cluster-virtualdelaggcategory/:delAggCategoryId',
                templateUrl: '~/App/tenant/views/cluster/delaggcategory/virtualdelaggcategory.cshtml',
                menu: 'Tenant.Cluster.DelAggCategory'
            });
        }

        //if (abp.auth.hasPermission('Pages.Tenant.Cluster.DelAggCategory')) {
        //    $stateProvider.state('tenant.delaggcategory', {
        //        url: '/cluster-delaggcategory',
        //        templateUrl: '~/App/tenant/views/cluster/delaggcategory/indexdelaggcategory.cshtml',
        //        menu: 'Tenant.Cluster.DelAggCategory'
        //    });
        //}

        if (abp.auth.hasPermission('Pages.Tenant.Cluster.DelAggCategory')) {
            $stateProvider.state('tenant.delaggcategorycreate', {
                url: '/cluster-delaggcategorycreate/:id',
                templateUrl: '~/App/tenant/views/cluster/delaggcategory/delaggcategory.cshtml',
                menu: 'Tenant.Cluster.DelAggCategory'
            });
        }

        if (abp.auth.hasPermission("Pages.Administration.Languages")) {
            $stateProvider.state("delaggLanguages", {
                url: "/delagg-languages",
                templateUrl: "~/App/tenant/views/cluster/delaggLanguages/indexdelaggLanguages.cshtml",
                menu: "Administration.DelAggLanguages"
            });

            if (abp.auth.hasPermission("Pages.Administration.Languages.ChangeTexts")) {
                $stateProvider.state("delaggLanguageTexts", {
                    url: "/delagg-languages/texts/:languageName?filterText",
                    templateUrl: "~/App/tenant/views/cluster/delaggLanguages/textsdelaggLanguages.cshtml",
                    menu: "Administration.DelAggLanguages"
                });
            }
        }

        //DelAggCharge routes
        if (abp.auth.hasPermission('Pages.Tenant.Cluster.DelAggCharge')) {
            $stateProvider.state('tenant.delaggcharge', {
                url: '/cluster-delaggcharge',
                templateUrl: '~/App/tenant/views/cluster/delaggcharge/indexdelaggcharge.cshtml',
                menu: 'Tenant.Cluster.DelAggCharge'
            });
        }

        if (abp.auth.hasPermission("Pages.Tenant.Cluster.DelAggCharge")) {
            $stateProvider.state("tenant.delaggchargedetail", {
                url: "/cluster-delaggchargedetail/:id",
                templateUrl: '~/App/tenant/views/cluster/delaggcharge/delaggcharge.cshtml',
                menu: "Pages.Tenant.Cluster.DelAggCharge"
            });
        }


        if (abp.auth.hasPermission('Pages.Tenant.Cluster.DelAggImage')) {
            $stateProvider.state('tenant.delaggimage', {
                url: '/cluster-delaggimage',
                templateUrl: '~/App/tenant/views/cluster/delaggimage/indexdelaggimage.cshtml',
                menu: 'Tenant.Cluster.DelAggImage'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Cluster.DelAggImage')) {
            $stateProvider.state('tenant.delaggimagecreate', {
                url: '/cluster-delaggimagecreate/:id',
                templateUrl: '~/App/tenant/views/cluster/delaggimage/delaggimage.cshtml',
                menu: 'Tenant.Cluster.DelAggImage'
            });
        }

    }
]);
appModule.run([
    "$rootScope", "settings", "$state", function ($rootScope, settings, $state) {
        $rootScope.$state = $state;
        $rootScope.$settings = settings;

        $rootScope.safeApply = function (fn) {
            var phase = this.$root.$$phase;
            if (phase == "$apply" || phase == "$digest") {
                if (fn && (typeof (fn) === "function")) {
                    fn();
                }
            } else {
                this.$apply(fn);
            }
        };
    }
]);