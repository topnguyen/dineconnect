﻿using DinePlan.DineConnect.Web.Bundling;
using System.Web.Optimization;

namespace DinePlan.DineConnect.Web.App.Startup
{
    public static class AppBundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //LIBRARIES

            AddAppCssLibs(bundles, isRTL: false);
            AddAppCssLibs(bundles, isRTL: true);

            bundles.Add(
                new ScriptBundle("~/Bundles/App/libs/js")
                    .Include(
                        ScriptPaths.Json2,
                        ScriptPaths.JQuery,
                        ScriptPaths.JQuery_UI,
                        ScriptPaths.JQuery_Migrate,
                        ScriptPaths.Bootstrap,
                        ScriptPaths.Bootstrap_Hover_Dropdown,
                        ScriptPaths.JQuery_Slimscroll,
                        ScriptPaths.JQuery_BlockUi,
                        ScriptPaths.JQuery_Cookie,
                        ScriptPaths.JQuery_Uniform,
                        ScriptPaths.JQuery_FileUpload,
                        ScriptPaths.SignalR,
                        ScriptPaths.JQuery_Sparkline,
                        ScriptPaths.JsTree,
                        ScriptPaths.Bootstrap_Switch,
                        ScriptPaths.SpinJs,
                        ScriptPaths.SpinJs_JQuery,
                        ScriptPaths.SweetAlert,
                        ScriptPaths.Toastr,
                        ScriptPaths.MomentJs,
                        ScriptPaths.Bootstrap_DateRangePicker,
                        ScriptPaths.Bootstrap_DateTimePicker,
                        ScriptPaths.Bootstrap_FileInput,
                        ScriptPaths.Bootstrap_Select,
                        ScriptPaths.Underscore,
                        ScriptPaths.Angular,
                        ScriptPaths.Angular_Sanitize,
                        ScriptPaths.Angular_Touch,
                        ScriptPaths.Angular_Ui_Router,
                        ScriptPaths.Angular_Ui_Utils,
                        ScriptPaths.Angular_Ui_Bootstrap_Tpls,
                        ScriptPaths.Angular_Ui_Grid,
                        ScriptPaths.Angular_Ui_Select,
                        ScriptPaths.Angular_OcLazyLoad,
                        ScriptPaths.Angular_File_Upload,
                        ScriptPaths.Angular_File_Thumb,
                        ScriptPaths.Angular_DateRangePicker,
                        ScriptPaths.Angular_Moment,
                        ScriptPaths.Angular_Ui_MultiDate,
                        ScriptPaths.Angular_Bootstrap_Switch,
                        ScriptPaths.Abp,
                        ScriptPaths.Abp_JQuery,
                        ScriptPaths.Abp_Toastr,
                        ScriptPaths.Abp_BlockUi,
                        ScriptPaths.Abp_SpinJs,
                        ScriptPaths.Abp_SweetAlert,
                        ScriptPaths.Abp_Angular,
                        ScriptPaths.HighCharts,
                        ScriptPaths.TimeLine,
                        /*QueryBuilder*/
                        ScriptPaths.QueryBuilderDirective,
                        ScriptPaths.QueryBuilderExtender,
                        ScriptPaths.QueryBuilderDot,
                        ScriptPaths.QueryBuilder_Selectize,
                        ScriptPaths.QueryBuilder,

                        /*WSYIWYG EDITOR*/
                        ScriptPaths.Summernote,
                        ScriptPaths.AnSummernote,

                        ScriptPaths.MultiSelectList,
                        ScriptPaths.jsPdf,
                        ScriptPaths.Angular_Index_DB,
                        ScriptPaths.Angular_AutoComplete,
                        ScriptPaths.Angular_MouseTrap,
                        ScriptPaths.Angular_HotKey,
                        ScriptPaths.Angular_Drag,
                        ScriptPaths.Angular_ChartJs,
                        ScriptPaths.Angular_ChartJsLib,
                        ScriptPaths.Angular_Bootstrap_Datetimepicker,
                        ScriptPaths.Angular_Bootstrap_Datetimeinput,
                        ScriptPaths.Angular_Bootstrap_validate,
                        ScriptPaths.Angular_Bootstrap_Datetimepicker_Template,
                        ScriptPaths.Time_Ranger,
                        ScriptPaths.NgTable,
                        ScriptPaths.Angular_ColorPicker,
                        ScriptPaths.Angular_Sortable,
                        ScriptPaths.Angular_Device_Detector,
                        ScriptPaths.Angular_Re_Tree,
                        ScriptPaths.Angualr_DateTimeRanger,
                        ScriptPaths.Calendar,
                        ScriptPaths.FullCalendar,
                        ScriptPaths.NgTagsInput,
                        ScriptPaths.Quill,
                        ScriptPaths.NgQuill,
                        ScriptPaths.Ui_Sortable,
                        ScriptPaths.NgFileUpload,
                        ScriptPaths.Angular_Translate,
                        ScriptPaths.File_Manager,
                        ScriptPaths.AngularFormlyApi,
                        ScriptPaths.AngularFormly,
                        ScriptPaths.AngularFormlyTemplates,

                        //GrapesJs
                        ScriptPaths.GrapesJs
                    ).ForceOrdered()
                );

            //METRONIC

            AddAppMetrinicCss(bundles, isRTL: false);
            AddAppMetrinicCss(bundles, isRTL: true);

            bundles.Add(
              new ScriptBundle("~/Bundles/App/metronic/js")
                  .Include(
                      "~/metronic/assets/global/scripts/app.js",
                      "~/metronic/assets/admin/layout4/scripts/layout.js"
                  ).ForceOrdered()
              );

            //APPLICATION

            bundles.Add(
                new StyleBundle("~/Bundles/App/css")
                    .IncludeDirectory("~/App", "*.css", true)
                    .ForceOrdered()
                );

            bundles.Add(
                new ScriptBundle("~/Bundles/App/js")
                    .IncludeDirectory("~/App", "*.js", true)
                    .ForceOrdered()
                );

            bundles.Add(
               new ScriptBundle("~/Bundles/AppOutlet/js")
                   .IncludeDirectory("~/AppOutlet", "*.js", true)
                   .ForceOrdered()
               );

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
        }

        private static void AddAppCssLibs(BundleCollection bundles, bool isRTL)
        {
            bundles.Add(
                new StyleBundle("~/Bundles/App/libs/css" + (isRTL ? "RTL" : ""))
                    .Include(StylePaths.FontAwesome, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(StylePaths.Simple_Line_Icons, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(StylePaths.FamFamFamFlags, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(isRTL ? StylePaths.BootstrapRTL : StylePaths.Bootstrap, new CssRewriteUrlWithVirtualDirectoryTransform())
                    //.Include(StylePaths.JQuery_Uniform, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(StylePaths.JsTree, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(StylePaths.SweetAlert)
                    .Include(StylePaths.Toastr)
                    .Include(StylePaths.Angular_Ui_Grid, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(StylePaths.Uig, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(StylePaths.Angular_Ui_Select, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(StylePaths.Angular_Multi_Select, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(StylePaths.Angular_AutoComplete, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(StylePaths.Angular_HotKey, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(StylePaths.Bootstrap_DateRangePicker)
                    .Include(StylePaths.Bootstrap_DateTimePicker)
                    .Include(StylePaths.Bootstrap_FileInput)
                    .Include(StylePaths.Bootstrap_Select)
                    .Include(StylePaths.Bootstrap_Switch)
                    .Include(StylePaths.Angular_Bootstrap_DateTimePicker)
                    .Include(StylePaths.Angular_Ranger)
                    .Include(StylePaths.Angular_ngTable)
                    .Include(StylePaths.Angular_Color)
                    .Include(StylePaths.Angular_Timeline)
                    .Include(StylePaths.Angular_QB_Selectize)
                    .Include(StylePaths.Angular_QB)
                    .Include(StylePaths.Summernote)
                    .Include(StylePaths.Angular_Time_Ranger)
                    .Include(StylePaths.JQuery_FileUpload)
                    .Include(StylePaths.MultiSelectList)
                    .Include(StylePaths.FullCalendar)
                    .Include(StylePaths.NgTagsInput)
                    .Include(StylePaths.NgQuill)
                    .Include(StylePaths.NgQuillBubble)
                    .Include(StylePaths.GrapesJs)
                    .ForceOrdered()
                );
        }

        private static void AddAppMetrinicCss(BundleCollection bundles, bool isRTL)
        {
            bundles.Add(
                new StyleBundle("~/Bundles/App/metronic/css" + (isRTL ? "RTL" : ""))
                    .Include("~/metronic/assets/global/css/components-md" + (isRTL ? "-rtl" : "") + ".css", new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include("~/metronic/assets/global/css/plugins-md" + (isRTL ? "-rtl" : "") + ".css", new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include("~/metronic/assets/admin/layout4/css/layout" + (isRTL ? "-rtl" : "") + ".css", new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include("~/metronic/assets/admin/layout4/css/themes/light" + (isRTL ? "-rtl" : "") + ".css", new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include("~/metronic/assets/frontend/pages/css/todo-2.css", new CssRewriteUrlWithVirtualDirectoryTransform())
                    .ForceOrdered()
                );
        }
    }
}