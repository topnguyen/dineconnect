﻿(function () {
    appModule.factory('appSession', [
        function () {

            var _session = {
                user: null,
                tenant: null,
                location: null,
                housedecimals: 2,
                connectdecimals: 2,
                accountDateDto: null,
                locationsBasedOnUser: null,
                locationsBasedOnUserLastUpdateTime: moment(),
                locationList: null,
                locationListLastUpdateTime: moment(),
                timeOut: 60,
                sessionId : null
            };

            abp.services.app.session.getCurrentLoginInformations({ async: false }).done(function (result) {
                _session.user = result.user;
                _session.tenant = result.tenant;
                _session.location = result.location;
                _session.housedecimals = result.housedecimals;
                _session.connectdecimals = result.connectdecimals;
                _session.accountDateDto = result.accountDateDto;
                _session.locationsBasedOnUser = result.locationsBasedOnUser;
                _session.locationLastUpdateTime = moment();
                _session.locationList = result.locationList;
                _session.locationListLastUpdateTime = moment();
                _session.timeOut = 60;
                _session.sessionId = result.sessionId;
            });

            return _session;
        }
    ]);
})();