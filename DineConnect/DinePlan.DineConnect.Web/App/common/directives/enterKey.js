﻿(function () {
    appModule.directive('enterKey', [
        function () {
            return function (scope, element, attrs) {
                element.bind("keydown keypress", function (event) {
                    var code = event.keyCode || event.which;
                    if (code === 13) {
                        scope.$apply(function () {
                            scope.$eval(attrs.enterKey);
                        });
                        event.preventDefault();
                    }
                });
            };
        }
    ]);
})();