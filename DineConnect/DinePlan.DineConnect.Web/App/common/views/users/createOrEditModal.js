﻿(function () {
    appModule.controller('common.views.users.createOrEditModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.user', 'userId', 'abp.services.app.location', 'abp.services.app.organizationUnit', 'uiGridConstants', 'abp.services.app.dinePlanUserRole', 'abp.services.app.language',
        function ($scope, $uibModalInstance, userService, userId, locationService, organizationUnitService, uiGridConstants, dineplanuserroleService, languageService) {
            var vm = this;

            vm.saving = false;
            vm.userId = userId;
            vm.user = null;
            vm.profilePictureId = null;
            vm.roles = [];
            vm.setRandomPassword = (userId == null);
            vm.sendActivationEmail = (userId == null);
            vm.canChangeUserName = true;
            vm.allowedCompany = [];
            vm.defaultLocationRefId = null;
            vm.requestlocations = [];
            vm.minDate = new Date();
            vm.autoSelection = false;
            vm.selectedUserRoles = [];
            vm.passwordPolicyChecked = null;
            //vm.password = null;

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];

            $('input[name="gltgv"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                minDate: vm.minDate
            });

            $('input[name="gltgb"]').daterangepicker({
                locale: {
                    format: $scope.format
                },
                singleDatePicker: true,
                showDropdowns: true,
                minDate: vm.minDate
            });

            $scope.validateDate = function () {
                var startDate = $("#gltgv").val();
                var endDate = $("#gltgb").val();

                if (startDate == null || startDate == '')
                    return;

                if (endDate == null || endDate == '')
                    endDate = startDate;


                if (moment(startDate) > moment(endDate)) {
                    abp.notify.error(app.localize('shouldNotBeGreaterThan', 'StartDate', 'EndDate'));
                    vm.user.gLTGB = null;
                    return;
                }
            };

            vm.refcompany = [];

            function fillDropDownCompany() {
                locationService.getCompanyForCombobox({}).success(function (result) {
                    vm.refcompany = result.items;
                    if (vm.refcompany.length == 1) {
                        vm.allowedCompany = vm.refcompany;
                        vm.filterLocations();
                    }
                    vm.init();
                });
            }

            vm.filterLocations = function () {
                vm.templocs = [];
                angular.forEach(vm.allowedCompany, function (companyvalue, companykey) {
                    angular.forEach(vm.locationlist, function (value, key) {
                        if (value.companyRefId == companyvalue.value) {
                            vm.templocs.push(value);
                        }
                    });
                });

                vm.locations = $.parseJSON(JSON.stringify(vm.templocs));
            }

            vm.submitData = function () {
                debugger;
                if (vm.setRandomPassword || vm.user.id > 0) {
                    vm.save();
                } else {
                    //TODO: when have password
                    userService.checkPasswordWhenCreateUser({
                        loginUserName: vm.user.surname,
                        passWord: vm.user.password,
                        userId: 0
                    }).success(function (result) {
                        vm.passwordPolicyChecked = result;
                        if (result.errorFlag == false) {
                            vm.save();
                        }
                    });
				}
               
            }


            vm.save = function () {
                var assignedRoleNames = _.map(
                    _.where(vm.selectedUserRoles, { isAssigned: true }), //Filter assigned roles
                    function (role) {
                        return role.roleName; //Get names
                    });

                //if (vm.user.password == null) {
                //	vm.user.password = vm.password;
                //}
                if (vm.setRandomPassword) {
                    vm.user.password = null;
                }

                if (!vm.isUndefinedOrNull(vm.user.userJsonDataObject.gLTGV))
                    vm.user.userJsonDataObject.gLTGV = moment(vm.user.userJsonDataObject.gLTGV).format($scope.format);


                if (!vm.isUndefinedOrNull(vm.user.userJsonDataObject.gLTGB))
                    vm.user.userJsonDataObject.gLTGB = moment(vm.user.userJsonDataObject.gLTGB).format($scope.format);

                vm.user.userJsonData = angular.toJson(vm.user.userJsonDataObject);
                vm.saving = true;
                userService.createOrUpdateUser({
                    user: vm.user,
                    assignedRoleNames: assignedRoleNames,
                    sendActivationEmail: vm.sendActivationEmail
                }).success(function (result) {
                    vm.user.id = result.id;
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $uibModalInstance.close();
                }).finally(function () {
                    //organizationUnitService.addLocationToUser({
                    //	userId: vm.user.id,
                    //	locations: vm.requestlocations,
                    //	defaultLocationRefId : vm.defaultLocationRefId
                    //}).success(function () {
                    //	abp.notify.success(app.localize('SuccessfullyAdded'));
                    //	vm.saving = false;
                    //});

                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.getAssignedRoleCount = function () {
                return _.where(vm.roles, { isAssigned: true }).length;
            };
            vm.setRoleGrid = function () {
                vm.roleOptions = {
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                    columnDefs: [
                        {
                            name: app.localize("Id"),
                            field: "roleId",
                            width: "70"
                        },
                        {
                            name: app.localize("Name"),
                            field: "roleDisplayName",
                            width: "1000"
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.griduserroleApi = gridApi;
                        vm.userRolesApi = gridApi;
                        $scope.griduserroleApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                            if (!sortColumns.length || !sortColumns[0].field) {
                                requestParams.sorting = null;
                            } else {
                                requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                            }
                            vm.init();
                        });
                        gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                            requestParams.skipCount = (pageNumber - 1) * pageSize;
                            requestParams.maxResultCount = pageSize;
                            vm.init();
                        });
                        gridApi.selection.on.rowSelectionChanged($scope,
                            function (row) {
                                if (row.isSelected) {
                                    vm.pushUserRoles(row.entity);
                                } else {
                                    for (let i = 0; i < vm.selectedUserRoles.length; i++) {
                                        const obj = vm.selectedUserRoles[i];

                                        if (obj.roleId == row.entity.roleId) {
                                            vm.selectedUserRoles.splice(i, 1);
                                        }
                                    }

                                }
                            });
                    },
                    data: []
                };
            }
            vm.setRoleGrid();

            vm.pushUserRoles = function (myObj) {
                if (myObj != null) {
                    var myIndex = -1;
                    if (vm.selectedUserRoles != null) {
                        vm.selectedUserRoles.some(function (value, key) {
                            if (value.roleId == myObj.roleId) {
                                myIndex = value;
                                return true;
                            }
                        });
                    }

                    if (myIndex == -1) {
                        myObj.isAssigned = true;
                        vm.selectedUserRoles.push(myObj);
                    } else {
                        if (vm.autoSelection == false)
                            abp.notify.error(myObj.roleDisplayName + "  " + app.localize("Available"));
                    }
                }
            };

            vm.setUserRolesSelection = function () {
                if (vm.roles.length == 0)
                    return;
                vm.userRolesApi.grid.modifyRows(vm.roleOptions.data);
                vm.userRolesApi.selection.clearSelectedRows();
                vm.autoSelection = true;
                angular.forEach(vm.roleOptions.data,
                    function (value, key) {
                        vm.selectedUserRoles.some(function (selectedData, selectedKey) {
                            if (value.roleId == selectedData.roleId) {
                                vm.userRolesApi.selection.selectRow(vm.roleOptions.data[key]);
                                return true;
                            }
                        });
                    });
                vm.autoSelection = false;
            };
            vm.init = function () {
                userService.getUserForEdit({
                    id: userId,
                    filterText: vm.filterUserRole
                }).success(function (result) {
                    vm.user = result.user;
                    //vm.password = result.user.password;
                    //vm.user.password = null;
                    vm.profilePictureId = result.profilePictureId;
                    vm.user.passwordRepeat = vm.user.password;
                    vm.roles = result.roles;
                    vm.canChangeUserName = vm.user.userName != app.consts.userManagement.defaultAdminUserName;

                    vm.roleOptions.data = vm.roles;
                    vm.roleOptions.totalItems = vm.roles.length;
                    angular.forEach(vm.roleOptions.data,
                        function (value, key) {
                            if (value.isAssigned) {
                                vm.selectedUserRoles.push(value);
                            }
                        });
                    vm.setUserRolesSelection();
                    vm.defaultLocationRefId = result.defaultLocationRefId;
                    vm.requestlocations = result.locations;
                    vm.allowedCompany = result.companyList;
                    vm.filterLocations();
                    if (vm.user.id == null) {
                        vm.minDate = new Date();
                    }
                    else {
                        vm.minDate = moment(vm.user.creationTime).format($scope.format);
                    }
                    if (!vm.isUndefinedOrNull(vm.user.userJsonDataObject.gltgv))
                        vm.user.userJsonDataObject.gltgv = moment(vm.user.userJsonDataObject.gltgv).format($scope.format);
                    else
                        vm.user.userJsonDataObject.gltgv = vm.minDate;

                    if (!vm.isUndefinedOrNull(vm.user.userJsonDataObject.gltgb))
                        vm.user.userJsonDataObject.gltgb = moment(vm.user.userJsonDataObject.gltgb).format($scope.format);
                    else
                        vm.user.userJsonDataObject.gltgb = vm.minDate;

                    $('input[name="gltgv"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        minDate: vm.minDate,
                        startDate: vm.user.userJsonDataObject.gltgv
                    });

                    $('input[name="gltgb"]').daterangepicker({
                        locale: {
                            format: $scope.format
                        },
                        singleDatePicker: true,
                        showDropdowns: true,
                        minDate: vm.minDate,
                        startDate: vm.user.userJsonDataObject.gltgb
                    });
                });
            }

            vm.locations = [];

            vm.getLocations = function () {
                vm.locationlist = [];
                locationService.getAll({
                    skipCount: 0,
                    maxResultCount: 1000,
                    sorting: null
                }).success(function (result) {
                    vm.locationlist = result.items;
                }).finally(function (result) {
                    fillDropDownCompany();
                });
            };

            vm.getLocations();

            function fillDropDownLanguageCode() {
                languageService.getLanguages({
                }).success(function (result) {
                    vm.reflanguageCode = result.items;
                }).finally(function () {

                });
            }
            fillDropDownLanguageCode();
        }
    ]);
})();