﻿(function () {
    appModule.controller("common.views.smtpsetting.index", [
        "$scope", "abp.services.app.tenantSettings",
        function ($scope, tenantSettingsService) {
            var vm = this;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.settings = null;
            vm.disableFlag = false;
            vm.getSettings = function () {
                vm.loading = true;
                tenantSettingsService.getAllSettings()
                    .success(function (result) {
                        vm.settings = result;
                    }).finally(function () {
                        vm.loading = false;
                    });
            };
            vm.smtptestLogin = function () {
                tenantSettingsService.smtpSettingLoginValidation(
                    vm.settings
                ).success(function (result) {
                    if (result == true) {
                        abp.message.info(app.localize('LoginSMTPSuccess'));
                    }
                    else {
                        abp.message.info(app.localize('LoginSMTPFail'));
                    }
                });
            }
            vm.editSetting = function () {
                vm.disableFlag = false;
            }
            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };
            vm.validateEmail = function (email) {
                const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(String(email).toLowerCase());
            }
            vm.save = function () {
                vm.loading = true;
                vm.disableFlag = true;
                var defaultFromDisplayName = vm.settings.email.defaultFromDisplayName;
                var smtpHost = vm.settings.email.smtpHost;
                var defaultFromAddress = vm.settings.email.defaultFromAddress;
                var smtpUserName = vm.settings.email.smtpUserName;
                var smtpPassword = vm.settings.email.smtpPassword;
                var smtpPort = vm.settings.email.smtpPort;
                if (vm.isUndefinedOrNull(defaultFromDisplayName)
                    || vm.isUndefinedOrNull(smtpHost)
                    || vm.isUndefinedOrNull(defaultFromAddress)
                    || vm.isUndefinedOrNull(smtpUserName)
                    || vm.isUndefinedOrNull(smtpPassword)
                    || vm.isUndefinedOrNull(smtpPort)) {
                    abp.message.info(app.localize('PleaseInsertRecommendField'));
                    vm.loading = false;
                    vm.disableFlag = false;
                }
                else {
                    vm.settings.email.smtpDomain = null;
                    if (vm.validateEmail(defaultFromAddress)) {
                        tenantSettingsService.updateAllSettings(
                            vm.settings
                        ).success(function () {
                            abp.notify.info(app.localize('SavedSuccessfully'));
                        }).finally(function () {
                            vm.loading = false;
                            vm.disableFlag = false;
                        });
                    }
                    else {
                        abp.message.info(app.localize('ValidEmail'));
                        vm.loading = false;
                        vm.disableFlag = false;
                    }
                }
            };
            vm.getSettings();
        }
    ]);
})();