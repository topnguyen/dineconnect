﻿(function () {
    appModule.controller('common.views.trackerLogs.detailModal', [
        '$scope', '$uibModalInstance', 'auditLog',
        function ($scope, $uibModalInstance, auditLog) {
            var vm = this;

            vm.auditLog = auditLog;

            vm.getExecutionTime = function () {
                return moment(vm.auditLog.eventDateUTC).fromNow() + ' (' + moment(vm.auditLog.eventDateUTC).format('YYYY-MM-DD hh:mm:ss') + ')';
            };

            vm.getEventType = function () {
                switch (vm.auditLog.eventType) {
                    case 0:
                        return "Added";

                    case 1:
                        return "Deleted";

                    case 2:
                        return "Modified";

                    case 3:
                        return "SoftDeleted";

                    case 4:
                        return "UnDeleted";

                    default:
                        return "UnChanged";
                }
            };

            vm.getFormattedParameters = function () {
                var json = JSON.parse(vm.auditLog.parameters);
                return JSON.stringify(json, null, 4);
            };

            vm.close = function () {
                $uibModalInstance.dismiss();
            };
        }
    ]);
})();