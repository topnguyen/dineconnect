﻿(function () {
    appModule.controller('common.views.layout.header', [
        '$rootScope', '$scope', '$q', '$uibModal', 'appSession', 'appUserNotificationHelper', 'abp.services.app.notification', 'abp.services.app.userLink', 'abp.services.app.setup', 'abp.services.app.location', 'abp.services.app.organizationUnit', 'abp.services.app.reportBackground', 'abp.services.app.tenantSettings',
        function ($rootScope, $scope, $q, $uibModal, appSession, appUserNotificationHelper,
            notificationService, userLinkService,
            setupService, locationService, organizationUnitService, reportBackgroundService, tenantSettingService) {

            /* eslint-disable */
            var vm = this;
            $scope.$on('$includeContentLoaded', function () {
                Layout.initHeader(); // init header
            });

            vm.languages = abp.localization.languages;
            vm.currentLanguage = abp.localization.currentLanguage;
            vm.user = null;

            vm.isImpersonatedLogin = abp.session.impersonatorUserId;
            vm.notifications = [];
            vm.unreadNotificationCount = 0;
            vm.recentlyUsedLinkedUsers = [];
            vm.currentUserId = abp.session.userId;
            vm.currentUserName = appSession.user.name;

            vm.locations = [];
            vm.defaultLocationRefId = appSession.user.locationRefId;
            vm.currentLocation = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.selectReport = function() {
                $uibModal.open({
                    size: 'lg',
                    templateUrl: '~/App/common/views/report/backgroudView.cshtml',
                    controller: 'common.views.report.backgroudView as vm',
                    backdrop: 'static'
                });
            }
            vm.getLocations = function () {
                vm.loading = true;
                locationService.getLocationBasedOnUser({
                    userId: vm.currentUserId
                }).success(function (result) {
                    vm.locations = $.parseJSON(JSON.stringify(result.items));
                    if (vm.defaultLocationRefId == null || vm.defaultLocationRefId == 0) {
                        if (result.items.length > 0)
                            vm.changeLocation(result.items[0]);
                    } else {
                        vm.locations.some(function (value, key) {
                            if (value.id == vm.defaultLocationRefId) {
                                vm.currentLocation = value;
                                return true;
                            }
                        });
                    }
                }).finally(function (result) {
                    vm.loading = false;
                });
            };

            vm.spaces = '  ';

            vm.getLocations();

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null;
            };

            vm.changeLocation = function (selectedLocation) {
                vm.loading = true;
                vm.currentLocation = selectedLocation;
                appSession.user.locationRefId = selectedLocation.id;
                abp.notify.success(app.localize('LocationChanged'));
                organizationUnitService.appendDefaultUserOrganization({
                    userId: vm.currentUserId,
                    organizationUnitId: selectedLocation.id,
                    organizationId: selectedLocation.companyRefId
                }).success(function () {
                    if (appSession.user.organisationRefId !== selectedLocation.companyRefId) {
                        location.href = abp.appPath + 'Account/Logout';
                    } else {
                        location.href = abp.appPath + 'Localization/ChangeCulture?cultureName=' + vm.currentLanguage.name + '&returnUrl=' + window.location.href;
                    }
                }).finally(function () {
                    vm.loading = false;
                });

            };

            vm.setupPer = {
                setup: abp.auth.hasPermission('Pages.Tenant.Setup'),
                importData: abp.auth.hasPermission('Pages.Tenant.Setup.ImportSeedData'),
                clearData: abp.auth.hasPermission('Pages.Tenant.Setup.CleanSeedData')
            };

            vm.getShownUserName = function () {
                if (!abp.multiTenancy.isEnabled) {
                    return appSession.user.userName;
                } else {
                    if (appSession.tenant) {
                        return appSession.tenant.tenancyName + '\\' + appSession.user.userName;
                    } else {
                        return '.\\' + appSession.user.userName;
                    }
                }
            };

            vm.getShownLinkedUserName = function (linkedUser) {
                return app.getShownLinkedUserName(linkedUser);
            };

            vm.editMySettings = function () {
                $uibModal.open({
                    templateUrl: '~/App/common/views/profile/mySettingsModal.cshtml',
                    controller: 'common.views.profile.mySettingsModal as vm',
                    backdrop: 'static'
                });
            };

            vm.changePassword = function () {
                $uibModal.open({
                    templateUrl: '~/App/common/views/profile/changePassword.cshtml',
                    controller: 'common.views.profile.changePassword as vm',
                    backdrop: 'static',
                    resolve: {
                        user: function () {
                            return vm.user;
                        }
                    }
                });
            };

            vm.changePicture = function () {
                $uibModal.open({
                    templateUrl: '~/App/common/views/profile/changePicture.cshtml',
                    controller: 'common.views.profile.changePicture as vm',
                    backdrop: 'static'
                });
            };

            vm.changeLanguage = function (languageName) {
                location.href = abp.appPath + 'Localization/ChangeCulture?cultureName=' + languageName + '&returnUrl=' + window.location.href;
            };

            vm.backToMyAccount = function () {
                abp.ajax({
                    url: abp.appPath + 'Account/BackToImpersonator'
                });
            }

            vm.loadNotifications = function () {
                notificationService.getUserNotifications({
                    maxResultCount: 3
                }).success(function (result) {
                    vm.unreadNotificationCount = result.unreadCount;
                    vm.notifications = [];
                    $.each(result.items, function (index, item) {
                        vm.notifications.push(appUserNotificationHelper.format(item));
                    });
                });
            }
           
            vm.setAllNotificationsAsRead = function () {
                appUserNotificationHelper.setAllAsRead();
            };

            vm.setNotificationAsRead = function (userNotification) {
                appUserNotificationHelper.setAsRead(userNotification.userNotificationId);
            }

            vm.openNotificationSettingsModal = function () {
                appUserNotificationHelper.openSettingsModal();
            };

            vm.manageLinkedAccounts = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '~/App/common/views/profile/linkedAccountsModal.cshtml',
                    controller: 'common.views.profile.linkedAccountsModal as vm',
                    backdrop: 'static'
                });

                modalInstance.result.finally(function () {
                    vm.getRecentlyUsedLinkedUsers();
                });
            };

            vm.getRecentlyUsedLinkedUsers = function () {
                userLinkService.getRecentlyUsedLinkedUsers()
                    .success(function (result) {
                        vm.recentlyUsedLinkedUsers = result.items;
                    }).finally(function () {
                    });
            }

            vm.switchToUser = function (linkedUser) {
                abp.ajax({
                    url: abp.appPath + 'Account/SwitchToLinkedAccount',
                    data: JSON.stringify({
                        targetUserId: linkedUser.id
                    })
                });
            };

            vm.cleanAll = function () {
                abp.message.confirm(
                    app.localize('CleanAllWarning'),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            vm.loading = true;
                            setupService.clearAll()
                                .success(function () {
                                    abp.notify.success(app.localize('SuccessfullyCleared'));
                                    vm.loading = false;
                                });
                        }
                    }
                );
            };
            vm.cleanMenu = function () {
                abp.message.confirm(
                    app.localize('CleanMenuWarning'),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            vm.loading = true;
                            setupService.clearMenu()
                                .success(function () {
                                    abp.notify.success(app.localize('SuccessfullyCleared'));
                                    vm.loading = false;
                                });
                        }
                    }
                );
            };
            vm.cleanMaerial = function () {
                abp.message.confirm(
                    app.localize('CleanMaterialWarning'),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            vm.loading = true;
                            setupService.clearMaterials()
                                .success(function () {
                                    abp.notify.success(app.localize('SuccessfullyCleared'));
                                    vm.loading = false;
                                });
                        }
                    }
                );
            };
            vm.clearTransaction = function () {
                abp.message.confirm(
                    app.localize('CleanDataWarning'),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            vm.loading = true;
                            setupService.clearTransaction()
                                .success(function () {
                                    abp.notify.success(app.localize('SuccessfullyCleared'));
                                    vm.loading = false;
                                });
                        }
                    }
                );
            };

            vm.clearHouseTransaction = function () {
                abp.message.confirm(
                    app.localize('CleanDataWarning'),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            vm.loading = true;
                            setupService.clearAllHouseTransactions()
                                .success(function () {
                                    abp.notify.success(app.localize('SuccessfullyCleared'));
                                    vm.loading = false;
                                });
                        }
                    }
                );
            };

            vm.importAll = function () {
                abp.message.confirm(
                    app.localize('ImportWarning'),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            vm.loading = true;
                            setupService.seedAll()
                                .success(function () {
                                    abp.notify.success(app.localize('SuccessfullyImported'));
                                    vm.loading = false;
                                });

                        }
                    }
                );
            }
            function openImportModel() {
                $uibModal.open({
                    templateUrl: '~/App/common/views/layout/importModal.cshtml',
                    controller: 'common.views.connect.setup.importmodal as vm',
                    backdrop: 'static'
                });
            }

            vm.importData = function () {
                openImportModel();
            };


            abp.event.on('abp.notifications.received', function (userNotification) {
                appUserNotificationHelper.show(userNotification);
                vm.loadNotifications();
            });

            abp.event.on('app.notifications.refresh', function () {
                vm.loadNotifications();
            });

            abp.event.on('app.notifications.read', function (userNotificationId) {
                for (var i = 0; i < vm.notifications.length; i++) {
                    if (vm.notifications[i].userNotificationId == userNotificationId) {
                        vm.notifications[i].state = 'READ';
                    }
                }

                vm.unreadNotificationCount -= 1;
            });

            function init() {
                vm.loadNotifications();
                vm.getRecentlyUsedLinkedUsers();
            }

            init();

            vm.selectLocation = function () {
                vm.loading = true;
                var modalInstance = $uibModal.open({
                    templateUrl: '~/App/tenant/views/connect/location/selectCurrentLocation.cshtml',
                    controller: 'tenant.views.connect.location.select.selectCurrentLocation as vm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        userId: function () {
                            return vm.currentUserId;
                        },
                        userName: function () {
                            return vm.currentUserName;
                        },
                        defaultLocationRefId: function () {
                            return vm.currentLocation.id;
                        }
                    }
                });

                modalInstance.result.then(function (result) {

                    if (result != null)
                        vm.changeLocation(result.selectedLocation);
                    else
                        vm.loading = false;
                });
            }
        }
    ]);
})();