﻿(function () {
    appModule.controller('common.views.connect.setup.importmodal', [
        '$scope', '$uibModalInstance', 'abp.services.app.setup', 'appSession', 'abp.services.app.location',
        function ($scope, $modalInstance, setupService, appSession, locationService) {
            var vm = this;
            vm.defaultLocationRefId = appSession.user.locationRefId;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            $scope.formats = ['YYYY-MM-DD', 'DD-MMMM-YYYY', 'DD.MM.YYYY', 'shortDate'];
            $scope.format = $scope.formats[0];

            vm.saving = false;
            vm.save = function () {
                vm.saving = true;
                setupService.seedSampleData($.extend({}, vm.dateRangeModel)).success(function () {
                    abp.notify.info(app.localize('SuccessfullyImported'));
                    vm.importSampleInward();
                    $modalInstance.close();
                }).finally(function () {
                  
                });
            };

            vm.importSampleInward = function () {
                abp.message.confirm(
                    app.localize('ImportInwardWarning'),
                    function (isConfirmed) {
                        vm.loading = true;
                        if (isConfirmed) {
                            locationService.getLocations().success(function (result) {
                                vm.loading = true;
                                if (result.items.length == 0) {
                                    abp.message.warn(app.localize('ImportLocationFirst'));
                                }
                                angular.forEach(result.items, function (value, key) {
                                    setupService.seedSampleInwardDirectCredit(
                                        {
                                            startDate: moment(vm.dateRangeModel.startDate).format($scope.format),
                                            endDate: moment(vm.dateRangeModel.endDate).format($scope.format),
                                            locationRefId: value.id
                                        }).success(function () {
                                            abp.notify.success(value.name + ' ' + app.localize('SuccessfullyImported'));
                                        }).finally(function () {

                                        });
                                });


                            }).finally(function () {
                                $modalInstance.close();
                                vm.loading = false;
                            });
                        }
                    });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

        }
    ]);
})();

