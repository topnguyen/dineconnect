﻿(function () {
    appModule.controller('common.views.roles.createOrEditModal', [
        '$scope', '$uibModalInstance', '$window',  'abp.services.app.role', 'roleId',
        function ($scope, $uibModalInstance, $window, roleService, roleId) {
            var vm = this;

            vm.saving = false;
            vm.role = null;
            vm.permissionEditData = null;

            vm.refreshPage = function () {
                $window.location.reload();
            };

            vm.save = function () {
                vm.saving = true;
                roleService.createOrUpdateRole({
                    role: vm.role,
                    grantedPermissionNames: vm.permissionEditData.grantedPermissionNames
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.refreshPage();
                    $uibModalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            function init() {
                roleService.getRoleForEdit({
                    id: roleId
                }).success(function (result) {
                    vm.role = result.role;
                    vm.permissionEditData = {
                        permissions: result.permissions,
                        grantedPermissionNames: result.grantedPermissionNames
                    };
                });
            }

            init();
        }
    ]);
})();