﻿(function() {
    appModule.controller("common.views.report.backgroudView", [
        "$scope", "$uibModalInstance", "uiGridConstants", "abp.services.app.reportBackground", "abp.services.app.connectLookup",
        function ($scope, $modalInstance, uiGridConstants, reportBackground, connectLookupService) {
            var vm = this;

            vm.saving = false;
            vm.reportName = "";
            vm.allReports = [];
            vm.allRequestReports = [];

            vm.locationOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Select"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                                "  <button ng-disable=\"!row.complete\" ng-click=\"grid.appScope.downloadReport(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Download") + "\"><i class=\"fa fa-cloud-download\"></i></button>" +
                            "</div>",
                        width: 80
                    },
                    {
                        name: app.localize("Id"),
                        field: "id",
                        width : 80
                    },
                    {
                        name: app.localize("ReportName"),
                        field: "reportName"
                    },
                    {
                        name: app.localize("Completed"),
                        field: "completed",
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents\">" +
                                "  <span ng-show=\"row.entity.completed\" class=\"label label-success\">" + app.localize("Yes") + "</span>" +
                                "  <span ng-show=\"!row.entity.completed\" class=\"label label-default\">" + app.localize("No") + "</span>" +
                            "</div>",
                        width: 100
                    },
                    {
                        name: app.localize("UpdateTime"),
                        field: "lastModificationTime",
                        cellFilter: "momentFormat: 'YYYY-MM-DD HH:mm:ss'",
                    },
                    {
                        name: app.localize("CreatedTime"),
                        field: "creationTime",
                        cellFilter: "momentFormat: 'YYYY-MM-DD HH:mm:ss'"
                    }
                ],
                onRegisterApi: function(gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        vm.getLocations();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function(pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        vm.getLocations();
                    });
                },
                data: []
            };

            vm.cancel = function() {
                $uibModalInstance.dismiss();
            };
            vm.downloadReport = function(obj) {
                reportBackground.getFileDto(obj.id
                ).success(function(result) {
                    if (result != null) 
                        app.openTempFileOnly(result);
                }).finally(function() {
                    vm.loading = false;
                });
            };

            vm.getAll = function () {
                vm.loading = true;
                reportBackground.getAll({
                    reports: vm.allRequestReports
                }).success(function (result) {

                    vm.locationOptions.totalItems = result.totalCount;
                    vm.locationOptions.data = result.items;

                    vm.getReports();

                }).finally(function () {
                    vm.loading = false;
                });
            }
            vm.getReports = function () {
                connectLookupService.getReportNames({
                }).success(function (result) {
                    vm.allReports = result.items;
                }).finally(function (result) {
                });
            };

            vm.cancel = function() {
                $modalInstance.close(null);
            };

            vm.getAll();

        }
    ]);
})();