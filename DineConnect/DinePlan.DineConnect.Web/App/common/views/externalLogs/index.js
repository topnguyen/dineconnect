﻿(function () {
    appModule.controller('common.views.externalLogs.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.auditLog', "abp.services.app.connectReport", 
        function ($scope, $uibModal, uiGridConstants, auditLogService, connectReportAppService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.auditType = [];

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: 'id'
            };

            //Date Range
            $scope.formats = ["YYYY-MM-DD", "DD-MMMM-YYYY", "DD.MM.YYYY", "shortDate"];
            $scope.format = $scope.formats[0];
            var todayAsString = moment().format("YYYY-MM-DD");
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            
            //#endregion
            

            vm.gridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize('Date'),
                        field: 'logTime',
                        cellFilter: 'momentFormat: \'YYYY-MM-DD HH:mm:ss\'',
                        minWidth: 120
                    },
                    {
                        name: app.localize('Type'),
                        field: 'externalLogType',
                        minWidth: 100
                    },
                    {
                        name: app.localize('LogDescription'),
                        field: 'logDescription',
                        minWidth: 200
                    },
                    {
                        name: app.localize('LogData'),
                        field: 'logData',
                        minWidth: 200
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getExternalLogs();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;

                        vm.getExternalLogs();
                    });
                },
                data: []
            };

            vm.getExternalLogs = function () {
                vm.loading = true;
                
                connectReportAppService.getAllExternalLog({
                    startDate: moment(vm.dateRangeModel.startDate).lang("en").format("YYYY-MM-DD"),
                    endDate: moment(vm.dateRangeModel.endDate).lang("en").format("YYYY-MM-DD"),
                    auditTypes: vm.auditType,
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting
                }).success(function (result) {
                    vm.gridOptions.totalItems = result.totalCount;
                    vm.gridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.getExternalLogInput = function (confirmation, obj) {
                return {
                    startDate: moment(vm.dateRangeModel.startDate).lang("en").format($scope.format),
                    endDate: moment(vm.dateRangeModel.endDate).lang("en").format($scope.format),
                    auditTypes: vm.auditType,
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    runInBackground: confirmation,
                    exportOutputType: obj
                };
            };
            vm.exportToExcel = function (obj) {
                abp.ui.setBusy("#MyLoginForm");
                abp.message.confirm(app.localize("AreYouSure"),
                    app.localize("RunInBackground"),
                    function (isConfirmed) {
                        var myConfirmation = false;
                        if (isConfirmed) {
                            myConfirmation = true;
                        }
                        vm.disableExport = true;
                        connectReportAppService.getExternalLogExcel(vm.getExternalLogInput(myConfirmation, obj)).success(function (result) {
                                if (result != null) {
                                    app.downloadTempFile(result);
                                }
                            }).finally(function () {
                                abp.ui.clearBusy("#MyLoginForm");
                                vm.disableExport = false;
                            });
                    });
            };

            vm.clear = function () {
                vm.dateRangeModel = {
                    startDate: todayAsString,
                    endDate: todayAsString
                };
                vm.auditType = [];
                vm.getExternalLogs();
            }

            function getAuditType() {
                vm.saving = true;
                auditLogService.getAuditTypes()
                    .success(function (result) {
                        vm.auditTypes = result.items;
                    }).finally(function (result) {
                        vm.saving = false;
                    });
            }
            getAuditType();

            vm.getExternalLogs();
        }
    ]);
})();