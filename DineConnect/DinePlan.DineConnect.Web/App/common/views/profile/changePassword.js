﻿(function () {
    appModule.controller('common.views.profile.changePassword', [
        '$scope', '$uibModalInstance', 'abp.services.app.profile', 'user', 'abp.services.app.user', 'appSession', 'abp.services.app.tenantSettings',
        function ($scope, $uibModalInstance, profileService, user, userService, appSession, tenantSettingService) {
            var vm = this;

            vm.enforce = false;
            

            vm.saving = false;
            vm.user = [];
            if (user == null) {
                angular.copy(appSession.user, vm.user);
            } else {
                angular.copy(user, vm.user);
			}
            vm.userId = vm.user.id;
            //vm.user.id = null;
            vm.passwordInfo = null;

            vm.isUndefinedOrNull = function (val) {
                return angular.isUndefined(val) || val == null || val == '';
            };

            vm.save = function () {
                //if (!vm.isUndefinedOrNull(vm.user)) {
                //    vm.passwordInfo.userId = vm.user.id
                //}
                //else {
                //    if (vm.isUndefinedOrNull(vm.passwordInfo.currentPassword)) {
                //        abp.notify.error("Please Enter Current Password");
                //        return;
                //    }
                //}
                vm.passwordInfo.userId = vm.user.id;
                if (vm.isUndefinedOrNull(vm.passwordInfo.currentPassword)) {
                    abp.notify.error("Please Enter Current Password");
                    return;
                }
                vm.saving = true;
                profileService.changePassword(vm.passwordInfo)
                    .success(function () {
                        abp.notify.info(app.localize('YourPasswordHasChangedSuccessfully'));
                        $uibModalInstance.close();
                    }).finally(function () {
                        vm.saving = false;
                    });
            };

            vm.getPasswordPolicyAccepted = function (argsaveFlag) {
                if (vm.isUndefinedOrNull(vm.passwordInfo.currentPassword)) {
                    abp.notify.error("Please Enter Current Password");
                    return;
                }
                vm.passwordPolicyChecked = null;
                userService.checkPasswordPolicyWithTheGivenPassword({
                    loginUserName: vm.user.userName,
                    passWord: vm.passwordInfo.newPassword,
                    userId: appSession.user.id
                }).success(function (result) {
                    vm.passwordPolicyChecked = result;
                    if (argsaveFlag && result.errorFlag==false) {
                        vm.save();
                    }
                });
            }


            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.passwordPolicy = null;
            vm.getPasswordPolicyRule = function () {
                vm.passwordPolicy = null;
                userService.getPasswordPolicy({

                }).success(function (result) {
                    vm.passwordPolicy = result;
                });
            }
            vm.getPasswordPolicyRule();
        }
    ]);
})();