﻿(function () {
    appModule.controller('common.views.maintenance.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.caching', 'abp.services.app.webLog',
        function ($scope, $uibModal, uiGridConstants, cacheService, webLogService) {
            var vm = this;
            /* eslint-disable */
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.caches = null;
            vm.logs = '';

            vm.getCaches = function () {
                vm.loading = true;
                //locationService.getLocationBasedOnUser({
                //    userId: vm.currentUserId
                //}).success(function (result) {
                //    vm.allowedLocation = result.items;
                //}).finally(function () {

                //    });

                cacheService.getAllCaches()
                    .success(function(result)  {
                        vm.caches = result.items;
                    })
                    .finally(function () { vm.loading = false; });
            };

            vm.clearCache = function (cacheName) {
                cacheService.clearCache({ id: cacheName })
                    .success(function (result){
                        abp.notify.info(app.localize('CacheSuccessfullyCleared'));
                    });
            };

            vm.clearAllCaches = function () {
                cacheService.clearAllCaches()
                    .success(function (result) {
                        abp.notify.info(app.localize('AllCachesSuccessfullyCleared'));
                    });
            };

            vm.getWebLogs = function () {
                webLogService.getLatestWebLogs()
                    .success(function (result) {
                        vm.logs = result.latestWebLogLines;
                        vm.fixWebLogsPanelHeight();
                    });
            };

            vm.downloadWebLogs = function () {
                webLogService.downloadWebLogs()
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.getLogClass = function (log) {
                if (log.startsWith('DEBUG')) {
                    return 'badge badge-dark';
                }

                if (log.startsWith('INFO')) {
                    return 'badge badge-info';
                }

                if (log.startsWith('WARN')) {
                    return 'badge badge-warning';
                }

                if (log.startsWith('ERROR')) {
                    return 'badge badge-danger';
                }

                if (log.startsWith('FATAL')) {
                    return 'badge badge-danger';
                }

                return '';
            };

            vm.getLogType = function (log) {
                if (log.startsWith('DEBUG')) {
                    return 'DEBUG';
                }

                if (log.startsWith('INFO')) {
                    return 'INFO';
                }

                if (log.startsWith('WARN')) {
                    return 'WARN';
                }

                if (log.startsWith('ERROR')) {
                    return 'ERROR';
                }

                if (log.startsWith('FATAL')) {
                    return 'FATAL';
                }

                return '';
            };

            vm.getRawLogContent = function (log) {
                return _.escape(log)
                    .replace('DEBUG', '')
                    .replace('INFO', '')
                    .replace('WARN', '')
                    .replace('ERROR', '')
                    .replace('FATAL', '');
            };

            vm.fixWebLogsPanelHeight = function () {
                var panel = document.getElementsByClassName('full-height')[0];
                var windowHeight = document.body.clientHeight;
                var panelHeight = panel.clientHeight;
                var difference = windowHeight - panelHeight;
                var fixedHeight = panelHeight + difference;
                panel.style.height = (fixedHeight - 420) + 'px';
            };

            vm.onResize = function (event) {
                vm.fixWebLogsPanelHeight();
            };

            vm.getCaches();
            vm.getWebLogs();
        }
    ]);
})();