﻿(function () {
    appModule.controller('common.views.keyentry.index', [
        '$scope',
        function ($scope) {
            var vm = this;
            vm.key = null;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.validate = function () {
                vm.importpath = abp.appPath + 'Home/KeyEntry?key=' + vm.key;

                window.location = vm.importpath;
            };
        }]);
})();