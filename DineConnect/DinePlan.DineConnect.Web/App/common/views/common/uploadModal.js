﻿(function () {
    appModule.controller('common.views.common.uploadModal', [
        '$scope', '$uibModalInstance', 'FileUploader',
        function ($scope, $uibModalInstance, fileUploader) {
            var vm = this;
            vm.files = [];
            vm.status = "No uploads";
            vm.serverUrl = null;
            vm.accessKey = null;
            vm.secretKey = null;

            vm.uploader = new fileUploader({
                url: abp.appPath + 'FileUpload/UploadFile',
                filters: [{
                    name: 'imageFilter',
                    queueLimit: 1,
                    fn: function (item, options) {
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        return true;
                    }
                }]
            });

            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.error != null) {
                    abp.message.warn(response.error.message);
                    $uibModalInstance.close();
                    vm.loading = false;
                    return;
                }
                abp.notify.info(app.localize("FINISHED"));
                console.log(response);

                $uibModalInstance.close();
                vm.loading = false;
            };

            vm.chooseFile = function () {
                document.querySelector("#fileUpload").click();
                vm.uploader.queue = [];
            };

            vm.save = function () {
                vm.loading = true;

                vm.uploader.uploadAll();
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }

    ]);
})();