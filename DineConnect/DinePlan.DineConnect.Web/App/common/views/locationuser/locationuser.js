﻿(function () {
    appModule.controller("common.views.locationuser", [
        "$scope", "$uibModal", "$q", "uiGridConstants", "abp.services.app.organizationUnit", "abp.services.app.commonLookup", "lookupModal", "abp.services.app.location", "$rootScope",
        function ($scope, $uibModal, $q, uiGridConstants, organizationUnitService, commonLookupService, lookupModal, locationService, $rootScope) {
            var vm = this;
            vm.allowedCompany = [];
            vm.allowedLocationGroup = [];
            vm.selectedLocation = null;
            $rootScope.settings.layout.pageSidebarClosed = true;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var rightrequestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.refcompany = [];

            function fillDropDownCompany() {
                locationService.getCompanyForCombobox({}).success(function (result) {
                    vm.refcompany = result.items;
                });
            }
            vm.refLocationGroups = [];

            function fillDropDownLocationGroup() {
                locationService.getLocationGroupForCombobox({}).success(function (result) {
                    vm.refLocationGroups = result.items;
                });
            }

            fillDropDownCompany();
            fillDropDownLocationGroup();

            vm.getLocations = function () {
                vm.loading = true;
                locationService.getLocationsAllForGivenOrganisationList({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterLocationText,
                    companyList: vm.allowedCompany,
                    locationGroups: vm.allowedLocationGroup
                }).success(function (result) {
                    vm.locationOptions.totalItems = result.totalCount;
                    vm.locationOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                    vm.selectedLocations = [];
                    vm.selectedLocation = null;
                });
            };

            vm.locationOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                enableRowSelection: true,
                selectionRowHeaderWidth: 35,
                rowHeight: 35,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                   
                    {
                        name: app.localize("Id"),
                        field: "id",
                        maxWidth: 60
                    },
                    {
                        name: app.localize("Code"),
                        field: "code"
                    },
                    {
                        name: app.localize("Name"),
                        field: "name"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        vm.getLocations();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        vm.getLocations();
                    });

                    gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                        vm.selectLocationFromGrid(gridApi);
                    });

                    gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
                        vm.selectLocationFromGrid(gridApi);
                    });
                },
                data: []
            };

            vm.getLocations();

            $scope.$on("$viewContentLoaded", function () {
                App.initAjax();
            });

            vm.permissions = {
                manageOrganizationTree: abp.auth.hasPermission("Pages.Administration.OrganizationUnits.ManageOrganizationTree"),
                manageMembers: abp.auth.hasPermission("Pages.Administration.OrganizationUnits.ManageMembers")
            };

            vm.members = {
                gridOptions: {
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    useExternalSorting: true,
                    appScopeProvider: vm,
                    columnDefs: [
                        {
                            name: app.localize("Actions"),
                            enableSorting: false,
                            width: 60,
                            cellTemplate:
                                "<div class=\"ui-grid-cell-contents\">" +
                                "  <button ng-if=\"grid.appScope.permissions.manageMembers\" class=\"btn btn-default btn-xs\" ng-click=\"grid.appScope.members.remove(row.entity)\" title=\"" + app.localize("Delete") + "\"><i class=\"fa fa-trash-o\"></i></button>" +
                                "</div>"
                        },
                        {
                            name: app.localize("UserName"),
                            field: "userName",
                            cellTemplate:
                                "<div class=\"ui-grid-cell-contents\" title=\"{{row.entity.name + ' ' + row.entity.surname + ' (' + row.entity.emailAddress + ')'}}\">" +
                                "  <img ng-if=\"row.entity.profilePictureId\" ng-src=\"" + abp.appPath + "Profile/GetProfilePictureById?id={{row.entity.profilePictureId}}\" width=\"22\" height=\"22\" class=\"img-rounded img-profile-picture-in-grid\" />" +
                                "  <img ng-if=\"!row.entity.profilePictureId\" src=\"" + abp.appPath + "Common/Images/default-profile-picture.png\" width=\"22\" height=\"22\" class=\"img-rounded\" />" +
                                "  {{COL_FIELD CUSTOM_FILTERS}} &nbsp;" +
                                "</div>",
                            minWidth: 140
                        },
                        {
                            name: app.localize("AddedTime"),
                            field: "addedTime",
                            cellFilter: "momentFormat: 'YYYY-MM-DD HH:mm:ss'",
                            minWidth: 100
                        }
                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                            if (!sortColumns.length || !sortColumns[0].field) {
                                rightrequestParams.sorting = null;
                            } else {
                                rightrequestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                            }

                            vm.members.loadforSelectedLocation(vm.selectedLocation);
                        });
                        gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                            rightrequestParams.skipCount = (pageNumber - 1) * pageSize;
                            rightrequestParams.maxResultCount = pageSize;

                            vm.members.loadforSelectedLocation(vm.selectedLocation);
                        });
                    },
                    data: []
                },

                loadforSelectedLocation: function (argSelectedLocation) {
                    if (argSelectedLocation === null) {
                        vm.members.gridOptions.totalItems = 0;
                        vm.members.gridOptions.data = [];
                        return;
                    }

                    organizationUnitService.getOrganizationUnitUsers({
                        id: argSelectedLocation.organizationUnitId,
                        skipCount: rightrequestParams.skipCount,
                        maxResultCount: rightrequestParams.maxResultCount,
                        sorting: rightrequestParams.sorting
                    }).success(function (result) {
                        vm.members.gridOptions.totalItems = result.totalCount;
                        vm.members.gridOptions.data = result.items;
                    });
                },

                add: function (userId) {
                    var ouIds = [];
                    angular.forEach(vm.selectedRows, function (row) {
                        ouIds.push(row.organizationUnitId);
                    });

                    if (ouIds.length === 0) {
                        return;
                    }
                    angular.forEach(ouIds, function (ouId, index) {
                        organizationUnitService.addUserToOrganizationUnit({
                            organizationUnitId: ouId,
                            userId: userId
                        }).success(function () {
                            if (index === ouIds.length - 1) {
                                abp.notify.success(app.localize("SuccessfullyAdded"));
                                //vm.organizationTree.incrementMemberCount(ouId, 1);
                                vm.members.loadforSelectedLocation(vm.selectedLocation);
                            }
                        });
                    });
                },

                remove: function (user) {
                    var ouId = vm.selectedLocation.organizationUnitId;
                    if (!ouId) {
                        return;
                    }

                    abp.message.confirm(
                        app.localize("RemoveUserFromOuWarningMessage", user.userName, vm.selectedLocation.name),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                organizationUnitService.removeUserFromOrganizationUnit({
                                    organizationUnitId: ouId,
                                    userId: user.id
                                }).success(function () {
                                    abp.notify.success(app.localize("SuccessfullyRemoved"));
                                    //vm.organizationTree.incrementMemberCount(ouId, -1);
                                    vm.members.loadforSelectedLocation(vm.selectedLocation);
                                });
                            }
                        }
                    );
                },

                openAddModal: function () {
                    var ouIds = [];
                    angular.forEach(vm.selectedRows, function (row) {
                        ouIds.push(row.organizationUnitId);
                    });
                    if (ouIds.length === 0) {
                        return;
                    }

                    lookupModal.open({
                        title: app.localize("SelectAUser"),
                        serviceMethod: commonLookupService.findUsers,

                        canSelect: function (item) {
                            return $q(function (resolve, reject) {
                                angular.forEach(ouIds, function (ouId) {
                                    organizationUnitService.isInOrganizationUnit({
                                        userId: item.value,
                                        organizationUnitId: ouId
                                    }).success(function (result) {
                                        if (result) {
                                            abp.message.warn(app.localize("UserIsAlreadyInTheOrganizationUnit"));
                                        }

                                        resolve(!result);
                                    }).catch(function () {
                                        reject();
                                    });
                                });
                            });
                        },

                        callback: function (selectedItem) {
                            vm.members.add(selectedItem.value);
                        }
                    });
                },

                init: function () {
                    if (!vm.permissions.manageMembers) {
                        vm.members.gridOptions.columnDefs.shift();
                    }
                }
            };
            vm.members.init();

            vm.selectLocationFromGrid = function (gridApi) {
                vm.selectedRows = gridApi.selection.getSelectedRows();

                vm.selectedLocations = [];
                angular.forEach(vm.selectedRows, function (row) {
                    vm.selectedLocations.push(row.id);
                });
                vm.selectedLocation = null;
                if (vm.selectedLocations.length === 1) {
                    vm.selectedLocation = vm.selectedRows[0];
                }

                vm.members.loadforSelectedLocation(vm.selectedLocation);
            };
        }
    ]);
})();