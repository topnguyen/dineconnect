﻿(function () {
    appModule.controller("tenant.views.common.lookup.select", [
        "$scope", "$uibModalInstance", "uiGridConstants", "location",
        function ($scope, $modalInstance, uiGridConstants, location) {
            var vm = this;
            vm.selectedLocations = location.locations;
            vm.singleLocation = location.single;
            vm.categoryId = location.categoryId;
            vm.productGroupId = location.productGroupId;
            vm.menuItemId = location.menuItemId;
            vm.dataList = location.dataList;
            vm.saving = false;
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null,
                userId: abp.session.userId
            };

            vm.locationOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: "<div ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'text-muted': !row.entity.isActive }\"  ui-grid-cell></div>",
                columnDefs: [
                    {
                        name: app.localize("Select"),
                        cellTemplate:
                            "<div class=\"ui-grid-cell-contents text-center\">" +
                            "  <button ng-click=\"grid.appScope.pushLocation(row.entity)\" class=\"btn btn-default btn-xs\" title=\"" + app.localize("Select") + "\"><i class=\"fa fa-dashcube\"></i></button>" +
                            "</div>"
                    },
                    {
                        name: app.localize("Id"),
                        field: "id"
                    },
                    {
                        name: app.localize("Name"),
                        field: "name"
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + " " + sortColumns[0].sort.direction;
                        }
                        vm.getLocations();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;
                        vm.getLocations();
                    });
                },
                data: []
            };

            vm.pushLocation = function (myObj) {
                if (vm.selectedLocations.indexOf(myObj) === -1) {
                    vm.selectedLocations.push(myObj);
                }

                if (vm.singleLocation)
                    vm.save();
            };

            vm.getLocations = function () {
                
                if (vm.dataList) {
                    vm.locationOptions.totalItems = vm.dataList.totalCount;
                    vm.locationOptions.data = vm.dataList.items;
                }
                else {
                    vm.loading = true;
                    location.service({
                        skipCount: requestParams.skipCount,
                        maxResultCount: requestParams.maxResultCount,
                        sorting: requestParams.sorting,
                        filter: vm.filterLocationText,
                        userId: abp.session.userId,
                        categoryId: vm.categoryId,
                        productGroupId: vm.productGroupId,
                        menuItemId: vm.menuItemId
                    }).success(function (result) {
                        //console.log(result);

                        vm.locationOptions.totalItems = result.totalCount;
                        vm.locationOptions.data = result.items;
                    }).finally(function () {
                        vm.loading = false;
                    });
                }
            };

            vm.save = function () {
                location.locations = vm.selectedLocations;
                $modalInstance.close(location);
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.clear = function () {
                vm.selectedLocations = [];
                vm.filterLocationText = null;
                vm.getLocations();
            };
            vm.getLocations();
        }
    ]);
})();