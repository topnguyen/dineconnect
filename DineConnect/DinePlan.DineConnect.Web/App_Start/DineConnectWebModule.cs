﻿using Abp.Hangfire;
using Abp.IO;
using Abp.Modules;
using Abp.Web.Mvc;
using Abp.Web.SignalR;
using Abp.Zero.Configuration;
using Castle.MicroKernel.Registration;
using DinePlan.DineConnect.Web.App.Startup;
using DinePlan.DineConnect.Web.Bundling;
using DinePlan.DineConnect.Web.Navigation;
using DinePlan.DineConnect.Web.Routing;
using DinePlan.DineConnect.WebApi;
using Microsoft.Owin.Security;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Abp.Hangfire.Configuration;
using Hangfire;
using Abp.Dependency;
using DinePlan.DineConnect.Background;
using DinePlan.DineConnect.Job.Connect;

namespace DinePlan.DineConnect.Web
{
    /// <summary>
    /// Web module of the application.
    /// This is the most top and entrance module that dependens on others.
    /// </summary>
    [DependsOn(
        typeof(AbpWebMvcModule),
        typeof(DineConnectDataModule),
        typeof(DineConnectApplicationModule),
        typeof(DineConnectWebApiModule),
        typeof(AbpWebSignalRModule),
        typeof(AbpHangfireModule))] //AbpHangfireModule dependency can be removed if not using Hangfire
    public class DineConnectWebModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Modules.Zero().LanguageManagement.EnableDbLocalization();

            Configuration.Navigation.Providers.Add<AppNavigationProvider>();
            Configuration.Navigation.Providers.Add<FrontEndNavigationProvider>();

            Configuration.BackgroundJobs.UseHangfire(configuration =>
            {
                configuration.GlobalConfiguration.UseSqlServerStorage("Default");
            });
        }

        public override void Initialize()
        {
            //Dependency Injection
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            IocManager.IocContainer.Register(
                Component
                    .For<IAuthenticationManager>()
                    .UsingFactoryMethod(() => HttpContext.Current.GetOwinContext().Authentication)
                    .LifestyleTransient()
                );

            //Areas
            AreaRegistration.RegisterAllAreas();

            //Routes
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            //Bundling
            BundleTable.Bundles.IgnoreList.Clear();
            CommonBundleConfig.RegisterBundles(BundleTable.Bundles);
            AppBundleConfig.RegisterBundles(BundleTable.Bundles);
            //FrontEndBundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        public override void PostInitialize()
        {
            var server = HttpContext.Current.Server;
            var appFolders = IocManager.Resolve<AppFolders>();

            var imageFolder = ConfigurationManager.AppSettings["ImagesFolder"];
            appFolders.ImagesFolder = server.MapPath("~/Common/Images/SampleProfilePics");
            if (!string.IsNullOrEmpty(imageFolder))
            {
                appFolders.ImagesFolder = imageFolder;
            }
           
            appFolders.TicketJournalFolder = server.MapPath("~/_Tickets/Journals/");

            var ticketTolder = ConfigurationManager.AppSettings["TicketJournalFolder"];
            if (!string.IsNullOrEmpty(ticketTolder))
            {
                appFolders.TicketJournalFolder = ticketTolder;
            }

            appFolders.FullTaxInvoiceFolder = server.MapPath("~/_Tickets/FullTax/");

            var fullTaxFolder = ConfigurationManager.AppSettings["FullTaxInvoiceFolder"];
            if (!string.IsNullOrEmpty(fullTaxFolder))
            {
                appFolders.FullTaxInvoiceFolder = fullTaxFolder;
            }

            appFolders.TempFileDownloadFolder = server.MapPath("~/Temp/Downloads");
            appFolders.PdfExeFile = server.MapPath("~/Pdf/OpenHtmlToPdf.WkHtmlToPdf.exe");
            appFolders.ImportFolder = server.MapPath("~/Common/Import");
            appFolders.UploadFolder = server.MapPath("~/Common/Upload");
            appFolders.WebLogsFolder = server.MapPath($"{Path.DirectorySeparatorChar}Logs");

            try
            {
                DirectoryHelper.CreateIfNotExists(appFolders.TempFileDownloadFolder);
                DirectoryHelper.CreateIfNotExists(appFolders.UploadFolder);
            }
            catch { }

            IocManager.RegisterIfNot<Background.IRecurringJobManager, HangfireRecurringJobManager>();

            var workManager = IocManager.Resolve<Background.IRecurringJobManager>();
            //var cron = "0 1 * * *"; // 1AM everyday
            var cron = "*/5 * * * *";
            workManager.AddOrUpdateAsync<MenuPriceResetJob, MenuPriceResetJobArgs>(new MenuPriceResetJobArgs(), cron);
        }
    }
}