﻿using Abp.Owin;
using DinePlan.DineConnect.Web;
using DinePlan.DineConnect.Web.App_Start.Validator;
using DinePlan.DineConnect.WebApi.Controllers;
using LomaCons.InstallKey.ClientSide;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.Twitter;
using Owin;
using System;
using System.Configuration;
using System.Web.Services.Description;
using Abp.Authorization;
using Abp.Runtime.Session;
using DinePlan.DineConnect.Authorization;
using Hangfire;
using Hangfire.Dashboard;
using Abp.Timing;

[assembly: OwinStartup(typeof(Startup))]
namespace DinePlan.DineConnect.Web
{
    public class Startup
    {
        protected ClientSideValidator ClientValidator
        {
            get;
            private set;
        }

      

        public void Configuration(IAppBuilder app)
        {
            app.UseAbp();

            //TODO: License

            //app.Use(async (context, next) =>
            //{
            //    if (!IsValidated())
            //    {
            //        if (context.Request.Path.Value == "/Home/KeyEntry")
            //        {
            //            await next();
            //        }
            //        else
            //            context.Response.Redirect("/Home/KeyEntry");
            //    }
            //    else
            //    {
            //        await next();
            //    }
            //});

            app.UseOAuthBearerAuthentication(AccountController.OAuthBearerOptions);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login")
            });

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            if (IsTrue("ExternalAuth.Facebook.IsEnabled"))
            {
                app.UseFacebookAuthentication(CreateFacebookAuthOptions());
            }

            if (IsTrue("ExternalAuth.Twitter.IsEnabled"))
            {
                app.UseTwitterAuthentication(CreateTwitterAuthOptions());
            }

            if (IsTrue("ExternalAuth.Google.IsEnabled"))
            {
                app.UseGoogleAuthentication(CreateGoogleAuthOptions());
            }

            app.MapSignalR();
            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[] { new MyAuthorizationFilter() }
            });

        }

        private static FacebookAuthenticationOptions CreateFacebookAuthOptions()
        {
            var options = new FacebookAuthenticationOptions
            {
                AppId = ConfigurationManager.AppSettings["ExternalAuth.Facebook.AppId"],
                AppSecret = ConfigurationManager.AppSettings["ExternalAuth.Facebook.AppSecret"]
            };

            options.Scope.Add("email");
            options.Scope.Add("public_profile");

            return options;
        }

        private static TwitterAuthenticationOptions CreateTwitterAuthOptions()
        {
            return new TwitterAuthenticationOptions
            {
                ConsumerKey = ConfigurationManager.AppSettings["ExternalAuth.Twitter.ConsumerKey"],
                ConsumerSecret = ConfigurationManager.AppSettings["ExternalAuth.Twitter.ConsumerSecret"]
            };
        }

        private static GoogleOAuth2AuthenticationOptions CreateGoogleAuthOptions()
        {
            return new GoogleOAuth2AuthenticationOptions
            {
                ClientId = ConfigurationManager.AppSettings["ExternalAuth.Google.ClientId"],
                ClientSecret = ConfigurationManager.AppSettings["ExternalAuth.Google.ClientSecret"]
            };
        }

        private static bool IsTrue(string appSettingName)
        {
            return string.Equals(
                ConfigurationManager.AppSettings[appSettingName],
                "true",
                StringComparison.InvariantCultureIgnoreCase);
        }

        private bool IsValidated()
        {
            ClientValidator = new ClientSideValidator(
                ConfigurationManager.AppSettings["KeysetName"],
                ConfigurationManager.AppSettings["Password"],
                ConfigurationManager.AppSettings["FactorCode"],
                ConfigurationManager.AppSettings["ValidationCode"]
                );
            ClientValidator.SetServiceAddress(ConfigurationManager.AppSettings["ValidationUrl"]);
            ClientValidator.SuretyStorage = new DatabaseStorage("Default");
            ClientValidator.IdentifierFinders.Clear();
            ClientValidator.IdentifierFinders.Add(new MachineNameIdentifierFinder());

            var myLocalLicense = ClientValidator.IsValidated;
            return myLocalLicense;
        }
    }

    public class MyAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            // In case you need an OWIN context, use the next line, `OwinContext` class
            // is the part of the `Microsoft.Owin` package.
            var owinContext = new OwinContext(context.GetOwinEnvironment());

            // Allow all authenticated users to see the Dashboard (potentially dangerous).
            return owinContext.Authentication.User.Identity.IsAuthenticated;
        }
    }

   
}