﻿using LomaCons.InstallKey.ClientSide;
using LomaCons.InstallKey.Common;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Web.App_Start.Validator
{
    /// <summary>
    /// custom identifier finder
    /// </summary>
    public class MachineNameIdentifierFinder : IIdentifierFinder
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public MachineNameIdentifierFinder()
        {
            return;
        }

        /// <summary>
        /// the name of this finder
        /// </summary>
        public string Name
        {
            get { return ("MachineNameFinder"); }
        }

        /// <summary>
        /// called by the ClientValidator when we need to get the identifers
        /// </summary>
        /// <remarks>
        /// This will be called by the ValidateAgainstServer method and the
        /// IsValidated property
        /// </remarks>
        /// <returns>a list of identifiers</returns>
        public IList<InstallIdentifier> GetIdentifiers()
        {
            List<InstallIdentifier> list;
            InstallIdentifier identifer;

            // create the list object to return
            list = new List<InstallIdentifier>();

            // create an identifier object that contains the machine name
            // the identifier object will automatically hash the machine name
            // this is the object that will be added to the surety and
            // contains the values that will be persisted to the server database
            identifer = new InstallIdentifier("MachineName", Environment.MachineName);

            // add the identifier to the list and return
            list.Add(identifer);
            return (list);
        }
    }
}