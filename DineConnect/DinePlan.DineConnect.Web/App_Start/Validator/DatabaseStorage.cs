﻿using LomaCons.InstallKey.ClientSide;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DinePlan.DineConnect.Web.App_Start.Validator
{
    /// <summary>
    /// Database storage object will persist the surety to/from a database record
    /// </summary>
    public class DatabaseStorage : ISuretyStorage
    {
        private string _connectionStringName;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="connectionStringName"></param>
        public DatabaseStorage(string connectionStringName)
        {
            _connectionStringName = connectionStringName;
            return;
        }

        /// <summary>
        /// required for the interface, but not used in this sample
        /// </summary>
        /// <param name="keysetName"></param>
        public void Initialize(string keysetName)
        {
            return;
        }

        /// <summary>
        /// put the content in the database
        /// </summary>
        /// <param name="surety"></param>
        public void PersistSurety(string surety)
        {
            SqlConnection conn;
            SqlCommand cmd;

            conn = new SqlConnection(ConfigurationManager.ConnectionStrings[_connectionStringName].ConnectionString);
            using (conn)
            {
                conn.Open();
                cmd = new SqlCommand();
                using (cmd)
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText =
                        "IF EXISTS(SELECT * FROM dbo.SuretyStorage WHERE PKSuretyStorage = 1) BEGIN " +
                        "    UPDATE dbo.SuretyStorage SET Content = @content WHERE PKSuretyStorage = 1 " +
                        "END ELSE BEGIN " +
                        "    INSERT INTO dbo.SuretyStorage (PKSuretyStorage, Content) VALUES (1, @content) " +
                        "END";
                    cmd.Connection = conn;
                    cmd.Parameters.Add(new SqlParameter("@content", surety));
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
            return;
        }

        /// <summary>
        /// read the content from the database
        /// </summary>
        /// <returns></returns>
        public string RestoreSurety()
        {
            SqlConnection conn;
            SqlCommand cmd;
            object result;

            conn = new SqlConnection(ConfigurationManager.ConnectionStrings[_connectionStringName].ConnectionString);
            using (conn)
            {
                conn.Open();
                cmd = new SqlCommand();
                using (cmd)
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT Content FROM dbo.SuretyStorage WHERE PKSuretyStorage = 1";
                    cmd.Connection = conn;
                    result = cmd.ExecuteScalar();
                }
                conn.Close();
            }
            if (result == null || result == DBNull.Value)
                return (null);
            return (Convert.ToString(result));
        }
    }
}