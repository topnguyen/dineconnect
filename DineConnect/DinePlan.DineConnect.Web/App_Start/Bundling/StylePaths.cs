namespace DinePlan.DineConnect.Web.Bundling
{
    public static class StylePaths
    {
        public const string JQuery_UI = "~/libs/jquery-ui/jquery-ui.min.css";
        public const string Simple_Line_Icons = "~/libs/simple-line-icons/simple-line-icons.min.css";
        public const string Bootstrap = "~/libs/bootstrap/css/bootstrap.min.css";
        public const string BootstrapRTL = "~/libs/bootstrap/css/bootstrap-rtl.min.css";
        public const string SweetAlert = "~/libs/sweetalert/sweet-alert.css";
        public const string Toastr = "~/libs/toastr/toastr.min.css";
        public const string FontAwesome = "~/Content/font-awesome.min.css";
        public const string FamFamFamFlags = "~/Content/flags/famfamfam-flags.css";
        public const string JQuery_Uniform = "~/libs/jquery-uniform/css/uniform.default.css";
        public const string JsTree = "~/libs/jstree/themes/default/style.css";
        public const string Angular_Ui_Grid = "~/libs/angular-ui-grid/ui-grid.min.css";
        public const string Uig = "~/libs/ownScript/uig.css";
        public const string Angular_Ui_Select = "~/libs/ui-select/select.min.css";
        public const string Angular_Multi_Select = "~/libs/multiple-dp/multipleDatePicker.css";
        public const string Angular_AutoComplete = "~/libs/autocomplete/angucomplete-alt.css";
        public const string Angular_HotKey = "~/libs/hotkey/hotkeys.min.css";
        public const string Bootstrap_DateRangePicker = "~/libs/bootstrap-daterangepicker/daterangepicker.css";
        public const string Bootstrap_DateTimePicker = "~/Content/bootstrap-datetimepicker.css";
        public const string Bootstrap_FileInput = "~/metronic/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css";
        public const string Bootstrap_Select = "~/libs/bootstrap-select/bootstrap-select.min.css";
        public const string Bootstrap_Switch = "~/libs/bootstrap-switch/css/bootstrap-switch.min.css";
        public const string JQuery_jTable_Theme = "~/libs/jquery-jtable/themes/metro/blue/jtable.min.css";
        public const string Angular_Bootstrap_DateTimePicker = "~/libs/angular-bootstrap-datetimepicker/src/css/datetimepicker.css";
        public const string Angular_Ranger = "~/libs/rangeslider/angular-time-picker.min.css";
        public const string Angular_ngTable = "~/libs/ngtable/ng-table.min.css";
        public const string Angular_Color = "~/libs/color-picker/color-picker.min.css";
        public const string Angular_Timeline = "~/libs/timeline/angular-timeline.css";
        public const string JQuery_FileUpload = "~/metronic/assets/global/plugins/jquery-file-upload/css/*.css";
        public const string Angular_Time_Ranger = "~/libs/datetimeranger/datetime-range.min.css";

        /*QueryBuilder*/
        public const string Angular_QB = "~/libs/query/query-builder.default.min.css";
        public const string Angular_QB_Selectize = "~/libs/query/selectize.default.css";


        public const string Summernote = "~/Content/summernote.css";
        public const string MultiSelectList = "~/libs/multiselect/dualmultiselect.css";

        public const string FullCalendar = "~/Content/fullcalendar.css";
        public const string NgTagsInput = "~/libs/ng-tags-input/ng-tags-input.css";
        public const string NgQuill = "~/libs/quill/quill.snow.css";
        public const string NgQuillBubble = "~/libs/quill/quill.bubble.css";
        public const string File_Manager = "~/libs/file-manager/angular-filemanager.min.css";


        public const string Angular_Simple_Color = "~/libs/simplecolor/angular-colorpicker-dr.css";

        //GrapeJs
        public const string GrapesJs = "~/libs/grapesjs/css/grapes.min.css";

    }
}