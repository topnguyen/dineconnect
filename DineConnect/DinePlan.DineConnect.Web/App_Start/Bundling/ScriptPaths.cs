﻿using Abp.Extensions;
using System.IO;
using System.Threading;
using System.Web;

namespace DinePlan.DineConnect.Web.Bundling
{
    public static class ScriptPaths
    {
        public const string Json2 = "~/libs/json2/json2.min.js";

        public const string JQuery = "~/libs/jquery/jquery.min.js";
        public const string JQuery_Migrate = "~/libs/jquery/jquery-migrate.min.js";
        public const string JQuery_UI = "~/libs/jquery-ui/jquery-ui.min.js";
        public const string jsPdf = "~/libs/jsPDF-1.2.60/jspdf.js";
        public const string JQuery_Slimscroll = "~/libs/jquery-slimscroll/jquery.slimscroll.min.js";
        public const string JQuery_BlockUi = "~/libs/jquery-blockui/jquery.blockui.min.js";
        public const string JQuery_Cookie = "~/libs/jquery-cookie/jquery.cookie.min.js";
        public const string JQuery_Uniform = "~/libs/jquery-uniform/jquery.uniform.min.js";
        public const string JQuery_Ajax_Form = "~/libs/jquery-ajax-form/jquery.form.js";
        public const string JQuery_Sparkline = "~/libs/jquery-sparkline/jquery.sparkline.min.js";
        public const string JQuery_Validation = "~/libs/jquery-validation/js/jquery.validate.min.js";
        public const string JQuery_jTable = "~/libs/jquery-jtable/jquery.jtable.min.js";
        public const string JQuery_Bootstrap_Switch = "~/libs/bootstrap-switch/js/bootstrap-switch.min.js";

        public const string Bootstrap = "~/libs/bootstrap/js/bootstrap.min.js";
        public const string Bootstrap_Hover_Dropdown = "~/libs/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js";
        public const string Bootstrap_DateRangePicker = "~/libs/bootstrap-daterangepicker/daterangepicker.js";

        public const string Bootstrap_FileInput = "~/metronic/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js";
        public const string Bootstrap_Select = "~/libs/bootstrap-select/bootstrap-select.min.js";
        public const string Bootstrap_Switch = "~/libs/bootstrap-switch/js/bootstrap-switch.min.js";

        public const string SignalR = "~/Scripts/jquery.signalR-2.2.0.min.js";

        public const string JsTree = "~/libs/jstree/jstree.js";
        public const string SpinJs = "~/libs/spinjs/spin.js";
        public const string SpinJs_JQuery = "~/libs/spinjs/jquery.spin.js";

        public const string SweetAlert = "~/libs/sweetalert/sweet-alert.min.js";
        public const string Toastr = "~/libs/toastr/toastr.min.js";

        public const string MomentJs = "~/Scripts/moment-with-locales.min.js";
        public const string Underscore = "~/Scripts/underscore.min.js";

        public const string MustacheJs = "~/libs/mustachejs/mustache.min.js";

        public const string Angular = "~/Scripts/angular.min.js";
        public const string Angular_Sanitize = "~/Scripts/angular-sanitize.min.js";
        public const string Angular_Touch = "~/Scripts/angular-touch.min.js";
        public const string Angular_Ui_Router = "~/Scripts/angular-ui-router.min.js";
        public const string Angular_Ui_Utils = "~/Scripts/angular-ui/ui-utils.min.js";
        public const string Angular_Ui_Bootstrap_Tpls = "~/Scripts/angular-ui/ui-bootstrap-tpls.min.js";
        public const string Angular_Ui_Grid = "~/libs/angular-ui-grid/ui-grid.min.js";
        public const string Angular_Ui_Select = "~/libs/ui-select/select.min.js";
        public const string Angular_Ui_MultiDate = "~/libs/multiple-dp/multipleDatePicker.min.js";
        public const string Angular_Index_DB = "~/libs/indexdb/indexdb.js";
        public const string Angular_AutoComplete = "~/libs/autocomplete/angucomplete-alt.js";
        public const string Angular_HotKey = "~/libs/hotkey/hotkeys.min.js";
        public const string Angular_MouseTrap = "~/libs/hotkey/mousetrap.min.js";
        public const string Angular_Drag = "~/libs/dragula/dragula.min.js";
        public const string Angular_ChartJs = "~/libs/chartjs/Chart.min.js";
        public const string Angular_ChartJsLib = "~/libs/chartjs/angular-chart.min.js";

        public const string Angular_OcLazyLoad = "~/libs/angular-ocLazyLoad/ocLazyLoad.min.js";
        public const string Angular_File_Upload = "~/libs/angular-file-upload/angular-file-upload.min.js";
        public const string Angular_File_Thumb = "~/libs/angular-file-upload/thumb.js";

        public const string Angular_DateRangePicker = "~/libs/angular-daterangepicker/angular-daterangepicker.min.js";
        public const string Bootstrap_DateTimePicker = "~/Scripts/bootstrap-datetimepicker.js";
        public const string Angular_Moment = "~/libs/angular-moment/angular-moment.min.js";
        public const string Angular_Bootstrap_Switch = "~/libs/angular-bootstrap-switch/angular-bootstrap-switch.min.js";
        public const string Angular_Bootstrap_Datetimepicker = "~/libs/angular-bootstrap-datetimepicker/src/js/datetimepicker.js";
        public const string Angular_Bootstrap_Datetimeinput = "~/libs/angular-bootstrap-datetimepicker/src/js/datetimeinput.js";
        public const string Angular_Bootstrap_validate = "~/libs/angular-bootstrap-datetimepicker/src/js/validate.js";

        public const string Angular_Bootstrap_Datetimepicker_Template = "~/libs/angular-bootstrap-datetimepicker/src/js/datetimepicker.templates.js";
        public const string Time_Ranger = "~/libs/rangeslider/angular-time-picker.min.js";
        public const string NgTable = "~/libs/ngtable/ng-table.min.js";
        public const string Angular_ColorPicker = "~/libs/color-picker/color-picker.min.js";
        public const string Angular_Sortable = "~/libs/ui-sortable/sortable.js";

        public const string Abp = "~/Abp/Framework/scripts/abp.js";
        public const string Abp_JQuery = "~/Abp/Framework/scripts/libs/abp.jquery.js";
        public const string Abp_Toastr = "~/Abp/Framework/scripts/libs/abp.toastr.js";
        public const string Abp_BlockUi = "~/Abp/Framework/scripts/libs/abp.blockUI.js";
        public const string Abp_SpinJs = "~/Abp/Framework/scripts/libs/abp.spin.js";
        public const string Abp_SweetAlert = "~/Abp/Framework/scripts/libs/abp.sweet-alert.js";
        public const string Abp_Angular = "~/Abp/Framework/scripts/libs/angularjs/abp.ng.js";

        public const string HighCharts = "~/libs/highcharts/highcharts.js";

        public const string Angular_Device_Detector = "~/libs/ng-device-detector/ng-device-detector.min.js";
        public const string Angular_Re_Tree = "~/libs/re-tree/re-tree.min.js";
        public const string Angualr_DateTimeRanger = "~/libs/datimeranger/datetime-range.min.js";

        public const string TimeLine = "~/libs/timeline/angular-timeline.js";
        public const string JQuery_FileUpload = "~/metronic/assets/global/plugins/jquery-file-upload/css/*.js";

        /*QueryBuilder*/
        public const string QueryBuilderDirective = "~/libs/query/qbdirective.js";
        public const string QueryBuilderExtender = "~/libs/query/jQuery.extendext.min.js";
        public const string QueryBuilderDot = "~/libs/query/doT.min.js";
        public const string QueryBuilder = "~/libs/query/query-builder.min.js";
        public const string QueryBuilder_Selectize = "~/libs/query/selectize.min.js";


        /*Summernote*/
        public const string Summernote = "~/libs/summernote/summernote.min.js";
        public const string AnSummernote = "~/libs/summernote/angular-summernote.min.js";

        public const string MultiSelectList = "~/libs/multiselect/dualmultiselect.min.js";

        public const string FullCalendar = "~/Scripts/fullcalendar.js";
        public const string Calendar = "~/Scripts/calendar.js";

        public const string NgTagsInput = "~/libs/ng-tags-input/ng-tags-input.js";

        public const string Quill = "~/libs/quill/quill.js";
        public const string NgQuill = "~/libs/quill/ng-quill.js";
        public const string Ui_Sortable = "~/libs/ui-sortable-0.19.0/sortable.js";

        public const string File_Manager = "~/libs/file-manager/angular-filemanager.min.js";
        public const string NgFileUpload = "~/libs/ng-file-upload/ng-file-upload.min.js";
        public const string Angular_Translate = "~/libs/angular-translate/angular-translate.min.js";
        public const string Angular_UiGrid_AutoFitColumn = "~/libs/angular-ui-grid/ui-grid-auto-fit-column.min.js";

        public const string AngularSmallColor = "~/libs/simplecolor/angular-colorpicker-dr.js";

        //Formly
        public const string AngularFormly = "~/libs/formly/formly.js";
        public const string AngularFormlyTemplates = "~/libs/formly/formly-templates.js";
        public const string AngularFormlyApi = "~/libs/formly/api-check.min.js";

        //GrapeJs
        public const string GrapesJs = "~/libs/grapesjs/grapes.min.js";

        public static string Angular_Localization
        {
            get
            {
                return GetLocalizationFileForjAngularOrNull(Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                       ?? GetLocalizationFileForjAngularOrNull(Thread.CurrentThread.CurrentUICulture.Name.Left(2).ToLower())
                       ?? "~/Scripts/i18n/angular-locale_en-us.js";
            }
        }

        private static string GetLocalizationFileForjAngularOrNull(string cultureCode)
        {
            try
            {
                var relativeFilePath = "~/Scripts/i18n/angular-locale_" + cultureCode + ".js";
                var physicalFilePath = HttpContext.Current.Server.MapPath(relativeFilePath);
                if (File.Exists(physicalFilePath))
                {
                    return relativeFilePath;
                }
            }
            catch { }

            return null;
        }

        public static string JQuery_Validation_Localization
        {
            get
            {
                return GetLocalizationFileForjQueryValidationOrNull(Thread.CurrentThread.CurrentUICulture.Name.ToLower().Replace("-", "_"))
                       ?? GetLocalizationFileForjQueryValidationOrNull(Thread.CurrentThread.CurrentUICulture.Name.Left(2).ToLower())
                       ?? "~/libs/jquery-validation/js/localization/_messages_empty.js";
            }
        }

        private static string GetLocalizationFileForjQueryValidationOrNull(string cultureCode)
        {
            try
            {
                var relativeFilePath = "~/libs/jquery-validation/js/localization/messages_" + cultureCode + ".min.js";
                var physicalFilePath = HttpContext.Current.Server.MapPath(relativeFilePath);
                if (File.Exists(physicalFilePath))
                {
                    return relativeFilePath;
                }
            }
            catch { }

            return null;
        }

        public static string JQuery_JTable_Localization
        {
            get
            {
                return GetLocalizationFileForJTableOrNull(Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                       ?? GetLocalizationFileForJTableOrNull(Thread.CurrentThread.CurrentUICulture.Name.Left(2).ToLower())
                       ?? "~/libs/jquery-jtable/localization/_jquery.jtable.empty.js";
            }
        }

        private static string GetLocalizationFileForJTableOrNull(string cultureCode)
        {
            try
            {
                var relativeFilePath = "~/libs/jquery-jtable/localization/jquery.jtable." + cultureCode + ".js";
                var physicalFilePath = HttpContext.Current.Server.MapPath(relativeFilePath);
                if (File.Exists(physicalFilePath))
                {
                    return relativeFilePath;
                }
            }
            catch { }

            return null;
        }

        public static string Bootstrap_Select_Localization
        {
            get
            {
                return GetLocalizationFileForBootstrapSelect(Thread.CurrentThread.CurrentUICulture.Name.ToLower())
                       ?? GetLocalizationFileForBootstrapSelect(Thread.CurrentThread.CurrentUICulture.Name.Left(2).ToLower())
                       ?? "~/libs/bootstrap-select/i18n/defaults-en_US.js";
            }
        }

        private static string GetLocalizationFileForBootstrapSelect(string cultureCode)
        {
            var localizationFileList = new[]
            {
                "ar_AR",
                "bg_BG",
                "cs_CZ",
                "da_DK",
                "de_DE",
                "en_US",
                "es_CL",
                "eu",
                "fa_IR",
                "fi_FI",
                "fr_FR",
                "hu_HU",
                "id_ID",
                "it_IT",
                "ko_KR",
                "nb_NO",
                "nl_NL",
                "pl_PL",
                "pt_BR",
                "pt_PT",
                "ro_RO",
                "ru_RU",
                "sk_SK",
                "sl_SL",
                "sv_SE",
                "tr_TR",
                "ua_UA",
                "zh_CN",
                "zh_TW"
            };

            try
            {
                cultureCode = cultureCode.Replace("-", "_");

                foreach (var localizationFile in localizationFileList)
                {
                    if (localizationFile.StartsWith(cultureCode))
                    {
                        return "~/libs/bootstrap-select/i18n/defaults-" + localizationFile + ".js";
                    }
                }
            }
            catch { }

            return null;
        }
    }
}