namespace DinePlan.DineConnect.Web.Navigation
{
    public static class PageNames
    {
        public static class App
        {
            public static class Common
            {
                public const string Administration = "Administration";
                public const string Roles = "Administration.Roles";
                public const string Users = "Administration.Users";
                public const string AuditLogs = "Administration.AuditLogs";
                public const string SystemLogs = "Administration.SystemLogs";
                public const string UserLocation = "Administration.UserLocation";
                public const string OrganizationUnits = "Administration.OrganizationUnits";
                public const string Languages = "Administration.Languages";
                public const string LastSync = "Administration.LastSync";
                public const string SyncDashboard = "Administration.SyncDashboard";
                public const string CustomReport = "Administration.CustomReport";
                public const string Templates = "Administration.Templates";
                public const string TickMessage = "Administration.TickMessage";
                public const string TrackerLogs = "Administration.TrackerLogs";
                public const string ExternalLogs = "Administration.ExternalLogs";
                public const string DinePlanLanguages = "Administration.DinePlanLanguages";
                public const string Maintenance = "Administration.Maintenance";
                public const string SMTPSetting = "Administration.SMTPSetting";
            }

            public static class Host
            {
                public const string Tenants = "Tenants";
                public const string Editions = "Editions";
                public const string Settings = "Administration.Settings.Host";

                public const string Swipe = "Host.Swipe";
            }

            public static class Swipe
            {
                public const string Dashboard = "Host.Swipe.Dashboard";
                public const string Master = "Host.Swipe.Master";
                public const string Transaction = "Host.Swipe.Transaction";

                public const string SwipeMember = "Host.Swipe.Master.SwipeMember";

                public const string MemberCard = "Host.Swipe.Transaction.MemberCard";
                public const string SwipeCardType = "Host.Swipe.Master.SwipeCardType";
                public const string SwipeCard = "Host.Swipe.Master.SwipeCard";
                public const string SwipePaymentType = "Host.Swipe.Master.SwipePaymentType";

                public const string SwipeShift = "Host.Swipe.Transaction.SwipeShift";
            }

            public static class Tenant
            {
                public const string Dashboard = "Dashboard.Tenant";
                public const string Settings = "Administration.Settings.Tenant";

                public const string Connect = "Tenant.Connect";
                public const string House = "Tenant.House";
                public const string Cluster = "Tenant.Cluster";
                public const string Addons = "Tenant.Addons";
                public const string Engage = "Tenant.Engage";

                public const string Tick = "Tenant.Tick";
                public const string Wheel = "Tenant.Wheel";
                public const string Cater = "Tenant.Cater";
                public const string Go = "Tenant.Go";

                public const string Play = "Tenant.Play";
            }

            public static class Hr
            {
                public const string EmployeeDashboard = "Tenant.Hr.EmployeeDashboard";

                public const string Master = "Tenant.Hr.Master";
                public const string Employee = "Tenant.Hr.Employee";
                public const string Operation = "Tenant.Hr.Operation";
                public const string SkillSet = "Tenant.Hr.Master.SkillSet";
                public const string PersonalInformation = "Tenant.Hr.Master.PersonalInformation";
                public const string EmployeeSkillSet = "Tenant.Hr.Master.EmployeeSkillSet";
                public const string EmployeeDocumentInfo = "Tenant.Hr.Master.EmployeeDocumentInfo";
                public const string DocumentInfo = "Tenant.Hr.Master.DocumentInfo";
                public const string YearWiseLeaveAllowedForEmployee = "Tenant.Hr.Master.YearWiseLeaveAllowedForEmployee";

                public const string LeaveType = "Tenant.Hr.Master.LeaveType";
                public const string LeaveRequest = "Tenant.Hr.Master.LeaveRequest";
                public const string SalarySlip = "Tenant.Hr.Master.SalarySlip";
                public const string SalaryInfo = "Tenant.Hr.Master.SalaryInfo";

                public const string WorkDay = "Tenant.Hr.Master.WorkDay";
                public const string MonthWiseWorkDay = "Tenant.Hr.Master.MonthWiseWorkDay";
                public const string PublicHoliday = "Tenant.Hr.Master.PublicHoliday";

                public const string DcHeadMaster = "Tenant.Hr.Master.DcHeadMaster";
                public const string DcGroupMaster = "Tenant.Hr.Master.DcGroupMaster";

                public const string DcStationMaster = "Tenant.Hr.Master.DcStationMaster";
                public const string DcShiftMaster = "Tenant.Hr.Master.DcShiftMaster";

                public const string DutyChart = "Tenant.Hr.Master.DutyChart";

                public const string SalaryCost = "Tenant.Hr.Master.SalaryCost";

                public const string IncentiveCategory = "Tenant.Hr.Transaction.IncentiveCategory";
                public const string IncentiveTag = "Tenant.Hr.Transaction.IncentiveTag";
                public const string ManualIncentive = "Tenant.Hr.Transaction.ManualIncentive";

                public const string JobTitleMaster = "Tenant.Hr.Master.JobTitleMaster";
            }

            public static class DineConnect
            {
                public const string ProgramSettingTemplate = "Tenant.Connect.ProgramSetting.Template";
                public const string ProgramSetting = "Tenant.Connect.ProgramSetting.Settings";
                public const string ProgramSettingPTTOrSetting = "Tenant.Connect.ProgramSetting.PTTOrSetting";
                public const string ProgramSettings = "Tenant.Connect.ProgramSetting";
                public const string ExternalDelivery = "Tenant.Connect.ExternalDelivery";
                public const string FutureData = "Tenant.Connect.FutureData";
                public const string ReceiptContent = "Tenant.Connect.ReceiptContent";
                public const string Print = "Tenant.Connect.Print";
                public const string Printer = "Tenant.Connect.Printer";
                public const string PrintTemplate = "Tenant.Connect.PrintTemplate";
                public const string PrintJob = "Tenant.Connect.PrintJob";
                public const string PrintTemplateCondition = "Tenant.Connect.PrintTemplateCondition";
                public const string DineDevice = "Tenant.Connect.DineDevice";
                public const string Terminal = "Tenant.Connect.Terminal";
                public const string ImportSetting = "Tenant.Connect.ImportSetting";
                public const string FileManager = "Tenant.Connect.FileManager";
                public const string CustomSetting = "Tenant.Connect.CustomSetting";

                public const string Master = "Tenant.Connect.Master";
                public const string Location = "Tenant.Connect.Master.Location";
                public const string LocationGroup = "Tenant.Connect.Master.LocationGroup";
                public const string TillAccount = "Tenant.Connect.Master.TillAccount";
                public const string LocationTag = "Tenant.Connect.Master.LocationTag";

                public const string Company = "Tenant.Connect.Master.Company";
                public const string Department = "Tenant.Connect.Master.Department";
                public const string DepartmentGroup = "Tenant.Connect.Master.DepartmentGroup";
                public const string PlanReason = "Tenant.Connect.Master.PlanReason";

                public const string TransactionType = "Tenant.Connect.Master.TransactionType";
                public const string PaymentType = "Tenant.Connect.Master.PaymentType";
                public const string TicketType = "Tenant.Connect.Master.TicketType";
                public const string Numerator = "Tenant.Connect.Master.Numerator";
                public const string ForeignCurrency = "Tenant.Connect.Master.ForeignCurrency";

                public const string Menu = "Tenant.Connect.Menu";
                public const string Category = "Tenant.Connect.Menu.Category";
                public const string ProductGroup = "Tenant.Connect.Menu.ProductGroup";
                public const string MenuItem = "Tenant.Connect.Menu.MenuItem";
                public const string ScreenMenu = "Tenant.Connect.Menu.ScreenMenu";
                public const string ComboGroup = "Tenant.Connect.Menu.ComboGroup";
                public const string DeliveryAggregators = "Tenant.Connect.Menu.DeliveryAggregators";

                public const string Table = "Tenant.Connect.Table";
                public const string TableGroup = "Tenant.Connect.Table.Group";
                public const string ConnectTable = "Tenant.Connect.Table.ConnectTable";

                public const string LocationMenuPrice = "Tenant.Connect.Menu.LocationMenuPrice";
                public const string PriceTag = "Tenant.Connect.Menu.PriceTag";
                public const string OrderTagGroup = "Tenant.Connect.Menu.OrderTagGroup";
                public const string TicketTagGroup = "Tenant.Connect.Menu.TicketTagGroup";

                public const string Report = "Tenant.Connect.Report";
                public const string Tickets = "Tenant.Connect.Report.Tickets";
                public const string PromotionCategory = "Tenant.Connect.Menu.PromotionCategory";
                public const string Promotion = "Tenant.Connect.Report.Promotion";
                public const string Departments = "Tenant.Connect.Report.Departments";
                public const string DepartmentSalesReport = "Tenant.Connect.Report.Sales.Department";
                public const string DepartmentItemSalesReport = "Tenant.Connect.Report.Item.Sales.Department";
                public const string TillTransaction = "Tenant.Connect.Report.TillTransaction";

                public const string Customize = "Tenant.Connect.Report.Customize";

                public const string ProductMixReport = "Tenant.Connect.Report.ProductMixReport";

                public const string TopDownSellReport = "Tenant.Connect.Report.Customize.TopDownSellReport";
                public const string TopDownSellReportQuantitySummary = "Tenant.Connect.Report.Customize.TopDownSellReport.QuantitySummary";
                public const string TopDownSellReportAmountSummary = "Tenant.Connect.Report.Customize.TopDownSellReport.AmountSummary";
                public const string TopDownSellReportQuantityPlant = "Tenant.Connect.Report.Customize.TopDownSellReport.QuantityPlant";
                public const string TopDownSellReportAmountPlant = "Tenant.Connect.Report.Customize.TopDownSellReport.AmountPlant";
                public const string SalesTaxReport = "Tenant.Connect.Report.SalesTaxReport";
                public const string NonSalesTaxReport = "Tenant.Connect.Report.NonSalesTaxReport";
                public const string CashAudit = "Tenant.Connect.Report.CashAudit";

                public const string Comparision = "Tenant.Connect.Report.Comparision ";
                public const string MonthlySales = "Tenant.Connect.Report.MonthlySales";
                public const string MonthlySalesAdvance = "Tenant.Connect.Report.MonthlySalesAdvance";
                public const string DailySalesBreak = "Tenant.Connect.Report.DailySalesBreak";
                public const string MealTimeSales = "Tenant.Connect.Report.MealTimeSales";
                public const string SalesByPeriod = "Tenant.Connect.Report.SalesByPeriod";

                public const string SalesPromotion = "Tenant.Connect.Report.SalesPromotion";
                public const string SalesPromotionByPromotion = "Tenant.Connect.Report.SalesPromotionReport.SalesPromotionByPromotion";
                public const string SalesPromotionByPlant = "Tenant.Connect.Report.SalesPromotionReport.SalesPromotionByPlant";

                public const string MonthlyStoreSalesStats = "Tenant.Connect.Report.MonthlyStoreSalesStats";

                public const string Orders = "Tenant.Connect.Report.Orders";
                public const string OrderTags = "Tenant.Connect.Report.OrderTags";
                public const string OrderExchange = "Tenant.Connect.Report.Exchange";
                public const string ReturnProduct = "Tenant.Connect.Report.ReturnProduct";
                public const string Items = "Tenant.Connect.Report.Items";
                public const string ItemReport = "Tenant.Connect.Report.Items.Item";
                public const string MenuEngineering = "Tenant.Connect.Report.Items.MenuEngineering";
                public const string CategoryReport = "Tenant.Connect.Report.Items.Category";
                public const string ScheduleReport = "Tenant.Connect.Report.Items.Schedule";
                public const string DepartmentReport = "Tenant.Connect.Report.Items.Department";
                public const string ItemHourlySalesReport = "Tenant.Connect.Report.Items.HourlySales";
                public const string Date = "Tenant.Connect.Report.Date";
                public const string DateItemReport = "Tenant.Connect.Report.Date.DateItemReport";
                public const string TopBottomItemReport = "Tenant.Connect.Report.Items.TopBottomItem";
                public const string GroupReport = "Tenant.Connect.Report.Items.Group";
                public const string ItemTagReport = "Tenant.Connect.Report.Items.ItemTag";
                public const string PaymentExcessReport = "Tenant.Connect.Report.PaymentExcess";
                public const string ExchangeReport = "Tenant.Connect.Report.ExchangeReport";
                public const string DailyReconciliation = "Tenant.Connect.Report.DailyReconciliation";
                public const string InternalSaleMeetingReport = "Tenant.Connect.Report.InternalSaleMeetingReport";
                public const string CollectionReport = "Tenant.Connect.Report.Collection";
                public const string CashAuditReport = "Tenant.Connect.Report.CashAuditReport";
                public const string CreditCardReport = "Tenant.Connect.Report.CreditCardReport";
                public const string CreditSalesReport = "Tenant.Connect.Report.CreditSalesReport";
                public const string AbnormalEndDay = "Tenant.Connect.Report.AbnormalEndDay";
                public const string FullTaxInvoice = "Tenant.Connect.Report.Customize.FullTaxInvoice";

                public const string GST = "Tenant.Connect.Report.GST";
                public const string GSTItem = "Tenant.Connect.Report.GST.Item";

                public const string Summary = "Tenant.Connect.Report.Summary";
                public const string DatewiseSale = "Tenant.Connect.Report.Summary.DatewiseSale";
                public const string SaleSummaryNew = "Tenant.Connect.Report.Summary.SaleNewSummary";
                public const string DatewisePayment = "Tenant.Connect.Report.Summary.DatewisePayment";
                public const string DatewiseTransaction = "Tenant.Connect.Report.Summary.DatewiseTransaction";
                public const string WorkDaySummary = "Tenant.Connect.Report.WorkDay.Summary";
                public const string PlanWorkDay = "Tenant.Connect.Report.PlanWorkDay.Summary";
                public const string WeekDaySummary = "Tenant.Connect.Report.WeekDay.Summary";
                public const string WeekEndSummary = "Tenant.Connect.Report.WeekEnd.Summary";
                public const string LogSummary = "Tenant.Connect.Report.Summary.Log";

                public const string Thailand = "Tenant.Connect.Report.Thailand";
                public const string ThailandReport = "Tenant.Connect.Report.Thailand.Item";
                public const string ThailandReportDetails = "Tenant.Connect.Report.Thailand.Details";

                public const string Hour = "Tenant.Connect.Report.Hour";
                public const string HourSummary = "Tenant.Connect.Report.Hour.Summary";
                public const string LocationSummary = "Tenant.Connect.Report.Hour.LocationSummary";

                public const string User = "Tenant.Connect.User";
                public const string DinePlanUser = "Tenant.Connect.User.DinePlanUser";
                public const string DinePlanUserRole = "Tenant.Connect.User.DinePlanUserRole";

                public const string DinePlanTax = "Tenant.Connect.Master.DinePlanTax";
                public const string Calculation = "Tenant.Connect.Master.Calculation";

                public const string Card = "Tenant.Connect.Card";
                public const string CardType = "Tenant.Connect.Card.CardType";
                public const string Cards = "Tenant.Conect.Card.Cards";
                public const string ConnectCardTypeCategory = "Tenant.Connect.Card.ConnectCardTypeCategory";

                public const string FullTax = "Tenant.Connect.FullTax";
                public const string Member = "Tenant.Connect.FullTax.Member";
                public const string SaleTarget = "Tenant.Connect.Report.SaleTarget";
                public const string Discount = "Tenant.Connect.Report.Discount";
                public const string Tender = "Tenant.Connect.Report.Tender";

                public const string FeedbackUnread = "Tenant.Connect.Report.FeedbackUnread";
                public const string SpeedOfServiceReport = "Tenant.Connect.Report.SpeedOfServiceReport";
                public const string CashSummaryReport = "Tenant.Connect.Report.CashSummaryReport";
            }

            public static class Cater
            {
                public const string Customer = "Tenant.Cater.Customer";
                public const string QuotationHeader = "Tenant.Cater.QuotationHeader";
            }

            public static class DineEngage
            {
                public const string GiftVoucher = "Tenant.Engage.GiftVoucher";
                public const string GiftVoucherType = "Tenant.Engage.GiftVoucherType";
                public const string Member = "Tenant.Engage.Member";
                public const string Membership = "Tenant.Engage.Membership";
                public const string MembershipTier = "Tenant.Engage.MembershipTier";
                public const string Loyalty = "Tenant.Engage.Loyalty";
                public const string Account = "Tenant.Engage.Account";
                public const string Reservation = "Tenant.Engage.Reservation";
                public const string LoyaltyProgram = "Tenant.Engage.LoyaltyProgram";
                public const string LoyaltyProgramStage = "Tenant.Engage.LoyaltyProgram.Stage";
                public const string GiftVoucherCategory = "Tenant.Engage.GiftVoucherCategory";
                public const string ServiceInfo = "Tenent.Engage.ServiceInfo";
                public const string ServiceInfoTemplate = "Tenent.Engage.ServiceInfoTemplate";
            }

            public static class DineTick
            {
                public const string Dashboard = "Tenant.Tick.Dashboard";
                public const string Master = "Tenant.Tick.Master";

                public const string Employee = "Tenant.Tick.Employee";
                public const string Ticket = "Tenant.Tick.Ticket";
                public const string Department = "Tenant.Tick.Department";
                public const string EmployeeAttendance = "Tenant.Tick.EmployeeAttendance";

                public const string TimeAttendanceReport = "Tenant.Tick.Report.TimeAttendanceReport";
            }

            public static class DineWheel
            {
                public const string Dashboard = "Tenant.Wheel.Dashboard";
                public const string WheelOrder = "Tenant.Wheel.Order";
            }

            public static class DineCater
            {
                public const string Dashboard = "Tenant.Cater.Dashboard";
            }

            public static class DineTouch
            {
                public const string LocationMenuItem = "Tenant.Touch.LocationMenuItem";
            }

            public static class Catering
            {
                public const string Order = "Tenant.Catering.Order";
            }

            public static class DineAddons
            {
                public const string Index = "Tenant.Addons.Index";
                public const string QuickBooks = "Tenant.Addons.Addons";
            }

            public static class DineHouse
            {
                public const string Dashboard = "Tenant.House.Dashboard";
                public const string NotificationDashboard = "Tenant.House.NotificationDashboard";
                public const string Master = "Tenant.House.Master";
                public const string TransactionMenu = "Tenant.House.Transaction";
                public const string PurchaseMenu = "Tenant.House.Purchase";
                public const string ProductionMenu = "Tenant.House.Production";
                public const string MovementMenu = "Tenant.House.Movement";
                public const string TransferMenu = "Tenant.House.Transfer";

                public const string InventoryCycleTag = "Tenant.House.Master.InventoryCycleTag";
                public const string ProductionUnit = "Tenant.House.Master.ProductionUnit";

                public const string Brand = "Tenant.House.Master.Brand";
                public const string ManualReason = "Tenant.House.Master.ManualReason";

                public const string Scrapped = "Tenant.House.Scrapped";
                public const string RecipeMenu = "Tenant.House.RecipeMenu";
                public const string MaterialMenu = "Tenant.House.MaterialMenu";

                public const string PurchaseCategory = "Tenant.House.Master.PurchaseCategory";

                public const string InterTransfer = "Tenant.House.Transaction.InterTransfer";
                public const string InterTransferApproval = "Tenant.House.Transaction.InterTransferApproval";
                public const string InterTransferReceived = "Tenant.House.Transaction.InterTransferReceived";
                public const string InterTransferDirectApproval = "Tenant.House.Transaction.InterTransferDirectApproval";

                public const string RecipeGroup = "Tenant.House.Master.RecipeGroup";
                public const string Recipe = "Tenant.House.Master.Recipe";
                public const string RecipeIngredient = "Tenant.House.Master.RecipeIngredient";

                public const string MaterialGroup = "Tenant.House.Master.MaterialGroup";
                public const string MaterialGroupCategory = "Tenant.House.Master.MaterialGroupCategory";
                public const string Material = "Tenant.House.Master.Material";
                public const string MaterialStorageLocation = "Tenant.House.Master.MaterialStorageLocation";
                public const string MaterialLocationWiseStock = "Tenant.House.Master.MaterialLocationWiseStock";
                public const string MaterialBrandsLink = "Tenant.House.Master.MaterialBrandsLink";

                public const string Supplier = "Tenant.House.Master.Supplier";
                public const string SupplierContact = "Tenant.House.Master.SupplierContact";
                public const string SupplierMaterial = "Tenant.House.Master.SupplierMaterial";

                public const string Contact = "Tenant.House.Master.Contact";

                public const string StorageLocation = "Tenant.House.Master.StorageLocation";

                public const string Tax = "Tenant.House.Master.Tax";
                public const string SalesTax = "Tenant.House.Master.SalesTax";

                public const string Unit = "Tenant.House.Master.Unit";
                public const string UnitConversion = "Tenant.House.Master.UnitConversion";
                public const string MaterialUnitsLink = "Tenant.House.Master.MaterialUnitsLink";

                public const string InwardDirectCredit = "Tenant.House.Transaction.InwardDirectCredit";

                public const string Request = "Tenant.House.Transaction.Request";
                public const string Issue = "Tenant.House.Transaction.Issue";
                public const string Return = "Tenant.House.Transaction.Return";
                public const string Adjustment = "Tenant.House.Transaction.Adjustment";
                public const string Invoice = "Tenant.House.Transaction.Invoice";
                public const string MaterialLedger = "Tenant.House.Master.MaterialLedger";
                public const string UserDefaultOrganization = "Tenant.House.Master.UserDefaultOrganization";

                public const string Purchase = "Tenant.House.Transaction.Purchase";
                public const string PurchaseOrder = "Tenant.House.Transaction.PurchaseOrder";
                public const string BulkPurchaseOrder = "Tenant.House.Transaction.BulkPurchaseOrder";

                public const string PurchaseReturn = "Tenant.House.Transaction.PurchaseReturn";

                public const string ProductRecipesLink = "Tenant.House.Master.ProductRecipesLink";
                public const string HouseReport = "Tenant.House.Master.HouseReport";

                public const string Template = "Tenant.House.Master.Template";
                public const string Customer = "Tenant.House.Master.Customer";
                public const string CustomerMaterial = "Tenant.House.Master.CustomerMaterial";
                public const string CustomerTagDefinition = "Tenant.House.Master.CustomerTagDefinition";
                public const string CustomerTagMaterialPrice = "Tenant.House.Master.CustomerTagMaterialPrice";

                public const string MaterialIngredient = "Tenant.House.Master.MaterialIngredient";
                public const string MaterialMenuMapping = "Tenant.House.Master.MaterialMenuMapping";

                public const string Yield = "Tenant.House.Transaction.Yield";
                public const string Production = "Tenant.House.Transaction.Production";
                public const string SupplierDocument = "Tenant.House.Transaction.SupplierDocument";
                public const string SalesOrder = "Tenant.House.Transaction.SalesOrder";
                public const string SalesDeliveryOrder = "Tenant.House.Transaction.SalesDeliveryOrder";
                public const string SalesInvoice = "Tenant.House.Transaction.SalesInvoice";
                public const string ClosingStock = "Tenant.House.Transaction.ClosingStock";
                public const string ClosingStockEntry = "Tenant.House.Transaction.ClosingStockEntry";
                public const string MenuItemWastage = "Tenant.House.Transaction.MenuItemWastage";
                public const string Numbering = "Tenant.House.Numbering";
            }

            public static class DineCluster
            {
                public const string Master = "Tenant.Cluster.Master";
                public const string DelAggLocationGroup = "Tenant.Cluster.Master.DelAggLocationGroup";
                public const string DelAggLocation = "Tenant.Cluster.Master.DelAggLocation";
                public const string DelAggLocMapping = "Tenant.Cluster.Master.DelAggLocMapping";
                public const string DelAggLocationItem = "Tenant.Cluster.Master.DelAggLocationItem";
                public const string DelAggVariantGroup = "Tenant.Cluster.Master.DelAggVariantGroup";
                public const string DelAggModifierGroup = "Tenant.Cluster.Master.DelAggModifierGroup";
                public const string DelAggVariant = "Tenant.Cluster.Master.DelAggVariant";
                public const string DelAggModifier = "Tenant.Cluster.Master.DelAggModifier";
                public const string DelAggItem = "Tenant.Cluster.Master.DelAggItem";
                public const string DelAggItemGroup = "Tenant.Cluster.Master.DelAggItemGroup";
                public const string DelAggTax = "Tenant.Cluster.DelAggTax";
                public const string DelTimingGroup = "Tenant.Cluster.DelTimingGroup";
                public const string DelAggCategory = "Tenant.Cluster.DelAggCategory";
                public const string DelAggLanguages = "Administration.DelAggLanguages";
                public const string DelAggCharge = "Tenant.Cluster.DelAggCharge";
                public const string DelAggImage = "Tenant.Cluster.DelAggImage";
            }
            public static class DineGo
            {
                public const string DineGoBrand = "Tenant.Go.DineGoBrand";
                public const string DineGoDevice = "Tenant.Go.DineGoDevice";
                public const string DineGoDepartment = "Tenant.Go.DineGoDepartment";
                public const string DineGoPaymentType = "Tenant.Go.DineGoPaymentType";
                public const string DineGoCharge = "Tenant.Go.DineGoCharge";
            }

            public static class DinePlay
            {
                public const string DinePlayDisplays = "Tenant.Play.DinePlayDisplays";
                public const string DinePlayResolutions = "Tenant.Play.DinePlaySolutions";
                public const string DinePlayDisplayGroups = "Tenant.Play.DinePlayDisplayGroups";
                public const string DinePlayDayParting = "Tenant.Play.DinePlayDayParting";
                public const string DinePlayDayLayouts = "Tenant.Play.DinePlayLayouts";
                public const string DinePlayDaySchedules = "Tenant.Play.DinePlaySchedules";
                public const string DelAggTax = "Tenant.Cluster.Master.DelAggTax";
            }
        }

        public static class Frontend
        {
            public const string Home = "Frontend.Home";
            public const string About = "Frontend.About";
        }
    }
}