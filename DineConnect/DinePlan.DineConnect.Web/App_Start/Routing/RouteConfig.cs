﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace DinePlan.DineConnect.Web.Routing
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new {id = RouteParameter.Optional}
                );
           
            routes.MapRoute(
                "Image", "images/{filename}",
                new {controller = "Images", action = "Index", filename = ""}
                );

            routes.MapRoute(
                "Installer", "installer/{filename}",
                new {controller = "Installer", action = "Index", filename = ""}
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new {controller = "Account", action = "Login", id = UrlParameter.Optional},
                namespaces: new[] {"DinePlan.DineConnect.Web.Controllers"}
                );

            routes.MapRoute(
                name: "ServiceInfo",
                url: "{controller}/{action}/{templateId}/{ticketNo}/{memberId}",
                defaults: new { controller = "MemberServiceInfo", action = "ServiceInfo" },
                namespaces: new[] { "DinePlan.DineConnect.Web.Controllers" }
                );
        }
    }
}