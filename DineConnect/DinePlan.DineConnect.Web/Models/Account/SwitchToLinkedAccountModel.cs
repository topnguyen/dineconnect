﻿namespace DinePlan.DineConnect.Web.Models.Account
{
    public class SwitchToLinkedAccountModel
    {
        public long TargetUserId { get; set; }
    }
}