﻿/* eslint-disable */

// JScript File

//window.history.forward(1);

function validateMaxvalue(obj, pmaxVal) {
    var curVal = parseFloat(document.all[obj].value);
    var maxVal = parseFloat(pmaxVal);
    if (curVal > maxVal) {
        alert("The value should not be greater than : " + maxVal);
        document.all[obj].value = '';
    }
}

function validateRange(obj, pminVal, pmaxVal) {
    var curVal = parseFloat(document.all[obj].value);
    var minVal = parseFloat(pminVal);
    var maxVal = parseFloat(pmaxVal);

    if (curVal < minVal) {
        alert("The Value should not be less than : " + minVal);
        document.all[obj].value = '';
    }
    if (curVal > maxVal) {
        alert("The Value should not be greater than : " + maxVal);
        document.all[obj].value = '';
    }
}

function keyTrap(obj, e, objfocus, pointAccept) {
    if (pointAccept == 'Y') {
        return NumberWithPoint(obj, e, objfocus);
    }
    else {
        return NumberOnly(e, objfocus);
    }
}

function AcceptAllKeys(e, objfocus) {
    var keynum;
    var keychar;
    var charcheck;

    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }

    if (keynum == 13) {
        if (objfocus != null)
            document.all[objfocus].focus();
        return false;
    }
}

function ChangeAsUpperCase(e, objfocus) {
    var keynum;
    var keychar;
    var charcheck;

    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }

    if (keynum == 13) {
        if (objfocus != null)
            document.all[objfocus].focus();
        return false;
    }

    //keychar = String.fromCharCode(keynum);

    //keychar=keychar.toUpperCase();

    //charcheck = /[ABCDEFGHIJKLMNOPQRSTUVWXYZ. abcdefghijklmnopqrstuvwxyz]/;
    //return charcheck.test(keychar);

    if (keynum >= 97 && keynum <= 122) {
        var newKey = keynum - 32;

        if (window.event) // IE
        {
            e.keyCode = newKey;
            e.charCode = newKey;
        }
        else if (e.which) // Netscape/Firefox/Opera
        {
            keynum = e.which;
            e.which = newKey;
            e.charCode = newKey;
        }
    }
    return true;
}

function changetoupper(obj) {
    obj.value = obj.value.toUpperCase();
}

function NumberOnly(e, objfocus) {
    var keynum;
    var keychar;
    var charcheck;

    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }

    if (keynum == 11)
        return false;

    if (keynum == 13) {
        if (objfocus != null)
            document.all[objfocus].focus();
        return false;
    }

    keychar = String.fromCharCode(keynum);
    charcheck = /[0123456789]/;
    return charcheck.test(keychar);
}

function CreditCardOnly(e, objfocus) {
    var keynum;
    var keychar;
    var charcheck;

    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }

    if (keynum == 11)
        return false;

    if (keynum == 13) {
        if (objfocus != null)
            document.all[objfocus].focus();
        return false;
    }

    keychar = String.fromCharCode(keynum);
    charcheck = /[0123456789-]/;
    return charcheck.test(keychar);
}

function NumberOnly_woFocus(e) {
    var keynum;
    var keychar;
    var charcheck;

    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    if (keynum == 11)
        return true;

    if (keynum == 13) {
        return false;
    }

    keychar = String.fromCharCode(keynum);
    charcheck = /[0123456789]/;
    return charcheck.test(keychar);
}

function NumberWithPoint(obj, e, objfocus) {
    var keynum;
    var keychar;
    var charcheck;

    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    if (keynum == 11)
        return false;
    if (keynum == 13) {
        if (objfocus != null)
            document.all[objfocus].focus();
        return false;
    }

    if (SearchStringContainsDot(obj) && keynum == 46)  // "." Ascii Value is 46
    {
        keynum = 95;        // S
    }

    keychar = String.fromCharCode(keynum);
    charcheck = /[0123456789.-]/;
    return charcheck.test(keychar);
}

function NumberWithFractionBasedOnInput(inputtext, e, objfocus, argboolean) {
    var keynum;
    var keychar;
    var charcheck;

    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    if (keynum == 11)
        return false;
    if (keynum == 13) {
        if (objfocus != null)
            document.all[objfocus].focus();
        return false;
    }
    //alert(argboolean);

    if (argboolean == false)
        inputtext = inputtext + ".";

    var patt1 = /\./;
    var result = inputtext.search(patt1);
    if (result > 0 && keynum == 46)
        keynum = 95;

    keychar = String.fromCharCode(keynum);
    charcheck = /[0123456789.]/;
    return charcheck.test(keychar);
}

function NumberWithPointPassingText(inputtext, e, objfocus, argboolean) {
    var keynum;
    var keychar;
    var charcheck;

    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    if (keynum == 11)
        return false;
    if (keynum == 13) {
        if (objfocus != null)
            document.all[objfocus].focus();
        return false;
    }

    if (argboolean == false)
        inputtext = inputtext + ".";

    var patt1 = /\./;
    var result = inputtext.search(patt1);
    if (result >= 0 && keynum == 46)
        keynum = 95;

    keychar = String.fromCharCode(keynum);
    charcheck = /[0123456789.]/;
    return charcheck.test(keychar);
}

function NumberWithPointAndNegativePassingText(inputtext, e, objfocus, argboolean) {
    var keynum;
    var keychar;
    var charcheck;

    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    if (keynum == 11)
        return false;
    if (keynum == 13) {
        if (objfocus != null)
            document.all[objfocus].focus();
        return false;
    }

    if (argboolean == false) {
        inputtext = inputtext + ".-";
    }

    var patt1 = /\./;
    var result = inputtext.search(patt1);
    if (result >= 0 && keynum == 46)
        keynum = 95;

    var patt2 = /\-/;
    var result = inputtext.search(patt2);
    if (result >= 0 && keynum == 45)
        keynum = 95;

    keychar = String.fromCharCode(keynum);
    charcheck = /[0123456789.-]/;
    return charcheck.test(keychar);
}

function NumberWithPointPassingTextNotEnteredHandled(inputtext, e, objfocus, argboolean) {
    var keynum;
    var keychar;
    var charcheck;

    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    //if (keynum == 11)
    //    return false;
    if (keynum == 13) {
        //document.all[objfocus].focus();
        return true;
    }

    if (argboolean == false)
        inputtext = inputtext + ".";

    var patt1 = /\./;
    var result = inputtext.search(patt1);
    if (result >= 0 && keynum == 46)
        keynum = 95;

    keychar = String.fromCharCode(keynum);
    charcheck = /[0123456789.-]/;
    return charcheck.test(keychar);
}

function SetNextObject(e, objfocus) {
    var keynum;
    var keychar;
    var charcheck;

    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    if (keynum == 13) {
        if (objfocus != null)
            document.all[objfocus].focus();
        return false;
    }
    else {
        return true;
    }
}

function AlphaAToZOnly(e, objfocus) {
    var keynum;
    var keychar;
    var charcheck;

    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }

    if (keynum == 11)
        return true;

    if (keynum == 13) {
        if (objfocus != null)
            document.all[objfocus].focus();
        return false;
    }
    keychar = String.fromCharCode(keynum);
    //keychar=keychar.toUpperCase();

    charcheck = /[ABCDEFGHIJKLMNOPQRSTUVWXYZ. abcdefghijklmnopqrstuvwxyz]/;
    return charcheck.test(keychar);
}

function SearchStringContainsDot(obj) {
    var str = document.all[obj].value;
    var patt1 = /\./;

    var result = str.search(patt1);

    if (result >= 0)
        return true;
    else
        return false;
}

function gotFocus(txt) {
    document.all[txt].select();
    document.all[txt].style.background = '#00008b';
    document.all[txt].style.color = 'yellow';
}

function gotFocus2(txt) {
    document.all[txt].select();
    document.all[txt].style.background = '#00008b';
    document.all[txt].style.color = 'yellow';
}

function lostFocus(txt) {
    document.all[txt].style.background = 'white';
    document.all[txt].style.color = 'black';
}

function GetXmlHttpObject() {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        return new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE5
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
}

function closeWindow() {
    window.close();
}

function getSynchronousCallResult(serviceUrl, postData) {
    var xmlhttp = null;
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        // code for IE6, IE5
        if (new ActiveXObject("Microsoft.XMLHTTP"))
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        else
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    }

    serviceUrl = serviceUrl + "?rnd=" + Math.random();
    xmlhttp.open("POST", serviceUrl, false);
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=utf-8");
    xmlhttp.send(postData);
    var responseText = xmlhttp.responseText;
    return responseText;
}

function getAsynchronousCallResult(serviceUrl, postData) {
    var xmlhttp = null;
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        // code for IE6, IE5
        if (new ActiveXObject("Microsoft.XMLHTTP"))
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        else
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    }

    serviceUrl = serviceUrl + "?rnd=" + Math.random();
    xmlhttp.open("POST", serviceUrl, true);
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=utf-8");
    xmlhttp.send(postData);
    var responseText = xmlhttp.responseText;
    return responseText;
}

function findPos(obj) {
    var posX = obj.offsetLeft;
    var posY = obj.offsetTop;
    while (obj.offsetParent) {
        posX = posX + obj.offsetParent.offsetLeft;
        posY = posY + obj.offsetParent.offsetTop;

        if (obj == document.getElementsByTagName('body')[0]) {
            break
        }
        else {
            obj = obj.offsetParent;
        }
    }
    return [posX, posY]
}

function getReporterName(mnobj, obj) {
    var ec = document.getElementById(mnobj).value;

    var disobj = document.getElementById(obj);
    if (ec.length == 6) {
        var Qry = "SELECT EMPLOYEE_NAME ||','||PRESENT_DESIGNATION FROM ALL_PERINFO WHERE EMPLOYEE_CODE=" + ec + " AND RESIGNED_ON IS NULL";
        HMService.getSingleValue(Qry,
            function (result) {
                if (result != "0" && jQuery.trim(result).length < 65) {
                    var splitresult = result.split(",");
                    disobj.value = splitresult[0] + ' [' + splitresult[1] + ']';
                }
                else if (result == 0 || result == "0") {
                    disobj.value = "User Name Not Found !";
                }
                else {
                    disobj.value = "User Name Not Found !";
                }
            });
    }
    else {
        disobj.value = "";
    }
}

function preventNonNumericalInput(event) {
    event = event || window.event;
    var charCode = (typeof event.which === "undefined") ? event.keyCode : event.which;
    var charStr = String.fromCharCode(charCode);

    if (!charStr.match(/^[0-9]+$/))
        event.preventDefault();
}

$(function () { $("[data-toggle = 'tooltip']").tooltip(); });