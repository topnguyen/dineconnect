﻿#region using

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Excel;
using Abp.UI;

#endregion

namespace DinePlan.DineConnect.Web.Utility
{
    public class ExcelData
    {
        private readonly string _path;
        private readonly Stream _stream;

        public ExcelData(string path, Stream stream)
        {
            _path = path;
            _stream = stream;
        }

        private IExcelDataReader GetExcelReader()
        {
            IExcelDataReader reader = null;
            try
            {
                if (_path.EndsWith(".xls"))
                {
                    reader = ExcelReaderFactory.CreateBinaryReader(_stream);
                }
                if (_path.EndsWith(".xlsx"))
                {
                    reader = ExcelReaderFactory.CreateOpenXmlReader(_stream);
                }
                return reader;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private DataSet ConvertToDataSetOfStrings(DataSet sourceDataSet)
        {
            var result = new DataSet();
            result.Tables.AddRange(
                sourceDataSet.Tables.Cast<DataTable>().Select(srcDataTable =>
                    {
                        var destDataTable = new DataTable(srcDataTable.TableName, srcDataTable.Namespace);
                        // Copy each source column as System.String...
                        destDataTable.Columns.AddRange(
                            srcDataTable.Columns.Cast<DataColumn>()
                                .Select(col => new DataColumn(col.ColumnName, typeof(String)))
                                .ToArray()
                        );
                        // Implicitly convert all source cells to System.String using DataTable.ImportRow()
                        srcDataTable.Rows.OfType<DataRow>()
                            .ToList()
                            .ForEach(row => destDataTable.ImportRow(row));
                        return destDataTable;
                    })
                    .ToArray()
            );
            return result;
        }
        public IEnumerable<DataRow> GetDataAsSet(bool firstRowIsColumnNames = true)
        {
            try
            {
                var reader = GetExcelReader();
                var workbook = reader.AsDataSet();
                workbook = ConvertToDataSetOfStrings(workbook);
                var sheets = from DataTable sheet in workbook.Tables select sheet.TableName;
                reader.IsFirstRowAsColumnNames = firstRowIsColumnNames;
                var workSheet = reader.AsDataSet().Tables[sheets.First()];
                if (workSheet == null)
                    throw new UserFriendlyException("Records Not Found");
                var rows = from DataRow a in workSheet.Rows select a;
                return rows;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public IEnumerable<DataRow> GetData(bool firstRowIsColumnNames = true)
        {
            try
            {
                var reader = GetExcelReader();
                var workbook = reader.AsDataSet();
                var sheets = from DataTable sheet in workbook.Tables select sheet.TableName;
                reader.IsFirstRowAsColumnNames = firstRowIsColumnNames;
                var workSheet = reader.AsDataSet().Tables[sheets.First()];
                if (workSheet == null)
                    throw new UserFriendlyException("Records Not Found");
                var rows = from DataRow a in workSheet.Rows select a;
                return rows;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}