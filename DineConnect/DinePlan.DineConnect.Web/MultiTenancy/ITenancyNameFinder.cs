namespace DinePlan.DineConnect.Web.MultiTenancy
{
    public interface ITenancyNameFinder
    {
        string GetCurrentTenancyNameOrNull();
        bool IsAutoRegister(int tenantId);


    }
}