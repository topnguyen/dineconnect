﻿var app = app || {};
(function () {
    var appLocalizationSource = abp.localization.getSource("DineConnect");
    app.localize = function () {
        return appLocalizationSource.apply(this, arguments);
    };

    app.downloadTempFile = function (file) {
        location.href =
            abp.appPath +
            "File/DownloadTempFile?fileType=" +
            file.fileType +
            "&fileToken=" +
            file.fileToken +
            "&fileName=" +
            file.fileName;
    };

    app.downloadTempFileOnly = function (file) {
        location.href =
            abp.appPath +
            "File/DownloadTempFileOnly?fileType=" +
            file.fileType +
            "&fileToken=" +
            file.fileToken +
            "&fileName=" +
            file.fileName;
    };

    app.openTempFile = function (file) {
        const path = abp.appPath +
            "File/DownloadTempFile?fileType=" +
            file.fileType +
            "&fileToken=" +
            file.fileToken +
            "&fileName=" +
            file.fileName;
        window.open(path);
    };
    app.openTempFileOnly = function (file) {
        const path = abp.appPath +
            "File/DownloadTempFileOnly?fileType=" +
            file.fileType +
            "&fileToken=" +
            file.fileToken +
            "&fileName=" +
            file.fileName;
        window.open(path, '_blank', 'noreferrer');
    };
    app.createDateRangePickerOptions = function () {
        const todayAsString = moment().format("YYYY-MM-DD");

        const options = {
            locale: {
                format: "YYYY-MM-DD",
                applyLabel: app.localize("Apply"),
                cancelLabel: app.localize("Cancel"),
                customRangeLabel: app.localize("CustomRange")
            },
            min: moment().subtract(100, "year").startOf("month"),
            minDate: moment().subtract(100, "year").startOf("month"),
            max: todayAsString,
            maxDate: todayAsString,
            ranges: {}
        };

        options.ranges[app.localize("Today")] = [moment().startOf("day"), moment().endOf("day")];
        options.ranges[app.localize("Yesterday")] = [
            moment().subtract(1, "days").startOf("day"), moment().subtract(1, "days").endOf("day")
        ];
        options.ranges[app.localize("Last7Days")] =
            [moment().subtract(6, "days").startOf("day"), moment().endOf("day")];
        options.ranges[app.localize("Last30Days")] =
            [moment().subtract(29, "days").startOf("day"), moment().endOf("day")];
        options.ranges[app.localize("ThisMonth")] = [moment().startOf("month"), moment().endOf("month")];
        options.ranges[app.localize("LastMonth")] = [
            moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")
        ];
        options.ranges[app.localize("TwoMonthsAgo")] = [
            moment().subtract(2, "month").startOf("month"), moment().subtract(2, "month").endOf("month")
        ];
        return options;
    };

    app.createComparisonOptions = function () {
        const todayAsString = moment().format("YYYY-MM-DD");

        const options = {
            locale: {
                format: "YYYY-MM-DD",
                applyLabel: app.localize("Apply"),
                cancelLabel: app.localize("Cancel"),
                customRangeLabel: app.localize("CustomRange")
            },
            min: "2015-05-01",
            minDate: "2015-05-01",
            max: todayAsString,
            maxDate: todayAsString,
            ranges: {},
            showCustomRangeLabel: false
        };

        options.ranges[app.localize("Today")] = [moment().startOf("day"), moment().endOf("day")];
        options.ranges[app.localize("Yesterday")] = [
            moment().subtract(1, "days").startOf("day"), moment().subtract(1, "days").endOf("day")
        ];
        options.ranges[app.localize("LastWeek")] = [
            moment().subtract(6, "days").startOf("day"), moment().subtract(6, "days").endOf("day")
        ];

        return options;
    };
    app.createDateRangeFuturePickerOptions = function () {
        const todayAsString = moment().format("YYYY-MM-DD");

        const options = {
            locale: {
                format: "YYYY-MM-DD",
                applyLabel: app.localize("Apply"),
                cancelLabel: app.localize("Cancel"),
                customRangeLabel: app.localize("CustomRange")
            },
            min: todayAsString,
            minDate: todayAsString,
            max: "2099-05-01",
            maxDate: "2099-05-01",
            ranges: {}
        };
        options.ranges[app.localize("Today")] = [moment().startOf("day"), moment().endOf("day")];
        options.ranges[app.localize("Tomorrow")] =
            [moment().add(1, "days").startOf("day"), moment().add(1, "days").endOf("day")];
        options.ranges[app.localize("Next7Days")] = [moment().endOf("day"), moment().add(6, "days").startOf("day")];
        options.ranges[app.localize("Next30Days")] = [moment().endOf("day"), moment().add(29, "days").startOf("day")];
        options.ranges[app.localize("ThisMonth")] = [moment().add("month"), moment().endOf("month")];
        options.ranges[app.localize("NextMonth")] = [
            moment().add(1, "month").startOf("month"), moment().add(1, "month").endOf("month")
        ];

        return options;
    };

    app.createFlexibleDateRangePickerOptions = function () {
        const options = {
            locale: {
                format: "YYYY-MM-DD",
                applyLabel: app.localize("Apply"),
                cancelLabel: app.localize("Cancel"),
                customRangeLabel: app.localize("CustomRange")
            },
            min: moment().subtract(3, "year").startOf("month"),
            minDate: moment().subtract(3, "year").startOf("month"),
            max: "2099-05-01",
            maxDate: "2099-05-01",
            ranges: {}
        };
        options.ranges[app.localize("Today")] = [moment().startOf("day"), moment().endOf("day")];
        options.ranges[app.localize("Tomorrow")] = [moment().add(1, "days").startOf("day"), moment().add(1, "days").endOf("day")];
        options.ranges[app.localize("Next7Days")] = [moment().endOf("day"), moment().add(6, "days").startOf("day")];
        options.ranges[app.localize("Next30Days")] = [moment().endOf("day"), moment().add(29, "days").startOf("day")];
        options.ranges[app.localize("ThisMonth")] = [moment().startOf("month"), moment().endOf("month")];
        options.ranges[app.localize("NextMonth")] = [moment().add(1, "month").startOf("month"), moment().add(1, "month").endOf("month")];

        return options;
    };

    app.getUserProfilePicturePath = function (profilePictureId) {
        return profilePictureId
            ? (abp.appPath + "Profile/GetProfilePictureById?id=" + profilePictureId)
            : (abp.appPath + "Common/Images/default-profile-picture.png");
    };

    app.getShownLinkedUserName = function (linkedUser) {
        if (!abp.multiTenancy.isEnabled) {
            return linkedUser.userName;
        } else {
            if (linkedUser.tenancyName) {
                return linkedUser.tenancyName + "\\" + linkedUser.username;
            } else {
                return '.\\${linkedUser.username}';
            }
        }
    };

    app.notification = app.notification || {};

    app.notification.getUiIconBySeverity = function (severity) {
        switch (severity) {
            case abp.notifications.severity.SUCCESS:
                return "fa fa-check";
            case abp.notifications.severity.WARN:
                return "fa fa-warning";
            case abp.notifications.severity.ERROR:
                return "fa fa-bolt";
            case abp.notifications.severity.FATAL:
                return "fa fa-bomb";
            case abp.notifications.severity.INFO:
            default:
                return "fa fa-info";
        }
    };

    app.createBuilder = function (rules, filters) {
        const output = {
            options: {
                plugins: [
                    "filter-description",
                    "unique-filter",
                    "bt-tooltip-errors",
                    "bt-checkbox",
                    "invert"
                ],
                rules: rules,
                filters: filters,
                allow_empty: true
            },
            builder: {}
        };
        return output;
    };

    app.createLocationInputForSearch = function () {
        return {
            single: false,

            locations: [],
            groups: [],
            locationTags: [],
            nonLocations: [],

            group: false,
            locationTag: false,

            hideGroupDisplay: false,
            hideTagDisplay: false,
            hideRestrictLocation: true
        };
    };

    app.createLocationInputForCreate = function () {
        return {
            single: false,

            locations: [],
            groups: [],
            locationTags: [],
            nonLocations: [],

            group: false,
            locationTag: false,

            hideGroupDisplay: false,
            hideTagDisplay: false,
            hideRestrictLocation: false
        };
    };
    app.createLocationInputForCreateSingleLocation = function () {
        return {
            single: true,

            locations: [],
            groups: [],
            locationTags: [],
            nonLocations: [],

            group: false,
            locationTag: false,

            hideGroupDisplay: true,
            hideTagDisplay: true,
            hideRestrictLocation: true
        };
    };
})();