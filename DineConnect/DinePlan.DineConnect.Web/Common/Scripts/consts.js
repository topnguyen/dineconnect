﻿var app = app || {};
(function () {

    $.extend(app, {
        consts: {
            grid: {
                defaultPageSize: 20,
                defaultPageSizes: [10, 20, 50, 100],
                dateFormat: 'yyyy-MM-dd'
            },
            userManagement: {
                defaultAdminUserName: 'admin'
            },
            contentTypes: {
                formUrlencoded: 'application/x-www-form-urlencoded; charset=UTF-8'
            }
           
        }
    });

})();