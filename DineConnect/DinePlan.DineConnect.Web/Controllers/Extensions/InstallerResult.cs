﻿using System.IO;
using System.Web;
using System.Web.Mvc;
using Simple.ImageResizer;

namespace DinePlan.DineConnect.Web.Controllers.Extensions
{
    public class InstallerResult : FilePathResult
    {
        private readonly string _filePath;

        public InstallerResult(string filePath) :
            base(filePath, $"application/{filePath.FileExtensionInstallationForContentType()}")
        {
            _filePath = filePath;
          
        }

        protected override void WriteFile(HttpResponseBase response)
        {
            response.SetDefaultImageHeaders(_filePath);
            WriteFileToResponse(_filePath, response);
        }

        private static void WriteFileToResponse(string filePath, HttpResponseBase response)
        {
            using (var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                const int bufferLength = 65536;
                var buffer = new byte[bufferLength];

                while (true)
                {
                    int bytesRead = fs.Read(buffer, 0, bufferLength);

                    if (bytesRead == 0)
                    {
                        break;
                    }

                    response.OutputStream.Write(buffer, 0, bytesRead);
                }
            }
        }
    }
}
