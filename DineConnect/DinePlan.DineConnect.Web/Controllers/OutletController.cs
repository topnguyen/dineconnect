﻿using System.Web.Mvc;

namespace DinePlan.DineConnect.Web.Controllers
{
    public class OutletController : DineConnectControllerBase
    {
        // GET: Outlet
        public ActionResult Index()
        {
            /* Enable next line to redirect to Multi-Page Application */
            /* return RedirectToAction("Index", "Home", new {area = "Mpa"}); */

            return View("~/AppOutlet/noAuthLayout.cshtml"); //Layout of the angular application.
        }
    }
}