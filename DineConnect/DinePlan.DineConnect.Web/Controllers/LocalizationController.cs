﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Mvc;
using Abp;
using Abp.Auditing;
using Abp.Configuration;
using Abp.Localization;
using Abp.Timing;
using Abp.Web.Models;
using Abp.Web.Mvc.Controllers;
using Abp.Web.Mvc.Models;
using DinePlan.DineConnect.House;
using Hangfire.Annotations;

namespace DinePlan.DineConnect.Web.Controllers
{
    public class LocalizationController : AbpController
    {
      
        public virtual ActionResult ChangeCulture(string cultureName, string returnUrl = "")
        {
            if (!IsValidCultureCode(cultureName))
                throw new AbpException("Unknown language: " + cultureName + ". It must be a valid culture!");
            this.Response.Cookies.Add(new HttpCookie("Abp.Localization.CultureName", cultureName)
            {
                Expires = Clock.Now.AddYears(2)
            });
           
            if (this.Request.IsAjaxRequest())
                return (ActionResult) this.Json((object) new MvcAjaxResponse(), JsonRequestBehavior.AllowGet);

            if (!string.IsNullOrWhiteSpace(returnUrl))
            {
                var escapedReturnUrl = Uri.EscapeUriString(returnUrl);
                if (!(Request.Url is null))
                {
                    var localPath = LocalPathAndQuery(escapedReturnUrl, Request.Url.Host, Request.Url.Port);
                    if (!string.IsNullOrWhiteSpace(localPath))
                    {
                        var unescapedLocalPath = Uri.UnescapeDataString(localPath);
                        if (Url.IsLocalUrl(unescapedLocalPath))
                        {
                            return Redirect(unescapedLocalPath);
                        }
                    }
                }
            }

            return Redirect(Request.ApplicationPath);
        }

        public virtual string LocalPathAndQuery([NotNull] string url, [CanBeNull] string localHostName = null, [CanBeNull] int? localPort = null)
        {

            var uri = ParseWithUriBuilder(url) ?? ParseWithUri(url);
            if (uri != null && uri.IsWellFormedOriginalString())
            {
                if (uri.IsAbsoluteUri)
                {
                    var isValidScheme = uri.Scheme == Uri.UriSchemeHttp || uri.Scheme == Uri.UriSchemeHttps;
                    var isSameHost = string.Equals(localHostName, uri.Host, StringComparison.OrdinalIgnoreCase);
                    var isSamePort = localPort == uri.Port || (localPort == null && uri.IsDefaultPort);
                    if (isValidScheme && isSameHost && isSamePort)
                    {
                        return uri.PathAndQuery;
                    }
                }
                else if (!uri.IsAbsoluteUri)
                {
                    return uri.OriginalString;
                }
            }
            return null;
        }
       Uri ParseWithUriBuilder(string url)
        {
            try
            {
                return new UriBuilder(url).Uri;
            }
            catch (UriFormatException)
            {
                return null;
            }
        }

        Uri ParseWithUri(string url)
        {
            if (Uri.TryCreate(url, UriKind.RelativeOrAbsolute, out Uri returnUri))
            {
                return returnUri;
            }
            return null;
        }
        public static bool IsValidCultureCode(string cultureCode)
        {
            try
            {
                var cultureInfo = CultureInfo.GetCultureInfo(cultureCode);
                return true;
            }
            catch (CultureNotFoundException ex)
            {
                return false;
            }
        }
    }
}