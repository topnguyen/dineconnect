using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Abp.Auditing;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.IO.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Models;
using Abp.Web.Mvc.Authorization;
using Abp.Web.Mvc.Models;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Net.MimeTypes;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Master;
using System.IO;
using Abp.Domain.Repositories;

namespace DinePlan.DineConnect.Web.Controllers
{
    
    [AbpMvcAuthorize]
    public class ProfileController : DineConnectControllerBase
    {
        private readonly UserManager _userManager;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IRepository<Company> _companyRepo;

        public ProfileController(UserManager userManager, IBinaryObjectManager binaryObjectManager, IRepository<Company> companyRepo)
        {
            _userManager = userManager;
            _binaryObjectManager = binaryObjectManager;
            _companyRepo = companyRepo;
        }

        [DisableAuditing]
        public async Task<FileResult> GetProfilePicture()
        {
            var user = await _userManager.GetUserByIdAsync(AbpSession.GetUserId());
            if (user.ProfilePictureId == null)
            {
                return GetDefaultProfilePicture();
            }

            return await GetProfilePictureById(user.ProfilePictureId.Value);
        }

        [DisableAuditing]
        public async Task<FileResult> GetProfilePictureById(string id = "")
        {
            if (id.IsNullOrEmpty())
            {
                return GetDefaultProfilePicture();                
            }

            return await GetProfilePictureById(Guid.Parse(id));
        }

        [UnitOfWork]
        public async virtual Task<JsonResult> ChangeProfilePicture()
        {
            try
            {
                //Check input
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
                }

                var file = Request.Files[0];

                if (file.ContentLength > 30720) //30KB.
                {
                    throw new UserFriendlyException(L("ProfilePicture_Warn_SizeLimit"));
                }

                //Get user
                var user = await _userManager.GetUserByIdAsync(AbpSession.GetUserId());

                //Delete old picture
                if (user.ProfilePictureId.HasValue)
                {
                    await _binaryObjectManager.DeleteAsync(user.ProfilePictureId.Value);
                }

                //Save new picture
                var storedFile = new BinaryObject(file.InputStream.GetAllBytes());
                await _binaryObjectManager.SaveAsync(storedFile);

                //Update new picture on the user
                user.ProfilePictureId = storedFile.Id;

                //Return success
                return Json(new MvcAjaxResponse());
            }
            catch (UserFriendlyException ex)
            {
                //Return error message
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [UnitOfWork]
        public async virtual Task<JsonResult> ChangeCompanyProPicture(PictureDto input)
        {
            try
            {
                //Check input
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("Picture_Change_Error"));
                }

                bool temporaryCreation = false;

                if (input.Id == 0)
                    temporaryCreation = true;

                string CompCode = "Temp" + AbpSession.UserId;

                Company comp = null;
                if (temporaryCreation == false)
                {
                    comp = await _companyRepo.FirstOrDefaultAsync(input.Id);
                    if (comp == null)
                    {
                        throw new UserFriendlyException(L("EmployeeErr"));
                    }
                   CompCode = comp.Code;
                }


                var file = Request.Files[0];
                string fileName = file.FileName;

                string subPath = "~/UploadedFiles/" +CompCode;
                bool exists = System.IO.Directory.Exists(Server.MapPath(subPath));

                if (!exists)
                    System.IO.Directory.CreateDirectory(Server.MapPath(subPath));

                file.SaveAs(Path.Combine(Server.MapPath(subPath), fileName));

                if (file.ContentLength > 30720) //3000KB.
                {
                    throw new UserFriendlyException(L("ProfilePicture_Warn_SizeLimit"));
                }

                //Get user

                if (temporaryCreation == false)
                {
                    //Delete old picture
                    if (comp.CompanyProfilePictureId.HasValue)
                    {
                        await _binaryObjectManager.DeleteAsync(comp.CompanyProfilePictureId.Value);
                    }
                }

                //Save new picture
                var storedFile = new BinaryObject(file.InputStream.GetAllBytes());
                await _binaryObjectManager.SaveAsync(storedFile);

                if (temporaryCreation == false)
                {
                    //Update new picture on the user
                    //Return success

                    comp.CompanyProfilePictureId = storedFile.Id;
                    return Json(new MvcAjaxResponse(storedFile.Id));
                }
                else
                {
                    return Json(new MvcAjaxResponse(storedFile.Id));
                }

            }
            catch (UserFriendlyException ex)
            {
                //Return error message
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        private FileResult GetDefaultProfilePicture()
        {
            return File(Server.MapPath("~/Common/Images/default-profile-picture.png"), MimeTypeNames.ImagePng);
        }

        private async Task<FileResult> GetProfilePictureById(Guid profilePictureId)
        {
            var file = await _binaryObjectManager.GetOrNullAsync(profilePictureId);
            if (file == null)
            {
                return GetDefaultProfilePicture();
            }

            return File(file.Bytes, MimeTypeNames.ImageJpeg);
        }
    }
}