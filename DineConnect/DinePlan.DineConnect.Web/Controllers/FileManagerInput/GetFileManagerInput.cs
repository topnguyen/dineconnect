﻿namespace DinePlan.DineConnect.Web.Controllers.FileManagerInput
{
    public class GetFileManagerInput
    {
        public string Action { get; set; }

        public string Path { get; set; }
    }
}