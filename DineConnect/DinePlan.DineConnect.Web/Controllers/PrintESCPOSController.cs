﻿using System;
using System.Linq;
using System.Web.Mvc;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Engage;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.Swipe;
using Neodynamic.SDK.Web;

namespace DinePlan.DineConnect.Web.Controllers
{
    public class PrintESCPOSController : DineConnectControllerBase
    {
        private readonly IRepository<SwipeCardLedger> _cardledgerRepo;
        private readonly IRepository<Company> _companyRepo;
        private readonly IRepository<PersonalInformation> _employeeRepo;
        private readonly IRepository<SwipeShift> _shiftRepo;
        private readonly IRepository<SwipeCard> _swipecardRepo;
        private readonly IRepository<ConnectMember> _swipememberRepo;
        public int transactionId = 0;

        public PrintESCPOSController(
            IRepository<Company> companyRepo,
            IRepository<SwipeCard> swipecardRepo,
            IRepository<SwipeCardLedger> cardledgerRepo,
            IRepository<ConnectMember> swipememberRepo,
            IRepository<SwipeShift> shiftRepo,
            IRepository<PersonalInformation> employeeRepo
            )
        {
            _companyRepo = companyRepo;
            _swipecardRepo = swipecardRepo;
            _cardledgerRepo = cardledgerRepo;
            _swipememberRepo = swipememberRepo;
            _shiftRepo = shiftRepo;
            _employeeRepo = employeeRepo;
        }

        public ActionResult Index(int transactionId)
        {
            ViewBag.WCPScript = WebClientPrint.CreateScript(
                Url.Action("ProcessRequest", "WebClientPrintAPI", null, HttpContext.Request.Url.Scheme),
                Url.Action("PrintCommands", "PrintESCPOS", null, HttpContext.Request.Url.Scheme),
                HttpContext.Session.SessionID);

            ViewBag.TransactionId = transactionId;

            return View();
        }

        [AllowAnonymous]
        public void PrintCommands(string useDefaultPrinter, string printerName, /*int transactionId,*/
            string transactionIdList)
        {
            var ESC = "0x1B"; //ESC byte in hex notation
            var NewLine = "0x0A"; //LF byte in hex notation
            var cmds = "";

            try
            {
                if (transactionIdList == "")
                    return;
                var arrTempList = transactionIdList.Split(",");
                var arrTransactionList = arrTempList.Select(int.Parse).ToArray();
                if (arrTransactionList.Count() == 0)
                    return;

                var companyList = _companyRepo.GetAllList();
                var company = companyList.FirstOrDefault();
                cmds = ESC + "@";

                #region EachLedger

                //foreach (var lst in arrTransactionList)
                //{
                var lst = arrTransactionList[0];
                var ledger = _cardledgerRepo.FirstOrDefault(t => t.Id == lst);

                var shift = _shiftRepo.FirstOrDefault(t => t.Id == ledger.ShiftRefId);
                //var user = "Siva";

                if (ledger == null)
                {
                    throw new UserFriendlyException(L("TransactionIdDoesNotExist", transactionId));
                }

                var MemberName = L("Guest");
                var CardNumber = "";
                var EmployeeName = "";

                if (ledger.MemberRefId != null)
                {
                    var member = _swipememberRepo.FirstOrDefault(t => t.Id == ledger.MemberRefId);
                    if (member == null)
                    {
                        throw new UserFriendlyException(L("MemberIdDoesNotExist", ledger.MemberRefId));
                    }
                    MemberName = member.MemberCode + " - " + member.Name;
                }
                else if (ledger.EmployeeRefId != null)
                {
                    var employee = _employeeRepo.FirstOrDefault(t => t.Id == ledger.EmployeeRefId);
                    if (employee == null)
                    {
                        throw new UserFriendlyException(L("EmployeeIdDoesNotExist", ledger.EmployeeRefId));
                    }
                    EmployeeName = employee.EmployeeCode + " - " + employee.EmployeeName;
                }

                var card = _swipecardRepo.FirstOrDefault(t => t.Id == ledger.CardRefId);
                CardNumber = card.CardNumber;


                //cmds += ESC + "!" + "0x17";
                cmds += ESC + "!" + "0x38";
                cmds += ESC + "a" + "0x01"; //centered position

                #region PrintCompanyAddress

                cmds += company.Name; // "<COMPANY NAME>";
                cmds += NewLine;
                cmds += ESC + "!" + "0x00"; //Character font A selected (ESC ! 0)
                if (!company.Address1.IsNullOrEmpty())
                {
                    cmds += company.Address1; // "<Address1>";
                    cmds += NewLine;
                }
                if (!company.Address2.IsNullOrEmpty())
                {
                    cmds += company.Address2;
                    cmds += NewLine;
                }
                if (!company.Address3.IsNullOrEmpty())
                {
                    cmds += company.Address3;
                    cmds += NewLine;
                }
                if (!company.City.IsNullOrEmpty())
                {
                    cmds += company.City;
                    cmds += NewLine;
                }
                if (!company.PhoneNumber.IsNullOrEmpty())
                {
                    cmds += L("Phone") + " : " + company.PhoneNumber;
                    cmds += NewLine;
                }
                if (!company.GovtApprovalId.IsNullOrEmpty())
                {
                    cmds += L("GovtApprovalId") + " : " + company.GovtApprovalId;
                    cmds += NewLine;
                }

                #endregion

                #region PrintHeader

                cmds += NewLine;
                //			cmds += ESC + "!" + "0x17";
                cmds += ESC + "a" + "0x00"; //left printing position
                cmds += PrintLine(L("Id") + " :" + ledger.Id,
                    L("Date") + " : " + ledger.LedgerTime.ToString("dd-MMM-yyyy HH:mm"), 48);
                cmds += NewLine;
                cmds += NewLine;
                cmds += ESC + "!" + "0x18";
                cmds += L("Card") + " # : " + CardNumber;
                cmds += NewLine;
                cmds += ESC + "!" + "0x00";
                cmds += new string('-', 48);
                cmds += NewLine;

                #endregion

                #region PrintDetails

                cmds += ESC + "!" + "0x18";
                cmds += PrintLine(L("OpenBalance"), ledger.OpeningBalance, 48);
                cmds += NewLine;
                cmds += NewLine;

                cmds += PrintLine(ledger.Description, ledger.Credit == 0 ? ledger.Debit : ledger.Credit, 48);
                cmds += NewLine;
                cmds += NewLine;

                if (arrTransactionList.Count() > 1)
                {
                    var loopCnt = 0;
                    foreach (var lstOther in arrTransactionList)
                    {
                        if (loopCnt == 0)
                        {
                            loopCnt++;
                            continue;
                        }
                        ledger = _cardledgerRepo.FirstOrDefault(t => t.Id == lstOther);
                        cmds += PrintLine(ledger.Description, ledger.Credit == 0 ? ledger.Debit : ledger.Credit, 48);
                        cmds += NewLine;
                        cmds += NewLine;
                        loopCnt++;
                    }
                }

                cmds += PrintLine(L("Balance"), ledger.ClosingBalance, 48);
                cmds += NewLine;
                cmds += NewLine;
                cmds += ESC + "!" + "0x00";
                cmds += new string('-', 48);
                cmds += NewLine;
                cmds += NewLine;
                cmds += NewLine;
                cmds += NewLine;
                cmds += NewLine;
                cmds += NewLine;
                cmds += NewLine;
                cmds += AsciiControlChars.Escape + "G" + (char) 0; //Cut
                cmds += AsciiControlChars.GroupSeparator + "V" + (char) 66 + (char) 0; //Cut

                cmds += NewLine;

                #endregion

                //}

                #endregion

                //Create a ClientPrintJob and send it back to the client!
                var cpj = new ClientPrintJob();
                //set  ESCPOS commands to print...
                cpj.PrinterCommands = cmds;
                cpj.FormatHexValues = true;

                //set client printer...
                if (useDefaultPrinter == "checked" || useDefaultPrinter == "undefined" || printerName == "null")
                    cpj.ClientPrinter = new DefaultPrinter();
                //cpj.ClientPrinter = new ParallelPortPrinter("lpt1");
                else
                    cpj.ClientPrinter = new InstalledPrinter(printerName);

                //send it...
                System.Web.HttpContext.Current.Response.ContentType = "application/octet-stream";
                System.Web.HttpContext.Current.Response.BinaryWrite(cpj.GetContent());
                System.Web.HttpContext.Current.Response.End();
            }
            catch (Exception ex)
            {
                //throw new UserFriendlyException(ex.Message);
            }
        }

        private string PrintLine(string description, decimal amount, int linelength)
        {
            var spaceleft = linelength - description.Length;
            var returnDesc = description + new string(' ', spaceleft - amount.ToString().Length) + amount;
            return returnDesc;
        }

        private string PrintLine(string leftstring, string rightstring, int linelength)
        {
            var spaceleft = linelength - leftstring.Length;
            var returnDesc = leftstring + new string(' ', spaceleft - rightstring.Length) + rightstring;
            return returnDesc;
        }
    }
}