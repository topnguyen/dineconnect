﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DinePlan.DineConnect.Web.Controllers
{
    public static class AsciiControlChars
    {
        public const char Nul = (char)0x00;
        public const char FormFeed = (char)0x0C;
        public const char Escape = (char)0x1B;
        public const char Newline = (char)0x0A;
        public const char GroupSeparator = (char)0x1D;
        public const char HorizontalTab = (char)0x09;
        public const char CarriageReturn = (char)0x0D;
        public const char Cancel = (char)0x18;
        public const char DataLinkEscape = (char)0x10;
        public const char EndOfTransmission = (char)0x04;
        public const char FileSeparator = (char)0x1C;
    }
}