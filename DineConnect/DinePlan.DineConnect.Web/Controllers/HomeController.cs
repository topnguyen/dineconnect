﻿using Abp.Extensions;
using Abp.UI;
using Abp.Web.Models;
using Abp.Web.Mvc.Models;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Web.App_Start.Validator;
using LomaCons.InstallKey.ClientSide;
using LomaCons.InstallKey.Common;
using System.Configuration;
using System.Web.Mvc;

namespace DinePlan.DineConnect.Web.Controllers
{
    public class HomeController : DineConnectControllerBase
    {
        private readonly ILocationAppService _lService;

        protected ClientSideValidator ClientValidator
        {
            get;
            private set;
        }

        public HomeController(ILocationAppService locationAppService)
        {
            _lService = locationAppService;
        }

        public ActionResult Index()
        {
            var allL = _lService.GetLocations();
            return View();
        }

        public ActionResult KeyEntry(string key)
        {
            if (key.IsNullOrEmpty())
                return View("~/App/common/views/keyentry/index.cshtml");
            else
            {
                try
                {
                    
                    ClientValidator = new ClientSideValidator(
                       ConfigurationManager.AppSettings["KeysetName"],
                       ConfigurationManager.AppSettings["Password"],
                       ConfigurationManager.AppSettings["FactorCode"],
                       ConfigurationManager.AppSettings["ValidationCode"]
                       );
                    ClientValidator.SetServiceAddress(ConfigurationManager.AppSettings["ValidationUrl"]);
                    ClientValidator.SuretyStorage = new DatabaseStorage("Default");
                    ClientValidator.IdentifierFinders.Clear();
                    ClientValidator.IdentifierFinders.Add(new MachineNameIdentifierFinder());

                    EnumValidateResult result;
                    //return RedirectToAction("Login", "Account");

                    // check to see if the user entered something that looks like a key that matches our keyset
                    if (ClientValidator.DoesKeyMatchKeyset(key.Trim()) == false)
                    {
                        throw new UserFriendlyException(L("The key entered is not valid.  Please be sure you entered it correctly."));
                    }

                    // validate the key against the server
                    result = ClientValidator.ValidateAgainstServer(key.Trim());
                    switch (result)
                    {
                        case EnumValidateResult.Ok:
                            break;

                        case EnumValidateResult.InvalidKey:
                            throw new UserFriendlyException(L("The key entered is not valid becasue it does not exist on the server."));

                        case EnumValidateResult.Revoked:
                            throw new UserFriendlyException(L("The key entered is not valid becasue it has been revoked."));

                        case EnumValidateResult.Violation:
                            throw new UserFriendlyException(L("The key entered is not valid becasue it has been used too many times."));
                    }

                    return RedirectToAction("Login", "Account");
                }
                catch (UserFriendlyException ex)
                {
                    return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)), JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}