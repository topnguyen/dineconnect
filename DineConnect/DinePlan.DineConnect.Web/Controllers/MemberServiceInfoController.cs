﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Http=System.Web.Http;
using System.Web.Mvc;
using Newtonsoft.Json;
using DinePlan.DineConnect.Engage.ServiceInfo;
using DinePlan.DineConnect.Engage.ServiceInfo.Dtos;
using Abp.AutoMapper;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Web.Models;
using Newtonsoft.Json.Serialization;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Engage.Member;
using DinePlan.DineConnect.Engage.Member.Dtos.DinePlan.DineConnect.Engage.Dtos;

namespace DinePlan.DineConnect.Web.Controllers
{
    public class MemberServiceInfoController : DineConnectControllerBase
    {
        private readonly IMemberServiceInfoTemplateAppService _memberServiceInfoAppService;
        private readonly IMemberAppService _memberAppService;
        public MemberServiceInfoController(
                IMemberServiceInfoTemplateAppService memberServiceInfoTemplateAppService,
                IMemberAppService memberAppService
            )
        {
            _memberServiceInfoAppService = memberServiceInfoTemplateAppService;
            _memberAppService = memberAppService;
        }

        public async Task<ActionResult> ServiceInfo(int templateId, string ticketNo, int memberId) {
            var template = _memberServiceInfoAppService.GetTemplateById(templateId);

            var dlzTemplate = JsonConvert.DeserializeObject<TemplateOutput>(template.Template);
            var content = template.StoredAsPlainText? dlzTemplate.ContentInText:dlzTemplate.Html;
            var ticket = await _memberServiceInfoAppService.GetTicketDataByTicketNo(ticketNo);
            var member = await _memberAppService.GetMemberForInfo(new NullableIdInput {Id=memberId});
            var replacer = new ValueReplacer(content, ticket,member, template.StoredAsPlainText);

            content = replacer.GetReplacedContent();
            ViewData["Content"] = new HtmlString(content);
            ViewData["TemplateStyle"] = new HtmlString(dlzTemplate.Css);
            return View();
        }



        [Http.HttpPost]
        public async Task<JsonResult> StoreTemplate([Http.FromBody] TemplateInput template) {
            CreateOrUpdateMemberServiceInfoTemplateInput input = new CreateOrUpdateMemberServiceInfoTemplateInput();
            MemberServiceInfoTemplateEditDto msit = new MemberServiceInfoTemplateEditDto();

            var tp = template.MapTo<TemplateOutput>();

            msit.Id = template.TemplateId;
            msit.Name = template.TemplateName;
            msit.ServiceInfoTypeId = template.ServiceInfoTypeId;
            msit.IsActive = template.IsActive;
            msit.StoredAsPlainText = template.StoredAsPlainText;
            msit.Template = JsonConvert.SerializeObject(tp);
            input.MemberServiceInfoTemplate = msit;
            var result = await _memberServiceInfoAppService.CreateOrUpdateMemberServiceInfoTemplate(input);
            return Json(result);
        }

        [DontWrapResult]
        public async Task<JsonResult> LoadTemplate(object template)
        {
            NullableIdInput nullableIdInput = new NullableIdInput(14);
            var getResult = await _memberServiceInfoAppService.GetMemberServiceInfoTemplateForUpdate(nullableIdInput);
            var result = JsonConvert.DeserializeObject<TemplateOutput>(getResult.MemberServiceInfoTemplate.Template);
            // var result2 = Json(result.MapTo<TemplateOutput2>(),JsonRequestBehavior.AllowGet);
            var result2 = JsonConvert.SerializeObject(result,new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver()});
            // Response.ContentType = "Content-Type: application/json";
            return new JsonResult { 
                Data = result,
                ContentEncoding = System.Text.Encoding.UTF8,
                ContentType = "application/json",
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            //return Json(result,JsonRequestBehavior.AllowGet);
        }

        [DontWrapResult]
        public async Task<ContentResult> LoadTemplate2(int id)
        {
            NullableIdInput nullableIdInput = new NullableIdInput(id);
            var getResult = await _memberServiceInfoAppService.GetMemberServiceInfoTemplateForUpdate(nullableIdInput);
            var result = JsonConvert.DeserializeObject<TemplateOutput>(getResult.MemberServiceInfoTemplate.Template);
            var result2 = JsonConvert.SerializeObject(result, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            return new ContentResult
            {
                Content = result2,
                ContentEncoding = System.Text.Encoding.UTF8,
                ContentType = "application/json" 

            };
        }
    }

    public class ValueReplacer {
        private readonly string[] _TicketFields = "Id-Int32,TicketId-Int32,LocationId-Int32,LocationName-String,TicketNumber-String,InvoiceNo-String,TicketCreatedTime-DateTime,LastUpdateTime-DateTime,LastOrderTime-DateTime,LastPaymentTime-DateTime,IsClosed-Boolean,IsLocked-Boolean,RemainingAmount-Decimal,TotalAmount-Decimal,DepartmentName-String,DepartmentGroup-String,TicketTypeName-String,Note-String,LastModifiedUserName-String,TicketTags-String,TicketStates-String,TicketLogs-String,TicketLog1-String,TaxIncluded-Boolean,TerminalName-String,PreOrder-Boolean,TicketEntities-String,WorkPeriodId-Int32,Credit-Boolean,LastPaymentTimeTruc-DateTime,ReferenceNumber-String,QueueNumber-String,CreditProcessed-Boolean,FullTaxInvoiceDetails-String,TicketPromotionDetails-String,ReferenceTicket-String,TenantId-Int32,ExternalProcessed-Boolean,TicketStatus-String,IsTagged-Boolean,IsDeleted-Boolean,DeleterUserId-Nullable`1,DeletionTime-Nullable`1,LastModificationTime-Nullable`1,LastModifierUserId-Nullable`1,CreationTime-DateTime,CreatorUserId-Nullable`1".Split(',');
        private readonly string[] _PaymentFields = "Id-Int32,PaymentTypeId-Int32,PaymentCreatedTime-DateTime,TenderedAmount-Decimal,TerminalName-String,Amount-Decimal,PaymentUserName-String,PaymentTags-String,CreationTime-DateTime,CreatorUserId-Nullable`1,PaymentTypeName-String".Split(',');
        private readonly string[] _TransFields = "TransactionTypeName-String,TransactionTypeId-Int32,TicketId-Int32,Amount-Decimal,TransactionNote-String,CreationTime-DateTime,CreatorUserId-Nullable`1,Id-Int32".Split(',');
        private readonly string[] _OrderFields = "OrderRef-String,IsParent-Boolean,IsUpSelling-Boolean,UpSellingOrderRef-String,DepartmentName-String,MenuItemId-Int32,MenuItemName-String,PortionName-String,Price-Decimal,CostPrice-Decimal,Quantity-Decimal,PortionCount-Int32,Note-String,Locked-Boolean,CalculatePrice-Boolean,IncreaseInventory-Boolean,DecreaseInventory-Boolean,OrderNumber-String:nr,CreatingUserName-String,OrderCreatedTime-DateTime,PriceTag-String,Taxes-String,OrderTags-String,OrderStates-String,OrderStatus-String,IsPromotionOrder-Boolean,PromotionAmount-Decimal,PromotionSyncId-Int32,MenuItemPortionId-Int32,OrderPromotionDetails-String,OrderLog-String,BindState-String,BindStateValues-String,BindCompleted-Boolean,Description-String,OriginalPrice-Decimal,CreationTime-DateTime,CreatorUserId-Nullable`1,Id-Int32".Split(',');
        private readonly string[] _MemberFields = "Id-Int32,MemberCode-String,Name-String,LastName-String".Split(',');
        private readonly List<CKeyValuePair> _keyValues = new List<CKeyValuePair>();
        private readonly Ticket _ticket;
        private readonly GetMemberForEditOutput _member;
        private string _content;
        private readonly bool _IsPlainText;

        public ValueReplacer(string content, Ticket ticket, GetMemberForEditOutput member, bool isPlainText =false)
        {
            _content = content;
            _ticket = ticket;
            _member = member;
            _IsPlainText = isPlainText;
            LoadValues();
        }

        private void LoadValues() {
            var fieldSets = new string[][] { _TicketFields, _PaymentFields, _TransFields, _OrderFields, _MemberFields };
            object[] objs = new object[] { _ticket, _ticket.Payments,_ticket.Transactions,_ticket.Orders, _member.Member };

            var readVal = new Action<int, object, string[],int>((ind, obj, fields,ind2) => {
                var fieldSetNames = "Ticket,Payment,Trans,Order,Member".Split(',');
                foreach (var field in fields)
                {
                    string value = "";
                    var keyVal = field.Split('-');
                    bool noRepeat = false;
                    
                    if (keyVal[1].IndexOf(":") >= 0) {
                        var key2 = keyVal[1].Split(':');
                        keyVal[1] = key2[0];
                        noRepeat = key2[1] == "nr";
                    }

                    try
                    {
                        if (keyVal[0].IndexOf("LocationName") >= 0)
                        {
                            value = _ticket.Location.Name;
                        }
                        else if (keyVal[0].IndexOf("TransactionTypeName") >= 0) {
                            value = ((TicketTransaction)obj).TransactionType.Name;
                        }
                        else
                        {
                            if (obj.GetType().GetProperty(keyVal[0]) != null)
                            {
                                value = obj.GetType()
                                    .GetProperty(keyVal[0])?.GetValue(obj)?.ToString();
                                value = ConvertValueByType(keyVal[1], value);
                            }
                            else
                            {
                                value = "";
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.Print(ex.Message);
                        value = "";
                    }


                    var pv = new CKeyValuePair { Key = $"$[{fieldSetNames[ind]}.{keyVal[0]}]", Value = value };
                    if (ind2 != -1)
                    {
                        string tKey = $"$[{fieldSetNames[ind] }.{ keyVal[0]}]";
                        var tVal = _keyValues.Where(k => k.Key == tKey)?.FirstOrDefault();
                        if (tVal == null)
                        {
                            if (noRepeat == false && _IsPlainText) {
                                pv.Value = Environment.NewLine + pv.Value;
                            }
                            _keyValues.Add(pv);
                        }
                        else
                        {
                            if (noRepeat == false) {
                                tVal.Value = tVal.Value + (_IsPlainText ? Environment.NewLine:"</br>") + value;
                            }
                        }
                    }
                    else {
                        _keyValues.Add(pv);
                    }
                }
            });

            readVal(0, objs[0], fieldSets[0],-1);
            
            for (int i = 0; i < _ticket.Payments.Count; i++)
            {
                var tObj = (List<Payment>)objs[1];
                readVal(1, tObj[i], fieldSets[1],i);
            }
            for (int i = 0; i < _ticket.Transactions.Count; i++)
            {
                var tObj = (List<TicketTransaction>)objs[2];
                readVal(2, tObj[i], fieldSets[2], i);
            }
            for (int i = 0; i < _ticket.Orders.Count; i++)
            {
                var tObj = (List<Order>)objs[3];
                readVal(3, tObj[i], fieldSets[3], i);
            }

            readVal(4, objs[4], fieldSets[4], -1);
        }

        private string ConvertValueByType(string type, string value) {

            if (type == "DateTime")
            {
                DateTime dt;
                if (DateTime.TryParse(value, out dt))
                {
                    value = dt.ToString("yyyy-MM-dd HH:mm");
                }
                else
                {
                    value = "";
                }
            }

            if (type == "Date")
            {
                DateTime dt;
                if (DateTime.TryParse(value, out dt))
                {
                    value = dt.ToString("yyyy-MM-dd");
                }
                else
                {
                    value = "";
                }
            }

            if (type == "Boolean" || type == "bool")
            {
                if (Boolean.Parse(value))
                {
                    value = "Y";
                }
                else
                {
                    value = "N";
                }
            }

            if (type.ToLower() == "decimal")
            {
                value = Decimal.Parse(value).ToString("#,##0.00");
            }

            return value;
        }

        public string GetReplacedContent()
        {

            //var properties = _member.Member.GetType().GetProperties();
            //foreach (var property in properties)
            //{
            //    System.Diagnostics.Debug.Print(property.Name + "-" + property.PropertyType.Name + "-" + property.PropertyType.IsClass);
            //}
            foreach (var keyValue in _keyValues)
            {
                _content = _content.Replace(keyValue.Key, keyValue.Value);
            }
            return _content;
        }
    }

    [AutoMapTo(typeof(TemplateOutput))]
    public class TemplateInput {
        public string Assets { get; set; }
        public string Components { get; set; }
        public string Css { get; set; }
        public string Html { get; set; }
        public string Styles { get; set; }
        public int TemplateId { get; set; }
        public string TemplateName { get; set; }
        public int ServiceInfoTypeId { get; set; }
        public bool IsActive { get; set; }
        public bool StoredAsPlainText { get; set; }
        public string ContentInText { get; set; }
    }

    [AutoMapTo(typeof(TemplateOutput2))]
    public class TemplateOutput
    {
        public string Assets { get; set; }
        public string Components { get; set; }
        public string Css { get; set; }
        public string Html { get; set; }
        public string Styles { get; set; }
        public string ContentInText { get; set; }
    }

    public class TemplateOutput2
    {
        [JsonProperty(PropertyName = "gjs-assets")]
        public string Assets { get; set; }
        [JsonProperty(PropertyName = "gjs-components")]
        public string Components { get; set; }
        [JsonProperty(PropertyName = "gjs-css")]
        public string Css { get; set; }
        [JsonProperty(PropertyName = "gjs-html")]
        public string Html { get; set; }
        [JsonProperty(PropertyName = "gjs-Styles")]
        public string Styles { get; set; }
    }

    public class CKeyValuePair {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}