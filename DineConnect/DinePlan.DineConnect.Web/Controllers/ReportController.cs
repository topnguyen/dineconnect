﻿using System.Data;
using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;

namespace DinePlan.DineConnect.Web.Controllers
{
    [AbpMvcAuthorize]
    public class ReportController : DineConnectControllerBase
    {
        // GET: Outlet
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetReport(int? id)
        {
            return null;
        }

        public ActionResult ViewerEvent()
        {
            return null;
        }
    }
}