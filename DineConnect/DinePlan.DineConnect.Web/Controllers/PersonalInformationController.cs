using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.IO.Extensions;
using Abp.UI;
using Abp.Web.Models;
using Abp.Web.Mvc.Models;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.Net.MimeTypes;
using DinePlan.DineConnect.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Path = System.IO.Path;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.Connect.Master;
using Abp.Authorization;

namespace DinePlan.DineConnect.Web.Controllers
{
    public class PersonalInformationController : DineConnectControllerBase
    {
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IRepository<PersonalInformation> _personalinformationRepo;
        private readonly IRepository<EmployeeDocumentInfo> _employeedocumentinfoRepo;
        private readonly IRepository<LeaveRequest> _leaveRequestRepo;
        private readonly IRepository<Company> _companyRepo;
        private readonly IRepository<GpsAttendance> _gpsattendanceRepo;
        public TenantManager TenantManager { get; set; }

        public PersonalInformationController(
            IBinaryObjectManager binaryObjectManager,
            IRepository<EmployeeDocumentInfo> employeedocumentinfoRepo,
            IRepository<LeaveRequest> leaveRequestRepo,
            IRepository<PersonalInformation> personalinformationRepo,
            IRepository<Company> companyRepo,
            IRepository<GpsAttendance> gpsattendanceRepo
            )
        {
            _binaryObjectManager = binaryObjectManager;
            _personalinformationRepo = personalinformationRepo;
            _employeedocumentinfoRepo = employeedocumentinfoRepo;
            _leaveRequestRepo = leaveRequestRepo;
            _companyRepo = companyRepo;
            _gpsattendanceRepo = gpsattendanceRepo;
        }

        [DisableAuditing]
        public async Task<FileResult> GetPerInfoPicture(IdInput input)
        {
            var personal = await _personalinformationRepo.FirstOrDefaultAsync(input.Id);
            if (personal.ProfilePictureId == null)
            {
                return GetDefaultProfilePicture();
            }

            return await GetPerInfoPictureById(personal.ProfilePictureId.Value);
        }

        [DisableAuditing]
        public async Task<FileResult> GetPerInfoPictureById(string id = "")
        {
            if (id.IsNullOrEmpty())
            {
                return GetDefaultProfilePicture();
            }

            return await GetPerInfoPictureById(Guid.Parse(id));
        }

        [UnitOfWork]
        public async virtual Task<JsonResult> ChangePerInfoPicture(PictureDto input)
        {
            try
            {
                //Check input
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("Picture_Change_Error"));
                }

                bool temporaryCreation = false;

                if (input.Id == 0)
                    temporaryCreation = true;

                string EmpCode = "Temp" + AbpSession.UserId;

                PersonalInformation personal = null;
                if (temporaryCreation == false)
                {
                    personal = await _personalinformationRepo.FirstOrDefaultAsync(input.Id);
                    if (personal == null)
                    {
                        throw new UserFriendlyException(L("EmployeeErr"));
                    }
                    EmpCode = personal.EmployeeCode;
                }


                var file = Request.Files[0];
                string fileName = file.FileName;

                string subPath = "~/UploadedFiles/" + EmpCode;
                bool exists = System.IO.Directory.Exists(Server.MapPath(subPath));

                if (!exists)
                    System.IO.Directory.CreateDirectory(Server.MapPath(subPath));

                file.SaveAs(Path.Combine(Server.MapPath(subPath), fileName));

                if (file.ContentLength > 30720) //3000KB.
                {
                    throw new UserFriendlyException(L("ProfilePicture_Warn_SizeLimit"));
                }

                //Get user

                if (temporaryCreation == false)
                {
                    //Delete old picture
                    if (personal.ProfilePictureId.HasValue)
                    {
                        await _binaryObjectManager.DeleteAsync(personal.ProfilePictureId.Value);
                    }
                }

                //Save new picture
                var storedFile = new BinaryObject(file.InputStream.GetAllBytes());
                await _binaryObjectManager.SaveAsync(storedFile);

                if (temporaryCreation == false)
                {
                    //Update new picture on the user
                    //Return success

                    personal.ProfilePictureId = storedFile.Id;
                    return Json(new MvcAjaxResponse(storedFile.Id));
                }
                else
                {
                    return Json(new MvcAjaxResponse(storedFile.Id));
                }

            }
            catch (UserFriendlyException ex)
            {
                //Return error message
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        private FileResult GetDefaultProfilePicture()
        {
            return File(Server.MapPath("~/Common/Images/default-profile-picture.png"), MimeTypeNames.ImagePng);
        }

        private async Task<FileResult> GetPerInfoPictureById(Guid profilePictureId)
        {
            var file = await _binaryObjectManager.GetOrNullAsync(profilePictureId);
            if (file == null)
            {
                return GetDefaultProfilePicture();
            }

            return File(file.Bytes, MimeTypeNames.ImageJpeg);
        }

        [DisableAuditing]
        public async Task<FileResult> GetGpsPicture(IdInput input)
        {
            var personal = await _gpsattendanceRepo.FirstOrDefaultAsync(input.Id);
            if (personal.Photo == null)
            {
                return GetDefaultProfilePicture();
            }
            return await GetGpsImageById(input);
        }


        private async Task<FileResult> GetGpsImageById(IdInput input)
        {
            var file = await _gpsattendanceRepo.FirstOrDefaultAsync(input.Id);
            if (file == null)
            {
                return GetDefaultProfilePicture();
            }

            return File(file.Photo, MimeTypeNames.ImageJpeg);
        }

        [UnitOfWork]
        public async Task<FileResult> DownloadPersonalDocumentTemplate(EmployeeDocumentDto input)
        {

            EmployeeDocumentInfo empdocinfo = null;
            string EmpRefId;
            empdocinfo = await _employeedocumentinfoRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == input.EmployeeRefId && t.DocumentInfoRefId == input.DocumentInfoRefId);

            if (empdocinfo == null)
            {
                throw new UserFriendlyException(L("FileNotFound"));
            }

            var personal = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == empdocinfo.EmployeeRefId);
            if (personal == null)
            {
                throw new UserFriendlyException(L("EmployeeErr"));
            }

            EmpRefId = personal.EmployeeCode.ToUpper().ToString();

            string subPath = "\\DocumentFiles\\" + EmpRefId + "\\";

            var phyPath = Path.GetFullPath(subPath);

            bool exists = Directory.Exists((phyPath));//System.IO.Server.MapPath

            if (!exists)
            {
                throw new UserFriendlyException(EmpRefId + " " + L("FolderDoesNotExist"));
            }

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = empdocinfo.FileName,
                Inline = true,
            };

            if (empdocinfo.FileExtenstionType == "PDF")
                return File(phyPath + cd.FileName, MimeTypeNames.ApplicationPdf, empdocinfo.FileName);
            else
                return File(phyPath + cd.FileName, empdocinfo.FileExtenstionType, empdocinfo.FileName);

            // return File(Server.MapPath(subPath), MimeTypeNames.ApplicationPdf);
        }


        [UnitOfWork]
        public async virtual Task<JsonResult> ChangeDocumentInfo(DocumentDto input)
        {
            try
            {
                //Check input
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("UploadFileNotExist"));
                }

                string EmpRefId;

                EmployeeDocumentInfo empdocinfo = null;

                empdocinfo = await _employeedocumentinfoRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == input.EmployeeRefId && t.DocumentInfoRefId == input.DocumentInfoRefId);

                if (empdocinfo == null)
                {
                    throw new UserFriendlyException(L("FileNotFound"));
                }

                var personal = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == empdocinfo.EmployeeRefId);
                if (personal == null)
                {
                    throw new UserFriendlyException(L("EmployeeErr"));
                }

                EmpRefId = personal.EmployeeCode.ToUpper().ToString();

                var file = Request.Files[0];
                string fileName = file.FileName;

                string subPath = "\\DocumentFiles\\" + EmpRefId + "\\";

                var phyPath = Path.GetFullPath(subPath);

                bool exists = System.IO.Directory.Exists(Server.MapPath(phyPath));

                if (!exists)
                    System.IO.Directory.CreateDirectory(Server.MapPath(phyPath));

                file.SaveAs(Path.Combine(Server.MapPath(phyPath), fileName));

                //Save new Info
                var storedFile = file.FileName;
                //await _binaryObjectManager.SaveAsync(storedFile);

                //Update new Info on the user
                empdocinfo.FileName = fileName;
                empdocinfo.FileExtenstionType = file.ContentType;

                return Json(new MvcAjaxResponse());

            }
            catch (UserFriendlyException ex)
            {
                //Return error message
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [UnitOfWork]
        public async virtual Task<JsonResult> ChangeEmployeeDocumentInfo(DocumentDto input)
        {
            try
            {
                //Check input
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("UploadFileNotExist"));
                }

                string EmpRefId;

                EmployeeDocumentInfo empdocinfo = null;

                empdocinfo = await _employeedocumentinfoRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == input.EmployeeRefId && t.DocumentInfoRefId == input.DocumentInfoRefId);

                if (empdocinfo == null)
                {
                    throw new UserFriendlyException(L("FileNotFound"));
                }

                var personal = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == empdocinfo.EmployeeRefId);
                if (personal == null)
                {
                    throw new UserFriendlyException(L("EmployeeErr"));
                }

                EmpRefId = personal.EmployeeCode.ToUpper().ToString();

                var file = Request.Files[0];
                string fileName = file.FileName;

                //Update new Info on the user
                empdocinfo.FileName = fileName;
                empdocinfo.FileExtenstionType = file.ContentType;

                var fileSavedName = Path.GetFileName(file.FileName);

                //string Path1 = @"C:\DocumentFiles";
                //string Path2 = @"\" + EmpRefId + @"\";
                //string FullPath = Path.Combine(Path1, Path2.Substring(0, 1) == @"\" ? Path2.Substring(1, Path2.Length - 1) : Path2);

                string subPath = "\\DocumentFiles\\" + EmpRefId + "\\";

                var phyPath = Path.GetFullPath(subPath);

                bool isExists = System.IO.Directory.Exists(phyPath); // ... Checks directory: C:\Users\My\Desktop\Notes\ exists...
                if (!isExists)
                    System.IO.Directory.CreateDirectory(phyPath);

                file.SaveAs(Path.Combine(phyPath, fileSavedName));

                return Json(new MvcAjaxResponse());

            }
            catch (UserFriendlyException ex)
            {
                //Return error message
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

       
        [UnitOfWork]
        public async virtual Task<JsonResult> ChangeLeaveDocumentInfo(LeaveDocumentDto input)
        {
            try
            {
                //Check input
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("UploadFileNotExist"));
                }

                //LeaveRequest leaveRequest = null;

                //leaveRequest = await _leaveRequestRepo.FirstOrDefaultAsync(t => t.Id == input.LeaveRequestRefId/* && t.DocumentInfoRefId == input.DocumentInfoRefId*/);

                //if (leaveRequest == null)
                //{
                //    throw new UserFriendlyException(L("FileNotFound"));
                //}
                var rejectedString = L("Rejected");
                var leaveRequest = await _leaveRequestRepo.FirstOrDefaultAsync(t => t.Id == input.LeaveRequestRefId && t.LeaveStatus != rejectedString);
                if (leaveRequest == null)
                {
                    throw new UserFriendlyException(L("Error"));
                }

                var file = Request.Files[0];
                string fileName = file.FileName;

                //Update new Info on the user
                leaveRequest.FileName = fileName;
                leaveRequest.FileExtenstionType = file.ContentType;

                var fileSavedName = Path.GetFileName(file.FileName);

                //string Path1 = @"C:\DocumentFiles";
                //string Path2 = @"\" + EmpRefId + @"\";
                //string FullPath = Path.Combine(Path1, Path2.Substring(0, 1) == @"\" ? Path2.Substring(1, Path2.Length - 1) : Path2);

                string subPath = "\\DocumentFiles\\LeaveRequestDocuments\\";

                var phyPath = Path.GetFullPath(subPath);

                bool isExists = System.IO.Directory.Exists(phyPath); // ... Checks directory: C:\Users\My\Desktop\Notes\ exists...
                if (!isExists)
                    System.IO.Directory.CreateDirectory(phyPath);

                subPath += leaveRequest.Id + "\\";

                phyPath = Path.GetFullPath(subPath);

                isExists = System.IO.Directory.Exists(phyPath); // ... Checks directory: C:\Users\My\Desktop\Notes\ exists...
                if (!isExists)
                    System.IO.Directory.CreateDirectory(phyPath);

                file.SaveAs(Path.Combine(phyPath, fileSavedName));

                return Json(new MvcAjaxResponse());

            }
            catch (UserFriendlyException ex)
            {
                //Return error message
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
        [UnitOfWork]
        public async Task<FileResult> DownloadLeaveDocumentInfo(LeaveDocumentDto input)
        {
            var leaveRequest = await _leaveRequestRepo.FirstOrDefaultAsync(t => t.Id == input.LeaveRequestRefId);
            if (leaveRequest == null)
            {
                throw new UserFriendlyException(L("LeaveRequest") + L("Error"));
            }

            string subPath = "\\DocumentFiles\\LeaveRequestDocuments\\";

            var phyPath = Path.GetFullPath(subPath);

            bool isExists = System.IO.Directory.Exists(phyPath);

            if (!isExists)
                throw new UserFriendlyException("LeaveRequestDocuments" + " " + L("FolderDoesNotExist"));

            subPath += leaveRequest.Id + "\\";

            phyPath = Path.GetFullPath(subPath);

            isExists = System.IO.Directory.Exists(phyPath); // ... Checks directory: C:\Users\My\Desktop\Notes\ exists...
            if (!isExists)
                throw new UserFriendlyException(input.LeaveRequestRefId + " " + L("FolderDoesNotExist"));


            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = leaveRequest.FileName,
                Inline = true,
            };

            if (leaveRequest.FileExtenstionType == "PDF")
                return File(phyPath + cd.FileName, MimeTypeNames.ApplicationPdf, leaveRequest.FileName);
            else
                return File(phyPath + cd.FileName, leaveRequest.FileExtenstionType, leaveRequest.FileName);

            // return File(Server.MapPath(subPath), MimeTypeNames.ApplicationPdf);
        }
    
        public string GetFileExtenstion(string FileNameWithExtenstion)
        {
            string temp = FileNameWithExtenstion;
            int a = temp.LastIndexOf('.');
            string fileExt = temp.Substring(a + 1);
            return fileExt;
        }

        private List<String> GetSentences(String text, String word)
        {
            var sentences = text.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

            var matches = from sentence in sentences
                          where sentence.ToLower().Contains(word.ToLower())
                          select sentence;

            return matches.ToList();
        }

        //private void EncryptPDF(string source, string destination, string password)
        //{
        //    //Encryption Password Format ddMMM (ex:- 14/July/2018 should be entered like 14jul)

        //    using (PdfReader reader = new PdfReader(source))
        //    {
        //        using (PdfStamper pdfStamper = new PdfStamper(reader, new FileStream(destination, FileMode.Create)))
        //        {
        //            pdfStamper.SetEncryption(true, password.ToLower(), password.ToLower(), PdfWriter.AllowPrinting);
        //        }
        //    }
        //}

        private void DeletePDF(string fileName)
        {
            if (fileName != null || fileName != string.Empty)
            {
                if ((System.IO.File.Exists(fileName)))
                {
                    System.IO.File.Delete(fileName);
                }
            }
        }


        [AbpAuthorize]
        [UnitOfWork]
        public async Task<FileResult> SalarySlipReportDownload(SalaryPrepareDto input)
        {
            var personal = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
            var tenant = await TenantManager.GetByIdAsync(personal.TenantId);
            var tenantName = tenant.TenancyName;

            #region FilePathDefinition

            //string subPath = "\\DocumentFiles\\";

            //var phyPath = Path.GetFullPath(subPath);

            //bool exists = Directory.Exists((phyPath));

            //if (!exists)
            //{
            //    System.IO.Directory.CreateDirectory(phyPath);
            //}

            //subPath = "\\DocumentFiles\\" + "\\Reports\\";

            //phyPath = Path.GetFullPath(subPath);

            //exists = Directory.Exists((phyPath));

            //if (!exists)
            //{
            //    System.IO.Directory.CreateDirectory(phyPath);
            //}

            //subPath = "\\DocumentFiles\\" + "\\Reports\\SalarySingapore";

            //phyPath = Path.GetFullPath(subPath);

            //exists = Directory.Exists((phyPath));

            //if (!exists)
            //{
            //    System.IO.Directory.CreateDirectory(phyPath);
            //}
            ////templatePath = phyPath;

            //subPath = "\\DocumentFiles\\" + "\\Reports\\SalarySingapore\\" + input.EmployeeRefId;

            //phyPath = Path.GetFullPath(subPath);

            //exists = Directory.Exists((phyPath));

            //if (!exists)
            //{
            //    System.IO.Directory.CreateDirectory(phyPath);
            //}

            string subPath = "\\DocumentFiles\\" + "\\Reports\\" + tenantName + "\\" + input.MonthYearString + "\\" + personal.Id;

            string phyPath = Path.GetFullPath(subPath);

            bool exists = Directory.Exists((phyPath));

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(phyPath);
            }
            #endregion

            string fileName = personal.EmployeeCode + "_" + personal.EmployeeName /*+ input.StartDate.ToString("dd MMM yyyy") + input.EndDate.ToString("dd MMM yyyy")*/;
            string fileExtension = input.FileExtenstionType;
            if (!input.FileExtenstionType.ToUpper().Equals("PDF"))
            {
                fileExtension = "xlsx";
            }
            if (fileName.Contains("."))
            {
                fileName = fileName.Left(fileName.IndexOf("."));
            }
            fileName = fileName + "." + fileExtension;

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = fileName,
                Inline = true,
            };

            if (input.FileExtenstionType.ToUpper() == "PDF")
                return File(phyPath + @"\" + cd.FileName, MimeTypeNames.ApplicationPdf, cd.FileName);
            else
                return File(phyPath + @"\\" + cd.FileName, "xlsx", cd.FileName);
        }

    }
}
