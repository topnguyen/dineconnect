﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DinePlan.DineConnect.Web.Controllers.Extensions;

namespace DinePlan.DineConnect.Web.Controllers
{
    public class ImagesController : DineConnectControllerBase
    {
        private readonly IAppFolders _appFolders;


        public ImagesController(IAppFolders appFolders)
        {
            _appFolders = appFolders;
        }

        [OutputCache(VaryByParam = "*", Duration = 60 * 60 * 24 * 365)]
        public ImageResult Index(string file, int w = 0, int h = 0)
        {
            if (System.IO.File.Exists(file))
            {
                return new ImageResult(file);
            }
            var fullPath = Path.Combine(_appFolders.ImagesFolder, file);
            return new ImageResult(fullPath, w, h);
        }
    }

    
}