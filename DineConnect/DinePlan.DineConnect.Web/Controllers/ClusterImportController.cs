﻿using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.UI;
using Abp.Web.Models;
using Abp.Web.Mvc.Models;
using DinePlan.DineConnect.Cluster;
using DinePlan.DineConnect.Cluster.Master;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.Cluster;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.Net.MimeTypes;
using DinePlan.DineConnect.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Web.Mvc;
using DinePlan.DineConnect.Cluster.Master.Implementation;

namespace DinePlan.DineConnect.Web.Controllers
{
    public class ClusterImportController : DineConnectControllerBase
    {
        private const int MaxConnectCardSize = 2097152; //2MB
        public TenantManager TenantManager { get; set; }
        private readonly IRepository<DelAggCategory> _delaggcategoryRepo;
        private readonly IRepository<DelAggItemGroup> _delaggitemgroupRepo;
        private readonly IRepository<DelAggVariantGroup> _delaggvariantgroupRepo;
        private readonly IRepository<DelAggVariant> _delaggvariantRepo;
        private readonly IRepository<DelAggItem> _delaggitemRepo;
        private readonly IRepository<DelAggTax> _delaggtaxRepo;
        private readonly IRepository<MenuItem> _menuitemRepo;
        private readonly IRepository<MenuItemPortion> _menuitemportionRepo;
        private readonly IDelAggItemAppService _delaggItemAppService;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;


        public ClusterImportController(IRepository<DelAggCategory> delaggcategoryRepo,
        IRepository<DelAggItemGroup> delaggitemgroupRepo,
        IRepository<DelAggVariantGroup> delaggvariantgroupRepo,
        IRepository<DelAggVariant> delaggvariantRepo,
        IRepository<DelAggItem> delaggitemRepo,
        IRepository<DelAggTax> delaggtaxRepo,
        IRepository<MenuItem> menuitemRepo,
        IRepository<MenuItemPortion> menuitemportionRepo,
        IDelAggItemAppService delaggItemAppService,
        IReportBackgroundAppService rbas,
         IBackgroundJobManager bgm
            )
        {
            _delaggcategoryRepo = delaggcategoryRepo;
            _delaggitemgroupRepo = delaggitemgroupRepo;
            _delaggvariantgroupRepo = delaggvariantgroupRepo;
            _delaggvariantRepo = delaggvariantRepo;
            _delaggitemRepo = delaggitemRepo;
            _delaggtaxRepo = delaggtaxRepo;
            _menuitemRepo = menuitemRepo;
            _menuitemportionRepo = menuitemportionRepo;
            _delaggItemAppService = delaggItemAppService;
            _rbas = rbas;
            _bgm = bgm;
        }

        public FileResult ImportDelAggItem()
        {
            return File(Server.MapPath("~/Common/ClusterImport/DelAggItemTemplate.xlsx"),
               MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }

        
        public virtual async Task<JsonResult> ImportDelAggItemData()
        {
            int rowP = 0;

            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();

                List<CreateOrUpdateDelAggItemInput> bulkList = new List<CreateOrUpdateDelAggItemInput>();
                int? delAggItemId = null, delAggVariantGroupId = null, delAggVariantId=null, menuitemPortionId=null;
                string variantGroupName=null, variantName =null;
                decimal salesPrice = 0, markupPrice = 0;

                foreach (var row in allList)
                {
                    string categoryName = row["Category"].ToString().ToUpper();
                    string taxName = row["Tax"].ToString().ToUpper();
                    string itemGroupName = row["ItemGroup"].ToString().ToUpper();
                    string menuItemName = row["MenuItem"].ToString().ToUpper();
                    //variantGroupName = row["VariantGroupName"].ToString().ToUpper();
                    //variantName = row["VariantName"].ToString().ToUpper();
                    salesPrice = row["SalesPrice"] != null ? Convert.ToDecimal(row["SalesPrice"].ToString().Trim()) : 0M;
                    markupPrice = row["MarkupPrice"] != null ? Convert.ToDecimal(row["MarkupPrice"].ToString().Trim()) : 0M;
                    var item = new DelAggItemEditDto
                    {
                        Code = row["Code"]?.ToString().Trim() ?? "",
                        Name = row["Name"]?.ToString().Trim() ?? "",
                        Description = row["Description"]?.ToString().Trim() ?? "",
                        FoodType = row["FoodType"]?.ToString().Trim() ?? "",
                        Weight = row["Weight"] != null ? Convert.ToDecimal(row["Weight"].ToString().Trim()) : 0M,
                        Serves = row["Serves"] != null ? Convert.ToDecimal(row["Serves"].ToString().Trim()) : 0M,
                        //DelAggCatId = !string.IsNullOrEmpty(row["Category"].ToString()) ? Convert.ToInt32(row["Category"].ToString().Trim()) : 0,
                        //DelAggTaxId = !string.IsNullOrEmpty(row["Tax"].ToString()) ? Convert.ToInt32(row["Tax"].ToString().Trim()) : 0,
                        //DelAggItemGroupId = !string.IsNullOrEmpty(row["ItemGroup"].ToString()) ? Convert.ToInt32(row["ItemGroup"].ToString().Trim()) : 0,
                        //MenuItemId = !string.IsNullOrEmpty(row["MenuItem"].ToString()) ? Convert.ToInt32(row["MenuItem"].ToString().Trim()) : 0,
                        RowIndex = rowP + 2
                    };
                    
                    if (string.IsNullOrEmpty(item.Code))
                    {
                        continue;
                    }
                    if (string.IsNullOrEmpty(item.Name))
                    {
                        continue;
                    }
                    
                    var existsDelItem = await _delaggitemRepo.FirstOrDefaultAsync(t => t.Code.Equals(item.Code) || t.Name.Equals(item.Name));
                    if(existsDelItem != null)
                    {
                        delAggItemId = existsDelItem.Id;
                    }

                    var existsCategory = await _delaggcategoryRepo.FirstOrDefaultAsync(t => t.Name.Equals(categoryName));
                    if(existsCategory != null)
                    {
                        item.DelAggCatId = existsCategory.Id;
                        item.DelAggCatName = existsCategory.Name;
                    }
                    else
                    {
                        
                    }
                    var existsTax = await _delaggtaxRepo.FirstOrDefaultAsync(t => t.Name.Equals(taxName));
                    if (existsTax != null)
                    {
                        item.DelAggTaxId = existsTax.Id;
                        item.DelAggTaxName = existsTax.Name;
                    }
                    else
                    {

                    }
                    var existsMenuItem = await _menuitemRepo.FirstOrDefaultAsync(t => t.Name.Equals(menuItemName));
                    if (existsMenuItem != null)
                    {
                        item.MenuItemId = existsMenuItem.Id;
                        item.MenuItemName = existsMenuItem.Name;
                        var existsMenuItemPortion = await _menuitemportionRepo.FirstOrDefaultAsync(t => t.MenuItemId == item.MenuItemId);
                        if (existsMenuItemPortion != null)
                        {
                            menuitemPortionId = existsMenuItemPortion.Id;
                            variantGroupName = item.MenuItemId + "-" + item.MenuItemName;
                            variantName = menuitemPortionId + "-" + existsMenuItemPortion.Name;
                        }
                    }
                    else
                    {

                    }
                    var existsItemGroup = await _delaggitemgroupRepo.FirstOrDefaultAsync(t => t.Name.Equals(itemGroupName));
                    if (existsItemGroup != null)
                    {
                        item.DelAggItemGroupId = existsItemGroup.Id;
                        item.DelAggItemGroupName = existsItemGroup.Name;
                    }
                    else
                    {

                    }
                   

                    //var existsVariantGroupName = await _delaggvariantgroupRepo.FirstOrDefaultAsync(t => t.Name.Equals(variantGroupName));
                    //if(existsVariantGroupName != null)
                    //{
                    //    delAggVariantGroupId = existsVariantGroupName.Id;
                    //}
                    //var variantGroup = new DelAggVariantGroupEditDto
                    //{
                    //    Id = delAggVariantGroupId,
                    //    Name = variantGroupName
                    //};

                  
                    //var existsVariantName = await _delaggvariantRepo.FirstOrDefaultAsync(t => t.Name.Equals(variantName));
                    //if (existsVariantName != null)
                    //{
                    //    delAggVariantId = existsVariantName.Id;
                    //    variantName = existsVariantName.Name;
                    //    salesPrice = existsVariantName.SalesPrice;
                    //    markupPrice = existsVariantName.MarkupPrice;
                    //}
                    List<DelAggVariantEditDto> variantEditDto = new List<DelAggVariantEditDto>();

                    DelAggVariantEditDto variantDto = new DelAggVariantEditDto()
                    {
                        Name = variantName,
                        MenuItemPortionId=menuitemPortionId.Value,
                        SalesPrice=salesPrice,
                        MarkupPrice=markupPrice
                    };
                    DelAggVariantGroupEditDto variantGroupDto = new DelAggVariantGroupEditDto()
                    {
                        Name=variantGroupName
                    };
                    variantEditDto.Add(variantDto);

                    var DelAggVariants = variantEditDto.MapTo<List<DelAggVariantEditDto>>();
                    rowP++;
                    CreateOrUpdateDelAggItemInput DelAggItemDto = new CreateOrUpdateDelAggItemInput()
                    {
                        DelAggItem=item,
                        DelAggVariantGroup = variantGroupDto,
                        DelAggVariant = DelAggVariants
                    };
                    bulkList.Add(DelAggItemDto);
                }

                //var anyDuplicate = dtos.GroupBy(x => new { x.Name, x.TPName }).Where(g => g.Count() > 1);
                //if (anyDuplicate.Any())
                //{
                //    var builder = new StringBuilder();
                //    builder.Append(string.Format(L("DuplicateExistsInFile_f"), L("Name")));
                //    foreach (var allD in anyDuplicate)
                //    {
                //        builder.Append(allD.Key.Name);
                //        builder.Append(",");
                //    }
                //    throw new UserFriendlyException(builder.ToString());
                //}
                //var alaisDuplicates = dtos.Where(a => !string.IsNullOrEmpty(a.Code)).GroupBy(x => new { x.Code, x.TPName }).Where(g => g.Count() > 1);
                //if (alaisDuplicates.Any())
                //{
                //    var builder = new StringBuilder();
                //    builder.Append(string.Format(L("DuplicateExistsInFile_f"), L("Code")));
                //    foreach (var allD in alaisDuplicates)
                //    {
                //        builder.Append(allD.Key + ",");
                //    }
                //    throw new UserFriendlyException(builder.ToString());
                //}


                //if (bulkList.Count > 0)
                //{
                //    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                //    {
                //        ReportName = "Bulk Delivery Agg Item Import",
                //        Completed = false,
                //        UserId = AbpSession.UserId.Value,
                //        TenantId = AbpSession.TenantId.Value,
                //        ReportDescription = "Bulk Delivery Agg Item Import"
                //    });

                //    if (backGroundId > 0)
                //    {
                //        await _bgm.EnqueueAsync<DelAggItemBackgroundImport, DelAggItemBackgroundImportIdJobArgs>(new DelAggItemBackgroundImportIdJobArgs
                //        {
                //            BackGroundId = backGroundId,
                //            UserId = AbpSession.UserId.Value,
                //            TenantId = AbpSession.TenantId.Value,
                //            BulkList = bulkList
                //        });
                //        return Json(new MvcAjaxResponse(new FileDto { BackGroudId = backGroundId }));
                //        //return new FileDto { BackGroudId = backGroundId };
                //    }
                //}

               await _delaggItemAppService.BulkDelAggItemImport(new BulkDelAggItemImportDtos
                {
                    //BackGroundId = args.BackGroundId,
                    //TenantId = args.TenantId,
                    //UserId = args.UserId,
                    CreateOrUpdateDelAggItemInputs = bulkList
                });

                return Json(new MvcAjaxResponse());
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo($"{ex.Message} {L("Row")} {rowP}")));
            }
        }
    }
}