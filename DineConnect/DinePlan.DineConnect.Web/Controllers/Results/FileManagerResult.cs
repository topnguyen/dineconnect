﻿using System;
using Microsoft.OData.Edm.Library;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Web.Controllers.Results
{
    public class FileManagerResult
    {
        public FileManagerResult()
        {
            result = new List<FileManager>();
        }

        public List<FileManager> result { get; set; }
    }

    public class FileManager
    {
        public string name { get; set; }
        public string rights { get; set; }

        public string size { get; set; }
        public DateTime date { get; set; }
        public string dateStr { get; set; }

        public string type { get; set; }
        public string fullPath { get; set; }
    }
}