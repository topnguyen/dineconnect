using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.IO.Extensions;
using Abp.UI;
using Abp.Web.Models;
using Abp.Web.Mvc.Models;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Net.MimeTypes;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Web.Utility;
using Minio;
using Minio.Exceptions;
using OfficeOpenXml;

namespace DinePlan.DineConnect.Web.Controllers
{
    public class FileUploadController : DineConnectControllerBase
    {
        private const int MaxSize = 5242880; //5MB

        private readonly IAppFolders _appFolders;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IRepository<SupplierGSTInformation> _suppliergstinformationRepo;
        private readonly IRepository<Supplier> _supplierRepo;
        private readonly ITenantSettingsAppService _tenantSettingsService;

        public FileUploadController(IBinaryObjectManager binaryObjectManager,
            IRepository<SupplierGSTInformation> suppliergstinformationRepo,
            IRepository<Supplier> supplierRepo,
            IAppFolders appFolders, ITenantSettingsAppService tenantSettingsService
        )
        {
            _binaryObjectManager = binaryObjectManager;
            _suppliergstinformationRepo = suppliergstinformationRepo;
            _supplierRepo = supplierRepo;
            _appFolders = appFolders;
            _tenantSettingsService = tenantSettingsService;
        }

        [DisableAuditing]
        public async Task<FileResult> GetFileUploadInfo(IdInput input)
        {
            var docinfo = await _suppliergstinformationRepo.FirstOrDefaultAsync(input.Id);
            if (docinfo.GstCertificate == null) return GetDefaultFileUploadInfo();

            return await GetFileUploadInfoById(docinfo.GstCertificate.Value);
        }

        [DisableAuditing]
        public async Task<FileResult> GetFileUploadInfoById(string id = "")
        {
            if (id.IsNullOrEmpty()) return GetDefaultFileUploadInfo();

            return await GetFileUploadInfoById(Guid.Parse(id));
        }

        private FileResult GetDefaultFileUploadInfo()
        {
            return File(Server.MapPath("~/Common/Images/default-FileUpload-Info.png"), MimeTypeNames.ImagePng);
        }

        private async Task<FileResult> GetFileUploadInfoById(Guid FileUploadInfoId)
        {
            var file = await _binaryObjectManager.GetOrNullAsync(FileUploadInfoId);
            if (file == null) return GetDefaultFileUploadInfo();

            return File(file.Bytes, MimeTypeNames.ImageJpeg);
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ChangeFileUploadInfo(FileUploadDto input)
        {
            try
            {
                //Check input
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                    throw new UserFriendlyException(L("Info_Change_Error"));

                string SupRefId;

                SupplierGSTInformation suppliergstinfo = null;

                suppliergstinfo =
                    await _suppliergstinformationRepo.FirstOrDefaultAsync(t => t.Gstin.Equals(input.gstIn));

                if (suppliergstinfo == null) throw new UserFriendlyException(L("FileNotFound"));

                var supplier = await _supplierRepo.FirstOrDefaultAsync(t => t.Id == suppliergstinfo.SupplierRefId);
                if (supplier == null) throw new UserFriendlyException(L("SupplierErr"));

                SupRefId = supplier.SupplierCode.ToUpper();

                var file = Request.Files[0];
                var fileName = file.FileName;

                var subPath = "~/ReportFileUploads/" + SupRefId;
                var exists = Directory.Exists(Server.MapPath(subPath));

                if (!exists)
                    Directory.CreateDirectory(Server.MapPath(subPath));

                file.SaveAs(Path.Combine(Server.MapPath(subPath), fileName));
                //Get user

                //Save new Info
                var storedFile = new BinaryObject(file.InputStream.GetAllBytes());
                await _binaryObjectManager.SaveAsync(storedFile);
                // var storedFile = file.FileName;//

                //await _binaryObjectManager.SaveAsync(storedFile);

                //Update new Info on the user
                suppliergstinfo.GstCertificate = storedFile.Id;

                var fileExtenstion = file.ContentType.Substring(file.ContentType.LastIndexOf("/") + 1).ToUpper();

                //Return success
                return Json(new MvcAjaxResponse());
            }
            catch (UserFriendlyException ex)
            {
                //Return error message
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
        [UnitOfWork]
        public virtual async Task<JsonResult> UploadFileSalesTarget(FileDto input)
        {
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                    throw new UserFriendlyException(L("Info_Change_Error"));
                var upload = Request.Files[0];
                var fileName = upload.FileName;
                var info = new FileInfo(fileName);
                var myFileName = Guid.NewGuid().ToString("n") + info.Extension;
                var setting = await _tenantSettingsService.GetAllSettings();
                var serverUrl = setting.Connect.ServerUrl;
                var accessKey = setting.Connect.AccessKey;
                var secretKey = setting.Connect.SecretKey;
                var bucketName = setting.Connect.BucketName;
                var cloudinary = setting.Connect.Cloudinary;

                var isLocal = Request.Params["IsLocal"] == "true";

                if (serverUrl.IsNullOrEmpty() || accessKey.IsNullOrEmpty() || secretKey.IsNullOrEmpty() ||
                    bucketName.IsNullOrEmpty() || isLocal)
                {
                    var myFileNameSub = "Sales_Target.xlsx";
                    var fullPathSub = Path.Combine(_appFolders.ImportFolder, myFileNameSub);
                    if (System.IO.File.Exists(fullPathSub))
                    {
                        System.IO.File.Delete(fullPathSub);
                    }
                    upload.SaveAs(fullPathSub);
                    var fullPath = Path.Combine(_appFolders.UploadFolder, myFileName);
                    upload.SaveAs(fullPath);
                    var fileData = new FileData(myFileName, fullPath, input.FileTag);
                    return Json(new MvcAjaxResponse(fileData));
                }
                else
                {
                    return Json(new MvcAjaxResponse());
                }    
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        public JsonResult UploadTicketJournals(JournalInputDto input)
        {
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                    throw new UserFriendlyException(L("Info_Change_Error"));
                var upload = Request.Files[0];
                var fileName = upload.FileName;
                var subPath = _appFolders.TicketJournalFolder;

                if (input != null && !string.IsNullOrEmpty(input.FolderName))
                    subPath = _appFolders.TicketJournalFolder + "/" + input.FolderName;

                var exists = Directory.Exists(subPath);
                if (!exists)
                    Directory.CreateDirectory(subPath);

                var fullPath = Path.Combine(subPath, fileName);
                upload.SaveAs(fullPath);

                var fileData = new FileData(upload.FileName, fullPath);
                return Json(true);
            }
            catch (UserFriendlyException ex)
            {
                return Json(false);
            }
        }

        public JsonResult UploadFTIJournals(JournalInputDto input)
        {
            try
            {

                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                    throw new UserFriendlyException(L("Info_Change_Error"));
                var upload = Request.Files[0];
                var fileName = upload.FileName;
                var subPath = _appFolders.FullTaxInvoiceFolder;

                if (input != null && !string.IsNullOrEmpty(input.FolderName))
                    subPath = Path.Combine(subPath, input.FolderName);

                var exists = Directory.Exists(subPath);
                if (!exists)
                    Directory.CreateDirectory(subPath);

                var fullPath = Path.Combine(subPath, fileName);
                upload.SaveAs(fullPath);

                var fileData = new FileData(upload.FileName, fullPath);
                return Json(true);

            }
            catch (Exception exception)
            {
                Logger.Fatal("Issue in Uploading Start");
                Logger.Fatal(exception.Message);
                Logger.Fatal("Issue in Uploading End");

                return Json(false);
            }
        }

        public JsonResult UploadDocument()
        {
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                    throw new UserFriendlyException(L("Info_Change_Error"));
                var upload = Request.Files[0];
                var fileName = Path.GetRandomFileName();
                var fullPath = Path.Combine(_appFolders.TicketJournalFolder, fileName);
                upload.SaveAs(fullPath);
                var fileData = new FileData(upload.FileName, fullPath);
                return Json(new MvcAjaxResponse(fileData));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        public ActionResult DownloadDocument(string fileName, string fileSystemName)
        {
            if (!string.IsNullOrEmpty(fileName) && !string.IsNullOrEmpty(fileSystemName))
            {
                var fullPath = Path.Combine(_appFolders.ImagesFolder, fileSystemName);
                if (System.IO.File.Exists(fullPath))
                    return File(fullPath, MimeTypeNames.ApplicationOctetStream, fileName);
            }

            return null;
        }

        public JsonResult UploadMenuItem()
        {
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                    throw new UserFriendlyException(L("Info_Change_Error"));
                var upload = Request.Files[0];
                var fileName = Path.GetRandomFileName();
                var fullPath = Path.Combine(_appFolders.ImagesFolder, fileName);
                upload.SaveAs(fullPath);
                var fileData = new FileData(upload.FileName, fullPath);
                return Json(new MvcAjaxResponse(fileData));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        public ActionResult DownloadMenuItem(string fileName, string fileSystemName)
        {
            if (!string.IsNullOrEmpty(fileName) && !string.IsNullOrEmpty(fileSystemName))
            {
                var fullPath = Path.Combine(_appFolders.ImagesFolder, fileSystemName);
                if (System.IO.File.Exists(fullPath))
                    return File(fullPath, MimeTypeNames.ApplicationOctetStream, fileName);
            }

            return null;
        }

        public ActionResult DownloadMenuFromServer(string fileSystemName)
        {
            if (!string.IsNullOrEmpty(fileSystemName))
            {
                var fullPath = Path.Combine(_appFolders.ImagesFolder, fileSystemName);
                if (System.IO.File.Exists(fullPath))
                {
                    Stream image = new FileStream(fullPath, FileMode.Open);
                    return File(image, MimeTypeNames.ApplicationOctetStream);
                }
            }

            return null;
        }

        [DisableAuditing]
        public async Task<FileResult> GetMenuImage(string fname)
        {
            var fullPath = Path.Combine(_appFolders.ImagesFolder, fname);
            return System.IO.File.Exists(fullPath) ? File(fullPath, MimeTypeNames.ApplicationOctetStream, fname) : null;
        }

        #region Download File from UploadFolder or MinIO

        public async Task<FileResult> DownloadFile(string fileName, string url)
        {
            try
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                var serverUrl = setting.Connect.ServerUrl;
                var accessKey = setting.Connect.AccessKey;
                var secretKey = setting.Connect.SecretKey;

                var cloudinary = setting.Connect.Cloudinary;

                if (!cloudinary && !serverUrl.IsNullOrEmpty())
                {
                    var endpoint = serverUrl.Split("//")[1];
                    var minio = new MinioClient(endpoint, accessKey, secretKey);

                    var ms = new MemoryStream();
                    var bucketName = setting.Connect.BucketName;
                    await minio.GetObjectAsync(bucketName, fileName,
                        stream =>
                        {
                            stream.CopyTo(ms);
                            ms.Position = 0;
                        });

                    return File(ms, MediaTypeNames.Application.Octet, fileName);
                }

                if (cloudinary)
                {
                    var webRequest = (HttpWebRequest)WebRequest.Create(url);
                    webRequest.AllowWriteStreamBuffering = true;
                    webRequest.Timeout = 30000;

                    var webResponse = webRequest.GetResponse();
                    var stream = webResponse.GetResponseStream();
                    return File(stream, MimeTypeNames.ApplicationOctetStream, fileName);
                }

                var fullPath = Path.Combine(_appFolders.UploadFolder, fileName);
                return System.IO.File.Exists(fullPath)
                    ? File(fullPath, MimeTypeNames.ApplicationOctetStream, fileName)
                    : null;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Cannot download file!");
            }
        }

        #endregion Download File from UploadFolder or MinIO

        #region Download File To UploadFolder or MinIO

        public async Task<JsonResult> UploadFile(FileDto input)
        {
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                    throw new UserFriendlyException(L("Info_Change_Error"));

                var upload = Request.Files[0];
                var fileName = upload.FileName;

                var info = new FileInfo(fileName);
                var myFileName = Guid.NewGuid().ToString("n") + info.Extension;

                var setting = await _tenantSettingsService.GetAllSettings();
                var serverUrl = setting.Connect.ServerUrl;
                var accessKey = setting.Connect.AccessKey;
                var secretKey = setting.Connect.SecretKey;
                var bucketName = setting.Connect.BucketName;
                var cloudinary = setting.Connect.Cloudinary;

                var isLocal = Request.Params["IsLocal"] == "true";

                if (serverUrl.IsNullOrEmpty() || accessKey.IsNullOrEmpty() || secretKey.IsNullOrEmpty() ||
                    bucketName.IsNullOrEmpty() || isLocal)
                {
                    var fullPath = Path.Combine(_appFolders.UploadFolder, myFileName);
                    upload.SaveAs(fullPath);
                    var fileData = new FileData(myFileName, fullPath, input.FileTag);
                    return Json(new MvcAjaxResponse(fileData));
                }

                if (cloudinary)
                {
                    var myData = await UploadToCloudinary(serverUrl, accessKey, secretKey, bucketName, upload,
                        myFileName);
                    var fileData = new FileData(myFileName, myData, input.FileTag);
                    return Json(new MvcAjaxResponse(fileData));
                }
                else
                {
                    var myData = await UploadToMinIO(serverUrl, accessKey, secretKey, bucketName, upload,
                        myFileName);
                    var fileData = new FileData(myFileName, myData, input.FileTag);

                    return Json(new MvcAjaxResponse(fileData));
                }
            }
            catch (UserFriendlyException ex)
            {
                throw ex;
            }
        }

        public Cloudinary m_cloudinary { get; set; }

        private async Task<string> UploadToCloudinary(string url, string accessKey, string secretKey,
            string bucketName, HttpPostedFileBase upload, string mfileName)
        {
            var contentType = upload.ContentType;
            try
            {
                byte[] fileBytes;
                using (var stream = upload.InputStream)
                {
                    fileBytes = stream.GetAllBytes();
                }

                var name = Guid.NewGuid().ToString("N");
                var imageId = Guid.NewGuid().ToString("N");
                var uploadParam = new ImageUploadParams
                {
                    File = new FileDescription(name, new MemoryStream(fileBytes)),
                    PublicId = imageId
                };
                if (m_cloudinary == null)
                {
                    var acc = new Account(
                        bucketName,
                        accessKey,
                        secretKey);

                    m_cloudinary = new Cloudinary(acc);
                }

                if (m_cloudinary != null)
                {
                    var result = await m_cloudinary.UploadAsync(uploadParam);
                    return result.Url.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return null;
        }


        private async Task<string> UploadToMinIO(string url, string accessKey, string secretKey,
            string bucketName, HttpPostedFileBase upload, string mfileName)
        {

            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en");
                var endpoint = url.Split("//")[1];
                var minio = new MinioClient(endpoint, accessKey, secretKey);
                var contentType = upload.ContentType;
                var found = await minio.BucketExistsAsync(bucketName);
                if (!found) await minio.MakeBucketAsync(bucketName);

                await minio.PutObjectAsync(bucketName, mfileName, upload.InputStream, upload.InputStream.Length,
                    contentType);
                return new Uri(new Uri(url), $"{bucketName}/{mfileName}").ToString();
            }
            catch (MinioException e)
            {
                throw e;
            }
        }

        #endregion Download File To UploadFolder or MinIO
    }

    public class FileData
    {
        public FileData(string fileName, string fileSystemName)
        {
            FileName = fileName;
            FileSystemName = fileSystemName;
        }
        public FileData(string fileName, string fileSystemName, string fileTag)
        {
            FileName = fileName;
            FileSystemName = fileSystemName;
            FileTag = fileTag;
        }

        public string FileTag { get; set; }
        public string FileName { get; set; }
        public string FileSystemName { get; set; }
    }

    public class JournalInputDto
    {
        public string FolderName { get; set; }
    }
}