﻿using Abp.Auditing;
using Abp.UI;
using Abp.Web.Mvc.Authorization;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Storage;
using System;
using System.IO;
using System.Web.Mvc;

namespace DinePlan.DineConnect.Web.Controllers
{
    public class FileController : DineConnectControllerBase
    {
        private readonly IAppFolders _appFolders;
        private readonly ITempFileCacheManager _tempFileCacheManager;

        public FileController(IAppFolders appFolders, ITempFileCacheManager tempFileCacheManager)
        {
            _appFolders = appFolders;
            _tempFileCacheManager = tempFileCacheManager;
        }

        [AbpMvcAuthorize]
        [DisableAuditing]
        public ActionResult DownloadTempFile(FileDto file)
        {
            CheckModelState();

            var filePath = Path.Combine(_appFolders.TempFileDownloadFolder, file.FileToken);
            var fileBytes = _tempFileCacheManager.GetFile(file.FileToken);
            if (fileBytes == null)
            {
                if (!System.IO.File.Exists(filePath))
                {
                    throw new UserFriendlyException(L("RequestedFileDoesNotExists"));
                }
                else
                {
                    fileBytes = System.IO.File.ReadAllBytes(filePath);

                    try
                    {
                        System.IO.File.Delete(filePath);
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }

            return File(fileBytes, file.FileType, file.FileName);
        }

        [AbpMvcAuthorize]
        [DisableAuditing]
        public ActionResult DownloadTempFileOnly(FileDto file)
        {
            CheckModelState();

            var filePath = Path.Combine(_appFolders.TempFileDownloadFolder, file.FileToken);
            if (!System.IO.File.Exists(filePath))
            {
                throw new UserFriendlyException(L("RequestedFileDoesNotExists"));
            }

            var fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, file.FileType, file.FileName);
        }
    }
}