using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Abp.Auditing;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.IO.Extensions;
using Abp.UI;
using Abp.Web.Models;
using Abp.Web.Mvc.Authorization;
using Abp.Web.Mvc.Models;
using DinePlan.DineConnect.Net.MimeTypes;
using DinePlan.DineConnect.Storage;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using System.IO;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.Web.Controllers
{
    [AbpMvcAuthorize]
    public class DocumentUploadController : DineConnectControllerBase
    {
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IRepository<Invoice> _invoiceRepo;
        private readonly IRepository<PurchaseOrder> _purchaseorderRepo;
        private readonly IRepository<Supplier> _supplierRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<SupplierDocument> _supplierdocumentRepo;
        private readonly IRepository<SalesOrder> _salesorderRepo;
        private readonly IRepository<House.Customer> _customerRepo;

        public DocumentUploadController( 
            IBinaryObjectManager binaryObjectManager,
            IRepository<Invoice> invoiceRepo,
            IRepository<PurchaseOrder> purchaseorderRepo,
            IRepository<Location> locationRepo, IRepository<SupplierDocument> supplierdocumentRepo,
        IRepository<Supplier> supplierRepo, IRepository<SalesOrder> salesorderRepo, IRepository<House.Customer> customerRepo)
        {
            _binaryObjectManager = binaryObjectManager;
            _invoiceRepo = invoiceRepo;
            _purchaseorderRepo = purchaseorderRepo;
            _supplierRepo = supplierRepo;
            _locationRepo = locationRepo;
            _supplierdocumentRepo = supplierdocumentRepo;
            _salesorderRepo = salesorderRepo;
            _customerRepo = customerRepo;
        }

     
        [UnitOfWork]
        public virtual async Task<JsonResult> ChangeDocumentInfo(DocumentDto input)
        {
            try
            {
                //Check input
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("Info_Change_Error"));
                }

                string LocRefId;
                string SupRefId;

                Invoice invoice = null;

                invoice = await _invoiceRepo.FirstOrDefaultAsync(t => t.LocationRefId == input.LocationRefId && t.SupplierRefId == input.SupplierRefId);

                if (invoice == null)
                {
                    throw new UserFriendlyException(L("FileNotFound"));
                }

                var supplier = await _supplierRepo.FirstOrDefaultAsync(t => t.Id == invoice.SupplierRefId);
                if (supplier == null)
                {
                    throw new UserFriendlyException(L("SupplierErr"));
                }

                SupRefId = supplier.SupplierName.ToUpper().ToString();

                var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == invoice.LocationRefId);
                if (location == null)
                {
                    throw new UserFriendlyException(L("LocationErr"));
                }

                LocRefId = location.Name.ToUpper().ToString();

                var file = Request.Files[0];
                string fileName = file.FileName;

                string subPath = "~/DocumentFiles/Invoice/" + invoice.InvoiceDate.ToString("dd-MMM-yyyy") + "_"+invoice.InvoiceNumber+ "_"+ SupRefId + "_"+ LocRefId ;
                bool exists = System.IO.Directory.Exists(Server.MapPath(subPath));

                if (!exists)
                    System.IO.Directory.CreateDirectory(Server.MapPath(subPath));

                file.SaveAs(Path.Combine(Server.MapPath(subPath), fileName));
                //Get user

                //Save new Info
                var storedFile = file.FileName;
                //await _binaryObjectManager.SaveAsync(storedFile);

                //Update new Info on the user
                //invoice.FileName = fileName;
                //invoice.FileExtenstionType = file.ContentType;

                string fileExtenstion = file.ContentType.Substring(file.ContentType.LastIndexOf("/") + 1).ToUpper();

                //Return success
                return Json(new MvcAjaxResponse());
              
            }
            catch (UserFriendlyException ex)
            {
                //Return error message
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [UnitOfWork]
        public async virtual Task<JsonResult> UploadPoDocumentInfo(SupplierDocumentDto input)
        {
            try
            {
                //Check input
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("Info_Change_Error"));
                }

                string SupRefId;

                SupplierDocument sd = null;

                sd = await _supplierdocumentRepo.FirstOrDefaultAsync(t => t.DocumentRefId == input.DocumentRefId && t.SupplierRefId == input.SupplierRefId);

                if (sd == null)
                {
                    throw new UserFriendlyException(L("FileNotFound"));
                }

                var supplier = await _supplierRepo.FirstOrDefaultAsync(t => t.Id == sd.SupplierRefId);
                if (supplier == null)
                {
                    throw new UserFriendlyException(L("SupplierErr"));
                }

                SupRefId = supplier.SupplierName.ToUpper().ToString();

                //var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == sd.DocumentRefId);
                //if (location == null)
                //{
                //    throw new UserFriendlyException(L("LocationErr"));
                //}

                //DocRefId = location.Name.ToUpper().ToString();

                var file = Request.Files[0];
                string fileName = file.FileName;

                string subPath = "~/DocumentFiles/";

                if (sd.DocumentRefId == 0)
                {
                     subPath = subPath+ "PO/" + SupRefId;
                }else if(sd.DocumentRefId == 1)
                {
                     subPath = subPath + "DC/" + SupRefId;
                }
                else
                {
                     subPath = subPath + "Invoice/" + SupRefId;
                }

                bool exists = System.IO.Directory.Exists(Server.MapPath(subPath));

                if (!exists)
                    System.IO.Directory.CreateDirectory(Server.MapPath(subPath));

                file.SaveAs(Path.Combine(Server.MapPath(subPath), fileName));
                //Get user

                //Save new Info
                var storedFile = file.FileName;
                //await _binaryObjectManager.SaveAsync(storedFile);

               // Update new Info on the user
                sd.FileName = fileName;
                //invoice.FileExtenstionType = file.ContentType;

                string fileExtenstion = file.ContentType.Substring(file.ContentType.LastIndexOf("/") + 1).ToUpper();

                //Return success
                return Json(new MvcAjaxResponse());

            }
            catch (UserFriendlyException ex)
            {
                //Return error message
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
        [UnitOfWork]
        public async virtual Task<JsonResult> UploadSoDocumentInfo(SupplierDocumentDto input)
        {
            try
            {
                //Check input
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("Info_Change_Error"));
                }

                string CusRefId;

                SupplierDocument sd = null;

                sd = await _supplierdocumentRepo.FirstOrDefaultAsync(t => t.DocumentRefId == input.DocumentRefId && t.SupplierRefId == input.SupplierRefId);

                if (sd == null)
                {
                    throw new UserFriendlyException(L("FileNotFound"));
                }

                var customer = await _customerRepo.FirstOrDefaultAsync(t => t.Id == sd.SupplierRefId);
                if (customer == null)
                {
                    throw new UserFriendlyException(L("CustomerErr"));
                }

                CusRefId = customer.CustomerName.ToUpper().ToString();

                //var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == sd.DocumentRefId);
                //if (location == null)
                //{
                //    throw new UserFriendlyException(L("LocationErr"));
                //}

                //DocRefId = location.Name.ToUpper().ToString();

                var file = Request.Files[0];
                string fileName = file.FileName;

                string subPath = "~/DocumentFiles/";

                if (sd.DocumentRefId == 0)
                {
                    subPath = subPath + "SO/" + CusRefId;
                }
                else if (sd.DocumentRefId == 1)
                {
                    subPath = subPath + "DO/" + CusRefId;
                }
                else
                {
                    subPath = subPath + "SalesInvoice/" + CusRefId;
                }

                bool exists = System.IO.Directory.Exists(Server.MapPath(subPath));

                if (!exists)
                    System.IO.Directory.CreateDirectory(Server.MapPath(subPath));

                file.SaveAs(Path.Combine(Server.MapPath(subPath), fileName));
                //Get user

                //Save new Info
                var storedFile = file.FileName;
                //await _binaryObjectManager.SaveAsync(storedFile);

                // Update new Info on the user
                sd.FileName = fileName;
                //invoice.FileExtenstionType = file.ContentType;

                string fileExtenstion = file.ContentType.Substring(file.ContentType.LastIndexOf("/") + 1).ToUpper();

                //Return success
                return Json(new MvcAjaxResponse());

            }
            catch (UserFriendlyException ex)
            {
                //Return error message
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
        public async Task<FileResult> DownloadPoUpload(DocumentDto input)
        {
            PurchaseOrder po = null;
            string DocRefId;
            string SupRefId;
            po = await _purchaseorderRepo.FirstOrDefaultAsync(t => t.LocationRefId == input.LocationRefId && t.SupplierRefId == input.SupplierRefId);

            if (po == null)
            {
                throw new UserFriendlyException(L("FileNotFound"));
            }

            var supplier = await _supplierRepo.FirstOrDefaultAsync(t => t.Id == po.SupplierRefId);
            if (supplier == null)
            {
                throw new UserFriendlyException(L("SupplierErr"));
            }

            SupRefId = supplier.SupplierName.ToUpper().ToString();

            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == po.LocationRefId);
            if (location == null)
            {
                throw new UserFriendlyException(L("LocationErr"));
            }

            DocRefId = location.Name.ToUpper().ToString();

            string subPath = "~/DocumentFiles/PO/" + po.PoDate.ToString("ddMMMyyyy") + po.PoReferenceCode.ToString().Replace("/", ""); 
            bool exists = System.IO.Directory.Exists(Server.MapPath(subPath));

            if (!exists)
            {
                throw new UserFriendlyException(po.PoDate.ToString("ddMMMyyyy") + po.PoReferenceCode.ToString().Replace("/", "")  + " " + L("FolderDoesNotExist"));
            }

            subPath = subPath + "/" + "Payment.pdf";

            return File(Server.MapPath(subPath), MimeTypeNames.ApplicationPdf);
        }
        public async Task<FileResult> DownloadSoUpload(DocumentDto input)
        {
            SalesOrder so = null;
            string DocRefId;
            string CusRefId;
            so = await _salesorderRepo.FirstOrDefaultAsync(t => t.LocationRefId == input.LocationRefId && t.CustomerRefId == input.CustomerRefId);

            if (so == null)
            {
                throw new UserFriendlyException(L("FileNotFound"));
            }

            var customer = await _customerRepo.FirstOrDefaultAsync(t => t.Id == so.CustomerRefId);
            if (customer == null)
            {
                throw new UserFriendlyException(L("CustomerErr"));
            }

            CusRefId = customer.CustomerName.ToUpper().ToString();

            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == so.LocationRefId);
            if (location == null)
            {
                throw new UserFriendlyException(L("LocationErr"));
            }

            DocRefId = location.Name.ToUpper().ToString();

            string subPath = "~/DocumentFiles/SO/" + so.SoDate.ToString("ddMMMyyyy") + so.SoReferenceCode.ToString().Replace("/", "");
            bool exists = System.IO.Directory.Exists(Server.MapPath(subPath));

            if (!exists)
            {
                throw new UserFriendlyException(so.SoDate.ToString("ddMMMyyyy") + so.SoReferenceCode.ToString().Replace("/", "") + " " + L("FolderDoesNotExist"));
            }

            subPath = subPath + "/" + "Payment.pdf";

            return File(Server.MapPath(subPath), MimeTypeNames.ApplicationPdf);
        }
    }
}