﻿using Abp.UI;
using Abp.Web.Models;
using DinePlan.DineConnect.Net.MimeTypes;
using DinePlan.DineConnect.Web.Controllers.FileManagerInput;
using DinePlan.DineConnect.Web.Controllers.Results;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using Abp.Extensions;

namespace DinePlan.DineConnect.Web.Controllers
{
    public class FileManagerController : DineConnectControllerBase
    {
        [System.Web.Http.HttpPost]
        [DontWrapResult]
        public JsonResult GetListUrl([FromBody] GetFileManagerInput input)
        {
            var path = "~/_Tickets/" + input.Path;
            DirectoryInfo di = new DirectoryInfo(Server.MapPath(path));
            DirectoryInfo[] directories = null;
            var list = new List<FileManager>();

            try
            {
                directories = di.GetDirectories("*");

                foreach (var dir in directories)
                {
                    list.Add(new FileManager
                    {
                        name = dir.Name,
                        fullPath = dir.FullName,
                        rights = "drwxr-xr-x",
                        size = GetDirectorySize(dir).ToString(),
                        dateStr = dir.CreationTime.ToString("dd-MM-yy HH:mm"),
                        date = dir.CreationTime,
                        type = "dir"
                    });
                }

                list.AddRange(GetFiles(di));
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine("There was an error with UnauthorizedAccessException");
            }

            var result = new FileManagerResult { result = list };

            return Json(result);
        }

        public long GetDirectorySize(DirectoryInfo dir)
        {
            FileInfo[] files = dir.GetFiles("*", SearchOption.AllDirectories);

            long size = 0;
            foreach (var file in files)
            {
                size += file.Length;
            }
            return size;
        }

        public List<FileManager> GetFiles(DirectoryInfo dir)
        {
            var files = dir.GetFiles("*");
            var list = files.Select(f => new FileManager
            {
                name = f.Name,
                fullPath = f.FullName,
                rights = "-rw-r--r--",
                size = f.Length.ToString(),
                dateStr = f.CreationTime.ToString("dd-MM-yy HH:mm"),
                date = f.CreationTime,
                type = "file"
            }).ToList();

            return list;
        }

        public ActionResult DownloadFileUrl([FromBody] GetFileManagerInput input)
        {
            var pathSplit = input.Path.Split(new[] { "/" }, StringSplitOptions.None);
            var fileName = pathSplit[pathSplit.Length - 1];

            var path = Server.MapPath("~/_Tickets/" + input.Path);

            var filePath = Path.Combine(path);
            if (!System.IO.File.Exists(filePath))
            {
                throw new UserFriendlyException(L("RequestedFileDoesNotExists"));
            }

            return File(filePath, MimeTypeNames.ApplicationOctetStream, fileName);
        }

        public ActionResult DownloadMultipleUrl([FromBody] DownloadMultipleUrlInput input)
        {
            //var pathSplit = input.Path.Split(new[] { "/" }, StringSplitOptions.None);
            //var fileName = pathSplit[pathSplit.Length - 1];

            //var path = Server.MapPath("~/Common" + input.Path);

            //var filePath = Path.Combine(path);
            //if (!System.IO.File.Exists(filePath))
            //{
            //    throw new UserFriendlyException(L("RequestedFileDoesNotExists"));
            //}

            //return File(filePath, MimeTypeNames.ApplicationOctetStream, fileName);

            return null;
        }
    }
}