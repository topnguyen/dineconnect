﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Localization;
using Abp.UI;
using Abp.Web.Models;
using Abp.Web.Mvc.Models;
using DinePlan.DineConnect.Connect;
using DinePlan.DineConnect.Connect.Card;
using DinePlan.DineConnect.Connect.ConnectCard.Card;
using DinePlan.DineConnect.Connect.ConnectCard.Card.Dtos;
using DinePlan.DineConnect.Connect.Custom;
using DinePlan.DineConnect.Connect.Custom.Dto;
using DinePlan.DineConnect.Connect.DayClose;
using DinePlan.DineConnect.Connect.FutureData;
using DinePlan.DineConnect.Connect.FutureData.Dtos;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Table;
using DinePlan.DineConnect.Connect.Table.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Gift;
using DinePlan.DineConnect.Engage.Gift.DinePlan.DineConnect.Engage;
using DinePlan.DineConnect.Engage.Gift.Dtos;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Transaction;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.Hr.Master;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Localization;
using DinePlan.DineConnect.Localization.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using DinePlan.DineConnect.Swipe;
using DinePlan.DineConnect.Swipe.Master;
using DinePlan.DineConnect.Swipe.Master.Dtos;
using DinePlan.DineConnect.Utilities;
using DinePlan.DineConnect.Utility.Exporter;
using DinePlan.DineConnect.Web.Utility;
using iTextSharp.text;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using DinePlan.DineConnect.Hr.Master.Implementation;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.MultiTenancy.Dto;
using Abp.BackgroundJobs;
using System.Collections.ObjectModel;
using DinePlan.DineConnect.Helper;
using static DinePlan.DineConnect.Connect.OrderTag.Dtos.GetOrderExchangeReportInput;
using DinePlan.DineConnect.Connect.OrderTags;
using DinePlan.DineConnect.Connect.OrderTag;
using DinePlan.DineConnect.Connect.OrderTag.Dtos;

namespace DinePlan.DineConnect.Web.Controllers
{
    public class ImportController : DineConnectControllerBase
    {
        private readonly IAdjustmentAppService _adjustmentAppService;
        private readonly ICategoryAppService _categoryAppService;
        private readonly IRepository<Company> _companyRepo;
        private readonly IConnectTableGroupAppService _conTaAppService;
        private readonly IDayCloseAppService _dayCloseAppService;
        private readonly IHouseReportAppService _housereportAppService;
        private readonly ILocationAppService _locationAppService;
        private readonly IRepository<Location> _locationRepo;
        private readonly IMaterialAppService _materialAppService;
        private readonly IMaterialGroupCategoryAppService _materialgroupcategoryAppService;
        private readonly IMaterialLocationWiseStockAppService _materiallocationwisestockAppService;
        private readonly IRepository<MaterialLocationWiseStock> _materiallocationwisestockRepo;
        private readonly IRepository<MaterialGroupCategory> _materialGroupCategoryRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<MaterialUnitsLink> _materialUnitLinkRepo;
        private readonly IMenuItemAppService _menuItemAppService;
        private readonly IPriceTagAppService _priceTagService;
        private readonly IScreenMenuAppService _screenMenuAppService;
        private readonly ISupplierAppService _supplierAppService;
        private readonly IRepository<SupplierMaterial> _supplierMaterial;
        private readonly ISupplierMaterialAppService _supplierMaterialAppService;
        private readonly IRepository<Supplier> _supplierRepo;
        private readonly ISwipeCardAppService _swipecardAppService;
        private readonly IRepository<SwipeCardType> _swipecardTypeRepo;
        private readonly IUnitConversionAppService _unitConversionAppService;
        private readonly IRepository<UnitConversion> _unitConversionRepo;
        private readonly IRepository<Unit> _unitRepo;
        private string _currentMaterialStatus;
        private string _currentProcess;
        private string processString;
        private readonly IRepository<PersonalInformation> _personalinformationRepo;
        private readonly IRepository<SalaryInfo> _salaryinfoRepo;
        private readonly IPersonalInformationAppService _personalAppSerivce;
        private readonly IRepository<SkillSet> _skillsetRepo;
        private readonly IAttendanceLogAppService _attendancelogAppService;
        private readonly IRepository<AttendanceLog> _attendancelogRepo;
        private readonly IRepository<EmployeeSkillSet> _employeeSkillSet;
        private readonly IRepository<AttendanceMachineLocation> _attendanceMachineLocationRepo;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IRepository<MenuItemDescription> _menuItemDescriptionRepository;
        private readonly IExcelExporter _excelExporter;
        private readonly IRepository<MenuItemPortion> _menuItemPortionRepo;
        private readonly IRepository<MaterialMenuMapping> _materialMenuMappingRepo;
        private readonly IMaterialMenuMappingAppService _materialMenuMappingAppService;
        private readonly IRepository<Department> _departmentRepo;
        private readonly IRepository<ConnectCardType> _connectCardTypeRepository;
        private readonly IConnectCardAppService _connectCardAppService;
        private readonly IFutureDataAppService _futureDataAppService;
        private readonly IFullTaxAppService _fullTaxAppService;
        private readonly ILanguageAppService _languageAppService;
        private readonly ILocalizationManager _localizationManager;
        private readonly IRepository<JobTitleMaster> _jobTitleMasterRepo;
        private readonly IRepository<Connect.Card.ConnectCard> _connectCardRepository;
        private readonly IRepository<GiftVoucherType> _giftvouchertypeRepo;
        private readonly IGiftVoucherTypeAppService _giftVoucherTypeAppService;
        private readonly IRepository<ApplicationLanguageText, long> _languageTextRepository;
        private readonly IRepository<ScreenMenuCategory> _screenCatRepo;
        private readonly IRepository<ScreenMenuItem> _screenItemRepo;
        private readonly IRepository<ScreenMenu> _screenMenu;
        private readonly IScreenMenuManager _screenmenuManager;

        private readonly IRepository<OrderTagGroup> _orderTagGroupRepo;
        private readonly IRepository<OrderTagLocationPrice> _ordertagLocationPriceRepo;
        private readonly IRepository<OrderTagLocationPrice> _tagLocationPrice;
        private readonly IRepository<OrderTagMap> _orderMapManager;
        private readonly IRepository<Connect.OrderTags.OrderTag> _orderTagManager;
        private readonly IOrderTagGroupManager _ordertaggroupManager;
        private readonly IRepository<Category> _cateRepo;
        private readonly IOrderTagGroupAppService _orderTagGroupAppService;

        private const int MaxConnectCardSize = 2097152; //2MB
        public TenantManager TenantManager { get; set; }
        public ImportController(
            IRepository<ScreenMenuItem> sm,
            IRepository<ScreenMenuCategory> smc,
             IRepository<ScreenMenu> screenMenuRepo,
            IRepository<GiftVoucherType> giftvouchertypeRepo,
            IGiftVoucherTypeAppService giftVoucherTypeAppService,
            ILocationAppService locationAppService,
            IRepository<Company> compRepo,
            IMenuItemAppService menuItemAppService,
            IMaterialGroupCategoryAppService materialgroupcategoryAppService,
            IMaterialAppService materialAppService,
            ICategoryAppService categoryAppService,
            IScreenMenuAppService screenMenuAppService,
            ISupplierAppService supplierAppService,
            ISupplierMaterialAppService supplierMaterialAppService,
            IHouseReportAppService housereportAppService,
            IRepository<Location> locationRepo,
            IRepository<Material> materialRepo,
            IAdjustmentAppService adjustmentAppService,
            IRepository<MaterialLocationWiseStock> materiallocationwisestockRepo,
            IMaterialLocationWiseStockAppService materiallocationwisestockAppService,
            IPriceTagAppService priceService,
            IRepository<Unit> unitRepo,
            IRepository<UnitConversion> unitConversionRepo, IDayCloseAppService dayCloseAppService,
            IRepository<SwipeCardType> swipecardTypeRepo, IConnectTableGroupAppService conTaAppService,
            ISwipeCardAppService swipecardAppService,
            IRepository<Supplier> supplierRepo,
            IRepository<SupplierMaterial> supplierMaterial,
            IUnitConversionAppService unitConversionAppService,
            IRepository<PersonalInformation> personalinformationRepo,
            IRepository<SalaryInfo> salaryinfoRepo,
            IPersonalInformationAppService personalAppSerivce,
            IRepository<SkillSet> skillsetRepo,
            IAttendanceLogAppService attendancelogAppService,
            IRepository<AttendanceLog> attendancelogRepo,
            IRepository<EmployeeSkillSet> employeeSkillSet,
            IRepository<AttendanceMachineLocation> attendanceMachineLocationRepo,
            IRepository<MenuItem> menuItemRepository,
            IRepository<MenuItemDescription> menuItemDescriptionRepository,
            IExcelExporter excelExporter,
            IRepository<MenuItemPortion> menuItemPortionRepo,
            IRepository<MaterialUnitsLink> materialUnitLinkRepo,
            IRepository<MaterialMenuMapping> materialMenuMappingRepo,
            IMaterialMenuMappingAppService materialMenuMappingAppService,
            IRepository<Department> departmentRepo,
            IRepository<ConnectCardType> connectCardTypeRepository,
            IConnectCardAppService connectCardAppService,
            IRepository<MaterialGroupCategory> materialGroupCategoryRepo,
            IFutureDataAppService futureDataAppService,
            IFullTaxAppService fullTaxAppService,
            ILanguageAppService languageAppService,
            ILocalizationManager localizationManager,
            IRepository<JobTitleMaster> jobTitleMasterRepo,
            IRepository<Connect.Card.ConnectCard> connectCardRepository,
            IRepository<ApplicationLanguageText, long> languageTextRepository,
            IRepository<OrderTagGroup> orderTagGroupRepo,
            IRepository<OrderTagLocationPrice> ordertagLocationPriceRepo,
            IRepository<OrderTagLocationPrice> tagLocationPrice,
            IRepository<OrderTagMap> orderMapManager,
            IRepository<Connect.OrderTags.OrderTag> orderTagManager,
            IOrderTagGroupManager ordertaggroupManager,
            IRepository<Category> cateRepo,
            IOrderTagGroupAppService orderTagGroupAppService
            )
        {
            _screenCatRepo = smc;
            _screenItemRepo = sm;
            _screenMenu = screenMenuRepo;

            _giftvouchertypeRepo = giftvouchertypeRepo;
            _giftVoucherTypeAppService = giftVoucherTypeAppService;
            _attendanceMachineLocationRepo = attendanceMachineLocationRepo;
            _menuItemRepository = menuItemRepository;
            _menuItemDescriptionRepository = menuItemDescriptionRepository;
            _excelExporter = excelExporter;
            _personalinformationRepo = personalinformationRepo;
            _salaryinfoRepo = salaryinfoRepo;
            _personalAppSerivce = personalAppSerivce;
            _skillsetRepo = skillsetRepo;
            _attendancelogAppService = attendancelogAppService;
            _attendancelogRepo = attendancelogRepo;
            _employeeSkillSet = employeeSkillSet;
            _supplierMaterialAppService = supplierMaterialAppService;
            _supplierRepo = supplierRepo;
            _priceTagService = priceService;
            _companyRepo = compRepo;
            _locationAppService = locationAppService;
            _menuItemAppService = menuItemAppService;
            _screenMenuAppService = screenMenuAppService;
            _categoryAppService = categoryAppService;
            _conTaAppService = conTaAppService;
            _materialgroupcategoryAppService = materialgroupcategoryAppService;
            _materialAppService = materialAppService;
            _unitRepo = unitRepo;
            _supplierAppService = supplierAppService;
            _housereportAppService = housereportAppService;
            _dayCloseAppService = dayCloseAppService;
            _locationRepo = locationRepo;
            _materialRepo = materialRepo;
            _materiallocationwisestockRepo = materiallocationwisestockRepo;
            _materiallocationwisestockAppService = materiallocationwisestockAppService;
            _adjustmentAppService = adjustmentAppService;
            _unitConversionRepo = unitConversionRepo;
            _swipecardTypeRepo = swipecardTypeRepo;
            _swipecardAppService = swipecardAppService;
            _supplierMaterial = supplierMaterial;
            _unitConversionAppService = unitConversionAppService;
            _menuItemPortionRepo = menuItemPortionRepo;
            _materialUnitLinkRepo = materialUnitLinkRepo;
            _materialMenuMappingRepo = materialMenuMappingRepo;
            _materialMenuMappingAppService = materialMenuMappingAppService;
            _departmentRepo = departmentRepo;
            _connectCardTypeRepository = connectCardTypeRepository;
            _connectCardAppService = connectCardAppService;
            _materialGroupCategoryRepo = materialGroupCategoryRepo;
            _futureDataAppService = futureDataAppService;
            _fullTaxAppService = fullTaxAppService;
            _languageAppService = languageAppService;
            _localizationManager = localizationManager;
            _jobTitleMasterRepo = jobTitleMasterRepo;
            _connectCardRepository = connectCardRepository;
            _languageTextRepository = languageTextRepository;
            _orderTagGroupRepo = orderTagGroupRepo;
            _ordertagLocationPriceRepo = ordertagLocationPriceRepo;
            _tagLocationPrice = tagLocationPrice;
            _orderMapManager = orderMapManager;
            _orderTagManager = orderTagManager;
            _ordertaggroupManager = ordertaggroupManager;
            _cateRepo = cateRepo;
            _orderTagGroupAppService = orderTagGroupAppService;
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportLocation()
        {
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();
                var rowCount = 1;
                var dtos = new List<LocationEditDto>();
                foreach (var row in allList)
                {
                    if (row["OrganizationCode"].ToString().IsNullOrEmpty() || row["Code"].ToString().IsNullOrEmpty() || row["Name"].ToString().IsNullOrEmpty())
                    {
                        throw new UserFriendlyException(L("ImportLocationError") + " Row: " + rowCount);
                    }
                    var cCode = row["OrganizationCode"].ToString();
                    var company = _companyRepo.FirstOrDefault(a => a.Code.Equals(cCode));
                    if (company == null)
                    {
                        throw new UserFriendlyException(L("CompanyError", cCode) + " Row: " + rowCount);
                    }
                    var dto = new LocationEditDto
                    {
                        Code = row["Code"].ToString(),
                        CompanyCode = row["OrganizationCode"].ToString(),
                        Name = row["Name"].ToString(),
                        Address1 = row["Address1"].ToString(),
                        Address2 = row["Address2"].ToString(),
                        Address3 = row["Address3"].ToString(),
                        City = row["City"].ToString(),
                        State = row["State"].ToString(),
                        Country = row["Country"].ToString(),
                        PhoneNumber = row["PhoneNumber"].ToString(),
                        Fax = row["Fax"].ToString(),
                        Website = row["Website"].ToString(),
                        Email = row["Email"].ToString(),
                        CompanyRefId = company.Id
                    };
                    dtos.Add(dto);
                    rowCount++;
                }
                await _locationAppService.CreatLocations(dtos);

                return Json(new MvcAjaxResponse());
            }
            catch (UserFriendlyException ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportGiftVoucher(int id)
        {
            try
            {
                var voucherStart = new GiftVoucherStatsDto();

                var allVoucherStart = await _giftVoucherTypeAppService.GetVouchers(new GetGiftVoucherInput() { VoucherType = 0 });

                if (id != 0)
                    voucherStart = await _giftVoucherTypeAppService.GetVouchers(new GetGiftVoucherInput() { VoucherType = id });
                else
                {
                    voucherStart.ValidGiftVouchers = new PagedResultOutput<GiftVoucherListDto>();
                    voucherStart.DashBoardDto = new DashboardGiftVoucher();
                }
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();
                var rowCount = 1;

                voucherStart.DashBoardDto = new DashboardGiftVoucher()
                {
                    TotalVoucherCount = voucherStart.DashBoardDto.TotalVoucherCount + allList.Count()
                };

                var validGiftVouchers = voucherStart.ValidGiftVouchers.Items.ToList();

                var allValidGiftVouchers = allVoucherStart.ValidGiftVouchers.Items.ToList();

                var validGiftVoucherNames = new List<string>();

                foreach (var item in allValidGiftVouchers)
                {
                    validGiftVoucherNames.Add(item.Voucher);
                }
                var invalidGiftVouchers = new List<GiftVoucherListDto>();
                var exceedLimitationGiftVouchers = new List<GiftVoucherListDto>();

                foreach (var row in allList)
                {
                    if (row["VoucherNumber"].ToString().IsNullOrEmpty())
                    {
                        throw new UserFriendlyException(L("ImportGiftVoucherError") + " Row: " + rowCount);
                    }
                    var voucherNumber = row["VoucherNumber"].ToString();

                    var giftVoucher = new GiftVoucherListDto
                    {
                        Voucher = voucherNumber,
                    };

                    if (giftVoucher.Id != 0)
                        continue;

                    if (giftVoucher.Voucher.Length > 40)
                    {
                        exceedLimitationGiftVouchers.Add(giftVoucher);
                    }
                    else
                    {
                        if (!validGiftVoucherNames.Contains(giftVoucher.Voucher))
                        {
                            validGiftVouchers.Add(giftVoucher);
                            validGiftVoucherNames.Add(giftVoucher.Voucher);
                        }
                        else
                        {
                            invalidGiftVouchers.Add(giftVoucher);
                        }
                    }

                    rowCount++;
                }

                voucherStart.ValidGiftVouchers = new PagedResultOutput<GiftVoucherListDto>()
                {
                    Items = validGiftVouchers,
                    TotalCount = validGiftVouchers.Count
                };

                voucherStart.InvalidGiftVouchers = new PagedResultOutput<GiftVoucherListDto>()
                {
                    Items = invalidGiftVouchers,
                    TotalCount = invalidGiftVouchers.Count
                };

                voucherStart.ExceedLimitationGiftVouchers = new PagedResultOutput<GiftVoucherListDto>()
                {
                    Items = exceedLimitationGiftVouchers,
                    TotalCount = exceedLimitationGiftVouchers.Count
                };

                if (id != 0)
                {
                    var giftVoucherType = await _giftVoucherTypeAppService.GetGiftVoucherTypeForEdit(new NullableIdInput() { Id = id });
                    if (voucherStart.InvalidGiftVouchers.TotalCount
                            + voucherStart.ExceedLimitationGiftVouchers.TotalCount
                            + voucherStart.ValidGiftVouchers.TotalCount > giftVoucherType.GiftVoucherType.VoucherCount)
                    {
                        return Json(new MvcAjaxResponse
                        {
                            Success = false,
                            Error = new ErrorInfo()
                            {
                                Message = L("NumberOfRowsInImportFileInvalid")
                            }
                        });
                    }
                }

                return Json(new MvcAjaxResponse
                {
                    Success = true,
                    Result = voucherStart
                });
            }
            catch (UserFriendlyException ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportPrices(int locationId)
        {
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];
                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();
                var allItems = new List<LocationPriceInput>();

                foreach (var row in allList)
                {
                    var valueM = row["Id"] != null ? Convert.ToInt32(row["Id"].ToString()) : 0;
                    if (valueM != 0)
                    {
                        var locationPrice = L("LocationPrice");
                        var price = row[locationPrice] != null
                            ? Convert.ToDecimal(row[locationPrice].ToString())
                            : 0M;
                        allItems.Add(new LocationPriceInput
                        {
                            PortionId = valueM,
                            Price = price,
                            LocationId = locationId
                        });
                    }
                }
                await _menuItemAppService.UpdateLocationPrice(allItems);
                return Json(new MvcAjaxResponse());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportItems(int locationId)
        {
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];
                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();
                var allItems = new List<int>();
                StringBuilder builder = new StringBuilder();
                foreach (var row in allList)
                {
                    var valueM = row[0].ToString().Length > 0 ? Convert.ToInt32(row[0].ToString()) : 0;
                    if (valueM != 0)
                    {
                        allItems.Add(valueM);
                    }
                    else
                    {
                        var rowName = row[2].ToString().Length > 0 ? Convert.ToString(row[2]) : "";
                        if (!string.IsNullOrEmpty(rowName))
                        {
                            var item = await _menuItemAppService.GetMenuItemById(rowName);
                            if (item != null)
                            {
                                allItems.Add(Convert.ToInt32(item.Value));
                                continue;
                            }
                            else
                            {
                                builder.Append(rowName);
                                builder.Append(" , ");
                            }
                        }
                    }
                }

                await _menuItemAppService.CreateMenuItemToLocation(new MenuItemLocationInput
                {
                    LocationId = locationId,
                    MenuItems = allItems,
                    MenuItemId = 0
                });

                if (builder.Length > 0)
                {
                    throw new UserFriendlyException(L("MissingItmes") + "  " + builder.ToString());
                }
                return Json(new MvcAjaxResponse());
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        public async Task<FileResult> ImportLocationTemplate()
        {
            return File(Server.MapPath("~/Common/Import/Locations.xlsx"),
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }

        public async Task<FileResult> ImportGiftVoucherTemplate()
        {

            return File(Server.MapPath("~/Common/Import/GiftVoucherTypeTemplate.xlsx"),
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }

        public async Task<FileResult> ImportCardTemplate()
        {
            return File(Server.MapPath("~/Common/Import/CardImportTemplate.xlsx"),
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportMenuItem()
        {
            int rowP = 0;

            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();

                var dtos = new List<MenuItemEditDto>();

                foreach (var row in allList)
                {
                    var menu = new MenuItemEditDto
                    {
                        CategoryName = row["Category"]?.ToString().Trim() ?? "",
                        AliasCode = row["AliasCode"]?.ToString().Trim() ?? "",
                        Name = row["MenuName"]?.ToString().Trim() ?? "",
                        AliasName = row["AliasName"]?.ToString().Trim() ?? "",
                        TPName = row["Portion"]?.ToString() ?? "Normal",
                        TPPrice = row["Price"] != null ? Convert.ToDecimal(row["Price"].ToString().Trim()) : 0M,
                        BarCode = row["Barcode"]?.ToString().Trim() ?? "",
                        ItemDescription = row["Description"]?.ToString().Trim() ?? "",
                        ForceQuantity = false,
                        ForceChangePrice = false,
                        RestrictPromotion = false,
                        Tag = row["Tag"]?.ToString().Trim() ?? "",
                        NoTax = false,
                        GroupName = row["Group"]?.ToString().Trim() ?? "",
                        GroupCode = row["GroupCode"]?.ToString().Trim() ?? "",
                        CategoryCode = row["CategoryCode"]?.ToString().Trim() ?? "",
                        TPNumberOfPax = !string.IsNullOrEmpty(row["NumberOfPax"].ToString()) ? Convert.ToInt32(row["NumberOfPax"].ToString().Trim()) : 0,
                        RowIndex = rowP + 2
                    };

                    if (string.IsNullOrEmpty(menu.CategoryName))
                    {
                        continue;
                    }
                    if (string.IsNullOrEmpty(menu.Name))
                    {
                        continue;
                    }

                    if (!string.IsNullOrEmpty(row["Id"].ToString().Trim()))
                    {
                        var myId = Convert.ToInt32(row["Id"].ToString());
                        menu.Id = myId;
                    }

                    if (!string.IsNullOrEmpty(row["ForceQuantity"].ToString().Trim()) &&
                        row["ForceQuantity"].ToString().Trim().Equals("1"))
                    {
                        menu.ForceQuantity = true;
                    }

                    if (!string.IsNullOrEmpty(row["ForceChangePrice"].ToString()) &&
                        row["ForceChangePrice"].ToString().Trim().Equals("1"))
                    {
                        menu.ForceChangePrice = true;
                    }
                    if (!string.IsNullOrEmpty(row["RestrictPromotion"].ToString().Trim()) &&
                       row["RestrictPromotion"].ToString().Trim().Equals("1"))
                    {
                        menu.RestrictPromotion = true;
                    }
                    if (!string.IsNullOrEmpty(row["NoTax"].ToString().Trim()) &&
                                          row["NoTax"].ToString().Trim().Equals("1"))
                    {
                        menu.NoTax = true;
                    }
                    rowP++;
                    dtos.Add(menu);
                }

                var anyDuplicate = dtos.GroupBy(x => new { x.Name, x.TPName }).Where(g => g.Count() > 1);
                if (anyDuplicate.Any())
                {
                    var builder = new StringBuilder();
                    builder.Append(string.Format(L("DuplicateExistsInFile_f"), L("Name")));
                    foreach (var allD in anyDuplicate)
                    {
                        builder.Append(allD.Key.Name);
                        builder.Append(",");
                    }
                    throw new UserFriendlyException(builder.ToString());
                }
                var alaisDuplicates = dtos.Where(a => !string.IsNullOrEmpty(a.AliasCode)).GroupBy(x => new { x.AliasCode, x.TPName }).Where(g => g.Count() > 1);
                if (alaisDuplicates.Any())
                {
                    var builder = new StringBuilder();
                    builder.Append(string.Format(L("DuplicateExistsInFile_f"), L("AliasCode")));
                    foreach (var allD in alaisDuplicates)
                    {
                        builder.Append(allD.Key + ",");
                    }
                    throw new UserFriendlyException(builder.ToString());
                }
                foreach (var group in dtos.GroupBy(a => new { a.CategoryName, a.GroupName, a.CategoryCode, a.GroupCode }))
                {
                    var cateInput = new CategoryEditDto
                    {
                        Name = group.Key.CategoryName,
                        GroupName = group.Key.GroupName,
                        Code = group.Key.CategoryCode,
                        GroupCode = group.Key.GroupCode
                    };

                    rowP = group.OrderBy(g => g.RowIndex).FirstOrDefault().RowIndex;
                    var cate = await _categoryAppService.GetOrCreateCategory(cateInput);

                    CreateOrUpdateMenuItemInput input = null;

                    foreach (var myItem in group.OrderBy(a => a.Name))
                    {
                        if (input == null)
                        {
                            var meD = new MenuItemEditDto
                            {
                                AliasCode = myItem.AliasCode,
                                AliasName = myItem.AliasName,
                                BarCode = myItem.BarCode,
                                CategoryId = cate.Id,
                                ForceChangePrice = myItem.ForceChangePrice,
                                ForceQuantity = myItem.ForceQuantity,
                                RestrictPromotion = myItem.RestrictPromotion,
                                NoTax = myItem.NoTax,
                                ItemDescription = myItem.ItemDescription,
                                Name = myItem.Name,
                                Tag = myItem.Tag,
                                Id = myItem.Id
                            };

                            meD.Portions.Add(new MenuItemPortionEditDto
                            {
                                Name = myItem.TPName,
                                MultiPlier = 1,
                                Price = myItem.TPPrice,
                                NumberOfPax = myItem.TPNumberOfPax
                            });

                            input = new CreateOrUpdateMenuItemInput
                            {
                                MenuItem = meD,
                                NotOverride = true
                            };
                        }
                        else if (myItem.Name.Equals(input.MenuItem.Name))
                        {
                            input.MenuItem.Portions.Add(new MenuItemPortionEditDto
                            {
                                Name = myItem.TPName,
                                MultiPlier = 1,
                                Price = myItem.TPPrice,
                                NumberOfPax = myItem.TPNumberOfPax
                            });
                        }
                        else
                        {
                            if (input.MenuItem.Id > 0)
                                input.UpdateItem = true;

                            await _menuItemAppService.CreateOrUpdateMenuItem(input);

                            var newMenuItem = new MenuItemEditDto
                            {
                                AliasCode = myItem.AliasCode,
                                AliasName = myItem.AliasName,
                                BarCode = myItem.BarCode,
                                CategoryId = cate.Id,
                                ForceChangePrice = myItem.ForceChangePrice,
                                ForceQuantity = myItem.ForceQuantity,
                                RestrictPromotion = myItem.RestrictPromotion,
                                NoTax = myItem.NoTax,
                                ItemDescription = myItem.ItemDescription,
                                Name = myItem.Name,
                                Tag = myItem.Tag,
                                Id = myItem.Id
                            };
                            newMenuItem.Portions.Add(new MenuItemPortionEditDto
                            {
                                Name = myItem.TPName,
                                MultiPlier = 1,
                                Price = myItem.TPPrice,
                                NumberOfPax = myItem.TPNumberOfPax
                            });
                            input = new CreateOrUpdateMenuItemInput
                            {
                                MenuItem = newMenuItem,
                                NotOverride = true
                            };
                        }
                    }

                    if (input != null)
                    {
                        if (input.MenuItem.Id > 0)
                            input.UpdateItem = true;

                        await _menuItemAppService.CreateOrUpdateMenuItem(input);
                    }
                }

                return Json(new MvcAjaxResponse());
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo($"{ex.Message} {L("Row")} {rowP}")));
            }
        }

        public async Task<FileResult> ImportMenuItemTemplate()
        {
            return File(Server.MapPath("~/Common/Import/MenuItemTemplate.xlsx"),
               MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportMenuItemDescription()
        {
            int rowP = 0;

            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];
                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();

                var dtos = new List<MenuItemDescriptionEditDto>();

                var languages = _localizationManager.GetAllLanguages();
                foreach (var row in allList)
                {
                    var menu = new MenuItemDescriptionEditDto
                    {
                        MenuItemId = row[0] != null ? Convert.ToInt32(row[0].ToString().Trim()) : 0,
                        LangugeName = row[2]?.ToString().Trim() ?? "",
                        ProductName = row[3]?.ToString().Trim() ?? "",
                        Description = row[4]?.ToString().Trim() ?? ""
                    };
                    menu.LanguageCode = languages.FirstOrDefault(l => l.DisplayName == menu.LangugeName)?.Name;

                    if (!menu.ProductName.IsNullOrEmpty())
                        dtos.Add(menu);
                }
                var anyDuplicate = dtos.GroupBy(x => new { x.LangugeName, x.LanguageCode, x.MenuItemId }).Where(g => g.Count() > 1);
                if (anyDuplicate.Any())
                {
                    var builder = new StringBuilder();
                    builder.Append(string.Format(L("DuplicateExistsInFile_f"), L("Language")));
                    foreach (var allD in anyDuplicate)
                    {
                        builder.Append(allD.Key.LangugeName);
                        builder.Append(",");
                    }
                    throw new UserFriendlyException(builder.ToString());
                }

                foreach (var des in dtos)
                {
                    var isExistsMenuItem = _menuItemRepository.GetAll().Select(m => m.Id).Contains(des.MenuItemId);

                    if (!isExistsMenuItem)
                    {
                        throw new UserFriendlyException(L("NotExistsMenuItem"));
                    }

                    var existsDescription = _menuItemDescriptionRepository.GetAll().Where(m => m.MenuItemId == des.MenuItemId && m.LanguageCode == des.LanguageCode);
                    if (existsDescription.Any())
                    {
                        var exist = existsDescription.FirstOrDefault();
                        des.MapTo(exist);
                        await _menuItemDescriptionRepository.UpdateAsync(exist);
                    }
                    else

                        await _menuItemDescriptionRepository.InsertAsync(des.MapTo<MenuItemDescription>());
                    rowP++;
                }

                return Json(new MvcAjaxResponse());
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message + " Row " + rowP)));
            }
        }

        public FileResult ImportMenuItemDescriptionTemplate()
        {
            var subPath = "~/Common/Import/MenuItemDescriptions.xlsx";
            bool exists = System.IO.Directory.Exists(Server.MapPath(subPath));

            if (!exists)
            {
                var file = new FileDto("MenuItemDescriptions.xlsx",
                        MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
                using (var package = new ExcelPackage())
                {
                    var worksheet = package.Workbook.Worksheets.Add("Template");
                    var row = 1;
                    var col = 1;

                    var columnNames = new[] { "MenuItemId", "Language", "Name", "Description" };
                    foreach (var cn in columnNames)
                    {
                        worksheet.Cells[row, col].Value = cn;
                        worksheet.Column(col).AutoFit();
                        worksheet.Cells[1, col].Style.Font.Bold = true;
                        col++;
                    }

                    var filePath = Path.Combine(Server.MapPath("~/Common/Import"), file.FileName);
                    package.SaveAs(new FileInfo(filePath));
                }
            }

            return File(Server.MapPath(subPath), MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportScreenMenu()
        {
            var rowCount = 1;

            try
            {
                var locations = new Dictionary<string, LocationListDto>();

                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();
                var dtos = new List<MenuitemImportDto>();
                foreach (var row in allList)
                {
                    if (row["Location Name"].ToString().IsNullOrEmpty() && row["MenuName"].ToString().IsNullOrEmpty())
                    {
                        continue;
                    }
                    var menu = new MenuitemImportDto
                    {
                        Location = row["Location Name"]?.ToString().Trim() ?? "",
                        CategoryName = row["Category"]?.ToString().Trim() ?? "",
                        AliasCode = row["AliasCode"]?.ToString().Trim() ?? "",
                        Name = row["MenuName"]?.ToString().Trim() ?? "",
                        AliasName = row["AliasName"]?.ToString().Trim() ?? "",
                        TPName = row["Portion"]?.ToString().Trim() ?? "",
                        TPPrice = row["Price"] != null ? Convert.ToDecimal(row["Price"].ToString().Trim()) : 0M,
                        BarCode = row["Barcode"]?.ToString().Trim() ?? "",
                        ItemDescription = row["Description"]?.ToString().Trim() ?? "",
                        ForceQuantity = false,
                        ForceChangePrice = false,
                        RestrictPromotion = false,
                        NoTax = false,
                        Tag = row["Tag"]?.ToString().Trim() ?? "",
                        RowIndex = rowCount
                    };
                    dtos.Add(menu);
                    rowCount++;
                }

                foreach (var loc in dtos.GroupBy(a => a.Location))
                {
                    var lId = await _locationAppService.GetLocationByName(loc.Key);
                    if (lId == null || lId.Id <= 0)
                    {
                        throw new UserFriendlyException(L("LocationErr", loc.Key) + " Row: " + loc.FirstOrDefault()?.RowIndex);
                    }
                    locations.Add(loc.Key, lId);
                }

                foreach (var group in dtos.GroupBy(a => a.CategoryName))
                {
                    var category = await _categoryAppService.GetOrCreateByName(group.Key);
                    CreateOrUpdateMenuItemInput menuItemInput = null;
                    foreach (var menuItem in group.OrderBy(a => a.Name))
                    {
                        var myLocation = locations[menuItem.Location];
                        var allLocations = new List<SimpleLocationDto>
                        {
                            new SimpleLocationDto
                            {
                                Id = myLocation.Id,
                                Name = myLocation.Name,
                                Code = myLocation.Code
                            }
                        };
                        var existMenuItem = _menuItemRepository.GetAll().FirstOrDefault(a => a.Name.ToUpper() == menuItem.Name.ToUpper());
                        if (existMenuItem == null)
                        {
                            if (menuItemInput == null)
                            {
                                var meD = new MenuItemEditDto
                                {
                                    AliasCode = menuItem.AliasCode,
                                    AliasName = menuItem.AliasName,
                                    BarCode = menuItem.BarCode,
                                    CategoryId = category.Id,
                                    ForceChangePrice = menuItem.ForceChangePrice,
                                    ForceQuantity = menuItem.ForceQuantity,
                                    RestrictPromotion = menuItem.RestrictPromotion,
                                    NoTax = menuItem.NoTax,
                                    ItemDescription = menuItem.ItemDescription,
                                    Name = menuItem.Name,
                                    Locations = JsonConvert.SerializeObject(allLocations),
                                    Tag = menuItem.Tag
                                };

                                meD.Portions.Add(new MenuItemPortionEditDto
                                {
                                    Name = menuItem.TPName,
                                    MultiPlier = 1,
                                    Price = menuItem.TPPrice
                                });

                                menuItemInput = new CreateOrUpdateMenuItemInput
                                {
                                    MenuItem = meD,
                                    LocationGroup = { Locations = allLocations }
                                };
                            }
                            else if (menuItem.Name.Equals(menuItemInput.MenuItem.Name))
                            {
                                menuItemInput.MenuItem.Portions.Add(new MenuItemPortionEditDto
                                {
                                    Name = menuItem.TPName,
                                    MultiPlier = 1,
                                    Price = menuItem.TPPrice
                                });
                            }
                            else
                            {
                                await _menuItemAppService.CreateOrUpdateMenuItem(menuItemInput);
                                var meD = new MenuItemEditDto
                                {
                                    AliasCode = menuItem.AliasCode,
                                    AliasName = menuItem.AliasName,
                                    BarCode = menuItem.BarCode,
                                    CategoryId = category.Id,
                                    ForceChangePrice = menuItem.ForceChangePrice,
                                    ForceQuantity = menuItem.ForceQuantity,
                                    RestrictPromotion = menuItem.RestrictPromotion,
                                    NoTax = menuItem.NoTax,
                                    ItemDescription = menuItem.ItemDescription,
                                    Name = menuItem.Name,
                                    Locations = JsonConvert.SerializeObject(allLocations),
                                    Tag = menuItem.Tag
                                };
                                meD.Portions.Add(new MenuItemPortionEditDto
                                {
                                    Name = menuItem.TPName,
                                    MultiPlier = 1,
                                    Price = menuItem.TPPrice
                                });
                                menuItemInput = new CreateOrUpdateMenuItemInput
                                {
                                    MenuItem = meD,
                                    LocationGroup = { Locations = allLocations }
                                };
                            }
                        }
                        else
                        {
                            var editMenu = await _menuItemAppService.GetMenuItemForEdit(new NullableIdInput { Id = existMenuItem.Id });
                            editMenu.LocationGroup.Locations = allLocations;
                            editMenu.MenuItem.CategoryId = category.Id;
                            editMenu.MenuItem.AliasCode = menuItem.AliasCode;
                            editMenu.MenuItem.AliasName = menuItem.AliasName;
                            editMenu.MenuItem.ItemDescription = menuItem.ItemDescription;
                            editMenu.MenuItem.BarCode = menuItem.BarCode;
                            editMenu.MenuItem.Tag = menuItem.Tag;

                            editMenu.MenuItem.Portions.Add(new MenuItemPortionEditDto
                            {
                                Name = menuItem.TPName,
                                MultiPlier = 1,
                                Price = menuItem.TPPrice
                            });

                            var update = new CreateOrUpdateMenuItemInput
                            {
                                MenuItem = editMenu.MenuItem,
                                ProductType = editMenu.ProductType,
                                Combo = editMenu.Combo,
                                LocationGroup = { Locations = allLocations },
                                UpdateItem = true,
                                UrbanPiper = editMenu.UrbanPiper
                            };

                            await _menuItemAppService.CreateOrUpdateMenuItem(update);
                        }

                    }
                    if (menuItemInput != null)
                    {
                        await _menuItemAppService.CreateOrUpdateMenuItem(menuItemInput);
                    }
                }


                var allMenuitem = _menuItemRepository.GetAll();
                CreateOrUpdateScreenMenuInput screenMenuInput = null;
                for (int i = 0; i < locations.Values.Count; i++)
                {
                    var location = locations.Values.ElementAt(i);

                    var existSM = await _screenMenuAppService.GetScreenMenuByNameAndLocation(location.Name, location.Id);
                    if (existSM != null)
                    {
                        throw new UserFriendlyException(string.Format(L("ScreenMenuErr"), location.Name) + "\n Row: " + i + 1);
                    }

                    var allLocations = new List<SimpleLocationDto>
                    {
                        new SimpleLocationDto
                        {
                            Id = location.Id,
                            Name = location.Name,
                            Code = location.Code
                        }
                    };
                    var sm = new ScreenMenuEditDto
                    {
                        Name = location.Name
                    };
                    screenMenuInput = new CreateOrUpdateScreenMenuInput
                    {
                        ScreenMenu = sm,
                        LocationGroup = { Locations = allLocations }
                    };

                    var menu = new ScreenMenu
                    {
                        Name = location.Name,
                        Departments = null,
                        Locations = JsonHelper.Serialize<List<SimpleLocationDto>>(allLocations),
                        CategoryColumnCount = 2,
                        CategoryColumnWidthRate = 45
                    };

                    menu.Categories = new Collection<ScreenMenuCategory>();
                    var dtoGroup = from d in dtos
                                   group d by new { d.CategoryName }
                                   into g
                                   select new { CategoryName = g.Key.CategoryName, Items = g };
                    foreach (var smc in dtoGroup)
                    {
                        var smcNew = new ScreenMenuCategory
                        {
                            Name = smc.CategoryName,
                            MostUsedItemsCategory = false,
                            ImagePath = null,
                            MainButtonHeight = 60,
                            MainButtonColor = "#78ea86",
                            MainFontSize = 15,
                            ColumnCount = 1,
                            MenuItemButtonHeight = 80,
                            SubButtonHeight = 60,
                            SubButtonRows = 1,
                            SubButtonColorDef = null,
                            MaxItems = 0,
                            PageCount = 1,
                            WrapText = false,
                            SortOrder = 0,
                            NumeratorType = 0
                        };
                        menu.Categories.Add(smcNew);
                        smcNew.MenuItems = new Collection<ScreenMenuItem>();

                        foreach (var smi in smc.Items)
                        {
                            var findMenuItem = allMenuitem.FirstOrDefault(s => s.Name == smi.Name);
                            if (findMenuItem != null)
                            {
                                var mi = new ScreenMenuItem
                                {
                                    Name = smi.Name,
                                    DownloadImage = Guid.NewGuid(),
                                    ImagePath = null,
                                    ShowAliasName = true,
                                    AutoSelect = false,
                                    ButtonColor = "Blue",
                                    FontSize = 20,
                                    SubMenuTag = null,
                                    OrderTags = null,
                                    ItemPortion = null,
                                    MenuItemId = findMenuItem.Id,
                                    SortOrder = 0
                                };
                                smcNew.MenuItems.Add(mi);
                            }
                        }
                    }
                    if (menu.Categories.Any())
                    {
                        await _screenMenu.InsertOrUpdateAndGetIdAsync(menu);
                    }
                }
                return Json(new MvcAjaxResponse());
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
        [UnitOfWork]
        public virtual async Task<JsonResult> ImportOrderTagGroup()
        {
            var rowCount = 1;

            try
            {
                var locations = new Dictionary<string, LocationListDto>();

                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();
                var allCategory = _cateRepo.GetAll();
                var allMenu = _menuItemRepository.GetAll();
                var allDepart = _departmentRepo.GetAll();
                var dtos = new List<ImportOrderTagGroupDto>();
                foreach (var row in allList)
                {
                    if (row["Tag Group Name"].ToString().IsNullOrEmpty())
                    {
                        continue;
                    }
                    var menu = new ImportOrderTagGroupDto
                    {
                        Id = !string.IsNullOrEmpty(row["Id"].ToString()) ? Convert.ToInt32(row["Id"].ToString()) : 0,
                        Departments = row["Departments"]?.ToString().Trim() ?? "",
                        TagGroupName = row["Tag Group Name"]?.ToString().Trim() ?? "",
                        TagAliasName = row["Tag Alias Name"]?.ToString().Trim() ?? "",
                        Minimum = !string.IsNullOrEmpty(row["Minimum"].ToString()) ? Convert.ToInt32(row["Minimum"].ToString()) : 0,
                        Maximum = !string.IsNullOrEmpty(row["Maximum"].ToString()) ? Convert.ToInt32(row["Maximum"].ToString()) : 0,
                        Category = row["Category"]?.ToString().Trim() ?? "",
                        Menu = row["Menu"]?.ToString().Trim() ?? "",
                        AddTagPriceToOrder = !string.IsNullOrEmpty(row["Add Tag Price To Order"].ToString()) ? row["Add Tag Price To Order"].ToString() == "0" ? false : true : false,
                        SaveFreeTags = !string.IsNullOrEmpty(row["Save Free Tags"].ToString()) ? row["Save Free Tags"].ToString() == "0" ? false : true : false,
                        FreeTagging = !string.IsNullOrEmpty(row["Free Tagging"].ToString()) ? row["Free Tagging"].ToString() == "0" ? false : true : false,
                        MenuItem = row["Menu Item"]?.ToString().Trim() ?? "",
                        TagPrice = !string.IsNullOrEmpty(row["Tag Price"].ToString()) ? Convert.ToInt32(row["Tag Price"].ToString()) : 0,
                        Tag = row["Tag"]?.ToString().Trim() ?? "",
                        TagQuantity = !string.IsNullOrEmpty(row["Tag Quantity"].ToString()) ? Convert.ToInt32(row["Tag Quantity"].ToString()) : 0,
                        TaxFree = !string.IsNullOrEmpty(row["Tax Free"].ToString()) ? row["Tax Free"].ToString() == "0" ? false : true : false,
                    };
                    dtos.Add(menu);
                    rowCount++;
                }

                var orderByName = from d in dtos
                                  group d by new { d.TagGroupName, d.Id }
                                  into g
                                  select new { TagGroupName = g.Key.TagGroupName, Id = g.Key.Id, Items = g };

                var allOrder = _orderTagGroupRepo.GetAll();
                foreach (var group in orderByName)
                {
                    var findOrderTagGroup = allOrder.FirstOrDefault(s => s.Name == group.TagGroupName || (group.Id != 0 && s.Id == group.Id));
                    var result = new CreateOrUpdateOrderTagGroupInput();
                    var orderTagGroup = new OrderTagGroupEditDto();
                    var listOrderTagMap = new Collection<OrderMapDto>();
                    var listOrderTag = new Collection<OrderTagDto>();
                    var listDepart = new List<ComboboxItemDto>();
                    // no exist
                    foreach (var item in group.Items)
                    {
                        var findCateId = !string.IsNullOrEmpty(item.Category) ? allCategory.FirstOrDefault(s => s.Name == item.Category)?.Id : null;
                        var findMenuId = !string.IsNullOrEmpty(item.Menu) ? allMenu.FirstOrDefault(s => s.Name == item.Menu)?.Id : null;
                        var findMenuItemId = !string.IsNullOrEmpty(item.MenuItem) ? allMenu.FirstOrDefault(s => s.Name == item.MenuItem)?.Id : null;
                        var findDepart = !string.IsNullOrEmpty(item.Departments) ? item.Departments.Trim() != "*" ? allDepart.FirstOrDefault(s => s.Name == item.Departments) : null : null;
                        if (!string.IsNullOrEmpty(item.Tag))
                        {
                            listOrderTag.Add(new OrderTagDto
                            {
                                AlternateName = item.TagAliasName,
                                MaxQuantity = item.TagQuantity,
                                MenuItemId = findMenuItemId,
                                Name = item.Tag,
                                Price = item.TagPrice,
                            });
                        }
                        if (findCateId != null || findMenuId != null)
                        {
                            listOrderTagMap.Add(new OrderMapDto
                            {
                                CategoryId = findCateId,
                                MenuItemId = findMenuId,
                            });
                        }
                        if (findDepart != null)
                        {
                            listDepart.Add(new ComboboxItemDto
                            {
                                DisplayText = findDepart.Name,
                                IsSelected = false,
                                Value = findDepart.Id.ToString(),
                            });
                        }
                    }
                    var firstGroup = group.Items.FirstOrDefault();
                    var strDepart = JsonConvert.SerializeObject(listDepart);
                    orderTagGroup.Id = findOrderTagGroup?.Id;
                    orderTagGroup.Name = group.TagGroupName;
                    orderTagGroup.MaxSelectedItems = firstGroup.Maximum;
                    orderTagGroup.MinSelectedItems = firstGroup.Minimum;
                    orderTagGroup.Departments = strDepart;
                    orderTagGroup.SaveFreeTags = firstGroup.SaveFreeTags;
                    orderTagGroup.AddTagPriceToOrderPrice = firstGroup.AddTagPriceToOrder;
                    orderTagGroup.TaxFree = firstGroup.TaxFree;
                    orderTagGroup.FreeTagging = firstGroup.FreeTagging;
                    orderTagGroup.Maps = listOrderTagMap;
                    orderTagGroup.Tags = listOrderTag;
                    result.OrderTagGroup = orderTagGroup;
                    await _orderTagGroupAppService.CreateOrUpdateOrderTagGroup(result);
                }
                return Json(new MvcAjaxResponse());
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
        public async Task<FileResult> ImportScreenMenuTemplate()
        {
            return File(Server.MapPath("~/Common/Import/ScreenMenu.xlsx"),
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }

        public async Task<FileResult> ImportTableTemplate()
        {
            return File(Server.MapPath("~/Common/Import/Tables.xlsx"),
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportTable()
        {
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];
                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();

                var dtos = allList.Select(row => new ImportTableDto
                {
                    TableName = row["TableName"]?.ToString() ?? "",
                    Group = row["Group"]?.ToString() ?? "",
                    Pax = row["Pax"] != null ? Convert.ToInt32(row["Pax"]) : 0
                }).ToList();
                foreach (var dd in dtos.GroupBy(a => a.Group))
                {
                    if (!string.IsNullOrEmpty(dd.Key))
                    {
                        var gdto = new ConnectTableGroupEditDto();

                        var input = new CreateOrUpdateConnectTableGroupInput
                        {
                            ConnectTableGroup = gdto
                        };
                        gdto.Name = dd.Key;
                        gdto.Locations = "";
                        gdto.ConnectTables = new List<ConnectTableEditDto>();

                        foreach (var allItems in dd)
                        {
                            var inputTa = new CreateOrUpdateConnectTableInput();
                            inputTa.Table = new ConnectTableEditDto { Name = allItems.TableName, Pax = allItems.Pax };
                            var myId =
                                await _conTaAppService.CreateOrUpdateConnectTable(inputTa);
                            if (myId > 0)
                            {
                                gdto.ConnectTables.Add(new ConnectTableEditDto
                                {
                                    Id = myId
                                });
                            }
                        }
                        await _conTaAppService.CreateOrUpdateConnectTableGroup(input);
                    }
                }
                return Json(new MvcAjaxResponse());
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportPriceTag(int futureDataId)
        {
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];
                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();

                var dtos = allList.Select(row => new TagMenuItemPortionDto
                {
                    Id = row["Id"] != null ? Convert.ToInt32(row["Id"]) : 0,
                    TagId = row["TagId"] != null ? Convert.ToInt32(row["TagId"]) : 0,
                    MenuName = row["MenuName"]?.ToString() ?? "",
                    PortionName = row["PortionName"]?.ToString() ?? "",
                    Price = row["Price"] != null ? Convert.ToDecimal(row["Price"].ToString()) : 0M,
                    TagPrice = row["TagPrice"] != null ? Convert.ToDecimal(row["TagPrice"].ToString()) : 0M
                }).ToList();
                foreach (var dd in dtos)
                {
                    await _priceTagService.UpdateTagPrice(new TagPriceInput
                    {
                        FutureDataId = futureDataId,
                        PortionId = dd.Id.Value,
                        Price = dd.TagPrice,
                        TagId = dd.TagId
                    });
                }
                return Json(new MvcAjaxResponse());
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        public async Task<FileResult> ImportMenuMappingTemplate()
        {
            return File(Server.MapPath("~/Common/Import/MenuMapping.xlsx"),
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportMenuMapping()
        {
            var currentRow = 1;
            int previousMenuQty = 0;
            decimal previousMenuId = 0;
            decimal userSnoAuto = 0;
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();
                var deptList = await _departmentRepo.GetAllListAsync();
                var rsMaterial = await _materialRepo.GetAllListAsync();
                var rsUnit = await _unitRepo.GetAllListAsync();
                var rsMenuItem = await _menuItemRepository.GetAllListAsync();
                var rsMenuPortion = await _menuItemPortionRepo.GetAllListAsync();
                var rsMaterialUnitLink = await _materialUnitLinkRepo.GetAllListAsync();

                foreach (var row in allList)
                {
                    currentRow++;

                    #region Menu And Portion Id Handling / Menu Quantity

                    if (row["Menu Item Name"].ToString().Length == 0 && row["Portion Name"].ToString().Length == 0)
                    {
                        continue;
                    }

                    int menuItemId = 0;
                    string baseMenu = "";
                    {
                        if (row["Menu Item Name"].ToString().Length == 0)
                        {
                            throw new UserFriendlyException("Menu Item Name Missing in Row " + currentRow);
                        }

                        baseMenu = row["Menu Item Name"]?.ToString() ?? "";
                        baseMenu = baseMenu.ToUpper().Trim();

                        var menu = rsMenuItem.FirstOrDefault(t => t.Name.ToUpper().Equals(baseMenu));

                        if (menu != null)
                        {
                            menuItemId = menu.Id;
                        }
                        else
                        {
                            throw new UserFriendlyException("Menu Item Name : " + baseMenu + " Not Exist in Menu Master");
                        }
                    }
                    if (menuItemId == 0)
                    {
                        throw new UserFriendlyException("Menu Item Id Can not be Zero. ");
                    }

                    if (menuItemId != previousMenuQty)
                        userSnoAuto = 0;

                    userSnoAuto++;

                    #endregion Menu And Portion Id Handling / Menu Quantity

                    int menuPortionReferenceId = 0;
                    {
                        if (row["Portion Name"].ToString().Length == 0)
                        {
                            throw new UserFriendlyException("Portion Name Missing ");
                        }

                        var menuPortion = row["Portion Name"]?.ToString() ?? "";
                        menuPortion = menuPortion.ToUpper().Trim();

                        var portion = rsMenuPortion.FirstOrDefault(t => t.MenuItemId == menuItemId && t.Name.ToUpper().Equals(menuPortion));
                        if (portion != null)
                        {
                            menuPortionReferenceId = portion.Id;
                        }
                        else
                        {
                            throw new UserFriendlyException("Portion : " + menuPortion + " Not Available Under Menu : " + baseMenu + " ");
                        }
                    }

                    if (menuPortionReferenceId == 0)
                    {
                        throw new UserFriendlyException("Menu Portion Id Can not be Zero.");
                    }

                    int menuQuantity = 0;
                    if (menuItemId == previousMenuId)
                        menuQuantity = previousMenuQty;
                    else
                    {
                        if (row["Menu Quantity"].ToString().Length == 0)
                        {
                            throw new UserFriendlyException("Menu Quantity Required For Menu :" + baseMenu);
                        }
                        var menuQtyInString = row["Menu Quantity"]?.ToString() ?? "0";
                        menuQuantity = 0;
                        if (int.TryParse(menuQtyInString, out menuQuantity))
                            menuQuantity = int.Parse(menuQtyInString);
                        if (menuQuantity == 0)
                        {
                            throw new UserFriendlyException("Menu Quantity Can not be Zero");
                        }
                    }

                    var userSerialNumberInString = row["Serial Order"]?.ToString() ?? userSnoAuto.ToString();
                    decimal userSerialNumber = 0;
                    if (decimal.TryParse(userSerialNumberInString, out userSerialNumber))
                        userSerialNumber = decimal.Parse(userSerialNumberInString);

                    int materialRefId = 0;
                    string materialCode = "";
                    string materialName = "";
                    {
                        if (row["Material Code"].ToString().Length == 0 && row["Material Name"].ToString().Length == 0)
                        {
                            throw new UserFriendlyException("Material Code & Name Missing ");
                        }

                        materialCode = row["Material Code"]?.ToString() ?? "";
                        materialCode = materialCode.ToUpper().Trim();

                        var material = rsMaterial.FirstOrDefault(t => t.MaterialPetName.ToUpper().Equals(materialCode));

                        if (material != null)
                        {
                            materialRefId = material.Id;
                            materialName = material.MaterialName;
                        }
                        else
                        {
                            materialName = row["Material Name"]?.ToString() ?? "";
                            materialName = materialName.ToUpper().Trim();

                            material = rsMaterial.FirstOrDefault(t => t.MaterialName.ToUpper().Equals(materialName));
                            if (material != null)
                            {
                                materialRefId = material.Id;
                                materialName = material.MaterialName;
                            }
                            else
                            {
                                throw new UserFriendlyException("Material : " + materialName + " Not Exist in Material Master");
                            }
                        }
                    }

                    var portionQtyInString = row["Portion Qty"]?.ToString() ?? "0";
                    decimal portionQty = 0;
                    if (decimal.TryParse(portionQtyInString, out portionQty))
                        portionQty = decimal.Parse(portionQtyInString);
                    if (portionQty == 0)
                    {
                        throw new UserFriendlyException("Portion Quantity Can not be Zero");
                    }

                    int unitRefId;
                    string baseUnit;
                    {
                        if (row["Unit"].ToString().Length == 0)
                        {
                            throw new UserFriendlyException("Portion Unit Missing ");
                        }

                        baseUnit = row["Unit"]?.ToString() ?? "";
                        baseUnit = baseUnit.ToUpper().Trim();

                        var unit = rsUnit.FirstOrDefault(t => t.Name.ToUpper().Equals(baseUnit));

                        if (unit != null)
                        {
                            unitRefId = unit.Id;
                        }
                        else
                        {
                            throw new UserFriendlyException("Portion Unit " + baseUnit + " Not Exist In Unit Master");
                        }

                        var materialLinkUnit = rsMaterialUnitLink.FirstOrDefault(t => t.MaterialRefId == materialRefId && t.UnitRefId == unitRefId);
                        if (materialLinkUnit == null)
                        {
                            throw new UserFriendlyException("Material " + materialName + " do not have the Unit Reference link with " + baseUnit);
                        }
                    }

                    decimal wastageExpected = 0;
                    //var wastageExpectedInString = row["Wastage Expected"]?.ToString() ?? "0";
                    //if (decimal.TryParse(wastageExpectedInString, out wastageExpected))
                    //    wastageExpected = decimal.Parse(wastageExpectedInString);

                    var autoSalesDeductionString = row["AutoSalesDeduction"]?.ToString() ?? "NO";
                    bool autoSalesDeduction = false;
                    if (autoSalesDeductionString.ToUpper().Equals("TRUE") || autoSalesDeductionString.ToUpper().Equals("YES"))
                        autoSalesDeduction = true;

                    //if (deptDtos == null || deptDtos.Count == 0)
                    List<ComboboxItemDto> deptDtos = new List<ComboboxItemDto>();
                    var departmentList = row["Department List"]?.ToString() ?? "ALL";
                    if (departmentList.ToUpper().Equals("ALL"))
                    {
                        deptDtos.Add(new ComboboxItemDto { Value = "0", DisplayText = L("All") });
                    }
                    else
                    {
                        // Pending Needs to handle Diff Departments
                        var arrDepartments = departmentList.Split(',');
                        foreach (var currentDept in arrDepartments)
                        {
                            var deptMentioned = deptList.FirstOrDefault(t => t.Name.Equals(currentDept));
                            if (deptMentioned == null)
                            {
                                throw new UserFriendlyException(" " + currentDept + " " + L("Department") + " " +
                                                                L("NotExist"));
                            }
                            var u = new ComboboxItemDto { Value = deptMentioned.Id.ToString(), DisplayText = deptMentioned.Name };
                            deptDtos.Add(u);
                        }
                    }

                    var alreadyExists = await _materialMenuMappingRepo.FirstOrDefaultAsync(
                                t => t.PosMenuPortionRefId == menuPortionReferenceId && t.MaterialRefId == materialRefId && t.MenuQuantitySold == menuQuantity && t.PortionQty == portionQty && t.PortionUnitId == unitRefId && t.AutoSalesDeduction == autoSalesDeduction);
                    if (alreadyExists != null)
                        continue;

                    List<MaterialMenuMappingListDto> materialMenuMappingDetail = new List<MaterialMenuMappingListDto>();

                    MaterialMenuMappingListDto newDto = new MaterialMenuMappingListDto
                    {
                        PosMenuPortionRefId = menuPortionReferenceId,
                        MenuQuantitySold = menuQuantity,
                        UserSerialNumber = userSerialNumber,
                        MaterialRefId = materialRefId,
                        MaterialRefName = materialName,
                        PortionQty = portionQty,
                        PortionUnitId = unitRefId,
                        UnitRefName = baseUnit,
                        WastageExpected = wastageExpected,
                        AutoSalesDeduction = autoSalesDeduction,
                        LocationRefId = null,
                        DeptList = deptDtos
                    };
                    materialMenuMappingDetail.Add(newDto);
                    //CreateOrUpdateMaterialMenuMapping(CreateOrUpdateMaterialMenuMappingInput input)
                    CreateOrUpdateMaterialMenuMappingInput inputDto = new CreateOrUpdateMaterialMenuMappingInput { MaterialMenuMappingDetail = materialMenuMappingDetail };
                    await _materialMenuMappingAppService.CreateOrUpdateMaterialMenuMapping(inputDto);
                    previousMenuId = menuItemId;
                    previousMenuQty = menuQuantity;
                }

                return Json(new MvcAjaxResponse());
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(L("ErrRow", currentRow) + ex.Message)));
            }
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportMenuMappingChangingMaterials()
        {
            var currentRow = 1;
            int previousMenuQty = 0;
            decimal previousMenuId = 0;
            int currentMaterialRefId = 0;
            //decimal userSnoAuto = 0;
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();
                var deptList = await _departmentRepo.GetAllListAsync();
                var rsMaterial = await _materialRepo.GetAllListAsync();
                var rsUnit = await _unitRepo.GetAllListAsync();
                var rsMenuItem = await _menuItemRepository.GetAllListAsync();
                var rsMenuPortion = await _menuItemPortionRepo.GetAllListAsync();
                var rsMaterialUnitLink = await _materialUnitLinkRepo.GetAllListAsync();

                foreach (var row in allList)
                {
                    currentRow++;

                    if (row["Menu Item Name"].ToString().Length == 0 && row["Portion Name"].ToString().Length == 0)
                    {
                        continue;
                    }

                    if (row["Revised Material Code"].ToString().Length == 0 && row["Revised Material Name"].ToString().Length == 0)
                    {
                        continue;
                    }

                    #region Menu And Portion Id Handling / Menu Quantity

                    int menuItemId = 0;
                    string baseMenu = "";
                    {
                        if (row["Menu Item Name"].ToString().Length == 0)
                        {
                            throw new UserFriendlyException("Menu Item Name Missing in Row " + currentRow);
                        }

                        baseMenu = row["Menu Item Name"]?.ToString() ?? "";
                        baseMenu = baseMenu.ToUpper().Trim();

                        var menu = rsMenuItem.FirstOrDefault(t => t.Name.ToUpper().Equals(baseMenu));

                        if (menu != null)
                        {
                            menuItemId = menu.Id;
                        }
                        else
                        {
                            throw new UserFriendlyException("Menu Item Name : " + baseMenu + " Not Exist in Menu Master");
                        }
                    }
                    if (menuItemId == 0)
                    {
                        throw new UserFriendlyException("Menu Item Id Can not be Zero. ");
                    }

                    //if (menuItemId != previousMenuQty)
                    //    userSnoAuto = 0;

                    //userSnoAuto++;

                    int menuPortionReferenceId = 0;
                    {
                        if (row["Portion Name"].ToString().Length == 0)
                        {
                            throw new UserFriendlyException("Portion Name Missing ");
                        }

                        var menuPortion = row["Portion Name"]?.ToString() ?? "";
                        menuPortion = menuPortion.ToUpper().Trim();

                        var portion = rsMenuPortion.FirstOrDefault(t => t.MenuItemId == menuItemId && t.Name.ToUpper().Equals(menuPortion));
                        if (portion != null)
                        {
                            menuPortionReferenceId = portion.Id;
                        }
                        else
                        {
                            throw new UserFriendlyException("Portion : " + menuPortion + " Not Available Under Menu : " + baseMenu + " ");
                        }
                    }

                    if (menuPortionReferenceId == 0)
                    {
                        throw new UserFriendlyException("Menu Portion Id Can not be Zero.");
                    }

                    int menuQuantity = 0;
                    if (menuItemId == previousMenuId)
                        menuQuantity = previousMenuQty;
                    else
                    {
                        if (row["Menu Qty"].ToString().Length == 0)
                        {
                            throw new UserFriendlyException("Menu Quantity Required For Menu :" + baseMenu);
                        }
                        var menuQtyInString = row["Menu Qty"]?.ToString() ?? "0";
                        menuQuantity = 0;
                        if (int.TryParse(menuQtyInString, out menuQuantity))
                            menuQuantity = int.Parse(menuQtyInString);
                        if (menuQuantity == 0)
                        {
                            throw new UserFriendlyException("Menu Quantity Can not be Zero");
                        }
                    }

                    #endregion Menu And Portion Id Handling / Menu Quantity

                    //var userSerialNumberInString = row["Serial Order"]?.ToString() ?? userSnoAuto.ToString();
                    //decimal userSerialNumber = 0;
                    //if (decimal.TryParse(userSerialNumberInString, out userSerialNumber))
                    //    userSerialNumber = decimal.Parse(userSerialNumberInString);
                    string materialCode = "";
                    string materialName = "";

                    if (currentMaterialRefId == 0)
                    {
                        #region Getting Current Material Ref Id

                        {
                            if (row["Material Code"].ToString().Length == 0 && row["Material Name"].ToString().Length == 0)
                            {
                                throw new UserFriendlyException("Material Code & Name Missing ");
                            }

                            materialCode = row["Material Code"]?.ToString() ?? "";
                            materialCode = materialCode.ToUpper().Trim();

                            var material = rsMaterial.FirstOrDefault(t => t.MaterialPetName.ToUpper().Equals(materialCode));

                            if (material != null)
                            {
                                currentMaterialRefId = material.Id;
                                materialName = material.MaterialName;
                            }
                            else
                            {
                                materialName = row["Material Name"]?.ToString() ?? "";
                                materialName = materialName.ToUpper().Trim();

                                material = rsMaterial.FirstOrDefault(t => t.MaterialName.ToUpper().Equals(materialName));
                                if (material != null)
                                {
                                    currentMaterialRefId = material.Id;
                                    materialName = material.MaterialName;
                                }
                                else
                                {
                                    throw new UserFriendlyException("Material : " + materialName + " Not Exist in Material Master");
                                }
                            }
                        }

                        #endregion Getting Current Material Ref Id
                    }

                    var alreadyExists = await _materialMenuMappingRepo.FirstOrDefaultAsync(t => t.PosMenuPortionRefId == menuPortionReferenceId && t.MaterialRefId == currentMaterialRefId);

                    if (alreadyExists == null)
                    {
                        continue;
                    }

                    int revisedMaterialRefId = 0;

                    #region Get Revised Material Refid

                    {
                        if (row["Revised Material Code"].ToString().Length == 0 && row["Revised Material Name"].ToString().Length == 0)
                        {
                            throw new UserFriendlyException("Material Code & Name Missing ");
                        }

                        materialCode = row["Revised Material Code"]?.ToString() ?? "";
                        materialCode = materialCode.ToUpper().Trim();

                        var material = rsMaterial.FirstOrDefault(t => t.MaterialPetName.ToUpper().Equals(materialCode));

                        if (material != null)
                        {
                            revisedMaterialRefId = material.Id;
                            materialName = material.MaterialName;
                        }
                        else
                        {
                            materialName = row["Revised Material Name"]?.ToString() ?? "";
                            materialName = materialName.ToUpper().Trim();

                            material = rsMaterial.FirstOrDefault(t => t.MaterialName.ToUpper().Equals(materialName));
                            if (material != null)
                            {
                                revisedMaterialRefId = material.Id;
                                materialName = material.MaterialName;
                            }
                            else
                            {
                                throw new UserFriendlyException("Revised Material : " + materialCode + " " + materialName + " Not Exist in Material Master");
                            }
                        }
                    }

                    #endregion Get Revised Material Refid

                    #region Get Portion Qty and Unit

                    var portionQtyInString = row["Revised Portion Qty"]?.ToString() ?? "0";
                    decimal revisedportionQty = 0;
                    if (decimal.TryParse(portionQtyInString, out revisedportionQty))
                        revisedportionQty = decimal.Parse(portionQtyInString);
                    if (revisedportionQty == 0)
                    {
                        throw new UserFriendlyException("Revised Portion Quantity Can not be Zero");
                    }

                    int revisedUnitRefId;
                    string baseUnit;
                    {
                        if (row["Revised Unit"].ToString().Length == 0)
                        {
                            throw new UserFriendlyException("Revised Portion Unit Missing ");
                        }

                        baseUnit = row["Revised Unit"]?.ToString() ?? "";
                        baseUnit = baseUnit.ToUpper().Trim();

                        var unit = rsUnit.FirstOrDefault(t => t.Name.ToUpper().Equals(baseUnit));

                        if (unit != null)
                        {
                            revisedUnitRefId = unit.Id;
                        }
                        else
                        {
                            throw new UserFriendlyException("Portion Unit " + baseUnit + " Not Exist In Unit Master");
                        }

                        var materialLinkUnit = rsMaterialUnitLink.FirstOrDefault(t => t.MaterialRefId == revisedMaterialRefId && t.UnitRefId == revisedUnitRefId);
                        if (materialLinkUnit == null)
                        {
                            throw new UserFriendlyException("Revised Material " + materialName + " do not have the Unit Reference link with " + baseUnit);
                        }
                    }

                    #endregion Get Portion Qty and Unit

                    #region Department Get

                    //var wastageExpectedInString = row["Wastage Expected"]?.ToString() ?? "0";
                    //decimal wastageExpected = 0;
                    //if (decimal.TryParse(wastageExpectedInString, out wastageExpected))
                    //    wastageExpected = decimal.Parse(wastageExpectedInString);

                    //var autoSalesDeductionString = row["Revised Auto Sales Deduction"]?.ToString() ?? "NO";
                    //bool autoSalesDeduction = false;
                    //if (autoSalesDeductionString.ToUpper().Equals("TRUE") || autoSalesDeductionString.ToUpper().Equals("YES"))
                    //    autoSalesDeduction = true;

                    //List<ComboboxItemDto> deptDtos = new List<ComboboxItemDto>();
                    //var departmentList = row["Department List"]?.ToString() ?? "ALL";
                    //if (departmentList.ToUpper().Equals("ALL"))
                    //{
                    //    deptDtos.Add(new ComboboxItemDto { Value = "0", DisplayText = L("All") });
                    //}
                    //else
                    //{
                    //    // Pending Needs to handle Diff Departments
                    //    var arrDepartments = departmentList.Split(',');
                    //    foreach (var currentDept in arrDepartments)
                    //    {
                    //        var deptMentioned = deptList.FirstOrDefault(t => t.Name.Equals(currentDept));
                    //        if (deptMentioned == null)
                    //        {
                    //            throw new UserFriendlyException(" " + currentDept + " " + L("Department") + " " +
                    //                                            L("NotExist"));
                    //        }
                    //        var u = new ComboboxItemDto { Value = deptMentioned.Id.ToString(), DisplayText = deptMentioned.Name };
                    //        deptDtos.Add(u);
                    //    }
                    //}

                    //List<MaterialMenuMappingListDto> materialMenuMappingDetail = new List<MaterialMenuMappingListDto>();

                    //MaterialMenuMappingListDto newDto = new MaterialMenuMappingListDto
                    //{
                    //    PosMenuPortionRefId = menuPortionReferenceId,
                    //    MenuQuantitySold = menuQuantity,
                    //    UserSerialNumber = userSerialNumber,
                    //    MaterialRefId = materialRefId,
                    //    MaterialRefName = materialName,
                    //    PortionQty = portionQty,
                    //    PortionUnitId = unitRefId,
                    //    UnitRefName = baseUnit,
                    //    WastageExpected = wastageExpected,
                    //    AutoSalesDeduction = autoSalesDeduction,
                    //    LocationRefId = null,
                    //    DeptList = deptDtos
                    //};
                    //materialMenuMappingDetail.Add(newDto);
                    ////CreateOrUpdateMaterialMenuMapping(CreateOrUpdateMaterialMenuMappingInput input)
                    //CreateOrUpdateMaterialMenuMappingInput inputDto = new CreateOrUpdateMaterialMenuMappingInput { MaterialMenuMappingDetail = materialMenuMappingDetail };
                    //await _materialMenuMappingAppService.CreateOrUpdateMaterialMenuMapping(inputDto);

                    #endregion Department Get

                    if (currentMaterialRefId != revisedMaterialRefId)
                    {
                        alreadyExists.MaterialRefId = revisedMaterialRefId;
                        alreadyExists.PortionQty = revisedportionQty;
                        alreadyExists.PortionUnitId = revisedUnitRefId;
                        await _materialMenuMappingRepo.UpdateAsync(alreadyExists);
                    }
                    previousMenuId = menuItemId;
                    previousMenuQty = menuQuantity;
                }

                return Json(new MvcAjaxResponse());
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(L("ErrRow", currentRow) + ex.Message)));
            }
        }

        public async Task<FileResult> ImportUnitTemplate()
        {
            return File(Server.MapPath("~/Common/Import/UnitMaster.xlsx"),
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportUnit()
        {
            var currentRow = 1;
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();

                foreach (var row in allList)
                {
                    currentRow++;
                    if (row["Unit Name"].ToString().Length == 0)
                    {
                        continue;
                    }

                    int baseUnitRefId;
                    {
                        if (row["Unit Name"].ToString().Length == 0)
                        {
                            throw new UserFriendlyException("BaseUnit Missing in Row " + currentRow);
                        }

                        var baseUnit = row["Unit Name"]?.ToString() ?? "";
                        baseUnit = baseUnit.ToUpper();

                        var unit =
                            await _unitRepo.FirstOrDefaultAsync(t => t.Name.ToUpper().Equals(baseUnit));

                        if (unit == null)
                        {
                            Unit baseUnitDto = new Unit { Name = baseUnit };
                            baseUnitRefId = await _unitRepo.InsertOrUpdateAndGetIdAsync(baseUnitDto);
                        }
                    }
                }

                return Json(new MvcAjaxResponse());
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(L("ErrRow", currentRow) + ex.Message)));
            }
        }


        public async Task<FileResult> ImportUnitConvesionTemplate()
        {
            return File(Server.MapPath("~/Common/Import/UnitMasterConversionTemplate.xlsx"),
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportUnitConversion()
        {
            var currentRow = 1;
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();

                foreach (var row in allList)
                {
                    currentRow++;
                    if (row["BaseUnit"].ToString().Length == 0 && row["ReferenceUnit"].ToString().Length == 0)
                    {
                        continue;
                    }

                    int baseUnitRefId;
                    {
                        if (row["BaseUnit"].ToString().Length == 0)
                        {
                            throw new UserFriendlyException("BaseUnit Missing in Row " + currentRow);
                        }

                        var baseUnit = row["BaseUnit"]?.ToString() ?? "";
                        baseUnit = baseUnit.ToUpper();

                        var unit =
                            await _unitRepo.FirstOrDefaultAsync(t => t.Name.ToUpper().Equals(baseUnit));

                        if (unit != null)
                        {
                            baseUnitRefId = unit.Id;
                        }
                        else
                        {
                            Unit baseUnitDto = new Unit { Name = baseUnit };
                            baseUnitRefId = await _unitRepo.InsertOrUpdateAndGetIdAsync(baseUnitDto);
                        }
                    }

                    int referenceUnitRefId;
                    {
                        if (row["ReferenceUnit"].ToString().Length == 0)
                        {
                            continue;
                        }

                        var referenceUnit = row["ReferenceUnit"]?.ToString() ?? "";
                        referenceUnit = referenceUnit.ToUpper();

                        var refUnit =
                            await _unitRepo.FirstOrDefaultAsync(t => t.Name.ToUpper().Equals(referenceUnit));
                        if (refUnit != null)
                        {
                            referenceUnitRefId = refUnit.Id;
                        }
                        else
                        {
                            Unit refUnitDto = new Unit { Name = referenceUnit };
                            referenceUnitRefId = await _unitRepo.InsertOrUpdateAndGetIdAsync(refUnitDto);
                        }
                    }

                    if (baseUnitRefId == referenceUnitRefId)
                    {
                        continue;
                    }

                    if (baseUnitRefId == 0)
                    {
                        throw new UserFriendlyException("BaseUnit Id Can not be Zero. Row " + currentRow);
                    }

                    if (referenceUnitRefId == 0)
                    {
                        throw new UserFriendlyException("ReferenceUnit Id Can not be Zero. Row " + currentRow);
                    }

                    if (row["ConversionFactor"].ToString().Length == 0)
                    {
                        throw new UserFriendlyException("ConversionFactor Missing in Row " + currentRow);
                    }

                    var conversionInString = row["ConversionFactor"]?.ToString() ?? "0";
                    decimal conversionFactor = 0;
                    if (decimal.TryParse(conversionInString, out conversionFactor))
                        conversionFactor = decimal.Parse(conversionInString);
                    if (conversionFactor == 0)
                    {
                        throw new UserFriendlyException("ConversionFactor Can not be Zero. Row " + currentRow);
                    }

                    var decimalRoundInString = row["RoundOffDecimal"]?.ToString() ?? "14";
                    byte decimalRound = 0;
                    if (byte.TryParse(decimalRoundInString, out decimalRound))
                        decimalRound = byte.Parse(decimalRoundInString);

                    var alreadyExists = await _unitConversionRepo.FirstOrDefaultAsync(
                                t => t.BaseUnitId == baseUnitRefId && t.RefUnitId == referenceUnitRefId);

                    var unitconerstion = new CreateOrUpdateUnitConversionInput
                    {
                        UnitConversion = new UnitConversionEditDto
                        {
                            BaseUnitId = baseUnitRefId,
                            RefUnitId = referenceUnitRefId,
                            Conversion = conversionFactor,
                            DecimalPlaceRounding = decimalRound,
                        },
                    };

                    if (alreadyExists == null)
                    {
                        unitconerstion.UnitConversion.Id = null;
                    }
                    else
                    {
                        unitconerstion.UnitConversion.Id = alreadyExists.Id;
                    }
                    await _unitConversionAppService.CreateOrUpdateUnitConversion(unitconerstion);
                }

                return Json(new MvcAjaxResponse());
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(L("ErrRow", currentRow) + ex.Message)));
            }
        }

        public async Task<FileResult> ImportMaterialTemplate()
        {
            return File(Server.MapPath("~/Common/Import/Materials.xlsx"),
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportMaterial()
        {
            var currentRow = 1;
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();

                var dtos = new List<MaterialEditDto>();

                var colList = new object[20];

                var ingrFlag = false;

                var lstIngredients = new List<MaterialIngredientEditDto>();
                var lstRecipeType = new List<MaterialRecipeTypesEditDto>();

                var existMaterials = await _materialRepo.GetAllListAsync();
                decimal autoSno = 0;
                if (existMaterials != null && existMaterials.Count > 0)
                    autoSno = existMaterials.Max(t => t.UserSerialNumber);

                var rsUnit = await _unitRepo.GetAllListAsync();
                var rsUnitConversion = await _unitConversionRepo.GetAllListAsync();

                #region ReadExcel

                var userSno = 0;
                foreach (var row in allList)
                {
                    currentRow++;

                    var materialTypeid = 0;

                    string materialName;

                    if (row["MaterialType"].ToString().Length == 0 && row["Name"].ToString().Length == 0 &&
                        row["Group"].ToString().Length == 0 && row["Category"].ToString().Length == 0)
                    {
                        continue;
                    }

                    if (row["MaterialType"].ToString().ToUpper().Trim().Equals("RAW"))
                    {
                        ingrFlag = false;
                        userSno = 0;
                        materialTypeid = (int)MaterialType.RAW;
                    }
                    else if ((row["MaterialType"].ToString().ToUpper().Equals("SEMI")))
                    {
                        ingrFlag = false;
                        userSno = 0;
                        materialTypeid = (int)MaterialType.SEMI;
                    }
                    else if ((row["MaterialType"].ToString().ToUpper().Equals("INGR")))
                    {
                        userSno++;
                        materialName = row["Name"]?.ToString() ?? "";
                        if (materialName.Length == 0)
                        {
                            throw new UserFriendlyException(L("MaterialName"));
                        }
                        if (materialName.Length > 70)
                        {
                            materialName = materialName.Substring(0, 70);
                        }

                        materialName = materialName.ToUpper().Trim();
                        _currentMaterialStatus = L("Material") + " : " + materialName;

                        ingrFlag = true;
                        var ingrMaterialName = row["IngredientName"]?.ToString() ?? "";
                        if (ingrMaterialName.Length == 0)
                        {
                            throw new UserFriendlyException(L("Ingredient"));
                        }
                        if (ingrMaterialName.Length > 70)
                        {
                            ingrMaterialName = ingrMaterialName.Substring(0, 70);
                        }

                        ingrMaterialName = ingrMaterialName.ToUpper().Trim();

                        var ingrQuantity = string.IsNullOrEmpty(row["Quantity"]?.ToString())
                            ? 0
                            : Convert.ToDecimal(row["Quantity"]);

                        if (ingrQuantity == 0)
                        {
                            throw new UserFriendlyException(L("Ingredient") + L("Quantity"));
                        }

                        var mingrDto = new MaterialIngredientEditDto
                        {
                            RecipeRefName = materialName,
                            MaterialRefName = ingrMaterialName,
                            MaterialUsedQty = ingrQuantity,
                            UserSerialNumber = userSno
                        };

                        lstIngredients.Add(mingrDto);
                        continue;
                    }
                    else
                        throw new UserFriendlyException(L("MaterialType"));

                    materialName = row["Name"]?.ToString() ?? "";
                    if (materialName.Length == 0)
                    {
                        throw new UserFriendlyException(L("MaterialName"));
                    }
                    if (materialName.Length > 70)
                    {
                        materialName = materialName.Substring(0, 70);
                    }

                    materialName = materialName.ToUpper().Trim();
                    _currentMaterialStatus = L("Material") + " : " + materialName;

                    if (dtos.Any(t => t.MaterialName.Equals(materialName)))
                    {
                        throw new UserFriendlyException(L("MaterialName") + " " +
                                                        materialName + " " + L("AlreadyExist"));
                    }

                    string barcode;
                    barcode = row["Barcode"]?.ToString() ?? "";
                    if (barcode.Length == 0)
                    {
                        barcode = null;
                    }
                    else if (barcode.Length > 50)
                    {
                        barcode = barcode.Substring(0, 50);
                    }

                    if (!string.IsNullOrEmpty(barcode))
                    {
                        var dtoBarcode = dtos.FirstOrDefault(t => t.Barcode == barcode);
                        if (dtoBarcode != null)
                        {
                            throw new UserFriendlyException(L("BarcodeAlreadyExists", dtoBarcode.MaterialName));
                        }
                        barcode = barcode.Replace("'", "");
                    }

                    int? materialId = null;
                    var mat = existMaterials.FirstOrDefault(t => t.MaterialName.ToUpper().Equals(materialName));
                    if (mat != null)
                    {
                        materialId = mat.Id;
                    }

                    decimal sortOrder = string.IsNullOrEmpty(row["SortOrder"]?.ToString())
                        ? 0
                        : Convert.ToInt32(row["SortOrder"]);
                    if (sortOrder == 0)
                    {
                        if (mat != null)
                        {
                            sortOrder = mat.UserSerialNumber;
                        }
                        else
                        {
                            sortOrder = autoSno++;
                        }
                    }

                    var materialPetName = row["Code"]?.ToString() ?? "";
                    if (materialPetName.Length == 0)
                    {
                        if (materialName.Length > 70)
                            materialPetName = materialName.Substring(0, 70);
                        else
                            materialPetName = materialName;
                    }
                    else if (materialPetName.Length > 70)
                    {
                        materialPetName = materialPetName.Substring(0, 70);
                    }

                    if (dtos.Any(t => t.MaterialPetName.Equals(materialPetName)))
                    {
                        throw new UserFriendlyException(L("Code") + " " +
                                                        materialPetName + " " + L("AlreadyExist"));
                    }

                    var materialHsnAccountCode = row["Account Code"]?.ToString() ?? "";

                    var currentUom = row["Unit"].ToString().ToUpper().Trim();
                    if (currentUom.Length == 0)
                    {
                        throw new UserFriendlyException(L("Unit"));
                    }

                    int defaultUnitId;

                    var unit = rsUnit.FirstOrDefault(t => t.Name.Equals(currentUom));
                    if (unit == null)
                    {
                        throw new UserFriendlyException(currentUom + " " + L("Unit") + " " + L("NotExist"));
                    }
                    defaultUnitId = unit.Id;

                    var issueUom = row["IssueUnit"].ToString().ToUpper().Trim();
                    if (issueUom.Length == 0)
                    {
                        throw new UserFriendlyException(L("IssueUnit"));
                    }

                    int issueUnitId;

                    var issueunit = rsUnit.FirstOrDefault(t => t.Name.Equals(issueUom));
                    if (issueunit == null)
                    {
                        throw new UserFriendlyException(issueUom + " " + L("IssueUnit") + " " + L("NotExist"));
                    }
                    issueUnitId = issueunit.Id;

                    if (defaultUnitId != issueUnitId)
                    {
                        var convUnit =
                            rsUnitConversion.Where(t => (t.BaseUnitId == defaultUnitId && t.RefUnitId == issueUnitId))
                                .ToList();

                        if (convUnit.Count == 0)
                        {
                            throw new UserFriendlyException(L("ConversionFactorNotExist", issueUom, currentUom));
                        }

                        convUnit =
                            rsUnitConversion.Where(t => (t.BaseUnitId == issueUnitId && t.RefUnitId == defaultUnitId))
                                .ToList();

                        if (convUnit.Count == 0)
                        {
                            throw new UserFriendlyException(L("ConversionFactorNotExist", issueUom, currentUom));
                        }
                    }

                    //"Transfer Unit"
                    var transferUom = row["Transfer Unit"].ToString().ToUpper().Trim();
                    if (transferUom.Length == 0)
                    {
                        throw new UserFriendlyException(L("Transfer") + " " + L("Unit"));
                    }

                    int transferUnitId;

                    var transferunit = rsUnit.FirstOrDefault(t => t.Name.Equals(transferUom));
                    if (transferunit == null)
                    {
                        throw new UserFriendlyException(transferUom + " " + L("Transfer") + " " + L("Unit") + " " + L("NotExist"));
                    }
                    transferUnitId = transferunit.Id;

                    if (defaultUnitId != transferUnitId)
                    {
                        var convUnit =
                            rsUnitConversion.Where(t => (t.BaseUnitId == defaultUnitId && t.RefUnitId == transferUnitId))
                                .ToList();

                        if (convUnit.Count == 0)
                        {
                            throw new UserFriendlyException(L("ConversionFactorNotExist", transferUom, currentUom));
                        }

                        convUnit =
                            rsUnitConversion.Where(t => (t.BaseUnitId == transferUnitId && t.RefUnitId == defaultUnitId))
                                .ToList();

                        if (convUnit.Count == 0)
                        {
                            throw new UserFriendlyException(L("ConversionFactorNotExist", transferUom, currentUom));
                        }
                    }

                    //"Stock Adj Unit"
                    var stockAdjUom = row["Stock Adj Unit"].ToString().ToUpper().Trim();
                    if (stockAdjUom.Length == 0)
                    {
                        throw new UserFriendlyException(L("Stock") + " " + L("Adjustment") + " " + L("Unit"));
                    }

                    int stkAdjUnitId;

                    var stkAdjunit = rsUnit.FirstOrDefault(t => t.Name.Equals(stockAdjUom));
                    if (stkAdjunit == null)
                    {
                        throw new UserFriendlyException(transferUom + " " + L("Stock") + " " + L("Adjustment") + " " + L("Unit") + " " + L("NotExist"));
                    }
                    stkAdjUnitId = stkAdjunit.Id;

                    if (defaultUnitId != stkAdjUnitId)
                    {
                        var convUnit =
                            rsUnitConversion.Where(t => (t.BaseUnitId == defaultUnitId && t.RefUnitId == stkAdjUnitId))
                                .ToList();

                        if (convUnit.Count == 0)
                        {
                            throw new UserFriendlyException(L("ConversionFactorNotExist", stockAdjUom, currentUom));
                        }

                        convUnit =
                            rsUnitConversion.Where(t => (t.BaseUnitId == stkAdjUnitId && t.RefUnitId == defaultUnitId))
                                .ToList();

                        if (convUnit.Count == 0)
                        {
                            throw new UserFriendlyException(L("ConversionFactorNotExist", stockAdjUom, currentUom));
                        }
                    }

                    var unitList = new List<ComboboxItemDto>();

                    var currentUnitList = row["UnitList"].ToString().ToUpper().Trim();
                    if (currentUnitList.Length > 0)
                    {
                        var arrUnits = currentUnitList.Split(',');

                        foreach (var currentUnit in arrUnits)
                        {
                            var unitGiven = rsUnit.FirstOrDefault(t => t.Name.Equals(currentUnit));
                            if (unitGiven == null)
                            {
                                throw new UserFriendlyException(" " + currentUnit + " " + L("Unit") + " " +
                                                                L("NotExist"));
                            }
                            if (defaultUnitId != issueUnitId)
                            {
                                var convUnit =
                                    rsUnitConversion.Where(
                                        t => (t.BaseUnitId == defaultUnitId && t.RefUnitId == unitGiven.Id)).ToList();

                                if (convUnit.Count == 0)
                                {
                                    throw new UserFriendlyException(L("ConversionFactorNotExist", currentUnit,
                                        currentUom));
                                }
                            }
                            var u = new ComboboxItemDto { Value = unitGiven.Id.ToString(), DisplayText = unitGiven.Name };
                            unitList.Add(u);
                        }
                    }

                    var generalLifeDays = 0;

                    _currentProcess = "Procesing OwnPreparation";

                    var ownpreparation = row["OwnPreparation"]?.ToString() ?? "FALSE";
                    if (ownpreparation.Trim().Length == 0 || ownpreparation == "" || ownpreparation == null)
                        ownpreparation = "FALSE";
                    var ownpreparationflag = Convert.ToBoolean(ownpreparation);

                    _currentProcess = "Procesing WipeOutStock";
                    var wipeoutstock = row["WipeOutStock"]?.ToString() ?? "FALSE";

                    if (wipeoutstock.Trim().Length == 0 || wipeoutstock == "" || wipeoutstock == null)
                    {
                        if (materialTypeid == (int)MaterialType.SEMI)
                            throw new UserFriendlyException(L("WipeOutStockErr"));
                        wipeoutstock = "FALSE";
                    }
                    var wipeoutstockflag = Convert.ToBoolean(wipeoutstock);

                    var isHighValueStock = false;
                    _currentProcess = "Procesing IsHighValueStock";
                    var highvalueStock = row["IsHighValueStock"]?.ToString() ?? "FALSE";
                    if (highvalueStock.Trim().Length == 0)
                        highvalueStock = "FALSE";
                    isHighValueStock = Convert.ToBoolean(highvalueStock);

                    var convertAsZeroStockWhenClosingStockNegative = false;
                    _currentProcess = "Procesing Convert As Zero Stock When Closing Stock Negative";
                    var convertAsZeroStockWhenClosingStockNegativeString = row["Convert As Zero Stock When Closing Stock Negative"]?.ToString() ?? "FALSE";
                    if (convertAsZeroStockWhenClosingStockNegativeString.Trim().Length == 0)
                        convertAsZeroStockWhenClosingStockNegative = false;
                    else
                    {
                        if (convertAsZeroStockWhenClosingStockNegativeString.ToUpper().Equals("YES") || convertAsZeroStockWhenClosingStockNegativeString.ToUpper().Equals("TRUE"))
                            convertAsZeroStockWhenClosingStockNegative = true;
                    }

                    if (materialTypeid == (int)MaterialType.SEMI)
                    {
                        _currentProcess = "Procesing ProdQty";
                        var prdBatchQty = string.IsNullOrEmpty(row["ProdQty"]?.ToString())
                            ? 0
                            : Convert.ToDecimal(row["ProdQty"]);

                        _currentProcess = "Procesing FixedCost";
                        var fixedCost = string.IsNullOrEmpty(row["FixedCost"]?.ToString())
                            ? 0
                            : Convert.ToDecimal(row["FixedCost"]);

                        _currentProcess = "Procesing LifeTime";
                        var lifeTime = string.IsNullOrEmpty(row["LifeTime"]?.ToString())
                            ? 0
                            : Convert.ToInt16(row["LifeTime"]);

                        _currentProcess = "Procesing AlarmTime";
                        var alarmTime = string.IsNullOrEmpty(row["AlarmTime"]?.ToString())
                            ? 0
                            : Convert.ToInt16(row["AlarmTime"]);

                        if (alarmTime > lifeTime)
                        {
                            throw new UserFriendlyException(" " + materialName + " " + L("AlarmMismatchErr", alarmTime));
                        }

                        var alarmFlag = false;
                        if (alarmTime > 0)
                            alarmFlag = true;

                        var mrectypeDto = new MaterialRecipeTypesEditDto
                        {
                            MaterialRefName = materialName,
                            PrdBatchQty = prdBatchQty,
                            AlarmTimeInMinutes = alarmTime,
                            LifeTimeInMinutes = lifeTime,
                            AlarmFlag = true,
                            FixedCost = fixedCost
                        };

                        lstRecipeType.Add(mrectypeDto);
                    }

                    var material = new MaterialEditDto
                    {
                        Id = materialId,
                        MaterialTypeId = materialTypeid,
                        MaterialGroupName = row["Group"]?.ToString().ToUpper().Trim() ?? "",
                        MaterialCategoryName = row["Category"]?.ToString() ?? "",
                        Barcode = barcode,
                        MaterialName = materialName, // row["Name"] ? .ToString().ToUpper() ?? "",
                        MaterialPetName = materialPetName, // row["PETNAME"]?.ToString().ToUpper() ?? "",
                        DefaultUnitId = defaultUnitId,
                        IssueUnitId = issueUnitId,
                        TransferUnitId = transferUnitId,
                        StockAdjustmentUnitId = stkAdjUnitId,
                        UserSerialNumber = sortOrder,
                        GeneralLifeDays = generalLifeDays,
                        OwnPreparation = ownpreparationflag,
                        WipeOutStockOnClosingDay = wipeoutstockflag,
                        IsBranded = false,
                        MRPPriceExists = false,
                        IsQuoteNeededForPurchase = false,
                        IsNeedtoKeptinFreezer = false,
                        IsHighValueItem = isHighValueStock,
                        PosReferencecodeifany = 0,
                        UnitList = unitList,
                        ConvertAsZeroStockWhenClosingStockNegative = convertAsZeroStockWhenClosingStockNegative,
                        Hsncode = materialHsnAccountCode
                    };
                    dtos.Add(material);
                    unitList = new List<ComboboxItemDto>();
                    if (currentRow % 10 == 0)
                    {
                        var a = "Test";
                    }
                    _currentProcess = "";
                }

                #endregion ReadExcel

                #region DtosAddPart

                currentRow = 1;

                foreach (var lst in dtos)
                {
                    currentRow++;

                    var mgcdto =
                        await
                            _materialgroupcategoryAppService.GetOrCreateByName(lst.MaterialCategoryName,
                                lst.MaterialGroupName);

                    CreateOrUpdateMaterialInput input = null;
                    if (mgcdto.Id != null)
                    {
                        var materialDto = new MaterialEditDto
                        {
                            Id = lst.Id,
                            MaterialGroupCategoryRefId = (int)mgcdto.Id,
                            MaterialName = lst.MaterialName,
                            MaterialPetName = lst.MaterialPetName,
                            MaterialTypeId = lst.MaterialTypeId,
                            OwnPreparation = lst.OwnPreparation,
                            UserSerialNumber = lst.UserSerialNumber,
                            GeneralLifeDays = lst.GeneralLifeDays,
                            DefaultUnitId = lst.DefaultUnitId,
                            IssueUnitId = lst.IssueUnitId,
                            ReceivingUnitId = lst.DefaultUnitId,
                            TransferUnitId = lst.TransferUnitId,
                            StockAdjustmentUnitId = lst.StockAdjustmentUnitId,
                            IsMfgDateExists = lst.IsMfgDateExists,
                            IsBranded = lst.IsBranded,
                            WipeOutStockOnClosingDay = lst.WipeOutStockOnClosingDay,
                            IsFractional = lst.IsFractional,
                            MRPPriceExists = lst.MRPPriceExists,
                            IsNeedtoKeptinFreezer = lst.IsNeedtoKeptinFreezer,
                            IsQuoteNeededForPurchase = lst.IsQuoteNeededForPurchase,
                            PosReferencecodeifany = lst.PosReferencecodeifany,
                            IsHighValueItem = lst.IsHighValueItem,
                            Hsncode = lst.Hsncode,
                            Barcode = lst.Barcode,
                        };

                        var materialRecipeType = new MaterialRecipeTypesEditDto();

                        if (lst.MaterialTypeId == (int)MaterialType.SEMI)
                        {
                            var retDto = lstRecipeType.FirstOrDefault(t => t.MaterialRefName == lst.MaterialName);
                            if (retDto != null)
                                materialRecipeType = retDto;
                        }

                        var unitList = new List<ComboboxItemDto>();
                        unitList = lst.UnitList;

                        input = new CreateOrUpdateMaterialInput
                        {
                            Material = materialDto,
                            MaterialRecipeType = materialRecipeType,
                            UnitList = unitList,
                        };
                    }

                    if (input != null)
                    {
                        var matid = await _materialAppService.CreateOrUpdateMaterial(input);
                    }
                }

                #endregion DtosAddPart

                #region IngrDtosFills

                existMaterials = await _materialRepo.GetAllListAsync();

                foreach (var group in lstIngredients.GroupBy(t => t.RecipeRefName))
                {
                    var curMat = existMaterials.FirstOrDefault(t => t.MaterialName == group.Key);
                    if (curMat == null)
                    {
                        throw new UserFriendlyException(group.Key + " " + L("Ingredient") + " " + L("NotAvailable"));
                    }

                    var matingrlist = new List<MaterialIngredientEditDto>();

                    foreach (var ingrdto in group)
                    {
                        var ingrMaterial = existMaterials.FirstOrDefault(t => t.MaterialName == ingrdto.MaterialRefName);
                        if (ingrMaterial == null)
                        {
                            throw new UserFriendlyException(ingrdto.MaterialRefName + " " + L("Ingredient") + " " +
                                                            L("NotAvailable"));
                        }
                        ingrdto.MaterialRefId = ingrMaterial.Id;
                        ingrdto.RecipeRefId = curMat.Id;
                        ingrdto.UnitRefId = ingrMaterial.DefaultUnitId;
                        matingrlist.Add(ingrdto);
                    }

                    var materialDto = await _materialAppService.GetMaterialForEdit(new NullableIdInput { Id = curMat.Id });
                    var dtoEdit = new CreateOrUpdateMaterialInput
                    {
                        Material = materialDto.Material,
                        UnitList = materialDto.UnitList,
                        BrandList = materialDto.BrandList,
                        SupplierMaterial = materialDto.SupplierMaterial,
                        CustomerMaterial = materialDto.CustomerMaterial,
                        CustomerTagMaterialPrice = materialDto.CustomerTagMaterialPrice,
                        MaterialIngredient = matingrlist,
                        MaterialRecipeType = materialDto.MaterialRecipeType
                    };
                    await _materialAppService.CreateOrUpdateMaterial(dtoEdit);
                }

                #endregion IngrDtosFills

                return Json(new MvcAjaxResponse());
            }
            catch (Exception ex)
            {
                return
                    Json(
                        new MvcAjaxResponse(
                            new ErrorInfo(_currentProcess + " " + _currentMaterialStatus + " " + L("ErrRow", currentRow) +
                                          ex.Message)));
                //throw new UserFriendlyException(L("ErrRow", currentRow) + ex.Message);
            }
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportStockWithReOrderLevel()
        {
            var currentRow = 1;
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();

                var newAdjDto = new CreateOrUpdateAdjustmentInput();

                var masDto = new AdjustmentEditDto();
                var detDtos = new List<AdjustmentDetailViewDto>();

                masDto.AdjustmentRemarks = L("OnHandInitialSetup");
                masDto.ApprovedPersonId = (int)AbpSession.UserId;

                masDto.EnteredPersonId = (int)AbpSession.UserId;
                masDto.TokenRefNumber = 666;

                int locationRefId;
                var sno = 1;
                var locdata = new Location();
                //LOCATION	MATERIALNAME	MINIMUMSTOCK	MAXIMUMSTOCK	ONHAND	REORDERLEVEL
                var rsLocation = await _locationRepo.GetAllListAsync();
                var rsMaterial = await _materialRepo.GetAllListAsync();
                List<MaterialLocationWiseStock> rsMaterialLocationWiseStock = new List<MaterialLocationWiseStock>();
                foreach (var row in allList)
                {
                    currentRow++;

                    if (row[L("Location")].ToString().Length == 0 || row[L("Material")].ToString().Length == 0)
                    {
                        continue;
                    }

                    var location = row[L("Location")]?.ToString() ?? "";
                    if (location.Length == 0)
                    {
                        throw new UserFriendlyException(L("ErrRow", currentRow) + L("Location"));
                    }

                    locdata = rsLocation.FirstOrDefault(t => t.Name.ToUpper().Equals(location.ToUpper()));
                    if (locdata == null)
                    {
                        throw new UserFriendlyException(L("ErrRow", currentRow) + L("Location"));
                    }

                    locationRefId = locdata.Id;

                    masDto.LocationRefId = locationRefId;

                    var material = row[L("Material")]?.ToString() ?? "";
                    if (material.Length == 0)
                    {
                        throw new UserFriendlyException(L("ErrRow", currentRow) + L("Material"));
                    }

                    var matdata =
                        rsMaterial.FirstOrDefault(t => t.MaterialName.ToUpper().Equals(material.ToUpper()));
                    if (matdata == null)
                    {
                        throw new UserFriendlyException(L("MaterialNotFound") + ' ' + material);
                    }

                    var materialRefId = matdata.Id;
                    var unitRefId = matdata.DefaultUnitId;
                    if (materialRefId == 0)
                    {
                        throw new UserFriendlyException(L("MaterialNotFound") + ' ' + material);
                    }

                    var matlocstock = rsMaterialLocationWiseStock.FirstOrDefault(t => t.LocationRefId == locationRefId && t.MaterialRefId == materialRefId);
                    if (matlocstock == null)
                    {
                        rsMaterialLocationWiseStock = await _materiallocationwisestockRepo.GetAllListAsync(
                                    t => t.LocationRefId == locationRefId);
                        //var matlocstock = await
                        //        _materiallocationwisestockRepo.FirstOrDefaultAsync(
                        //            t => t.LocationRefId == locationRefId && t.MaterialRefId == materialRefId);
                        matlocstock = rsMaterialLocationWiseStock.FirstOrDefault(t => t.LocationRefId == locationRefId && t.MaterialRefId == materialRefId);
                    }

                    if (matlocstock == null)
                    {
                        throw new UserFriendlyException(L("MaterialNotFound") + ' ' + material);
                    }

                    decimal decCurrentStock = 0;
                    if (matlocstock.CurrentInHand > 0)
                    {
                        decCurrentStock = matlocstock.CurrentInHand;
                    }
                    else
                    {
                        var currentstock = row[L("OnHand")]?.ToString() ?? "";
                        decCurrentStock = 0;
                        if (decimal.TryParse(currentstock, out decCurrentStock))
                            decCurrentStock = decimal.Parse(currentstock);

                        if (decCurrentStock < 0)
                        {
                            throw new UserFriendlyException("OnHandShouldNotBeNegative");
                        }

                        var det = new AdjustmentDetailViewDto();

                        det.MaterialRefId = materialRefId;
                        det.AdjustmentQty = decCurrentStock;
                        det.AdjustmentApprovedRemarks = L("Initial");
                        det.AdjustmentMode = L("Excess");
                        det.UnitRefId = unitRefId;
                        det.Sno = sno;
                        sno++;

                        detDtos.Add(det);

                        decCurrentStock = matlocstock.CurrentInHand;
                    }

                    decimal decMinimumStock = 0;
                    var minstock = row[L("MinimumStock")]?.ToString() ?? "";
                    decMinimumStock = 0;
                    if (decimal.TryParse(minstock, out decMinimumStock))
                        decMinimumStock = decimal.Parse(minstock);

                    if (decMinimumStock == 0)
                        decMinimumStock = matlocstock.MinimumStock;

                    decimal decMaximumStock = 0;
                    var maxstock = row[L("MaximumStock")]?.ToString() ?? "";
                    decMaximumStock = 0;
                    if (decimal.TryParse(maxstock, out decMaximumStock))
                        decMaximumStock = decimal.Parse(maxstock);

                    if (decMaximumStock == 0)
                        decMaximumStock = matlocstock.MaximumStock;

                    if (decMinimumStock > decMaximumStock)
                    {
                        throw new UserFriendlyException(L("MinMaxStockErr") + " " + material);
                    }

                    decimal decReOrderLevel = 0;
                    var reorderlevel = row[L("Re-OrderLevel")]?.ToString() ?? "";
                    decReOrderLevel = 0;
                    if (decimal.TryParse(reorderlevel, out decReOrderLevel))
                        decReOrderLevel = decimal.Parse(reorderlevel);

                    if (decReOrderLevel == 0)
                        decReOrderLevel = matlocstock.ReOrderLevel;

                    if (decMinimumStock > decReOrderLevel)
                    {
                        throw new UserFriendlyException(L("MinROLError") + " " + material);
                    }

                    if (decMaximumStock < decReOrderLevel)
                    {
                        throw new UserFriendlyException(L("MaxROLError") + " " + material);
                    }

                    var convertAsZeroStockWhenClosingStockNegativeString = row[L("ConvertAsZeroStockWhenClosingStockNegative")]?.ToString() ?? "NO";
                    bool convertAsZeroStockWhenClosingStockNegative = false;
                    if (convertAsZeroStockWhenClosingStockNegativeString.ToUpper().Equals("TRUE") || convertAsZeroStockWhenClosingStockNegativeString.ToUpper().Equals("YES"))
                        convertAsZeroStockWhenClosingStockNegative = true;


                    var isActiveInLocationString = row[L("IsActiveInLocation")]?.ToString() ?? "YES";
                    bool isActiveInLocation = true;
                    if (isActiveInLocationString.ToUpper().Equals("FALSE") || isActiveInLocationString.ToUpper().Equals("NO"))
                        isActiveInLocation = false;

                    var matlocupdatedto = new MaterialLocationWiseStockEditDto
                    {
                        Id = matlocstock.Id,
                        MaterialRefId = materialRefId,
                        LocationRefId = locationRefId,
                        MinimumStock = decMinimumStock,
                        MaximumStock = decMaximumStock,
                        CurrentInHand = matlocstock.CurrentInHand,
                        ReOrderLevel = decReOrderLevel,
                        IsOrderPlaced = matlocstock.IsOrderPlaced,
                        IsActiveInLocation = isActiveInLocation,
                        ConvertAsZeroStockWhenClosingStockNegative = convertAsZeroStockWhenClosingStockNegative,
                    };

                    var input = new CreateOrUpdateMaterialLocationWiseStockInput
                    {
                        MaterialLocationWiseStock = matlocupdatedto
                    };

                    if (input != null)
                    {
                        await _materiallocationwisestockAppService.CreateOrUpdateMaterialLocationWiseStock(input);
                    }
                }

                AccountDateDto closedDay;
                if (locdata.HouseTransactionDate != null)
                {
                    masDto.AdjustmentDate = locdata.HouseTransactionDate.Value;
                }
                else
                {
                    closedDay = await _dayCloseAppService.GetDayCloseStatus(new IdInput { Id = locdata.Id });
                    masDto.AdjustmentDate = closedDay.AccountDate;
                }

                if (detDtos.Count > 0)
                {
                    newAdjDto.Adjustment = masDto;
                    newAdjDto.AdjustmentDetail = detDtos;
                    await _adjustmentAppService.CreateOrUpdateAdjustment(newAdjDto);
                }

                return Json(new MvcAjaxResponse());
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(L("ErrRow", currentRow) + ex.Message)));
            }
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportMaterialActiveUpdation()
        {
            var currentRow = 1;
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();

                var newAdjDto = new CreateOrUpdateAdjustmentInput();

                var masDto = new AdjustmentEditDto();
                var detDtos = new List<AdjustmentDetailViewDto>();

                masDto.AdjustmentRemarks = L("OnHandInitialSetup");
                masDto.ApprovedPersonId = (int)AbpSession.UserId;

                masDto.EnteredPersonId = (int)AbpSession.UserId;
                masDto.TokenRefNumber = 666;

                int locationRefId;
                var locdata = new Location();
                //LOCATION	MATERIALNAME	MINIMUMSTOCK	MAXIMUMSTOCK	ONHAND	REORDERLEVEL
                var rsLocation = await _locationRepo.GetAllListAsync();
                var rsMaterial = await _materialRepo.GetAllListAsync();
                List<MaterialLocationWiseStock> rsMaterialLocationWiseStock = new List<MaterialLocationWiseStock>();
                foreach (var row in allList)
                {
                    currentRow++;

                    if (row[L("Location")].ToString().Length == 0 || row[L("Material")].ToString().Length == 0)
                    {
                        continue;
                    }

                    var location = row[L("Location")]?.ToString() ?? "";
                    if (location.Length == 0)
                    {
                        throw new UserFriendlyException(L("ErrRow", currentRow) + L("Location"));
                    }

                    locdata = rsLocation.FirstOrDefault(t => t.Name.ToUpper().Equals(location.ToUpper()));
                    if (locdata == null)
                    {
                        throw new UserFriendlyException(L("ErrRow", currentRow) + L("Location"));
                    }

                    locationRefId = locdata.Id;

                    masDto.LocationRefId = locationRefId;

                    var material = row[L("Material")]?.ToString() ?? "";
                    if (material.Length == 0)
                    {
                        throw new UserFriendlyException(L("ErrRow", currentRow) + L("Material"));
                    }

                    var matdata =
                        rsMaterial.FirstOrDefault(t => t.MaterialName.ToUpper().Equals(material.ToUpper()));
                    if (matdata == null)
                    {
                        throw new UserFriendlyException(L("MaterialNotFound") + ' ' + material);
                    }

                    var materialRefId = matdata.Id;
                    var unitRefId = matdata.DefaultUnitId;
                    if (materialRefId == 0)
                    {
                        throw new UserFriendlyException(L("MaterialNotFound") + ' ' + material);
                    }

                    var convertAsZeroStockWhenClosingStockNegativeString = row[L("ConvertAsZeroStockWhenClosingStockNegative")]?.ToString() ?? "NO";
                    bool convertAsZeroStockWhenClosingStockNegative = false;
                    if (convertAsZeroStockWhenClosingStockNegativeString.ToUpper().Equals("TRUE") || convertAsZeroStockWhenClosingStockNegativeString.ToUpper().Equals("YES"))
                        convertAsZeroStockWhenClosingStockNegative = true;


                    var isActiveInLocationString = row[L("IsActiveInLocation")]?.ToString() ?? "YES";
                    bool isActiveInLocation = true;
                    if (isActiveInLocationString.ToUpper().Equals("FALSE") || isActiveInLocationString.ToUpper().Equals("NO"))
                        isActiveInLocation = false;

                    var existRecord = await _materiallocationwisestockRepo.FirstOrDefaultAsync(t => t.LocationRefId == locationRefId && t.MaterialRefId == materialRefId);
                    if (existRecord != null)
                    {
                        existRecord.IsActiveInLocation = isActiveInLocation;
                        existRecord.ConvertAsZeroStockWhenClosingStockNegative = convertAsZeroStockWhenClosingStockNegative;
                        await _materiallocationwisestockRepo.UpdateAsync(existRecord);
                    }
                }

                return Json(new MvcAjaxResponse());
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(L("ErrRow", currentRow) + ex.Message)));
            }
        }

        public async Task<FileResult> GetPdfFile(string filePath)
        {
            return File(Server.MapPath(filePath), MimeTypeNames.ApplicationPdf);
        }

        public async Task<FileResult> ImportSupplierMaterialTemplate()
        {
            return File(Server.MapPath("~/Common/Import/SupplierMaterial.xlsx"),
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportSupplierMaterial()
        {
            var currentRow = 1;
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();

                var rsLocations = await _locationRepo.GetAllListAsync();
                var rsSuppliers = await _supplierRepo.GetAllListAsync();
                var rsMaterials = await _materialRepo.GetAllListAsync();
                var rsUnits = await _unitRepo.GetAllListAsync();
                ImportDataResult importDataResult = new ImportDataResult();
                foreach (var row in allList)
                {
                    currentRow++;

                    if (row["Supplier"].ToString().Length == 0)
                    {
                        continue;
                    }
                    ImportDetailDataDto importDetailDataDto = new ImportDetailDataDto { RowNumber = currentRow };
                    string errorRemarks = "";
                    var supplierName = row["Supplier"]?.ToString() ?? "";
                    if (supplierName.Length == 0)
                    {
                        continue;
                    }
                    supplierName = supplierName.ToUpper();
                    importDetailDataDto.Description = supplierName;

                    var supplier = rsSuppliers.FirstOrDefault(t => t.SupplierName.ToUpper().Equals(supplierName));
                    if (supplier == null)
                    {
                        errorRemarks += L("NotExist_f", L("Supplier"), supplierName);
                        //throw new UserFriendlyException(L("NotExist_f", L("Supplier"), supplierName));
                    }

                    var materialName = row["Material"]?.ToString() ?? "";
                    materialName = materialName.Trim();
                    var material = rsMaterials.FirstOrDefault(t => t.MaterialName.ToUpper().Equals(materialName.ToUpper()));
                    if (material == null)
                    {
                        errorRemarks += L("NotExist_f", L("Material"), materialName);
                        //throw new UserFriendlyException(L("NotExist_f", L("Material"), materialName));
                    }

                    var unitName = row["Unit"]?.ToString() ?? "";
                    var unit = rsUnits.FirstOrDefault(t => t.Name.ToUpper().Equals(unitName.ToUpper()));
                    if (unit == null)
                    {
                        errorRemarks += L("NotExist_f", L("Unit"), unitName);
                        //throw new UserFriendlyException(L("NotExist_f", L("Unit"), unitName));
                    }

                    var priceInString = row["Price"]?.ToString() ?? "0";
                    decimal price = 0;
                    if (decimal.TryParse(priceInString, out price))
                        price = decimal.Parse(priceInString);

                    var moqInString = row["Minimum Order Qty"]?.ToString() ?? "0";
                    decimal minimumorderqty = 0;
                    if (decimal.TryParse(moqInString, out minimumorderqty))
                        minimumorderqty = decimal.Parse(moqInString);

                    var maxorderqtyInString = row["Maximum Order Qty"]?.ToString() ?? "0";
                    decimal maxorderQty = 0;
                    if (decimal.TryParse(maxorderqtyInString, out maxorderQty))
                        maxorderQty = decimal.Parse(maxorderqtyInString);

                    var materialAliasName = row["Material Alias Name"]?.ToString() ?? "";

                    int? appendedId = null;
                    if (errorRemarks.Length == 0)
                    {
                        List<int> locationIds = new List<int>();
                        string importStatus = "";
                        var locationsList = row["Locations"]?.ToString() ?? "";
                        if (locationsList.IsNullOrEmpty() || locationsList.IsNullOrWhiteSpace())
                        {
                            int? existId = null;
                            var existAlready =
                                await
                                    _supplierMaterial.FirstOrDefaultAsync(
                                        t => t.SupplierRefId == supplier.Id && t.MaterialRefId == material.Id);
                            if (existAlready != null)
                            {
                                existId = existAlready.Id;
                                existAlready.MaterialPrice = price;
                                existAlready.MinimumOrderQuantity = minimumorderqty;
                                existAlready.MaximumOrderQuantity = maxorderQty;
                                existAlready.UnitRefId = unit.Id;
                                existAlready.LastQuoteDate = DateTime.Now;
                                existAlready.YieldPercentage = null;
                                existAlready.SupplierMaterialAliasName = materialAliasName;
                                await _supplierMaterial.UpdateAsync(existAlready);
                                appendedId = existAlready.Id;
                            }
                            else
                            {
                                var supplierMaterialDto = new SupplierMaterial
                                {
                                    SupplierRefId = supplier.Id,
                                    MaterialRefId = material.Id,
                                    MaterialPrice = price,
                                    MinimumOrderQuantity = minimumorderqty,
                                    MaximumOrderQuantity = maxorderQty,
                                    UnitRefId = unit.Id,
                                    LastQuoteDate = DateTime.Now,
                                    YieldPercentage = null,
                                    SupplierMaterialAliasName = materialAliasName
                                };
                                await _supplierMaterial.InsertOrUpdateAndGetIdAsync(supplierMaterialDto);
                                appendedId = existAlready.Id;
                            }
                        }
                        else
                        {
                            var arrlocations = locationsList.Split(',');
                            foreach (var currentLocaiton in arrlocations)
                            {
                                var locGiven = rsLocations.FirstOrDefault(t => t.Code.Equals(currentLocaiton));
                                if (locGiven == null)
                                {
                                    locGiven = rsLocations.FirstOrDefault(t => t.Name.Equals(currentLocaiton));
                                }
                                if (locGiven == null)
                                {
                                    throw new UserFriendlyException(" " + currentLocaiton + " " + L("Location") + " " +
                                                                    L("NotExist"));
                                }

                                int? existId = null;
                                var existAlready =
                                    await
                                        _supplierMaterial.FirstOrDefaultAsync(
                                            t => t.SupplierRefId == supplier.Id && t.MaterialRefId == material.Id && t.LocationRefId == locGiven.Id);

                                if (existAlready != null)
                                {
                                    existId = existAlready.Id;
                                    existAlready.MaterialPrice = price;
                                    existAlready.MinimumOrderQuantity = minimumorderqty;
                                    existAlready.MaximumOrderQuantity = maxorderQty;
                                    existAlready.UnitRefId = unit.Id;
                                    existAlready.LastQuoteDate = DateTime.Now;
                                    existAlready.YieldPercentage = null;
                                    existAlready.LocationRefId = locGiven.Id;
                                    existAlready.SupplierMaterialAliasName = materialAliasName;
                                    await _supplierMaterial.UpdateAsync(existAlready);
                                    appendedId = existAlready.Id;
                                }
                                else
                                {
                                    var supplierMaterialDto = new SupplierMaterial
                                    {
                                        SupplierRefId = supplier.Id,
                                        MaterialRefId = material.Id,
                                        MaterialPrice = price,
                                        MinimumOrderQuantity = minimumorderqty,
                                        MaximumOrderQuantity = maxorderQty,
                                        UnitRefId = unit.Id,
                                        LastQuoteDate = DateTime.Now,
                                        YieldPercentage = null,
                                        LocationRefId = locGiven.Id,
                                        SupplierMaterialAliasName = materialAliasName
                                    };
                                    await _supplierMaterial.InsertOrUpdateAndGetIdAsync(supplierMaterialDto);
                                    appendedId = existAlready.Id;
                                }
                            }
                        }
                    }

                    var refUnitName = row["Conversion Unit"]?.ToString() ?? "";
                    if (refUnitName.Length > 0)
                    {
                        var refUnit = rsUnits.FirstOrDefault(t => t.Name.ToUpper().Equals(refUnitName.ToUpper()));
                        if (refUnit != null)
                        {
                            var refUnitConversion = row["Conversion Factor"]?.ToString() ?? "";
                            if (refUnitConversion.Length > 0)
                            {
                                decimal decUnitConversionFactor = 0.0m;
                                if (decimal.TryParse(refUnitConversion, out decUnitConversionFactor))
                                    decUnitConversionFactor = decimal.Parse(refUnitConversion);
                                if (decUnitConversionFactor > 0)
                                {
                                    UnitConversionEditDto unitConversionEditDto = new UnitConversionEditDto
                                    {
                                        BaseUnitId = unit.Id,
                                        RefUnitId = refUnit.Id,
                                        Conversion = decUnitConversionFactor,
                                        DecimalPlaceRounding = 14,
                                    };

                                    CreateOrUpdateUnitConversionInput inputUcDto = new CreateOrUpdateUnitConversionInput
                                    {
                                        SupplierRefId = supplier.Id,
                                        UnitConversion = unitConversionEditDto,
                                        RecursiveFlag = false
                                    };

                                    try
                                    {
                                        await _unitConversionAppService.CreateOrUpdateUnitConversion(inputUcDto);
                                    }
                                    catch (Exception ex)
                                    {
                                        errorRemarks += System.Environment.NewLine + ex.Message + (ex.InnerException == null ? "" : ex.InnerException.Message);
                                        throw;
                                    }
                                }
                            }
                        }
                    }

                    importDetailDataDto.AppendedId = appendedId;
                    importDetailDataDto.ErrorRemarks = errorRemarks;
                    importDataResult.ImportDetailDataDtos.Add(importDetailDataDto);
                }

                return Json(new MvcAjaxResponse(importDataResult));
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(L("ErrRow", currentRow) + ex.Message)));
            }
        }

        public async Task<FileResult> ImportSupplierTemplate()
        {
            return File(Server.MapPath("~/Common/Import/Suppliers.xlsx"),
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportSupplier()
        {
            var currentRow = 1;
            var currentSupplier = "";
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();

                //SupplierName    Address1 Address2    Address3 City    State Country ZipCode PhoneNumber Fax Website Email

                foreach (var row in allList)
                {
                    currentRow++;
                    if (row["SupplierCode"].ToString().Length == 0)
                    {
                        continue;
                    }

                    if (row["SupplierCode"].ToString().Length == 0 && row["SupplierName"].ToString().Length == 0)
                    {
                        continue;
                    }

                    var supplierCode = row["SupplierCode"]?.ToString() ?? "";
                    if (supplierCode.Length == 0)
                    {
                        throw new UserFriendlyException(L("ErrRow", currentRow) + L("Supplier") + " " + L("Code"));
                    }

                    supplierCode = supplierCode.ToUpper().Trim();

                    var supplierName = row["SupplierName"]?.ToString() ?? "";
                    supplierName = supplierName.Trim();
                    if (supplierName.Length == 0)
                    {
                        throw new UserFriendlyException(L("ErrRow", currentRow) + L("Supplier"));
                    }

                    supplierName = supplierName.ToUpper();
                    currentSupplier = supplierName;
                    var taxRegnNumber = row["TaxRegnNumber"]?.ToString() ?? "";
                    if (taxRegnNumber == "0")
                        taxRegnNumber = "";
                    var defaultCreditDays = row["DefaultCreditDays"]?.ToString() ?? "0";
                    var creditDays = 0;
                    if (int.TryParse(defaultCreditDays, out creditDays))
                        creditDays = int.Parse(defaultCreditDays);

                    var loadOnlyLinkedMaterialsString = row["Load Supplier Materials Only"]?.ToString() ?? "NO";
                    bool loadOnlyLinkedMaterials = false;
                    if (loadOnlyLinkedMaterialsString.ToUpper().Equals("TRUE") || loadOnlyLinkedMaterialsString.ToUpper().Equals("YES"))
                        loadOnlyLinkedMaterials = true;
                    int? existId = null;
                    var alreadyExist = await _supplierRepo.FirstOrDefaultAsync(t => t.SupplierCode.ToUpper().Equals(supplierCode.ToUpper()));
                    if (alreadyExist != null)
                        existId = alreadyExist.Id;

                    var invoicePayMode = (int) InvoicePayModeEnum.Cash;
                    var invoicePayModeString = row["Default Pay Mode"]?.ToString() ?? "";
                    if (invoicePayModeString.ToUpper().Equals("CREDIT"))
                        invoicePayMode = (int)InvoicePayModeEnum.Credit;

                    var supplier = new SupplierEditDto
                    {
                        Id = existId,
                        SupplierCode = supplierCode,
                        SupplierName = supplierName,
                        Address1 = row["Address1"]?.ToString() ?? "",
                        Address2 = row["Address2"]?.ToString() ?? "",
                        Address3 = row["Address3"]?.ToString() ?? "",
                        City = row["City"]?.ToString() ?? "",
                        State = row["State"]?.ToString() ?? "",
                        Country = row["Country"]?.ToString() ?? "",
                        ZipCode = row["ZipCode"]?.ToString() ?? "",
                        PhoneNumber1 = row["PhoneNumber"]?.ToString() ?? "",
                        DefaultCreditDays = creditDays,
                        Website = row["Website"]?.ToString() ?? "",
                        Email = row["Email"]?.ToString() ?? "",
                        FaxNumber = row["FaxNumber"]?.ToString() ?? "",
                        TaxRegistrationNumber = taxRegnNumber,
                        LoadOnlyLinkedMaterials = loadOnlyLinkedMaterials,
                        DefaultPayModeEnumRefId = invoicePayMode
                    };

                    var input = new CreateOrUpdateSupplierInput
                    {
                        Supplier = supplier
                    };

                    if (input != null)
                    {
                        await _supplierAppService.CreateOrUpdateSupplier(input);
                    }
                }

                return Json(new MvcAjaxResponse());
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(L("ErrRow", currentRow) + "Supplier : " + currentSupplier + " " + ex.Message)));
            }
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportStockAdjustment()
        {
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();

                var dtos = new List<MaterialAdjustmentStockDto>();

                var rownumber = 1;

                foreach (var row in allList)
                {
                    string errMessage;
                    rownumber = (rownumber + 1);

                    var matid = row["Code"].ToString();
                    if (matid == null || matid.Length == 0)
                    {
                        errMessage = string.Concat(L("Row"), rownumber.ToString(), L("MaterialIdNoExists"));
                        throw new UserFriendlyException(errMessage);
                    }

                    var matname = row["Name"].ToString();
                    if (matname == null || matname.Length == 0)
                    {
                        errMessage = string.Concat(L("Row"), rownumber.ToString(), L("MaterialNameNoExists", matname));
                        throw new UserFriendlyException(errMessage);
                    }

                    var closingstock = row["ClosingStock"].ToString(); // ds.Tables[0].Rows[i][2].ToString();
                    if (closingstock == null || closingstock.Length == 0)
                    {
                        errMessage = string.Concat(L("Row"), rownumber.ToString(), matname, " ",
                            L("ClosingStockNoExists"));
                        throw new UserFriendlyException(errMessage);
                    }

                    var currentstock = row["OnHand"].ToString(); // ds.Tables[0].Rows[i][3].ToString();
                    if (currentstock == null || currentstock.Length == 0)
                    {
                        errMessage = string.Concat(L("Row"), rownumber.ToString(), matname, " ",
                            L("CurrentStockNoExists"));
                        throw new UserFriendlyException(errMessage);
                    }

                    var currentStock = Convert.ToDecimal(row["ClosingStock"].ToString());
                    var onHand = Convert.ToDecimal(row["OnHand"].ToString());
                    var diffStockvalue = onHand - currentStock;
                    string diffStatus;
                    if (diffStockvalue < 0)
                        diffStatus = "Shortage";
                    else if (diffStockvalue == 0)
                        diffStatus = "Equal";
                    else
                        diffStatus = "Excess";

                    var stk = new MaterialAdjustmentStockDto
                    {
                        MaterialRefId = Convert.ToInt16(row["Code"]),
                        MaterialName = row["Name"].ToString(),
                        ClBalance = Convert.ToDecimal(row["ClosingStock"].ToString()),
                        CurrentStock = Convert.ToDecimal(row["OnHand"]),
                        DifferenceQuantity = Math.Abs(diffStockvalue),
                        AdjustmentStatus = diffStatus
                    };

                    dtos.Add(stk);
                }

                return Json(new MvcAjaxResponse(dtos));
                //return dtos;
            }
            catch (Exception ex)
            {
                //throw new UserFriendlyException(ex.Message + ex.InnerException);
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

      

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportStockAdjustmentBasedOnStockTakenReport()
        {
            var rownumber = 1;
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }

                var givenDate = Request.Params["closingStockDate"];
                DateTime closingStockDate;

                var rsMaterialList = await _materialRepo.GetAllListAsync();
                var rsMaterialGroupCategory = await _materialGroupCategoryRepo.GetAllListAsync();
                var rsUnit = await _unitRepo.GetAllListAsync();

                if (DateTime.TryParse(givenDate, out closingStockDate))
                    closingStockDate = DateTime.Parse(givenDate);
                else
                    throw new UserFriendlyException("DateErr");

                var location = Request.Params["locationRefId"];
                int locationRefId;

                if (int.TryParse(location, out locationRefId))
                    locationRefId = int.Parse(location);
                else
                    throw new UserFriendlyException("LocationErr");

                var forceAdjustmentFlagstring = Request.Params["forceAdjustmentFlag"];
                bool forceAdjustmentFlag;
                if (bool.TryParse(forceAdjustmentFlagstring, out forceAdjustmentFlag))
                    forceAdjustmentFlag = bool.Parse(forceAdjustmentFlagstring);
                else
                    throw new UserFriendlyException(L("ForceAdjustmentErr"));

                bool stockEntryOnly;
                var stockEntryOnlystring = Request.Params["stockEntryOnlyFlag"];
                if (stockEntryOnlystring == "" || stockEntryOnlystring == null)
                    stockEntryOnly = false;
                else
                {
                    if (bool.TryParse(stockEntryOnlystring, out stockEntryOnly))
                        forceAdjustmentFlag = bool.Parse(forceAdjustmentFlagstring);
                    else
                        throw new UserFriendlyException(L("ForceAdjustmentErr"));
                }

                var outputClosingStock = new GetStockSummaryDtoOutput();
                var StockSummary = new List<MaterialLedgerDto>();
                if (stockEntryOnly == false)
                {
                    outputClosingStock =
                        await _housereportAppService.GetStockSummary(new GetHouseReportInput
                        {
                            StartDate = closingStockDate,
                            EndDate = closingStockDate,
                            LocationRefId = locationRefId,
                            ExcludeRateView = true
                        });

                    StockSummary = outputClosingStock.StockSummary;
                }
                // *** In future if any client request to account sales portion for forceclose also then we need to uncomment this

                //List<MaterialMenuMappingWithWipeOut> liveStockDetails = new List<MaterialMenuMappingWithWipeOut>();
                //if (forceAdjustmentFlag)
                //{
                //	liveStockDetails = await _materialAppService.GetLiveStockSales(new GetMaterialLocationWiseStockInput
                //	{
                //		LocationRefId = locationRefId,
                //		EndDate = closingStockDate,
                //		LiveStock = true
                //	});
                //}

                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();

                var dtos = new List<MaterialAdjustmentStockDto>();

                var rsMaterials = await _materialRepo.GetAllListAsync();

                var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

                var noOfUnits = 1;
                foreach (var row in allList)
                {
                    string errMessage;
                    rownumber = (rownumber + 1);

                    if (rownumber == 2)
                    {
                        try
                        {
                            noOfUnits = (row.ItemArray.Count() - 3) / 2;
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }

                    var unitWiseQuantityList = new List<UnitWiseQuantity>();
                    var valueExistFlag = false;
                    for (var uomIndex = 0; uomIndex < noOfUnits; uomIndex++)
                    {
                        _currentMaterialStatus = "OnHand-" + uomIndex;
                        var stronHand = row[2 + uomIndex * 2 + 1].ToString();
                        // ds.Tables[0].Rows[i][3].ToString();
                        if (stronHand == null || stronHand.Length == 0)
                        {
                            continue;
                        }
                        valueExistFlag = true;
                        var strUom = row[2 + uomIndex * 2 + 2].ToString();
                        var curunit = rsUnit.FirstOrDefault(t => t.Name.ToUpper().Equals(strUom.ToUpper()));
                        if (curunit == null)
                        {
                            throw new UserFriendlyException(L("Unit") + " " + L("Error") + " " + strUom);
                        }
                        decimal uomQuantityOnHand = 0;
                        if (decimal.TryParse(stronHand, out uomQuantityOnHand))
                        {
                            uomQuantityOnHand = decimal.Parse(stronHand);
                        }
                        else
                        {
                            throw new UserFriendlyException(L("Quantity") + " " + L("Error") + " " + stronHand);
                        }
                        uomQuantityOnHand = Math.Round(uomQuantityOnHand, 6, MidpointRounding.AwayFromZero);
                        if (uomQuantityOnHand < 0)
                        {
                            errMessage = string.Concat(L("OnHandShouldNotBeNegative"));
                            throw new UserFriendlyException(errMessage);
                        }
                        var dtoUnitQty = new UnitWiseQuantity
                        {
                            UnitRefId = curunit.Id,
                            UnitRefName = curunit.Name,
                            OnHand = uomQuantityOnHand
                        };
                        unitWiseQuantityList.Add(dtoUnitQty);
                    }

                    if (valueExistFlag == false)
                        continue;

                    _currentMaterialStatus = "Code";
                    var matid = row[L("Code")].ToString();

                    if (matid == null || matid.Length == 0)
                    {
                        continue;
                    }
                    int materialid = Convert.ToInt16(row[L("Code")]);

                    if (rownumber == 93)
                    {
                        _currentMaterialStatus = "Name";
                    }

                    _currentMaterialStatus = "Name";
                    var matname = row["Name"].ToString();
                    if (matname == null || matname.Length == 0)
                    {
                        errMessage = L("MATERIALNAMEISWRONG", matname);
                        throw new UserFriendlyException(errMessage);
                    }

                    var mat = rsMaterials.FirstOrDefault(t => t.Id == materialid);
                    if (mat == null)
                    {
                        errMessage = L("MATERIALNAMEISWRONG", matname);
                        errMessage = string.Concat(errMessage);
                        throw new UserFriendlyException(errMessage);
                    }

                    decimal closingstock = 0;
                    if (stockEntryOnly)
                    {
                        closingstock = 0;
                    }
                    else
                    {
                        _currentMaterialStatus = "Verify Stock From Stock Summary";
                        var stk = StockSummary.FirstOrDefault(t => t.MaterialRefId == materialid);
                        if (stk == null)
                        {
                            errMessage = string.Concat(matname + " " + L("NoRecordsFoundInStockSummary"));
                            throw new UserFriendlyException(errMessage);
                        }
                        closingstock = stk.ClBalance;
                    }

                    // *** In future if any client request to account sales portion for forceclose also then we need to uncomment this

                    //if (forceAdjustmentFlag)
                    //{
                    //	var salesDeduction = liveStockDetails.FirstOrDefault(t => t.MaterialRefId == materialid);
                    //	if (salesDeduction != null)
                    //		closingstock = closingstock - salesDeduction.PortionQty;
                    //}

                    _currentMaterialStatus = L("MaterialName") + " : " + matname + " ";

                    var currentStock = closingstock;

                    decimal onHand = 0;
                    foreach (var lst in unitWiseQuantityList)
                    {
                        if (mat.DefaultUnitId == lst.UnitRefId)
                        {
                            lst.Conversion = 1;
                            lst.ConvertedQuantity = lst.OnHand;
                            onHand = onHand + lst.OnHand;
                        }
                        else
                        {
                            var unitConversion =
                                rsUc.FirstOrDefault(
                                    t => t.BaseUnitId == lst.UnitRefId && t.RefUnitId == mat.DefaultUnitId && t.MaterialRefId == mat.Id);
                            if (unitConversion == null)
                            {
                                unitConversion =
                                rsUc.FirstOrDefault(
                                    t => t.BaseUnitId == lst.UnitRefId && t.RefUnitId == mat.DefaultUnitId);
                            }
                            if (unitConversion == null)
                                throw new UserFriendlyException(L("UnitConversion") + " " + L("NotExist") + " - " +
                                                                lst.UnitRefName);
                            lst.Conversion = unitConversion.Conversion;
                            lst.ConvertedQuantity = Math.Round(unitConversion.Conversion * lst.OnHand, 6, MidpointRounding.AwayFromZero);
                            onHand = onHand + Math.Round(unitConversion.Conversion * lst.OnHand, 6, MidpointRounding.AwayFromZero);
                        }
                    }
                    onHand = Math.Round(onHand, 6, MidpointRounding.AwayFromZero);

                    if (onHand < 0)
                    {
                        errMessage = string.Concat(L("OnHandShouldNotBeNegative"));
                        throw new UserFriendlyException(errMessage);
                    }

                    if (currentStock == onHand && currentStock == 0)
                        continue;

                    var diffStockvalue = onHand - currentStock;
                    string diffStatus;
                    if (diffStockvalue < 0)
                        diffStatus = L("Shortage");
                    else if (diffStockvalue == 0)
                        diffStatus = L("Equal");
                    else
                        diffStatus = L("Excess");

                    int materialRefId = Convert.ToInt16(row[L("Code")]);
                    var material = rsMaterialList.FirstOrDefault(t => t.Id == materialRefId);

                    if (material == null)
                    {
                        throw new UserFriendlyException(L("Material") + " " + L("Error") + " " + matname);
                    }

                    var unit = rsUnit.FirstOrDefault(t => t.Id == mat.DefaultUnitId);
                    if (unit == null)
                    {
                        throw new UserFriendlyException(L("Unit") + " " + L("Error") + " " + matname);
                    }

                    var materialGroupCategory = rsMaterialGroupCategory.FirstOrDefault(t => t.Id == material.MaterialGroupCategoryRefId);

                    var stock = new MaterialAdjustmentStockDto
                    {
                        MaterialRefId = materialRefId,
                        MaterialName = row[L("Name")].ToString(),
                        MaterialPetName = material.MaterialPetName,
                        MaterialGroupCategoryRefId = material.MaterialGroupCategoryRefId,
                        MaterialGroupCategoryRefName = materialGroupCategory.MaterialGroupCategoryName,
                        ClBalance = closingstock, //Convert.ToDecimal(row["ClosingStock"].ToString()),
                        CurrentStock = onHand, // Convert.ToDecimal(row[L("OnHand")]),
                        DifferenceQuantity = Math.Abs(diffStockvalue),
                        AdjustmentStatus = diffStatus,
                        DefaultUnitId = mat.DefaultUnitId,
                        DefaultUnitName = unit.Name,
                        UnitRefId = mat.DefaultUnitId,
                        UnitRefName = unit.Name,
                        UnitWiseQuantityList = unitWiseQuantityList
                    };

                    dtos.Add(stock);
                }

                return Json(new MvcAjaxResponse(dtos));
                //return dtos;
            }
            catch (Exception ex)
            {
                //throw new UserFriendlyException(ex.Message + ex.InnerException);
                return
                    Json(
                        new MvcAjaxResponse(
                            new ErrorInfo(L("Row") + " : " + rownumber + " " + _currentMaterialStatus + " " + ex.Message +
                                          " " + ex.InnerException)));
            }
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportDirectTransfer()
        {
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }

                var location = Request.Params["requestLocationRefId"];
                int requestLocationRefId;

                if (int.TryParse(location, out requestLocationRefId))
                    requestLocationRefId = int.Parse(location);
                else
                    throw new UserFriendlyException("LocationErr");

                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData(false);

                var outputDtos = new List<DirectTransferImportDto>();

                var rownumber = 0;

                var materialList = _materialRepo.GetAll();
                var locationList = _locationRepo.GetAll().ToList();
                var unitList = _unitRepo.GetAll().ToList();

                var sno = 1;

                foreach (var row in allList)
                {
                    rownumber = rownumber + 1;
                    if (rownumber == 1)
                    {
                        //if (!row[0].ToString().Equals(L("LocationId")))
                        //{
                        //    throw new UserFriendlyException(L("FirstColumnHeaderShouldBe", L("RequestLocation")));
                        //}
                        //if (!row[1].ToString().Equals(L("RequestLocation")))
                        //{
                        //    throw new UserFriendlyException(L("FirstColumnHeaderShouldBe", L("RequestLocation")));
                        //}
                        //if (!row[2].ToString().Equals(L("MaterialId")))
                        //{
                        //    throw new UserFriendlyException(L("SecondColumnHeaderShouldBe", L("MaterialName")));
                        //}
                        //if (!row[3].ToString().Equals(L("MaterialName")))
                        //{
                        //    throw new UserFriendlyException(L("SecondColumnHeaderShouldBe", L("MaterialName")));
                        //}
                        //if (!row[4].ToString().Equals(L("RequestQty")))
                        //{
                        //    throw new UserFriendlyException(L("ThirdColumnHeaderShouldBe", L("RequestQty")));
                        //}
                        continue;
                    }

                    string errMessage;
                    _currentMaterialStatus = "";

                    var locationId = row[0].ToString();
                    var locationName = row[1].ToString().ToUpper();
                    var materialCode = row[3].ToString();
                    var materialName = row[4].ToString().ToUpper();
                    materialName = materialName.Trim();
                    var requestQtyInString = row[6].ToString();

                    if (materialName.Trim().Length == 0 && locationName.Trim().Length == 0 &&
                        requestQtyInString.Trim().Length == 0)
                    {
                        continue;
                    }

                    if (requestQtyInString.Length == 0 || requestQtyInString == null)
                    {
                        continue;
                    }

                    var material =
                        materialList.FirstOrDefault(
                            t =>
                                t.MaterialName.ToUpper().Equals(materialName) ||
                                t.MaterialPetName.ToUpper().Equals(materialName));

                    if (material == null)
                    {
                        errMessage = string.Concat(L("Row"), " ", rownumber.ToString(), " ",
                            L("MATERIALNAMEISWRONG", materialName));
                        throw new UserFriendlyException(errMessage);
                    }

                    var materialRefId = material.Id;
                    var materialRefName = material.MaterialName;

                    var unit = unitList.FirstOrDefault(t => t.Id == material.DefaultUnitId);
                    if (unit == null)
                    {
                        errMessage = string.Concat(L("Row"), " ", rownumber.ToString(), " ", L("Unit"));
                        throw new UserFriendlyException(errMessage);
                    }
                    var uom = unit.Name;
                    var unitRefId = material.DefaultUnitId;

                    var materialTypeId = material.MaterialTypeId;
                    var materialTypeName = "";

                    if (materialTypeId == (int)MaterialType.RAW)
                        materialTypeName = L("RAW");
                    else if (materialTypeId == (int)MaterialType.SEMI)
                        materialTypeName = L("SEMI");

                    var loc =
                        locationList.FirstOrDefault(
                            t => t.Name.ToUpper().Equals(locationName) || t.Code.ToUpper().Equals(locationName));
                    if (loc == null)
                    {
                        errMessage = string.Concat(L("Row"), " ", rownumber.ToString(), " ",
                            L("LocationNameDoesNotExists", locationName));
                        throw new UserFriendlyException(errMessage);
                    }
                    var locationRefId = loc.Id;
                    var locationRefName = loc.Name;

                    if (requestQtyInString.Length == 0 || requestQtyInString == null)
                    {
                        continue;
                    }

                    decimal requestQty;
                    if (decimal.TryParse(requestQtyInString, out requestQty))
                        requestQty = decimal.Parse(requestQtyInString);
                    else
                    {
                        errMessage = string.Concat(L("Row"), " ", rownumber.ToString(), " ",
                            L("RequestQtyError", requestQtyInString));
                        throw new UserFriendlyException(errMessage);
                    }

                    if (locationRefId == requestLocationRefId)
                    {
                        errMessage = string.Concat(L("Row"), " ", rownumber.ToString(), " ",
                            L("TransferWithLocationNotAllowed"));
                        throw new UserFriendlyException(errMessage);
                    }

                    var existAlready =
                        outputDtos.Where(t => t.MaterialRefId == materialRefId && t.LocationRefId == locationRefId)
                            .Count();

                    if (existAlready > 0)
                    {
                        errMessage = string.Concat(L("Row"), " ", rownumber.ToString(), " ",
                            L("DuplicateEntry", locationRefName, materialRefName));
                        throw new UserFriendlyException(errMessage);
                    }

                    var dto = new DirectTransferImportDto();

                    dto.RequestLocationRefId = requestLocationRefId;
                    dto.LocationRefId = locationRefId;
                    dto.LocationRefName = locationRefName;
                    dto.MaterialRefId = materialRefId;
                    dto.MaterialRefName = materialRefName;
                    dto.Barcode = material.Barcode == null ? material.MaterialName : material.Barcode;
                    dto.RequestQty = requestQty;
                    dto.Sno = sno;
                    sno++;
                    dto.RequestLocationName = loc.Name;
                    dto.Currenteditstatus = false;
                    dto.MaterialTypeName = materialTypeName;
                    dto.UnitRefId = unitRefId;
                    dto.Uom = uom;

                    outputDtos.Add(dto);
                }

                outputDtos = outputDtos.OrderBy(t => t.MaterialRefId).ToList();

                return Json(new MvcAjaxResponse(outputDtos));
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(_currentMaterialStatus + ex.Message)));
            }
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportCards()
        {
            var currentRow = 1;
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var stringCardTypeRefId = Request.Params["cardTypeRefId"];
                var cardTypeRefId = int.Parse(stringCardTypeRefId);
                var cardTypeDto = await _swipecardTypeRepo.FirstOrDefaultAsync(t => t.Id == cardTypeRefId);
                if (cardTypeDto == null)
                {
                    throw new UserFriendlyException(L("CardTypeError"));
                }

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetDataAsSet();

                //CardNumber
                List<string> allCards = new List<string>();
                foreach (var row in allList)
                {
                    currentRow++;

                    if (row["CardNumber"].ToString().Length == 0)
                    {
                        continue;
                    }

                    var cardNumber = row["CardNumber"]?.ToString() ?? "";
                    if (cardNumber.Length == 0)
                    {
                        throw new UserFriendlyException(L("ErrRow", currentRow) + L("CardNumber"));
                    }
                    allCards.Add(cardNumber);
                }

                var duplicateKeys = allCards.GroupBy(x => x)
                    .Where(group => group.Count() > 1)
                    .Select(group => group.Key);

                if (duplicateKeys.Any())
                {
                    var myCards = String.Join(",", duplicateKeys.ToArray());
                    return Json(new MvcAjaxResponse(new ErrorInfo(L("DuplicateCards : ") + myCards)));
                }

                foreach (var row in allCards)
                {
                    var cardNumber = row.Trim();
                    var activeStatus = false;
                    if (!cardTypeDto.AutoActivateRegExpression.IsNullOrEmpty() &&
                        cardTypeDto.AutoActivateRegExpression.Length > 0)
                    {
                        var pattern = cardTypeDto.AutoActivateRegExpression.Replace(@"\\", @"\");
                        if (Regex.IsMatch(cardNumber, pattern))
                        {
                            activeStatus = true;
                        }
                    }

                    var swipeCard = new SwipeCardEditDto
                    {
                        CardTypeRefId = cardTypeDto.Id,
                        CardNumber = cardNumber,
                        DepositRequired = cardTypeDto.DepositRequired,
                        DepositAmount = cardTypeDto.DepositAmount,
                        ActiveStatus = activeStatus,
                        Balance = 0M,
                        CanIssueWithOutMember = cardTypeDto.CanIssueWithOutMember,
                        ExpiryDaysFromIssue = cardTypeDto.ExpiryDaysFromIssue,
                        RefundAllowed = cardTypeDto.RefundAllowed,
                        IsEmployeeCard = cardTypeDto.IsEmployeeCard,
                        IsAutoRechargeCard = cardTypeDto.IsAutoRechargeCard,
                        DailyLimit = cardTypeDto.DailyLimit,
                        WeeklyLimit = cardTypeDto.WeeklyLimit,
                        MonthlyLimit = cardTypeDto.MonthlyLimit,
                        YearlyLimit = cardTypeDto.YearlyLimit,
                        CanExcludeTaxAndCharges = cardTypeDto.CanExcludeTaxAndCharges
                    };

                    var input = new CreateOrUpdateSwipeCardInput
                    {
                        SwipeCard = swipeCard,
                        Import = true
                    };

                    await _swipecardAppService.CreateOrUpdateSwipeCard(input);
                }

                return Json(new MvcAjaxResponse());
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo($"{ex.Message} {L("Row")} {currentRow}")));
            }
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportIssue()
        {
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }

                var location = Request.Params["locationRefId"];
                int locationRefId;

                if (int.TryParse(location, out locationRefId))
                    locationRefId = int.Parse(location);
                else
                    throw new UserFriendlyException("LocationErr");

                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData(false);

                var outputDtos = new List<ImportIssueDto>();
                var rownumber = 0;

                var materialList = _materialRepo.GetAll();
                var locationList = _locationRepo.GetAll().ToList();
                var unitList = _unitRepo.GetAll().ToList();

                var rsMaterialStock =
                    _materiallocationwisestockRepo.GetAll().Where(t => t.LocationRefId == locationRefId);

                var sno = 1;

                foreach (var row in allList)
                {
                    rownumber = rownumber + 1;
                    if (rownumber == 1)
                    {
                        continue;
                    }

                    string errMessage;
                    _currentMaterialStatus = "";

                    var locationId = row[0].ToString();
                    var locationName = row[1].ToString().ToUpper();
                    var materialCode = row[3].ToString();
                    var materialName = row[4].ToString().ToUpper();
                    materialName = materialName.Trim();
                    var requestQtyInString = row[6].ToString();

                    if (materialName.Trim().Length == 0 && locationName.Trim().Length == 0 &&
                        requestQtyInString.Trim().Length == 0)
                    {
                        continue;
                    }

                    if (requestQtyInString.Length == 0 || requestQtyInString == null)
                    {
                        continue;
                    }

                    var material =
                        materialList.FirstOrDefault(
                            t =>
                                t.MaterialName.ToUpper().Equals(materialName) ||
                                t.MaterialPetName.ToUpper().Equals(materialName));

                    if (material == null)
                    {
                        errMessage = string.Concat(L("Row"), " ", rownumber.ToString(), " ",
                            L("MATERIALNAMEISWRONG", materialName));
                        throw new UserFriendlyException(errMessage);
                    }

                    var materialRefId = material.Id;
                    var materialRefName = material.MaterialName;

                    var unit = unitList.FirstOrDefault(t => t.Id == material.DefaultUnitId);
                    if (unit == null)
                    {
                        errMessage = string.Concat(L("Row"), " ", rownumber.ToString(), " ", L("Unit"));
                        throw new UserFriendlyException(errMessage);
                    }
                    var uom = unit.Name;
                    var unitRefId = material.DefaultUnitId;

                    var materialTypeId = material.MaterialTypeId;
                    var materialTypeName = "";

                    if (materialTypeId == (int)MaterialType.RAW)
                        materialTypeName = L("RAW");
                    else if (materialTypeId == (int)MaterialType.SEMI)
                        materialTypeName = L("SEMI");

                    var loc =
                        locationList.FirstOrDefault(
                            t => t.Name.ToUpper().Equals(locationName) || t.Code.ToUpper().Equals(locationName));
                    if (loc == null)
                    {
                        errMessage = string.Concat(L("Row"), " ", rownumber.ToString(), " ",
                            L("LocationNameDoesNotExists", locationName));
                        throw new UserFriendlyException(errMessage);
                    }
                    var locationRefName = loc.Name;

                    if (requestQtyInString.Length == 0 || requestQtyInString == null)
                    {
                        continue;
                    }

                    decimal requestQty;
                    if (decimal.TryParse(requestQtyInString, out requestQty))
                        requestQty = decimal.Parse(requestQtyInString);
                    else
                    {
                        errMessage = string.Concat(L("Row"), " ", rownumber.ToString(), " ",
                            L("RequestQtyError", requestQtyInString));
                        throw new UserFriendlyException(errMessage);
                    }

                    if (requestQty <= 0)
                        continue;

                    var existAlready =
                        outputDtos.Where(t => t.MaterialRefId == materialRefId).Count();

                    if (existAlready > 0)
                    {
                        errMessage = string.Concat(L("Row"), " ", rownumber.ToString(), " ",
                            L("DuplicateEntry", locationRefName, materialRefName));
                        throw new UserFriendlyException(errMessage);
                    }

                    var ml = rsMaterialStock.FirstOrDefault(t => t.MaterialRefId == materialRefId);
                    if (ml == null)
                    {
                        errMessage = string.Concat(L("Row"), " ", rownumber.ToString(), " ",
                            L("StockNotExist", locationRefName, materialRefName));
                        throw new UserFriendlyException(errMessage);
                    }

                    var dto = new ImportIssueDto();

                    dto.LocationRefId = locationRefId;
                    dto.LocationRefName = locationRefName;
                    dto.MaterialRefId = materialRefId;
                    dto.MaterialRefName = materialRefName;
                    dto.Barcode = material.Barcode == null ? material.MaterialName : material.Barcode;
                    dto.IssueQty = requestQty;
                    dto.Sno = sno;
                    sno++;
                    dto.MaterialTypeName = materialTypeName;
                    dto.UnitRefId = unitRefId;
                    dto.Uom = uom;
                    dto.DefaultUnitId = unitRefId;
                    dto.DefaultUnit = uom;
                    dto.OnHand = ml.CurrentInHand;
                    outputDtos.Add(dto);
                }

                outputDtos = outputDtos.OrderBy(t => t.MaterialRefId).ToList();

                return Json(new MvcAjaxResponse(outputDtos));
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(_currentMaterialStatus + ex.Message)));
            }
        }
        public async Task<FileResult> ImportPersonalInformationTemplate()
        {
            return File(Server.MapPath("~/Common/Import/EmployeeTemplate.xlsx"),
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }
        public async Task<FileResult> ImportSalesTargetTemplate()
        {
            return File(Server.MapPath("~/Common/Import/Sales_Target.xlsx"),
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportPersonalInformation()
        {
            int currentRow = 1;
            try
            {
                bool defaultFileFlag = false;

                IEnumerable<System.Data.DataRow> allList;

                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    //throw new UserFriendlyException(L("NoFile"));
                    defaultFileFlag = true;
                    var file = File(Server.MapPath("~/Common/Import/EmployeeTemplate.xlsx"),
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

                    //var data = new ExcelData(file.FileName, System.Web.HttpInputStream);

                    //allList = data.GetData();
                    allList = null;
                }
                else
                {
                    var file = Request.Files[0];

                    var data = new ExcelData(file.FileName, file.InputStream);

                    allList = data.GetData();
                }

                var dtos = new List<PersonalInformationEditDto>();

                var rsSkillsets = await _skillsetRepo.GetAllListAsync();
                var rsEmployee = await _personalinformationRepo.GetAllListAsync();
                var employeeList = rsEmployee.MapTo<List<PersonalInformationEditDto>>();

                foreach (var row in allList)
                {
                    currentRow++;
                    if (row["LOCATION"].ToString().Length == 0 && row["EMPLOYEENAME"].ToString().Length == 0)
                    {
                        continue;
                    }
                    processString = "Location";
                    string currentLocation = row["Location"].ToString().ToUpper();
                    if (currentLocation.Length == 0)
                    {
                        throw new UserFriendlyException(L("Location"));
                    }

                    int defaultLocationId;
                    int defaultCompanyId;

                    var location = await _locationRepo.FirstOrDefaultAsync(t => t.Name.ToUpper().Equals(currentLocation) || t.Code.ToUpper().Equals(currentLocation));
                    if (location == null)
                    {
                        throw new UserFriendlyException(" " + currentLocation + " " + L("Location") + " Not Exist");
                    }
                    else
                    {
                        defaultLocationId = location.Id;
                        defaultCompanyId = location.CompanyRefId;
                    }

                    processString = "BIOMETRICCODE";
                    string biometriccodeLong = null;
                    string bioMetricCodestring = row["BIOMETRICCODE"]?.ToString() ?? "";
                    if (bioMetricCodestring.Length > 0)
                    {
                        biometriccodeLong = bioMetricCodestring;
                    }

                    processString = "EMPLOYEE GIVEN NAME";
                    string employeeGivenName = row["GIVEN NAME"]?.ToString() ?? "";
                    if (employeeGivenName.Length == 0)
                    {
                        throw new UserFriendlyException(L("Employee Given Name"));
                    }

                    processString = "EMPLOYEE SUR NAME";
                    string employeeSurName = row["SUR NAME"]?.ToString() ?? "";
                    if (employeeGivenName.Length == 0)
                    {
                        throw new UserFriendlyException(L("Employee Sur Name"));
                    }

                    processString = "EMPLOYEENAME";
                    string employeeName = row["EMPLOYEENAME"]?.ToString() ?? "";
                    if (employeeName.Length == 0)
                    {
                        throw new UserFriendlyException(L("EmployeeName"));
                    }

                    processString = "EMPLOYEECODE";
                    string employeeCode = row["EMPLOYEECODE"]?.ToString() ?? "";
                    if (employeeCode.Length == 0)
                    {
                        throw new UserFriendlyException(L("EmployeeCode"));
                    }

                    employeeCode = employeeCode.ToUpper();

                    var existemployee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.EmployeeCode.Equals(employeeCode));
                    int? empRefId = null;
                    int? companyRefId = null;
                    int? locationRefId = null;
                    if (existemployee != null)
                    {
                        empRefId = existemployee.Id;
                        locationRefId = existemployee.LocationRefId;
                        companyRefId = existemployee.LocationRefId;
                    }

                    processString = "USERSERIALNUMBER";
                    string userserialNumberString = row["USERSERIALNUMBER"]?.ToString() ?? "";
                    decimal userserialNumber = 0;
                    if (userserialNumberString.Length == 0)
                    {
                        userserialNumber = _personalinformationRepo.GetAll().Max(t => t.UserSerialNumber) + 1;
                    }
                    else
                    {
                        userserialNumber = decimal.Parse(userserialNumberString);
                    }

                    processString = "NationalIdentificationNumber";
                    string nationalIdentificationNumber = row["NATIONAL IDENTIFICATION NUMBER"]?.ToString() ?? "";

                    processString = "Gender";
                    string genderString = row["Gender"]?.ToString() ?? "";
                    if (genderString.Length == 0)
                    {
                        throw new UserFriendlyException(L("EmployeeCode"));
                    }

                    if (genderString.Substring(0, 1).ToUpper().Equals("M"))
                        genderString = "Male";
                    else if (genderString.Substring(0, 1).ToUpper().Equals("F"))
                        genderString = "Female";

                    if (!(genderString.Equals("Male") || genderString.Equals("Female")))
                    {
                        throw new UserFriendlyException(L("Gender"));
                    }

                    processString = "DEFAULTSKILLSETCODE";
                    int defaultSkillSetRefId = 0;
                    string defaultSkillSetCode = row["DEFAULTSKILLSETCODE"]?.ToString() ?? "";
                    if (defaultSkillSetCode.Length == 0)
                    {
                        throw new UserFriendlyException(L("SkillSet"));
                    }

                    if (currentRow == 122)
                    {
                        //int I = 10;
                    }

                    defaultSkillSetCode = defaultSkillSetCode.Trim();

                    var skillExists = rsSkillsets.FirstOrDefault(t => t.SkillCode.Equals(defaultSkillSetCode));

                    if (skillExists == null)
                    {
                        var skill = rsSkillsets.FirstOrDefault(t => t.Description.Equals(defaultSkillSetCode));

                        if (skill == null)
                        {
                            throw new UserFriendlyException(L("SkillCodeDoesNotExist", defaultSkillSetCode));
                        }
                        else
                        {
                            defaultSkillSetCode = skill.SkillCode;
                            defaultSkillSetRefId = skill.Id;
                        }
                    }

                    #region SupervisorEmployeeRefId

                    processString = "Supervisor";
                    defaultSkillSetCode = skillExists.SkillCode;
                    defaultSkillSetRefId = skillExists.Id;

                    int? supervisorRefId = null;
                    string supervisor = row["Supervisor"]?.ToString() ?? "";
                    if (supervisor.Length == 0)
                    {
                        supervisorRefId = null;
                    }
                    else
                    {
                        supervisor = supervisor.Trim().ToUpper();

                        var svr = employeeList.FirstOrDefault(t => t.EmployeeCode == supervisor);
                        if (svr == null)
                        {
                            svr = employeeList.FirstOrDefault(t => t.EmployeeName.ToUpper() == supervisor.ToUpper());
                        }
                        if (svr == null)
                        {
                            supervisorRefId = null;
                        }
                        else
                        {
                            supervisorRefId = svr.Id;
                        }
                    }

                    #endregion SupervisorEmployeeRefId

                    #region ManagerEmployeeRefId

                    processString = "Manager";
                    int? managerRefId = null;
                    string manager = row["Manager"]?.ToString() ?? "";
                    if (manager.Length == 0)
                    {
                        managerRefId = null;
                    }
                    else
                    {
                        manager = manager.Trim().ToUpper();

                        var mgr = employeeList.FirstOrDefault(t => t.EmployeeCode == manager);
                        if (mgr == null)
                        {
                            mgr = employeeList.FirstOrDefault(t => t.EmployeeName.ToUpper() == manager.ToUpper());
                        }
                        if (mgr == null)
                        {
                            managerRefId = null;
                        }
                        else
                        {
                            managerRefId = mgr.Id;
                        }
                    }

                    #endregion ManagerEmployeeRefId

                    processString = "JOBTITLE";
                    string jobTitle = row["JOBTITLE"]?.ToString() ?? "";
                    if (jobTitle.Length == 0)
                    {
                        jobTitle = "";
                    }
                    var jobTitleRefId = 0;

                    var jt = await _jobTitleMasterRepo.FirstOrDefaultAsync(t => t.JobTitle.Equals(jobTitle));
                    if (jt == null)
                    {
                        throw new UserFriendlyException("Job Title : " + jobTitle + " not exists");
                    }
                    jobTitleRefId = jt.Id;

                    processString = "EMAIL";

                    string email = row["EMAIL"]?.ToString() ?? "";

                    processString = "USER NAME";

                    string userName = row["UserName"]?.ToString() ?? "";
                    userName = userName.Trim();

                    processString = "Staff Type";
                    string staffType = row["StaffType"]?.ToString() ?? "";
                    if (staffType == "" || (staffType.ToUpper() != "PERMANENT" && staffType.ToUpper() != "FREELANCER"))
                    {
                        throw new UserFriendlyException(L("StaffTypeWrong", staffType));
                    }

                    var phoneString = row["Phone Numbers"]?.ToString() ?? "";
                    string handphone1 = "";
                    string handphone2 = "";
                    if (phoneString != "")
                    {
                        if (phoneString.Contains('/'))
                        {
                            var phoneNumbers = phoneString.Split('/');
                            handphone1 = phoneNumbers[0];
                            handphone2 = phoneNumbers[1];
                        }
                        else
                        {
                            handphone1 = phoneString;
                        }
                    }

                    #region Date Related

                    processString = "DOB";
                    string dobstring = row["DOB"]?.ToString() ?? "";
                    if (dobstring.Length == 0)
                    {
                        throw new UserFriendlyException(L("DOB"));
                    }

                    DateTime dob = DateTime.Parse(dobstring);
                    TimeSpan diff = DateTime.Now.Subtract(dob);

                    if (Math.Abs(diff.TotalDays) < (14 * 365))
                    {
                        throw new UserFriendlyException(L("AgeIssue_LessThan_14"));
                    }

                    if (dob > DateTime.Now)
                    {
                        throw new UserFriendlyException(L("FutureDateErr"));
                    }

                    processString = "DATEHIRED";

                    string datehiredstring = row["DATE HIRED"]?.ToString() ?? "";
                    if (datehiredstring.Length == 0)
                    {
                        throw new UserFriendlyException(L("DateHired"));
                    }

                    DateTime datehired = DateTime.Parse(datehiredstring);
                    TimeSpan diffdate = DateTime.Now.Subtract(datehired);

                    if (Math.Abs(diffdate.TotalDays) > (30 * 365))
                    {
                        throw new UserFriendlyException(L("HiredMorethan30Years"));
                    }

                    if (datehired > DateTime.Now)
                    {
                        throw new UserFriendlyException(L("FutureDateErr"));
                    }

                    if (Math.Abs(datehired.Subtract(dob).TotalDays) > (60 * 365))
                    {
                        throw new UserFriendlyException(L("MismatchBetweenDOBAndDateHired"));
                    }

                    #endregion Date Related

                    processString = "INSERT PERINFO";

                    PersonalInformationEditDto newpersonal;

                    newpersonal = new PersonalInformationEditDto
                    {
                        Id = empRefId,
                        CompanyRefId = defaultCompanyId,
                        LocationRefId = defaultLocationId,
                        BioMetricCode = biometriccodeLong,
                        EmployeeCode = employeeCode.ToUpper(),
                        EmployeeName = employeeName.ToUpper(),
                        EmployeeGivenName = employeeGivenName.ToUpper(),
                        EmployeeSurName = employeeSurName,
                        Gender = genderString,
                        UserSerialNumber = userserialNumber,
                        DefaultSkillSetCode = defaultSkillSetCode,
                        DateHired = datehired,
                        DateOfBirth = dob,
                        ActiveStatus = true,
                        IsAttendanceRequired = true,
                        JobTitleRefId = jobTitleRefId,
                        Email = email,
                        HandPhone1 = handphone1,
                        HandPhone2 = handphone2,
                        LoginUserName = userName,
                        NationalIdentificationNumber = nationalIdentificationNumber,
                        StaffType = staffType,
                        SupervisorEmployeeRefId = supervisorRefId,
                        InchargeEmployeeRefId = managerRefId,
                        DefaultSkillRefId = defaultSkillSetRefId,
                        DefaultWeekOff = 0,
                        IsDayCloseStatusMailRequired = false,
                    };

                    var inputperinfodto = new CreateOrUpdatePersonalInformationInput
                    {
                        PersonalInformation = newpersonal,
                        UpdateSerialNumberRequired = false
                    };

                    if (empRefId > 0)
                    {
                        //int A = 1;
                    }
                    else
                    {
                        //int A = 2;
                    }
                    IdInput employee = await _personalAppSerivce.CreateOrUpdatePersonalInformation(inputperinfodto);

                    newpersonal.Id = employee.Id;
                    if (empRefId == null)
                        employeeList.Add(newpersonal);

                    empRefId = employee.Id;

                    #region Salary Info

                    //processString = "SALARYTYPE";
                    //string salarytypestring = row["SALARYTYPE"]?.ToString() ?? "";
                    //if (salarytypestring.Length == 0)
                    //{
                    //    salarytypestring = "MONTHLY";
                    //}
                    //int salaryModeType = 0;
                    //if (salarytypestring.Equals("MONTHLY"))
                    //{
                    //    salaryModeType = (int)SalaryType.MONTHLY;
                    //}
                    //else if (salarytypestring.Equals("DAILY"))
                    //{
                    //    salaryModeType = (int)SalaryType.DAILY;
                    //}
                    //else if (salarytypestring.Equals("HOURLY"))
                    //{
                    //    salaryModeType = (int)SalaryType.HOURLY;
                    //}
                    //else
                    //{
                    //    throw new UserFriendlyException(employeeName + " " + L("SalaryTypeError"));
                    //}

                    //processString = "BASICSALARY";
                    //string basicsalarystring = row["BASICSALARY"]?.ToString() ?? "";
                    //if (basicsalarystring.Length == 0)
                    //{
                    //    throw new UserFriendlyException(L("BasicSalary"));
                    //}
                    //decimal basicSalary = Convert.ToDecimal(basicsalarystring);

                    //processString = "ALLOWANCE";
                    //string allowancestring = row["ALLOWANCE"]?.ToString() ?? "";
                    //if (allowancestring.Length == 0)
                    //    allowancestring = "0";
                    //decimal allowance = Convert.ToDecimal(allowancestring);

                    //processString = "ACCOMODATION";
                    //string accomodationstring = row["ACCOMODATION"]?.ToString() ?? "";
                    //accomodationstring = accomodationstring.Trim();
                    //if (accomodationstring.Length == 0)
                    //    accomodationstring = "0";
                    //decimal accomodation = Convert.ToDecimal(accomodationstring);

                    //processString = "LEVY";
                    //string levystring = row["levy"]?.ToString() ?? "";
                    //levystring = levystring.Trim();
                    //if (levystring.Length == 0)
                    //    levystring = "0";
                    //decimal levy = Convert.ToDecimal(levystring);

                    //processString = "DEDUCTION";
                    //string deductionstring = row["DEDUCTION"]?.ToString() ?? "";
                    //deductionstring = deductionstring.Trim();
                    //if (deductionstring.Length == 0)
                    //    deductionstring = "0";
                    //decimal deduction = Convert.ToDecimal(deductionstring);

                    //var salaryId = await _salaryinfoRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == empRefId);
                    //if (salaryId == null)
                    //{
                    //    throw new UserFriendlyException(L("SalaryRecordNotFound"));
                    //}

                    //SalaryInfoEditDto newsalary = new SalaryInfoEditDto
                    //{
                    //    Id = salaryId.Id,
                    //    EmployeeRefId = (int)empRefId,
                    //    CompanyRefId = (int)companyRefId,
                    //    SalaryMode = salaryModeType,
                    //    BasicSalary = basicSalary,
                    //    Allowance = allowance,
                    //    Accomodation = accomodation,
                    //    Levy = levy,
                    //    Deduction = deduction,
                    //};

                    //CreateOrUpdateSalaryInfoInput inputsalarydto = new CreateOrUpdateSalaryInfoInput
                    //{
                    //    SalaryInfo = newsalary
                    //};

                    //await _salaryAppService.CreateOrUpdateSalaryInfo(inputsalarydto);

                    #endregion Salary Info

                    processString = "KNOWN SKILL CODES";
                    string knownskillcodes = row["KNOWN SKILL CODES"]?.ToString() ?? "";
                    if (knownskillcodes.Trim().Length > 0)
                    {
                        string[] arrknownskills = knownskillcodes.ToUpper().Trim().Split(',');
                        foreach (var skillemp in arrknownskills)
                        {
                            var skill = await _skillsetRepo.FirstOrDefaultAsync(t => t.SkillCode.ToUpper().Equals(skillemp));
                            //if (skill != null)
                            //{
                            //    var existAlready = await _employeeSkillSet.FirstOrDefaultAsync(t => t.EmployeeRefId == employee.Id && t.SkillSetRefId == skill.Id);
                            //    if (existAlready == null)
                            //    {
                            //        var empskilldto = new EmployeeSkillSet
                            //        {
                            //            EmployeeRefId = employee.Id,
                            //            SkillSetRefId = skill.Id
                            //        };
                            //        await _employeeSkillSet.InsertOrUpdateAndGetIdAsync(empskilldto);
                            //    }
                            //}

                            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.SoftDelete))
                            {
                                var existAlready = await _employeeSkillSet.FirstOrDefaultAsync(t => t.EmployeeRefId == employee.Id
                                    && t.SkillSetRefId == skill.Id);
                                if (existAlready == null)
                                {
                                    var newskillset = new EmployeeSkillSet
                                    {
                                        EmployeeRefId = employee.Id,
                                        SkillSetRefId = skill.Id
                                    };
                                    await _employeeSkillSet.InsertOrUpdateAndGetIdAsync(newskillset);
                                }
                                else
                                {
                                    if (existAlready.IsDeleted)
                                    {
                                        existAlready.DeleterUserId = null;
                                        existAlready.DeletionTime = null;
                                        existAlready.IsDeleted = false;
                                        await _employeeSkillSet.InsertOrUpdateAndGetIdAsync(existAlready);
                                    }
                                }
                            }
                        }
                    }
                }
                return Json(new MvcAjaxResponse());
            }
            catch (DbEntityValidationException e)
            {
                string errorMessage = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    errorMessage = errorMessage + "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:";

                    foreach (var ve in eve.ValidationErrors)
                    {
                        errorMessage = errorMessage + "- Property: " + ve.PropertyName + " Error: " + ve.ErrorMessage + " ";
                    }
                }
                return Json(new MvcAjaxResponse(new ErrorInfo(errorMessage + processString)));
            }
            catch (Exception ex)
            {
                //throw new UserFriendlyException( ex.Message + ex.InnerException.Message + processString);
                MethodBase m = MethodBase.GetCurrentMethod();
                string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                //throw new UserFriendlyException("Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " " + innerExceptionMessage);

                return Json(new MvcAjaxResponse(new ErrorInfo(L("ErrRow", currentRow) + ex.Message + " " + processString + " " + innerExceptionMessage)));
            }
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportmanualAttendanceLog()
        {
            int currentRow = 0;
            List<StringInput> EmployeeListExcluded = new List<StringInput>();
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var supportedTypes = new[] { "txt", "rtf", "html", "xaml", "xslx", "pdf", "doc", "docx", "csv" };

                var fileExt = System.IO.Path.GetExtension(file.FileName).Substring(1);

                if (!supportedTypes.Contains(fileExt))
                {
                    ModelState.AddModelError("file", "Invalid type. Only the following types (txt, rtf, html, xslx, pdf, xaml, doc, docx, csv) are supported.");
                    //return View();
                }
                if (file.ContentLength > 600000)
                {
                    ModelState.AddModelError("file", "The size of the file should not exceed 200 KB");
                }
                string dateattendancestring;
                //if (file.ContentLength > 0)
                //{
                //    var fileName = Path.GetFileName(file.FileName);
                //    var path = Path.Combine(Server.MapPath("~/uploads"), fileName);
                //    file.SaveAs(path);
                //}
                //string[] lines = System.IO.File.ReadAllLines(file.FileName);

                if (fileExt.ToLower().Equals("xlsx") || fileExt.ToLower().Equals("xls"))
                {
                    var data = new ExcelData(file.FileName, file.InputStream);
                    var allList = data.GetData();
                    var allLocations = await _attendanceMachineLocationRepo.GetAllListAsync();

                    foreach (var row in allList)
                    {
                        currentRow++;
                        string ipnumber = row["ID NUMBER"]?.ToString() ?? "";

                        if (row["ID NUMBER"].ToString().Length == 0)
                        {
                            continue;
                        }
                        string empcode = row["EMPLOYEE CODE"]?.ToString() ?? "";

                        if (row["EMPLOYEE CODE"].ToString().Length == 0)
                        {
                            continue;
                        }

                        var existemployee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.EmployeeCode == empcode);
                        if (existemployee == null)
                            existemployee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.NationalIdentificationNumber == ipnumber);
                        if (existemployee == null)
                            existemployee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.NationalIdentificationNumber == ipnumber);

                        if (existemployee == null)
                        {
                            string specifiedname = row["EMPLOYEE NAME"]?.ToString() ?? "";
                            if (specifiedname.Contains("("))
                            {
                                int lidx = specifiedname.LastIndexOf("(");
                                specifiedname = specifiedname.Substring(0, specifiedname.Length - (specifiedname.Length - lidx)).Trim();
                                var pers = _personalinformationRepo.GetAll().Where(t => t.EmployeeName.Equals(specifiedname.ToUpper())).ToList();
                                if (pers.Count() == 1)
                                {
                                    var perdto = await _personalinformationRepo.GetAsync(pers[0].Id);
                                    perdto.NationalIdentificationNumber = ipnumber;
                                    await _personalinformationRepo.UpdateAsync(perdto);
                                }
                                else if (pers.Count == 0)
                                {
                                    throw new UserFriendlyException(L("FinNumberNotMatched", specifiedname, ipnumber));
                                }
                                existemployee = pers[0];
                            }
                        }

                        int empRefId;
                        //if (existemployee == null)
                        //{
                        //    if (EmployeeListExcluded.Count > 0)
                        //    {
                        //        var existBioAlready = EmployeeListExcluded.FirstOrDefault(t => t.StringId == ipnumber);
                        //        if (existBioAlready == null)
                        //            EmployeeListExcluded.Add(new StringInput { StringId = ipnumber });
                        //    }
                        //    else
                        //    {
                        //        EmployeeListExcluded.Add(new StringInput { StringId = ipnumber });
                        //    }
                        //    continue;
                        //}
                        empRefId = existemployee.Id;
                        string inout = row["CHECK TYPE"]?.ToString() ?? "";
                        if (inout.Length == 0)
                        {
                        }
                        inout = inout.ToUpper();
                        string checktypecode;
                        if (inout == "IN")
                        {
                            checktypecode = "TI";
                        }
                        else if (inout == "OUT")
                        {
                            checktypecode = "TO";
                        }
                        else
                        {
                            checktypecode = "EX";
                        }
                        //29 01 2018  6:58:00 AM
                        dateattendancestring = row["TIME"]?.ToString() ?? "";

                        DateTime tt0 = new DateTime(2018, 01, 14, 13, 15, 25);

                        string z = tt0.ToString("dd MM yyyy h:mm:ss tt");
                        string tt1 = tt0.ToString("dd/MM/yyyy HH:mm:ss tt");
                        string tt2 = tt0.ToString("dd/MM/yyyy HH:mm:ss tt");
                        string tt3 = tt0.ToString("MM/dd/yyyy h:mm:ss tt");
                        string tt4 = tt0.ToString("MM/dd/yyyy HH:mm:ss tt");

                        DateTime dateAttendance;
                        if (DateTime.TryParseExact(dateattendancestring, "MM/dd/yyyy h:mm:ss tt", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out dateAttendance))
                        {
                            dateAttendance = DateTime.ParseExact(dateattendancestring, "MM/dd/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
                        }
                        else if (DateTime.TryParseExact(dateattendancestring, "M/dd/yyyy HH:mm:ss tt", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out dateAttendance))
                        {
                            dateAttendance = DateTime.ParseExact(dateattendancestring, "M/dd/yyyy HH:mm:ss tt", CultureInfo.InvariantCulture);
                        }
                        else if (DateTime.TryParseExact(dateattendancestring, "dd/MM/yy h:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateAttendance))
                        {
                            dateAttendance = DateTime.ParseExact(dateattendancestring, "dd/MM/yy h:mm tt", CultureInfo.InvariantCulture);
                        }
                        else if (DateTime.TryParseExact(dateattendancestring, "dd/MM/yyyy HH:mm:ss tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateAttendance))
                        {
                            dateAttendance = DateTime.ParseExact(dateattendancestring, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        }
                        else if (DateTime.TryParseExact(dateattendancestring, "dd/MM/yyyy h:mm:ss tt", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out dateAttendance))
                        {
                            dateAttendance = DateTime.ParseExact(dateattendancestring, "dd/MM/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
                        }
                        else if (DateTime.TryParseExact(dateattendancestring, "dd MM yyyy HH:mm:ss tt", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out dateAttendance))
                        {
                            dateAttendance = DateTime.ParseExact(dateattendancestring, "dd/MM/yy h:mm tt", CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            //9 01 2018  12:44:00 PM
                            // 1/9/2018 12:44:00 PM"
                            string tempDate = dateattendancestring;
                            string[] arrDate = tempDate.Split(' ');
                            string[] attDateString = arrDate[0].Split('/');
                            int day;
                            int month;
                            int year;
                            //int hour;
                            //int minutes;
                            //int seconds;
                            if (int.TryParse(attDateString[0], out month))
                            {
                                month = int.Parse(attDateString[0]);
                            }
                            if (int.TryParse(attDateString[1], out day))
                            {
                                day = int.Parse(attDateString[1]);
                            }
                            if (int.TryParse(attDateString[2], out year))
                            {
                                year = int.Parse(attDateString[2]);
                            }

                            DateTime tm = DateTime.Parse(arrDate[1] + " " + arrDate[2]);

                            //string[] arrtime = arrDate[1].Split(':');
                            //if (int.TryParse(arrtime[0], out hour))
                            //{
                            //	hour = int.Parse(arrtime[0]);
                            //}
                            //if (int.TryParse(arrtime[1], out minutes))
                            //{
                            //	minutes = int.Parse(arrtime[1]);
                            //}
                            //if (int.TryParse(arrtime[2], out seconds))
                            //{
                            //	seconds = int.Parse(arrtime[2]);
                            //}

                            dateAttendance = new DateTime(year, month, day, tm.Hour, tm.Minute, tm.Second);
                        }

                        TimeSpan diffdate = DateTime.Now.Subtract(dateAttendance);

                        if (diffdate.TotalDays < 0)
                        {
                            throw new UserFriendlyException(L("ErrRow", currentRow) + L("FutureDateErr"));
                        }
                        //if (Math.Abs(diffdate.TotalDays) > 90)
                        //{
                        //	throw new UserFriendlyException(L("ErrRow", currentRow) + L("CanNotLessThan90Days"));
                        //}
                        if (Math.Abs(diffdate.TotalDays) > 240)
                        {
                            throw new UserFriendlyException(L("ErrRow", currentRow) + L("CanNotLessThan240Days"));
                        }
                        //if (Math.Abs(diffdate.TotalDays) > 60)
                        //{
                        //   throw new UserFriendlyException(L("ErrRow", currentRow) + L("CanNotLessThan60Days"));
                        //}
                        //if (Math.Abs(diffdate.TotalDays) > 30)
                        //{
                        //   throw new UserFriendlyException(L("ErrRow", currentRow) + L("CanNotLessThan60Days"));
                        //}

                        if (dateAttendance > DateTime.Now)
                        {
                            throw new UserFriendlyException(L("ErrRow", currentRow) + L("FutureDateErr"));
                        }

                        AttendanceLog existAlready;
                        using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.SoftDelete))
                        {
                            existAlready = await _attendancelogRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == empRefId && (DateTime)t.CheckTime == dateAttendance);
                        }

                        if (existAlready != null)
                            continue;

                        var newAttendance = new AttendanceLogEditDto
                        {
                            EmployeeRefId = empRefId,
                            CheckTime = dateAttendance,
                            VerifyMode = 1,
                            AuthorisedBy = null,
                            BioMetricCode = existemployee.BioMetricCode,
                            CheckTypeCode = checktypecode,
                            AttendanceMachineLocationRefId = 2,
                        };

                        var inputperinfodto = new CreateOrUpdateAttendanceLogInput
                        {
                            AttendanceLog = newAttendance,
                            //ManualAttendanceFlag=true
                        };

                        await _attendancelogAppService.CreateOrUpdateAttendanceLog(inputperinfodto);
                    }
                }

                if (EmployeeListExcluded.Count > 0)
                    EmployeeListExcluded = EmployeeListExcluded.OrderBy(t => t.StringId).ToList();

                return Json(new MvcAjaxResponse(EmployeeListExcluded));
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(L("ErrRow", currentRow) + ex.Message)));
            }
        }

        public async Task<FileResult> ImportAttLogTemplate()
        {
            return File(Server.MapPath("~/Common/Import/Manual Attendance List.xlsx"),
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportAttendanceLog()
        {
            int currentRow = 0;
            List<StringInput> EmployeeListExcluded = new List<StringInput>();
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var supportedTypes = new[] { "txt", "rtf", "html", "xaml", "xslx", "pdf", "doc", "docx", "csv" };

                var fileExt = System.IO.Path.GetExtension(file.FileName).Substring(1);

                if (!supportedTypes.Contains(fileExt))
                {
                    ModelState.AddModelError("file", "Invalid type. Only the following types (txt, rtf, html, xslx, pdf, xaml, doc, docx, csv) are supported.");
                    //return View();
                }
                if (file.ContentLength > 600000)
                {
                    ModelState.AddModelError("file", "The size of the file should not exceed 200 KB");
                }
                string dateattendancestring;
                //if (file.ContentLength > 0)
                //{
                //    var fileName = Path.GetFileName(file.FileName);
                //    var path = Path.Combine(Server.MapPath("~/uploads"), fileName);
                //    file.SaveAs(path);
                //}
                //string[] lines = System.IO.File.ReadAllLines(file.FileName);

                if (fileExt.ToLower().Equals("xlsx") || fileExt.ToLower().Equals("xls"))
                {
                    var data = new ExcelData(file.FileName, file.InputStream);
                    var allList = data.GetData();
                    var allLocations = await _attendanceMachineLocationRepo.GetAllListAsync();
                    //SupplierName    Address1 Address2    Address3 City    State Country ZipCode PhoneNumber Fax Website Email

                    foreach (var row in allList)
                    {
                        if (currentRow == 102)
                        {
                            //int i = 1;
                        }
                        currentRow++;
                        string ipnumber = row["IC/WP NO"]?.ToString() ?? "";

                        if (row["IC/WP NO"].ToString().Length == 0)
                        {
                            continue;
                        }
                        ipnumber = ipnumber.Replace("-", "");

                        string machinelocation = row["LOCATION"]?.ToString() ?? "";

                        if (row["LOCATION"].ToString().Length == 0)
                        {
                            throw new UserFriendlyException(L("Location") + " ?"); ;
                        }
                        int mlId = 2;
                        //var ml = allLocations.FirstOrDefault(T => T.MachinePrefix.ToUpper() == machinelocation.ToUpper());
                        //if (ml == null)
                        //{
                        //	//throw new UserFriendlyException(L("AttendanceLocationNotMatched") + " "+ machinelocation.ToUpper());
                        //}
                        //else
                        //{
                        //}

                        var existemployee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.BioMetricCode.Replace("-", "") == ipnumber);

                        if (existemployee == null)
                        {
                            string specifiedname = row["WORKER NAME"]?.ToString() ?? "";
                            if (specifiedname.Contains("("))
                            {
                                int lidx = specifiedname.LastIndexOf("(");
                                specifiedname = specifiedname.Substring(0, specifiedname.Length - (specifiedname.Length - lidx)).Trim();
                                var pers = _personalinformationRepo.GetAll().Where(t => t.EmployeeName.Equals(specifiedname.ToUpper())).ToList();
                                if (pers.Count() == 1)
                                {
                                    var perdto = await _personalinformationRepo.GetAsync(pers[0].Id);
                                    await _personalinformationRepo.UpdateAsync(perdto);
                                }
                                else if (pers.Count == 0)
                                {
                                    throw new UserFriendlyException(L("FinNumberNotMatched", specifiedname, ipnumber));
                                }
                                existemployee = pers[0];
                            }
                        }

                        int empRefId;
                        if (existemployee == null)
                        {
                            if (EmployeeListExcluded.Count > 0)
                            {
                                var existBioAlready = EmployeeListExcluded.FirstOrDefault(t => t.StringId == ipnumber);
                                if (existBioAlready == null)
                                    EmployeeListExcluded.Add(new StringInput { StringId = ipnumber });
                            }
                            else
                            {
                                EmployeeListExcluded.Add(new StringInput { StringId = ipnumber });
                            }
                            continue;
                        }
                        empRefId = existemployee.Id;
                        string inout = row["SCAN TYPE"]?.ToString() ?? "";
                        if (inout.Length == 0)
                        {
                        }
                        inout = inout.ToUpper();
                        string checktypecode;
                        if (inout == "IN")
                        {
                            checktypecode = "TI";
                        }
                        else if (inout == "OUT")
                        {
                            checktypecode = "TO";
                        }
                        else
                        {
                            checktypecode = "EX";
                        }
                        //29 01 2018  6:58:00 AM
                        dateattendancestring = row["TIME"]?.ToString() ?? "";

                        DateTime tt0 = new DateTime(2018, 01, 14, 13, 15, 25);

                        string z = tt0.ToString("dd MM yyyy h:mm:ss tt");
                        string tt1 = tt0.ToString("dd/MM/yyyy HH:mm:ss tt");
                        string tt2 = tt0.ToString("dd/MM/yyyy HH:mm:ss tt");
                        string tt3 = tt0.ToString("MM/dd/yyyy h:mm:ss tt");
                        string tt4 = tt0.ToString("MM/dd/yyyy HH:mm:ss tt");

                        DateTime dateAttendance;
                        if (DateTime.TryParseExact(dateattendancestring, "MM/dd/yyyy h:mm:ss tt", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out dateAttendance))
                        {
                            dateAttendance = DateTime.ParseExact(dateattendancestring, "MM/dd/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
                        }
                        else if (DateTime.TryParseExact(dateattendancestring, "M/dd/yyyy HH:mm:ss tt", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out dateAttendance))
                        {
                            dateAttendance = DateTime.ParseExact(dateattendancestring, "M/dd/yyyy HH:mm:ss tt", CultureInfo.InvariantCulture);
                        }
                        else if (DateTime.TryParseExact(dateattendancestring, "dd/MM/yy h:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateAttendance))
                        {
                            dateAttendance = DateTime.ParseExact(dateattendancestring, "dd/MM/yy h:mm tt", CultureInfo.InvariantCulture);
                        }
                        else if (DateTime.TryParseExact(dateattendancestring, "dd/MM/yyyy HH:mm:ss tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateAttendance))
                        {
                            dateAttendance = DateTime.ParseExact(dateattendancestring, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        }
                        else if (DateTime.TryParseExact(dateattendancestring, "dd/MM/yyyy h:mm:ss tt", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out dateAttendance))
                        {
                            dateAttendance = DateTime.ParseExact(dateattendancestring, "dd/MM/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
                        }
                        else if (DateTime.TryParseExact(dateattendancestring, "dd MM yyyy HH:mm:ss tt", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out dateAttendance))
                        {
                            dateAttendance = DateTime.ParseExact(dateattendancestring, "dd/MM/yy h:mm tt", CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            //9 01 2018  12:44:00 PM
                            // 1/9/2018 12:44:00 PM"
                            string tempDate = dateattendancestring;
                            string[] arrDate = tempDate.Split(' ');
                            string[] attDateString = arrDate[0].Split('/');
                            int day;
                            int month;
                            int year;
                            //int hour;
                            //int minutes;
                            //int seconds;
                            if (int.TryParse(attDateString[0], out month))
                            {
                                month = int.Parse(attDateString[0]);
                            }
                            if (int.TryParse(attDateString[1], out day))
                            {
                                day = int.Parse(attDateString[1]);
                            }
                            if (int.TryParse(attDateString[2], out year))
                            {
                                year = int.Parse(attDateString[2]);
                            }

                            DateTime tm = DateTime.Parse(arrDate[1] + " " + arrDate[2]);

                            //string[] arrtime = arrDate[1].Split(':');
                            //if (int.TryParse(arrtime[0], out hour))
                            //{
                            //	hour = int.Parse(arrtime[0]);
                            //}
                            //if (int.TryParse(arrtime[1], out minutes))
                            //{
                            //	minutes = int.Parse(arrtime[1]);
                            //}
                            //if (int.TryParse(arrtime[2], out seconds))
                            //{
                            //	seconds = int.Parse(arrtime[2]);
                            //}

                            dateAttendance = new DateTime(year, month, day, tm.Hour, tm.Minute, tm.Second);
                        }

                        TimeSpan diffdate = DateTime.Now.Subtract(dateAttendance);

                        if (diffdate.TotalDays < 0)
                        {
                            throw new UserFriendlyException(L("ErrRow", currentRow) + L("FutureDateErr"));
                        }
                        //if (Math.Abs(diffdate.TotalDays) > 90)
                        //{
                        //	throw new UserFriendlyException(L("ErrRow", currentRow) + L("CanNotLessThan90Days"));
                        //}
                        if (Math.Abs(diffdate.TotalDays) > 240)
                        {
                            throw new UserFriendlyException(L("ErrRow", currentRow) + L("CanNotLessThan240Days"));
                        }
                        //if (Math.Abs(diffdate.TotalDays) > 60)
                        //{
                        //   throw new UserFriendlyException(L("ErrRow", currentRow) + L("CanNotLessThan60Days"));
                        //}
                        //if (Math.Abs(diffdate.TotalDays) > 30)
                        //{
                        //   throw new UserFriendlyException(L("ErrRow", currentRow) + L("CanNotLessThan60Days"));
                        //}

                        if (dateAttendance > DateTime.Now)
                        {
                            throw new UserFriendlyException(L("ErrRow", currentRow) + L("FutureDateErr"));
                        }

                        AttendanceLog existAlready;
                        using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.SoftDelete))
                        {
                            existAlready = await _attendancelogRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == empRefId && (DateTime)t.CheckTime == dateAttendance);
                        }

                        if (existAlready != null)
                            continue;

                        var newAttendance = new AttendanceLogEditDto
                        {
                            EmployeeRefId = empRefId,
                            CheckTime = dateAttendance,
                            VerifyMode = 0,
                            AuthorisedBy = null,
                            BioMetricCode = existemployee.BioMetricCode,
                            CheckTypeCode = checktypecode,
                            AttendanceMachineLocationRefId = mlId
                        };

                        var inputperinfodto = new CreateOrUpdateAttendanceLogInput
                        {
                            AttendanceLog = newAttendance,
                        };

                        await _attendancelogAppService.CreateOrUpdateAttendanceLog(inputperinfodto);
                    }
                }
                else
                {
                    var dtos = new List<AttendanceLogEditDto>();

                    StreamReader csvreader = new StreamReader(file.InputStream);

                    bool ZkFlag = true;

                    while (!csvreader.EndOfStream)
                    {
                        currentRow++;
                        var line = csvreader.ReadLine();
                        if (line.Trim().Length == 0)
                            continue;

                        var attData = line.Split('\t');

                        string bioMetricCodestring = attData[0];

                        var def = _attendanceMachineLocationRepo.GetAll().OrderBy(t => t.Id).ToList();
                        if (def.Count == 0)
                        {
                            throw new UserFriendlyException(L("RequiredMachineLocation"));
                        }

                        int machineLocationId = def[0].Id; ;

                        int checktypeint;

                        if (bioMetricCodestring.Trim() == "No")
                        {
                            ZkFlag = false;
                            continue;
                        }

                        if (ZkFlag == true)
                        {
                            bioMetricCodestring = attData[0];
                            dateattendancestring = attData[1];
                            checktypeint = int.Parse(attData[3]);
                            //machineLocationId = 1
                        }
                        else
                        {
                            bioMetricCodestring = attData[2];
                            dateattendancestring = attData[4];
                            checktypeint = 6;
                            machineLocationId = 3;
                        }

                        if (dateattendancestring.Length == 0)
                        {
                            throw new UserFriendlyException(L("ErrRow", currentRow) + L("Date"));
                        }

                        DateTime dateAttendance = DateTime.Parse(dateattendancestring);
                        TimeSpan diffdate = DateTime.Now.Subtract(dateAttendance);

                        if (Math.Abs(diffdate.TotalDays) > 60)
                        {
                            continue;
                        }

                        if (dateAttendance > DateTime.Now)
                        {
                            throw new UserFriendlyException(L("ErrRow", currentRow) + L("FutureDateErr"));
                        }

                        if (bioMetricCodestring.Length == 0)
                        {
                            throw new UserFriendlyException(L("ErrRow", currentRow) + L("BioMetricCode"));
                        }
                        string biometriccodeLong = (bioMetricCodestring);

                        var existemployee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.BioMetricCode == biometriccodeLong);
                        int empRefId;
                        if (existemployee != null)
                        {
                            empRefId = existemployee.Id;
                        }
                        else
                        {
                            if (EmployeeListExcluded.Count > 0)
                            {
                                var existBioAlready = EmployeeListExcluded.FirstOrDefault(t => t.StringId == (biometriccodeLong.ToString()));
                                if (existBioAlready == null)
                                    EmployeeListExcluded.Add(new StringInput { StringId = biometriccodeLong.ToString() });
                            }
                            else
                            {
                                EmployeeListExcluded.Add(new StringInput { StringId = (biometriccodeLong.ToString()) });
                            }
                            continue;
                            //throw new UserFriendlyException(L("BioMetricCodeDoesNotExist", biometriccodeLong));
                        }

                        string checktypecode;
                        if (checktypeint == 0)
                        {
                            checktypecode = "TI";
                        }
                        else if (checktypeint == 1)
                        {
                            checktypecode = "TO";
                        }
                        else if (checktypeint == 2)
                        {
                            checktypecode = "BO";
                        }
                        else if (checktypeint == 3)
                        {
                            checktypecode = "BI";
                        }
                        else if (checktypeint == 4)
                        {
                            checktypecode = "OO";
                        }
                        else if (checktypeint == 5)
                        {
                            checktypecode = "OI";
                        }
                        else
                        {
                            checktypecode = "EX";
                        }
                        AttendanceLog existAlready;
                        using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.SoftDelete))
                        {
                            existAlready = await _attendancelogRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == empRefId && (DateTime)t.CheckTime == dateAttendance);
                        }

                        if (existAlready != null)
                            continue;

                        var newAttendance = new AttendanceLogEditDto
                        {
                            AttendanceMachineLocationRefId = machineLocationId,
                            EmployeeRefId = empRefId,
                            CheckTime = dateAttendance,
                            VerifyMode = 0,
                            AuthorisedBy = null,
                            BioMetricCode = biometriccodeLong,
                            CheckTypeCode = checktypecode,
                        };

                        var inputperinfodto = new CreateOrUpdateAttendanceLogInput
                        {
                            AttendanceLog = newAttendance,
                        };

                        await _attendancelogAppService.CreateOrUpdateAttendanceLog(inputperinfodto);
                    }
                }

                if (EmployeeListExcluded.Count > 0)
                    EmployeeListExcluded = EmployeeListExcluded.OrderBy(t => t.StringId).ToList();
                return Json(new MvcAjaxResponse(EmployeeListExcluded));
            }
            catch (Exception ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(L("ErrRow", currentRow) + ex.Message)));
            }
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportConnectCards()
        {
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];

                var stringCardTypeId = Request.Params["connectCardTypeId"];
                var cardTypeRefId = int.Parse(stringCardTypeId);
                var cardTypeDto = await _connectCardTypeRepository.FirstOrDefaultAsync(t => t.Id == cardTypeRefId);

                if (cardTypeDto == null)
                {
                    throw new UserFriendlyException(L("CardTypeError"));
                }

                var data = new ExcelData(file.FileName, file.InputStream);

                var dtos = GetConnectCardFromFile(data, cardTypeDto.Id);
                foreach (var dto in dtos)
                {
                    var result = await _connectCardAppService.CreateOrUpdateConnectCard(dto);
                }

                return Json(new MvcAjaxResponse());
            }
            catch (UserFriendlyException ex)
            {
                //Return error message
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        private List<ConnectCardEditDto> GetConnectCardFromFile(ExcelData data, int cardTypeId)
        {
            var currentRow = 1;
            try
            {
                var dtos = new List<ConnectCardEditDto>();
                foreach (var row in data.GetData())
                {
                    currentRow++;
                    var cardNumber = row["CardNumber"]?.ToString().Trim() ?? "";

                    if (cardNumber.IsNullOrEmpty())
                    {
                        throw new UserFriendlyException(L("ErrRow", currentRow) + L("CardNumber"));
                    }
                    var redeemedString = row["Redeemed"]?.ToString() ?? "NO";
                    bool redeemedValue = false;
                    if (redeemedString.ToUpper().Equals("TRUE") || redeemedString.ToUpper().Equals("YES"))
                        redeemedValue = true;
                    var connectCard = new ConnectCardEditDto
                    {
                        ConnectCardTypeId = cardTypeId,
                        CardNo = cardNumber,
                        Redeemed = redeemedValue
                    };
                    dtos.Add(connectCard);
                }

                return dtos;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }

        }
        public async Task<List<ConnectCardEditDto>> GetConnectCardDataFromFile(ExcelData data, bool active)
        {
            var currentRow = 1;
            try
            {
                var dtos = new List<ConnectCardEditDto>();
                foreach (var row in data.GetData())
                {
                    currentRow++;

                    if (row["CardNumber"].ToString().Length == 0)
                    {
                        continue;
                    }

                    var cardNumber = row["CardNumber"]?.ToString().Trim() ?? "";
                    if (cardNumber.IsNullOrEmpty())
                    {
                        throw new UserFriendlyException(L("ErrRow", currentRow) + L("CardNumber"));
                    }

                    string connectCardTypeName = row["CardType"]?.ToString() ?? "";
                    connectCardTypeName = connectCardTypeName.Trim().ToUpper();
                    if (connectCardTypeName.Length == 0)
                    {
                        throw new UserFriendlyException(L("ErrRow", currentRow) + L("CardType") + " : " + connectCardTypeName + " " + L("NotExists"));
                    }

                    int? id = null, cardTypeId = null; bool redeemedValue = false;
                    var exists = await _connectCardTypeRepository.FirstOrDefaultAsync(t => t.Name == connectCardTypeName);
                    if (exists != null)
                    {
                        cardTypeId = exists.Id;
                        if (exists.UseOneTime == true)
                        {
                            redeemedValue = false;
                        }
                    }
                    else
                    {
                        cardTypeId = null;
                    }

                    if (cardTypeId == null)
                    {
                        throw new UserFriendlyException(L("ErrRow", currentRow) + L("CardType") + " : " + connectCardTypeName + " " + L("NotExists"));
                    }

                    var alreadyExists = await _connectCardRepository.FirstOrDefaultAsync(t => t.CardNo == cardNumber);
                    if (alreadyExists != null)
                    {
                        id = alreadyExists.Id;
                    }

                    var connectCard = new ConnectCardEditDto
                    {
                        Id = id,
                        ConnectCardTypeId = cardTypeId.Value,
                        CardNo = cardNumber,
                        Redeemed = redeemedValue,
                        Active = active
                    };

                    dtos.Add(connectCard);
                }

                return dtos;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
        public FileResult ImportConnectCardTemplate()
        {
            var subPath = "~/Common/Import/ConnectCardImportTemplate.xlsx";
            bool exists = System.IO.File.Exists(Server.MapPath(subPath));

            if (!exists)
            {
                var file = new FileDto("ConnectCardImportTemplate.xlsx",
                            MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
                using (var package = new ExcelPackage())
                {
                    var worksheet = package.Workbook.Worksheets.Add("Template");
                    var row = 1;
                    var col = 1;

                    var columnNames = new[] { "CardNumber", "Redeemed" };
                    foreach (var cn in columnNames)
                    {
                        worksheet.Cells[row, col].Value = cn;
                        worksheet.Column(col).AutoFit();
                        worksheet.Cells[1, col].Style.Font.Bold = true;
                        col++;
                    }

                    var filePath = Path.Combine(Server.MapPath("~/Common/Import"), file.FileName);
                    package.SaveAs(new FileInfo(filePath));
                }
            }

            return File(Server.MapPath(subPath), MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }
        public FileResult ImportConnectCard()
        {
            var subPath = "~/Common/Import/ConnectCardTemplate.xlsx";
            bool exists = System.IO.File.Exists(Server.MapPath(subPath));

            if (!exists)
            {
                var file = new FileDto("ConnectCardTemplate.xlsx",
                        MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
                using (var package = new ExcelPackage())
                {
                    var worksheet = package.Workbook.Worksheets.Add("Template");
                    var row = 1;
                    var col = 1;

                    var columnNames = new[] { "CardNumber", "CardType" };
                    foreach (var cn in columnNames)
                    {
                        worksheet.Cells[row, col].Value = cn;
                        worksheet.Column(col).AutoFit();
                        worksheet.Cells[1, col].Style.Font.Bold = true;
                        col++;
                    }

                    var filePath = Path.Combine(Server.MapPath("~/Common/Import"), file.FileName);
                    package.SaveAs(new FileInfo(filePath));
                }
            }

            return File(Server.MapPath(subPath), MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }
        public virtual async Task<JsonResult> ImportConnectCardData(bool active, string jobName)
        {
            try
            {
                #region Saving the Request Template File Locally
                //Check input
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("File") + " " + L("NotExist"));
                }

                #region FilePathDefinition
                var tenant = await TenantManager.GetByIdAsync(AbpSession.TenantId.Value);
                var tenantName = tenant.TenancyName;
                var tenantId = tenant.Id;
                string subPath = "\\DocumentFiles\\" + tenantName + "\\ImportConnectCardData\\" + DateTime.Now.ToString("dd-MMM-yyyy HH-mm-ss") + "\\";

                string phyPath = Path.GetFullPath(subPath);

                bool exists = Directory.Exists((phyPath));//System.IO.Server.MapPath

                if (!exists)
                {
                    System.IO.Directory.CreateDirectory(phyPath);
                }
                #endregion

                string fileSavedName = "";
                string fileContentType = "";
                var file = Request.Files[0];

                if (file.ContentLength > MaxConnectCardSize)
                {
                    throw new UserFriendlyException(L("TheUploadedFileIsLargerThan2MB.PleaseUploadNewFile."));
                }

                fileSavedName = Path.GetFileName(file.FileName);
                fileContentType = file.ContentType;
                file.SaveAs(Path.Combine(phyPath, fileSavedName));

                string templateFileName = phyPath + fileSavedName;

                #endregion

                var connectCard = new ConnectCardStatusDto();
                List<ConnectCardEditDto> allItems = new List<ConnectCardEditDto>();
                ImportConnectCardsInBackgroundInputDto inputDto = new ImportConnectCardsInBackgroundInputDto
                {
                    ConnectCard = allItems,
                    FileName = templateFileName,
                    Active = active,
                    TenantId = tenantId,
                    TenantName = tenantName,
                    JobName = jobName
                };
                await _connectCardAppService.ImportConnectCards(inputDto);
                return Json(new MvcAjaxResponse());
            }
            catch (UserFriendlyException ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
        public virtual async Task<JsonResult> ImportMenuItemPricesForFutureData(int futureDataId)
        {
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }
                var file = Request.Files[0];
                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();

                foreach (var row in allList)
                {
                    var menuItemId = int.Parse(row["MenuPortionId"]?.ToString().Trim());

                    var dto = new FuturePriceDto
                    {
                        FutureDataId = futureDataId,
                        MenuPortionId = int.Parse(row["MenuPortionId"]?.ToString().Trim()),
                        FuturePrice = row["Future Price"] != null ? Convert.ToDecimal(row["Future Price"]?.ToString()) : 0M
                    };

                    await _futureDataAppService.UpdateFuturePrice(dto);
                }

                return Json(new MvcAjaxResponse());
            }
            catch (UserFriendlyException ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        public FileResult FuturePriceTemplate()
        {
            var subPath = "~/Common/Import/FuturePriceTemplate.xlsx";
            bool exists = System.IO.File.Exists(Server.MapPath(subPath));

            if (!exists)
            {
                var file = new FileDto("FuturePriceTemplate.xlsx",
                        MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
                using (var package = new ExcelPackage())
                {
                    var worksheet = package.Workbook.Worksheets.Add("Template");
                    var row = 1;
                    var col = 1;

                    var columnNames = new[] { "MenuPortionId", "MenuName", "Portion", "Price", "Future Price" };
                    foreach (var cn in columnNames)
                    {
                        worksheet.Cells[row, col].Value = cn;
                        worksheet.Column(col).AutoFit();
                        worksheet.Cells[1, col].Style.Font.Bold = true;
                        col++;
                    }

                    var filePath = Path.Combine(Server.MapPath("~/Common/Import"), file.FileName);
                    package.SaveAs(new FileInfo(filePath));
                }
            }

            return File(Server.MapPath(subPath), MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
        }

        public async Task<JsonResult> ImportFullTaxMember()
        {
            var currentRow = 1;
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }

                var file = Request.Files[0];
                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();
                var listDto = new List<CreateOrUpdateFullTaxMemberInput>();

                foreach (var row in allList)
                {
                    currentRow++;
                    var member = new FullTaxMemberEditDto
                    {
                        TaxId = row["Tax Id"]?.ToString().Trim(),
                        BranchId = row["Branch Id"]?.ToString().Trim(),
                        Name = row["Name"]?.ToString().Trim(),
                        Phone = row["Phone"]?.ToString().Trim()
                    };

                    var dto = new CreateOrUpdateFullTaxMemberInput
                    {
                        TaxMember = member
                    };

                    await _fullTaxAppService.ValidateForCreateOrUpdateMember(member);
                    listDto.Add(dto);
                }

                listDto.ForEach(m => _fullTaxAppService.CreateOrUpdateFullTaxMember(m));

                return Json(new MvcAjaxResponse());
            }
            catch (UserFriendlyException ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(L("ErrRow", currentRow) + ex.Message)));
            }
        }

        public async Task<JsonResult> ImportDinePlanLanguage(string languageName, string sourceName)
        {
            var currentRow = 1;
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }

                var file = Request.Files[0];
                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();
                var listDto = new List<UpdateLanguageTextInput>();

                foreach (var row in allList)
                {
                    currentRow++;
                    var text = new UpdateLanguageTextInput
                    {
                        LanguageName = languageName,
                        SourceName = sourceName,
                        Key = row["Key"]?.ToString().Trim(),
                        Value = row["Value"]?.ToString().Trim()
                    };

                    await _languageAppService.UpdateDinePlanLanguageText(text);
                }

                return Json(new MvcAjaxResponse());
            }
            catch (UserFriendlyException ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(L("ErrRow", currentRow) + ex.Message)));
            }
        }

        public async Task<JsonResult> ImportDineConnectLanguage(string languageName, string sourceName)
        {
            var currentRow = 1;
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }

                var file = Request.Files[0];
                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();
                var listDto = new List<ApplicationLanguageText>();

                foreach (var row in allList)
                {
                    currentRow++;
                    var key = row["Key"]?.ToString().Trim();
                    var targetValue = row["Target value"]?.ToString().Trim();
                    var text = new ApplicationLanguageText
                    {
                        LanguageName = languageName,
                        Source = sourceName,
                        Key = key,
                        Value = targetValue,
                        TenantId = AbpSession.TenantId
                    };
                    var rsAlreadyExists = await _languageTextRepository.FirstOrDefaultAsync(t => t.Key.ToUpper().Equals(text.Key.ToUpper()) && t.LanguageName.ToUpper().Equals(text.LanguageName.ToUpper()) && t.Source.ToUpper().Equals(text.Source.ToUpper()) && t.TenantId == text.TenantId.Value);
                    if (rsAlreadyExists == null)
                    {
                        await _languageTextRepository.InsertAndGetIdAsync(text);
                    }
                    else
                    {
                        rsAlreadyExists.Value = text.Value;
                        await _languageTextRepository.UpdateAsync(rsAlreadyExists);
                    }
                }
                return Json(new MvcAjaxResponse());
            }
            catch (UserFriendlyException ex)
            {
                return Json(new MvcAjaxResponse(new ErrorInfo(L("ErrRow", currentRow) + ex.Message)));
            }
        }

        [UnitOfWork]
        public virtual async Task<JsonResult> ImportStockAdjustmentBasedOnStockTakenMultipleSupplierAliasNameReport()
        {
            var rownumber = 1;
            try
            {
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("NoFile"));
                }

                var givenDate = Request.Params["closingStockDate"];
                DateTime closingStockDate;

                var rsMaterialList = await _materialRepo.GetAllListAsync();
                var rsMaterialGroupCategory = await _materialGroupCategoryRepo.GetAllListAsync();
                var rsUnit = await _unitRepo.GetAllListAsync();

                if (DateTime.TryParse(givenDate, out closingStockDate))
                    closingStockDate = DateTime.Parse(givenDate);
                else
                    throw new UserFriendlyException("DateErr");

                var location = Request.Params["locationRefId"];
                int locationRefId;

                if (int.TryParse(location, out locationRefId))
                    locationRefId = int.Parse(location);
                else
                    throw new UserFriendlyException("LocationErr");

                var forceAdjustmentFlagstring = Request.Params["forceAdjustmentFlag"];
                bool forceAdjustmentFlag;
                if (bool.TryParse(forceAdjustmentFlagstring, out forceAdjustmentFlag))
                    forceAdjustmentFlag = bool.Parse(forceAdjustmentFlagstring);
                else
                    throw new UserFriendlyException(L("ForceAdjustmentErr"));

                bool stockEntryOnly;
                var stockEntryOnlystring = Request.Params["stockEntryOnlyFlag"];
                if (stockEntryOnlystring == "" || stockEntryOnlystring == null)
                    stockEntryOnly = false;
                else
                {
                    if (bool.TryParse(stockEntryOnlystring, out stockEntryOnly))
                        forceAdjustmentFlag = bool.Parse(forceAdjustmentFlagstring);
                    else
                        throw new UserFriendlyException(L("ForceAdjustmentErr"));
                }

                var outputClosingStock = new GetStockSummaryDtoOutput();
                var StockSummary = new List<MaterialLedgerDto>();
                if (stockEntryOnly == false)
                {
                    outputClosingStock =
                        await _housereportAppService.GetStockSummary(new GetHouseReportInput
                        {
                            StartDate = closingStockDate,
                            EndDate = closingStockDate,
                            LocationRefId = locationRefId,
                            ExcludeRateView = true
                        });

                    StockSummary = outputClosingStock.StockSummary;
                }
                // *** In future if any client request to account sales portion for forceclose also then we need to uncomment this

                //List<MaterialMenuMappingWithWipeOut> liveStockDetails = new List<MaterialMenuMappingWithWipeOut>();
                //if (forceAdjustmentFlag)
                //{
                //	liveStockDetails = await _materialAppService.GetLiveStockSales(new GetMaterialLocationWiseStockInput
                //	{
                //		LocationRefId = locationRefId,
                //		EndDate = closingStockDate,
                //		LiveStock = true
                //	});
                //}

                var file = Request.Files[0];

                var data = new ExcelData(file.FileName, file.InputStream);
                var allList = data.GetData();

                var rsMaterials = await _materialRepo.GetAllListAsync();

                var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();
                var rsSupplierMaterialDtos = (from smat in _supplierMaterial.GetAll()
                                              join sup in _supplierRepo.GetAll() on smat.SupplierRefId equals sup.Id
                                              join un in _unitRepo.GetAll() on smat.UnitRefId equals un.Id
                                              select new SupplierMaterialEditDto
                                              {
                                                  Id = smat.Id,
                                                  SupplierRefId = smat.SupplierRefId,
                                                  SupplierRefName = sup.SupplierName,
                                                  MaterialPrice = smat.MaterialPrice,
                                                  MinimumOrderQuantity = smat.MinimumOrderQuantity,
                                                  MaximumOrderQuantity = smat.MaximumOrderQuantity,
                                                  UnitRefId = smat.UnitRefId,
                                                  MaterialRefId = smat.MaterialRefId,
                                                  LastQuoteDate = smat.LastQuoteDate,
                                                  LastQuoteRefNo = smat.LastQuoteRefNo,
                                                  Uom = un.Name,
                                                  YieldPercentage = smat.YieldPercentage,
                                                  SupplierMaterialAliasName = smat.SupplierMaterialAliasName
                                              }).ToList();

                var noOfUnits = 1;
                var fixedColumns = 5;
                var tempDtos = new List<MaterialAdjustmentStockDto>();
                List<MaterialSuccessOrErrorMessageOutput> errorList = new List<MaterialSuccessOrErrorMessageOutput>();
                foreach (var row in allList)
                {
                    string errMessage;
                    rownumber = (rownumber + 1);
                    if (rownumber == 2)
                    {
                        try
                        {
                            noOfUnits = (row.ItemArray.Count() - fixedColumns) / 2;
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }

                    var unitWiseQuantityList = new List<UnitWiseQuantity>();
                    var valueExistFlag = false;
                    for (var uomIndex = 0; uomIndex < noOfUnits; uomIndex++)
                    {
                        _currentMaterialStatus = "OnHand-" + uomIndex;
                        var stronHand = row[fixedColumns-1 + uomIndex * 2 + 1].ToString();
                        // ds.Tables[0].Rows[i][3].ToString();
                        if (stronHand == null || stronHand.Length == 0)
                        {
                            continue;
                        }
                        valueExistFlag = true;
                        var strUom = row[fixedColumns-1 + uomIndex * 2 + 2].ToString();
                        var curunit = rsUnit.FirstOrDefault(t => t.Name.ToUpper().Equals(strUom.ToUpper()));
                        if (curunit == null)
                        {
                            throw new UserFriendlyException(L("Unit") + " " + L("Error") + " " + strUom);
                        }
                        decimal uomQuantityOnHand = 0;
                        if (decimal.TryParse(stronHand, out uomQuantityOnHand))
                        {
                            uomQuantityOnHand = decimal.Parse(stronHand);
                        }
                        else
                        {
                            throw new UserFriendlyException(L("Quantity") + " " + L("Error") + " " + stronHand);
                        }
                        uomQuantityOnHand = Math.Round(uomQuantityOnHand, 6, MidpointRounding.AwayFromZero);
                        if (uomQuantityOnHand < 0)
                        {
                            errMessage = string.Concat(L("OnHandShouldNotBeNegative"));
                            throw new UserFriendlyException(errMessage);
                        }
                        var dtoUnitQty = new UnitWiseQuantity
                        {
                            UnitRefId = curunit.Id,
                            UnitRefName = curunit.Name,
                            OnHand = uomQuantityOnHand
                        };
                        unitWiseQuantityList.Add(dtoUnitQty);
                    }

                    if (valueExistFlag == false)
                        continue;

                    #region Fill Material Details
                    _currentMaterialStatus = "Code";
                    var matid = row[L("Code")].ToString();
                    if (matid == null || matid.Length == 0)
                    {
                        continue;
                    }
                    int materialid = Convert.ToInt16(row[L("Code")]);

                    if (rownumber == 93)
                    {
                        _currentMaterialStatus = "Name";
                    }

                    _currentMaterialStatus = "Name";
                    var matname = row[L("Name")].ToString();
                    if (matname == null || matname.Length == 0)
                    {
                        errMessage = L("MATERIALNAMEISWRONG", matname);
                        throw new UserFriendlyException(errMessage);
                    }
              
                    var mat = rsMaterials.FirstOrDefault(t => t.Id == materialid);
                    if (mat == null)
                    {
                        errMessage = L("MATERIALNAMEISWRONG", matname);
                        errMessage = string.Concat(errMessage);
                        throw new UserFriendlyException(errMessage);
                    }

                    var unit = rsUnit.FirstOrDefault(t => t.Id == mat.DefaultUnitId);
                    if (unit == null)
                    {
                        throw new UserFriendlyException(L("Unit") + " " + L("Error") + " " + matname);
                    }

                    var materialGroupCategory = rsMaterialGroupCategory.FirstOrDefault(t => t.Id == mat.MaterialGroupCategoryRefId);

                    var materialSupplierAliasName = row[L("SupplierMaterialAliasName")].ToString();
                    if (materialSupplierAliasName == null || materialSupplierAliasName.Length == 0)
                    {

                    }
                    #endregion


                    decimal closingstock = 0;
                    if (stockEntryOnly)
                    {
                        closingstock = 0;
                    }
                    else
                    {
                        _currentMaterialStatus = "Verify Stock From Stock Summary";
                        var stk = StockSummary.FirstOrDefault(t => t.MaterialRefId == materialid);
                        if (stk == null)
                        {
                            errMessage = string.Concat(matname + " " + L("NoRecordsFoundInStockSummary"));
                            throw new UserFriendlyException(errMessage);
                        }
                        closingstock = stk.ClBalance;
                    }

                    // *** In future if any client request to account sales portion for forceclose also then we need to uncomment this

                    //if (forceAdjustmentFlag)
                    //{
                    //	var salesDeduction = liveStockDetails.FirstOrDefault(t => t.MaterialRefId == materialid);
                    //	if (salesDeduction != null)
                    //		closingstock = closingstock - salesDeduction.PortionQty;
                    //}

                    _currentMaterialStatus = L("MaterialName") + " : " + matname + " ";

                    var currentStock = closingstock;

                    decimal onHand = 0;

                    foreach (var lst in unitWiseQuantityList)
                    {
                        if (mat.DefaultUnitId == lst.UnitRefId)
                        {
                            lst.Conversion = 1;
                            lst.ConvertedQuantity = lst.OnHand;
                            onHand = onHand + lst.OnHand;
                        }
                        else
                        {
                            var supplierMaterialDtos = rsSupplierMaterialDtos.Where(t => t.MaterialRefId == mat.Id && t.SupplierMaterialAliasName.ToUpper()==materialSupplierAliasName.ToUpper()).ToList();
                            List<UnitConversionListDto> supplierunitConversions = new List<UnitConversionListDto>();
                            foreach (var supplier in supplierMaterialDtos)
                            {
                                var supunitConversion = await _materialAppService.GetConversion(new GetConversionBasedOnBaseUnitRefUnit
                                {
                                    BaseUnitId = lst.UnitRefId,
                                    RefUnitId = mat.DefaultUnitId,
                                    ConversionUnitList = rsUc,
                                    SupplierRefId = supplier.SupplierRefId,
                                    MaterialRefId = mat.Id,
                                });
                                if (supunitConversion!=null)
                                    supplierunitConversions.Add(supunitConversion);
                            }

                            if(supplierunitConversions.Count==0)
                            {
                                var supunitConversion = await _materialAppService.GetConversion(new GetConversionBasedOnBaseUnitRefUnit
                                {
                                    BaseUnitId = lst.UnitRefId,
                                    RefUnitId = mat.DefaultUnitId,
                                    ConversionUnitList = rsUc,
                                    MaterialRefId = mat.Id,
                                });
                                if (supunitConversion != null)
                                    supplierunitConversions.Add(supunitConversion);
                            }

                            UnitConversionListDto unitConversion = null;
                            if (supplierunitConversions.Count == 1)
                            {
                                unitConversion = supplierunitConversions.FirstOrDefault();
                            }
                            else if (supplierunitConversions.Count>1)
                            {
                                List<decimal> convList = supplierunitConversions.Select(t => t.Conversion).Distinct().ToList();
                                if (convList.Count == 1)
                                {
                                    unitConversion = supplierunitConversions.FirstOrDefault();
                                }
                                else
                                {
                                    string supplierList = string.Join(" , ", supplierMaterialDtos.Select(t => t.SupplierRefName).ToList());
                                    string conversionList = string.Join(" , ", convList);
                                    MaterialSuccessOrErrorMessageOutput errDto = new MaterialSuccessOrErrorMessageOutput
                                    {
                                        MaterialRefId = mat.Id,
                                        MaterialRefName = mat.MaterialName,
                                        ErrorMessage = L("MulitplUOMSupplierSameAliasNameConversionError", supplierList, conversionList)

                                    };
                                    errorList.Add(errDto);
                                    continue;
                                }
                            }
                            else if (supplierunitConversions.Count == 0 || unitConversion == null)
                            {
                                MaterialSuccessOrErrorMessageOutput errDto = new MaterialSuccessOrErrorMessageOutput
                                {
                                    MaterialRefId = mat.Id,
                                    MaterialRefName = mat.MaterialName,
                                    ErrorMessage ="Unit Conversion is not mapped correctly"
                                };
                                errorList.Add(errDto);
                                continue;
                            }
                            lst.Conversion = unitConversion.Conversion;
                            lst.ConvertedQuantity = Math.Round(unitConversion.Conversion * lst.OnHand, 14, MidpointRounding.AwayFromZero);
                            onHand = onHand + Math.Round(unitConversion.Conversion * lst.OnHand, 14, MidpointRounding.AwayFromZero);
                            //bool globalConversion = false;
                            //var unitConversion =
                            //    rsUc.FirstOrDefault(
                            //        t => t.BaseUnitId == lst.UnitRefId && t.RefUnitId == mat.DefaultUnitId && t.MaterialRefId == mat.Id);
                            //if (unitConversion != null) // Check conversion based on Supplier Alias Name
                            //{
                            //    var 
                            //    editUnitConversionList = await _unitConversionAppService.GetLinkedUnitCoversionForGivenMaterial(new IdInput { Id = mat.Id });
                            //}
                            //else if (unitConversion == null)
                            //{
                            //    globalConversion = true;
                            //    unitConversion =
                            //    rsUc.FirstOrDefault(
                            //        t => t.BaseUnitId == lst.UnitRefId && t.RefUnitId == mat.DefaultUnitId);
                            //}
                            //if (unitConversion == null)
                            //    throw new UserFriendlyException(L("UnitConversion") + " " + L("NotExist") + " - " +
                            //                                    lst.UnitRefName);
                            //lst.Conversion = unitConversion.Conversion;
                            //lst.ConvertedQuantity = Math.Round(unitConversion.Conversion * lst.OnHand, 6, MidpointRounding.AwayFromZero);
                            //onHand = onHand + Math.Round(unitConversion.Conversion * lst.OnHand, 6, MidpointRounding.AwayFromZero);


                        }
                    }
                    onHand = Math.Round(onHand, 6, MidpointRounding.AwayFromZero);

                    if (onHand < 0)
                    {
                        errMessage = string.Concat(L("OnHandShouldNotBeNegative"));
                        throw new UserFriendlyException(errMessage);
                    }

                    if (currentStock == onHand && currentStock == 0)
                        continue;

                    var diffStockvalue = onHand - currentStock;
                    string diffStatus;
                    if (diffStockvalue < 0)
                        diffStatus = L("Shortage");
                    else if (diffStockvalue == 0)
                        diffStatus = L("Equal");
                    else
                        diffStatus = L("Excess");

                    var stock = new MaterialAdjustmentStockDto
                    {
                        MaterialRefId = mat.Id,
                        MaterialName = mat.MaterialName,
                        MaterialPetName = mat.MaterialPetName,
                        MaterialGroupCategoryRefId = mat.MaterialGroupCategoryRefId,
                        MaterialGroupCategoryRefName = materialGroupCategory.MaterialGroupCategoryName,
                        ClBalance = closingstock, //Convert.ToDecimal(row["ClosingStock"].ToString()),
                        CurrentStock = onHand, // Convert.ToDecimal(row[L("OnHand")]),
                        DifferenceQuantity = Math.Abs(diffStockvalue),
                        AdjustmentStatus = diffStatus,
                        DefaultUnitId = mat.DefaultUnitId,
                        DefaultUnitName = unit.Name,
                        UnitRefId = mat.DefaultUnitId,
                        UnitRefName = unit.Name,
                        UnitWiseQuantityList = unitWiseQuantityList
                    };
                    tempDtos.Add(stock);
                }

                var dtos = new List<MaterialAdjustmentStockDto>();
                var arrMaterialRefIds = tempDtos.Select(t => t.MaterialRefId).Distinct().ToList();
                var arrErrorMaterialRefIds = errorList.Select(t => t.MaterialRefId).Distinct().ToList();
                arrMaterialRefIds = arrMaterialRefIds.Except(arrErrorMaterialRefIds).ToList();
                foreach (var matId in arrMaterialRefIds)
                {
                    string errMessage = "";
                    var gp = tempDtos.Where(t => t.MaterialRefId == matId).ToList();
                    MaterialAdjustmentStockDto consDto = gp.FirstOrDefault();

                    var onHand = gp.Sum(t => t.CurrentStock);
                    var currentStock = gp.FirstOrDefault().ClBalance;

                    if (onHand < 0)
                    {
                        errMessage = string.Concat(L("OnHandShouldNotBeNegative"));
                        throw new UserFriendlyException(errMessage);
                    }

                    if (currentStock == onHand && currentStock == 0)
                        continue;

                    var diffStockvalue = onHand - currentStock;
                    string diffStatus;
                    if (diffStockvalue < 0)
                        diffStatus = L("Shortage");
                    else if (diffStockvalue == 0)
                        diffStatus = L("Equal");
                    else
                        diffStatus = L("Excess");

                    consDto.CurrentStock = onHand;
                    if (gp.Count > 1)
                    {
                        consDto.UnitWiseQuantityList = new List<UnitWiseQuantity>();
                        consDto.UnitWiseQuantityList.Add(new UnitWiseQuantity
                        {
                            OnHand = onHand,
                            UnitRefId = consDto.DefaultUnitId,
                            UnitRefName = consDto.UnitRefName
                        });
                    }
                    dtos.Add(consDto);
                }

                ClosingStockExcelReadOutputDto outputDto = new ClosingStockExcelReadOutputDto
                {
                    MaterialStockList = dtos,
                    ErrorList = errorList
                };
                return Json(new MvcAjaxResponse(outputDto));
                //return dtos;
            }
            catch (Exception ex)
            {
                //throw new UserFriendlyException(ex.Message + ex.InnerException);
                return
                    Json(
                        new MvcAjaxResponse(
                            new ErrorInfo(L("Row") + " : " + rownumber + " " + _currentMaterialStatus + " " + ex.Message +
                                          " " + ex.InnerException)));
            }
        }

    }
}