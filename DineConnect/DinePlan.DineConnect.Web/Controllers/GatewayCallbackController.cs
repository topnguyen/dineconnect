﻿using Abp.Domain.Repositories;
using DinePlan.DineConnect.Addon;
using DinePlan.DineConnect.Addons.QuickBooks;
using DinePlan.DineConnect.Addons.Xero;
using DinePlan.DineConnect.Addons.ZohoBooks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DinePlan.DineConnect.Web.Controllers
{
    [Route("gatewaycallback")]
    public class GatewayCallbackController : Controller
    {
        private readonly IRepository<ConnectAddOn> _addOn;
        private readonly IXeroAppService _xeroService;
        private readonly IQuickBooksAppService _qbAppService;
        private readonly IZohoBooksAppService _zohoBooksAppService;

        public GatewayCallbackController(IRepository<ConnectAddOn> addOn, 
            IXeroAppService xeroService,
            IQuickBooksAppService qbService,
            IZohoBooksAppService zohoBooksAppService)
        {
            _zohoBooksAppService = zohoBooksAppService;
            _qbAppService = qbService;
            _xeroService = xeroService;
            _addOn = addOn;
        }

        [Route("xero")]
        [HttpGet]
        public async Task<ActionResult> Xero(XeroToken token)
        {
            var result = _xeroService.KeepTokenComingFromGateway(token);

            if (await result)
            {
                return Redirect($"/Application/Index#/tenant/addons-xero/");
            }
            return null;
        }

        [Route("quickbooks")]
        [HttpGet]
        public async Task<ActionResult> QuickBooks(QuickBooksToken token)
        {
            var x = token;
            var result = _qbAppService.KeepTokenComingFromGateway(token);
            
            if (await result)
            {
                return Redirect($"/Application/Index#/tenant/addons-qb/");
            }
            
            return null;
        }

        [Route("zohobooks")]
        [HttpGet]
        public async Task<ActionResult> ZohoBooks(ZohoBooksToken token)
        {
            var result = _zohoBooksAppService.KeepTokenComingFromGateway(token);

            if (await result)
            {
                return Redirect($"/Application/Index#/tenant/addons-zohobooks/");
            }

            return null;
        }
    }
}