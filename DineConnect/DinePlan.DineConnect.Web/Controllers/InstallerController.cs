﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DinePlan.DineConnect.Web.Controllers.Extensions;

namespace DinePlan.DineConnect.Web.Controllers
{
    public class InstallerController : DineConnectControllerBase
    {
        public InstallerResult Index(string file)
        {
            if (System.IO.File.Exists(file))
            {
                return new InstallerResult(file);
            }
            string filepath = Path.Combine(Server.MapPath("~/Installer/"), file);
            return new InstallerResult(filepath);
        }
    }

    
}