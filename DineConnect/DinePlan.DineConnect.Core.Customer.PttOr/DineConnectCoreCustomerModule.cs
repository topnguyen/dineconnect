﻿using System.Reflection;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Modules;
using Abp.Zero;
using Abp.Zero.Configuration;
using Abp.Zero.Ldap;
using DinePlan.DineConnect.Authorization.Roles;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Features;
using DinePlan.DineConnect.Notifications;

namespace DinePlan.DineConnect.Core.Customer.PttOr
{
    /// <summary>
    /// Core (domain) module of the application.
    /// </summary>
    [DependsOn(typeof(AbpZeroCoreModule), typeof(AbpZeroLdapModule))]
    public class DineConnectCoreCustomerModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Add/remove localization sources
            //Configuration.Localization.Sources.Add(
            //    new DictionaryBasedLocalizationSource(
            //        "DineConnect",
            //        new XmlEmbeddedFileLocalizationDictionaryProvider(
            //            Assembly.GetExecutingAssembly(),
            //            "DinePlan.DineConnect.Localization.DineConnect"
            //            )
            //        )
            //    );

            //Adding feature providers
            //Configuration.Features.Providers.Add<AppFeatureProvider>();

            //Adding setting providers
            //Configuration.Settings.Providers.Add<AppSettingProvider>();

            //Adding notification providers
            //Configuration.Notifications.Providers.Add<AppNotificationProvider>();

            //Enable this line to create a multi-tenant application.
            //Configuration.MultiTenancy.IsEnabled = true;

            //Enable LDAP authentication (It can be enabled only if MultiTenancy is disabled!)
            //Configuration.Modules.ZeroLdap().Enable(typeof(AppLdapAuthenticationSource));

            //Configure roles
            //AppRoleConfig.Configure(Configuration.Modules.Zero().RoleManagement);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
