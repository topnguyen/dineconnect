﻿using DinePlan.DineConnect.Connect.Users;
using PttDatabaseCommon.Data;

namespace PttInboundConsole.Data
{
    public interface IDinePlanUserRepository : IRepository<DinePlanUser, int>
    {
    }
}