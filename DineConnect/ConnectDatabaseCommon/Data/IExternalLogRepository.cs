﻿using DinePlan.DineConnect.Connect.Audit;
using PttDatabaseCommon.Data;

namespace PttInboundConsole.Data
{
    public interface IExternalLogRepository : IRepository<ExternalLog, int>
    {
    }
}