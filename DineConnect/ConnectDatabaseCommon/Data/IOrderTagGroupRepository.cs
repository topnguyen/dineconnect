﻿using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.MultiLingual;
using DinePlan.DineConnect.Connect.OrderTags;
using PttDatabaseCommon.Data;

namespace PttInboundConsole.Data
{
    public interface IOrderTagGroupRepository : IRepository<OrderTagGroup, int>
    {
    }

    public interface IOrderTagRepository : IRepository<OrderTag, int>
    {
    }

    public interface IOrderTagMapRepository : IRepository<OrderTagMap, int>
    {
    }
}