﻿using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.MultiLingual;
using PttDatabaseCommon.Data;

namespace PttInboundConsole.Data
{
    public interface IMenuItemRepository : IRepository<MenuItem, int>
    {
    }

    public interface ILocationMenuItemRepository : IRepository<LocationMenuItem, int>
    {
    }

    public interface ILanguageDescriptionRepository : IRepository<LanguageDescription, int>
    {
    }

    public interface ILocationMenuItemPriceRepository : IRepository<LocationMenuItemPrice, int>
    {
    }
    public interface IProductComboRepository : IRepository<ProductCombo, int>
    {
    }
    public interface IProductComboItemRepository : IRepository<ProductComboItem, int>
    {
    }
}