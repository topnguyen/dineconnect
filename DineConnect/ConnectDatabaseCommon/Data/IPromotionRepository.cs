﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Period;
using DinePlan.DineConnect.Hr;
using PttDatabaseCommon.Data;

namespace PttInboundConsole.Data
{
   

    public interface IPromotionCategoryRepository : IRepository<PromotionCategory, int>
    {
    }

    public interface IPromotionRepository : IRepository<Promotion, int>
    {
    }
    public interface ITimerPromotionRepository : IRepository<TimerPromotion, int>
    {
    }
    public interface IPriceTagDefinitionRepository : IRepository<PriceTagDefinition, int>
    {
    }

    public interface IPriceTagRepository : IRepository<PriceTag, int>
    {
    }

    public interface IPriceTagLocationRepository : IRepository<PriceTagLocationPrice, int>
    {
    }
    public interface IUpMenuItemRepository : IRepository<UpMenuItem, int>
    {
    }

    public interface IUpMenuItemLocationPriceRepository : IRepository<UpMenuItemLocationPrice, int>
    {
    }
    public interface IWorkDayRepository : IRepository<WorkDay, int>
    {
    }

    public interface IWorkPeriodRepository : IRepository<WorkPeriod, int>
    {
    }
}
