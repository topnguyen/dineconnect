﻿using DinePlan.DineConnect.Connect.Users;
using PttDatabaseCommon.Data;

namespace PttInboundConsole.Data
{
    public interface IDinePlanUserRoleRepository : IRepository<DinePlanUserRole, int>
    {
    }
}