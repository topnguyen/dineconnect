﻿using System;
using PttInboundConsole.Data;

namespace PttDatabaseCommon.Data
{
    public interface IUnitOfWork : IDisposable
    {
        IExternalLogRepository ExternalLogs { get; }
        ICompanyRepository Companies{ get; }

        ILocationRepository Locations { get; }
        ILocationBranchRepository LocationBranches { get; }
        ILocationGroupRepository LocationGroups { get; }
        ICategoryRepository Categories { get; }
        IProductGroupRepository ProductGroups { get; }
        IMenuItemRepository MenuItems { get; }

        IDepartmentRepository Departments { get; }
        IDinePlanUserRepository DinePlanUsers { get; }
        IDinePlanUserRoleRepository DinePlanUserRoles { get; }

        ILocationMenuItemRepository LocationItems { get; }
        ILanguageDescriptionRepository LangaugeDescriptions { get; }
        ILocationMenuItemPriceRepository LocationMenuItemPrices { get; }

        IProductComboRepository ProductComboes { get; }
        IProductComboItemRepository ProductComboItems { get; }

        IOrderTagGroupRepository OrderTagGroups { get; }
        IOrderTagRepository OrderTags { get; }
        IOrderTagMapRepository OrderTagMaps { get; }

        ISettingRepository Settings { get; }
        IPromotionCategoryRepository PromotionCategories { get; }
        IPromotionRepository Promotions { get; }
        ITimerPromotionRepository TimerPromotions { get; }

        IPriceTagDefinitionRepository PriceTagDefinitions { get; }
        IPriceTagRepository PriceTags { get; }
        IPriceTagLocationRepository PriceTagLocations { get; }
        IUpMenuItemRepository UpMenuItems { get; }
        IUpMenuItemLocationPriceRepository UpMenuItemLocationPrices { get; }

        IFullTaxMemberRepository FullTaxMembers { get; }
        IFullTaxAddressRepository FullTaxAddresses { get; }

        ITicketTagGroupRepository TicketTagGroups { get; }
        ITicketTagRepository TicketTags{ get; }

        ITicketRepository Tickets { get; }
        IOrderRepository Orders { get; }
        IPaymentRepository Payments { get; }
        ITicketTransactionRepository TicketTransactions { get; }
        IPaymentTypeRepository PaymentTypes{ get; }
        IWorkDayRepository WorkDays { get; }
        IWorkPeriodRepository WorkPeriods { get; }

        IGiftVoucherRepository GiftVouchers { get; }

        IGiftVoucherTypeRepository GiftVoucherTypes { get; }

        int Complete();
    }
}