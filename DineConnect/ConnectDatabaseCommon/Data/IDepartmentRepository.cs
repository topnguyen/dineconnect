﻿using DinePlan.DineConnect.Connect.Master;
using PttDatabaseCommon.Data;

namespace PttInboundConsole.Data
{
    public interface IDepartmentRepository : IRepository<Department, int>
    {
    }
}