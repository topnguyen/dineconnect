﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace PttDatabaseCommon.Data
{
    public interface IRepository<TEntity, TPrimaryKey> where TEntity : class
    {
        TEntity Get(TPrimaryKey id);

        IEnumerable<TEntity> GetAll();

        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        IQueryable<TEntity> FindQuery(Expression<Func<TEntity, bool>> predicate);


        void Add(TEntity entity);
        void Update(TEntity entity);

        void AddRange(IEnumerable<TEntity> entities);

        void Remove(TEntity entity);

        void RemoveRange(IEnumerable<TEntity> entities);
    }

    public interface IRepository<TEntity> : IRepository<TEntity, long> where TEntity : class
    {
    }
}