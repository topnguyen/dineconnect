﻿using DinePlan.DineConnect.Connect.Invoice;

namespace PttDatabaseCommon.Data
{
    public interface IFullTaxMemberRepository : IRepository<FullTaxMember, int>
    {
    }
}