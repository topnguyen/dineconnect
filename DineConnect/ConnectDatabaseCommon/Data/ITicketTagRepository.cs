﻿using DinePlan.DineConnect.Connect.Tag;

namespace PttDatabaseCommon.Data
{
   

    public interface ITicketTagGroupRepository : IRepository<TicketTagGroup, int>
    {
    }

    public interface ITicketTagRepository : IRepository<TicketTag, int>
    {
    }

   
}