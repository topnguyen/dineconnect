﻿using System.Data.Entity;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Invoice;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.MultiLingual;
using DinePlan.DineConnect.Connect.OrderTags;
using DinePlan.DineConnect.Connect.Period;
using DinePlan.DineConnect.Connect.Tag;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.Users;
using DinePlan.DineConnect.Engage.Gift;
using DinePlan.DineConnect.Hr;

namespace ConnectDatabaseCommon.Data
{
    public class PttInboundDbContext : DbContext
    {
        public virtual DbSet<Company> Companies{ get; set; }
        public virtual DbSet<LocationGroup> LocationGroups { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<MenuItem> MenuItems { get; set; }
        public virtual DbSet<MenuItemPortion> MenuItemPortions { get; set; }
        public virtual DbSet<DinePlanUser> DinePlanUsers { get; set; }
        public virtual DbSet<DinePlanUserRole> DinePlanUserRoles{ get; set; }
        public virtual DbSet<LanguageDescription> LanguageDescriptions{ get; set; }
        public virtual DbSet<ProductCombo> ProductComboes{ get; set; }
        public virtual DbSet<ProductComboItem> ProductComboItems{ get; set; }
        public virtual DbSet<ExternalLog> ExteralLogs{ get; set; }

        public virtual DbSet<OrderTagGroup> OrderTagGroups{ get; set; }
        public virtual DbSet<OrderTag> OrderTags{ get; set; }
        public virtual DbSet<OrderTagMap> OrderTagMaps{ get; set; }

        public virtual DbSet<PromotionCategory> PromotionCategories{ get; set; }
        public virtual DbSet<Promotion> Promotions{ get; set; }
        public virtual DbSet<TimerPromotion> TimerPromotions{ get; set; }
        public virtual DbSet<PriceTagDefinition> PriceTagDefinitions{ get; set; }
        public virtual DbSet<PriceTag> PriceTags{ get; set; }
        public virtual DbSet<PriceTagLocationPrice> PriceTagLocationPrices{ get; set; }
        public virtual DbSet<UpMenuItem> UpMenuItems{ get; set; }
        public virtual DbSet<UpMenuItemLocationPrice> UpMenuItemLocationPrices{ get; set; }

        public virtual DbSet<FullTaxMember> FullTaxMembers{ get; set; }
        public virtual DbSet<FullTaxAddress> FullTaxAddress{ get; set; }

        public virtual DbSet<TicketTagGroup> TicketTagGroups { get; set; }
        public virtual DbSet<TicketTag> TicketTags { get; set; }
        public virtual DbSet<WorkDay> WorkDays { get; set; }
        public virtual DbSet<WorkPeriod> WorkPeriods { get; set; }

        public virtual IDbSet<GiftVoucher> GiftVouchers { get; set; }

        public virtual IDbSet<GiftVoucherType> GiftVoucherTypes { get; set; }

        public PttInboundDbContext(): base("Default")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ProductCombo>()
                .HasMany<ProductComboGroup>(s => s.ComboGroups)
                .WithMany(c => c.ProductCombos)
                .Map(cs =>
                {
                    cs.MapLeftKey("ProductComboId");
                    cs.MapRightKey("ProductComboGroupId");
                    cs.ToTable("ProductComboGroupDetails");
                });

            modelBuilder.Entity<Category>().Ignore(e => e.Locations);
            modelBuilder.Entity<Order>().Ignore(e => e.MenuItemPortion);
            modelBuilder.Entity<Ticket>().Ignore(e => e.Location);
        }
    }
}