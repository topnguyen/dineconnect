﻿using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.MultiLingual;
using PttDatabaseCommon.Data;
using PttDatabaseCommon.Data.Repositories;

namespace PttInboundConsole.Data.Repositories
{
    public class MenuItemRepository : Repository<MenuItem, int>, IMenuItemRepository
    {
        public MenuItemRepository(PttInboundDbContext context) : base(context)
        {
        }
    }

    public class LocationMenuItemRepository : Repository<LocationMenuItem, int>, ILocationMenuItemRepository
    {
        public LocationMenuItemRepository(PttInboundDbContext context) : base(context)
        {
        }
    }

    public class LanguageDescriptionRepository : Repository<LanguageDescription, int>, ILanguageDescriptionRepository
    {
        public LanguageDescriptionRepository(PttInboundDbContext context) : base(context)
        {
        }
    }

    public class LocationMenuItemPriceRepository : Repository<LocationMenuItemPrice, int>, ILocationMenuItemPriceRepository
    {
        public LocationMenuItemPriceRepository(PttInboundDbContext context) : base(context)
        {
        }
    }

    public class ProductComboRepository : Repository<ProductCombo, int>, IProductComboRepository
    {
        public ProductComboRepository(PttInboundDbContext context) : base(context)
        {
        }
    }

    public class ProductComboItemRepository : Repository<ProductComboItem, int>, IProductComboItemRepository
    {
        public ProductComboItemRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
}