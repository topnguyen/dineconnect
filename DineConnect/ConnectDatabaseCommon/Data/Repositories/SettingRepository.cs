﻿using Abp.Configuration;
using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Master;
using PttDatabaseCommon.Data;
using PttDatabaseCommon.Data.Repositories;

namespace PttInboundConsole.Data.Repositories
{
    public class SettingRepository : Repository<Setting, int>, ISettingRepository
    {
        public SettingRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
}