﻿using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Audit;
using PttDatabaseCommon.Data;
using PttDatabaseCommon.Data.Repositories;

namespace PttInboundConsole.Data.Repositories
{
    public class ExternalLogRepository : Repository<ExternalLog, int>, IExternalLogRepository
    {
        public ExternalLogRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
}