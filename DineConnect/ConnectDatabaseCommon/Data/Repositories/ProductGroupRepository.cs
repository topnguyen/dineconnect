﻿using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Master;
using PttDatabaseCommon.Data;
using PttDatabaseCommon.Data.Repositories;

namespace PttInboundConsole.Data.Repositories
{
    public class ProductGroupRepository : Repository<ProductGroup, int>, IProductGroupRepository
    {
        public ProductGroupRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
}