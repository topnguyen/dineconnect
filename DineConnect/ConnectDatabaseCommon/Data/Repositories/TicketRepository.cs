﻿using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Period;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Hr;
using PttInboundConsole.Data;
using PttInboundConsole.Data.Repositories;

namespace PttDatabaseCommon.Data.Repositories
{
    public class TicketRepository : Repository<Ticket, int>, ITicketRepository
    {
        public TicketRepository(PttInboundDbContext context) : base(context)
        {
        }
    }

    public class OrderRepository : Repository<Order, int>, IOrderRepository
    {
        public OrderRepository(PttInboundDbContext context) : base(context)
        {
        }
    }

    public class PaymentRepository : Repository<Payment, int>, IPaymentRepository
    {
        public PaymentRepository(PttInboundDbContext context) : base(context)
        {
        }
    }

    public class TicketTransactionRepository : Repository<TicketTransaction, int>, ITicketTransactionRepository
    {
        public TicketTransactionRepository(PttInboundDbContext context) : base(context)
        {
        }
    }

    public class PaymentTypeRepository : Repository<PaymentType, int>, IPaymentTypeRepository
    {
        public PaymentTypeRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
    
    public class WorkDayRepository : Repository<WorkDay, int>, IWorkDayRepository
    {
        public WorkDayRepository(PttInboundDbContext context) : base(context)
        {
        }
    }


    public class WorkPeriodRepository : Repository<WorkPeriod, int>, IWorkPeriodRepository
    {
        public WorkPeriodRepository(PttInboundDbContext context) : base(context)
        {
        }
    }


}