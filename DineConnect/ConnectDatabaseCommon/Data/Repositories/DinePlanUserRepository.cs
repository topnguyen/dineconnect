﻿using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Users;
using PttDatabaseCommon.Data;
using PttDatabaseCommon.Data.Repositories;

namespace PttInboundConsole.Data.Repositories
{
    public class DinePlanUserRepository : Repository<DinePlanUser, int>, IDinePlanUserRepository
    {
        public DinePlanUserRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
}