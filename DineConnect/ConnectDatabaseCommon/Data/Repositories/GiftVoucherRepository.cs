﻿using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Engage.Gift;
using PttDatabaseCommon.Data;
using PttDatabaseCommon.Data.Repositories;

namespace PttInboundConsole.Data.Repositories
{
    public class GiftVoucherRepository : Repository<GiftVoucher, int>, IGiftVoucherRepository
    {
        public GiftVoucherRepository(PttInboundDbContext context) : base(context)
        {
        }
    }

    public class GiftVoucherTypeRepository : Repository<GiftVoucherType, int>, IGiftVoucherTypeRepository
    {
        public GiftVoucherTypeRepository(PttInboundDbContext context) : base(context)
        {
        }
    }

}