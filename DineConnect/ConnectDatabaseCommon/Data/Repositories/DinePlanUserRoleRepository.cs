﻿using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Users;
using PttDatabaseCommon.Data;
using PttDatabaseCommon.Data.Repositories;

namespace PttInboundConsole.Data.Repositories
{
    public class DinePlanUserRoleRepository : Repository<DinePlanUserRole, int>, IDinePlanUserRoleRepository
    {
        public DinePlanUserRoleRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
}