﻿using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Master;
using PttDatabaseCommon.Data;
using PttDatabaseCommon.Data.Repositories;

namespace PttInboundConsole.Data.Repositories
{
    public class CompanyRepository : Repository<Company, int>, ICompanyRepository
    {
        public CompanyRepository(PttInboundDbContext context) : base(context)
        {
        }
    }

    public class LocationRepository : Repository<Location, int>, ILocationRepository
    {
        public LocationRepository(PttInboundDbContext context) : base(context)
        {
        }
    }

    public class LocationBranchRepository : Repository<LocationBranch, int>, ILocationBranchRepository
    {
        public LocationBranchRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
}