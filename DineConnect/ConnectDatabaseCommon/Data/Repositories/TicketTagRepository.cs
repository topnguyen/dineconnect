﻿using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Tag;
using PttInboundConsole.Data.Repositories;

namespace PttDatabaseCommon.Data.Repositories
{
   

    public class TicketTagGroupRepository : Repository<TicketTagGroup, int>, ITicketTagGroupRepository
    {
        public TicketTagGroupRepository(PttInboundDbContext context) : base(context)
        {
        }
    }

    public class TicketTagRepository : Repository<TicketTag, int>, ITicketTagRepository
    {
        public TicketTagRepository(PttInboundDbContext context) : base(context)
        {
        }
    }

   
}