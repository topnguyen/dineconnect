﻿using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Invoice;
using PttInboundConsole.Data.Repositories;

namespace PttDatabaseCommon.Data.Repositories
{
    public class FullTaxMemberRepository : Repository<FullTaxMember, int>, IFullTaxMemberRepository
    {
        public FullTaxMemberRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
}