﻿using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Master;
using PttDatabaseCommon.Data;
using PttDatabaseCommon.Data.Repositories;

namespace PttInboundConsole.Data.Repositories
{
    public class DepartmentRepository : Repository<Department, int>, IDepartmentRepository
    {
        public DepartmentRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
}