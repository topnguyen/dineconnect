﻿using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Master;
using PttDatabaseCommon.Data;
using PttDatabaseCommon.Data.Repositories;

namespace PttInboundConsole.Data.Repositories
{
    public class CategoryRepository : Repository<Category, int>, ICategoryRepository
    {
        public CategoryRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
}