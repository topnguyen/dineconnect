﻿using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Master;
using PttInboundConsole.Data;
using PttInboundConsole.Data.Repositories;

namespace PttDatabaseCommon.Data.Repositories
{
    public class LocationGroupRepository : Repository<LocationGroup, int>, ILocationGroupRepository
    {
        public LocationGroupRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
}