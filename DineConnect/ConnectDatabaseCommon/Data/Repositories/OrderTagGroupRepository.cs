﻿using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.OrderTags;
using PttInboundConsole.Data;
using PttInboundConsole.Data.Repositories;

namespace PttDatabaseCommon.Data.Repositories
{
    public class OrderTagGroupRepository : Repository<OrderTagGroup, int>, IOrderTagGroupRepository
    {
        public OrderTagGroupRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
    public class OrderTagRepository : Repository<OrderTag, int>, IOrderTagRepository
    {
        public OrderTagRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
    public class OrderTagMapRepository : Repository<OrderTagMap, int>, IOrderTagMapRepository
    {
        public OrderTagMapRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
}