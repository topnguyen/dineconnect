﻿using Abp.Domain.Repositories;
using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using PttDatabaseCommon.Data;
using PttDatabaseCommon.Data.Repositories;

namespace PttInboundConsole.Data.Repositories
{
    public class PromotionRepository : Repository<Promotion, int>, IPromotionRepository
    {
        public PromotionRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
    public class TimerPromotionRepository : Repository<TimerPromotion, int>, ITimerPromotionRepository
    {
        public TimerPromotionRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
    public class PromotionCategoryRepository : Repository<PromotionCategory, int>, IPromotionCategoryRepository
    {
        public PromotionCategoryRepository(PttInboundDbContext context) : base(context)
        {
        }
    }

    public class PriceTagDefinitionRepository : Repository<PriceTagDefinition, int>, IPriceTagDefinitionRepository
    {
        public PriceTagDefinitionRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
    public class PriceTagRepository : Repository<PriceTag, int>, IPriceTagRepository
    {
        public PriceTagRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
    public class PriceTagLocationRepository : Repository<PriceTagLocationPrice, int>, IPriceTagLocationRepository
    {
        public PriceTagLocationRepository(PttInboundDbContext context) : base(context)
        {
        }
    }

    public class UpMenuItemRepository : Repository<UpMenuItem, int>, IUpMenuItemRepository
    {
        public UpMenuItemRepository(PttInboundDbContext context) : base(context)
        {
        }
    }

    public class UpMenuItemLocationPriceRepository : Repository<UpMenuItemLocationPrice, int>, IUpMenuItemLocationPriceRepository
    {
        public UpMenuItemLocationPriceRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
}