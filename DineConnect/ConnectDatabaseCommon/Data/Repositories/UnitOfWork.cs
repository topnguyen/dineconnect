﻿using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Period;
using DinePlan.DineConnect.Hr;
using PttInboundConsole.Data;
using PttInboundConsole.Data.Repositories;

namespace PttDatabaseCommon.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly PttInboundDbContext _context;

        public UnitOfWork(PttInboundDbContext context)
        {
            _context = context;

            ExternalLogs = new ExternalLogRepository(context);

            Companies = new CompanyRepository(context);
            LocationGroups = new LocationGroupRepository(context);
            LocationBranches = new LocationBranchRepository(context);
            Locations = new LocationRepository(context);
            Categories = new CategoryRepository(context);
            ProductGroups = new ProductGroupRepository(context);

            Departments = new DepartmentRepository(context);
            MenuItems = new MenuItemRepository(context);
            DinePlanUsers = new DinePlanUserRepository(context);
            DinePlanUserRoles = new DinePlanUserRoleRepository(context);

            LocationItems = new LocationMenuItemRepository(context);
            LangaugeDescriptions = new LanguageDescriptionRepository(context);
            LocationMenuItemPrices = new LocationMenuItemPriceRepository(context);
            ProductComboes = new ProductComboRepository(context);
            ProductComboItems = new ProductComboItemRepository(context);

            OrderTagGroups = new OrderTagGroupRepository(context);
            OrderTags = new OrderTagRepository(context);
            OrderTagMaps = new OrderTagMapRepository(context);
            Settings = new SettingRepository(context);

            PromotionCategories = new PromotionCategoryRepository(context);
            Promotions = new PromotionRepository(context);
            TimerPromotions = new TimerPromotionRepository(context);

            PriceTagDefinitions = new PriceTagDefinitionRepository(context);
            PriceTags = new PriceTagRepository(context);
            PriceTagLocations = new PriceTagLocationRepository(context);

            UpMenuItems = new UpMenuItemRepository(context);
            UpMenuItemLocationPrices = new UpMenuItemLocationPriceRepository(context);
            
            FullTaxMembers = new FullTaxMemberRepository(context);
            FullTaxAddresses = new FullTaxAddressRepository(context);
            TicketTagGroups = new TicketTagGroupRepository(context);
            TicketTags = new TicketTagRepository(context);

            Tickets = new TicketRepository(context);
            Orders = new OrderRepository(context);
            Payments = new PaymentRepository(context);
            TicketTransactions = new TicketTransactionRepository(context);
            TicketTagGroups = new TicketTagGroupRepository(context);
            PaymentTypes  = new PaymentTypeRepository(context);
            WorkDays  = new WorkDayRepository(context);
            WorkPeriods  = new WorkPeriodRepository(context);
            GiftVouchers = new GiftVoucherRepository(context);
            GiftVoucherTypes = new GiftVoucherTypeRepository(context);

        }
        public IFullTaxMemberRepository FullTaxMembers { get; }
        public IFullTaxAddressRepository FullTaxAddresses { get; }

        public IExternalLogRepository ExternalLogs { get; }

        public ICompanyRepository Companies { get; }

        public ILocationRepository Locations { get; }
        public ILocationBranchRepository LocationBranches { get; }
        public ILocationGroupRepository LocationGroups { get; }
        public ICategoryRepository Categories { get; }
        public IProductGroupRepository ProductGroups { get; }
        public IMenuItemRepository MenuItems { get; }
        public IDepartmentRepository Departments { get; }
        public IDinePlanUserRepository DinePlanUsers { get; }
        public IDinePlanUserRoleRepository DinePlanUserRoles { get; }
        public ILocationMenuItemRepository LocationItems { get; }
        public ILanguageDescriptionRepository LangaugeDescriptions { get; }
        public ILocationMenuItemPriceRepository LocationMenuItemPrices { get; }
        public IProductComboRepository ProductComboes { get;  }
        public IProductComboItemRepository ProductComboItems { get;  }

        public IOrderTagGroupRepository OrderTagGroups { get;  }
        public IOrderTagRepository OrderTags { get;  }
        public IOrderTagMapRepository OrderTagMaps { get;  }
        public ISettingRepository Settings{ get;  }
        public IPromotionCategoryRepository PromotionCategories{ get;  }
        public IPromotionRepository Promotions { get;  }
        public ITimerPromotionRepository TimerPromotions { get;  }

        public IPriceTagDefinitionRepository PriceTagDefinitions { get; }
        public IPriceTagRepository PriceTags { get; }
        public IPriceTagLocationRepository PriceTagLocations { get; }
        public IUpMenuItemRepository UpMenuItems { get; }
        public IUpMenuItemLocationPriceRepository UpMenuItemLocationPrices { get; }

        public ITicketTagGroupRepository TicketTagGroups { get; }
        public ITicketTagRepository TicketTags { get; }

        public ITicketRepository  Tickets {get;}
        public IOrderRepository  Orders {get;}
        public IPaymentRepository  Payments {get;}
        public ITicketTransactionRepository  TicketTransactions {get;}
        public IPaymentTypeRepository  PaymentTypes {get;}
        public IWorkDayRepository WorkDays {get;}
        public IWorkPeriodRepository WorkPeriods {get;}

        public IGiftVoucherRepository GiftVouchers { get; }

        public IGiftVoucherTypeRepository GiftVoucherTypes { get; }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}