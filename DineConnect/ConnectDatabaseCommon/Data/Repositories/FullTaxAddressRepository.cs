﻿using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Invoice;
using PttInboundConsole.Data.Repositories;

namespace PttDatabaseCommon.Data.Repositories
{
    public class FullTaxAddressRepository : Repository<FullTaxAddress, int>, IFullTaxAddressRepository
    {
        public FullTaxAddressRepository(PttInboundDbContext context) : base(context)
        {
        }
    }
}