﻿using Abp.Configuration;
using PttInboundConsole.Data;

namespace PttDatabaseCommon.Data
{
    public interface ISettingRepository : IRepository<Setting, int>
    {
    }

    
}