﻿using DinePlan.Infrastructure.ExceptionReporter;

namespace PttDatabaseCommon.Data.Exception
{
    public static class DinePlanLogger
    {
        public static string Log(params System.Exception[] exception)
        {
            var ri = new ExceptionReportInfo();
            ri.SetExceptions(exception);
            var rg = new ExceptionReportGenerator(ri);
            return rg.CreateExceptionReport();
        }
    }
}
