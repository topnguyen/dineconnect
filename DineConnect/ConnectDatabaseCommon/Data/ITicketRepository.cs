﻿using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Transaction;

namespace PttDatabaseCommon.Data
{
    public interface ITicketRepository : IRepository<Ticket, int>
    {
    }

    public interface IOrderRepository : IRepository<Order, int>
    {
    }


    public interface IPaymentRepository : IRepository<Payment, int>
    {
    }

    public interface ITicketTransactionRepository : IRepository<TicketTransaction, int>
    {
    }

  

    public interface IPaymentTypeRepository : IRepository<PaymentType, int>
    {
    }

  
}