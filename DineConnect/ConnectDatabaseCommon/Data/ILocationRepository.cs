﻿using DinePlan.DineConnect.Connect.Master;
using PttDatabaseCommon.Data;

namespace PttInboundConsole.Data
{
    public interface ICompanyRepository : IRepository<Company, int>
    {
    }
    public interface ILocationRepository : IRepository<Location, int>
    {
    }

    public interface ILocationBranchRepository : IRepository<LocationBranch, int>
    {
    }
}