﻿using DinePlan.DineConnect.Connect.Master;
using PttDatabaseCommon.Data;

namespace PttInboundConsole.Data
{
    public interface ICategoryRepository : IRepository<Category, int>
    {
    }
}