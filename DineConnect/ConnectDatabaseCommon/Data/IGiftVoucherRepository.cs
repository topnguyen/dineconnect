﻿using DinePlan.DineConnect.Engage.Gift;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PttDatabaseCommon.Data
{
    public interface IGiftVoucherRepository : IRepository<GiftVoucher, int>
    {
    }

    public interface IGiftVoucherTypeRepository : IRepository<GiftVoucherType, int>
    {
    }
}
