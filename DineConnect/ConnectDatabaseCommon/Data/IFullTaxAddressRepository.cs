﻿using DinePlan.DineConnect.Connect.Invoice;

namespace PttDatabaseCommon.Data
{
    public interface IFullTaxAddressRepository : IRepository<FullTaxAddress, int>
    {
    }
}