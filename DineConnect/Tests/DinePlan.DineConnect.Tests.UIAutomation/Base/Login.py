from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
from datetime import datetime
import platform
import time
import sys
import re

def WithPwd(driver, tenancy, user, pwd):
    WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body > div.content > form > div.form-group > input'))).send_keys(tenancy)
    
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body > div.content > form > div:nth-child(3) > div:nth-child(1) > input'))).send_keys(user)

    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body > div.content > form > div:nth-child(3) > div:nth-child(2) > input'))).send_keys(pwd)

    WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body > div.content > form > div:nth-child(3) > div:nth-child(3) > button'))).click()