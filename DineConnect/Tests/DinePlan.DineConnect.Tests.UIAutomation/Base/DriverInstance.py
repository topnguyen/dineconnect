from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import platform

def Create():
    driver = None
    
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_experimental_option('excludeSwitches', ['enable-logging'])
    chrome_options.add_argument('--disable-features=EnableEphemeralFlashPermission')
    chrome_options.add_argument('--mute-audio')

    desired_capabilities = DesiredCapabilities.CHROME
    desired_capabilities['pageLoadStrategy'] = 'eager'

    if platform.system() == 'Windows':
        driver = webdriver.Chrome('./chromedriver.exe', options=chrome_options)
    else:
        driver = webdriver.Chrome('./chromedriver', options=chrome_options)

    driver.set_page_load_timeout(60)
    driver.set_script_timeout(60)

    return driver