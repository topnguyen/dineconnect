from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
from datetime import datetime
import platform
import time
import sys
import re
import random
from termcolor import colored

def Test(driver):
    should_create_organization_success(driver)

def should_create_organization_success(driver):
    connectNav = WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body > div.page-container > div.page-sidebar-wrapper.page-sidebar-menu-closed.ng-scope > div > ul > li:nth-child(2) > a')))
    actionChains = ActionChains(driver)
    actionChains.move_to_element(connectNav).perform()

    WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body > div.page-container > div.page-sidebar-wrapper.page-sidebar-menu-closed.ng-scope > div > ul > li:nth-child(2) > ul > li.ng-scope.start > a'))).click()

    WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body > div.page-container > div.page-sidebar-wrapper.page-sidebar-menu-closed.ng-scope > div > ul > li:nth-child(2) > ul > li.ng-scope.start.open > ul > li.ng-scope.start > a'))).click()

    WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body > div.page-container > div.page-content-wrapper > div > div > div > div > div.row.margin-bottom-5 > div.col-xs-6.text-right > button.btn.btn-primary.blue.ng-scope'))).click()

    code = RandomStr(5)
    name = RandomStr(10)

    WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body > div.page-container > div.page-content-wrapper > div > div > div > div > form > div.portlet.light > div.portlet-body > div.tab-container.tabbable-line.ng-isolate-scope > div > div.tab-pane.ng-scope.active > div:nth-child(1) > div > div > input'))).send_keys(code)

    WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body > div.page-container > div.page-content-wrapper > div > div > div > div > form > div.portlet.light > div.portlet-body > div.tab-container.tabbable-line.ng-isolate-scope > div > div.tab-pane.ng-scope.active > div:nth-child(2) > div > div > input'))).send_keys(name)

    WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body > div.page-container > div.page-content-wrapper > div > div > div > div > form > div.portlet.light > div.modal-footer > button.btn.btn-primary.blue.ng-isolate-scope'))).click()

    toastMessge = WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#toast-container > div > div')))
    assert 'successfully' in toastMessge.text, 'UIAutomation Test -> create organization [FAILURE]'
    print(colored('UIAutomation Test -> create organization [PASS]', 'green'))

def RandomStr(num):
    sourceChar = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    salt = ''
    for i in range(num):
        salt += random.choice(sourceChar)
    return salt