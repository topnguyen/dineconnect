import Base.DriverInstance
import Base.Login
import Specs.OrganizationSpec

if __name__ == "__main__":
    driver = Base.DriverInstance.Create()
    driver.get('http://localhost')

    Base.Login.WithPwd(driver, 'Megumi', 'admin', '123qwe')
    
    Specs.OrganizationSpec.Test(driver)

    driver.quit()