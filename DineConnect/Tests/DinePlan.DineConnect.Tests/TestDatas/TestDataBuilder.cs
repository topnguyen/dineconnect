﻿using EntityFramework.DynamicFilters;
using DinePlan.DineConnect.EntityFramework;

namespace DinePlan.DineConnect.Tests.TestDatas
{
    public class TestDataBuilder
    {
        private readonly DineConnectDbContext _context;

        public TestDataBuilder(DineConnectDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new TestOrganizationUnitsBuilder(_context).Create();

            _context.SaveChanges();
        }
    }
}
