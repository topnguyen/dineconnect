﻿using Abp.Modules;
using Abp.Zero.Configuration;

namespace DinePlan.DineConnect.Tests
{
    [DependsOn(
        typeof(DineConnectApplicationModule),
        typeof(DineConnectDataModule))]
    public class DineConnectTestModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Use database as language management
            Configuration.Modules.Zero().LanguageManagement.EnableDbLocalization();
        }
    }
}
