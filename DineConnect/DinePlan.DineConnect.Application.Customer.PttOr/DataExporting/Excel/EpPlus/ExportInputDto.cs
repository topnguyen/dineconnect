﻿using DinePlan.DineConnect.Application.Customer.PttOr.Exporter;
using OpenHtmlToPdf;

namespace DinePlan.DineConnect.Application.Customer.PttOr.DataExporting.Excel.EpPlus
{
    public interface IFileExport
    {
        ExportType ExportOutputType { get; set; }
        PaperSize PaperSize { get; set; }
        bool Portrait { get; set; }
    }

    public interface IBackgroundExport
    {
        bool RunInBackground { get; set; }
        string ReportDescription { get; set; }

    }
}
