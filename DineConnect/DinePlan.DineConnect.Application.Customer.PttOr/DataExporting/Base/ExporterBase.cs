﻿using System;
using System.Collections.Generic;
using System.IO;
using Abp.Collections.Extensions;
using Abp.Dependency;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace DinePlan.DineConnect.Application.Customer.PttOr.DataExporting.Base
{
    public abstract class ExporterBase : DineConnectServiceBase, ITransientDependency
    {
        public IAppFolders AppFolders { get; set; }

    }
}