﻿using System;

namespace DinePlan.DineConnect.Application.Customer.PttOr.DataExporting.Pdf
{
    public class PdfReportDto
    {
        public string Name { get; set; }
        public string ReportUser { get; set; }

        public string ReportDate { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }

        public PdfReportDto()
        {
            ReportDate = DateTime.Now.ToLongDateString();
        }
    }
}