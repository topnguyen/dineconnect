﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Application.Customer.PttOr.Dto
{
    public class PagedAndSortedInputDto : PagedInputDto, ISortedResultRequest
    {
        public string Sorting { get; set; }

        public PagedAndSortedInputDto()
        {
            MaxResultCount = AppConsts.DefaultPageSize;
        }
    }

    public class MaxPagedAndSortedInputDto : MaxPagedInputDto, ISortedResultRequest
    {
        public string Sorting { get; set; }

        public MaxPagedAndSortedInputDto()
        {
            MaxResultCount = AppConsts.MaximumPageSize;
        }
    }
}