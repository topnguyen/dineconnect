﻿namespace DinePlan.DineConnect.Application.Customer.PttOr.Dto
{
    public class PagedSortedAndFilteredInputDto : PagedAndSortedInputDto
    {
        public string Filter { get; set; }
    }
}