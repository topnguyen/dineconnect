﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Application.Customer.PttOr.Dto
{
    public class FileDto : IDoubleWayDto
    {
        [Required]
        public string FileName { get; set; }

        [Required]
        public string FileType { get; set; }

        [Required]
        public string FileToken { get; set; }
        public string FileSystemName { get; set; }
        public string FileTag { get; set; }
        public FileDto()
        {
            FileToken = Guid.NewGuid().ToString("N");
        }

        public FileDto(string fileName, string fileType)
        {
            FileName = fileName;
            FileType = fileType;
            FileToken = Guid.NewGuid().ToString("N");
        }
    }
}