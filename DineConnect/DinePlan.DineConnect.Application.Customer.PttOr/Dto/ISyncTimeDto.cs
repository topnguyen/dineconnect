﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Application.Customer.PttOr.Dto
{
    public interface ISyncTimeDto
    {
        DateTime LastSyncTime { get; set; }
        TimeZoneInfo TimeZone { get; set; }
    }
}
