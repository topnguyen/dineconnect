﻿using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Transaction;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System;
using Abp.Configuration;
using DinePlan.DineConnect.Connect.Period;
using DinePlan.DineConnect.Application.Customer.PttOr.CreditCardReport.Dto;
using DinePlan.DineConnect.Application.Customer.PttOr.CreditCardReport.Exporting;
using DinePlan.DineConnect.Application.Customer.PttOr.Shared;
using DinePlan.DineConnect.Application.Customer.PttOr.Dto;
using DinePlan.DineConnect.Connect.ConnectConsts;
using DinePlan.DineConnect.Configuration.Tenants;

namespace DinePlan.DineConnect.Application.Customer.PttOr.CreditCardReport
{
    public class InternalSaleReportAppService : CustomerPttAppServiceBase, IInternalSaleReportAppService
    {
        private readonly IRepository<Company> _companyRe;
        private readonly IRepository<Department> _dRepo;
        private readonly ICustomerPttSharedAppService _customerPttSharedService;
        private readonly IRepository<Location> _lRepo;
        private readonly IRepository<Ticket> _ticketManager;
        private readonly IRepository<Order> _orderManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IBackgroundJobManager _bgm;
        private readonly IInternalSaleReportExporter _exporter;
        private readonly IRepository<WorkPeriod> _workPeriod;
        private readonly IRepository<PaymentType> _payRe;
        private readonly IRepository<Payment> _paymentRepository;
        private readonly IRepository<Terminal> _terminalRepository;
        private readonly ITenantSettingsAppService _tenantSettingsService;
        public InternalSaleReportAppService(
            IRepository<Ticket> ticketManager,
            ICustomerPttSharedAppService customerPttSharedService,
            IRepository<Company> comR,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Department> drepo,
            IRepository<Location> lRepo,
            IRepository<Order> orderManager,
            IBackgroundJobManager bgm,
            IInternalSaleReportExporter exporter,
            IRepository<WorkPeriod> workPeriod,
            IRepository<PaymentType> payRe,
            IRepository<Payment> paymentRepository,
            IRepository<Terminal> terminalRepository,
            ITenantSettingsAppService tenantSettingsService
            )
        {
            _ticketManager = ticketManager;
            _companyRe = comR;
            _unitOfWorkManager = unitOfWorkManager;
            _dRepo = drepo;
            _customerPttSharedService = customerPttSharedService;
            _lRepo = lRepo;
            _orderManager = orderManager;
            _bgm = bgm;
            _exporter = exporter;
            _workPeriod = workPeriod;
            _payRe = payRe;
            _paymentRepository = paymentRepository;
            _terminalRepository = terminalRepository;
            _tenantSettingsService = tenantSettingsService;
        }


        public async Task<GetInternalSaleReportOutput> GetInternalSaleReport(GetInternalSaleReportInput input)
        {
            var result = new GetInternalSaleReportOutput();
          
            var tickets = GetInternalSaleReportForCondition(input);
            var internals = new List<GetInternalSaleReportItem>();
            foreach (var ticket in tickets.ToList())
            {
                if (!string.IsNullOrEmpty(ticket.TicketTags) && !string.IsNullOrEmpty(ticket.GetTicketTagValue(ConnectConsts.OREmployeeID)))
                {

                    var item = new GetInternalSaleReportItem()
                    {
                        SiteCode = ticket.Location.Code,
                        SiteName = ticket.Location.Name,
                        TransactionNo = ticket.TicketNumber,
                        TransactionDate = ticket.LastPaymentTime,
                        InternalSaleType = ticket.DepartmentName,
                        OREmployeeID = ticket.GetTicketTagValue(ConnectConsts.OREmployeeID),
                        OREmployeeName = ticket.GetTicketTagValue(ConnectConsts.OREmployeeName),
                        ORDepartmentCode = ticket.GetTicketTagValue(ConnectConsts.ORDepartmentCode),
                        ORDepartmentName = ticket.GetTicketTagValue(ConnectConsts.ORDepartmentName),
                        OREmployeeStatusCode = ticket.GetTicketTagValue(ConnectConsts.OREmployeeStatusCode),
                        ORCostCenter = ticket.GetTicketTagValue(ConnectConsts.ORCostCenter),
                        ORIO = ticket.GetTicketTagValue(ConnectConsts.ORIO),
                        VatTotal = ticket.GetTaxTotal(),
                        Total = ticket.TotalAmount,
                    };
                    internals.Add(item);
                }
            }

            var dataAsQueryable = internals.AsQueryable();
            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                {
                    dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }
            }

            var dataBy_SiteCode = dataAsQueryable.OrderBy(x => x.TransactionNo).GroupBy(x => x.SiteCode).Select(x => new GetInternalSaleReportSiteCode
            {
                SiteCode = x.Key,
                ListItem = x.ToList()
            });
            result.ListItem = dataBy_SiteCode.OrderBy(x => x.SiteCode).ToList();
            return await Task.FromResult(result);
        }

        public IQueryable<Ticket> GetInternalSaleReportForCondition(GetInternalSaleReportInput input)
        {
            IQueryable<Ticket> output;

            var correctDate = CorrectInputDate(input);
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                output = _ticketManager.GetAll();
                if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
                {
                    if (correctDate)
                    {
                        output =
                            output.Where(
                                a =>
                                    a.LastPaymentTime >=
                                    input.StartDate
                                    &&
                                    a.LastPaymentTime <=
                                    input.EndDate);
                    }
                    else
                    {
                        var mySt = input.StartDate.Date;
                        var myEn = input.EndDate.Date;

                        output =
                            output.Where(
                                a =>
                                    a.LastPaymentTime >= mySt
                                    &&
                                    a.LastPaymentTime <= myEn);
                    }
                }
            }

            if (input.Location > 0)
            {
                output = output.Where(a => a.LocationId == input.Location);
            }
            else if (input.Locations != null && input.Locations.Any())
            {
                var locations = input.Locations.Select(a => a.Id).ToList();
                output = output.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                var locations = _customerPttSharedService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                if (locations.Any()) output = output.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _customerPttSharedService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                output = output.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                var locations = _customerPttSharedService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
                output = output.Where(a => locations.Contains(a.LocationId));
            }
            if (input.LocationGroup != null)
            {
                if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
                {
                    var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
                    output = output.Where(a => !nonlocations.Contains(a.LocationId));
                }
            }

            else if (input.LocationGroup == null)
            {
                var locations = _customerPttSharedService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
                if (input.UserId > 0)
                    output = output.Where(a => locations.Contains(a.LocationId));
            }
            output = output.Where(x => !x.PreOrder && !x.Credit);
            return output;
        }

        public async Task<FileDto> GetInternalSaleReportToExcel(GetInternalSaleReportInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;
                var output = await _exporter.ExportInternalSaleReport(input, this);
                return output;
            }
            return null;
        }
        public bool CorrectInputDate(IDateInput input)
        {
            if (input.NotCorrectDate) return true;
            var returnStatus = true;

            var operateHours = 0M;

            if (input.Locations != null && input.Locations.Any() && input.Locations.Count() == 1)
                operateHours = _lRepo.Get(input.Locations.First().Id).ExtendedBusinessHours;

            if (operateHours == 0M && input.Location > 0)
                operateHours = _lRepo.Get(input.Location).ExtendedBusinessHours;
            if (operateHours == 0M && input.LocationGroup != null && input.LocationGroup.Locations.Any() &&
                input.LocationGroup.Locations.Count() == 1)
                operateHours = _lRepo.Get(input.LocationGroup.Locations.First().Id).ExtendedBusinessHours;
            if (operateHours == 0M)
                operateHours = SettingManager.GetSettingValue<decimal>(AppSettings.ConnectSettings.OperateHours);

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                if (operateHours == 0M) returnStatus = false;
                input.EndDate = input.EndDate.AddDays(1);
                input.StartDate = input.StartDate.AddHours(Convert.ToDouble(operateHours));
                input.EndDate = input.EndDate.AddHours(Convert.ToDouble(operateHours)).AddMinutes(-1);
            }
            else
            {
                returnStatus = false;
            }

            return returnStatus;
        }


    }
}
