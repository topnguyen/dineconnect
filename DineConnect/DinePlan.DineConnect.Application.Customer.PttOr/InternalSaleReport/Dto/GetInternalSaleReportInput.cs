﻿using DinePlan.DineConnect.Application.Customer.PttOr.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Application.Customer.PttOr.Dto;
using DinePlan.DineConnect.Application.Customer.PttOr.Exporter;
using DinePlan.DineConnect.Application.Customer.PttOr.Shared;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Application.Customer.PttOr.CreditCardReport.Dto
{
    public class GetInternalSaleReportInput : PagedAndSortedInputDto, IFileExport, IDateInput, IBackgroundExport
    {
        public GetInternalSaleReportInput()
        {
            Locations = new List<SimpleLocationDto>();
            LocationGroup = new LocationGroupDto();
        }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Location { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
        public long UserId { get; set; }
        public bool NotCorrectDate { get; set; }
        public bool Credit { get; set; }
        public bool Refund { get; set; }
        public string TicketNo { get; set; }
        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }
        public string DynamicFilter { get; set; }
        public string DateFormat { get; set; }
        public string DatetimeFormat { get; set; }

    }
}
