﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Application.Customer.PttOr.CreditCardReport.Dto
{
	public class GetInternalSaleReportOutput 
	{
        public List<GetInternalSaleReportSiteCode> ListItem { get; set; }
		public decimal VatTotal => ListItem.Sum(x => x.VatTotal);
		public decimal SubTotal
        {
            get
            {
                return ListItem.Sum(x => x.SubTotal);
            }
        }
        public decimal Total
        {
            get
            {
                return ListItem.Sum(x => x.Total);
            }
        }
        public GetInternalSaleReportOutput()
		{
            ListItem = new List<GetInternalSaleReportSiteCode>();
        }
    }
    public class GetInternalSaleReportSiteCode 
    {
        public string SiteCode { get; set; }
        public List<GetInternalSaleReportItem> ListItem { get; set; }
        public decimal VatTotal
        {
            get
            {
                return ListItem.Sum(x => x.VatTotal);
            }
        }
        public decimal SubTotal
        {
            get
            {
                return ListItem.Sum(x => x.SubTotal);
            }
        }
        public decimal Total
        {
            get
            {
                return ListItem.Sum(x => x.Total);
            }
        }
        public GetInternalSaleReportSiteCode()
        {
            ListItem = new List<GetInternalSaleReportItem>();
        }
    }
    public class GetInternalSaleReportItem 
    {
        public string SiteCode { get; set; }
        public string SiteName { get; set; }
        public string TransactionNo { get; set; }

        public DateTime TransactionDate { get; set; }

        public string InternalSaleType { get; set; }

        public string OREmployeeID { get; set; }

        public string OREmployeeName { get; set; }

        public string ORDepartmentCode { get; set; }

        public string ORDepartmentName { get; set; }

        public string OREmployeeStatusCode { get; set; }

        public string ORCostCenter { get; set; }

        public string ORIO { get; set; }
        public decimal VatTotal { get;set; }
        public decimal Total { get; set; }
        public decimal SubTotal => Total - VatTotal;

    }
}
