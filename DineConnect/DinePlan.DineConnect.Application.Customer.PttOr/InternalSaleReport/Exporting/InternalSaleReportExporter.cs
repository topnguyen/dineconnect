﻿using Abp.Configuration;
using DinePlan.DineConnect.Application.Customer.PttOr.CreditCardReport.Dto;
using DinePlan.DineConnect.Application.Customer.PttOr.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Application.Customer.PttOr.Dto;
using DinePlan.DineConnect.Application.Customer.PttOr.Net.MimeTypes;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.ConnectConsts;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Application.Customer.PttOr.CreditCardReport.Exporting
{
	public class InternalSaleReportExporter : FileExporterBase, IInternalSaleReportExporter
    {
        public async Task<FileDto> ExportInternalSaleReport(GetInternalSaleReportInput input, IInternalSaleReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("InternalSaleMeetingReport"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                await GetInternalSaleReport(excelPackage, input, appService);


                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }
        private async Task GetInternalSaleReport(ExcelPackage package, GetInternalSaleReportInput input, IInternalSaleReportAppService appService)
        {

            var worksheet = package.Workbook.Worksheets.Add(L("InteralSaleMeetingReport"));
            worksheet.OutLineApplyStyle = true;

            worksheet.Cells[2, 7].Value = L("InternalSaleMeetingReport");
            worksheet.Cells[3, 7].Value = "During the day " + input.StartDate.ToString(input.DateFormat) + " To " + input.EndDate.ToString(input.DateFormat);
            worksheet.Cells[2, 7].Style.Font.Bold = true;

            var headers = new List<string>
            {
                L("SiteCode"),
                L("SiteName"),
                L("TransactionNo"),
                L("TransactionDate"),
                L("InternalSaleType"),
                L("OREmployeeID"),
                L("OREmployeeName"),
                L("ORDepartmentCode"),
                L("ORDepartmentName"),
                L("OREmployeeStatusCode"),
                L("ORCostCenter"),
                L("ORIO"),
                L("SubTotal"),
                L("VatTotal"),
                L("Total"),
            };


            AddHeader(
                worksheet, 5, headers.ToArray()
            );

            //Add  report data
            var row = 6;
            var dtos = await appService.GetInternalSaleReport(input);
            foreach (var databySite in dtos.ListItem)
            {
                foreach (var item in databySite.ListItem)
                {
                        worksheet.Cells[row, 1].Value = item.SiteCode;
                        worksheet.Cells[row, 2].Value = item.SiteName;
                        worksheet.Cells[row, 3].Value = item.TransactionNo;
                        worksheet.Cells[row, 4].Value = item.TransactionDate.ToString(input.DatetimeFormat); ;
                        worksheet.Cells[row, 5].Value = item.InternalSaleType;
                        worksheet.Cells[row, 6].Value = item.OREmployeeID;
                         worksheet.Cells[row, 7].Value = item.OREmployeeName;
                        worksheet.Cells[row, 8].Value = item.ORDepartmentCode;
                        worksheet.Cells[row, 9].Value = item.ORDepartmentName;
                        worksheet.Cells[row, 10].Value = item.OREmployeeStatusCode;
                        worksheet.Cells[row, 11].Value = item.ORCostCenter;
                        worksheet.Cells[row, 12].Value = item.ORIO;
                        worksheet.Cells[row, 13].Value = item.SubTotal.ToString(ConnectConsts.NumberFormat);
                        worksheet.Cells[row, 14].Value = item.VatTotal.ToString(ConnectConsts.NumberFormat);
                        worksheet.Cells[row, 15].Value = item.Total.ToString(ConnectConsts.NumberFormat);
                        row++;
                }
                worksheet.Cells[row, 12].Value = L("Total") +" " + databySite.SiteCode;
                worksheet.Cells[row, 13].Value = databySite.SubTotal.ToString(ConnectConsts.NumberFormat);
                worksheet.Cells[row, 14].Value = databySite.VatTotal.ToString(ConnectConsts.NumberFormat);
                worksheet.Cells[row, 15].Value = databySite.Total.ToString(ConnectConsts.NumberFormat);
                row++;
            }

            worksheet.Cells[row, 12].Value = L("Total");
            worksheet.Cells[row, 13].Value = dtos.SubTotal.ToString(ConnectConsts.NumberFormat);
            worksheet.Cells[row, 14].Value = dtos.VatTotal.ToString(ConnectConsts.NumberFormat);
            worksheet.Cells[row, 15].Value = dtos.Total.ToString(ConnectConsts.NumberFormat);

            for (var i = 1; i <= 15; i++)
            {
                worksheet.Column(i).AutoFit();
            }
        }

    }
}