﻿using DinePlan.DineConnect.Application.Customer.PttOr.CreditCardReport.Dto;
using DinePlan.DineConnect.Application.Customer.PttOr.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Application.Customer.PttOr.CreditCardReport.Exporting
{
    public interface IInternalSaleReportExporter
    {
        Task<FileDto> ExportInternalSaleReport(GetInternalSaleReportInput input, IInternalSaleReportAppService appService);
    }
}