﻿
using Abp.Application.Services;
using DinePlan.DineConnect.Application.Customer.PttOr.CreditCardReport.Dto;
using DinePlan.DineConnect.Application.Customer.PttOr.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Application.Customer.PttOr.CreditCardReport
{
	public interface IInternalSaleReportAppService : IApplicationService
	{
		Task<GetInternalSaleReportOutput> GetInternalSaleReport(GetInternalSaleReportInput input);
		Task<FileDto> GetInternalSaleReportToExcel(GetInternalSaleReportInput input);

	}
}
