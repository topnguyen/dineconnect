﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Application.Customer.PttOr.Shared
{
    public interface IDateInput
    {
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
        int Location { get; set; }
        long UserId { get; set; }
        bool NotCorrectDate { get; set; }
        List<SimpleLocationDto> Locations { get; set; }
        LocationGroupDto LocationGroup { get; set; }
        bool Credit { get; set; }
        bool Refund { get; set; }
        string TicketNo { get; set; }

    }
}
