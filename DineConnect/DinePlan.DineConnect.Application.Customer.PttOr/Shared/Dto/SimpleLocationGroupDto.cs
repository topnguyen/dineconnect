﻿using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Application.Customer.PttOr.Shared
{
    [AutoMapFrom(typeof(LocationGroup))]
    public class SimpleLocationGroupDto
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }
}
