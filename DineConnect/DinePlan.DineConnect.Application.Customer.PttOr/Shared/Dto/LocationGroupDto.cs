﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Application.Customer.PttOr.Shared
{
    public class LocationGroupDto
    {
        public LocationGroupDto()
        {
            Locations = new List<SimpleLocationDto>();
            Groups = new List<SimpleLocationGroupDto>();
            Group = false;
            LocationTags = new List<SimpleLocationTagDto>();
            LocationTag = false;
            NonLocations = new List<SimpleLocationDto>();
        }

        public List<SimpleLocationDto> Locations { get; set; }
        public List<SimpleLocationGroupDto> Groups { get; set; }
        public List<SimpleLocationTagDto> LocationTags { get; set; }
        public List<SimpleLocationDto> NonLocations { get; set; }

        public bool Group { get; set; }
        public bool LocationTag { get; set; }
        public long UserId { get; set; }
    }
}
