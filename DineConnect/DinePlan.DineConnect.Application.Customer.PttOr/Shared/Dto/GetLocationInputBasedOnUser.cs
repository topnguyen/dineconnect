﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Application.Customer.PttOr.Shared.Dto
{
    public class GetLocationInputBasedOnUser
    {
        public long UserId { get; set; }
        public List<ComboboxItemDto> CompanyList { get; set; }
        public string LocationName { get; set; }
    }
}
