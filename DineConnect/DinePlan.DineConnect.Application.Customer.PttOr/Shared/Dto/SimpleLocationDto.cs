﻿using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Application.Customer.PttOr.Shared
{
    [AutoMapFrom(typeof(Location), typeof(LocationListDto))]
    public class SimpleLocationDto
    {
        public virtual int Id { get; set; }
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual long OrganizationUnitId { get; set; }
        public virtual int CompanyRefId { get; set; }
    }

}
