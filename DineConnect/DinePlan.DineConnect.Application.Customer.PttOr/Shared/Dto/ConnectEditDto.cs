﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Application.Customer.PttOr.Shared
{
    public class ConnectEditDto
    {
        public string Locations { get; set; }
        public bool Group { get; set; }
        public string NonLocations { get; set; }
        public virtual bool LocationTag { get; set; }

    }
}
