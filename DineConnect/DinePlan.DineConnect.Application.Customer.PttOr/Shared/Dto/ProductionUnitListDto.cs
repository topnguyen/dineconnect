﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DinePlan.DineConnect.House;

namespace DinePlan.DineConnect.Application.Customer.PttOr.Shared
{
    [AutoMapFrom(typeof(ProductionUnit))]
    public class ProductionUnitListDto : FullAuditedEntityDto
    {
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public string Name { get; set; }

        public string Code { get; set; }

        public string MaterialAllowed { get; set; }

        public string IsActive { get; set; }

        public int TenantId { get; set; }

    }
}
