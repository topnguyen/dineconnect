﻿using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.Application.Customer.PttOr.Shared
{
    [AutoMapFrom(typeof(LocationTag))]
    public class SimpleLocationTagDto
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }
}
