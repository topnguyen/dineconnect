﻿using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.Application.Customer.PttOr.Shared
{
    [AutoMap(typeof(LocationSchdule))]
    public class ScheduleListDto
    {
        public string Name { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }
        public int LocationId { get; set; }
    }
}
