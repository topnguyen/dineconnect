﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Application.Customer.PttOr.Shared
{
    [AutoMapFrom(typeof(Location))]
    public class LocationListDto : FullAuditedEntityDto
    {
        public int CompanyRefId { get; set; }
        public Company Company { get; set; }

        public string CompanyName
        {
            get { return Company?.Name; }
            set { }
        }

        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual string Address1 { get; set; }
        public virtual string Address2 { get; set; }
        public virtual string Address3 { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string Country { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual decimal SharingPercentage { get; set; }
        public virtual string Fax { get; set; }
        public virtual string Website { get; set; }
        public virtual string Email { get; set; }
        public virtual bool IsPurchaseAllowed { get; set; }
        public virtual bool IsProductionAllowed { get; set; }
        public virtual int DefaultRequestLocationRefId { get; set; }
        public virtual string RequestedLocations { get; set; }
        public virtual DateTime? HouseTransactionDate { get; set; }
        public virtual string TallyCompanyName { get; set; }
        public bool IsProductionUnitAllowed { get; set; }

        public List<ProductionUnitListDto> ProductionUnitList { get; set; }
        public virtual int ExtendedBusinessHours { get; set; }
        public virtual bool IsDayCloseRecursiveUptoDateAllowed { get; set; }
        public int? LocationGroupId { get; set; }
        public string LocationGroupName { get; set; }
        public List<ScheduleListDto> Schedules { get; set; }
        public bool DoesDayCloseRunInBackGround { get; set; }
        public DateTime? BackGroundStartTime { get; set; }
        public DateTime? BackGrandEndTime { get; set; }
        public decimal TransferRequestLockHours { get; set; }
        public decimal TransferRequestGraceHours { get; set; }


    }
}
