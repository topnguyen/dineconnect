﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Application.Customer.PttOr.Shared
{
	public interface ICustomerPttSharedAppService: IApplicationService
	{
		List<int> GetLocationForUserAndGroup(LocationGroupDto locationDto);
	}
}
