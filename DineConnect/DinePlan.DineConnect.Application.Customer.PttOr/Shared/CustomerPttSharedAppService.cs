﻿using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using DinePlan.DineConnect.Application.Customer.PttOr.Shared.Dto;
using DinePlan.DineConnect.Connect.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Application.Customer.PttOr.Shared
{
	public class CustomerPttSharedAppService : CustomerPttAppServiceBase, ICustomerPttSharedAppService
	{
        private readonly IRepository<Location> _locationRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        public CustomerPttSharedAppService(
            IRepository<Location> locationRepo  ,
            IUnitOfWorkManager unitOfWorkManager ,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository
            )
		{
            _locationRepo = locationRepo;
            _unitOfWorkManager = unitOfWorkManager;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;

        }
        public List<int> GetLocationForUserAndGroup(LocationGroupDto dto)
        {
            var rLocations = new List<int>();
            if (!dto.Group && !dto.Locations.Any())
            {
                var userId = AbpSession.UserId ?? dto.UserId;
                if (userId > 0)
                {
                    var allLocations = GetLocationsForUser(new GetLocationInputBasedOnUser { UserId = userId });
                    if (allLocations != null && allLocations.Any())
                    {
                        rLocations = allLocations.Select(a => a.Id).ToList();
                    }
                }
            }
            else if (!dto.Group)
            {
                rLocations = dto.Locations.Select(a => a.Id).ToList();
            }
            else
            {
                var allLgids = dto.Groups.Select(a => a.Id).ToList();
                var allLoca =
                    _locationRepo.GetAll()
                        .Where(a => a.LocationGroups.Where(lg => allLgids.Contains(lg.Id)).Any());

                var userId = AbpSession.UserId ?? dto.UserId;
                if (userId > 0)
                {
                    var allLocations = GetLocationsForUser(new GetLocationInputBasedOnUser { UserId = userId }).Select(a => a.Id).ToList();
                    rLocations = allLoca.Where(a => allLocations.Contains(a.Id)).Select(a => a.Id).ToList();
                }
            }
            return rLocations;
        }
        public List<SimpleLocationDto> GetLocationsForUser(GetLocationInputBasedOnUser input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var allItems = GetLocationByUserId(input.UserId);

                if (input.CompanyList != null && input.CompanyList.Count > 0)
                {
                    var tempCompanies = input.CompanyList.Select(t => t.Value).ToArray();
                    var arrCompanies = tempCompanies.Select(lst => int.Parse(lst)).ToList();

                    allItems = allItems.Where(t => arrCompanies.Contains(t.CompanyRefId));
                }

                if (!input.LocationName.IsNullOrEmpty())
                {
                    allItems =
                        allItems.Where(t => t.Name.Contains(input.LocationName) || t.Code.Contains(input.LocationName));
                }

                var sortMenuItems = allItems.ToList();

                var allListDtos = sortMenuItems.MapTo<List<SimpleLocationDto>>();
                return allListDtos;
            }
        }
        private IQueryable<Location> GetLocationByUserId(long userId)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                IQueryable<Location> allItems;

                var userorg = _userOrganizationUnitRepository.GetAll().Where(a => a.UserId == userId);
                if (!userorg.Any())
                {
                    allItems = _locationRepo.GetAll();
                }
                else
                {
                    allItems = from loc in _locationRepo.GetAll().Where(a => !a.IsDeleted)
                               join ou in userorg
                                   on loc.Id equals ou.OrganizationUnitId
                               select loc;
                }
                return allItems;
            }
        }
    }
}
