﻿using System.Reflection;
using Abp.AutoMapper;
using Abp.Modules;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Core.Customer.PttOr;

namespace DinePlan.DineConnect.Application.Customer.PttOr
{
    /// <summary>
    /// Application layer module of the application.
    /// </summary>
    [DependsOn(typeof(DineConnectCoreCustomerModule), typeof(AbpAutoMapperModule))]
    public class DineConnectCustomerApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Adding authorization providers
            Configuration.Auditing.IsEnabled = false;
            //Configuration.Authorization.Providers.Add<AppAuthorizationProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            CustomerDtoMapper.CreateMappings();
        }

        public override void PostInitialize()
        {
          
        }
    }
}