﻿using Abp.Application.Services;
using Abp.IdentityFramework;
using Abp.MultiTenancy;
using Abp.Runtime.Session;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.MultiTenancy;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Configuration;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Helper;
using DinePlan.DineConnect.Application.Customer.PttOr.Shared;

namespace DinePlan.DineConnect.Application.Customer.PttOr
{
    /// <summary>
    /// All application services in this application is derived from this class.
    /// We can add common application service methods here.
    /// </summary>

    public abstract class CustomerPttAppServiceBase : ApplicationService
    {
        public TenantManager TenantManager { get; set; }

        public UserManager UserManager { get; set; }

        private string _dTFormat = "yyyy-MM-dd hh:mm tt";

        private  string _dateFormat = "yyyy-MM-dd";

        protected CustomerPttAppServiceBase()
        {
            LocalizationSourceName = DineConnectConsts.LocalizationSourceName;
        }

        protected virtual Task<User> GetCurrentUserAsync()
        {
            var user = UserManager.FindByIdAsync(AbpSession.GetUserId());
            if (user == null)
            {
                throw new ApplicationException("There is no current user!");
            }

            return user;
        }

        protected virtual int? GetCurrentUserLocationAsync()
        {
            return UserManager.GetDefaultLocationForUserAsync(AbpSession.GetUserId());
        }

        protected virtual User GetCurrentUser()
        {
            var user = UserManager.FindById(AbpSession.GetUserId());
            if (user == null)
            {
                throw new ApplicationException("There is no current user!");
            }

            return user;
        }

        protected virtual Task<Location> GetLocationInfo(int? argLocationId)
        {
            return UserManager.GetDefaultLocationInfoAsync(argLocationId);
        }

        protected virtual Task<Tenant> GetCurrentTenantAsync()
        {
            return TenantManager.GetByIdAsync(AbpSession.GetTenantId());
        }

        protected virtual Tenant GetCurrentTenant()
        {
            return TenantManager.GetById(AbpSession.GetTenantId());
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public virtual string GetDateTimeFormat()
        {
            try
            {
                return _dTFormat;
                //if (SettingManager != null && GetCurrentTenant() != null && GetCurrentTenant().Id > 0)
                //{
                //    var myFormat = SettingManager.GetSettingValueForTenant(AppSettings.ConnectSettings.SimpleDateFormat,
                //        GetCurrentTenant().Id);
                //    if (!string.IsNullOrEmpty(myFormat))
                //    {
                //        _dTFormat = myFormat + " HH:mm";
                //    }
                //}
            }
            catch (Exception)
            {
                // ignored
            }
            return _dTFormat;
        }

        public virtual string GetSimpleDateTimeFormat()
        {

            try
            {
                if (SettingManager != null && GetCurrentTenant() != null && GetCurrentTenant().Id > 0)
                {
                    var myFormat = SettingManager.GetSettingValueForTenant(AppSettings.ConnectSettings.DateTimeFormat,
                        GetCurrentTenant().Id);
                    if (!string.IsNullOrEmpty(myFormat))
                    {
                        _dateFormat = myFormat;
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }
            return _dateFormat;
            
        }

        protected void UpdateLocationAndNonLocationForEdit(ConnectEditDto editDto, LocationGroupDto output)
        {
            /*Start Group*/
            output.Group = editDto.Group;
            output.LocationTag = editDto.LocationTag;
            if (!string.IsNullOrEmpty(editDto.Locations))
            {
                if (editDto.Group)
                {
                    output.Groups =
                        JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(editDto.Locations);
                }
                else if (editDto.LocationTag)
                {
                    output.LocationTags =
                        JsonConvert.DeserializeObject<List<SimpleLocationTagDto>>(editDto.Locations);
                }
                else
                {
                    output.Locations =
                        JsonConvert.DeserializeObject<List<SimpleLocationDto>>(editDto.Locations);
                }
            }

            if (!string.IsNullOrEmpty(editDto.NonLocations))
            {
                output.NonLocations =
                    JsonConvert.DeserializeObject<List<SimpleLocationDto>>(editDto.NonLocations);
            }
            /*End Group*/
        }

        protected void UpdateLocationAndNonLocation(ConnectFullMultiTenantAuditEntity item, LocationGroupDto lgroup)
        {
            var allLocations = "";
            var allNonLocation = "";

            if (lgroup != null)
            {
                if (lgroup.Group && DynamicQueryable.Any(lgroup.Groups))
                {
                    allLocations = JsonConvert.SerializeObject(lgroup.Groups);
                }
                else if (!lgroup.Group && lgroup.LocationTag && DynamicQueryable.Any(lgroup.LocationTags))
                {
                    allLocations = JsonConvert.SerializeObject(lgroup.LocationTags);
                }
                else if (!lgroup.Group && !lgroup.LocationTag && DynamicQueryable.Any(lgroup.Locations))
                {
                    allLocations = JsonConvert.SerializeObject(lgroup.Locations);
                }

                if (lgroup.NonLocations != null && DynamicQueryable.Any(lgroup.NonLocations))
                {
                    allNonLocation = JsonConvert.SerializeObject(lgroup.NonLocations);
                }
                item.Group = lgroup.Group;
                item.LocationTag = lgroup.LocationTag;
            }
            item.Locations = allLocations;
            item.NonLocations = allNonLocation;
        }

        protected IEnumerable<ConnectFullMultiTenantAuditEntity> SearchLocation(IQueryable<ConnectFullMultiTenantAuditEntity> allEntities, LocationGroupDto lgGroup)
        {
            if (allEntities.Any() && lgGroup != null)
            {
                if (!lgGroup.LocationTag && !lgGroup.Group && lgGroup.Locations.Any())
                {
                    var allCodes = lgGroup.Locations.Select(a => a.Id).ToList();
                    var mllEntities = allEntities.Where(a=>!a.LocationTag && !a.Group && !string.IsNullOrEmpty(a.Locations)).ToList();
                    var finalReturnEntities = mllEntities.Where(a =>
                        (!string.IsNullOrEmpty(a.Locations) && CompareList(a.Locations, allCodes)) || string.IsNullOrEmpty(a.Locations));

                    return finalReturnEntities;
                }

                if (lgGroup.Group & lgGroup.Groups.Any())
                {
                    var allCodes = lgGroup.Groups.Select(a => a.Id).ToList();
                    var mllEntities = allEntities.Where(a => !string.IsNullOrEmpty(a.Locations) && a.Group).ToList();
                    var finalReturnEntities = mllEntities.Where(a => CompareList(a.Locations, allCodes));
                    return finalReturnEntities;

                }

                if (lgGroup.LocationTag & lgGroup.LocationTags.Any())
                {

                    var allCodes = lgGroup.LocationTags.Select(a => a.Id).ToList();
                    var mllEntities = allEntities.Where(a => !string.IsNullOrEmpty(a.Locations) && a.LocationTag).ToList();
                    var finalReturnEntities = mllEntities.Where(a => CompareList(a.Locations, allCodes));
                    return finalReturnEntities;
                }
            }
            return allEntities.ToList();
           
        }

        private bool CompareList(string locations, List<int> listB)
        {
            if (!string.IsNullOrEmpty(locations))
            {
                var allLo = JsonHelper.Deserialize<List<SimpleLocationDto>>(locations);
                if (allLo != null && allLo.Any())
                {
                    List<int> listA = allLo.Select(a => a.Id).ToList();
                    return listA.Any(x => listB.Contains(x));

                }
            }

            return false;
        }
    }
}