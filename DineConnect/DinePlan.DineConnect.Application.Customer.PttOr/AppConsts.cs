﻿namespace DinePlan.DineConnect.Application.Customer.PttOr
{
    /// <summary>
    /// Some consts used in the application.
    /// </summary>
    public class AppConsts
    {
        public const int DefaultPageSize = 10;
        public const int MaxPageSize = 10000;
        public const int MaximumPageSize=1000000000;
        public const string ConnectFilter = "ConnectFilter";
    }
}