﻿using System;
using System.Collections.Generic;
using OpenHtmlToPdf;

namespace DinePlan.DineConnect.Application.Customer.PttOr.Exporter.Util
{
    // Input Class to Get the Export Files
    public class ExportInputObject
    {
        // Name of the File to Exported
        public string FileName { get; set; }


        // It has tags called {NL} which are New Lines
        public string FirstPage { get; set; }
        
        // It has tags called {NL} which are New Lines
        public string LastPage { get; set; }

        // It has the fields to be displayed in Tables and other spec of the Fields are in FieldAttributes
        public Dictionary<string, FieldAttributes> Fields { get; set; }
        public List<ExportDataObject> ExportObjects { get; set; }
        public ExportType ExportType { get; set; }

        #region PDF-Options
        public PaperSize PaperSize{ get; set; }
        public bool Portrait { get; set; }

        #endregion
        public ExportInputObject()
        {
            Fields = new Dictionary<string, FieldAttributes>();
            PaperSize = PaperSize.A4;
            Portrait = true;
        }
    }

 
    /**
     * What output is expected
     */


    /**
     * FieldAttributes -- To Display how you want it 
     */
    public class FieldAttributes
    {
        // Column Name
        public string DisplayName { get; set; }
        // What format it has to be applied
        public string Format { get; set; }
        //Whether the total is required for the column
        public bool TotalRequired { get; set; }
        // Bold is required or not 
        public bool BoldRequired { get; set; }
        public FieldAlignment Alignment { get; set; }
        public ExportFiledType FieldType { get; set; }
        public List<string> PairObjectDisplayNames{ get; set; }

        public FieldAttributes()
        {
            FieldType = ExportFiledType.String;
            Alignment = FieldAlignment.Left;
            PairObjectDisplayNames = new List<string>();
        }
    }

    public enum FieldAlignment
    {
        Left = 0,
        Center = 1,
        Right = 2
    }

    public enum ExportFiledType
    {

        Decimal = 0,
        Integer = 1,
        String = 2,
        Date = 3,
        Pair = 4
    }

    /**
     * Just an Abstract Class
     */
    public abstract class ExportDataObject
    {
        
    }

    /**
     * Implementation of Abstract Class
     */

    public class TicketExport : ExportDataObject
    {
        public int No { get; set; }
        public string Name { get; set; }
        //If No CreatedTime, use Default Name
        public DateTime CreatedTime { get; set; }
        public decimal Total { get; set; }
        public PairObject Payments { get; set; }
    }

    public class DifferentObject : ExportDataObject
    {
        public int No { get; set; }
        public string Name { get; set; }

        public decimal PurchasePrice { get; set; }

        //If No CreatedTime, use Default Name
        public DateTime CreatedTime { get; set; }
        public decimal Total { get; set; }

    }

    public class PairObject
    {
        public PairObject()
        {
            Datas = new Dictionary<string, string>();
        }
        public string[] FieldNames { get; set; }
        public Dictionary<string, string> Datas { get; set; }
        public string[] Format { get; set; }
        public FieldAlignment[] Alignments { get; set; }
        public ExportFiledType[] FieldTypes { get; set; }
    }
}