﻿using DinePlan.DineConnect.Application.Customer.PttOr.Dto;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DinePlan.DineConnect.Application.Customer.PttOr.Exporter.Util
{
    public abstract class DineConnectAbstractExporter
    {
        protected ExportInputObject ExportDataObject;
        protected readonly Dictionary<string, decimal> _totals;

        public DineConnectAbstractExporter()
        {
            _totals = new Dictionary<string, decimal>();
        }

        public abstract void Export(string folder, FileDto fileInfo, string body="");

        protected string GetFieldValue(ExportDataObject item, KeyValuePair<string, FieldAttributes> field)
        {
            var val = GetFieldValueObj(item, field);
            if (val != null)
            {
                return val.ToString();
            }
            return "";
        }

        protected object GetFieldValueObj(ExportDataObject item, KeyValuePair<string, FieldAttributes> field)
        {
            object fieldValue = null;
            var fieldName = field.Key;
            var property = item.GetType().GetProperties().FirstOrDefault(x => x.Name == fieldName);
            var value = item.GetType().GetProperty(fieldName)?.GetValue(item);

            if (value != null)
            {
                if (string.IsNullOrEmpty(field.Value.Format) == false)
                {
                    if (value is decimal)
                    {
                        fieldValue = ((decimal) value).ToString(field.Value.Format);
                    }
                    else if (value is DateTime)
                    {
                        fieldValue = ((DateTime) value).ToString(field.Value.Format);
                    }
                    else
                    {
                        fieldValue = value.ToString();
                    }
                }
                else
                {
                    fieldValue = value.ToString();
                }

                if (field.Value.TotalRequired && value.GetType() != typeof (PairObject))
                {
                    if (_totals.Keys.Contains(field.Key))
                    {
                        _totals[field.Key] += decimal.Parse(fieldValue.ToString());
                    }
                    else
                    {
                        _totals[field.Key] = decimal.Parse(fieldValue.ToString());
                    }
                }
                else
                {
                    if (field.Value.TotalRequired)
                    {
                        if (_totals.Keys.Contains(field.Key))
                        {
                            _totals[field.Key] += decimal.Parse(fieldValue.ToString());
                        }
                        else
                        {
                            _totals[field.Key] = decimal.Parse(fieldValue.ToString());
                        }
                    }
                    else
                    {
                        _totals[field.Key] = 0;
                    }
                }
            }
            return fieldValue;
        }

        protected string GetPairObjectValue(ExportDataObject item, KeyValuePair<string, FieldAttributes> field)
        {
            var fieldName = field.Key;
            var property = item.GetType().GetProperties().FirstOrDefault(x => x.Name == fieldName);
            var value = (PairObject) item.GetType().GetProperty(fieldName)?.GetValue(item);

            if (value != null)
            {
                var content = "";

                for (var i = 0; i < value.FieldNames.Length; i++)
                {
                    if (value.Datas.Keys.Contains(value.FieldNames[i]))
                    {
                        var alignmentCss = GetAlignmentCss(GetAlignment(ref field, value, i));
                        var fieldType = ExportFiledType.String;
                        if (i < value.FieldTypes.Length)
                        {
                            fieldType = value.FieldTypes[i];
                        }
                        var fieldFormat = "";
                        if (i < value.Format?.Length)
                        {
                            fieldFormat = value.Format[i];
                        }

                        var fv = value.Datas[value.FieldNames[i]];
                        content += $"<td {alignmentCss} >{GetFormatedValue(fv, fieldType, fieldFormat)}</td>";

                        var dVal = 0.0M;

                        if (fieldType == ExportFiledType.Decimal)
                        {
                            dVal = decimal.Parse(fv);
                        }
                        else if (fieldType == ExportFiledType.Integer)
                        {
                            dVal = int.Parse(fv);
                        }

                        var pairFieldName = fieldName + "_" + value.FieldNames[i];
                        if (field.Value.TotalRequired)
                        {
                            if (_totals.Keys.Contains(pairFieldName))
                            {
                                _totals[pairFieldName] += dVal;
                            }
                            else
                            {
                                _totals[pairFieldName] = dVal;
                            }
                        }
                    }
                    else
                    {
                        content += $"<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>";
                    }
                }
                return content;
            }

            return "";
        }

        public List<string> GetPairFieldNames(string fieldName)
        {
            var fieldNames = new List<string>();
            foreach (var eo in ExportDataObject.ExportObjects)
            {
                var v = (PairObject) eo.GetType().GetProperty(fieldName)?.GetValue(eo);
                if (v?.FieldNames.Length > fieldNames.Count)
                {
                    fieldNames = new List<string>(v.FieldNames);
                }
            }
            return fieldNames;
        }

        public string GetFormatedValue(string value, ExportFiledType fieldType, string fieldFormat)
        {
            var formatedValue = value;
            switch (fieldType)
            {
                case ExportFiledType.Date:
                    var date = DateTime.Parse(value);
                    if (string.IsNullOrEmpty(fieldFormat) == false)
                    {
                        formatedValue = date.ToString(fieldFormat);
                    }
                    break;
                case ExportFiledType.Decimal:
                    var dVal = decimal.Parse(value);
                    if (string.IsNullOrEmpty(fieldFormat) == false)
                    {
                        formatedValue = dVal.ToString(fieldFormat);
                    }
                    break;
                case ExportFiledType.Integer:
                    var iVal = DateTime.Parse(value);
                    if (string.IsNullOrEmpty(fieldFormat) == false)
                    {
                        formatedValue = iVal.ToString(fieldFormat);
                    }
                    break;
                case ExportFiledType.Pair:

                    break;
            }
            return formatedValue;
        }

        public static FieldAlignment GetAlignment(ref KeyValuePair<string, FieldAttributes> field, PairObject value,
            int i)
        {
            var alighment = field.Value.Alignment;

            var fv = value.Datas[value.FieldNames[i]];
            if (i < value.Alignments.Length)
            {
                alighment = value.Alignments[i];
            }

            return alighment;
        }

        public object GetAlignmentCss(FieldAlignment alighment)
        {
            var css = "style=\"text-align:left\"";
            switch (alighment)
            {
                case FieldAlignment.Center:
                    css = "style=\"text-align:center\"";
                    break;
                case FieldAlignment.Right:
                    css = "style=\"text-align:right\"";
                    break;
            }
            return css;
        }

        protected string GetTotalRow(bool isRequired, decimal amount)
        {
            var totalRowHtml = "";
            if (isRequired)
            {
                totalRowHtml = $"<tr>" +
                               $"<td> </td>" +
                               $"<td></td>" +
                               $"<td><strong>Total</strong></td>" +
                               $"<td><strong>{amount}</strong></td>" +
                               $"<tr/>";
            }
            return totalRowHtml;
        }

        protected string FormatDate(DateTime dateTime)
        {
            var format = ExportDataObject.Fields["CreatedTime"].Format;
            var formatedText = dateTime.ToString();

            if (string.IsNullOrEmpty(format) == false)
            {
                formatedText = dateTime.ToString(format);
            }
            return formatedText;
        }

        protected string FormatNumber(decimal total)
        {
            var format = ExportDataObject.Fields["Total"].Format;
            var formatedText = total.ToString();

            if (string.IsNullOrEmpty(format) == false)
            {
                formatedText = total.ToString(format);
            }
            return formatedText;
        }

        protected string MakeBold(string text, string field)
        {
            var boldRequired = ExportDataObject.Fields[field].BoldRequired;

            if (boldRequired)
            {
                text = $"<strong>{text}</strong>";
            }

            return text;
        }
    }
}