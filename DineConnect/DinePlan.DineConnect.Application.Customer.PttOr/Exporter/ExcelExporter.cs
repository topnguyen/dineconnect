﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using DinePlan.DineConnect.Application.Customer.PttOr.Dto;
using DinePlan.DineConnect.Application.Customer.PttOr.Exporter.Util;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace DinePlan.DineConnect.Application.Customer.PttOr.Exporter
{
    public class ExcelExporter : DineConnectAbstractExporter
    {
        private readonly int firstPageStart = 1;

        public ExcelExporter(ExportInputObject exportDataObject)
        {
            ExportDataObject = exportDataObject;
        }

        public override void Export(string folder, FileDto fileInfo, string body)
        {
            using (var excelPkg = new ExcelPackage())
            {

                #region Fields

                var contentWorksheet = excelPkg.Workbook.Worksheets.Add("Content");
                var col = 1;
                foreach (var item in ExportDataObject.Fields)
                {
                    if (item.Value.FieldType == ExportFiledType.Pair)
                    {
                        foreach (var pObjField in item.Value.PairObjectDisplayNames)
                        {
                            contentWorksheet.Cells[1, col].Style.Font.Bold = true;
                            contentWorksheet.Cells[1, col].Value = pObjField.ToUpper();
                            contentWorksheet.Cells[1, col].Style.Font.Size = 15;
                            contentWorksheet.Cells[1, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            contentWorksheet.Cells[1, col].Style.Fill.BackgroundColor.SetColor(Color.BlueViolet);
                            contentWorksheet.Cells[1, col].Style.Font.Color.SetColor(Color.White);

                            col++;
                        }
                    }
                    else
                    {
                        var displayName = string.IsNullOrEmpty(item.Value.DisplayName)
                            ? item.Key
                            : item.Value.DisplayName;
                        contentWorksheet.Cells[1, col].Style.Font.Bold = true;
                        contentWorksheet.Cells[1, col].Value = displayName.ToUpper();
                        contentWorksheet.Cells[1, col].Style.Font.Size = 15;
                        contentWorksheet.Cells[1, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        contentWorksheet.Cells[1, col].Style.Fill.BackgroundColor.SetColor(Color.BlueViolet);
                        contentWorksheet.Cells[1, col].Style.Font.Color.SetColor(Color.White);
                        col++;
                    }
                }
                for (int i = 1; i <= 1; i++)
                {
                    contentWorksheet.Column(i).AutoFit();
                }
                #endregion
                #region ContentRows

                var row = 2;
                foreach (var item in ExportDataObject.ExportObjects)
                {
                    col = 1;
                    foreach (var field in ExportDataObject.Fields)
                    {
                        contentWorksheet.Cells[row, col].Style.Font.Bold = field.Value.BoldRequired;
                        contentWorksheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        var alignment = field.Value.Alignment;
                        if (alignment == FieldAlignment.Center)
                        {
                            contentWorksheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        }
                        else if (alignment == FieldAlignment.Right)
                        {
                            contentWorksheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }

                        if (field.Value.FieldType == ExportFiledType.Pair)
                        {
                            var fieldName = field.Key;
                            var value = (PairObject) item.GetType().GetProperty(fieldName)?.GetValue(item);
                            if (value != null)
                            {
                                for (var i = 0; i < value.FieldNames.Length; i++)
                                {
                                    if (value.Datas.Keys.Contains(value.FieldNames[i]))
                                    {
                                        var fieldType = ExportFiledType.String;
                                        if (i < value.FieldTypes.Length)
                                        {
                                            fieldType = value.FieldTypes[i];
                                        }
                                        var fieldFormat = "";
                                        if (value.Format!=null && i < value.Format.Length)
                                        {
                                            fieldFormat = value.Format[i];
                                        }

                                        if (i < value.Alignments.Length)
                                        {
                                            var pobjAlignment = value.Alignments[i];
                                            if (pobjAlignment == FieldAlignment.Center)
                                            {
                                                contentWorksheet.Cells[row, col].Style.HorizontalAlignment =
                                                    ExcelHorizontalAlignment.Center;
                                            }
                                            else if (pobjAlignment == FieldAlignment.Right)
                                            {
                                                contentWorksheet.Cells[row, col].Style.HorizontalAlignment =
                                                    ExcelHorizontalAlignment.Right;
                                            }
                                        }

                                        if (field.Value.BoldRequired)
                                        {
                                            contentWorksheet.Cells[row, col].Style.Font.Bold = true;
                                        }
                                        var ov = value.Datas[value.FieldNames[i]];

                                        if (fieldType == ExportFiledType.Decimal)
                                        {
                                            var dVal = decimal.Parse(ov);
                                            if (string.IsNullOrEmpty(fieldFormat) == false)
                                            {
                                                contentWorksheet.Cells[row, col].Style.Numberformat.Format = "0.00";
                                                contentWorksheet.Cells[row, col].Value = dVal;
                                            }
                                            else
                                            {
                                                contentWorksheet.Cells[row, col].Value = dVal;
                                            }
                                        }
                                        else if (fieldType == ExportFiledType.Integer)
                                        {
                                            contentWorksheet.Cells[row, col].Value = int.Parse(ov);
                                        }
                                        else if (fieldType == ExportFiledType.Date)
                                        {
                                            var date = DateTime.Parse(ov);

                                            if (string.IsNullOrEmpty(fieldFormat) == false)
                                            {
                                                contentWorksheet.Cells[row, col].DataValidation.AddCustomDataValidation();
                                                contentWorksheet.Cells[row, col].Value = date.ToString(fieldFormat);
                                            }
                                            else
                                            {
                                                contentWorksheet.Cells[row, col].DataValidation.AddCustomDataValidation();
                                                contentWorksheet.Cells[row, col].Value = date;
                                            }
                                        }
                                        else
                                        {
                                            contentWorksheet.Cells[row, col].Value = ov;
                                        }

                                        var pairFieldName = fieldName + "_" + value.FieldNames[i];

                                        if (field.Value.TotalRequired)
                                        {
                                            if (_totals.Keys.Contains(pairFieldName))
                                            {
                                                _totals[pairFieldName] += decimal.Parse(ov);
                                            }
                                            else
                                            {
                                                _totals[pairFieldName] = decimal.Parse(ov);
                                            }
                                        }
                                    }
                                    col++;
                                }
                            }
                        }
                        else
                        {
                            if (field.Value.FieldType == ExportFiledType.Decimal)
                            {
                                contentWorksheet.Cells[row, col++].Value = decimal.Parse(GetFieldValue(item, field));
                            }
                            else if (field.Value.FieldType == ExportFiledType.Integer)
                            {
                                contentWorksheet.Cells[row, col++].Value = int.Parse(GetFieldValue(item, field));
                            }
                            else if (field.Value.FieldType == ExportFiledType.Date)
                            {
                                contentWorksheet.Cells[row, col++].Value = GetFieldValue(item, field);
                            }
                            else
                            {
                                contentWorksheet.Cells[row, col++].Value = GetFieldValue(item, field);
                            }
                        }
                    }
                    row++;
                }

                #endregion
                #region Total

                col = 1;
                var totalText = "TOTAL";
                foreach (var field in ExportDataObject.Fields)
                {
                    if (!field.Value.TotalRequired)
                    {
                        col++;
                        continue;
                    }

                    if (field.Value.FieldType == ExportFiledType.Pair)
                    {
                        foreach (var item in field.Value.PairObjectDisplayNames)
                        {
                            var keyName = field.Key + "_" + item;
                            if (_totals.Keys.Contains(keyName) && _totals[keyName] > 0)
                            {
                                contentWorksheet.Cells[row, col].Style.Font.Bold = true;
                                contentWorksheet.Cells[row, col].Value = _totals[keyName];
                            }
                            col++;
                        }
                    }
                    else
                    {
                        if (_totals.Keys.Contains(field.Key) && _totals[field.Key] > 0)
                        {
                            contentWorksheet.Cells[row, col].Style.Font.Bold = true;
                            contentWorksheet.Cells[row, col].Value = _totals[field.Key];
                        }
                        col++;
                    }

                }
                contentWorksheet.Cells[row,1].Value = totalText;

                for (int i = col-1; i > 0; i--)
                {
                    contentWorksheet.Cells[row, i].Style.Font.Bold = true;
                    contentWorksheet.Cells[row, i].Style.Font.Size = 15;
                    contentWorksheet.Cells[row, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    contentWorksheet.Cells[row, i].Style.Fill.BackgroundColor.SetColor(Color.Red);
                    contentWorksheet.Cells[row, i].Style.Font.Color.SetColor(Color.White);
                }

                #endregion
                contentWorksheet.Cells[contentWorksheet.Dimension.Address].AutoFitColumns();

                if (!string.IsNullOrEmpty(ExportDataObject.LastPage))
                {
                    var thirdWorksheet = excelPkg.Workbook.Worksheets.Add("Summary");
                    thirdWorksheet.Cells[1, 1].Value = ExportDataObject.LastPage.Replace("{NL}", "\r");
                }

                var filePath = Path.Combine(folder, fileInfo.FileToken);
                excelPkg.SaveAs(new FileInfo(filePath));
            }
        }
    }
}