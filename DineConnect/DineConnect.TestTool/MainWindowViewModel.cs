﻿using DineConnect.TestTool.Models;
using DineConnect.TestTool.Models.DTO;
using DineConnect.TestTool.Models.DTO.Auth;
using DineConnect.TestTool.Models.DTO.Ticket;
using DineConnect.TestTool.Services;
using Microsoft.Win32;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DineConnect.TestTool
{
    public class MainWindowViewModel : BindableBase
    {
        public MainWindowViewModel()
        {
            StartConnectCommand = new DelegateCommand(OnStartConnectCommand);
            ResetConnectCommand = new DelegateCommand(OnResetConnectCommand);
            AddHeaderCommand = new DelegateCommand(OnAddHeaderCommand);
            AddParamsCommand = new DelegateCommand(OnAddParamsCommand);
            SelectFileCommand = new DelegateCommand(OnSelectFile);
            TestCommand = new DelegateCommand(OnTestCommand);

            Loading = false;
            Connected = false;
            ResultVisibility = Visibility.Collapsed;
            Status = "Not Started";

            Methods = new List<string>
            {
                "GET",
                "POST",
                "PUT",
                "DELETE"
            };
            BodyTypes = new List<string>
            {
                "Key/Value",
                "Raw Body"
            };

            Request = new TestRequest
            {
                Method = Methods.First(),
                BodyType = BodyTypes.First(),
                Host = "http://localhost",
                Endpoint = "api/services/app/ticket/CreateOrUpdateTicket",
                RequestsNumber = 10,
                Headers = new ObservableCollection<Parameters>(),
                ParameterBody = new ObservableCollection<Parameters>(),
                TenantName = "Cheezy", //
                Username = "admin", //
                Password = "123qwe" //
            };

            Result = new TestResult()
            {
                Requests = new List<RequestResult>()
            };
        }

        public List<string> Methods { get; set; }
        public List<string> BodyTypes { get; set; }
        public TestRequest Request { get; set; }
        public TestResult Result { get; set; }

        private string status { get; set; }
        public string Status 
        { 
            get => status; 
            set
            {
                status = value;
                OnPropertyChanged(() => Status);
            }
        }

        private bool loading { get; set; }
        public bool Loading
        {
            get => loading;
            set
            {
                loading = value;
                OnPropertyChanged(() => Loading);
            }
        }

        private bool connected { get; set; }
        public bool Connected
        {
            get => connected;
            set
            {
                connected = value;
                OnPropertyChanged(() => Connected);
            }
        }

        private string error { get; set; }
        public string Error
        {
            get => error;
            set
            {
                error = value;
                OnPropertyChanged(() => Error);
            }
        }

        public string SelectedFilePath { get; set; } 

        private Visibility resultVisibility { get; set; }
        public Visibility ResultVisibility 
        {
            get => resultVisibility;
            set 
            {
                resultVisibility = value;
                OnPropertyChanged(() => ResultVisibility);
            }
        }

        public ICommand StartConnectCommand { get; set; }
        public ICommand ResetConnectCommand { get; set; }
        public ICommand AddHeaderCommand { get; set; }
        public ICommand AddParamsCommand { get; set; }
        public ICommand SelectFileCommand { get; set; }
        public ICommand TestCommand { get; set; }

        private string token { get; set; }

        private async void OnStartConnectCommand()
        {
            Loading = true;
            Error = null;

            using (var restClient = new RestClient())
            {
                var url = Request.Host + Constants.AuthEndpoint;
                var body = new AuthRequest
                {
                    tenancyName = Request.TenantName,
                    usernameOrEmailAddress = Request.Username,
                    password = Request.Password
                };
                var response = await restClient.RequestAsync<string>("POST", url, null, body);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    token = response?.Data;
                    Connected = true;
                }
                else
                {
                    switch(response.StatusCode)
                    {
                        case HttpStatusCode.NotFound:
                            Error = "Server not found"; break;
                        case HttpStatusCode.Unauthorized:
                        case HttpStatusCode.BadRequest:
                            Error = "Cannot login to server"; break;
                    }
                }
            }

            Loading = false;
        }

        private void OnResetConnectCommand()
        {
            Connected = false;
            token = null;
        }

        private void OnAddHeaderCommand()
        {
            var header = new Parameters();
            header.DeleteCommand = new DelegateCommand(() =>
            {
                Request.Headers.Remove(header);
            });
            Request.Headers.Add(header);
        }

        private void OnAddParamsCommand()
        {
            var param = new Parameters();
            param.DeleteCommand = new DelegateCommand(() =>
            {
                Request.ParameterBody.Remove(param);
            });
            Request.ParameterBody.Add(param);
        }

        private void OnSelectFile()
        {
            var openfile = new OpenFileDialog();
            openfile.DefaultExt = ".json";

            var result = openfile.ShowDialog();
            if (result ?? false)
            {
                SelectedFilePath = openfile.FileName;
                OnPropertyChanged(() => SelectedFilePath);
                Request.Body = File.ReadAllText(openfile.FileName);
            }
        }

        private async void OnTestCommand()
        {
            Loading = true;

            ResultVisibility = Visibility.Collapsed;
            Status = "In Progress";

            Result.Requests.Clear();
            using (var restClient = new RestClient(token))
            {
                var requests = new List<Task<ResponseResult<object>>>();
                var url = $"{Request.Host}/{Request.Endpoint}";
                var body = Request.Method == "POST" || Request.Method == "PUT" 
                    ? Request.Body.Parse<Dictionary<string, object>>()
                    : (object)Request.ParameterBody.ToList();
                var headers = Request.Headers.ToList();

                for (int i = 0; i < Request.RequestsNumber; i++)
                {
                    var request = restClient.RequestAsync<object>(Request.Method, url, headers , body);
                    requests.Add(request);
                }
                var responses = await Task.WhenAll(requests.ToArray());

                foreach (var response in responses)
                {
                    Result.Requests.Add(new RequestResult
                    {
                        StatusCode = response.StatusCode,
                        ResponseTime = response.ReponseTime
                    });
                }
            }
            Status = "Done";
            ResultVisibility = Visibility.Visible;
            OnPropertyChanged(() => Result);

            Loading = false;
        }
    }
}
