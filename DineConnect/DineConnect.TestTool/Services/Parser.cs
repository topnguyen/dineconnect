﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.TestTool.Services
{
    public static class Parser
    {
        public static T Parse<T>(this object data)
        {
            var json = JsonConvert.SerializeObject(data);
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static T Parse<T>(this string data)
        {
            return JsonConvert.DeserializeObject<T>(data);
        }
    }
}
