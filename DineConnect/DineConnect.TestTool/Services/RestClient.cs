﻿using DineConnect.TestTool.Models;
using DineConnect.TestTool.Models.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DineConnect.TestTool.Services
{
    public class RestClient : IDisposable
    {
        private HttpClient httpClient { get; set; }

        public RestClient(string token = null)
        {
            httpClient = new HttpClient();
            if (token != null)
            {
                httpClient.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", token);
            }
        }

        public async Task<ResponseResult<T>> RequestAsync<T>(string method, string url, List<Parameters> headers, object data = null)
        {
            try
            {
                return await CalculateResponseTime(async () =>
                {
                    var methodGetDelete = method == "GET" || method == "DELETE";
                    var urlFull = methodGetDelete && data != null
                        ? PrepareUrl(url, data.Parse<List<ParamNameValue>>())
                        : url;
                    var request = new HttpRequestMessage
                    {
                        Method = GetMethod(method),
                        RequestUri = new Uri(urlFull),
                    };
                    if (!methodGetDelete)
                        request.Content = PrepareRequestBody(data);
                    PrepareRequestHeaders(request.Headers, headers);
                    var response = await httpClient.SendAsync(request);
                    return await PrepareResult<T>(response);
                });
            }
            catch(Exception e)
            {
                return new ResponseResult<T>
                {
                    StatusCode = HttpStatusCode.NotFound
                };
            }
        }

        private HttpMethod GetMethod(string method)
        {
            switch (method)
            {
                case "GET": return HttpMethod.Get;
                case "PUT": return HttpMethod.Put;
                case "DELETE": return HttpMethod.Delete;
                case "POST":
                default: return HttpMethod.Post;
            }
        }

        private string PrepareUrl(string url, List<ParamNameValue> parameters)
        {
            var builder = new UriBuilder(url);
            builder.Port = -1;
            var query = HttpUtility.ParseQueryString(builder.Query);
            foreach (var parameter in parameters)
            {
                query[parameter.Name] = parameter.Value;
            }
            builder.Query = query.ToString();
            return builder.ToString();
        }

        private StringContent PrepareRequestBody(object data)
        {
            var jsonData = JsonConvert.SerializeObject(data);
            var httpContent = new StringContent(jsonData, Encoding.UTF8, "application/json");
            return httpContent;
        }

        private void PrepareRequestHeaders(HttpRequestHeaders headers, List<Parameters> headerParams)
        {
            if (headerParams != null)
            {
                foreach (var headerParam in headerParams)
                {
                    if (!string.IsNullOrEmpty(headerParam.Name))
                        headers.Add(headerParam.Name, headerParam.Value);
                }
            }
        }

        private async Task<ResponseResult<T>> PrepareResult<T>(HttpResponseMessage response)
        {
            var json = await response.Content.ReadAsStringAsync();
            var result = new ResponseResult<T>
            {
                StatusCode = response.StatusCode,
            };
            try
            {
                var output = JsonConvert.DeserializeObject<Response<T>>(json);
                result.Data = output.result;
                if (result.StatusCode == HttpStatusCode.OK && !output.success)
                {
                    if (output.unAuthorizedRequest)
                        result.StatusCode = HttpStatusCode.Unauthorized;
                    else
                        result.StatusCode = HttpStatusCode.BadRequest;
                }   
            }
            catch(Exception e) 
            {
            }
            return result;
        }

        private async Task<ResponseResult<T>> CalculateResponseTime<T>(Func<Task<ResponseResult<T>>> requestAPI)
        {
            var stopWatch = Stopwatch.StartNew();
            ResponseResult<T> result;
            result = await requestAPI();
            result.ReponseTime = stopWatch.Elapsed.TotalMilliseconds;
            stopWatch.Stop();
            return result;
        }

        public void Dispose()
        {
            httpClient.Dispose();
        }
    }
}
