﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.TestTool.Models
{
    public class TestResult
    {
        public List<RequestResult> Requests { get; set; }
        private List<RequestResult> successRequests => 
            Requests.Where(x => x.StatusCode == HttpStatusCode.OK).ToList();

        public int SuccessNumber => Requests.Count(x => x.StatusCode == HttpStatusCode.OK);
        public int FailNumber => Requests.Count(x => (int)x.StatusCode >= 400);
        public double MinResponseTime
        {
            get
            {
                return successRequests.Count() > 0
                    ? successRequests.Min(x => x.ResponseTime)
                    : 0;
            }
        }
        public double MaxResponseTime
        {
            get
            {
                return successRequests.Count() > 0
                    ? successRequests.Max(x => x.ResponseTime)
                    : 0;
            }
        }
        public double AverageResponseTime
        {
            get {
                return successRequests.Count() > 0
                    ? successRequests.Average(x => x.ResponseTime)
                    : 0;
            }
        }
    }
}
