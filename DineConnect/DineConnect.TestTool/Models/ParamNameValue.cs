﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.TestTool.Models
{
    public class ParamNameValue
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
