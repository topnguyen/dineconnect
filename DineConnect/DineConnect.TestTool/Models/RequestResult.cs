﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.TestTool.Models
{
    public class RequestResult
    {
        public HttpStatusCode StatusCode { get; set; }
        public double ResponseTime { get; set; }
    }
}
