﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.TestTool.Models.DTO.Auth
{
    public class AuthRequest
    {
        public string tenancyName { get; set; }
        public string usernameOrEmailAddress { get; set; }
        public string password { get; set; }
    }
}
