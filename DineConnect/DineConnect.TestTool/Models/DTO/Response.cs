﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.TestTool.Models.DTO
{
    public class Response<T>
    {
        public bool success { get; set; }
        public T result { get; set; }
        public object error { get; set; }
        public bool unAuthorizedRequest { get; set; }
    }
}
