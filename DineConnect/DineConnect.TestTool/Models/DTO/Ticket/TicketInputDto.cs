﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.TestTool.Models.DTO.Ticket
{
    public class TicketInputDto
    {
        public int? id { get; set; }
        public int locationId { get; set; }

        /** Copy from DineConnect **/
        public int ticketId { get; set; }
        public int tenantId { get; set; }
        public string ticketNumber { get; set; }
        public string invoiceNo { get; set; }
        public DateTime ticketCreatedTime { get; set; }
        public DateTime lastUpdateTime { get; set; }
        public DateTime lastOrderTime { get; set; }
        public DateTime lastPaymentTime { get; set; }
        public bool isClosed { get; set; }
        public bool isLocked { get; set; }
        public decimal remainingAmount { get; set; }
        public decimal totalAmount { get; set; }
        public string departmentName { get; set; }
        public string ticketTypeName { get; set; }
        public string note { get; set; }
        public string lastModifiedUserName { get; set; }
        public string ticketTags { get; set; }
        public string ticketStates { get; set; }
        public string ticketLogs { get; set; }
        public bool taxIncluded { get; set; }
        public string terminalName { get; set; }
        public bool preOrder { get; set; }
        public string ticketEntities { get; set; }
        public int WorkPeriodId { get; set; }

        /**End**/
        public List<OrderInputDto> orders { get; set; }
        public List<PaymentInputDto> payments { get; set; }
        public List<TransactionInputDto> transactions { get; set; }
        public bool credit { get; set; }
        public string referenceNumber { get; set; }
        public string ticketPromotionDetails { get; set; }
        public string departmentGroup { get; set; }
    }
}
