﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.TestTool.Models.DTO.Ticket
{
    public class CreateOrUpdateTicketInput
    {
        public TicketInputDto Ticket { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
