﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DineConnect.TestTool.Models
{
    public class Parameters : ParamNameValue
    {
        public ICommand DeleteCommand { get; set; }
    }
}
