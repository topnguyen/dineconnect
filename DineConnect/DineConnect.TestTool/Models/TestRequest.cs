﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.TestTool.Models
{
    public class TestRequest
    {
        public string Method { get; set; }
        public string Host { get; set; }
        public string Endpoint { get; set; }
        public string TenantName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int RequestsNumber { get; set; }
        public ObservableCollection<Parameters> Headers { get; set; }
        public string BodyType { get; set; }
        public string Body { get; set; }
        public ObservableCollection<Parameters> ParameterBody { get; set; }
    }
}
