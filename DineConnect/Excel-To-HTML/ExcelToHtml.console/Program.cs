﻿using System;
using System.IO;
using ExcelToHtml.Helpers.WkHtmlToPdf;

namespace DineConnect.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // Excel file to convert
                string fileName = @"C:\_Downloads\T.xlsx";
                FileInfo newFile = new FileInfo(fileName);
                var htmlContent = new ExcelToHtml.ToHtml(newFile);
                var allHtml = htmlContent.GetHtml();
                File.WriteAllText(@"C:\_Downloads\T.html", allHtml);
            }
            catch (Exception exception)
            {
                var mess = exception.Message;
            }


        }
    }
}