﻿using DinePlan.DineConnect.Connect.Master;

namespace YogyaOutbound.Data
{
    public interface IPaymentTypeRepository : IRepository<PaymentType, int>
    {
    }
}