﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YogyaOutboundConsole.Enums.Data
{
    public enum DataWareHouseType
    {
        EDC,
        Float,
        Media,
        Membership,
        Promotion,
        SupervisorIntervention,
        TotalTransaction,
        TransactionDetails
    }
}
