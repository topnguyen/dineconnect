﻿using DinePlan.DineConnect.Connect.Master;
using YogyaOutbound.Data.Repositories;

namespace YogyaOutbound.Data
{
    public interface IDepartmentRepository : IRepository<Department, int>
    {
    }
}