﻿using DinePlan.DineConnect.Connect.Menu;
using YogyaOutbound.Data.Repositories;

namespace YogyaOutbound.Data
{
    public interface IMenuItemRepository : IRepository<MenuItem, int>
    {
    }
}