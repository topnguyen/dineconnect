﻿using DinePlan.DineConnect.Connect.Master;
using YogyaInboundConsole.Data.Repositories;
using YogyaOutboundConsole.Data;

namespace YogyaOutbound.Data.Repositories
{
    public class LocationRepository : Repository<Location, int>, ILocationRepository
    {
        public LocationRepository(YogyaOutboundDbContext context) : base(context)
        {
        }
    }
}