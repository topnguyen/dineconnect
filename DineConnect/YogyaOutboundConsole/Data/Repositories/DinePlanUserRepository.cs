﻿using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Users;
using YogyaInboundConsole.Data;
using YogyaInboundConsole.Data.Repositories;
using YogyaOutboundConsole.Data;

namespace YogyaOutbound.Data.Repositories
{
    public class DinePlanUserRepository : Repository<DinePlanUser, int>, IDinePlanUserRepository
    {
        public DinePlanUserRepository(YogyaOutboundDbContext context) : base(context)
        {
        }
    }
}