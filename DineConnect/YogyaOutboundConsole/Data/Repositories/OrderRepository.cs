﻿using DinePlan.DineConnect.Connect.Transaction;
using YogyaInboundConsole.Data.Repositories;
using YogyaOutboundConsole.Data;

namespace YogyaOutbound.Data.Repositories
{
    public class OrderRepository : Repository<Order, int>, IOrderRepository
    {
        public OrderRepository(YogyaOutboundDbContext context) : base(context)
        {
        }
    }
}