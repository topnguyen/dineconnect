﻿using DinePlan.DineConnect.Connect.Transaction;
using YogyaInboundConsole.Data.Repositories;
using YogyaOutboundConsole.Data;

namespace YogyaOutbound.Data.Repositories
{
    public class TicketTransactionRepository : Repository<TicketTransaction, int>, ITicketTransactionRepository
    {
        public TicketTransactionRepository(YogyaOutboundDbContext context) : base(context)
        {
        }
    }
}
