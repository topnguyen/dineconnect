﻿using DinePlan.DineConnect.Connect.Master;
using YogyaInboundConsole.Data.Repositories;
using YogyaOutboundConsole.Data;

namespace YogyaOutbound.Data.Repositories
{
    public class CategoryRepository : Repository<Category, int>, ICategoryRepository
    {
        public CategoryRepository(YogyaOutboundDbContext context) : base(context)
        {
        }
    }
}