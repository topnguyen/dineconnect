﻿using DinePlan.DineConnect.Connect.Master;
using YogyaInboundConsole.Data;
using YogyaInboundConsole.Data.Repositories;
using YogyaOutboundConsole.Data;

namespace YogyaOutbound.Data.Repositories
{
    public class DepartmentRepository : Repository<Department, int>, IDepartmentRepository
    {
        public DepartmentRepository(YogyaOutboundDbContext context) : base(context)
        {
        }
    }
}