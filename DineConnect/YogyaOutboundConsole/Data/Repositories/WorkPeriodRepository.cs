﻿using DinePlan.DineConnect.Connect.Period;
using DinePlan.DineConnect.Connect.Transaction;
using YogyaInboundConsole.Data.Repositories;
using YogyaOutboundConsole.Data;

namespace YogyaOutbound.Data.Repositories
{
    public class WorkPeriodRepository : Repository<WorkPeriod, int>, IWorkPeriodRepository
    {
        public WorkPeriodRepository(YogyaOutboundDbContext context) : base(context)
        {
        }
    }
}