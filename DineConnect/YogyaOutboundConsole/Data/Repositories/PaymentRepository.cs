﻿using DinePlan.DineConnect.Connect.Transaction;
using YogyaInboundConsole.Data.Repositories;
using YogyaOutboundConsole.Data;

namespace YogyaOutbound.Data.Repositories
{
    public class PaymentRepository : Repository<Payment, int>, IPaymentRepository
    {
        public PaymentRepository(YogyaOutboundDbContext context) : base(context)
        {
        }
    }
}