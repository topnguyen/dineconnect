﻿using DinePlan.DineConnect.Connect.Menu;

using YogyaInboundConsole.Data.Repositories;
using YogyaOutboundConsole.Data;

namespace YogyaOutbound.Data.Repositories
{
    public class MenuItemRepository : Repository<MenuItem, int>, IMenuItemRepository
    {
        public MenuItemRepository(YogyaOutboundDbContext context) : base(context)
        {
        }
    }
}