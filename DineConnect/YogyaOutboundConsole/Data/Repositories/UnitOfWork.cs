﻿using YogyaInboundConsole.Data;
using YogyaOutboundConsole.Data;

namespace YogyaOutbound.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly YogyaOutboundDbContext _context;

        public UnitOfWork(YogyaOutboundDbContext context)
        {
            _context = context;

            Payments = new PaymentRepository(context);
            PaymentTypes = new PaymentTypeRepository(context);
            Tickets = new TicketRepository(context);
            Orders = new OrderRepository(context);
            Transactions = new TicketTransactionRepository(context);
            Locations = new LocationRepository(context);
            WorkPeriods = new WorkPeriodRepository(context);
        }

        public IPaymentRepository Payments { get; }
        public IPaymentTypeRepository PaymentTypes { get; }
        public ITicketRepository Tickets { get; }
        public IOrderRepository Orders { get; }
        public ITicketTransactionRepository Transactions { get; }
        public ILocationRepository Locations { get; }
        public IWorkPeriodRepository WorkPeriods { get; }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}