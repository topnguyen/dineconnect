﻿using Abp.Authorization.Users;
using DinePlan.DineConnect.Connect.Users;
using YogyaInboundConsole.Data;
using YogyaInboundConsole.Data.Repositories;
using YogyaOutboundConsole.Data;

namespace YogyaOutbound.Data.Repositories
{
    public class DinePlanUserRoleRepository : Repository<DinePlanUserRole, int>, IDinePlanUserRoleRepository
    {
        public DinePlanUserRoleRepository(YogyaOutboundDbContext context) : base(context)
        {
        }
    }
}