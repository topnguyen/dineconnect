﻿using DinePlan.DineConnect.Connect.Master;
using YogyaInboundConsole.Data.Repositories;
using YogyaOutboundConsole.Data;

namespace YogyaOutbound.Data.Repositories
{
    public class PaymentTypeRepository : Repository<PaymentType, int>, IPaymentTypeRepository
    {
        public PaymentTypeRepository(YogyaOutboundDbContext context) : base(context)
        {
        }
    }
}