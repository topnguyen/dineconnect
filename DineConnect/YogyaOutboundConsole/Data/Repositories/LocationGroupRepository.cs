﻿using DinePlan.DineConnect.Connect.Master;
using YogyaInboundConsole.Data.Repositories;
using YogyaOutboundConsole.Data;

namespace YogyaOutbound.Data.Repositories
{
    public class LocationGroupRepository : Repository<LocationGroup, int>, ILocationGroupRepository
    {
        public LocationGroupRepository(YogyaOutboundDbContext context) : base(context)
        {
        }
    }
}