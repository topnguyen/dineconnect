﻿using DinePlan.DineConnect.Connect.Transaction;
using YogyaInboundConsole.Data.Repositories;
using YogyaOutboundConsole.Data;

namespace YogyaOutbound.Data.Repositories
{
    public class TicketRepository : Repository<Ticket, int>, ITicketRepository
    {
        public TicketRepository(YogyaOutboundDbContext context) : base(context)
        {
        }
    }
}