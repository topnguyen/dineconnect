﻿using DinePlan.DineConnect.Connect.Transaction;

namespace YogyaOutbound.Data
{
    public interface ITicketTransactionRepository: IRepository<TicketTransaction, int>
    {
    }
}
