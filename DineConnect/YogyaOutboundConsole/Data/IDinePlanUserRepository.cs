﻿
using DinePlan.DineConnect.Connect.Users;
using YogyaOutbound.Data.Repositories;

namespace YogyaOutbound.Data
{
    public interface IDinePlanUserRepository : IRepository<DinePlanUser, int>
    {
    }
}