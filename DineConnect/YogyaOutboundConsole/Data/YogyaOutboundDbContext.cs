﻿using System.Data.Entity;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.Users;

namespace YogyaOutboundConsole.Data
{
    public class YogyaOutboundDbContext : DbContext
    {
        public virtual IDbSet<LocationGroup> LocationGroups { get; set; }
        public virtual IDbSet<Location> Locations { get; set; }
        public virtual IDbSet<Category> Categories { get; set; }
        public virtual IDbSet<Department> Departments { get; set; }
        public virtual IDbSet<MenuItem> MenuItems { get; set; }
        public virtual IDbSet<MenuItemPortion> MenuItemPortions { get; set; }
        public virtual IDbSet<DinePlanUser> DinePlanUsers { get; set; }
        public virtual IDbSet<DinePlanUserRole> DinePlanUserRoles{ get; set; }
        public virtual IDbSet<Payment> Payments { get; set; }

        public YogyaOutboundDbContext(): base("Default")
        {
            var db = Database;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Payment>().Property(a => a.TenderedAmount).HasPrecision(18, 3);
            modelBuilder.Entity<Payment>().Property(a => a.Amount).HasPrecision(18, 3);

        }
    }
}