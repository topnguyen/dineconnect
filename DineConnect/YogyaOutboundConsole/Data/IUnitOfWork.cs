﻿using System;
using YogyaOutbound.Data.Repositories;

namespace YogyaOutbound.Data
{
    public interface IUnitOfWork : IDisposable
    {
        IPaymentRepository Payments { get; }
        IPaymentTypeRepository PaymentTypes { get; }
        ITicketRepository Tickets { get; }
        IOrderRepository Orders { get; }
        ITicketTransactionRepository Transactions { get; }
        IWorkPeriodRepository WorkPeriods { get; }
        ILocationRepository Locations { get; }

        int Complete();
    }
}