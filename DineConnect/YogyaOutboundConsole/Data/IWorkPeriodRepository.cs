﻿
using DinePlan.DineConnect.Connect.Period;
using DinePlan.DineConnect.Connect.Transaction;

namespace YogyaOutbound.Data
{
    public interface IWorkPeriodRepository : IRepository<WorkPeriod, int>
    {
    }
}