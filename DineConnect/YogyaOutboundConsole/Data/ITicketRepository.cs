﻿using DinePlan.DineConnect.Connect.Transaction;

namespace YogyaOutbound.Data
{
    public interface ITicketRepository : IRepository<Ticket, int>
    {
    }
}