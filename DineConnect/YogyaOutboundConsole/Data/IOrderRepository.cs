﻿using DinePlan.DineConnect.Connect.Transaction;

namespace YogyaOutbound.Data
{
    public interface IOrderRepository : IRepository<Order, int>
    {
    }
}