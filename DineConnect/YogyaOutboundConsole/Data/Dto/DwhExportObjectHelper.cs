﻿using System;
using System.Collections.Generic;

namespace YogyaOutboundConsole.Data.Dto
{
    public static class DwhExportObjectHelper
    {
        private static void Example()
        {
            var data = new List<string[]>()
            {
                new string[] { "102", "29/01/17 11:12", "2", "98746" },
                new string[] { "102", "29/01/17 11:12", "2", "98746" },
                new string[] { "102", "29/01/17 11:12", "2", "98746" },
            };

            var header = new string[]
            {
                "store", "datetime", "termnmbr", "transnmbr"
            };

            var alignments = new List<CellAlignment> { CellAlignment.Right, CellAlignment.Right, CellAlignment.Center };

            var text = GetExportTable(data, header, alignments);

        }

        public static string GetExportTable(this List<string[]> data, string[] header = null, List<CellAlignment> alignments = null)
        {
            int columns;
            var columMaxLength = new List<int>();

            GetExportTableMetadata(data, out columns, out columMaxLength, header);
            var text = GetExportTableText(data, columns, columMaxLength, header, alignments);

            return text;
        }

        private static void GetExportTableMetadata(List<string[]> data, out int columns, out List<int> columMaxLength, string[] header = null)
        {
            var cloneData = new List<string[]>(data);

            if (header != null)
                cloneData.Insert(0, header);

            columns = 0;
            columMaxLength = new List<int>();

            for (int row = 0; row < cloneData.Count; row++)
            {
                if (cloneData[row] != null)
                {
                    if (cloneData[row].Length > columns)
                        columns = cloneData[row].Length;

                    for (int column = 0; column < cloneData[row].Length; column++)
                    {
                        if (cloneData[row][column] != null)
                        {
                            if (columMaxLength.Count < column + 1)
                                columMaxLength.Add(cloneData[row][column].Length);
                            else if (columMaxLength[column] < cloneData[row][column].Length)
                                columMaxLength[column] = cloneData[row][column].Length;
                        }
                    }
                }
            }
        }

        private static string GetExportTableText(List<string[]> data, int columns, List<int> columMaxLength, string[] header = null, List<CellAlignment> alignments = null)
        {
            try
            {
                string text = "";

                // Print Header
                if(header != null)
                {
                    for (int column = 0; column < columns; column++)
                    {
                        text += PadCenter(GetNullableString(header[column]), columMaxLength[column]);
                        if (column + 1 < columns)
                            text += " | ";
                    }
                    text += Environment.NewLine;

                    // Print divider header underline
                    for (int column = 0; column < columns; column++)
                    {
                        text += GetLine(columMaxLength[column]);
                        if (column + 1 < columns)
                            text += "-+-";
                    }
                    text += Environment.NewLine;
                }

                int percent = 5;
                // Print data
                for (int row = 0; row < data.Count; row++)
                {
                    // Print percent
                    if(row * 100 / data.Count > percent)
                    {
                        Console.WriteLine($" {percent}% ");
                        percent += 5;
                    } else if(row == data.Count - 1)
                    {
                        percent = 100;
                        Console.WriteLine($" {percent}% ");
                    }

                    for (int column = 0; column < columns; column++)
                    {
                        if (alignments != null && alignments.Count - 1 >= column)
                            text += GetStringWithAlignment(data[row][column], alignments[column], columMaxLength[column]);
                        else
                            text += GetStringWithAlignment(data[row][column], CellAlignment.Left, columMaxLength[column]);

                        if (column + 1 < columns)
                            text += " | ";
                    }
                    text += Environment.NewLine;
                }

                return text;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        public static string GetNullableString(string s)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;
            return s;
        }

        public static string GetStringWithAlignment(string str, CellAlignment alignment, int maxLength)
        {
            switch (alignment)
            {
                case CellAlignment.Left: return GetNullableString(str).PadRight(maxLength);
                case CellAlignment.Right: return GetNullableString(str).PadLeft(maxLength);
                case CellAlignment.Center: return PadCenter(GetNullableString(str), maxLength);
                default: return GetNullableString(str).PadRight(maxLength);
            }
        }

        public static string GetLine(int length)
        {
            if (length > 1)
            {
                string line = "";

                for (int i = 0; i < length; i++)
                    line += "-";

                return line;
            }

            return string.Empty;
        }

        public static string PadCenter(string str, int length)
        {
            int spaces = length - str.Length;
            int padLeft = spaces / 2 + str.Length;
            return str.PadLeft(padLeft).PadRight(length);
        }
    }

    public enum CellAlignment
    {
        Left,
        Right,
        Center
    }
}