﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YogyaOutboundConsole.Data.Dto
{
    public class GoldExportObject
    {
        public string SiteCode { get; set; }
        public string TillCode { get; set; }
        public DateTime SalesDate { get; set; }
        public decimal SalesAmount { get; set; }
        public decimal SalesQuantity { get; set; }
        public string ArticleType { get; set; }
        public bool IsMember { get; set; }
        public string Attributes1 { get; set; }
        public string Attributes2 { get; set; }
    }
}
