﻿using System;
using System.Collections.Generic;

namespace YogyaOutboundConsole.Data.Dto
{
    public class OrcFinSalesExportObject
    {
        public string SiteCode { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public int SalesCode { get; set; }
        public string Category { get; set; }

        public override string ToString()
        {
            if (SalesCode != 9 && SalesCode != 12 && SalesCode != 13 && SalesCode != 14 && SalesCode != 15)
                Category = "";

            return $"{SiteCode};" +
                   $"\"{this.Date.ToString("dd-MMM-yyyy").ToUpper()}\";" +
                   $"\"{Description}\";" +
                   $"{Amount.ToString("#.00")};" +
                   $"{SalesCode};" +
                   $"\"{Category}\";" +
                   $"\n";
        }
    }
    public class OrcFinCashierMedExportObject
    {
        public string CashierId { get; set; }

        public string CashierName { get; set; }

        /// <summary>
        /// Payment Method (Cash/BCA Card/Master Card/Visa Card/ BCA Debit Card/Yogya Voucher/Supplier Voucher)
        /// </summary>
        public string MediaDescription { get; set; }

        public string CurrencySymbol { get; set; }

        /// <summary>
        /// number of transaction using the payment method
        /// </summary>
        public decimal EstimatedCnt { get; set; }

        /// <summary>
        /// amount of transaction using the payment method
        /// </summary>
        public decimal EstimatedAmt { get; set; }

        /// <summary>
        /// not used, fill with 0
        /// </summary>
        public int DeclarationAmt { get => 0; }

        /// <summary>
        /// number of float
        /// </summary>
        public int FloatCnt { get; set; }

        /// <summary>
        /// amount of float
        /// </summary>
        public decimal FloatAmt { get; set; }

        /// <summary>
        /// number of pick up
        /// </summary>
        public int PickupCnt { get; set; }

        /// <summary>
        /// amount of pick up
        /// </summary>
        public int PickupAmt { get; set; }

        /// <summary>
        /// not used, fill with 0
        /// </summary>
        public int PayInCnt { get => 0; }

        /// <summary>
        /// not used, fill with 0
        /// </summary>
        public int PayInAmt { get => 0; }

        /// <summary>
        /// not used, fill with 0
        /// </summary>
        public int PayOutCnt { get => 0; }

        /// <summary>
        /// not used, fill with 0
        /// </summary>
        public int PayOutAmt { get => 0; }

        /// <summary>
        /// = Float Amt - Estimated Amt
        /// </summary>
        public decimal Differences { get => FloatAmt - EstimatedAmt; }

        public override string ToString()
        {
            return $"{CashierId}," +
                   $"{CashierName ?? ""}," +
                   $"{MediaDescription}," +
                   $"{CurrencySymbol}," +
                   $"\"{EstimatedCnt:n0}\"," +
                   $"\"{EstimatedAmt:n0}\"," +
                   $"\"{DeclarationAmt:n0}\"," +
                   $"\"{FloatCnt:n0}\"," +
                   $"\"{FloatAmt:n0}\"," +
                   $"\"{PickupCnt:n0}\"," +
                   $"\"{PickupAmt:n0}\"," +
                   $"\"{PayInCnt:n0}\"," +
                   $"\"{PayInAmt:n0}\"," +
                   $"\"{PayOutCnt:n0}\"," +
                   $"\"{PayOutAmt:n0}\"," +
                   $"\"{Differences:n0}\"" +
                   Environment.NewLine;
        }

        public static string ToTotalString(OrcFinCashierMedExportObject summary)
        {
            return $"{summary.CashierId}," + // Cashier Numer
                   "," + // Cashier Name
                   "," + // Media Description (Payment Method)
                   "," + // Currency Symbol
                   $"\"{summary.EstimatedCnt:n0}\"," +
                   $"\"{summary.EstimatedAmt:n0}\"," +
                   $"\"{summary.DeclarationAmt:n0}\"," +
                   "," + // Float Count
                   $"\"{summary.FloatAmt:n0}\"," +
                   "," + // Pick up Count
                   $"\"{summary.PickupAmt:n0}\"," +
                   "," + // Pay-in Count
                   $"\"{summary.PayInAmt:n0}\"," +
                   "," + // Pay-out Count
                   $"\"{summary.PayOutAmt:n0}\"," +
                   $"\"{summary.Differences:n0}\"" +
                   Environment.NewLine;
        }
    }

    public class SalesCodeTable
    {
        private static SalesCodeTable _instance;

        public static SalesCodeTable Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new SalesCodeTable();

                return _instance;
            }
        }

        private readonly IDictionary<int, string> _data;

        private SalesCodeTable()
        {
            _data = new Dictionary<int, string>()
            {
                { 1, "cash sales" },
                { 2, "credit card" },
                { 3, "gift voucher yogya" },
                { 4, "gift voucher supplier" },
                { 5, "minus cash sales" },
                { 6, "over cash sales" },
                { 9, "others current debts" },
                { 10, "vat – cafetaria" },
                { 11, "vat – sales" },
                { 12, "discount sales outright" },
                { 13, "discount sales consignment" },
                { 14, "outright sales" },
                { 15, "consignment sales" },
            };
        }
    }
}