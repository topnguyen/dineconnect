﻿using DinePlan.DineConnect.Connect.Transaction;

namespace YogyaOutbound.Data
{
    public interface IPaymentRepository : IRepository<Payment, int>
    {
    }
}