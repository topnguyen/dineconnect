﻿using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Users;
using YogyaOutbound.Data.Repositories;

namespace YogyaOutbound.Data
{
    public interface IDinePlanUserRoleRepository : IRepository<DinePlanUserRole, int>
    {
    }
}