﻿using System;
using System.Configuration;
using System.IO;
using Castle.Core.Logging;
using Castle.Facilities.Logging;
using Castle.MicroKernel.Registration;
using Castle.Services.Logging.Log4netIntegration;
using Castle.Windsor;
using YogyaOutbound;
using YogyaOutbound.Data;
using YogyaOutbound.Data.Repositories;
using YogyaOutbound.Services;
using YogyaOutboundConsole.Data;
using YogyaOutboundConsole.Services;

namespace YogyaOutboundConsole

{
    public class Program
    {
        private static void Main(string[] args)
        {
            var container = new WindsorContainer();
            container.AddFacility<LoggingFacility>(f => f.LogUsing<Log4netFactory>());

            var logger = container.Resolve<ILogger>();

            var appConfig = GetAppConfig(logger);

            container.Register(
                Component.For<GoldService>().LifestyleSingleton(),
                Component.For<OracleFinanceService>().LifestyleSingleton(),
                Component.For<DataWareHouseService>().LifestyleSingleton(),
                Component.For<IbobService>().LifestyleSingleton(),
                Component.For<AppConfig>().Instance(appConfig).LifestyleSingleton(),
                Component.For<IWindsorContainer>().Instance(container).LifestyleSingleton(),
                Component.For<YogyaOutboundDbContext>().LifestyleTransient(),
                Component.For<IUnitOfWork>().ImplementedBy<UnitOfWork>().LifestyleTransient()
            );

            int totalReports = 12;

            //var goldService = container.Resolve<GoldService>();
            //Console.WriteLine($"1/{totalReports}"); goldService.ExportGold();

            var oracleFinanceService = container.Resolve<OracleFinanceService>();
            Console.WriteLine($"2/{totalReports}"); oracleFinanceService.ExportSales();
            //Console.WriteLine($"3/{totalReports}"); oracleFinanceService.ExportCashierMediaSummary();

            //var dataWareHouseService = container.Resolve<DataWareHouseService>();
            //Console.WriteLine($"4/{totalReports}"); dataWareHouseService.Export(Enums.Data.DataWareHouseType.EDC);
            //Console.WriteLine($"5/{totalReports}"); dataWareHouseService.Export(Enums.Data.DataWareHouseType.Float);
            //Console.WriteLine($"6/{totalReports}"); dataWareHouseService.Export(Enums.Data.DataWareHouseType.Media);
            //Console.WriteLine($"7/{totalReports}"); dataWareHouseService.Export(Enums.Data.DataWareHouseType.Membership);
            //Console.WriteLine($"8/{totalReports}"); dataWareHouseService.Export(Enums.Data.DataWareHouseType.Promotion);
            //Console.WriteLine($"9/{totalReports}"); dataWareHouseService.Export(Enums.Data.DataWareHouseType.SupervisorIntervention);
            //Console.WriteLine($"10/{totalReports}"); dataWareHouseService.Export(Enums.Data.DataWareHouseType.TotalTransaction);
            //Console.WriteLine($"11/{totalReports}"); dataWareHouseService.Export(Enums.Data.DataWareHouseType.TransactionDetails);

            //var ibobService = container.Resolve<IbobService>();
            //Console.WriteLine($"12/{totalReports}"); ibobService.ExportIbobDplitem();
        }

        private static AppConfig GetAppConfig(ILogger logger)
        {
            try
            {
                var appConfig = new AppConfig
                {
                    InputFolder = ConfigurationManager.AppSettings["InputFolderLocation"],
                    SuccessFolder = ConfigurationManager.AppSettings["SuccessFolderLocation"],
                    FailFolder = ConfigurationManager.AppSettings["FailFolderLocation"]
                };

                var tenant = ConfigurationManager.AppSettings["TenantId"];
                appConfig.Tenant = int.TryParse(tenant, out var second) ? second : 1;
                appConfig.CompanyId = int.TryParse(tenant, out var third) ? third : 1;

                ValidateFolder(logger, appConfig.InputFolder, "Initial Folder - Available");
                ValidateFolder(logger, appConfig.SuccessFolder, "Completed Folder - Available");
                ValidateFolder(logger, appConfig.FailFolder, "Failure Folder - Available");

                return appConfig;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                throw;
            }
        }

        private static void ValidateFolder(ILogger logger, string path, string folderName)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            logger.Info($"{folderName}'s information was checked successfully.");
        }
    }
}