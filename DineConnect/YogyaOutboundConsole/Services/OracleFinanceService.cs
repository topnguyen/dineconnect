﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Castle.Core.Logging;
using Castle.Windsor;
using YogyaOutbound.Data;
using YogyaOutboundConsole.Data.Dto;

namespace YogyaOutbound.Services
{
    public class OracleFinanceService
    {
        private readonly AppConfig _config;
        private readonly IWindsorContainer _container;

        public OracleFinanceService(
            IWindsorContainer container,
            AppConfig config)
        {
            _container = container;
            _config = config;
            Logger = NullLogger.Instance;
        }

        public ILogger Logger { get; set; }

        public void ExportSales()
        {
            Console.WriteLine("Exporting Oracle Finance Sales...");

            var expList = new List<OrcFinSalesExportObject>();
            using (var uow = _container.Resolve<IUnitOfWork>())
            {
                var locations = uow.Locations.GetAll();
                foreach (var location in locations)
                {
                    var orders = uow.Orders.GetAll().Where(x => x.Location.Id == location.Id);

                    var payments = uow.Payments.GetAll();

                    var index = 1;

                    foreach (var order in orders)
                    {
                        var payment = payments.Where(x => x.TicketId == order.TicketId).FirstOrDefault();

                        var description = "";
                        var salesCode = 0;
                        var paymentTypeName = payment != null ? payment.PaymentType.Name : "";
                        if (paymentTypeName.ToLower().Contains("cash"))
                        {
                            description = "cash sales";
                            salesCode = 1;
                        }
                        else if (paymentTypeName.ToLower().Contains("credit") || paymentTypeName.ToLower().Contains("debit"))
                        {
                            description = "cash sales";
                            salesCode = 2;
                        }
                        else if (paymentTypeName.ToLower().Contains("gift voucher yogya"))
                        {
                            description = "gift voucher yogya";
                            salesCode = 3;
                        }
                        else if (paymentTypeName.ToLower().Contains("gift voucher supplier"))
                        {
                            description = "gift voucher supplier";
                            salesCode = 4;
                        }
                        else if (paymentTypeName.ToLower().Contains("minus cash sales"))
                        {
                            description = "minus cash sales";
                            salesCode = 5;
                        }
                        else if (paymentTypeName.ToLower().Contains("over cash sales"))
                        {
                            description = "minus cash sales";
                            salesCode = 6;
                        }
                        else if (paymentTypeName.ToLower().Contains("others current debts"))
                        {
                            description = "others current debts";
                            salesCode = 9;
                        }
                        else if (paymentTypeName.ToLower().Contains("surcharge"))
                        {
                            description = "vat - cafetaria";
                            salesCode = 10;
                        }
                        else if (paymentTypeName.ToLower().Contains("vat - sales"))
                        {
                            description = "vat - sales";
                            salesCode = 11;
                        }
                        else if (paymentTypeName.ToLower().Contains("discount sales outright"))
                        {
                            description = "discount sales outright";
                            salesCode = 12;
                        }
                        else if (paymentTypeName.ToLower().Contains("discount sales outright"))
                        {
                            description = "discount sales outright";
                            salesCode = 13;
                        }
                        else if (paymentTypeName.ToLower().Contains("outright sales"))
                        {
                            description = "outright sales";
                            salesCode = 14;
                        }
                        else if (paymentTypeName.ToLower().Contains("consignment sales"))
                        {
                            description = "consignment sales";
                            salesCode = 15;
                        }

                        expList.Add(new OrcFinSalesExportObject()
                        {
                            SiteCode = order.Location.Code,
                            Date = order.OrderCreatedTime,
                            Amount = order.GetTotal() - order.PromotionAmount,
                            Description = description,
                            SalesCode = salesCode,
                            Category = "" // To do
                        });
                    }

                    if (expList.Count == 0)
                        return;

                    string fileName = $@"{_config.SuccessFolder}SALES_{location.Code}_{DateTime.Now.ToString("MMddyyyy")}.csv";
                    try
                    {
                        // Check if file already exists. If yes, delete it.     
                        if (File.Exists(fileName))
                        {
                            File.Delete(fileName);
                        }

                        // Create a new file     
                        using (FileStream fs = File.Create(fileName))
                        {
                            // Add some text to file    
                            foreach (var expObj in expList)
                            {
                                var text = new UTF8Encoding(true).GetBytes(expObj.ToString());
                                fs.Write(text, 0, text.Length);
                            }
                        }
                    }
                    catch (Exception Ex)
                    {
                        Console.WriteLine(Ex.ToString());
                    }
                }
            }
        }
        public void ExportCashierMediaSummary()
        {
            Console.WriteLine("Exporting Cashier Media Summary...");

            string header = "Cashier Number," +
                "Cashier Name," +
                "Media Description," +
                "Curr. Symbol," +
                "Estimated Cnt," +
                "Estimated Amt," +
                "Declaration Amt," +
                "Float Cnt," +
                "Float Amt," +
                "Pickup Cnt," +
                "Pickup Amt," +
                "Pay-in Cnt," +
                "Pay-in Amt," +
                "Pay-out Cnt," +
                "Pay-out Amt," +
                "Differences";

            var expList = new List<OrcFinCashierMedExportObject>();

            using (var uow = _container.Resolve<IUnitOfWork>())
            {
                var payments = uow.Payments.GetAll();
                var locations = uow.Locations.GetAll();
                {
                    foreach (var location in locations)
                    {
                        var paymentBasedOnLoctions = payments.Where(x => x.Ticket.LocationId == location.Id);
                        foreach (var payment in paymentBasedOnLoctions)
                        {
                            expList.Add(new OrcFinCashierMedExportObject()
                            {
                                CashierId = "", // Todo
                                CashierName = payment.PaymentUserName,
                                MediaDescription = payment.PaymentType.Name,
                                CurrencySymbol = "", // Todo
                                EstimatedCnt = 0, // Todo
                                EstimatedAmt = payment.Ticket.TotalAmount,
                                FloatCnt = 0, // Todo
                                FloatAmt = payment.Amount,
                                PickupCnt = 0, // Todo
                                PickupAmt = 0, // Todo
                            });
                        }
                        if (expList.Count == 0)
                            return;

                        string storeCode = location.Name;
                        DateTime date = DateTime.Now;
                        string fileName = $@"{_config.SuccessFolder}{storeCode}_{date:yyyyMMddHHmmss}._reports_CashierMediaD.csv";
                        try
                        {
                            // Check if file already exists. If yes, delete it.     
                            if (File.Exists(fileName))
                            {
                                File.Delete(fileName);
                            }

                            // Create a new file     
                            using (FileStream fs = File.Create(fileName))
                            {
                                var headerBytes = new UTF8Encoding(true).GetBytes(header + Environment.NewLine);
                                fs.Write(headerBytes, 0, headerBytes.Length);

                                var groupExpList = new List<OrcFinCashierMedExportObject>();

                                var cashierGroupList = expList.GroupBy(x => x.CashierName);

                                foreach (var cashier in cashierGroupList)
                                {
                                    var mediaGroups = cashier.GroupBy(x => x.MediaDescription);

                                    foreach (var media in mediaGroups)
                                    {
                                        groupExpList.Add(new OrcFinCashierMedExportObject()
                                        {
                                            CashierId = media.First().CashierId,
                                            CashierName = media.First().CashierName,
                                            CurrencySymbol = media.First().CurrencySymbol,
                                            EstimatedAmt = media.Sum(x => x.EstimatedAmt),
                                            EstimatedCnt = media.Sum(x => x.EstimatedCnt),
                                            FloatAmt = media.Sum(x => x.FloatAmt),
                                            FloatCnt = media.Sum(x => x.FloatCnt),
                                            MediaDescription = media.First().MediaDescription,
                                            PickupAmt = media.Sum(x => x.PickupAmt),
                                            PickupCnt = media.Sum(x => x.PickupCnt),
                                        });
                                    }
                                }

                                var cashierGroups = groupExpList.GroupBy(x => x.CashierName);

                                var totalAllCashiers = new OrcFinCashierMedExportObject()
                                {
                                    CashierId = "Total for all Cashier(s)"
                                };

                                foreach (var cashier in cashierGroups)
                                {
                                    var cashierSummary = new OrcFinCashierMedExportObject()
                                    {
                                        CashierId = "Total fir Cashier Number " + (cashier.First().CashierId ?? "")
                                    };

                                    foreach (var item in cashier)
                                    {
                                        if (item != cashier.First())
                                        {
                                            item.CashierId = "";
                                            item.CashierName = "";
                                        }

                                        cashierSummary.EstimatedCnt += item.EstimatedCnt;
                                        cashierSummary.EstimatedAmt += item.EstimatedAmt;
                                        cashierSummary.FloatAmt += item.FloatAmt;
                                        cashierSummary.PickupAmt += item.PickupAmt;

                                        var itemText = new UTF8Encoding(true).GetBytes(item.ToString());
                                        fs.Write(itemText, 0, itemText.Length);
                                    }

                                    var cashierSummaryText = new UTF8Encoding(true).GetBytes(
                                        OrcFinCashierMedExportObject.ToTotalString(cashierSummary));
                                    fs.Write(cashierSummaryText, 0, cashierSummaryText.Length);

                                    var endLine = new UTF8Encoding(true).GetBytes(Environment.NewLine);
                                    fs.Write(endLine, 0, endLine.Length);

                                    totalAllCashiers.EstimatedCnt += cashierSummary.EstimatedCnt;
                                    totalAllCashiers.EstimatedAmt += cashierSummary.EstimatedAmt;
                                    totalAllCashiers.FloatAmt += cashierSummary.FloatAmt;
                                    totalAllCashiers.PickupAmt += cashierSummary.PickupAmt;
                                }
                                var totalSummaryText = new UTF8Encoding(true).GetBytes(
                                    OrcFinCashierMedExportObject.ToTotalString(totalAllCashiers));
                                fs.Write(totalSummaryText, 0, totalSummaryText.Length);
                            }
                        }
                        catch (Exception Ex)
                        {
                            Console.WriteLine(Ex.ToString());
                        }

                    }
                }
            }
        }
    }
}