﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Castle.Core.Logging;
using Castle.Windsor;
using DinePlan.DineConnect.Connect.Transaction;
using YogyaOutbound.Data;
using YogyaOutboundConsole.Data.Dto;

namespace YogyaOutbound.Services
{
    public class IbobService
    {

        private readonly AppConfig _config;
        private readonly IWindsorContainer _container;

        public IbobService(
            IWindsorContainer container,
            AppConfig config)
        {
            _container = container;
            _config = config;
            Logger = NullLogger.Instance;
        }

        public ILogger Logger { get; set; }

        public void ExportIbobDplitem()
        {
            Console.WriteLine("Exporting Ibob Dplitem...");
            using (var uow = _container.Resolve<IUnitOfWork>())
            {
                var tickets = uow.Tickets.GetAll().ToList();
                var locationIds = tickets.Select(x => x.LocationId).Distinct().ToList();
                foreach (var locationId in locationIds)
                {
                    var location = uow.Locations.Get(locationId);
                    var data = new List<string[]>();

                    foreach (var groupTickets in tickets.Where(x=>x.LocationId == locationId).GroupBy(x=>x.CreatorUserId))
                    {
                        var firstTicket = groupTickets.FirstOrDefault();
                        if(firstTicket != null)
                        {
                            var orders = new List<Order>();

                            foreach (var ticket in groupTickets)
                            {
                                if (ticket.Orders != null)
                                    orders.AddRange(ticket.Orders);
                            }

                            var noOfProducts = orders.Select(o => o.MenuItemId)?.Distinct().Count() ?? 0;

                            data.Add(new string[]
                            {
                                (location?.Code ?? "").PadRight(9),
                                (firstTicket.CreatorUserId?.ToString() ?? "").PadLeft(9),
                                (firstTicket.TerminalName ?? "").PadLeft(9),
                                noOfProducts.ToString().PadLeft(5),
                                firstTicket.CreationTime.ToString("yyyyMMdd00"),
                            });
                        }
                    }

                    var dataText = data.GetExportTable();
                    string fileName = $@"{_config.SuccessFolder}Dplitem_{DateTime.Today:yyyyMMdd}.{location?.Code ?? ""}";
                    try
                    {
                        // Check if file already exists. If yes, delete it.     
                        if (File.Exists(fileName))
                        {
                            File.Delete(fileName);
                        }

                        // Create a new file     
                        using (FileStream fs = File.Create(fileName))
                        {
                            var bytes = new UTF8Encoding(true).GetBytes(dataText);
                            fs.Write(bytes, 0, bytes.Length);
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }
    }
}
