﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Castle.Core.Logging;
using Castle.Windsor;
using DinePlan.DineConnect.Connect.Master;
using YogyaOutbound.Data;
using YogyaOutboundConsole.Data.Dto;

namespace YogyaOutbound.Services
{
    public class GoldService
    {
        private readonly AppConfig _config;
        private readonly IWindsorContainer _container;

        public GoldService(
            IWindsorContainer container,
            AppConfig config)
        {
            _container = container;
            _config = config;
            Logger = NullLogger.Instance;
        }

        public ILogger Logger { get; set; }


        public void ExportGold()
        {
            Console.WriteLine("Exporting Gold...");

            var expList = new List<GoldExportObject>();

            using (var uow = _container.Resolve<IUnitOfWork>())
            {
                var locations = uow.Locations.GetAll();
                foreach (var location in locations)
                {
                    var orders = uow.Orders.GetAll().Where(x => x.Location.Id == location.Id);

                    var index = 1;

                    foreach (var order in orders)
                    {
                        expList.Add(new GoldExportObject()
                        {
                            SiteCode = order.Location.Code,
                            TillCode = "", // Todo
                            SalesDate = order.OrderCreatedTime,
                            SalesAmount = order.GetTotal() - order.PromotionAmount,
                            SalesQuantity = order.Quantity,
                            ArticleType = "", //Todo
                            Attributes1 = "", //Todo
                            IsMember = false, // Todo,
                            Attributes2 = "" //Todo
                        });

                        Console.WriteLine($"{index++}/{orders.Count()}");
                    }

                    if (expList.Count == 0)
                        return;
                    string siteCode = location?.Code;
                    DateTime date = DateTime.Now;
                    string fileName = $@"{_config.SuccessFolder}SALES{siteCode.ToUpper()}{date:ddMMyy}.dpl";
                    try
                    {
                        // Check if file already exists. If yes, delete it.     
                        if (File.Exists(fileName))
                        {
                            File.Delete(fileName);
                        }

                        // Create a new file     
                        using (FileStream fs = File.Create(fileName))
                        {
                            // Add some text to file    
                            foreach (var expObj in expList)
                            {
                                if (expObj.SalesAmount != 0)
                                {
                                    if (expObj.SiteCode.Length < 5)
                                    {
                                        var length = expObj.SiteCode.Length;
                                        for (int i = 0; i < 5 - length; i++)
                                        {
                                            expObj.SiteCode = $"0{expObj.SiteCode}";
                                        }
                                    }

                                    if (expObj.TillCode.Length < 14)
                                    {
                                        var length = expObj.TillCode.Length;
                                        for (int i = 0; i < 14 - length; i++)
                                        {
                                            expObj.TillCode += " ";
                                        }
                                    }

                                    var salesAmount = expObj.SalesAmount.ToString();

                                    if (salesAmount.Length < 15)
                                    {
                                        var length = salesAmount.Length;
                                        for (int i = 0; i < 15 - length; i++)
                                        {
                                            salesAmount = $" {salesAmount}";
                                        }
                                    }

                                    var salesQuantity = expObj.SalesQuantity.ToString();
                                    if (salesQuantity.Length < 10)
                                    {
                                        var length = salesQuantity.Length;
                                        for (int i = 0; i < 10 - length; i++)
                                        {
                                            salesQuantity = $" {salesQuantity}";
                                        }
                                    }

                                    var articleType = expObj.ArticleType.ToString();
                                    if (articleType.Length < 3)
                                    {
                                        var length = articleType.Length;
                                        for (int i = 0; i < 3 - length; i++)
                                        {
                                            articleType = $" {articleType}";
                                        }
                                    }

                                    var attributes1 = expObj.Attributes1.ToString();
                                    if (attributes1.Length < 20)
                                    {
                                        var length = attributes1.Length;
                                        for (int i = 0; i < 20 - length; i++)
                                        {
                                            attributes1 = $" {attributes1}";
                                        }
                                    }

                                    var attributes2 = expObj.Attributes2.ToString();
                                    if (attributes2.Length < 20)
                                    {
                                        var length = attributes2.Length;
                                        for (int i = 0; i < 20 - length; i++)
                                        {
                                            attributes2 = $" {attributes2}";
                                        }
                                    }

                                    var member = expObj.IsMember ? "1" : "0";

                                    var bytes = new UTF8Encoding(true).GetBytes($"{expObj.SiteCode}" +
                                        $"{expObj.TillCode}{DateTime.Now.ToString("ddMMyy")}" +
                                        $"{salesAmount}{salesQuantity}{articleType}{member}" +
                                        $"{attributes1}{attributes2}\n");

                                    fs.Write(bytes, 0, bytes.Length);
                                }
                            }
                        }
                    }
                    catch (Exception Ex)
                    {
                        Console.WriteLine(Ex.ToString());
                    }

                }
            }
        }

        private string FormatDecimal(decimal value)
        {
            var valueString = value.ToString();
            return valueString.Length > 3
                ? valueString.Insert(valueString.Length - 3, ".")
                : valueString;
        }
    }
}