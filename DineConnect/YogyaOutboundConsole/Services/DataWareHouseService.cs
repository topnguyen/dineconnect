﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Castle.Core.Logging;
using Castle.Windsor;
using YogyaOutbound;
using YogyaOutbound.Data;
using YogyaOutboundConsole.Data.Dto;
using YogyaOutboundConsole.Enums.Data;

namespace YogyaOutboundConsole.Services
{
    public class DataWareHouseService
    {
        private readonly AppConfig _config;
        private readonly IWindsorContainer _container;
        public DataWareHouseService(
            IWindsorContainer container,
            AppConfig config)
        {
            _container = container;
            _config = config;
            Logger = NullLogger.Instance;
        }

        public ILogger Logger { get; set; }

        public void Export(DataWareHouseType type)
        {
            switch (type)
            {
                case DataWareHouseType.EDC:
                    ExportEDC();
                    break;
                case DataWareHouseType.Float:
                    ExportFloat();
                    break;
                case DataWareHouseType.Media:
                    ExportMedia();
                    break;
                case DataWareHouseType.Membership:
                    ExportMembership();
                    break;
                case DataWareHouseType.Promotion:
                    ExportPromotion();
                    break;
                case DataWareHouseType.SupervisorIntervention:
                    ExportSupervisorIntervention();
                    break;
                case DataWareHouseType.TotalTransaction:
                    ExportTotalTransaction();
                    break;
                case DataWareHouseType.TransactionDetails:
                    ExportTransactionDetails();
                    break;
            }
        }

        private void ExportEDC()
        {
            Console.WriteLine("Exporting EDC...");
            var headers = new string[]
            {
                "store",
                "date",
                "termnmbr",
                "transnmbr",
                "edc"
            };
            using (var uow = _container.Resolve<IUnitOfWork>())
            {
                var tickets = uow.Tickets.GetAll().ToList();
                var locationIds = tickets.Select(x => x.LocationId).Distinct().ToList();
                var data = new List<string[]>();
                foreach (var locationId in locationIds)
                {
                    var location = uow.Locations.Get(locationId);
                    foreach (var ticket in tickets)
                    {
                        data.Add(new string[]
                        {
                            location?.Code ?? "",
                            ticket.LastPaymentTimeTruc.ToString("dd/MM/yy"),
                            ticket.TerminalName,
                            ticket.TicketNumber,
                            "", //Todo
                        });
                    }
                    var dataText = data.GetExportTable(headers);
                    string fileName = $@"{_config.SuccessFolder}DplMkgEDC{DateTime.Today:yyyyMMdd}.{location?.Code ?? ""}";
                    try
                    {
                        // Check if file already exists. If yes, delete it.     
                        if (File.Exists(fileName))
                        {
                            File.Delete(fileName);
                        }

                        // Create a new file     
                        using (FileStream fs = File.Create(fileName))
                        {
                            var bytes = new UTF8Encoding(true).GetBytes(dataText);
                            fs.Write(bytes, 0, bytes.Length);
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }

        private void ExportFloat()
        {
            Console.WriteLine("Exporting Float...");
            var headers = new string[]
            {
                "store",
                "date",
                "termnmbr",
                "transnmbr",
                "mediaamt",
                "cshrnmbr",
                "empname"
            };
            using (var uow = _container.Resolve<IUnitOfWork>())
            {
                var tickets = uow.Tickets.GetAll().ToList();
                var locationIds = tickets.Select(x => x.LocationId).Distinct().ToList();
                var data = new List<string[]>();
                foreach (var locationId in locationIds)
                {
                    var location = uow.Locations.Get(locationId);
                    foreach (var ticket in tickets)
                    {
                        data.Add(new string[]
                        {
                            location?.Code ?? "",
                            ticket.LastPaymentTimeTruc.ToString("dd/MM/yy"),
                            ticket.TerminalName,
                            ticket.WorkPeriodId.ToString(),
                            ticket.TotalAmount.ToString(),
                            ticket.LastModifierUserId.ToString(),
                            ticket.LastModifiedUserName
                        });
                    }
                    var dataText = data.GetExportTable(headers);
                    string fileName = $@"{_config.SuccessFolder}DplMkgFloat{DateTime.Today:yyyyMMdd}.{location?.Code ?? ""}";
                    try
                    {
                        // Check if file already exists. If yes, delete it.     
                        if (File.Exists(fileName))
                        {
                            File.Delete(fileName);
                        }

                        // Create a new file     
                        using (FileStream fs = File.Create(fileName))
                        {
                            var bytes = new UTF8Encoding(true).GetBytes(dataText);
                            fs.Write(bytes, 0, bytes.Length);
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }

        private void ExportMedia()
        {
            Console.WriteLine("Exporting Media...");
            var headers = new string[]
            {
                "store",
                "datetime",
                "termnmbr",
                "transnmbr",
                "mdesc",
                "mediaamnt",
                "accountnmbr"
            };
            using (var uow = _container.Resolve<IUnitOfWork>())
            {
                var tickets = uow.Tickets.GetAll().ToList();
                var payments = uow.Payments.GetAll().ToList();
                var paymentTypes = uow.PaymentTypes.GetAll().ToList();
                var locationIds = tickets.Select(x => x.LocationId).Distinct().ToList();
                var data = new List<string[]>();
                foreach (var locationId in locationIds)
                {
                    var location = uow.Locations.Get(locationId);
                    foreach (var ticket in tickets.Where(x => x.LocationId == locationId).ToList())
                    {
                        var payment = payments.FirstOrDefault(x => x.TicketId == ticket.TicketId);
                        if (payment == null)
                            continue;
                        var paymentType = paymentTypes.FirstOrDefault(x => x.Id == payment.PaymentTypeId);

                        if (!(paymentType is null))
                            data.Add(new string[]
                            {
                                location?.Code ?? "",
                                ticket.LastPaymentTimeTruc.ToString("dd/MM/yy HH:mm"),
                                ticket.TerminalName,
                                ticket.TicketNumber.ToString(),
                                paymentType.Name,
                                payment.Amount.ToString(),
                                "" //To do
                            });
                    }
                    var dataText = data.GetExportTable(headers);
                    string fileName = $@"{_config.SuccessFolder}DplMkgMed{DateTime.Today:yyyyMMdd}.{location?.Code ?? ""}";
                    try
                    {
                        // Check if file already exists. If yes, delete it.     
                        if (File.Exists(fileName))
                        {
                            File.Delete(fileName);
                        }

                        // Create a new file     
                        using (FileStream fs = File.Create(fileName))
                        {
                            var bytes = new UTF8Encoding(true).GetBytes(dataText);
                            fs.Write(bytes, 0, bytes.Length);
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }

        private void ExportMembership()
        {
            Console.WriteLine("Exporting Membership...");
            var headers = new string[]
            {
                "store",
                "datetime",
                "termnmbr",
                "transnmbr",
                "member",
                "cshrnmbr",
                "empname",
                "accountnmbr"
            };
            using (var uow = _container.Resolve<IUnitOfWork>())
            {
                var tickets = uow.Tickets.GetAll().ToList();
                var locationIds = tickets.Select(x => x.LocationId).Distinct().ToList();
                var data = new List<string[]>();
                foreach (var locationId in locationIds)
                {
                    var location = uow.Locations.Get(locationId);
                    foreach (var ticket in tickets)
                    {
                        data.Add(new string[]
                        {
                            location?.Code ?? "",
                            ticket.LastPaymentTimeTruc.ToString("dd/MM/yy HH:mm"),
                            ticket.TerminalName,
                            ticket.TicketNumber,
                            "", //Todo
                            ticket.LastModifierUserId.ToString(),
                            ticket.LastModifiedUserName,
                            "" //Todo
                        });
                    }
                    var dataText = data.GetExportTable(headers);
                    string fileName = $@"{_config.SuccessFolder}DplMkgMem{DateTime.Today:yyyyMMdd}.{location?.Code ?? ""}";
                    try
                    {
                        // Check if file already exists. If yes, delete it.     
                        if (File.Exists(fileName))
                        {
                            File.Delete(fileName);
                        }

                        // Create a new file     
                        using (FileStream fs = File.Create(fileName))
                        {
                            var bytes = new UTF8Encoding(true).GetBytes(dataText);
                            fs.Write(bytes, 0, bytes.Length);
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }

        private void ExportPromotion()
        {
            Console.WriteLine("Exporting Promotion...");
            var headers = new string[]
            {
                "store",
                "date",
                "termnmbr",
                "transnmbr",
                "disclinkid",
                "linkdescription",
                "qty",
                "value",
                "printmsg",
            };
            using (var uow = _container.Resolve<IUnitOfWork>())
            {
                var tickets = uow.Tickets.GetAll().ToList();
                var orders = uow.Orders.GetAll().ToList();
                var locations = orders.Select(x => x.Ticket.Location).Distinct().ToList();
                var data = new List<string[]>();
                foreach (var location in locations)
                {
                    foreach (var order in orders.Where(x => x.Ticket.LocationId == location.Id))
                    {
                        data.Add(new string[]
                        {
                            location?.Code ?? "",
                            order.CreationTime.ToString("dd/MM/yy"),
                            order.Ticket.TerminalName,
                            order.Id.ToString(),
                            order.PromotionSyncId.ToString(),
                            "", // Todo: linkdescription: promo descriotiom
                            "", // Todo: qty: qty promo
                            order.PromotionAmount.ToString(),
                            "", // Todo: printmsg: article code for item which have promotion

                        });
                    }
                    var dataText = data.GetExportTable(headers);
                    string fileName = $@"{_config.SuccessFolder}DplMkgPromo{DateTime.Today:yyyyMMdd}.{location?.Code ?? ""}";
                    try
                    {
                        // Check if file already exists. If yes, delete it.     
                        if (File.Exists(fileName))
                        {
                            File.Delete(fileName);
                        }

                        // Create a new file     
                        using (FileStream fs = File.Create(fileName))
                        {
                            var bytes = new UTF8Encoding(true).GetBytes(dataText);
                            fs.Write(bytes, 0, bytes.Length);
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }

        private void ExportSupervisorIntervention()
        {
            Console.WriteLine("Exporting Supervisor Intervention...");
            var headers = new string[]
            {
                "store",
                "date",
                "termnmbr",
                "transnmbr",
                "supervisor",
                "cshrnmbr",
                "funcdesc",
                "amount",
                "spvname",
            };
            using (var uow = _container.Resolve<IUnitOfWork>())
            {
                var tickets = uow.Tickets.GetAll().ToList();
                var trans = uow.Transactions.GetAll();
                var locationIds = tickets.Select(x => x.LocationId).Distinct().ToList();
                var data = new List<string[]>();
                foreach (var locationId in locationIds)
                {
                    var location = uow.Locations.Get(locationId);
                    foreach (var tx in trans.Where(x => x.Ticket.LocationId == locationId))
                    {
                        data.Add(new string[]
                        {
                            location?.Code ?? "",
                            tx.CreationTime.ToString("dd/MM/yy"),
                            tx.Ticket.TerminalName,
                            tx.Id.ToString(),
                            "", // Todo: supervisor: supervisor ID
                            tx.Ticket.CreatorUserId.ToString(), 
                            "", // Todo: funcdesc: what intervention
                            "", // Todo: amount: intervention value is the Amount?
                            "", // Todo: spvname: supervisor name
                        });
                    }
                    var dataText = data.GetExportTable(headers);
                    string fileName = $@"{_config.SuccessFolder}DplMkgSupInt{DateTime.Today:yyyyMMdd}.{location?.Code ?? ""}";
                    try
                    {
                        // Check if file already exists. If yes, delete it.     
                        if (File.Exists(fileName))
                        {
                            File.Delete(fileName);
                        }

                        // Create a new file     
                        using (FileStream fs = File.Create(fileName))
                        {
                            var bytes = new UTF8Encoding(true).GetBytes(dataText);
                            fs.Write(bytes, 0, bytes.Length);
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }

        private void ExportTotalTransaction()
        {
            Console.WriteLine("Exporting Total Transaction...");
            var headers = new string[]
            {
                "store",
                "date",
                "termnmbr",
                "transnmbr",
                "amnt",
                "posgrpnmbr",
            };
            using (var uow = _container.Resolve<IUnitOfWork>())
            {
                var tickets = uow.Tickets.GetAll().ToList();
                var trans = uow.Transactions.GetAll();
                var locationIds = tickets.Select(x => x.LocationId).Distinct().ToList();
                var data = new List<string[]>();
                foreach (var locationId in locationIds)
                {
                    var location = uow.Locations.Get(locationId);
                    foreach (var tx in trans.Where(x => x.Ticket.LocationId == locationId))
                    {
                        data.Add(new string[]
                        {
                            location?.Code ?? "",
                            tx.CreationTime.ToString("dd/MM/yy"),
                            tx.Ticket.TerminalName,
                            tx.Id.ToString(),
                            tx.Amount.ToString(), // Question: Total transaction is the Amount?
                            "", // Todo: posgrpnmbr: first 2 digit from site_code
                        });
                    }
                    var dataText = data.GetExportTable(headers);
                    string fileName = $@"{_config.SuccessFolder}DplMkgTotal{DateTime.Today:yyyyMMdd}.{location?.Code ?? ""}";
                    try
                    {
                        // Check if file already exists. If yes, delete it.     
                        if (File.Exists(fileName))
                        {
                            File.Delete(fileName);
                        }

                        // Create a new file     
                        using (FileStream fs = File.Create(fileName))
                        {
                            var bytes = new UTF8Encoding(true).GetBytes(dataText);
                            fs.Write(bytes, 0, bytes.Length);
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }

        private void ExportTransactionDetails()
        {
            Console.WriteLine("Exporting Transaction Details...");
            var headers = new string[]
            {
                "store",
                "date",
                "termnmbr",
                "transnmbr",
                "plunmbr",
                "pludesc",
                "div",
                "gross",
                "qty",
                "disc",
                "sp",
                "skunmbr",
            };
            using (var uow = _container.Resolve<IUnitOfWork>())
            {
                var tickets = uow.Tickets.GetAll().ToList();
                var trans = uow.Transactions.GetAll();
                var locationIds = tickets.Select(x => x.LocationId).Distinct().ToList();
                var data = new List<string[]>();
                foreach (var locationId in locationIds)
                {
                    var location = uow.Locations.Get(locationId);
                    foreach (var tx in trans.Where(x => x.Ticket.LocationId == locationId))
                    {
                        data.Add(new string[]
                        {
                            location?.Code ?? "",
                            tx.CreationTime.ToString("dd/MM/yy"),
                            tx.Ticket.TerminalName,
                            tx.Id.ToString(),
                            "", // Todo: plunmbr: Item Number in Entry Number Record (page 7)
                            "", // Todo: pludesc: plu description
                            "", // Todo: div: merchandise hierarchy (category code 2 digit)
                            tx.Amount.ToString(), // Todo: gross price (before promotion discount)
                            "", // Todo: qty: quantity
                            "", // Todo: disc: discount value
                            "", // Todo: contain of 6 digits, example: 101001. digit 1 return (if return, fill 1, if not fill 0) digit 2 PPN / VAT. digit 3 PB1. digit 4 - 6 sales variance(001)
                            "", // Todo: skunmbr: Entry Number in Entry Number Record (page 7)
                        });
                    }
                    var dataText = data.GetExportTable(headers);
                    string fileName = $@"{_config.SuccessFolder}DplMkgTrn{DateTime.Today:yyyyMMdd}.{location?.Code ?? ""}";
                    try
                    {
                        // Check if file already exists. If yes, delete it.     
                        if (File.Exists(fileName))
                        {
                            File.Delete(fileName);
                        }

                        // Create a new file     
                        using (FileStream fs = File.Create(fileName))
                        {
                            var bytes = new UTF8Encoding(true).GetBytes(dataText);
                            fs.Write(bytes, 0, bytes.Length);
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }
    }
}