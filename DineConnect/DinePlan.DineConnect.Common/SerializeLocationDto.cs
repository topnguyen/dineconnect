﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Common
{
    [DataContract]

    public class SerializeLocationDto
    {
        public virtual int Id { get; set; }
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual long OrganizationUnitId { get; set; }
        public virtual int CompanyRefId { get; set; }
    }
}
