﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XeroLibrary
{
    public interface IXeroAuthorizationService
    {
        string BuildLoginUrl(string clientid);

        Task<XeroToken> GetToken(string authorizationCode);
        Task<XeroToken> RefreshToken(string refreshToken);
    }
}
