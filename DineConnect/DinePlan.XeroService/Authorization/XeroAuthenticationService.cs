﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace XeroLibrary
{
    public class XeroAuthenticationService : IXeroAuthorizationService
    {

        const string client_id = "04DBCBD5AB4140E39ED1598E43B5D9C7";
        const string client_secret = "5TVcSOEgoHhJiHo1DsVPQRGK6KI23eaMvS3TvSjjwMlFE8ib";
        const string secret = "aTN61uiqPfLu4Vd_JZ3pXXWKMGZIeLQOtA7h2tF_96cRLQUA";
        const string redirect_url = "http://localhost:3000";


        public string BuildLoginUrl(string clientid)
        {
            return string.Format("https://login.xero.com/identity/connect/authorize?response_type=code&client_id={0}&redirect_uri={1}&scope=openid profile email accounting.transactions&state=123", client_id, redirect_url);
        }

        public async Task<XeroToken> GetToken(string authorizationCode)
        {
            using(var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri("https://identity.xero.com/connect/token");
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", "MDREQkNCRDVBQjQxNDBFMzlFRDE1OThFNDNCNUQ5Qzc6NVRWY1NPRWdvSGhKaUhvMURzVlBRUkdLNktJMjNlYU12UzNUdlNqandNbEZFOGli");
                var content = new StringContent(string.Format("grant_type=authorization_code&code={0}&redirect_uri={1}", authorizationCode, redirect_url), Encoding.UTF8, "application/x-www-form-urlencoded");
                var result = await httpClient.PostAsync("", content);
                var resultInString = await result.Content.ReadAsStringAsync();
                var token = JsonConvert.DeserializeObject<XeroToken>(resultInString);
                return token;
            }

        }

        public Task<XeroToken> RefreshToken(string refreshToken)
        {
            throw new NotImplementedException();
        }
    }

}
