﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using XeroLibrary.Data;

namespace XeroLibrary.RestClient
{
    public class XeroRestClient : HttpClient
    {
        private Uri API_ENDPOINT = new Uri("https://api.xero.com/api.xro/2.0/");
        private string tenantId;
        private string accessToken;

        private XeroRestClient() { }
        private XeroRestClient(string tenantId, string accessToken)
        {
            this.tenantId = tenantId;
            this.accessToken = accessToken;
            this.BaseAddress = API_ENDPOINT;
            this.DefaultRequestHeaders.Add("Xero-tenant-id", this.tenantId);
            this.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.accessToken);
        }

        public Task<HttpResponseMessage> GetXeroInfo(string path)
        {
            return this.GetAsync(API_ENDPOINT + "/" + path);
        }

        public static XeroRestClient Create(string tenantId, string accessToken)
        {
            return new XeroRestClient(tenantId, accessToken);
        }   
    }
}
