﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using XeroLibrary.Data;

namespace XeroLibrary.Extensions
{
    public static class XeroEntityExtensions
    {
        public static string ToJson(this IXeroEntity entity)
        {
            return JsonConvert.SerializeObject(entity);
        }

        public static StringContent ToHttpStringContent(this IXeroEntity entity)
        {
            var content = new StringContent(entity.ToJson(), Encoding.UTF8, "application/json");
            return content;
        }

    }
}
