﻿using Abp.Application.Editions;
using Abp.Application.Features;
using Abp.Domain.Repositories;

namespace DinePlan.DineConnect.Editions
{
    public class EditionManager : AbpEditionManager
    {
        public const string ConnectEdition = "DineConnect";
        public EditionManager(
            IRepository<Edition> editionRepository, 
            IRepository<EditionFeatureSetting, long> editionFeatureRepository) 
            : base(
                editionRepository, 
                editionFeatureRepository
            )
        {

        }
    }
}
