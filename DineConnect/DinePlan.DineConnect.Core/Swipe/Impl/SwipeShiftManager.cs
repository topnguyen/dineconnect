﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Swipe;
using Abp.UI;

namespace DinePlan.DineConnect.House.Impl
{
	public class SwipeShiftManager : DineConnectServiceBase, ISwipeShiftManager, ITransientDependency
	{

		private readonly IRepository<SwipeShift> _swipeShiftRepo;

		public SwipeShiftManager(IRepository<SwipeShift> swipeShift)
		{
			_swipeShiftRepo = swipeShift;
		}

		public async Task<IdentityResult> CreateSync(SwipeShift swipeShift)
		{

			//  if the New Addition
			if (swipeShift.Id == 0)
			{
				//	Check any Unclosed Shifts Exists

				var existUnclosed = await _swipeShiftRepo.FirstOrDefaultAsync(t => t.UserId == swipeShift.UserId && t.ShiftEndTime == null);
				if (existUnclosed != null)
				{
					string[] strArrays = { L("ShiftNotClosedForThisUser", existUnclosed.ShiftStartTime.ToString("dd-MMM-yyyy HH:mm")) };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;

					//	throw new UserFriendlyException(L("",);
				}
				await _swipeShiftRepo.InsertAndGetIdAsync(swipeShift);
				return IdentityResult.Success;

			}
			else
			{
				return IdentityResult.Success;
			}
		}

	}
}