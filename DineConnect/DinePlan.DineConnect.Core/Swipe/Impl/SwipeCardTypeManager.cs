﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Swipe;

namespace DinePlan.DineConnect.House.Impl
{
	public class SwipeCardTypeManager : DineConnectServiceBase, ISwipeCardTypeManager, ITransientDependency
	{

		private readonly IRepository<SwipeCardType> _swipeCardTypeRepo;

		public SwipeCardTypeManager(IRepository<SwipeCardType> swipeCardType)
		{
			_swipeCardTypeRepo = swipeCardType;
		}

		public async Task<IdentityResult> CreateSync(SwipeCardType swipeCardType)
		{
			//  if the New Addition
			if (swipeCardType.Id == 0)
			{
				if (_swipeCardTypeRepo.GetAll().Any(a => a.Name.Equals(swipeCardType.Name)))
				{
					string[] strArrays = { L("NameAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				await _swipeCardTypeRepo.InsertAndGetIdAsync(swipeCardType);
				return IdentityResult.Success;

			}
			else
			{
				List<SwipeCardType> lst = _swipeCardTypeRepo.GetAll().Where(a => a.Name.Equals(swipeCardType.Name) && a.Id != swipeCardType.Id).ToList();
				if (lst.Count > 0)
				{
					string[] strArrays = { L("NameAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				return IdentityResult.Success;
			}
		}

	}
}

