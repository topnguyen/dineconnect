﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Swipe;
using DinePlan.DineConnect.Engage;

namespace DinePlan.DineConnect.House.Impl
{
	public class SwipeMemberManager : DineConnectServiceBase, ISwipeMemberManager, ITransientDependency
	{

		private readonly IRepository<ConnectMember> _swipeMemberRepo;

		public SwipeMemberManager(IRepository<ConnectMember> swipeMember)
		{
			_swipeMemberRepo = swipeMember;
		}

		public async Task<IdentityResult> CreateSync(ConnectMember swipeMember)
		{
			//  if the New Addition
			if (swipeMember.Id == 0)
			{
				if (_swipeMemberRepo.GetAll().Any(a => a.MemberCode.ToUpper().Equals(swipeMember.MemberCode.ToUpper())))
				{
					string[] strArrays = { L("MemberCodeAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				await _swipeMemberRepo.InsertAndGetIdAsync(swipeMember);
				return IdentityResult.Success;

			}
			else
			{
				List<ConnectMember> lst = _swipeMemberRepo.GetAll().Where(a => a.MemberCode.ToUpper().Equals(swipeMember.MemberCode.ToUpper()) && a.Id != swipeMember.Id).ToList();
				if (lst.Count > 0)
				{
					string[] strArrays = { L("MemberCodeAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				return IdentityResult.Success;
			}
		}

	}
}
