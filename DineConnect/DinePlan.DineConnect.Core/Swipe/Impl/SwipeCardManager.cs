﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Swipe;

namespace DinePlan.DineConnect.House.Impl
{
	public class SwipeCardManager : DineConnectServiceBase, ISwipeCardManager, ITransientDependency
	{

		private readonly IRepository<SwipeCard> _swipeCardRepo;

		public SwipeCardManager(IRepository<SwipeCard> swipeCard)
		{
			_swipeCardRepo = swipeCard;
		}

		public async Task<IdentityResult> CreateSync(SwipeCard swipeCard)
		{
			//  if the New Addition
			if (swipeCard.Id == 0)
			{
				if (_swipeCardRepo.GetAll().Any(a => a.CardNumber.ToUpper().Equals(swipeCard.CardNumber.ToUpper())))
				{
					string[] strArrays = { L("CardNumberAlreadyExists", swipeCard.CardNumber) };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				await _swipeCardRepo.InsertAndGetIdAsync(swipeCard);
				return IdentityResult.Success;

			}
			else
			{
				List<SwipeCard> lst = _swipeCardRepo.GetAll().Where(a => a.CardNumber.ToUpper().Equals(swipeCard.CardNumber.ToUpper()) && a.Id != swipeCard.Id).ToList();
				if (lst.Count > 0)
				{
					string[] strArrays = { L("CardNumberAlreadyExists", swipeCard.CardNumber) };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				return IdentityResult.Success;
			}
		}

	}
}
