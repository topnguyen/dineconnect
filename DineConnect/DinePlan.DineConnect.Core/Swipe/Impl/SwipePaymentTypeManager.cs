﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Swipe;

namespace DinePlan.DineConnect.House.Impl
{
	public class SwipePaymentTypeManager : DineConnectServiceBase, ISwipePaymentTypeManager, ITransientDependency
	{

		private readonly IRepository<SwipePaymentType> _swipePaymentTypeRepo;

		public SwipePaymentTypeManager(IRepository<SwipePaymentType> swipePaymentType)
		{
			_swipePaymentTypeRepo = swipePaymentType;
		}

		public async Task<IdentityResult> CreateSync(SwipePaymentType swipePaymentType)
		{
			//  if the New Addition
			if (swipePaymentType.Id == 0)
			{
				if (_swipePaymentTypeRepo.GetAll().Any(a => a.AccountCode.ToUpper().Equals(swipePaymentType.AccountCode.ToUpper())))
				{
					string[] strArrays = { L("AccountCodeAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				await _swipePaymentTypeRepo.InsertAndGetIdAsync(swipePaymentType);
				return IdentityResult.Success;

			}
			else
			{
				List<SwipePaymentType> lst = _swipePaymentTypeRepo.GetAll().Where(a => a.AccountCode.ToUpper().Equals(swipePaymentType.AccountCode.ToUpper()) && a.Id != swipePaymentType.Id).ToList();
				if (lst.Count > 0)
				{
					string[] strArrays = { L("AccountCodeAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				return IdentityResult.Success;
			}
		}

	}
}
