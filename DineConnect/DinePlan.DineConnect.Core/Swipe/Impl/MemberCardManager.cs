﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Swipe;
using DinePlan.DineConnect.Engage;

namespace DinePlan.DineConnect.House.Impl
{
	public class MemberCardManager : DineConnectServiceBase, IMemberCardManager, ITransientDependency
	{

		private readonly IRepository<MemberCard> _memberCardRepo;
		private readonly IRepository<ConnectMember> _swipememberRepo;
		private readonly IRepository<SwipeCard> _swipecardRepo;

		public MemberCardManager(IRepository<MemberCard> memberCard,
			IRepository<ConnectMember> swipememberRepo,
			IRepository<SwipeCard> swipecardRepo)
		{
			_memberCardRepo = memberCard;
			_swipememberRepo = swipememberRepo;
			_swipecardRepo = swipecardRepo;
		}

		public async Task<IdentityResult> CreateSync(MemberCard memberCard)
		{
			ConnectMember memberExists = new ConnectMember();
			if (memberCard.MemberRefId.HasValue)
			{
				memberExists = await _swipememberRepo.FirstOrDefaultAsync(t => t.Id == memberCard.MemberRefId);
				if (memberExists == null)
				{
					string[] strArrays = { L("MemberNotFound", memberCard.MemberRefId) };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
			}

			var cardExists = await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == memberCard.CardRefId);
			if (cardExists == null)
			{
				string[] strArrays = { L("SwipeCardNotFound", memberCard.CardRefId) };
				var success = AbpIdentityResult.Failed(strArrays);
				return success;
			}

			var alreadyIssued = await _memberCardRepo.FirstOrDefaultAsync(t => t.CardRefId == memberCard.CardRefId && t.IsActive == true);
			if (alreadyIssued != null)
			{
				if (alreadyIssued.MemberRefId != memberCard.MemberRefId)
				{
					string[] strArrays = { L("CardNumberAlreadyIssuedToMember_F", cardExists.CardNumber, memberExists==null?"" : memberExists.MemberCode + " - " + memberExists.Name) };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				else
				{
					memberCard.Id = alreadyIssued.Id;
				}
			}

			//  if the New Addition
			if (memberCard.Id == 0)
			{
				if (_memberCardRepo.GetAll().Any(a => a.MemberRefId == memberCard.MemberRefId && a.CardRefId == memberCard.CardRefId && a.IsActive==true ))
				{
					string[] strArrays = { L("MemberCardAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				await _memberCardRepo.InsertAndGetIdAsync(memberCard);
				return IdentityResult.Success;
			}
			else
			{
				List<MemberCard> lst = _memberCardRepo.GetAll().Where(a => a.MemberRefId == memberCard.MemberRefId && a.CardRefId == memberCard.CardRefId && a.IsActive == true && a.Id != memberCard.Id).ToList();
				if (lst.Count > 0)
				{
					string[] strArrays = { L("MemberCardAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				return IdentityResult.Success;
			}
		}

		public async Task<IdentityResult> CardCreateSync(SwipeCard swipeCard)
		{
			//  if the New Addition
			if (swipeCard.Id == 0)
			{
				if (_swipecardRepo.GetAll().Any(a => a.CardNumber == swipeCard.CardNumber))
				{
					string[] strArrays = { L("CardAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				await _swipecardRepo.InsertAndGetIdAsync(swipeCard);
				return IdentityResult.Success;
			}
			else
			{
				//var lst = await _swipecardRepo.GetAll().Where(a => a.CardNumber == swipeCard.CardNumber && a.Id != swipeCard.Id).ToList();
				var lst =  _swipecardRepo.GetAll().Where(a => a.CardNumber == swipeCard.CardNumber && a.Id != swipeCard.Id).ToList();
				if (lst.Count > 0)
				{
					string[] strArrays = { L("CardAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				return IdentityResult.Success;
			}
		}
		
	}
}

