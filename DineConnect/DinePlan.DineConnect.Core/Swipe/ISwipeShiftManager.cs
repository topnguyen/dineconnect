﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Swipe;

namespace DinePlan.DineConnect.House.Impl
{
	public interface ISwipeShiftManager
	{
		Task<IdentityResult> CreateSync(SwipeShift swipeShift);
	}
}
