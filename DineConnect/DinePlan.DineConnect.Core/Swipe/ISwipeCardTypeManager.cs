﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Swipe;

namespace DinePlan.DineConnect.House.Impl
{
	public interface ISwipeCardTypeManager
	{
		Task<IdentityResult> CreateSync(SwipeCardType swipeCardType);
	}
}
