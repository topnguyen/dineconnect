﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Swipe;
using DinePlan.DineConnect.Engage;

namespace DinePlan.DineConnect.House.Impl
{
	public interface ISwipeMemberManager
	{
		Task<IdentityResult> CreateSync(ConnectMember swipeMember);
	}
}