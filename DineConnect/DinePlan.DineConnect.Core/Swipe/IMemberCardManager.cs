﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Swipe;

namespace DinePlan.DineConnect.House.Impl
{
	public interface IMemberCardManager
	{
		Task<IdentityResult> CreateSync(MemberCard memberCard);

		Task<IdentityResult> CardCreateSync(SwipeCard memberCard);
	}
}
