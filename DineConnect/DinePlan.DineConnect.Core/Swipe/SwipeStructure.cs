﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Engage;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Swipe
{
    [Table("SwipePaymentTypes")]
    public class SwipePaymentType : FullAuditedEntity
    {
        [StringLength(50)]
        public virtual string Name { get; set; }

        public virtual bool AcceptChange { get; set; }
        public virtual bool Hide { get; set; }

        [StringLength(50)]
        public virtual string AccountCode { get; set; }

        public virtual int SortOrder { get; set; }
        public bool TopupAcceptFlag { get; set; }
        public bool RefundAcceptFlag { get; set; }
    }

    [Table("SwipeCardTypes")]
    public class SwipeCardType : FullAuditedEntity
    {
        [Index("UQ_CARDTYPENAME", IsUnique = true)]
        [MaxLength(30)]
        public virtual string Name { get; set; }
        public virtual bool DepositRequired { get; set; }
        public virtual decimal DepositAmount { get; set; }
        public virtual int? ExpiryDaysFromIssue { get; set; }
        public virtual bool RefundAllowed { get; set; }
        public virtual bool CanIssueWithOutMember { get; set; }
        public virtual bool CanExcludeTaxAndCharges { get; set; }
        public virtual bool IsEmployeeCard { get; set; }
        public virtual bool IsAutoRechargeCard { get; set; }
        public virtual decimal DailyLimit { get; set; }
        public virtual decimal WeeklyLimit { get; set; }
        public virtual decimal MonthlyLimit { get; set; }
        public virtual decimal YearlyLimit { get; set; }
		public virtual string AutoActivateRegExpression { get; set; }
        public virtual decimal MinimumTopUpAmount { get; set; }
    }

    [Table("SwipeCards")]
    public class SwipeCard : FullAuditedEntity
    {
        public virtual int CardTypeRefId { get; set; }

        [ForeignKey("CardTypeRefId")]
        public SwipeCardType SwipeCardTypes { get; set; }

        [Index("UQ_CARDNUMBER", IsUnique = true)]
        [MaxLength(20)]
        public virtual string CardNumber { get; set; }

        public virtual decimal Balance { get; set; }
        public virtual bool ActiveStatus { get; set; }
        public virtual bool DepositRequired { get; set; }
        public virtual decimal DepositAmount { get; set; }
        public virtual int? ExpiryDaysFromIssue { get; set; }
        public virtual bool RefundAllowed { get; set; }
        public virtual bool CanIssueWithOutMember { get; set; }
        public virtual bool CanExcludeTaxAndCharges { get; set; }
        public virtual bool IsEmployeeCard { get; set; }
        public virtual bool IsAutoRechargeCard { get; set; }
        public virtual decimal DailyLimit { get; set; }
        public virtual decimal WeeklyLimit { get; set; }
        public virtual decimal MonthlyLimit { get; set; }
        public virtual decimal YearlyLimit { get; set; }
        public virtual decimal MinimumTopUpAmount { get; set; }

    }

    [Table("MemberCards")]
    public class MemberCard : FullAuditedEntity
    {
        public virtual int? MemberRefId { get; set; }

        [ForeignKey("MemberRefId")]
        public ConnectMember ConnectMember { get; set; }

        public virtual int CardRefId { get; set; }

        [ForeignKey("CardRefId")]
        public SwipeCard SwipeCards { get; set; }

        public virtual bool IsPrimaryCard { get; set; }
        public virtual int PrimaryCardRefId { get; set; }

        [ForeignKey("CardRefId")]
        public SwipeCard PrimaryCards { get; set; }

        public virtual DateTime IssueDate { get; set; }
        public virtual DateTime? ReturnDate { get; set; }
        public virtual DateTime? ExpiryDate { get; set; }
        public virtual decimal DepositAmount { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual int? EmployeeRefId { get; set; }

        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformation { get; set; }
    }

    [Table("SwipeTransactionDetails")]
    public class SwipeTransactionDetail : FullAuditedEntity
    {
        public virtual DateTime TransactionTime { get; set; }
        public virtual int? MemberRefId { get; set; }

        [ForeignKey("MemberRefId")]
        public ConnectMember ConnectMembers { get; set; }

        public virtual int CardRefId { get; set; }

        [ForeignKey("CardRefId")]
        public SwipeCard SwipeCards { get; set; }

        public virtual int PrimaryCardRefId { get; set; }

        [ForeignKey("CardRefId")]
        public SwipeCard PrimaryCards { get; set; }

        public virtual int TransactionType { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal Credit { get; set; }
        public virtual decimal Debit { get; set; }
    }


    [Table("SwipeCardLedgers")]
    public class SwipeCardLedger : FullAuditedEntity
    {
        public virtual DateTime LedgerTime { get; set; }
        public virtual int? MemberRefId { get; set; }

        [ForeignKey("MemberRefId")]
        public ConnectMember ConnectMembers { get; set; }

        public virtual int? EmployeeRefId { get; set; }

        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformation { get; set; }

        public virtual int CardRefId { get; set; }

        [ForeignKey("CardRefId")]
        public SwipeCard SwipeCards { get; set; }

        public virtual int PrimaryCardRefId { get; set; }

        [ForeignKey("CardRefId")]
        public SwipeCard PrimaryCards { get; set; }

        public virtual int TransactionType { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal OpeningBalance { get; set; }
        public virtual decimal Credit { get; set; }
        public virtual decimal Debit { get; set; }
        public virtual decimal ClosingBalance { get; set; }
        public virtual int? LocationId { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }

        [StringLength(30)]
        public virtual string TicketNumber { get; set; }

        public virtual int? ShiftRefId { get; set; }

        [ForeignKey("ShiftRefId")]
        public SwipeShift SwipeShift { get; set; }
    }

    [Table("SwipePaymentTenders")]
    public class SwipePaymentTender : CreationAuditedEntity
    {
        public virtual int SwipeCardLedgerRefId { get; set; }

        [ForeignKey("SwipeCardLedgerRefId")]
        public virtual SwipeCardLedger SwipeCardLedger { get; set; }

        public virtual int PaymentTypeId { get; set; }

        [ForeignKey("PaymentTypeId")]
        public virtual SwipePaymentType SwipePaymentType { get; set; }

        public virtual decimal TenderedAmount { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual string PaymentUserName { get; set; }
    }

    [Table("SwipeShifts")]
    public class SwipeShift : FullAuditedEntity
    {
        public virtual int UserId { get; set; }
        public virtual DateTime ShiftStartTime { get; set; }
        public virtual DateTime? ShiftEndTime { get; set; }
        public virtual decimal TenderOpenAmount { get; set; }        
        public virtual decimal TenderCredit { get; set; }
        public virtual decimal TenderDebit { get; set; }
        public virtual decimal TillShiftCashIn { get; set; }
        public virtual decimal TillShiftCashOut { get; set; }

        public virtual decimal ActualTenderClosing { get; set; }
        
        public virtual decimal TenderCloseAmount { get; set; }
        public virtual decimal UserTenderEntered { get; set; }
        public virtual string ExcessShortageStatus { get; set; } //	Excess, Shortage , No Difference
        public virtual decimal ExcessShortageAmount { get; set; }
    }

    [Table("SwipeShiftTender")]
    public class SwipeShiftTender : FullAuditedEntity
    {
        public virtual int ShiftRefId { get; set; }

        [ForeignKey("ShiftRefId")]
        public SwipeShift SwipeShift { get; set; }

        public virtual int ShiftInOutStatus { get; set; } //	0 For Shift In , 1 For Shift Close Out, 2 For Cash In, 3 For Cash Out

        public virtual int PaymentTypeId { get; set; }
        [ForeignKey("PaymentTypeId")]
        public virtual SwipePaymentType PaymentType { get; set; }

        public virtual decimal ActualAmount { get; set; } //	Only Shown for Shift Close Out

        public virtual decimal TenderedAmount { get; set; }
    }

}
