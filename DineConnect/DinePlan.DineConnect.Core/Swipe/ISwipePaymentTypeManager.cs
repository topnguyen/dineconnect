﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Swipe;

namespace DinePlan.DineConnect.House.Impl
{
	public interface ISwipePaymentTypeManager
	{
		Task<IdentityResult> CreateSync(SwipePaymentType swipePaymentType);
	}
}