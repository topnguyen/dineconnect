﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House
{
    public interface ISalesTaxManager
    {
        Task<IdentityResult> CreateSync(SalesTax tax);
    }
}