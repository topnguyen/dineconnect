﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IRecipeManager
    {
        Task<IdentityResult> CreateSync(Recipe recipe);
    }
}
