﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
	public interface IInventoryCycleTagManager
	{
		Task<IdentityResult> CreateSync(InventoryCycleTag inventoryCycleTag);
	}
}