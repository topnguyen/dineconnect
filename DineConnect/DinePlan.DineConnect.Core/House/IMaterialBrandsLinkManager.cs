﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IMaterialBrandsLinkManager
    {
        Task<IdentityResult> CreateSync(MaterialBrandsLink materialBrandsLink);
    }
}