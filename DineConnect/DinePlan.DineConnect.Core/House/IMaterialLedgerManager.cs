﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IMaterialLedgerManager
    {
        Task<IdentityResult> CreateSync(MaterialLedger materialLedger);
    }
}

