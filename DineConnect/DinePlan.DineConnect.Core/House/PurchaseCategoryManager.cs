﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
	public class PurchaseCategoryManager : DineConnectServiceBase, IPurchaseCategoryManager, ITransientDependency
	{

		private readonly IRepository<PurchaseCategory> _purchaseCategoryRepo;

		public PurchaseCategoryManager(IRepository<PurchaseCategory> purchaseCategory)
		{
			_purchaseCategoryRepo = purchaseCategory;
		}

		public async Task<IdentityResult> CreateSync(PurchaseCategory purchaseCategory)
		{
			//  if the New Addition
			if (purchaseCategory.Id == 0)
			{
				if (_purchaseCategoryRepo.GetAll().Any(a => a.PurchaseCategoryName.Equals(purchaseCategory.PurchaseCategoryName)))
				{
					string[] strArrays = { L("NameAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				await _purchaseCategoryRepo.InsertAndGetIdAsync(purchaseCategory);
				return IdentityResult.Success;

			}
			else
			{
				List<PurchaseCategory> lst = _purchaseCategoryRepo.GetAll().Where(a => a.PurchaseCategoryName.Equals(purchaseCategory.PurchaseCategoryName) && a.Id != purchaseCategory.Id).ToList();
				if (lst.Count > 0)
				{
					string[] strArrays = { L("NameAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				return IdentityResult.Success;
			}
		}

	}
}

