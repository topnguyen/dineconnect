﻿
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IUnitConversionManager
    {
        Task<IdentityResult> CreateSync(UnitConversion unitConversion);
    }


}
