﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House
{
    public interface ITaxManager
    {
        Task<IdentityResult> CreateSync(Tax tax);
    }
}