﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IInterTransferManager
    {
        Task<IdentityResult> CreateSync(InterTransfer interTransfer);
    }
}

