﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IRecipeIngredientManager
    {
        Task<IdentityResult> CreateSync(RecipeIngredient recipeIngredient);
    }
}