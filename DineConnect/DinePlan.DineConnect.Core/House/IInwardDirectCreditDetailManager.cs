﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IInwardDirectCreditDetailManager
    {
        Task<IdentityResult> CreateSync(InwardDirectCreditDetail inwardDirectCreditDetail);
    }
}
