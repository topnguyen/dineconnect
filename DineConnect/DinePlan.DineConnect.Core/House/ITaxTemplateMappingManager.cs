﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface ITaxTemplateMappingManager
    {
        Task<IdentityResult> CreateSync(TaxTemplateMapping taxTemplateMapping);
    }
}
