﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IManualReasonManager
    {
        Task<IdentityResult> CreateSync(ManualReason manualReasons);
    }
}
