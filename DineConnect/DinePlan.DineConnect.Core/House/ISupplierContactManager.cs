﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface ISupplierContactManager
    {
        Task<IdentityResult> CreateSync(SupplierContact supplierContact);

    }
}