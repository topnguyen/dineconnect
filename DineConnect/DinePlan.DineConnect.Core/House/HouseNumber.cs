﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace DinePlan.DineConnect.House
{
    [Table("HouseNumbers")]
    public class HouseNumber : FullAuditedEntity, IMustHaveTenant
    {
        [MaxLength(50)] public virtual string Name { get; set; }
        public bool Active  { get; set; }
        public bool LocationPrefix  { get; set; }
        public string Prefix  { get; set; }
        public int Length { get; set; }
        public string Suffix { get; set; }
        public int Begining { get; set; }
        public virtual int TenantId { get; set; }
    }
}
