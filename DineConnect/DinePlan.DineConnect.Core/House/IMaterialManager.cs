﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IMaterialManager
    {
        Task<IdentityResult> CreateSync(Material material);
    }
}