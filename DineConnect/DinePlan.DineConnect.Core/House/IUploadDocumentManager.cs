﻿using System.Threading.Tasks;
using DinePlan.DineConnect.House.Document;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House
{
    public interface IUploadDocumentManager
    {
        Task<IdentityResult> CreateOrUpdateSync(UploadDocument uploadDocument);
    }
}