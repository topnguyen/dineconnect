﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IRecipeGroupManager
    {
        Task<IdentityResult> CreateSync(RecipeGroup recipeGroup);
    }
}
