﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface ILocationContactManager
    {
        Task<IdentityResult> CreateSync(LocationContact locationContact);
    }
}