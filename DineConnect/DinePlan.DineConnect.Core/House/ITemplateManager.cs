﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface ITemplateManager
    {
        Task<IdentityResult> CreateSync(Template template);

        Task<IdentityResult> CreateDayCloseTemplateSync(DayCloseTemplate dayclosetemplate);

    }
}

