﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface ISupplierMaterialManager
    {
        Task<IdentityResult> CreateSync(SupplierMaterial supplierMaterial);
    }
}
