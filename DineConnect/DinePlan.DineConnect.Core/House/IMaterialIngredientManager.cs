﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IMaterialIngredientManager
    {
        Task<IdentityResult> CreateSync(MaterialIngredient materialIngredient);
    }
}
