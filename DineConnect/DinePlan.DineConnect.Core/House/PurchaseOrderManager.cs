﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class PurchaseOrderManager : DineConnectServiceBase, IPurchaseOrderManager, ITransientDependency
    {

        private readonly IRepository<PurchaseOrder> _purchaseOrderRepo;

        public PurchaseOrderManager(IRepository<PurchaseOrder> purchaseOrder)
        {
            _purchaseOrderRepo = purchaseOrder;
        }

        public async Task<IdentityResult> CreateSync(PurchaseOrder purchaseOrder)
        {
            //  if the New Addition
            if (purchaseOrder.Id == 0)
            {
                if (_purchaseOrderRepo.GetAll().Any(a => a.Id.Equals(purchaseOrder.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _purchaseOrderRepo.InsertAndGetIdAsync(purchaseOrder);
                return IdentityResult.Success;

            }
            else
            {
                List<PurchaseOrder> lst = _purchaseOrderRepo.GetAll().Where(a => a.Id.Equals(purchaseOrder.Id) && a.Id != purchaseOrder.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
