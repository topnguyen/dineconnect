﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class IssueManager : DineConnectServiceBase, IIssueManager, ITransientDependency
    {

        private readonly IRepository<Issue> _issueRepo;

        public IssueManager(IRepository<Issue> issue)
        {
            _issueRepo = issue;
        }

        public async Task<IdentityResult> CreateSync(Issue issue)
        {
            //  if the New Addition
            if (issue.Id == 0)
            {
                if (_issueRepo.GetAll().Any(a => a.Id.Equals(issue.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _issueRepo.InsertAndGetIdAsync(issue);
                return IdentityResult.Success;

            }
            else
            {
                List<Issue> lst = _issueRepo.GetAll().Where(a => a.Id.Equals(issue.Id) && a.Id != issue.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}