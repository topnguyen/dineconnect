﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IMaterialGroupManager
    {
        Task<IdentityResult> CreateSync(MaterialGroup materialGroup);
    }
}
