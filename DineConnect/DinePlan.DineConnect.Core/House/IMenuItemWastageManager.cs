﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IMenuItemWastageManager
    {
        Task<IdentityResult> CreateSync(MenuItemWastage menuItemWastage);
    }
}