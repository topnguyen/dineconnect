﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IMaterialLocationWiseStockManager
    {
        Task<IdentityResult> CreateSync(MaterialLocationWiseStock materialLocationWiseStock);
    }
}