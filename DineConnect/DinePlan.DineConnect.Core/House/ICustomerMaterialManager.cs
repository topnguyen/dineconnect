﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface ICustomerMaterialManager
    {
        Task<IdentityResult> CreateSync(CustomerMaterial customerMaterial);
    }
}