﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IAdjustmentManager
    {
        Task<IdentityResult> CreateSync(Adjustment adjustment);
    }
}