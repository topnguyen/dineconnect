﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IUserDefaultOrganizationManager
    {
        Task<IdentityResult> CreateSync(UserDefaultOrganization userDefaultOrganization);
    }
}
