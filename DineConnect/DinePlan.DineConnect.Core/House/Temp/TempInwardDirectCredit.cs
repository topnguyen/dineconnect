﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.House.Temp
{
    [Table("TempInwardDirectCreditDetails")]
    public class TempInwardDirectCreditDetail : FullAuditedEntity, IMustHaveTenant
    {
        public long DcRefId { get; set; }
        public int MaterialRefId { get; set; }
        public float DcReceivedQty { get; set; }
        public bool MaterialReceivedStatus { get; set; }
        public int TenantId { get; set; }
    }
}
