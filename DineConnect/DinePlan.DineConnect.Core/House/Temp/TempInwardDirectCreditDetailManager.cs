﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.House.Temp;

namespace DinePlan.DineConnect.House.Impl
{
    public class TempInwardDirectCreditDetailManager : DineConnectServiceBase, ITempInwardDirectCreditDetailManager, ITransientDependency
    {

        private readonly IRepository<TempInwardDirectCreditDetail> _tempInwardDirectCreditDetailRepo;

        public TempInwardDirectCreditDetailManager(IRepository<TempInwardDirectCreditDetail> tempInwardDirectCreditDetail)
        {
            _tempInwardDirectCreditDetailRepo = tempInwardDirectCreditDetail;
        }

        public async Task<IdentityResult> CreateSync(TempInwardDirectCreditDetail tempInwardDirectCreditDetail)
        {
            //  if the New Addition
            if (tempInwardDirectCreditDetail.Id == 0)
            {
                if (_tempInwardDirectCreditDetailRepo.GetAll().Any(a => a.Id.Equals(tempInwardDirectCreditDetail.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _tempInwardDirectCreditDetailRepo.InsertAndGetIdAsync(tempInwardDirectCreditDetail);
                return IdentityResult.Success;

            }
            else
            {
                List<TempInwardDirectCreditDetail> lst = _tempInwardDirectCreditDetailRepo.GetAll().Where(a => a.Id.Equals(tempInwardDirectCreditDetail.Id) && a.Id != tempInwardDirectCreditDetail.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
