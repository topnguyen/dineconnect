﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.House.Temp;

namespace DinePlan.DineConnect.House.Impl
{
    public interface ITempInwardDirectCreditDetailManager
    {
        Task<IdentityResult> CreateSync(TempInwardDirectCreditDetail tempInwardDirectCreditDetail);
    }
}
