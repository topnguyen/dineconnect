﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IYieldManager 
    {
        Task<IdentityResult> CreateSync(Yield yield);
    }
}
