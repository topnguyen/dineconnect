﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface ISalesInvoiceManager
    {
        Task<IdentityResult> CreateSync(SalesInvoice invoice);
    }
}
