﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IMaterialMenuMappingManager
    {
        Task<IdentityResult> CreateSync(MaterialMenuMapping materialMenuMapping);
    }
}
