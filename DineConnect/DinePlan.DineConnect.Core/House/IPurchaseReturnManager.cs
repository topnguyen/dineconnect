﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IPurchaseReturnManager
    {
        Task<IdentityResult> CreateSync(PurchaseReturn purchaseReturn);
    }
}
