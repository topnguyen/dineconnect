﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface ISupplierDocumentManager
    {
        Task<IdentityResult> CreateSync(SupplierDocument supplierDocument);
    }
}
