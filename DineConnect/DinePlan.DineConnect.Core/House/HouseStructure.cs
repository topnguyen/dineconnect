﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.MultiTenancy;

namespace DinePlan.DineConnect.House
{
    [Table("Brands")]
    public class Brand : FullAuditedEntity, IMustHaveTenant
    {
        public Brand()
        {
        }

        public Brand(int argTenantId, string argBrandName)
        {
            TenantId = argTenantId;
            Name = argBrandName;
        }

        [Required] [MaxLength(150)] public virtual string Name { get; set; }

        public virtual int TenantId { get; set; }
    }

    [Table("Units")]
    public class Unit : FullAuditedEntity, IMustHaveTenant
    {
        public Unit()
        {
        }

        public Unit(string name)
        {
            Name = name;
        }

        [Required] [MaxLength(150)] public virtual string Name { get; set; }

        public int? SyncId { get; set; }

        public DateTime? SyncLastModification { get; set; }

        public virtual ICollection<UnitConversion> UnitConversions { get; set; }

        // For Seed Purpose

        public int TenantId { get; set; }
    }

    [Table("UnitConversions")]
    public class UnitConversion : FullAuditedEntity
    {
        public UnitConversion()
        {
        }

        //For Seed Purpose
        public UnitConversion(int baseUnitId, int refUnitID, decimal argConversion)
        {
            BaseUnitId = baseUnitId;
            RefUnitId = refUnitID;
            Conversion = argConversion;
            DecimalPlaceRounding = 14;
        }
        public int? MaterialRefId { get; set; }
        [ForeignKey("MaterialRefId")] public virtual Material Materials { get; set; }

        [Required] public int BaseUnitId { get; set; }

        [Required] public int RefUnitId { get; set; }

        [Required] [DecimalPrecision(18, 8)] public decimal Conversion { get; set; }

        [Required] public byte DecimalPlaceRounding { get; set; }

        public int? SyncId { get; set; }
        public DateTime? SyncLastModification { get; set; }

        //[DecimalPrecision(18, 6)]
        //public virtual decimal Price { get; set; }

        //[Required]
        //public float FlatConversion { get; set; }

        [ForeignKey("BaseUnitId")] public Unit Unit { get; set; }
        public bool CreatedBasedOnRecursive { get; set; }
        public int? ReferenceRecursiveId { get; set; }
    }

    [Table("UnitConversionVsSuppliers")]
    public class UnitConversionVsSupplier : FullAuditedEntity
    {
        [Required] public int UnitConversionRefId { get; set; }

        [ForeignKey("UnitConversionRefId")] public UnitConversion UnitConversions { get; set; }
        [Required] public int SupplierRefId { get; set; }

        [ForeignKey("SupplierRefId")] public Supplier Suppliers { get; set; }

    }

    [Table("LocationContacts")]
    public class LocationContact : FullAuditedEntity
    {
        [Required] public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Locations { get; set; }

        [Required] [MaxLength(150)] public string ContactPersonName { get; set; }

        [MaxLength(150)] public string Designation { get; set; }

        [MaxLength(150)] public string PhoneNumber { get; set; }

        [MaxLength(150)] public string EmpRefCode { get; set; }

        [MaxLength(150)] public string Email { get; set; }

        public int Priority { get; set; }
    }

    //--------------------------------------------------------------------------------------------------------------------


    //[Table("MaterialGroups")]
    //public class MaterialGroup : FullAuditedEntity, IMustHaveTenant
    //{
    //    public MaterialGroup()
    //    {
    //    }

    //    //  For Seed Purpose
    //    public MaterialGroup(int argTenantId, string argMaterialGroupName)
    //    {
    //        TenantId = argTenantId;
    //        MaterialGroupName = argMaterialGroupName;
    //    }

    //    [MaxLength(150)] [Required] public string MaterialGroupName { get; set; }

    //    public int? SyncId { get; set; }
    //    public DateTime? SyncLastModification { get; set; }

    //    public virtual ICollection<MaterialGroupCategory> MaterialGroupCategories { get; set; }

    //    [Required] public int TenantId { get; set; }
    //}

    [Table("MaterialGroups")]
    public class MaterialGroup : FullAuditedEntity, IMustHaveTenant
    {
        public MaterialGroup()
        {
        }

        //  For Seed Purpose
        public MaterialGroup(int argTenantId, string argMaterialGroupName, string argCode)
        {
            TenantId = argTenantId;
            MaterialGroupName = argMaterialGroupName;
            Code = argCode;
        }
        [MaxLength(50)]
        public virtual string Code { get; set; }

        [ForeignKey("ParentId")]
        public virtual MaterialGroup Parent { get; set; }
        public virtual int? ParentId { get; set; }


        [MaxLength(150)] [Required] public string MaterialGroupName { get; set; }

        public int? SyncId { get; set; }
        public DateTime? SyncLastModification { get; set; }

        public virtual ICollection<MaterialGroupCategory> MaterialGroupCategories { get; set; }

        [Required] public int TenantId { get; set; }
    }


    [Table("MaterialGroupCategories")]
    public class MaterialGroupCategory : FullAuditedEntity, IMustHaveTenant
    {
        public MaterialGroupCategory()
        {
        }

        // For Seed Purpose
        public MaterialGroupCategory(int argMaterialGroupRefId, string argMaterialGroupCategoryName)
        {
            MaterialGroupRefId = argMaterialGroupRefId;
            MaterialGroupCategoryName = argMaterialGroupCategoryName;
        }

        [Required] [MaxLength(150)] public string MaterialGroupCategoryName { get; set; }

        [Required] public int MaterialGroupRefId { get; set; }

        [ForeignKey("MaterialGroupRefId")] public MaterialGroup MaterialGroups { get; set; }

        public int? SyncId { get; set; }
        public DateTime? SyncLastModification { get; set; }

        public virtual ICollection<Material> Material { get; set; }

        [MaxLength(200)] public string AddOn { get; set; }

        public int TenantId { get; set; }
    }


    [Table("Materials")]
    public class Material : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int MaterialTypeId { get; set; }

        [Required] [MaxLength(70)] public string MaterialName { get; set; }

        [Required] [MaxLength(70)] public string MaterialPetName { get; set; }

        [Required] public int MaterialGroupCategoryRefId { get; set; }

        [ForeignKey("MaterialGroupCategoryRefId")]
        public MaterialGroupCategory MaterialGroupCategorys { get; set; }

        [Required] public int DefaultUnitId { get; set; }

        [ForeignKey("DefaultUnitId")] public Unit Units { get; set; }

        [Required] public int IssueUnitId { get; set; }

        [ForeignKey("IssueUnitId")] public Unit IssueUnits { get; set; }

        public int ReceivingUnitId { get; set; }

        [ForeignKey("ReceivingUnitId")] public Unit ReceivingUnits { get; set; }

        public int TransferUnitId { get; set; }

        [ForeignKey("TransferUnitId")] public Unit TransferUnits { get; set; }

        public int StockAdjustmentUnitId { get; set; }

        [ForeignKey("StockAdjustmentUnitId")] public Unit StockAdjustmentUnits { get; set; }

        [Required] public decimal UserSerialNumber { get; set; }

        [Required] public bool IsFractional { get; set; }

        [Required] public bool IsBranded { get; set; }

        [Required] public bool IsQuoteNeededForPurchase { get; set; }

        [Required] public int GeneralLifeDays { get; set; }

        [Required] public bool OwnPreparation { get; set; }

        [Required] public bool IsNeedtoKeptinFreezer { get; set; }

        [Required] public bool MRPPriceExists { get; set; }

        [Required] public int PosReferencecodeifany { get; set; }

        [Required] public bool IsMfgDateExists { get; set; }

        [Required] public bool WipeOutStockOnClosingDay { get; set; }

        [Required] public bool IsHighValueItem { get; set; }

        public virtual ICollection<MaterialLocationWiseStock> MaterialStock { get; set; }
        public virtual ICollection<MaterialLedger> MaterialLedger { get; set; }


        [MaxLength(150)] public string Barcode { get; set; }

        public virtual ICollection<MaterialBarCode> Barcodes { get; set; }

        [MaxLength(100)] public virtual string Hsncode { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual int? SyncId { get; set; }
        public virtual DateTime? SyncLastModification { get; set; }

        public virtual decimal? YieldPercentage { get; set; }
        public int AveragePriceTagRefId { get; set; }
        public int NoOfMonthAvgTaken { get; set; }

        [MaxLength(200)] public string AddOn { get; set; }

        public bool ConvertAsZeroStockWhenClosingStockNegative { get; set; }

        public int TenantId { get; set; }
    }


    [Table("MaterialBarCodes")]
    public class MaterialBarCode : CreationAuditedEntity, IMustHaveTenant
    {
        [MaxLength(150)] public virtual string Barcode { get; set; }

        public virtual int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public virtual Material Material { get; set; }

        public int TenantId { get; set; }
    }


    [Table("CustomerTagDefinitions")]
    public class CustomerTagDefinition : FullAuditedEntity, IMustHaveTenant
    {
        public CustomerTagDefinition()
        {
        }

        public CustomerTagDefinition(string tagCode, string tagName)
        {
            TagCode = tagCode;
            TagName = tagName;
        }

        [Required] [MaxLength(130)] public string TagCode { get; set; } //  Short Description

        [Required] [MaxLength(130)] public string TagName { get; set; } //  Tag Name

        public int TenantId { get; set; }
    }

    [Table("CustomerTagWiseMaterialPrices")]
    public class CustomerTagMaterialPrice : FullAuditedEntity
    {
        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Material { get; set; }
        
        [Required] [MaxLength(150)] public string TagCode { get; set; } //  Short Description

        [Required] public decimal MaterialPrice { get; set; }

        public bool AvgRateBasedPrice { get; set; }
        public decimal MarginPercentage { get; set; }
        public decimal MinimumPrice { get; set; }
    }

    [Table("MaterialUnitLinks")]
    public class MaterialUnitsLink : FullAuditedEntity
    {
        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Material { get; set; }

        [Required] public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Unit { get; set; }
    }

    [Table("MaterialLocationWiseStocks")]
    public class MaterialLocationWiseStock : FullAuditedEntity
    {
        [Required]
        [Index("MPK_LocationRefId_MaterialRefId", IsUnique = true, Order = 0)]
        public int LocationRefId { get; set; }
        [ForeignKey("LocationRefId")] public Location Location { get; set; }

        
        [Required]
        [Index("MPK_LocationRefId_MaterialRefId", IsUnique = true, Order = 1)] 
        public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Material { get; set; }

        [Required] public decimal CurrentInHand { get; set; }

        [Required] public decimal MinimumStock { get; set; }

        [Required] public decimal MaximumStock { get; set; }

        [Required] public decimal ReOrderLevel { get; set; }

        [Required] public bool IsOrderPlaced { get; set; }

        [Required] public bool IsActiveInLocation { get; set; }

        public bool ConvertAsZeroStockWhenClosingStockNegative { get; set; }
    }

    [Table("MaterialIngredients")]
    public class MaterialIngredient : FullAuditedEntity
    {
        [Required] public int RecipeRefId { get; set; }

        [ForeignKey("MaterialRefId")] public virtual Material RecipeMaterials { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public virtual Material Materials { get; set; }

        [Required] public decimal MaterialUsedQty { get; set; }

        [Required] public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }

        [Required] public bool VariationflagForProduction { get; set; }

        [Required] public decimal UserSerialNumber { get; set; }
    }

    [Table("MaterialLedgers")]
    public class MaterialLedger : FullAuditedEntity
    {
        [Required] public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Location { get; set; }

        [Required] public DateTime? LedgerDate { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Material { get; set; }

        [Required] public decimal OpenBalance { get; set; }

        [Required] public decimal Received { get; set; }

        [Required] public decimal Issued { get; set; }

        [Required] public decimal Sales { get; set; }

        [Required] public decimal TransferIn { get; set; }

        [Required] public decimal TransferOut { get; set; }

        [Required] public decimal Damaged { get; set; }

        [Required] public decimal SupplierReturn { get; set; }

        [Required] public decimal ExcessReceived { get; set; }

        [Required] public decimal Shortage { get; set; }

        [Required] public decimal Return { get; set; }

        [Required] public decimal ClBalance { get; set; }
    }

    [Table("MaterialBrandLinks")]
    public class MaterialBrandsLink : FullAuditedEntity
    {
        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Material { get; set; }

        [Required] public int BrandRefId { get; set; }

        [ForeignKey("BrandRefId")] public Brand Brand { get; set; }
    }


    [Table("MaterialRecipeTypes")]
    public class MaterialRecipeTypes : FullAuditedEntity
    {
        [Required] public int MaterailRefId { get; set; }

        [ForeignKey("MaterailRefId")] public Material Material { get; set; }

        [Required] public decimal PrdBatchQty { get; set; }

        [Required] public int LifeTimeInMinutes { get; set; }

        [Required] public bool AlarmFlag { get; set; }

        [Required] public int AlarmTimeInMinutes { get; set; }

        [Required] public decimal FixedCost { get; set; }
    }

    [Table("MaterialMenuMappings")]
    public class MaterialMenuMapping : FullAuditedEntity
    {
        [Required] public int PosMenuPortionRefId { get; set; }

        [ForeignKey("PosMenuPortionRefId")] public MenuItemPortion MenuItemPortions { get; set; }

        [Required] public int MenuQuantitySold { get; set; }

        [Required] public decimal UserSerialNumber { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Materials { get; set; }

        [Required] public decimal PortionQty { get; set; }

        [Required] public int PortionUnitId { get; set; }

        [ForeignKey("PortionUnitId")] public Unit Units { get; set; }

        [Required] public decimal WastageExpected { get; set; }

        [Required] public bool AutoSalesDeduction { get; set; }

        public int? LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Locations { get; set; }
    }

    public class MenuMappingDepartment : FullAuditedEntity, IMustHaveTenant
    {
        public int MenuMappingRefId { get; set; }

        [ForeignKey("MenuMappingRefId")] public MaterialMenuMapping MaterialMenuMappings { get; set; }

        public int DepartmentRefId { get; set; }

        [ForeignKey("DepartmentRefId")] public Department Departments { get; set; }

        public int TenantId { get; set; }
    }


    //--------------------------------------------------------------------------------------------------------------------

    [Table("Taxes")]
    public class Tax : FullAuditedEntity, IMustHaveTenant
    {
        [Required] [MaxLength(150)] public string TaxName { get; set; }

        [Required] public decimal Percentage { get; set; }

        [Required] [MaxLength(150)] public string TaxCalculationMethod { get; set; }

        [Required] public int SortOrder { get; set; }

        [Required] public int Rounding { get; set; }

        public int? SyncId { get; set; }
        public DateTime? SyncLastModification { get; set; }

        [MaxLength(200)] public string AddOn { get; set; }

        public int TenantId { get; set; }
    }

    [Table("TaxTemplateMappings")]
    public class TaxTemplateMapping : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int TaxRefId { get; set; }

        [ForeignKey("TaxRefId")] public Tax Taxes { get; set; }

        public int? CompanyRefId { get; set; }

        [ForeignKey("CompanyRefId")] public Company companies { get; set; }

        public int? LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Locations { get; set; }


        public int? MaterialGroupRefId { get; set; }

        [ForeignKey("MaterialGroupRefId")] public MaterialGroup MaterialGroups { get; set; }

        public int? MaterialGroupCategoryRefId { get; set; }

        [ForeignKey("MaterialGroupCategoryRefId")]
        public MaterialGroupCategory MaterialGroupCategorys { get; set; }

        public int? MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public virtual Material Materials { get; set; }

        public int TenantId { get; set; }
    }


    [Table("SalesTaxes")]
    public class SalesTax : FullAuditedEntity, IMustHaveTenant
    {
        [Required] [MaxLength(150)] public string TaxName { get; set; }

        [Required] public decimal Percentage { get; set; }

        [Required] [MaxLength(150)] public string TaxCalculationMethod { get; set; }

        [Required] public int SortOrder { get; set; }

        [Required] public int Rounding { get; set; }

        public int TenantId { get; set; }
    }

    [Table("SalesTaxTemplateMappings")]
    public class SalesTaxTemplateMapping : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int SalesTaxRefId { get; set; }

        [ForeignKey("SalesTaxRefId")] public SalesTax SalesTaxes { get; set; }

        public int? CompanyRefId { get; set; }

        [ForeignKey("CompanyRefId")] public Company companies { get; set; }

        public int? LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Locations { get; set; }

        public int? MaterialGroupRefId { get; set; }

        [ForeignKey("MaterialGroupRefId")] public MaterialGroup MaterialGroups { get; set; }

        public int? MaterialGroupCategoryRefId { get; set; }

        [ForeignKey("MaterialGroupCategoryRefId")]
        public MaterialGroupCategory MaterialGroupCategorys { get; set; }

        public int? MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public virtual Material Materials { get; set; }

        public int TenantId { get; set; }
    }


    [Table("ManualReasons")]
    public class ManualReason : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int ManualReasonCategoryRefId { get; set; }

        [MaxLength(150)] public virtual string ManualReasonName { get; set; }

        public virtual int TenantId { get; set; }
    }

    public enum ManualReasonCategory
    {
        General = 99,

        //PurchaseOrder = 1,
        //Receipt = 2,
        //Invoice = 3,
        PurchaseReturn = 4,
        Adjustment = 5,
        MenuWastage = 6
    }

    public enum UnitTypeEnum
    {
        Default = 0,
        Transaction = 1,
        Transfer = 2,
        Stock_Adjustment=3
    }

    //--------------------------------------------------------------------------------------------------------------------

    [Table("RecipeGroups")]
    public class RecipeGroup : FullAuditedEntity, IMustHaveTenant
    {
        [Required] [MaxLength(50)] public string RecipeGroupName { get; set; }

        [Required] [MaxLength(50)] public string RecipeGroupShortName { get; set; }

        public virtual ICollection<Recipe> Recipes { get; set; }

        public int TenantId { get; set; }
    }


    [Table("Recipes")]
    public class Recipe : FullAuditedEntity
    {
        [Required] [MaxLength(150)] public string RecipeName { get; set; }

        [Required] public int RecipeGroupRefId { get; set; }

        [ForeignKey("RecipeGroupRefId")] public RecipeGroup RecipeGroup { get; set; }


        [Required] [MaxLength(150)] public string RecipeShortName { get; set; }

        [Required] public decimal PrdBatchQty { get; set; }

        [Required] public int PrdUnitId { get; set; }

        [ForeignKey("PrdUnitId")] public Unit UnitPrd { get; set; }

        [Required] public int SelUnitId { get; set; }

        [ForeignKey("SelUnitId")] public Unit UnitSel { get; set; }

        [Required] public int LifeTimeInMinutes { get; set; }

        [Required] public bool AlarmFlag { get; set; }

        [Required] public int AlarmTimeInMinutes { get; set; }

        [Required] public decimal FixedCost { get; set; }
    }

    [Table("RecipeIngredients")]
    public class RecipeIngredient : FullAuditedEntity
    {
        [Required] public int RecipeRefId { get; set; }

        [ForeignKey("RecipeRefId")] public Recipe Recipes { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public virtual Material Materials { get; set; }

        [Required] public decimal MaterialUsedQty { get; set; }

        [Required] public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }

        [Required] public bool VariationflagForProduction { get; set; }

        [Required] public decimal UserSerialNumber { get; set; }
    }


    [Table("ProductRecipeLinks")]
    public class ProductRecipesLink : FullAuditedEntity
    {
        [Required] public int PosRefId { get; set; }

        // @@Pending Needs to link with DineConnect for getting Data
        //[ForeignKey("PosRefId")]
        //public PosProduct PosProducts { get; set; }


        [Required] public decimal UserSerialNumber { get; set; }

        [Required] public int RecipeRefId { get; set; }

        [ForeignKey("RecipeRefId")] public Recipe Recipes { get; set; }

        [Required] public decimal PortionQty { get; set; }

        [Required] public int PortionUnitId { get; set; }

        [ForeignKey("PortionUnitId")] public Unit Units { get; set; }

        [Required] [MaxLength(150)] public string RecipeType { get; set; }

        [Required] public decimal WastageExpected { get; set; }
    }

    //--------------------------------------------------------------------------------------------------------------------

    [Table("Suppliers")]
    public class Supplier : FullAuditedEntity, IMustHaveTenant
    {
        [Required] [MaxLength(80)] public virtual string SupplierName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public virtual string City { get; set; }

        public virtual string State { get; set; }

        public virtual string StateCode { get; set; }

        public virtual string Country { get; set; }

        public string ZipCode { get; set; }

        public string PhoneNumber1 { get; set; }

        public int DefaultCreditDays { get; set; }

        public string OrderPlacedThrough { get; set; }

        public string Email { get; set; }

        public string FaxNumber { get; set; }

        public string Website { get; set; }

        public virtual ICollection<SupplierMaterial> SupplierMaterials { get; set; }

        public virtual string SupplierCode { get; set; }

        public virtual bool TaxApplicable { get; set; }

        public virtual string TaxRegistrationNumber { get; set; }

        [MaxLength(200)] public virtual string AddOn { get; set; }

        public virtual bool LoadOnlyLinkedMaterials { get; set; }
        public int DefaultPayModeEnumRefId { get; set; }
        public int? SyncId { get; set; }
        public DateTime? SyncLastModification { get; set; }

        public virtual int TenantId { get; set; }
    }

    public enum InvoicePayModeEnum
    {
        Cash = 1,
        Credit = 2
    }

    [Table("SupplierGSTInformations")]
    public class SupplierGSTInformation : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int SupplierRefId { get; set; }

        [ForeignKey("SupplierRefId")] public Supplier Suppliers { get; set; }

        [MaxLength(150)] public string SupplierLegalName { get; set; }

        public int? GstStatus { get; set; }
        public int? ConstitutionStatus { get; set; }
        [MaxLength(150)] public string Gstin { get; set; }
        public Guid? GstCertificate { get; set; }
        public bool ReverseChargeApplicable { get; set; }
        public bool PayGstApplicable { get; set; }
        public virtual int TenantId { get; set; }
    }


    [Table("SupplierMaterials")]
    public class SupplierMaterial : FullAuditedEntity
    {
        [Required] public int SupplierRefId { get; set; }

        [ForeignKey("SupplierRefId")] public Supplier Suppliers { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Materials { get; set; }

        public decimal MaterialPrice { get; set; }
        public decimal MinimumOrderQuantity { get; set; }
        public decimal MaximumOrderQuantity { get; set; }
        public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }

        [MaxLength(150)] public string LastQuoteRefNo { get; set; }

        public DateTime LastQuoteDate { get; set; }
        public int? SyncId { get; set; }
        public DateTime? SyncLastModification { get; set; }
        public virtual decimal? YieldPercentage { get; set; }

        public int? LocationRefId { get; set; }
        [ForeignKey("LocationRefId")] 
        public Location Location { get; set; }
        public string SupplierMaterialAliasName { get; set; }
    }

    [Table("SupplierContacts")]
    public class SupplierContact : FullAuditedEntity
    {
        [Required] public int SupplierRefId { get; set; }

        [ForeignKey("SupplierRefId")] public Supplier Suppliers { get; set; }

        [Required] [MaxLength(150)] public string ContactPersonName { get; set; }

        [MaxLength(150)] public string Designation { get; set; }

        [MaxLength(150)] public string PhoneNumber { get; set; }

        [MaxLength(150)] public string Email { get; set; }
    }


    [Table("InwardDirectCredits")]
    public class InwardDirectCredit : FullAuditedEntity, IMustHaveTenant
    {
        public DateTime? AccountDate { get; set; }

        [Required] public DateTime DcDate { get; set; }

        [Required] public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Locations { get; set; }

        [Required] public int SupplierRefId { get; set; }

        [ForeignKey("SupplierRefId")] public Supplier Suppliers { get; set; }

        [Required] [MaxLength(150)] public string DcNumberGivenBySupplier { get; set; }

        [Required] public bool IsDcCompleted { get; set; }

        [MaxLength(150)] public string PoReferenceCode { get; set; }

        public int? PoRefId { get; set; }

        [ForeignKey("PoRefId")] public PurchaseOrder PurchaseOrder { get; set; }

        public int? AdjustmentRefId { get; set; }

        [ForeignKey("AdjustmentRefId")] public Adjustment Adjustment { get; set; }

        [MaxLength(150)] public string PurchaseOrderReferenceFromOtherErp { get; set; }

        [MaxLength(150)] public string InvoiceNumberReferenceFromOtherErp { get; set; }

        public int TenantId { get; set; }
    }

    [Table("InwardDirectCreditDetails")]
    public class InwardDirectCreditDetail : FullAuditedEntity
    {
        [Required] public int DcRefId { get; set; }

        [ForeignKey("DcRefId")] public InwardDirectCredit InwardDirectCredit { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Materials { get; set; }

        [Required] public decimal DcReceivedQty { get; set; }

        public decimal DcConvertedQty { get; set; }

        [Required] public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }

        [Required] [MaxLength(150)] public string MaterialReceivedStatus { get; set; }

        public decimal? YieldPercentage { get; set; }

        [MaxLength(150)] public string YieldMode { get; set; }

        public decimal? YieldExcessShortageQty { get; set; }
    }

    //--------------------------------------------------------------------------------------------------------------------

    [TrackChanges]
    [Table("InterTransfers")]
    public class InterTransfer : FullAuditedEntity, IMustHaveTenant
    {
        [ForeignKey("LocationRefId")] public Location LocationReference;

        [ForeignKey("LocationRefId")] public Location Locations;

        [Required] public int LocationRefId { get; set; }

        [Required] public int RequestLocationRefId { get; set; }

        [Required] public DateTime RequestDate { get; set; }

        [Required] public DateTime DeliveryDateRequested { get; set; }
        public DateTime? AccountDate { get; set; }
        public DateTime? InterTransferReceivedAccountDate { get; set; }

        [Required]
        [StringLength(1)] //  P - Pending  (Default ) , C - Completed, X - Cancel
        [MaxLength(1)]
        public string CurrentStatus { get; set; }

        [SkipTracking]
        [MaxLength(150)] public string DocReferenceNumber { get; set; }

        public DateTime? CompletedTime { get; set; }
        public DateTime? ReceivedTime { get; set; }
        public int? ApprovedPersonId { get; set; }
        public int? ReceivedPersonId { get; set; }
        public decimal? TransferValue { get; set; }
        public decimal? ReceivedValue { get; set; }
        public int? AdjustmentRefId { get; set; }

        [ForeignKey("AdjustmentRefId")] public Adjustment Adjustment { get; set; }

        public int? InterTransferRefId { get; set; }

        [ForeignKey("InterTransferRefId")]
        public InterTransfer InterTransferRef { get; set; }

        [SkipTracking]
        [MaxLength(150)] public string VehicleNumber { get; set; }

        [SkipTracking]
        [MaxLength(150)] public string PersonInCharge { get; set; }

        public bool DoesPOCreatedForThisRequest { get; set; }

        public int? ParentRequestRefId { get; set; }
        //[ForeignKey("ParentRequestRefId")]
        //public InterTransfer ParentInterTransferRef { get; set; }

        public int TenantId { get; set; }
        [ForeignKey("TenantId")] public Tenant Tenants { get; set; }
    }

    [TrackChanges]
    [Table("InterTransferDetails")]
    public class InterTransferDetail : FullAuditedEntity
    {
        [Required] public int InterTransferRefId { get; set; }

        [ForeignKey("InterTransferRefId")] public InterTransfer InterTransfer { get; set; }

        [SkipTracking]
        [Required] public int Sno { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Material { get; set; }

        [Required] public decimal RequestQty { get; set; }

        [Required] public decimal IssueQty { get; set; }

        [Required] public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }

        public decimal ConversionWithDefaultUnit { get; set; }
        public decimal Price { get; set; }

        public decimal IssueValue { get; set; }

        [Required]
        [StringLength(1)] //  P - Pending  (Default ) , C - Completed, X - Cancel, R  - Rejected , A - Approved 
        [MaxLength(1)]
        public string CurrentStatus { get; set; }

        [SkipTracking]
        [MaxLength(100)] public string RequestRemarks { get; set; }

        [SkipTracking]
        [MaxLength(100)] public string ApprovedRemarks { get; set; }

        public int? PurchaseOrderRefId { get; set; }

        [ForeignKey("PurchaseOrderRefId")] public PurchaseOrder PurchaseOrders { get; set; }
    }

    [Table("InterTransferReceivedDetails")]
    public class InterTransferReceivedDetail : FullAuditedEntity
    {
        [Required] public int InterTransferRefId { get; set; }

        [ForeignKey("InterTransferRefId")] public InterTransfer InterTransfer { get; set; }

        [Required] public int Sno { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Material { get; set; }

        [Required] public decimal ReceivedQty { get; set; }

        [Required] public decimal ReturnQty { get; set; }

        [MaxLength(150)] public string AdjustmentMode { get; set; }

        [Required] public decimal AdjustmentQty { get; set; }

        [Required] public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }
        public decimal Price { get; set; }

        public decimal ReceivedValue { get; set; }

        [Required]
        [StringLength(1)] //  P - Pending  (Default ) , C - Completed, X - Cancel, R  - Rejected , A - Approved 
        [MaxLength(1)]
        public string CurrentStatus { get; set; }
    }
    //--------------------------------------------------------------------------------------------------------------------

    [Table("Issues")]
    public class Issue : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Location { get; set; }

        [MaxLength(150)] public string RequestSlipNumber { get; set; }

        [Required] public DateTime RequestTime { get; set; }
        public DateTime? AccountDate { get; set; }

        public DateTime IssueTime { get; set; }

        public int? RequestRefId { get; set; }

        [ForeignKey("RequestRefId")] public Request Request { get; set; }

        //public int? RecipeRefId { get; set; }
        //[ForeignKey("RecipeRefId")]
        //public Material RecipeMaterials { get; set; }

        //public decimal? RecipeProductionQty { get; set; }

        public int? MaterialGroupCategoryRefId { get; set; }

        //public bool MultipleBatchProductionAllowed { get; set; }

        public int? ProductionUnitRefId { get; set; }

        [ForeignKey("ProductionUnitRefId")] public ProductionUnit ProductionUnit { get; set; }

        [Required] public int TokenNumber { get; set; }

        [Required] public bool CompletedStatus { get; set; }

        public int TenantId { get; set; }
    }


    [Table("IssueRecipeDetails")]
    public class IssueRecipeDetail : FullAuditedEntity
    {
        public int IssueRefId { get; set; }

        [ForeignKey("IssueRefId")] public Issue Issue { get; set; }

        public int Sno { get; set; }

        public int RecipeRefId { get; set; }

        [ForeignKey("RecipeRefId")] public Material RecipeMaterials { get; set; }

        public decimal RecipeProductionQty { get; set; }

        public decimal CompletedQty { get; set; }

        public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }

        public bool MultipleBatchProductionAllowed { get; set; }

        public bool CompletionFlag { get; set; }

        [MaxLength(150)] public string Remarks { get; set; }
    }


    [Table("IssueDetails")]
    public class IssueDetail : FullAuditedEntity
    {
        public int IssueRefId { get; set; }

        [ForeignKey("IssueRefId")] public Issue Issue { get; set; }

        [Required] public int Sno { get; set; }

        public int? RecipeRefId { get; set; }

        [ForeignKey("RecipeRefId")] public Material RecipeDetails { get; set; }


        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Material { get; set; }

        [Required] public decimal RequestQty { get; set; }

        [Required] public decimal IssueQty { get; set; }

        [Required] public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }
    }

    [Table("Requests")]
    public class Request : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Location { get; set; }

        [MaxLength(150)] public string RequestSlipNumber { get; set; }

        [Required] public DateTime RequestTime { get; set; }

        //public int? RecipeRefId { get; set; }
        //[ForeignKey("RecipeRefId")]
        //public Material RecipeMaterials { get; set; }

        //public decimal? RecipeProductionQty { get; set; }

        public int MaterialGroupCategoryRefId { get; set; }

        public int? ProductionUnitRefId { get; set; }

        [ForeignKey("ProductionUnitRefId")] public ProductionUnit ProductionUnit { get; set; }


        //public bool MultipleBatchProductionAllowed { get; set; }

        [Required] public bool CompletedStatus { get; set; }


        public int TenantId { get; set; }
    }

    [Table("RequestRecipeDetails")]
    public class RequestRecipeDetail : FullAuditedEntity
    {
        public int RequestRefId { get; set; }

        [ForeignKey("RequestRefId")] public Request Request { get; set; }

        public int Sno { get; set; }

        public int RecipeRefId { get; set; }

        [ForeignKey("RecipeRefId")] public Material RecipeMaterials { get; set; }

        public decimal RecipeProductionQty { get; set; }

        public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }

        public bool MultipleBatchProductionAllowed { get; set; }

        [MaxLength(150)] public string Remarks { get; set; }
    }


    [Table("RequestDetails")]
    public class RequestDetail : FullAuditedEntity
    {
        public int RequestRefId { get; set; }

        [ForeignKey("RequestRefId")] public Request Request { get; set; }

        [Required] public int Sno { get; set; }

        //public int? RecipeRefId { get; set; }
        //[ForeignKey("RecipeRefId")]
        //public Material RecipeDetails { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Material { get; set; }

        [Required] public decimal RequestQty { get; set; }

        [Required] public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }
    }

    [Table("Returns")]
    public class Return : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Location { get; set; }

        [Required] public DateTime ReturnDate { get; set; }
        public DateTime? AccountDate { get; set; }

        public int? MaterialGroupCategoryRefId { get; set; }

        [Required] public long TokenRefNumber { get; set; }

        public int? ProductionUnitRefId { get; set; }

        [ForeignKey("ProductionUnitRefId")] public ProductionUnit ProductionUnit { get; set; }

        public int? IssueRefId { get; set; }

        [ForeignKey("IssueRefId")] public Issue Issues { get; set; }

        public int TenantId { get; set; }
    }

    [Table("ReturnDetails")]
    public class ReturnDetail : Entity
    {
        [ForeignKey("MaterialRefId")] public Material Material;

        [Required] public int ReturnRefId { get; set; }

        [ForeignKey("ReturnRefId")] public Return Return { get; set; }

        [Required] public int Sno { get; set; }

        public int RecipeRefId { get; set; }

        [ForeignKey("MaterialRefId")] public virtual Material RecipeMaterials { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [Required] public decimal ReturnQty { get; set; }

        [Required] public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }

        [MaxLength(150)] public string Remarks { get; set; }
    }


    [Table("Adjustments")]
    public class Adjustment : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Location { get; set; }

        [Required] public DateTime AdjustmentDate { get; set; }

        [Required] public long TokenRefNumber { get; set; }

        [Required] [MaxLength(100)] public string AdjustmentRemarks { get; set; }

        public DateTime? ClosingStockDate { get; set; }
        public DateTime? AccountDate { get; set; }

        public int TenantId { get; set; }
        [ForeignKey("TenantId")] public Tenant Tenants { get; set; }
    }

    [Table("AdjustmentDetails")]
    public class AdjustmentDetail : Entity
    {
        [ForeignKey("MaterialRefId")] public Material Material;

        public int AdjustmentRefIf { get; set; }

        [ForeignKey("AdjustmentRefIf")] public Adjustment Adjustment { get; set; }

        [Required] public int Sno { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [Required] public decimal AdjustmentQty { get; set; }

        public decimal? StockEntry { get; set; }
        public decimal? ClosingStock { get; set; }


        [Required] public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }

        public decimal ConversionWithDefaultUnit { get; set; }
        [Required] [MaxLength(20)] public string AdjustmentMode { get; set; }

        [MaxLength(100)] public string AdjustmentApprovedRemarks { get; set; }
    }

    //--------------------------------------------------------------------------------------------------------------------


    [TrackChanges]
    [Table("Invoices")]
    public class Invoice : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Location { get; set; }

        [Required] public DateTime InvoiceDate { get; set; }

        //[Required] [MaxLength(150)] public string InvoiceMode { get; set; }
        public int InvoicePayMode { get; set; }

        [Required] public DateTime AccountDate { get; set; }

        [Required] public int SupplierRefId { get; set; }

        [ForeignKey("SupplierRefId")] public Supplier Suppliers { get; set; }

        [Required] [MaxLength(100)] public string InvoiceNumber { get; set; }

        [Required] public DateTime DcFromDate { get; set; }

        [Required] public DateTime DcToDate { get; set; }

        [Required] public decimal TotalDiscountAmount { get; set; }

        [Required] public decimal TotalShipmentCharges { get; set; }

        [Required] public decimal RoundedAmount { get; set; }

        [Required] public decimal InvoiceAmount { get; set; }

        [Required] public bool IsDirectInvoice { get; set; }

        public int? PurchaseCategoryRefId { get; set; }

        [ForeignKey("PurchaseCategoryRefId")] public PurchaseCategory PurchaseCategories { get; set; }

        public int? AdjustmentRefId { get; set; }

        [ForeignKey("AdjustmentRefId")] public Adjustment Adjustment { get; set; }

        public DateTime? ExportDateTimeForExternalImport { get; set; }

        public int TenantId { get; set; }
        [ForeignKey("TenantId")] public Tenant Tenants { get; set; }
    }

    [Table("InvoiceDetails")]
    public class InvoiceDetail : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int InvoiceRefId { get; set; }

        [ForeignKey("InvoiceRefId")] public Invoice Invoices { get; set; }

        [Required] public int Sno { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Materials { get; set; }

        [Required] public decimal TotalQty { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal TotalAmount { get; set; }

        [Required]
        [MaxLength(1)] //  F - Fixed , P - Percentage
        public string DiscountFlag { get; set; }

        [Required] public decimal DiscountPercentage { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal DiscountAmount { get; set; }

        [Required] public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal TaxAmount { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal NetAmount { get; set; }

        [MaxLength(150)] public string Remarks { get; set; }

        public decimal? YieldPercentage { get; set; }

        [MaxLength(150)] public string YieldMode { get; set; }

        public decimal? YieldExcessShortageQty { get; set; }
        public int TenantId { get; set; }
        [ForeignKey("TenantId")] public Tenant Tenants { get; set; }
    }

    [Table("InvoiceTaxDetails")]
    public class InvoiceTaxDetail : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int InvoiceRefId { get; set; }

        [ForeignKey("InvoiceRefId")] public Invoice Invoices { get; set; }

        [Required] public int Sno { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Materials { get; set; }

        [Required] public int TaxRefId { get; set; }

        [ForeignKey("TaxRefId")] public Tax Taxes { get; set; }

        public decimal TaxRate { get; set; }

        public decimal TaxValue { get; set; }


        public int TenantId { get; set; }
        [ForeignKey("TenantId")] public Tenant Tenants { get; set; }
    }

    [Table("InvoiceDirectCreditLink")]
    public class InvoiceDirectCreditLink : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int InvoiceRefId { get; set; }

        [ForeignKey("InvoiceRefId")] public Invoice Invoices { get; set; }

        [Required] public int Sno { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Materials { get; set; }

        [Required] public int InwardDirectCreditRefId { get; set; }

        [ForeignKey("InwardDirectCreditRefId")]
        public InwardDirectCredit InwardDirectCredit { get; set; }

        public int TenantId { get; set; }
        [ForeignKey("TenantId")] public Tenant Tenants { get; set; }
    }

    [TrackChanges]
    [Table("PurchaseOrders")]
    public class PurchaseOrder : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public DateTime PoDate { get; set; }

        [SkipTracking]
        [Required] [MaxLength(170)] public string PoReferenceCode { get; set; }

        [Required] public int ShippingLocationId { get; set; }

        [ForeignKey("ShippingLocationId")] public Location LocationShipping { get; set; }

        [SkipTracking]
        [MaxLength(170)] public string QuoteReference { get; set; }

        [Required] public int SupplierRefId { get; set; }

        [ForeignKey("SupplierRefId")] public Supplier Supplier { get; set; }

        [Required] public decimal PoNetAmount { get; set; }

        [MaxLength(150)] public string Remarks { get; set; }

        [SkipTracking]
        public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Location { get; set; }

        [Required] public DateTime DeliveryDateExpected { get; set; }

        [SkipTracking]
        public int CreditDays { get; set; }

        public DateTime? ApprovedTime { get; set; }

        [MaxLength(150)] public string PoCurrentStatus { get; set; }

        public bool PoBasedOnInterTransferRequest { get; set; }
        public bool PoBasedOnCater { get; set; }
        public DateTime? ExportDateToAccountSoftware { get; set; }
        public int TenantId { get; set; }
        [ForeignKey("TenantId")] public Tenant Tenants { get; set; }
    }

    [Table("PurchaseOrderDetails")]
    public class PurchaseOrderDetail : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int PoRefId { get; set; }

        [ForeignKey("PoRefId")] public PurchaseOrder PurchaseOrders { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Materials { get; set; }

        [Required] [DecimalPrecision(18, 6)] public decimal QtyOrdered { get; set; }

        [Required] [DecimalPrecision(18, 6)] public decimal QtyReceived { get; set; }

        [Required] public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal TotalAmount { get; set; }

        [Required]
        [MaxLength(1)] //  F - Fixed , P - Percentage
        public string DiscountOption { get; set; }

        [Required] public decimal DiscountValue { get; set; }

        [Required] public decimal TaxAmount { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal NetAmount { get; set; }
        [SkipTracking]
        [Required] public bool PoStatus { get; set; }
        [SkipTracking]
        [MaxLength(150)] public string Remarks { get; set; }


        public int TenantId { get; set; }
        [ForeignKey("TenantId")] public Tenant Tenants { get; set; }
    }

    [Table("PurchaseOrderTaxDetails")]
    public class PurchaseOrderTaxDetail : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int PoRefId { get; set; }

        [ForeignKey("PoRefId")] public PurchaseOrder PurchaseOrders { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Materials { get; set; }

        [Required] public int TaxRefId { get; set; }

        [ForeignKey("TaxRefId")] public Tax Taxes { get; set; }

        public decimal TaxRate { get; set; }

        public decimal TaxValue { get; set; }
        public int TenantId { get; set; }
        [ForeignKey("TenantId")] public Tenant Tenants { get; set; }
    }

    [Table("UserDefaultOrganizations")]
    public class UserDefaultOrganization : FullAuditedEntity, IMustHaveTenant
    {
        //public UserDefaultOrganization() { }

        //public UserDefaultOrganization(int tenantId, long userId, long organizationUnitId)
        //{
        //    TenantId = tenantId;
        //    UserId = userId;
        //    OrganizationUnitId = organizationUnitId;
        //}

        public virtual long OrganizationUnitId { get; set; }

        public virtual long UserId { get; set; }

        public virtual int TenantId { get; set; }
    }

    [Table("HouseReports")]
    public class HouseReport : Entity
    {
        public HouseReport()
        {
        }


        public HouseReport(string argReportKey, string argDesc, string argReportRole)
        {
            ReportKey = argReportKey;
            Description = argDesc;
            ReportRole = argReportRole;
        }

        [MaxLength(50)] public string ReportKey { get; set; }

        [MaxLength(150)] public string Description { get; set; }

        [MaxLength(150)] public string ReportRole { get; set; }
    }


    [Table("Templates")]
    public class Template : FullAuditedEntity, IMustHaveTenant
    {
        [Required] [MaxLength(150)] public string Name { get; set; }

        public int TemplateType { get; set; }
        public string TemplateData { get; set; }
        public int? LocationRefId { get; set; } //  If it is null, Global Level Reference
        public virtual int TenantId { get; set; }
    }

    [Table("Customers")]
    public class Customer : FullAuditedEntity, IMustHaveTenant
    {
        [Required] [MaxLength(160)] public virtual string CustomerName { get; set; }

        [Required] public int CustomerTagRefId { get; set; }

        [ForeignKey("CustomerTagRefId")] public CustomerTagDefinition CustomerTagDefinition { get; set; }

        [MaxLength(150)] public string Address1 { get; set; }


        [MaxLength(150)] public string Address2 { get; set; }

        [MaxLength(150)] public string Address3 { get; set; }

        [MaxLength(150)] public virtual string City { get; set; }

        [MaxLength(150)] public virtual string State { get; set; }

        [MaxLength(150)] public virtual string Country { get; set; }

        [MaxLength(150)] public string ZipCode { get; set; }

        [MaxLength(150)] public string PhoneNumber { get; set; }

        public int DefaultCreditDays { get; set; }

        [MaxLength(150)] public string OrderPlacedThrough { get; set; }

        [MaxLength(150)] public string Email { get; set; }

        [MaxLength(150)] public string FaxNumber { get; set; }

        [MaxLength(150)] public string Website { get; set; }

        [MaxLength(100)] public string AddOn { get; set; }

        public virtual int TenantId { get; set; }
    }


    [Table("CustomerMaterials")]
    public class CustomerMaterial : FullAuditedEntity
    {
        [Required] public int CustomerRefId { get; set; }

        [ForeignKey("CustomerRefId")] public Customer Customers { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Materials { get; set; }

        public decimal MaterialPrice { get; set; }

        [MaxLength(150)] public string LastQuoteRefNo { get; set; }

        public DateTime LastQuoteDate { get; set; }
    }

    [Table("Yield")]
    public class Yield : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Location { get; set; }

        [MaxLength(150)] public string RequestSlipNumber { get; set; }

        [Required] public DateTime IssueTime { get; set; }
        public DateTime? AccountDate { get; set; }
        public DateTime? YieldOutputAccountDate { get; set; }

        public DateTime? CompletedTime { get; set; }

        [Required] public int TokenNumber { get; set; }

        [Required] [MaxLength(150)] public string Status { get; set; }

        public int? RecipeRefId { get; set; }

        [ForeignKey("RecipeRefId")] public Material RecipeMaterials { get; set; }

        public decimal? RecipeProductionQty { get; set; }


        public int? ProductionUnitRefId { get; set; }

        [ForeignKey("ProductionUnitRefId")] public ProductionUnit ProductionUnit { get; set; }


        public virtual int TenantId { get; set; }
    }

    [Table("YieldInput")]
    public class YieldInput : FullAuditedEntity
    {
        public int YieldRefId { get; set; }

        [ForeignKey("YieldRefId")] public Yield Yield { get; set; }

        [Required] public int Sno { get; set; }

        public int InputMaterialRefId { get; set; }

        [ForeignKey("InputMaterialRefId")] public Material Materials { get; set; }

        public decimal InputQty { get; set; }

        public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }
    }

    [Table("YieldOutput")]
    public class YieldOutput : FullAuditedEntity
    {
        public int YieldRefId { get; set; }

        [ForeignKey("YieldRefId")] public Yield Yield { get; set; }

        [Required] public int Sno { get; set; }

        public int OutputMaterialRefId { get; set; }

        [ForeignKey("OutputMaterialRefId")] public Material Materials { get; set; }

        public decimal OutputQty { get; set; }

        public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }

        public decimal YieldPrice { get; set; }
    }


    [Table("Productions")]
    public class Production : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Location { get; set; }

        [MaxLength(150)] public string ProductionSlipNumber { get; set; }

        [Required] public DateTime ProductionTime { get; set; }

        [Required] [MaxLength(150)] public string Status { get; set; }

        public DateTime? AccountDate { get; set; }
        public int? ProductionUnitRefId { get; set; }

        [ForeignKey("ProductionUnitRefId")] public ProductionUnit ProductionUnit { get; set; }

        public virtual int TenantId { get; set; }
    }

    [Table("ProductionDetails")]
    public class ProductionDetail : FullAuditedEntity
    {
        public int ProductionRefId { get; set; }

        [ForeignKey("ProductionRefId")] public Production Production { get; set; }

        [Required] public int Sno { get; set; }

        public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Materials { get; set; }

        public decimal ProductionQty { get; set; }

        public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }

        public decimal ConversionWithDefaultUnit { get; set; }

        public int? IssueRefId { get; set; }

        [ForeignKey("IssueRefId")] public Issue Issue { get; set; }
        public decimal Price { get; set; }
    }

    [Table("PurchaseReturns")]
    public class PurchaseReturn : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Locations { get; set; }


        [Required] public int SupplierRefId { get; set; }

        [ForeignKey("SupplierRefId")] public Supplier Suppliers { get; set; }

        public DateTime ReturnDate { get; set; }
        public DateTime? AccountDate { get; set; }

        [MaxLength(150)] public string ReferenceNumber { get; set; }

        public int? DcRefId { get; set; }

        public int? InvoiceRefId { get; set; }

        public decimal PurchaseReturnAmount { get; set; }

        [MaxLength(150)] public string Remarks { get; set; }

        public virtual int TenantId { get; set; }
    }

    [Table("PurchaseReturnDetails")]
    public class PurchaseReturnDetail : FullAuditedEntity
    {
        [Required] public int PurchaseReturnRefId { get; set; }

        [ForeignKey("PurchaseReturnRefId")] public PurchaseReturn PurchaseReturn { get; set; }

        public int Sno { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Material { get; set; }

        [Required] public decimal Quantity { get; set; }


        [Required]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal TotalAmount { get; set; }

        [Required] public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal TaxAmount { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal NetAmount { get; set; }

        [MaxLength(150)] public string Remarks { get; set; }
    }

    [Table("PurchaseReturnTaxDetails")]
    public class PurchaseReturnTaxDetail : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int PurchaseReturnRefId { get; set; }

        [ForeignKey("PurchaseReturnRefId")] public PurchaseReturn PurchaseReturn { get; set; }

        [Required] public int Sno { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Materials { get; set; }

        [Required] public int TaxRefId { get; set; }

        [ForeignKey("TaxRefId")] public Tax Taxes { get; set; }

        public decimal TaxRate { get; set; }

        public decimal TaxValue { get; set; }

        public int TenantId { get; set; }
    }


    [Table("SupplierDocuments")]
    public class SupplierDocument : FullAuditedEntity
    {
        [Required] public int DocumentRefId { get; set; }

        [Required] public int SupplierRefId { get; set; }

        [Required] [MaxLength(100)] public string FileName { get; set; }


        [Required] public bool IsEntryFinished { get; set; }

        public DateTime? CompletedTime { get; set; }
    }

    [Table("ProductionUnits")]
    public class ProductionUnit : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Locations { get; set; }

        [MaxLength(150)] public string Name { get; set; }

        [MaxLength(150)] public string Code { get; set; }

        [MaxLength(100)] public string MaterialAllowed { get; set; }

        [MaxLength(100)] public string IsActive { get; set; }

        public int TenantId { get; set; }
    }

    [Table("SalesOrders")]
    public class SalesOrder : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public DateTime SoDate { get; set; }

        [Required] [MaxLength(150)] public string SoReferenceCode { get; set; }

        [Required] public int ShippingLocationId { get; set; }

        [ForeignKey("ShippingLocationId")] public Location LocationShipping { get; set; }

        [MaxLength(150)] public string QuoteReference { get; set; }

        [Required] public int CustomerRefId { get; set; }

        [ForeignKey("CustomerRefId")] public Customer Customer { get; set; }

        [Required] public decimal SoNetAmount { get; set; }

        [MaxLength(150)] public string Remarks { get; set; }

        public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Location { get; set; }

        [Required] public DateTime DeliveryDateExpected { get; set; }

        public int CreditDays { get; set; }

        public DateTime? ApprovedTime { get; set; }

        [MaxLength(150)] public string SoCurrentStatus { get; set; }

        public int TenantId { get; set; }
    }

    [Table("SalesOrderDetails")]
    public class SalesOrderDetail : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int SoRefId { get; set; }

        [ForeignKey("SoRefId")] public SalesOrder SalesOrders { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Materials { get; set; }

        [Required] [DecimalPrecision(18, 3)] public decimal QtyOrdered { get; set; }

        [Required] [DecimalPrecision(18, 3)] public decimal QtyReceived { get; set; }

        [Required] public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal TotalAmount { get; set; }

        [Required]
        [MaxLength(1)] //  F - Fixed , P - Percentage
        public string DiscountOption { get; set; }

        [Required] public decimal DiscountValue { get; set; }

        [Required] public decimal TaxAmount { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal NetAmount { get; set; }

        [Required] public bool SoStatus { get; set; }

        [MaxLength(150)] public string Remarks { get; set; }


        public int TenantId { get; set; }
    }

    [Table("SalesOrderTaxDetails")]
    public class SalesOrderTaxDetail : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int SoRefId { get; set; }

        [ForeignKey("SoRefId")] public SalesOrder SalesOrders { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Materials { get; set; }

        [Required] public int SalesTaxRefId { get; set; }

        [ForeignKey("SalesTaxRefId")] public SalesTax SalesTaxes { get; set; }

        public decimal SalesTaxRate { get; set; }

        public decimal SalesTaxValue { get; set; }
        public int TenantId { get; set; }
    }

    [Table("SalesDeliveryOrders")]
    public class SalesDeliveryOrder : FullAuditedEntity, IMustHaveTenant
    {
        public DateTime? AccountDate { get; set; }
        [Required] public DateTime DeliveryDate { get; set; }

        [Required] public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Locations { get; set; }

        [Required] public int CustomerRefId { get; set; }

        [ForeignKey("CustomerRefId")] public Customer Customers { get; set; }

        [Required] [MaxLength(150)] public string DcNumberGivenByCustomer { get; set; }

        [Required] public bool IsDcCompleted { get; set; }

        [MaxLength(150)] public string SoReferenceCode { get; set; }

        public int? SoRefId { get; set; }

        [ForeignKey("SoRefId")] public SalesOrder SalesOrders { get; set; }

        public int TenantId { get; set; }
    }

    [Table("SalesDeliveryOrderDetails")]
    public class SalesDeliveryOrderDetail : FullAuditedEntity
    {
        [Required] public int DeliveryRefId { get; set; }

        [ForeignKey("DeliveryRefId")] public SalesDeliveryOrder SalesDeliveryOrders { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Materials { get; set; }

        [Required] public decimal DeliveryQty { get; set; }

        public decimal BillConvertedQty { get; set; }

        [Required] public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }

        [Required] [MaxLength(150)] public string MaterialReceivedStatus { get; set; }
    }

    [Table("SalesInvoices")]
    public class SalesInvoice : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Location { get; set; }

        [Required] public DateTime InvoiceDate { get; set; }

        [Required] [MaxLength(150)] public string InvoiceMode { get; set; }

        [Required] public DateTime AccountDate { get; set; }

        [Required] public int CustomerRefId { get; set; }

        [ForeignKey("CustomerRefId")] public Customer Customers { get; set; }

        [Required] [MaxLength(150)] public string InvoiceNumber { get; set; }

        [Required] public DateTime DcFromDate { get; set; }

        [Required] public DateTime DcToDate { get; set; }

        [Required] public decimal TotalDiscountAmount { get; set; }

        [Required] public decimal TotalShipmentCharges { get; set; }

        [Required] public decimal RoundedAmount { get; set; }

        [Required] public decimal InvoiceAmount { get; set; }

        [Required] public bool IsDirectSalesInvoice { get; set; }

        public int TenantId { get; set; }
    }

    [Table("SalesInvoiceDetails")]
    public class SalesInvoiceDetail : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int SalesInvoiceRefId { get; set; }

        [ForeignKey("SalesInvoiceRefId")] public SalesInvoice SalesInvoices { get; set; }

        [Required] public int Sno { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Materials { get; set; }

        [Required] public decimal TotalQty { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal TotalAmount { get; set; }

        [Required]
        [MaxLength(1)] //  F - Fixed , P - Percentage

        public string DiscountFlag { get; set; }

        [Required] public decimal DiscountPercentage { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal DiscountAmount { get; set; }

        [Required] public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Units { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal SalesTaxAmount { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal NetAmount { get; set; }

        [MaxLength(150)] public string Remarks { get; set; }

        public int TenantId { get; set; }
    }

    [Table("SalesInvoiceTaxDetails")]
    public class SalesInvoiceTaxDetail : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int SalesInvoiceRefId { get; set; }

        [ForeignKey("SalesInvoiceRefId")] public SalesInvoice Invoices { get; set; }

        [Required] public int Sno { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Materials { get; set; }

        [Required] public int SalesTaxRefId { get; set; }

        [ForeignKey("SalesTaxRefId")] public SalesTax SalesTaxes { get; set; }

        public decimal SalesTaxRate { get; set; }

        public decimal SalesTaxValue { get; set; }


        public int TenantId { get; set; }
    }

    [Table("SalesInvoiceDeliveryOrderLinks")]
    public class SalesInvoiceDeliveryOrderLink : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int SalesInvoiceRefId { get; set; }

        [ForeignKey("SalesInvoiceRefId")] public SalesInvoice SalesInvoices { get; set; }

        [Required] public int Sno { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Materials { get; set; }

        [Required] public int DeliveryOrderRefId { get; set; }

        [ForeignKey("DeliveryOrderRefId")] public SalesDeliveryOrder SalesDeliveryOrders { get; set; }

        public int TenantId { get; set; }
    }

    [Table("ClosingStocks")]
    public class ClosingStock : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Locations { get; set; }

        [Required] public DateTime StockDate { get; set; }

        public long? ApprovedUserId { get; set; }

        public int? AdjustmentRefId { get; set; }

        [Required] public int TenantId { get; set; }
    }

    [Table("ClosingStockDetails")]
    public class ClosingStockDetail : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int ClosingStockRefId { get; set; }

        [ForeignKey("ClosingStockRefId")] public ClosingStock ClosingStocks { get; set; }

        [Required] public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Material { get; set; }

        [Required] public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Unit { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }
        [Required] public decimal OnHand { get; set; }

        public int TenantId { get; set; }
    }

    [Table("ClosingStockUnitWiseDetails")]
    public class ClosingStockUnitWiseDetail : FullAuditedEntity
    {
        [Required] public int ClosingStockDetailRefId { get; set; }

        [ForeignKey("ClosingStockDetailRefId")]
        public ClosingStockDetail ClosingStockDetails { get; set; }

        [Required] public decimal OnHand { get; set; }

        [Required] public int UnitRefId { get; set; }

        [ForeignKey("UnitRefId")] public Unit Unit { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }
    }


    [Table("MenuItemWastages")]
    public class MenuItemWastage : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Location { get; set; }

        [Required] public DateTime SalesDate { get; set; }

        public DateTime? AccountDate { get; set; }

        public int? AdjustmentRefId { get; set; }

        [ForeignKey("AdjustmentRefId")] public Adjustment Adjustment { get; set; }

        [Required] public long TokenRefNumber { get; set; }

        [Required] [MaxLength(150)] public string Remarks { get; set; }

        public int TenantId { get; set; }
    }

    [Table("MenuItemWastageDetails")]
    public class MenuItemWastageDetail : Entity
    {
        public int MenuItemWastageRefId { get; set; }

        [ForeignKey("MenuItemWastageRefId")] public MenuItemWastage MenuItemWastages { get; set; }

        [Required] public int Sno { get; set; }

        [Required] public int PosMenuPortionRefId { get; set; }

        [ForeignKey("PosMenuPortionRefId")] public MenuItemPortion MenuItemPortions { get; set; }

        [Required] public decimal WastageQty { get; set; }

        public decimal Cost { get; set; }

        [MaxLength(150)] public string WastageRemarks { get; set; }
    }

    [Table("DayCloseTemplates")]
    public class DayCloseTemplate : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Locations { get; set; }

        [Required] public DateTime ClosedDate { get; set; }

        [Required] [MaxLength(100)] public string TableName { get; set; }

        [Required] public int TemplateType { get; set; }

        [Required] public string TemplateData { get; set; }

        public virtual int TenantId { get; set; }
    }

    [Table("MaterialRateTemplates")]
    public class MaterialRateTemplate : FullAuditedEntity, IMustHaveTenant
    {
        [Required] public int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Locations { get; set; }

        [Required] public DateTime StartDate { get; set; }

        [Required] public DateTime EndDate { get; set; }

        [Required] [MaxLength(150)] public int TemplateType { get; set; }


        public string TemplateData { get; set; }

        public virtual int TenantId { get; set; }

        //public Enum TemplateModal { get; set; }
    }

    [Table("InventoryCycleTags")]
    public class InventoryCycleTag : FullAuditedEntity, IMustHaveTenant
    {
        [Index("InventoryCycleTagCode", IsUnique = true)]
        [Required]
        [MaxLength(150)]

        public virtual string InventoryCycleTagCode { get; set; }

        [MaxLength(150)] public virtual string InventoryCycleTagDescription { get; set; }

        public virtual int InventoryCycleMode { get; set; }

        [MaxLength(150)] public virtual string InventoryModeSchedule { get; set; }


        public int TenantId { get; set; }
    }


    [Table("InventoryCycleLinkWithLocations")]
    public class InventoryCycleLinkWithLocation : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        [Index("MPK_LocationGroupId_LocationRefId_LocationTagRefId_InvCycleRefId", IsUnique = true, Order = 0)]
        public int InventoryCycleTagRefId { get; set; }

        [ForeignKey("InventoryCycleTagRefId")] public InventoryCycleTag InventoryCycleTag { get; set; }

        [Index("MPK_LocationGroupId_LocationRefId_LocationTagRefId_InvCycleRefId", IsUnique = true, Order = 1)]
        public int? LocationGroupRefId { get; set; }

        [ForeignKey("LocationGroupRefId")] public LocationGroup LocationGroups { get; set; }

        [Index("MPK_LocationGroupId_LocationRefId_LocationTagRefId_InvCycleRefId", IsUnique = true, Order = 2)]
        public int? LocationTagRefId { get; set; }

        [ForeignKey("LocationTagRefId")] public LocationTag LocationTags { get; set; }


        [Index("MPK_LocationGroupId_LocationRefId_LocationTagRefId_InvCycleRefId", IsUnique = true, Order = 3)]
        public int? LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Locations { get; set; }


        public int TenantId { get; set; }
    }

    [Table("MaterialStockCycleLinks")]
    public class MaterialStockCycleLink : FullAuditedEntity, IMustHaveTenant
    {
        //[Index("MPK_LocationGroupId_LocationRefId_MaterialRefId_InvCycleRefId", IsUnique = true, Order = 3)]
        //public int? LocationGroupRefId { get; set; }
        //[ForeignKey("LocationGroupRefId")]
        //public LocationGroup LocationGroup { get; set; }

        //[Index("MPK_LocationGroupId_LocationRefId_MaterialRefId_InvCycleRefId", IsUnique = true, Order = 2)]
        //public int? LocationRefId { get; set; }
        //[ForeignKey("LocationRefId")]
        //public Location Locations { get; set; }

        [Required]
        //[Index("MPK_MaterialRefId_InvCycleRefId", IsUnique = true, Order = 0)]

        public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Material { get; set; }

        [Required]
        //[Index("MPK_MaterialRefId_InvCycleRefId", IsUnique = true, Order = 1)]

        public int InventoryCycleTagRefId { get; set; }

        [ForeignKey("InventoryCycleTagRefId")] public InventoryCycleTag InventoryCycleTag { get; set; }

        public int TenantId { get; set; }
    }


    [Table("PurchaseCategories")]
    public class PurchaseCategory : FullAuditedEntity, IMustHaveTenant
    {
        [MaxLength(150)] public virtual string PurchaseCategoryName { get; set; }

        public virtual int TenantId { get; set; }
    }

   

    [Table("LocationWiseMaterialRateViews")]
    public class LocationWiseMaterialRateView : FullAuditedEntity, IMustHaveTenant
    {
        [Index("MPK_LocationRefId_MaterialRefId_AveragePriceTagRefId", IsUnique = true, Order = 0)]
        public int? LocationRefId { get; set; }

        [ForeignKey("LocationRefId")] public Location Locations { get; set; }

        [Index("MPK_LocationRefId_MaterialRefId_AveragePriceTagRefId", IsUnique = true, Order = 1)]
        public int MaterialRefId { get; set; }

        [ForeignKey("MaterialRefId")] public Material Material { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        [Index("MPK_LocationRefId_MaterialRefId_AveragePriceTagRefId", IsUnique = true, Order = 2)]
        public int AveragePriceTagRefId { get; set; }

        public decimal BilledQty { get; set; }
        public int DefaultUnitId { get; set; }
        public decimal NetAmount { get; set; }
        public decimal AvgRate { get; set; }
        public bool RefreshRequired { get; set; }
        public DateTime LastUpdatedTime { get; set; }

        [MaxLength(150)] public string InvoiceDateRange { get; set; }

        public bool IsOmitFOCMaterialQuantities { get; set; }
        public bool IsCalculateAverageRateWithoutTax { get; set; }
        public int TenantId { get; set; }
    }


    [AttributeUsage(AttributeTargets.Property)]
    public sealed class DecimalPrecisionAttribute : Attribute
    {
        public DecimalPrecisionAttribute(byte precision, byte scale)
        {
            Precision = precision;
            Scale = scale;
        }

        public byte Precision { get; set; }
        public byte Scale { get; set; }
    }
}