﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IPurchaseOrderManager
    {
        Task<IdentityResult> CreateSync(PurchaseOrder purchaseOrder);
    }
}
