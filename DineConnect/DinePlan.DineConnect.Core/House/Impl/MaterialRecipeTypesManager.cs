﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class MaterialRecipeTypesManager : DineConnectServiceBase, IMaterialRecipeTypesManager, ITransientDependency
    {

        private readonly IRepository<MaterialRecipeTypes> _materialRecipeTypesRepo;

        public MaterialRecipeTypesManager(IRepository<MaterialRecipeTypes> materialRecipeTypes)
        {
            _materialRecipeTypesRepo = materialRecipeTypes;
        }

        public async Task<IdentityResult> CreateSync(MaterialRecipeTypes materialRecipeTypes)
        {
            //  if the New Addition
            if (materialRecipeTypes.Id == 0)
            {
                if (_materialRecipeTypesRepo.GetAll().Any(a => a.MaterailRefId==materialRecipeTypes.MaterailRefId ))
                {
                    string[] strArrays = { L("CodeAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _materialRecipeTypesRepo.InsertAndGetIdAsync(materialRecipeTypes);
                return IdentityResult.Success;

            }
            else
            {
                List<MaterialRecipeTypes> lst = _materialRecipeTypesRepo.GetAll().Where(a => a.MaterailRefId == materialRecipeTypes.MaterailRefId && a.Id != materialRecipeTypes.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("CodeAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
