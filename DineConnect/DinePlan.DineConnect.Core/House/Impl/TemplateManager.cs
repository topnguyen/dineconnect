﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace DinePlan.DineConnect.House.Impl
{
    public class TemplateManager : DineConnectServiceBase, ITemplateManager, ITransientDependency
    {

        private readonly IRepository<Template> _templateRepo;
        private readonly IRepository<DayCloseTemplate> _daycloseTemplateRepo;

        public TemplateManager(IRepository<Template> template,
            IRepository<DayCloseTemplate> daycloseTemplateRepo)
        {
            _templateRepo = template;
            _daycloseTemplateRepo = daycloseTemplateRepo;
        }

        public async Task<IdentityResult> CreateSync(Template template)
        {
            //  if the New Addition
            if (template.Id == 0)
            {
                if (_templateRepo.GetAll().Any(a => a.Name.Equals(template.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _templateRepo.InsertAndGetIdAsync(template);
                return IdentityResult.Success;

            }
            else
            {
                List<Template> lst = _templateRepo.GetAll().Where(a => a.Name.Equals(template.Name) && a.Id != template.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

        public async Task<IdentityResult> CreateDayCloseTemplateSync(DayCloseTemplate dayclosetemplate)
        {
            //  if the New Addition
            if (dayclosetemplate.Id == 0)
            {
                if (_daycloseTemplateRepo.GetAll().Any(a => a.TableName.Equals(dayclosetemplate.TableName) && DbFunctions.TruncateTime(a.ClosedDate) == DbFunctions.TruncateTime(dayclosetemplate.ClosedDate) && dayclosetemplate.LocationRefId== a.LocationRefId) )
                {
                    string[] strArrays = { L("DayCloseTableNameAlreadyExists",dayclosetemplate.TableName,
                        dayclosetemplate.ClosedDate.ToString("dd-MMM-yy")) };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _daycloseTemplateRepo.InsertAndGetIdAsync(dayclosetemplate);
                return IdentityResult.Success;

            }
            else
            {
                List<DayCloseTemplate> lst = _daycloseTemplateRepo.GetAll().Where(a => a.TableName.Equals(dayclosetemplate.TableName) && DbFunctions.TruncateTime(a.ClosedDate) == DbFunctions.TruncateTime(dayclosetemplate.ClosedDate) && dayclosetemplate.LocationRefId == a.LocationRefId && a.Id != dayclosetemplate.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("DayCloseTableNameAlreadyExists",dayclosetemplate.TableName,
                        dayclosetemplate.ClosedDate.ToString("dd-MMM-yy")) };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }
    }
}