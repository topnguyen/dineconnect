﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class TaxManager : DineConnectServiceBase, ITaxManager, ITransientDependency
    {

        private readonly IRepository<Tax> _taxRepo;

        public TaxManager(IRepository<Tax> tax)
        {
            _taxRepo = tax;
        }

        public async Task<IdentityResult> CreateSync(Tax tax)
        {
            //  if the New Addition
            if (tax.Id == 0)
            {
                if (_taxRepo.GetAll().Any(a => a.TaxName.Equals(tax.TaxName)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _taxRepo.InsertAndGetIdAsync(tax);
                return IdentityResult.Success;

            }
            else
            {
                List<Tax> lst = _taxRepo.GetAll().Where(a => a.TaxName.Equals(tax.TaxName) && a.Id != tax.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

