﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class CustomerManager : DineConnectServiceBase, ICustomerManager, ITransientDependency
    {

        private readonly IRepository<Customer> _customerRepo;

        public CustomerManager(IRepository<Customer> customer)
        {
            _customerRepo = customer;
        }

        public async Task<IdentityResult> CreateSync(Customer customer)
        {
            //  if the New Addition
            if (customer.Id == 0)
            {
                if (_customerRepo.GetAll().Any(a => a.CustomerName.Equals(customer.CustomerName)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _customerRepo.InsertAndGetIdAsync(customer);
                return IdentityResult.Success;

            }
            else
            {
                List<Customer> lst = _customerRepo.GetAll().Where(a => a.CustomerName.Equals(customer.CustomerName) && a.Id != customer.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

