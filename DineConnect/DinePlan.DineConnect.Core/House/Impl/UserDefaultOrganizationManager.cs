﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class UserDefaultOrganizationManager : DineConnectServiceBase, IUserDefaultOrganizationManager, ITransientDependency
    {

        private readonly IRepository<UserDefaultOrganization> _userDefaultOrganizationRepo;

        public UserDefaultOrganizationManager(IRepository<UserDefaultOrganization> userDefaultOrganization)
        {
            _userDefaultOrganizationRepo = userDefaultOrganization;
        }

        public async Task<IdentityResult> CreateSync(UserDefaultOrganization userDefaultOrganization)
        {
            //  if the New Addition
            if (userDefaultOrganization.Id == 0)
            {
                if (_userDefaultOrganizationRepo.GetAll().Any(a => a.Id.Equals(userDefaultOrganization.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _userDefaultOrganizationRepo.InsertAndGetIdAsync(userDefaultOrganization);
                return IdentityResult.Success;

            }
            else
            {
                List<UserDefaultOrganization> lst = _userDefaultOrganizationRepo.GetAll().Where(a => a.Id.Equals(userDefaultOrganization.Id) && a.Id != userDefaultOrganization.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
