﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class MaterialLedgerManager : DineConnectServiceBase, IMaterialLedgerManager, ITransientDependency
    {

        private readonly IRepository<MaterialLedger> _materialLedgerRepo;

        public MaterialLedgerManager(IRepository<MaterialLedger> materialLedger)
        {
            _materialLedgerRepo = materialLedger;
        }

        public async Task<IdentityResult> CreateSync(MaterialLedger materialLedger)
        {
            //  if the New Addition
            if (materialLedger.Id == 0)
            {
                if (_materialLedgerRepo.GetAll().Any(a => a.Id.Equals(materialLedger.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _materialLedgerRepo.InsertAndGetIdAsync(materialLedger);
                return IdentityResult.Success;

            }
            else
            {
                List<MaterialLedger> lst = _materialLedgerRepo.GetAll().Where(a => a.Id.Equals(materialLedger.Id) && a.Id != materialLedger.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

