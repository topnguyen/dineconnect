﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class MaterialBrandsLinkManager : DineConnectServiceBase, IMaterialBrandsLinkManager, ITransientDependency
    {

        private readonly IRepository<MaterialBrandsLink> _materialBrandsLinkRepo;

        public MaterialBrandsLinkManager(IRepository<MaterialBrandsLink> materialBrandsLink)
        {
            _materialBrandsLinkRepo = materialBrandsLink;
        }

        public async Task<IdentityResult> CreateSync(MaterialBrandsLink materialBrandsLink)
        {
            //  if the New Addition
            if (materialBrandsLink.Id == 0)
            {
                if (_materialBrandsLinkRepo.GetAll().Any(a => a.MaterialRefId == materialBrandsLink.MaterialRefId && a.BrandRefId == materialBrandsLink.BrandRefId))
                {
                    string[] strArrays = { L("SameDataAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _materialBrandsLinkRepo.InsertAndGetIdAsync(materialBrandsLink);
                return IdentityResult.Success;

            }
            else
            {
                List<MaterialBrandsLink> lst = _materialBrandsLinkRepo.GetAll().Where(a => a.MaterialRefId == materialBrandsLink.MaterialRefId && a.BrandRefId == materialBrandsLink.BrandRefId && a.Id != materialBrandsLink.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("SameDataAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}