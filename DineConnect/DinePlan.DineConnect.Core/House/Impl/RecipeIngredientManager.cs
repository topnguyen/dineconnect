﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class RecipeIngredientManager : DineConnectServiceBase, IRecipeIngredientManager, ITransientDependency
    {

        private readonly IRepository<RecipeIngredient> _recipeIngredientRepo;

        public RecipeIngredientManager(IRepository<RecipeIngredient> recipeIngredient)
        {
            _recipeIngredientRepo = recipeIngredient;
        }

        public async Task<IdentityResult> CreateSync(RecipeIngredient recipeIngredient)
        {
            //  if the New Addition
            if (recipeIngredient.Id == 0)
            {
                if (_recipeIngredientRepo.GetAll().Any(a => a.RecipeRefId == recipeIngredient.RecipeRefId && a.MaterialRefId == recipeIngredient.MaterialRefId))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _recipeIngredientRepo.InsertAndGetIdAsync(recipeIngredient);
                return IdentityResult.Success;

            }
            else
            {
                List<RecipeIngredient> lst = _recipeIngredientRepo.GetAll().Where(a => a.RecipeRefId == recipeIngredient.RecipeRefId && a.MaterialRefId == recipeIngredient.MaterialRefId && a.Id != recipeIngredient.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
