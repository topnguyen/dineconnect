﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class TaxTemplateMappingManager : DineConnectServiceBase, ITaxTemplateMappingManager, ITransientDependency
    {

        private readonly IRepository<TaxTemplateMapping> _taxTemplateMappingRepo;

        public TaxTemplateMappingManager(IRepository<TaxTemplateMapping> taxTemplateMapping)
        {
            _taxTemplateMappingRepo = taxTemplateMapping;
        }

        public async Task<IdentityResult> CreateSync(TaxTemplateMapping taxTemplateMapping)
        {
            //  if the New Addition
            if (taxTemplateMapping.Id == 0)
            {
                if (_taxTemplateMappingRepo.GetAll().Any(a => a.Id.Equals(taxTemplateMapping.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _taxTemplateMappingRepo.InsertAndGetIdAsync(taxTemplateMapping);
                return IdentityResult.Success;

            }
            else
            {
                List<TaxTemplateMapping> lst = _taxTemplateMappingRepo.GetAll().Where(a => a.Id.Equals(taxTemplateMapping.Id) && a.Id != taxTemplateMapping.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
