﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.House.Document;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class UploadDocumentManager : DineConnectServiceBase, IUploadDocumentManager, ITransientDependency
    {

        private readonly IRepository<UploadDocument> _uploadDocumentRepo;

        public UploadDocumentManager(IRepository<UploadDocument> uploadDocument)
        {
            _uploadDocumentRepo = uploadDocument;
        }

        public async Task<IdentityResult> CreateOrUpdateSync(UploadDocument uploadDocument)
        {

            if (uploadDocument.Id == 0)
            {
                await _uploadDocumentRepo.InsertAndGetIdAsync(uploadDocument);
            }
            return IdentityResult.Success;

        }
       
    }
}
