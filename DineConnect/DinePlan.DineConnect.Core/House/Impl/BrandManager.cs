﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;



namespace DinePlan.DineConnect.House.Impl
{
    public class BrandManager : DineConnectServiceBase, IBrandManager, ITransientDependency
    {
        private readonly IRepository<Brand> _brandRepo;

        public BrandManager(IRepository<Brand> brand)
        {
            _brandRepo = brand;
        }

        public async Task<IdentityResult> CreateSync(Brand brand)
        {
            if (_brandRepo.GetAll().Any(a => a.Name.Equals(brand.Name)))
            {
                string[] strArrays = {L("NameAlreadyExists")};
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }
            await _brandRepo.InsertAndGetIdAsync(brand);
            return IdentityResult.Success;
        }
    }
}