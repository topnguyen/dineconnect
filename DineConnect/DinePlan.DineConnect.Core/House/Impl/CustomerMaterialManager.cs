﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class CustomerMaterialManager : DineConnectServiceBase, ICustomerMaterialManager, ITransientDependency
    {

        private readonly IRepository<CustomerMaterial> _customerMaterialRepo;

        public CustomerMaterialManager(IRepository<CustomerMaterial> customerMaterial)
        {
            _customerMaterialRepo = customerMaterial;
        }

        public async Task<IdentityResult> CreateSync(CustomerMaterial customerMaterial)
        {
            //  if the New Addition
            if (customerMaterial.Id == 0)
            {
                if (_customerMaterialRepo.GetAll().Any(a => a.Id.Equals(customerMaterial.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _customerMaterialRepo.InsertAndGetIdAsync(customerMaterial);
                return IdentityResult.Success;

            }
            else
            {
                List<CustomerMaterial> lst = _customerMaterialRepo.GetAll().Where(a => a.Id.Equals(customerMaterial.Id) && a.Id != customerMaterial.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

