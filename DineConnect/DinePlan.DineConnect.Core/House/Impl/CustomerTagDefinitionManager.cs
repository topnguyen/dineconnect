﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class CustomerTagDefinitionManager : DineConnectServiceBase, ICustomerTagDefinitionManager, ITransientDependency
    {

        private readonly IRepository<CustomerTagDefinition> _customerTagDefinitionRepo;

        public CustomerTagDefinitionManager(IRepository<CustomerTagDefinition> customerTagDefinition)
        {
            _customerTagDefinitionRepo = customerTagDefinition;
        }

        public async Task<IdentityResult> CreateSync(CustomerTagDefinition customerTagDefinition)
        {
            //  if the New Addition
            if (customerTagDefinition.Id == 0)
            {
                if (_customerTagDefinitionRepo.GetAll().Any(a => a.TagCode.Equals(customerTagDefinition.TagCode)))
                {
                    string[] strArrays = { L("CodeAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                if (_customerTagDefinitionRepo.GetAll().Any(a => a.TagName.Equals(customerTagDefinition.TagName)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _customerTagDefinitionRepo.InsertAndGetIdAsync(customerTagDefinition);
                return IdentityResult.Success;

            }
            else
            {
                List<CustomerTagDefinition> lst = _customerTagDefinitionRepo.GetAll().Where(a => a.TagCode.Equals(customerTagDefinition.TagCode) && a.Id != customerTagDefinition.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("CodeAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }

                List<CustomerTagDefinition> lst2 = _customerTagDefinitionRepo.GetAll().Where(a => a.TagName.Equals(customerTagDefinition.TagName) && a.Id != customerTagDefinition.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
