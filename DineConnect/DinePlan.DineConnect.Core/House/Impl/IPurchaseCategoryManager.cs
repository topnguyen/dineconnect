﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
	public interface IPurchaseCategoryManager
	{
		Task<IdentityResult> CreateSync(PurchaseCategory purchaseCategory);
	}
}
