﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class RecipeGroupManager : DineConnectServiceBase, IRecipeGroupManager, ITransientDependency
    {

        private readonly IRepository<RecipeGroup> _recipeGroupRepo;

        public RecipeGroupManager(IRepository<RecipeGroup> recipeGroup)
        {
            _recipeGroupRepo = recipeGroup;
        }

        public async Task<IdentityResult> CreateSync(RecipeGroup recipeGroup)
        {
            //  if the New Addition
            if (recipeGroup.Id == 0)
            {
                if (_recipeGroupRepo.GetAll().Any(a => a.RecipeGroupName.Equals(recipeGroup.RecipeGroupName)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                if (_recipeGroupRepo.GetAll().Any(a => a.RecipeGroupShortName.Equals(recipeGroup.RecipeGroupShortName)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _recipeGroupRepo.InsertAndGetIdAsync(recipeGroup);
                return IdentityResult.Success;

            }
            else
            {
                List<RecipeGroup> lst = _recipeGroupRepo.GetAll().Where(a => a.RecipeGroupName.Equals(recipeGroup.RecipeGroupName) && a.Id != recipeGroup.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                List<RecipeGroup> lst1 = _recipeGroupRepo.GetAll().Where(a => a.RecipeGroupShortName.Equals(recipeGroup.RecipeGroupShortName) && a.Id != recipeGroup.Id).ToList();
                if (lst1.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}