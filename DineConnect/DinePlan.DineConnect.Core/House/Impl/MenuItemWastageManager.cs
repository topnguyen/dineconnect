﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class MenuItemWastageManager : DineConnectServiceBase, IMenuItemWastageManager, ITransientDependency
    {

        private readonly IRepository<MenuItemWastage> _menuItemWastageRepo;

        public MenuItemWastageManager(IRepository<MenuItemWastage> menuItemWastage)
        {
            _menuItemWastageRepo = menuItemWastage;
        }

        public async Task<IdentityResult> CreateSync(MenuItemWastage menuItemWastage)
        {
            //  if the New Addition
            if (menuItemWastage.Id == 0)
            {
                if (_menuItemWastageRepo.GetAll().Any(a => a.Id.Equals(menuItemWastage.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _menuItemWastageRepo.InsertAndGetIdAsync(menuItemWastage);
                return IdentityResult.Success;

            }
            else
            {
                List<MenuItemWastage> lst = _menuItemWastageRepo.GetAll().Where(a => a.Id.Equals(menuItemWastage.Id) && a.Id != menuItemWastage.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
