﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class ProductionManager : DineConnectServiceBase, IProductionManager, ITransientDependency
    {

        private readonly IRepository<Production> _productionRepo;

        public ProductionManager(IRepository<Production> production)
        {
            _productionRepo = production;
        }

        public async Task<IdentityResult> CreateSync(Production production)
        {
            //  if the New Addition
            if (production.Id == 0)
            {
                if (_productionRepo.GetAll().Any(a => a.Id.Equals(production.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _productionRepo.InsertAndGetIdAsync(production);
                return IdentityResult.Success;

            }
            else
            {
                List<Production> lst = _productionRepo.GetAll().Where(a => a.Id.Equals(production.Id) && a.Id != production.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
