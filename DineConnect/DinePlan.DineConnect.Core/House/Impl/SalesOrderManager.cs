﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class SalesOrderManager : DineConnectServiceBase, ISalesOrderManager, ITransientDependency
    {

        private readonly IRepository<SalesOrder> _salesOrderRepo;

        public SalesOrderManager(IRepository<SalesOrder> salesOrder)
        {
            _salesOrderRepo = salesOrder;
        }

        public async Task<IdentityResult> CreateSync(SalesOrder salesOrder)
        {
            //  if the New Addition
            if (salesOrder.Id == 0)
            {
                if (_salesOrderRepo.GetAll().Any(a => a.Id.Equals(salesOrder.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _salesOrderRepo.InsertAndGetIdAsync(salesOrder);
                return IdentityResult.Success;

            }
            else
            {
                List<SalesOrder> lst = _salesOrderRepo.GetAll().Where(a => a.Id.Equals(salesOrder.Id) && a.Id != salesOrder.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
