﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class UnitManager : DineConnectServiceBase, IUnitManager, ITransientDependency
    {

        private readonly IRepository<Unit> _unitRepo;

        public UnitManager(IRepository<Unit> unit)
        {
            _unitRepo = unit;
        }

        public async Task<IdentityResult> CreateSync(Unit unit)
        {
            //  if the New Addition
            if (unit.Id == 0)
            {
                if (_unitRepo.GetAll().Any(a => a.Name.Equals(unit.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _unitRepo.InsertAndGetIdAsync(unit);
                return IdentityResult.Success;

            }
            else
            {
                List<Unit> lst = _unitRepo.GetAll().Where(a => a.Name.Equals(unit.Name) && a.Id != unit.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}