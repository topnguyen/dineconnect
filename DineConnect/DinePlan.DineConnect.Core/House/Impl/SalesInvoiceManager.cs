﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class SalesInvoiceManager : DineConnectServiceBase, ISalesInvoiceManager, ITransientDependency
    {

        private readonly IRepository<SalesInvoice> _salesinvoiceRepo;

        public SalesInvoiceManager(IRepository<SalesInvoice> invoice)
        {
            _salesinvoiceRepo = invoice;
        }

        public async Task<IdentityResult> CreateSync(SalesInvoice invoice)
        {
            //  if the New Addition
            if (invoice.Id == 0)
            {
                if (_salesinvoiceRepo.GetAll().Any(a => a.Id.Equals(invoice.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _salesinvoiceRepo.InsertAndGetIdAsync(invoice);
                return IdentityResult.Success;

            }
            else
            {
                List<SalesInvoice> lst = _salesinvoiceRepo.GetAll().Where(a => a.Id.Equals(invoice.Id) && a.Id != invoice.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}