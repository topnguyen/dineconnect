﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class ProductRecipesLinkManager : DineConnectServiceBase, IProductRecipesLinkManager, ITransientDependency
    {

        private readonly IRepository<ProductRecipesLink> _productRecipesLinkRepo;

        public ProductRecipesLinkManager(IRepository<ProductRecipesLink> productRecipesLink)
        {
            _productRecipesLinkRepo = productRecipesLink;
        }

        public async Task<IdentityResult> CreateSync(ProductRecipesLink productRecipesLink)
        {
            //  if the New Addition
            if (productRecipesLink.Id == 0)
            {
                if (_productRecipesLinkRepo.GetAll().Any(a => a.PosRefId == productRecipesLink.PosRefId && a.RecipeRefId == productRecipesLink.RecipeRefId))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _productRecipesLinkRepo.InsertAndGetIdAsync(productRecipesLink);
                return IdentityResult.Success;

            }
            else
            {
                List<ProductRecipesLink> lst = _productRecipesLinkRepo.GetAll().Where(a => a.PosRefId == productRecipesLink.PosRefId && a.RecipeRefId == productRecipesLink.RecipeRefId && a.Id != productRecipesLink.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}