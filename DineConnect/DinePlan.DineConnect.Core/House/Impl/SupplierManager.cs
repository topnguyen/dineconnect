﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class SupplierManager : DineConnectServiceBase, ISupplierManager, ITransientDependency
    {

        private readonly IRepository<Supplier> _supplierRepo;

        public SupplierManager(IRepository<Supplier> supplier)
        {
            _supplierRepo = supplier;
        }

        public async Task<IdentityResult> CreateSync(Supplier supplier)
        {
            if (supplier.Id == 0)
            {
                var existsSupplier = await _supplierRepo.FirstOrDefaultAsync(a => a.SupplierName.Equals(supplier.SupplierName));
                if (existsSupplier != null)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }

                if (!string.IsNullOrEmpty(supplier.SupplierCode))
                {
                    existsSupplier = await _supplierRepo.FirstOrDefaultAsync(a => a.SupplierCode.Equals(supplier.SupplierCode));
                    if (existsSupplier != null)
                    {
                        string[] strArrays = { string.Format(L("DuplicateCodeExists_f"), supplier.SupplierName) };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }
                }

                //if (!string.IsNullOrEmpty(supplier.TaxRegistrationNumber))
                //{
                //    existsSupplier = await _supplierRepo.FirstOrDefaultAsync(a => a.TaxRegistrationNumber.Equals(supplier.TaxRegistrationNumber));
                //    if (existsSupplier != null)
                //    {
                //        string[] strArrays = { L("TaxRegnNumberAlreadyExists") };
                //        var success = AbpIdentityResult.Failed(strArrays);
                //        return success;
                //    }
                //}

                await _supplierRepo.InsertAndGetIdAsync(supplier);
                return IdentityResult.Success;
            }
            else
            {
                //List<Supplier> lst = _supplierRepo.GetAll().Where(a => a.SupplierName.Equals(supplier.SupplierName) && a.Id != supplier.Id).ToList();
                var existsSupplier = await _supplierRepo.FirstOrDefaultAsync(a => a.Id != supplier.Id && a.SupplierName.Equals(supplier.SupplierName));
                if (existsSupplier!=null)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }

                if (!string.IsNullOrEmpty(supplier.SupplierCode))
                {
                    existsSupplier = await _supplierRepo.FirstOrDefaultAsync(a => a.SupplierCode.Equals(supplier.SupplierCode) && a.Id!=supplier.Id);
                    if (existsSupplier != null)
                    {
                        string[] strArrays = { string.Format(L("DuplicateCodeExists_f"), supplier.SupplierCode) };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }
                }

                if (!string.IsNullOrEmpty(supplier.TaxRegistrationNumber))
                {
                    existsSupplier = await _supplierRepo.FirstOrDefaultAsync(a => a.TaxRegistrationNumber.Equals(supplier.TaxRegistrationNumber) && a.Id!=supplier.Id);
                    if (existsSupplier != null)
                    {
                        string[] strArrays = { L("TaxRegnNumberAlreadyExists") };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }
                }
                return IdentityResult.Success;
            }
        }

    }
}
