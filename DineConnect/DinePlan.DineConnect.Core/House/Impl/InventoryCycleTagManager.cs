﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
	public class InventoryCycleTagManager : DineConnectServiceBase, IInventoryCycleTagManager, ITransientDependency
	{

		private readonly IRepository<InventoryCycleTag> _inventoryCycleTagRepo;

		public InventoryCycleTagManager(IRepository<InventoryCycleTag> inventoryCycleTag)
		{
			_inventoryCycleTagRepo = inventoryCycleTag;
		}

		public async Task<IdentityResult> CreateSync(InventoryCycleTag inventoryCycleTag)
		{
			//  if the New Addition
			if (inventoryCycleTag.Id == 0)
			{
				if (_inventoryCycleTagRepo.GetAll().Any(a => a.InventoryCycleTagCode.Equals(inventoryCycleTag.InventoryCycleTagCode)))
				{
					string[] strArrays = { L("NameAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				await _inventoryCycleTagRepo.InsertAndGetIdAsync(inventoryCycleTag);
				return IdentityResult.Success;

			}
			else
			{
				List<InventoryCycleTag> lst = _inventoryCycleTagRepo.GetAll().Where(a => a.InventoryCycleTagCode.Equals(inventoryCycleTag.InventoryCycleTagCode) && a.Id != inventoryCycleTag.Id).ToList();
				if (lst.Count > 0)
				{
					string[] strArrays = { L("NameAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				return IdentityResult.Success;
			}
		}

	}
}

