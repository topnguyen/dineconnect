﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class MaterialMenuMappingManager : DineConnectServiceBase, IMaterialMenuMappingManager, ITransientDependency
    {

        private readonly IRepository<MaterialMenuMapping> _materialMenuMappingRepo;

        public MaterialMenuMappingManager(IRepository<MaterialMenuMapping> materialMenuMapping)
        {
            _materialMenuMappingRepo = materialMenuMapping;
        }

        public async Task<IdentityResult> CreateSync(MaterialMenuMapping materialMenuMapping)
        {
            //  if the New Addition
            if (materialMenuMapping.Id == 0)
            {
                if (_materialMenuMappingRepo.GetAll().Any(a => a.PosMenuPortionRefId== materialMenuMapping.PosMenuPortionRefId && a.MaterialRefId == materialMenuMapping.MaterialRefId && a.LocationRefId==materialMenuMapping.LocationRefId))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _materialMenuMappingRepo.InsertAndGetIdAsync(materialMenuMapping);
                return IdentityResult.Success;

            }
            else
            {
                List<MaterialMenuMapping> lst = _materialMenuMappingRepo.GetAll().Where(a => a.PosMenuPortionRefId == materialMenuMapping.PosMenuPortionRefId && a.MaterialRefId == materialMenuMapping.MaterialRefId && a.LocationRefId == materialMenuMapping.LocationRefId && a.Id != materialMenuMapping.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

