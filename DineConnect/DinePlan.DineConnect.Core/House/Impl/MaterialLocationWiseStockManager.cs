﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class MaterialLocationWiseStockManager : DineConnectServiceBase, IMaterialLocationWiseStockManager, ITransientDependency
    {

        private readonly IRepository<MaterialLocationWiseStock> _materialLocationWiseStockRepo;

        public MaterialLocationWiseStockManager(IRepository<MaterialLocationWiseStock> materialLocationWiseStock)
        {
            _materialLocationWiseStockRepo = materialLocationWiseStock;
        }

        public async Task<IdentityResult> CreateSync(MaterialLocationWiseStock materialLocationWiseStock)
        {
            //  if the New Addition
            if (materialLocationWiseStock.Id == 0)
            {
                if (_materialLocationWiseStockRepo.GetAll().Any(a => a.MaterialRefId == materialLocationWiseStock.MaterialRefId && a.LocationRefId == materialLocationWiseStock.LocationRefId))
                {
                    string[] strArrays = { L("SameDataAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _materialLocationWiseStockRepo.InsertAndGetIdAsync(materialLocationWiseStock);
                return IdentityResult.Success;

            }
            else
            {
                List<MaterialLocationWiseStock> lst = _materialLocationWiseStockRepo.GetAll().Where(a => a.MaterialRefId == materialLocationWiseStock.MaterialRefId && a.LocationRefId == materialLocationWiseStock.LocationRefId && a.Id != materialLocationWiseStock.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("SameDataAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}