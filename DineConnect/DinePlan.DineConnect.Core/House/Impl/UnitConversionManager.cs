﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
  
    public class UnitConversionManager : DineConnectServiceBase, IUnitConversionManager, ITransientDependency
    {

        private readonly IRepository<UnitConversion> _unitConversionRepo;

        public UnitConversionManager(IRepository<UnitConversion> unitConversion)
        {
            _unitConversionRepo = unitConversion;
        }

        public async Task<IdentityResult> CreateSync(UnitConversion unitConversion)
        {
            //  if the New Addition
            if (unitConversion.Id == 0)
            {
                if (_unitConversionRepo.GetAll().Any(a => a.BaseUnitId == unitConversion.BaseUnitId && a.RefUnitId == unitConversion.RefUnitId && a.MaterialRefId==unitConversion.MaterialRefId))
                {
                    string[] strArrays = { L("SameDataAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _unitConversionRepo.InsertAndGetIdAsync(unitConversion);
                return IdentityResult.Success;

            }
            else
            {
                List<UnitConversion> lst = _unitConversionRepo.GetAll().Where(a => a.BaseUnitId == unitConversion.BaseUnitId && a.RefUnitId == unitConversion.RefUnitId && a.MaterialRefId == unitConversion.MaterialRefId && a.Id != unitConversion.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("SameDataAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
