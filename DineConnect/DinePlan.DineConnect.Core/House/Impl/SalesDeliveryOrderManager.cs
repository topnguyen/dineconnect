﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class SalesDeliveryOrderManager : DineConnectServiceBase, ISalesDeliveryOrderManager, ITransientDependency
    {

        private readonly IRepository<SalesDeliveryOrder> _isalesDeliveryOrderRepo;

        public SalesDeliveryOrderManager(IRepository<SalesDeliveryOrder> isalesDeliveryOrder)
        {
            _isalesDeliveryOrderRepo = isalesDeliveryOrder;
        }

        public async Task<IdentityResult> CreateSync(SalesDeliveryOrder isalesDeliveryOrder)
        {
            //  if the New Addition
            if (isalesDeliveryOrder.Id == 0)
            {
                //  @@Pending Date And Supplier and SupplierReference DC Number
                //  @@Pending Date Comparision Pending
                if (_isalesDeliveryOrderRepo.GetAll().Any(a => a.Id.Equals(isalesDeliveryOrder.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _isalesDeliveryOrderRepo.InsertAndGetIdAsync(isalesDeliveryOrder);
                return IdentityResult.Success;

            }
            else
            {
                List<SalesDeliveryOrder> lst = _isalesDeliveryOrderRepo.GetAll().Where(a => a.Id.Equals(isalesDeliveryOrder.Id) && a.Id != isalesDeliveryOrder.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

