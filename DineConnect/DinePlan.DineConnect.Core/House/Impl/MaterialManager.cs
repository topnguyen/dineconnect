﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class MaterialManager : DineConnectServiceBase, IMaterialManager, ITransientDependency
    {

        private readonly IRepository<Material> _materialRepo;

        public MaterialManager(IRepository<Material> material)
        {
            _materialRepo = material;
        }

        public async Task<IdentityResult> CreateSync(Material material)
        {
            //  if the New Addition
            if (material.Id == 0)
            {
                var existMaterial = await _materialRepo.FirstOrDefaultAsync(a => a.MaterialName.Equals(material.MaterialName));

                //if (_materialRepo.GetAll().Any(a => a.MaterialName.Equals(material.MaterialName)))
                if (existMaterial!=null)
                {
                    string[] strArrays = { L("NameAlreadyExists") , existMaterial.Id.ToString(), existMaterial.MaterialName};
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                existMaterial = await _materialRepo.FirstOrDefaultAsync(a => a.MaterialPetName.Equals(material.MaterialPetName));
                //if (_materialRepo.GetAll().Any(a => a.MaterialPetName.Equals(material.MaterialPetName)))
                if (existMaterial != null)
                {
                    string[] strArrays = { L("AliasNameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }

                if (material.Barcode != null && material.Barcode != "")
                {
                    var barcodeAlreadyExists = _materialRepo.FirstOrDefault(t => t.Barcode == material.Barcode);
                    if (barcodeAlreadyExists != null)
                    {
                        string[] strArrays = { L("BarcodeAlreadyExists", barcodeAlreadyExists.MaterialName) };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }
                }

                await _materialRepo.InsertAndGetIdAsync(material);
                return IdentityResult.Success;

            }
            else
            {
                List<Material> lst = _materialRepo.GetAll().Where(a => a.MaterialName.Equals(material.MaterialName) && a.Id != material.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists"), lst[0].Id.ToString(), lst[0].MaterialName };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                List<Material> lst1 = _materialRepo.GetAll().Where(a => a.MaterialPetName.Equals(material.MaterialPetName) && a.Id != material.Id).ToList();
                if (lst1.Count > 0)
                {
                    string[] strArrays = { L("ShortNameAlreadyExists"), lst[0].Id.ToString(), lst[0].MaterialPetName};
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                if (material.Barcode != null && material.Barcode != "")
                {
                    var barcodeAlreadyExists = _materialRepo.FirstOrDefault(t => t.Barcode == material.Barcode && t.Id != material.Id);
                    if (barcodeAlreadyExists != null)
                    {
                        string[] strArrays = { L("BarcodeAlreadyExists", barcodeAlreadyExists.MaterialName) };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }
                }
                return IdentityResult.Success;
            }
        }

    }
}
