﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class YieldManager : DineConnectServiceBase, IYieldManager, ITransientDependency
    {

        private readonly IRepository<Yield> _yieldRepo;

        public YieldManager(IRepository<Yield> yield)
        {
            _yieldRepo = yield;
        }

        public async Task<IdentityResult> CreateSync(Yield yield)
        {
            //  if the New Addition
            if (yield.Id == 0)
            {
                if (_yieldRepo.GetAll().Any(a => a.Id.Equals(yield.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _yieldRepo.InsertAndGetIdAsync(yield);
                return IdentityResult.Success;

            }
            else
            {
                List<Yield> lst = _yieldRepo.GetAll().Where(a => a.Id.Equals(yield.Id) && a.Id != yield.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
