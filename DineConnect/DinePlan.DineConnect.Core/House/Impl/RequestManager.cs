﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class RequestManager : DineConnectServiceBase, IRequestManager, ITransientDependency
    {

        private readonly IRepository<Request> _requestRepo;

        public RequestManager(IRepository<Request> request)
        {
            _requestRepo = request;
        }

        public async Task<IdentityResult> CreateSync(Request request)
        {
            //  if the New Addition
            if (request.Id == 0)
            {
                if (_requestRepo.GetAll().Any(a => a.RequestSlipNumber.Equals(request.RequestSlipNumber) && a.CompletedStatus == false ))
                {
                    string[] strArrays = { L("RequestSlipNumberAlreadyExists",request.RequestTime.Date.ToString("D")) };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _requestRepo.InsertAndGetIdAsync(request);
                return IdentityResult.Success;

            }
            else
            {
                List<Request> lst = _requestRepo.GetAll().Where(a => a.RequestSlipNumber.Equals(request.RequestSlipNumber) && a.CompletedStatus == false && a.Id != request.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("RequestSlipNumberAlreadyExists", request.RequestTime.Date.ToString("D")) };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

