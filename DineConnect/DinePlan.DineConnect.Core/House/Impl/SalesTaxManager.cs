﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class SalesTaxManager : DineConnectServiceBase, ISalesTaxManager, ITransientDependency
    {

        private readonly IRepository<SalesTax> _salestaxRepo;

        public SalesTaxManager(IRepository<SalesTax> tax)
        {
            _salestaxRepo = tax;
        }

        public async Task<IdentityResult> CreateSync(SalesTax tax)
        {
            //  if the New Addition
            if (tax.Id == 0)
            {
                if (_salestaxRepo.GetAll().Any(a => a.TaxName.Equals(tax.TaxName)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _salestaxRepo.InsertAndGetIdAsync(tax);
                return IdentityResult.Success;

            }
            else
            {
                List<SalesTax> lst = _salestaxRepo.GetAll().Where(a => a.TaxName.Equals(tax.TaxName) && a.Id != tax.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

