﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class SupplierContactManager : DineConnectServiceBase, ISupplierContactManager, ITransientDependency
    {

        private readonly IRepository<SupplierContact> _supplierContactRepo;

        public SupplierContactManager(IRepository<SupplierContact> supplierContact)
        {
            _supplierContactRepo = supplierContact;
        }

        public async Task<IdentityResult> CreateSync(SupplierContact supplierContact)
        {
            //  if the New Addition
            if (supplierContact.Id == 0)
            {
                if (_supplierContactRepo.GetAll().Any(a => a.SupplierRefId == supplierContact.SupplierRefId && a.ContactPersonName == supplierContact.ContactPersonName ))
                {
                    string[] strArrays = { L("SameDataAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _supplierContactRepo.InsertAndGetIdAsync(supplierContact);
                return IdentityResult.Success;

            }
            else
            {
                List<SupplierContact> lst = _supplierContactRepo.GetAll().Where(a => a.SupplierRefId == supplierContact.SupplierRefId && a.ContactPersonName == supplierContact.ContactPersonName &&  a.Id != supplierContact.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("SameDataAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
