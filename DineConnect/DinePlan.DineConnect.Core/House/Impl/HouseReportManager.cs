﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class HouseReportManager : DineConnectServiceBase, IHouseReportManager, ITransientDependency
    {

        private readonly IRepository<HouseReport> _houseReportRepo;

        public HouseReportManager(IRepository<HouseReport> houseReport)
        {
            _houseReportRepo = houseReport;
        }

        public async Task<IdentityResult> CreateSync(HouseReport houseReport)
        {
            //  if the New Addition
            if (houseReport.Id == 0)
            {
                if (_houseReportRepo.GetAll().Any(a => a.Id.Equals(houseReport.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _houseReportRepo.InsertAndGetIdAsync(houseReport);
                return IdentityResult.Success;

            }
            else
            {
                List<HouseReport> lst = _houseReportRepo.GetAll().Where(a => a.Id.Equals(houseReport.Id) && a.Id != houseReport.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

