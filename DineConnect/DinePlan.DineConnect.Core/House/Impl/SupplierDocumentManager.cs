﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class SupplierDocumentManager : DineConnectServiceBase, ISupplierDocumentManager, ITransientDependency
    {

        private readonly IRepository<SupplierDocument> _supplierDocumentRepo;

        public SupplierDocumentManager(IRepository<SupplierDocument> supplierDocument)
        {
            _supplierDocumentRepo = supplierDocument;
        }

        public async Task<IdentityResult> CreateSync(SupplierDocument supplierDocument)
        {
            //  if the New Addition
            if (supplierDocument.Id == 0)
            {
                if (_supplierDocumentRepo.GetAll().Any(a => a.Id.Equals(supplierDocument.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _supplierDocumentRepo.InsertAndGetIdAsync(supplierDocument);
                return IdentityResult.Success;

            }
            else
            {
                List<SupplierDocument> lst = _supplierDocumentRepo.GetAll().Where(a => a.Id.Equals(supplierDocument.Id) && a.Id != supplierDocument.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
