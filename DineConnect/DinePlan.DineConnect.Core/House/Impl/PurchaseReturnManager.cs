﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class PurchaseReturnManager : DineConnectServiceBase, IPurchaseReturnManager, ITransientDependency
    {

        private readonly IRepository<PurchaseReturn> _purchaseReturnRepo;

        public PurchaseReturnManager(IRepository<PurchaseReturn> purchaseReturn)
        {
            _purchaseReturnRepo = purchaseReturn;
        }

        public async Task<IdentityResult> CreateSync(PurchaseReturn purchaseReturn)
        {
            //  if the New Addition
            if (purchaseReturn.Id == 0)
            {
                if (_purchaseReturnRepo.GetAll().Any(a => a.SupplierRefId.Equals(purchaseReturn.SupplierRefId) && a.ReferenceNumber.Equals(purchaseReturn.ReferenceNumber)))
                {
                    string[] strArrays = { L("ReferenceNoAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _purchaseReturnRepo.InsertAndGetIdAsync(purchaseReturn);
                return IdentityResult.Success;

            }
            else
            {
                List<PurchaseReturn> lst = _purchaseReturnRepo.GetAll().Where(a => a.SupplierRefId.Equals(purchaseReturn.SupplierRefId) && a.ReferenceNumber.Equals(purchaseReturn.ReferenceNumber) && a.Id != purchaseReturn.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("ReferenceNoAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

