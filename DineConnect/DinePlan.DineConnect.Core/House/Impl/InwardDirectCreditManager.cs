﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class InwardDirectCreditManager : DineConnectServiceBase, IInwardDirectCreditManager, ITransientDependency
    {

        private readonly IRepository<InwardDirectCredit> _inwardDirectCreditRepo;

        public InwardDirectCreditManager(IRepository<InwardDirectCredit> inwardDirectCredit)
        {
            _inwardDirectCreditRepo = inwardDirectCredit;
        }

        public async Task<IdentityResult> CreateSync(InwardDirectCredit inwardDirectCredit)
        {
            //  if the New Addition
            if (inwardDirectCredit.Id == 0)
            {
                //  @@Pending Date And Supplier and SupplierReference DC Number
                //  @@Pending Date Comparision Pending
                if (_inwardDirectCreditRepo.GetAll().Any(a => a.Id.Equals(inwardDirectCredit.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _inwardDirectCreditRepo.InsertAndGetIdAsync(inwardDirectCredit);
                return IdentityResult.Success;

            }
            else
            {
                List<InwardDirectCredit> lst = _inwardDirectCreditRepo.GetAll().Where(a => a.Id.Equals(inwardDirectCredit.Id) && a.Id != inwardDirectCredit.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

