﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class MaterialGroupManager : DineConnectServiceBase, IMaterialGroupManager, ITransientDependency
    {

        private readonly IRepository<MaterialGroup> _materialGroupRepo;

        public MaterialGroupManager(IRepository<MaterialGroup> materialGroup)
        {
            _materialGroupRepo = materialGroup;
        }

        public async Task<IdentityResult> CreateSync(MaterialGroup materialGroup)
        {
            //  if the New Addition
            if (materialGroup.Id == 0)
            {
                if (_materialGroupRepo.GetAll().Any(a => a.MaterialGroupName.Equals(materialGroup.MaterialGroupName)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _materialGroupRepo.InsertAndGetIdAsync(materialGroup);
                return IdentityResult.Success;

            }
            else
            {
                List<MaterialGroup> lst = _materialGroupRepo.GetAll().Where(a => a.MaterialGroupName.Equals(materialGroup.MaterialGroupName) && a.Id != materialGroup.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
