﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class ManualReasonManager : DineConnectServiceBase, IManualReasonManager, ITransientDependency
    {

        private readonly IRepository<ManualReason> _manualReasonsRepo;

        public ManualReasonManager(IRepository<ManualReason> manualReasons)
        {
            _manualReasonsRepo = manualReasons;
        }

        public async Task<IdentityResult> CreateSync(ManualReason manualReasons)
        {
            //  if the New Addition
            if (manualReasons.Id == 0)
            {
                if (_manualReasonsRepo.GetAll().Any(a => a.ManualReasonName.Equals(manualReasons.ManualReasonName)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _manualReasonsRepo.InsertAndGetIdAsync(manualReasons);
                return IdentityResult.Success;

            }
            else
            {
                List<ManualReason> lst = _manualReasonsRepo.GetAll().Where(a => a.ManualReasonName.Equals(manualReasons.ManualReasonName) && a.Id != manualReasons.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

