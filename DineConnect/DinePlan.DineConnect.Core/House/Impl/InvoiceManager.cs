﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class InvoiceManager : DineConnectServiceBase, IInvoiceManager, ITransientDependency
    {

        private readonly IRepository<Invoice> _invoiceRepo;

        public InvoiceManager(IRepository<Invoice> invoice)
        {
            _invoiceRepo = invoice;
        }

        public async Task<IdentityResult> CreateSync(Invoice invoice)
        {
            //  if the New Addition
            if (invoice.Id == 0)
            {
                if (_invoiceRepo.GetAll().Any(a => a.Id.Equals(invoice.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _invoiceRepo.InsertAndGetIdAsync(invoice);
                return IdentityResult.Success;

            }
            else
            {
                List<Invoice> lst = _invoiceRepo.GetAll().Where(a => a.Id.Equals(invoice.Id) && a.Id != invoice.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}