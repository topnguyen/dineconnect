﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class SupplierMaterialManager : DineConnectServiceBase, ISupplierMaterialManager, ITransientDependency
    {

        private readonly IRepository<SupplierMaterial> _supplierMaterialRepo;

        public SupplierMaterialManager(IRepository<SupplierMaterial> supplierMaterial)
        {
            _supplierMaterialRepo = supplierMaterial;
        }

        public async Task<IdentityResult> CreateSync(SupplierMaterial supplierMaterial)
        {
            //  if the New Addition
            if (supplierMaterial.Id == 0)
            {
                if (_supplierMaterialRepo.GetAll().Any(a => a.SupplierRefId == supplierMaterial.SupplierRefId && a.MaterialRefId == supplierMaterial.MaterialRefId && a.LocationRefId==supplierMaterial.LocationRefId))
                {
                    string[] strArrays = { L("SameDataAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _supplierMaterialRepo.InsertAndGetIdAsync(supplierMaterial);
                return IdentityResult.Success;

            }
            else
            {
                List<SupplierMaterial> lst = _supplierMaterialRepo.GetAll().Where(a => a.SupplierRefId == supplierMaterial.SupplierRefId && a.MaterialRefId == supplierMaterial.MaterialRefId && a.LocationRefId == supplierMaterial.LocationRefId && a.Id != supplierMaterial.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("SameDataAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

