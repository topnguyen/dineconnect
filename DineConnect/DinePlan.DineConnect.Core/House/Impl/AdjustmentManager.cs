﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class AdjustmentManager : DineConnectServiceBase, IAdjustmentManager, ITransientDependency
    {

        private readonly IRepository<Adjustment> _adjustmentRepo;

        public AdjustmentManager(IRepository<Adjustment> adjustment)
        {
            _adjustmentRepo = adjustment;
        }

        public async Task<IdentityResult> CreateSync(Adjustment adjustment)
        {
            //  if the New Addition
            if (adjustment.Id == 0)
            {
                if (_adjustmentRepo.GetAll().Any(a => a.Id.Equals(adjustment.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _adjustmentRepo.InsertAndGetIdAsync(adjustment);
                return IdentityResult.Success;

            }
            else
            {
                List<Adjustment> lst = _adjustmentRepo.GetAll().Where(a => a.Id.Equals(adjustment.Id) && a.Id != adjustment.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

