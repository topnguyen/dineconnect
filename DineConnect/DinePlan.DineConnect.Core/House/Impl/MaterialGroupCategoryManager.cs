﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using Abp.Runtime.Session;
using Abp.Application.Services;

namespace DinePlan.DineConnect.House.Impl
{
    public class MaterialGroupCategoryManager : DineConnectServiceBase, IMaterialGroupCategoryManager, ITransientDependency, IApplicationService
    {

        private readonly IRepository<MaterialGroupCategory> _materialGroupCategoryRepo;
        private readonly IRepository<MaterialGroup> _materialGroupRepo;

        public MaterialGroupCategoryManager(IRepository<MaterialGroupCategory> materialGroupCategory, IRepository<MaterialGroup> materialGroupRepo)
        {
            _materialGroupCategoryRepo = materialGroupCategory;
            _materialGroupRepo = materialGroupRepo;
        }

        public async Task<IdentityResult> CreateSync(MaterialGroupCategory materialGroupCategory)
        {
            //  if the New Addition
            if (materialGroupCategory.Id == 0)
            {
                if (_materialGroupCategoryRepo.GetAll().Any(a => a.MaterialGroupCategoryName.Equals(materialGroupCategory.MaterialGroupCategoryName) && a.MaterialGroupRefId == materialGroupCategory.MaterialGroupRefId) )
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _materialGroupCategoryRepo.InsertAndGetIdAsync(materialGroupCategory);
                return IdentityResult.Success;

            }
            else
            {
                List<MaterialGroupCategory> lst = _materialGroupCategoryRepo.GetAll().Where(a => a.MaterialGroupCategoryName.Equals(materialGroupCategory.MaterialGroupCategoryName) && a.MaterialGroupRefId == materialGroupCategory.MaterialGroupRefId && a.Id != materialGroupCategory.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
