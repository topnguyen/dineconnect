﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class InterTransferManager : DineConnectServiceBase, IInterTransferManager, ITransientDependency
    {

        private readonly IRepository<InterTransfer> _interTransferRepo;

        public InterTransferManager(IRepository<InterTransfer> interTransfer)
        {
            _interTransferRepo = interTransfer;
        }

        public async Task<IdentityResult> CreateSync(InterTransfer interTransfer)
        {
            //  if the New Addition
            if (interTransfer.Id == 0)
            {
                if (_interTransferRepo.GetAll().Any(a => a.Id.Equals(interTransfer.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _interTransferRepo.InsertAndGetIdAsync(interTransfer);
                return IdentityResult.Success;

            }
            else
            {
                List<InterTransfer> lst = _interTransferRepo.GetAll().Where(a => a.Id.Equals(interTransfer.Id) && a.Id != interTransfer.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
