﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class LocationContactManager : DineConnectServiceBase, ILocationContactManager, ITransientDependency
    {

        private readonly IRepository<LocationContact> _locationContactRepo;

        public LocationContactManager(IRepository<LocationContact> locationContact)
        {
            _locationContactRepo = locationContact;
        }

        public async Task<IdentityResult> CreateSync(LocationContact locationContact)
        {
            //  if the New Addition
            if (locationContact.Id == 0)
            {
                if (_locationContactRepo.GetAll().Any(a => a.LocationRefId == locationContact.LocationRefId && a.ContactPersonName == locationContact.ContactPersonName ))
                {
                    string[] strArrays = { L("SameDataAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _locationContactRepo.InsertAndGetIdAsync(locationContact);
                return IdentityResult.Success;

            }
            else
            {
                List<LocationContact> lst = _locationContactRepo.GetAll().Where(a => a.LocationRefId == locationContact.LocationRefId && a.ContactPersonName == locationContact.ContactPersonName && a.Id != locationContact.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("SameDataAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

