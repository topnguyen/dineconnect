﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class ClosingStockManager : DineConnectServiceBase, IClosingStockManager, ITransientDependency
    {

        private readonly IRepository<ClosingStock> _closingStockRepo;

        public ClosingStockManager(IRepository<ClosingStock> closingStock)
        {
            _closingStockRepo = closingStock;
        }

        public async Task<IdentityResult> CreateSync(ClosingStock closingStock)
        {
            //  if the New Addition
            if (closingStock.Id == 0)
            {
                if (_closingStockRepo.GetAll().Any(a => a.Id.Equals(closingStock.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _closingStockRepo.InsertAndGetIdAsync(closingStock);
                return IdentityResult.Success;

            }
            else
            {
                List<ClosingStock> lst = _closingStockRepo.GetAll().Where(a => a.Id.Equals(closingStock.Id) && a.Id != closingStock.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}