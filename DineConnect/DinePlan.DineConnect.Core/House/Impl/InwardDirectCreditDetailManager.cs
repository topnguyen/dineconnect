﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class InwardDirectCreditDetailManager : DineConnectServiceBase, IInwardDirectCreditDetailManager, ITransientDependency
    {

        private readonly IRepository<InwardDirectCreditDetail> _inwardDirectCreditDetailRepo;

        public InwardDirectCreditDetailManager(IRepository<InwardDirectCreditDetail> inwardDirectCreditDetail)
        {
            _inwardDirectCreditDetailRepo = inwardDirectCreditDetail;
        }



        public async Task<IdentityResult> CreateSync(InwardDirectCreditDetail inwardDirectCreditDetail)
        {
            //  if the New Addition
            if (inwardDirectCreditDetail.Id == 0)
            {
                if (_inwardDirectCreditDetailRepo.GetAll().Any(a => a.Id.Equals(inwardDirectCreditDetail.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _inwardDirectCreditDetailRepo.InsertAndGetIdAsync(inwardDirectCreditDetail);
                return IdentityResult.Success;

            }
            else
            {
                List<InwardDirectCreditDetail> lst = _inwardDirectCreditDetailRepo.GetAll().Where(a => a.Id.Equals(inwardDirectCreditDetail.Id) && a.Id != inwardDirectCreditDetail.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}