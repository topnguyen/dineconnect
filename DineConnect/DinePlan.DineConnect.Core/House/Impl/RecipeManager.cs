﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class RecipeManager : DineConnectServiceBase, IRecipeManager, ITransientDependency
    {

        private readonly IRepository<Recipe> _recipeRepo;

        public RecipeManager(IRepository<Recipe> recipe)
        {
            _recipeRepo = recipe;
        }

        public async Task<IdentityResult> CreateSync(Recipe recipe)
        {
            //  if the New Addition
            if (recipe.Id == 0)
            {
                if (_recipeRepo.GetAll().Any(a => a.RecipeName.Equals(recipe.RecipeName)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _recipeRepo.InsertAndGetIdAsync(recipe);
                return IdentityResult.Success;

            }
            else
            {
                List<Recipe> lst = _recipeRepo.GetAll().Where(a => a.RecipeName.Equals(recipe.RecipeName) && a.Id != recipe.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
