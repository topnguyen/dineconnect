﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class MaterialUnitsLinkManager : DineConnectServiceBase, IMaterialUnitsLinkManager, ITransientDependency
    {

        private readonly IRepository<MaterialUnitsLink> _materialUnitsLinkRepo;

        public MaterialUnitsLinkManager(IRepository<MaterialUnitsLink> materialUnitsLink)
        {
            _materialUnitsLinkRepo = materialUnitsLink;
        }

        public async Task<IdentityResult> CreateSync(MaterialUnitsLink materialUnitsLink)
        {
            //  if the New Addition
            if (materialUnitsLink.Id == 0)
            {
                if (_materialUnitsLinkRepo.GetAll().Any(a => a.MaterialRefId == materialUnitsLink.MaterialRefId && a.UnitRefId == materialUnitsLink.UnitRefId))
                {
                    string[] strArrays = { L("SameDataAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _materialUnitsLinkRepo.InsertAndGetIdAsync(materialUnitsLink);
                return IdentityResult.Success;

            }
            else
            {
                List<MaterialUnitsLink> lst = _materialUnitsLinkRepo.GetAll().Where(a => a.MaterialRefId == materialUnitsLink.MaterialRefId && a.UnitRefId == materialUnitsLink.UnitRefId && a.Id != materialUnitsLink.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("SameDataAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
