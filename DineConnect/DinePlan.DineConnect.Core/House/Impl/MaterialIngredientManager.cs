﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class MaterialIngredientManager : DineConnectServiceBase, IMaterialIngredientManager, ITransientDependency
    {

        private readonly IRepository<MaterialIngredient> _materialIngredientRepo;

        public MaterialIngredientManager(IRepository<MaterialIngredient> materialIngredient)
        {
            _materialIngredientRepo = materialIngredient;
        }

        public async Task<IdentityResult> CreateSync(MaterialIngredient materialIngredient)
        {
            //  if the New Addition
            if (materialIngredient.Id == 0)
            {
                if (_materialIngredientRepo.GetAll().Any(a => a.MaterialRefId== materialIngredient.MaterialRefId && a.RecipeRefId == materialIngredient.RecipeRefId))
                {
                    string[] strArrays = { L("SameDataAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _materialIngredientRepo.InsertAndGetIdAsync(materialIngredient);
                return IdentityResult.Success;

            }
            else
            {
                List<MaterialIngredient> lst = _materialIngredientRepo.GetAll().Where(a => a.MaterialRefId == materialIngredient.MaterialRefId && a.RecipeRefId == materialIngredient.RecipeRefId && a.Id != materialIngredient.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("SameDataAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

