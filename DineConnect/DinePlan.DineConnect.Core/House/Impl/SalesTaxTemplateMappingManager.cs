﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class SalesTaxTemplateMappingManager : DineConnectServiceBase, ISalesTaxTemplateMappingManager, ITransientDependency
    {

        private readonly IRepository<SalesTaxTemplateMapping> _salestaxTemplateMappingRepo;

        public SalesTaxTemplateMappingManager(IRepository<SalesTaxTemplateMapping> taxTemplateMapping)
        {
            _salestaxTemplateMappingRepo = taxTemplateMapping;
        }

        public async Task<IdentityResult> CreateSync(SalesTaxTemplateMapping taxTemplateMapping)
        {
            //  if the New Addition
            if (taxTemplateMapping.Id == 0)
            {
                if (_salestaxTemplateMappingRepo.GetAll().Any(a => a.Id.Equals(taxTemplateMapping.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _salestaxTemplateMappingRepo.InsertAndGetIdAsync(taxTemplateMapping);
                return IdentityResult.Success;

            }
            else
            {
                List<SalesTaxTemplateMapping> lst = _salestaxTemplateMappingRepo.GetAll().Where(a => a.Id.Equals(taxTemplateMapping.Id) && a.Id != taxTemplateMapping.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
