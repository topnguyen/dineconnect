﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class CustomerTagMaterialPriceManager : DineConnectServiceBase, ICustomerTagMaterialPriceManager, ITransientDependency
    {

        private readonly IRepository<CustomerTagMaterialPrice> _customerTagMaterialPriceRepo;

        public CustomerTagMaterialPriceManager(IRepository<CustomerTagMaterialPrice> customerTagMaterialPrice)
        {
            _customerTagMaterialPriceRepo = customerTagMaterialPrice;
        }

        public async Task<IdentityResult> CreateSync(CustomerTagMaterialPrice customerTagMaterialPrice)
        {
            //  if the New Addition
            if (customerTagMaterialPrice.Id == 0)
            {
                if (_customerTagMaterialPriceRepo.GetAll().Any(a => a.MaterialRefId.Equals(customerTagMaterialPrice.MaterialRefId) && a.TagCode.Equals(customerTagMaterialPrice.TagCode)))
                {
                    string[] strArrays = { L("PriceAlreadyExistsForThisTag") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _customerTagMaterialPriceRepo.InsertAndGetIdAsync(customerTagMaterialPrice);
                return IdentityResult.Success;

            }
            else
            {
                List<CustomerTagMaterialPrice> lst = _customerTagMaterialPriceRepo.GetAll().Where(a => a.MaterialRefId.Equals(customerTagMaterialPrice.MaterialRefId) && a.TagCode.Equals(customerTagMaterialPrice.TagCode) && a.Id != customerTagMaterialPrice.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("PriceAlreadyExistsForThisTag") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
