﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class ReturnManager : DineConnectServiceBase, IReturnManager, ITransientDependency
    {

        private readonly IRepository<Return> _returnRepo;

        public ReturnManager(IRepository<Return> retturn)
        {
            _returnRepo = retturn;
        }

        public async Task<IdentityResult> CreateSync(Return retturn)
        {
            //  if the New Addition
            if (retturn.Id == 0 )
            {
                if (_returnRepo.GetAll().Any(a => a.Id.Equals(retturn.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _returnRepo.InsertAndGetIdAsync(retturn);
                return IdentityResult.Success;

            }
            else
            {
                List<Return> lst = _returnRepo.GetAll().Where(a => a.Id.Equals(retturn.Id) && a.Id != retturn.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
