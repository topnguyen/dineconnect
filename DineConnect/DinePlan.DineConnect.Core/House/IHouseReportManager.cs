﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IHouseReportManager
    {
        Task<IdentityResult> CreateSync(HouseReport houseReport);
    }
}