﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface ICustomerTagMaterialPriceManager
    {
        Task<IdentityResult> CreateSync(CustomerTagMaterialPrice customerTagMaterialPrice);
    }
}

