﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Audit;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.House.Document
{
    [Table("UploadDocuments")]
    public class UploadDocument :  FullAuditedEntity, IMustHaveTenant, IAuditable
    {

        [ForeignKey("SupplierId")]
        public Supplier Supplier{ get; set; }
        public int? SupplierId { get; set; }

        [ForeignKey("LocationId")]
        public Location Location { get; set; }
        public int LocationId { get; set; }

        [ForeignKey("InvoiceId")]
        public Invoice Invoice { get; set; }
        public int? InvoiceId { get; set; }

        public int DocumentStatus { get; set; }
        public virtual ICollection<UploadDocumentComment> Comments { get; set; }

        public string Url { get; set; }
        public int TenantId { get; set; }
    }

    [Table("UploadDocumentComments")]
    public class UploadDocumentComment : CreationAuditedEntity
    {
        public string Comments { get; set; }

    }

    public enum DocumentStatus
    {
        ActionRequired,
        Processing,
        Completed
    }
}
