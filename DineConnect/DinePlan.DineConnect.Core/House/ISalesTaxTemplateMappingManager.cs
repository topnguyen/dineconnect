﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface ISalesTaxTemplateMappingManager
    {
        Task<IdentityResult> CreateSync(SalesTaxTemplateMapping salesTaxTemplateMapping);
    }
}
