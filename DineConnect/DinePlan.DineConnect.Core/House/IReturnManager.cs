﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IReturnManager
    {
        Task<IdentityResult> CreateSync(Return retturn);
    }
}