﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface ICustomerTagDefinitionManager
    {
        Task<IdentityResult> CreateSync(CustomerTagDefinition customerTagDefinition);
    }
}

