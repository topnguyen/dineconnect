﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IMaterialUnitsLinkManager
    {
        Task<IdentityResult> CreateSync(MaterialUnitsLink materialUnitsLink);
    }
}
