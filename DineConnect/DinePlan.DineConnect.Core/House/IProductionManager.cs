﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IProductionManager
    {
        Task<IdentityResult> CreateSync(Production production);
    }
}
