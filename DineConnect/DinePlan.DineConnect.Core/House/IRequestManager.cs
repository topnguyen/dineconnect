﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IRequestManager
    {
        Task<IdentityResult> CreateSync(Request request);
    }
}

