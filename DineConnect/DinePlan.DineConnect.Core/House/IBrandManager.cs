﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House
{
    public interface IBrandManager 
    {
        Task<IdentityResult> CreateSync(Brand brand);
    }
}
