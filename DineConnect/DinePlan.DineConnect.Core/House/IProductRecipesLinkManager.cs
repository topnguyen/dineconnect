﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IProductRecipesLinkManager
    {
        Task<IdentityResult> CreateSync(ProductRecipesLink productRecipesLink);
    }
}