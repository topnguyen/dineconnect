﻿using System.Threading.Tasks;
using Abp.Notifications;
using DinePlan.DineConnect.Authorization.Users;

namespace DinePlan.DineConnect.Notifications
{
    public interface IAppNotifier
    {
        Task WelcomeToTheApplicationAsync(User user);

        Task NewUserRegisteredAsync(User user);

        Task SendMessageAsync(long userId, string message, NotificationSeverity severity = NotificationSeverity.Info);
    }
}
