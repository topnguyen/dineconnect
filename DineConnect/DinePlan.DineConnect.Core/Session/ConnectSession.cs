﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Abp.Dependency;
using DinePlan.DineConnect.Authorization.Users;
using Microsoft.Owin.Security;

namespace DinePlan.DineConnect.Session
{
    public class ConnectSession : ITransientDependency
    {
        public ConnectSession()
        {

        }
        public int OrgId 
        {
            get
            {
                var claimsPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
                var tenantIdClaim = claimsPrincipal?.Claims.FirstOrDefault(c => c.Type.Equals( "Organization"));
                if (!string.IsNullOrEmpty(tenantIdClaim?.Value))
                {
                    return Convert.ToInt32(tenantIdClaim.Value);
                }
                return 0;
            }

            set
            {
                var currentPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
                var identity = currentPrincipal.Identity as ClaimsIdentity;
                if (identity == null)
                    return;

                var existingClaim = identity.FindFirst("Organization");
                if (existingClaim != null)
                    identity.RemoveClaim(existingClaim);

                // add new claim
                identity.AddClaim(new Claim("Organization", value.ToString()));
            }
           
        }
    }

  
}
