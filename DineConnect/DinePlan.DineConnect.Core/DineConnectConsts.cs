﻿namespace DinePlan.DineConnect
{
    /// <summary>
    ///     Some general constants for the application.
    /// </summary>
    public class DineConnectConsts
    {
        public const string LocalizationSourceName = "DineConnect";
        public const string DefaultTenant = "Connect";
        public static readonly string[] OrderStates = { "Comp", "Gift", "Void" };
        public static readonly string[] ReasonGroups = { "Exchange", "Comp", "Gift", "Void", "Reprint", "Refund","CashAudit","CashOutCode" };

        public static readonly string[] LogConsts =
        {
            "BACK OFFICE MENU OPENED",
            "CANCEL COMP",
            "CANCEL GIFT",
            "CANCEL ORDER",
            "CANCEL VOID",
            "CLEAR DATA",
            "CLEAR MENU",
            "CLEAR ORDERS",
            "CLEAR TABLES",
            "CLOSE TICKET",
            "COMP",
            "DISPLAY TICKET",
            "GIFT",
            "GIFT ALL",
            "USER LOGIN",
            "USER LOGOUT",
            "MODEL SAVED",
            "MODULE CLICKED",
            "NEW ORDER",
            "OPEN TILL",
            "ORDER MOVED",
            "PAYMENT",
            "PAY TICKET",
            "PRINT BILL",
            "PRINT DUPLICATE TICKET",
            "PRINT INVOICE",
            "PRINT LAST TICKET",
            "REOPEN TICKET",
            "REPRINT KITCHEN",
            "TICKET ENTITY CHANGED",
            "TICKETS MERGED",
            "UNLOCK TICKET",
            "USER PASSWORD CHANGED",
            "VOID",
            "VOID ALL",
            "PRE-ORDER",
            "NON PRE-ORDER"
        };

        public const long YogyaInBoundUserId = 7;
        public const int YogyaInBoundTenantId = 6;
    }
}