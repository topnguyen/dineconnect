﻿using System.Threading;

namespace DinePlan.DineConnect.Localization
{
    public static class CultureHelper
    {
        public static bool IsRtl => Thread.CurrentThread.CurrentUICulture.TextInfo.IsRightToLeft;
    }
}
