﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace DinePlan.DineConnect.Authorization
{
    /// <summary>
    ///     Application's authorization provider.
    ///     Defines permissions for the application.
    ///     See <see cref="AppPermissions" /> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            #region Common

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ??
                        context.CreatePermission(AppPermissions.Pages, L("Pages"));

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));

            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions,
                L("ChangingPermissions"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangeUserPassword,
                L("ChangingUserPassword"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Lock,
                L("Lock"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Unlock,
                L("Unlock"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_GenerateNewPassword,
                L("GenerateNewPassword"));

            var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages,
                L("Languages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create,
                L("CreatingNewLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts,
                L("ChangingTexts"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Maintenance, L("Maintenance"));

            var organizationUnits =
                administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits,
                    L("OrganizationUnits"));
            organizationUnits.CreateChildPermission(
                AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree,
                L("ManagingOrganizationTree"));
            organizationUnits.CreateChildPermission(
                AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"));

            #endregion Common

            #region Host

            //HOST-SPECIFIC PERMISSIONS

            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"),
                multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"),
                multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"),
                multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"),
                multiTenancySides: MultiTenancySides.Host);

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"),
                multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"),
                multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"),
                multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"),
                multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"),
                multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"),
                multiTenancySides: MultiTenancySides.Host);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"),
                multiTenancySides: MultiTenancySides.Host);

            #endregion Host

            #region Tenant

            //TENANT-SPECIFIC PERMISSIONS
            pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"),
                multiTenancySides: MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Templates, L("Templates"),
                multiTenancySides: MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_LastSync, L("LastSync"),
                multiTenancySides: MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_SyncDashboard, L("SyncDashboard"),
                multiTenancySides: MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"),
                multiTenancySides: MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_SMTPSetting, L("SMTPSetting"),
                multiTenancySides: MultiTenancySides.Tenant);

            var tickmessagePermission = administration.CreateChildPermission(AppPermissions.Pages_Tenant_TickMessage, L("TickMessage"), multiTenancySides: MultiTenancySides.Tenant);
            tickmessagePermission.CreateChildPermission(AppPermissions.Pages_Tenant_TickMessage_Delete, L("DeleteTickMessage"), multiTenancySides: MultiTenancySides.Tenant);
            tickmessagePermission.CreateChildPermission(AppPermissions.Pages_Tenant_TickMessage_Edit, L("EditTickMessage"), multiTenancySides: MultiTenancySides.Tenant);
            tickmessagePermission.CreateChildPermission(AppPermissions.Pages_Tenant_TickMessage_Create, L("CreateTickMessage"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion Tenant

            #region Connect

            //Connect Permissions

            var connectPermission = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Connect, L("Connect"),
                multiTenancySides: MultiTenancySides.Tenant);

            var importSettingPermission = connectPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_ImportSetting,
                L("ImportSetting"),
                multiTenancySides: MultiTenancySides.Tenant);

            var fileManagerPermission = connectPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_FileManager,
                L("FileManager"),
                multiTenancySides: MultiTenancySides.Tenant);

            var futureDataPermission = connectPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_FutureData,
                L("FutureData"), multiTenancySides: MultiTenancySides.Tenant);
            futureDataPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_FutureData_Delete,
                L("DeleteFutureData"), multiTenancySides: MultiTenancySides.Tenant);
            futureDataPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_FutureData_Edit,
                L("EditFutureData"), multiTenancySides: MultiTenancySides.Tenant);
            futureDataPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_FutureData_Create,
                L("CreateFutureData"), multiTenancySides: MultiTenancySides.Tenant);

            var terminalPermission = connectPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Terminal,
               L("Terminal"), multiTenancySides: MultiTenancySides.Tenant);
            terminalPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Terminal_Delete,
                L("DeleteTerminal"), multiTenancySides: MultiTenancySides.Tenant);
            terminalPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Terminal_Edit,
                L("EditTerminal"), multiTenancySides: MultiTenancySides.Tenant);
            terminalPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Terminal_Create,
                L("CreateTerminal"), multiTenancySides: MultiTenancySides.Tenant);

            var programSettingsPermission =
                connectPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_ProgramSetting,
                    L("ProgramSetting"), multiTenancySides: MultiTenancySides.Tenant);

            var programSettingPermission =
                programSettingsPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_ProgramSetting_Settings,
                    L("Settings"), multiTenancySides: MultiTenancySides.Tenant);
            programSettingPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_ProgramSetting_Settings_Delete,
                L("DeleteProgramSetting"), multiTenancySides: MultiTenancySides.Tenant);
            programSettingPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_ProgramSetting_Settings_Edit,
                L("EditProgramSetting"), multiTenancySides: MultiTenancySides.Tenant);
            programSettingPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_ProgramSetting_Settings_Create,
                L("CreateProgramSetting"), multiTenancySides: MultiTenancySides.Tenant);

            var programSettingTemplatePermission =
                programSettingsPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_ProgramSetting_Template,
                    L("Template"), multiTenancySides: MultiTenancySides.Tenant);
            programSettingTemplatePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_ProgramSetting_Template_Delete,
                L("DeleteTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            programSettingTemplatePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_ProgramSetting_Template_Edit,
                L("EditTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            programSettingTemplatePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_ProgramSetting_Template_Create,
                L("CreateTemplate"), multiTenancySides: MultiTenancySides.Tenant);

            // permission for PPTOrSetting
            programSettingsPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_ProgramSetting_PTTOrSetting,
                L("PTTOrSettings"), multiTenancySides: MultiTenancySides.Tenant);

            var masterPermission = connectPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master,
                L("Master"),
                multiTenancySides: MultiTenancySides.Tenant);

            var locationPermission = masterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Location,
                L("Location"), multiTenancySides: MultiTenancySides.Tenant);
            locationPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Location_Delete,
                L("DeleteLocation"), multiTenancySides: MultiTenancySides.Tenant);
            locationPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Location_Edit,
                L("EditLocation"), multiTenancySides: MultiTenancySides.Tenant);
            locationPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Location_Create,
                L("CreateLocation"), multiTenancySides: MultiTenancySides.Tenant);

            var locationGroupPermission =
                masterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_LocationGroup,
                    L("LocationGroup"), multiTenancySides: MultiTenancySides.Tenant);
            locationGroupPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_LocationGroup_Create,
                L("CreateLocationGroup"), multiTenancySides: MultiTenancySides.Tenant);

            var locationTagPermission =
                masterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_LocationTag,
                    L("LocationTag"), multiTenancySides: MultiTenancySides.Tenant);
            locationTagPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_LocationTag_Create,
                L("CreateLocationTag"), multiTenancySides: MultiTenancySides.Tenant);
            locationTagPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_LocationTag_Delete,
                L("DeleteLocationTag"), multiTenancySides: MultiTenancySides.Tenant);
            locationTagPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_LocationTag_Edit,
                L("EditLocationTag"), multiTenancySides: MultiTenancySides.Tenant);

            var planreasonPermission = masterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PlanReason, L("PlanReason"), multiTenancySides: MultiTenancySides.Tenant);
            planreasonPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PlanReason_Delete, L("DeletePlanReason"), multiTenancySides: MultiTenancySides.Tenant);
            planreasonPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PlanReason_Edit, L("EditPlanReason"), multiTenancySides: MultiTenancySides.Tenant);
            planreasonPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PlanReason_Create, L("CreatePlanReason"), multiTenancySides: MultiTenancySides.Tenant);

            var paymenttypePermission =
                masterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PaymentType,
                    L("PaymentType"), multiTenancySides: MultiTenancySides.Tenant);
            paymenttypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PaymentType_Delete,
                L("DeletePaymentType"), multiTenancySides: MultiTenancySides.Tenant);
            paymenttypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PaymentType_Edit,
                L("EditPaymentType"), multiTenancySides: MultiTenancySides.Tenant);
            paymenttypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_PaymentType_Create,
                L("CreatePaymentType"), multiTenancySides: MultiTenancySides.Tenant);

            var tickettypePermission =
                masterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TicketType,
                    L("TicketType"), multiTenancySides: MultiTenancySides.Tenant);
            tickettypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TicketType_Delete,
                L("DeleteTicketType"), multiTenancySides: MultiTenancySides.Tenant);
            tickettypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TicketType_Edit,
                L("EditTicketType"), multiTenancySides: MultiTenancySides.Tenant);
            tickettypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TicketType_Create,
                L("CreateTicketType"), multiTenancySides: MultiTenancySides.Tenant);

            var numeratorPermission =
               masterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Numerator,
                   L("Numerator"), multiTenancySides: MultiTenancySides.Tenant);
            numeratorPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Numerator_Delete,
                L("DeleteNumerator"), multiTenancySides: MultiTenancySides.Tenant);
            numeratorPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Numerator_Edit,
                L("EditNumerator"), multiTenancySides: MultiTenancySides.Tenant);
            numeratorPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Numerator_Create,
                L("CreateNumerator"), multiTenancySides: MultiTenancySides.Tenant);

            var foreigncurrencyPermission =
                masterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_ForeignCurrency,
                    L("ForeignCurrency"), multiTenancySides: MultiTenancySides.Tenant);
            foreigncurrencyPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_ForeignCurrency_Delete, L("DeleteForeignCurrency"),
                multiTenancySides: MultiTenancySides.Tenant);
            foreigncurrencyPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_ForeignCurrency_Edit, L("EditForeignCurrency"),
                multiTenancySides: MultiTenancySides.Tenant);
            foreigncurrencyPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_ForeignCurrency_Create, L("CreateForeignCurrency"),
                multiTenancySides: MultiTenancySides.Tenant);

            var transactiontypePermission =
                masterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TransactionType,
                    L("TransactionType"), multiTenancySides: MultiTenancySides.Tenant);
            transactiontypePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_TransactionType_Delete, L("DeleteTransactionType"),
                multiTenancySides: MultiTenancySides.Tenant);
            transactiontypePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_TransactionType_Edit, L("EditTransactionType"),
                multiTenancySides: MultiTenancySides.Tenant);
            transactiontypePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_TransactionType_Create, L("CreateTransactionType"),
                multiTenancySides: MultiTenancySides.Tenant);

            var entitytypePermission =
                masterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_EntityType,
                    L("EntityType"), multiTenancySides: MultiTenancySides.Tenant);
            entitytypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_EntityType_Delete,
                L("DeleteEntityType"), multiTenancySides: MultiTenancySides.Tenant);
            entitytypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_EntityType_Edit,
                L("EditEntityType"), multiTenancySides: MultiTenancySides.Tenant);
            entitytypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_EntityType_Create,
                L("CreateEntityType"), multiTenancySides: MultiTenancySides.Tenant);

            var departmentPermission =
                masterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Department,
                    L("Department"), multiTenancySides: MultiTenancySides.Tenant);
            departmentPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Department_Delete,
                L("DeleteDepartment"), multiTenancySides: MultiTenancySides.Tenant);
            departmentPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Department_Edit,
                L("EditDepartment"), multiTenancySides: MultiTenancySides.Tenant);
            departmentPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Department_Create,
                L("CreateDepartment"), multiTenancySides: MultiTenancySides.Tenant);

            var departmentgroupPermission = masterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_DepartmentGroup, L("DepartmentGroup"), multiTenancySides: MultiTenancySides.Tenant);
            departmentgroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_DepartmentGroup_Delete, L("DeleteDepartmentGroup"), multiTenancySides: MultiTenancySides.Tenant);
            departmentgroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_DepartmentGroup_Edit, L("EditDepartmentGroup"), multiTenancySides: MultiTenancySides.Tenant);
            departmentgroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_DepartmentGroup_Create, L("CreateDepartmentGroup"), multiTenancySides: MultiTenancySides.Tenant);

            var tablePermission = connectPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Table,
                L("Table"),
                multiTenancySides: MultiTenancySides.Tenant);

            var connectGroupPerm =
                tablePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Table_Group,
                    L("ConnectTableGroup"),
                    multiTenancySides: MultiTenancySides.Tenant);

            connectGroupPerm.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Table_Group_Create,
                L("CreateConnectTableGroup"), multiTenancySides: MultiTenancySides.Tenant);
            connectGroupPerm.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Table_Group_Edit,
                L("EditConnectTableGroup"), multiTenancySides: MultiTenancySides.Tenant);
            connectGroupPerm.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Table_Group_Delete,
                L("DeleteConnectTableGroup"), multiTenancySides: MultiTenancySides.Tenant);

            var tablesPerm =
                tablePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tables, L("Tables"),
                    multiTenancySides: MultiTenancySides.Tenant);

            tablesPerm.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tables_Create,
                L("CreateConnectTables"), multiTenancySides: MultiTenancySides.Tenant);
            tablesPerm.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tables_Edit,
                L("EditConnectTables"), multiTenancySides: MultiTenancySides.Tenant);
            tablesPerm.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Tables_Delete,
                L("DeleteConnectTables"), multiTenancySides: MultiTenancySides.Tenant);

            var menuPermission = connectPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu,
                L("Menu"),
                multiTenancySides: MultiTenancySides.Tenant);

            var menuitemPermission =
                menuPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_MenuItem, L("MenuItem"),
                    multiTenancySides: MultiTenancySides.Tenant);

            menuitemPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_MenuItem_Delete,
                L("DeleteMenuItem"), multiTenancySides: MultiTenancySides.Tenant);
            menuitemPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_MenuItem_CloneAsMaterial,
                L("CloneAsMaterial"), multiTenancySides: MultiTenancySides.Tenant);
            menuitemPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_MenuItem_Edit,
                L("EditMenuItem"), multiTenancySides: MultiTenancySides.Tenant);
            menuitemPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_MenuItem_Create,
                L("CreateMenuItem"), multiTenancySides: MultiTenancySides.Tenant);
            menuPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_MenuItemPrice,
                L("MenuItemPrice"), multiTenancySides: MultiTenancySides.Tenant);

            var productGroupPermission = menuPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ProductGroup,
                L("ProductGroup"), multiTenancySides: MultiTenancySides.Tenant);
            productGroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ProductGroup_ManageProductGroup,
                L("ManagingProductGroup"), multiTenancySides: MultiTenancySides.Tenant);

            var screenmenuPermission =
                connectPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ScreenMenu,
                    L("ScreenMenu"), multiTenancySides: MultiTenancySides.Tenant);
            screenmenuPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ScreenMenu_Delete,
                L("DeleteScreenMenu"), multiTenancySides: MultiTenancySides.Tenant);
            screenmenuPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ScreenMenu_Edit,
                L("EditScreenMenu"), multiTenancySides: MultiTenancySides.Tenant);
            screenmenuPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ScreenMenu_Create,
                L("CreateScreenMenu"), multiTenancySides: MultiTenancySides.Tenant);

            var pricetagPermission =
                menuPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_PriceTag, L("PriceTag"),
                    multiTenancySides: MultiTenancySides.Tenant);
            pricetagPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_PriceTag_Delete,
                L("DeletePriceTag"), multiTenancySides: MultiTenancySides.Tenant);
            pricetagPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_PriceTag_Edit,
                L("EditPriceTag"), multiTenancySides: MultiTenancySides.Tenant);
            pricetagPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_PriceTag_Create,
                L("CreatePriceTag"), multiTenancySides: MultiTenancySides.Tenant);

            var promotioncategoryPermission = menuPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_PromotionCategory, L("PromotionCategory"), multiTenancySides: MultiTenancySides.Tenant);
            promotioncategoryPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_PromotionCategory_Delete, L("DeletePromotionCategory"), multiTenancySides: MultiTenancySides.Tenant);
            promotioncategoryPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_PromotionCategory_Edit, L("EditPromotionCategory"), multiTenancySides: MultiTenancySides.Tenant);
            promotioncategoryPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_PromotionCategory_Create, L("CreatePromotionCategory"), multiTenancySides: MultiTenancySides.Tenant);

            var promotionPermission = menuPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_Promotion,
                L("Promotion"), multiTenancySides: MultiTenancySides.Tenant);
            promotionPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_Promotion_Delete,
                L("DeletePromotion"), multiTenancySides: MultiTenancySides.Tenant);
            promotionPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_Promotion_Edit,
                L("EditPromotion"), multiTenancySides: MultiTenancySides.Tenant);
            promotionPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_Promotion_Create,
                L("CreatePromotion"), multiTenancySides: MultiTenancySides.Tenant);

            var comboGroupPermission = menuPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ComboGroup,
                L("ComboGroup"), multiTenancySides: MultiTenancySides.Tenant);
            comboGroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ComboGroup_Delete,
                L("DeleteComboGroup"), multiTenancySides: MultiTenancySides.Tenant);
            comboGroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ComboGroup_Edit,
                L("EditComboGroup"), multiTenancySides: MultiTenancySides.Tenant);
            comboGroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_ComboGroup_Create,
                L("CreateComboGroup"), multiTenancySides: MultiTenancySides.Tenant);

            var deliveryAggregatorPermission = menuPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_DeliveryAggregator,
                L("DeliveryAggregator"), multiTenancySides: MultiTenancySides.Tenant);
            deliveryAggregatorPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_DeliveryAggregator_Delete,
                L("DeleteDeliveryAggregator"), multiTenancySides: MultiTenancySides.Tenant);
            deliveryAggregatorPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_DeliveryAggregator_Edit,
                L("EditDeliveryAggregator"), multiTenancySides: MultiTenancySides.Tenant);
            deliveryAggregatorPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_DeliveryAggregator_Create,
                L("CreateDeliveryAggregator"), multiTenancySides: MultiTenancySides.Tenant);

            var tillaccountPermission =
                masterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TillAccount,
                    L("TillAccount"), multiTenancySides: MultiTenancySides.Tenant);
            tillaccountPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TillAccount_Delete,
                L("DeleteTillAccount"), multiTenancySides: MultiTenancySides.Tenant);
            tillaccountPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TillAccount_Edit,
                L("EditTillAccount"), multiTenancySides: MultiTenancySides.Tenant);
            tillaccountPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_TillAccount_Create,
                L("CreateTillAccount"), multiTenancySides: MultiTenancySides.Tenant);

            var ordertaggroupPermission =
                menuPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_OrderTagGroup,
                    L("OrderTagGroup"), multiTenancySides: MultiTenancySides.Tenant);
            ordertaggroupPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Menu_OrderTagGroup_Delete, L("DeleteOrderTagGroup"),
                multiTenancySides: MultiTenancySides.Tenant);
            ordertaggroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_OrderTagGroup_Edit,
                L("EditOrderTagGroup"), multiTenancySides: MultiTenancySides.Tenant);
            ordertaggroupPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Menu_OrderTagGroup_Create, L("CreateOrderTagGroup"),
                multiTenancySides: MultiTenancySides.Tenant);

            var ticketTaggroupPermission =
                menuPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_TicketTagGroup,
                    L("TicketTagGroup"), multiTenancySides: MultiTenancySides.Tenant);
            ticketTaggroupPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Menu_TicketTagGroup_Delete, L("DeleteTicketTagGroup"),
                multiTenancySides: MultiTenancySides.Tenant);
            ticketTaggroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_TicketTagGroup_Edit,
                L("EditTicketTagGroup"), multiTenancySides: MultiTenancySides.Tenant);
            ticketTaggroupPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Menu_TicketTagGroup_Create, L("CreateTicketTagGroup"),
                multiTenancySides: MultiTenancySides.Tenant);

            var categoryPermission =
                menuPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_Category, L("Category"),
                    multiTenancySides: MultiTenancySides.Tenant);
            categoryPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_Category_Delete,
                L("DeleteCategory"), multiTenancySides: MultiTenancySides.Tenant);
            categoryPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_Category_Edit,
                L("EditCategory"), multiTenancySides: MultiTenancySides.Tenant);
            categoryPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Menu_Category_Create,
                L("CreateCategory"), multiTenancySides: MultiTenancySides.Tenant);

            var setupPermission = connectPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Setup, L("Setup"),
                multiTenancySides: MultiTenancySides.Tenant);
            setupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Setup_CleanSeedData, L("CleanSeedData"),
                multiTenancySides: MultiTenancySides.Tenant);
            setupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Setup_ImportSeedData, L("ImportSeedData"),
                multiTenancySides: MultiTenancySides.Tenant);
            setupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Setup_ImportData, L("ImportData"),
                multiTenancySides: MultiTenancySides.Tenant);
            setupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Setup_ExportData, L("ExportData"),
                multiTenancySides: MultiTenancySides.Tenant);

            var reportPermission = connectPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report,
                L("Report"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Tickets,
                L("Tickets"), multiTenancySides: MultiTenancySides.Tenant);
            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Gst,
                L("GST"), multiTenancySides: MultiTenancySides.Tenant);
            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Orders,
                L("Orders"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Departments,
                L("Departments"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_AbnormalEndDay,
              L("AbnormalEndDay"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_CashAudits,
               L("CashAudits"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_SpeedOfServiceReport,
              L("SpeedOfServiceReport"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_CreditCard,
              L("CreditCard"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Customize,
             L("Customize"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_TopDownSellReport,
              L("TopDownSellReport"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_SalesByPeriodReport,
              L("SalesByPeriod"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_SalesByPeriodByReport,
           L("SalesByPeriodReport"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_SalesPromotionReport,
            L("SalesPromotion"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_CashSummaryReport,
            L("CashSummaryReport"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Items,
                L("Items"),
                multiTenancySides: MultiTenancySides.Tenant);
            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Summary,
                L("Summary"),
                multiTenancySides: MultiTenancySides.Tenant);
            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Schedule,
                L("Schedule"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Collection,
                L("Collection"), multiTenancySides: MultiTenancySides.Tenant);
            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_FeedbackUnread,
                L("FeedbackUnread"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_CreditSales,
                L("CreditSales"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_Comparision,
             L("Comparision"), multiTenancySides: MultiTenancySides.Tenant);


            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_TopDownSellReportByQuantitySummary,
                L("TopDownSellReportByQuantitySummary"), multiTenancySides: MultiTenancySides.Tenant);
            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_TopDownSellReportByAmountPlant,
                L("TopDownSellReportByAmountPlant"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_TopDownSellReportByAmountSummary,
                L("TopDownSellReportByAmountSummary"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_TopDownSellReportByQuantityPlant,
             L("TopDownSellReportByQuantityPlant"), multiTenancySides: MultiTenancySides.Tenant);

            // diff
            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_MealTimeReport,
                  L("MealTimeSales"), multiTenancySides: MultiTenancySides.Tenant);
            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_SalesPromotionByPlantReport,
                  L("SalesPromotionReportByPlant"), multiTenancySides: MultiTenancySides.Tenant);

            reportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Report_SalesPromotionByProReport,
                  L("SalesPromotionReportByPromotion"), multiTenancySides: MultiTenancySides.Tenant);
         
            var userPermissions = connectPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_User,
                L("User"),
                multiTenancySides: MultiTenancySides.Tenant);

            var dineplanuserPermission =
                userPermissions.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_User_DinePlanUser,
                    L("DinePlanUser"), multiTenancySides: MultiTenancySides.Tenant);
            dineplanuserPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_User_DinePlanUser_Delete,
                L("DeleteDinePlanUser"), multiTenancySides: MultiTenancySides.Tenant);
            dineplanuserPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_User_DinePlanUser_Edit,
                L("EditDinePlanUser"), multiTenancySides: MultiTenancySides.Tenant);
            dineplanuserPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_User_DinePlanUser_Create,
                L("CreateDinePlanUser"), multiTenancySides: MultiTenancySides.Tenant);

            var dineplanuserrolePermission =
                userPermissions.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_User_DinePlanUserRole,
                    L("DinePlanUserRole"), multiTenancySides: MultiTenancySides.Tenant);
            dineplanuserrolePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_User_DinePlanUserRole_Delete, L("DeleteDinePlanUserRole"),
                multiTenancySides: MultiTenancySides.Tenant);
            dineplanuserrolePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_User_DinePlanUserRole_Edit, L("EditDinePlanUserRole"),
                multiTenancySides: MultiTenancySides.Tenant);
            dineplanuserrolePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_User_DinePlanUserRole_Create, L("CreateDinePlanUserRole"),
                multiTenancySides: MultiTenancySides.Tenant);

            //  DinePlanTax

            var dineplantaxPermission =
                masterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_DinePlanTax,
                    L("DinePlanTax"), multiTenancySides: MultiTenancySides.Tenant);
            dineplantaxPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_DinePlanTax_Delete,
                L("DeleteDinePlanTax"), multiTenancySides: MultiTenancySides.Tenant);
            dineplantaxPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_DinePlanTax_Edit,
                L("EditDinePlanTax"), multiTenancySides: MultiTenancySides.Tenant);
            dineplantaxPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_DinePlanTax_Create,
                L("CreateDinePlanTax"), multiTenancySides: MultiTenancySides.Tenant);

            var calculationPermission =
                masterPermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_Connect_Master_Calculation,
                    L("Calculation"), multiTenancySides: MultiTenancySides.Tenant);
            calculationPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_Calculation_Delete,
                L("DeleteCalculation"), multiTenancySides: MultiTenancySides.Tenant);
            calculationPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_Calculation_Edit,
                L("EditCalculation"), multiTenancySides: MultiTenancySides.Tenant);
            calculationPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Connect_Master_Calculation_Create,
                L("CreateCalculation"), multiTenancySides: MultiTenancySides.Tenant);

            //Company
            var companyPermission =
                masterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Company, L("Company"),
                    multiTenancySides: MultiTenancySides.Tenant);
            companyPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Company_Delete,
                L("DeleteCompany"), multiTenancySides: MultiTenancySides.Tenant);
            companyPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Company_Edit,
                L("EditCompany"), multiTenancySides: MultiTenancySides.Tenant);
            companyPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Master_Company_Create,
                L("CreateCompany"), multiTenancySides: MultiTenancySides.Tenant);

            //card
            var connectCardPermission = connectPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card, L("Card"),
                    multiTenancySides: MultiTenancySides.Tenant);
            var connectCardsPermission = connectCardPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_Cards, L("ConnectCard"),
                    multiTenancySides: MultiTenancySides.Tenant);
            connectCardsPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_Cards_Import,
                L("ImportConnectCard"), multiTenancySides: MultiTenancySides.Tenant);
            connectCardsPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_Cards_Delete,
                L("DeleteConnectCard"), multiTenancySides: MultiTenancySides.Tenant);
            connectCardsPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_Cards_Edit,
                L("EditConnectCard"), multiTenancySides: MultiTenancySides.Tenant);
            connectCardsPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_Cards_Create,
                L("CreateConnectCard"), multiTenancySides: MultiTenancySides.Tenant);
            connectCardsPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_Cards_Active,
                L("ActiveConnectCard"), multiTenancySides: MultiTenancySides.Tenant);
            connectCardsPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_Cards_DeleteMultipleConnectCards,
                L("DeleteMultipleConnectCard"), multiTenancySides: MultiTenancySides.Tenant);

            var connectCardTypePermission =
                connectCardPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_CardType,
                    L("ConnectCardType"), multiTenancySides: MultiTenancySides.Tenant);
            connectCardTypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_CardType_Delete,
                L("DeleteConnectCardType"), multiTenancySides: MultiTenancySides.Tenant);
            connectCardTypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_CardType_Edit,
                L("EditConnectCardType"), multiTenancySides: MultiTenancySides.Tenant);
            connectCardTypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_CardType_Create,
                L("CreateConnectCardType"), multiTenancySides: MultiTenancySides.Tenant);

            var connectcardtypecategoryPermission = connectCardPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_ConnectCardTypeCategory, L("ConnectCardTypeCategory"), multiTenancySides: MultiTenancySides.Tenant);
            connectcardtypecategoryPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_ConnectCardTypeCategory_Delete, L("DeleteConnectCardTypeCategory"), multiTenancySides: MultiTenancySides.Tenant);
            connectcardtypecategoryPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_ConnectCardTypeCategory_Edit, L("EditConnectCardTypeCategory"), multiTenancySides: MultiTenancySides.Tenant);
            connectcardtypecategoryPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Card_ConnectCardTypeCategory_Create, L("CreateConnectCardTypeCategory"), multiTenancySides: MultiTenancySides.Tenant);

            var connectFullTaxPermission = connectPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_FullTax, L("FullTax"),
                   multiTenancySides: MultiTenancySides.Tenant);

            connectFullTaxPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_FullTax_Report, L("Report"),
                    multiTenancySides: MultiTenancySides.Tenant);

            var connectMemberPermission = connectFullTaxPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_FullTax_Member, L("FullTaxMember"),
                    multiTenancySides: MultiTenancySides.Tenant);
            connectMemberPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_FullTax_Member_Delete,
                L("DeleteMember"), multiTenancySides: MultiTenancySides.Tenant);
            connectMemberPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_FullTax_Member_Edit,
                L("EditMember"), multiTenancySides: MultiTenancySides.Tenant);
            connectMemberPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_FullTax_Member_Create,
                L("CreateMember"), multiTenancySides: MultiTenancySides.Tenant);

            var connectDisplayPermission = connectPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display, L("Print"),
                   multiTenancySides: MultiTenancySides.Tenant);

            var connectPrinterPermission = connectDisplayPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_Printer, L("Printer"),
                      multiTenancySides: MultiTenancySides.Tenant);
            connectPrinterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_Printer_Delete,
                L("DeletePrinter"), multiTenancySides: MultiTenancySides.Tenant);
            connectPrinterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_Printer_Edit,
                L("EditPrinter"), multiTenancySides: MultiTenancySides.Tenant);
            connectPrinterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_Printer_Create,
                L("CreatePrinter"), multiTenancySides: MultiTenancySides.Tenant);

            var connectPrintJobPermission = connectDisplayPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_PrintJob, L("PrintJob"),
                       multiTenancySides: MultiTenancySides.Tenant);
            connectPrintJobPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_PrintJob_Delete,
                L("DeletePrintJob"), multiTenancySides: MultiTenancySides.Tenant);
            connectPrintJobPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_PrintJob_Edit,
                L("EditPrintJob"), multiTenancySides: MultiTenancySides.Tenant);
            connectPrintJobPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_PrintJob_Create,
                L("CreatePrintJob"), multiTenancySides: MultiTenancySides.Tenant);

            //var connectReceipContentPermission = connectDisplayPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_PrinterMap, L("PrinterMap"),
            //          multiTenancySides: MultiTenancySides.Tenant);
            //connectReceipContentPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_PrinterMap_Delete,
            //    L("DeletePrinterMap"), multiTenancySides: MultiTenancySides.Tenant);
            //connectReceipContentPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_PrinterMap_Edit,
            //    L("EditPrinterMap"), multiTenancySides: MultiTenancySides.Tenant);
            //connectReceipContentPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_PrinterMap_Create,
            //    L("CreatePrinterMap"), multiTenancySides: MultiTenancySides.Tenant);

            var connectPrintTemplatePermission = connectDisplayPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_PrintTemplate, L("PrintTemplate"),
                      multiTenancySides: MultiTenancySides.Tenant);
            connectPrintTemplatePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_PrintTemplate_Delete,
                L("DeletePrintTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            connectPrintTemplatePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_PrintTemplate_Edit,
                L("EditPrintTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            connectPrintTemplatePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_PrintTemplate_Create,
                L("CreatePrintTemplate"), multiTenancySides: MultiTenancySides.Tenant);

            var printTemplateConditionPermission = connectDisplayPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_PrintTemplateCondition, L("PrintTemplateCondition"),
                multiTenancySides: MultiTenancySides.Tenant);
            printTemplateConditionPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_PrintTemplateCondition_Delete,
                L("DeletePrintTemplateCondition"), multiTenancySides: MultiTenancySides.Tenant);
            printTemplateConditionPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_PrintTemplateCondition_Edit,
                L("EditPrintTemplateCondition"), multiTenancySides: MultiTenancySides.Tenant);
            printTemplateConditionPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_PrintTemplateCondition_Create,
                L("CreatePrintTemplateCondition"), multiTenancySides: MultiTenancySides.Tenant);

            var dinedevicePermission = connectDisplayPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_DineDevice, L("DineDevice"), multiTenancySides: MultiTenancySides.Tenant);
            dinedevicePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_DineDevice_Delete, L("DeleteDineDevice"), multiTenancySides: MultiTenancySides.Tenant);
            dinedevicePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_DineDevice_Edit, L("EditDineDevice"), multiTenancySides: MultiTenancySides.Tenant);
            dinedevicePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Connect_Display_DineDevice_Create, L("CreateDineDevice"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion Connect

            #region Touch

            var touchPer = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Touch, L("Touch"),
                multiTenancySides: MultiTenancySides.Tenant);
            touchPer.CreateChildPermission(AppPermissions.Pages_Tenant_Touch_Menu,
                L("LocationMenuItem"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion Touch

            #region House

            //House Permission

            var housePermission = pages.CreateChildPermission(AppPermissions.Pages_Tenant_House, L("House"),
                multiTenancySides: MultiTenancySides.Tenant);

            housePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Dashboard,
                L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);
            housePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Numbering,
                L("Numbering"), multiTenancySides: MultiTenancySides.Tenant);

            housePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_NotificationDashboard, L("NotificationDashboard"), multiTenancySides: MultiTenancySides.Tenant);

            // House Master Permission
            var masterHousePermission = housePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master,
                L("Master"),
                multiTenancySides: MultiTenancySides.Tenant);

            var supplierPermission =
                masterHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Supplier,
                    L("Supplier"), multiTenancySides: MultiTenancySides.Tenant);
            supplierPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Supplier_Delete,
                L("DeleteSupplier"), multiTenancySides: MultiTenancySides.Tenant);
            supplierPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Supplier_Edit,
                L("EditSupplier"), multiTenancySides: MultiTenancySides.Tenant);
            supplierPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Supplier_Create,
                L("CreateSupplier"), multiTenancySides: MultiTenancySides.Tenant);
            supplierPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Supplier_Import,
              L("ImportData"), multiTenancySides: MultiTenancySides.Tenant);
            

            var unitPermission =
                masterHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Unit, L("Unit"),
                    multiTenancySides: MultiTenancySides.Tenant);
            unitPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Unit_Delete, L("DeleteUnit"),
                multiTenancySides: MultiTenancySides.Tenant);
            unitPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Unit_Edit, L("EditUnit"),
                multiTenancySides: MultiTenancySides.Tenant);
            unitPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Unit_Create, L("CreateUnit"),
                multiTenancySides: MultiTenancySides.Tenant);

            var materialgroupPermission =
                masterHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_MaterialGroup,
                    L("MaterialGroup"), multiTenancySides: MultiTenancySides.Tenant);
            materialgroupPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_MaterialGroup_Delete, L("DeleteMaterialGroup"),
                multiTenancySides: MultiTenancySides.Tenant);
            materialgroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_MaterialGroup_Edit,
                L("EditMaterialGroup"), multiTenancySides: MultiTenancySides.Tenant);
            materialgroupPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_MaterialGroup_Create, L("CreateMaterialGroup"),
                multiTenancySides: MultiTenancySides.Tenant);

            var materialgroupcategoryPermission =
                masterHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Master_MaterialGroupCategory, L("MaterialGroupCategory"),
                    multiTenancySides: MultiTenancySides.Tenant);
            materialgroupcategoryPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_MaterialGroupCategory_Delete, L("DeleteMaterialGroupCategory"),
                multiTenancySides: MultiTenancySides.Tenant);
            materialgroupcategoryPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_MaterialGroupCategory_Edit, L("EditMaterialGroupCategory"),
                multiTenancySides: MultiTenancySides.Tenant);
            materialgroupcategoryPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_MaterialGroupCategory_Create, L("CreateMaterialGroupCategory"),
                multiTenancySides: MultiTenancySides.Tenant);

            //Material Master
            var materialPermission =
                masterHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Material,
                    L("Material"), multiTenancySides: MultiTenancySides.Tenant);
            materialPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Material_Delete,
                L("DeleteMaterial"), multiTenancySides: MultiTenancySides.Tenant);
            materialPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Material_Edit,
                L("EditMaterial"), multiTenancySides: MultiTenancySides.Tenant);
            materialPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Material_Create,
                L("CreateMaterial"), multiTenancySides: MultiTenancySides.Tenant);
            materialPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Material_Barcode_Usage,
                L("Barcode"), multiTenancySides: MultiTenancySides.Tenant);
            materialPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_Material_ChangeDefaultUnit,
                L("ChangeDefaultUnit"), multiTenancySides: MultiTenancySides.Tenant);
            materialPermission.CreateChildPermission(
               AppPermissions.Pages_Tenant_House_Master_Material_ChangeActiveStatus,
               L("ChangeActiveStatus"), multiTenancySides: MultiTenancySides.Tenant);
            materialPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_Material_PrintMaterialCode,
                L("PrintMaterialCode"), multiTenancySides: MultiTenancySides.Tenant);

            //MaterialLocationWiseStock
            var materiallocationwisestockPermission =
                masterHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Master_MaterialLocationWiseStock, L("MaterialLocationWiseStock"),
                    multiTenancySides: MultiTenancySides.Tenant);

            masterHousePermission.CreateChildPermission(
                  AppPermissions.Pages_Tenant_House_Master_Material_AllLocation_WiseStock, L("MaterialAllLocationWiseStock"),
                  multiTenancySides: MultiTenancySides.Tenant);

            //MaterialLedger

            var materialledgerPermission =
                masterHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_MaterialLedger,
                    L("MaterialLedger"), multiTenancySides: MultiTenancySides.Tenant);
            materialledgerPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_MaterialLedger_NegativeStock, L("NegativeStock"),
                multiTenancySides: MultiTenancySides.Tenant);
            materialledgerPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_MaterialLedger_View, L("ViewMaterialLedger"),
                multiTenancySides: MultiTenancySides.Tenant);

            //Tax
            var taxPermission = masterHousePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_Tax, L("Tax"), multiTenancySides: MultiTenancySides.Tenant);
            taxPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Tax_Delete, L("DeleteTax"),
                multiTenancySides: MultiTenancySides.Tenant);
            taxPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Tax_Edit, L("EditTax"),
                multiTenancySides: MultiTenancySides.Tenant);
            taxPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Tax_Create, L("CreateTax"),
                multiTenancySides: MultiTenancySides.Tenant);

            var suppliermaterialPermission =
                masterHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_SupplierMaterial,
                    L("SupplierMaterial"), multiTenancySides: MultiTenancySides.Tenant);
            suppliermaterialPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_SupplierMaterial_Delete, L("DeleteSupplierMaterial"),
                multiTenancySides: MultiTenancySides.Tenant);
            suppliermaterialPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_SupplierMaterial_Edit, L("EditSupplierMaterial"),
                multiTenancySides: MultiTenancySides.Tenant);
            suppliermaterialPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_SupplierMaterial_Create, L("CreateSupplierMaterial"),
                multiTenancySides: MultiTenancySides.Tenant);

            var housereportPermission =
                masterHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_HouseReport,
                    L("HouseReport"), multiTenancySides: MultiTenancySides.Tenant);
            housereportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_HouseReport_Stock,
                L("Stock"), multiTenancySides: MultiTenancySides.Tenant);
            housereportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_HouseReport_Invoice,
                L("Invoice"), multiTenancySides: MultiTenancySides.Tenant);
            housereportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_HouseReport_Costing,
                L("Costing"), multiTenancySides: MultiTenancySides.Tenant);
            housereportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_HouseReport_Analytical,
                L("Analytical"), multiTenancySides: MultiTenancySides.Tenant);
            housereportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_HouseReport_Material,
                L("Material"), multiTenancySides: MultiTenancySides.Tenant);
            housereportPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_HouseReport_InterTransfer,
               L("InterTransfer"), multiTenancySides: MultiTenancySides.Tenant);

            var materialingredientPermission =
                masterHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Master_MaterialIngredient, L("MaterialIngredient"),
                    multiTenancySides: MultiTenancySides.Tenant);
            materialingredientPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_MaterialIngredient_Delete, L("DeleteMaterialIngredient"),
                multiTenancySides: MultiTenancySides.Tenant);
            materialingredientPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_MaterialIngredient_Edit, L("EditMaterialIngredient"),
                multiTenancySides: MultiTenancySides.Tenant);
            materialingredientPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_MaterialIngredient_Create, L("CreateMaterialIngredient"),
                multiTenancySides: MultiTenancySides.Tenant);

            // Material Menu Portion Mapping

            var materialmenumappingPermission =
                masterHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Master_MaterialMenuMapping, L("MaterialMenuMapping"),
                    multiTenancySides: MultiTenancySides.Tenant);
            materialmenumappingPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_MaterialMenuMapping_Delete, L("DeleteMaterialMenuMapping"),
                multiTenancySides: MultiTenancySides.Tenant);
            materialmenumappingPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_MaterialMenuMapping_Edit, L("EditMaterialMenuMapping"),
                multiTenancySides: MultiTenancySides.Tenant);
            materialmenumappingPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_MaterialMenuMapping_Create, L("CreateMaterialMenuMapping"),
                multiTenancySides: MultiTenancySides.Tenant);

            var transactionHousePermission =
                housePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction, L("Transaction"),
                    multiTenancySides: MultiTenancySides.Tenant);

            #region Purchase

            var purchaseHousePermission =
                transactionHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Purchase,
                    L("Purchase"),
                    multiTenancySides: MultiTenancySides.Tenant);

            //PurchaseOrder
            var purchaseorderPermission =
                purchaseHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Transaction_PurchaseOrder, L("PurchaseOrder"),
                    multiTenancySides: MultiTenancySides.Tenant);
            purchaseorderPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_PurchaseOrder_Delete, L("DeletePurchaseOrder"),
                multiTenancySides: MultiTenancySides.Tenant);
            purchaseorderPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_PurchaseOrder_Edit, L("EditPurchaseOrder"),
                multiTenancySides: MultiTenancySides.Tenant);
            purchaseorderPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_PurchaseOrder_Create, L("CreatePurchaseOrder"),
                multiTenancySides: MultiTenancySides.Tenant);
            purchaseorderPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_PurchaseOrder_CreateBulkPurchaseOrder, L("CreateBulkPurchaseOrder"),
                multiTenancySides: MultiTenancySides.Tenant);
            purchaseorderPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_PurchaseOrder_Approve, L("ApprovePurchaseOrder"),
                multiTenancySides: MultiTenancySides.Tenant);
            purchaseorderPermission.CreateChildPermission(
            AppPermissions.Pages_Tenant_House_Transaction_PurchaseOrder_CanViewMultipleLocationPurchaseOrder, L("ViewMultipleLocationPurchaseOrder"),
            multiTenancySides: MultiTenancySides.Tenant);
            

            purchaseorderPermission.CreateChildPermission(
            AppPermissions.Pages_Tenant_House_Transaction_PurchaseOrder_CanEditApprovePurchaseOrderIfNotLinked, L("CanEditApprovePurchaseOrderIfNotLinked"),
            multiTenancySides: MultiTenancySides.Tenant);
            
            //purchaseorderPermission.CreateChildPermission(
            //    AppPermissions.Pages_Tenant_House_Transaction_InterTransfer_GracePeriodAllowed, L("GracePeriodAllowed"),
            //    multiTenancySides: MultiTenancySides.Tenant);

            //InwardDirectCredit
            var inwarddirectcreditPermission =
                purchaseHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Transaction_InwardDirectCredit, L("InwardDirectCredit"),
                    multiTenancySides: MultiTenancySides.Tenant);
            inwarddirectcreditPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InwardDirectCredit_Delete, L("DeleteInwardDirectCredit"),
                multiTenancySides: MultiTenancySides.Tenant);
            inwarddirectcreditPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InwardDirectCredit_Edit, L("EditInwardDirectCredit"),
                multiTenancySides: MultiTenancySides.Tenant);
            inwarddirectcreditPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InwardDirectCredit_Create, L("CreateInwardDirectCredit"),
                multiTenancySides: MultiTenancySides.Tenant);

            //Invoice

            var invoicePermission =
                purchaseHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Invoice,
                    L("Invoice"), multiTenancySides: MultiTenancySides.Tenant);
            invoicePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Invoice_Delete,
                L("DeleteInvoice"), multiTenancySides: MultiTenancySides.Tenant);
            //invoicePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Invoice_Edit, L("EditInvoice"), multiTenancySides: MultiTenancySides.Tenant);
            invoicePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Invoice_Create,
                L("CreateInvoice"), multiTenancySides: MultiTenancySides.Tenant);

            var purchasecategoryPermission =
                purchaseHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Master_PurchaseCategory, L("PurchaseCategory"),
                    multiTenancySides: MultiTenancySides.Tenant);
            purchasecategoryPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_PurchaseCategory_Delete, L("DeletePurchaseCategory"),
                multiTenancySides: MultiTenancySides.Tenant);
            purchasecategoryPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_PurchaseCategory_Edit, L("EditPurchaseCategory"),
                multiTenancySides: MultiTenancySides.Tenant);
            purchasecategoryPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_PurchaseCategory_Create, L("CreatePurchaseCategory"),
                multiTenancySides: MultiTenancySides.Tenant);

            #endregion Purchase

            #region Movement

            // MOVEMENT Permission
            var movementHousePermission =
                transactionHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Movement,
                    L("Movement"),
                    multiTenancySides: MultiTenancySides.Tenant);

            //Request
            var requestPermission =
                movementHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Request,
                    L("Request"), multiTenancySides: MultiTenancySides.Tenant);
            requestPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Request_Delete,
                L("DeleteRequest"), multiTenancySides: MultiTenancySides.Tenant);
            requestPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Request_Edit,
                L("EditRequest"), multiTenancySides: MultiTenancySides.Tenant);
            requestPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Request_Create,
                L("CreateRequest"), multiTenancySides: MultiTenancySides.Tenant);

            //Issue
            var issuePermission =
                movementHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Issue,
                    L("Issue"), multiTenancySides: MultiTenancySides.Tenant);
            issuePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Issue_Delete,
                L("DeleteIssue"), multiTenancySides: MultiTenancySides.Tenant);
            issuePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Issue_Edit,
                L("EditIssue"), multiTenancySides: MultiTenancySides.Tenant);
            issuePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Issue_Create,
                L("CreateIssue"), multiTenancySides: MultiTenancySides.Tenant);

            var returnPermission =
                movementHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Return,
                    L("Return"), multiTenancySides: MultiTenancySides.Tenant);
            returnPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Return_Delete,
                L("DeleteReturn"), multiTenancySides: MultiTenancySides.Tenant);
            returnPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Return_Edit,
                L("EditReturn"), multiTenancySides: MultiTenancySides.Tenant);
            returnPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Return_Create,
                L("CreateReturn"), multiTenancySides: MultiTenancySides.Tenant);

            //Adjustment
            var adjustmentPermission =
                movementHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Transaction_Adjustment, L("Adjustment"),
                    multiTenancySides: MultiTenancySides.Tenant);
            adjustmentPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Adjustment_Delete,
                L("DeleteAdjustment"), multiTenancySides: MultiTenancySides.Tenant);
            //adjustmentPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Adjustment_Edit, L("EditAdjustment"), multiTenancySides: MultiTenancySides.Tenant);
            adjustmentPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Adjustment_Create,
                L("CreateAdjustment"), multiTenancySides: MultiTenancySides.Tenant);
            adjustmentPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Adjustment_Force,
                L("ForceAdjustmentWithDayClose"), multiTenancySides: MultiTenancySides.Tenant);

            //menuitemwatage

            var menuitemwastagePermission =
                movementHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Transaction_MenuItemWastage, L("MenuItemWastage"),
                    multiTenancySides: MultiTenancySides.Tenant);
            menuitemwastagePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_MenuItemWastage_Delete, L("DeleteMenuItemWastage"),
                multiTenancySides: MultiTenancySides.Tenant);
            menuitemwastagePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_MenuItemWastage_Edit, L("EditMenuItemWastage"),
                multiTenancySides: MultiTenancySides.Tenant);
            menuitemwastagePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_MenuItemWastage_Create, L("CreateMenuItemWastage"),
                multiTenancySides: MultiTenancySides.Tenant);

            //Closing stock

            var closingstockPermission =
                movementHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Transaction_ClosingStock, L("ClosingStock"),
                    multiTenancySides: MultiTenancySides.Tenant);
            closingstockPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_ClosingStock_Delete, L("DeleteClosingStock"),
                multiTenancySides: MultiTenancySides.Tenant);
            closingstockPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_ClosingStock_Edit, L("EditClosingStock"),
                multiTenancySides: MultiTenancySides.Tenant);
            closingstockPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_ClosingStock_Create, L("CreateClosingStock"),
                multiTenancySides: MultiTenancySides.Tenant);

            var closingstockEntryPermission =
                transactionHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Transaction_ClosingStockEntry, L("ClosingStockEntry"),
                    multiTenancySides: MultiTenancySides.Tenant);
            closingstockEntryPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_ClosingStockEntry_Delete, L("DeleteClosingStockEntry"),
                multiTenancySides: MultiTenancySides.Tenant);
            closingstockEntryPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_ClosingStockEntry_Edit, L("EditClosingStockEntry"),
                multiTenancySides: MultiTenancySides.Tenant);
            closingstockEntryPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_ClosingStockEntry_Create, L("CreateClosingStockEntry"),
                multiTenancySides: MultiTenancySides.Tenant);

            #endregion Movement

            #region Production

            var productionHousePermission =
                transactionHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Production,
                    L("Production"),
                    multiTenancySides: MultiTenancySides.Tenant);

            var yieldPermission =
                productionHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Yield,
                    L("Yield"), multiTenancySides: MultiTenancySides.Tenant);
            yieldPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Yield_Delete,
                L("DeleteYield"), multiTenancySides: MultiTenancySides.Tenant);
            yieldPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Yield_Edit,
                L("EditYield"), multiTenancySides: MultiTenancySides.Tenant);
            yieldPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Yield_Create,
                L("CreateYield"), multiTenancySides: MultiTenancySides.Tenant);

            var productionPermission =
                productionHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Transaction_Production, L("Production"),
                    multiTenancySides: MultiTenancySides.Tenant);
            productionPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Production_Delete,
                L("DeleteProduction"), multiTenancySides: MultiTenancySides.Tenant);
            productionPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Production_Edit,
                L("EditProduction"), multiTenancySides: MultiTenancySides.Tenant);
            productionPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_Production_Create,
                L("CreateProduction"), multiTenancySides: MultiTenancySides.Tenant);
            productionPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_Production_Allowed_Without_Issue,
                L("ProductionAllowedWithOutIssue"), multiTenancySides: MultiTenancySides.Tenant);
            productionPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_Automatic_Issue_Based_On_Production,
                L("AutomaticIssueForProduction"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion Production

            #region Transfer

            // House Transfer Permission
            var transferHousePermission =
                transactionHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transfer,
                    L("Transfer"),
                    multiTenancySides: MultiTenancySides.Tenant);

            //  Inter Transfer
            var intertransferPermission =
                transferHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Transaction_InterTransfer, L("InterTransferRequest"),
                    multiTenancySides: MultiTenancySides.Tenant);

            intertransferPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InterTransfer_Delete, L("DeleteInterTransferRequest"),
                multiTenancySides: MultiTenancySides.Tenant);
            intertransferPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InterTransfer_Edit, L("EditInterTransferRequest"),
                multiTenancySides: MultiTenancySides.Tenant);
            intertransferPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InterTransfer_Create, L("CreateInterTransferRequest"),
                multiTenancySides: MultiTenancySides.Tenant);
            intertransferPermission.CreateChildPermission(
              AppPermissions.Pages_Tenant_House_Transaction_InterTransfer_Approve, L("Approve"),
              multiTenancySides: MultiTenancySides.Tenant);
            intertransferPermission.CreateChildPermission(
               AppPermissions.Pages_Tenant_House_Transaction_InterTransfer_GracePeriodAllowed, L("GracePeriodAllowed"),
               multiTenancySides: MultiTenancySides.Tenant);
            intertransferPermission.CreateChildPermission(
               AppPermissions.Pages_Tenant_House_Transaction_InterTransfer_EditDeliveryDate, L("EditDeliveryDate"),
               multiTenancySides: MultiTenancySides.Tenant);


            //  Inter Transfer Approval
            var intertransferapprovalPermission =
                transferHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Transaction_InterTransferApproval, L("InterTransferApproval"),
                    multiTenancySides: MultiTenancySides.Tenant);
            intertransferapprovalPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InterTransferApproval_Delete,
                L("DeleteInterTransferApproval"), multiTenancySides: MultiTenancySides.Tenant);
            intertransferapprovalPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InterTransferApproval_Edit, L("EditInterTransferApproval"),
                multiTenancySides: MultiTenancySides.Tenant);
            intertransferapprovalPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InterTransferApproval_Create,
                L("CreateInterTransferApproval"), multiTenancySides: MultiTenancySides.Tenant);
            intertransferapprovalPermission.CreateChildPermission(
               AppPermissions.Pages_Tenant_House_Transaction_InterTransferApproval_CanChangeRequestQuantity, L("CanChangeRequestQtyWhileApproval"),
               multiTenancySides: MultiTenancySides.Tenant); 
            intertransferapprovalPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InterTransferApproval_Approve, L("InterTransferApproval"),
                multiTenancySides: MultiTenancySides.Tenant);
            intertransferapprovalPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InterTransferApproval_TransportDetail,
                L("TransportDetailAllowed"),
                multiTenancySides: MultiTenancySides.Tenant);

            //  Inter Transfer Received
            var intertransferreceivedPermission =
                transferHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Transaction_InterTransferReceived, L("InterTransferReceived"),
                    multiTenancySides: MultiTenancySides.Tenant);
            intertransferreceivedPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InterTransferReceived_Delete,
                L("DeleteInterTransferReceived"), multiTenancySides: MultiTenancySides.Tenant);
            intertransferreceivedPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InterTransferReceived_Edit, L("EditInterTransferReceived"),
                multiTenancySides: MultiTenancySides.Tenant);
            intertransferreceivedPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InterTransferReceived_Received,
                L("CreateInterTransferReceived"),
                multiTenancySides: MultiTenancySides.Tenant);

            //return

            #endregion Transfer

            #region CustomerSales

            // MOVEMENT Permission
            var customerSalesHousePermission =
                transactionHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Sales, L("Sales"),
                    multiTenancySides: MultiTenancySides.Tenant);

            //customer

            var customerPermission =
                customerSalesHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Customer,
                    L("Customer"), multiTenancySides: MultiTenancySides.Tenant);
            customerPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Customer_Delete,
                L("DeleteCustomer"), multiTenancySides: MultiTenancySides.Tenant);
            customerPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Customer_Edit,
                L("EditCustomer"), multiTenancySides: MultiTenancySides.Tenant);
            customerPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Customer_Create,
                L("CreateCustomer"), multiTenancySides: MultiTenancySides.Tenant);

            //CustomerMaterial

            var customermaterialPermission =
                customerSalesHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Master_CustomerMaterial,
                    L("CustomerMaterial"), multiTenancySides: MultiTenancySides.Tenant);
            customermaterialPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_CustomerMaterial_Delete, L("DeleteCustomerMaterial"),
                multiTenancySides: MultiTenancySides.Tenant);
            customermaterialPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_CustomerMaterial_Edit, L("EditCustomerMaterial"),
                multiTenancySides: MultiTenancySides.Tenant);
            customermaterialPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_CustomerMaterial_Create, L("CreateCustomerMaterial"),
                multiTenancySides: MultiTenancySides.Tenant);

            //Customer Tag Definition

            var customertagdefinitionPermission =
                customerSalesHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Master_CustomerTagDefinition, L("CustomerTagDefinition"),
                    multiTenancySides: MultiTenancySides.Tenant);
            customertagdefinitionPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_CustomerTagDefinition_Delete, L("DeleteCustomerTagDefinition"),
                multiTenancySides: MultiTenancySides.Tenant);
            customertagdefinitionPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_CustomerTagDefinition_Edit, L("EditCustomerTagDefinition"),
                multiTenancySides: MultiTenancySides.Tenant);
            customertagdefinitionPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_CustomerTagDefinition_Create, L("CreateCustomerTagDefinition"),
                multiTenancySides: MultiTenancySides.Tenant);

            //Customer Material Tag Price Definition

            var customertagmaterialpricePermission =
                customerSalesHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Master_CustomerTagMaterialPrice, L("CustomerTagMaterialPrice"),
                    multiTenancySides: MultiTenancySides.Tenant);
            //    customertagmaterialpricePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_CustomerTagMaterialPrice_Delete, L("DeleteCustomerTagMaterialPrice"), multiTenancySides: MultiTenancySides.Tenant);
            customertagmaterialpricePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_CustomerTagMaterialPrice_Edit,
                L("EditCustomerTagMaterialPrice"), multiTenancySides: MultiTenancySides.Tenant);
            // customertagmaterialpricePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_CustomerTagMaterialPrice_Create, L("CreateCustomerTagMaterialPrice"), multiTenancySides: MultiTenancySides.Tenant);

            //SalesTax

            var salestaxPermission = customerSalesHousePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_SalesTax, L("SalesTax"),
                multiTenancySides: MultiTenancySides.Tenant);
            salestaxPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_SalesTax_Delete,
                L("DeleteSalesTax"),
                multiTenancySides: MultiTenancySides.Tenant);
            salestaxPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_SalesTax_Edit,
                L("EditSalesTax"),
                multiTenancySides: MultiTenancySides.Tenant);
            salestaxPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_SalesTax_Create,
                L("CreateSalesTax"),
                multiTenancySides: MultiTenancySides.Tenant);

            //Sales Order

            var salesorderPermission =
                customerSalesHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Transaction_SalesOrder, L("SalesOrder"),
                    multiTenancySides: MultiTenancySides.Tenant);
            salesorderPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_SalesOrder_Delete,
                L("DeleteSalesOrder"), multiTenancySides: MultiTenancySides.Tenant);
            salesorderPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_SalesOrder_Edit,
                L("EditSalesOrder"), multiTenancySides: MultiTenancySides.Tenant);
            salesorderPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_SalesOrder_Create,
                L("CreateSalesOrder"), multiTenancySides: MultiTenancySides.Tenant);
            salesorderPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_SalesOrder_Approve, L("ApproveSalesOrder"),
                multiTenancySides: MultiTenancySides.Tenant);
            //Sales Delivery Order

            var salesdeliveryorderPermission =
                customerSalesHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Transaction_SalesDeliveryOrder, L("SalesDeliveryOrder"),
                    multiTenancySides: MultiTenancySides.Tenant);
            salesdeliveryorderPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_SalesDeliveryOrder_Delete, L("DeleteSalesDeliveryOrder"),
                multiTenancySides: MultiTenancySides.Tenant);
            salesdeliveryorderPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_SalesDeliveryOrder_Edit, L("EditSalesDeliveryOrder"),
                multiTenancySides: MultiTenancySides.Tenant);
            salesdeliveryorderPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_SalesDeliveryOrder_Create, L("CreateSalesDeliveryOrder"),
                multiTenancySides: MultiTenancySides.Tenant);
            //SaleInvoice

            var salesinvoicePermission =
                customerSalesHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Transaction_SalesInvoice, L("SalesInvoice"),
                    multiTenancySides: MultiTenancySides.Tenant);
            salesinvoicePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_SalesInvoice_Delete, L("DeleteSalesInvoice"),
                multiTenancySides: MultiTenancySides.Tenant);
            salesinvoicePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_SalesInvoice_Edit, L("EditSalesInvoice"),
                multiTenancySides: MultiTenancySides.Tenant);
            salesinvoicePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_SalesInvoice_Create, L("CreateSalesInvoice"),
                multiTenancySides: MultiTenancySides.Tenant);

            #endregion CustomerSales

            var dayclosePermission =
                transactionHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Transaction_DayClose, L("DayClose"),
                    multiTenancySides: MultiTenancySides.Tenant);
            dayclosePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Transaction_DayClose_Create,
                L("CreateDayClose"), multiTenancySides: MultiTenancySides.Tenant);
            dayclosePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_DayClose_SameDayClose,
                L("SameDayClose"), multiTenancySides: MultiTenancySides.Tenant);
            dayclosePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_DayClose_MissingSalesIntoDayClose,
                L("IncludeSalesIntoClosedLedger"), multiTenancySides: MultiTenancySides.Tenant);

            var hideStockAndSalesPermission =
                transactionHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Transaction_DayCloseHideSalesAndStock, L("HideStockAndSales"),
                    multiTenancySides: MultiTenancySides.Tenant);
            hideStockAndSalesPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_DayClose_HideSales,
                L("HideSalesInDayClose"), multiTenancySides: MultiTenancySides.Tenant);
            hideStockAndSalesPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_DayClose_HideCarryOverStock,
                L("HideCarryOverStockInDayClose"), multiTenancySides: MultiTenancySides.Tenant);
            hideStockAndSalesPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_DayClose_HideStockInAdjustment,
                L("HideStockInAdjustment"), multiTenancySides: MultiTenancySides.Tenant);
            hideStockAndSalesPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_DayClose_HideStockInTransfer,
                L("HideStockInTransfer"), multiTenancySides: MultiTenancySides.Tenant);

            var multipleUOMAllowed =
                transactionHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Transaction_MultipleUOMAllowed, L("MultipleUOMEntryAllowed"),
                    multiTenancySides: MultiTenancySides.Tenant);

            var InterTransferDirectApprovalPermission =
                transferHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Transaction_InterTransferDirectApproval,
                    L("InterTransferDirectApproval"), multiTenancySides: MultiTenancySides.Tenant);
            InterTransferDirectApprovalPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InterTransferDirectApproval_Delete,
                L("DeleteInterTransferDirectApproval"), multiTenancySides: MultiTenancySides.Tenant);
            InterTransferDirectApprovalPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InterTransferDirectApproval_Edit,
                L("EditInterTransferDirectApproval"), multiTenancySides: MultiTenancySides.Tenant);
            InterTransferDirectApprovalPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InterTransferDirectApproval_Create,
                L("CreateInterTransferDirectApproval"), multiTenancySides: MultiTenancySides.Tenant);

            InterTransferDirectApprovalPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InterTransferApproval_DirectApprove,
                L("InterTransferDirectApproval"),
                multiTenancySides: MultiTenancySides.Tenant);
            InterTransferDirectApprovalPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InterTransferApproval_DirectApproveAndReceive,
                L("InterTransferDirectApproveAndReceive"),
                multiTenancySides: MultiTenancySides.Tenant);
            InterTransferDirectApprovalPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_InterTransferApproval_RequestForOtherLocation,
                L("InterTransferRequestForOtherLocation"),
                multiTenancySides: MultiTenancySides.Tenant);

            //  Production Unit
            var productionunitPermission =
                masterHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_ProductionUnit,
                    L("ProductionUnit"), multiTenancySides: MultiTenancySides.Tenant);
            productionunitPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_ProductionUnit_Delete, L("DeleteProductionUnit"),
                multiTenancySides: MultiTenancySides.Tenant);
            productionunitPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_ProductionUnit_Edit, L("EditProductionUnit"),
                multiTenancySides: MultiTenancySides.Tenant);
            productionunitPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_ProductionUnit_Create, L("CreateProductionUnit"),
                multiTenancySides: MultiTenancySides.Tenant);

            //  Purchase Return
            var purchasereturnPermission =
                purchaseHousePermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_House_Transaction_PurchaseReturn, L("PurchaseReturn"),
                    multiTenancySides: MultiTenancySides.Tenant);
            purchasereturnPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_PurchaseReturn_Delete, L("DeletePurchaseReturn"),
                multiTenancySides: MultiTenancySides.Tenant);
            purchasereturnPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_PurchaseReturn_Edit, L("EditPurchaseReturn"),
                multiTenancySides: MultiTenancySides.Tenant);
            purchasereturnPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_PurchaseReturn_EditPrice, L("EditPurchaseReturnPrice"),
                multiTenancySides: MultiTenancySides.Tenant);
            purchasereturnPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Transaction_PurchaseReturn_Create, L("CreatePurchaseReturn"),
                multiTenancySides: MultiTenancySides.Tenant);

            var inventorycycletagPermission =
                masterHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_InventoryCycleTag,
                    L("InventoryCycleTag"), multiTenancySides: MultiTenancySides.Tenant);
            inventorycycletagPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_InventoryCycleTag_Delete, L("DeleteInventoryCycleTag"),
                multiTenancySides: MultiTenancySides.Tenant);
            inventorycycletagPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_InventoryCycleTag_Edit, L("EditInventoryCycleTag"),
                multiTenancySides: MultiTenancySides.Tenant);
            inventorycycletagPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_House_Master_InventoryCycleTag_Create, L("CreateInventoryCycleTag"),
                multiTenancySides: MultiTenancySides.Tenant);

            #endregion House
            #region Cluster
            var clusterPermission = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster, L("Cluster"),
               multiTenancySides: MultiTenancySides.Tenant);
            // Cluster Master Permission
            var masterClusterPermission = clusterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master,
                L("Master"),
                multiTenancySides: MultiTenancySides.Tenant);


            var delagglocationgroupPermission = masterClusterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocationGroup, L("DelAggLocationGroup"), multiTenancySides: MultiTenancySides.Tenant);
            delagglocationgroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocationGroup_Delete, L("DeleteDelAggLocationGroup"), multiTenancySides: MultiTenancySides.Tenant);
            delagglocationgroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocationGroup_Edit, L("EditDelAggLocationGroup"), multiTenancySides: MultiTenancySides.Tenant);
            delagglocationgroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocationGroup_Create, L("CreateDelAggLocationGroup"), multiTenancySides: MultiTenancySides.Tenant);

            var delagglocationPermission = masterClusterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocation, L("DelAggLocation"), multiTenancySides: MultiTenancySides.Tenant);
            delagglocationPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocation_Delete, L("DeleteDelAggLocation"), multiTenancySides: MultiTenancySides.Tenant);
            delagglocationPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocation_Edit, L("EditDelAggLocation"), multiTenancySides: MultiTenancySides.Tenant);
            delagglocationPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocation_Create, L("CreateDelAggLocation"), multiTenancySides: MultiTenancySides.Tenant);

            var delagglocmappingPermission = masterClusterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocMapping, L("DelAggLocMapping"), multiTenancySides: MultiTenancySides.Tenant);
            delagglocmappingPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocMapping_Delete, L("DeleteDelAggLocMapping"), multiTenancySides: MultiTenancySides.Tenant);
            delagglocmappingPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocMapping_Edit, L("EditDelAggLocMapping"), multiTenancySides: MultiTenancySides.Tenant);
            delagglocmappingPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocMapping_Create, L("CreateDelAggLocMapping"), multiTenancySides: MultiTenancySides.Tenant);

            var delagglocationitemPermission = masterClusterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocationItem, L("DelAggLocationItem"),multiTenancySides: MultiTenancySides.Tenant);
            delagglocationitemPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocationItem_Delete, L("DeleteDelAggLocationItem"), multiTenancySides: MultiTenancySides.Tenant);
            delagglocationitemPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocationItem_Edit, L("EditDelAggLocationItem"), multiTenancySides: MultiTenancySides.Tenant);
            delagglocationitemPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggLocationItem_Create, L("CreateDelAggLocationItem"), multiTenancySides: MultiTenancySides.Tenant);

            var delaggvariantgroupPermission = masterClusterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggVariantGroup, L("DelAggVariantGroup"), multiTenancySides: MultiTenancySides.Tenant);
            delaggvariantgroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggVariantGroup_Delete, L("DeleteDelAggVariantGroup"), multiTenancySides: MultiTenancySides.Tenant);
            delaggvariantgroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggVariantGroup_Edit, L("EditDelAggVariantGroup"), multiTenancySides: MultiTenancySides.Tenant);
            delaggvariantgroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggVariantGroup_Create, L("CreateDelAggVariantGroup"), multiTenancySides: MultiTenancySides.Tenant);

            var delaggmodifiergroupPermission = masterClusterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggModifierGroup, L("DelAggModifierGroup"), multiTenancySides: MultiTenancySides.Tenant);
            delaggmodifiergroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggModifierGroup_Delete, L("DeleteDelAggModifierGroup"), multiTenancySides: MultiTenancySides.Tenant);
            delaggmodifiergroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggModifierGroup_Edit, L("EditDelAggModifierGroup"), multiTenancySides: MultiTenancySides.Tenant);
            delaggmodifiergroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggModifierGroup_Create, L("CreateDelAggModifierGroup"), multiTenancySides: MultiTenancySides.Tenant);

            var delaggvariantPermission = masterClusterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggVariant, L("DelAggVariant"), multiTenancySides: MultiTenancySides.Tenant);
            delaggvariantPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggVariant_Delete, L("DeleteDelAggVariant"), multiTenancySides: MultiTenancySides.Tenant);
            delaggvariantPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggVariant_Edit, L("EditDelAggVariant"), multiTenancySides: MultiTenancySides.Tenant);
            delaggvariantPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggVariant_Create, L("CreateDelAggVariant"), multiTenancySides: MultiTenancySides.Tenant);

            var delaggmodifierPermission = masterClusterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggModifier, L("DelAggModifier"), multiTenancySides: MultiTenancySides.Tenant);
            delaggmodifierPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggModifier_Delete, L("DeleteDelAggModifier"), multiTenancySides: MultiTenancySides.Tenant);
            delaggmodifierPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggModifier_Edit, L("EditDelAggModifier"), multiTenancySides: MultiTenancySides.Tenant);
            delaggmodifierPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggModifier_Create, L("CreateDelAggModifier"), multiTenancySides: MultiTenancySides.Tenant);

            var delaggitemPermission = masterClusterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggItem, L("DelAggItem"), multiTenancySides: MultiTenancySides.Tenant);
            delaggitemPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggItem_Delete, L("DeleteDelAggItem"), multiTenancySides: MultiTenancySides.Tenant);
            delaggitemPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggItem_Edit, L("EditDelAggItem"), multiTenancySides: MultiTenancySides.Tenant);
            delaggitemPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggItem_Create, L("CreateDelAggItem"), multiTenancySides: MultiTenancySides.Tenant);


            var delaggitemgroupPermission = masterClusterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggItemGroup, L("DelAggItemGroup"), multiTenancySides: MultiTenancySides.Tenant);
            delaggitemgroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggItemGroup_Delete, L("DeleteDelAggItemGroup"), multiTenancySides: MultiTenancySides.Tenant);
            delaggitemgroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggItemGroup_Edit, L("EditDelAggItemGroup"), multiTenancySides: MultiTenancySides.Tenant);
            delaggitemgroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggItemGroup_Create, L("CreateDelAggItemGroup"), multiTenancySides: MultiTenancySides.Tenant);

            var delaggtaxPermission = masterClusterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggTax, L("DelAggTax"), multiTenancySides: MultiTenancySides.Tenant);
            delaggtaxPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggTax_Delete, L("DeleteDelAggTax"), multiTenancySides: MultiTenancySides.Tenant);
            delaggtaxPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggTax_Edit, L("EditDelAggTax"), multiTenancySides: MultiTenancySides.Tenant);
            delaggtaxPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_Master_DelAggTax_Create, L("CreateDelAggTax"), multiTenancySides: MultiTenancySides.Tenant);

            var deltiminggroupPermission = masterClusterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_DelTimingGroup, L("DelTimingGroup"), multiTenancySides: MultiTenancySides.Tenant);
            deltiminggroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_DelTimingGroup_Delete, L("DeleteDelTimingGroup"), multiTenancySides: MultiTenancySides.Tenant);
            deltiminggroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_DelTimingGroup_Edit, L("EditDelTimingGroup"), multiTenancySides: MultiTenancySides.Tenant);
            deltiminggroupPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_DelTimingGroup_Create, L("CreateDelTimingGroup"), multiTenancySides: MultiTenancySides.Tenant);

            var delaggcategoryPermission = masterClusterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_DelAggCategory, L("DelAggCategory"), multiTenancySides: MultiTenancySides.Tenant);
            delaggcategoryPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_DelAggCategory_Delete, L("DeleteDelAggCategory"), multiTenancySides: MultiTenancySides.Tenant);
            delaggcategoryPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_DelAggCategory_Edit, L("EditDelAggCategory"), multiTenancySides: MultiTenancySides.Tenant);
            delaggcategoryPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_DelAggCategory_Create, L("CreateDelAggCategory"), multiTenancySides: MultiTenancySides.Tenant);

            var delagglanguages = masterClusterPermission.CreateChildPermission(AppPermissions.Pages_Administration_DelAggLanguages,
                L("DelAggLanguages")) ;
            delagglanguages.CreateChildPermission(AppPermissions.Pages_Administration_DelAggLanguages_Create,
                L("CreateNewDelAggLanguage"));
            delagglanguages.CreateChildPermission(AppPermissions.Pages_Administration_DelAggLanguages_Edit, L("EditDelAggLanguage"));
            delagglanguages.CreateChildPermission(AppPermissions.Pages_Administration_DelAggLanguages_Delete, L("DeleteDelAggLanguage"));
            delagglanguages.CreateChildPermission(AppPermissions.Pages_Administration_DelAggLanguages_ChangeTexts,
                L("ChangingTexts"));

            var delaggchargePermission = masterClusterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_DelAggCharge, L("DelAggCharge"), multiTenancySides: MultiTenancySides.Tenant);
            delaggchargePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_DelAggCharge_Delete, L("DeleteDelAggCharge"), multiTenancySides: MultiTenancySides.Tenant);
            delaggchargePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_DelAggCharge_Edit, L("EditDelAggCharge"), multiTenancySides: MultiTenancySides.Tenant);
            delaggchargePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_DelAggCharge_Create, L("CreateDelAggCharge"), multiTenancySides: MultiTenancySides.Tenant);

            var delaggimagePermission = masterClusterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_DelAggImage, L("DelAggImage"), multiTenancySides: MultiTenancySides.Tenant);
            delaggimagePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_DelAggImage_Delete, L("DeleteDelAggImage"), multiTenancySides: MultiTenancySides.Tenant);
            delaggimagePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_DelAggImage_Edit, L("EditDelAggImage"), multiTenancySides: MultiTenancySides.Tenant);
            delaggimagePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cluster_DelAggImage_Create, L("CreateDelAggImage"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion Cluster

            #region Engage

            //Engage Permissions
            var engagePermission = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Engage, L("Engage"),
                multiTenancySides: MultiTenancySides.Tenant);

            var giftvouchertypePermission =
                engagePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_GiftVoucherType,
                    L("GiftVoucherType"), multiTenancySides: MultiTenancySides.Tenant);
            giftvouchertypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_GiftVoucherType_Delete,
                L("DeleteGiftVoucherType"), multiTenancySides: MultiTenancySides.Tenant);
            giftvouchertypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_GiftVoucherType_Edit,
                L("EditGiftVoucherType"), multiTenancySides: MultiTenancySides.Tenant);
            giftvouchertypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_GiftVoucherType_Create,
                L("CreateGiftVoucherType"), multiTenancySides: MultiTenancySides.Tenant);

            var membershipPermission = engagePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Engage_Membership,
                L("Membership"),
                multiTenancySides:MultiTenancySides.Tenant);

            #region Member Permissions
            var memberPermission = membershipPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Engage_Member,
                L("Member"),
                multiTenancySides: MultiTenancySides.Tenant);

                memberPermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_Engage_Member_Delete,
                    L("DeleteMember"),
                    multiTenancySides: MultiTenancySides.Tenant);

                memberPermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_Engage_Member_Edit,
                    L("EditMember"),
                    multiTenancySides: MultiTenancySides.Tenant);

                memberPermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_Engage_Member_Create,
                    L("CreateMember"),
                    multiTenancySides: MultiTenancySides.Tenant);

                memberPermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_Engage_MemberPoint_Delete,
                    L("DeleteMemberPoint"),
                    multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            #region Member Account Permissions
            var memberAccountPermission = membershipPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Engage_Member_Account,
                L("Account"),
                multiTenancySides: MultiTenancySides.Tenant
                );

                memberAccountPermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_Engage_Member_Account_Create,
                    L("CreateAccount"),
                    multiTenancySides: MultiTenancySides.Tenant
                    );
                memberAccountPermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_Engage_Member_Account_Edit,
                    L("EditAccount"),
                    multiTenancySides: MultiTenancySides.Tenant
                    );
                memberAccountPermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_Engage_Member_Account_Delete,
                    L("DeleteAccount"),
                    multiTenancySides: MultiTenancySides.Tenant
                    );
            #endregion

            #region Membership Tier Permissions
            var membershipTierPermission = membershipPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Engage_MembershipTier,
                L("MembershipTier"),
                multiTenancySides: MultiTenancySides.Tenant
                );
                membershipTierPermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_Engage_MembershipTier_Create,
                    L("CreateMembershipTier"),
                    multiTenancySides: MultiTenancySides.Tenant
                    );
                membershipTierPermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_Engage_MembershipTier_Edit,
                    L("EditMembershipTier"),
                    multiTenancySides: MultiTenancySides.Tenant
                    );
                membershipTierPermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_Engage_MembershipTier_Delete,
                    L("DeleteMembershipTier"),
                    multiTenancySides: MultiTenancySides.Tenant
                    );
            #endregion

            var serviceInfoPermission = memberPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Engage_ServiceInfo,
                L("ServiceInfo"),
                multiTenancySides: MultiTenancySides.Tenant
                );
            var serviceInfoTemplatePermission = serviceInfoPermission.CreateChildPermission(
                    AppPermissions.Pages_Tenant_Engage_ServiceInfo_Template,
                    L("ServiceInfoTemplate"),
                    multiTenancySides: MultiTenancySides.Tenant
                );
            serviceInfoTemplatePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Engage_ServiceInfo_Template_Create,
                L("CreateServiceInfoTemplate"),
                multiTenancySides: MultiTenancySides.Tenant
                );
            serviceInfoTemplatePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Engage_ServiceInfo_Template_Edit,
                L("EditServiceInfoTemplate"),
                multiTenancySides: MultiTenancySides.Tenant
                );
            serviceInfoTemplatePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Engage_ServiceInfo_Template_Delete,
                L("DeleteServiceInfoTemplate"),
                multiTenancySides: MultiTenancySides.Tenant
                );

            // Reservation

            var reservationPermission =
                engagePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_Reservation, L("Reservation"),
                    multiTenancySides: MultiTenancySides.Tenant);
            reservationPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_Reservation_Delete,
                L("DeleteReservation"), multiTenancySides: MultiTenancySides.Tenant);
            reservationPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_Reservation_Edit,
                L("EditReservation"), multiTenancySides: MultiTenancySides.Tenant);
            reservationPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_Reservation_Create,
                L("CreateReservation"), multiTenancySides: MultiTenancySides.Tenant);

            var loyaltyProgramPermission = engagePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_LoyaltyProgram,
                L("LoyaltyProgram"), multiTenancySides: MultiTenancySides.Tenant);
            loyaltyProgramPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_LoyaltyProgram_Delete,
                L("DeleteLoyaltyProgram"), multiTenancySides: MultiTenancySides.Tenant);
            loyaltyProgramPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_LoyaltyProgram_Edit,
                L("EditLoyaltyProgram"), multiTenancySides: MultiTenancySides.Tenant);
            loyaltyProgramPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_LoyaltyProgram_Create,
                L("CreateLoyaltyProgram"), multiTenancySides: MultiTenancySides.Tenant);

            var stagePermission = engagePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_Stage,
                L("Stage"), multiTenancySides: MultiTenancySides.Tenant);
            stagePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_Stage_Delete,
                L("DeleteStage"), multiTenancySides: MultiTenancySides.Tenant);
            stagePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_Stage_Edit,
                L("EditStage"), multiTenancySides: MultiTenancySides.Tenant);
            stagePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_Stage_Create,
                L("CreateStage"), multiTenancySides: MultiTenancySides.Tenant);

            var giftvouchercategoryPermission = engagePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_GiftVoucherCategory, L("GiftVoucherCategory"), multiTenancySides: MultiTenancySides.Tenant);
            giftvouchercategoryPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_GiftVoucherCategory_Delete, L("DeleteGiftVoucherCategory"), multiTenancySides: MultiTenancySides.Tenant);
            giftvouchercategoryPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_GiftVoucherCategory_Edit, L("EditGiftVoucherCategory"), multiTenancySides: MultiTenancySides.Tenant);
            giftvouchercategoryPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Engage_GiftVoucherCategory_Create, L("CreateGiftVoucherCategory"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion Engage

            #region Wheel

            //Wheel Permissions
            var wheelPermission = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Wheel, L("Wheel"),
                multiTenancySides: MultiTenancySides.Tenant);

            wheelPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Wheel_Dashboard,
                L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);
            wheelPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Wheel_Order,
                L("Order"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion Wheel

            #region Swipe

            //Swipe Permission

            var swipePermission = pages.CreateChildPermission(AppPermissions.Pages_Host_Swipe, L("Swipe"),
                multiTenancySides: MultiTenancySides.Tenant);

            swipePermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Dashboard,
                L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);

            var masterSwipePermission = swipePermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Master,
                L("Master"), multiTenancySides: MultiTenancySides.Tenant);

            var swipecardPermission =
                masterSwipePermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Master_SwipeCard,
                    L("SwipeCard"), multiTenancySides: MultiTenancySides.Tenant);

            swipecardPermission.CreateChildPermission(
                AppPermissions.Pages_Host_Swipe_Master_SwipeCard_CanOverRide_CardType, L("CanOverRide_CardType"),
                multiTenancySides: MultiTenancySides.Tenant);
            swipecardPermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Master_SwipeCard_Delete,
                L("DeleteSwipeCard"), multiTenancySides: MultiTenancySides.Tenant);
            swipecardPermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Master_SwipeCard_Edit,
                L("EditSwipeCard"), multiTenancySides: MultiTenancySides.Tenant);
            swipecardPermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Master_SwipeCard_Create,
                L("CreateSwipeCard"), multiTenancySides: MultiTenancySides.Tenant);

            var swipecardtypePermission =
                masterSwipePermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Master_SwipeCardType,
                    L("SwipeCardType"), multiTenancySides: MultiTenancySides.Tenant);
            swipecardtypePermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Master_SwipeCardType_Delete,
                L("DeleteSwipeCardType"), multiTenancySides: MultiTenancySides.Tenant);
            swipecardtypePermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Master_SwipeCardType_Lock,
                L("LockSwipeCardType"), multiTenancySides: MultiTenancySides.Tenant);
            swipecardtypePermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Master_SwipeCardType_Edit,
                L("EditSwipeCardType"), multiTenancySides: MultiTenancySides.Tenant);
            swipecardtypePermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Master_SwipeCardType_Create,
                L("CreateSwipeCardType"), multiTenancySides: MultiTenancySides.Tenant);

            var swipepaymenttypePermission =
                masterSwipePermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Master_SwipePaymentType,
                    L("SwipePaymentType"), multiTenancySides: MultiTenancySides.Tenant);
            swipepaymenttypePermission.CreateChildPermission(
                AppPermissions.Pages_Host_Swipe_Master_SwipePaymentType_Delete, L("DeleteSwipePaymentType"),
                multiTenancySides: MultiTenancySides.Tenant);
            swipepaymenttypePermission.CreateChildPermission(
                AppPermissions.Pages_Host_Swipe_Master_SwipePaymentType_Edit, L("EditSwipePaymentType"),
                multiTenancySides: MultiTenancySides.Tenant);
            swipepaymenttypePermission.CreateChildPermission(
                AppPermissions.Pages_Host_Swipe_Master_SwipePaymentType_Create, L("CreateSwipePaymentType"),
                multiTenancySides: MultiTenancySides.Tenant);

            var swipememberPermission =
                masterSwipePermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Master_SwipeMember,
                    L("SwipeMember"), multiTenancySides: MultiTenancySides.Tenant);
            swipememberPermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Master_SwipeMember_Delete,
                L("DeleteSwipeMember"), multiTenancySides: MultiTenancySides.Tenant);
            swipememberPermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Master_SwipeMember_Edit,
                L("EditSwipeMember"), multiTenancySides: MultiTenancySides.Tenant);
            swipememberPermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Master_SwipeMember_Create,
                L("CreateSwipeMember"), multiTenancySides: MultiTenancySides.Tenant);

            var transactionSwipePermission =
                swipePermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Transaction,
                    L("Transaction"), multiTenancySides: MultiTenancySides.Tenant);

            var membercardPermission =
                transactionSwipePermission.CreateChildPermission(
                    AppPermissions.Pages_Host_Swipe_Transaction_MemberCard, L("MemberCard"),
                    multiTenancySides: MultiTenancySides.Tenant);
            membercardPermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Transaction_MemberCard_Delete,
                L("DeleteMemberCard"), multiTenancySides: MultiTenancySides.Tenant);
            membercardPermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Transaction_MemberCard_Edit,
                L("EditMemberCard"), multiTenancySides: MultiTenancySides.Tenant);
            membercardPermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Transaction_MemberCard_Create,
                L("CreateMemberCard"), multiTenancySides: MultiTenancySides.Tenant);

            var swipeshiftPermission =
                transactionSwipePermission.CreateChildPermission(
                    AppPermissions.Pages_Host_Swipe_Transaction_SwipeShift, L("SwipeShift"),
                    multiTenancySides: MultiTenancySides.Tenant);
            swipeshiftPermission.CreateChildPermission(
                AppPermissions.Pages_Host_Swipe_Transaction_SwipeShift_BlindShift, L("BlindShift"),
                multiTenancySides: MultiTenancySides.Tenant);
            swipeshiftPermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Transaction_SwipeShift_Create,
                L("StartShift"), multiTenancySides: MultiTenancySides.Tenant);
            swipeshiftPermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Transaction_SwipeShift_End,
                L("StopShift"), multiTenancySides: MultiTenancySides.Tenant);

            swipePermission.CreateChildPermission(AppPermissions.Pages_Host_Swipe_Report,
                L("Reports"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion Swipe

            #region Tick

            //TICK Permission

            var tickPermission = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Tick, L("Tick"),
                multiTenancySides: MultiTenancySides.Tenant);

            tickPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Tick_Dashboard, L("Dashboard"),
                multiTenancySides: MultiTenancySides.Tenant);

            //var mastPermi = tickPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Tick_Master, L("Master"),
            //    multiTenancySides: MultiTenancySides.Tenant);

            #region HR Under Tick

            //var hrPermission = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Hr, L("HR"),multiTenancySides: MultiTenancySides.Tenant);

            var hrmasterPermission = tickPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master,
                L("Master"),
                multiTenancySides: MultiTenancySides.Tenant);

            var hrAdminPermission = tickPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_AdminMaster,
               L("HRAdmin"),
               multiTenancySides: MultiTenancySides.Tenant);

            hrAdminPermission.CreateChildPermission(AppPermissions.Pages_Tenant_EmployeeDashboard, L("EmployeeDashboard"), multiTenancySides: MultiTenancySides.Tenant);
            hrAdminPermission.CreateChildPermission(AppPermissions.Pages_Tenant_EmployeeDashboard_ShowSalary, L("ShowSalaryInEmployeeDashBoard"), multiTenancySides: MultiTenancySides.Tenant);

            var hrtransactionPermission = tickPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Transaction,
              L("Transaction"),
              multiTenancySides: MultiTenancySides.Tenant);

            var skillsetPermission = hrAdminPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_SkillSet, L("SkillSet"), multiTenancySides: MultiTenancySides.Tenant);
            skillsetPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_SkillSet_Delete, L("DeleteSkillSet"), multiTenancySides: MultiTenancySides.Tenant);
            skillsetPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_SkillSet_Edit, L("EditSkillSet"), multiTenancySides: MultiTenancySides.Tenant);
            skillsetPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_SkillSet_Create, L("CreateSkillSet"), multiTenancySides: MultiTenancySides.Tenant);

            //personalinformation

            var personalinformationPermission = hrmasterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_PersonalInformation, L("PersonalInformation"), multiTenancySides: MultiTenancySides.Tenant);
            personalinformationPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_PersonalInformation_Delete, L("ResignationEntry"), multiTenancySides: MultiTenancySides.Tenant);
            personalinformationPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_PersonalInformation_Edit, L("EditPersonalInformation"), multiTenancySides: MultiTenancySides.Tenant);
            personalinformationPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_PersonalInformation_Create, L("CreatePersonalInformation"), multiTenancySides: MultiTenancySides.Tenant);
            personalinformationPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_PersonalInformation_Import, L("ImportPersonalInformation"), multiTenancySides: MultiTenancySides.Tenant);
            personalinformationPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_PersonalInformation_Clone, L("ClonePersonalInformation"), multiTenancySides: MultiTenancySides.Tenant);
            personalinformationPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_PersonalInformation_LeaveGrant, L("LeaveGrant"), multiTenancySides: MultiTenancySides.Tenant);
            personalinformationPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_PersonalInformation_ChangeEmployeeCode, L("ChangeEmployeeCode"), multiTenancySides: MultiTenancySides.Tenant);

            var employeeattendancePermission =
               hrmasterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Tick_EmployeeAttendance,
                   L("EmployeeAttendance"), multiTenancySides: MultiTenancySides.Tenant);
            employeeattendancePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Tick_EmployeeAttendance_Delete, L("DeleteEmployeeAttendance"),
                multiTenancySides: MultiTenancySides.Tenant);
            employeeattendancePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Tick_EmployeeAttendance_Edit, L("EditEmployeeAttendance"),
                multiTenancySides: MultiTenancySides.Tenant);
            employeeattendancePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Tick_EmployeeAttendance_Create, L("CreateEmployeeAttendance"),
                multiTenancySides: MultiTenancySides.Tenant);

            //employeeskillset
            var employeeskillsetPermission = hrmasterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_EmployeeSkillSet, L("EmployeeSkillSet"), multiTenancySides: MultiTenancySides.Tenant);
            employeeskillsetPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_EmployeeSkillSet_Delete, L("DeleteEmployeeSkillSet"), multiTenancySides: MultiTenancySides.Tenant);
            employeeskillsetPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_EmployeeSkillSet_Edit, L("EditEmployeeSkillSet"), multiTenancySides: MultiTenancySides.Tenant);
            employeeskillsetPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_EmployeeSkillSet_Create, L("CreateEmployeeSkillSet"), multiTenancySides: MultiTenancySides.Tenant);

            //EmployeeDocumentInfo
            var employeedocumentinfoPermission = hrmasterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_EmployeeDocumentInfo, L("EmployeeDocumentInfo"), multiTenancySides: MultiTenancySides.Tenant);
            employeedocumentinfoPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_EmployeeDocumentInfo_Delete, L("DeleteEmployeeDocumentInfo"), multiTenancySides: MultiTenancySides.Tenant);
            employeedocumentinfoPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_EmployeeDocumentInfo_Edit, L("EditEmployeeDocumentInfo"), multiTenancySides: MultiTenancySides.Tenant);
            employeedocumentinfoPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_EmployeeDocumentInfo_Create, L("CreateEmployeeDocumentInfo"), multiTenancySides: MultiTenancySides.Tenant);

            //Attendance Log
            var attendancelogPermission = hrmasterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_AttendanceLog, L("AttendanceLog"), multiTenancySides: MultiTenancySides.Tenant);
            attendancelogPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_AttendanceLog_Delete, L("DeleteAttendanceLog"), multiTenancySides: MultiTenancySides.Tenant);
            attendancelogPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_AttendanceLog_Edit, L("EditAttendanceLog"), multiTenancySides: MultiTenancySides.Tenant);
            attendancelogPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_AttendanceLog_Create, L("CreateAttendanceLog"), multiTenancySides: MultiTenancySides.Tenant);
            attendancelogPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_AttendanceLog_Import, L("ImportAttLog"), multiTenancySides: MultiTenancySides.Tenant);

            //  Document Infos

            var documentinfoPermission = hrAdminPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DocumentInfo, L("DocumentInfo"), multiTenancySides: MultiTenancySides.Tenant);
            documentinfoPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DocumentInfo_Delete, L("DeleteDocumentInfo"), multiTenancySides: MultiTenancySides.Tenant);
            documentinfoPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DocumentInfo_Edit, L("EditDocumentInfo"), multiTenancySides: MultiTenancySides.Tenant);
            documentinfoPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DocumentInfo_Create, L("CreateDocumentInfo"), multiTenancySides: MultiTenancySides.Tenant);

            //Salary Info

            var salaryinfoPermission = hrAdminPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_SalaryInfo, L("SalaryInfo"), multiTenancySides: MultiTenancySides.Tenant);
            salaryinfoPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_SalaryInfo_ShowSalary, L("ShowSalary"), multiTenancySides: MultiTenancySides.Tenant);
            salaryinfoPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_SalaryInfo_Delete, L("DeleteSalaryInfo"), multiTenancySides: MultiTenancySides.Tenant);
            salaryinfoPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_SalaryInfo_Edit, L("EditSalaryInfo"), multiTenancySides: MultiTenancySides.Tenant);
            salaryinfoPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_SalaryInfo_Create, L("CreateSalaryInfo"), multiTenancySides: MultiTenancySides.Tenant);

            //	Incentive Tags
            var incentivetagPermission = hrAdminPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Transaction_IncentiveTag, L("IncentiveTag"), multiTenancySides: MultiTenancySides.Tenant);
            incentivetagPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Transaction_IncentiveTag_Delete, L("DeleteIncentiveTag"), multiTenancySides: MultiTenancySides.Tenant);
            incentivetagPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Transaction_IncentiveTag_Edit, L("EditIncentiveTag"), multiTenancySides: MultiTenancySides.Tenant);
            incentivetagPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Transaction_IncentiveTag_Create, L("CreateIncentiveTag"), multiTenancySides: MultiTenancySides.Tenant);
            incentivetagPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Transaction_IncentiveTag_BulkUpdateForAllEligibleEmployee, L("BulkUpdateForEligibleEmployees"), multiTenancySides: MultiTenancySides.Tenant);

            var incentivecategoryPermission = hrAdminPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Transaction_IncentiveCategory, L("IncentiveCategory"), multiTenancySides: MultiTenancySides.Tenant);
            incentivecategoryPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Transaction_IncentiveCategory_Delete, L("DeleteIncentiveCategory"), multiTenancySides: MultiTenancySides.Tenant);
            incentivecategoryPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Transaction_IncentiveCategory_Edit, L("EditIncentiveCategory"), multiTenancySides: MultiTenancySides.Tenant);
            incentivecategoryPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Transaction_IncentiveCategory_Create, L("CreateIncentiveCategory"), multiTenancySides: MultiTenancySides.Tenant);

            var manualincentivePermission = hrAdminPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Transaction_ManualIncentive, L("ManualIncentive"), multiTenancySides: MultiTenancySides.Tenant);
            manualincentivePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Transaction_ManualIncentive_Delete, L("DeleteManualIncentive"), multiTenancySides: MultiTenancySides.Tenant);
            manualincentivePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Transaction_ManualIncentive_Edit, L("EditManualIncentive"), multiTenancySides: MultiTenancySides.Tenant);
            manualincentivePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Transaction_ManualIncentive_Create, L("CreateManualIncentive"), multiTenancySides: MultiTenancySides.Tenant);

            var manualReasonPermission = masterHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_ManualReason, L("ManualReason"), multiTenancySides: MultiTenancySides.Tenant);
            manualReasonPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_ManualReason_Delete, L("DeleteManualReason"), multiTenancySides: MultiTenancySides.Tenant);
            manualReasonPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_ManualReason_Edit, L("EditManualReason"), multiTenancySides: MultiTenancySides.Tenant);
            manualReasonPermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_ManualReason_Create, L("CreateManualReason"), multiTenancySides: MultiTenancySides.Tenant);

            // House Template
            var houseTemplatePermission = masterHousePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Template, L("Template"), multiTenancySides: MultiTenancySides.Tenant);
            houseTemplatePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Template_Delete, L("Delete"), multiTenancySides: MultiTenancySides.Tenant);
            houseTemplatePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Template_Edit, L("Edit"), multiTenancySides: MultiTenancySides.Tenant);
            houseTemplatePermission.CreateChildPermission(AppPermissions.Pages_Tenant_House_Master_Template_Create, L("Create"), multiTenancySides: MultiTenancySides.Tenant);

            //LeaveType

            var leavetypePermission = hrAdminPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_LeaveType, L("LeaveType"), multiTenancySides: MultiTenancySides.Tenant);
            leavetypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_LeaveType_Delete, L("DeleteLeaveType"), multiTenancySides: MultiTenancySides.Tenant);
            leavetypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_LeaveType_Edit, L("EditLeaveType"), multiTenancySides: MultiTenancySides.Tenant);
            leavetypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_LeaveType_Create, L("CreateLeaveType"), multiTenancySides: MultiTenancySides.Tenant);

            //LeaveRequest

            var leaverequestPermission = hrAdminPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_LeaveRequest, L("LeaveRequest"), multiTenancySides: MultiTenancySides.Tenant);
            leaverequestPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_LeaveRequest_Delete, L("DeleteLeaveRequest"), multiTenancySides: MultiTenancySides.Tenant);
            leaverequestPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_LeaveRequest_Edit, L("EditLeaveRequest"), multiTenancySides: MultiTenancySides.Tenant);
            leaverequestPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_LeaveRequest_Create, L("CreateLeaveRequest"), multiTenancySides: MultiTenancySides.Tenant);
            leaverequestPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_LeaveRequest_Print, L("PrintLeaveRequest"), multiTenancySides: MultiTenancySides.Tenant);

            var yearwiseleaveallowedforemployeePermission = hrAdminPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_YearWiseLeaveAllowedForEmployee, L("YearWiseLeaveAllowedForEmployee"), multiTenancySides: MultiTenancySides.Tenant);
            yearwiseleaveallowedforemployeePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_YearWiseLeaveAllowedForEmployee_Delete, L("DeleteYearWiseLeaveAllowedForEmployee"), multiTenancySides: MultiTenancySides.Tenant);
            yearwiseleaveallowedforemployeePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_YearWiseLeaveAllowedForEmployee_Edit, L("EditYearWiseLeaveAllowedForEmployee"), multiTenancySides: MultiTenancySides.Tenant);
            yearwiseleaveallowedforemployeePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_YearWiseLeaveAllowedForEmployee_Create, L("CreateYearWiseLeaveAllowedForEmployee"), multiTenancySides: MultiTenancySides.Tenant);

            //Work Days
            var workdayPermission = hrAdminPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_WorkDay, L("WorkDay"), multiTenancySides: MultiTenancySides.Tenant);

            workdayPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_WorkDay_Edit, L("EditWorkDay"), multiTenancySides: MultiTenancySides.Tenant);

            //Monthwise Work Days

            var monthwiseworkdayPermission = hrAdminPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_MonthWiseWorkDay, L("MonthWiseWorkDay"), multiTenancySides: MultiTenancySides.Tenant);
            monthwiseworkdayPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_MonthWiseWorkDay_Delete, L("DeleteMonthWiseWorkDay"), multiTenancySides: MultiTenancySides.Tenant);
            monthwiseworkdayPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_MonthWiseWorkDay_Edit, L("EditMonthWiseWorkDay"), multiTenancySides: MultiTenancySides.Tenant);
            monthwiseworkdayPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_MonthWiseWorkDay_Create, L("CreateMonthWiseWorkDay"), multiTenancySides: MultiTenancySides.Tenant);

            //  Public Holiday
            var publicholidayPermission = hrAdminPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_PublicHoliday, L("PublicHoliday"), multiTenancySides: MultiTenancySides.Tenant);
            publicholidayPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_PublicHoliday_Delete, L("DeletePublicHoliday"), multiTenancySides: MultiTenancySides.Tenant);
            publicholidayPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_PublicHoliday_Edit, L("EditPublicHoliday"), multiTenancySides: MultiTenancySides.Tenant);
            publicholidayPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_PublicHoliday_Create, L("CreatePublicHoliday"), multiTenancySides: MultiTenancySides.Tenant);

            var dcheadmasterPermission = hrAdminPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DcHeadMaster, L("DcHeadMaster"), multiTenancySides: MultiTenancySides.Tenant);
            dcheadmasterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DcHeadMaster_Delete, L("DeleteDcHeadMaster"), multiTenancySides: MultiTenancySides.Tenant);
            dcheadmasterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DcHeadMaster_Edit, L("EditDcHeadMaster"), multiTenancySides: MultiTenancySides.Tenant);
            dcheadmasterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DcHeadMaster_Create, L("CreateDcHeadMaster"), multiTenancySides: MultiTenancySides.Tenant);

            var dcgroupmasterPermission = hrAdminPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DcGroupMaster, L("DcGroupMaster"), multiTenancySides: MultiTenancySides.Tenant);
            dcgroupmasterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DcGroupMaster_Delete, L("DeleteDcGroupMaster"), multiTenancySides: MultiTenancySides.Tenant);
            dcgroupmasterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DcGroupMaster_Edit, L("EditDcGroupMaster"), multiTenancySides: MultiTenancySides.Tenant);
            dcgroupmasterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DcGroupMaster_Create, L("CreateDcGroupMaster"), multiTenancySides: MultiTenancySides.Tenant);

            var dcstationmasterPermission = hrAdminPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DcStationMaster, L("DcStationMaster"), multiTenancySides: MultiTenancySides.Tenant);
            dcstationmasterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DcStationMaster_Delete, L("DeleteDcStationMaster"), multiTenancySides: MultiTenancySides.Tenant);
            dcstationmasterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DcStationMaster_Edit, L("EditDcStationMaster"), multiTenancySides: MultiTenancySides.Tenant);
            dcstationmasterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DcStationMaster_Create, L("CreateDcStationMaster"), multiTenancySides: MultiTenancySides.Tenant);

            var dcshiftmasterPermission = hrAdminPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DcShiftMaster, L("DcShiftMaster"), multiTenancySides: MultiTenancySides.Tenant);
            dcshiftmasterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DcShiftMaster_Delete, L("DeleteDcShiftMaster"), multiTenancySides: MultiTenancySides.Tenant);
            dcshiftmasterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DcShiftMaster_Edit, L("EditDcShiftMaster"), multiTenancySides: MultiTenancySides.Tenant);
            dcshiftmasterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Master_DcShiftMaster_Create, L("CreateDcShiftMaster"), multiTenancySides: MultiTenancySides.Tenant);

            var dutychartPermission = hrtransactionPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Transaction_DutyChart, L("DutyChart"), multiTenancySides: MultiTenancySides.Tenant);
            dutychartPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Transaction_DutyChart_Delete, L("DeleteDutyChart"), multiTenancySides: MultiTenancySides.Tenant);
            dutychartPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Transaction_DutyChart_Edit, L("EditDutyChart"), multiTenancySides: MultiTenancySides.Tenant);
            dutychartPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Hr_Transaction_DutyChart_Create, L("CreateDutyChart"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion HR Under Tick

            //var departmentInfoPermission =
            //    mastPermi.CreateChildPermission(AppPermissions.Pages_Tenant_Tick_Employee_Department,
            //        L("DepartmentInfo"), multiTenancySides: MultiTenancySides.Tenant);
            //departmentInfoPermission.CreateChildPermission(
            //    AppPermissions.Pages_Tenant_Tick_Employee_Department_Delete,
            //    L("DeleteTickDepratment"),
            //    multiTenancySides: MultiTenancySides.Tenant);

            //departmentInfoPermission.CreateChildPermission(
            //    AppPermissions.Pages_Tenant_Tick_Employee_Department_Edit,
            //    L("EditTickDepratment"),
            //    multiTenancySides: MultiTenancySides.Tenant);

            //departmentInfoPermission.CreateChildPermission(
            //    AppPermissions.Pages_Tenant_Tick_Employee_Department_Create,
            //    L("CreateTickDepratment"),
            //    multiTenancySides: MultiTenancySides.Tenant);

            //var employeeinfoPermission =
            //    tickPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Tick_Employee,
            //        L("EmployeeInfo"), multiTenancySides: MultiTenancySides.Tenant);

            //employeeinfoPermission.CreateChildPermission(
            //    AppPermissions.Pages_Tenant_Tick_Employee_Delete, L("DeleteEmployee"),
            //    multiTenancySides: MultiTenancySides.Tenant);

            //employeeinfoPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Tick_Employee_Edit,
            //    L("EditEmployee"), multiTenancySides: MultiTenancySides.Tenant);

            //employeeinfoPermission.CreateChildPermission(
            //    AppPermissions.Pages_Tenant_Tick_Employee_Create, L("CreateEmployee"),
            //    multiTenancySides: MultiTenancySides.Tenant);

            //var ticketInfoPermission =
            //    tickPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Tick_Ticket,
            //        L("TicketInfo"), multiTenancySides: MultiTenancySides.Tenant);

            //ticketInfoPermission.CreateChildPermission(
            //    AppPermissions.Pages_Tenant_Tick_Ticket_Delete,
            //    L("DeleteTickTicket"),
            //    multiTenancySides: MultiTenancySides.Tenant);

            //ticketInfoPermission.CreateChildPermission(
            //    AppPermissions.Pages_Tenant_Tick_Ticket_Edit,
            //    L("EditTickTicket"),
            //    multiTenancySides: MultiTenancySides.Tenant);

            //ticketInfoPermission.CreateChildPermission(
            //    AppPermissions.Pages_Tenant_Tick_Ticket_Create,
            //    L("CreateTickTicket"),
            //    multiTenancySides: MultiTenancySides.Tenant);

            #endregion Tick

            #region Addons

            var addonPermission = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Addons, L("Addons"),
                multiTenancySides: MultiTenancySides.Tenant);

            #endregion Addons

            #region Cater

            //Cater Permissions
            var caterPermission = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Cater, L("Cater"),
                multiTenancySides: MultiTenancySides.Tenant);

            caterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cater_Dashboard,
                L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);

            var caterCustomer = caterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cater_Customer,
                L("Customer"), multiTenancySides: MultiTenancySides.Tenant);
            caterCustomer.CreateChildPermission(AppPermissions.Pages_Tenant_Cater_Customer_Delete,
            L("DeleteCustomer"), multiTenancySides: MultiTenancySides.Tenant);
            caterCustomer.CreateChildPermission(AppPermissions.Pages_Tenant_Cater_Customer_Edit,
                L("EditCustomer"), multiTenancySides: MultiTenancySides.Tenant);
            caterCustomer.CreateChildPermission(AppPermissions.Pages_Tenant_Cater_Customer_Create,
                L("CreateCustomer"), multiTenancySides: MultiTenancySides.Tenant);

            var caterQuotationHeader = caterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cater_QuotationHeader,
                L("Quotation"), multiTenancySides: MultiTenancySides.Tenant);
            caterQuotationHeader.CreateChildPermission(AppPermissions.Pages_Tenant_Cater_QuotationHeader_Delete,
                L("DeleteQuotation"), multiTenancySides: MultiTenancySides.Tenant);
            caterQuotationHeader.CreateChildPermission(AppPermissions.Pages_Tenant_Cater_QuotationHeader_Edit,
                L("EditQuotation"), multiTenancySides: MultiTenancySides.Tenant);
            caterQuotationHeader.CreateChildPermission(AppPermissions.Pages_Tenant_Cater_QuotationHeader_Create,
                L("CreateQuotation"), multiTenancySides: MultiTenancySides.Tenant);

            var caterQuotationSetting = caterPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Cater_QuotationHeader_Setting,
                L("Setting"), multiTenancySides: MultiTenancySides.Tenant);

            caterQuotationHeader.CreateChildPermission(AppPermissions.Pages_Tenant_Cater_QuotationHeader_Setting_Costing,
               L("Costing"), multiTenancySides: MultiTenancySides.Tenant);

            caterQuotationHeader.CreateChildPermission(AppPermissions.Pages_Tenant_Cater_QuotationHeader_Setting_CanGetOrderForOtherLocation,
               L("GetCaterOrderForOtherLocation"), multiTenancySides: MultiTenancySides.Tenant);

            caterQuotationHeader.CreateChildPermission(AppPermissions.Pages_Tenant_Cater_QuotationHeader_AutoPo,
             L("AutoPO"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion Cater

            #region DineGo

            //Go Permissions
            var goPermission = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Go, L("Go"),
                multiTenancySides: MultiTenancySides.Tenant);

            var dinegobrandPermission = goPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoBrand, L("Brand"), multiTenancySides: MultiTenancySides.Tenant);
            dinegobrandPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoBrand_Delete, L("DeleteDineGoBrand"), multiTenancySides: MultiTenancySides.Tenant);
            dinegobrandPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoBrand_Edit, L("EditDineGoBrand"), multiTenancySides: MultiTenancySides.Tenant);
            dinegobrandPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoBrand_Create, L("CreateDineGoBrand"), multiTenancySides: MultiTenancySides.Tenant);

            var dinegodevicePermission = goPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoDevice, L("Device"), multiTenancySides: MultiTenancySides.Tenant);
            dinegodevicePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoDevice_Delete, L("DeleteDineGoDevice"), multiTenancySides: MultiTenancySides.Tenant);
            dinegodevicePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoDevice_Edit, L("EditDineGoDevice"), multiTenancySides: MultiTenancySides.Tenant);
            dinegodevicePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoDevice_Create, L("CreateDineGoDevice"), multiTenancySides: MultiTenancySides.Tenant);

            var dinegodepartmentPermission = goPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoDepartment, L("Department"), multiTenancySides: MultiTenancySides.Tenant);
            dinegodepartmentPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoDepartment_Delete, L("DeleteDineGoDepartment"), multiTenancySides: MultiTenancySides.Tenant);
            dinegodepartmentPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoDepartment_Edit, L("EditDineGoDepartment"), multiTenancySides: MultiTenancySides.Tenant);
            dinegodepartmentPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoDepartment_Create, L("CreateDineGoDepartment"), multiTenancySides: MultiTenancySides.Tenant);

            var dinegopaymenttypePermission = goPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoPaymentType, L("PaymentType"), multiTenancySides: MultiTenancySides.Tenant);
            dinegopaymenttypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoPaymentType_Delete, L("DeleteDineGoPaymentType"), multiTenancySides: MultiTenancySides.Tenant);
            dinegopaymenttypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoPaymentType_Edit, L("EditDineGoPaymentType"), multiTenancySides: MultiTenancySides.Tenant);
            dinegopaymenttypePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoPaymentType_Create, L("CreateDineGoPaymentType"), multiTenancySides: MultiTenancySides.Tenant);

            var dinegochargePermission = goPermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoCharge, L("Charges"), multiTenancySides: MultiTenancySides.Tenant);
            dinegochargePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoCharge_Delete, L("DeleteDineGoCharge"), multiTenancySides: MultiTenancySides.Tenant);
            dinegochargePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoCharge_Edit, L("EditDineGoCharge"), multiTenancySides: MultiTenancySides.Tenant);
            dinegochargePermission.CreateChildPermission(AppPermissions.Pages_Tenant_Go_DineGoCharge_Create, L("CreateDineGoCharge"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion DineGo

            #region Play

            var dinePlayPermission = pages.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play, L("Play"), multiTenancySides: MultiTenancySides.Tenant);

            var dinePlayResolutionPermission = dinePlayPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_Resolution, L("Resolution"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinePlayResolutionPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_Resolution_Create, L("CreateDinePlayResolution"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinePlayResolutionPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_Resolution_Edit, L("EditDinePlayResolution"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinePlayResolutionPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_Resolution_Delete, L("DeleteDinePlayResolution"),
                multiTenancySides: MultiTenancySides.Tenant);

            var dinePlayDisplayGroupPermission = dinePlayPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_DisplayGroup, L("DisplayGroup"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinePlayDisplayGroupPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_DisplayGroup_Create, L("CreateDinePlayDisplayGroup"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinePlayDisplayGroupPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_DisplayGroup_Edit, L("EditDinePlayDisplayGroup"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinePlayDisplayGroupPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_DisplayGroup_Delete, L("DeleteDinePlayDisplayGroup"),
                multiTenancySides: MultiTenancySides.Tenant);

            var dinePlayDisplayPermission = dinePlayPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_Display, L("Display"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinePlayDisplayPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_Display_Create, L("CreateDinePlayDisplay"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinePlayDisplayPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_Display_Edit, L("EditDinePlayDisplay"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinePlayDisplayPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_Display_Delete, L("DeleteDinePlayDisplay"),
                multiTenancySides: MultiTenancySides.Tenant);

            var dinePlayDayPartingPermission = dinePlayPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_DayParting, L("Dayparting"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinePlayDayPartingPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_DayParting_Create, L("CreateDinePlayDayparting"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinePlayDayPartingPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_DayParting_Edit, L("EditDinePlayDayparting"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinePlayDayPartingPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_DayParting_Delete, L("DeleteDinePlayDayparting"),
                multiTenancySides: MultiTenancySides.Tenant);

            var dinePlayLayoutPermission = dinePlayPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_Layout, L("Layout"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinePlayLayoutPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_Layout_Create, L("CreateDinePlayLayout"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinePlayLayoutPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_Layout_Edit, L("EditDinePlayLayout"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinePlayLayoutPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_Layout_Delete, L("DeleteDinePlayLayout"),
                multiTenancySides: MultiTenancySides.Tenant);

            var dinePlaySchedulePermission = dinePlayPermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_Schedule, L("Schedule"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinePlaySchedulePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_Schedule_Event_Create, L("CreateDinePlayScheduledEvent"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinePlaySchedulePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_Schedule_Event_Edit, L("EditDinePlayScheduledEvent"),
                multiTenancySides: MultiTenancySides.Tenant);
            dinePlaySchedulePermission.CreateChildPermission(
                AppPermissions.Pages_Tenant_Play_Schedule_Event_Delete, L("DeleteDinePlayScheduledEvent"),
                multiTenancySides: MultiTenancySides.Tenant);

            #endregion Play
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, DineConnectConsts.LocalizationSourceName);
        }
    }
}