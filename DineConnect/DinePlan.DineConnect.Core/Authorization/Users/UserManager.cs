﻿using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Configuration;
using Abp.Configuration.Startup;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Organizations;
using Abp.Runtime.Caching;
using Abp.UI;
using Abp.Zero.Configuration;
using DinePlan.DineConnect.Authorization.Roles;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.MultiTenancy;
using System.Threading.Tasks;
using System.Linq;
using System;
using DinePlan.DineConnect.Hr;


namespace DinePlan.DineConnect.Authorization.Users
{
    /// <summary>
    /// User manager.
    /// Used to implement domain logic for users.
    /// Extends <see cref="AbpUserManager{TTenant,TRole,TUser}"/>.
    /// </summary>
    public class UserManager : AbpUserManager<Tenant, Role, User>
    {
        private IRepository<UserDefaultInformation> _userdefaultinformationRepo;
        private IRepository<PersonalInformation> _personalinformationRepo;
        private readonly IRepository<UserDefaultOrganization> _userDefaultOrganizationRepo;
        public IRepository<Location> _locationRepo;
        public IRepository<ProductionUnit> _productionunitRepo;
        private UserStore _userStore;


        public UserManager(
            UserStore userStore,
            RoleManager roleManager,
            IRepository<Tenant> tenantRepository,
            IMultiTenancyConfig multiTenancyConfig,
            IPermissionManager permissionManager,
            IUnitOfWorkManager unitOfWorkManager,
            ISettingManager settingManager,
            IUserManagementConfig userManagementConfig,
            IIocResolver iocResolver,
            ICacheManager cacheManager,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IOrganizationUnitSettings organizationUnitSettings,
            IRepository<UserDefaultOrganization> userDefaultOrganizationRepo,
            IRepository<Location> locationRepo,
            IRepository<ProductionUnit> productionunitRepo,
            IRepository<UserDefaultInformation> userdefaultinformationRepo,
            IRepository<PersonalInformation> personalinformationRepo
       )
            : base(
                userStore,
                roleManager,
                tenantRepository,
                multiTenancyConfig,
                permissionManager,
                unitOfWorkManager,
                settingManager,
                userManagementConfig,
                iocResolver,
                cacheManager,
                organizationUnitRepository,
                userOrganizationUnitRepository,
                organizationUnitSettings)
        {
            _userDefaultOrganizationRepo = userDefaultOrganizationRepo;
            _locationRepo = locationRepo;
            _productionunitRepo = productionunitRepo;
            _userStore = userStore;
            _userdefaultinformationRepo = userdefaultinformationRepo;
            _personalinformationRepo = personalinformationRepo;
        }

        public virtual int? GetDefaultLocationForUserAsync(long argUserId)
        {
            var a = _userDefaultOrganizationRepo.FirstOrDefault(p => p.UserId == argUserId);

            int? defaultLocationId = 0;

            if (a != null)
            {
                defaultLocationId = (int)a.OrganizationUnitId;
            }

            return defaultLocationId;
        }

        public async Task<Location> GetDefaultLocationInfoAsync(int? argLocationId)
        {
            var location = await _locationRepo.FirstOrDefaultAsync(p => p.Id == argLocationId);

            if (location != null)
            {
                if (location.IsProductionAllowed == true && location.IsProductionUnitAllowed == true)
                {
                    var productionUnits = _productionunitRepo.GetAll().Where(t => t.LocationRefId == location.Id).ToList();
                    location.ProductionUnitList = productionUnits;
                }
            }
            return location;
        }

#pragma warning disable CS1998 // This async method lacks 'await' operators and will run synchronously. Consider using the 'await' operator to await non-blocking API calls, or 'await Task.Run(...)' to do CPU-bound work on a background thread.
        public async Task<User> GetUsers(int roleId)
#pragma warning restore CS1998 // This async method lacks 'await' operators and will run synchronously. Consider using the 'await' operator to await non-blocking API calls, or 'await Task.Run(...)' to do CPU-bound work on a background thread.
        {
            return null;
        }



        public virtual async Task<UserDefaultInformation> GetDefaultInformationForUserAsync(long argUserId)
        {
            var currentUser = await _userdefaultinformationRepo.FirstOrDefaultAsync(p => p.UserId == argUserId);
            return currentUser;
        }

    }
}