﻿namespace DinePlan.DineConnect.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        #region Common

        public const string Pages = "Pages";
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";
        public const string Pages_Administration_Users_ChangeUserPassword = "Pages.Administration.Users.ChangeUserPassword";
        public const string Pages_Administration_Users_Lock = "Pages.Administration.Users.Lock";
        public const string Pages_Administration_Users_Unlock = "Pages.Administration.Users.Unlock";
        public const string Pages_Administration_Users_GenerateNewPassword = "Pages.Administration.Users.GenerateNewPassword";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";
        public const string Pages_Administration_Maintenance = "Pages.Administration.Maintenance";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";

        public const string Pages_Administration_LastSync = "Pages.Administration.LastSync";
        public const string Pages_Administration_SyncDashboard = "Pages.Administration.SyncDashboard";

        public const string Pages_Tenant_TickMessage = "Pages.Tenant.TickMessage";
        public const string Pages_Tenant_TickMessage_Create = "Pages.Tenant.TickMessage.Create";
        public const string Pages_Tenant_TickMessage_Delete = "Pages.Tenant.TickMessage.Delete";
        public const string Pages_Tenant_TickMessage_Edit = "Pages.Tenant.TickMessage.Edit";

        public const string Pages_Administration_CustomReport = "Pages.Administration.CustomReport";
        public const string Pages_Administration_Templates = "Pages.Administration.Templates";
        public const string Pages_Administration_SMTPSetting = "Pages.Administration.SMTPSetting";

        #endregion Common

        #region Host

        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";

        #endregion Host

        #region Tenant

        //TENANT-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";
        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_Tenant_Setup = "Pages.Tenant.Setup";
        public const string Pages_Tenant_Setup_ImportSeedData = "Pages.Tenant.Setup.ImportSeedData";
        public const string Pages_Tenant_Setup_CleanSeedData = "Pages.Tenant.Setup.CleanSeedData";

        public const string Pages_Tenant_Setup_ImportData = "Pages.Tenant.Setup.ImportData";
        public const string Pages_Tenant_Setup_ExportData = "Pages.Tenant.Setup.ExportData";

        public const string Pages_Tenant_Addons = "Pages.Tenant.Addons";

        #endregion Tenant

        #region Connect

        //Connect Permissions

        public const string Pages_Tenant_Connect = "Pages.Tenant.Connect";
        //Company

        public const string Pages_Tenant_Connect_Master_Company = "Pages.Tenant.Connect.Master.Company";
        public const string Pages_Tenant_Connect_Master_Company_Create = "Pages.Tenant.Connect.Master.Company.Create";
        public const string Pages_Tenant_Connect_Master_Company_Delete = "Pages.Tenant.Connect.Master.Company.Delete";
        public const string Pages_Tenant_Connect_Master_Company_Edit = "Pages.Tenant.Connect.Master.Company.Edit";

        public const string Pages_Tenant_Connect_ImportSetting = "Pages.Tenant.Connect.ImportSetting";
        public const string Pages_Tenant_Connect_FileManager = "Pages.Tenant.Connect.FileManager";

        public const string Pages_Tenant_Connect_Terminal = "Pages.Tenant.Connect.Terminal";
        public const string Pages_Tenant_Connect_Terminal_Create = "Pages.Tenant.Connect.Terminal.Create";
        public const string Pages_Tenant_Connect_Terminal_Delete = "Pages.Tenant.Connect.Terminal.Delete";
        public const string Pages_Tenant_Connect_Terminal_Edit = "Pages.Tenant.Connect.Terminal.Edit";

        public const string Pages_Tenant_Connect_FutureData = "Pages.Tenant.Connect.FutureData";
        public const string Pages_Tenant_Connect_FutureData_Create = "Pages.Tenant.Connect.FutureData.Create";
        public const string Pages_Tenant_Connect_FutureData_Delete = "Pages.Tenant.Connect.FutureData.Delete";
        public const string Pages_Tenant_Connect_FutureData_Edit = "Pages.Tenant.Connect.FutureData.Edit";

        public const string Pages_Tenant_Connect_Master = "Pages.Tenant.Connect.Master";
        public const string Pages_Tenant_Connect_Menu = "Pages.Tenant.Connect.Menu";
        public const string Pages_Tenant_Connect_Table = "Pages.Tenant.Connect.Table";

        public const string Pages_Tenant_Connect_Report = "Pages.Tenant.Connect.Report";
        public const string Pages_Tenant_Connect_Report_Gst = "Pages.Tenant.Connect.Report.Gst";

        public const string Pages_Tenant_Connect_Report_Schedule = "Pages.Tenant.Connect.Report.Schedule";

        public const string Pages_Tenant_Connect_User = "Pages.Tenant.Connect.User";

        public const string Pages_Tenant_Connect_ProgramSetting = "Pages.Tenant.Connect.ProgramSetting";
        public const string Pages_Tenant_Connect_ProgramSetting_Settings = "Pages.Tenant.Connect.ProgramSetting.Settings";
        public const string Pages_Tenant_Connect_ProgramSetting_Settings_Create = "Pages.Tenant.Connect.ProgramSetting.Settings.Create";
        public const string Pages_Tenant_Connect_ProgramSetting_Settings_Delete = "Pages.Tenant.Connect.ProgramSetting.Settings.Delete";
        public const string Pages_Tenant_Connect_ProgramSetting_Settings_Edit = "Pages.Tenant.Connect.ProgramSetting.Settings.Edit";

        public const string Pages_Tenant_Connect_ProgramSetting_Template = "Pages.Tenant.Connect.ProgramSetting.Template";
        public const string Pages_Tenant_Connect_ProgramSetting_Template_Create = "Pages.Tenant.Connect.ProgramSetting.Template.Create";
        public const string Pages_Tenant_Connect_ProgramSetting_Template_Delete = "Pages.Tenant.Connect.ProgramSetting.Template.Delete";
        public const string Pages_Tenant_Connect_ProgramSetting_Template_Edit = "Pages.Tenant.Connect.ProgramSetting.Template.Edit";

        public const string Pages_Tenant_Connect_ProgramSetting_PTTOrSetting = "Pages.Tenant.Connect.ProgramSetting.PTTOrSetting";

        public const string Pages_Tenant_Connect_Master_Location = "Pages.Tenant.Connect.Master.Location";
        public const string Pages_Tenant_Connect_Master_Location_Create = "Pages.Tenant.Connect.Master.Location.Create";
        public const string Pages_Tenant_Connect_Master_Location_Delete = "Pages.Tenant.Connect.Master.Location.Delete";
        public const string Pages_Tenant_Connect_Master_Location_Edit = "Pages.Tenant.Connect.Master.Location.Edit";

        public const string Pages_Tenant_Connect_Master_LocationGroup = "Pages.Tenant.Connect.Master.LocationGroup";
        public const string Pages_Tenant_Connect_Master_LocationGroup_Create = "Pages.Tenant.Connect.Master.LocationGroup.Create";

        public const string Pages_Tenant_Connect_Master_LocationTag = "Pages.Tenant.Connect.Master.LocationTag";
        public const string Pages_Tenant_Connect_Master_LocationTag_Create = "Pages.Tenant.Connect.Master.LocationTag.Create";
        public const string Pages_Tenant_Connect_Master_LocationTag_Delete = "Pages.Tenant.Connect.Master.LocationTag.Delete";
        public const string Pages_Tenant_Connect_Master_LocationTag_Edit = "Pages.Tenant.Connect.Master.LocationTag.Edit";

        public const string Pages_Tenant_Connect_Master_PlanReason = "Pages.Tenant.Connect.Master.PlanReason";
        public const string Pages_Tenant_Connect_Master_PlanReason_Create = "Pages.Tenant.Connect.Master.PlanReason.Create";
        public const string Pages_Tenant_Connect_Master_PlanReason_Delete = "Pages.Tenant.Connect.Master.PlanReason.Delete";
        public const string Pages_Tenant_Connect_Master_PlanReason_Edit = "Pages.Tenant.Connect.Master.PlanReason.Edit";

        public const string Pages_Tenant_Connect_Master_PaymentType = "Pages.Tenant.Connect.Master.PaymentType";
        public const string Pages_Tenant_Connect_Master_PaymentType_Create = "Pages.Tenant.Connect.Master.PaymentType.Create";
        public const string Pages_Tenant_Connect_Master_PaymentType_Delete = "Pages.Tenant.Connect.Master.PaymentType.Delete";
        public const string Pages_Tenant_Connect_Master_PaymentType_Edit = "Pages.Tenant.Connect.Master.PaymentType.Edit";

        public const string Pages_Tenant_Connect_Master_TicketType = "Pages.Tenant.Connect.Master.TicketType";
        public const string Pages_Tenant_Connect_Master_TicketType_Create = "Pages.Tenant.Connect.Master.TicketType.Create";
        public const string Pages_Tenant_Connect_Master_TicketType_Delete = "Pages.Tenant.Connect.Master.TicketType.Delete";
        public const string Pages_Tenant_Connect_Master_TicketType_Edit = "Pages.Tenant.Connect.Master.TicketType.Edit";

        public const string Pages_Tenant_Connect_Master_Numerator = "Pages.Tenant.Connect.Master.Numerator";
        public const string Pages_Tenant_Connect_Master_Numerator_Create = "Pages.Tenant.Connect.Master.Numerator.Create";
        public const string Pages_Tenant_Connect_Master_Numerator_Delete = "Pages.Tenant.Connect.Master.Numerator.Delete";
        public const string Pages_Tenant_Connect_Master_Numerator_Edit = "Pages.Tenant.Connect.Master.Numerator.Edit";

        public const string Pages_Tenant_Connect_Master_ForeignCurrency = "Pages.Tenant.Connect.Master.ForeignCurrency";
        public const string Pages_Tenant_Connect_Master_ForeignCurrency_Create = "Pages.Tenant.Connect.Master.ForeignCurrency.Create";
        public const string Pages_Tenant_Connect_Master_ForeignCurrency_Delete = "Pages.Tenant.Connect.Master.ForeignCurrency.Delete";
        public const string Pages_Tenant_Connect_Master_ForeignCurrency_Edit = "Pages.Tenant.Connect.Master.ForeignCurrency.Edit";

        public const string Pages_Tenant_Connect_Master_TransactionType = "Pages.Tenant.Connect.Master.TransactionType";
        public const string Pages_Tenant_Connect_Master_TransactionType_Create = "Pages.Tenant.Connect.Master.TransactionType.Create";
        public const string Pages_Tenant_Connect_Master_TransactionType_Delete = "Pages.Tenant.Connect.Master.TransactionType.Delete";
        public const string Pages_Tenant_Connect_Master_TransactionType_Edit = "Pages.Tenant.Connect.Master.TransactionType.Edit";

        public const string Pages_Tenant_Connect_Master_EntityType = "Pages.Tenant.Connect.Master.EntityType";
        public const string Pages_Tenant_Connect_Master_EntityType_Create = "Pages.Tenant.Connect.Master.EntityType.Create";
        public const string Pages_Tenant_Connect_Master_EntityType_Delete = "Pages.Tenant.Connect.Master.EntityType.Delete";
        public const string Pages_Tenant_Connect_Master_EntityType_Edit = "Pages.Tenant.Connect.Master.EntityType.Edit";

        public const string Pages_Tenant_Connect_Master_Department = "Pages.Tenant.Connect.Master.Department";
        public const string Pages_Tenant_Connect_Master_Department_Create = "Pages.Tenant.Connect.Master.Department.Create";
        public const string Pages_Tenant_Connect_Master_Department_Delete = "Pages.Tenant.Connect.Master.Department.Delete";
        public const string Pages_Tenant_Connect_Master_Department_Edit = "Pages.Tenant.Connect.Master.Department.Edit";

        public const string Pages_Tenant_Connect_Master_DepartmentGroup = "Pages.Tenant.Connect.Master.DepartmentGroup";
        public const string Pages_Tenant_Connect_Master_DepartmentGroup_Create = "Pages.Tenant.Connect.Master.DepartmentGroup.Create";
        public const string Pages_Tenant_Connect_Master_DepartmentGroup_Delete = "Pages.Tenant.Connect.Master.DepartmentGroup.Delete";
        public const string Pages_Tenant_Connect_Master_DepartmentGroup_Edit = "Pages.Tenant.Connect.Master.DepartmentGroup.Edit";

        public const string Pages_Tenant_Connect_Report_Tickets = "Pages.Tenant.Connect.Report.Tickets";
        public const string Pages_Tenant_Connect_Report_Orders = "Pages.Tenant.Connect.Report.Orders";

        public const string Pages_Tenant_Connect_Report_Items = "Pages.Tenant.Connect.Report.Items";
        public const string Pages_Tenant_Connect_Report_Summary = "Pages.Tenant.Connect.Report.Summary";
        public const string Pages_Tenant_Connect_Report_Collection = "Pages.Tenant.Connect.Report.Collection";
        public const string Pages_Tenant_Connect_Report_FeedbackUnread = "Pages.Tenant.Connect.Report.FeedbackUnread";
        public const string Pages_Tenant_Connect_Report_AbnormalEndDay = "Pages.Tenant.Connect.Report.AbnormalEndDay";
        public const string Pages_Tenant_Connect_Report_CashAudits = "Pages.Tenant.Connect.Report.CashAudits";
        public const string Pages_Tenant_Connect_Report_SpeedOfServiceReport = "Pages.Tenant.Connect.Report.SpeedOfServiceReport";
        public const string Pages_Tenant_Connect_Report_CreditCard = "Pages.Tenant.Connect.Report.CreditCard";
        public const string Pages_Tenant_Connect_Report_Customize = "Pages.Tenant.Connect.Report.Customize";
        public const string Pages_Tenant_Connect_Report_CashSummaryReport = "Pages.Tenant.Connect.Report.CashSummaryReport";
        public const string Pages_Tenant_Connect_Report_Comparision = "Pages.Tenant.Connect.Report.Comparision";
        public const string Pages_Tenant_Connect_Report_CreditSales = "Pages.Tenant.Connect.Report.CreditSales";
        public const string Pages_Tenant_Connect_Report_TopDownSellReport = "Pages.Tenant.Connect.Report.TopDownSellReport";

        public const string Pages_Tenant_Connect_Report_TopDownSellReportByAmountPlant = "Pages.Tenant.Connect.Report.Customize.TopDownSellReport.AmountPlant";
        public const string Pages_Tenant_Connect_Report_TopDownSellReportByQuantityPlant = "Pages.Tenant.Connect.Report.Customize.TopDownSellReport.QuantityPlant";
        public const string Pages_Tenant_Connect_Report_TopDownSellReportByAmountSummary = "Pages.Tenant.Connect.Report.Customize.TopDownSellReport.AmountSummary";
        public const string Pages_Tenant_Connect_Report_TopDownSellReportByQuantitySummary = "Pages.Tenant.Connect.Report.Customize.TopDownSellReport.QuantitySummary";
        public const string Pages_Tenant_Connect_Report_MealTimeReport = "Pages.Tenant.Connect.Report.MealTimeSales";


        public const string Pages_Tenant_Connect_Report_SalesPromotionReport = "Pages.Tenant.Connect.Report.SalesPromotionReport";
        public const string Pages_Tenant_Connect_Report_SalesPromotionByProReport = "Pages.Tenant.Connect.Report.SalesPromotionReport.SalesPromotionByPromotion";
        public const string Pages_Tenant_Connect_Report_SalesPromotionByPlantReport = "Pages.Tenant.Connect.Report.SalesPromotionReport.SalesPromotionByPlant";

        public const string Pages_Tenant_Connect_Report_SalesByPeriodReport = "Pages.Tenant.Connect.Report.SalesByPeriodReport";
        public const string Pages_Tenant_Connect_Report_SalesByPeriodByReport = "Pages.Tenant.Connect.Report.SalesByPeriodByReport";

        public const string Pages_Tenant_Connect_Menu_Category = "Pages.Tenant.Connect.Menu.Category";
        public const string Pages_Tenant_Connect_Menu_Category_Create = "Pages.Tenant.Connect.Menu.Category.Create";
        public const string Pages_Tenant_Connect_Menu_Category_Delete = "Pages.Tenant.Connect.Menu.Category.Delete";
        public const string Pages_Tenant_Connect_Menu_Category_Edit = "Pages.Tenant.Connect.Menu.Category.Edit";

        public const string Pages_Tenant_Connect_Report_ProductMixReport = "Pages.Tenant.Connect.Report.ProductMixReport";

        public const string Pages_Tenant_Connect_Menu_ProductGroup = "Pages.Tenant.Connect.Menu.ProductGroup";
        public const string Pages_Tenant_Connect_Menu_ProductGroup_ManageProductGroup = "Pages.Tenant.Connect.Menu.ProductGroup.ManageProductGroup";

        public const string Pages_Tenant_Connect_Menu_MenuItem = "Pages.Tenant.Connect.Menu.MenuItem";
        public const string Pages_Tenant_Connect_Menu_MenuItemPrice = "Pages.Tenant.Connect.Menu.MenuItemPrice";
        public const string Pages_Tenant_Connect_Menu_MenuItem_Create = "Pages.Tenant.Connect.Menu.MenuItem.Create";
        public const string Pages_Tenant_Connect_Menu_MenuItem_Delete = "Pages.Tenant.Connect.Menu.MenuItem.Delete";
        public const string Pages_Tenant_Connect_Menu_MenuItem_Edit = "Pages.Tenant.Connect.Menu.MenuItem.Edit";
        public const string Pages_Tenant_Connect_Menu_MenuItem_CloneAsMaterial = "Pages.Tenant.Connect.Menu.MenuItem.CloneAsMaterial";

        public const string Pages_Tenant_Connect_Menu_ScreenMenu = "Pages.Tenant.Connect.Menu.ScreenMenu";
        public const string Pages_Tenant_Connect_Menu_ScreenMenu_Create = "Pages.Tenant.Connect.Menu.ScreenMenu.Create";
        public const string Pages_Tenant_Connect_Menu_ScreenMenu_Delete = "Pages.Tenant.Connect.Menu.ScreenMenu.Delete";
        public const string Pages_Tenant_Connect_Menu_ScreenMenu_Edit = "Pages.Tenant.Connect.Menu.ScreenMenu.Edit";

        public const string Pages_Tenant_Connect_Menu_PriceTag = "Pages.Tenant.Connect.Menu.PriceTag";
        public const string Pages_Tenant_Connect_Menu_PriceTag_Create = "Pages.Tenant.Connect.Menu.PriceTag.Create";
        public const string Pages_Tenant_Connect_Menu_PriceTag_Delete = "Pages.Tenant.Connect.Menu.PriceTag.Delete";
        public const string Pages_Tenant_Connect_Menu_PriceTag_Edit = "Pages.Tenant.Connect.Menu.PriceTag.Edit";

        public const string Pages_Tenant_Connect_Menu_OrderTagGroup = "Pages.Tenant.Connect.Menu.OrderTagGroup";
        public const string Pages_Tenant_Connect_Menu_OrderTagGroup_Create = "Pages.Tenant.Connect.Menu.OrderTagGroup.Create";
        public const string Pages_Tenant_Connect_Menu_OrderTagGroup_Delete = "Pages.Tenant.Connect.Menu.OrderTagGroup.Delete";
        public const string Pages_Tenant_Connect_Menu_OrderTagGroup_Edit = "Pages.Tenant.Connect.Menu.OrderTagGroup.Edit";

        public const string Pages_Tenant_Connect_Menu_TicketTagGroup = "Pages.Tenant.Connect.Menu.TicketTagGroup";
        public const string Pages_Tenant_Connect_Menu_TicketTagGroup_Create = "Pages.Tenant.Connect.Menu.TicketTagGroup.Create";
        public const string Pages_Tenant_Connect_Menu_TicketTagGroup_Delete = "Pages.Tenant.Connect.Menu.TicketTagGroup.Delete";
        public const string Pages_Tenant_Connect_Menu_TicketTagGroup_Edit = "Pages.Tenant.Connect.Menu.TicketTagGroup.Edit";

        public const string Pages_Tenant_Connect_Menu_PromotionCategory = "Pages.Tenant.Connect.Menu.PromotionCategory";
        public const string Pages_Tenant_Connect_Menu_PromotionCategory_Create = "Pages.Tenant.Connect.Menu.PromotionCategory.Create";
        public const string Pages_Tenant_Connect_Menu_PromotionCategory_Delete = "Pages.Tenant.Connect.Menu.PromotionCategory.Delete";
        public const string Pages_Tenant_Connect_Menu_PromotionCategory_Edit = "Pages.Tenant.Connect.Menu.PromotionCategory.Edit";

        public const string Pages_Tenant_Connect_Menu_Promotion = "Pages.Tenant.Connect.Menu.Promotion";
        public const string Pages_Tenant_Connect_Menu_Promotion_Create = "Pages.Tenant.Connect.Menu.Promotion.Create";
        public const string Pages_Tenant_Connect_Menu_Promotion_Delete = "Pages.Tenant.Connect.Menu.Promotion.Delete";
        public const string Pages_Tenant_Connect_Menu_Promotion_Edit = "Pages.Tenant.Connect.Menu.Promotion.Edit";

        public const string Pages_Tenant_Connect_Menu_ComboGroup = "Pages.Tenant.Connect.Menu.ComboGroup";
        public const string Pages_Tenant_Connect_Menu_ComboGroup_Create = "Pages.Tenant.Connect.Menu.ComboGroup.Create";
        public const string Pages_Tenant_Connect_Menu_ComboGroup_Delete = "Pages.Tenant.Connect.Menu.ComboGroup.Delete";
        public const string Pages_Tenant_Connect_Menu_ComboGroup_Edit = "Pages.Tenant.Connect.Menu.ComboGroup.Edit";

        public const string Pages_Tenant_Connect_Menu_DeliveryAggregator = "Pages.Tenant.Connect.Menu.DeliveryAggregator";
        public const string Pages_Tenant_Connect_Menu_DeliveryAggregator_Create = "Pages.Tenant.Connect.Menu.DeliveryAggregator.Create";
        public const string Pages_Tenant_Connect_Menu_DeliveryAggregator_Delete = "Pages.Tenant.Connect.Menu.DeliveryAggregator.Delete";
        public const string Pages_Tenant_Connect_Menu_DeliveryAggregator_Edit = "Pages.Tenant.Connect.Menu.DeliveryAggregator.Edit";

        public const string Pages_Tenant_Connect_Report_Departments = "Pages.Tenant.Connect.Report.Departments";

        public const string Pages_Tenant_Connect_User_DinePlanUser = "Pages.Tenant.Connect.User.DinePlanUser";
        public const string Pages_Tenant_Connect_User_DinePlanUser_Create = "Pages.Tenant.Connect.User.DinePlanUser.Create";
        public const string Pages_Tenant_Connect_User_DinePlanUser_Delete = "Pages.Tenant.Connect.User.DinePlanUser.Delete";
        public const string Pages_Tenant_Connect_User_DinePlanUser_Edit = "Pages.Tenant.Connect.User.DinePlanUser.Edit";

        public const string Pages_Tenant_Connect_User_DinePlanUserRole = "Pages.Tenant.Connect.User.DinePlanUserRole";
        public const string Pages_Tenant_Connect_User_DinePlanUserRole_Create = "Pages.Tenant.Connect.User.DinePlanUserRole.Create";
        public const string Pages_Tenant_Connect_User_DinePlanUserRole_Delete = "Pages.Tenant.Connect.User.DinePlanUserRole.Delete";
        public const string Pages_Tenant_Connect_User_DinePlanUserRole_Edit = "Pages.Tenant.Connect.User.DinePlanUserRole.Edit";

        public const string Pages_Tenant_Connect_Table_Group = "Pages.Tenant.Conect.Table.Group";
        public const string Pages_Tenant_Connect_Table_Group_Create = "Pages.Tenant.Conect.Table.Group.Create";
        public const string Pages_Tenant_Connect_Table_Group_Delete = "Pages.Tenant.Conect.Table.Group.Delete";
        public const string Pages_Tenant_Connect_Table_Group_Edit = "Pages.Tenant.Conect.Table.Group.Edit";

        public const string Pages_Tenant_Connect_Tables = "Pages.Tenant.Conect.Tables";
        public const string Pages_Tenant_Connect_Tables_Create = "Pages.Tenant.Conect.Tables.Create";
        public const string Pages_Tenant_Connect_Tables_Delete = "Pages.Tenant.Conect.Tables.Delete";
        public const string Pages_Tenant_Connect_Tables_Edit = "Pages.Tenant.Conect.Tables.Edit";

        public const string Pages_Tenant_Connect_Master_TillAccount = "Pages.Tenant.Connect.Master.TillAccount";
        public const string Pages_Tenant_Connect_Master_TillAccount_Create = "Pages.Tenant.Connect.Master.TillAccount.Create";
        public const string Pages_Tenant_Connect_Master_TillAccount_Delete = "Pages.Tenant.Connect.Master.TillAccount.Delete";
        public const string Pages_Tenant_Connect_Master_TillAccount_Edit = "Pages.Tenant.Connect.Master.TillAccount.Edit";

        public const string Pages_Tenant_Connect_Card = "Pages.Tenant.Conect.Card";

        public const string Pages_Tenant_Connect_Card_Cards = "Pages.Tenant.Conect.Card.Cards";
        public const string Pages_Tenant_Connect_Card_Cards_Create = "Pages.Tenant.Conect.Card.Cards.Create";
        public const string Pages_Tenant_Connect_Card_Cards_Edit = "Pages.Tenant.Conect.Card.Cards.Edit";
        public const string Pages_Tenant_Connect_Card_Cards_Delete = "Pages.Tenant.Conect.Card.Cards.Delete";
        public const string Pages_Tenant_Connect_Card_Cards_Import = "Pages.Tenant.Conect.Card.Cards.Import";
        public const string Pages_Tenant_Connect_Card_Cards_Active = "Pages.Tenant.Conect.Card.Cards.Active";
        public const string Pages_Tenant_Connect_Card_Cards_DeleteMultipleConnectCards = "Pages.Tenant.Conect.Card.Cards.DeleteMultipleConnectCards";

        public const string Pages_Tenant_Connect_Card_CardType = "Pages.Tenant.Conect.Card.CardType";
        public const string Pages_Tenant_Connect_Card_CardType_Create = "Pages.Tenant.Conect.Card.CardType.Create";
        public const string Pages_Tenant_Connect_Card_CardType_Edit = "Pages.Tenant.Conect.Card.CardType.Edit";
        public const string Pages_Tenant_Connect_Card_CardType_Delete = "Pages.Tenant.Conect.Card.CardType.Delete";

        public const string Pages_Tenant_Connect_Card_ConnectCardTypeCategory = "Pages.Tenant.Connect.Card.ConnectCardTypeCategory";
        public const string Pages_Tenant_Connect_Card_ConnectCardTypeCategory_Create = "Pages.Tenant.Connect.Card.ConnectCardTypeCategory.Create";
        public const string Pages_Tenant_Connect_Card_ConnectCardTypeCategory_Delete = "Pages.Tenant.Connect.Card.ConnectCardTypeCategory.Delete";
        public const string Pages_Tenant_Connect_Card_ConnectCardTypeCategory_Edit = "Pages.Tenant.Connect.Card.ConnectCardTypeCategory.Edit";

        public const string Pages_Tenant_Connect_FullTax = "Pages.Tenant.Connect.FullTax";

        public const string Pages_Tenant_Connect_FullTax_Member = "Pages.Tenant.Connect.FullTax.Member";
        public const string Pages_Tenant_Connect_FullTax_Member_Create = "Pages.Tenant.Connect.FullTax.Member.Create";
        public const string Pages_Tenant_Connect_FullTax_Member_Edit = "Pages.Tenant.Connect.FullTax.Member.Edit";
        public const string Pages_Tenant_Connect_FullTax_Member_Delete = "Pages.Tenant.Connect.FullTax.Member.Delete";

        public const string Pages_Tenant_Connect_FullTax_Report = "Pages.Tenant.Connect.FullTax.Report";

        public const string Pages_Tenant_Connect_Display = "Pages.Tenant.Connect.Display";

        public const string Pages_Tenant_Connect_Display_Printer = "Pages.Tenant.Connect.Display.Printer";
        public const string Pages_Tenant_Connect_Display_Printer_Create = "Pages.Tenant.Connect.Display.Printer.Create";
        public const string Pages_Tenant_Connect_Display_Printer_Edit = "Pages.Tenant.Connect.Display.Printer.Edit";
        public const string Pages_Tenant_Connect_Display_Printer_Delete = "Pages.Tenant.Connect.Display.Printer.Delete";

        public const string Pages_Tenant_Connect_Display_PrintJob = "Pages.Tenant.Connect.Display.PrintJob";
        public const string Pages_Tenant_Connect_Display_PrintJob_Create = "Pages.Tenant.Connect.Display.PrintJob.Create";
        public const string Pages_Tenant_Connect_Display_PrintJob_Edit = "Pages.Tenant.Connect.Display.PrintJob.Edit";
        public const string Pages_Tenant_Connect_Display_PrintJob_Delete = "Pages.Tenant.Connect.Display.PrintJob.Delete";

        public const string Pages_Tenant_Connect_Display_PrintTemplate = "Pages.Tenant.Connect.Display.PrintTemplate";
        public const string Pages_Tenant_Connect_Display_PrintTemplate_Create = "Pages.Tenant.Connect.Display.PrintTemplate.Create";
        public const string Pages_Tenant_Connect_Display_PrintTemplate_Edit = "Pages.Tenant.Connect.Display.PrintTemplate.Edit";
        public const string Pages_Tenant_Connect_Display_PrintTemplate_Delete = "Pages.Tenant.Connect.Display.PrintTemplate.Delete";

        public const string Pages_Tenant_Connect_Display_PrintTemplateCondition = "Pages.Tenant.Connect.Display.PrintTemplateCondition";
        public const string Pages_Tenant_Connect_Display_PrintTemplateCondition_Create = "Pages.Tenant.Connect.Display.PrintTemplateCondition.Create";
        public const string Pages_Tenant_Connect_Display_PrintTemplateCondition_Edit = "Pages.Tenant.Connect.Display.PrintTemplateCondition.Edit";
        public const string Pages_Tenant_Connect_Display_PrintTemplateCondition_Delete = "Pages.Tenant.Connect.Display.PrintTemplateCondition.Delete";

        public const string Pages_Tenant_Connect_Display_DineDevice = "Pages.Tenant.Connect.Display.DineDevice";
        public const string Pages_Tenant_Connect_Display_DineDevice_Create = "Pages.Tenant.Connect.Display.DineDevice.Create";
        public const string Pages_Tenant_Connect_Display_DineDevice_Delete = "Pages.Tenant.Connect.Display.DineDevice.Delete";
        public const string Pages_Tenant_Connect_Display_DineDevice_Edit = "Pages.Tenant.Connect.Display.DineDevice.Edit";

        #endregion Connect

        #region House

        //House Permissions
        public const string Pages_Tenant_House = "Pages.Tenant.House";
        

        public const string Pages_Tenant_House_Dashboard = "Pages.Tenant.House.Dashboard";
        public const string Pages_Tenant_House_NotificationDashboard = "Pages.Tenant.House.NotificationDashboard";
        public const string Pages_Tenant_House_Numbering = "Pages.Tenant.House.Numbering";

        public const string Pages_Tenant_House_Master = "Pages.Tenant.House.Master";
        public const string Pages_Tenant_House_Transaction = "Pages.Tenant.House.Transaction";
        public const string Pages_Tenant_House_Purchase = "Pages.Tenant.House.Purchase";
        public const string Pages_Tenant_House_Production = "Pages.Tenant.House.Production";
        public const string Pages_Tenant_House_Movement = "Page.Tenant.House.Movement";
        public const string Pages_Tenant_House_Transfer = "Page.Tenant.House.Transfer";
        public const string Pages_Tenant_House_Sales = "Page.Tenant.House.Sales";

        //brand
        public const string Pages_Tenant_House_Master_Brand = "Pages.Tenant.House.Master.Brand";

        public const string Pages_Tenant_House_Master_Brand_Create = "Pages.Tenant.House.Master.Brand.Create";
        public const string Pages_Tenant_House_Master_Brand_Delete = "Pages.Tenant.House.Master.Brand.Delete";
        public const string Pages_Tenant_House_Master_Brand_Edit = "Pages.Tenant.House.Master.Brand.Edit";
        //unit

        public const string Pages_Tenant_House_Master_Supplier = "Pages.Tenant.House.Master.Supplier";
        public const string Pages_Tenant_House_Master_Supplier_Create = "Pages.Tenant.House.Master.Supplier.Create";
        public const string Pages_Tenant_House_Master_Supplier_Delete = "Pages.Tenant.House.Master.Supplier.Delete";
        public const string Pages_Tenant_House_Master_Supplier_Edit = "Pages.Tenant.House.Master.Supplier.Edit";
        public const string Pages_Tenant_House_Master_Supplier_Import = "Pages.Tenant.House.Master.Supplier.Import";

        public const string Pages_Tenant_House_Master_Unit = "Pages.Tenant.House.Master.Unit";
        public const string Pages_Tenant_House_Master_Unit_Create = "Pages.Tenant.House.Master.Unit.Create";
        public const string Pages_Tenant_House_Master_Unit_Delete = "Pages.Tenant.House.Master.Unit.Delete";
        public const string Pages_Tenant_House_Master_Unit_Edit = "Pages.Tenant.House.Master.Unit.Edit";

        //Recipe
        public const string Pages_Tenant_House_Master_Recipe = "Pages.Tenant.House.Master.Recipe";

        public const string Pages_Tenant_House_Master_Recipe_Create = "Pages.Tenant.House.Master.Recipe.Create";
        public const string Pages_Tenant_House_Master_Recipe_Delete = "Pages.Tenant.House.Master.Recipe.Delete";
        public const string Pages_Tenant_House_Master_Recipe_Edit = "Pages.Tenant.House.Master.Recipe.Edit";

        //Recipe Ingredient
        public const string Pages_Tenant_House_Master_RecipeIngredient = "Pages.Tenant.House.Master.RecipeIngredient";

        public const string Pages_Tenant_House_Master_RecipeIngredient_Create = "Pages.Tenant.House.Master.RecipeIngredient.Create";
        public const string Pages_Tenant_House_Master_RecipeIngredient_Delete = "Pages.Tenant.House.Master.RecipeIngredient.Delete";
        public const string Pages_Tenant_House_Master_RecipeIngredient_Edit = "Pages.Tenant.House.Master.RecipeIngredient.Edit";

        //Material Group
        public const string Pages_Tenant_House_Master_MaterialGroup = "Pages.Tenant.House.Master.MaterialGroup";

        public const string Pages_Tenant_House_Master_MaterialGroup_Create = "Pages.Tenant.House.Master.MaterialGroup.Create";
        public const string Pages_Tenant_House_Master_MaterialGroup_Delete = "Pages.Tenant.House.Master.MaterialGroup.Delete";
        public const string Pages_Tenant_House_Master_MaterialGroup_Edit = "Pages.Tenant.House.Master.MaterialGroup.Edit";

        //Material group Category
        public const string Pages_Tenant_House_Master_MaterialGroupCategory = "Pages.Tenant.House.Master.MaterialGroupCategory";

        public const string Pages_Tenant_House_Master_MaterialGroupCategory_Create = "Pages.Tenant.House.Master.MaterialGroupCategory.Create";
        public const string Pages_Tenant_House_Master_MaterialGroupCategory_Delete = "Pages.Tenant.House.Master.MaterialGroupCategory.Delete";
        public const string Pages_Tenant_House_Master_MaterialGroupCategory_Edit = "Pages.Tenant.House.Master.MaterialGroupCategory.Edit";

        //Material Master
        public const string Pages_Tenant_House_Master_Material = "Pages.Tenant.House.Master.Material";

        public const string Pages_Tenant_House_Master_Material_Create = "Pages.Tenant.House.Master.Material.Create";
        public const string Pages_Tenant_House_Master_Material_Delete = "Pages.Tenant.House.Master.Material.Delete";
        public const string Pages_Tenant_House_Master_Material_Edit = "Pages.Tenant.House.Master.Material.Edit";
        public const string Pages_Tenant_House_Master_Material_Barcode_Usage = "Pages.Tenant.House.Master.Material.Barcode";
        public const string Pages_Tenant_House_Master_Material_ChangeDefaultUnit = "Pages.Tenant.House.Master.Material.ChangeDefaultUnit";
        public const string Pages_Tenant_House_Master_Material_ChangeActiveStatus = "Pages.Tenant.House.Master.Material.ChangeActiveStatus";
        public const string Pages_Tenant_House_Master_Material_PrintMaterialCode = "Pages.Tenant.House.Master.Material.PrintMaterialCode";

        public const string Pages_Tenant_House_Master_InventoryCycleTag = "Pages.Tenant.House.Master.InventoryCycleTag";
        public const string Pages_Tenant_House_Master_InventoryCycleTag_Create = "Pages.Tenant.House.Master.InventoryCycleTag.Create";
        public const string Pages_Tenant_House_Master_InventoryCycleTag_Delete = "Pages.Tenant.House.Master.InventoryCycleTag.Delete";
        public const string Pages_Tenant_House_Master_InventoryCycleTag_Edit = "Pages.Tenant.House.Master.InventoryCycleTag.Edit";

        public const string Pages_Tenant_House_Transaction_ClosingStockEntry = "Pages.Tenant.House.Transaction.ClosingStockEntry";
        public const string Pages_Tenant_House_Transaction_ClosingStockEntry_Create = "Pages.Tenant.House.Transaction.ClosingStockEntry.Create";
        public const string Pages_Tenant_House_Transaction_ClosingStockEntry_Delete = "Pages.Tenant.House.Transaction.ClosingStockEntry.Delete";
        public const string Pages_Tenant_House_Transaction_ClosingStockEntry_Edit = "Pages.Tenant.House.Transaction.ClosingStockEntry.Edit";

        //Storage Location

        public const string Pages_Tenant_House_Master_StorageLocation = "Pages.Tenant.House.Master.StorageLocation";
        public const string Pages_Tenant_House_Master_StorageLocation_Create = "Pages.Tenant.House.Master.StorageLocation.Create";
        public const string Pages_Tenant_House_Master_StorageLocation_Delete = "Pages.Tenant.House.Master.StorageLocation.Delete";
        public const string Pages_Tenant_House_Master_StorageLocation_Edit = "Pages.Tenant.House.Master.StorageLocation.Edit";

        //Material Units Link

        public const string Pages_Tenant_House_Master_MaterialUnitsLink = "Pages.Tenant.House.Master.MaterialUnitsLink";
        public const string Pages_Tenant_House_Master_MaterialUnitsLink_Create = "Pages.Tenant.House.Master.MaterialUnitsLink.Create";
        public const string Pages_Tenant_House_Master_MaterialUnitsLink_Delete = "Pages.Tenant.House.Master.MaterialUnitsLink.Delete";
        public const string Pages_Tenant_House_Master_MaterialUnitsLink_Edit = "Pages.Tenant.House.Master.MaterialUnitsLink.Edit";

        //Material Storage Location

        public const string Pages_Tenant_House_Master_MaterialStorageLocation = "Pages.Tenant.House.Master.MaterialStorageLocation";
        public const string Pages_Tenant_House_Master_MaterialStorageLocation_Create = "Pages.Tenant.House.Master.MaterialStorageLocation.Create";
        public const string Pages_Tenant_House_Master_MaterialStorageLocation_Delete = "Pages.Tenant.House.Master.MaterialStorageLocation.Delete";
        public const string Pages_Tenant_House_Master_MaterialStorageLocation_Edit = "Pages.Tenant.House.Master.MaterialStorageLocation.Edit";
        //MaterialLocationWiseStock

        public const string Pages_Tenant_House_Master_MaterialLocationWiseStock = "Pages.Tenant.House.Master.MaterialLocationWiseStock";

        public const string Pages_Tenant_House_Master_Material_AllLocation_WiseStock = "Pages.Tenant.House.Master.MaterialAllLocationWiseStock";

        //MaterialStockIndividualStore

        public const string Pages_Tenant_House_Master_MaterialStockIndividualStore = "Pages.Tenant.House.Master.MaterialStockIndividualStore";
        public const string Pages_Tenant_House_Master_MaterialStockIndividualStore_Create = "Pages.Tenant.House.Master.MaterialStockIndividualStore.Create";
        public const string Pages_Tenant_House_Master_MaterialStockIndividualStore_Delete = "Pages.Tenant.House.Master.MaterialStockIndividualStore.Delete";
        public const string Pages_Tenant_House_Master_MaterialStockIndividualStore_Edit = "Pages.Tenant.House.Master.MaterialStockIndividualStore.Edit";

        //Tax

        public const string Pages_Tenant_House_Master_Tax = "Pages.Tenant.House.Master.Tax";
        public const string Pages_Tenant_House_Master_Tax_Create = "Pages.Tenant.House.Master.Tax.Create";
        public const string Pages_Tenant_House_Master_Tax_Delete = "Pages.Tenant.House.Master.Tax.Delete";
        public const string Pages_Tenant_House_Master_Tax_Edit = "Pages.Tenant.House.Master.Tax.Edit";

        //SalesTax

        public const string Pages_Tenant_House_Master_SalesTax = "Pages.Tenant.House.Master.SalesTax";
        public const string Pages_Tenant_House_Master_SalesTax_Create = "Pages.Tenant.House.Master.SalesTax.Create";
        public const string Pages_Tenant_House_Master_SalesTax_Delete = "Pages.Tenant.House.Master.SalesTax.Delete";
        public const string Pages_Tenant_House_Master_SalesTax_Edit = "Pages.Tenant.House.Master.SalesTax.Edit";

        //Manual Reason

        public const string Pages_Tenant_House_Master_ManualReason = "Pages.Tenant.House.Master.ManualReason";
        public const string Pages_Tenant_House_Master_ManualReason_Create = "Pages.Tenant.House.Master.ManualReason.Create";
        public const string Pages_Tenant_House_Master_ManualReason_Delete = "Pages.Tenant.House.Master.ManualReason.Delete";
        public const string Pages_Tenant_House_Master_ManualReason_Edit = "Pages.Tenant.House.Master.ManualReason.Edit";

        //supplierMaterial

        public const string Pages_Tenant_House_Master_SupplierMaterial = "Pages.Tenant.House.Master.SupplierMaterial";
        public const string Pages_Tenant_House_Master_SupplierMaterial_Create = "Pages.Tenant.House.Master.SupplierMaterial.Create";
        public const string Pages_Tenant_House_Master_SupplierMaterial_Delete = "Pages.Tenant.House.Master.SupplierMaterial.Delete";
        public const string Pages_Tenant_House_Master_SupplierMaterial_Edit = "Pages.Tenant.House.Master.SupplierMaterial.Edit";

        //Material Wise Brand

        public const string Pages_Tenant_House_Master_MaterialBrandsLink = "Pages.Tenant.House.Master.MaterialBrandsLink";
        public const string Pages_Tenant_House_Master_MaterialBrandsLink_Create = "Pages.Tenant.House.Master.MaterialBrandsLink.Create";
        public const string Pages_Tenant_House_Master_MaterialBrandsLink_Delete = "Pages.Tenant.House.Master.MaterialBrandsLink.Delete";
        public const string Pages_Tenant_House_Master_MaterialBrandsLink_Edit = "Pages.Tenant.House.Master.MaterialBrandsLink.Edit";

        //  InwardDirectCredit
        public const string Pages_Tenant_House_Transaction_InwardDirectCredit = "Pages.Tenant.House.Transaction.InwardDirectCredit";

        public const string Pages_Tenant_House_Transaction_InwardDirectCredit_Create = "Pages.Tenant.House.Transaction.InwardDirectCredit.Create";
        public const string Pages_Tenant_House_Transaction_InwardDirectCredit_Delete = "Pages.Tenant.House.Transaction.InwardDirectCredit.Delete";
        public const string Pages_Tenant_House_Transaction_InwardDirectCredit_Edit = "Pages.Tenant.House.Transaction.InwardDirectCredit.Edit";

        //Issue
        public const string Pages_Tenant_House_Transaction_Issue = "Pages.Tenant.House.Transaction.Issue";

        public const string Pages_Tenant_House_Transaction_Issue_Create = "Pages.Tenant.House.Transaction.Issue.Create";
        public const string Pages_Tenant_House_Transaction_Issue_Delete = "Pages.Tenant.House.Transaction.Issue.Delete";
        public const string Pages_Tenant_House_Transaction_Issue_Edit = "Pages.Tenant.House.Transaction.Issue.Edit";

        //  Inter Transfer

        public const string Pages_Tenant_House_Transaction_InterTransfer = "Pages.Tenant.House.Transaction.InterTransfer";
        public const string Pages_Tenant_House_Transaction_InterTransfer_Create = "Pages.Tenant.House.Transaction.InterTransfer.Create";
        public const string Pages_Tenant_House_Transaction_InterTransfer_Delete = "Pages.Tenant.House.Transaction.InterTransfer.Delete";
        public const string Pages_Tenant_House_Transaction_InterTransfer_Edit = "Pages.Tenant.House.Transaction.InterTransfer.Edit";
        public const string Pages_Tenant_House_Transaction_InterTransfer_Approve = "Pages.Tenant.House.Transaction.InterTransfer.Approve";
        public const string Pages_Tenant_House_Transaction_InterTransfer_GracePeriodAllowed = "Pages.Tenant.House.Transaction.InterTransfer.GracePeriodAllowed";
        public const string Pages_Tenant_House_Transaction_InterTransfer_EditDeliveryDate = "Pages.Tenant.House.Transaction.InterTransfer.EditDeliveryDate";

        //  Inter Transfer - Approval

        public const string Pages_Tenant_House_Transaction_InterTransferApproval = "Pages.Tenant.House.Transaction.InterTransferApproval";

        public const string Pages_Tenant_House_Transaction_InterTransferApproval_Create = "Pages.Tenant.House.Transaction.InterTransferApproval.Create";
        public const string Pages_Tenant_House_Transaction_InterTransferApproval_Delete = "Pages.Tenant.House.Transaction.InterTransferApproval.Delete";
        public const string Pages_Tenant_House_Transaction_InterTransferApproval_Edit = "Pages.Tenant.House.Transaction.InterTransferApproval.Edit";
        public const string Pages_Tenant_House_Transaction_InterTransferApproval_Approve = "Pages.Tenant.House.Transaction.InterTransferApproval.Approve";

        public const string Pages_Tenant_House_Transaction_InterTransferApproval_TransportDetail = "Pages.Tenant.House.Transaction.InterTransferApproval.TransportAllowed";
        public const string Pages_Tenant_House_Transaction_InterTransferApproval_CanChangeRequestQuantity = "Pages.Tenant.House.Transaction.InterTransferApproval.CanChangeRequestQuantity";
        //  Inter Transfer - Received

        public const string Pages_Tenant_House_Transaction_InterTransferReceived = "Pages.Tenant.House.Transaction.InterTransferReceived";

        //public const string Pages_Tenant_House_Transaction_InterTransferReceived_Create = "Pages.Tenant.House.Transaction.InterTransferReceived.Create";
        public const string Pages_Tenant_House_Transaction_InterTransferReceived_Delete = "Pages.Tenant.House.Transaction.InterTransferReceived.Delete";

        public const string Pages_Tenant_House_Transaction_InterTransferReceived_Edit = "Pages.Tenant.House.Transaction.InterTransferReceived.Edit";
        public const string Pages_Tenant_House_Transaction_InterTransferReceived_Received = "Pages.Tenant.House.Transaction.InterTransferReceived.Received";

        //Returns

        public const string Pages_Tenant_House_Transaction_Return = "Pages.Tenant.House.Transaction.Return";
        public const string Pages_Tenant_House_Transaction_Return_Create = "Pages.Tenant.House.Transaction.Return.Create";
        public const string Pages_Tenant_House_Transaction_Return_Delete = "Pages.Tenant.House.Transaction.Return.Delete";
        public const string Pages_Tenant_House_Transaction_Return_Edit = "Pages.Tenant.House.Transaction.Return.Edit";

        //Adjustment
        public const string Pages_Tenant_House_Transaction_Adjustment = "Pages.Tenant.House.Transaction.Adjustment";

        public const string Pages_Tenant_House_Transaction_Adjustment_Create = "Pages.Tenant.House.Transaction.Adjustment.Create";
        public const string Pages_Tenant_House_Transaction_Adjustment_Delete = "Pages.Tenant.House.Transaction.Adjustment.Delete";
        public const string Pages_Tenant_House_Transaction_Adjustment_Force = "Pages.Tenant.House.Transaction.Adjustment.ForceAdjustment";
        // public const string Pages_Tenant_House_Transaction_Adjustment_Edit = "Pages.Tenant.House.Transaction.Adjustment.Edit";

        //Invoice
        public const string Pages_Tenant_House_Transaction_Invoice = "Pages.Tenant.House.Transaction.Invoice";

        public const string Pages_Tenant_House_Transaction_Invoice_Create = "Pages.Tenant.House.Transaction.Invoice.Create";
        public const string Pages_Tenant_House_Transaction_Invoice_Delete = "Pages.Tenant.House.Transaction.Invoice.Delete";
        public const string Pages_Tenant_House_Transaction_Invoice_Edit = "Pages.Tenant.House.Transaction.Invoice.Edit";
        //MaterialLedger

        public const string Pages_Tenant_House_Master_MaterialLedger = "Pages.Tenant.House.Master.MaterialLedger";
        public const string Pages_Tenant_House_Master_MaterialLedger_Create = "Pages.Tenant.House.Master.MaterialLedger.Create";
        public const string Pages_Tenant_House_Master_MaterialLedger_View = "Pages.Tenant.House.Master.MaterialLedger.View";
        public const string Pages_Tenant_House_Master_MaterialLedger_Edit = "Pages.Tenant.House.Master.MaterialLedger.Edit";
        public const string Pages_Tenant_House_Master_MaterialLedger_NegativeStock = "Pages.Tenant.House.Master.MaterialLedger.NegativeStock";

        //UserDefaultOrganization
        public const string Pages_Tenant_House_Master_UserDefaultOrganization = "Pages.Tenant.House.Master.UserDefaultOrganization";

        public const string Pages_Tenant_House_Master_UserDefaultOrganization_Create = "Pages.Tenant.House.Master.UserDefaultOrganization.Create";
        public const string Pages_Tenant_House_Master_UserDefaultOrganization_Delete = "Pages.Tenant.House.Master.UserDefaultOrganization.Delete";
        public const string Pages_Tenant_House_Master_UserDefaultOrganization_Edit = "Pages.Tenant.House.Master.UserDefaultOrganization.Edit";

        //ProductRecipesLink
        public const string Pages_Tenant_House_Master_ProductRecipesLink = "Pages.Tenant.House.Master.ProductRecipesLink";

        public const string Pages_Tenant_House_Master_ProductRecipesLink_Create = "Pages.Tenant.House.Master.ProductRecipesLink.Create";
        public const string Pages_Tenant_House_Master_ProductRecipesLink_Delete = "Pages.Tenant.House.Master.ProductRecipesLink.Delete";
        public const string Pages_Tenant_House_Master_ProductRecipesLink_Edit = "Pages.Tenant.House.Master.ProductRecipesLink.Edit";

        //PurchaseOrder
        public const string Pages_Tenant_House_Transaction_PurchaseOrder = "Pages.Tenant.House.Transaction.PurchaseOrder";

        public const string Pages_Tenant_House_Transaction_PurchaseOrder_Create = "Pages.Tenant.House.Transaction.PurchaseOrder.Create";
        public const string Pages_Tenant_House_Transaction_PurchaseOrder_Delete = "Pages.Tenant.House.Transaction.PurchaseOrder.Delete";
        public const string Pages_Tenant_House_Transaction_PurchaseOrder_Edit = "Pages.Tenant.House.Transaction.PurchaseOrder.Edit";
        public const string Pages_Tenant_House_Transaction_PurchaseOrder_Approve = "Pages.Tenant.House.Transaction.PurchaseOrder.Approve";
        public const string Pages_Tenant_House_Transaction_PurchaseOrder_CanEditApprovePurchaseOrderIfNotLinked = "Pages.Tenant.House.Transaction.PurchaseOrder.CanEditApprovePurchaseOrderIfNotLinked";
        public const string Pages_Tenant_House_Transaction_PurchaseOrder_CanViewMultipleLocationPurchaseOrder = "Pages.Tenant.House.Transaction.PurchaseOrder.CanViewMultipleLocationPurchaseOrder";
        public const string Pages_Tenant_House_Transaction_PurchaseOrder_CreateBulkPurchaseOrder = "Pages.Tenant.House.Transaction.PurchaseOrder.CreateBulkPurchaseOrder";

        //HouseReport

        public const string Pages_Tenant_House_Master_HouseReport = "Pages.Tenant.House.Master.HouseReport";
        public const string Pages_Tenant_House_Master_HouseReport_Costing = "Pages.Tenant.House.Master.HouseReport.Costing";
        public const string Pages_Tenant_House_Master_HouseReport_Stock = "Pages.Tenant.House.Master.HouseReport.Stock";
        public const string Pages_Tenant_House_Master_HouseReport_Invoice = "Pages.Tenant.House.Master.HouseReport.Invoice";
        public const string Pages_Tenant_House_Master_HouseReport_Material = "Pages.Tenant.House.Master.HouseReport.Material";
        public const string Pages_Tenant_House_Master_HouseReport_Analytical = "Pages.Tenant.House.Master.HouseReport.Analytical";
        public const string Pages_Tenant_House_Master_HouseReport_InterTransfer = "Pages.Tenant.House.Master.HouseReport.InterTransfer";

        //Template

        public const string Pages_Tenant_House_Master_Template = "Pages.Tenant.House.Master.Template";
        public const string Pages_Tenant_House_Master_Template_Create = "Pages.Tenant.House.Master.Template.Create";
        public const string Pages_Tenant_House_Master_Template_Delete = "Pages.Tenant.House.Master.Template.Delete";
        public const string Pages_Tenant_House_Master_Template_Edit = "Pages.Tenant.House.Master.Template.Edit";

        //customer

        public const string Pages_Tenant_House_Master_Customer = "Pages.Tenant.House.Master.Customer";
        public const string Pages_Tenant_House_Master_Customer_Create = "Pages.Tenant.House.Master.Customer.Create";
        public const string Pages_Tenant_House_Master_Customer_Delete = "Pages.Tenant.House.Master.Customer.Delete";
        public const string Pages_Tenant_House_Master_Customer_Edit = "Pages.Tenant.House.Master.Customer.Edit";

        //CustomerMaterial

        public const string Pages_Tenant_House_Master_CustomerMaterial = "Pages.Tenant.House.Master.CustomerMaterial";
        public const string Pages_Tenant_House_Master_CustomerMaterial_Create = "Pages.Tenant.House.Master.CustomerMaterial.Create";
        public const string Pages_Tenant_House_Master_CustomerMaterial_Delete = "Pages.Tenant.House.Master.CustomerMaterial.Delete";
        public const string Pages_Tenant_House_Master_CustomerMaterial_Edit = "Pages.Tenant.House.Master.CustomerMaterial.Edit";

        // Customer Tag Definition

        public const string Pages_Tenant_House_Master_CustomerTagDefinition = "Pages.Tenant.House.Master.CustomerTagDefinition";
        public const string Pages_Tenant_House_Master_CustomerTagDefinition_Create = "Pages.Tenant.House.Master.CustomerTagDefinition.Create";
        public const string Pages_Tenant_House_Master_CustomerTagDefinition_Delete = "Pages.Tenant.House.Master.CustomerTagDefinition.Delete";
        public const string Pages_Tenant_House_Master_CustomerTagDefinition_Edit = "Pages.Tenant.House.Master.CustomerTagDefinition.Edit";

        //   Customer Material Price
        public const string Pages_Tenant_House_Master_CustomerTagMaterialPrice = "Pages.Tenant.House.Master.CustomerTagMaterialPrice";

        public const string Pages_Tenant_House_Master_CustomerTagMaterialPrice_Edit = "Pages.Tenant.House.Master.CustomerTagMaterialPrice.Edit";

        //   Material Ingredient
        public const string Pages_Tenant_House_Master_MaterialIngredient = "Pages.Tenant.House.Master.MaterialIngredient";

        public const string Pages_Tenant_House_Master_MaterialIngredient_Create = "Pages.Tenant.House.Master.MaterialIngredient.Create";
        public const string Pages_Tenant_House_Master_MaterialIngredient_Delete = "Pages.Tenant.House.Master.MaterialIngredient.Delete";
        public const string Pages_Tenant_House_Master_MaterialIngredient_Edit = "Pages.Tenant.House.Master.MaterialIngredient.Edit";

        // Material Menu Mappings
        public const string Pages_Tenant_House_Master_MaterialMenuMapping = "Pages.Tenant.House.Master.MaterialMenuMapping";

        public const string Pages_Tenant_House_Master_MaterialMenuMapping_Create = "Pages.Tenant.House.Master.MaterialMenuMapping.Create";
        public const string Pages_Tenant_House_Master_MaterialMenuMapping_Delete = "Pages.Tenant.House.Master.MaterialMenuMapping.Delete";
        public const string Pages_Tenant_House_Master_MaterialMenuMapping_Edit = "Pages.Tenant.House.Master.MaterialMenuMapping.Edit";

        // Yield

        public const string Pages_Tenant_House_Transaction_Yield = "Pages.Tenant.House.Transaction.Yield";
        public const string Pages_Tenant_House_Transaction_Yield_Create = "Pages.Tenant.House.Transaction.Yield.Create";
        public const string Pages_Tenant_House_Transaction_Yield_Delete = "Pages.Tenant.House.Transaction.Yield.Delete";
        public const string Pages_Tenant_House_Transaction_Yield_Edit = "Pages.Tenant.House.Transaction.Yield.Edit";

        //  Production

        public const string Pages_Tenant_House_Transaction_Production = "Pages.Tenant.House.Transaction.Production";
        public const string Pages_Tenant_House_Transaction_Production_Create = "Pages.Tenant.House.Transaction.Production.Create";
        public const string Pages_Tenant_House_Transaction_Production_Delete = "Pages.Tenant.House.Transaction.Production.Delete";
        public const string Pages_Tenant_House_Transaction_Production_Edit = "Pages.Tenant.House.Transaction.Production.Edit";
        public const string Pages_Tenant_House_Transaction_Production_Allowed_Without_Issue = "Pages.Tenant.House.Transaction.Production.Allowed_Without_Issue";
        public const string Pages_Tenant_House_Transaction_Automatic_Issue_Based_On_Production = "Pages.Tenant.House.Transaction.Production.Automatic_Issue_Based_On_Production";

        //  Day Close
        public const string Pages_Tenant_House_Transaction_DayClose = "Pages.Tenant.House.Transaction.DayClose";

        public const string Pages_Tenant_House_Transaction_DayClose_Create = "Pages.Tenant.House.Transaction.DayClose.Create";
        public const string Pages_Tenant_House_Transaction_DayClose_SameDayClose = "Pages.Tenant.House.Transaction.DayClose.SameDayClose";
        public const string Pages_Tenant_House_Transaction_DayClose_MissingSalesIntoDayClose = "Pages.Tenant.House.Transaction.DayClose.MissingSalesIntoDayClose";

        public const string Pages_Tenant_House_Transaction_DayCloseHideSalesAndStock = "Pages.Tenant.House.Transaction.DayCloseHideSalesAndStock";
        public const string Pages_Tenant_House_Transaction_DayClose_HideSales = "Pages.Tenant.House.Transaction.DayClose.HideSalesOnDayClose";
        public const string Pages_Tenant_House_Transaction_DayClose_HideCarryOverStock = "Pages.Tenant.House.Transaction.DayClose.HideCarryOverStockInDayClose";
        public const string Pages_Tenant_House_Transaction_DayClose_HideStockInTransfer = "Pages.Tenant.House.Transaction.DayClose.HideStockInTransfer";
        public const string Pages_Tenant_House_Transaction_DayClose_HideStockInAdjustment = "Pages.Tenant.House.Transaction.DayClose.HideStockInAdjustment";

        //  MultipleUOM Allowed
        public const string Pages_Tenant_House_Transaction_MultipleUOMAllowed = "Pages.Tenant.House.Transaction.MultipleUOMAllowed";

        // Direct Approval
        public const string Pages_Tenant_House_Transaction_InterTransferDirectApproval = "Pages.Tenant.House.Transaction.InterTransferDirectApproval";

        public const string Pages_Tenant_House_Transaction_InterTransferDirectApproval_Create = "Pages.Tenant.House.Transaction.InterTransferDirectApproval.Create";
        public const string Pages_Tenant_House_Transaction_InterTransferDirectApproval_Delete = "Pages.Tenant.House.Transaction.InterTransferDirectApproval.Delete";
        public const string Pages_Tenant_House_Transaction_InterTransferDirectApproval_Edit = "Pages.Tenant.House.Transaction.InterTransferDirectApproval.Edit";

        public const string Pages_Tenant_House_Transaction_InterTransferApproval_DirectApprove = "Pages.Tenant.House.Transaction.InterTransferApproval.DirectApprove";
        public const string Pages_Tenant_House_Transaction_InterTransferApproval_DirectApproveAndReceive = "Pages.Tenant.House.Transaction.InterTransferApproval.DirectApproveAndReceive";
        public const string Pages_Tenant_House_Transaction_InterTransferApproval_RequestForOtherLocation = "Pages.Tenant.House.Transaction.InterTransferApproval.RequestForOtherLocation";

        // Dine Plan Tax
        public const string Pages_Tenant_Connect_Master_DinePlanTax = "Pages.Tenant.Connect.Master.DinePlanTax";

        public const string Pages_Tenant_Connect_Master_DinePlanTax_Create = "Pages.Tenant.Connect.Master.DinePlanTax.Create";
        public const string Pages_Tenant_Connect_Master_DinePlanTax_Delete = "Pages.Tenant.Connect.Master.DinePlanTax.Delete";
        public const string Pages_Tenant_Connect_Master_DinePlanTax_Edit = "Pages.Tenant.Connect.Master.DinePlanTax.Edit";

        public const string Pages_Tenant_Connect_Master_Calculation = "Pages.Tenant.Connect.Master.Calculation";
        public const string Pages_Tenant_Connect_Master_Calculation_Create = "Pages.Tenant.Connect.Master.Calculation.Create";
        public const string Pages_Tenant_Connect_Master_Calculation_Delete = "Pages.Tenant.Connect.Master.Calculation.Delete";
        public const string Pages_Tenant_Connect_Master_Calculation_Edit = "Pages.Tenant.Connect.Master.Calculation.Edit";

        //  Request
        public const string Pages_Tenant_House_Transaction_Request = "Pages.Tenant.House.Transaction.Request";

        public const string Pages_Tenant_House_Transaction_Request_Create = "Pages.Tenant.House.Transaction.Request.Create";
        public const string Pages_Tenant_House_Transaction_Request_Delete = "Pages.Tenant.House.Transaction.Request.Delete";
        public const string Pages_Tenant_House_Transaction_Request_Edit = "Pages.Tenant.House.Transaction.Request.Edit";

        //  Proudction Unit
        public const string Pages_Tenant_House_Master_ProductionUnit = "Pages.Tenant.House.Master.ProductionUnit";

        public const string Pages_Tenant_House_Master_ProductionUnit_Create = "Pages.Tenant.House.Master.ProductionUnit.Create";
        public const string Pages_Tenant_House_Master_ProductionUnit_Delete = "Pages.Tenant.House.Master.ProductionUnit.Delete";
        public const string Pages_Tenant_House_Master_ProductionUnit_Edit = "Pages.Tenant.House.Master.ProductionUnit.Edit";

        //Purchase Return
        public const string Pages_Tenant_House_Transaction_PurchaseReturn = "Pages.Tenant.House.Transaction.PurchaseReturn";

        public const string Pages_Tenant_House_Transaction_PurchaseReturn_Create = "Pages.Tenant.House.Transaction.PurchaseReturn.Create";
        public const string Pages_Tenant_House_Transaction_PurchaseReturn_Delete = "Pages.Tenant.House.Transaction.PurchaseReturn.Delete";
        public const string Pages_Tenant_House_Transaction_PurchaseReturn_Edit = "Pages.Tenant.House.Transaction.PurchaseReturn.Edit";
        public const string Pages_Tenant_House_Transaction_PurchaseReturn_EditPrice = "Pages.Tenant.House.Transaction.PurchaseReturn.EditPrice";

        //SalesOrder

        public const string Pages_Tenant_House_Transaction_SalesOrder = "Pages.Tenant.House.Transaction.SalesOrder";
        public const string Pages_Tenant_House_Transaction_SalesOrder_Create = "Pages.Tenant.House.Transaction.SalesOrder.Create";
        public const string Pages_Tenant_House_Transaction_SalesOrder_Delete = "Pages.Tenant.House.Transaction.SalesOrder.Delete";
        public const string Pages_Tenant_House_Transaction_SalesOrder_Edit = "Pages.Tenant.House.Transaction.SalesOrder.Edit";
        public const string Pages_Tenant_House_Transaction_SalesOrder_Approve = "Pages.Tenant.House.Transaction.SalesOrder.Approve";

        //SalesOrderDeliveryOrder
        public const string Pages_Tenant_House_Transaction_SalesDeliveryOrder = "Pages.Tenant.House.Transaction.SalesDeliveryOrder";

        public const string Pages_Tenant_House_Transaction_SalesDeliveryOrder_Create = "Pages.Tenant.House.Transaction.SalesDeliveryOrder.Create";
        public const string Pages_Tenant_House_Transaction_SalesDeliveryOrder_Delete = "Pages.Tenant.House.Transaction.SalesDeliveryOrder.Delete";
        public const string Pages_Tenant_House_Transaction_SalesDeliveryOrder_Edit = "Pages.Tenant.House.Transaction.SalesDeliveryOrder.Edit";
        //SalesInvoice

        public const string Pages_Tenant_House_Transaction_SalesInvoice = "Pages.Tenant.House.Transaction.SalesInvoice";
        public const string Pages_Tenant_House_Transaction_SalesInvoice_Create = "Pages.Tenant.House.Transaction.SalesInvoice.Create";
        public const string Pages_Tenant_House_Transaction_SalesInvoice_Delete = "Pages.Tenant.House.Transaction.SalesInvoice.Delete";
        public const string Pages_Tenant_House_Transaction_SalesInvoice_Edit = "Pages.Tenant.House.Transaction.SalesInvoice.Edit";
        //ClosingStock

        public const string Pages_Tenant_House_Transaction_ClosingStock = "Pages.Tenant.House.Transaction.ClosingStock";
        public const string Pages_Tenant_House_Transaction_ClosingStock_Create = "Pages.Tenant.House.Transaction.ClosingStock.Create";
        public const string Pages_Tenant_House_Transaction_ClosingStock_Delete = "Pages.Tenant.House.Transaction.ClosingStock.Delete";
        public const string Pages_Tenant_House_Transaction_ClosingStock_Edit = "Pages.Tenant.House.Transaction.ClosingStock.Edit";

        //MenuItemWastage
        public const string Pages_Tenant_House_Transaction_MenuItemWastage = "Pages.Tenant.House.Transaction.MenuItemWastage";

        public const string Pages_Tenant_House_Transaction_MenuItemWastage_Create = "Pages.Tenant.House.Transaction.MenuItemWastage.Create";
        public const string Pages_Tenant_House_Transaction_MenuItemWastage_Delete = "Pages.Tenant.House.Transaction.MenuItemWastage.Delete";
        public const string Pages_Tenant_House_Transaction_MenuItemWastage_Edit = "Pages.Tenant.House.Transaction.MenuItemWastage.Edit";

        //PurchaseCategory
        public const string Pages_Tenant_House_Master_PurchaseCategory = "Pages.Tenant.House.Master.PurchaseCategory";

        public const string Pages_Tenant_House_Master_PurchaseCategory_Create = "Pages.Tenant.House.Master.PurchaseCategory.Create";
        public const string Pages_Tenant_House_Master_PurchaseCategory_Delete = "Pages.Tenant.House.Master.PurchaseCategory.Delete";
        public const string Pages_Tenant_House_Master_PurchaseCategory_Edit = "Pages.Tenant.House.Master.PurchaseCategory.Edit";

        #endregion House

        #region Cluster
        public const string Pages_Tenant_Cluster = "Pages.Tenant.Cluster";
        public const string Pages_Tenant_Cluster_Master = "Pages.Tenant.Cluster.Master";

        public const string Pages_Tenant_Cluster_Master_DelAggLocationGroup = "Pages.Tenant.Cluster.Master.DelAggLocationGroup";
        public const string Pages_Tenant_Cluster_Master_DelAggLocationGroup_Create = "Pages.Tenant.Cluster.Master.DelAggLocationGroup.Create";
        public const string Pages_Tenant_Cluster_Master_DelAggLocationGroup_Delete = "Pages.Tenant.Cluster.Master.DelAggLocationGroup.Delete";
        public const string Pages_Tenant_Cluster_Master_DelAggLocationGroup_Edit = "Pages.Tenant.Cluster.Master.DelAggLocationGroup.Edit";

        public const string Pages_Tenant_Cluster_Master_DelAggLocation = "Pages.Tenant.Cluster.Master.DelAggLocation";
        public const string Pages_Tenant_Cluster_Master_DelAggLocation_Create = "Pages.Tenant.Cluster.Master.DelAggLocation.Create";
        public const string Pages_Tenant_Cluster_Master_DelAggLocation_Delete = "Pages.Tenant.Cluster.Master.DelAggLocation.Delete";
        public const string Pages_Tenant_Cluster_Master_DelAggLocation_Edit = "Pages.Tenant.Cluster.Master.DelAggLocation.Edit";

        public const string Pages_Tenant_Cluster_Master_DelAggLocMapping = "Pages.Tenant.Cluster.Master.DelAggLocMapping";
        public const string Pages_Tenant_Cluster_Master_DelAggLocMapping_Create = "Pages.Tenant.Cluster.Master.DelAggLocMapping.Create";
        public const string Pages_Tenant_Cluster_Master_DelAggLocMapping_Delete = "Pages.Tenant.Cluster.Master.DelAggLocMapping.Delete";
        public const string Pages_Tenant_Cluster_Master_DelAggLocMapping_Edit = "Pages.Tenant.Cluster.Master.DelAggLocMapping.Edit";

        public const string Pages_Tenant_Cluster_Master_DelAggLocationItem = "Pages.Tenant.Cluster.Master.DelAggLocationItem";
        public const string Pages_Tenant_Cluster_Master_DelAggLocationItem_Create = "Pages.Tenant.Cluster.Master.DelAggLocationItem.Create";
        public const string Pages_Tenant_Cluster_Master_DelAggLocationItem_Delete = "Pages.Tenant.Cluster.Master.DelAggLocationItem.Delete";
        public const string Pages_Tenant_Cluster_Master_DelAggLocationItem_Edit = "Pages.Tenant.Cluster.Master.DelAggLocationItem.Edit";

        public const string Pages_Tenant_Cluster_Master_DelAggVariantGroup = "Pages.Tenant.Cluster.Master.DelAggVariantGroup";
        public const string Pages_Tenant_Cluster_Master_DelAggVariantGroup_Create = "Pages.Tenant.Cluster.Master.DelAggVariantGroup.Create";
        public const string Pages_Tenant_Cluster_Master_DelAggVariantGroup_Delete = "Pages.Tenant.Cluster.Master.DelAggVariantGroup.Delete";
        public const string Pages_Tenant_Cluster_Master_DelAggVariantGroup_Edit = "Pages.Tenant.Cluster.Master.DelAggVariantGroup.Edit";

        public const string Pages_Tenant_Cluster_Master_DelAggModifierGroup = "Pages.Tenant.Cluster.Master.DelAggModifierGroup";
        public const string Pages_Tenant_Cluster_Master_DelAggModifierGroup_Create = "Pages.Tenant.Cluster.Master.DelAggModifierGroup.Create";
        public const string Pages_Tenant_Cluster_Master_DelAggModifierGroup_Delete = "Pages.Tenant.Cluster.Master.DelAggModifierGroup.Delete";
        public const string Pages_Tenant_Cluster_Master_DelAggModifierGroup_Edit = "Pages.Tenant.Cluster.Master.DelAggModifierGroup.Edit";

        public const string Pages_Tenant_Cluster_Master_DelAggVariant = "Pages.Tenant.Cluster.Master.DelAggVariant";
        public const string Pages_Tenant_Cluster_Master_DelAggVariant_Create = "Pages.Tenant.Cluster.Master.DelAggVariant.Create";
        public const string Pages_Tenant_Cluster_Master_DelAggVariant_Delete = "Pages.Tenant.Cluster.Master.DelAggVariant.Delete";
        public const string Pages_Tenant_Cluster_Master_DelAggVariant_Edit = "Pages.Tenant.Cluster.Master.DelAggVariant.Edit";

        public const string Pages_Tenant_Cluster_Master_DelAggModifier = "Pages.Tenant.Cluster.Master.DelAggModifier";
        public const string Pages_Tenant_Cluster_Master_DelAggModifier_Create = "Pages.Tenant.Cluster.Master.DelAggModifier.Create";
        public const string Pages_Tenant_Cluster_Master_DelAggModifier_Delete = "Pages.Tenant.Cluster.Master.DelAggModifier.Delete";
        public const string Pages_Tenant_Cluster_Master_DelAggModifier_Edit = "Pages.Tenant.Cluster.Master.DelAggModifier.Edit";

        public const string Pages_Tenant_Cluster_Master_DelAggItem = "Pages.Tenant.Cluster.Master.DelAggItem";
        public const string Pages_Tenant_Cluster_Master_DelAggItem_Create = "Pages.Tenant.Cluster.Master.DelAggItem.Create";
        public const string Pages_Tenant_Cluster_Master_DelAggItem_Delete = "Pages.Tenant.Cluster.Master.DelAggItem.Delete";
        public const string Pages_Tenant_Cluster_Master_DelAggItem_Edit = "Pages.Tenant.Cluster.Master.DelAggItem.Edit";

        public const string Pages_Tenant_Cluster_Master_DelAggItemGroup = "Pages.Tenant.Cluster.Master.DelAggItemGroup";
        public const string Pages_Tenant_Cluster_Master_DelAggItemGroup_Create = "Pages.Tenant.Cluster.Master.DelAggItemGroup.Create";
        public const string Pages_Tenant_Cluster_Master_DelAggItemGroup_Delete = "Pages.Tenant.Cluster.Master.DelAggItemGroup.Delete";
        public const string Pages_Tenant_Cluster_Master_DelAggItemGroup_Edit = "Pages.Tenant.Cluster.Master.DelAggItemGroup.Edit";

        public const string Pages_Tenant_Cluster_Master_DelAggTax = "Pages.Tenant.Cluster.Master.DelAggTax";
        public const string Pages_Tenant_Cluster_Master_DelAggTax_Create = "Pages.Tenant.Cluster.Master.DelAggTax.Create";
        public const string Pages_Tenant_Cluster_Master_DelAggTax_Delete = "Pages.Tenant.Cluster.Master.DelAggTax.Delete";
        public const string Pages_Tenant_Cluster_Master_DelAggTax_Edit = "Pages.Tenant.Cluster.Master.DelAggTax.Edit";

        public const string Pages_Tenant_Cluster_DelTimingGroup = "Pages.Tenant.Cluster.DelTimingGroup";
        public const string Pages_Tenant_Cluster_DelTimingGroup_Create = "Pages.Tenant.Cluster.DelTimingGroup.Create";
        public const string Pages_Tenant_Cluster_DelTimingGroup_Delete = "Pages.Tenant.Cluster.DelTimingGroup.Delete";
        public const string Pages_Tenant_Cluster_DelTimingGroup_Edit = "Pages.Tenant.Cluster.DelTimingGroup.Edit";

        public const string Pages_Tenant_Cluster_DelAggCategory = "Pages.Tenant.Cluster.DelAggCategory";
        public const string Pages_Tenant_Cluster_DelAggCategory_Create = "Pages.Tenant.Cluster.DelAggCategory.Create";
        public const string Pages_Tenant_Cluster_DelAggCategory_Delete = "Pages.Tenant.Cluster.DelAggCategory.Delete";
        public const string Pages_Tenant_Cluster_DelAggCategory_Edit = "Pages.Tenant.Cluster.DelAggCategory.Edit";

        public const string Pages_Administration_DelAggLanguages = "Pages.Administration.DelAggLanguages";
        public const string Pages_Administration_DelAggLanguages_Create = "Pages.Administration.DelAggLanguages.Create";
        public const string Pages_Administration_DelAggLanguages_Edit = "Pages.Administration.DelAggLanguages.Edit";
        public const string Pages_Administration_DelAggLanguages_Delete = "Pages.Administration.DelAggLanguages.Delete";
        public const string Pages_Administration_DelAggLanguages_ChangeTexts = "Pages.Administration.DelAggLanguages.ChangeTexts";

        public const string Pages_Tenant_Cluster_DelAggCharge = "Pages.Tenant.Cluster.DelAggCharge";
        public const string Pages_Tenant_Cluster_DelAggCharge_Create = "Pages.Tenant.Cluster.DelAggCharge.Create";
        public const string Pages_Tenant_Cluster_DelAggCharge_Delete = "Pages.Tenant.Cluster.DelAggCharge.Delete";
        public const string Pages_Tenant_Cluster_DelAggCharge_Edit = "Pages.Tenant.Cluster.DelAggCharge.Edit";

        public const string Pages_Tenant_Cluster_DelAggImage = "Pages.Tenant.Cluster.DelAggImage";
        public const string Pages_Tenant_Cluster_DelAggImage_Create = "Pages.Tenant.Cluster.DelAggImage.Create";
        public const string Pages_Tenant_Cluster_DelAggImage_Delete = "Pages.Tenant.Cluster.DelAggImage.Delete";
        public const string Pages_Tenant_Cluster_DelAggImage_Edit = "Pages.Tenant.Cluster.DelAggImage.Edit";


        #endregion Cluster

        #region Engage

        //Enage Permissions
        public const string Pages_Tenant_Engage = "Pages.Tenant.Engage";

        public const string Pages_Tenant_Engage_GiftVoucherType = "Pages.Tenant.Engage.GiftVoucherType";
        public const string Pages_Tenant_Engage_GiftVoucherType_Create = "Pages.Tenant.Engage.GiftVoucherType.Create";
        public const string Pages_Tenant_Engage_GiftVoucherType_Delete = "Pages.Tenant.Engage.GiftVoucherType.Delete";
        public const string Pages_Tenant_Engage_GiftVoucherType_Edit = "Pages.Tenant.Engage.GiftVoucherType.Edit";

        public const string Pages_Tenant_Engage_Membership = "Pages.Tenant.Engage.Membership";

        public const string Pages_Tenant_Engage_MembershipTier = "Pages.Tenant.Engage.MembershipTier";
        public const string Pages_Tenant_Engage_MembershipTier_Create = "Pages.Tenant.Engage.MembershipTier.Create";
        public const string Pages_Tenant_Engage_MembershipTier_Edit = "Pages.Tenant.Engage.MembershipTier.Edit";
        public const string Pages_Tenant_Engage_MembershipTier_Delete = "Pages.Tenant.Engage.MembershipTier.Delete";

        public const string Pages_Tenant_Engage_Member_Account = "Pages.Tenant.Engage.Member.Account";
        public const string Pages_Tenant_Engage_Member_Account_Create = "Pages.Tenant.Engage.Member.Account.Create";
        public const string Pages_Tenant_Engage_Member_Account_Edit = "Pages.Tenant.Engage.Member.Account.Edit";
        public const string Pages_Tenant_Engage_Member_Account_Delete = "Pages.Tenant.Engage.Member.Account.Delete";

        public const string Pages_Tenant_Engage_Member = "Pages.Tenant.Engage.Member";
        public const string Pages_Tenant_Engage_Member_Create = "Pages.Tenant.Engage.Member.Create";
        public const string Pages_Tenant_Engage_Member_Delete = "Pages.Tenant.Engage.Member.Delete";
        public const string Pages_Tenant_Engage_Member_Edit = "Pages.Tenant.Engage.Member.Edit";
        public const string Pages_Tenant_Engage_MemberPoint_Delete = "Pages.Tenant.Engage.MemberPoint.Delete";

        public const string Pages_Tenant_Engage_LoyaltyProgram = "Pages.Tenant.Engage.LoyaltyProgram";
        public const string Pages_Tenant_Engage_LoyaltyProgram_Create = "Pages.Tenant.Engage.LoyaltyProgram.Create";
        public const string Pages_Tenant_Engage_LoyaltyProgram_Delete = "Pages.Tenant.Engage.LoyaltyProgram.Delete";
        public const string Pages_Tenant_Engage_LoyaltyProgram_Edit = "Pages.Tenant.Engage.LoyaltyProgram.Edit";

        public const string Pages_Tenant_Engage_Stage = "Pages.Tenant.Engage.Stage";
        public const string Pages_Tenant_Engage_Stage_Create = "Pages.Tenant.Engage.Stage.Create";
        public const string Pages_Tenant_Engage_Stage_Delete = "Pages.Tenant.Engage.Stage.Delete";
        public const string Pages_Tenant_Engage_Stage_Edit = "Pages.Tenant.Engage.Stage.Edit";

        public const string Pages_Tenant_Engage_Reservation = "Pages.Tenant.Engage.Reservation";

        public const string Pages_Tenant_Engage_Reservation_Create = "Pages.Tenant.Engage.Reservation.Create";
        public const string Pages_Tenant_Engage_Reservation_Delete = "Pages.Tenant.Engage.Reservation.Delete";
        public const string Pages_Tenant_Engage_Reservation_Edit = "Pages.Tenant.Engage.Reservation.Edit";

        public const string Pages_Tenant_Engage_GiftVoucherCategory = "Pages.Tenant.Engage.GiftVoucherCategory";
        public const string Pages_Tenant_Engage_GiftVoucherCategory_Create = "Pages.Tenant.Engage.GiftVoucherCategory.Create";
        public const string Pages_Tenant_Engage_GiftVoucherCategory_Delete = "Pages.Tenant.Engage.GiftVoucherCategory.Delete";
        public const string Pages_Tenant_Engage_GiftVoucherCategory_Edit = "Pages.Tenant.Engage.GiftVoucherCategory.Edit";

        public const string Pages_Tenant_Engage_ServiceInfo = "Pages.Tenant.Engage.ServiceInfo";
        public const string Pages_Tenant_Engage_ServiceInfo_Template = "Pages.Tenant.Engage.ServiceInfo.Template";
        public const string Pages_Tenant_Engage_ServiceInfo_Template_Create = "Pages.Tenant.Engage.ServiceInfo.Template.Create";
        public const string Pages_Tenant_Engage_ServiceInfo_Template_Edit = "Pages.Tenant.Engage.ServiceInfo.Template.Edit";
        public const string Pages_Tenant_Engage_ServiceInfo_Template_Delete = "Pages.Tenant.Engage.ServiceInfo.Template.Delete";

        #endregion Engage

        #region Wheel

        //Wheel Permissions

        public const string Pages_Tenant_Wheel = "Pages.Tenant.Wheel";
        public const string Pages_Tenant_Wheel_Dashboard = "Pages.Tenant.Wheel.Dashboard";
        public const string Pages_Tenant_Wheel_Order = "Pages.Tenant.Wheel.Order";

        #endregion Wheel

        #region Cater

        //Cater Permissions

        public const string Pages_Tenant_Cater = "Pages.Tenant.Cater";
        public const string Pages_Tenant_Cater_Dashboard = "Pages.Tenant.Cater.Dashboard";

        public const string Pages_Tenant_Cater_Customer = "Pages.Tenant.Cater.Customer";
        public const string Pages_Tenant_Cater_Customer_Create = "Pages.Tenant.Cater.Customer.Create";
        public const string Pages_Tenant_Cater_Customer_Delete = "Pages.Tenant.Cater.Customer.Delete";
        public const string Pages_Tenant_Cater_Customer_Edit = "Pages.Tenant.Cater.Customer.Edit";

        public const string Pages_Tenant_Cater_QuotationHeader = "Pages.Tenant.Cater.QuotationHeader";
        public const string Pages_Tenant_Cater_QuotationHeader_Create = "Pages.Tenant.Cater.QuotationHeader.Create";
        public const string Pages_Tenant_Cater_QuotationHeader_Delete = "Pages.Tenant.Cater.QuotationHeader.Delete";
        public const string Pages_Tenant_Cater_QuotationHeader_Edit = "Pages.Tenant.Cater.QuotationHeader.Edit";

        public const string Pages_Tenant_Cater_QuotationHeader_Setting = "Pages.Tenant.Cater.QuotationHeader.Setting";
        public const string Pages_Tenant_Cater_QuotationHeader_Setting_Costing = "Pages.Tenant.Cater.QuotationHeader.Setting.Costing";

        public const string Pages_Tenant_Cater_QuotationHeader_Setting_CanGetOrderForOtherLocation = "Pages.Tenant.Cater.QuotationHeader.Setting.GetCaterOrderForOtherLocation";
        public const string Pages_Tenant_Cater_QuotationHeader_AutoPo = "Pages.Tenant.Cater.QuotationHeader.AutoPo";

        #endregion Cater

        #region Touch

        //Touch Permissions
        public const string Pages_Tenant_Touch = "Pages.Tenant.Touch";

        public const string Pages_Tenant_Touch_Menu = "Pages.Tenant.Touch.Menu";

        #endregion Touch

        #region Swipe

        //Swipe Permissions
        public const string Pages_Host_Swipe = "Pages.Host.Swipe";

        public const string Pages_Host_Swipe_Dashboard = "Pages.Host.Swipe.Dashboard";
        public const string Pages_Host_Swipe_Master = "Pages.Host.Swipe.Master";
        public const string Pages_Host_Swipe_Transaction = "Pages.Host.Swipe.Transaction";
        public const string Pages_Host_Swipe_Report = "Pages.Host.Swipe.Report";

        public const string Pages_Host_Swipe_Master_SwipeMember = "Pages.Host.Swipe.Master.SwipeMember";
        public const string Pages_Host_Swipe_Master_SwipeMember_Create = "Pages.Host.Swipe.Master.SwipeMember.Create";
        public const string Pages_Host_Swipe_Master_SwipeMember_Delete = "Pages.Host.Swipe.Master.SwipeMember.Delete";
        public const string Pages_Host_Swipe_Master_SwipeMember_Edit = "Pages.Host.Swipe.Master.SwipeMember.Edit";

        public const string Pages_Host_Swipe_Transaction_MemberCard = "Pages.Host.Swipe.Transaction.MemberCard";
        public const string Pages_Host_Swipe_Transaction_MemberCard_Create = "Pages.Host.Swipe.Transaction.MemberCard.Create";
        public const string Pages_Host_Swipe_Transaction_MemberCard_Delete = "Pages.Host.Swipe.Transaction.MemberCard.Delete";
        public const string Pages_Host_Swipe_Transaction_MemberCard_Edit = "Pages.Host.Swipe.Transaction.MemberCard.Edit";

        public const string Pages_Host_Swipe_Master_SwipeCard = "Pages.Host.Swipe.Master.SwipeCard";
        public const string Pages_Host_Swipe_Master_SwipeCard_Create = "Pages.Host.Swipe.Master.SwipeCard.Create";
        public const string Pages_Host_Swipe_Master_SwipeCard_Delete = "Pages.Host.Swipe.Master.SwipeCard.Delete";
        public const string Pages_Host_Swipe_Master_SwipeCard_Edit = "Pages.Host.Swipe.Master.SwipeCard.Edit";
        public const string Pages_Host_Swipe_Master_SwipeCard_CanOverRide_CardType = "Pages.Host.Swipe.Master.SwipeCard.CanOverRide_CardType";

        public const string Pages_Host_Swipe_Master_SwipePaymentType = "Pages.Host.Swipe.Master.SwipePaymentType";
        public const string Pages_Host_Swipe_Master_SwipePaymentType_Create = "Pages.Host.Swipe.Master.SwipePaymentType.Create";
        public const string Pages_Host_Swipe_Master_SwipePaymentType_Delete = "Pages.Host.Swipe.Master.SwipePaymentType.Delete";
        public const string Pages_Host_Swipe_Master_SwipePaymentType_Edit = "Pages.Host.Swipe.Master.SwipePaymentType.Edit";

        public const string Pages_Host_Swipe_Transaction_SwipeShift = "Pages.Host.Swipe.Transaction.SwipeShift";
        public const string Pages_Host_Swipe_Transaction_SwipeShift_Create = "Pages.Host.Swipe.Transaction.SwipeShift.Create";
        public const string Pages_Host_Swipe_Transaction_SwipeShift_End = "Pages.Host.Swipe.Transaction.SwipeShift.End";
        public const string Pages_Host_Swipe_Transaction_SwipeShift_BlindShift = "Pages.Host.Swipe.Transaction.SwipeShift.BlindShift";

        public const string Pages_Host_Swipe_Master_SwipeCardType = "Pages.Host.Swipe.Master.SwipeCardType";
        public const string Pages_Host_Swipe_Master_SwipeCardType_Create = "Pages.Host.Swipe.Master.SwipeCardType.Create";
        public const string Pages_Host_Swipe_Master_SwipeCardType_Delete = "Pages.Host.Swipe.Master.SwipeCardType.Delete";
        public const string Pages_Host_Swipe_Master_SwipeCardType_Edit = "Pages.Host.Swipe.Master.SwipeCardType.Edit";
        public const string Pages_Host_Swipe_Master_SwipeCardType_Lock = "Pages.Host.Swipe.Master.SwipeCardType.Lock";

        #endregion Swipe

        #region Tick

        public const string Pages_Tenant_Hr_Master = "Pages.Tenant.Hr.Master";

        public const string Pages_Tenant_Hr_AdminMaster = "Pages.Tenant.Hr.AdminMaster";
        public const string Pages_Tenant_Hr_Transaction = "Pages.Tenant.Hr.Transaction";
        public const string Pages_Tenant_EmployeeDashboard = "Pages.Tenant.EmployeeDashboard";
        public const string Pages_Tenant_EmployeeDashboard_ShowSalary = "Pages.Tenant.EmployeeDashboard.ShowSalaryInEmployeeDashBoard";

        public const string Pages_Tenant_Hr_Master_SkillSet = "Pages.Tenant.Hr.Master.SkillSet";
        public const string Pages_Tenant_Hr_Master_SkillSet_Create = "Pages.Tenant.Hr.Master.SkillSet.Create";
        public const string Pages_Tenant_Hr_Master_SkillSet_Delete = "Pages.Tenant.Hr.Master.SkillSet.Delete";
        public const string Pages_Tenant_Hr_Master_SkillSet_Edit = "Pages.Tenant.Hr.Master.SkillSet.Edit";

        public const string Pages_Tenant_Tick_EmployeeAttendance = "Pages.Tenant.Tick.EmployeeAttendance";
        public const string Pages_Tenant_Tick_EmployeeAttendance_Create = "Pages.Tenant.Tick.EmployeeAttendance.Create";
        public const string Pages_Tenant_Tick_EmployeeAttendance_Delete = "Pages.Tenant.Tick.EmployeeAttendance.Delete";
        public const string Pages_Tenant_Tick_EmployeeAttendance_Edit = "Pages.Tenant.Tick.EmployeeAttendance.Edit";

        //PersonalInformation

        public const string Pages_Tenant_Hr_Master_PersonalInformation = "Pages.Tenant.Hr.Master.PersonalInformation";
        public const string Pages_Tenant_Hr_Master_PersonalInformation_Create = "Pages.Tenant.Hr.Master.PersonalInformation.Create";
        public const string Pages_Tenant_Hr_Master_PersonalInformation_Delete = "Pages.Tenant.Hr.Master.PersonalInformation.Delete";
        public const string Pages_Tenant_Hr_Master_PersonalInformation_Edit = "Pages.Tenant.Hr.Master.PersonalInformation.Edit";
        public const string Pages_Tenant_Hr_Master_PersonalInformation_Import = "Pages.Tenant.Hr.Master.PersonalInformation.Import";
        public const string Pages_Tenant_Hr_Master_PersonalInformation_Clone = "Pages.Tenant.Hr.Master.PersonalInformation.Clone";
        public const string Pages_Tenant_Hr_Master_PersonalInformation_LeaveGrant = "Pages.Tenant.Hr.Master.PersonalInformation.LeaveGrant";
        public const string Pages_Tenant_Hr_Master_PersonalInformation_ChangeEmployeeCode = "Pages.Tenant.Hr.Master.PersonalInformation.ChangeEmployeeCode";

        //EmployeeDocumentInfo

        public const string Pages_Tenant_Hr_Master_EmployeeDocumentInfo = "Pages.Tenant.Hr.Master.EmployeeDocumentInfo";
        public const string Pages_Tenant_Hr_Master_EmployeeDocumentInfo_Create = "Pages.Tenant.Hr.Master.EmployeeDocumentInfo.Create";
        public const string Pages_Tenant_Hr_Master_EmployeeDocumentInfo_Delete = "Pages.Tenant.Hr.Master.EmployeeDocumentInfo.Delete";
        public const string Pages_Tenant_Hr_Master_EmployeeDocumentInfo_Edit = "Pages.Tenant.Hr.Master.EmployeeDocumentInfo.Edit";

        //Employee SkillSet

        public const string Pages_Tenant_Hr_Master_EmployeeSkillSet = "Pages.Tenant.Hr.Master.EmployeeSkillSet";
        public const string Pages_Tenant_Hr_Master_EmployeeSkillSet_Create = "Pages.Tenant.Hr.Master.EmployeeSkillSet.Create";
        public const string Pages_Tenant_Hr_Master_EmployeeSkillSet_Delete = "Pages.Tenant.Hr.Master.EmployeeSkillSet.Delete";
        public const string Pages_Tenant_Hr_Master_EmployeeSkillSet_Edit = "Pages.Tenant.Hr.Master.EmployeeSkillSet.Edit";

        //Salary Info

        public const string Pages_Tenant_Hr_Master_SalaryInfo = "Pages.Tenant.Hr.Master.SalaryInfo";
        public const string Pages_Tenant_Hr_Master_SalaryInfo_Create = "Pages.Tenant.Hr.Master.SalaryInfo.Create";
        public const string Pages_Tenant_Hr_Master_SalaryInfo_Delete = "Pages.Tenant.Hr.Master.SalaryInfo.Delete";
        public const string Pages_Tenant_Hr_Master_SalaryInfo_Edit = "Pages.Tenant.Hr.Master.SalaryInfo.Edit";
        public const string Pages_Tenant_Hr_Master_SalaryInfo_ShowSalary = "Pages.Tenant.Hr.Master.SalaryInfo.ShowSalary";
        public const string Pages_Tenant_Hr_Master_SalaryInfo_GenerateSalary = "Pages.Tenant.Hr.Master.SalaryInfo.GenerateSalary";
        public const string Pages_Tenant_Hr_Master_SalaryInfo_SalaryCostView = "Pages.Tenant.Hr.Master.SalaryInfo.SalaryCostView";

        //	Incentive Tags

        public const string Pages_Tenant_Hr_Transaction_IncentiveTag = "Pages.Tenant.Hr.Transaction.IncentiveTag";
        public const string Pages_Tenant_Hr_Transaction_IncentiveTag_Create = "Pages.Tenant.Hr.Transaction.IncentiveTag.Create";
        public const string Pages_Tenant_Hr_Transaction_IncentiveTag_Delete = "Pages.Tenant.Hr.Transaction.IncentiveTag.Delete";
        public const string Pages_Tenant_Hr_Transaction_IncentiveTag_Edit = "Pages.Tenant.Hr.Transaction.IncentiveTag.Edit";
        public const string Pages_Tenant_Hr_Transaction_IncentiveTag_BulkUpdateForAllEligibleEmployee = "Pages.Tenant.Hr.Transaction.IncentiveTag.BulkUpdateForAllEligibleEmployee";

        public const string Pages_Tenant_Hr_Transaction_IncentiveCategory = "Pages.Tenant.Hr.Transaction.IncentiveCategory";
        public const string Pages_Tenant_Hr_Transaction_IncentiveCategory_Create = "Pages.Tenant.Hr.Transaction.IncentiveCategory.Create";
        public const string Pages_Tenant_Hr_Transaction_IncentiveCategory_Delete = "Pages.Tenant.Hr.Transaction.IncentiveCategory.Delete";
        public const string Pages_Tenant_Hr_Transaction_IncentiveCategory_Edit = "Pages.Tenant.Hr.Transaction.IncentiveCategory.Edit";

        public const string Pages_Tenant_Hr_Transaction_ManualIncentive = "Pages.Tenant.Hr.Transaction.ManualIncentive";
        public const string Pages_Tenant_Hr_Transaction_ManualIncentive_Create = "Pages.Tenant.Hr.Transaction.ManualIncentive.Create";
        public const string Pages_Tenant_Hr_Transaction_ManualIncentive_Delete = "Pages.Tenant.Hr.Transaction.ManualIncentive.Delete";
        public const string Pages_Tenant_Hr_Transaction_ManualIncentive_Edit = "Pages.Tenant.Hr.Transaction.ManualIncentive.Edit";

        public const string Pages_Tenant_Hr_Master_EmployeeVsIncentive = "Pages.Tenant.Hr.Master.EmployeeVsIncentive";
        public const string Pages_Tenant_Hr_Master_EmployeeVsIncentive_Create = "Pages.Tenant.Hr.Master.EmployeeVsIncentive.Create";
        public const string Pages_Tenant_Hr_Master_EmployeeVsIncentive_Delete = "Pages.Tenant.Hr.Master.EmployeeVsIncentive.Delete";
        public const string Pages_Tenant_Hr_Master_EmployeeVsIncentive_Edit = "Pages.Tenant.Hr.Master.EmployeeVsIncentive.Edit";

        // Document Infos
        public const string Pages_Tenant_Hr_Master_DocumentInfo = "Pages.Tenant.Hr.Master.DocumentInfo";

        public const string Pages_Tenant_Hr_Master_DocumentInfo_Create = "Pages.Tenant.Hr.Master.DocumentInfo.Create";
        public const string Pages_Tenant_Hr_Master_DocumentInfo_Delete = "Pages.Tenant.Hr.Master.DocumentInfo.Delete";
        public const string Pages_Tenant_Hr_Master_DocumentInfo_Edit = "Pages.Tenant.Hr.Master.DocumentInfo.Edit";

        //AttendanceLog
        public const string Pages_Tenant_Hr_Master_AttendanceLog = "Pages.Tenant.Hr.Master.AttendanceLog";

        public const string Pages_Tenant_Hr_Master_AttendanceLog_Create = "Pages.Tenant.Hr.Master.AttendanceLog.Create";
        public const string Pages_Tenant_Hr_Master_AttendanceLog_Delete = "Pages.Tenant.Hr.Master.AttendanceLog.Delete";
        public const string Pages_Tenant_Hr_Master_AttendanceLog_Edit = "Pages.Tenant.Hr.Master.AttendanceLog.Edit";
        public const string Pages_Tenant_Hr_Master_AttendanceLog_Import = "Pages.Tenant.Hr.Master.AttendanceLog.Import";

        //LeaveType

        public const string Pages_Tenant_Hr_Master_LeaveType = "Pages.Tenant.Hr.Master.LeaveType";
        public const string Pages_Tenant_Hr_Master_LeaveType_Create = "Pages.Tenant.Hr.Master.LeaveType.Create";
        public const string Pages_Tenant_Hr_Master_LeaveType_Delete = "Pages.Tenant.Hr.Master.LeaveType.Delete";
        public const string Pages_Tenant_Hr_Master_LeaveType_Edit = "Pages.Tenant.Hr.Master.LeaveType.Edit";

        //LeaveRequest

        public const string Pages_Tenant_Hr_Master_LeaveRequest = "Pages.Tenant.Hr.Master.LeaveRequest";
        public const string Pages_Tenant_Hr_Master_LeaveRequest_Create = "Pages.Tenant.Hr.Master.LeaveRequest.Create";
        public const string Pages_Tenant_Hr_Master_LeaveRequest_Delete = "Pages.Tenant.Hr.Master.LeaveRequest.Delete";
        public const string Pages_Tenant_Hr_Master_LeaveRequest_Edit = "Pages.Tenant.Hr.Master.LeaveRequest.Edit";
        public const string Pages_Tenant_Hr_Master_LeaveRequest_Print = "Pages.Tenant.Hr.Master.LeaveRequest.Print";

        public const string Pages_Tenant_Hr_Master_YearWiseLeaveAllowedForEmployee = "Pages.Tenant.Hr.Master.YearWiseLeaveAllowedForEmployee";
        public const string Pages_Tenant_Hr_Master_YearWiseLeaveAllowedForEmployee_Create = "Pages.Tenant.Hr.Master.YearWiseLeaveAllowedForEmployee.Create";
        public const string Pages_Tenant_Hr_Master_YearWiseLeaveAllowedForEmployee_Delete = "Pages.Tenant.Hr.Master.YearWiseLeaveAllowedForEmployee.Delete";
        public const string Pages_Tenant_Hr_Master_YearWiseLeaveAllowedForEmployee_Edit = "Pages.Tenant.Hr.Master.YearWiseLeaveAllowedForEmployee.Edit";

        //Monthwise Work Days

        public const string Pages_Tenant_Hr_Master_MonthWiseWorkDay = "Pages.Tenant.Hr.Master.MonthWiseWorkDay";
        public const string Pages_Tenant_Hr_Master_MonthWiseWorkDay_Create = "Pages.Tenant.Hr.Master.MonthWiseWorkDay.Create";
        public const string Pages_Tenant_Hr_Master_MonthWiseWorkDay_Delete = "Pages.Tenant.Hr.Master.MonthWiseWorkDay.Delete";
        public const string Pages_Tenant_Hr_Master_MonthWiseWorkDay_Edit = "Pages.Tenant.Hr.Master.MonthWiseWorkDay.Edit";

        //Work Days
        public const string Pages_Tenant_Hr_Master_WorkDay = "Pages.Tenant.Hr.Master.WorkDay";

        public const string Pages_Tenant_Hr_Master_WorkDay_Edit = "Pages.Tenant.Hr.Master.WorkDay.Edit";

        public const string Pages_Tenant_Hr_Master_PublicHoliday = "Pages.Tenant.Hr.Master.PublicHoliday";
        public const string Pages_Tenant_Hr_Master_PublicHoliday_Create = "Pages.Tenant.Hr.Master.PublicHoliday.Create";
        public const string Pages_Tenant_Hr_Master_PublicHoliday_Delete = "Pages.Tenant.Hr.Master.PublicHoliday.Delete";
        public const string Pages_Tenant_Hr_Master_PublicHoliday_Edit = "Pages.Tenant.Hr.Master.PublicHoliday.Edit";

        public const string Pages_Tenant_Hr_Master_DcHeadMaster = "Pages.Tenant.Hr.Master.DcHeadMaster";
        public const string Pages_Tenant_Hr_Master_DcHeadMaster_Create = "Pages.Tenant.Hr.Master.DcHeadMaster.Create";
        public const string Pages_Tenant_Hr_Master_DcHeadMaster_Delete = "Pages.Tenant.Hr.Master.DcHeadMaster.Delete";
        public const string Pages_Tenant_Hr_Master_DcHeadMaster_Edit = "Pages.Tenant.Hr.Master.DcHeadMaster.Edit";

        public const string Pages_Tenant_Hr_Master_DcGroupMaster = "Pages.Tenant.Hr.Master.DcGroupMaster";
        public const string Pages_Tenant_Hr_Master_DcGroupMaster_Create = "Pages.Tenant.Hr.Master.DcGroupMaster.Create";
        public const string Pages_Tenant_Hr_Master_DcGroupMaster_Delete = "Pages.Tenant.Hr.Master.DcGroupMaster.Delete";
        public const string Pages_Tenant_Hr_Master_DcGroupMaster_Edit = "Pages.Tenant.Hr.Master.DcGroupMaster.Edit";

        public const string Pages_Tenant_Hr_Master_DcStationMaster = "Pages.Tenant.Hr.Master.DcStationMaster";
        public const string Pages_Tenant_Hr_Master_DcStationMaster_Create = "Pages.Tenant.Hr.Master.DcStationMaster.Create";
        public const string Pages_Tenant_Hr_Master_DcStationMaster_Delete = "Pages.Tenant.Hr.Master.DcStationMaster.Delete";
        public const string Pages_Tenant_Hr_Master_DcStationMaster_Edit = "Pages.Tenant.Hr.Master.DcStationMaster.Edit";

        public const string Pages_Tenant_Hr_Master_DcShiftMaster = "Pages.Tenant.Hr.Master.DcShiftMaster";
        public const string Pages_Tenant_Hr_Master_DcShiftMaster_Create = "Pages.Tenant.Hr.Master.DcShiftMaster.Create";
        public const string Pages_Tenant_Hr_Master_DcShiftMaster_Delete = "Pages.Tenant.Hr.Master.DcShiftMaster.Delete";
        public const string Pages_Tenant_Hr_Master_DcShiftMaster_Edit = "Pages.Tenant.Hr.Master.DcShiftMaster.Edit";

        public const string Pages_Tenant_Hr_Transaction_DutyChart = "Pages.Tenant.Hr.Transaction.DutyChart";
        public const string Pages_Tenant_Hr_Transaction_DutyChart_Create = "Pages.Tenant.Hr.Transaction.DutyChart.Create";
        public const string Pages_Tenant_Hr_Transaction_DutyChart_Delete = "Pages.Tenant.Hr.Transaction.DutyChart.Delete";
        public const string Pages_Tenant_Hr_Transaction_DutyChart_Edit = "Pages.Tenant.Hr.Transaction.DutyChart.Edit";

        //Job Title Master

        public const string Pages_Tenant_Hr_Master_JobTitleMaster = "Pages.Tenant.Hr.Master.JobTitleMaster";
        public const string Pages_Tenant_Hr_Master_JobTitleMaster_Create = "Pages.Tenant.Hr.Master.JobTitleMaster.Create";
        public const string Pages_Tenant_Hr_Master_JobTitleMaster_Delete = "Pages.Tenant.Hr.Master.JobTitleMaster.Delete";
        public const string Pages_Tenant_Hr_Master_JobTitleMaster_Edit = "Pages.Tenant.Hr.Master.JobTitleMaster.Edit";

        public const string Pages_Tenant_Tick = "Pages.Tenant.Tick";
        public const string Pages_Tenant_Tick_Dashboard = "Pages.Tenant.Tick.Dashboard";

        public const string Pages_Tenant_Tick_Employee_Department = "Pages.Tenant.Tick.Department";
        public const string Pages_Tenant_Tick_Employee_Department_Create = "Pages.Tenant.Tick.Department.Create";
        public const string Pages_Tenant_Tick_Employee_Department_Delete = "Pages.Tenant.Tick.Department.Delete";
        public const string Pages_Tenant_Tick_Employee_Department_Edit = "Pages.Tenant.Tick.Department.Edit";

        #endregion Tick

        #region Go

        public const string Pages_Tenant_Go = "Pages.Tenant.Go";

        public const string Pages_Tenant_Go_DineGoBrand = "Pages.Tenant.Go.DineGoBrand";
        public const string Pages_Tenant_Go_DineGoBrand_Create = "Pages.Tenant.Go.DineGoBrand.Create";
        public const string Pages_Tenant_Go_DineGoBrand_Delete = "Pages.Tenant.Go.DineGoBrand.Delete";
        public const string Pages_Tenant_Go_DineGoBrand_Edit = "Pages.Tenant.Go.DineGoBrand.Edit";

        public const string Pages_Tenant_Go_DineGoDevice = "Pages.Tenant.Go.DineGoDevice";
        public const string Pages_Tenant_Go_DineGoDevice_Create = "Pages.Tenant.Go.DineGoDevice.Create";
        public const string Pages_Tenant_Go_DineGoDevice_Delete = "Pages.Tenant.Go.DineGoDevice.Delete";
        public const string Pages_Tenant_Go_DineGoDevice_Edit = "Pages.Tenant.Go.DineGoDevice.Edit";

        public const string Pages_Tenant_Go_DineGoDepartment = "Pages.Tenant.Go.DineGoDepartment";
        public const string Pages_Tenant_Go_DineGoDepartment_Create = "Pages.Tenant.Go.DineGoDepartment.Create";
        public const string Pages_Tenant_Go_DineGoDepartment_Delete = "Pages.Tenant.Go.DineGoDepartment.Delete";
        public const string Pages_Tenant_Go_DineGoDepartment_Edit = "Pages.Tenant.Go.DineGoDepartment.Edit";

        public const string Pages_Tenant_Go_DineGoPaymentType = "Pages.Tenant.Go.DineGoPaymentType";
        public const string Pages_Tenant_Go_DineGoPaymentType_Create = "Pages.Tenant.Go.DineGoPaymentType.Create";
        public const string Pages_Tenant_Go_DineGoPaymentType_Delete = "Pages.Tenant.Go.DineGoPaymentType.Delete";
        public const string Pages_Tenant_Go_DineGoPaymentType_Edit = "Pages.Tenant.Go.DineGoPaymentType.Edit";

        public const string Pages_Tenant_Go_DineGoCharge = "Pages.Tenant.Go.DineGoCharge";
        public const string Pages_Tenant_Go_DineGoCharge_Create = "Pages.Tenant.Go.DineGoCharPages_Tenant_Play_DisplayGroupge.Create";
        public const string Pages_Tenant_Go_DineGoCharge_Delete = "Pages.Tenant.Go.DineGoCharge.Delete";
        public const string Pages_Tenant_Go_DineGoCharge_Edit = "Pages.Tenant.Go.DineGoCharge.Edit";

        #endregion Go

        #region Play

        public const string Pages_Tenant_Play = "Pages.Tenant.Play";
        public const string Pages_Tenant_Play_Resolution = "Pages.Tenant.Play.Resolution";
        public const string Pages_Tenant_Play_Resolution_Create = "Pages.Tenant.Play.Resolution.Create";
        public const string Pages_Tenant_Play_Resolution_Edit = "Pages.Tenant.Play.Resolution.Edit";
        public const string Pages_Tenant_Play_Resolution_Delete = "Pages.Tenant.Play.Resolution.Delete";

        public const string Pages_Tenant_Play_DisplayGroup = "Pages.Tenant.Play.DisplayGroup";
        public const string Pages_Tenant_Play_DisplayGroup_Create = "Pages.Tenant.Play.DisplayGroup.Create";
        public const string Pages_Tenant_Play_DisplayGroup_Edit = "Pages.Tenant.Play.DisplayGroup.Edit";
        public const string Pages_Tenant_Play_DisplayGroup_Delete = "Pages.Tenant.Play.DisplayGroup.Delete";

        public const string Pages_Tenant_Play_Display = "Pages.Tenant.Play.Display";
        public const string Pages_Tenant_Play_Display_Create = "Pages.Tenant.Play.Display.Create";
        public const string Pages_Tenant_Play_Display_Edit = "Pages.Tenant.Play.Display.Edit";
        public const string Pages_Tenant_Play_Display_Delete = "Pages.Tenant.Play.Display.Delete";

        public const string Pages_Tenant_Play_DayParting = "Pages.Tenant.Play.DayParting";
        public const string Pages_Tenant_Play_DayParting_Create = "Pages.Tenant.Play.DayParting.Create";
        public const string Pages_Tenant_Play_DayParting_Edit = "Pages.Tenant.Play.DayParting.Edit";
        public const string Pages_Tenant_Play_DayParting_Delete = "Pages.Tenant.Play.DayParting.Delete";

        public const string Pages_Tenant_Play_Layout = "Pages.Tenant.Play.Layout";
        public const string Pages_Tenant_Play_Layout_Create = "Pages.Tenant.Play.Layout.Create";
        public const string Pages_Tenant_Play_Layout_Edit = "Pages.Tenant.Play.Layout.Edit";
        public const string Pages_Tenant_Play_Layout_Delete = "Pages.Tenant.Play.Layout.Delete";

        public const string Pages_Tenant_Play_Schedule = "Pages.Tenant.Play.Schedule";
        public const string Pages_Tenant_Play_Schedule_Event_Create = "Pages.Tenant.Play.Schedule.Event.Create";
        public const string Pages_Tenant_Play_Schedule_Event_Edit = "Pages.Tenant.Play.Schedule.Event.Edit";
        public const string Pages_Tenant_Play_Schedule_Event_Delete = "Pages.Tenant.Play.Schedule.Event.Delete";

        #endregion Play
    }
}