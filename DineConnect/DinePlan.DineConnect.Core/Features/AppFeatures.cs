﻿namespace DinePlan.DineConnect.Features
{
	public static class AppFeatures
	{
		public const string Connect = "DinePlan.DineConnect.Connect";
		public const string ConnectLocationCount = "DinePlan.DineConnect.Connect.LocationCount";
		public const string ConnectCountry = "DinePlan.DineConnect.Connect.Country";
		public const string ConnectCustomer = "DinePlan.DineConnect.Connect.Customer";
		public const string ConnectCurrency = "DinePlan.DineConnect.Connect.Currency";
        public const string ConnectCurrencyCode = "DinePlan.DineConnect.Connect.CurrencyCode";
		public const string ConnectFranchise = "DinePlan.DineConnect.Connect.Franchise";
		public const string Expiry = "DinePlan.DineConnect.Expiry";
		public const string Custom = "DinePlan.DineConnect.Feature.Custom";
		public const string UrbanPiper = "DinePlan.DineConnect.Connect.UrbanPiper";
		public const string DeliveryHero = "DinePlan.DineConnect.Connect.DeliveryHero";

		
		public const string House = "DinePlan.DineConnect.House";
		public const string Cater = "DinePlan.DineConnect.Cater";
		public const string Tick = "DinePlan.DineConnect.Tick";
		public const string Engage = "DinePlan.DineConnect.Engage";
		public const string Addons = "DinePlan.DineConnect.Addons";
		public const string Wheel = "DinePlan.DineConnect.Wheel";
		public const string Touch = "DinePlan.DineConnect.Touch";
		public const string Swipe = "DinePlan.DineConnect.Swipe";

		public const string Go = "DinePlan.DineConnect.Go";
		public const string GoDeviceCount = "DinePlan.DineConnect.Go.GoDeviceCount";

		public const string Play = "DinePlan.DineConnect.Play";
		public const string PlayDeviceCount = "DinePlan.DineConnect.Play.PlayDeviceCount";

        public const string Cluster = "DinePlan.DineConnect.Cluster";



	}
}