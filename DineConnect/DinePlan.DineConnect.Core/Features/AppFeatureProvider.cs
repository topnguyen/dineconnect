﻿using Abp.Application.Features;
using Abp.Localization;
using Abp.Runtime.Validation;
using Abp.UI.Inputs;

namespace DinePlan.DineConnect.Features
{
    /* This feature provider is just for an example.
     * You can freely delete all features and add your own.
     */

    public class AppFeatureProvider : FeatureProvider
    {
        public override void SetFeatures(IFeatureDefinitionContext context)
        {
            var connectFeature = context.Create(
                AppFeatures.Connect, "true", L("Connect"),
                inputType: new CheckboxInputType()
                );

            connectFeature.CreateChildFeature(
                AppFeatures.Expiry, "2025-12-12", L("Expiry"),
                inputType: new SingleLineStringInputType()
                );
            connectFeature.CreateChildFeature(
                AppFeatures.ConnectLocationCount, "6", L("LocationCount"),
                inputType: new SingleLineStringInputType(new NumericValueValidator(1, 1000000))
                );

            connectFeature.CreateChildFeature(
                AppFeatures.ConnectCountry, "SG", L("Country"),
                inputType: new ComboboxInputType(
                    new StaticLocalizableComboboxItemSource(
                        new LocalizableComboboxItem("SG", L("Singapore")),
                        new LocalizableComboboxItem("IN", L("India")),
                        new LocalizableComboboxItem("MR", L("Myanmar")),
                        new LocalizableComboboxItem("UAE", L("UAE")),
                        new LocalizableComboboxItem("IA", L("Indonesia")),
                        new LocalizableComboboxItem("MY", L("Malaysia")),
                        new LocalizableComboboxItem("KSA", L("KSA")),
                        new LocalizableComboboxItem("TH", L("Thailand")),
                        new LocalizableComboboxItem("PH", L("Philpines"))
                        )
                    )
                );

            connectFeature.CreateChildFeature(
                AppFeatures.ConnectCustomer, "", L("Customer"),
                inputType: new ComboboxInputType(
                    new StaticLocalizableComboboxItemSource(
                        new LocalizableComboboxItem("1FM", L("FoodMaster Singapore")),
                        new LocalizableComboboxItem("2EATC", L("EatingCircles India")),
                        new LocalizableComboboxItem("3PD", L("Paradise Myanmar")),
                        new LocalizableComboboxItem("4RR", L("RR India")),
                        new LocalizableComboboxItem("5WC", L("WRITERS CAFE")),
                        new LocalizableComboboxItem("6PTT", L("PTT")),
                        new LocalizableComboboxItem("7DCT", L("DCT")),
                        new LocalizableComboboxItem("8PBSG", L("Parris Baggutte-SG"))



                        )
                    )
                );

            connectFeature.CreateChildFeature(
                AppFeatures.ConnectCurrency, "S$", L("Currency"),
                inputType: new SingleLineStringInputType(new StringValueValidator(1, 3))
                );
            connectFeature.CreateChildFeature(
                AppFeatures.ConnectCurrencyCode, "SGD", L("Symbol"),
                inputType: new SingleLineStringInputType(new StringValueValidator(1, 3))
            );
            connectFeature.CreateChildFeature(
                AppFeatures.ConnectFranchise, "false", L("Franchise"),
                inputType: new CheckboxInputType()
                );
            connectFeature.CreateChildFeature(
                AppFeatures.UrbanPiper, "false", L("UrbanPiper"),
                inputType: new CheckboxInputType()
            );
            connectFeature.CreateChildFeature(
                AppFeatures.DeliveryHero, "false", L("DeliveryHero"),
                inputType: new CheckboxInputType()
            );

            var hosueFeature = context.Create(
                AppFeatures.House, "false", L("House"),
                inputType: new CheckboxInputType()
                );

            var caterFeature = context.Create(
                AppFeatures.Cater, "false", L("Cater"),
                inputType: new CheckboxInputType()
            );
            var touchFeature = context.Create(
                AppFeatures.Touch, "false", L("Touch"),
                inputType: new CheckboxInputType()
                );

            var tickFeature = context.Create(
                AppFeatures.Tick, "false", L("Tick"),
                inputType: new CheckboxInputType()
                );

            var engageFeature = context.Create(
                AppFeatures.Engage, "false", L("Engage"),
                inputType: new CheckboxInputType()
                );

            var addonsFeature = context.Create(
                AppFeatures.Addons, "false", L("Addons"),
                inputType: new CheckboxInputType()
                );
            var wheelFeature = context.Create(
                AppFeatures.Wheel, "false", L("Wheel"),
                inputType: new CheckboxInputType()
                );

            var swipeFeature = context.Create(
                AppFeatures.Swipe, "false", L("Swipe"),
                inputType: new CheckboxInputType()
                );


            var customFeature = context.Create(
                AppFeatures.Custom, "false", L("Custom"),
                inputType: new CheckboxInputType()
            );

            var goFeature = context.Create(
                AppFeatures.Go, "false", L("Go"),
                inputType: new CheckboxInputType());

            goFeature.CreateChildFeature(
                    AppFeatures.GoDeviceCount, "6", L("DeviceCount"),
                    inputType: new SingleLineStringInputType(new NumericValueValidator(1, 1000000))
            );

            var playFeature = context.Create(
                AppFeatures.Play, "false", L("Play"),
                inputType: new CheckboxInputType());

            playFeature.CreateChildFeature(
                AppFeatures.PlayDeviceCount, "6", L("DeviceCount"),
                inputType: new SingleLineStringInputType(new NumericValueValidator(1, 1000000))
            );

            var clusterFeature = context.Create(
                AppFeatures.Cluster, "false", L("Cluster"),
                inputType: new CheckboxInputType());

           
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, DineConnectConsts.LocalizationSourceName);
        }
    }
}