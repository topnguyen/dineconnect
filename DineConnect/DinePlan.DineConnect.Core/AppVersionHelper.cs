﻿using System;
using System.IO;

namespace DinePlan.DineConnect
{
    /// <summary>
    /// Central point for application version.
    /// </summary>
    public class AppVersionHelper
    {
        /// <summary>
        /// Gets current version of the application.
        /// All project's assembly versions are changed when this value is changed.
        /// It's also shown in the web page.
        /// </summary>

        private const string DineConnectVersion = "2";
        private const string DineConnectDbVersion = "486";
        private const string DineConnectMinorVersion = "1";

        public const string Version = DineConnectVersion+"."+DineConnectDbVersion+"."+DineConnectMinorVersion;

        /// <summary>
        /// Gets release (last build) date of the application.
        /// It's shown in the web page.
        /// </summary>
        public static DateTime ReleaseDate => new FileInfo(typeof(AppVersionHelper).Assembly.Location).LastWriteTime;
    }
}