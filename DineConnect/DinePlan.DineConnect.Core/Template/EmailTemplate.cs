﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace DinePlan.DineConnect.Template
{
    public class EmailTemplate : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public string Name { get; set; }
        public string Template { get; set; }
        public string Variables { get; set; }
    }
}