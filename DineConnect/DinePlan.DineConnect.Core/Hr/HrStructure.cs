﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Users;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.MultiTenancy;

namespace DinePlan.DineConnect.Hr
{

    [Table("SkillSets")]
    public class SkillSet : FullAuditedEntity, IMustHaveTenant
    {
        [Index("SkillCode", IsUnique = true)]
        [Required, MaxLength(20)]
        public virtual string SkillCode { get; set; }

        [Required, MaxLength(40)]
        public virtual string Description { get; set; }

        public bool IsActive { get; set; }

        public int TenantId { get; set; }

        public SkillSet() 
        { 
        
        }

        // For Seed Purpose
        public SkillSet(int tenantId, string skillcode, string description)
        {
            TenantId = tenantId;
            SkillCode = skillcode;
            Description = description;
        }

    }

    [Table("JobTitleMasters")]
    public class JobTitleMaster : FullAuditedEntity, IMustHaveTenant
    {
        [MaxLength(50)]
        [Index("JobTitle_TenantId", IsUnique = true, Order =1)]
        public string JobTitle { get; set; }

        [Index("JobTitle_TenantId", IsUnique = true, Order = 2)]
        public int TenantId { get; set; }

        public JobTitleMaster()
        {

        }

        public JobTitleMaster(string jobTitle)
        {
            JobTitle = jobTitle;
        }
    }

    [Table("EmployeeSalaryClassifications")]
    public class EmployeeSalaryClassification : FullAuditedEntity, IMustHaveTenant
    {
        public string SalaryClassification { get; set; }
        public int TenantId { get; set; }
        [ForeignKey("TenantId")]
        public Tenant Tenants { get; set; }

        public EmployeeSalaryClassification()
        {

        }

        public EmployeeSalaryClassification(int tenantId, string argSalaryClassification)
        {
            TenantId = tenantId;
            SalaryClassification = argSalaryClassification;
        }
    }


    [Table("EmployeeSectionMasters")]
    public class EmployeeSectionMaster : FullAuditedEntity, IMustHaveTenant
    {
        public string SectionName { get; set; }
        public int TenantId { get; set; }
        [ForeignKey("TenantId")]
        public Tenant Tenants { get; set; }

        public EmployeeSectionMaster()
        {

        }

        public EmployeeSectionMaster(int tenantId, string argSectionName)
        {
            TenantId = tenantId;
            SectionName = argSectionName;
        }
    }



    [Table("PersonalInformations")]
    public class PersonalInformation : FullAuditedEntity, IMustHaveTenant
    {
        public int LocationRefId { get; set; }
        [ForeignKey("LocationRefId")]
        public Location Location { get; set; }

        [Required, MaxLength(20)]
        [Index("BioMetricCode", IsUnique = true)]
        public string BioMetricCode { get; set; }

        [Required]
        public decimal UserSerialNumber { get; set; }


        [Required]
        [MaxLength(50)]
        public string EmployeeGivenName { get; set; }

        [Required]
        [MaxLength(50)]
        public string EmployeeSurName { get; set; }

        [Required]
        [MaxLength(50)]
        public string EmployeeName { get; set; }

        [Required, MaxLength(20)]
        [Index("EmployeeCode", IsUnique = true)]
        public string EmployeeCode { get; set; }


        [MaxLength(50)]
        public string PerAddress1 { get; set; }
        [MaxLength(50)]
        public string PerAddress2 { get; set; }
        [MaxLength(50)]
        public string PerAddress3 { get; set; }
        [MaxLength(50)]
        public string PerAddress4 { get; set; }
        [MaxLength(50)]
        public string PostalCode { get; set; }

        [MaxLength(50)]
        public string ComAddress1 { get; set; }
        [MaxLength(50)]
        public string ComAddress2 { get; set; }
        [MaxLength(50)]
        public string ComAddress3 { get; set; }
        [MaxLength(50)]
        public string ComAddress4 { get; set; }
        [MaxLength(50)]
        public string ZipCode { get; set; }

        [Required]
        [MaxLength(50)]
        public string StaffType { get; set; }   // Permanent/Freelancer

        [MaxLength(50)]
        public string NationalIdentificationNumber { get; set; }

        public int EmployeeResidentStatusRefId { get; set; }
        [ForeignKey("EmployeeResidentStatusRefId")]
        public EmployeeResidentStatus EmployeeResidentStatuses { get; set; }

        public int RaceRefId { get; set; }
        [ForeignKey("RaceRefId")]
        public EmployeeRace EmployeeRaces { get; set; }

        public int EmployeeCostCentreRefId { get; set; }
        [ForeignKey("EmployeeCostCentreRefId")]
        public EmployeeCostCentreMaster EmployeeCostCentreMasters { get; set; }

        public int ReligionRefId { get; set; }
        [ForeignKey("ReligionRefId")]
        public EmployeeReligion EmployeeReligion { get; set; }

        public DateTime DateOfBirth { get; set; }
        [MaxLength(50)]
        public string Gender { get; set; }

        [MaxLength(50)]
        public string MaritalStatus { get; set; }
        public DateTime? MaritalDate { get; set; }
        [MaxLength(50)]
        public string PassPortNumber { get; set; }

        public DateTime DateHired { get; set; }

        public int? JobTitleRefId { get; set; }
        [ForeignKey("JobTitleRefId")]
        public JobTitleMaster JobTitleMasters { get; set; }


        public DateTime? LastWorkingDate { get; set; }

        public bool ActiveStatus { get; set; }
        public virtual Guid? ProfilePictureId { get; set; }

        public int DefaultSkillRefId { get; set; }

        public int DefaultWeekOff { get; set; }
        public bool IsAttendanceRequired { get; set; }

        [MaxLength(50)]
        public string HandPhone1 { get; set; }
        [MaxLength(50)]
        public string HandPhone2 { get; set; }
        [MaxLength(50)]
        public string Email { get; set; }
        [MaxLength(50)]
        public string PersonalEmail { get; set; }

        public bool IncentiveAllowedFlag { get; set; }

        public int? SupervisorEmployeeRefId { get; set; }
        public int? InchargeEmployeeRefId { get; set; }

        public bool IsFixedCostCentreInDutyChart { get; set; }
        public DateTime? DutyFixedUptoDate { get; set; }
        public bool IsSmartAttendanceFingerAllowed { get; set; }

        public bool IsDayCloseStatusMailRequired { get; set; }

        public bool isBillable { get; set; }

        public bool IsBillableAttendanceRequired { get; set; }
        


        public PersonalInformation()
        {

        }

        public PersonalInformation(int argLocationRefId, int argBiometricCode,string argEmployeeCode, string argEmployeeName, string argEmployeeGivenName, string argEmployeeSurName, int argUserSerialNumber)
        {
            EmployeeCode = argEmployeeCode;
            EmployeeName = argEmployeeName;
            EmployeeGivenName = argEmployeeGivenName;
            EmployeeSurName = argEmployeeSurName;
            UserSerialNumber = argUserSerialNumber;
        }

        [MaxLength(50)]
        public string WPClassNumber { get; set; }
        public DateTime? WorkPassStartDate { get; set; }
        public DateTime? WorkPassEndDate { get; set; }
        [MaxLength(50)]
        public string PRNumber { get; set; }
        public DateTime? PRStartDate { get; set; }
        public DateTime? PREndDate { get; set; }

        [ForeignKey("RawId")]
        public virtual EmployeeRawInfo EmployeeRawInfo { get; set; }
        public int? RawId { get; set; }

        [ForeignKey("DinePlanUserId")]
        public virtual DinePlanUser DinePlanUser { get; set; }
        public int? DinePlanUserId { get; set; }

        public int TenantId { get; set; }
       
    }

    public class EmployeeRawInfo : CreationAuditedEntity
    {

        public string Identity { get; set; }

        public byte[] IdenitityData { get; set; }
    }

    [Table("EmployeeSkillSets")]
    public class EmployeeSkillSet : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        [Index("MPK_EmpRefId_SkillRefId", IsUnique = true, Order = 0)]
        public int EmployeeRefId { get; set; }
        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformation { get; set; }

        [Required]
        [Index("MPK_EmpRefId_SkillRefId", IsUnique = true, Order = 1)]
        public int SkillSetRefId { get; set; }
        [ForeignKey("SkillSetRefId")]
        public SkillSet SkillSet { get; set; }

        public int TenantId { get; set; }
       
    }


    [Table("DocumentInfos")]
    public class DocumentInfo : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        [MaxLength(50)]
        public string DocumentName { get; set; }
        public DocumentType DocumentType { get; set; }
        public bool LifeTimeFlag { get; set; }
        public int DefaultAlertDays { get; set; }
        public int DefaultExpiryDays { get; set; }

        public int TenantId { get; set; }
       

    }

    public enum DocumentType
    {
        AllType = 0,
        Employee = 1,
        Company = 2
    }

    [Table("EmployeeDocumentInfos")]
    public class EmployeeDocumentInfo : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        public int EmployeeRefId { get; set; }
        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformation { get; set; }

        [Required]
        public int DocumentInfoRefId { get; set; }
        [ForeignKey("DocumentInfoRefId")]
        public DocumentInfo DocumentInfo { get; set; }

        [MaxLength(50)]
        public string DocReferenceNumber { get; set; }

        public int DefaultAlertDays { get; set; }

        public bool LifeTimeFlag { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        [Required]
        [MaxLength(50)]
        public string FileExtenstionType { get; set; }

        [MaxLength(50)]
        public string FileName { get; set; }

        public DateTime? AlertFrom => EndDate.HasValue == true ? EndDate.Value.AddDays(-1 * DefaultAlertDays) : DateTime.MinValue;


        public int TenantId { get; set; }
       

    }

    [Table("DocumentForSkillSets")]
    public class DocumentForSkillSet : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        [Index("MPK_DocumentRefId_SkillRefId", IsUnique = true, Order = 0)]
        public int DocumentRefId { get; set; }
        [ForeignKey("DocumentRefId")]
        public DocumentInfo DocumentInfo { get; set; }

        [Required]
        [Index("MPK_DocumentRefId_SkillRefId", IsUnique = true, Order = 1)]
        public int SkillSetRefId { get; set; }
        [ForeignKey("SkillSetRefId")]
        public SkillSet SkillSet { get; set; }

        public int TenantId { get; set; }
      

    }

    [Table("AttendanceMachineLocations")]
    public class AttendanceMachineLocation : FullAuditedEntity, IMustHaveTenant
    {
        public int LocationRefId { get; set; }
        [ForeignKey("LocationRefId")]
        public Location Locations { get; set; }

        [MaxLength(50)]
        public string MachineLocation { get; set; }

        [StringLength(4)]
        [Index("MachinePrefix", IsUnique = true)]
        [MaxLength(4)]
        public string MachinePrefix { get; set; }

        public bool IsActive { get; set; }

        public virtual int TenantId { get; set; }
    }


    [Table("AttendanceLogs")]
    public class AttendanceLog : FullAuditedEntity, IMustHaveTenant
    {
        public int LocationRefId { get; set; }
        [ForeignKey("LocationRefId")]
        public virtual Location Locations { get; set; }

        public int? AttendanceMachineLocationRefId { get; set; }
        [ForeignKey("AttendanceMachineLocationRefId")]
        public AttendanceMachineLocation AttendanceMachineLocation { get; set; }

        public int EmployeeRefId { get; set; }
        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformation { get; set; }

        public DateTime? CheckTime { get; set; }

        [Column(TypeName = "date")] 
        public DateTime CheckTimeTrunc { get; set; }

        
        public int CheckType { get; set; }

        [MaxLength(50)]
        public string CheckTypeCode { get; set; }

        public int VerifyMode { get; set; }

        public int? AuthorisedBy { get; set; }

        [ForeignKey("AuthorisedBy")]
        public PersonalInformation PersonalInformations { get; set; }

        public virtual int TenantId { get; set; }

        public bool Deleted { get; set; }

    }


    [Table("GpsAttendances")]
    public class GpsAttendance : FullAuditedEntity //, IMustHaveTenant
    {
        public int EmployeeRefId { get; set; }
        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformation { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
        [MaxLength(50)]
        public string Address { get; set; }
        public byte[] Photo { get; set; }
        public int AttendanceStatus { get; set; }
        public int LoginEmployeeRefId { get; set; }
        public bool IsSmartAttendanceFingerReceived { get; set; }
    }

    [Table("GpsLive")]
    public class GpsLive : CreationAuditedEntity //, IMustHaveTenant
    {
        public int EmployeeRefId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
        [MaxLength(50)]
        public string Address { get; set; }

    }


    [Table("EmployeeMailMessages")]
    public class EmployeeMailMessage : FullAuditedEntity, IMustHaveTenant
    {
        public int EmployeeRefId { get; set; }
        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformation { get; set; }

        public DateTime MailTime { get; set; }

        public DateTime? DutyChartDate { get; set; }

        [MaxLength(50)]
        public string DutyStatusMailed { get; set; }

        public int MessageType { get; set; }
        [MaxLength(50)]
        public string ReceipientsMailIdsList { get; set; }
        [MaxLength(200)]
        public string MessageText { get; set; }
        public long MessageApprovedBy { get; set; }
        public int TenantId { get; set; }
       

    }


    [Table("CommonMailMessages")]
    public class CommonMailMessage : FullAuditedEntity, IMustHaveTenant
    {
        public DateTime MailTime { get; set; }
        public int MessageType { get; set; }
        [MaxLength(50)]
        public string ReceipientsMailIdsList { get; set; }
        [MaxLength(200)]
        public string MessageText { get; set; }
        public long MessageApprovedBy { get; set; }
        public int TenantId { get; set; }
       

    }

    [Table("UserDefaultInformations")]
    public class UserDefaultInformation : FullAuditedEntity, IMustHaveTenant
    {

        [Required]
        public virtual long UserId { get; set; }
        [ForeignKey("UserId")]
        public User Users { get; set; }


        public virtual int? EmployeeRefId { get; set; }
        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformation { get; set; }

        public virtual int TenantId { get; set; }
    }

    [Table("LeaveTypes")]
    public class LeaveType : FullAuditedEntity
    {
        [Required]
        [MaxLength(50)]
        public string LeaveTypeName { get; set; }
        [Required]
        [MaxLength(50)]
        public string LeaveTypeShortName { get; set; }
        [Required]
        public Decimal NumberOfLeaveAllowed { get; set; }
        public decimal DefaultNumberOfLeaveAllowed { get; set; }
        [Required]
        public DateTime CalculationPeriodStarts { get; set; }

        [Required]
        public DateTime CalculationPeriodEnds { get; set; }

        public bool MaleGenderAllowed { get; set; }
        public bool FemaleGenderAllowed { get; set; }
        public bool AllowedForMarriedEmployeesOnly { get; set; }

        public bool IsSupportingDocumentsRequired { get; set; }

        public bool EligibleOnlyAnnualLeaveExhausted { get; set; }

        public LeaveType() { }

        // For Seed Purpose
        public LeaveType(int tenantId, string leaveTypeName, string leaveTypeShortName, Decimal numberOfLeaveAllowed,
            DateTime calculationPeriodStarts, DateTime calculationPeriodEnds)
        {
            TenantId = tenantId;
            LeaveTypeName = leaveTypeName;
            LeaveTypeShortName = leaveTypeShortName;
            NumberOfLeaveAllowed = numberOfLeaveAllowed;
            CalculationPeriodStarts = calculationPeriodStarts;
            CalculationPeriodEnds = calculationPeriodStarts;
            MaleGenderAllowed = true;
            FemaleGenderAllowed = true;
        }
        public virtual int TenantId { get; set; }
    }

    [Table("LeaveExhaustedLists")]
    public class LeaveExhaustedList : FullAuditedEntity, IMustHaveTenant
    {
        public int LeaveTypeRefId { get; set; }
        [ForeignKey("LeaveTypeRefId")]
        public LeaveType LeaveType { get; set; }

        public int LeaveExhaustedRefId { get; set; }
        [ForeignKey("LeaveExhaustedRefId")]
        public LeaveType LeaveTypeExhausted { get; set; }

        public virtual int TenantId { get; set; }
      
    }

    [Table("YearWiseLeaveAllowedForEmployees")]
    public class YearWiseLeaveAllowedForEmployee : FullAuditedEntity, IMustHaveTenant
    {
        public int AcYear { get; set; }

        [Required]
        public DateTime LeaveAcYearFrom { get; set; }

        [Required]
        public DateTime LeaveAcYearTo { get; set; }


        [Required]
        public int EmployeeRefId { get; set; }
        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformation { get; set; }

        [Required]
        public int LeaveTypeRefCode { get; set; }
        [ForeignKey("LeaveTypeRefCode")]
        public LeaveType LeaveType { get; set; }

        public Decimal NumberOfLeaveAllowed { get; set; }

        public virtual int TenantId { get; set; }

    }


    [Table("LeaveRequests")]
    public class LeaveRequest : FullAuditedEntity
    {
        [Required]
        public int EmployeeRefId { get; set; }
        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformation { get; set; }

        [Required]
        public int LeaveTypeRefCode { get; set; }
        [ForeignKey("LeaveTypeRefCode")]
        public LeaveType LeaveType { get; set; }

        [Required]
        public DateTime DateOfApply { get; set; }
        [Required]
        public DateTime LeaveFrom { get; set; }
        [Required]
        public DateTime LeaveTo { get; set; }
        [Required]
        public Decimal TotalNumberOfDays { get; set; }
        [Required]
        [MaxLength(50)]
        public string LeaveReason { get; set; }

        [MaxLength(50)]
        public string LeaveStatus { get; set; }     //  Approved , Rejected, Pending , DocumentPending

        [MaxLength(50)]
        public string HandOverWorkTo { get; set; } // Stores Employee Ref Id

        [MaxLength(50)]
        public string ContactOfWorkIncharge { get; set; }
        public bool HalfDayFlag { get; set; }
        [MaxLength(50)]
        public string HalfDayTimeSpecification { get; set; }

        public int? ApprovedPersonRefId { get; set; }   //  Used for Rejected PersonRefId from 26 Jan 2019

        public DateTime? ApprovedTime { get; set; }     //  Used for Rejected Time from 26 Jan 2019
        [MaxLength(50)]
        public string ApprovedRemarks { get; set; }     //  Used for Rejected Remarks from 26 Jan 2019

        [MaxLength(50)]
        public string FileName { get; set; }
        [MaxLength(50)]
        public string FileExtenstionType { get; set; }

        public bool IsSupportingDocumentsOverRide { get; set; }
        public int? SupportingDocumentsOverRideUserId { get; set; }

        public int? CommonMailMessageRefId { get; set; }
        [ForeignKey("CommonMailMessageRefId")]
        public CommonMailMessage CommonMailMessages { get; set; }
        public bool DoesSalaryPayableForThisLeaveRequest { get; set; }


        public virtual int TenantId { get; set; }


    }



    [Table("SalaryInfos")]
    public class SalaryInfo : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        public int EmployeeRefId { get; set; }

        [ForeignKey("EmployeeRefId")]

        public PersonalInformation PersonalInformation { get; set; }

        [Required]
        public int SalaryMode { get; set; }
        public DateTime? WithEffectFrom { get; set; }
        public decimal BasicSalary { get; set; }
        public decimal Levy { get; set; }
        public decimal Ot1PerHour { get; set; }
        public decimal Ot2PerHour { get; set; }
        public bool IsOtSlabBasedApplicable { get; set; }
        public bool OtAllowedIfWorkTimeOver { get; set; }
        public DateTime? MaximumOTAllowedEffectiveFrom { get; set; }
        public decimal ReportOtPerHour { get; set; }
        public decimal NormalOTPerHour { get; set; }
        public int IncentiveAndOTCalculationPeriodRefId { get; set; }   //  
        public decimal MaximumAllowedOTHoursOnPublicHoliday { get; set; }
        public bool IsIdleStandByCostExists { get; set; }
        public decimal IdleStandByCost { get; set; }
        public int? HourlySalaryModeRefId { get; set; }
        public int TenantId { get; set; }
       
    }


    public enum HourlySalaryMode
    {
        Claimed_Hours = 1,
        Attendance_Hours = 2,
        WorkDay_Hours = 3
    }

    [Table("SalaryVsStatutories")]
    public class SalaryVsStatutory : FullAuditedEntity, IMustHaveTenant
    {
        public int EmployeeRefId { get; set; }
        public int StatutoryRefId { get; set; }
        public int TenantId { get; set; }
      
    }


    [Table("SalaryPaidMasters")]
    public class SalaryPaidMaster : FullAuditedEntity, IMustHaveTenant
    {
        public int EmployeeRefId { get; set; }
        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformation { get; set; }
        [MaxLength(50)]
        public string JobTitle { get; set; }
        public bool IsBillable { get; set; }
        public int CompanyRefId { get; set; }
        [ForeignKey("CompanyRefId")]
        public Company Company { get; set; }
        public DateTime SalaryAsOnDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int SalaryMode { get; set; }
        public DateTime? WithEffectFrom { get; set; }
        public decimal BasicSalary { get; set; }
        public decimal Ot1PerHour { get; set; }
        public decimal Ot2PerHour { get; set; }
        public bool OtAllowedIfWorkTimeOver { get; set; }
        public decimal ReportOtPerHour { get; set; }
        public int IncentiveAndOTCalculationPeriodRefId { get; set; }   //  
        public decimal MaximumOTAllowedHoursOnPublicHoliday { get; set; }
        public decimal NoOfStandardWorkingDays { get; set; }
        public decimal NoOfWorkedDays { get; set; }
        public decimal NoOfPaidLeaveDays { get; set; }
        public decimal NoOfAbsentDays { get; set; }
        public decimal NoOfIdleDays { get; set; }
        public decimal NoOfExcessLevyDays { get; set; }
        public decimal BasicPay { get; set; }
        [MaxLength(50)]
        public string BasicPayRemarks { get; set; }
        public decimal GrossSalaryAmount { get; set; }
        public decimal DeductionAmount { get; set; }
        public decimal NetSalaryAmount { get; set; }
        public decimal IdleSalaryAmount { get; set; }
        [MaxLength(50)]
        public string IdleSalaryRemarks { get; set; }
        public decimal IdleSalarypercentage { get; set; }
        public decimal AbsentLevyAmount { get; set; }
        [MaxLength(50)]
        public string AbsentLevyRemarks { get; set; }
        public decimal ExcessLevyAmount { get; set; }
        [MaxLength(50)]
        public string ExcessLevyRemarks { get; set; }
        public decimal NoOfWorkHours { get; set; }
        public decimal NoOfIdleHours { get; set; }
        public decimal NoOfOT1Hours { get; set; }
        public decimal NoOfOT2Hours { get; set; }

        public decimal StatutoryAllowance { get; set; }
        public decimal StatutoryDeduction { get; set; }

        public int TenantId { get; set; }
     
    }

    [Table("SalaryPaidConsolidatedIncentives")]
    public class SalaryPaidConsolidatedIncentives : FullAuditedEntity, IMustHaveTenant
    {
        public int SalaryPaidMasterRefId { get; set; }
        [ForeignKey("SalaryPaidMasterRefId")]
        public SalaryPaidMaster SalaryPaidMasters { get; set; }

        public int IncentiveTagRefId { get; set; }
        [ForeignKey("IncentiveTagRefId")]
        public IncentiveTag IncentiveTags { get; set; }

        public decimal AmountPerUnit { get; set; }
        public decimal OverAllManualOtHours { get; set; }
        public decimal Count { get; set; }
        public decimal TotalIncentiveAmount { get; set; }
        [MaxLength(50)]
        public string Remarks { get; set; }
        public int TenantId { get; set; }
        
    }

    [Table("SalaryPaidDetails")]
    public class SalaryPaidDetail : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        [Index("MPK_SalaryPaidMasterRefId_SalaryRefId", IsUnique = true, Order = 0)]

        public int SalaryPaidMasterRefId { get; set; }
        [ForeignKey("SalaryPaidMasterRefId")]
        public SalaryPaidMaster SalaryPaidMasters { get; set; }

        public int IncentivePayModeId { get; set; }   //  1   Daily , 2 Monthly

        public int AllowanceOrDeductionRefId { get; set; } //  1 Allowance Payable , 2 Deduction  Receivable, 3 Statutory Allowance , 4 Statutory Deduction

        [Required]
        [Index("MPK_SalaryPaidMasterRefId_SalaryRefId", IsUnique = true, Order = 1)]
        public int SalaryRefId { get; set; }
        [ForeignKey("SalaryRefId")]
        public IncentiveTag SalaryTag { get; set; }
        [MaxLength(50)]
        public string SalaryTagAliasName { get; set; }
        public DateTime EffectiveFrom { get; set; }
        public DateTime? RollBackFrom { get; set; }
        public decimal Amount { get; set; }
        public decimal MinimumAmount { get; set; }
        public decimal MaximumAmount { get; set; }
        [MaxLength(50)]
        public string Remarks { get; set; }
        public bool IsDynamicAmount { get; set; }
        public bool IsFixedAmount { get; set; }
        public int CalculationPayModeRefId { get; set; } // Daily = 1 or Monthly = 2
        public bool IsSytemGeneratedIncentive { get; set; }
        public int? FormulaRefId { get; set; }
        public bool IsItDeductableFromGrossSalary { get; set; }
        public bool IsThisStatutoryAddedWithGrossSalary { get; set; }
        public decimal AmountCalculatedForSalaryProcess { get; set; }
        public decimal IdleAmount { get; set; }
        [MaxLength(50)]
        public string IdleRemarks { get; set; }
        public int TenantId { get; set; }
     
    }


    public enum IncentiveOTCalculationPeriod
    {
        Salary_Month = 1,
        Previous_Month = 2
    }

    [Table("EmployeeVsSalaryTags")]
    public class EmployeeVsSalaryTag : FullAuditedEntity, IMustHaveTenant
    {
        public int EmployeeRefId { get; set; }
        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformation { get; set; }
        public int SalaryRefId { get; set; }
        [ForeignKey("SalaryRefId")]
        public IncentiveTag SalaryTag { get; set; }
        [MaxLength(50)]
        public string SalaryTagAliasName { get; set; }
        [MaxLength(50)]
        public string SalaryTagCalculationFormula { get; set; }
        public DateTime EffectiveFrom { get; set; }
        public DateTime? RollBackFrom { get; set; }
        public decimal Amount { get; set; }
        public decimal MinimumAmount { get; set; }
        public decimal MaximumAmount { get; set; }
        [MaxLength(50)]
        public string Remarks { get; set; }
        [MaxLength(50)]
        public string RemoveRemarks { get; set; }
        public bool IsDynamicAmount { get; set; }
        public bool IsFixedAmount { get; set; }
        public int? SalaryReferenceEntityRefId { get; set; }    //  PUB
        public int CalculationPayModeRefId { get; set; } // Daily = 1 or Monthly = 2
        public int TenantId { get; set; }
        
    }

    public enum SalaryReferenceEntity
    {
        PUB_CHARGES = 1,
        LOAN_REPAYMENT = 2
    }


    [Table("IncentiveCategories")]
    public class IncentiveCategory : FullAuditedEntity, IMustHaveTenant
    {
        [Index("IncentiveCategoryCode", IsUnique = true)]
        [MaxLength(30)]
        public string IncentiveCategoryCode { get; set; }

        [Index("IncentiveCategoryName", IsUnique = true)]
        [MaxLength(50)]
        public string IncentiveCategoryName { get; set; }

        public bool BasedOnSkillSet { get; set; }   //  Either one should be true
        public bool BasedOnQuantity { get; set; }   //  Either One should be true
        public decimal MaximumIncenitivePerQuanityPerTimeSheet { get; set; } // -1 for No measurements

        public int TenantId { get; set; }
       

        public IncentiveCategory()
        {

        }

        public IncentiveCategory(int tenantId, string argIncentiveCategoryCode, string argIncentiveCategoryName)
        {
            TenantId = tenantId;
            IncentiveCategoryCode = argIncentiveCategoryCode;
            IncentiveCategoryName = argIncentiveCategoryName;
        }
    }


    [Table("IncentiveTags")]
    public class IncentiveTag : FullAuditedEntity, IMustHaveTenant
    {
        public int IncentivePayModeId { get; set; }   //  1   Daily , 2 Monthly

        public int AllowanceOrDeductionRefId { get; set; }    //  1 Allowance Payable , 2 Deduction  Receivable

        public int IncentiveCategoryRefId { get; set; }
        [ForeignKey("IncentiveCategoryRefId")]
        public IncentiveCategory IncentiveCategory { get; set; }

        [Index("IncentiveTagCode", IsUnique = true)]
        [MaxLength(30)]
        public string IncentiveTagCode { get; set; }

        [Index("IncentiveTagName", IsUnique = true)]
        [MaxLength(50)]
        public string IncentiveTagName { get; set; }

        [MaxLength(50)]
        public string IncentiveDescription { get; set; }

        public bool HoursBased { get; set; }
        public bool FullWorkHours { get; set; }
        public decimal MinimumHours { get; set; }
        public decimal MinimumOtHours { get; set; }
        public decimal MinimumClaimedHours { get; set; }
        public bool IncludeInConsolidationPerDay { get; set; }
        public decimal IncentiveAmount { get; set; }
        public bool IncenitveBasedOnSalaryTags { get; set; }
        public int? SalaryTagRefId { get; set; }
        public decimal? ProportianteValueOfSalaryTags { get; set; }
        public bool OverTimeBasedIncentive { get; set; }
        public bool DisplayNotApplicable { get; set; }
        public bool HourlyClaimableTimeSheets { get; set; }
        public int IncentiveEntryMode { get; set; }
        public decimal MinRange { get; set; }
        public decimal MaxRange { get; set; }
        public bool CanIssueToIdleStatus { get; set; }
        public bool ActiveStatus { get; set; }
        public bool CanBeAssignedToAllEmployees { get; set; }
        public bool OnlyOnePersonOfTheTeamCanReceived { get; set; }
        public bool TimeSheetRequiredForGivenDate { get; set; }
        public bool IsMaterialQuantityBasedIncentiveFlag { get; set; }
        public bool IsCertificateMandatory { get; set; }
        [MaxLength(50)]
        public string CalculationFormula { get; set; }
        public bool AnySpecialFormulaRefId { get; set; }
        public int? FormulaRefId { get; set; }
        public decimal CalculationFormulaMinimumValue { get; set; }
        public decimal CalculationFormulaMaximumValue { get; set; }
        public bool OnlyAllowedOnWorkDay { get; set; }
        public bool WorkDayHourBasedIncentive { get; set; }
        public bool DoesIssueOnlyForBillableEmployee { get; set; }

        public bool IsSytemGeneratedIncentive { get; set; }
        public bool IsItDeductableFromGrossSalary { get; set; }
        public bool IsFixedAmount { get; set; }
        public bool IsBulkUpdateAllowed { get; set; }
        public bool IsPfPayable { get; set; }
        public bool IsGovernmentInsurancePayable { get; set; }

        public int TenantId { get; set; }
      
        public IncentiveTag()
        {

        }

        public IncentiveTag(int tenantId, int argIncentivePayModeId, int argAllowanceOrDeductionRefId, string argIncentiveCategoryName, int argIncentiveCategoryRefId, string argIncentiveTagCode, string argIncentiveTagName, string argIncentiveDescription, int argIncentiveEntryMode, bool argActiveStatus, bool argAnySpecialFormulaRefId, int? argFormulaRefId, decimal argCalculationFormulaMinimumValue, decimal argCalculationFormulaMaximumValue, bool argIsSytemGeneratedIncentive, bool argIsItDeductedFromGrossSalary)
        {
            TenantId = tenantId;
            IncentivePayModeId = argIncentivePayModeId;
            AllowanceOrDeductionRefId = argAllowanceOrDeductionRefId;
            IncentiveTagCode = argIncentiveTagCode;
            IncentiveTagName = argIncentiveTagName;
            IncentiveDescription = argIncentiveDescription;
            IncentiveEntryMode = argIncentiveEntryMode;
            ActiveStatus = argActiveStatus;
            AnySpecialFormulaRefId = argAnySpecialFormulaRefId;
            FormulaRefId = argFormulaRefId;
            CalculationFormulaMinimumValue = argCalculationFormulaMinimumValue;
            CalculationFormulaMaximumValue = argCalculationFormulaMaximumValue;
            IsSytemGeneratedIncentive = argIsSytemGeneratedIncentive;
            IsItDeductableFromGrossSalary = argIsItDeductedFromGrossSalary;
        }
    }

    public enum SpecialFormulaTagReference
    {
        SG_Employer_CPF = 1,
        SG_Employee_CPF = 2,
        SG_SINDA_DEDUCTION = 3,
        SG_MBMF = 4,
        SG_SDL = 5,
        SG_CDAC = 6,
        SG_Eurasian = 7,
        Levy = 8,

        IN_Employer_PF = 21,
        IN_Employer_PENSION = 22,
        IN_Employee_PF = 23,
        IN_Employer_EPF_ADMIN_CHARGES = 24,
        IN_ESI_Employer = 25,
        IN_ESI_Employee = 26,
        IN_PROFESSIONAL_TAX = 27,
        IN_LABOUR_WELFARE_FUND = 28

    }

    public enum IncentiveEntryMode
    {
        AUTOMATIC,
        MANUAL,
        BOTH
    }

    public enum IncentivePayMode
    {
        Daily = 1,
        Monthly = 2
    }

    public enum AllowanceOrDeductionMode
    {
        Allowance = 1,
        Deduction = 2,
        Statutory_Allowance = 3,
        Statutory_Deduction = 4
    }

    public enum IncentiveEntityType
    {
        SKILLSET = 5,
        MATERIAL = 6,
        WORKPOSITION = 7,
        TEAMSIZE = 9,
        CERTIFICATE = 10,
        GENDER = 11,
        RESIDENT_STATUS = 12,
        RACE = 13,
        RELIGION = 14
    }

    [Table("IncentiveExcludedLists")]
    public class IncentiveExcludedList : FullAuditedEntity, IMustHaveTenant
    {
        public int IncentiveTagRefId { get; set; }
        [ForeignKey("IncentiveTagRefId")]
        public IncentiveTag IncentiveTags { get; set; }

        public int ExcludedIncentiveTagRefId { get; set; }
        [ForeignKey("ExcludedIncentiveTagRefId")]
        public IncentiveTag ExcludedIncentiveTags { get; set; }

        [MaxLength(50)]
        public string ReasonRemarks { get; set; }

        public virtual int TenantId { get; set; }
       
    }


    [Table("IncentiveVsEntities")]
    public class IncentiveVsEntity : FullAuditedEntity, IMustHaveTenant
    {
        public int IncentiveTagRefId { get; set; }
        [ForeignKey("IncentiveTagRefId")]
        public IncentiveTag IncentiveTag { get; set; }
        public int EntityType { get; set; }
        public int EntityRefId { get; set; }    //  0   For All
        public bool ExcludeFlag { get; set; }
        public bool EitherOr_ForSameEntity { get; set; }
        public int TenantId { get; set; }
      
        public IncentiveVsEntity()
        {

        }

        public IncentiveVsEntity(int tenantId, int argEntityType, int argIncentiveTagRefId, int argEntityRefId, bool argExcludeFlag, bool argEitherOr_ForSameEntity)
        {
            TenantId = tenantId;
            IncentiveTagRefId = argIncentiveTagRefId;
            EntityType = argEntityType;
            EntityRefId = argEntityRefId;
            ExcludeFlag = argExcludeFlag;
            EitherOr_ForSameEntity = argEitherOr_ForSameEntity;
        }
    }

    [Table("EmployeeVsIncentives")]
    public class EmployeeVsIncentive : FullAuditedEntity, IMustHaveTenant
    {
        public int EmployeeRefId { get; set; }
        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformation { get; set; }


        public int IncentiveTagRefId { get; set; }
        [ForeignKey("IncentiveTagRefId")]
        public IncentiveTag IncentiveTag { get; set; }

        public DateTime EffectiveFrom { get; set; }
        public DateTime? RollBackFrom { get; set; }

        [MaxLength(50)]
        public string ReasonRemarks { get; set; }
        [MaxLength(50)]
        public string RemoveRemarks { get; set; }
        public int TenantId { get; set; }
      
    }


    [Table("WorkDays")]
    public class WorkDay : FullAuditedEntity, IMustHaveTenant
    {
        [Index("TenantId_DayOfWeekRefId_EmployeeRefId", IsUnique = true, Order = 3)]
        [Index("TenantId_DayOfWeekRefName_EmployeeRefId", IsUnique = true, Order = 3)]
        public int? EmployeeRefId { get; set; }
        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformations { get; set; }

        [Required]
        [Index("TenantId_DayOfWeekRefId_EmployeeRefId", IsUnique = true, Order = 2)]

        public int DayOfWeekRefId { get; set; }

        [Required]
        [StringLength(15)]
        [Index("TenantId_DayOfWeekRefName_EmployeeRefId", IsUnique = true, Order = 2)]
        [MaxLength(15)]
        public string DayOfWeekRefName { get; set; }

        [Required]
        [StringLength(15)]
        [MaxLength(15)]
        public string DayOfWeekShortName { get; set; }
        public decimal NoOfHourWorks { get; set; }
        public decimal RestHours { get; set; }
        public decimal NoOfWorkDays { get; set; }
        public decimal OtRatio { get; set; }
        public decimal MinimumOTHoursNeededForAutoManualOTEligible { get; set; }
        public decimal MaximumOTAllowedHours { get; set; }
        



        [Index("TenantId_DayOfWeekRefId_EmployeeRefId", IsUnique = true, Order = 1)]

        [Index("TenantId_DayOfWeekRefName_EmployeeRefId", IsUnique = true, Order = 1)]
        public int TenantId { get; set; }

        public WorkDay()
        {

        }

        public WorkDay(int tenantId, int dayofWeekRefId, string dayName, string dayShortName, decimal noOfWorks, decimal restHours, decimal otRatio, decimal noOfWorksdays, decimal maximumOTAllowed)
        {

            TenantId = tenantId;
            DayOfWeekRefId = dayofWeekRefId;
            DayOfWeekRefName = dayName;
            DayOfWeekShortName = dayShortName;
            NoOfHourWorks = noOfWorks;
            RestHours = restHours;
            OtRatio = otRatio;
            NoOfWorkDays = noOfWorksdays;
            MaximumOTAllowedHours = maximumOTAllowed;
        }
    }

    [Table("ManualIncentives")]
    public class ManualIncentive : FullAuditedEntity, IMustHaveTenant
    {
        public DateTime IncentiveDate { get; set; }

        public int EmployeeRefId { get; set; }
        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformations { get; set; }

        public int IncentiveTagRefId { get; set; }
        [ForeignKey("IncentiveTagRefId")]
        public IncentiveTag IncentiveTags { get; set; }

        public decimal IncentiveAmount { get; set; }

        public decimal? ManualOtHours { get; set; }

[MaxLength(50)]
        public string Remarks { get; set; }

        public int? RecommendedUserId { get; set; }

        public int? ApprovedUserId { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public int TenantId { get; set; }
        
    }



    [Table("PublicHolidays")]
    public class PublicHoliday : FullAuditedEntity, IMustHaveTenant
    {
        public virtual DateTime HolidayDate { get; set; }

        [MaxLength(50)]
        public virtual String HolidayReason { get; set; }
        public virtual decimal OTProportion { get; set; }
        public int TenantId { get; set; }
       
    }



    [Table("EMailInformations")]
    public class EMailInformation : FullAuditedEntity, IMustHaveTenant
    {
        [MaxLength(50)]
        public string EmailId { get; set; }
        [MaxLength(50)]
        public string Password { get; set; }
        [MaxLength(50)]
        public string SmtpPort { get; set; }
        [MaxLength(50)]
        public string SmtpHost { get; set; }
        public int SmtpDeliveryMethod { get; set; }
        public int SmtpTimeOutSeconds { get; set; }
        public bool SSLStatus { get; set; }
        public int TenantId { get; set; }
      
    }

   
    [Table("EMailLinkedWithAction")]
    public class EMailLinkedWithActions : FullAuditedEntity
    {
        public int EMailInformationRefId { get; set; }
        [ForeignKey("EMailInformationRefId")]
        public EMailInformation EMailInformations { get; set; }

        public int MailActionRefId { get; set; }
        [MaxLength(50)]
        public string ToEmailList { get; set; }
        [MaxLength(50)]
        public string CCEmailList { get; set; }
        [MaxLength(50)]
        public string BCCEmailList { get; set; }

        public int? SupervisorEmployeeRefId { get; set; }
        [ForeignKey("SupervisorEmployeeRefId")]
        public PersonalInformation SupervisorPersonalInformation { get; set; }

        public int? InchargeEmployeeRefId { get; set; }
        [ForeignKey("InchargeEmployeeRefId")]
        public PersonalInformation InchargePersonalInformation { get; set; }


    }

    [Table("MonthWiseWorkDays")]
    public class MonthWiseWorkDay : FullAuditedEntity, IMustHaveTenant
    {
        [MaxLength(50)]
        public string MonthYear { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int NoOfDays { get; set; }
        public int NoOfLeaves { get; set; }
        public int NoOfGovernmentHolidays { get; set; }
        public int NoOfWorkDays { get; set; }
        public int SalaryCalculationDays { get; set; }
        public int TenantId { get; set; }
       

    }


    [Table("DcHeadMasters")]

    public class DcHeadMaster : FullAuditedEntity,IMustHaveTenant
    {
        [MaxLength(50)]
        public string HeadName { get; set; }
        public bool IsActive { get; set; }
        public int TenantId { get; set; }
       

    }

    [Table("DcGroupMasters")]
    public class DcGroupMaster : FullAuditedEntity, IMustHaveTenant
    {
        public int DcHeadRefId { get; set; }
        [ForeignKey("DcHeadRefId")]
        public DcHeadMaster DcHeadMasters { get; set; }

        [MaxLength(50)]
        public string GroupName { get; set; }

        public decimal PriorityLevel { get; set; }

        public bool IsActive { get; set; }

        public int TenantId { get; set; }
       

    }

    [Table("DcGroupMasterVsSkillSets")]
    public class DcGroupMasterVsSkillSet : FullAuditedEntity,IMustHaveTenant
    {
        public int DcGropupRefId { get; set; }
        [ForeignKey("DcGropupRefId")]
        public DcGroupMaster DcGroupMasters { get; set; }

        public int SkillSetRefId { get; set; }
        [ForeignKey("SkillSetRefId")]
        public SkillSet SkillSet { get; set; }

        public int TenantId { get; set; }
     
    }

    [Table("DcStationMasters")]
    public class DcStationMaster : FullAuditedEntity,IMustHaveTenant
    {
        public int LocationRefId { get; set; }
        [ForeignKey("LocationRefId")]
        public Location Location { get; set; }

        [MaxLength(50)]
        public string StationName { get; set; }

        public int DcGroupRefId { get; set; }
        [ForeignKey("DcGroupRefId")]
        public DcGroupMaster DcGroupMasters { get; set; }

        public int MaximumStaffRequired { get; set; }
        public int MinimumStaffRequired { get; set; }
        public decimal PriorityLevel { get; set; }

        public bool IsMandatory { get; set; }

        public bool IsActive { get; set; }

        public bool NightFlag { get; set; }

        public bool DefaultTimeFlag { get; set; }

        [Required]
        public int TimeIn { get; set; }

        [Required]
        public bool BreakFlag { get; set; }

        [Required]
        public int BreakOut { get; set; }

        [Required]
        public int BreakIn { get; set; }

        public int BreakMinutes { get; set; }

        [Required]
        public int TimeOut { get; set; }

        [MaxLength(50)]
        public string TimeDescription { get; set; }

        public int TenantId { get; set; }
      
    }


    [Table("DcShiftMasters")]
    public class DcShiftMaster : FullAuditedEntity, IMustHaveTenant
    {
        public int LocationRefId { get; set; }
        [ForeignKey("LocationRefId")]
        public Location Location { get; set; }

        [MaxLength(50)]
        public string DutyDesc { get; set; }

        public int StationRefId { get; set; }
        [ForeignKey("StationRefId")]
        public DcStationMaster DcStationMaster { get; set; }

        public int MaximumStaffRequired { get; set; }

        public int MinimumStaffRequired { get; set; }

        public decimal PriorityLevel { get; set; }

        public bool IsMandatory { get; set; }

        public bool IsActive { get; set; }

        public bool NightFlag { get; set; }

        public int TimeIn { get; set; }

        public bool BreakFlag { get; set; }

        public int BreakOut { get; set; }

        public int BreakIn { get; set; }

        public int BreakMinutes { get; set; }

        public int TimeOut { get; set; }

        public int ShiftId { get; set; }

        public int TenantId { get; set; }
      

        [MaxLength(50)]
        public string TimeDescription { get; set; }
    }

    [Table("EmployeeDefaultWorkStations")]
    public class EmployeeDefaultWorkStation : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        public decimal UserSerialNumber { get; set; }

        [Index("EmployeeRefId", IsUnique = true)]
        [Required]
        public int EmployeeRefId { get; set; }
        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformations { get; set; }

        public int SkillRefId { get; set; }
        [ForeignKey("SkillRefId")]
        public SkillSet SkillSet { get; set; }

        public int? StationRefId { get; set; }
        [ForeignKey("StationRefId")]
        public DcStationMaster DcStationMasters { get; set; }

        public int? ShiftRefId { get; set; }
        [ForeignKey("ShiftRefId")]
        public DcShiftMaster DcShiftMasters { get; set; }

        public int TenantId { get; set; }
       

    }

    [Table("DutyCharts")]

    public class DutyChart : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        [Index("DutyChartDateLocationRefIdAndTenantId", 1, IsUnique = true)]
        public int LocationRefId { get; set; }
        [ForeignKey("LocationRefId")]
        public Location Location { get; set; }

        [Required]
        [Index("DutyChartDateLocationRefIdAndTenantId", 2, IsUnique = true)]
        public DateTime DutyChartDate { get; set; }

        [Required]
        [MaxLength(50)]
        public string Status { get; set; }  //  'Draft','Finished', ' ExecutionPending' , 'DayClosed' 

        [Index("DutyChartDateLocationRefIdAndTenantId", 3, IsUnique = true)]
        public int TenantId { get; set; }
        
    }

    [Table("DutyChartDetails")]
    public class DutyChartDetail : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        public int CompanyRefId { get; set; }
        [ForeignKey("CompanyRefId")]
        public Company Company { get; set; }

        [Required]
        public int DutyChartRefId { get; set; }
        [ForeignKey("DutyChartRefId")]
        public DutyChart DutyChart { get; set; }

        [Required]
        [Index("MPK_EmployeeRefId_DutyChartDate_LocationRefId",  IsUnique = true,Order =3)]
        public int LocationRefId { get; set; }
        [ForeignKey("LocationRefId")]
        public Location Location { get; set; }

        [Required]
        [Index("MPK_EmployeeRefId_DutyChartDate_LocationRefId", IsUnique = true, Order = 1)]
        public DateTime DutyChartDate { get; set; }

        [Required]
        [Index("MPK_EmployeeRefId_DutyChartDate_LocationRefId", IsUnique = true, Order = 2)]
        public decimal UserSerialNumber { get; set; }

        [Required]
        [Index("MPK_EmployeeRefId_DutyChartDate_LocationRefId", IsUnique = true, Order = 0)]
        public int EmployeeRefId { get; set; }
        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformation { get; set; }

        [Required]
        [MaxLength(50)]
        public string AllottedStatus { get; set; }

        [Required]
        public int SkillRefId { get; set; }
        [ForeignKey("SkillRefId")]
        public SkillSet SkillSet { get; set; }

        public int? StationRefId { get; set; }
        [ForeignKey("StationRefId")]
        public DcStationMaster DcStationMaster { get; set; }

        public int? ShiftRefId { get; set; }
        [ForeignKey("ShiftRefId")]
        public DcShiftMaster DcShiftMaster { get; set; }

        public bool NightFlag { get; set; }

        public int TimeIn { get; set; }

        public int TimeOut { get; set; }

        public int OtTimeIn { get; set; }

        public int OtTimeOut { get; set; }

        public bool BreakFlag { get; set; }

        public int? BreakOut { get; set; }

        public int? BreakIn { get; set; }
        public int BreakMinutes { get; set; }

        [MaxLength(50)]
        public string Remarks { get; set; }
        public bool HalfDayFlag { get; set; }
      
        public int TenantId { get; set; }
       
    }

    [Table("BioMetricExcludedEntries")]
    public class BioMetricExcludedEntry : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        public int EmployeeRefId { get; set; }
        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformation { get; set; }

        [Required]
        public DateTime ExcludeFromDate { get; set; }

        [Required]
        public DateTime ExcludeToDate { get; set; }

        [MaxLength(50)]
        public string Reason { get; set; }
        public int TenantId { get; set; }
        
    }


    [Table("EmployeeCostCentreMasters")]
    public class EmployeeCostCentreMaster : FullAuditedEntity, IMustHaveTenant
    {
        [MaxLength(50)]
        public string CostCentre { get; set; }
        public int TenantId { get; set; }
        public EmployeeCostCentreMaster()
        {

        }

        public EmployeeCostCentreMaster(int tenantId, string argcostCentre)
        {
            TenantId = tenantId;
            CostCentre = argcostCentre;
        }
    }

    [Table("EmployeeDepartmentMasters")]
    public class EmployeeDepartmentMaster : FullAuditedEntity, IMustHaveTenant
    {
        [MaxLength(50)]
        public string DepartmentName { get; set; }
        public int TenantId { get; set; }
        public EmployeeDepartmentMaster()
        {

        }

        public EmployeeDepartmentMaster(int tenantId, string argDepartmentName)
        {
            TenantId = tenantId;
            DepartmentName = argDepartmentName;
        }
    }

    [Table("EmployeeResidentStatuses")]
    public class EmployeeResidentStatus : FullAuditedEntity, IMustHaveTenant
    {
        [MaxLength(50)]
        public string ResidentStatus { get; set; }
        public int TenantId { get; set; }
      

        public EmployeeResidentStatus()
        {

        }

        public EmployeeResidentStatus(int tenantId, string argEmployeeResidentStatus)
        {
            TenantId = tenantId;
            ResidentStatus = argEmployeeResidentStatus;
        }
    }

    [Table("EmployeeRaces")]
    public class EmployeeRace : FullAuditedEntity, IMustHaveTenant
    {
        [MaxLength(50)]
        public string RaceName { get; set; }
        public int TenantId { get; set; }
       

        public EmployeeRace()
        {

        }

        public EmployeeRace(int tenantId, string argRaceName)
        {
            TenantId = tenantId;
            RaceName = argRaceName;
        }
    }

    [Table("EmployeeReligions")]
    public class EmployeeReligion : FullAuditedEntity, IMustHaveTenant
    {
        [MaxLength(50)]
        public string ReligionName { get; set; }
        public int TenantId { get; set; }
        
        public EmployeeReligion()
        {

        }

        public EmployeeReligion(int tenantId, string argReligionName)
        {
            TenantId = tenantId;
            ReligionName = argReligionName;
        }
    }

    [Table("TimeSheetMasters")]
    public class TimeSheetMaster : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        public int CompanyRefId { get; set; }
        [ForeignKey("CompanyRefId")]
        public Company Company { get; set; }

        [Required]
        public int AcYear { get; set; }

        [Required]
        [MaxLength(50)]
        public string TsMode { get; set; }    //  D   ,   W   , M

        [Required]
        public long ManualTsNumber { get; set; }

        public int? JobOtherLocationRefId { get; set; }
        [ForeignKey("JobOtherLocationRefId")]
        public Location JobOtherLocations { get; set; }

        public DateTime TsDateFrom { get; set; }

        public DateTime TsDateTo { get; set; }

        public decimal TotalManHours { get; set; }

        public decimal TotalOverTimeHours { get; set; }

        public int TsPreparedEmployeeRefId { get; set; }

        public DateTime? PreparedDate { get; set; }

        public DateTime? ApprovedDate { get; set; }
        [MaxLength(50)]
        public string TsStatus { get; set; }        //  P - Pending, C - Completed

        public DateTime? SubmissionDate { get; set; }
        public DateTime? SubmissionDateToInvoiceDepartment { get; set; }

        public DateTime? InvoiceDate { get; set; }

        [MaxLength(50)]
        public string SaleInvoiceReferenceNumber { get; set; }

        [MaxLength(50)]
        public string FileName { get; set; }
        [MaxLength(50)]
        public string FileExtenstionType { get; set; }

        [MaxLength(50)]
        public string RtSourceType { get; set; }

        [MaxLength(50)]
        public string MainCode { get; set; }
        [MaxLength(50)]
        public string ProjectNumber { get; set; }
        [MaxLength(50)]
        public string Module { get; set; }
        [MaxLength(50)]
        public string Location { get; set; }
        [MaxLength(50)]
        public string WoNumber { get; set; }
        [MaxLength(50)]
        public string PoNumber { get; set; }
        [MaxLength(50)]
        public string JVRNumber { get; set; }
        public bool DoesThisAttachmentSupply { get; set; }  //  Means the entire date All Jobs done by employee Duty will be updated with this timesheet.
        public int TenantId { get; set; }
       
    }


    [Table("TimeSheetDetails")]
    public class TimeSheetDetail : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        // [Index("MPK_TsMasterRefId_TsDate", IsUnique = true, Order = 0)]
        public int TsMasterRefId { get; set; }
        [ForeignKey("TsMasterRefId")]
        public TimeSheetMaster TimeSheetMaster { get; set; }

        //[Index("MPK_TsMasterRefId_TsDate", IsUnique = true, Order = 1)]
        public DateTime TsDate { get; set; }

        [MaxLength(50)]
        public string WorkDescription { get; set; }

        public int FromHour { get; set; }

        public int ToHour { get; set; }

        public int BreakOutHour { get; set; }

        public int BreakInHour { get; set; }

        public decimal TotalHours { get; set; }

        public int OverTimeFromHour { get; set; }

        public int OverTimeToHour { get; set; }

        public decimal TotalOverTimeHours { get; set; }

        [MaxLength(50)]
        public string Remarks { get; set; }

        public int TenantId { get; set; }
      


    }


    [Table("TimeSheetMaterialUsages")]
    public class TimeSheetMaterialUsage : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        public int TsMasterRefId { get; set; }
        [ForeignKey("TsMasterRefId")]
        public TimeSheetMaster TimeSheetMaster { get; set; }

        [Required]
        public int TsDetailRefId { get; set; }
        [ForeignKey("TsDetailRefId")]
        public TimeSheetDetail TimeSheetDetail { get; set; }

        public int MaterialRefId { get; set; }
        [ForeignKey("MaterialRefId")]
        public Material Materials { get; set; }

        public decimal QuantityUsed { get; set; }

        public int TenantId { get; set; }
      


    }

    [Table("TimeSheetEmployeeDetails")]
    public class TimeSheetEmployeeDetail : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        public int TsMasterRefId { get; set; }
        [ForeignKey("TsMasterRefId")]
        public TimeSheetMaster TimeSheetMaster { get; set; }

        [Required]
        public int TsDetailRefId { get; set; }
        [ForeignKey("TsDetailRefId")]
        public TimeSheetDetail TimeSheetDetail { get; set; }

        public int EmployeeRefId { get; set; }
        [ForeignKey("EmployeeRefId")]
        public PersonalInformation PersonalInformation { get; set; }

        public int? WorkPositionRefId { get; set; }


        public int TenantId { get; set; }
       

    }


    [Table("StatusMailDates")]
    public class StatusMailDate : FullAuditedEntity, IMustHaveTenant
    {
        public DateTime DutyChartDate { get; set; }
        [MaxLength(50)]
        public string EmployeeListJson { get; set; }
        public int TenantId { get; set; }
       
    }

    public enum MailActions
    {
        EmployeeLeaveRequest = 1,
        EmployeeLeaveApprovalOrReject = 2,
        EmployeeDashBoard,
        DailyDutyChartStatus,
    }

    [Table("OtBasedOnSlabs")]
    public class OtBasedOnSlab : FullAuditedEntity, IMustHaveTenant
    {
        public int EmployeeRefId { get; set; }
        [ForeignKey("EmployeeRefId")]

        public PersonalInformation PersonalInformation { get; set; }
        public int OTRefId { get; set; }

        public decimal FromHours { get; set; }
        public decimal ToHours { get; set; }
        public decimal OTAmount { get; set; }
        public decimal FixedOTAmount { get; set; }
        public bool DoesApplicableBothAmount { get; set; }

        public int TenantId { get; set; }
     
    }

    public enum OTTypes
    {
        OT1 = 1,
        OT2 = 2,
        FlatOT = 3
    }
}
