﻿
using System.Threading.Tasks;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Hr.Impl
{
    public interface IYearWiseLeaveAllowedForEmployeeManager
    {
        Task<IdentityResult> CreateSync(YearWiseLeaveAllowedForEmployee yearWiseLeaveAllowedForEmployee);
    }
}

