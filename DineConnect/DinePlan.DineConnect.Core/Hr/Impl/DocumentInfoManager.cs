﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Hr.Impl
{
    public class DocumentInfoManager : DineConnectServiceBase, IDocumentInfoManager, ITransientDependency
    {

        private readonly IRepository<DocumentInfo> _documentInfoRepo;

        public DocumentInfoManager(IRepository<DocumentInfo> documentInfo)
        {
            _documentInfoRepo = documentInfo;
        }

        public async Task<IdentityResult> CreateSync(DocumentInfo documentInfo)
        {
            //  if the New Addition
            if (documentInfo.Id == 0)
            {
                if (_documentInfoRepo.GetAll().Any(a => a.DocumentName.Equals(documentInfo.DocumentName)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _documentInfoRepo.InsertAndGetIdAsync(documentInfo);
                return IdentityResult.Success;

            }
            else
            {
                List<DocumentInfo> lst = _documentInfoRepo.GetAll().Where(a => a.DocumentName.Equals(documentInfo.DocumentName) && a.Id != documentInfo.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
