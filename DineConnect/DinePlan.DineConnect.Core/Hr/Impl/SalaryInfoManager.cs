﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Impl
{
    public class SalaryInfoManager : DineConnectServiceBase, ISalaryInfoManager, ITransientDependency
    {

        private readonly IRepository<SalaryInfo> _salaryInfoRepo;

        public SalaryInfoManager(IRepository<SalaryInfo> salaryInfo)
        {
            _salaryInfoRepo = salaryInfo;
        }

        public async Task<IdentityResult> CreateSync(SalaryInfo salaryInfo)
        {
            //  if the New Addition
            if (salaryInfo.Id == 0)
            {
                if (_salaryInfoRepo.GetAll().Any(a => a.Id.Equals(salaryInfo.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _salaryInfoRepo.InsertAndGetIdAsync(salaryInfo);
                return IdentityResult.Success;

            }
            else
            {
                List<SalaryInfo> lst = _salaryInfoRepo.GetAll().Where(a => a.Id.Equals(salaryInfo.Id) && a.Id != salaryInfo.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }



	}
}