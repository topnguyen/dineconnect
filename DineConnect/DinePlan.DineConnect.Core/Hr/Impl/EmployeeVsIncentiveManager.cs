﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Hr.Impl;

namespace DinePlan.DineConnect.Hr.Impl
{
	public class EmployeeVsIncentiveManager : DineConnectServiceBase, IEmployeeVsIncentiveManager, ITransientDependency
	{

		private readonly IRepository<EmployeeVsIncentive> _employeeVsIncentiveRepo;

		public EmployeeVsIncentiveManager(IRepository<EmployeeVsIncentive> employeeVsIncentive)
		{
			_employeeVsIncentiveRepo = employeeVsIncentive;
		}

		public async Task<IdentityResult> CreateSync(EmployeeVsIncentive employeeVsIncentive)
		{
			//  if the New Addition
			if (employeeVsIncentive.Id == 0)
			{
				if (_employeeVsIncentiveRepo.GetAll().Any(a => a.EmployeeRefId.Equals(employeeVsIncentive.EmployeeRefId)))
				{
					string[] strArrays = { L("NameAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				await _employeeVsIncentiveRepo.InsertAndGetIdAsync(employeeVsIncentive);
				return IdentityResult.Success;

			}
			else
			{
				List<EmployeeVsIncentive> lst = _employeeVsIncentiveRepo.GetAll().Where(a => a.EmployeeRefId.Equals(employeeVsIncentive.EmployeeRefId) && a.Id != employeeVsIncentive.Id).ToList();
				if (lst.Count > 0)
				{
					string[] strArrays = { L("NameAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				return IdentityResult.Success;
			}
		}

	}
}
