﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.Hr.Impl
{
	public interface IIncentiveTagManager
	{
		Task<IdentityResult> CreateSync(IncentiveTag IncentiveTag);

		Task<IdentityResult> IncentiveCategoryCreateSync(IncentiveCategory IncentiveTag);
	}
}
