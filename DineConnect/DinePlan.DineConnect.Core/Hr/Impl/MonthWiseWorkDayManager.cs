﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Hr.Impl
{
    public class MonthWiseWorkDayManager : DineConnectServiceBase, IMonthWiseWorkDayManager, ITransientDependency
    {

        private readonly IRepository<MonthWiseWorkDay> _monthWiseWorkDayRepo;

        public MonthWiseWorkDayManager(IRepository<MonthWiseWorkDay> monthWiseWorkDay)
        {
            _monthWiseWorkDayRepo = monthWiseWorkDay;
        }

        public async Task<IdentityResult> CreateSync(MonthWiseWorkDay monthWiseWorkDay)
        {
            //  if the New Addition
            if (monthWiseWorkDay.Id == 0)
            {
                if (_monthWiseWorkDayRepo.GetAll().Any(a => a.MonthYear.Equals(monthWiseWorkDay.MonthYear)))
                {
                    string[] strArrays = { L("MonthAndYearAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _monthWiseWorkDayRepo.InsertAndGetIdAsync(monthWiseWorkDay);
                return IdentityResult.Success;

            }
            else
            {
                List<MonthWiseWorkDay> lst = _monthWiseWorkDayRepo.GetAll().Where(a => a.MonthYear.Equals(monthWiseWorkDay.MonthYear) && a.Id != monthWiseWorkDay.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("MonthAndYearAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}