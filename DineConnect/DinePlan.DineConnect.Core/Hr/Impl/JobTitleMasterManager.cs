﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public class JobTitleMasterManager : DineConnectServiceBase, IJobTitleMasterManager, ITransientDependency
    {

        private readonly IRepository<JobTitleMaster> _jobTitleMasterRepo;

        public JobTitleMasterManager(IRepository<JobTitleMaster> jobTitleMaster)
        {
            _jobTitleMasterRepo = jobTitleMaster;
        }

        public async Task<IdentityResult> CreateSync(JobTitleMaster jobTitleMaster)
        {
            //  if the New Addition
            if (jobTitleMaster.Id == 0)
            {
                if (_jobTitleMasterRepo.GetAll().Any(a => a.JobTitle.Equals(jobTitleMaster.JobTitle)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _jobTitleMasterRepo.InsertAndGetIdAsync(jobTitleMaster);
                return IdentityResult.Success;

            }
            else
            {
                List<JobTitleMaster> lst = _jobTitleMasterRepo.GetAll().Where(a => a.JobTitle.Equals(jobTitleMaster.JobTitle) && a.Id != jobTitleMaster.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}    

