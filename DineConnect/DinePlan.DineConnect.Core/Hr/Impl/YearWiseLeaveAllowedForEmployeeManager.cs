﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Hr.Impl
{
    public class YearWiseLeaveAllowedForEmployeeManager : DineConnectServiceBase, IYearWiseLeaveAllowedForEmployeeManager, ITransientDependency
    {

        private readonly IRepository<YearWiseLeaveAllowedForEmployee> _yearWiseLeaveAllowedForEmployeeRepo;

        public YearWiseLeaveAllowedForEmployeeManager(IRepository<YearWiseLeaveAllowedForEmployee> yearWiseLeaveAllowedForEmployee)
        {
            _yearWiseLeaveAllowedForEmployeeRepo = yearWiseLeaveAllowedForEmployee;
        }

        public async Task<IdentityResult> CreateSync(YearWiseLeaveAllowedForEmployee yearWiseLeaveAllowedForEmployee)
        {
            //  if the New Addition
            if (yearWiseLeaveAllowedForEmployee.Id == 0)
            {
                if (_yearWiseLeaveAllowedForEmployeeRepo.GetAll().Any(a => 
                a.EmployeeRefId == yearWiseLeaveAllowedForEmployee.EmployeeRefId && a.AcYear==yearWiseLeaveAllowedForEmployee.AcYear && a.LeaveTypeRefCode==a.LeaveTypeRefCode))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _yearWiseLeaveAllowedForEmployeeRepo.InsertAndGetIdAsync(yearWiseLeaveAllowedForEmployee);
                return IdentityResult.Success;

            }
            else
            {
                List<YearWiseLeaveAllowedForEmployee> lst = _yearWiseLeaveAllowedForEmployeeRepo.GetAll().Where(a => a.EmployeeRefId.Equals(yearWiseLeaveAllowedForEmployee.EmployeeRefId) && a.Id != yearWiseLeaveAllowedForEmployee.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}