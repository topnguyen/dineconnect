﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Hr.Impl
{
    public class EmployeeDocumentInfoManager : DineConnectServiceBase, IEmployeeDocumentInfoManager, ITransientDependency
    {

        private readonly IRepository<EmployeeDocumentInfo> _employeeDocumentInfoRepo;

        public EmployeeDocumentInfoManager(IRepository<EmployeeDocumentInfo> employeeDocumentInfo)
        {
            _employeeDocumentInfoRepo = employeeDocumentInfo;
        }

        public async Task<IdentityResult> CreateSync(EmployeeDocumentInfo employeeDocumentInfo)
        {
            //  if the New Addition
            if (employeeDocumentInfo.Id == 0)
            {
                if (_employeeDocumentInfoRepo.GetAll().Any(a => a.DocumentInfoRefId == employeeDocumentInfo.DocumentInfoRefId && a.EmployeeRefId == employeeDocumentInfo.EmployeeRefId))
                {
                    string[] strArrays = { L("DocumentAlreadyExistsForThisEmployee") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }

                await _employeeDocumentInfoRepo.InsertAndGetIdAsync(employeeDocumentInfo);
                return IdentityResult.Success;

            }
            else
            {
                List<EmployeeDocumentInfo> lst = _employeeDocumentInfoRepo.GetAll().Where(a => a.DocumentInfoRefId == employeeDocumentInfo.DocumentInfoRefId && 
                a.Id != employeeDocumentInfo.Id && a.EmployeeRefId == employeeDocumentInfo.EmployeeRefId).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("EmployeeAlreadyExistsForThisDocument") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }


                return IdentityResult.Success;
            }
        }

    }
}