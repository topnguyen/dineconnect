﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Hr;


namespace DinePlan.DineConnect.Hr.Impl
{
    public interface IPersonalInformationManager 
    {
        Task<IdentityResult> CreateSync(PersonalInformation personalInformation);

    }
}
