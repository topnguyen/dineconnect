﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class DcHeadMasterManager : DineConnectServiceBase, IDcHeadMasterManager, ITransientDependency
    {

        private readonly IRepository<DcHeadMaster> _dcHeadMasterRepo;

        public DcHeadMasterManager(IRepository<DcHeadMaster> dcHeadMaster)
        {
            _dcHeadMasterRepo = dcHeadMaster;
        }

        public async Task<IdentityResult> CreateSync(DcHeadMaster dcHeadMaster)
        {
            //  if the New Addition
            if (dcHeadMaster.Id == 0)
            {
                if (_dcHeadMasterRepo.GetAll().Any(a => a.HeadName.Equals(dcHeadMaster.HeadName)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _dcHeadMasterRepo.InsertAndGetIdAsync(dcHeadMaster);
                return IdentityResult.Success;

            }
            else
            {
                List<DcHeadMaster> lst = _dcHeadMasterRepo.GetAll().Where(a => a.HeadName.Equals(dcHeadMaster.HeadName) && a.Id != dcHeadMaster.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
