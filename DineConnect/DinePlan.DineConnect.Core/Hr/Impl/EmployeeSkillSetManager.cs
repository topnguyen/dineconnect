﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Hr.Impl
{
    public class EmployeeSkillSetManager : DineConnectServiceBase, IEmployeeSkillSetManager, ITransientDependency
    {

        private readonly IRepository<EmployeeSkillSet> _employeeSkillSetRepo;

        public EmployeeSkillSetManager(IRepository<EmployeeSkillSet> employeeSkillSet)
        {
            _employeeSkillSetRepo = employeeSkillSet;
        }

        public async Task<IdentityResult> CreateSync(EmployeeSkillSet employeeSkillSet)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var existAlready = await _employeeSkillSetRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == employeeSkillSet.EmployeeRefId
                    && t.SkillSetRefId == employeeSkillSet.SkillSetRefId );
                if (existAlready != null)
                {
                    existAlready.DeleterUserId = null;
                    existAlready.DeletionTime = null;
                    existAlready.IsDeleted = false;
                    await _employeeSkillSetRepo.InsertOrUpdateAndGetIdAsync(existAlready);
                    return IdentityResult.Success;
                }
            }

            //  if the New Addition
            if (employeeSkillSet.Id == 0)
            {
                if (_employeeSkillSetRepo.GetAll().Any(a => a.Id.Equals(employeeSkillSet.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }

                if (_employeeSkillSetRepo.GetAll().Any(a => a.Id.Equals(employeeSkillSet.Id) && a.SkillSetRefId.Equals(employeeSkillSet.SkillSetRefId) && a.EmployeeRefId.Equals(employeeSkillSet.EmployeeRefId)))
                {
                    string[] strArrays = { L("SkillSetAlreadyExistsForThisEmployee") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _employeeSkillSetRepo.InsertAndGetIdAsync(employeeSkillSet);
                return IdentityResult.Success;

            }
            else
            {
                List<EmployeeSkillSet> lst = _employeeSkillSetRepo.GetAll().Where(a => a.Id.Equals(employeeSkillSet.Id) && a.Id != employeeSkillSet.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}