﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Hr.Impl
{
    public class UserDefaultInformationManager : DineConnectServiceBase, IUserDefaultInformationManager, ITransientDependency
    {

        private readonly IRepository<UserDefaultInformation> _userDefaultInformationsRepo;

        public UserDefaultInformationManager(IRepository<UserDefaultInformation> userDefaultInformations)
        {
            _userDefaultInformationsRepo = userDefaultInformations;
        }

        public async Task<IdentityResult> CreateSync(UserDefaultInformation userDefaultInformations)
        {
            //  if the New Addition
            if (userDefaultInformations.Id == 0)
            {
                if (_userDefaultInformationsRepo.GetAll().Any(a => a.UserId.Equals(userDefaultInformations.UserId)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _userDefaultInformationsRepo.InsertAndGetIdAsync(userDefaultInformations);
                return IdentityResult.Success;
            }
            else
            {
                List<UserDefaultInformation> lst = _userDefaultInformationsRepo.GetAll().Where(a => a.UserId.Equals(userDefaultInformations.UserId) && a.Id != userDefaultInformations.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }
    }
}
