﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Impl
{
	public class WorkDayManager : DineConnectServiceBase, IWorkDayManager, ITransientDependency
	{

		private readonly IRepository<WorkDay> _workDayRepo;

		public WorkDayManager(IRepository<WorkDay> workDay)
		{
			_workDayRepo = workDay;
		}

		public async Task<IdentityResult> CreateSync(WorkDay workDay)
		{
			//  if the New Addition
			if (workDay.Id == 0)
			{
				if (_workDayRepo.GetAll().Any(a => a.DayOfWeekRefName.Equals(workDay.DayOfWeekRefName)))
				{
					string[] strArrays = { L("NameAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				if (_workDayRepo.GetAll().Any(a => a.DayOfWeekShortName.Equals(workDay.DayOfWeekShortName)))
				{
					string[] strArrays = { L("NameAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				await _workDayRepo.InsertAndGetIdAsync(workDay);
				return IdentityResult.Success;

			}
			else
			{
				List<WorkDay> lst = _workDayRepo.GetAll().Where(a => a.DayOfWeekRefName.Equals(workDay.DayOfWeekRefName) && a.Id != workDay.Id).ToList();
				if (lst.Count > 0)
				{
					string[] strArrays = { L("NameAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				lst = _workDayRepo.GetAll().Where(a => a.DayOfWeekShortName.Equals(workDay.DayOfWeekShortName) && a.Id != workDay.Id).ToList();
				if (lst.Count > 0)
				{
					string[] strArrays = { L("NameAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				return IdentityResult.Success;
			}
		}

	}
}

