﻿
using System.Threading.Tasks;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public interface IJobTitleMasterManager
    {
        Task<IdentityResult> CreateSync(JobTitleMaster jobTitleMaster);
    }
}