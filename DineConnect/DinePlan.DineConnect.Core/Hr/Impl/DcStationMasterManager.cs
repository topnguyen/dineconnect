﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class DcStationMasterManager : DineConnectServiceBase, IDcStationMasterManager, ITransientDependency
    {

        private readonly IRepository<DcStationMaster> _dcStationMasterRepo;
        private readonly IRepository<DcGroupMaster> _dcgroupMasterRepo;

        public DcStationMasterManager(IRepository<DcStationMaster> dcStationMaster,
            IRepository<DcGroupMaster> dcgroupMasterRepo)
        {
            _dcStationMasterRepo = dcStationMaster;
            _dcgroupMasterRepo = dcgroupMasterRepo;
        }

        public async Task<IdentityResult> CreateSync(DcStationMaster dcStationMaster)
        {
            //  if the New Addition
            if (dcStationMaster.Id == 0)
            {
                if (_dcStationMasterRepo.GetAll().Any(a => a.DcGroupRefId == dcStationMaster.DcGroupRefId && a.StationName.Equals(dcStationMaster.StationName)))
                {
                    string groupName = "";
                    var grp = await _dcgroupMasterRepo.FirstOrDefaultAsync(t => t.Id == dcStationMaster.DcGroupRefId);
                    if (grp != null)
                    {
                        groupName = grp.GroupName;
                    }
                    string[] strArrays = { L("Group") + " " + groupName + " : " + L("UniqueExists_f", L("Shift"), dcStationMaster.StationName) };

                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _dcStationMasterRepo.InsertAndGetIdAsync(dcStationMaster);
                return IdentityResult.Success;

            }
            else
            {
                List<DcStationMaster> lst = _dcStationMasterRepo.GetAll().Where(a => a.DcGroupRefId == dcStationMaster.DcGroupRefId && a.StationName.Equals(dcStationMaster.StationName) && a.Id != dcStationMaster.Id).ToList();
                if (lst.Count > 0)
                {
                    string groupName = "";
                    var grp = await _dcgroupMasterRepo.FirstOrDefaultAsync(t => t.Id == dcStationMaster.DcGroupRefId);
                    if (grp != null)
                    {
                        groupName = grp.GroupName;
                    }
                    string[] strArrays = { L("Group") + " " + groupName + " : " + L("UniqueExists_f", L("Shift"), dcStationMaster.StationName) };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}