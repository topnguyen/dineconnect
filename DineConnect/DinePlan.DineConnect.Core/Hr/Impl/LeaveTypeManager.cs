﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Impl
{
    public class LeaveTypeManager : DineConnectServiceBase, ILeaveTypeManager, ITransientDependency
    {

        private readonly IRepository<LeaveType> _leaveTypeRepo;

        public LeaveTypeManager(IRepository<LeaveType> leaveType)
        {
            _leaveTypeRepo = leaveType;
        }

        public async Task<IdentityResult> CreateSync(LeaveType leaveType)
        {
            //  if the New Addition
            if (leaveType.Id == 0)
            {
                if (_leaveTypeRepo.GetAll().Any(a => a.Id.Equals(leaveType.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _leaveTypeRepo.InsertAndGetIdAsync(leaveType);
                return IdentityResult.Success;

            }
            else
            {
                List<LeaveType> lst = _leaveTypeRepo.GetAll().Where(a => a.Id.Equals(leaveType.Id) && a.Id != leaveType.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

