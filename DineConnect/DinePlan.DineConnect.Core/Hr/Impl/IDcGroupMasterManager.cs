﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IDcGroupMasterManager
    {
        Task<IdentityResult> CreateSync(DcGroupMaster dcGroupMaster);
    }
}
