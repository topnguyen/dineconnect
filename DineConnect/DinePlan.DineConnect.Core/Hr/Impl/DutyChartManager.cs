﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.House.Impl
{
    public class DutyChartManager : DineConnectServiceBase, IDutyChartManager, ITransientDependency
    {

        private readonly IRepository<DutyChart> _dutyChartRepo;

        public DutyChartManager(IRepository<DutyChart> dutyChart)
        {
            _dutyChartRepo = dutyChart;
        }

        public async Task<IdentityResult> CreateSync(DutyChart dutyChart)
        {
            //  if the New Addition
            if (dutyChart.Id == 0)
            {
                if (_dutyChartRepo.GetAll().Any(a => a.LocationRefId == dutyChart.LocationRefId && a.DutyChartDate== dutyChart.DutyChartDate))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _dutyChartRepo.InsertAndGetIdAsync(dutyChart);
                return IdentityResult.Success;

            }
            else
            {
                List<DutyChart> lst = _dutyChartRepo.GetAll().Where(a => a.LocationRefId == dutyChart.LocationRefId && a.DutyChartDate == dutyChart.DutyChartDate && a.Id != dutyChart.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
