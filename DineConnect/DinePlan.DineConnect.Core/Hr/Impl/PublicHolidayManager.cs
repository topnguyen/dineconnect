﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Hr.Impl
{
    public class PublicHolidayManager : DineConnectServiceBase, IPublicHolidayManager, ITransientDependency
    {

        private readonly IRepository<PublicHoliday> _publicHolidayRepo;

        public PublicHolidayManager(IRepository<PublicHoliday> publicHoliday)
        {
            _publicHolidayRepo = publicHoliday;
        }

        public async Task<IdentityResult> CreateSync(PublicHoliday publicHoliday)
        {
            //  if the New Addition
            if (publicHoliday.Id == 0)
            {
                if (_publicHolidayRepo.GetAll().Any(a => a.HolidayDate.Equals(publicHoliday.HolidayDate)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _publicHolidayRepo.InsertAndGetIdAsync(publicHoliday);
                return IdentityResult.Success;

            }
            else
            {
                List<PublicHoliday> lst = _publicHolidayRepo.GetAll().Where(a => a.HolidayDate.Equals(publicHoliday.HolidayDate) && a.Id != publicHoliday.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

