﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Hr.Impl
{
    public class AttendanceLogManager : DineConnectServiceBase, IAttendanceLogManager, ITransientDependency
    {

        private readonly IRepository<AttendanceLog> _attendanceLogRepo;

        public AttendanceLogManager(IRepository<AttendanceLog> attendanceLog)
        {
            _attendanceLogRepo = attendanceLog;
        }

        public async Task<IdentityResult> CreateSync(AttendanceLog attendanceLog)
        {
            //  if the New Addition
            if (attendanceLog.Id == 0)
            {
                if (_attendanceLogRepo.GetAll().Any(a => a.Id.Equals(attendanceLog.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _attendanceLogRepo.InsertAndGetIdAsync(attendanceLog);
                return IdentityResult.Success;

            }
            else
            {
                List<AttendanceLog> lst = _attendanceLogRepo.GetAll().Where(a => a.Id.Equals(attendanceLog.Id) && a.Id != attendanceLog.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
