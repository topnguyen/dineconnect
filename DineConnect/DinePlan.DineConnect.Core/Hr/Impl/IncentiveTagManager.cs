﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.Hr.Impl;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Hr.Impl
{
	public class IncentiveTagManager : DineConnectServiceBase, IIncentiveTagManager, ITransientDependency
	{

		private readonly IRepository<IncentiveTag> _incentiveTagRepo;
		private readonly IRepository<IncentiveCategory> _incentivecategoryRepo;

		public IncentiveTagManager(IRepository<IncentiveTag> IncentiveTag,
			IRepository<IncentiveCategory> IncentivecategoryRepo
			)
		{
			_incentiveTagRepo = IncentiveTag;
			_incentivecategoryRepo = IncentivecategoryRepo;
		}

		public async Task<IdentityResult> CreateSync(IncentiveTag IncentiveTag)
		{
			//  if the New Addition
			if (IncentiveTag.Id == 0)
			{
                var exist = await _incentiveTagRepo.FirstOrDefaultAsync(a => a.IncentiveTagCode.Equals(IncentiveTag.IncentiveTagCode) || a.IncentiveTagName.Equals(IncentiveTag.IncentiveTagName));
                if (exist!=null)
				{
					string[] strArrays = { L("NameAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				await _incentiveTagRepo.InsertAndGetIdAsync(IncentiveTag);
				return IdentityResult.Success;
			}
			else
			{
                var lst = await _incentiveTagRepo.GetAll().Where(a => (a.IncentiveTagCode.Equals(IncentiveTag.IncentiveTagCode) || a.IncentiveTagName.Equals(IncentiveTag.IncentiveTagName)) && a.Id != IncentiveTag.Id).ToListAsync();
				if (lst.Count > 0)
				{
					string[] strArrays = { L("NameAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				return IdentityResult.Success;
			}
		}

		public async Task<IdentityResult> IncentiveCategoryCreateSync(IncentiveCategory incentiveCategory)
		{
			//  if the New Addition
			if (incentiveCategory.Id == 0)
			{
                var exist = await _incentivecategoryRepo.FirstOrDefaultAsync(a => a.IncentiveCategoryCode.Equals(incentiveCategory.IncentiveCategoryCode) || a.IncentiveCategoryName.Equals(incentiveCategory.IncentiveCategoryName));
                if (exist!=null)
				{
					string[] strArrays = { L("AlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				await _incentivecategoryRepo.InsertAndGetIdAsync(incentiveCategory);
				return IdentityResult.Success;

			}
			else
			{
				var lst = await _incentivecategoryRepo.GetAll().Where(a => (a.IncentiveCategoryCode.Equals(incentiveCategory.IncentiveCategoryCode) || a.IncentiveCategoryName.Equals(incentiveCategory.IncentiveCategoryName)) && a.Id != incentiveCategory.Id).ToListAsync();
				if (lst.Count > 0)
				{
					string[] strArrays = { L("AlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				return IdentityResult.Success;
			}
		}
	}
}
