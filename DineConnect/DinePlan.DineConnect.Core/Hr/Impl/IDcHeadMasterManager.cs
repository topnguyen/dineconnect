﻿
using System.Threading.Tasks;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IDcHeadMasterManager
    {
        Task<IdentityResult> CreateSync(DcHeadMaster dcHeadMaster);
    }
}