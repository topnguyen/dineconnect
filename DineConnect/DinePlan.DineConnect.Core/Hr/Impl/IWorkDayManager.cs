﻿
using System.Threading.Tasks;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Hr.Impl
{
	public interface IWorkDayManager
	{
		Task<IdentityResult> CreateSync(WorkDay workDay);
	}
}