﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Hr.Impl
{
    public class LeaveRequestManager : DineConnectServiceBase, ILeaveRequestManager, ITransientDependency
    {

        private readonly IRepository<LeaveRequest> _leaveRequestRepo;

        public LeaveRequestManager(IRepository<LeaveRequest> leaveRequest)
        {
            _leaveRequestRepo = leaveRequest;
        }

        public async Task<IdentityResult> CreateSync(LeaveRequest leaveRequest)
        {
            //  if the New Addition
            if (leaveRequest.Id == 0)
            {
                if (_leaveRequestRepo.GetAll().Any(a => a.Id.Equals(leaveRequest.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _leaveRequestRepo.InsertAndGetIdAsync(leaveRequest);
                return IdentityResult.Success;

            }
            else
            {
                List<LeaveRequest> lst = _leaveRequestRepo.GetAll().Where(a => a.Id.Equals(leaveRequest.Id) && a.Id != leaveRequest.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
