﻿
using System.Threading.Tasks;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Hr.Impl
{
    public interface ISalaryInfoManager
    {
        Task<IdentityResult> CreateSync(SalaryInfo salaryInfo);
    }
}