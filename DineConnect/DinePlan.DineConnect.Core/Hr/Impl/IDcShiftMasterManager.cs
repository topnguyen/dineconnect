﻿
using System.Threading.Tasks;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IDcShiftMasterManager
    {
        Task<IdentityResult> CreateSync(DcShiftMaster dcShiftMaster);
    }
}
