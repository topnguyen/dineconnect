﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Impl
{
    public interface IDocumentInfoManager
    {
        Task<IdentityResult> CreateSync(DocumentInfo documentInfo);
    }
}
