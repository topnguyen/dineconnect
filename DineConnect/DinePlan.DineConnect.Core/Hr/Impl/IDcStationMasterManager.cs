﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Hr;


namespace DinePlan.DineConnect.House.Impl
{
    public interface IDcStationMasterManager
    {
        Task<IdentityResult> CreateSync(DcStationMaster dcStationMaster);
    }
}
