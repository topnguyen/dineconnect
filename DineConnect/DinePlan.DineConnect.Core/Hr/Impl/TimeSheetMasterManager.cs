﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Hr.Impl
{
    public class TimeSheetMasterManager : DineConnectServiceBase, ITimeSheetMasterManager, ITransientDependency
    {

        private readonly IRepository<TimeSheetMaster> _timeSheetMasterRepo;

        public TimeSheetMasterManager(IRepository<TimeSheetMaster> timeSheetMaster)
        {
            _timeSheetMasterRepo = timeSheetMaster;
        }

        public async Task<IdentityResult> CreateSync(TimeSheetMaster timeSheetMaster)
        {
            //  if the New Addition
            if (timeSheetMaster.Id == 0)
            {
                if (_timeSheetMasterRepo.GetAll().Any(a => a.CompanyRefId == timeSheetMaster.CompanyRefId &&  a.AcYear == timeSheetMaster.AcYear &&  a.ManualTsNumber == timeSheetMaster.ManualTsNumber))
                {
                    string[] strArrays = { L("TimeSheetAlreadyExists", timeSheetMaster.ManualTsNumber) };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _timeSheetMasterRepo.InsertAndGetIdAsync(timeSheetMaster);
                return IdentityResult.Success;

            }
            else
            {
                List<TimeSheetMaster> lst = _timeSheetMasterRepo.GetAll().Where(a => a.CompanyRefId == timeSheetMaster.CompanyRefId && a.AcYear == timeSheetMaster.AcYear && a.ManualTsNumber == timeSheetMaster.ManualTsNumber && a.Id != timeSheetMaster.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("TimeSheetAlreadyExists", timeSheetMaster.ManualTsNumber) };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
