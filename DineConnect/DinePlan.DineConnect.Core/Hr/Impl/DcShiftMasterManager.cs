﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.House.Impl
{
    public class DcShiftMasterManager : DineConnectServiceBase, IDcShiftMasterManager, ITransientDependency
    {

        private readonly IRepository<DcShiftMaster> _dcShiftMasterRepo;
        private readonly IRepository<DcStationMaster> _dcStationMasterRepo;

        public DcShiftMasterManager(IRepository<DcShiftMaster> dcShiftMaster,
            IRepository<DcStationMaster> dcStationMasterRepo)
        {
            _dcShiftMasterRepo = dcShiftMaster;
            _dcStationMasterRepo = dcStationMasterRepo;
        }

        public async Task<IdentityResult> CreateSync(DcShiftMaster dcShiftMaster)
        {
            //  if the New Addition
            if (dcShiftMaster.Id == 0)
            {
                if (_dcShiftMasterRepo.GetAll().Any(a => a.StationRefId==dcShiftMaster.StationRefId && a.DutyDesc.Equals(dcShiftMaster.DutyDesc)))
                {
                    string stationName = "";
                    var stn = await _dcStationMasterRepo.FirstOrDefaultAsync(t => t.Id == dcShiftMaster.StationRefId);
                    if (stn!=null)
                    {
                        stationName = stn.StationName;
                    }
                    string[] strArrays = {L("Station") + " " + stationName + " : " +  L("UniqueExists_f", L("Shift"), dcShiftMaster.DutyDesc) };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _dcShiftMasterRepo.InsertAndGetIdAsync(dcShiftMaster);
                return IdentityResult.Success;

            }
            else
            {
                List<DcShiftMaster> lst = _dcShiftMasterRepo.GetAll().Where(a => a.StationRefId == dcShiftMaster.StationRefId && a.DutyDesc.Equals(dcShiftMaster.DutyDesc) && a.Id != dcShiftMaster.Id).ToList();
                if (lst.Count > 0)
                {
                    string stationName = "";
                    var stn = await _dcStationMasterRepo.FirstOrDefaultAsync(t => t.Id == dcShiftMaster.StationRefId);
                    if (stn != null)
                    {
                        stationName = stn.StationName;
                    }
                    string[] strArrays = { L("Station") + " " + stationName + " : " + L("UniqueExists_f", L("Shift"), dcShiftMaster.DutyDesc) };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}