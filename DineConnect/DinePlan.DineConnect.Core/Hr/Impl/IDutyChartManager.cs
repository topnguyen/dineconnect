﻿
using System.Threading.Tasks;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IDutyChartManager
    {
        Task<IdentityResult> CreateSync(DutyChart dutyChart);
    }
}
