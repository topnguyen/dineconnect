﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class DcGroupMasterManager : DineConnectServiceBase, IDcGroupMasterManager, ITransientDependency
    {

        private readonly IRepository<DcGroupMaster> _dcGroupMasterRepo;

        public DcGroupMasterManager(IRepository<DcGroupMaster> dcGroupMaster)
        {
            _dcGroupMasterRepo = dcGroupMaster;
        }

        public async Task<IdentityResult> CreateSync(DcGroupMaster dcGroupMaster)
        {
            //  if the New Addition
            if (dcGroupMaster.Id == 0)
            {
                if (_dcGroupMasterRepo.GetAll().Any(a => a.DcHeadRefId==dcGroupMaster.DcHeadRefId && a.GroupName.Equals(dcGroupMaster.GroupName)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _dcGroupMasterRepo.InsertAndGetIdAsync(dcGroupMaster);
                return IdentityResult.Success;

            }
            else
            {
                List<DcGroupMaster> lst = _dcGroupMasterRepo.GetAll().Where(a => a.GroupName.Equals(dcGroupMaster.GroupName) && a.Id != dcGroupMaster.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

