﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Hr;
using System.Data.Entity;

namespace DinePlan.DineConnect.Hr.Impl
{
    public class PersonalInformationManager : DineConnectServiceBase, IPersonalInformationManager, ITransientDependency
    {

        private readonly IRepository<PersonalInformation> _personalInformationRepo;

        public PersonalInformationManager(IRepository<PersonalInformation> personalInformation)
        {
            _personalInformationRepo = personalInformation;
        }

        public async Task<IdentityResult> CreateSync(PersonalInformation personalInformation)
        {
            //  if the New Addition
            if (personalInformation.Id == 0)
            {
                var exist = await _personalInformationRepo.FirstOrDefaultAsync(a => a.EmployeeCode.Equals(personalInformation.EmployeeCode));

                if (exist!=null)
                {
                    string[] strArrays = { L("EmployeeCodeAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                if (personalInformation.BioMetricCode != null)
                {
                    exist = await _personalInformationRepo.FirstOrDefaultAsync(a => a.BioMetricCode.Equals(personalInformation.BioMetricCode));
                    if (exist!=null)
                    {
                        string[] strArrays = { L("BioMetricCodeAlreadyExists") };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }
                }
                await _personalInformationRepo.InsertAndGetIdAsync(personalInformation);
                return IdentityResult.Success;
            }
            else
            {
                var lst = await _personalInformationRepo.GetAll().Where(a => a.EmployeeCode.Equals(personalInformation.EmployeeCode) && a.Id != personalInformation.Id).ToListAsync();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("EmployeeCodeAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                if (personalInformation.BioMetricCode != null)
                {
                    lst = await _personalInformationRepo.GetAll().Where(a => a.BioMetricCode == personalInformation.BioMetricCode 
                        && a.Id != personalInformation.Id).ToListAsync();
                    if (lst.Count > 0)
                    {
                        string[] strArrays = { L("BioMetricCodeAlreadyExists") };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }
                 
                }
                return IdentityResult.Success;
            }
        }

    }
}

