﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.Hr.Impl;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Hr.Impl
{
	public class ManualIncentiveManager : DineConnectServiceBase, IManualIncentiveManager, ITransientDependency
	{

		private readonly IRepository<ManualIncentive> _manualIncentiveRepo;

		public ManualIncentiveManager(IRepository<ManualIncentive> manualIncentive)
		{
			_manualIncentiveRepo = manualIncentive;
		}

		public async Task<IdentityResult> CreateSync(ManualIncentive manualIncentive)
		{
			//  if the New Addition
			if (manualIncentive.Id == 0)
			{
				if (_manualIncentiveRepo.GetAll().Any(a => a.EmployeeRefId.Equals(manualIncentive.EmployeeRefId) && a.IncentiveTagRefId==manualIncentive.IncentiveTagRefId && a.IncentiveDate==manualIncentive.IncentiveDate))
				{
					string[] strArrays = { L("AlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				await _manualIncentiveRepo.InsertAndGetIdAsync(manualIncentive);
				return IdentityResult.Success;

			}
			else
			{
				List<ManualIncentive> lst = _manualIncentiveRepo.GetAll().Where(a => a.EmployeeRefId.Equals(manualIncentive.EmployeeRefId) && a.IncentiveTagRefId == manualIncentive.IncentiveTagRefId && a.IncentiveDate == manualIncentive.IncentiveDate && a.Id != manualIncentive.Id).ToList();
				if (lst.Count > 0)
				{
					string[] strArrays = { L("AlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				return IdentityResult.Success;
			}
		}

	}
}

