﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Hr;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Hr.Impl
{
    public class SkillSetManager : DineConnectServiceBase, ISkillSetManager, ITransientDependency
    {

        private readonly IRepository<SkillSet> _skillSetRepo;

        public SkillSetManager(IRepository<SkillSet> skillSet)
        {
            _skillSetRepo = skillSet;
        }

        public async Task<IdentityResult> CreateSync(SkillSet skillSet)
        {
            //  if the New Addition
            if (skillSet.Id == 0)
            {
                if (_skillSetRepo.GetAll().Any(a => a.SkillCode.Equals(skillSet.SkillCode)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _skillSetRepo.InsertAndGetIdAsync(skillSet);
                return IdentityResult.Success;

            }
            else
            {
                List<SkillSet> lst = _skillSetRepo.GetAll().Where(a => a.SkillCode.Equals(skillSet.SkillCode) && a.Id != skillSet.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
