﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Impl
{
    public interface ISkillSetManager
    {
        Task<IdentityResult> CreateSync(SkillSet skillSet);
    }
}