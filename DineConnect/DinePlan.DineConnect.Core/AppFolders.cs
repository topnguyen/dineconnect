﻿using Abp.Dependency;

namespace DinePlan.DineConnect
{
    public class AppFolders : IAppFolders, ISingletonDependency
    {
        public string TempFileDownloadFolder { get; set; }
        public string ImagesFolder { get; set; }
        public string TicketJournalFolder { get; set; }
        public string FullTaxInvoiceFolder { get; set; }

        public string PdfExeFile { get; set; }
        public string UploadFolder { get; set; }
        public string ImportFolder { get; set; }
        public string WebLogsFolder { get; set; }
    }
}