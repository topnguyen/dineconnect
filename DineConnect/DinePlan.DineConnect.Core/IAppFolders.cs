﻿namespace DinePlan.DineConnect
{
    public interface IAppFolders
    {
        string TempFileDownloadFolder { get; }
        string ImagesFolder { get; }

        string TicketJournalFolder { get; }
        string FullTaxInvoiceFolder { get; }
        string ImportFolder { get; }
        string PdfExeFile { get; }
        string UploadFolder { get; }
        string WebLogsFolder { get; set; }
    }
}