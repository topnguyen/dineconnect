﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DinePlan.DineConnect.Connect.ConnectConsts
{
	public class ConnectConsts
	{
		public static readonly string Refund = "Refund";
		public static readonly string Returned = "Returned";
		public static readonly string Status = "Status";
		public const string OutTaxReports_Description_HardCoded_NormalTicket = "Sell products and services";
		public const string OutTaxReports_Description_HardCoded_Refund = "Cancel sales list";
		public const string OutTaxReports_Description_HardCoded_Return = "Return products or services";
		public const string CashSales = "CashSales";
		public const string SalesWithHoldingTax = "SalesWithHoldingTax";
		public static readonly string Cash = "Cash";
		public const string WithHoldTax = "WithHoldTax";
		public const string NumberFormat = "0.00";
		public const string TaxIds = "TaxIds";


		public const string OREmployeeID = "OREmployeeID";
		public const string OREmployeeName = "OREmployeeName";
		public const string ORDepartmentCode = "ORDepartmentCode";
		public const string ORDepartmentName = "ORDepartmentName";
		public const string OREmployeeStatusCode = "OREmployeeStatusCode";
		public const string ORCostCenter = "ORCostCenter";
		public const string ORIO = "ORIO";
		public const string InternalSaleType = "InternalSaleType";

		public static readonly string[] SupportedResolutions = { "1024_768", "1280_1024", "1280_800", "1366_768", "1920_1080", "2160_1440" };
		public static readonly string ResolutionDefault = "1920_1080";
		public static readonly double WindowScaleDefault = 0.9;
		public static readonly int RefreshEntityDefault = 8;

		public static readonly string MaxPackingTime = "MaxPackingTime";
		public static readonly string MaxOrderTime = "MaxOrderTime";


		public static readonly string Low = "Low";
		public static readonly string Hight = "Hight";
		public static readonly string Star = "Star";
		public static readonly string Puzzle = "Puzzle";
		public static readonly string Horse = "Horse";
		public static readonly string Dog = "Dog";
		
		public static Dictionary<string, string> BillStatuses = new Dictionary<string, string>
		{
			{ "C", "Sale"},
			{ "K", "CN"},
			{ "L", "DN"},
			{ "H", "Return"}
		};
		public static string S4HANA = "S4HANA";

	}
}
