﻿namespace DinePlan.DineConnect.HouseReportName
{
    public class HouseReportNames
    {
        public static string[] HouseAllReports =
        {
            CLOSINGSTOCK_GROUP_CATEGORY_MATERIAL_VALUE,
            MATERIAL_TRACK_INVOICE_INTERTRANSFER
        };

        public const string CLOSINGSTOCK_GROUP_CATEGORY_MATERIAL_VALUE = "Closing Stock Report - Group/Category/Material";
        public const string MATERIAL_TRACK_INVOICE_INTERTRANSFER = "Stock Tracking Invoice & Intertransfer";
    }
}