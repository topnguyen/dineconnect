﻿namespace DinePlan.DineConnect.Report
{
    public class ReportNames
    {
        public static string[] AllReports =
        {
            TICKETSALES,
            ITEMSALES,
            ITEMSALESBYPRICE,
            CATEGORYSALES,
            DEPARTMENTSALES,
            WEEKDAYSALES,
            WEEKENDSALES,
            ITEMLOCATIONS,
            ITEMLOCATIONSDETAIL,
            ORDERS,
            ORDERPROMOTION,
            ITEMSALESBYPORTION,
            DEPARTMENTSUMMARY,
            SALESUMMARY,
            SALESUMMARYNEW,
            TILLTRANSACTION,
            LOGGER,
            DAYSUMMARY,
            DAYDETAILS,
            DAYSUMMARYEXPORT,
            SCHEDULESALES,
            SALETARGET,
            DISCOUNT,
            FRANCHISE,
            TENDER,
            ITEMCOMBOSALES,
            ITEMTAGSALES,
            ITEMTAGSALESDETAIL,
            WRITERSCAFEFORTICKET,
            ABBREPRINT,
            TOPORBOTTOMITEMSALES,
            SCHEDULEREPORT,
            LOCATIONSCHEDULEREPORT,
            DAILYSALES,
            //DAILYRECONCILITION,
            LOCATIONCOMPARISON,
            CollectionReport,
            TIMEATTENDANCE,
            EXTERNALLOG ,
            CASHAUDIT,
            SPEEDOFSERVICEREPORT,
            CASHSUMMARYREPORT,
            CREDITCARD,
            CONSOLIDATED,
            PLANWORKDAY,
            MONTHLYSTORESALE,
            MEALTIMESALE,
            MONTHLYSTORESALESTATS,
            CREDITSALES,
            TICKETPROMOTIONDETAIL,
            TOPDOWNSELLAMOUNTPLANT,
            TOPDOWNSELLAMOUNTSUMMARY,
            TOPDOWNSELLQUANTITYPLANT,
            TOPDOWNSELLQUANTITYSUMMARY,
            SALESPROMOTIONBYPLANT,
            SALESPROMOTIONBYPROMOTION,
            PRODUCTMIX,
            MONTHLYSALEADVANCE,
            DAILYSALEBREAK
        };

        public const string CollectionReport = "CollectionReport";

        public const string LOCATIONCOMPARISON = "LocationComparison";
        public const string SCHEDULEREPORT = "ScheduleReport";
        public const string LOCATIONSCHEDULEREPORT = "LocationScheduleReport";

        public const string TICKETSALES = "TicketSales";
        public const string TICKETSYNCS = "TicketSyncs";
        public const string ITEMSALES = "ItemSales";
        public const string ITEMSALESBYPRICE = "ItemSalesByPrice";
        public const string ITEMSALESBYPORTION = "ItemSalesByPortion";
        public const string CATEGORYSALES = "CategorySales";
        public const string HOURLYSALES = "HourlySales";
        public const string DEPARTMENTSALES = "DepartmentSales";
        public const string GROUPSALES = "GroupSales";
        public const string ITEMCOMBOSALES = "ItemComboSales";
        public const string REFUNDTICKETDETAIL = "RefundTicketDetail";
        public const string ITEMANDORDERTAGSALES = "ItemAndOrderTagSales";
        public const string DAILYSALES = "DailySales";
        public const string DAILYRECONCILITION = "DailyReconciliation";
        public const string DAILYRECONCILITIONALL = "DailyReconciliationAll";

        public const string PLANWORKDAY = "PlanWorkDay";
        public const string WEEKDAYSALES = "WeekdaySales";
        public const string WEEKENDSALES = "WeekendSales";
        public const string ITEMHOURS = "ItemHoursSales";

        public const string ITEMLOCATIONS = "ItemLocations";
        public const string ITEMLOCATIONSDETAIL = "ItemLocationsDetail";
        public const string ORDERS = "ItemOrder";
        public const string EXCHANGEREPORT = "ExchangeReport";
        public const string ORDERTAGS = "ItemOrderTag";
        public const string ORDEREXCHANGE = "OrderExchange";
        public const string RETURNPRODUCT = "ReturnProduct";
        public const string ORDERPROMOTION = "OrderPromotionReport";
        public const string DEPARTMENTSUMMARY = "DepartmentSummary";
        public const string SALESUMMARY = "SaleSummary";
        public const string SALESUMMARYNEW = "SaleSummaryNew";
        public const string SALESUMMARYSECOND = "SaleSummarySecond";
        public const string TILLTRANSACTION = "TillTransaction";
        public const string LOGGER = "LoggerReport";
        public const string DAYSUMMARY = "DaySummary";
        public const string DAYDETAILS = "DayDetails";
        public const string DAYSUMMARYEXPORT = "DaySummaryExport";
        public const string SCHEDULESALES = "ScheduleSales";
        public const string SALETARGET = "SaleTarget";
        public const string DISCOUNT = "TicketPromotion";
        public const string TICKETPROMOTIONDETAIL = "TicketPromotionDetail";
        public const string FRANCHISE = "Franchise";
        public const string TENDER = "Tender";
        public const string PAYMENTEXCESSSUMMARY = "PaymentExcessSummary";
        public const string PAYMENTEXCESSDETAIL = "PaymentExcessDetail";
        public const string ABNORMALENDDAY = "AbnormalEndDay";

        public const string ITEMTAGSALES = "ItemTagSales";
        public const string ITEMTAGSALESDETAIL = "ItemTagSalesDetail";
        public const string TOPORBOTTOMITEMSALES = "TopOrBottomItemSales";

        public const string FULLTAXINVOICEREPORT = "FULLTAXINVOICEREPORT";

        public const string FEEDBACKUNREAD = "FeedBackUnread";
        public const string TOPDOWNSELLAMOUNTSUMMARY = "TopDownSellAmountSummary";
        public const string TOPDOWNSELLQUANTITYSUMMARY = "TopDownSellQuantitySummary";
        public const string TOPDOWNSELLAMOUNTPLANT = "TopDownSellAmountPlant";
        public const string TOPDOWNSELLQUANTITYPLANT = "TopDownSellQuantityPlant";

        public const string PRODUCTMIX = "ProductMix";

        public const string SALESPROMOTIONBYPROMOTION = "SALESPROMOTIONBYPROMOTION";
        public const string SALESPROMOTIONBYPLANT = "SALESPROMOTIONBYPLANT";

        public const string ABBREPRINT = "ABBReprint";
        public const string TIMEATTENDANCE = "TimeAttendance";

        

        //Custom
        public const string WRITERSCAFE = "WritersCafe";
        public const string WRITERSCAFEFORTICKET = "WritersCafeForTicket";
        public const string QUOTAS = "Plant Quotas";
        public const string QUOTATICKETS = "Quota Tickets";

        public const string EXTERNALLOG="ExternalLog";

        public const string CASHAUDIT = "CASHAUDIT";
        public const string CREDITCARD = "CREDITCARD";
        public const string CREDITSALES = "CREDITSALES";

        public const string SPEEDOFSERVICEREPORT = "SPEEDOFSERVICEREPORT";
        public const string CASHSUMMARYREPORT = "CASHSUMMARYREPORT";

        public const string CONSOLIDATED = "CONSOLIDATED";
        public const string MONTHLYSTORESALE = "MONTHLYSTORESALE";
        public const string MONTHLYSALEADVANCE = "MONTHLYSALEADVANCE";
        public const string DAILYSALEBREAK = "DAILYSALEBREAK";
        public const string MEALTIMESALE = "MEALTIMESALE";
        public const string SALESBYPERIOD = "SALESBYPERIOD";
        public const string MONTHLYSTORESALESTATS = "MONTHLYSTORESALESTATS";

    }
}