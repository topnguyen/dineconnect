﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace DinePlan.DineConnect.Addon
{

    [Table("SystemLoggings")]
    public class SystemLogging: Entity<int>
    {
        [Key]
        public Guid system_logging_guid { get; set; }
        public string log_application { get; set; }
        public DateTime log_date { get; set; }
        public string log_level { get; set; }
        public string log_logger { get; set; }
        public string log_message { get; set; }
        public string log_machine_name { get; set; }
        public string log_user_name { get; set; }
        public string log_call_site { get; set; }
        public string log_thread { get; set; }
        public string log_exception { get; set; }
        public string log_stacktrace { get; set; }
    }
}
