﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace DinePlan.DineConnect.Addon
{
    [Table("ConnectAddOns")]
    public class ConnectAddOn : FullAuditedEntity, IMustHaveTenant
    {
        [MaxLength(50)]
        public string Name { get; set; }
        public string AddOnSettings { get; set; }
        public string Settings { get; set; }
        public int TenantId { get; set; }
    }
}
