﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect
{
	public interface IAlertManager
	{
        Task<IdentityResult> CreateOrUpdateSync(Alert alert);

	}
}