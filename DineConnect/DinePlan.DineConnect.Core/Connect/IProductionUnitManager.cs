﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IProductionUnitManager
    {
        Task<IdentityResult> CreateSync(ProductionUnit productionUnit);
    }
}
