﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Connect.Discount;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class PromotionManager : DineConnectServiceBase, IPromotionManager, ITransientDependency
    {

        private readonly IRepository<Promotion> _promotionRepo;

        public PromotionManager(IRepository<Promotion> promotion)
        {
            _promotionRepo = promotion;
        }

        public async Task<IdentityResult> CreateOrUpdateSync(Promotion promotion)
        {
            //  if the New Addition
            if (promotion.Id == 0)
            {
                if (_promotionRepo.GetAll().Any(a => a.Id.Equals(promotion.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _promotionRepo.InsertAndGetIdAsync(promotion);
                return IdentityResult.Success;

            }
            else
            {
                List<Promotion> lst = _promotionRepo.GetAll().Where(a => a.Id.Equals(promotion.Id) && a.Id != promotion.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}