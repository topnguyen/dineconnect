﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Connect.Entity;
using DinePlan.DineConnect.Connect.Master;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class ConnectTableGroupManager : DineConnectServiceBase, IConnectTableGroupManager, ITransientDependency
    {
        private readonly IRepository<ConnectTableGroup> _tableGroupRepo;
        private readonly IRepository<ConnectTable> _tableRepo;

        public ConnectTableGroupManager(IRepository<ConnectTableGroup> department, IRepository<ConnectTable> tableRepo)
        {
            _tableGroupRepo = department;
            _tableRepo = tableRepo;
        }

        public async Task<IdentityResult> CreateOrUpdateSync(ConnectTableGroup department)
        {
            if (department.Id == 0)
            {
                if (_tableGroupRepo.GetAll().Any(a => a.Name.Equals(department.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _tableGroupRepo.InsertAndGetIdAsync(department);
                return IdentityResult.Success;
            }
            else
            {
                List<ConnectTableGroup> lst = _tableGroupRepo.GetAll().Where(a => a.Name.Equals(department.Name) && a.Id != department.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

        public async Task<IdentityResult> CreateOrUpdateSyncTable(ConnectTable item)
        {
            if (item.Id == 0)
            {
                if (_tableRepo.GetAll().Any(a => a.Name.Equals(item.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                var id = await _tableRepo.InsertAndGetIdAsync(item);
                item.Id = id;
                return IdentityResult.Success;
            }
            else
            {
                List<ConnectTable> lst = _tableRepo.GetAll().Where(a => a.Name.Equals(item.Name) && a.Id != item.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }
    }
}
