﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Connect.Menu;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class ScreenMenuManager : DineConnectServiceBase, IScreenMenuManager, ITransientDependency
    {

        private readonly IRepository<ScreenMenu> _screenMenuRepo;

        public ScreenMenuManager(IRepository<ScreenMenu> screenMenu)
        {
            _screenMenuRepo = screenMenu;
        }

        public async Task<IdentityResult> CreateOrUpdateSync(ScreenMenu screenMenu)
        {
            if (screenMenu.Id == 0)
            {
                if (_screenMenuRepo.GetAll().Any(a => a.Id.Equals(screenMenu.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _screenMenuRepo.InsertAndGetIdAsync(screenMenu);
                return IdentityResult.Success;

            }
            else
            {
                List<ScreenMenu> lst = _screenMenuRepo.GetAll().Where(a => a.Id.Equals(screenMenu.Id) && a.Id != screenMenu.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}