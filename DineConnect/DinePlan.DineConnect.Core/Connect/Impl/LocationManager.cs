﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Connect.Master;
using Abp.UI;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class LocationManager : DineConnectServiceBase, ILocationManager, ITransientDependency
    {

        private readonly IRepository<Location> _locationRepo;

        public LocationManager(IRepository<Location> location)
        {
            _locationRepo = location;
        }

        public async Task<IdentityResult> CreateSync(Location location)
        {
            try
            {
                //  if the New Addition
                if (location.Id == 0)
                {
                    if (_locationRepo.GetAll().Any(a => a.Name.Equals(location.Name)))
                    {
                        string myF = string.Format(L("NameAlreadyExists_f"), location.Name);
                        string[] strArrays = {myF };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }

                    if (_locationRepo.GetAll().Any(a => a.Code.Equals(location.Code)))
                    {
                        string[] strArrays = { L("CodeAlreadyExists") };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }

                    await _locationRepo.InsertAndGetIdAsync(location);
                    return IdentityResult.Success;
                }
                else
                {
                    List<Location> lst = _locationRepo.GetAll().Where(a => a.Name.Equals(location.Name) && a.Id != location.Id).ToList();
                    if (lst.Count > 0)
                    {
                        string[] strArrays = { L("NameAlreadyExists") };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }
                    lst = _locationRepo.GetAll().Where(a => a.Code.Equals(location.Code) && a.Id != location.Id).ToList();
                    if (lst.Count > 0)
                    {
                        string[] strArrays = { L("CodeAlreadyExists") };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }
                    return IdentityResult.Success;
                }

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

}
}
