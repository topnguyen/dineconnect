﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class ProductionUnitManager : DineConnectServiceBase, IProductionUnitManager, ITransientDependency
    {

        private readonly IRepository<ProductionUnit> _productionUnitRepo;

        public ProductionUnitManager(IRepository<ProductionUnit> productionUnit)
        {
            _productionUnitRepo = productionUnit;
        }

        public async Task<IdentityResult> CreateSync(ProductionUnit productionUnit)
        {
            //  if the New Addition
            if (productionUnit.Id == 0)
            {
                if (_productionUnitRepo.GetAll().Any(a => a.Code.Equals(productionUnit.Code)))
                {
                    string[] strArrays = { L("CodeAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                if (_productionUnitRepo.GetAll().Any(a => a.Name.Equals(productionUnit.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }

                await _productionUnitRepo.InsertAndGetIdAsync(productionUnit);
                return IdentityResult.Success;

            }
            else
            {
                List<ProductionUnit> lst = _productionUnitRepo.GetAll().Where(a => a.Code.Equals(productionUnit.Code) && a.Id != productionUnit.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("CodeAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }

                List<ProductionUnit> lst1 = _productionUnitRepo.GetAll().Where(a => a.Name.Equals(productionUnit.Name) && a.Id != productionUnit.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}


