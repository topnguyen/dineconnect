﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Connect.Entity;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class EntityTypeManager : DineConnectServiceBase, IEntityTypeManager, ITransientDependency
    {

        private readonly IRepository<EntityType> _entityTypeRepo;

        public EntityTypeManager(IRepository<EntityType> entityType)
        {
            _entityTypeRepo = entityType;
        }

        public async Task<IdentityResult> CreateSync(EntityType entityType)
        {
            //  if the New Addition
            if (entityType.Id == 0)
            {
                if (_entityTypeRepo.GetAll().Any(a => a.Id.Equals(entityType.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _entityTypeRepo.InsertAndGetIdAsync(entityType);
                return IdentityResult.Success;

            }
            else
            {
                List<EntityType> lst = _entityTypeRepo.GetAll().Where(a => a.Id.Equals(entityType.Id) && a.Id != entityType.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
