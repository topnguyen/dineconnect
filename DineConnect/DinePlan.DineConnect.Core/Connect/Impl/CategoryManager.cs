﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class CategoryManager : DineConnectServiceBase, ICategoryManager, ITransientDependency
    {
        private readonly IRepository<Category> _categoryRepo;

        public CategoryManager(IRepository<Category> category)
        {
            _categoryRepo = category;
        }

        public async Task<IdentityResult> CreateSync(Category category)
        {
            var nameExists = _categoryRepo.GetAll()
                .Where(a => a.Name != null && a.Name.Equals(category.Name))
                .WhereIf(category.Id != 0, a => a.Id != category.Id);

            var codeExists = _categoryRepo.GetAll()
                .Where(a => a.Code != null && a.Code.Equals(category.Code))
                .WhereIf(category.Id != 0, a => a.Id != category.Id);
            //  if the New Addition
            if (nameExists.Any())
            {
                string[] strArrays = { $"{L("Category")} {L("Name")} {category.Name} {L("AlreadyExists")}" };
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }
            if (codeExists.Any())
            {
                string[] strArrays = { $"{L("Category")} {L("Code") } {category.Code} {L("AlreadyExists")}" };
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }

            await _categoryRepo.InsertOrUpdateAndGetIdAsync(category);
            return IdentityResult.Success;
        }
    }
}