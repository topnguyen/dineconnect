﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Connect.MultiLingual;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Impl
{
    
    public class LanguageDescriptionManager : DineConnectServiceBase, ILanguageDescriptionManager, ITransientDependency
    {

        private readonly IRepository<LanguageDescription> _languageDescriptionRepo;

        public LanguageDescriptionManager(
            IRepository<LanguageDescription> languageDescriptionRepo)
        {
            _languageDescriptionRepo = languageDescriptionRepo;
        }

        public async Task<IdentityResult> CreateOrUpdateSync(LanguageDescription languageDescription)
        {
            if (languageDescription.Id == 0)
            {
                if (_languageDescriptionRepo.GetAll().Any(a => a.Id.Equals(languageDescription.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _languageDescriptionRepo.InsertAndGetIdAsync(languageDescription);
                return IdentityResult.Success;

            }
            else
            {
                List<LanguageDescription> lst = _languageDescriptionRepo.GetAll().Where(a => a.Id.Equals(languageDescription.Id) && a.Id != languageDescription.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
