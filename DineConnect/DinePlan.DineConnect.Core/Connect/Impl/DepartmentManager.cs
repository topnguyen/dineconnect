﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class DepartmentManager : DineConnectServiceBase, IDepartmentManager, ITransientDependency
    {
        private readonly IRepository<Master.Department> _departmentRepo;
        private readonly IRepository<DepartmentGroup> _departmentGroupRepo;
        public DepartmentManager(
            IRepository<Master.Department> department,
        IRepository<DepartmentGroup> departmentGroupRepo
            )
        {
            _departmentRepo = department;
            _departmentGroupRepo = departmentGroupRepo;
        }

        public async Task<IdentityResult> CreateOrUpdateSync(Master.Department department)
        {
            
             var codeExists = _departmentRepo.GetAll()
                .Where(a => a.Code != null && a.Code.Equals(department.Code))
                .WhereIf(department.Id != 0, a => a.Id != department.Id);

            var nameExists = _departmentRepo.GetAll()
                .Where(a => a.Name != null && a.Name.Equals(department.Name))
                .WhereIf(department.Id != 0, a => a.Id != department.Id);
            //  if the New Addition
            if (codeExists.Any())
            {
                string[] strArrays = { $"{L("Code") } {department.Code} {L("AlreadyExists")}" };
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }

            if (nameExists.Any())
            {
                string[] strArrays = { $"{L("Name")} {department.Name} {L("AlreadyExists")}" };
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }
           
            await _departmentRepo.InsertOrUpdateAndGetIdAsync(department);
            return IdentityResult.Success;
           
        }
        public async Task<IdentityResult> CreateDepartmentGroupSync(DepartmentGroup departmentGroup)
        {
            var codeExists = _departmentGroupRepo.GetAll()
                .Where(a => a.Code != null && a.Code.Equals(departmentGroup.Code))
                .WhereIf(departmentGroup.Id != 0, a => a.Id != departmentGroup.Id);

            var nameExists = _departmentGroupRepo.GetAll()
                .Where(a => a.Name != null && a.Name.Equals(departmentGroup.Name))
                .WhereIf(departmentGroup.Id != 0, a => a.Id != departmentGroup.Id);
            
            if (codeExists.Any())
            {
                string[] strArrays = { $"{L("Code") } {departmentGroup.Code} {L("AlreadyExists")}" };
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }
            if (nameExists.Any())
            {
                string[] strArrays = { $"{L("Name")} {departmentGroup.Name} {L("AlreadyExists")}" };
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }
            await _departmentGroupRepo.InsertOrUpdateAndGetIdAsync(departmentGroup);
            return IdentityResult.Success;
        }
    }
}
