﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Connect.Master;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class ForeignCurrencyManager : DineConnectServiceBase, IForeignCurrencyManager, ITransientDependency
    {

        private readonly IRepository<ForeignCurrency> _foreignCurrencyRepo;

        public ForeignCurrencyManager(IRepository<ForeignCurrency> foreignCurrency)
        {
            _foreignCurrencyRepo = foreignCurrency;
        }

        public async Task<IdentityResult> CreateSync(ForeignCurrency foreignCurrency)
        {
            //  if the New Addition
            if (foreignCurrency.Id == 0)
            {
                if (_foreignCurrencyRepo.GetAll().Any(a => a.Id.Equals(foreignCurrency.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _foreignCurrencyRepo.InsertAndGetIdAsync(foreignCurrency);
                return IdentityResult.Success;

            }
            else
            {
                List<ForeignCurrency> lst = _foreignCurrencyRepo.GetAll().Where(a => a.Id.Equals(foreignCurrency.Id) && a.Id != foreignCurrency.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
