﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Menu;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class MenuItemManager : DineConnectServiceBase, IMenuItemManager, ITransientDependency
    {
        private readonly IRepository<MenuItem> _miRepo;

        public MenuItemManager(IRepository<MenuItem> miRepo)
        {
            _miRepo = miRepo;
        }

        public async Task<IdentityResult> CreateOrUpdateSync(MenuItem menuItem)
        {
            var allPor = menuItem.Portions.Select(a => a.Name);
            if (allPor.Any())
            {
                var anyDuplicate = allPor.GroupBy(x => x).Any(g => g.Count() > 1);
                if (anyDuplicate)
                {
                    string[] strArrays = {string.Format(L("DuplicatePortionExists_f"), menuItem.Name)};
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
            }

            var codeExists = _miRepo.GetAll()
                .Where(a => a.AliasCode != null && a.AliasCode.Equals(menuItem.AliasCode))
                .WhereIf(menuItem.Id != 0, a => a.Id != menuItem.Id);

            var nameExists = _miRepo.GetAll()
                .Where(a => a.Name != null && a.Name.Equals(menuItem.Name))
                .WhereIf(menuItem.Id != 0, a => a.Id != menuItem.Id);

            if (codeExists.Any())
            {
                string[] strArrays = {$"{L("Code")} {menuItem.AliasCode} {L("AlreadyExists")}"};
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }

            if (nameExists.Any())
            {
                string[] strArrays = {$"{L("Name")} {menuItem.Name} {L("AlreadyExists")}"};
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }

            await _miRepo.InsertOrUpdateAndGetIdAsync(menuItem);
            return IdentityResult.Success;
        }
    }
}