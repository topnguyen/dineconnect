﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Connect.Users;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class DinePlanUserRoleManager : DineConnectServiceBase, IDinePlanUserRoleManager, ITransientDependency
    {

        private readonly IRepository<DinePlanUserRole> _dinePlanUserRoleRepo;

        public DinePlanUserRoleManager(IRepository<DinePlanUserRole> dinePlanUserRole)
        {
            _dinePlanUserRoleRepo = dinePlanUserRole;
        }

        public async Task<IdentityResult> CreateSync(DinePlanUserRole dinePlanUserRole)
        {
            //  if the New Addition
            if (dinePlanUserRole.Id == 0)
            {
                if (_dinePlanUserRoleRepo.GetAll().Any(a => a.Id.Equals(dinePlanUserRole.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _dinePlanUserRoleRepo.InsertAndGetIdAsync(dinePlanUserRole);
                return IdentityResult.Success;

            }
            else
            {
                List<DinePlanUserRole> lst = _dinePlanUserRoleRepo.GetAll().Where(a => a.Id.Equals(dinePlanUserRole.Id) && a.Id != dinePlanUserRole.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}