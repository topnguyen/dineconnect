﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Connect.Master;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class CalculationManager : DineConnectServiceBase, ICalculationManager, ITransientDependency
    {

        private readonly IRepository<Calculation> _calculationRepo;

        public CalculationManager(IRepository<Calculation> calculation)
        {
            _calculationRepo = calculation;
        }

        public async Task<IdentityResult> CreateSync(Calculation calculation)
        {
            //  if the New Addition
            if (calculation.Id == 0)
            {
                if (_calculationRepo.GetAll().Any(a => a.Name.Equals(calculation.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                
                await _calculationRepo.InsertAndGetIdAsync(calculation);
                return IdentityResult.Success;

            }
            else
            {
                List<Calculation> lst = _calculationRepo.GetAll().Where(a => a.Name.Equals(calculation.Name) && a.Id != calculation.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

       
    }
}

