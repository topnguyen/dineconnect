﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Connect.OrderTags;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class OrderTagGroupManager : DineConnectServiceBase, IOrderTagGroupManager, ITransientDependency
    {
        private readonly IRepository<OrderTagGroup> _orderTagGroupRepo;

        public OrderTagGroupManager(IRepository<OrderTagGroup> orderTagGroup)
        {
            _orderTagGroupRepo = orderTagGroup;
        }

        public async Task<IdentityResult> CreateSync(OrderTagGroup orderTagGroup)
        {
            if (orderTagGroup.Id == 0)
            {
                if (_orderTagGroupRepo.GetAll().Any(a => a.Name.Equals(orderTagGroup.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _orderTagGroupRepo.InsertAndGetIdAsync(orderTagGroup);
                return IdentityResult.Success;
            }
            else
            {
                List<OrderTagGroup> lst = _orderTagGroupRepo.GetAll().Where(a => a.Name.Equals(orderTagGroup.Name) && a.Id != orderTagGroup.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }
    }
}