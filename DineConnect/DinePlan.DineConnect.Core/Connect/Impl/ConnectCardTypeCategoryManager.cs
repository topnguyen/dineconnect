﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Card;
using DinePlan.DineConnect.Engage.Gift;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Impl
{

    public class ConnectCardTypeCategoryManager : DineConnectServiceBase, IConnectCardTypeCategoryManager, ITransientDependency
    {
        private readonly IRepository<ConnectCardTypeCategory> _connectcardtypecategoryRepo;

        public ConnectCardTypeCategoryManager(IRepository<ConnectCardTypeCategory> connectcardtypecategoryRepo)
        {
            _connectcardtypecategoryRepo = connectcardtypecategoryRepo;
        }

        public async Task<IdentityResult> CreateSync(ConnectCardTypeCategory connectCardTypeCategory)
        {
            var nameExists = _connectcardtypecategoryRepo.GetAll()
                .Where(a => a.Name != null && a.Name.Equals(connectCardTypeCategory.Name))
                .WhereIf(connectCardTypeCategory.Id != 0, a => a.Id != connectCardTypeCategory.Id);

            var codeExists = _connectcardtypecategoryRepo.GetAll()
                .Where(a => a.Code != null && a.Code.Equals(connectCardTypeCategory.Code))
                .WhereIf(connectCardTypeCategory.Id != 0, a => a.Id != connectCardTypeCategory.Id);
            if (nameExists.Any())
            {
                string[] strArrays = { $"{L("Category")} {L("Name")} {connectCardTypeCategory.Name} {L("AlreadyExists")}" };
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }
            if (codeExists.Any())
            {
                string[] strArrays = { $"{L("Category")} {L("Code") } {connectCardTypeCategory.Code} {L("AlreadyExists")}" };
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }

            await _connectcardtypecategoryRepo.InsertOrUpdateAndGetIdAsync(connectCardTypeCategory);
            return IdentityResult.Success;
        }
    }
}

