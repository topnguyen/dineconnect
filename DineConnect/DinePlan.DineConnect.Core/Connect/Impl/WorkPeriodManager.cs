﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Connect.Period;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class WorkPeriodManager : DineConnectServiceBase, IWorkPeriodManager, ITransientDependency
    {

        private readonly IRepository<Period.WorkPeriod> _workPeriodRepo;

        public WorkPeriodManager(IRepository<Period.WorkPeriod> workPeriod)
        {
            _workPeriodRepo = workPeriod;
        }

        public async Task<IdentityResult> CreateSync(Period.WorkPeriod workPeriod)
        {
            //  if the New Addition
            if (workPeriod.Id == 0)
            {
                if (_workPeriodRepo.GetAll().Any(a => a.Id.Equals(workPeriod.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _workPeriodRepo.InsertAndGetIdAsync(workPeriod);
                return IdentityResult.Success;

            }
            else
            {
                List<Period.WorkPeriod> lst = _workPeriodRepo.GetAll().Where(a => a.Id.Equals(workPeriod.Id) && a.Id != workPeriod.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}