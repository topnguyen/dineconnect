﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class TerminalManager : DineConnectServiceBase, ITerminalManager, ITransientDependency
    {
        private readonly IRepository<Terminal> _terminalRepo;

        public TerminalManager(IRepository<Terminal> terminal)
        {
            _terminalRepo = terminal;
        }

        public async Task<IdentityResult> CreateSync(Terminal terminal)
        {
            var nameExists = _terminalRepo.GetAll()
                .Where(a => a.Name != null && a.Name.Equals(terminal.Name))
                .WhereIf(terminal.Id != 0, a => a.Id != terminal.Id);

            var codeExists = _terminalRepo.GetAll()
                .Where(a => a.TerminalCode != null && a.TerminalCode.Equals(terminal.TerminalCode))
                .WhereIf(terminal.Id != 0, a => a.Id != terminal.Id);
            //  if the New Addition
            if (nameExists.Any())
            {
                string[] strArrays = { $"{L("Terminal")} {L("Name")} {terminal.Name} {L("AlreadyExists")}" };
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }
            if (codeExists.Any())
            {
                string[] strArrays = { $"{L("Terminal")} {L("Code") } {terminal.TerminalCode} {L("AlreadyExists")}" };
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }

            await _terminalRepo.InsertOrUpdateAndGetIdAsync(terminal);
            return IdentityResult.Success;
        }
    }
}
