﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Connect.Master;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class CompanyManager : DineConnectServiceBase, ICompanyManager, ITransientDependency
    {

        private readonly IRepository<Company> _companyRepo;

        public CompanyManager(IRepository<Company> company)
        {
            _companyRepo = company;
        }

        public async Task<IdentityResult> CreateSync(Company company)
        {
            //  if the New Addition
            if (company.Id == 0)
            {
                if (_companyRepo.GetAll().Any(a => a.Name.Equals(company.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                if (_companyRepo.GetAll().Any(a => a.Code.Equals(company.Code)))
                {
                    string[] strArrays = { L("CodeAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _companyRepo.InsertAndGetIdAsync(company);
                return IdentityResult.Success;

            }
            else
            {
                List<Company> lst = _companyRepo.GetAll().Where(a => a.Name.Equals(company.Name) && a.Id != company.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                lst = _companyRepo.GetAll().Where(a => a.Code.Equals(company.Code) && a.Id != company.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("CodeAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

