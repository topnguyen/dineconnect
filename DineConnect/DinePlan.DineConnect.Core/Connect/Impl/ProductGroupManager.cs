﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class ProductGroupManager : DineConnectServiceBase, IProductGroupManager, ITransientDependency
    {
        private readonly IRepository<ProductGroup> _productGroupRepo;

        public ProductGroupManager(IRepository<ProductGroup> productGroup)
        {
            _productGroupRepo = productGroup;
        }

        public async Task<IdentityResult> CreateSync(ProductGroup productGroup)
        {
            var nameExists = _productGroupRepo.GetAll()
                .Where(a => a.Name != null && a.Name.Equals(productGroup.Name))
                .WhereIf(productGroup.Id != 0, a => a.Id != productGroup.Id);

            var codeExists = _productGroupRepo.GetAll()
                .Where(a => a.Code != null && a.Code.Equals(productGroup.Code))
                .WhereIf(productGroup.Id != 0, a => a.Id != productGroup.Id);
            //  if the New Addition
            if (nameExists.Any())
            {
                string[] strArrays = { $"{L("Group")} {L("Name")} {productGroup.Name} {L("AlreadyExists")}" };
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }
            if (codeExists.Any())
            {
                string[] strArrays = { $"{L("Group")} {L("Code") } {productGroup.Code} {L("AlreadyExists")}" };
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }

            await _productGroupRepo.InsertOrUpdateAndGetIdAsync(productGroup);
            return IdentityResult.Success;
        }
    }
}