﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Discount;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class PromotionCategoryManager : DineConnectServiceBase, IPromotionCategoryManager, ITransientDependency
    {
        private readonly IRepository<PromotionCategory> _promotionCategoryRepo;

        public PromotionCategoryManager(IRepository<PromotionCategory> promotionCategoryRepo)
        {
            _promotionCategoryRepo = promotionCategoryRepo;
        }

        public async Task<IdentityResult> CreateOrUpdateSync(PromotionCategory promotionCategory)
        {
            var codeExists = _promotionCategoryRepo.GetAll()
                .Where(a => a.Code != null && a.Code.Equals(promotionCategory.Code))
                .WhereIf(promotionCategory.Id != 0, a => a.Id != promotionCategory.Id);

             
            if (codeExists.Any())
            {
                string[] strArrays = { $"{L("Code") } {promotionCategory.Code} {L("AlreadyExists")}" };
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }
           
            await _promotionCategoryRepo.InsertOrUpdateAndGetIdAsync(promotionCategory);
            return IdentityResult.Success;

        }

    }
}
