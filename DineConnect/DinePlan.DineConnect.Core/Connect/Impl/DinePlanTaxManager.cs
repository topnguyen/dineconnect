﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Connect.Taxes;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class DinePlanTaxManager : DineConnectServiceBase, IDinePlanTaxManager, ITransientDependency
    {

        private readonly IRepository<DinePlanTax> _dinePlanTaxesRepo;
        private readonly IRepository<DinePlanTaxLocation> _dinePlanTaxLocationsRepo;

        public DinePlanTaxManager(IRepository<DinePlanTax> dinePlanTaxes,
            IRepository<DinePlanTaxLocation> dinePlanTaxLocationsRepo
            )
        {
            _dinePlanTaxesRepo = dinePlanTaxes;
            _dinePlanTaxLocationsRepo = dinePlanTaxLocationsRepo;
        }
        
         public async Task<IdentityResult> CreateLocationSync(DinePlanTaxLocation dinePlanTaxlocation)
        {
            //if (dinePlanTaxlocation.Id == 0)
            //{
            //    if (_dinePlanTaxLocationsRepo.GetAll().Any(a => a.DinePlanTaxRefId.Equals(dinePlanTaxlocation.DinePlanTaxRefId) 
            //        && a.LocationRefId == dinePlanTaxlocation.LocationRefId))
            //    {
            //        string[] strArrays = { L("NameAlreadyExists") };
            //        var success = AbpIdentityResult.Failed(strArrays);
            //        return success;
            //    }

            //    List<DinePlanTaxLocation> lst = _dinePlanTaxLocationsRepo.GetAll().
            //                    Where(a => a.DinePlanTaxRefId.Equals(dinePlanTaxlocation.DinePlanTaxRefId)
            //                    && a.LocationRefId == dinePlanTaxlocation.LocationRefId
            //                    && a.TaxPercentage == dinePlanTaxlocation.TaxPercentage).ToList();
            //    if (lst.Count > 0)
            //    {
            //        string[] strArrays = { L("TaxPercentageAlreadyExist") };
            //        var success = AbpIdentityResult.Failed(strArrays);
            //        return success;
            //    }

            //    await _dinePlanTaxLocationsRepo.InsertAndGetIdAsync(dinePlanTaxlocation);
            //    return IdentityResult.Success;

            //}
            //else
            //{
            //    List<DinePlanTaxLocation> lst = _dinePlanTaxLocationsRepo.GetAll().
            //        Where(a => a.DinePlanTaxRefId.Equals(dinePlanTaxlocation.DinePlanTaxRefId) 
            //        && a.LocationRefId == dinePlanTaxlocation.LocationRefId 
            //        && a.TaxPercentage == dinePlanTaxlocation.TaxPercentage 
            //        && a.Id != dinePlanTaxlocation.Id).ToList();

            //    if (lst.Count > 0)
            //    {
            //        string[] strArrays = { L("TaxPercentageAlreadyExist") };
            //        var success = AbpIdentityResult.Failed(strArrays);
            //        return success;
            //    }
            //     lst = _dinePlanTaxLocationsRepo.GetAll().
            //      Where(a => a.DinePlanTaxRefId.Equals(dinePlanTaxlocation.DinePlanTaxRefId)
            //      && a.LocationRefId == dinePlanTaxlocation.LocationRefId
            //      && a.TransactionTypeId == dinePlanTaxlocation.TransactionTypeId
            //      && a.TaxPercentage != dinePlanTaxlocation.TaxPercentage
            //      && a.Id != dinePlanTaxlocation.Id).ToList();
            //    if (lst.Count > 0)
            //    {
            //        string[] strArrays = { L("TransactionTypeMappingAlreadyExists") };
            //        var success = AbpIdentityResult.Failed(strArrays);
            //        return success;
            //    }
            //    return IdentityResult.Success;
            //}

            return IdentityResult.Success;

        }
        public async Task<IdentityResult> CreateSync(DinePlanTax dinePlanTaxes)
        {
            //  if the New Addition
            if (dinePlanTaxes.Id == 0)
            {
                if (_dinePlanTaxesRepo.GetAll().Any(a => a.TaxName.Equals(dinePlanTaxes.TaxName)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _dinePlanTaxesRepo.InsertAndGetIdAsync(dinePlanTaxes);
                return IdentityResult.Success;

            }
            else
            {
                List<DinePlanTax> lst = _dinePlanTaxesRepo.GetAll().Where(a => a.TaxName.Equals(dinePlanTaxes.TaxName) && a.Id != dinePlanTaxes.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
