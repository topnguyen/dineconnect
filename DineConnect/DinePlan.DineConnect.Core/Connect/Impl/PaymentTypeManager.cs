﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Connect.Master;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class PaymentTypeManager : DineConnectServiceBase, IPaymentTypeManager, ITransientDependency
    {

        private readonly IRepository<PaymentType> _paRepo;

        public PaymentTypeManager(IRepository<PaymentType> paRepo)
        {
            _paRepo = paRepo;
        }

     

        public async Task<IdentityResult> CreateOrUpdateSync(PaymentType pType)
        {
            if (pType.Id == 0)
            {
                if (_paRepo.GetAll().Any(a => a.Name.Equals(pType.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _paRepo.InsertAndGetIdAsync(pType);
                return IdentityResult.Success;

            }
            else
            {
                List<PaymentType> lst = _paRepo.GetAll().Where(a => a.Name.Equals(pType.Name) && a.Id != pType.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }
    }
}

