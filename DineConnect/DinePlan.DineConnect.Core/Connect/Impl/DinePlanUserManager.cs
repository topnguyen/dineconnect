﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Users;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect.Impl
{
    public class DinePlanUserManager : DineConnectServiceBase, IDinePlanUserManager, ITransientDependency
    {

        private readonly IRepository<DinePlanUser> _dinePlanUserRepo;

        public DinePlanUserManager(IRepository<DinePlanUser> dinePlanUser)
        {
            _dinePlanUserRepo = dinePlanUser;
        }

        public async Task<IdentityResult> CreateSync(DinePlanUser user)
        {

            if (user.Id == 0)
            {
                if (_dinePlanUserRepo.GetAll().Any(a => a.Name.Equals(user.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                if (_dinePlanUserRepo.GetAll().Any(a => a.Code!=null && a.Code.Equals(user.Code)))
                {
                    string[] strArrays = { L("CodeAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                if (!string.IsNullOrEmpty(user.PinCode))
                {
                    if (_dinePlanUserRepo.GetAll().Any(a => a.PinCode!=null && a.PinCode.Equals(user.PinCode)))
                    {
                        string[] strArrays = { L("PinCodeAlreadyExists") };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }
                    if (!string.IsNullOrEmpty(user.SecurityCode))
                    {
                        if (_dinePlanUserRepo.GetAll().Any(a => a.PinCode != null && a.PinCode.Equals(user.SecurityCode)))
                        {
                            string[] strArrays = {L("PinCodeAlreadyExists")};
                            var success = AbpIdentityResult.Failed(strArrays);
                            return success;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(user.SecurityCode))
                {
                    if (_dinePlanUserRepo.GetAll().Any(a => a.SecurityCode!=null && a.SecurityCode.Equals(user.SecurityCode)))
                    {
                        string[] strArrays = { L("SecurityCodeAlreadyExists") };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }
                    if (!string.IsNullOrEmpty(user.PinCode))
                    {
                        if (_dinePlanUserRepo.GetAll().Any(a => a.SecurityCode != null && a.SecurityCode.Equals(user.PinCode)))
                        {
                            string[] strArrays = {L("SecurityCodeAlreadyExists")};
                            var success = AbpIdentityResult.Failed(strArrays);
                            return success;
                        }
                    }

                }
                await _dinePlanUserRepo.InsertAndGetIdAsync(user);
                return IdentityResult.Success;

            }
            else
            {
                List<DinePlanUser> lst = _dinePlanUserRepo.GetAll().Where(a => a.Name.Equals(user.Name) && a.Id != user.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }


                lst = _dinePlanUserRepo.GetAll().Where(a => a.Code!=null && a.Code.Equals(user.Code) && a.Id != user.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("CodeAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }


                if (!string.IsNullOrEmpty(user.PinCode))
                {
                    if (_dinePlanUserRepo.GetAll().Any(a => a.Id != user.Id && a.PinCode != null && a.PinCode.Equals(user.PinCode)))
                    {
                        string[] strArrays = { L("PinCodeAlreadyExists") };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }
                    if (!string.IsNullOrEmpty(user.SecurityCode))
                    {
                        if (_dinePlanUserRepo.GetAll().Any(a => a.Id != user.Id && a.PinCode != null && a.PinCode.Equals(user.SecurityCode)))
                        {
                            string[] strArrays = { L("PinCodeAlreadyExists") };
                            var success = AbpIdentityResult.Failed(strArrays);
                            return success;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(user.SecurityCode))
                {
                    if (_dinePlanUserRepo.GetAll().Any(a => a.Id != user.Id && a.SecurityCode != null && a.SecurityCode.Equals(user.SecurityCode)))
                    {
                        string[] strArrays = { L("SecurityCodeAlreadyExists") };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }
                    if (!string.IsNullOrEmpty(user.PinCode))
                    {
                        if (_dinePlanUserRepo.GetAll().Any(a => a.Id != user.Id && a.SecurityCode != null && a.SecurityCode.Equals(user.PinCode)))
                        {
                            string[] strArrays = { L("SecurityCodeAlreadyExists") };
                            var success = AbpIdentityResult.Failed(strArrays);
                            return success;
                        }
                    }
                }
                return IdentityResult.Success;
            }


           
        }

    }
}