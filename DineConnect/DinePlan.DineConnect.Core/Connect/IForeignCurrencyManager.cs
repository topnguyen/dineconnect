﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect
{
    public interface IForeignCurrencyManager
    {
        Task<IdentityResult> CreateSync(ForeignCurrency foreignCurrency);
    }
}