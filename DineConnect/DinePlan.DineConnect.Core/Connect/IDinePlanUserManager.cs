﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Users;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect
{
    public interface IDinePlanUserManager
    {
        Task<IdentityResult> CreateSync(DinePlanUser dinePlanUser);
    }
}