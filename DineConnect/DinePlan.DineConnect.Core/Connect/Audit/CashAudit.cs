﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Filter;

namespace DinePlan.DineConnect.Connect.Audit
{
    [Table("CashAudits")]
    public class CashAudit : ConnectMultiTenantEntity
    {

        public int LocalId { get; set; }
        public int LocationId { get; set; }
        public DateTime AuditTime { get; set; }
        public string User { get; set; }
        public string LoginBy { get; set; }
        public string ApprovedBy { get; set; }
        public decimal AuditValue { get; set; }
        public decimal ActualAmount { get; set; }
        public decimal CashSales { get; set; }
        public decimal FloatAmount { get; set; }
        public Guid UniqueGuid { get; set; }
        public decimal CashIn { get; set; }
        public decimal CashOut { get; set; }
        public string Denominations { get; set; }
        public string ReasonId { get; set; }
        public string Remark { get; set; }
        public int WorkPeriodId { get; set; }
        public string TerminalName { get; set; }
        public decimal PettyCash { get; set; }
    }
}
