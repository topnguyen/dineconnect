﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Filter;

namespace DinePlan.DineConnect.Connect.Audit
{
   

    [Table("ExternalLogs")]
    public class ExternalLog : ConnectMultiTenantNoTrackerEntity
    {
        [Column(TypeName = "date")] public virtual DateTime ExternalLogTrunc { get; set; }

        public int AuditType { get; set; }
        public string LogDescription { get; set; }
        public string LogData { get; set; }
        public DateTime LogTime { get; set; }
        




    }

    public enum ExternalLogType
    {
        TicketOutbound=0,
        PromotionOutbound = 1,
        TagOutbound = 2,
        LocationGroupInbound = 3,
        LocationInbound = 4,
        MaterialGroupInbound = 5,
        ComboInbound = 6,
        OrderTagGroupInbound = 7,
        InboundFile = 8,
        MaterialInbound = 9,
        MinioError= 10,
        Xero = 11,
        Grab = 12,
        DineCall = 13,
        SFBIFoodItConsole = 14,
        UrbanPiper = 15,


    }
}
