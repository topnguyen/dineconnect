﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.Connect.Audit
{
    [Table("Alerts")]
    public class Alert :CreationAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }

        public virtual Guid EventId { get; set; }
        public virtual int LocationId { get; set; }
        public AlertType AlertTypeId { get; set; }

        public AlertType[] HighAlerts  = new AlertType[]{AlertType.VoidOrder};

        public DateTime AlertTime { get; set; }
        [Column(TypeName = "date")] public virtual DateTime AlertDate { get; set; }

        public string TicketNumber { get; set; }

        public decimal AlertValue { get; set; }

        public string AlertLog { get; set; }

        public string AlertUser { get; set; }

       
    }

    public enum AlertType
    {
        VoidOrder = 0,
    }

    [Table("DvrDetails")]

    public class DvrDetail :CreationAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        public virtual int LocationId { get; set; }
        public string DvrUrl { get; set; }
        public string DvrPort { get; set; }
        public string DvrUsername { get; set; }
        public string DvrPassword { get; set; }
        public string CameraName { get; set; }
        public DvrType DvrTypeId { get; set; }

    }

    public enum DvrType
    {
        Unknown = 0,
        HikVision = 1,
    }

}
