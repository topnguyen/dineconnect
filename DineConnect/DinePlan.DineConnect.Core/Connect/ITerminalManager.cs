﻿using DinePlan.DineConnect.Connect.Master;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect
{
    public interface ITerminalManager
    {
        Task<IdentityResult> CreateSync(Terminal terminal);
    }
}
