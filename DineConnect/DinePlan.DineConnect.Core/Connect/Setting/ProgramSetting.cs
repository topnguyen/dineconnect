﻿using DinePlan.DineConnect.Filter;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Setting
{
   
    [Table("ProgramSettingTemplates")]
    public class ProgramSettingTemplate : ConnectMultiTenantEntity
    {
        public string Name { get; set; }
        public string DefaultValue { get; set; }
    }

    [Table("ProgramSettingValues")]
    public class ProgramSettingValue : ConnectFullMultiTenantAuditEntity
    {
        [ForeignKey("ProgramSettingTemplateId")]
        public ProgramSettingTemplate ProgramSettingTemplate { get; set; }
        public virtual int ProgramSettingTemplateId { get; set; }

        public virtual string ActualValue { get; set; }
    }
}