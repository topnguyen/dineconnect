﻿using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Filter;

namespace DinePlan.DineConnect.Connect
{
    [Table("TicketTypeConfigurations")]
    public class TicketTypeConfiguration : ConnectMultiTenantEntity
    {
        public string Name { get; set; }
    }


    [Table("TicketTypes")]
    public class TicketType : ConnectFullMultiTenantAuditEntity
    {
        [ForeignKey("InvoiceNumerator")] public int? TicketInvoiceNumeratorId { get; set; }

        [ForeignKey("TicketNumerator")] public int? TicketNumeratorId { get; set; }

        [ForeignKey("OrderNumerator")] public int? OrderNumeratorId { get; set; }

        [ForeignKey("PromotionNumerator")] public int? PromotionNumeratorId { get; set; }

        [ForeignKey("FullTaxNumerator")] 
        public int? FullTaxNumeratorId { get; set; }

        [ForeignKey("QueueNumerator")] public int? QueueNumeratorId { get; set; }

        [ForeignKey("ReturnNumerator")] public int? ReturnNumeratorId { get; set; }

        [ForeignKey("SaleTransactionType")] public int? SaleTransactionType_Id { get; set; }

        [ForeignKey("ScreenMenu")] public int? ScreenMenuId { get; set; }

        public virtual string Name { get; set; }
        public virtual Numerator InvoiceNumerator { get; set; }
        public virtual Numerator TicketNumerator { get; set; }
        public virtual Numerator OrderNumerator { get; set; }
        public virtual Numerator PromotionNumerator { get; set; }
        public virtual Numerator FullTaxNumerator { get; set; }
        public virtual Numerator QueueNumerator { get; set; }
        public virtual Numerator ReturnNumerator { get; set; }

        public virtual TransactionType SaleTransactionType { get; set; }
        public virtual ScreenMenu ScreenMenu { get; set; }
        public virtual int? TicketTypeConfigurationId { get; set; }
        public virtual bool IsTaxIncluded { get; set; }
        public virtual bool IsPreOrder { get; set; }
        public virtual bool IsAllowZeroPriceProducts { get; set; }
    }

    [Table("Numerators")]
    public class Numerator : ConnectMultiTenantEntity
    {
        public Numerator()
        {
            NumberFormat = "#";
            OutputFormat = "[Number]";
        }
        public virtual string Name { get; set; }
        public virtual string NumberFormat { get; set; }
        public virtual string OutputFormat { get; set; }

        public virtual bool ResetNumerator { get; set; }
        public virtual ResetNumeratorType ResetNumeratorType { get; set; }


    }

    public enum ResetNumeratorType
    {
        Daily = 0,
        Monthly = 1,
        Yearly = 2
    }
}