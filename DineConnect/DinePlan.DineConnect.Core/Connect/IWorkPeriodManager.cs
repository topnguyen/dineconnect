﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Period;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect
{
    public interface IWorkPeriodManager
    {
        Task<IdentityResult> CreateSync(Period.WorkPeriod workPeriod);
    }
}
