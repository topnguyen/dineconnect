﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Filter;

namespace DinePlan.DineConnect.Connect.MultiLingual
{
    [Table("LanguageDescriptions")]
    public class LanguageDescription : ConnectMultiTenantEntity
    {
        public virtual string LanguageCode { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public LanguageDescriptionType LanguageDescriptionType { get; set; }
        public int ReferenceId { get; set; }

    }

    public enum LanguageDescriptionType
    {
        ProductGroup = 1,
        Category = 2,
        MenuItem = 3,
        Promotion = 4,
        OrderTagGroup = 5,
        OrderTag = 6,
        TicketTagGroup = 7,
        TicketTag = 8,
        DepartmentName = 9,
        PaymentType = 10,
        Reason = 11,
        Location = 12,
        LocationGroup = 13,
        LocationGroupTag = 14,
        TillAccount = 15,
        ComboGroup = 16,
        LocationBranch = 17,
    }
}
