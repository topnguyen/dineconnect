﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Menu;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect
{
    public interface IScreenMenuManager
    {
        Task<IdentityResult> CreateOrUpdateSync
           (ScreenMenu screenMenu);
    }
}