﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace DinePlan.DineConnect.Connect.Language
{
    public class DinePlanLanguageText : FullAuditedEntity<long>, IMayHaveTenant
    {
        public int? TenantId { get; set; }

        public string LanguageName { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }
    }
}