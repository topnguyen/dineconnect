﻿using DinePlan.DineConnect.Connect.Card;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect
{
    public interface IConnectCardTypeCategoryManager
    {
        Task<IdentityResult> CreateSync(ConnectCardTypeCategory connectCardTypeCategory);
    }
}
