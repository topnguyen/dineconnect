﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Filter;

namespace DinePlan.DineConnect.Connect.Till
{
    public enum TillAccounTypes
    {
        Income=0,
        Expense=1,
        AccuralIncome = 2,
        AccuralExpense = 3
    }

    [Table("TillAccounts")]
    public class TillAccount : ConnectFullMultiTenantAuditEntity
    {
        public virtual string Name { get; set; }
        public virtual int TillAccountTypeId { get; set; }
    }
    [Table("TillTransactions")]
    public class TillTransaction : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int LocalId { get; set; }
        public virtual int WorkPeriodId { get; set; }
        public virtual int LocalWorkPeriodId { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        public virtual int LocationId { get; set; }
        public virtual string Remarks { get; set; }
        public virtual string User { get; set; }
        public virtual DateTime TransactionTime { get; set; }
        public virtual bool Status { get; set; }
        public virtual decimal TotalAmount { get; set; }

        [ForeignKey("TillAccountId")]
        public virtual TillAccount TillAccount { get; set; }
        public virtual int TillAccountId { get; set; }

        public virtual int TenantId { get; set; }
    }

}
