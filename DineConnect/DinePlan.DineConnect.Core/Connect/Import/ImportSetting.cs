﻿using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Filter;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Import
{
    [Table("ImportSettings")]
    public class ImportSetting : ConnectFullMultiTenantAuditEntity
    {
        public virtual string FilePath { get; set; }
        public virtual bool IsImported { get; set; }
    }
}