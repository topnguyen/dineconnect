﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Tag;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect
{
	public interface ICalculationManager
    {
		Task<IdentityResult> CreateSync(Calculation ticketTagGroup);
	}
}
