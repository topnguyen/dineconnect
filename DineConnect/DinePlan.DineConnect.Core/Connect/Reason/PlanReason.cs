﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Filter;

namespace DinePlan.DineConnect.Connect.Reason
{
    [Table("PlanReasons")]
    public class PlanReason : ConnectFullMultiTenantAuditEntity
    {
        public string Reason { get; set; }
        public string AliasReason { get; set; }
        public string ReasonGroup { get; set; }
    }

}
