﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Audit;
using DinePlan.DineConnect.Filter;

namespace DinePlan.DineConnect.Connect.Master
{
    public class Terminal : ConnectFullMultiTenantAuditEntity, IAuditable, IOrganization
    {
        public string TerminalCode { get; set; }
        public string Name { get; set; }
        public string Tid { get; set; }
        public string SerialNumber { get; set; }

        public decimal Float { get; set; }
        public bool AcceptTolerance{ get; set; }

        public decimal Tolerance { get; set; }
        public string Denominations { get; set; }

        public bool IsEndWorkTimeMoneyShown { get; set; }
        public bool IsCollectionTerminal { get; set; }
        public bool WorkTimeTotalInFloat { get; set; }
        public bool IsDefault { get; set; }

        public int Oid { get; set; }

        [ForeignKey("TicketTypeId")]
        public TicketTypeConfiguration TicketType { get; set; }
        public int? TicketTypeId { get; set; }
        public bool AutoLogout { get; set; }

        public virtual bool AutoDayClose { get; set; }
        public virtual string AutoDayCloseSettings { get; set; }

        public string Settings { get; set; }

    }
}