﻿
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace DinePlan.DineConnect.Connect.Master
{
    [Table("Companies")]
    public class Company : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        [MaxLength(10)]
        public virtual string Code { get; set; }

        [Required]
        [MaxLength(60)]
        public virtual string Name { get; set; }

        [MaxLength(100)]
        public virtual string Address1 { get; set; }
        [MaxLength(100)]
        public virtual string Address2 { get; set; }

        [MaxLength(100)]
        public virtual string Address3 { get; set; }

        [MaxLength(100)]
        public virtual string City { get; set; }

        [MaxLength(100)]
        public virtual string State { get; set; }

        [MaxLength(100)]
        public virtual string Country { get; set; }

        [MaxLength(20)]
        public virtual string PostalCode { get; set; }
        
        [MaxLength(20)]
        public virtual string PhoneNumber { get; set; }

        [MaxLength(20)]
        public virtual string FaxNumber { get; set; }

        [MaxLength(20)]
        public virtual string UserSerialNumber { get; set; }

        [MaxLength(30)]
        public virtual string GovtApprovalId { get; set; }
        public virtual Guid? CompanyProfilePictureId { get; set; }

        //public virtual string EmailIdList { get; set; }
        public bool NeedToTreatAnyWorkDayAsHalfDay { get; set; }
        public int? WeekDayTreatAsHalfDay { get; set; }

        public int TenantId { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
    }
}
