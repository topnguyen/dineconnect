﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Filter;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Master
{
    [Table("ProductGroups")]
    public class ProductGroup : ConnectMultiTenantEntity, IOrganization
    {
        [MaxLength(10)]
        public virtual string Code { get; set; }

        [MaxLength(50)]
        public virtual string Name { get; set; }
        
        public int Oid { get; set; }
        public virtual ICollection<Category> Categories { get; set; }

        [ForeignKey("ParentId")]
        public virtual ProductGroup Parent { get; set; }
        public virtual int? ParentId { get; set; }
    }

    [Table("Categories")]
    public class Category : ConnectMultiTenantEntity, IOrganization
    {
        public Category()
        {
            Locations = new HashSet<LocationCategoryShare>();
        }

        [MaxLength(10)]
        public virtual string Code { get; set; }

        [MaxLength(50)]
        public virtual string Name { get; set; }

        public virtual ICollection<MenuItem> MenuItems { get; set; }
        public virtual ICollection<LocationCategoryShare> Locations { get; set; }
        public int Oid { get; set; }

        [ForeignKey("ProductGroupId")]
        public virtual ProductGroup ProductGroup { get; set; }

        public int? ProductGroupId { get; set; }

       
    }

  

    [Table("LocationCategoryShares")]
    public class LocationCategoryShare : CreationAuditedEntity
    {
        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }

        public virtual int LocationId { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        public virtual int CategoryId { get; set; }

        public virtual decimal SharingPercentage { get; set; }
        public virtual bool InclusiveOfTax { get; set; }
    }
}