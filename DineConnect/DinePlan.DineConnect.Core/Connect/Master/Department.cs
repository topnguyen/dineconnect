﻿using System.ComponentModel.DataAnnotations;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Filter;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;

namespace DinePlan.DineConnect.Connect.Master
{
    public class Department : ConnectFullMultiTenantAuditEntity
    {

        [StringLength(100)]
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual string Tag { get; set; }
        [ForeignKey("TagId")]
        public virtual PriceTag PriceTag { get; set; }

        public virtual int? TagId { get; set; }
        public virtual int SortOrder { get; set; }
        public virtual string AddOns { get; set; }


        [ForeignKey("DepartmentGroupId")]

        public DepartmentGroup DepartmentGroup { get; set; }
        public int? DepartmentGroupId { get; set; }

        [ForeignKey("TicketTypeId")]
        public TicketTypeConfiguration TicketType{ get; set; }
        public int? TicketTypeId { get; set; }

    }

    [Table("DepartmentGroups")]
    public class DepartmentGroup : ConnectMultiTenantEntity
    {
        [StringLength(100)]
        public string Code { get; set; }
        public string Name { get; set; }
        public string AddOns { get; set; }
    }
}