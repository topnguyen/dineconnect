﻿using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace DinePlan.DineConnect.Connect.Master
{
    public class TransactionType : FullAuditedEntity, IMustHaveTenant
    {

        [StringLength(50)]

        public virtual string Name { get; set; }
        public int TenantId { get; set; }
        public virtual bool Tax { get; set; }
        public virtual bool Discount { get; set; }

        [StringLength(50)]

        public virtual string AccountCode { get; set; }
        public virtual bool HideOnReport { get; set; }


    }
}