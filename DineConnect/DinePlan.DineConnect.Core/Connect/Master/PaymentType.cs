﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Filter;

namespace DinePlan.DineConnect.Connect.Master
{
   

    [Table("PaymentTypes")]
    public class PaymentType : ConnectFullMultiTenantAuditEntity
    {
        [StringLength(50)] public virtual string AccountCode { get; set; }
        [StringLength(50)] public virtual string Name { get; set; }

        public virtual bool AcceptChange { get; set; }
        public virtual bool NoRefund { get; set; }
        public virtual bool AutoPayment { get; set; }
        public virtual bool DisplayInShift { get; set; }
        public virtual int SortOrder { get; set; }
        public virtual int PaymentProcessor { get; set; }
        public virtual string ProcessorName { get; set; }
        public virtual string Processors { get; set; }
        public virtual string ButtonColor { get; set; }
        public virtual string PaymentGroup { get; set; }
        public string Files { get; set; }
        public virtual Guid? DownloadImage { get; set; }
        public virtual string PaymentTag { get; set; }
        public virtual string Departments { get; set; }
        public virtual string AddOns { get; set; }
        public virtual PaymentTenderType PaymentTenderType { get; set; }

    }

    public enum PaymentTenderType
    {
        NonCash = 0,
        Cash = 1
    }

    public class ForeignCurrency : FullAuditedEntity, IMustHaveTenant
    {
        public string Name { get; set; }
        public string CurrencySymbol { get; set; }
        public decimal ExchangeRate { get; set; }
        public decimal Rounding { get; set; }

        [ForeignKey("PaymentTypeId")] public PaymentType PaymentType { get; set; }

        public int PaymentTypeId { get; set; }
        public int TenantId { get; set; }
    }
}