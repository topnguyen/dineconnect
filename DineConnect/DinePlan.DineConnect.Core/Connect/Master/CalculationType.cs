﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Filter;

namespace DinePlan.DineConnect.Connect.Master
{
    [Table("Calculations")]
    public class Calculation : ConnectFullMultiTenantAuditEntity, IOrganization
    {
        [MaxLength(50)]
        public virtual string Name { get; set; }

        [MaxLength(50)]
        public virtual string ButtonHeader { get; set; }

        public virtual int SortOrder { get; set; }
        public virtual int FontSize { get; set; }
        public virtual int ConfirmationType { get; set; }
        public virtual int CalculationTypeId { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual bool IncludeTax { get; set; }
        public virtual bool DecreaseAmount { get; set; }
        [ForeignKey("TransactionTypeId")]
        public virtual TransactionType TransactionType { get; set; }
        public virtual int TransactionTypeId { get; set; }


        [MaxLength(200)]
        public virtual string Departments { get; set; }
        public virtual int Oid { get; set; }
    }

    public enum CalculationType
    {
        RateFromTicketAmount = 0,
        RateFromPreviousTemplate = 1,
        FixedAmount = 2
    };

    public enum ConfirmationType
    {
        None = 0,
        Confirmation = 1,
        AdminPin = 2
    }
}