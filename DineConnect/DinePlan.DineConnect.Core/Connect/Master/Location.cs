﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Messages;
using DinePlan.DineConnect.House;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Master
{
    [Table("LocationTags")]
    public class LocationTag : FullAuditedEntity, IMustHaveTenant
    {
        public LocationTag()
        {
            Locations = new HashSet<Location>();
        }

        [Required]
        public virtual string Name { get; set; }

        public virtual string Code { get; set; }

        public int TenantId { get; set; }

        [JsonIgnore]
        public virtual ICollection<Location> Locations { get; set; }
    }

    [Table("LocationGroups")]
    public class LocationGroup : FullAuditedEntity, IMustHaveTenant
    {
        public LocationGroup()
        {
            Locations = new HashSet<Location>();
        }

        public LocationGroup(string code, string name)
        {
            Code = code;
            Name = name;
            Locations = new HashSet<Location>();
        }

        [Required]
        public virtual string Name { get; set; }

        public virtual string Code { get; set; }

        public int TenantId { get; set; }
        
        [JsonIgnore]
        public virtual ICollection<Location> Locations { get; set; }
    }

    [Table("Locations")]
    public class Location : FullAuditedEntity, IMustHaveTenant, IMustHaveOrganizationUnit
    {
        public Location()
        {
            LocationGroups = new HashSet<LocationGroup>();
            LocationTags = new HashSet<LocationTag>();
        }
        [Required]
        public virtual string Code { get; set; }
        [Required]
        public virtual string Name { get; set; }

        [Required]
        public int CompanyRefId { get; set; }
        [ForeignKey("CompanyRefId")]
        public Company Company { get; set; }

        public virtual string Address1 { get; set; }
        public virtual string Address2 { get; set; }
        public virtual string Address3 { get; set; }
        public virtual string District { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string Country { get; set; }

        public virtual string PhoneNumber { get; set; }
        public virtual string Fax { get; set; }
        public virtual string Website { get; set; }
        public virtual string Email { get; set; }
        public virtual List<LocationMenuItem> MenuItems { get; set; }
        public virtual List<LocationMenuItemPrice> Prices { get; set; }
        public virtual long OrganizationUnitId { get; set; }
        public virtual bool IsPurchaseAllowed { get; set; }
        public virtual bool IsProductionAllowed { get; set; }
        public virtual int DefaultRequestLocationRefId { get; set; }
        public virtual string RequestedLocations { get; set; }
        public virtual DateTime? HouseTransactionDate { get; set; }
        public bool DoesDayCloseRunInBackGround { get; set; }
        public DateTime? BackGroundStartTime { get; set; }
        public DateTime? BackGrandEndTime { get; set; }
        public virtual int TenantId { get; set; }
        public virtual decimal SharingPercentage { get; set; }
        public virtual bool TaxInclusive { get; set; }
        public virtual bool ChangeTax { get; set; }
        public bool IsProductionUnitAllowed { get; set; }
        public List<ProductionUnit> ProductionUnitList { get; set; }
        public virtual int ExtendedBusinessHours { get; set; }
        public virtual bool IsDayCloseRecursiveUptoDateAllowed { get; set; }
        public virtual ICollection<LocationSchdule> Schedules { get; set; }
        public virtual string ReceiptHeader { get; set; }
        public virtual string ReceiptFooter { get; set; }
        public virtual ICollection<LocationGroup> LocationGroups { get; set; }
        public virtual ICollection<LocationTag> LocationTags { get; set; }
        [JsonIgnore]
        public virtual ICollection<TickMessageReply> TickMessageReplies { get; set; }

        public virtual string ReferenceCode { get; set; }
        public string LocationTaxCode { get; set; }
        public virtual string AddOn { get; set; }
      
        public virtual string PostalCode { get; set; }

        public int? LocationBranchId{ get; set; }

        [ForeignKey("LocationBranchId")]
        public LocationBranch LocationBranch { get; set; }

        public decimal TransferRequestLockHours { get; set; }
        public decimal TransferRequestGraceHours { get; set; }
        public int? AvgPriceLocationRefId { get; set; } // Null means, current location else mentioned locaiton id
        public int? DayCloseSalesStockAdjustmentLocationRefId { get; set; } // Null means, current location else mentioned locaiton id

        public virtual bool QuickDineInActive { get; set; }
        public virtual bool RestrictSync { get; set; }

    }

    public class LocationBranch : CreationAuditedEntity
    {
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual string Address1 { get; set; }
        public virtual string Address2 { get; set; }
        public virtual string Address3 { get; set; }
        public virtual  string District { get; set; }

        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string Country { get; set; }
        public virtual string PostalCode { get; set; }

        public virtual string PhoneNumber { get; set; }
        public virtual string Fax { get; set; }
        public virtual string Website { get; set; }
        public virtual string Email { get; set; }
        public virtual string BranchTaxCode { get; set; }

        public virtual string AddOns { get; set; }

    }

    [Table("LocationSchdules")]
    public class LocationSchdule : CreationAuditedEntity
    {
        [ForeignKey("LocationId")]
        [JsonIgnore]
        public virtual Location Location { get; set; }

        public virtual int LocationId { get; set; }
        public virtual string Name { get; set; }
        public virtual int StartHour { get; set; }
        public virtual int StartMinute { get; set; }
        public virtual int EndHour { get; set; }
        public virtual int EndMinute { get; set; }
    }
}