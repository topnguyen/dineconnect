﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect
{
    public interface IDepartmentManager
    {
        Task<IdentityResult> CreateOrUpdateSync(Master.Department department);
        Task<IdentityResult> CreateDepartmentGroupSync(DepartmentGroup departmentGroup);
    }
}

