﻿using System;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Filter;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.Connect.Master;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.OrderTags
{
    [TrackChanges]
    [Table("OrderTagGroups")]
    public class OrderTagGroup : ConnectFullMultiTenantAuditEntity, IOrganization
    {
        public string Name { get; set; }
        public int MaxSelectedItems { get; set; }
        public int MinSelectedItems { get; set; }
        public int SortOrder { get; set; }
        public bool AddTagPriceToOrderPrice { get; set; }
        public bool SaveFreeTags { get; set; }
        public bool FreeTagging { get; set; }
        public bool TaxFree { get; set; }
        public string Prefix { get; set; }
        public string Departments { get; set; }
        public virtual ICollection<OrderTag> Tags { get; set; }
        public virtual ICollection<OrderTagMap> Maps { get; set; }

        public int Oid { get; set; }

        public string Files { get; set; }
        public virtual Guid DownloadImage { get; set; }
    }

    [TrackChanges]
    [Table("OrderTags")]
    public class OrderTag : CreationAuditedEntity
    {
        public string Name { get; set; }
        public string AlternateName { get; set; }
        public string Tag { get; set; }

        public int SortOrder { get; set; }
        public decimal Price { get; set; }
        public int MaxQuantity { get; set; }

        [ForeignKey("OrderTagGroupId")]
        [JsonIgnore]
        public virtual OrderTagGroup OrderTagGroup { get; set; }

        public virtual int OrderTagGroupId { get; set; }
        public virtual int? MenuItemId { get; set; }

        public string Files { get; set; }
        public virtual Guid DownloadImage { get; set; }
    }

    [TrackChanges]
    [Table("OrderTagLocationPrices")]
    public class OrderTagLocationPrice : CreationAuditedEntity
    {
        [ForeignKey("OrderTagId")]
        [JsonIgnore]
        public OrderTag OrderTag { get; set; }
        public int OrderTagId { get; set; }

        [ForeignKey("LocationId")]
        public Location Location { get; set; }
        public int LocationId { get; set; }
        public decimal Price { get; set; }
    }

    [TrackChanges]
    [Table("OrderTagMaps")]
    public class OrderTagMap : CreationAuditedEntity
    {
        [ForeignKey("OrderTagGroupId")]
        [JsonIgnore]
        public virtual OrderTagGroup OrderTagGroup { get; set; }

        public virtual int OrderTagGroupId { get; set; }
        public virtual int? MenuItemId { get; set; }
        public virtual int? CategoryId { get; set; }
    }
}