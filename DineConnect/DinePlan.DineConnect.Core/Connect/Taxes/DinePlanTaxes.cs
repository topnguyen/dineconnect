﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Display;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Filter;

namespace DinePlan.DineConnect.Connect.Taxes
{
    [Table("DinePlanTaxes")]
    public class DinePlanTax : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        [MaxLength(50)]
        public string TaxName { get; set; }
        public int TenantId { get; set; }
        [ForeignKey("TransactionTypeId")]
        public TransactionType TransactionType { get; set; }
        public int? TransactionTypeId { get; set; }

        public bool FutureData { get; set; }
    }

    [Table("DinePlanTaxLocations")]
    public class DinePlanTaxLocation : ConnectFullMultiTenantAuditEntity
    {
        //TODO: Delete Column
        public int LocationRefId { get; set; }

        [Required]
        public int DinePlanTaxRefId { get; set; }

        [ForeignKey("DinePlanTaxRefId")]
        public DinePlanTax DinePlanTax { get; set; }

        [Required]
        public decimal TaxPercentage { get; set; }
       
        //TODO: Delete Column
        public int? TransactionTypeId { get; set; }

        [StringLength(50)]
        public virtual string Tag { get; set; }

        private ICollection<DinePlanTaxMapping> _dinePlanTaxMappings;

        public virtual ICollection<DinePlanTaxMapping> DinePlanTaxMappings
        {
            get { return _dinePlanTaxMappings; }
            set { _dinePlanTaxMappings = value; }
        }

        public DinePlanTaxLocation()
        {
            _dinePlanTaxMappings = new List<DinePlanTaxMapping>();
        }

    }

        [Table("DinePlanTaxMappings")]
        public class DinePlanTaxMapping : FullAuditedEntity
        {
            public int? DepartmentId { get; set; }

            public int? CategoryId { get; set; }

            public int? MenuItemId { get; set; }

            [ForeignKey("DinePlanTaxLocationId")]
            public DinePlanTaxLocation DinePlanTaxLocation { get; set; }
            public int DinePlanTaxLocationId { get; set; }

            //TODO: Delete Column
            public int DinePlanTaxLocationRefId { get; set; }

    }
}