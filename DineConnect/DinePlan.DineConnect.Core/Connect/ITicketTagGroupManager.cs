﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Tag;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect
{
	public interface ITicketTagGroupManager
	{
		Task<IdentityResult> CreateSync(TicketTagGroup ticketTagGroup);
	    Task DeleteTagAsync(int id);
	}
}
