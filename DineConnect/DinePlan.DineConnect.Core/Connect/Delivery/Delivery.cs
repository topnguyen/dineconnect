﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Engage;

namespace DinePlan.DineConnect.Connect.Delivery
{
    [Table("DeliveryTickets")]
    public class DeliveryTicket : FullAuditedEntity, IMustHaveTenant
    {
        public string TicketNumber { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        public virtual int LocationId { get; set; }
        public virtual DateTime LastUpdateTime { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual string Note { get; set; }
        public virtual int TenantId { get; set; }
        public virtual int SyncId { get; set; }
        public virtual List<DeliveryOrder> Orders { get; set; }
        public virtual DeliveryTicketStatus DeliveryStatus { get; set; }
        public virtual DeliveryTicketType DeliveryTicketType { get; set; }

        [ForeignKey("ConnectMemberId")]
        public virtual ConnectMember ConnectMember { get; set; }
        public virtual int ConnectMemberId { get; set; }
        public virtual string Address { get; set; }
        public virtual string Name { get; set; }
        public virtual string Locality { get; set; }
        public virtual string Deliverer { get; set; }
        public bool Paid { get; set; }
        public string TableNumber { get; set; }
    }

    [Table("DeliveryOrders")]
    public class DeliveryOrder : CreationAuditedEntity
    {
        [ForeignKey("MenuItemPortionId")]
        public virtual MenuItemPortion MenuItemPortion { get; set; }
        public virtual int MenuItemPortionId { get; set; }
        public virtual decimal Price { get; set; }
        public virtual decimal Tax { get; set; }
        public virtual decimal Quantity { get; set; }
        public virtual string Note { get; set; }

        [ForeignKey("DeliveryTicketId")]
        public virtual DeliveryTicket DeliveryTicket { get; set; }
        public virtual int DeliveryTicketId { get; set; }
        public virtual string OrderTags { get; set; }

    }

    public enum DeliveryTicketStatus
    {
        NotAssigned=0,
        Preparing=1,
        Delivering=2,
        Delivered=3,
        Paid=4
    }

    public enum DeliveryTicketType
    {
        Delivery=1,
        PickUp = 2,
        SelfOrdering = 3

    }
}
