﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Discount;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect
{
    public interface IPromotionManager
    {
        Task<IdentityResult> CreateOrUpdateSync(Promotion promotion);
    }
}
