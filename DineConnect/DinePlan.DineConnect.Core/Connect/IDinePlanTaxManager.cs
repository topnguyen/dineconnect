﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Taxes;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect
{
    public interface IDinePlanTaxManager
    {
        Task<IdentityResult> CreateSync(DinePlanTax dinePlanTaxes);

        Task<IdentityResult> CreateLocationSync(DinePlanTaxLocation dineplanTaxLocatins);
    }
}
