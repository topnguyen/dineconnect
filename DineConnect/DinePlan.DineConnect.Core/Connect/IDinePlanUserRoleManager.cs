﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Users;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect
{
    public interface IDinePlanUserRoleManager
    {
        Task<IdentityResult> CreateSync(DinePlanUserRole dinePlanUserRole);
    }
}
