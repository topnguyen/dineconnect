﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Audit;
using DinePlan.DineConnect.Filter;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Card
{
    [Table("ConnectCardTypes")]
    public class ConnectCardType : ConnectMultiTenantEntity, IAuditable
    {

        [MaxLength(30)]
        public string Name { get; set; }
        public bool UseOneTime { get; set; }
        public int Oid { get; set; }
        public int Length { get; set; }
        public virtual DateTime ValidTill { get; set; }


        [ForeignKey("ConnectCardTypeCategoryId")]
        public virtual ConnectCardTypeCategory ConnectCardTypeCategory { get; set; }
        public virtual int? ConnectCardTypeCategoryId { get; set; }

        public string Tag { get; set; }

    }

    [Table("ConnectCardTypeCategories")]
    public class ConnectCardTypeCategory : ConnectMultiTenantEntity
    {

        [MaxLength(30)]
        public string Name { get; set; }
        public string Code { get; set; }

    }

    [Table("ConnectCards")]
    public class ConnectCard : CreationAuditedEntity
    {
        [ForeignKey("ConnectCardTypeId")]
        public virtual ConnectCardType ConnectCardType { get; set; }

        public virtual int ConnectCardTypeId { get; set; }

        [Index("IX_CardNo", IsUnique = true)]
        [StringLength(30)]
        public string CardNo { get; set; }

        public int TenantId { get; set; }
        public bool Redeemed { get; set; }
        public bool Active { get; set; }
        public bool Printed { get; set; }

    }

    [Table("ConnectCardRedemptions")]
    public class ConnectCardRedemption : CreationAuditedEntity
    {
        [ForeignKey("ConnectCardId")]
        public virtual ConnectCard ConnectCard { get; set; }

        public virtual int ConnectCardId { get; set; }

        [MaxLength(50)]
        public string TicketNumber { get; set; }
        public int LocationId { get; set; }
        public DateTime RedemptionDate { get; set; }

        [MaxLength(50)]
        public string RedemptionReferences { get; set; }
    }

    [Table("ImportCardDataDetails")]
    public class ImportCardDataDetail : CreationAuditedEntity, IMustHaveTenant
    {
        public string JobName { get; set; }
        public string FileName { get; set; }
        public string FileLocation { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string OutputJsonDto { get; set; }
        public int TenantId { get; set; }
    }

}