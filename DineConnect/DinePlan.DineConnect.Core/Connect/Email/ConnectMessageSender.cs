﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Net.Mail;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Emailing;
using DinePlan.DineConnect.ShortMessage;

namespace DinePlan.DineConnect.Connect.Email
{
    /// <summary>
    ///     Used to send email to users.
    /// </summary>
    public class ConnectMessageSender : DineConnectServiceBase, IConnectMessageSender, ITransientDependency
    {
        private readonly IEmailSender _emailSender;
        private readonly IEmailTemplateProvider _emailTemplateProvider;
        private readonly IRepository<Location> _lRepo;
        private readonly IShortMessageProvider _messageProvier;
        private readonly IRepository<TicketTransaction> _ticTrRepo;
        private readonly IRepository<TransactionType> _trTypes;

        private readonly SettingManager _settingManager;
        private readonly IRepository<Transaction.Ticket> _tRepo;
        private readonly IRepository<Transaction.Order> _orderRepo;


        public ConnectMessageSender(IEmailTemplateProvider emailTemplateProvider, IShortMessageProvider messageProvier,
            IEmailSender emailSender, SettingManager settingManager,
            IRepository<Transaction.Ticket> tRepo, IRepository<Transaction.Order> orderRepo, IRepository<TransactionType> trTypes, IRepository<Location> lRepo,
            IRepository<Payment> pRepo, IRepository<TicketTransaction> ticTrRepo)
        {
            _emailSender = emailSender;
            _messageProvier = messageProvier;
            _settingManager = settingManager;
            _emailTemplateProvider = emailTemplateProvider;
            _tRepo = tRepo;
            _trTypes =trTypes;
            _lRepo = lRepo;
            _ticTrRepo = ticTrRepo;
            _orderRepo = orderRepo;
        }

        public async Task SendEmailReceipt(string email, string ticketNo,
            int locationId)
        {
            var tick = await _tRepo.FirstOrDefaultAsync(a => a.LocationId == locationId
                                                             && a.TicketNumber.Equals(ticketNo));
          
            if (tick != null)
            {
                var location = await _lRepo.FirstOrDefaultAsync(a => a.Id == locationId);
                var allTax = _trTypes.GetAllList(a => a.Tax).Select(a => a.Id).ToList();
                var tiTra = _ticTrRepo.GetAllList(a => a.TicketId == tick.Id);
                var orders = _orderRepo.GetAllList(a => a.TicketId == tick.Id);

                decimal totalTax = 0M;
                if (tiTra != null && tiTra.Any())
                {
                    totalTax = tiTra.Where(a => allTax.Contains(a.TransactionTypeId)).Sum(tit => tit.Amount);
                }
                if (location != null)
                {
                    var emailTemplate = new StringBuilder(_emailTemplateProvider.GetReceiptTemplate());
                    emailTemplate.Replace("{EMAIL_TITLE}", L("Receipt") + " : " + tick.TicketNumber);
                    emailTemplate.Replace("{EMAIL_SUB_TITLE}", L("Location") + " : " + location.Name);

                    var mailBody = new StringBuilder();
                    mailBody.Append("<br/>");
                    if (totalTax > 0M)
                    {
                        mailBody.Append("TAX  : " + totalTax.ToString("N"));
                        mailBody.Append("<br/>");
                    }
                    mailBody.Append("AMOUNT  : " + tick.TotalAmount);
                    emailTemplate.Replace("{EMAIL_BODY}", mailBody.ToString());
                    StringBuilder builderO = new StringBuilder();

                    if (orders.Any())
                    {
                        builderO.Append(@"<table width='100%' border='1' cellspacing='10' cellpadding='10'>");

                        foreach (var allOr in orders)
                        {
                            builderO.Append(@"<tr>");

                            builderO.Append("<td align='left'>");
                            builderO.Append("<span style='font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #57697e;'>");
                            builderO.Append(allOr.MenuItemName);
                            builderO.Append("</span>");
                            builderO.Append("</td>");


                            builderO.Append("<td align='right'>");
                            builderO.Append("<span style='font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #57697e;'>");
                            builderO.Append(allOr.Quantity);
                            builderO.Append("</span>");
                            builderO.Append("</td>");

                            builderO.Append("<td align='right'>");
                            builderO.Append("<span style='font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #57697e;'>");
                            builderO.Append(allOr.Price);
                            builderO.Append("</span>");
                            builderO.Append("</td>");

                            builderO.Append(@"</tr>");

                        }
                        builderO.Append("</table>");
                    }

                    emailTemplate.Replace(" {ORDER_CONTENT}", builderO.ToString());


                    try
                    {
                        await _emailSender.SendAsync(email, location.Name + " - " +@L("Receipt") + " : " + ticketNo, emailTemplate.ToString());
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }
            }
        }

        public async Task SendMessageReceipt(string numbers, string ticketNo,
            int locationId)
        {
            var tick = await _tRepo.FirstOrDefaultAsync(a => a.LocationId == locationId
                                                             && a.TicketNumber.Equals(ticketNo));
            var allTax = _trTypes.GetAllList(a => a.Tax).Select(a=>a.Id).ToList();
            if (tick != null)
            {
                var tiTra = _ticTrRepo.GetAllList(a=>a.TicketId == tick.Id);
                if (tiTra != null && tiTra.Any())
                {
                    decimal totalTax = tiTra.Where(a => allTax.Contains(a.TransactionTypeId)).Sum(tit => tit.Amount);
                    var messageTemplate = new StringBuilder(_emailTemplateProvider.GetShortMessageTemplate());
                    messageTemplate.Replace("{TICKETNO}", tick.TicketNumber);
                    messageTemplate.Replace("{TOTAL}", tick.TotalAmount.ToString());
                    messageTemplate.Replace("{TAX}", totalTax.ToString("N"));
                    var messType =
                        await _settingManager.GetSettingValueAsync<int>(AppSettings.ShortMessageSettings.MessageType);

                    var messageSetting =
                        await _settingManager.GetSettingValueAsync(AppSettings.ShortMessageSettings.MessageSetting);

                    if (messType > 0 && !string.IsNullOrEmpty(messageSetting))
                    {
                        _messageProvier.SendMessage(messageTemplate.ToString(), numbers, messType, messageSetting);
                    }
                }
            }
        }

        public async Task SendEmail(string emailId, string subject, string content)
        {
            await _emailSender.SendAsync(emailId, subject, content);

        }
    }
}