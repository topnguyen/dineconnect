﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.Connect.Sync
{
    [Table("Syncers")]
    public class Syncer : FullAuditedEntity, IMustHaveTenant
    {
        public virtual string Name { get; set; }
        public virtual Guid SyncerGuid { get; set; }
        public virtual int TenantId { get; set; }
        public virtual ICollection<LocationSyncer> LocationSyncers { get; set; }
    }

    [Table("LocationSyncers")]
    public class LocationSyncer : CreationAuditedEntity
    {
        public virtual Guid SyncerGuid { get; set; }
        public virtual DateTime LastSyncTime { get; set; }

        public int LocationId { get; set; }
        [ForeignKey("LocationId")]
        public Location Location{ get; set; }

        public int Syncer_Id { get; set; }
        [ForeignKey("Syncer_Id")]
        public Syncer Syncer { get; set; }

    }

    public class SyncConsts
    {
        public static string USER = "USER";
        public static string TAX = "TAX";
        public static string MENU = "MENU";
        public static string SCREENMENU = "SCREENMENU";
        public static string PRICETAG = "PRICETAG";
        public static string DEPARTMENT = "DEPARTMENT";
        public static string ORDERTAG = "ORDERTAG";
        public static string PAYMENTTYPE = "PAYMENTTYPE";
        public static string TRANSACTIONTYPE = "TRANSACTIONTYPE";
        public static string PROMOTION = "PROMOTION";
        public static string TABLE = "TABLE";
        public static string TILLACCOUNT = "TILLACCOUNT";
        public static string TICKETTAG = "TICKETTAG";
        public static string CALCULATION = "CALCULATION";
        public static string LOCATION = "LOCATION";
        public static string FORIEGNCURRENCY = "FORIEGNCURRENCY";
        public static string REASON = "REASON";
        public static string TICKMESSAGE = "TICKMESSAGE";
        public static string TERMINAL = "TERMINAL";
        public static string PRINTER = "PRINTER";
        public static string PRINTERTEMPLATE = "PRINTERTEMPLATE";
        public static string PRINTERJOB = "PRINTERJOB";
        public static string PRINTERTEMPLATECONDITION = "PRINTERTEMPLATECONDITION";
        public static string PROGRAMSETTINGS = "PROGRAMSETTINGS";
        public static string LOCALIZATION = "LOCALIZATION";
        public static string TICKETTYPE = "TICKETTYPE";
        public static string DINEDEVICE = "DINEDEVICE";
        public static string POSTDATA = "POSTDATA";
        public static string FUTUREDATEINFORMATIONS = "FUTUREDATEINFORMATIONS";
    }

    [Table("LastSyncs")]
    public class LastSync : CreationAuditedEntity, IMustHaveTenant
    {
        public int LocationId { get; set; }
        [ForeignKey("LocationId")]
        public Location Location { get; set; }
        public int TenantId { get; set; }
        public DateTime LastPush { get; set; }
        public DateTime LastPull { get; set; }
        public string Version { get; set; }
        public string SystemIp { get; set; }
    }

    [Table("ReportBackgrounds")]
    public class ReportBackground : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public long UserId { get; set; }
        public string ReportDescription { get; set; }
        public string ReportName { get; set; }
        public string ReportOutput { get; set; }
        public bool Completed { get; set; }

    }
}
