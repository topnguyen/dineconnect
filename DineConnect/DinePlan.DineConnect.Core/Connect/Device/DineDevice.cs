﻿using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.Audit;
using DinePlan.DineConnect.Filter;

namespace DinePlan.DineConnect.Connect.Device
{
    [Table("DineDevices")]
    public class DineDevice : ConnectFullMultiTenantAuditEntity, IAuditable
    {
        public string Name { get; set; }
        public string Settings { get; set; }
        
        // 0-> DineChef 1->DineQueue
        public int DeviceType { get; set; }
    }

    public enum DineDeviceType
    {
        DineChef = 0,
        DineQueue = 1
    }
}