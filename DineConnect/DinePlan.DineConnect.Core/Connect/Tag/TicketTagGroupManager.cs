﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect.Tag
{
	public class TicketTagGroupManager : DineConnectServiceBase, ITicketTagGroupManager, ITransientDependency
	{

		private readonly IRepository<TicketTagGroup> _ticketTagGroupRepo;
        private readonly IRepository<TicketTag> _tagRepo;

        public TicketTagGroupManager(IRepository<TicketTagGroup> ticketTagGroup, IRepository<TicketTag> tRepo)
		{
			_ticketTagGroupRepo = ticketTagGroup;
            _tagRepo = tRepo;
		}

		public async Task<IdentityResult> CreateSync(TicketTagGroup ticketTagGroup)
		{
			//  if the New Addition
			if (ticketTagGroup.Id == 0)
			{
				if (_ticketTagGroupRepo.GetAll().Any(a => a.Name.Equals(ticketTagGroup.Name)))
				{
					string[] strArrays = { L("NameAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				await _ticketTagGroupRepo.InsertAndGetIdAsync(ticketTagGroup);
				return IdentityResult.Success;

			}
			else
			{
				List<TicketTagGroup> lst = _ticketTagGroupRepo.GetAll().Where(a => a.Name.Equals(ticketTagGroup.Name) && a.Id != ticketTagGroup.Id).ToList();
				if (lst.Count > 0)
				{
					string[] strArrays = { L("NameAlreadyExists") };
					var success = AbpIdentityResult.Failed(strArrays);
					return success;
				}
				return IdentityResult.Success;
			}
		}

	    public async Task DeleteTagAsync(int id)
	    {
	        await _tagRepo.DeleteAsync(id);
	    }
	}
}

