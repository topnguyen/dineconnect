﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Filter;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Tag
{

    public enum TicketTagGroupType
    {
        Alphanumeric = 0,
        Integer = 1,
        Decimal = 2
    }

    [Table("TicketTagGroups")]
    public class TicketTagGroup : ConnectFullMultiTenantAuditEntity, IOrganization
    {
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public string Departments { get; set; }
        public bool SaveFreeTags { get; set; }
        public bool FreeTagging { get; set; }
        public bool ForceValue { get; set; }
        public bool AskBeforeCreatingTicket { get; set; }
        public int DataType { get; set; }
        public int SubTagId { get; set; }
        public bool IsAlphanumeric => DataType == 0;
        public bool IsInteger => DataType == 1;
        public bool IsDecimal => DataType == 2;
        public virtual ICollection<TicketTag> Tags { get; set; }

        public int Oid { get; set; }
    }

    [Table("TicketTags")]
    public class TicketTag : CreationAuditedEntity
    {
        public string Name { get; set; }
        public string AlternateName { get; set; }

        [ForeignKey("TicketTagGroupId")]
        [JsonIgnore]
        public virtual TicketTagGroup TicketTagGroup { get; set; }
        public virtual int TicketTagGroupId { get; set; }
    }
}
