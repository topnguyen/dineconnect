﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Helper;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DinePlan.DineConnect.Filter;

namespace DinePlan.DineConnect.Connect.External
{
    [Table("ExternalDeliveryTickets")]
    public class ExternalDeliveryTicket : FullAuditedEntity, IMustHaveTenant
    {
        private IList<DeliveryContentValue> _deliveryContentValues;

        [Index("IX_ExternalDeliveryTicket", 1, IsUnique = true)]
        public virtual int LocationId { get; set; }

        [Index("IX_ExternalDeliveryTicket", 2, IsUnique = true)]
        [StringLength(30)]
        public virtual string TicketNumber { get; set; }

        [StringLength(2)]
        public virtual string Status { get; set; }
        public virtual bool Claimed { get; set; }
        public virtual string DeliveryContents { get; set; }

        internal IList<DeliveryContentValue> DeliveryContentValues =>
            _deliveryContentValues ??
            (_deliveryContentValues = JsonHelper.Deserialize<List<DeliveryContentValue>>(DeliveryContents));

        public virtual int TenantId { get; set; }

        public void AddDeliveryContentValue(DeliveryContentValue promotionDetailValue)
        {
            DeliveryContentValues.Add(promotionDetailValue);
            DeliveryContents = JsonHelper.Serialize(DeliveryContentValues);
        }

        public List<string> GetDeliveryContent()
        {
            return DeliveryContentValues.Select(c => c.Contents).ToList();
        }
    }
   
    public class DeliveryContentValue
    {
        public string TypeOfContent { get; set; }
        public string Contents { get; set; }
    }

    [Table("ExternalFiles")]
    public class ExternalFile : ConnectMultiTenantEntity
    {
        public string Path { get; set; }
    }
}