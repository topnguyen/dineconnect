﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect
{
    public interface ILocationManager
    {
        Task<IdentityResult> CreateSync(Location location);
    }
}