﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.House;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Menu
{
	[Table("MenuItemPortions")]
	public class MenuItemPortion : FullAuditedEntity
	{
		[StringLength(50)]
		public virtual string Name { get; set; }

		[StringLength(50)]
		public virtual string Barcode { get; set; }

		[StringLength(100)]
		public virtual string AliasName { get; set; }

		[ForeignKey("MenuItemId")]
		[JsonIgnore]
		public virtual MenuItem MenuItem { get; set; }
		public virtual int MenuItemId { get; set; }
		public virtual int MultiPlier { get; set; }
		[DecimalPrecision(18, 3)]
		public virtual decimal Price { get; set; }
		[DecimalPrecision(18, 3)]
		public virtual decimal CostPrice { get; set; }
		public int? PreparationTime { get; set; }

		public virtual ICollection<LocationMenuItemPrice> LocationPrices { get; set; }
		public virtual ICollection<PriceTagDefinition> PriceTags { get; set; }

		public int? NumberOfPax { get; set; }
	}
}