﻿using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Audit;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Taxes;
using DinePlan.DineConnect.Filter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Menu
{
    [Table("MenuItems")]
    public class MenuItem : ConnectFullMultiTenantAuditEntity, IAuditable, IOrganization
    {
        public MenuItem()
        {
            Portions = new List<MenuItemPortion>();
        }

        [StringLength(100)]
        public virtual string Name { get; set; }

        [StringLength(50)]
        public virtual string BarCode { get; set; }

        [StringLength(50)]
        public virtual string AliasCode { get; set; }

        [StringLength(100)]
        public virtual string AliasName { get; set; }

        [StringLength(1000)]
        public virtual string ItemDescription { get; set; }

        public virtual bool ForceQuantity { get; set; }
        public virtual bool ForceChangePrice { get; set; }
        public virtual bool RestrictPromotion { get; set; }
        public virtual bool NoTax { get; set; }

        [ForeignKey("CategoryId")]
        [JsonIgnore]
        public virtual Category Category { get; set; }

        public virtual int? CategoryId { get; set; }

        public virtual ICollection<MenuItemPortion> Portions { get; set; }
        public virtual ICollection<UpMenuItem> UpMenuItems { get; set; }
        public virtual ICollection<MenuItemSchedule> MenuItemSchedules { get; set; }
        public virtual ICollection<MenuItemDescription> MenuItemDescriptions { get; set; }

        public int ProductType { get; set; }

        [StringLength(500)]
        public virtual string Tag { get; set; }

        public virtual ICollection<MenuBarCode> Barcodes { get; set; }

        [ForeignKey("TransactionTypeId")]
        [JsonIgnore]

        public virtual TransactionType TransactionType { get; set; }

        public virtual int? TransactionTypeId { get; set; }
        public string Files { get; set; }

        /*INDIA GST */

        [StringLength(50)]
        public virtual string HsnCode { get; set; }

        public virtual Guid DownloadImage { get; set; }
        public virtual string Uom { get; set; }
        public virtual int Oid { get; set; }
        public virtual int FoodType { get; set; }
        public virtual string AddOns { get; set; }

       
    }

    public enum ProductFoodType
    {
        Veg = 1,
        NonVeg = 2,
        Egg = 3,
        NotSpecified = 4
    }

    [Table("MenuItemDescriptions")]
    public class MenuItemDescription : CreationAuditedEntity
    {
        public virtual string LanguageCode { get; set; }
        public virtual string ProductName { get; set; }
        public virtual string Description { get; set; }

        [ForeignKey("MenuItemId")]
        [JsonIgnore]

        public virtual MenuItem MenuItem { get; set; }

        public virtual int MenuItemId { get; set; }
    }

  

    [Table("MenuItemSchedules")]
    public class MenuItemSchedule : CreationAuditedEntity
    {
        public virtual int StartHour { get; set; }
        public virtual int StartMinute { get; set; }
        public virtual int EndHour { get; set; }
        public virtual int EndMinute { get; set; }
        public virtual string Days { get; set; }

        [ForeignKey("MenuItemId")]
        [JsonIgnore]
        public virtual MenuItem MenuItem { get; set; }

        public virtual int MenuItemId { get; set; }
    }

    [Table("UpMenuItems")]
    public class UpMenuItem : CreationAuditedEntity
    {
        [ForeignKey("MenuItemId")]
        [JsonIgnore]

        public virtual MenuItem MenuItem { get; set; }

        public virtual int MenuItemId { get; set; }
        public virtual int RefMenuItemId { get; set; }
        public virtual bool AddBaseProductPrice { get; set; }
        public virtual decimal Price { get; set; }
        public virtual int ProductType { get; set; }
        public int MinimumQuantity { get; set; }
        public int MaxQty { get; set; }
        public bool AddAuto { get; set; }
        public int AddQuantity { get; set; }
    }

    [TrackChanges]
    [Table("UpMenuItemLocationPrices")]
    public class UpMenuItemLocationPrice : CreationAuditedEntity
    {

        [ForeignKey("UpMenuItemId")]
        [JsonIgnore]

        public UpMenuItem UpMenuItem { get; set; }
        public int UpMenuItemId { get; set; }

        [ForeignKey("LocationId")]
        [JsonIgnore]

        public Location Location { get; set; }
        public int LocationId { get; set; }

        public decimal Price { get; set; }
    }

    [Table("MenuBarCodes")]
    public class MenuBarCode : CreationAuditedEntity
    {
        [StringLength(50)]
        public virtual string Barcode { get; set; }

        [ForeignKey("MenuItemId")]
        [JsonIgnore]

        public virtual MenuItem MenuItem { get; set; }

        public virtual int MenuItemId { get; set; }
    }

    [Table("ProductComboes")]
    public class ProductCombo : CreationAuditedEntity
    {
        private IList<ProductComboGroup> _comboGroups;

        public virtual IList<ProductComboGroup> ComboGroups
        {
            get { return _comboGroups; }
            set { _comboGroups = value; }
        }

        public ProductCombo()
        {
            _comboGroups = new List<ProductComboGroup>();
        }

        public int MenuItemId { get; set; }
        public string Name { get; set; }
        public bool AddPriceToOrderPrice { get; set; }
        public string Sorting { get; set; }

    }

    [Table("ProductComboGroups")]
    public class ProductComboGroup : ConnectMultiTenantEntity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public int SortOrder { get; set; }
        public int Minimum { get; set; }
        public int Maximum { get; set; }
        public int ProductComboId { get; set; }

        private IList<ProductCombo> _productCombos;

        public virtual IList<ProductCombo> ProductCombos
        {
            get { return _productCombos; }
            set { _productCombos = value; }
        }

        private IList<ProductComboItem> _comboItems;

        public virtual IList<ProductComboItem> ComboItems
        {
            get { return _comboItems; }
            set { _comboItems = value; }
        }

        public ProductComboGroup()
        {
            ComboItems = new List<ProductComboItem>();
            ProductCombos = new List<ProductCombo>();
        }
    }

    [Table("ProductComboItems")]
    public class ProductComboItem : CreationAuditedEntity
    {
        public ProductComboItem()
        {
            Count = 1;
        }

        public string Name { get; set; }

        [ForeignKey("ProductComboGroupId")]
        [JsonIgnore]

        public ProductComboGroup ProductComboGroup { get; set; }

        public int ProductComboGroupId { get; set; }

        [ForeignKey("MenuItemId")]
        [JsonIgnore]

        public MenuItem MenuItem { get; set; }

        public int MenuItemId { get; set; }
        public int MenuItemPortionId { get; set; }
        public bool AutoSelect { get; set; }
        public decimal Price { get; set; }
        public int SortOrder { get; set; }
        public int Count { get; set; }
        public bool AddSeperately { get; set; }
        public string GroupTag { get; set; }
        public string ButtonColor { get; set; }

        public string Files { get; set; }
        public virtual Guid DownloadImage { get; set; }
    }

    [Table("ProductComboGroupMappings")]
    public class ProductComboGroupMapping: CreationAuditedEntity
    {
        public ProductComboGroupMapping()
        {
        }

        [ForeignKey("ProductComboGroupId")]
        [JsonIgnore]

        public virtual ProductComboGroup ProductComboGroup { get; set; }

        public virtual int ProductComboGroupId { get; set; }
        public virtual int? MenuItemId { get; set; }
        public virtual int? CategoryId { get; set; }
       
    }


    public enum ProductType
    {
        MenuItem = 1,
        Combo = 2
    }
}