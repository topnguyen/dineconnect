﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Menu
{
    [Table("LocationMenuItems")]
    public class LocationMenuItem : CreationAuditedEntity
    {
        [ForeignKey("MenuItemId")]
        [JsonIgnore]
        public virtual MenuItem MenuItem { get; set; }
        public virtual int MenuItemId { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }

        public virtual int LocationId { get; set; }

    }
}
