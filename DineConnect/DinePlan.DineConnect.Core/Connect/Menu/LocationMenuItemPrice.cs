﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Menu
{
    [Table("LocationMenuItemPrices")]
    public class LocationMenuItemPrice : CreationAuditedEntity
    {

        [ForeignKey("MenuPortionId")]
        [JsonIgnore]

        public virtual MenuItemPortion MenuPortion { get; set; }
        public virtual int MenuPortionId { get; set; }

        [ForeignKey("LocationId")]
        [JsonIgnore]

        public virtual Location Location { get; set; }
        public virtual int LocationId{ get; set; }
        public virtual decimal Price { get; set; }
        
    }
}