﻿using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Audit;
using DinePlan.DineConnect.Filter;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.Connect.Card;

namespace DinePlan.DineConnect.Connect.Menu
{
    [Table("ScreenMenus")]
    public class ScreenMenu : ConnectFullMultiTenantAuditEntity, IAuditable, IOrganization
    {
        public ScreenMenu()
        {
            CategoryColumnWidthRate = 25;
            CategoryColumnCount = 1;
        }

        public virtual string Name { get; set; }
        public virtual int CategoryColumnCount { get; set; }
        public virtual int CategoryColumnWidthRate { get; set; }
        public virtual string Departments { get; set; }
        public virtual ICollection<ScreenMenuCategory> Categories { get; set; }

        public int Oid { get; set; }

        public bool FutureData { get; set; }
    }

    [Table("ScreenMenuCategories")]
    public class ScreenMenuCategory : CreationAuditedEntity
    {
        public ScreenMenuCategory()
        {
            MainButtonHeight = 60;
            MenuItemButtonHeight = 80;
            ColumnCount = 0;
            WrapText = false;
            MainButtonColor = "#78ea86";
            PageCount = 1;
            MainFontSize = 15;
            SubButtonHeight = 60;
            SubButtonRows = 1;
        }

        [ForeignKey("ScreenMenuId")]
        public virtual ScreenMenu ScreenMenu { get; set; }
        public virtual int ScreenMenuId { get; set; }
        public string Name { get; set; }
        public bool MostUsedItemsCategory { get; set; }
        public string ImagePath { get; set; }
        public int MainButtonHeight { get; set; }
        public string MainButtonColor { get; set; }
        public double MainFontSize { get; set; }
        public int ColumnCount { get; set; }
        public int MenuItemButtonHeight { get; set; }
        public int MaxItems { get; set; }
        public int PageCount { get; set; }
        public bool WrapText { get; set; }

        public virtual ICollection<ScreenMenuItem> MenuItems { get; set; }
        public int SortOrder { get; set; }

        public int SubButtonHeight { get; set; }
        public int SubButtonRows { get; set; }
        public string SubButtonColorDef { get; set; }
        public int NumeratorType { get; set; }

        public string Files { get; set; }
        public virtual Guid DownloadImage { get; set; }
        public string ScreenMenuCategorySchedule { get; set; }
    }

    public enum ScreenMenuNumerator
    {
        None = 0, Small = 1, Large = 2
    }

    public enum ScreenMenuItemConfirmationTypes
    {
        None = 0, Confirmation = 1, AdminPin = 2
    }

    [Table("ScreenMenuItems")]
    public class ScreenMenuItem : CreationAuditedEntity
    {
        public ScreenMenuItem()
        {
            FontSize = 20;
            SubMenuTag = "";
            ButtonColor = "#feff72";
        }

        [ForeignKey("ScreenCategoryId")]
        public virtual ScreenMenuCategory ScreenMenuCategory { get; set; }

        public int ConfirmationType { get; set; }
        public bool WeightedItem { get; set; }
        public int ScreenCategoryId { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public bool ShowAliasName { get; set; }
        public bool AutoSelect { get; set; }
        public bool VoucherItem { get; set; }

        [ForeignKey("ConnectCardTypeId")]
        public ConnectCardType ConnectCardType { get; set; }
        public int? ConnectCardTypeId { get; set; }

        public string ButtonColor { get; set; }
        public double FontSize { get; set; }
        public string SubMenuTag { get; set; }
        public string OrderTags { get; set; }
        public string ItemPortion { get; set; }
        [ForeignKey("MenuItemId")]
        public virtual MenuItem MenuItem { get; set; }
        public virtual int MenuItemId { get; set; }
        public int SortOrder { get; set; }
        public string Files { get; set; }
        public virtual Guid DownloadImage { get; set; }
    }

}