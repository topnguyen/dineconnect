﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Filter;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Menu
{
    [Table("PriceTags")]
    public class PriceTag : ConnectFullMultiTenantAuditEntity, IOrganization
    {
        public virtual int Type { get; set; }

        [StringLength(10)]
        public virtual string Name { get; set; }
        public virtual ICollection<PriceTagDefinition> PriceTagDefinitions { get; set; }
        public virtual string Departments { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsNormal => Type == 0;
        public bool IsDate => Type == 1;
        public int Oid { get; set; }

        public bool FutureData { get; set; }
    }

    [Table("PriceTagDefinitions")]
    public class PriceTagDefinition : CreationAuditedEntity
    {
        [ForeignKey("TagId")]
        [JsonIgnore]
        public virtual PriceTag PriceTag { get; set; }
        public virtual int TagId { get; set; }

        [ForeignKey("MenuPortionId")]
        [JsonIgnore]
        public virtual MenuItemPortion MenuPortion { get; set; }
        public virtual int MenuPortionId { get; set; }
        public virtual decimal Price { get; set; }

    }

    [TrackChanges]
    [Table("PriceTagLocationPrices")]
    public class PriceTagLocationPrice : CreationAuditedEntity
    {
        [ForeignKey("PriceTagDefinitionId")]
        public PriceTagDefinition PriceTagDefinition { get; set; }
        public int PriceTagDefinitionId { get; set; }

        [ForeignKey("LocationId")]
        public Location Location { get; set; }
        public int LocationId { get; set; }

        public decimal Price { get; set; }
    }
 
}