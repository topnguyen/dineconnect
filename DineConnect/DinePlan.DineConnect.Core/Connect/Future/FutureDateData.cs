﻿using DinePlan.DineConnect.Audit;
using DinePlan.DineConnect.Filter;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Future
{
    [Table("FutureDateInformations")]
    public class FutureDateInformation : ConnectFullMultiTenantAuditEntity, IAuditable, IOrganization
    {
        public string Name { get; set; }
        public DateTime FutureDate { get; set; }
        public DateTime? FutureDateTo { get; set; }

        public int FutureDateType { get; set; }
        public string Contents { get; set; }
        public string ObjectContents { get; set; }
        public int Oid { get; set; }
        public int  EntityId { get; set; }

        public bool Applied { get; set; }

    }

    public enum FutureDateType
    {
        //MenuItem = 1,
        PriceTag = 2,
        Tax = 3,
        ScreenMenu = 4,
        MenuItemPrice = 5,
    }
    public enum EnumChangeType
    {
        Percentage = 1,   
        Value = 2,
    }
}