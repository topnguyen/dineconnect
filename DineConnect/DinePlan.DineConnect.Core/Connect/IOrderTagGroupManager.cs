﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.OrderTags;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect
{
    public interface IOrderTagGroupManager
    {
        Task<IdentityResult> CreateSync(OrderTagGroup orderTagGroup);
    }
}