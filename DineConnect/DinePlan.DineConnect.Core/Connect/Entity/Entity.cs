﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Filter;

namespace DinePlan.DineConnect.Connect.Entity
{
    [Table("EntityTypes")]
    public class EntityType : FullAuditedEntity, IMustHaveTenant
    {
        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(50)]
        public string EntityName { get; set; }

        [MaxLength(50)]
        public string PrimaryFieldName { get; set; }

        [MaxLength(50)]
        public string PrimaryFieldFormat { get; set; }

        [MaxLength(50)]
        public string DisplayFormat { get; set; }
        public virtual ICollection<EntityCustomField> Fields { get; set; }
        public int TenantId { get; set; }
    }


    [Table("EntityCustomFields")]
    public class EntityCustomField : CreationAuditedEntity
    {

        [MaxLength(50)]
        public string Name { get; set; }
        public int FieldType { get; set; }
        [MaxLength(50)]
        public string EditingFormat { get; set; }
        [MaxLength(50)]
        public string ValueSource { get; set; }
        public bool Hidden { get; set; }

        [ForeignKey("EntityTypeId")]
        public EntityType EntityType { get; set; }

        public virtual int EntityTypeId { get; set; }
    }

    [Table("ConnectEntities")]
    public class ConnectEntity : CreationAuditedEntity
    {
        [MaxLength(50)]
        public string Name { get; set; }

        [ForeignKey("EntityTypeId")]
        public EntityType EntityType { get; set; }

        public virtual int EntityTypeId { get; set; }
        [MaxLength(200)]
        public string CustomData { get; set; }
    }


    public class ConnectTable : FullAuditedEntity, IMustHaveTenant
    {
        [MaxLength(50)]
        public string Name { get; set; }
        public int Pax { get; set; }
        public int TenantId { get; set; }
        public List<ConnectTableGroup> ConnectTableGroups { get; set; }

    }

    public class ConnectTableGroup : ConnectFullMultiTenantAuditEntity
    {
        [MaxLength(50)]
        public string Name { get; set; }
        public List<ConnectTable> ConnectTables { get; set; }
    }
}