﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect
{
    public interface IMenuItemManager
    {
        Task<IdentityResult> CreateOrUpdateSync(MenuItem menuItem);
    }
}

