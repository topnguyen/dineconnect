﻿using DinePlan.DineConnect.Connect.Master;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect
{
    public interface IProductGroupManager
    {
        Task<IdentityResult> CreateSync(ProductGroup productGroup);
    }
}