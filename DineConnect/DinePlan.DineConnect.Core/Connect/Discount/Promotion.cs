﻿using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Audit;
using DinePlan.DineConnect.Filter;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.Connect.Card;

namespace DinePlan.DineConnect.Connect.Discount
{
    public enum PromotionValueTypes
    {
        Percentage = 1,
        Amount = 2
    };
    public enum PromotionStepDiscountTypes
    {
        Quantity = 1,
        Price = 2
    };
    public enum PromotionApplyTypes
    {
        Highest_Price_Item,
        Lowest_Price_Item
    };
    public enum PromotionTypes
    {
        HappyHour = 1,
        FixedDiscountPercentage = 2,
        FixedDiscountValue = 3,
        BuyXItemGetYItemFree = 4,
        BuyXItemGetYatZValue = 5,
        OrderDiscount = 6,
        FreeItems = 7,
        TicketDiscount = 8,
        PaymentOrderDiscount = 9,
        PaymentTicketDiscount = 10,
        BuyXAndYAtZValue = 11,
        StepDiscount = 12

    };

    [Table("PromotionCategories")]
    public class PromotionCategory : ConnectMultiTenantEntity, IAuditable
    {
        [StringLength(10)]
        public virtual string Code { get; set; }
        [StringLength(50)]
        public virtual string Name { get; set; }

        [StringLength(50)]
        public virtual string Tag { get; set; }

    }

    [Table("Promotions")]
    public class Promotion : ConnectFullMultiTenantAuditEntity, IAuditable, IOrganization
    {
        public Promotion()
        {
            CombineAllPro = true;
            CombineProValues = "*";
        }

        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }

        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }

        public virtual Collection<PromotionSchedule> PromotionSchedules { get; set; }
        public virtual Collection<PromotionRestrictItem> PromotionRestrictItems { get; set; }

        public virtual int PromotionTypeId { get; set; }
        public virtual bool Active { get; set; }
        public virtual int SortOrder { get; set; }
        public virtual string Filter { get; set; }
        public int Oid { get; set; }
        public virtual string PromotionContents { get; set; }


        [ForeignKey("ConnectCardTypeId")]
        public virtual ConnectCardType ConnectCardType { get; set; }
        public virtual int? ConnectCardTypeId { get; set; }

        [ForeignKey("PromotionCategoryId")]
        public virtual PromotionCategory PromotionCategory { get; set; }
        public virtual int? PromotionCategoryId { get; set; }
        public virtual bool Limi1PromotionPerItem { get; set; }
        public virtual bool CombineAllPro { get; set; }
        public virtual string CombineProValues { get; set; }
        public virtual bool DisplayPromotion { get; set; }
        public virtual int PromotionPosition { get; set; }

        public virtual bool IgnorePromotionLimitation { get; set; }
        public virtual bool ApplyOffline { get; set; }

    }

    [Table("PromotionRestrictItems")]
    public class PromotionRestrictItem : CreationAuditedEntity
    {
        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }

        public virtual int PromotionId { get; set; }
        public virtual int MenuItemId { get; set; }
    }

    [Table("PromotionSchedules")]
    public class PromotionSchedule : CreationAuditedEntity
    {
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }

        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }

        public virtual int PromotionId { get; set; }
        public virtual int StartHour { get; set; }
        public virtual int StartMinute { get; set; }
        public virtual int EndHour { get; set; }
        public virtual int EndMinute { get; set; }

        public virtual string MonthDays { get; set; }

        [StringLength(100)]
        public virtual string Days { get; set; }
    }

    /*
     * HAPPY HOUR PROMOTION
     */

    [Table("TimerPromotions")]
    public class TimerPromotion : CreationAuditedEntity
    {
        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }

        public virtual int PromotionId { get; set; }
        public virtual int PriceTagId { get; set; }
    }

    /*
    * FIXED DISCOUNT PERCENTAGE,
    * FIXED DISCOUNT VALUE
    */

    [Table("FixedPromotions")]
    public class FixedPromotion : CreationAuditedEntity
    {
        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }

        public virtual int PromotionId { get; set; }
        public virtual int CategoryId { get; set; }
        public virtual int ProductGroupId { get; set; }
        public virtual int MenuItemId { get; set; }
        public virtual int MenuItemPortionId { get; set; }
        public virtual int PromotionValueType { get; set; }
        public virtual decimal PromotionValue { get; set; }
    }

    /*
     * Buy X Item Get Y Item Free
     */

    [Table("FreePromotions")]
    public class FreePromotion : CreationAuditedEntity
    {
        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }

        public virtual int PromotionId { get; set; }
        public virtual bool AllFrom { get; set; }
        public virtual bool AllTo { get; set; }
        public virtual int FromCount { get; set; }
        public virtual int ToCount { get; set; }
        public virtual ICollection<FreePromotionExecution> FreePromotionExecutions { get; set; }
    }

    [Table("FreePromotionExecutions")]
    public class FreePromotionExecution : CreationAuditedEntity
    {
        [ForeignKey("FreePromotionId")]
        public virtual FreePromotion FreePromotion { get; set; }

        public virtual int FreePromotionId { get; set; }
        public virtual int CategoryId { get; set; }
        public virtual int ProductGroupId { get; set; }
        public virtual int MenuItemId { get; set; }
        public virtual string PortionName { get; set; }
        public virtual int MenuItemPortionId { get; set; }
        public virtual bool From { get; set; }
    }

    /*
    * Buy X Item Get Y at Z Value
    */

    [Table("FreeValuePromotions")]
    public class FreeValuePromotion : CreationAuditedEntity
    {
        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }

        public virtual int PromotionId { get; set; }
        public virtual bool AllFrom { get; set; }
        public virtual int FromCount { get; set; }
        public virtual ICollection<FreeValuePromotionExecution> FreeValuePromotionExecutions { get; set; }
    }

    [Table("FreeValuePromotionExecution")]
    public class FreeValuePromotionExecution : CreationAuditedEntity
    {
        [ForeignKey("FreeValuePromotionId")]
        public virtual FreeValuePromotion FreeValuePromotion { get; set; }

        public virtual int FreeValuePromotionId { get; set; }
        public virtual int CategoryId { get; set; }
        public virtual int ProductGroupId { get; set; }
        public virtual int MenuItemId { get; set; }
        public virtual string PortionName { get; set; }
        public virtual int MenuItemPortionId { get; set; }
        public virtual bool From { get; set; }
        public virtual int ValueType { get; set; }
        public virtual decimal Value { get; set; }
    }

    /*
     * Demand Discount
     */

    [Table("DemandPromotions")]
    public class DemandDiscountPromotion : CreationAuditedEntity
    {
        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }

        public virtual int PromotionId { get; set; }
        public virtual int PromotionValueType { get; set; }

        public virtual string ButtonCaption { get; set; }

        [StringLength(10)]
        public virtual string ButtonColor { get; set; }

        public virtual decimal PromotionValue { get; set; }
        public virtual bool PromotionOverride { get; set; }
        public virtual bool AskReference { get; set; }
        public virtual bool AuthenticationRequired { get; set; }
        public virtual bool PromotionApplyOnce { get; set; }
        public virtual int PromotionApplyType { get; set; }
        public virtual string Group { get; set; }


        public virtual ICollection<DemandPromotionExecution> DemandPromotionExecutions { get; set; }
    }

    [Table("DemandPromotionExecutions")]
    public class DemandPromotionExecution : CreationAuditedEntity
    {
        public virtual DemandDiscountPromotion DemandDiscountPromotion { get; set; }
        public virtual int DemandDiscountPromotionId { get; set; }
        public virtual int ProductGroupId { get; set; }
        public virtual int CategoryId { get; set; }
        public virtual int MenuItemId { get; set; }
        public virtual int MenuItemPortionId { get; set; }
    }

    /*
    * Free Item
    */

    [Table("FreeItemPromotions")]
    public class FreeItemPromotion : CreationAuditedEntity
    {
        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }

        public virtual int PromotionId { get; set; }
        public virtual ICollection<FreeItemPromotionExecution> Executions { get; set; }
        public virtual int ItemCount { get; set; }
        public virtual bool Confirmation { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual bool MultipleTimes { get; set; }
    }

    [Table("FreeItemPromotionExecutions")]
    public class FreeItemPromotionExecution : CreationAuditedEntity
    {
        [ForeignKey("FreeItemPromotionId")]
        public virtual FreeItemPromotion FreeItemPromotion { get; set; }

        public virtual int FreeItemPromotionId { get; set; }
        public virtual int MenuItemId { get; set; }
        public virtual string PortionName { get; set; }
        public virtual int MenuItemPortionId { get; set; }
        public virtual decimal Price { get; set; }
    }

    [Table("TicketDiscountPromotions")]
    public class TicketDiscountPromotion : CreationAuditedEntity
    {
        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }

        public virtual int PromotionId { get; set; }
        public virtual int PromotionValueType { get; set; }

        [StringLength(50)]
        public virtual string ButtonCaption { get; set; }

        public virtual decimal PromotionValue { get; set; }
        public virtual bool AuthenticationRequired { get; set; }
        public virtual bool AskReference { get; set; }
        public bool ApplyAsPayment { get; set; }

        public bool PromotionApplyOnce { get; set; }
        public int PromotionApplyType { get; set; }
    }

    [Table("PromotionQuotas")]
    public class PromotionQuota : ConnectFullMultiTenantAuditEntity
    {
        public virtual int PromotionId { get; set; }
        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }
        public virtual int QuotaAmount { get; set; }
        public virtual int QuotaUsed { get; set; }

        public virtual int ResetType { get; set; }

        public virtual int? ResetWeekValue { get; set; }
        public virtual DateTime? ResetMonthValue { get; set; }
        public virtual DateTime? LastResetDate { get; set; }
        public virtual string Plants { get; set; }
        public virtual string LocationQuota { get; set; }
        public bool Each { get; set; }
    }
    public enum ResetType
    {
        Daily = 0,
        Weekly = 1,
        Monthly = 2,
        Yearly = 3
    }
}