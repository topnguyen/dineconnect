﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Entity;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect
{
    public interface IEntityTypeManager
    {
        Task<IdentityResult> CreateSync(EntityType entityType);
    }
}