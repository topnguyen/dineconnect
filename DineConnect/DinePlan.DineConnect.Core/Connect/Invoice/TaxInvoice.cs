﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Filter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Invoice
{
    [Table("FullTaxMembers")]
    public class FullTaxMember : ConnectFullMultiTenantAuditEntity, IMustHaveTenant
    {
        [Index("IX_FullTaxFirstAndSecond", 1, IsUnique = true)]
        [StringLength(20)]
        public virtual string TaxId { get; set; }

        [Index("IX_FullTaxFirstAndSecond", 2, IsUnique = true)]
        [StringLength(20)]
        public virtual string BranchId { get; set; }

        public virtual string Name { get; set; }
        public virtual string Phone { get; set; }
        public virtual string BranchName { get; set; }

        public virtual ICollection<FullTaxAddress> Addresses { get; set; }
    }

    [Table("FullTaxAddresses")]
    public class FullTaxAddress : CreationAuditedEntity
    {
        [ForeignKey("FullMemberId")]
        public virtual FullTaxMember FullTaxMember { get; set; }

        public virtual int FullMemberId { get; set; }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
    }

    [Table("FullTaxInvoices")]
    public class FullTaxInvoice : ConnectFullMultiTenantAuditEntity, IMustHaveTenant
    {
        [ForeignKey("FullMemberId")]
        public virtual FullTaxMember FullTaxMember { get; set; }

        public virtual int FullMemberId { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }

        public virtual int LocationId { get; set; }

        [ForeignKey("TicketId")]
        public virtual Ticket Ticket { get; set; }

        public virtual int TicketId { get; set; }

        public string FullInvoiceNumber { get; set; }
        public string Status { get; set; }
        public int CopyCount { get; set; }
        public string User { get; set; }
        public DateTime PrintTime { get; set; }
        public string PreviousInvoiceNumber { get; set; }
        public string Reason { get; set; }
        public virtual IList<FullTaxInvoiceCopyCount> CopyCounts { get; set; }
    }
    public enum TaxInvoiceStatus
    {
        Void = 0,
        ReIssue = 1,
        ReGenerate = 2,
        RePrint = 3,
        New = 4
    }
    [Table("FullTaxInvoiceCopyCounts")]
    public class FullTaxInvoiceCopyCount : CreationAuditedEntity
    {
        [ForeignKey("FullTaxInvoiceId")]
        public virtual FullTaxInvoice FullTaxInvoice { get; set; }

        public virtual int FullTaxInvoiceId { get; set; }
        public virtual DateTime CopyPrintTime { get; set; }
        public virtual string CopyPrintUser { get; set; }
        public virtual string Reason { get; set; }
        public virtual bool IsCopy { get; set; }
        public virtual string Name { get; set; }
    }
}