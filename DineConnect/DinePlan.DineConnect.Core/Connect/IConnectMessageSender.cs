﻿using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect
{
    public interface IConnectMessageSender
    {
        Task SendEmailReceipt(string emailId, string ticketNo, int locationId);
        Task SendMessageReceipt(string phone, string ticketNo, int locationId);
        Task SendEmail(string emailId, string subject, string content);

    }
}
