﻿using DinePlan.DineConnect.Connect.Discount;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect
{
    public interface IPromotionCategoryManager
    {
        Task<IdentityResult> CreateOrUpdateSync(PromotionCategory promotionCategory);
    }
}
