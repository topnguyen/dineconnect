﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Entity;
using DinePlan.DineConnect.Connect.Master;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect
{
    public interface IConnectTableGroupManager
    {
        Task<IdentityResult> CreateOrUpdateSync(ConnectTableGroup department);
        Task<IdentityResult> CreateOrUpdateSyncTable(ConnectTable item);
    }
}

