﻿using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Filter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Connect.Messages
{
    [Table("TickMessages")]
    public class TickMessage : ConnectFullMultiTenantAuditEntity
    {
        public string MessageHeading { get; set; }
        public string MessageContent { get; set; }
        public TickMessageType MessageType { get; set; }
        public bool Acknowledgement { get; set; }
        public bool OneTimeMessage { get; set; }
        public bool InstantMessage { get; set; }

        public bool Reply { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string TimeIntervals { get; set; }
        public virtual ICollection<TickMessageReply> Replies { get; set; }
    }

    public enum TickMessageType
    {
        Text = 0,
        Image = 1,
        Video = 2,
        PDF = 3,
        Word = 4,
        Excel = 5
    }

    [Table("TickMessageReplies")]
    public class TickMessageReply : CreationAuditedEntity, IHasModificationTime
    {
        public TickMessageReply()
        {
            Acknowledged = true;
        }

        [ForeignKey("TickMessageId")]
        public TickMessage TicketMessage { get; set; }

        public int TickMessageId { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }

        public virtual int LocationId { get; set; }

        public string User { get; set; }
        public string UserId { get; set; }
        public string Reply { get; set; }
        public bool Acknowledged { get; set; }

        [ForeignKey("TerminalId")]
        public Terminal Terminal { get; set; }
        public int? TerminalId { get; set; }
        public DateTime? LastModificationTime { get; set; }
    }
}