﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;

namespace DinePlan.DineConnect.Connect.Transaction
{
      [Table("Tickets")]
    public class Ticket : FullAuditedEntity, IMustHaveTenant
    {
        
        public virtual int TicketId { get; set; }

        [ForeignKey("LocationId")] 
        public virtual Location Location { get; set; }

        [Index("IX_FirstAndSecond", 1, IsUnique = true)]
        public virtual int LocationId { get; set; }

        [Index("IX_FirstAndSecond", 2, IsUnique = true)]
        [StringLength(100)]
        public virtual string TicketNumber { get; set; }

        public virtual string InvoiceNo { get; set; }
        public virtual DateTime TicketCreatedTime { get; set; }
        public virtual DateTime LastUpdateTime { get; set; }
        public virtual DateTime LastOrderTime { get; set; }
        public virtual DateTime LastPaymentTime { get; set; }
        public virtual bool IsClosed { get; set; }
        public virtual bool IsLocked { get; set; }
        public virtual decimal RemainingAmount { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual string DepartmentName { get; set; }
        public virtual string DepartmentGroup { get; set; }
        public virtual string TicketTypeName { get; set; }
        public virtual string Note { get; set; }
        public virtual string LastModifiedUserName { get; set; }
        public virtual string TicketTags { get; set; }
        public virtual string TicketStates { get; set; }
        public virtual string TicketLogs { get; set; }
        public virtual string TicketLog1 { get; set; }
        public virtual bool TaxIncluded { get; set; }
        public virtual string TerminalName { get; set; }
        public virtual bool PreOrder { get; set; }
        public virtual string TicketEntities { get; set; }
        public virtual int WorkPeriodId { get; set; }
        public virtual bool Credit { get; set; }
        
        [Column(TypeName = "date")] 
        public virtual DateTime LastPaymentTimeTruc { get; set; }

        [StringLength(100)] 
        public virtual string ReferenceNumber { get; set; }

        [StringLength(100)] 
        public virtual string QueueNumber { get; set; }

        public virtual bool CreditProcessed { get; set; }
        public virtual string FullTaxInvoiceDetails { get; set; }
        public virtual string TicketPromotionDetails { get; set; }

        public string ReferenceTicket { get; set; }
        public virtual int TenantId { get; set; }

        public virtual bool ExternalProcessed { get; set; }

        public virtual string TicketStatus { get; set; }

        public virtual IList<Order> Orders { get; set; }
        public virtual IList<Payment> Payments { get; set; }
        public virtual IList<TicketTransaction> Transactions { get; set; }

    
      


       
        private IList<PromotionDetailValue> _promotionDetailValues;

        internal IList<PromotionDetailValue> TicketPromotionDetailsList =>
            _promotionDetailValues ?? (_promotionDetailValues = JsonHelper.Deserialize<List<PromotionDetailValue>>(TicketPromotionDetails));
        public IList<PromotionDetailValue> GetTicketPromotionList()
        {
            return TicketPromotionDetailsList;
        }

        private IList<TicketStateValue> _ticketStateValues;
        internal IList<TicketStateValue> TicketStateValues => _ticketStateValues ??
                                                              (_ticketStateValues =
                                                                  JsonHelper.Deserialize<List<TicketStateValue>>(
                                                                      TicketStates));
        public IEnumerable<TicketStateValue> GetTicketStateValues()
        {
            return TicketStateValues;
        }

        private IList<TicketTagValue> _ticketTagValues;
        internal IList<TicketTagValue> TicketTagValues
        {
            get
            {
                if(_ticketTagValues == null)
                {
                    if (string.IsNullOrEmpty(TicketTags))
                        _ticketTagValues = new List<TicketTagValue>();
                    else
                        _ticketTagValues = JsonConvert.DeserializeObject<List<TicketTagValue>>(TicketTags);
                }
                return _ticketTagValues;
            }
        }
      
        private IList<TicketLogValue> _ticketLogValues;
        internal IList<TicketLogValue> TicketLogValues =>
            _ticketLogValues ?? (_ticketLogValues = JsonHelper.Deserialize<List<TicketLogValue>>(TicketLogs));
        public IEnumerable<TicketLogValue> GetTicketLogValues()
        {
            return TicketLogValues;
        }

    
        public bool IsTagged
        {
            get { return TicketTagValues.Any(x => !string.IsNullOrEmpty(x.TagValue)); }
        }
        public decimal GetPlainSum()
        {
            return Orders.Sum(item => item.GetTotal());
        }

        public decimal GetPreTaxServicesTotal()
        {
            var plainSum = GetPlainSum();
            return 0M;
        }

        public decimal GetTaxTotal()
        {
            var result = Orders.Sum(x => x.GetTotalTaxAmount(TaxIncluded, GetPlainSum(), GetPreTaxServicesTotal()));
            return decimal.Round(result, 2, MidpointRounding.AwayFromZero);
        }

      

       
        public bool IsInState(string stateName, string state)
        {
            stateName = stateName.Trim();
            state = state.Trim();
            if (stateName == "*") return TicketStateValues.Any(x => x.State.Equals(state));
            if (string.IsNullOrEmpty(state)) return TicketStateValues.All(x => !x.StateName.Equals(stateName));
            return TicketStateValues.Any(x => x.StateName.Equals(stateName) && x.State.Equals(state));
        }
        public void SetLogs(IList<TicketLogValue> logs)
        {
            TicketLogs = JsonHelper.Serialize(logs);
            _ticketLogValues = null;
        }

        public void AddLog(string userName, string category, string log)
        {
            var lm = new TicketLogValue(TicketNumber, userName) { Category = category, Log = log };
            TicketLogValues.Add(lm);
            SetLogs(TicketLogValues);
        }

        public string GetLog(string category)
        {
            if (!string.IsNullOrEmpty(TicketLogs))
            {
                var myLog = TicketLogValues.LastOrDefault(a => a.Category.Equals(category));
                if (myLog != null) return myLog.Log;
            }

            return null;
        }
        public TicketStateValue GetState(string groupName)
        {
            return TicketStateValues.FirstOrDefault(x => x.State.Equals(groupName)) ?? TicketStateValue.Default;
        }

        public IEnumerable<TicketLogValue> GetLogTicketLogs(string category)
        {
            if (!string.IsNullOrEmpty(TicketLogs))
            {
                var myLog = TicketLogValues.Where(a => a.Category.Equals(category));
                return myLog;
            }

            return null;
        }

        public IEnumerable<TicketTagValue> GetTicketTagValues()
        {
            return TicketTagValues;
        }
        public string GetTicketTagValue(string name)
        {
            try
            {
                if (!string.IsNullOrEmpty(name))
                {
                    var tt = TicketTagValues.FirstOrDefault(a => a.TagName.Equals(name));
                    if (tt != null)
                        return tt.TagValue;
                }

                return "";
            }
            catch
            {
                return "";
            }
            
        }

        public string GetTagData()
        {
            return string.Join("\r",
                TicketTagValues.Where(x => !string.IsNullOrEmpty(x.TagValue))
                    .Select(x => string.Format("{0}: {1}", x.TagName, x.TagValue)));
        }

        public decimal GetIncludedAmount()
        {
            var taxAmout = TotalAmount * 7 / 107;
            return taxAmout;
        }

        public decimal CalculateTax(decimal plainSum, decimal preTaxServices)
        {
            var taxTotal = 0M;
            foreach (var order in Orders.Where(x=>x.CalculatePrice))
            {
                taxTotal += order.GetTotalTaxAmount(TaxIncluded, GetPlainSum(), preTaxServices);
            }
            var myRound = decimal.Round(taxTotal, 2, MidpointRounding.AwayFromZero);
            return myRound;
        }
    }

    [DataContract]
    public class TicketStateValue : IEquatable<TicketStateValue>
    {
        private static TicketStateValue _default;
        public TicketStateValue()
        {
            LastUpdateTime = DateTime.Now;
        }

        [DataMember(Name = "SN")] public string StateName { get; set; }

        [DataMember(Name = "S")] public string State { get; set; }

        [DataMember(Name = "SV", EmitDefaultValue = false)]
        public string StateValue { get; set; }

        [DataMember(Name = "Q", EmitDefaultValue = false)]
        public int Quantity { get; set; }

        [DataMember(Name = "D", IsRequired = false, EmitDefaultValue = false)]
        public DateTime LastUpdateTime { get; set; }

        public static TicketStateValue Default => _default ?? (_default = new TicketStateValue());
        public bool Equals(TicketStateValue other)
        {
            if (other == null) return false;
            return other.StateName == StateName && other.State == State;
        }

        public override int GetHashCode()
        {
            return (StateName + "_" + StateValue).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var item = obj as TicketStateValue;
            return item != null && Equals(item);
        }
    }

    [DataContract]
    public class TicketTagValue : IEquatable<TicketTagValue>
    {
        [DataMember(Name = "TN")] public string TagName { get; set; }

        [DataMember(Name = "TV")] public string TagValue { get; set; }

        [DataMember(Name = "TT")] public int TagType { get; set; }

        public string TagNameShort => string.Join("", TagName.Where(char.IsUpper));

        public bool Equals(TicketTagValue other)
        {
            if (other == null) return false;
            return other.TagName == TagName && other.TagValue == TagValue;
        }

        public override int GetHashCode()
        {
            return (TagName + "_" + TagValue).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var item = obj as TicketTagValue;
            return item != null && Equals(item);
        }
    }

    [DataContract]
    public class TicketLogValue
    {
        public TicketLogValue()
        {
        }

        public TicketLogValue(string ticketNo, string userName)
        {
            DateTime = DateTime.Now;
            TicketNo = ticketNo;
            UserName = userName;
        }

        [DataMember(Name = "N")] public string TicketNo { get; set; }

        [DataMember(Name = "D")] public DateTime DateTime { get; set; }

        [DataMember(Name = "U")] public string UserName { get; set; }

        [DataMember(Name = "C")] public string Category { get; set; }

        [DataMember(Name = "L")] public string Log { get; set; }

        [DataMember(Name = "I")] public Guid Guid { get; set; }
    }
}