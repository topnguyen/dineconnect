﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Helper;

namespace DinePlan.DineConnect.Connect.Transaction
{
    [Table("PreTickets")]
    public class PreTicket : FullAuditedEntity
    {
        private IList<PreContentValue> _preTicketContentValue;

        private IList<PreErrorValue> _preTicketErrorValue;

        [ForeignKey("LocationId")] public virtual Location Location { get; set; }

        [Index("IX_FirstAndSecond", 1, IsUnique = true)]
        public virtual int LocationId { get; set; }

        [Index("IX_FirstAndSecond", 2, IsUnique = true)]
        [StringLength(30)]
        public virtual string TicketNumber { get; set; }

        public virtual string Contents { get; set; }

        public virtual string Errors { get; set; }

        public virtual bool Processed { get; set; }

        internal IList<PreContentValue> PreTicketContentValues => _preTicketContentValue ??
                                                                        (_preTicketContentValue =
                                                                            JsonHelper
                                                                                .Deserialize<
                                                                                    List<PreContentValue>
                                                                                >(
                                                                                    Contents));

        internal IList<PreErrorValue> PreTicketErrorValues => _preTicketErrorValue ??
                                                                    (_preTicketErrorValue =
                                                                        JsonHelper
                                                                            .Deserialize<List<PreErrorValue>>(
                                                                                Errors));

        public void AddPreTicketContent(PreContentValue promotionDetailValue, bool applyStack)
        {
            if (!applyStack) PreTicketContentValues.Clear();
            PreTicketContentValues.Add(promotionDetailValue);
            Contents = JsonHelper.Serialize(PreTicketContentValues);
        }

        public void AddPreTicketContents(List<PreContentValue> preTicketContents)
        {
            PreTicketContentValues.Clear();

            if (preTicketContents.Any())
            {
                foreach (var pdv in preTicketContents) PreTicketContentValues.Add(pdv);
                Contents = JsonHelper.Serialize(PreTicketContentValues);
            }
            else
            {
                Contents = null;
            }
        }

        public void AddPreTicketErrorValue(PreErrorValue promotionDetailValue, bool applyStack)
        {
            if (!applyStack) PreTicketErrorValues.Clear();
            PreTicketErrorValues.Add(promotionDetailValue);
            Errors = JsonHelper.Serialize(PreTicketErrorValues);
        }

        public void AddPreTicketErrorValues(List<PreErrorValue> preTicketErrorValues)
        {
            PreTicketErrorValues.Clear();

            if (preTicketErrorValues.Any())
            {
                foreach (var pdv in preTicketErrorValues) PreTicketErrorValues.Add(pdv);
                Errors = JsonHelper.Serialize(PreTicketErrorValues);
            }
            else
            {
                Errors = null;
            }
        }
    }
}