﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.Connect.Transaction
{
    [Table("TicketTransactions")]
    public class TicketTransaction : CreationAuditedEntity
    {
        [ForeignKey("TransactionTypeId")]
        public virtual TransactionType TransactionType { get; set; }
        public virtual int TransactionTypeId { get; set; }

        [ForeignKey("TicketId")]
        public virtual Ticket Ticket { get; set; }
        public virtual int TicketId { get; set; }

        public decimal Amount { get; set; }
        public string TransactionNote { get; set; }


    }
}