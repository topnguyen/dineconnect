﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Helper;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Transaction
{

    public enum PostContentType
    {
        Ticket = 0,
        Task = 1,
        WorkPeriod = 2,
        TillTransaction = 3,
        Clock = 4,
        Tax = 5
    }

    [Table("PostDatas")]
    public class PostData : FullAuditedEntity, IMustHaveTenant
    {
        private IList<PreContentValue> _preContentValues;

        private List<ErrorValue> _preErrorValues;

        public int? TicketId { get; set; }

        public virtual Location Location { get; set; }

        public virtual int LocationId { get; set; }

        public virtual string Contents { get; set; }

        public virtual string Errors { get; set; }

        public virtual bool Processed { get; set; }

        public virtual int ContentType { get; set; }

        public virtual bool Synced { get; set; }

        public virtual int SyncedId { get; set; }

        public virtual bool IsReProcess { get; set; }

        public virtual int ReFail { get; set; }

        public virtual int RePass { get; set; }

        public  virtual bool ResultReProcess { get; set; }

        public virtual int ReProcessCount { get; set; }

        internal IList<PreContentValue> PreContentValues => _preContentValues ??
                                                                        (_preContentValues =
                                                                            JsonHelper
                                                                                .Deserialize<
                                                                                    List<PreContentValue>
                                                                                >(
                                                                                    Contents));

        internal List<ErrorValue> PreErrorValues => _preErrorValues ?? (_preErrorValues = string.IsNullOrWhiteSpace(Errors) ? new List<ErrorValue>() :
                                                                        JsonConvert.DeserializeObject<List<ErrorValue>>(Errors));

        public void AddPreContent(PreContentValue preContentValue, bool applyStack)
        {
            if (!applyStack) PreContentValues.Clear();
            PreContentValues.Add(preContentValue);
            Contents = JsonHelper.Serialize(PreContentValues);
        }

        public void AddPreContents(List<PreContentValue> preContents)
        {
            PreContentValues.Clear();

            if (preContents.Any())
            {
                foreach (var pdv in preContents) PreContentValues.Add(pdv);
                Contents = JsonHelper.Serialize(PreContentValues);
            }
            else
            {
                Contents = null;
            }
        }

        public void AddErrorContent(ErrorValue preErrorValue, bool applyStack)
        {
            if (!applyStack) PreErrorValues.Clear();
            PreErrorValues.Add(preErrorValue);
            Errors = JsonConvert.SerializeObject(PreErrorValues);
        }

        public void AddErrorContents(List<ErrorValue> preErrorValues)
        {
            PreErrorValues.Clear();

            if (preErrorValues.Any())
            {
                foreach (var pdv in preErrorValues) PreErrorValues.Add(pdv);
                Errors = JsonHelper.Serialize(PreErrorValues);
            }
            else
            {
                Errors = null;
            }
        }

        public int TenantId { get; set; }
        public int Tried { get; set; }
    }

    public class ErrorValue
    {
        public string Contents { get; set; }

        public DateTime ErrorDate { get; set; }
    }

    [DataContract]
    public class PreContentValue : IEquatable<PreContentValue>
    {
        public string Contents { get; set; }

        public bool Equals(PreContentValue obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PreContentValue)obj);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((PreContentValue)obj);
        }

        public override int GetHashCode()
        {
            return GetHashCode();
        }
    }


    [DataContract]
    public class PreErrorValue : IEquatable<PreErrorValue>
    {
        public string Contents { get; set; }

        public DateTime ErrorDate { get; set; }

        public bool Equals(PreErrorValue obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PreErrorValue)obj);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((PreErrorValue)obj);
        }

        public override int GetHashCode()
        {
            return GetHashCode();
        }
    }
}