﻿using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;

namespace DinePlan.DineConnect.Connect.Transaction
{
    [Table("Orders")]
    public class Order : CreationAuditedEntity
    {
        private IList<OrderTagValue> _orderTagValues;
        private IList<TaxValue> _taxValues;
        private IList<PromotionDetailValue> _promoDetails;
        public virtual int OrderId { get; set; }

        [JsonIgnore]
        [ForeignKey("TicketId")]
        public virtual Ticket Ticket { get; set; }

        [ForeignKey("Location_Id")] public virtual Location Location { get; set; }

        public virtual int Location_Id { get; set; }

        public virtual int TicketId { get; set; }

        public virtual int MenuItemType { get; set; }
        public virtual string OrderRef { get; set; }
        public virtual bool IsParent { get; set; }
        public virtual bool IsUpSelling { get; set; }
        public virtual string UpSellingOrderRef { get; set; }

        [StringLength(50)] public virtual string DepartmentName { get; set; }

        [ForeignKey("MenuItemId")] public virtual MenuItem MenuItem { get; set; }

        public virtual int MenuItemId { get; set; }

        [StringLength(100)] public virtual string MenuItemName { get; set; }

        [StringLength(50)] public virtual string PortionName { get; set; }

        public virtual decimal Price { get; set; }
        public virtual decimal CostPrice { get; set; }
        public virtual decimal Quantity { get; set; }
        public virtual int PortionCount { get; set; }
        public virtual string Note { get; set; }
        public virtual bool Locked { get; set; }
        public virtual bool CalculatePrice { get; set; }
        public virtual bool IncreaseInventory { get; set; }
        public virtual bool DecreaseInventory { get; set; }
        public virtual string OrderNumber { get; set; }

        [StringLength(50)] public virtual string CreatingUserName { get; set; }

        public virtual DateTime OrderCreatedTime { get; set; }

        [StringLength(50)] public virtual string PriceTag { get; set; }

        public virtual string Taxes { get; set; }
        public virtual string OrderTags { get; set; }
        public virtual string OrderStates { get; set; }
        public virtual string OrderStatus { get; set; }

        public virtual bool IsPromotionOrder { get; set; }
        public virtual decimal PromotionAmount { get; set; }
        public virtual int PromotionSyncId { get; set; }

        [ForeignKey("MenuItemPortionId")]
        public virtual MenuItemPortion MenuItemPortion { get; set; }

        public virtual int MenuItemPortionId { get; set; }
        public virtual IList<TransactionOrderTag> TransactionOrderTags { get; set; }

        public virtual string OrderPromotionDetails { get; set; }
        public virtual string OrderLog { get; set; }

        public string BindState { get; set; }

        public string BindStateValues { get; set; }

        public bool BindCompleted { get; set; }

        public int NumberOfPax { get; set; }

        [NotMapped]
        public IList<PromotionDetailValue> OrderPromotionDetailsList =>
            _promoDetails ?? (_promoDetails = JsonHelper.Deserialize<List<PromotionDetailValue>>(OrderPromotionDetails));
        public IList<PromotionDetailValue> GetOrderPromotionList()
        {
            return OrderPromotionDetailsList;
        }

        internal IList<TaxValue> TaxValues =>
            _taxValues ?? (_taxValues = JsonHelper.Deserialize<List<TaxValue>>(Taxes));

        [NotMapped]
        public IList<OrderTagValue> OrderTagValues =>
            _orderTagValues ?? (_orderTagValues = JsonHelper.Deserialize<List<OrderTagValue>>(OrderTags));

        private IList<OrderStateValue> _orderStateValues;

        internal IList<OrderStateValue> OrderStateValues
        {
            get
            {
                return _orderStateValues ??
                       (_orderStateValues = JsonHelper.Deserialize<List<OrderStateValue>>(OrderStates));
            }
        }

        public IList<OrderStateValue> GetOrderStateValues()
        {
            return OrderStateValues;
        }

        public decimal GetTaxablePrice()
        {
            var result = Price + OrderTagValues.Where(x => !x.TaxFree && !x.AddTagPriceToOrderPrice).Sum(x => x.Price * x.Quantity);
            return result;
        }
        public decimal GetTaxableOriginalPrice()
        {
            var result = OriginalPrice + OrderTagValues.Where(x => !x.TaxFree && !x.AddTagPriceToOrderPrice).Sum(x => x.Price * x.Quantity);
            return result;
        }
        public decimal GetTaxAmount(bool taxIncluded, decimal plainSum, decimal preTaxServices)
        {
            if (!CalculatePrice)
                return 0;

            var myReturnTax = 0M;
            foreach (var myTax in TaxValues)
            {
                var retuTax = myTax.GetTaxAmount(taxIncluded, GetTaxablePrice(), TaxValues.Sum(y => y.TaxRate),
                    plainSum,
                    preTaxServices);

                myReturnTax += decimal.Round(retuTax, 2, MidpointRounding.AwayFromZero);
            }

            return myReturnTax;
        }

        public decimal GetTotal()
        {
            if (CalculatePrice)
            {
                return GetValue();
            }
            return 0;
        }
        public decimal GetOriginalPriceTotal()
        {
            if (CalculatePrice)
            {
                return GetValue();
            }
            return 0;
        }
        public decimal GetValue()
        {
            var totalValue = GetPrice() * Quantity;
            if (Quantity < 0) 
                totalValue = totalValue * -1;
            return totalValue;
        }
        public decimal GetOriginalValue()
        {
            return GetTaxableOriginalPrice() * Quantity;
        }
        public decimal GetPrice()
        {
            var result = Price + OrderTagValues.Sum(x => x.Price * x.Quantity);
            return result;
        }

        public decimal GetTotalTaxAmount(bool taxIncluded, decimal plainSum, decimal preTaxServices)
        {
            if (!CalculatePrice)
                return 0;
            var myReturnTax = 0M;
            foreach (var myTax in TaxValues)
            {
                var retuTax = myTax.GetTaxAmount(taxIncluded, GetTaxablePrice(), TaxValues.Sum(y => y.TaxRate),
                                  plainSum,
                                  preTaxServices) * Quantity;

                myReturnTax += retuTax;
            }

            return myReturnTax;
        }

        public bool IsInState(string stateName, string state, string stateValue)
        {
            state = state.Trim();
            stateName = stateName.Trim();
            stateValue = stateValue.Trim();
            return OrderStateValues.Any(x => x.StateName == stateName && x.State == state && x.StateValue == stateValue);
        }

        public bool IsInState(string stateName, string state)
        {
            state = state.Trim();
            stateName = stateName.Trim();
            if (stateName == "*") return OrderStateValues.Any(x => x.State == state);
            if (string.IsNullOrEmpty(state)) return OrderStateValues.All(x => x.StateName != stateName);
            return OrderStateValues.Any(x => x.StateName == stateName && x.State == state);
        }

        public decimal GetTaxTotal()
        {
            decimal taxPrice = 0M;

            if (!TaxValues.Any())
            {
                return 0M;
            }
            else
            {
                foreach (var tax in TaxValues)
                {
                    if (Ticket.TaxIncluded)
                    {
                        var tRate = 100 + tax.TaxRate;
                        var taxVale = (GetTaxablePrice() * tRate / 100) - GetTaxablePrice();
                        taxPrice += taxVale;
                    }
                    else
                    {
                        taxPrice += (GetTaxablePrice() * tax.TaxRate / 100);
                    }
                }
            }
            return taxPrice;
        }

        public string GetPortionDesc()
        {
            if (PortionCount > 1
                && !string.IsNullOrEmpty(PortionName)
                && !string.IsNullOrEmpty(PortionName.Trim('\b', ' ', '\t'))
                && PortionName.ToLower() != "normal")
                return "." + PortionName;
            return "";
        }

        public string Description
        {
            get
            {
                var desc = MenuItemName + GetPortionDesc();
                //if (SelectedQuantity > 0 && SelectedQuantity != Quantity)
                //    desc = $"({SelectedQuantity:#.##}) {desc}";
                return desc;
            }
        }

        public decimal OriginalPrice { get; set; }
    }

    [DataContract]
    public class OrderTagValue : IEquatable<OrderTagValue>
    {
        private static OrderTagValue _empty;
        private string _shortName;

        public OrderTagValue()
        {
            TagValue = "";
        }

        [DataMember(Name = "AP", EmitDefaultValue = false)]
        public bool AddTagPriceToOrderPrice { get; set; }

        public static OrderTagValue Empty
        {
            get
            {
                var orderTagValue = _empty;
                if (orderTagValue == null)
                {
                    var orderTagValue1 = new OrderTagValue
                    {
                        TagValue = "",
                        OrderKey = ""
                    };
                    orderTagValue = orderTagValue1;
                    _empty = orderTagValue;
                }

                return orderTagValue;
            }
        }

        [DataMember(Name = "FT", EmitDefaultValue = false)]
        public bool FreeTag { get; set; }

        [DataMember(Name = "MI", EmitDefaultValue = false)]
        public int MenuItemId { get; set; }

        [DataMember(Name = "OK")] public string OrderKey { get; set; }

        [DataMember(Name = "OI")] public int OrderTagGroupId { get; set; }

        [DataMember(Name = "PN", EmitDefaultValue = false)]
        public string PortionName { get; set; }

        [DataMember(Name = "PR", EmitDefaultValue = false)]
        public decimal Price { get; set; }

        [DataMember(Name = "Q", EmitDefaultValue = false)]
        public decimal Quantity { get; set; }

        public string ShortName
        {
            get
            {
                var str = _shortName;
                if (str == null)
                {
                    var num = ToShort(TagValue);
                    var str1 = num;
                    _shortName = num;
                    str = str1;
                }

                return str;
            }
        }

        [DataMember(Name = "TN")] public string TagName { get; set; }

        [DataMember(Name = "TO", EmitDefaultValue = false)]
        public string TagNote { get; set; }

        [DataMember(Name = "TV")] public string TagValue { get; set; }

        [DataMember(Name = "TF", EmitDefaultValue = false)]
        public bool TaxFree { get; set; }

        [DataMember(Name = "UI")] public int UserId { get; set; }

        [DataMember(Name = "GID")] public int GroupSync { get; set; }

        [DataMember(Name = "TID")] public int TagSync { get; set; }
        [DataMember(Name = "PSI")] public int PromotionSyncId { get; set; }

        [DataMember(Name = "ID")] public int Id { get; set; }

        //[DataMember(Name = "AA")] public decimal AppliedAmount { get; set; }

        public bool Equals(OrderTagValue other)
        {
            if (other == null) return false;
            if (other.TagName != TagName || !(other.TagValue == TagValue) || !(other.Price == Price) ||
                !(other.OrderKey == OrderKey)) return false;
            return other.Quantity == Quantity;
        }

        public override bool Equals(object obj)
        {
            var orderTagValue = obj as OrderTagValue;
            if (orderTagValue == null) return false;
            return Equals(orderTagValue);
        }

        public override int GetHashCode()
        {
            object[] tagName = { TagName, "_", TagValue, "_", Price, "_", OrderKey, "_", Quantity };
            return string.Concat(tagName).GetHashCode();
        }

        public int GetSortOrder()
        {
            int num;
            try
            {
                if (OrderKey.Length != 6)
                {
                    num = Convert.ToInt32(OrderKey);
                }
                else
                {
                    var str = OrderKey.Substring(3);
                    num = Convert.ToInt32(str);
                }
            }
            catch (Exception)
            {
                num = 0;
            }

            return num;
        }

        public bool Identifies(string tagExp)
        {
            if (!tagExp.Contains("=")) return TagName == tagExp;
            return string.Concat(TagName, "=", TagValue) == tagExp;
        }

        private string ToShort(string name)
        {
            if (string.IsNullOrEmpty(name)) return "";
            if (TagValue.Length < 3) return name;
            if (!name.Contains(" ")) return TagValue.Substring(0, 2);
            char[] chrArray = { ' ' };
            return string.Join("", name.Split(chrArray).Select(x =>
            {
                if (char.IsNumber(x.ElementAt(0))) return x;
                return x.ElementAt(0).ToString();
            }));
        }

        public void UpdatePrice(decimal orderTagPrice)
        {
            Price = orderTagPrice;
        }
    }

    [DataContract]
    public class TaxValue
    {
        [DataMember(Name = "TR")] public decimal TaxRate { get; set; }

        [DataMember(Name = "RN")] public int Rounding { get; set; }

        [DataMember(Name = "TN")] public string TaxTemplateName { get; set; }

        [DataMember(Name = "TT")] public int TaxTemplateId { get; set; }

        [DataMember(Name = "AT")] public int TaxTempleteAccountTransactionTypeId { get; set; }

        public decimal GetTax(bool taxIncluded, decimal price, decimal totalRate)
        {
            decimal result;
            if (taxIncluded && totalRate > 0)
            {
                if (Rounding > 0)
                    result = decimal.Round(price * TaxRate / (100 + totalRate), Rounding,
                        MidpointRounding.AwayFromZero);
                else
                    result = price * TaxRate / (100 + totalRate);
            }
            else if (TaxRate > 0)
            {
                result = price * TaxRate / 100;
            }
            else
            {
                result = 0;
            }

            return result;
        }

        public decimal GetTaxAmount(bool taxIncluded, decimal price, decimal totalRate, decimal plainSum,
            decimal preTaxServices)
        {
            if (preTaxServices != 0 && plainSum > 0)
                price += (price * preTaxServices) / plainSum;

            var result = GetTax(taxIncluded, price, totalRate);
            return result;
        }
    }

    [Table("TransactionOrderTags")]
    public class TransactionOrderTag : CreationAuditedEntity
    {
        [ForeignKey("OrderId")] public virtual Order Order { get; set; }

        public virtual int OrderId { get; set; }

        public int OrderTagGroupId { get; set; }
        public int OrderTagId { get; set; }
        public int MenuItemPortionId { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
        public string TagName { get; set; }
        public string TagNote { get; set; }
        public string TagValue { get; set; }
        public bool TaxFree { get; set; }
        public bool AddTagPriceToOrderPrice { get; set; }
    }

    [DataContract]
    public class OrderStateValue : IEquatable<OrderStateValue>
    {
        private static OrderStateValue _default;

        public OrderStateValue()
        {
            LastUpdateTime = DateTime.Now;
        }

        [DataMember(Name = "SN")]
        public string StateName { get; set; }

        [DataMember(Name = "S")]
        public string State { get; set; }

        [DataMember(Name = "SV")]
        public string StateValue { get; set; }

        [DataMember(Name = "OK", EmitDefaultValue = false)]
        public string OrderKey { get; set; }

        [DataMember(Name = "D", IsRequired = false, EmitDefaultValue = false)]
        public DateTime LastUpdateTime { get; set; }

        [DataMember(Name = "U", IsRequired = false, EmitDefaultValue = false)]
        public int UserId { get; set; }

        [DataMember(Name = "UN", IsRequired = false, EmitDefaultValue = false)]
        public string UserName { get; set; }

        public bool Equals(OrderStateValue other)
        {
            if (other == null) return false;
            return other.State == State && other.StateName == StateName && other.StateValue == StateValue;
        }

        public override int GetHashCode()
        {
            return (StateName + "_" + StateValue).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var item = obj as OrderStateValue;
            return item != null && Equals(item);
        }
    }
}