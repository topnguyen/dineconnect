﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Connect;
using DinePlan.DineConnect.Connect.Master;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Core.Connect.Location
{
    public class LocationGroupManager : DineConnectServiceBase, ILocationGroupManager, ITransientDependency
    {
        private readonly IRepository<LocationGroup> _locationGroupRepo;

        public LocationGroupManager(IRepository<LocationGroup> locationGroup)
        {
            _locationGroupRepo = locationGroup;
        }

        public async Task<IdentityResult> CreateSync(LocationGroup locationGroup)
        {
            //  if the New Addition
            if (locationGroup.Id == 0)
            {
                if (_locationGroupRepo.GetAll().Any(a => a.Name.Equals(locationGroup.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }

                if (_locationGroupRepo.GetAll().Any(a => a.Code.Equals(locationGroup.Code)))
                {
                    string[] strArrays = { L("CodeAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }

                await _locationGroupRepo.InsertAndGetIdAsync(locationGroup);
                return IdentityResult.Success;
            }
            else
            {
                List<LocationGroup> lst = _locationGroupRepo.GetAll().Where(a => a.Name.Equals(locationGroup.Name) && a.Id != locationGroup.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                if (_locationGroupRepo.GetAll().Any(a => a.Code.Equals(locationGroup.Code) && a.Id != locationGroup.Id))
                {
                    string[] strArrays = { L("CodeAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }
    }
}