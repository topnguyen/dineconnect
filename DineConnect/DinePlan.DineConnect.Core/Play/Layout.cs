﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Filter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play
{
    [Table("PlayLayouts")]
    public class Layout: FullAuditedEntity, IMustHaveTenant
    {
        public const int MaxNameLength = 100;
        public const int MaxDescLength = 255;

        [Required]
        [MaxLength(MaxNameLength)]
        public virtual string Name { get; set; }
        [ForeignKey("ResolutionId")]
        public virtual Resolution Resolution { get; set; }
        public virtual int ResolutionId { get; set; }
        [MaxLength(MaxDescLength)]
        public virtual string Description { get; set; }
        [Required]
        public virtual int TemplateId { get; set; }
        public virtual int TenantId { get; set; }
        public virtual ICollection<LayoutContent> Contents { get; set; }
    }
}
