﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play
{
    [Table("PlayScheduledEvents")]
    public class ScheduledEvent : FullAuditedEntity, IMustHaveTenant
    {
        public const int MaxNameLength = 100;
        public int TenantId { get; set; }
        [MaxLength(MaxNameLength)]
        [Required]
        public virtual string Name { get; set; }
        public virtual int? DisplayId { get; set; }
        [ForeignKey("DisplayId")]
        public virtual Display Display { get; set; }
        public virtual int? DisplayGroupId { get; set; }
        [ForeignKey("DisplayGroupId")]
        public virtual DisplayGroup DisplayGroup { get; set; }
        public virtual int? DaypartId { get; set; }
        [ForeignKey("DaypartId")]
        public virtual Daypart Daypart { get; set; }
        public virtual DateTime StartTime { get; set; }
        public virtual DateTime? EndTime { get; set; }
        [Required]
        public virtual int? LayoutId { get; set; }
        [ForeignKey("LayoutId")]
        public virtual Layout Layout { get; set; }
        public virtual int Priority { get; set; }
        public virtual string RepeatType { get; set; }
        public virtual int? RepeatInterval { get; set; }
        public virtual string RepeatTimeUnit { get; set; }
        public virtual string RepeatWeekdays { get; set; }
        public virtual DateTime? RepeatUntil { get; set; }
        public virtual int? ParentId { get; set; }
    }
}
