﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play
{
    [Table("PlayLayoutContents")]
    public class LayoutContent : FullAuditedEntity, IMustHaveTenant
    {
        public const int MaxFileNameLength = 255;
        public const int MaxSysFileNameLength = 255;
        public const int MaxFileTypeLength = 50;

        public virtual int TenantId { get; set; }
        public virtual int LayoutId { get; set; }
        [ForeignKey("LayoutId")]
        public virtual Layout Layout { get; set; }
        [Required]
        public virtual int RegionNo { get; set; }
        [MaxLength(MaxFileNameLength)]
        public virtual string FileName { get; set; }
        [Required]
        [MaxLength(MaxSysFileNameLength)]
        public virtual string SystemFileName { get; set; }
        [MaxLength(MaxFileTypeLength)]
        public virtual string FileType { get; set; }
        public virtual int DisplayLength { get; set; }
        public virtual bool Stretched { get; set; }
        public bool HasFile { get; set; }
    }
}
