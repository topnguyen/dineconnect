﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play
{
    [Table("PlayDaypartExceptions")]
    public class DaypartException: FullAuditedEntity, IMustHaveTenant
    {
        public const int MaxDayLength = 10;

        [MaxLength(MaxDayLength)]
        [Required]
        public virtual string Day { get; set; }
        [Required]
        public virtual int StartHour { get; set; }
        [Required]
        public virtual int StartMinute { get; set; }
        [Required]
        public virtual int EndHour { get; set; }
        [Required]
        public virtual int EndMinute { get; set; }
        public int TenantId { get; set; }
        [ForeignKey("DaypartId")]
        public Daypart Daypart { get; set; }
        public virtual int DaypartId { get; set; }
    }
}
