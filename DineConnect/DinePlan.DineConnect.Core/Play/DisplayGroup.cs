﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play
{
    [Table("PlayDisplayGroups")]
    public class DisplayGroup : FullAuditedEntity, IMustHaveTenant
    {
        public DisplayGroup()
        {
            Displays = new HashSet<Display>();
        }
        public const int CodeMaxLength = 30;
        public const int NameMaxLength = 100;
        public int TenantId { get; set; }
        [Required]
        [MaxLength(CodeMaxLength)]
        public virtual string Code { get; set; }
        [MaxLength(NameMaxLength)]
        public virtual string Name { get; set; }
        public virtual ICollection<Display> Displays { get; set; }
    }
}
