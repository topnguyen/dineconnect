﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play
{
    [Table("PlayDisplays")]
    public class Display : FullAuditedEntity, IMustHaveTenant
    {
        public Display()
        {
            DisplayGroups = new HashSet<DisplayGroup>();
        }

        public const int NameMaxLength = 40;
        public const int MacAddressMaxLength = 17;
        public int TenantId { get; set; }
        [Required]
        [MaxLength(NameMaxLength)]
        public virtual string Name { get; set; }
        [ForeignKey("ResolutionId")]
        public virtual Resolution Resolution { get; set; }
        public virtual int ResolutionId { get; set; }
        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        public virtual int LocationId { get; set; }
        [MaxLength(MacAddressMaxLength)]
        public virtual string MacAddress { get; set; }
        [Required]
        public virtual bool Active { get; set; }
        [Required]
        public virtual bool Approved { get; set; }
        public ICollection<DisplayGroup> DisplayGroups { get; set; }
        public virtual string RegistrationKey { get; set; }

    }
}
