﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play
{
    [Table("PlayDayparts")]
    public class Daypart : FullAuditedEntity, IMustHaveTenant
    {
        public const int MaxNameLength = 100;

        public int TenantId { get; set; }
        [MaxLength(MaxNameLength)]
        [Required]
        public virtual string Name { get; set; }
        [Required]
        public virtual int StartHour { get; set; }
        [Required]
        public virtual int StartMinute { get; set; }
        [Required]
        public virtual int EndHour { get; set; }
        [Required]
        public virtual int EndMinute { get; set; }
        public List<DaypartException> Exceptions { get; set; }

    }
}
