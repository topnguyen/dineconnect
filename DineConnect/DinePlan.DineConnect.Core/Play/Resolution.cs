﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Filter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play
{
    [Table("PlayResolutions")]
    public class Resolution: FullAuditedEntity, IMustHaveTenant
    {
        public const int MaxNameLength = 100;

        [Required]
        [MaxLength(MaxNameLength)]
        public virtual string Name { get; set; }
        [Required]
        public virtual int Width { get; set; }
        [Required]
        public virtual int Height { get; set; }
        [Required]
        public virtual bool Enabled { get; set; }
        public virtual int TenantId { get; set; }
    }
}
