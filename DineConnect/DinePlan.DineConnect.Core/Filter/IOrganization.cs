﻿namespace DinePlan.DineConnect.Filter
{
    public interface IOrganization
    {
        int Oid { get; set; }
    }
}
