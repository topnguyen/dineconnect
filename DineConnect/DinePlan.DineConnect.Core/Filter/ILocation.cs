﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Helper;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Filter
{
    [TrackChanges]
    public class ConnectFullMultiTenantAuditEntity : FullAuditedEntity, IMustHaveTenant
    {
        public virtual string Locations { get; set; }
        public virtual string NonLocations { get; set; }
        public virtual bool Group { get; set; }
        public virtual bool LocationTag { get; set; }
        public virtual int TenantId { get; set; }

        private IEnumerable<SerializeLocationDto> _locationExtracts;

        private IEnumerable<SerializeLocationDto> LocationExtracts =>
            _locationExtracts ?? (_locationExtracts = JsonHelper.Deserialize<List<SerializeLocationDto>>(Locations));
        public IEnumerable<SerializeLocationDto> GetLocationExtracts()
        {
            return LocationExtracts;
        }

       
    }
    
    [TrackChanges]
    public class ConnectMultiTenantEntity : FullAuditedEntity, IMustHaveTenant
    {
       
        public virtual int TenantId { get; set; }
    }

    public class ConnectMultiTenantNoTrackerEntity : FullAuditedEntity, IMustHaveTenant
    {
       
        public virtual int TenantId { get; set; }
    }
    
    public class ConnectFullMultiTenantAuditEntityDto : FullAuditedEntityDto, IMustHaveTenant
    {
        public virtual string Locations { get; set; }
        public virtual bool Group { get; set; }
        public virtual bool LocationTag { get; set; }
        public virtual string NonLocations { get; set; }
        public virtual int TenantId { get; set; }
    }
}