﻿
using System.Threading.Tasks;
using DinePlan.DineConnect.Book;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IBookInvoiceManager
    {
        Task<IdentityResult> CreateSync(BookInvoice bookInvoice);
    }
}
