﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.Book
{
    [Table("BookInvoices")]
    public class BookInvoice: FullAuditedEntity, IMustHaveTenant
    {
        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        public virtual int LocationId { get; set; }
        public virtual long UserId { get; set;}
        public int TenantId { get; set; }
        public string Files { get; set; }
        public BookInvoiceStatus Status { get; set; }

        public int? InvoiceId { get; set; }

    }

    public enum BookInvoiceStatus
    {
        Pending=0, Partial =1, WaitingForReason=3, Completed=4
    }
}
