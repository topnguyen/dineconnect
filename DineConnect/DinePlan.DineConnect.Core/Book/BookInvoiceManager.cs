﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Book;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class BookInvoiceManager : DineConnectServiceBase, IBookInvoiceManager, ITransientDependency
    {

        private readonly IRepository<BookInvoice> _bookInvoiceRepo;

        public BookInvoiceManager(IRepository<BookInvoice> bookInvoice)
        {
            _bookInvoiceRepo = bookInvoice;
        }

        public async Task<IdentityResult> CreateSync(BookInvoice bookInvoice)
        {
            //  if the New Addition
            if (bookInvoice.Id == 0)
            {
                //if (_bookInvoiceRepo.GetAll().Any(a => a.Name.Equals(bookInvoice.Name)))
                //{
                //    string[] strArrays = { L("NameAlreadyExists") };
                //    var success = AbpIdentityResult.Failed(strArrays);
                //    return success;
                //}
                await _bookInvoiceRepo.InsertAndGetIdAsync(bookInvoice);
                return IdentityResult.Success;

            }
            else
            {
                //List<BookInvoice> lst = _bookInvoiceRepo.GetAll().Where(a => a.Name.Equals(bookInvoice.Name) && a.Id != bookInvoice.Id).ToList();
                //if (lst.Count > 0)
                //{
                //    string[] strArrays = { L("NameAlreadyExists") };
                //    var success = AbpIdentityResult.Failed(strArrays);
                //    return success;
                //}
                return IdentityResult.Success;
            }
        }

    }
}
