﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.CaterModel;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.House;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Cater.Quotation
{
    [Table("CaterQuotationHeaders")]
    public class CaterQuotationHeader : ConnectMultiTenantEntity
    {
        public int LocationRefId { get; set; }
        [ForeignKey("LocationRefId")]
        public Location Location { get; set; }


        [ForeignKey("CaterCustomerId")]
        public CaterCustomer CaterCustomer { get; set; }

        public int CaterCustomerId { get; set; }

        [ForeignKey("EventAddressId")]
        public CaterCustomerAddressDetail EventAddress { get; set; }

        public int EventAddressId { get; set; }

        public DateTime EventDateTime { get; set; }
        public int NoOfPax { get; set; }

        public OrderChannels OrderChannel { get; set; }

        public decimal TotalAmount { get; set; }

        public decimal QuoteTotalAmount { get; set; }

        public string Note { get; set; }
        public bool DoesPOCreatedForThisCateringOrder { get; set; }
    }

    [Table("CaterQuotationCharges")]
    public class CaterQuotationCharge : ConnectMultiTenantEntity
    {
        [ForeignKey("HeaderId")]
        public CaterQuotationHeader CaterQuotationHeader { get; set; }

        public int HeaderId { get; set; }

        [ForeignKey("CaterChargeId")]
        public CaterCharge CaterCharge { get; set; }

        public int CaterChargeId { get; set; }
        public decimal Total { get; set; }
        public string Note { get; set; }
        public decimal ChargeValue { get; set; }
        public decimal Percentage { get; set; }
    }

    [Table("CaterQuotationDetails")]
    public class CaterQuotationDetail : ConnectMultiTenantEntity
    {
        [ForeignKey("HeaderId")]
        public CaterQuotationHeader CaterQuotationHeader { get; set; }

        public int HeaderId { get; set; }
        public string GroupName { get; set; }
        public string ProductName { get; set; }

        [ForeignKey("MenuItemPortionId")]
        public MenuItemPortion MenuItemPortion { get; set; }

        public int MenuItemPortionId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public string Taxes { get; set; }
        public string Note { get; set; }
        public string Tags { get; set; }
    }

    [Table("CaterQuoteRequiredMaterials")]
    public class CaterQuoteRequiredMaterial : FullAuditedEntity, IMustHaveTenant
    {
        public int CaterQuoteHeaderId { get; set; }
        [ForeignKey("CaterQuoteHeaderId")]
        public CaterQuotationHeader CaterQuotationHeader { get; set; }

        public int MaterialRefId { get; set; }
        //public string MaterialRefName { get; set; }
        public decimal TotalQty { get; set; }
        public int UnitRefId { get; set; }
        public int? StatusRefId { get; set; }
        //public string Uom { get; set; }
        public string Remarks { get; set; }
        //public List<MaterialMenuMappingWithWipeOut> MenuMappedSubList { get; set; }
        //public List<ConsolidatedMaterialRequiredWithLinkedMenuDto> ConsolidatedSubDetails { get; set; }  
        //[ForeignKey("PurchaseOrderRefId")]
        //public int? PurchaseOrderRefId { get; set; }
        //public PurchaseOrder PurchaseOrders { get; set; }
        public virtual int TenantId { get; set; }
    }

    public enum CaterQuotationRequiredMaterialStatus
    {
        Po_Not_Generated = 1,
        Po_Partially_Generated = 2,
        Po_Generated = 3,
        Quantity_Increased = 4,
        Quantity_Reduced = 5,
    }
   
    [Table("CaterQuoteRequiredMaterialVsMenuLinks")]
    public class CaterQuoteRequiredMaterialVsMenuLink : FullAuditedEntity, IMustHaveTenant
    {
        
        public int CaterQuoteHeaderId { get; set; }
        [ForeignKey("CaterQuoteHeaderId")]
        public CaterQuotationHeader CaterQuotationHeaders { get; set; }

        
        public int CaterQuoteRequiredMaterialDataRefId { get; set; }
        [ForeignKey("CaterQuoteRequiredMaterialDataRefId")]
        public CaterQuoteRequiredMaterial CaterQuoteRequiredMaterials { get; set; }

        public int PosMenuPortionRefId { get; set; }
        //public string PosRefName { get; set; }
        public int MenuQuantitySold { get; set; }
        public int MaterialRefId { get; set; }
        //public string MaterialRefName { get; set; }
        public decimal PortionPerUnitOfSales { get; set; }
        public decimal PortionQty { get; set; }
        public int PortionUnitId { get; set; }
        //public string UnitRefName { get; set; }
        //public int DefaultUnitId { get; set; }
        //public string DefaultUnitName { get; set; }
        public decimal WastageExpected { get; set; }
        public int NoOfPax { get; set; }
        public decimal TotalSaleQuantity { get; set; }
        public virtual int TenantId { get; set; }
    }

    [Table("CaterQuoteRequiredMaterialVsPurchaseOrderLinks")]
    public class CaterQuoteRequiredMaterialVsPurchaseOrderLink : FullAuditedEntity, IMustHaveTenant
    {

        public int CaterQuoteRequiredMaterialDataRefId { get; set; }
        [ForeignKey("CaterQuoteRequiredMaterialDataRefId")]
        public CaterQuoteRequiredMaterial CaterQuoteRequiredMaterial { get; set; }

        public int PurchaseOrderRefId { get; set; }
        [ForeignKey("PurchaseOrderRefId")]
        public PurchaseOrder PurchaseOrders { get; set; }
        public virtual int TenantId { get; set; }
    }


}