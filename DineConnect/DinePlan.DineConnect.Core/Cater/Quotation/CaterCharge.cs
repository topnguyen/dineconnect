﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Filter;

namespace DinePlan.DineConnect.Cater.Quotation
{
    public class CaterCharge  : ConnectMultiTenantEntity
    {
        public string Name { get; set; }
        public bool Decrease { get; set; }
        public decimal TotalValue { get; set; }
        public bool Percentage { get; set; }
        public bool ChangeInQuote { get; set; }

    }
}
