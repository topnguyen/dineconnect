﻿using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Filter;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.CaterModel
{
    [Table("CaterCustomers")]
    public class CaterCustomer : ConnectMultiTenantEntity
    {
        public CaterCustomer()
        {
            CaterCustomerAddresses = new HashSet<CaterCustomerAddressDetail>();
        }

        [Required]
        [MaxLength(60)]
        public virtual string CustomerName { get; set; }

        [MaxLength(60)]
        public virtual string LegalEntityName { get; set; }

        [MaxLength(60)]
        public virtual string TaxRegistrationNumber { get; set; }

        public string CreditTerm { get; set; }

        [MaxLength(50)]
        public string Email { get; set; }

        [MaxLength(50)]
        public string FaxNumber { get; set; }

        [MaxLength(50)]
        public string Website { get; set; }

        [MaxLength(50)]
        public string PhoneNumber { get; set; }

        [MaxLength(100)]
        public string AddOn { get; set; }

        public int OrderChannel { get; set; }
        public ICollection<CaterCustomerAddressDetail> CaterCustomerAddresses { get; set; }
    }

    [Table("CaterCustomerAddressDetails")]
    public class CaterCustomerAddressDetail : CreationAuditedEntity
    {
        [ForeignKey("CaterCustomerId")]
        public virtual CaterCustomer CaterCustomer { get; set; }

        public virtual int CaterCustomerId { get; set; }

        [MaxLength(50)]
        public string Address1 { get; set; }

        [MaxLength(50)]
        public string Address2 { get; set; }

        [MaxLength(50)]
        public string Address3 { get; set; }

        [MaxLength(50)]
        public virtual string City { get; set; }

        [MaxLength(50)]
        public virtual string State { get; set; }

        [MaxLength(50)]
        public virtual string Country { get; set; }

        [MaxLength(50)]
        public string ZipCode { get; set; }

        [MaxLength(50)]
        public string PhoneNumber { get; set; }
    }

    public enum OrderChannels
    {
        Phone = 0,
        Web = 1,
        Online = 2,
        Message = 3
    }
}