﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Z.EntityFramework.Plus;

namespace DinePlan.DineConnect.Audit
{
    public class TenantAuditEntry : AuditEntry, IMustHaveTenant
    {
        public int TenantId { get; set; }
    }

    public class TenantAuditEntryProperty : AuditEntryProperty
    {
        
    }
}
