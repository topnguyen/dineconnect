﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Utilities
{
    public class DineConnectUtil
    {
        public static bool IsInteger(string value)
        {
            int num;
            return int.TryParse(value, out num);
        }

        public static bool IsDecimal(string value)
        {
            decimal num;
            return decimal.TryParse(value, NumberStyles.Any,CultureInfo.InvariantCulture, out num);
        }

    }
}
