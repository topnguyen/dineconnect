﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Engage.Gift;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Engage
{
    public interface IGiftVoucherManager
    {
        Task<IdentityResult> CreateSync(GiftVoucher giftVoucher);
    }
}
