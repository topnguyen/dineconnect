﻿using DinePlan.DineConnect.Template;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage
{
    public interface IEngageMessageSender
    {
        Task SendMemberVoucher(int memberId, string voucher);
        Task SendResetPassword(EmailTemplate argEmailTemplate, string userName, string newPassword, string sender, string emailAddress);

        Task SendRandomPassword(string userName, string newPassword, string email, string sender, int tenantId);
    }
}
