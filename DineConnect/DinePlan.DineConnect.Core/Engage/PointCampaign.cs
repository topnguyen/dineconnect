﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Engage
{
    [Table("EngagePointCampaign")]
    public class PointCampaign : FullAuditedEntity, IMustHaveTenant
    {
        public PointCampaign()
        {
            this.Modifiers = new HashSet<PointCampaignModifier>();
        }
        public virtual int TenantId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual int OfferedPoints { get; set; }
        public virtual decimal RequiredAmount { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public ICollection<PointCampaignModifier> Modifiers { get; set; }
    }

    [Table("EngagePoinCampaignModifier")]
    public class PointCampaignModifier: Entity {
        // public int Id { get; set; }
        [ForeignKey("CampaignId")]
        public virtual PointCampaign Campaign { get;set; }
        public int CampaignId { get; set; }
        [ForeignKey("ModifierTypeId")]
        public PointCampaignModifierType ModifierType { get; set; }
        public int ModifierTypeId { get; set; }
        public decimal ModifyingFactor { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        [ForeignKey("MembershipTierId")]
        public MembershipTier MembershipTier { get; set; }
        public int? MembershipTierId { get; set; }
    }

    [Table("EngagePoinCampaignModifierType")]
    public class PointCampaignModifierType: Entity {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
