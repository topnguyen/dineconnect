﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage
{
    [Table("EngageMemberAccountTransactions")]
    public class MemberAccountTransaction : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public virtual int AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual MemberAccount Account { get; set; }
        public virtual decimal PreviousBalance { get; set; }
        public virtual decimal TransactionalUnitCount { get; set; }
        [MaxLength(100)]
        public virtual string Reference { get; set; }
        [MaxLength(150)]
        public virtual string Description { get; set; }

    }
}
