﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage
{
    [Table("EngageAccountTypes")]
    public class MemberAccountType : FullAuditedEntity
    {
        public virtual string Code { get; set; }
        public virtual string Description { get; set; }

    }

    public enum MemberAccountTypeEnum { 
        PNT=1,
        VCH=2,
        STV=3,
        STM=4,
        PPI=5
    }
}
