﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Engage.Gift;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Engage.Impl
{
    public class GiftVoucherManager : DineConnectServiceBase, IGiftVoucherManager, ITransientDependency
    {

        private readonly IRepository<GiftVoucher> _giftVoucherRepo;

        public GiftVoucherManager(IRepository<GiftVoucher> giftVoucherRepo)
        {
            _giftVoucherRepo = giftVoucherRepo;
        }

        public async Task<IdentityResult> CreateSync(GiftVoucher voucher)
        {
            if (voucher.Id == 0)
            {
                if (_giftVoucherRepo.GetAll().Any(a => a.Voucher.Contains(voucher.Voucher)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _giftVoucherRepo.InsertAsync(voucher);
                return IdentityResult.Success;
            }
            return IdentityResult.Success;
           
        }

    }
}
