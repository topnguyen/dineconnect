﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Engage.Gift;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Engage.Impl
{
    public class GiftVoucherTypeManager : DineConnectServiceBase, IGiftVoucherTypeManager, ITransientDependency
    {

        private readonly IRepository<GiftVoucherType> _giftVoucherTypeRepo;

        public GiftVoucherTypeManager(IRepository<GiftVoucherType> giftVoucherType)
        {
            _giftVoucherTypeRepo = giftVoucherType;
        }

        public async Task<IdentityResult> CreateSync(GiftVoucherType giftVoucherType)
        {
            //  if the New Addition
            if (giftVoucherType.Id == 0)
            {
                if (_giftVoucherTypeRepo.GetAll().Any(a => a.Id.Equals(giftVoucherType.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _giftVoucherTypeRepo.InsertAndGetIdAsync(giftVoucherType);
                return IdentityResult.Success;

            }
            else
            {
                List<GiftVoucherType> lst = _giftVoucherTypeRepo.GetAll().Where(a => a.Id.Equals(giftVoucherType.Id) && a.Id != giftVoucherType.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
