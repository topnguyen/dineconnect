﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Engage.Impl
{
    public class ReservationManager : DineConnectServiceBase, IReservationManager, ITransientDependency
    {

        private readonly IRepository<Reservation.Reservation> _reservationRepo;

        public ReservationManager(IRepository<Reservation.Reservation> reservation)
        {
            _reservationRepo = reservation;
        }

        public async Task<IdentityResult> CreateSync(Reservation.Reservation reservation)
        {
            //  if the New Addition
            if (reservation.Id == 0)
            {
                if (_reservationRepo.GetAll().Any(a => a.Id.Equals(reservation.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _reservationRepo.InsertAndGetIdAsync(reservation);
                return IdentityResult.Success;

            }
            else
            {
                List<Reservation.Reservation> lst = _reservationRepo.GetAll().Where(a => a.Id.Equals(reservation.Id) && a.Id != reservation.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}

