﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Engage.Gift;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage.Impl
{

    public class GiftVoucherCategoryManager : DineConnectServiceBase, IGiftVoucherCategoryManager, ITransientDependency
    {
         private readonly IRepository<GiftVoucherCategory> _giftvouchercategoryRepo;

        public GiftVoucherCategoryManager(IRepository<GiftVoucherCategory> giftvouchercategoryRepo)
        {
            _giftvouchercategoryRepo = giftvouchercategoryRepo;
        }

        public async Task<IdentityResult> CreateSync(GiftVoucherCategory giftVoucherCategory)
        {
            var nameExists = _giftvouchercategoryRepo.GetAll()
                .Where(a => a.Name != null && a.Name.Equals(giftVoucherCategory.Name))
                .WhereIf(giftVoucherCategory.Id != 0, a => a.Id != giftVoucherCategory.Id);

            var codeExists = _giftvouchercategoryRepo.GetAll()
                .Where(a => a.Code != null && a.Code.Equals(giftVoucherCategory.Code))
                .WhereIf(giftVoucherCategory.Id != 0, a => a.Id != giftVoucherCategory.Id);
            //  if the New Addition
            if (nameExists.Any())
            {
                string[] strArrays = { $"{L("Category")} {L("Name")} {giftVoucherCategory.Name} {L("AlreadyExists")}" };
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }
            if (codeExists.Any())
            {
                string[] strArrays = { $"{L("Category")} {L("Code") } {giftVoucherCategory.Code} {L("AlreadyExists")}" };
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }

            await _giftvouchercategoryRepo.InsertOrUpdateAndGetIdAsync(giftVoucherCategory);
            return IdentityResult.Success;
        }
    }
}
