﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage.Impl
{
    public class StageManager : DineConnectServiceBase, IStageManager, ITransientDependency
    {
        private readonly IRepository<ProgramStage> _stageRepo;

        public StageManager(IRepository<ProgramStage> stageRepo)
        {
            _stageRepo = stageRepo;
        }

        public async Task<IdentityResult> CreateOrUpdateSync(ProgramStage stage)
        {
            try
            {
                if (stage.Id == 0)
                {
                    if (_stageRepo.GetAll().Any(a => a.Name.Equals(stage.Name)))
                    {
                        string[] strArrays = { L("NameAlreadyExists") };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }
                }
                else
                {
                    List<ProgramStage> lst = _stageRepo.GetAll().Where(a => a.Name.Equals(stage.Name) && a.Id != stage.Id).ToList();
                    if (lst.Count > 0)
                    {
                        string[] strArrays = { L("NameAlreadyExists") };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }
                }
                var mem = await _stageRepo.InsertOrUpdateAndGetIdAsync(stage);
                return IdentityResult.Success;
            }
            catch (Exception exception)
            {
                var mess = exception.Message;
                string[] strArrays = { L("SaveFailed") };
                return AbpIdentityResult.Failed(strArrays);
            }
        }
    }
}
