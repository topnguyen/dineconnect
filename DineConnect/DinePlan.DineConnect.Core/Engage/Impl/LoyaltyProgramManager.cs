﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage.Impl
{
    public class LoyaltyProgramManager : DineConnectServiceBase, ILoyaltyProgramManager, ITransientDependency
    {
        private readonly IRepository<LoyaltyProgram> _programRepo;

        public LoyaltyProgramManager(IRepository<LoyaltyProgram> programRepo)
        {
            _programRepo = programRepo;
        }

        public async Task<IdentityResult> CreateOrUpdateSync(LoyaltyProgram program)
        {
            try
            {
                if (program.Id == 0)
                {
                    if (_programRepo.GetAll().Any(a => a.Name.Equals(program.Name)))
                    {
                        string[] strArrays = { L("NameAlreadyExists") };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }
                }
                else
                {
                    List<LoyaltyProgram> lst = _programRepo.GetAll().Where(a => a.Name.Equals(program.Name) && a.Id != program.Id).ToList();
                    if (lst.Count > 0)
                    {
                        string[] strArrays = { L("NameAlreadyExists") };
                        var success = AbpIdentityResult.Failed(strArrays);
                        return success;
                    }
                }
                var mem = await _programRepo.InsertOrUpdateAndGetIdAsync(program);
                return IdentityResult.Success;
            }
            catch (Exception exception)
            {
                var mess = exception.Message;
                string[] strArrays = { L("SaveFailed") };
                return AbpIdentityResult.Failed(strArrays);
            }
        }
    }
}
