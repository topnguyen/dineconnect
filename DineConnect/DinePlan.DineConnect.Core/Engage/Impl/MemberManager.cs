﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Engage.Impl
{
    public class MemberManager : DineConnectServiceBase, IMemberManager, ITransientDependency
    {

        private readonly IRepository<ConnectMember> _memberRepo;
        private readonly IRepository<MemberPoint> _memberPointRepo;


        public MemberManager(IRepository<ConnectMember> member, IRepository<MemberPoint> memberPoint )
        {
            _memberRepo = member;
            _memberPointRepo = memberPoint;
        }

        public async Task<IdentityResult> CreateSync(ConnectMember member)
        {
            if (member.Id == 0)
            {
                if (_memberRepo.GetAll().Any(a => a.MemberCode.Equals(member.MemberCode)))
                {
                    string[] strArrays = { L("MemberAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                try
                {
                   var mem =   await _memberRepo.InsertAndGetIdAsync(member);
                    return IdentityResult.Success;
                }
                catch (Exception exception)
                {
                    var mess = exception.Message;
                }
              

            }
            return IdentityResult.Success;
        }

        public async Task<IdentityResult> CreateOrUpdate(ConnectMember member)
        {
            if (member.Id == 0)
            {
                if (_memberRepo.GetAll().Any(a => a.MemberCode.Equals(member.MemberCode)))
                {
                    return IdentityResult.Success;
                }
                try
                {
                    var mem =   await _memberRepo.InsertAndGetIdAsync(member);
                    return IdentityResult.Success;
                }
                catch (Exception exception)
                {
                    var mess = exception.Message;
                }
            }
            return IdentityResult.Success;
        }

       

        public async Task UpdateMember(ConnectMember entity)
        {
            await _memberRepo.UpdateAsync(entity);
        }

    }
}