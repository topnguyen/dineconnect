﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Engage
{
    [Table("EngageMemberServiceInfoType")]
    public class MemberServiceInfoType : FullAuditedEntity
    {
        [Required]
        public virtual string Code { get; set; }
        [MaxLength(100)]
        public virtual string Description { get; set; }
    }

    public enum MemberServiceInfoTypeEnum { 
        Ess = 1, //eService Slip
        Nwl = 2, //News Letter
        EssNotiEmail = 3, // eService Slip Noti Email
        EssNotiSms = 4, // eService Slip Noti SMS
    }
}
