﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Engage.Gift;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Engage
{
    
    public interface IGiftVoucherCategoryManager
    {
        Task<IdentityResult> CreateSync(GiftVoucherCategory giftVoucherCategory);
    }
}
