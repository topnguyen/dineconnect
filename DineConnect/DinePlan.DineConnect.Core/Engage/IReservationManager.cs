﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Engage.Impl
{
    public interface IReservationManager
    {
        Task<IdentityResult> CreateSync(Reservation.Reservation reservation);
    }
}
