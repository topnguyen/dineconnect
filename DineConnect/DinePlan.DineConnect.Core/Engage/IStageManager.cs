﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage
{
    public interface IStageManager
    {
        Task<IdentityResult> CreateOrUpdateSync(ProgramStage stage);
    }
}
