﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage
{
    [Table("EngageMemberAccounts")]
    public class MemberAccount : FullAuditedEntity, IMustHaveTenant
    {
        public const int NameMaxLength = 100;
        public const int RefMaxLength = 100;
        public int TenantId { get; set; }
        [Required]
        public virtual int MemberId { get; set; }
        [Required]
        public virtual string Number { get; set; }
        [Required]
        [MaxLength(NameMaxLength)]
        public virtual string Name { get; set; }
        [Required]
        public virtual int AccountTypeId { get; set; }
        [ForeignKey("AccountTypeId")]
        public virtual MemberAccountType AccountType { get; set; }
        public virtual decimal Balance { get; set; }
        [Required]
        public virtual bool IsActive { get; set; }

    }
}
