﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace DinePlan.DineConnect.Engage.Feed
{
    public enum QuestionType
    {
        Input = 0,
        SelectSingle = 1,
        SelectMulti = 2,
        Rating = 3
    }

  
    [Table("FeedBackGroups")]
    public class FeedBackGroup : FullAuditedEntity, IMustHaveTenant
    {
        private IList<FeedBackQuestion> _feedbackQuestions;
        public FeedBackGroup()
        {
            _feedbackQuestions = new List<FeedBackQuestion>();
        }
        public virtual IList<FeedBackQuestion> FeedbackQuestions
        {
            get { return _feedbackQuestions; }
            set { _feedbackQuestions = value; }
        }
        public string Name { get; set; }
        public int TenantId { get; set; }
    }
    [Table("FeedBackQuestions")]

    public class FeedBackQuestion : CreationAuditedEntity
    {
        private IList<FeedBackGroup> _feedbackGroups;
        public FeedBackQuestion()
        {
            _feedbackGroups = new List<FeedBackGroup>();
            this.QuestionType = QuestionType.Input;
        }
        public string Question { get; set; }
        public QuestionType QuestionType { get; set; }
        public string Answers { get; set; }
        public string NegativeAnswer { get; set; }
        public string DefaultAnswer { get; set; }
        public virtual IList<FeedBackGroup> FeedbackGroups
        {
            get { return _feedbackGroups; }
            set { _feedbackGroups = value; }
        }
        public string Name { get; set; }
    }
    [Table("FeedBacks")]

    public class FeedBack : FullAuditedEntity, IMustHaveTenant
    {
        public FeedBackGroup FeedbackGroup { get; set; }
        [ForeignKey("FeedbackGroup")]
        public int FeedbackGroupId { get; set; }
        public string TicketNo { get; set; }
        public List<FeedBackAnswer> Answers { get; set; }
        public DateTime CreatedDate { get; set; }

        public FeedBack()
        {
            Answers = new List<FeedBackAnswer>();
            CreatedDate = DateTime.Now;
        }
        public int TenantId { get; set; }
    }
    [Table("FeedBackAnswers")]

    public class FeedBackAnswer : CreationAuditedEntity
    {
        public FeedBack Feedback { get; set; }
        [ForeignKey("Feedback")]
        public int FeedbackId { get; set; }
        public FeedBackQuestion FeedbackQuestion { get; set; }
        [ForeignKey("FeedbackQuestion")]
        public int FeedbackQuestionId { get; set; }
        public string Answer { get; set; }
        public bool IsNegative { get; set; }
    }
}
