﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.Engage.Reservation
{
    [Table("Reservation")]
    public class Reservation : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int LocationRefId { get; set; }

        [ForeignKey("LocationRefId")]
        public virtual Location Locations { get; set; }

        public virtual DateTime ReservationDate { get; set; }
        public virtual int ConnectMemberId { get; set; }

        [ForeignKey("ConnectMemberId")]
        public virtual ConnectMember ConnectMember { get; set; }

        public virtual int Pax { get; set; }
        public virtual bool RemainderRequired { get; set; }
        public virtual string RemainderTimes { get; set; }
        //  123   -   1 - One Day Before, 2 - Same Day , 3 - Two Hours Before
        public virtual string RemainderMediums { get; set; } //  ES - E - Email, S - SMS, P - Phone
        public virtual string Remarks { get; set; }
        public virtual bool Status { get; set; } //  By Default, 'Pending', 'Completed' while customers reached location
        public virtual int TenantId { get; set; }
    }


    public enum RemainderTimes
    {
        OneDayBefore,
        SameDay,
        TwoHoursBefore
    };


    public enum RemainderMediums
    {
        Email,
        SMS
    };
}