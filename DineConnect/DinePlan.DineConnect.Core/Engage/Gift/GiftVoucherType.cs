﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Audit;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Filter;

namespace DinePlan.DineConnect.Engage.Gift
{
    [Table("GiftVoucherTypes")]
    public class GiftVoucherType : ConnectFullMultiTenantAuditEntity
    {
        public virtual string Name { get; set; }
        public virtual int Type { get; set; }   //0 -> Percentage, 1 -> Fixed
        public virtual string Prefix { get; set; }
        public virtual string Suffix { get; set; }
        public virtual int VoucherDigit { get; set; }
        public virtual int VoucherCount { get; set; }
        public virtual decimal VoucherValue { get; set; }
        public virtual DateTime ValidTill { get; set; }
        public virtual bool IsPointsApplicable { get; set; }
        public virtual bool IsRepeatApplicable { get; set; }
        public virtual int RepeatCount { get; set; }
        public virtual decimal MinimumTicketTotal { get; set; }
        public virtual bool AutoAttach { get; set; }
        public virtual string ConfNumber { get; set; }
        public virtual int RedeemStartHour { get; set; }
        public virtual int RedeemStartMinute { get; set; }
        public virtual int RedeemEndHour { get; set; }
        public virtual int RedeemEndMinute { get; set; }
        public virtual decimal RequiredPoints { get; set; }
        public bool IsPercent => Type == 0;
        public bool IsFixed => Type == 1;
        public virtual ICollection<GiftVoucher> GiftVouchers { get; set; }
        [ForeignKey("GiftVoucherCategoryId")]
        public virtual GiftVoucherCategory GiftVoucherCategory { get; set; }
        public virtual int? GiftVoucherCategoryId { get; set; }
    }

    [Table("GiftVouchers")]
    public class GiftVoucher : FullAuditedEntity, IMustHaveTenant
    {
        [Index("VoucherId", IsUnique = true)]
        [MaxLength(30)]
        public virtual string Voucher { get; set; }

        [ForeignKey("GiftVoucherTypeId")]
        public virtual GiftVoucherType GiftVoucherType { get; set; }
        public virtual int GiftVoucherTypeId { get; set; }
        public virtual bool Claimed { get; set; }
        public virtual bool Assigned { get; set; }

        public virtual DateTime? ClaimedDate { get; set; }
        public virtual string Remarks { get; set; }
        public virtual decimal ClaimedAmount { get; set; }

        public virtual string TicketNumber { get; set; }

        public virtual int LocationId { get; set; }
        public virtual int TenantId { get; set; }

        [ForeignKey("ConnectMemberId")]
        public virtual ConnectMember ConnectMember { get; set; }
        public virtual int? ConnectMemberId{ get; set; }
    }
    [Table("GiftVoucherCategories")]
    public class GiftVoucherCategory : ConnectMultiTenantEntity
    {
        [StringLength(10)]
        public virtual string Code { get; set; }

        [StringLength(50)]
        public virtual string Name { get; set; }
    }

}
