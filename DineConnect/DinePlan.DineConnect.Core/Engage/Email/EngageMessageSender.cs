﻿using System;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Net.Mail;
using Abp.Runtime.Session;
using Abp.UI;
using Castle.Core.Logging;
using DinePlan.DineConnect.Engage.Gift;
using DinePlan.DineConnect.Template;

namespace DinePlan.DineConnect.Engage.Email
{
    /// <summary>
    ///     Used to send email to users.
    /// </summary>
    public class EngageMessageSender : DineConnectServiceBase, IEngageMessageSender, ITransientDependency
    {
        private readonly IRepository<ConnectMember> _conMember;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<EmailTemplate> _emailTemplate;
        private readonly IRepository<GiftVoucher> _giftVoucher;
        private readonly ILogger _logger;

        public EngageMessageSender(
            IEmailSender emailSender,
            IRepository<ConnectMember> tRepo, IRepository<GiftVoucher> gVoucher,
            IRepository<EmailTemplate> emailTemplate,
            ILogger logger)
        {
            _emailSender = emailSender;
            _conMember = tRepo;
            _giftVoucher = gVoucher;
            _emailTemplate = emailTemplate;
            _logger = logger;
        }

        public async Task SendMemberVoucher(int memberId, string voucher)
        {
            var member = await _conMember.FirstOrDefaultAsync(a => a.Id == memberId);
            var gVoucher = await _giftVoucher.FirstOrDefaultAsync(a => a.Voucher.Equals(voucher));
            var template = await _emailTemplate.FirstOrDefaultAsync(a => a.Name.Equals("MEMBER VOUCHER"));
            if (member != null && gVoucher != null && template != null)
            {
                var emailTemplate = new StringBuilder(template.Template);

                emailTemplate.Replace("{MEMBER NAME}", member.Name);
                emailTemplate.Replace("{MEMBER CODE}", member.MemberCode);
                emailTemplate.Replace("{VOUCHER NO}", voucher);

                try
                {
                    await _emailSender.SendAsync(member.EmailId, "MEMBER VOUCHER", emailTemplate.ToString());
                    _logger.Info("Email send successfully to this user: " + member.EmailId);
                }
                catch (Exception ex)
                {
                    _logger.Error("Unable to send the mail : ", ex);
                    throw new UserFriendlyException(ex.Message + " " + ex.InnerException);
                }
            }
        }

        public async Task SendResetPassword(EmailTemplate argEmailTemplate, string userName, string newPassword, string sender, string emailAddress)
        {
            var emailTemplate = new StringBuilder(argEmailTemplate.Template);

            emailTemplate.Replace("{USERNAME}", userName);
            emailTemplate.Replace("{NEWPASSWORD}", newPassword);
            emailTemplate.Replace("{SENDER}", sender);

            try
            {
                await _emailSender.SendAsync(emailAddress, "Reset Password" , emailTemplate.ToString());
                _logger.Info("Email send successfully to this user: " + emailAddress);
            }
            catch (Exception ex)
            {
                _logger.Error("Unable to send the mail : ", ex);
                throw new UserFriendlyException(ex.Message + " " + ex.InnerException);
            }
        }

        public async Task SendRandomPassword(string userName, string newPassword, string email, string sender, int tenantId)
        {
            var template = await _emailTemplate.FirstOrDefaultAsync(a => a.Name.Equals("RANDOM PASSWORD") && a.TenantId == tenantId);
            if (template != null)
            {
                var emailTemplate = new StringBuilder(template.Template);
                emailTemplate.Replace("{USERNAME}", userName);
                emailTemplate.Replace("{PASSWORD}", newPassword);
                emailTemplate.Replace("{SENDER}", sender);
                try
                {
                    await _emailSender.SendAsync(email, "YOUR PASSWORD", emailTemplate.ToString());
                    _logger.Info("New Random Password Email send successfully to this user: " + email);
                }
                catch (Exception ex)
                {
                    _logger.Error("Unable to send the Random Password email : ", ex);
                    throw new UserFriendlyException(ex.Message + " " + ex.InnerException);
                }
            }
        }
    }
}