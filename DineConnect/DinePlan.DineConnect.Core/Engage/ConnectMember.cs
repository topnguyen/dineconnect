﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Engage.Gift;

namespace DinePlan.DineConnect.Engage
{
    [Table("ConnectMembers")]
    public class ConnectMember : FullAuditedEntity, IMustHaveTenant
    {
        [Index("MemberCode", IsUnique = true)]
        [MaxLength(50)]
        public virtual string MemberCode { get; set; }

        [MaxLength(50)]
        public virtual string Name { get; set; }
        public virtual string LastName { get; set; }

        [MaxLength(50)]
        public virtual string EmailId { get; set; }
        [MaxLength(50)]
        public virtual string PhoneNumber { get; set; }
        [MaxLength(2000)]
        public virtual string CustomData { get; set; }
        public virtual decimal TotalPoints { get; set; }
        public virtual DateTime? LastVisitDate { get; set; }
        public virtual DateTime? BirthDay { get; set; }
        public virtual int? DefaultAddressRefId { get; set; }
        public virtual int? MembershipTierId { get; set; }
        public virtual ICollection<GiftVoucher> Vouchers { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual int TenantId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public long? UserId { get; set; }
    }
    [Table("MemberContactDetails")]
    public class MemberContactDetail : CreationAuditedEntity
    {
        [ForeignKey("ConnectMemberId")]
        public virtual ConnectMember ConnectMember { get; set; }
        public virtual int ConnectMemberId { get; set; }
        public virtual ContactDetailType ContactDetailType { get; set; }
        public virtual string Detail { get; set; }
        public virtual bool AcceptToCommunicate { get; set; }

    }
    public enum ContactDetailType
    {
        HandPhone=1,
        Phone = 2,
        Fax = 3,
        Email = 4,
        Instagram = 5,
        LinkedIn = 6,
        FaceBook = 7
    }
    [Table("MemberAddresses")]
    public class MemberAddress : CreationAuditedEntity
    {
        [ForeignKey("ConnectMemberId")]
        public virtual ConnectMember ConnectMember { get; set; }
        public virtual int ConnectMemberId { get; set; }
        public virtual string Tag { get; set; }
        public virtual bool isPrimary { get; set; }
        public virtual int? LocationId { get; set; }
        [MaxLength(50)]
        public virtual string Address { get; set; }
        [MaxLength(50)]
        public virtual string Locality { get; set; }
        [MaxLength(50)]
        public virtual string City { get; set; }
        [MaxLength(50)]
        public virtual string State { get; set; }
        [MaxLength(50)]
        public virtual string Country { get; set; }
        [MaxLength(50)]
        public virtual string PostalCode { get; set; }
        
    }

    [Table("MemberPoints")]
    public class MemberPoint : CreationAuditedEntity
    {
        [ForeignKey("ConnectMemberId")]
        public virtual ConnectMember ConnectMember { get; set; }

        public virtual int ConnectMemberId { get; set; }
        public virtual int LocationId { get; set; }
        [MaxLength(50)]
        public virtual string TicketNumber { get; set; }
        public virtual DateTime? TicketDate { get; set; }
        public virtual decimal TicketTotal { get; set; }
        public virtual bool IsPointAccumulation { get; set; }
        public virtual bool IsPointRedemption { get; set; }
        public virtual decimal PointsAccumulation { get; set; }
        public virtual decimal PointsAccumulationFactor { get; set; }
        public virtual decimal PointsRedemption { get; set; }
        public virtual decimal PointsRedemptionFactor { get; set; }
        public virtual decimal TotalPoints { get; set; }
        public virtual bool CalculationOnOrder { get; set; }
        public virtual bool OnHold { get; set; }
        public Guid RedemtionId { get; set; }
    }

    [Table("LoyaltyPrograms")]
    public class LoyaltyProgram : CreationAuditedEntity
    {
        [MaxLength(30)]
        public virtual string Name { get; set; }
        public virtual bool Expiry { get; set; }
        public virtual int ExpiryDays { get; set; }
        public int TenantId { get; set; }
        public DateTime StartDate { get; set; }
        public virtual Collection<ProgramStage> Stages { get; set; }
    }

    [Table("ProgramStages")]
    public class ProgramStage : CreationAuditedEntity
    {
        [ForeignKey("LoyaltyProgramId")]
        public virtual LoyaltyProgram LoyaltyProgram { get; set; }
        public virtual int LoyaltyProgramId { get; set; }
        [MaxLength(30)]
        public virtual string Name { get; set; }
        [MaxLength(200)]
        public virtual string Condition { get; set; }
        public virtual int ConditionDayValue { get; set; }
        public virtual int? NextStageId { get; set; }
        public virtual bool PointsExpiryAllowed { get; set; }
        public virtual decimal MinRedemptionPoints { get; set; }
        public virtual decimal RedemptionValue { get; set; }
        public virtual int PointExpiryDays { get; set; }

        public virtual Collection<PointAccumulation> PointAccumulations { get; set; }
    }

    [Table("PointAccumulations")]
    public class PointAccumulation : CreationAuditedEntity
    {
        [ForeignKey("StageId")]
        public virtual ProgramStage Stage { get; set; }
        public virtual int StageId { get; set; }
        public virtual int PAccTypeId { get; set; }
        public virtual decimal Point { get; set; }
        public virtual bool RoundOff { get; set; }
    }

    
}