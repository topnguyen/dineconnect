﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Engage.Gift;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage
{
    [Table("EngageMembershipTiers")]
    public class MembershipTier : FullAuditedEntity, IMustHaveTenant
    {
        public const int NameMaxLength = 50;

        public int TenantId { get; set; }
        [Required]
        [MaxLength(NameMaxLength)]
        public virtual string Name { get; set; }
        [Required]
        public virtual int TierLevel { get; set; }
        public virtual string Description { get; set; }
        [Required]
        public virtual decimal Price { get; set; }
        [Required]
        public virtual decimal RewardingPoints { get; set; }
        [ForeignKey("RewardingVoucherId")]
        public virtual GiftVoucherType RewardingVoucher { get; set; }
        public virtual int? RewardingVoucherId { get; set; }
        public virtual int RewardingVoucherCount { get; set; }
        [Required]
        public virtual decimal CashPaymentDiscount { get; set; }
        [Required]
        public virtual decimal CreditCardPaymentDiscount { get; set; }
        [Required]
        public virtual string TextOfBenefits { get; set; }
        [Required]
        public virtual string  TextOfConditions { get; set; }
    }
}
