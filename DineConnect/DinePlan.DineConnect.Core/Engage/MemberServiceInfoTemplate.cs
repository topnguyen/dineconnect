﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DinePlan.DineConnect.Engage
{
    [Table("EngageMemberServiceInfoTemplates")]
    public class MemberServiceInfoTemplate : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        [Required]
        [MaxLength(100)]
        public virtual string Name { get; set; }
        [Required]
        public virtual int ServiceInfoTypeId { get; set; }
        [ForeignKey("ServiceInfoTypeId")]
        public virtual MemberServiceInfoType ServiceInfoType { get; set; }
        public virtual string Template { get; set; }
        [Required]
        public virtual bool IsActive { get; set; }
        [Required]
        public virtual bool StoredAsPlainText { get; set; }
    }
}
