﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Engage
{
    public interface IMemberManager
    {
        Task<IdentityResult> CreateSync(ConnectMember member);
        Task<IdentityResult> CreateOrUpdate(ConnectMember member);

        Task UpdateMember(ConnectMember entity);
    }
}