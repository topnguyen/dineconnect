﻿using Abp.Configuration;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;

namespace DinePlan.DineConnect.Configuration
{
    /// <summary>
    /// Defines settings for the application.
    /// See <see cref="AppSettings"/> for setting names.
    /// </summary>
    public class AppSettingProvider : SettingProvider
    {
        public override IEnumerable<SettingDefinition> GetSettingDefinitions(SettingDefinitionProviderContext context)
        {
            var lastSyncDefaults = new List<LastSyncStatusDto>()
            {
                new LastSyncStatusDto{ startHour = "0", startMinute = "5", endHour = "0", endMinute = "59", status = "Live"},
                new LastSyncStatusDto{ startHour = "1", startMinute = "0", endHour = "7", endMinute = "59", status = "Warning"},
                new LastSyncStatusDto{ startHour = "8", startMinute = "0", endHour = "11", endMinute = "59", status = "Critical"},
                new LastSyncStatusDto{ startHour = "12", startMinute = "0", endHour = "23", endMinute = "59", status = "Offline"},
                new LastSyncStatusDto{ startHour = "0", startMinute = "0", endHour = "0", endMinute = "0", status = "Unknown"}
            };

            return new[]
                   {
                       //Host settings
                       new SettingDefinition(AppSettings.General.WebSiteRootAddress, "http://localhost:7200/"),
                       new SettingDefinition(AppSettings.ShortMessageSettings.MessageType, "1"),
                       new SettingDefinition(AppSettings.ShortMessageSettings.MessageSetting, ""),

                       //Tenant settings
                       new SettingDefinition(AppSettings.UserManagement.AllowSelfRegistration, ConfigurationManager.AppSettings[AppSettings.UserManagement.UseCaptchaOnRegistration] ?? "false", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.UserManagement.IsNewRegisteredUserActiveByDefault, ConfigurationManager.AppSettings[AppSettings.UserManagement.IsNewRegisteredUserActiveByDefault] ?? "false", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.UserManagement.UseCaptchaOnRegistration, ConfigurationManager.AppSettings[AppSettings.UserManagement.UseCaptchaOnRegistration] ?? "true", scopes: SettingScopes.Tenant),

                       new SettingDefinition(AppSettings.General.Syncurl,"", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.General.TenantId,"0", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.General.TenantName,"", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.General.User,"", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.General.Password,"", scopes: SettingScopes.Tenant),

                        //Connect Settings
                       new SettingDefinition(AppSettings.ConnectSettings.OperateHours,"0", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.TimeZoneUtc,"+0", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.Decimals,"2", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.SimpleDateFormat,"dd-MM-yyyy", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.DateTimeFormat,"dd-MM-yyyy HH:ss:mm", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.Schedules,"", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.WeekDay,"", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.WeekEnd,"", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.LastSyncStatuses, JsonConvert.SerializeObject(lastSyncDefaults) , scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.BucketName, "dineconnect" , scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.ServerUrl, "http://ftp.dineconnect.net:9000" , scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.AccessKey, "7NFNBWN0DCNIGMR1LBER" , scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.SecretKey, "0yQnCJOgDsD+QhtzZcX+cnBStz2s0oyPjNY7PbV8" , scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.Cloudinary, "false" , scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.SendEmailOnWorkDayClose, "false" , scopes: SettingScopes.Tenant),

                       new SettingDefinition(AppSettings.ConnectSettings.UrbanPiperUserName, "biz_adm_clients_gwAxdKMvMNOp" , scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.UrbanPiperApiKey, "5331069c5199a2397702055220578c6366efa04a" , scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.UrbanPiperPriceTag, "UP" , scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.UrbanPiperUrl, "https://staging.urbanpiper.com" , scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.UrbanPiperMenu, "UpMenu" , scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.LocationGroupAsBrand, "false" , scopes: SettingScopes.Tenant),

                       new SettingDefinition(AppSettings.ConnectSettings.DeliveryHeroUrl, "https://integration-middleware.as.restaurant-partners.com/v2/login " , scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.DeliveryHeroUser, "plugin-levelfive-pte-ltd-001" , scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.DeliveryHeroPassword, "COMlRsFqYv" , scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.DeliveryHeroPriceTag, "DH" , scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.DeliveryHeroPushMenuUrl, "https://integration-middleware.as.restaurant-partners.com/v2/chains/{your_chaincode_here}/remoteVendors/{your_remoteid_here}/menuImport" , scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.ConnectSettings.CreditSaleReport, "[CSR,CSR1]" , scopes: SettingScopes.Tenant),
                       //Cater Settings
                       new SettingDefinition(AppSettings.CaterSettings.CaterMenu,"", scopes: SettingScopes.Tenant),


                       //Engage Settings
                       new SettingDefinition(AppSettings.EngageSettings.PointsAccumulation,"1", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.EngageSettings.PointsAccumulationFactor,"10", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.EngageSettings.PointsRedemption,"1", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.EngageSettings.PointsRedemptionFactor,"10", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.EngageSettings.OnBoardMemberFromPos,"true", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.EngageSettings.CalculatePoints,"true", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.EngageSettings.CalculatePointsOnOrder,"true", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.EngageSettings.CalculatePointsOnMultiple,"true", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.EngageSettings.AutoGenerateMemberCode,"false", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.EngageSettings.CodeFormat,"", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.EngageSettings.CreateNewUserOnActivation,"false", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.EngageSettings.MemberUserRoleID,"0", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.EngageSettings.MemberUserRoleName,"", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.EngageSettings.DefaultCity,"Singapore", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.EngageSettings.DefaultState,"Singapore", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.EngageSettings.DefaultCountry,"Singapore", scopes: SettingScopes.Tenant),

                       // House Settings
                        new SettingDefinition(AppSettings.HouseSettings.SupplierMaterialTightlyCoupled,"true",scopes: SettingScopes.Tenant),
                        new SettingDefinition(AppSettings.HouseSettings.DirectTransferAllowed,"true",scopes: SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.InvoiceSupplierRateChangeFlag,"true",scopes: SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.TallyIntegrationFlag,"false",
                            scopes:SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.ExtraMaterialReceivedInDc,"true",
                            scopes:SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.InterTransferValueShown,"true",
                            scopes:SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.SmartPrintOutExists,"true",
                            scopes:SettingScopes.Tenant,isVisibleToClients:true),
                         new SettingDefinition(AppSettings.HouseSettings.MessageNotification,"true",
                            scopes:SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.SoftMessageNotification,"true",
                            scopes:SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.ForceAdjustmentWithDayClose,"false",
                            scopes:SettingScopes.Tenant,isVisibleToClients:true),
                              new SettingDefinition(AppSettings.HouseSettings.MaterialCostBasedOnLastPurchase,"false",
                                  scopes:SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.Decimals,"2",
                                  scopes:SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.IsMaterialCodeTreatedAsBarCode,"false",
                           scopes: SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.DefaultAveragePriceTagRefId,"0",
                           scopes: SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.NoOfMonthAvgTaken,"1",
                                  scopes:SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.IsOmitFOCMaterialQuantities,"false",
                           scopes: SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.IsCalculateAverageRateWithoutTax,"false",
                           scopes: SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.AbsentDeductionPerDay, "0",scopes: SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.IsInwardPurchaseOrderReferenceFromOtherErpRequired,"false",
                           scopes: SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.IsInwardInvoiceNumberReferenceFromOtherErpRequired,"false",
                           scopes: SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.MaterialCountToUpdate,"50",
                                  scopes:SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.WhileClosingStockTakenCanAllUnitAllowed,"true",
                        scopes: SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.BackGroundDayCloseCanBeAllowed,"false",
                            scopes: SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.TransferRequestLockHours,"0",
                                scopes:SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.TransferRequestGraceHours,"0",
                            scopes:SettingScopes.Tenant,isVisibleToClients:true),
                        new SettingDefinition(AppSettings.HouseSettings.PurchaseOrderCCMailList,"",
                            scopes:SettingScopes.Tenant,isVisibleToClients:true),

                        //Touch settings
                        new SettingDefinition(AppSettings.TouchSaleSettings.Currency,"RS", scopes: SettingScopes.Tenant),
                        new SettingDefinition(AppSettings.TouchSaleSettings.Decimals,"2", scopes: SettingScopes.Tenant),
                        new SettingDefinition(AppSettings.TouchSaleSettings.Rounding,"1", scopes: SettingScopes.Tenant),
                        new SettingDefinition(AppSettings.TouchSaleSettings.TaxInclusive,"true", scopes: SettingScopes.Tenant),
                        new SettingDefinition(AppSettings.TouchSaleSettings.ReceiptHeader,"DineTouch", scopes: SettingScopes.Tenant),
                        new SettingDefinition(AppSettings.TouchSaleSettings.ReceiptFooter,"http://www.dineplan.net", scopes: SettingScopes.Tenant),

                        new SettingDefinition(AppSettings.TouchSaleSettings.IsActivity,"true", scopes: SettingScopes.Tenant),
                        new SettingDefinition(AppSettings.TouchSaleSettings.IsReport,"true", scopes: SettingScopes.Tenant),
                        new SettingDefinition(AppSettings.TouchSaleSettings.IsBlindShift,"true", scopes: SettingScopes.Tenant),
                        new SettingDefinition(AppSettings.TouchSaleSettings.DuplicateReceipt,"false", scopes: SettingScopes.Tenant),

						      // Swipe Settings
                        new SettingDefinition(AppSettings.SwipeSettings.CanIssueCardDirectly,"false",scopes: SettingScopes.Tenant,isVisibleToClients:true),
                              new SettingDefinition(AppSettings.SwipeSettings.DepositRequiredDefault,"false",scopes: SettingScopes.Tenant,isVisibleToClients:true),
                              new SettingDefinition(AppSettings.SwipeSettings.DepositAmountDefault,"0",scopes: SettingScopes.Tenant,isVisibleToClients:true),
                              new SettingDefinition(AppSettings.SwipeSettings.ExpiryDaysFromIssueDefault,"0",
                                  scopes:SettingScopes.Tenant,isVisibleToClients:true),
                              new SettingDefinition(AppSettings.SwipeSettings.RefundAllowedDefault,"true",
                                  scopes:SettingScopes.Tenant,isVisibleToClients:true),

                               //AddOnSettings
                        new SettingDefinition(AppSettings.AddOnSettings.QBRefreshToken,"", scopes: SettingScopes.Tenant),
                        new SettingDefinition(AppSettings.AddOnSettings.QBRefreshTokenExpiresAt,"", scopes: SettingScopes.Tenant),
                        new SettingDefinition(AppSettings.AddOnSettings.QBAccessTokenExpiresAt,"", scopes: SettingScopes.Tenant),

                        //  Tick Setting
                        new SettingDefinition(AppSettings.TickSettings.IsBiometricAttendanceRequired,"", scopes: SettingScopes.Tenant),

                        //Password Policy
                         new SettingDefinition(AppSettings.PasswordPolicySettings.Min_Length,"0", scopes: SettingScopes.Tenant),
                         new SettingDefinition(AppSettings.PasswordPolicySettings.Max_Length,"0", scopes: SettingScopes.Tenant),
                         new SettingDefinition(AppSettings.PasswordPolicySettings.Min_LCL,"0", scopes: SettingScopes.Tenant),
                         new SettingDefinition(AppSettings.PasswordPolicySettings.Min_UCL,"0", scopes: SettingScopes.Tenant),
                         new SettingDefinition(AppSettings.PasswordPolicySettings.Min_AC,"0", scopes: SettingScopes.Tenant),
                         new SettingDefinition(AppSettings.PasswordPolicySettings.Min_SC,"0", scopes: SettingScopes.Tenant),
                         new SettingDefinition(AppSettings.PasswordPolicySettings.Size,"6", scopes: SettingScopes.Tenant),
                         new SettingDefinition(AppSettings.PasswordPolicySettings.Impermissible_Passwords,"", scopes: SettingScopes.Tenant),
                         new SettingDefinition(AppSettings.PasswordPolicySettings.Maximum_IT,"30", scopes: SettingScopes.Tenant),
                         new SettingDefinition(AppSettings.PasswordPolicySettings.Password_VP,"90", scopes: SettingScopes.Tenant),
                         new SettingDefinition(AppSettings.PasswordPolicySettings.Allow_LIDPP,"false", scopes: SettingScopes.Tenant),
                         new SettingDefinition(AppSettings.PasswordPolicySettings.Allow_OPPP,"false", scopes: SettingScopes.Tenant),
                         new SettingDefinition(AppSettings.PasswordPolicySettings.Allow_UCOP,"false", scopes: SettingScopes.Tenant),
                         new SettingDefinition(AppSettings.PasswordPolicySettings.Enforce,"false", scopes: SettingScopes.Tenant),
                         new SettingDefinition(AppSettings.PasswordPolicySettings.Maximum_NFLA,"6", scopes: SettingScopes.Tenant),
                         new SettingDefinition(AppSettings.PasswordPolicySettings.Suggest_Message,"1", scopes: SettingScopes.Tenant),
                   };
        }

        private class LastSyncStatusDto
        {
            public string status { get; set; }

            public string startHour { get; set; }
            public string startMinute { get; set; }

            public string endHour { get; set; }
            public string endMinute { get; set; }
        }
    }
}