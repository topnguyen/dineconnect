using System;

namespace DinePlan.DineConnect.Configuration
{
    /// <summary>
    /// Defines string constants for setting names in the application.
    /// See <see cref="AppSettingProvider"/> for setting definitions.
    /// </summary>
    public static class AppSettings
    {
        public static class General
        {
            public const string WebSiteRootAddress = "App.General.WebSiteRootAddress";
            public const string Syncurl = ".App.General.Syncurl";
            public const string TenantId = ".App.General.TenantId";
            public const string User = ".App.General.User";
            public const string Password = ".App.General.Password";
            public const string TenantName = ".App.General.TenantName";
        }

        public static class UserManagement
        {
            public const string AllowSelfRegistration = "App.UserManagement.AllowSelfRegistration";
            public const string IsNewRegisteredUserActiveByDefault = "App.UserManagement.IsNewRegisteredUserActiveByDefault";
            public const string UseCaptchaOnRegistration = "App.UserManagement.UseCaptchaOnRegistration";
        }

        public static class ConnectSettings
        {
            public const string OperateHours = "App.Connect.OperateHours";
            public const string TimeZoneUtc = "App.Connect.TimeZoneUtc";
            public const string Decimals = "App.Connect.Decimals";
            public const string DateTimeFormat = "App.Connect.DateTimeFormat";
            public const string SimpleDateFormat = "App.Connect.SimpleDateFormat";
            public const string Schedules = "App.Connect.Schedules";
            public const string WeekDay = "App.Connect.WeekDay";
            public const string WeekEnd = "App.Connect.WeekEnd";
            public const string LastSyncStatuses = "App.Connect.LastSyncStatuses";
            public const string LocationGroupAsBrand = "App.Connect.LocationGroupAsBrand";

            public const string UrbanPiperUserName = "App.Connect.UrbanPiperUserName";
            public const string UrbanPiperApiKey = "App.Connect.UrbanPiperApiKey";
            public const string UrbanPiperPriceTag = "App.Connect.UrbanPiperPriceTag";
            public const string UrbanPiperUrl = "App.Connect.UrbanPiperUrl";
            public const string UrbanPiperMenu = "App.Connect.UrbanPiperMenu";

            public const string DeliveryHeroUser = "App.Connect.DeliveryHeroUser";
            public const string DeliveryHeroPassword = "App.Connect.DeliveryHeroPassword";
            public const string DeliveryHeroUrl = "App.Connect.DeliveryHeroUrl";
            public const string DeliveryHeroPriceTag = "App.Connect.DeliveryHeroPriceTag";
            public const string DeliveryHeroPushMenuUrl = "App.Connect.DeliveryHeroPushMenuUrl";

            public const string BucketName = "App.Connect.BucketName";
            public const string ServerUrl = "App.Connect.ServerUrl";
            public const string AccessKey = "App.Connect.AccessKey";
            public const string SecretKey = "App.Connect.SecretKey";

            public const string Cloudinary = "App.Connect.Cloudinary";
            public const string SendEmailOnWorkDayClose = "App.Connect.SendEmailOnWorkDayClose";

            public const string CreditSaleReport = "App.Connect.CreditSaleReport";

        }

        public static class CaterSettings
        {
            public const string CaterMenu = "App.Cater.CaterMenu";

        }

        public static class ShortMessageSettings
        {
            public const string MessageType = "App.Connect.ShortMessage.MessageType";
            public const string MessageSetting = "App.Connect.ShortMessage.MessageSetting";
        }

        public static class EngageSettings
        {
            public const string PointsAccumulation = "App.Engage.PointsAccumulation";
            public const string PointsAccumulationFactor = "App.Engage.PointsAccumulationFactor";

            public const string PointsRedemption = "App.Engage.PointsRedemption";
            public const string PointsRedemptionFactor = "App.Engage.PointsRedemptionFactor";

            public const string OnBoardMemberFromPos = "App.Engage.OnBoardMemberFromPOS";
            public const string CalculatePoints = "App.Engage.CalculatePoints";

            public const string CalculatePointsOnOrder = "App.Engage.CalculatePointsOnOrder";
            public const string CalculatePointsOnMultiple = "App.Engage.CalculatePointsOnMultiple";

            public const string AutoGenerateMemberCode = "App.Engage.AutoGenerateMemberCode";
            public const string CodeFormat = "App.Engage.CodeFormat";
            public const string CreateNewUserOnActivation = "App.Engage.CreateNewUserOnActivation";
            public const string MemberUserRoleID = "App.Engage.MemberUserRoleID";
            public const string MemberUserRoleName = "App.Engage.MemberUserRoleName";

            public const string DefaultCity = "App.Engage.DefaultCity";
            public const string DefaultState = "App.Engage.DefaultState";
            public const string DefaultCountry = "App.Engage.DefaultCountry";
        }

        public static class HouseSettings
        {
            public const string SupplierMaterialTightlyCoupled = "App.House.SupplierMaterialTightlyCoupled";
            public const string DirectTransferAllowed = "App.House.DirectTransferAllowed";
            public const string InvoiceSupplierRateChangeFlag = "App.House.InvoiceSupplierRateChangeFlag";
            public const string TallyIntegrationFlag = "App.House.TallyIntegrationFlag";
            public const string ExtraMaterialReceivedInDc = "App.House.ExtraMaterialReceivedInDc";
            public const string InterTransferValueShown = "App.House.InterTransferValueShown";
            public const string SmartPrintOutExists = "App.House.SmartPrintOutExists";
            public const string MessageNotification = "App.House.MessageNotification";
            public const string SoftMessageNotification = "App.House.SoftMessageNotification";
            public const string ForceAdjustmentWithDayClose = "App.House.ForceAdjustmentWithDayClose";
            public const string MaterialCostBasedOnLastPurchase = "App.House.MaterialCostBasedOnLastPurchase";
            public const string Decimals = "App.House.Decimals";
            public const string IsMaterialCodeTreatedAsBarCode = "App.House.IsMaterialCodeTreatedAsBarCode";
            public const string DefaultAveragePriceTagRefId = "App.House.DefaultAveragePriceTagRefId";
            public const string NoOfMonthAvgTaken = "App.House.NoOfMonthAvgTaken";
            public const string IsOmitFOCMaterialQuantities = "App.House.IsOmitFOCMaterialQuantities";
            public const string IsCalculateAverageRateWithoutTax = "App.House.IsCalculateAverageRateWithoutTax";
            public const string AbsentDeductionPerDay = "App.House.AbsentDeductionPerDay";
            public const string IsInwardPurchaseOrderReferenceFromOtherErpRequired = "App.House.IsInwardPurchaseOrderReferenceFromOtherErpRequired";
            public const string IsInwardInvoiceNumberReferenceFromOtherErpRequired = "App.House.IsInwardInvoiceNumberReferenceFromOtherErpRequired";
            public const string OmitQueueInOrderWhileAutoPoRequest = "App.House.OmitQueueInOrderWhileAutoPoRequest";
            public const string MaterialCountToUpdate = "App.House.MaterialCountToUpdate";

            public const string WhileClosingStockTakenCanAllUnitAllowed = "App.House.WhileClosingStockTakenCanAllUnitAllowed";
            public const string BackGroundDayCloseCanBeAllowed = "App.House.BackGroundDayCloseCanBeAllowed";
            public const string TransferRequestLockHours = "App.House.TransferRequestLockHours";
            public const string TransferRequestGraceHours = "App.House.TransferRequestGraceHours";
            public const string PurchaseOrderCCMailList = "App.House.PurchaseOrderCCMailList";
        }

        public static class TouchSaleSettings
        {
            public const string Currency = "App.Connect.Compact.Currency";
            public const string Decimals = "App.Connect.Compact.Decimals";
            public const string Rounding = "App.Connect.Compact.Rounding";
            public const string TaxInclusive = "App.Connect.Compact.TaxInclusive";
            public const string ReceiptHeader = "App.Connect.Compact.ReceiptHeader";
            public const string ReceiptFooter = "App.Connect.Compact.ReceiptFooter";

            public const string IsActivity = "App.Connect.Compact.IsActivity";
            public const string IsReport = "App.Connect.Compact.IsReport";
            public const string IsBlindShift = "App.Connect.Compact.IsBlindShift";
            public const string DuplicateReceipt = "App.Connect.Compact.DuplicateReceipt";
        }

        public static class SwipeSettings
        {
            public const string DepositRequiredDefault = "App.Swipe.DepositRequiredDefault";
            public const string DepositAmountDefault = "App.Swipe.DepositAmountDefault";
            public const string ExpiryDaysFromIssueDefault = "App.Swipe.ExpiryDaysFromIssueDefault";
            public const string RefundAllowedDefault = "App.Swipe.RefundAllowedDefault";

            public const string CanIssueCardDirectly = "App.Swipe.CanIssueCardDirectly";
        }

        public static class AddOnSettings
        {
            public const string QBRefreshToken = "App.AddOn.QB.RefreshToken";
            public const string QBAccessTokenExpiresAt = "App.AddOn.QB.AccessTokenExpiresAt";
            public const string QBRefreshTokenExpiresAt = "App.AddOn.QB.RefreshTokenExpiresAt";
        }

        public static class TickSettings
        {
            public const string IsBiometricAttendanceRequired = "App.Tick.IsBiometricAttendanceRequired";
        }

        public class PasswordPolicySettings
        {
            public const string Min_Length = "App.PasswordPolicy.Min_Length";
            public const string Max_Length = "App.PasswordPolicy.Max_Length";
            public const string Min_LCL = "App.PasswordPolicy.Min_LCL";
            public const string Min_UCL = "App.PasswordPolicy.Min_UCL";
            public const string Min_AC = "App.PasswordPolicy.Min_AC";

            public const string Min_SC = "App.PasswordPolicy.Min_SC";
            public const string Size = "App.PasswordPolicy.Size";
            public const string Impermissible_Passwords = "App.PasswordPolicy.Impermissible_Passwords";
            public const string Maximum_IT = "App.PasswordPolicy.Maximum_IT";
            public const string Password_VP = "App.PasswordPolicy.Password_VP";

            public const string Allow_LIDPP = "App.PasswordPolicy.Allow_LIDPP"; // Allow Logon ID as Part of Password
            public const string Allow_OPPP = "App.PasswordPolicy.Allow_OPPP";   // Allow Old Password as Part of New Password
            public const string Allow_UCOP = "App.PasswordPolicy.Allow_UCOP";   // Allow Users to Change Their Own Passwords
            public const string Enforce = "App.PasswordPolicy.Enforce";
            public const string Maximum_NFLA = "App.PasswordPolicy.Maximum_NFLA";

            public const string Suggest_Message = "App.PasswordPolicy.Suggest_Message";
        }

      
    }
}