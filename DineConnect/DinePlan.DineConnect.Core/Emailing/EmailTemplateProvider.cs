﻿using System.Reflection;
using System.Text;
using Abp.Dependency;
using Abp.IO.Extensions;

namespace DinePlan.DineConnect.Emailing
{
    public class EmailTemplateProvider : IEmailTemplateProvider, ITransientDependency
    {
        public string GetDefaultTemplate()
        {
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("DinePlan.DineConnect.Emailing.EmailTemplates.default.html"))
            {
                var bytes = stream.GetAllBytes();
                return Encoding.UTF8.GetString(bytes, 3, bytes.Length - 3);
            }
        }


        public string GetDefaultPurchaseOrderTemplate()
        {
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("DinePlan.DineConnect.Emailing.EmailTemplates.PoTemplate.html"))
            {
                var bytes = stream.GetAllBytes();
                return Encoding.UTF8.GetString(bytes, 3, bytes.Length - 3);
            }
        }

        public string GetReceiptTemplate()
        {
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("DinePlan.DineConnect.Emailing.EmailTemplates.receipt.html"))
            {
                var bytes = stream.GetAllBytes();
                return Encoding.UTF8.GetString(bytes, 3, bytes.Length - 3);
            }
        }

        public string GetShortMessageTemplate()
        {
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("DinePlan.DineConnect.Emailing.EmailTemplates.shortmessage.html"))
            {
                var bytes = stream.GetAllBytes();
                return Encoding.UTF8.GetString(bytes, 3, bytes.Length - 3);
            }
        }
    }
}