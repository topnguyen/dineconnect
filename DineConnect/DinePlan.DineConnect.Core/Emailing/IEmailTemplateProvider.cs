﻿namespace DinePlan.DineConnect.Emailing
{
    public interface IEmailTemplateProvider
    {
        string GetDefaultTemplate();

        string GetDefaultPurchaseOrderTemplate();

        string GetReceiptTemplate();
        string GetShortMessageTemplate();

    }
}
