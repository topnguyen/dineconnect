﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;



namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggLocationItemManager : DineConnectServiceBase, IDelAggLocationItemManager, ITransientDependency
    {
        private readonly IRepository<DelAggLocationItem> _delAggLocationItemRepo;

        public DelAggLocationItemManager(IRepository<DelAggLocationItem> delAggLocationItemRepo)
        {
            _delAggLocationItemRepo = delAggLocationItemRepo;
        }

        public async Task<IdentityResult> CreateSync(DelAggLocationItem delAggLocationItem)
        {
            var existed = _delAggLocationItemRepo.GetAll().Any(a =>
                                             a.DelAggLocMappingId == delAggLocationItem.DelAggLocMappingId
                                             && a.DelAggItemId == delAggLocationItem.DelAggItemId);
            if (existed)
            {
                string[] strArrays = { L("NameAlreadyExists") };
                var success = AbpIdentityResult.Failed(strArrays);
                return success;
            }
            await _delAggLocationItemRepo.InsertAndGetIdAsync(delAggLocationItem);
            return IdentityResult.Success;

        }

        public async Task<IdentityResult> UpdateSync(DelAggLocationItem delAggLocationItem)
        {
            
            await _delAggLocationItemRepo.UpdateAsync(delAggLocationItem);
            return IdentityResult.Success;

        }
    }
}