﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;



namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggLocationGroupManager : DineConnectServiceBase, IDelAggLocationGroupManager, ITransientDependency
    {
        private readonly IRepository<DelAggLocationGroup> _delAggLocationGroupRepo;

        public DelAggLocationGroupManager(IRepository<DelAggLocationGroup> locationgroup)
        {
            _delAggLocationGroupRepo = locationgroup;
        }

        public async Task<IdentityResult> CreateSync(DelAggLocationGroup delAggLocationGroup)
        {
            //  if the New Addition
            if (delAggLocationGroup.Id == 0)
            {
                if (_delAggLocationGroupRepo.GetAll().Any(a => a.Name.Equals(delAggLocationGroup.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _delAggLocationGroupRepo.InsertAndGetIdAsync(delAggLocationGroup);
                return IdentityResult.Success;

            }
            else
            {
                List<DelAggLocationGroup> lst = _delAggLocationGroupRepo.GetAll().Where(a => a.Id.Equals(delAggLocationGroup.Id) && a.Id != delAggLocationGroup.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }


           
        }
    }
}