﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Cluster;


namespace DinePlan.DineConnect.House.Impl
{
    public class DelAggCategoryManager : DineConnectServiceBase, IDelAggCategoryManager, ITransientDependency
    {

        private readonly IRepository<DelAggCategory> _delAggCategoryRepo;

        public DelAggCategoryManager(IRepository<DelAggCategory> delAggCategory)
        {
            _delAggCategoryRepo = delAggCategory;
        }

        public async Task<IdentityResult> CreateSync(DelAggCategory delAggCategory)
        {
            //  if the New Addition
            if (delAggCategory.Id == 0)
            {
                if (_delAggCategoryRepo.GetAll().Any(a => a.Name.Equals(delAggCategory.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _delAggCategoryRepo.InsertAndGetIdAsync(delAggCategory);
                return IdentityResult.Success;

            }
            else
            {
                List<DelAggCategory> lst = _delAggCategoryRepo.GetAll().Where(a => a.Name.Equals(delAggCategory.Name) && a.Id != delAggCategory.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}