﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;



namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggModifierManager : DineConnectServiceBase, IDelAggModifierManager, ITransientDependency
    {
        private readonly IRepository<DelAggModifier> _delAggModifierRepo;

        public DelAggModifierManager(IRepository<DelAggModifier> modifier)
        {
            _delAggModifierRepo = modifier;
        }

        public async Task<IdentityResult> CreateSync(DelAggModifier delAggModifier)
        {
            //  if the New Addition
            if (delAggModifier.Id == 0)
            {
                if (_delAggModifierRepo.GetAll().Any(a => a.Name.Equals(delAggModifier.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _delAggModifierRepo.InsertAndGetIdAsync(delAggModifier);
                return IdentityResult.Success;

            }
            else
            {
                List<DelAggModifier> lst = _delAggModifierRepo.GetAll().Where(a => a.Id.Equals(delAggModifier.Id) && a.Id != delAggModifier.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }


           
        }
    }
}