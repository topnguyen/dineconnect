﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggVariantManager : DineConnectServiceBase, IDelAggVariantManager, ITransientDependency
    {
        private readonly IRepository<DelAggVariant> _delAggVariantRepo;

        public DelAggVariantManager(IRepository<DelAggVariant> variant)
        {
            _delAggVariantRepo = variant;
        }

        public async Task<IdentityResult> CreateSync(DelAggVariant delAggVariant)
        {
            //  if the New Addition
            if (delAggVariant.Id == 0)
            {
                if (_delAggVariantRepo.GetAll().Any(a => a.Name.Equals(delAggVariant.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _delAggVariantRepo.InsertAndGetIdAsync(delAggVariant);
                return IdentityResult.Success;

            }
            else
            {
                List<DelAggVariant> lst = _delAggVariantRepo.GetAll().Where(a => a.Id.Equals(delAggVariant.Id) && a.Id != delAggVariant.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }


           
        }
    }
}