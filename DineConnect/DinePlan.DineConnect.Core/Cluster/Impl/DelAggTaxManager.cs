﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Cluster;

namespace DinePlan.DineConnect.House.Impl
{
    public class DelAggTaxManager : DineConnectServiceBase, IDelAggTaxManager, ITransientDependency
    {

        private readonly IRepository<DelAggTax> _delAggTaxRepo;

        public DelAggTaxManager(IRepository<DelAggTax> delAggTax)
        {
            _delAggTaxRepo = delAggTax;
        }

        public async Task<IdentityResult> CreateSync(DelAggTax delAggTax)
        {
            //  if the New Addition
            if (delAggTax.Id == 0)
            {
                if (_delAggTaxRepo.GetAll().Any(a => a.Name.Equals(delAggTax.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _delAggTaxRepo.InsertAndGetIdAsync(delAggTax);
                return IdentityResult.Success;

            }
            else
            {
                List<DelAggTax> lst = _delAggTaxRepo.GetAll().Where(a => a.Name.Equals(delAggTax.Name) && a.Id != delAggTax.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
