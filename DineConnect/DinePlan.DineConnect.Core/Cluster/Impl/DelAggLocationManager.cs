﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;



namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggLocationManager : DineConnectServiceBase, IDelAggLocationManager, ITransientDependency
    {
        private readonly IRepository<DelAggLocation> _delAggLocationRepo;

        public DelAggLocationManager(IRepository<DelAggLocation> location)
        {
            _delAggLocationRepo = location;
        }

        public async Task<IdentityResult> CreateSync(DelAggLocation delAggLocation)
        {
            if (delAggLocation.Id == 0)
            {
                if (_delAggLocationRepo.GetAll().Any(a => a.Id.Equals(delAggLocation.Id) && a.LocationId.Equals(delAggLocation.Location) && a.DelAggLocationGroupId==delAggLocation.DelAggLocationGroupId))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _delAggLocationRepo.InsertAndGetIdAsync(delAggLocation);
                return IdentityResult.Success;

            }
            else
            {
                List<DelAggLocation> lst = _delAggLocationRepo.GetAll().Where(a => a.Id.Equals(delAggLocation.Id) && a.Id != delAggLocation.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }

           
        }
    }
}