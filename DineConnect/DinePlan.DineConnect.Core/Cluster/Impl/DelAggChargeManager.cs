﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Cluster;


namespace DinePlan.DineConnect.House.Impl
{
    public class DelAggChargeManager : DineConnectServiceBase, IDelAggChargeManager, ITransientDependency
    {

        private readonly IRepository<DelAggCharge> _delAggChargeRepo;

        public DelAggChargeManager(IRepository<DelAggCharge> delAggCharge)
        {
            _delAggChargeRepo = delAggCharge;
        }

        public async Task<IdentityResult> CreateSync(DelAggCharge delAggCharge)
        {
            //  if the New Addition
            if (delAggCharge.Id == 0)
            {
                if (_delAggChargeRepo.GetAll().Any(a => a.Name.Equals(delAggCharge.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _delAggChargeRepo.InsertAndGetIdAsync(delAggCharge);
                return IdentityResult.Success;

            }
            else
            {
                List<DelAggCharge> lst = _delAggChargeRepo.GetAll().Where(a => a.Name.Equals(delAggCharge.Name) && a.Id != delAggCharge.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
