﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;



namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggLocMappingManager : DineConnectServiceBase, IDelAggLocMappingManager, ITransientDependency
    {
        private readonly IRepository<DelAggLocMapping> _delAggLocMappingRepo;

        public DelAggLocMappingManager(IRepository<DelAggLocMapping> locationmapping)
        {
            _delAggLocMappingRepo = locationmapping;
        }

        public async Task<IdentityResult> CreateSync(DelAggLocMapping delAggLocMapping)
        {
            //  if the New Addition
            if (delAggLocMapping.Id == 0)
            {
                if (_delAggLocMappingRepo.GetAll().Any(a => a.Id.Equals(delAggLocMapping.Id) && a.Name.Contains(delAggLocMapping.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _delAggLocMappingRepo.InsertAndGetIdAsync(delAggLocMapping);
                return IdentityResult.Success;

            }
            else
            {
                List<DelAggLocMapping> lst = _delAggLocMappingRepo.GetAll().Where(a => a.Id.Equals(delAggLocMapping.Id) && a.Id != delAggLocMapping.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }

           
        }
    }
}