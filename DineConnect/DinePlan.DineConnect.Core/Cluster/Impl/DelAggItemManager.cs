﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Cluster;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class DelAggItemManager : DineConnectServiceBase, IDelAggItemManager, ITransientDependency
    {

        private readonly IRepository<DelAggItem> _delAggItemRepo;

        public DelAggItemManager(IRepository<DelAggItem> delAggItem)
        {
            _delAggItemRepo = delAggItem;
        }

        public async Task<IdentityResult> CreateSync(DelAggItem delAggItem)
        {
            //  if the New Addition
            if (delAggItem.Id == 0)
            {
                if (_delAggItemRepo.GetAll().Any(a => a.Name.Equals(delAggItem.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _delAggItemRepo.InsertAndGetIdAsync(delAggItem);
                return IdentityResult.Success;

            }
            else
            {
                List<DelAggItem> lst = _delAggItemRepo.GetAll().Where(a => a.Name.Equals(delAggItem.Name) && a.Id != delAggItem.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
