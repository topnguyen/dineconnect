﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DinePlan.DineConnect.Cluster;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public class DelAggItemGroupManager : DineConnectServiceBase, IDelAggItemGroupManager, ITransientDependency
    {

        private readonly IRepository<DelAggItemGroup> _delAggItemGroupRepo;

        public DelAggItemGroupManager(IRepository<DelAggItemGroup> delAggItemGroup)
        {
            _delAggItemGroupRepo = delAggItemGroup;
        }

        public async Task<IdentityResult> CreateSync(DelAggItemGroup delAggItemGroup)
        {
            //  if the New Addition
            if (delAggItemGroup.Id == 0)
            {
                if (_delAggItemGroupRepo.GetAll().Any(a => a.Id.Equals(delAggItemGroup.Id)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _delAggItemGroupRepo.InsertAndGetIdAsync(delAggItemGroup);
                return IdentityResult.Success;

            }
            else
            {
                List<DelAggItemGroup> lst = _delAggItemGroupRepo.GetAll().Where(a => a.Id.Equals(delAggItemGroup.Id) && a.Id != delAggItemGroup.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }
        }

    }
}
