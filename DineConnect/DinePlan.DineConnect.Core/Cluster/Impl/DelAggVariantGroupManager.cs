﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Cluster.Impl
{
    public class DelAggVariantGroupManager : DineConnectServiceBase, IDelAggVariantGroupManager, ITransientDependency
    {
        private readonly IRepository<DelAggVariantGroup> _delAggVariantGroupRepo;

        public DelAggVariantGroupManager(IRepository<DelAggVariantGroup> variantgroup)
        {
            _delAggVariantGroupRepo = variantgroup;
        }

        public async Task<IdentityResult> CreateSync(DelAggVariantGroup delAggVariantGroup)
        {
            //  if the New Addition
            if (delAggVariantGroup.Id == 0)
            {
                if (_delAggVariantGroupRepo.GetAll().Any(a => a.Name.Equals(delAggVariantGroup.Name)))
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                await _delAggVariantGroupRepo.InsertAndGetIdAsync(delAggVariantGroup);
                return IdentityResult.Success;

            }
            else
            {
                List<DelAggVariantGroup> lst = _delAggVariantGroupRepo.GetAll().Where(a => a.Name.Equals(delAggVariantGroup.Name) && a.Id != delAggVariantGroup.Id).ToList();
                if (lst.Count > 0)
                {
                    string[] strArrays = { L("NameAlreadyExists") };
                    var success = AbpIdentityResult.Failed(strArrays);
                    return success;
                }
                return IdentityResult.Success;
            }


           
        }
    }
}