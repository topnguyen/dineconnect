﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Cluster;


namespace DinePlan.DineConnect.House.Impl
{
    public interface IDelAggImageManager
    {
        Task<IdentityResult> CreateSync(DelAggImage delAggImage);
    }
}