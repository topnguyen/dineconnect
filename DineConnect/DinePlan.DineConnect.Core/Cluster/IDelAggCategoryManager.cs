﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Cluster;


namespace DinePlan.DineConnect.House.Impl
{
    public interface IDelAggCategoryManager
    {
        Task<IdentityResult> CreateSync(DelAggCategory delAggCategory);
    }
}