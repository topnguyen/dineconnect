﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggVariantManager
    {
        Task<IdentityResult> CreateSync(DelAggVariant delAggVariant);
    }
}
