﻿
using System.Threading.Tasks;
using DinePlan.DineConnect.Cluster;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IDelAggTaxManager
    {
        Task<IdentityResult> CreateSync(DelAggTax delAggTax);
    }
}