﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggModifierManager
    {
        Task<IdentityResult> CreateSync(DelAggModifier delAggModifier);
    }
}
