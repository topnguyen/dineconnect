﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Cluster;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.House.Impl
{
    public interface IDelAggItemManager
    {
        Task<IdentityResult> CreateSync(DelAggItem delAggItem);
    }
}