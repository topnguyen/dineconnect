﻿using DinePlan.DineConnect.Connect.MultiLingual;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggLanguageManager
    {
        Task<IdentityResult> CreateOrUpdateSync(DelAggLanguage delAggLanguage);
    }
}
