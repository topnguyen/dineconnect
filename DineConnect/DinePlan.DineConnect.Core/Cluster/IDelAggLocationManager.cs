﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggLocationManager
    {
        Task<IdentityResult> CreateSync(DelAggLocation delAggLocation);
    }
}
