﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Cluster
{
    public interface IDelAggModifierGroupManager
    {
        Task<IdentityResult> CreateSync(DelAggModifierGroup delAggModifierGroup);
    }
}
