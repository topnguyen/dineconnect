﻿
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using DinePlan.DineConnect.Cluster;


namespace DinePlan.DineConnect.House.Impl
{
    public interface IDelTimingGroupManager
    {
        Task<IdentityResult> CreateSync(DelTimingGroup delTimingGroup);
    }
}

