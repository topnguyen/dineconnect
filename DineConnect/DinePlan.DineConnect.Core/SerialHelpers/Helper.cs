﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace DinePlan.DineConnect.SerialHelpers
{
    public class Serializer
    {
        public T Deserialize<T>(string input) where T : class
        {
            var ser = new XmlSerializer(typeof(T));

            using (var sr = new StringReader(input))
            {
                return (T) ser.Deserialize(sr);
            }
        }

        public T DeserializeToObject<T>(string filepath) where T : class
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using (StreamReader sr = new StreamReader(filepath))
            {
                return (T) ser.Deserialize(sr);
            }
        }
    }


    public class XmlStringConvert
    {
        public static string ConvertStringToXml(string content)
        {
            if (!string.IsNullOrEmpty(content))
            {
                return content.Replace("&lt;", "<")
                    .Replace("&amp;", "&")
                    .Replace("&gt;", ">")
                    .Replace("&quot;", "\"")
                    .Replace("&apos;", "'");
            }
            return "";
        }
    }
}