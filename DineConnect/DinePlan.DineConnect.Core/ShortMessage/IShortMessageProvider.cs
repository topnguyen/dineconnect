﻿namespace DinePlan.DineConnect.ShortMessage
{
    public interface IShortMessageProvider
    {
        void SendMessage(string message, string numbers, int messageType, string settings);
    }
}
