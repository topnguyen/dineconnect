﻿using System;
using System.Net;
using System.Reflection;
using System.Text;
using Abp.Dependency;
using Abp.IO.Extensions;

namespace DinePlan.DineConnect.ShortMessage.Impl
{
    public class ShortMessageProvider : IShortMessageProvider, ITransientDependency
    {
        public void SendMessage(string message, string numbers, int messageType, string settings)
        {
            if (!string.IsNullOrEmpty(message))
            {
                if (messageType == 1)
                {
                    settings = settings.Replace("{NUMBER}", numbers);
                    settings = settings.Replace("{MESSAGE}", message);
                    try
                    {
                        WebClient webClient = new WebClient();
                        webClient.UploadDataAsync(new Uri(settings), Encoding.GetEncoding(857).GetBytes(settings));
                    }
                    catch (Exception exception)
                    {
                        var mess = exception.Message;
                    }
                }
            }
        }
    }
}