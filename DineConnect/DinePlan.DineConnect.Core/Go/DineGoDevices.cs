﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Filter;

namespace DinePlan.DineConnect.Go
{
    [Table("DineGoDevices")]
    public class DineGoDevice : ConnectMultiTenantEntity
    {
        public string Name { get; set; }
        public int IdleTime { get; set; }
        public bool PushToConnect{ get; set; }

        public string OrderLink { get; set; }
        public string PinCode { get; set; }

        private IList<DineGoBrand> _dineGoBrands;

        private IList<DineGoPaymentType> _dineGoPaymentTypes;
        public virtual IList<DineGoBrand> DineGoBrands
        {
            get { return _dineGoBrands; }
            set { _dineGoBrands = value; }
        }

        public virtual IList<DineGoPaymentType> DineGoPaymentTypes
        {
            get { return _dineGoPaymentTypes; }
            set { _dineGoPaymentTypes = value; }
        }
        public DineGoDevice()
        {
            _dineGoBrands = new List<DineGoBrand>();
            _dineGoPaymentTypes = new List<DineGoPaymentType>();
        }
        public string ThemeSettings { get; set; }
        public Guid ChangeGuid { get; set; }

    }

    public class DeviceThemeSettings
    {
        public string IdleContents { get; set; }
        public string BannerContents { get; set; }
        public string BackgroundColor { get; set; }
        public string Logo { get; set; }
    }

    [Table("DineGoBrands")]
    public class DineGoBrand : ConnectMultiTenantEntity
    {
        public string Label { get; set; }

        [ForeignKey("LocationId")]
        public Location Location { get; set; }
        public virtual int LocationId { get; set; }
        [ForeignKey("ScreenMenuId")]
        public ScreenMenu ScreenMenu { get; set; }
        public virtual int ScreenMenuId { get; set; }

        private IList<DineGoDevice> _dineGoDevices;
        private IList<DineGoDepartment> _dineGoDepartments;
        public virtual IList<DineGoDevice> DineGoDevices
        {
            get { return _dineGoDevices; }
            set { _dineGoDevices = value; }
        }
       
        public virtual IList<DineGoDepartment> DineGoDepartments
        {
            get { return _dineGoDepartments; }
            set { _dineGoDepartments = value; }
        }

        public DineGoBrand()
        {
            _dineGoDevices = new List<DineGoDevice>();
            _dineGoDepartments = new List<DineGoDepartment>();
        }
    }

    [Table("DineGoDepartments")]
    public class DineGoDepartment : ConnectMultiTenantEntity
    {
        public string Label { get; set; }

        [ForeignKey("DepartmentId")]
        public Department Department { get; set; }
        public virtual int DepartmentId { get; set; }

        private IList<DineGoCharge> _dineGoCharges;
        private IList<DineGoBrand> _dineGoBrands;
        public virtual IList<DineGoCharge> DineGoCharges
        {
            get { return _dineGoCharges; }
            set { _dineGoCharges = value; }
        }
        public virtual IList<DineGoBrand> DineGoBrands
        {
            get { return _dineGoBrands; }
            set { _dineGoBrands = value; }
        }
        public DineGoDepartment()
        {
            _dineGoCharges = new List<DineGoCharge>();
            _dineGoBrands = new List<DineGoBrand>();
        }
    }

    [Table("DineGoPaymentTypes")]
    public class DineGoPaymentType : ConnectMultiTenantEntity
    {
        public string Label { get; set; }

        [ForeignKey("PaymentTypeId")]
        public PaymentType PaymentType { get; set; }
        public virtual int PaymentTypeId { get; set; }

        private IList<DineGoDevice> _dineGoDevices;
        public virtual IList<DineGoDevice> DineGoDevices
        {
            get { return _dineGoDevices; }
            set { _dineGoDevices = value; }
        }
        public DineGoPaymentType()
        {
            _dineGoDevices = new List<DineGoDevice>();
        }

    }

    [Table("DineGoCharges")]
    public class DineGoCharge : ConnectMultiTenantEntity
    {
        public string Label { get; set; }
        public bool IsPercent { get; set; }
        public bool TaxIncluded { get; set; }
        public decimal ChargeValue { get; set; }

        [ForeignKey("TransactionTypeId")]
        public TransactionType TransactionType { get; set; }
        public virtual int TransactionTypeId { get; set; }


        private IList<DineGoDepartment> _dineGoDepartments;

        public virtual IList<DineGoDepartment> DineGoDepartments
        {
            get { return _dineGoDepartments; }
            set { _dineGoDepartments = value; }
        }

        public DineGoCharge()
        {
            _dineGoDepartments = new List<DineGoDepartment>();
        }

    }
}