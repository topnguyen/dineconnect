﻿namespace DinePlan.OdooService.Infrastructure.Interfaces
{
    public interface IOdooConverter<out T> where T : IOdooObject, new()
    {
        T GetObject();

    }
}
