﻿namespace DinePlan.OdooService.Infrastructure.Interfaces
{
    public interface IOdooObject
    {
        
        int Id { get; set; }

        string Name { get; set; }
    }
}
