﻿using CookComputing.XmlRpc;

namespace DinePlan.OdooService.Infrastructure.Interfaces
{
    public interface IOdooDatabase : IXmlRpcProxy
    {
        [XmlRpcMethod("list")]
        object List();
    }
}
