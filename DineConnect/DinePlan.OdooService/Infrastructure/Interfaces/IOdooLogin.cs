﻿using CookComputing.XmlRpc;

namespace DinePlan.OdooService.Infrastructure.Interfaces
{
    public interface IOdooLogin : IXmlRpcProxy
    {
        [XmlRpcMethod("authenticate")]
        object Login(string database, string username, string password, object param);
    }
}
