﻿namespace DinePlan.OdooService.Infrastructure.Enums
{
    public enum OdooType
    {
        Undefined,
        Boolean,
        Integer,
        Float,
        Char,
        Text,
        Date,
        Datetime,
        Binary,
        Selection,
        Many2Many,
        Many2One
    }
}
