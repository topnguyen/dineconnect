﻿using System;

namespace DinePlan.OdooService.Models
{
    public class ResultSet
    {
        public object[] Data { get; set; }

        public ResultSet(Object[] data)
        {
            this.Data = data;
        }
    }
}
