﻿using OpenHtmlToPdf.Assets;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace OpenHtmlToPdf
{
    internal static class HtmlToPdfConverterProcess
    {
        public static void ConvertToPdf(
            string html,
            IDictionary<string, string> globalSettings,
            IDictionary<string, string> objectSettings, string execFile)
        {
            Convert(ToConversionSource(html, globalSettings, objectSettings, execFile));
        }

        private static void Convert(ConversionSource conversionSource)
        {
            var processStartInfo = GetProcessStartInfo(conversionSource.PdfExecutable);
            var process = Process.Start(processStartInfo);

            process.Convert(conversionSource);
        }

        private static void Convert(this Process process, ConversionSource conversionSource)
        {
            process.WriteToStandardInput(conversionSource);
            process.WaitForExit();
            RaiseExceptionIfErrorOccured(process);
        }

        private static void RaiseExceptionIfErrorOccured(Process process)
        {
            if (process.ExitCode != 0)
                throw new PdfDocumentCreationFailedException(process.StandardError.ReadToEnd());
        }

        private static void WriteToStandardInput(this Process process, ConversionSource conversionSource)
        {
            process.StandardInput.Write(SerializeToBase64EncodedString(conversionSource));
            process.StandardInput.Close();
        }

        private static string SerializeToBase64EncodedString(ConversionSource conversionSource)
        {
            var result = SerializeToJson(conversionSource);
            return System.Convert.ToBase64String(Encoding.UTF8.GetBytes(result));
        }

        private static string SerializeToJson(ConversionSource conversionSource)
        {
            using (var stream = new MemoryStream())
            {
                new DataContractJsonSerializer(typeof(ConversionSource)).WriteObject(stream, conversionSource);
                stream.Position = 0;
                return new StreamReader(stream).ReadToEnd();
            }
        }

        private static ProcessStartInfo GetProcessStartInfo(string pdfEFile)
        {

            var process = new ProcessStartInfo
            {
                FileName = pdfEFile,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true
            };

            return process;
        }

        private static ConversionSource ToConversionSource(string html, IDictionary<string, string> globalSettings,
            IDictionary<string, string> objectSettings, string exe)
        {
            var conversionSource = new ConversionSource
            {
                Html = html,
                GlobalSettings = globalSettings,
                ObjectSettings = objectSettings
            };

            if (!String.IsNullOrEmpty(exe))
            {
                conversionSource.PdfExecutable = exe;
            }
            return conversionSource;
        }
    }
}