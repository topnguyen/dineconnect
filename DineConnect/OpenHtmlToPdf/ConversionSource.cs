﻿using System.Collections.Generic;

namespace OpenHtmlToPdf
{
    public sealed class ConversionSource
    {
        public string Html { get; set; }
        public IDictionary<string, string> GlobalSettings { get; set; }
        public IDictionary<string, string> ObjectSettings { get; set; }
        public string PdfExecutable { get; set; }

    }
}