﻿namespace OpenHtmlToPdf
{
    public interface IPdfDocument
    {
        IPdfDocument WithGlobalSetting(string key, string value);
        IPdfDocument WithObjectSetting(string key, string value);
        byte[] Content();
        byte[] Content(string fileName);
        byte[] Content(string fileName, string exeFile);


    }
}