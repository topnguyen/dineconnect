﻿namespace OpenHtmlToPdf
{
    public class PaperSize
    {
        private readonly Length _width;
        private readonly Length _height;

        public PaperSize(Length width, Length height)
        {
            _width = width;
            _height = height;
        }

       
        public static PaperSize A4 { get { return new PaperSize(210.Millimeters(), 297.Millimeters()); } }
         public static PaperSize A2 { get { return new PaperSize(420.Millimeters(), 594.Millimeters()); } }
      
        public string Width
        {
            get { return _width.SettingString; }
        }

        public string Height
        {
            get { return _height.SettingString; }
        }
    }
}