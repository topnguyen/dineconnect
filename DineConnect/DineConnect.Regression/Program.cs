﻿using System;
using System.IO;
using Abp;
using Abp.Dependency;
using DineConnect.Regression.Code;

namespace DineConnect.Regression
{
    public class Program
    {
        public static void Main(string[] args)
        {
            using (var bootstrapper = new AbpBootstrapper())
            {
                bootstrapper.Initialize();
                var tester = IocManager.Instance.Resolve<Executor>();

                tester.Run();

                Console.Write("Done");
                Console.ReadKey();
            }
        }
    }
}

  
