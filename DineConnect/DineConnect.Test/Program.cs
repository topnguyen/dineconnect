﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.EppPlusHtml;
using OfficeOpenXml;

namespace DineConnect.Test
{
    internal class Program
    {
        private static FileInfo Test001 = new FileInfo(@"C:\_Downloads\Tickets.xlsx");

        private static void Main(string[] args)
        {
            var package = new ExcelPackage(Test001);
            var worksheet = package.Workbook.Worksheets[1];
            string html = worksheet.ToHtml();
            Show(html);
        }

        static void Show(string html)
        {
            string tmpFile = Path.GetTempFileName() + ".html";
            File.WriteAllText(tmpFile, html);
            Process.Start(tmpFile);
        }
    }
}
