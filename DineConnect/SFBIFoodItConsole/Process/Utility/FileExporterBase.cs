﻿using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Extensions;
using DinePlan.DineConnect;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using SFBIFoodItConsole.Process.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace SFBIFoodItConsole.Process.Utility
{
    public abstract class FileExporterBase : ITransientDependency
    {
        protected AppConfig _appConfig;
        public FileExporterBase(AppConfig appConfig)
        {
            _appConfig = appConfig;
        }
        public string _dateTimeFormat = "dd-MM-yyyy HH:mm:ss";
        public int _roundDecimals = 2;
        public string _simpleDateFormat = "dd-MM-yyyy";
        public string _timeFormat = "HH:mm:ss";

        public IAppFolders AppFolders { get; set; }

        protected FileDto CreateExcelPackage(string fileName, Action<ExcelPackage> creator, bool csv = false)
        {
            FileDto file;
            if (csv)
            {
                file = new FileDto(fileName, MimeTypeNames.TextCsv);
            }
            else
            {
                file = new FileDto(fileName, MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            }
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (var excelPackage = GetExcelPackage())
            {
                creator(excelPackage);
                if (csv)
                {
                    SaveAsCsv(excelPackage, file);
                }
                else
                {
                    Save(excelPackage, file);
                }
            }

            return file;
        }

        private static ExcelPackage GetExcelPackage()
        {
            return new ExcelPackage();
        }

        protected void AddHeader(ExcelWorksheet sheet, params string[] headerTexts)
        {
            if (headerTexts.IsNullOrEmpty())
            {
                return;
            }

            for (var i = 0; i < headerTexts.Length; i++)
            {
                AddHeader(sheet, i + 1, headerTexts[i]);
            }
        }

        protected void AddHeader(ExcelWorksheet sheet, bool upper, params string[] headerTexts)
        {
            if (headerTexts.IsNullOrEmpty())
            {
                return;
            }

            for (var i = 0; i < headerTexts.Length; i++)
            {
                AddHeader(sheet, i + 1, upper ? headerTexts[i].ToUpper() : headerTexts[i]);
            }
        }

        protected void AddHeader(ExcelWorksheet sheet, int startingPoint, params string[] headerTexts)
        {
            if (headerTexts.IsNullOrEmpty())
            {
                return;
            }

            for (var i = 0; i < headerTexts.Length; i++)
            {
                AddHeader(sheet, startingPoint, i + 1, headerTexts[i]);
            }
        }

        protected void AddHeaderWithColor(ExcelWorksheet sheet, int startingPoint, Color background, Color foreground, params string[] headerTexts)
        {
            if (headerTexts.IsNullOrEmpty())
            {
                return;
            }

            for (var i = 0; i < headerTexts.Length; i++)
            {
                AddHeaderWithColor(sheet, startingPoint, i + 1, background, foreground, headerTexts[i]);
            }
        }

        protected void AddRow(ExcelWorksheet sheet, int rowIndex, params string[] headerTexts)
        {
            if (headerTexts.IsNullOrEmpty())
            {
                return;
            }

            for (var i = 0; i < headerTexts.Length; i++)
            {
                sheet.Cells[rowIndex, i + 1].Value = headerTexts[i];
            }
        }

        protected void AddHeader(ExcelWorksheet sheet, int columnIndex, string headerText)
        {
            sheet.Cells[1, columnIndex].Value = headerText;
            sheet.Cells[1, columnIndex].Style.Font.Bold = true;
        }

        protected void AddHeader(ExcelWorksheet sheet, int rowIndex, int columnIndex, string headerText)
        {
            sheet.Cells[rowIndex, columnIndex].Value = headerText;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
        }

        protected void AddHeaderWithColor(ExcelWorksheet sheet, int rowIndex, int columnIndex, Color background, Color foreground, string headerText)
        {
            sheet.Cells[rowIndex, columnIndex].Value = headerText;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Color.SetColor(foreground);
            sheet.Cells[rowIndex, columnIndex].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[rowIndex, columnIndex].Style.Fill.BackgroundColor.SetColor(background);
        }

        protected void AddHeader(ExcelWorksheet sheet, int rowIndex, int columnIndex, decimal headerText)
        {
            sheet.Cells[rowIndex, columnIndex].Value = headerText;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
        }

        protected void AddHeaderWithMerge(ExcelWorksheet sheet, int rowIndex, int columnIndex, string headerText)
        {
            sheet.Cells[rowIndex, columnIndex].Value = headerText;
            sheet.Cells[rowIndex, columnIndex, rowIndex, columnIndex + 1].Merge = true;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
        }

        protected void AddHeaderWithMergeWithColumnCountColor(ExcelWorksheet sheet, int rowIndex, int columnIndex, int columnCount, string headerText, Color background, Color foreground)
        {
            if (columnCount == 0)
                columnCount = 1;

            sheet.Cells[rowIndex, columnIndex].Value = headerText;
            sheet.Cells[rowIndex, columnIndex, rowIndex, columnIndex + columnCount].Merge = true;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Color.SetColor(foreground);
            sheet.Cells[rowIndex, columnIndex].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[rowIndex, columnIndex].Style.Fill.BackgroundColor.SetColor(background);
        }

        protected void AddHeaderWithMergeWithColumnCount(ExcelWorksheet sheet, int rowIndex, int columnIndex, int columnCount, string headerText)
        {
            if (columnCount == 0)
                columnCount = 1;

            sheet.Cells[rowIndex, columnIndex].Value = headerText;
            sheet.Cells[rowIndex, columnIndex, rowIndex, columnIndex + columnCount].Merge = true;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;

        }

        protected void AddHeaderWithMergeWithColumnCountAndAlignment(ExcelWorksheet sheet, int rowIndex, int columnIndex, int columnCount, string headerText, ExcelHorizontalAlignment alignment)
        {
            if (columnCount == 0)
                columnCount = 1;

            sheet.Cells[rowIndex, columnIndex].Value = headerText;
            sheet.Cells[rowIndex, columnIndex, rowIndex, columnIndex + columnCount].Merge = true;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = alignment;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
        }

        protected void AddHeaderWithMergeWithColumnCount(ExcelWorksheet sheet, int rowIndex, int columnIndex, int columnCount, decimal headerText)
        {
            if (columnCount == 0)
                columnCount = 1;

            sheet.Cells[rowIndex, columnIndex].Value = headerText;
            sheet.Cells[rowIndex, columnIndex, rowIndex, columnIndex + columnCount].Merge = true;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
        }

        protected void AddHeaderWithMerge(ExcelWorksheet sheet, int rowIndex, int columnIndex, decimal headerText)
        {
            sheet.Cells[rowIndex, columnIndex].Value = Math.Round(headerText, 2, MidpointRounding.AwayFromZero);
            sheet.Cells[rowIndex, columnIndex, rowIndex, columnIndex + 1].Merge = true;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
        }

        protected void AddDetail(ExcelWorksheet sheet, int rowIndex, int columnIndex, string detailText)
        {
            sheet.Cells[rowIndex, columnIndex].Value = detailText;

        }

        protected void AddDetail(ExcelWorksheet sheet, int rowIndex, int columnIndex, string detailText, ExcelHorizontalAlignment align)
        {
            sheet.Cells[rowIndex, columnIndex].Value = detailText;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = align;
        }

        protected void AddDetail(ExcelWorksheet sheet, int rowIndex, int columnIndex, decimal detailText)
        {
            sheet.Cells[rowIndex, columnIndex].Value = detailText;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
        }

        protected void AddDetailWithNumberFormat(ExcelWorksheet sheet, int rowIndex, int columnIndex, decimal detailText, string digitFormat)
        {
            sheet.Cells[rowIndex, columnIndex].Value = detailText;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            if (digitFormat.IsNullOrEmpty() || digitFormat.IsNullOrWhiteSpace())
                digitFormat = "0.00";
            sheet.Cells[rowIndex, columnIndex].Style.Numberformat.Format = digitFormat;
        }

        protected void AddDetail(ExcelWorksheet sheet, int rowIndex, int columnIndex, int detailText)
        {
            sheet.Cells[rowIndex, columnIndex].Value = detailText;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
        }

        protected void AddDetail(ExcelWorksheet sheet, int rowIndex, int columnIndex, decimal detailText,
             OfficeOpenXml.Style.ExcelHorizontalAlignment align)
        {
            sheet.Cells[rowIndex, columnIndex].Value = detailText;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = align;
        }


        protected void AddFooter(ExcelWorksheet sheet, int rowIndex, int columnIndex, decimal detailText,
            OfficeOpenXml.Style.ExcelHorizontalAlignment align)
        {
            sheet.Cells[rowIndex, columnIndex].Value = detailText;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = align;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Size = 20;
            sheet.Cells[rowIndex, columnIndex].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            sheet.Cells[rowIndex, columnIndex].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
        }

        protected void AddFooter(ExcelWorksheet sheet, int rowIndex, int columnIndex, string detailText,
            OfficeOpenXml.Style.ExcelHorizontalAlignment align)
        {
            sheet.Cells[rowIndex, columnIndex].Value = detailText;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = align;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Size = 20;

        }
        protected void AddObjects<T>(ExcelWorksheet sheet, int startRowIndex, IList<T> items, params Func<T, object>[] propertySelectors)
        {
            if (items.IsNullOrEmpty() || propertySelectors.IsNullOrEmpty())
            {
                return;
            }

            for (var i = 0; i < items.Count; i++)
            {
                for (var j = 0; j < propertySelectors.Length; j++)
                {
                    sheet.Cells[i + startRowIndex, j + 1].Value = propertySelectors[j](items[i]);
                }
            }
        }

        protected void SaveAsCsv(ExcelPackage excelPackage, FileDto file)
        {
            var filePath = Path.Combine(_appConfig.ProcessingFolder, file.FileName);
            var bytes = excelPackage.ConvertToCsv();
            File.WriteAllBytes(filePath, bytes);
        }

        protected void Save(ExcelPackage excelPackage, FileDto file)
        {
            var filePath = Path.Combine(_appConfig.ProcessingFolder, file.FileName);
            excelPackage.SaveAs(new FileInfo(filePath));
        }

        protected void SetBorder(ExcelWorksheet sheet, int rowIndex, int columnIndex, ExcelRange excelRange, ExcelBorderStyle excelBorderStyle)
        {
            excelRange.Style.Border.BorderAround(excelBorderStyle);
        }

        public void AutoFilterRangeGiven(ExcelWorksheet sheet, int startingRowIndex, int startingColIndex, int endingRowIndex, int endingColumnIndex)
        {
            using (ExcelRange autoFilterCells = sheet.Cells[
                           startingRowIndex, startingColIndex,
                           endingRowIndex, endingColumnIndex])
            {
                autoFilterCells.AutoFilter = true;
            }
        }


        protected void ChangeText(ExcelWorksheet sheet, string cellAddress, string tagName, string tagValue)
        {
            string temp = sheet.Cells[cellAddress].Value.ToString();
            temp = temp.Replace(tagName, tagValue);
            sheet.Cells[cellAddress].Value = temp;
        }
        protected void AddText(ExcelWorksheet sheet, string cellAddress, string cellText)
        {
            sheet.Cells[cellAddress].Value = cellText;
        }

        protected void AddText(ExcelWorksheet sheet, string cellAddress, decimal cellValue)
        {
            sheet.Cells[cellAddress].Value = cellValue;
        }

        public string RemoveRightSideFromGivenChar(string input, char searchChar)
        {
            var idx = input.LastIndexOf(searchChar);
            if (idx > 0)
                input = input.Substring(0, idx - 1);
            return input.Trim();
        }

        public void LinesForEmptyRow(ExcelWorksheet worksheet, int fromRow, int fromColumn, int toRow, int toColumn)
        {
            var emptyRows = toRow - fromRow;
            if (emptyRows >= 0)
            {
                if (emptyRows < 3)
                {
                    for (int i = fromRow; i <= emptyRows + fromRow; i++)
                    {
                        var straightLine = worksheet.Drawings.AddShape("StraightLine_" + worksheet.Name + "_" + i, eShapeStyle.Line);
                        //to draw a straightline
                        straightLine.From.Row = i - 1;
                        straightLine.From.Column = fromColumn;
                        straightLine.To.Row = i - 1;
                        straightLine.To.Column = toColumn;
                        //to set the straightline with desired position
                        straightLine.SetPosition(i - 1, 12, 0, 4);
                        //To Set the border of the line
                        straightLine.Border.Width = 1;
                        straightLine.Border.Fill.Color = Color.Black;
                        //row++;
                    }
                }
                else
                {
                    //to draw a straightline
                    var straightLine = worksheet.Drawings.AddShape("StraightLine_" + worksheet.Name + "_" + fromRow, eShapeStyle.Line);
                    straightLine.From.Row = fromRow - 1;
                    straightLine.From.Column = fromColumn;
                    straightLine.To.Row = fromRow - 1;
                    straightLine.To.Column = toColumn;
                    //to set the straightline with desired position
                    straightLine.SetPosition(fromRow - 1, 12, 0, 4);
                    //To Set the border of the line
                    straightLine.Border.Width = 1;
                    straightLine.Border.Fill.Color = Color.Black;
                    fromRow++;

                    //to draw a cross line
                    var crossedLine = worksheet.Drawings.AddShape("CrossLine_" + worksheet.Name + "_" + fromRow + "_To_" + toColumn, eShapeStyle.Line);
                    crossedLine.From.Row = fromRow - 1;
                    crossedLine.From.Column = fromColumn;
                    crossedLine.To.Row = toRow;
                    crossedLine.To.Column = toColumn;
                    crossedLine.Border.Fill.Color = Color.Black;
                    crossedLine.SetPosition(fromRow - 1, 3, 0, 2);
                    crossedLine.Border.Width = 1;
                }
            }


        }

        protected void AddHeaderWithHorizontalMergeWithRowCount(ExcelWorksheet sheet, int rowIndex, int columnIndex, int rowcount, string headerText)
        {
            if (rowcount == 0)
                rowcount = 1;

            sheet.Cells[rowIndex, columnIndex].Value = headerText;
            sheet.Cells[rowIndex, columnIndex, rowIndex + rowcount - 1, columnIndex].Merge = true;
            sheet.Cells[rowIndex, columnIndex].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            sheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            sheet.Cells[rowIndex, columnIndex].Style.Font.Bold = true;
        }

        protected string GetDecimalFormat(decimal number)
        {
            var format = $"{{0:F{_roundDecimals}}}";
            return string.Format(format, number);

        }

    }
}
