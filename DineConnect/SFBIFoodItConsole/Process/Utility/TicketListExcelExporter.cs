﻿using SFBIFoodItConsole.Process.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master;

namespace SFBIFoodItConsole.Process.Utility
{
    public class TicketListExcelExporter : FileExporterBase
    {

        public TicketListExcelExporter(AppConfig appConfig) : base(appConfig)
        {

        }
        public FileDto ExportToFile(List<TicketOutputDto> ticketOutputDtos, Location location, DateTime printDate)
        {
            return CreateExcelPackage(
                $"ztotal.csv",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add("zTotal");
                    sheet.OutLineApplyStyle = true;

                    int row = 1;
                    AddHeader(sheet, row, 1, 1);
                    AddHeader(sheet, row, 2, location.Code);
                    AddHeader(sheet, row, 3, "001");
                    AddHeader(sheet, row, 4, printDate.ToString("yyyyMMdd"));
                    AddHeader(sheet, row, 5, printDate.ToString("yyyyMMdd") + "0000");
                    AddHeader(sheet, row, 6, 1);

                    row++;
                    AddHeader(sheet, row, 1, "集計 番号");
                    AddHeader(sheet, row, 2, "名称");
                    AddHeader(sheet, row, 3, "金額 項番");
                    AddHeader(sheet, row, 4, "金額 カウンタ");
                    AddHeader(sheet, row, 5, "点数 項番");
                    AddHeader(sheet, row, 6, "点数 カウンタ");
                    AddHeader(sheet, row, 7, "回数 項番");
                    AddHeader(sheet, row, 8, "回数 カウンタ");
                    AddHeader(sheet, row, 9, "枚数 項番");
                    AddHeader(sheet, row, 10, "枚数 カウンタ");
                    AddHeader(sheet, row, 11, "組数 項番");
                    AddHeader(sheet, row, 12, "組数 カウンタ");
                    AddHeader(sheet, row, 13, "客数 項番");
                    AddHeader(sheet, row, 14, "客数 カウンタ");

                    row++;
                    AddObjects(
                        sheet, row, ticketOutputDtos,
                        _ => _.Number,
                        _ => _.Name,
                        _ => 1,
                        _ =>
                        {
                            if (string.IsNullOrEmpty(_.AmountOfMoneyStr))
                                return decimal.Round(_.AmountOfMoney, 2, MidpointRounding.AwayFromZero).ToString("F");
                            return _.AmountOfMoneyStr;
                        },
                        _ => 2,
                        _ =>
                        {
                            if (string.IsNullOrEmpty(_.PointStr))
                                return _.Points;
                            return _.PointStr;
                        },
                        _ => 3,
                        _ => _.NoOfTimes,
                        _ => 4,
                        _ => _.NoOfSheets,
                        _ => 5,
                        _ => _.NoOfSets,
                        _ => 6,
                        _ => _.NoOfGuests
                        );

                    //Formatting cells

                    for (var i = 1; i <= 8; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                }, true);
        }
    }
}
