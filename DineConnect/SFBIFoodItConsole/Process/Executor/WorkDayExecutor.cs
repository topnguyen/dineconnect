﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Helper;
using Newtonsoft.Json;
using PttDatabaseCommon.Data;
using PttOutboundConsole.Process.Model;

namespace PttOutboundConsole.Process.Executor
{
    public class OutboundExecutor
    {
        public static string Run(AppConfig config, IUnitOfWork uow, PttInboundDbContext dbContext)
        {
            try
            {
                StringBuilder myBuilderReason = new StringBuilder();

                int skipCount = 0;
                int maxResultCount = config.MaxTicketCount;

                var workPeriods = uow.WorkPeriods.GetAll();

                foreach (var period in workPeriods)
                {
                    var tickets = uow.Tickets.FindQuery(x => x.LastModificationTime >= period.StartTime && x.LastModificationTime <= period.EndTime
                                                            && x.LocationId == period.LocationId);

                    var location = uow.Locations.Find(a => a.Id == period.LocationId).LastOrDefault();

                    if (location == null)
                    {
                        throw new Exception("Location Id is missing " + period.LocationId);
                    }
                    var ticket = tickets.FirstOrDefault();


                    if (period.TotalTicketCount == tickets.Count())
                    {
                        var my = new WORKDAYXML
                        {
                            RETAILSTOREID = location.Code,
                            BUSINESSDAYDATE = period.CreationTime.ToString(OutboundConsts.DateFormat),
                            TRANSACTIONTYPECODE = "Z009",
                            WORKSTATIONID = ticket?.TerminalName,
                            TRANSACTIONSEQUENCENUMBER = ticket == null ? null : (IsRefund(ticket) ? ticket.TicketNumber + "_V" : ticket.TicketNumber),
                            BEGINDATETIMESTAMP = period.StartTime.ToString(OutboundConsts.DateTimeFormat),
                            ENDDATETIMESTAMP = period.EndTime.ToString(OutboundConsts.DateTimeFormat),
                            OPERATORID = period.EndUser,
                            TRANSACTIONCURRENCY = OutboundConsts.Currency,
                            ORIGTRANSNUMBER = ticket?.TicketNumber,
                            RETAILTYPECODE = "ZNPS",
                            SALESAMOUNT = tickets.Any() ? tickets.Sum(x => x.TotalAmount).ToString() : "",
                            LINEITEMCOUNT = period.TotalTicketCount,
                        };
                        var discounts = new List<WORKSHIFTDISCOUNT>();
                        var taxs = new List<WORKSHIFTTAX>();
                        var tenders = new List<WORKSHIFTTENDER>();
                        foreach (var myTicket in tickets)
                        {
                            foreach (var ticketOrder in myTicket.Orders)
                            {
                                foreach (var promotionDetailValue in ticketOrder.GetOrderPromotionList())
                                {
                                    var myDiscount = new WORKSHIFTDISCOUNT()
                                    {
                                        DISCOUNTTYPE = GetPromotionCategory(uow, promotionDetailValue.PromotionSyncId),
                                        REDUCTIONAMOUNT = string.Format(OutboundConsts.DecimalFormat, promotionDetailValue.PromotionAmount)
                                    };
                                    discounts.Add(myDiscount);
                                }

                                if (!string.IsNullOrEmpty(ticketOrder.Taxes))
                                {
                                    var allMyTaxes = JsonHelper.Deserialize<List<TaxValue>>(ticketOrder.Taxes);
                                    foreach (var taxValue in allMyTaxes)
                                    {
                                        var taxItem = new WORKSHIFTTAX();
                                        taxItem.TAXAMOUNT = string.Format(OutboundConsts.DecimalFormat,
                                            taxValue.GetTax(myTicket.TaxIncluded, ticketOrder.Price, taxValue.TaxRate));
                                        taxItem.TAXTYPECODE = taxValue.TaxTemplateName;

                                        taxs.Add(taxItem);
                                    }
                                }
                            }
                            foreach (var myPayment in myTicket.Payments)
                            {
                                var tender = new WORKSHIFTTENDER
                                {
                                    TENDERTYPECODE = GetTenderTypeCode(uow, myPayment.PaymentTypeId),
                                    TENDERAMOUNT = string.Format(OutboundConsts.DecimalFormat, myPayment.TenderedAmount)
                                };

                                tenders.Add(tender);
                            }
                        }
                        var gdiscount = discounts.GroupBy(x => x.DISCOUNTTYPE);
                        foreach (var g in gdiscount)
                        {
                            var discount = new WORKSHIFTDISCOUNT
                            {
                                DISCOUNTTYPE = g.Key,
                                DISCOUNTCOUNT = g.Count()
                            };
                            decimal sum = 0M;
                            foreach (var val in g)
                            {
                                decimal.TryParse(val.REDUCTIONAMOUNT, out decimal amt);
                                sum += amt;
                            }
                            discount.REDUCTIONAMOUNT = string.Format(OutboundConsts.DecimalFormat, sum);
                            my.DISCOUNTS.Add(discount);
                        }

                        var gtax = taxs.GroupBy(x => x.TAXTYPECODE);
                        foreach (var g in gtax)
                        {
                            var tax = new WORKSHIFTTAX
                            {
                                TAXTYPECODE = g.Key,
                                TAXCOUNT = g.Count()
                            };
                            decimal sum = 0M;
                            foreach (var val in g)
                            {
                                decimal.TryParse(val.TAXAMOUNT, out decimal amt);
                                sum += amt;
                            }
                            tax.TAXAMOUNT = string.Format(OutboundConsts.DecimalFormat, sum);
                            my.TAXS.Add(tax);
                        }

                        var gtender = tenders.GroupBy(x => x.TENDERTYPECODE);
                        foreach (var g in gtender)
                        {
                            var tender = new WORKSHIFTTENDER
                            {
                                TENDERTYPECODE = g.Key,
                                TENDERCOUNT = g.Count()
                            };
                            decimal sum = 0M;
                            foreach (var val in g)
                            {
                                decimal.TryParse(val.TENDERAMOUNT, out decimal amt);
                                sum += amt;
                            }
                            tender.TENDERAMOUNT = string.Format(OutboundConsts.DecimalFormat, sum);
                            my.TENDERS.Add(tender);
                        }

                        //Write Workday to XML
                        var periodPath = Path.Combine(config.ProcessingFolder, "WORKDAY_" + DateTime.Now.ToString(OutboundConsts.DateTimeFileFormat) + ".xml");
                        WriteDataToXml(my, typeof(WORKDAYXML), periodPath);
                    }
                    
                    if(!string.IsNullOrEmpty(period.WorkPeriodInformations))
                    {
                        var infos = JsonConvert.DeserializeObject<List<WorkShiftDto>>(period.WorkPeriodInformations);
                        foreach (var info in infos)
                        {
                            var shift = new WORKSHIFTXML
                            {
                                RETAILSTOREID = location.Code,
                                BUSINESSDAYDATE = period.CreationTime.ToString(OutboundConsts.DateFormat),
                                TRANSACTIONTYPECODE = "Z009",
                                WORKSTATIONID = info.TerminalName,
                                TRANSACTIONSEQUENCENUMBER = ticket == null ? null : (IsRefund(ticket) ? ticket.TicketNumber + "_V" : ticket.TicketNumber),
                                BEGINDATETIMESTAMP = info.StartDate.ToString(OutboundConsts.DateTimeFormat),
                                ENDDATETIMESTAMP = info.EndDate.ToString(OutboundConsts.DateTimeFormat),
                                OPERATORID = info.StopUser,
                                TRANSACTIONCURRENCY = OutboundConsts.Currency,
                                ORIGTRANSNUMBER = ticket?.TicketNumber
                            };

                            var shiftPath = Path.Combine(config.ProcessingFolder, "WORKSHIFT_" + DateTime.Now.ToString(OutboundConsts.DateTimeFileFormat) + ".xml");
                            WriteDataToXml(shift, typeof(WORKSHIFTXML), shiftPath);
                        }
                    }
                }

                for (int i = 0; i < config.RunCycle; i++)
                {
                    var allTickets = uow.Tickets
                        .FindQuery(a => a.ExternalProcessed == false && a.TenantId == config.TenantId)
                        .OrderByDescending(a => a.Id);

                    if (allTickets.Any())
                    {
                        var myTickets = allTickets.Skip(skipCount).Take(maxResultCount).ToList();
                        foreach (var myTicket in myTickets)
                        {
                            var myGenerate = GenerateFile(myTicket, config, uow,dbContext, out var reason);
                            if (myGenerate)
                            {
                                myTicket.ExternalProcessed = true;
                                uow.Tickets.Update(myTicket);
                                uow.Complete();
                            }
                            else
                            {
                                myBuilderReason.Append(reason);
                            }
                        }
                    }

                    skipCount = 0 * i + maxResultCount;
                }

                return myBuilderReason.ToString();
            }
            catch (Exception ex)
            {
                var myDate = DateTime.Now;
                uow.ExternalLogs.Add(new ExternalLog
                {
                    AuditType =  (int)  ExternalLogType.TicketOutbound,
                    CreatorUserId = config.AppUserId,
                    TenantId = config.TenantId,
                    CreationTime = myDate,
                    LogTime = myDate,
                    ExternalLogTrunc = myDate.Date,
                    LogData = ex.Message,
                    LogDescription = ex.StackTrace
                });
                uow.Complete();
                return ex.Message;
            }
            
        }

        static void WriteDataToXml(object data, Type type, string path)
        {
            var writer = new StreamWriter(path);
            var serializer = new XmlSerializer(type);
            serializer.Serialize(writer, data);
            writer.Close();
        }

        private static bool GenerateFile(Ticket ticket, AppConfig config, IUnitOfWork uow, PttInboundDbContext dbContext, out string reason)
        {
            bool returnStatus = false;
            try
            {
                Location location = uow.Locations.Find(a => a.Id == ticket.LocationId).LastOrDefault();
                if (location == null)
                {
                    throw new Exception("Location Id is missing " + ticket.LocationId);
                }

                Company company = uow.Companies.Find(a => a.Id == location.CompanyRefId).LastOrDefault();

                var myPath = Path.Combine(config.ProcessingFolder, ticket.TicketNumber+"_"+DateTime.Now.ToString(OutboundConsts.DateTimeFileFormat)+ ".xml");
                PosTransactionSend_MT my = new PosTransactionSend_MT
                {
                    TRANSACTION =
                    {
                        RETAILSTOREID = location.Code,
                        BUSINESSDAYDATE = ticket.LastPaymentTimeTruc.ToString(OutboundConsts.DateFormat),
                        TRANSACTIONTYPECODE = IsRefund(ticket) ? "Z004" : "Z001",
                        WORKSTATIONID = ticket.TerminalName,
                        TRANSACTIONSEQUENCENUMBER =
                            IsRefund(ticket) ? ticket.TicketNumber + "_V" : ticket.TicketNumber,
                        BEGINDATETIMESTAMP = ticket.TicketCreatedTime.ToString(OutboundConsts.DateTimeFormat),
                        ENDDATETIMESTAMP = ticket.LastPaymentTime.ToString(OutboundConsts.DateTimeFormat),
                        OPERATORID = ticket.LastModifiedUserName,
                        TRANSACTIONCURRENCY = OutboundConsts.Currency,
                        ORIGTRANSNUMBER = ticket.TicketNumber,
                        EXTENSION = new TRANEXTENSION {REF_BONNUMBER = ticket.ReferenceNumber}
                    }
                };


                if (IsRefund(ticket))
                {
                    my.TRANSACTION.VOIDED.VOIDEDRETAILSTOREID = location.Code;
                    my.TRANSACTION.VOIDED.VOIDEDBUSINESSDAYDATE =ticket.LastPaymentTimeTruc.ToString(OutboundConsts.DateFormat);
                    my.TRANSACTION.VOIDED.VOIDEDWORKSTATIONID = ticket.TerminalName;
                    my.TRANSACTION.VOIDED.VOIDEDTRANSACTIONSEQUENCENUMBE = ticket.TicketNumber;
                    my.TRANSACTION.VOIDEDADDITIONAL.TRANSREASONCODE = "ZVD1";
                }

                int sequenceNumber = 1;
                foreach (var ticketOrder in ticket.Orders)
                {
                    MenuItem menuItem = uow.MenuItems.Find(a => a.Id == ticketOrder.MenuItemId).LastOrDefault();
                    if (menuItem == null)
                    {
                        throw new Exception("MenuItem Id is not available : " + ticketOrder.MenuItemId);
                    }

                    if (!(company is null))
                    {
                        var myDepartment = uow.Departments.Find(a =>
                            a.TenantId == config.TenantId && a.Name.ToUpper().Equals(ticket.DepartmentName.ToUpper())).LastOrDefault();

                        var salesChannel = ticket.DepartmentName;

                        if (myDepartment != null)
                        {
                            salesChannel = myDepartment.Code;
                        }

                        SALESITEM item = new SALESITEM
                        {
                            RETAILSEQUENCENUMBER = sequenceNumber.ToString(),
                            RETAILTYPECODE = IsRefund(ticket) ? "BLANK" : (IsReturnTicket(ticket) ? "ZNPR" : "ZNPS"),
                            RETAILREASONCODE = "ZS01",
                            ITEMIDQUALIFIER = "2",
                            ITEMID = !string.IsNullOrEmpty(menuItem.BarCode)?menuItem.BarCode:menuItem.AliasCode,
                            RETAILQUANTITY =  string.Format(OutboundConsts.DecimalFormat, ticketOrder.Quantity),
                            SALESUNITOFMEASURE = menuItem.Uom,
                            SALESAMOUNT = string.Format(OutboundConsts.DecimalFormat, ticketOrder.GetTotal()),
                            NORMALSALESAMOUNT = string.Format(OutboundConsts.DecimalFormat,ticketOrder.GetOriginalValue()),
                            ACTUALUNITPRICE = string.Format(OutboundConsts.DecimalFormat,ticketOrder.OriginalPrice),
                            STORAGELOCATION = company.UserSerialNumber,
                            EXTENSION = {SALESCHANNEL = salesChannel, SALESMODE = ticket.DepartmentGroup},
                        };

                        StringBuilder sb = new StringBuilder();
                        foreach (var myTag in ticketOrder.TransactionOrderTags.Where(a=>!a.TagName.Equals("PROMOTION")))
                        {
                            if (sb.Length > 0)
                                sb.Append(",");
                            sb.Append(myTag.TagValue);
                        }

                        item.EXTENSION.ITEMOPTION = sb.ToString();
                        if (!string.IsNullOrEmpty(ticketOrder.OrderRef))
                        {
                            item.EXTENSION.COMBOPARENTITEM = GetComboParentid(ticket,ticketOrder);
                        }

                        int discoutnSequence = 1;
                        foreach (var promotionDetailValue in ticketOrder.GetOrderPromotionList())
                        {
                            var myDiscount = new DISCOUNTITEM()
                            {
                                DISCOUNTID = promotionDetailValue.PromotionSyncId.ToString(),
                                DISCOUNTSEQUENCENUMBER = discoutnSequence.ToString(),
                                DISCOUNTTYPECODE = GetPromotionCategory(uow, promotionDetailValue.PromotionSyncId),
                                REDUCTIONAMOUNT = string.Format(OutboundConsts.DecimalFormat, promotionDetailValue.PromotionAmount)
                            };
                            item.DISCOUNTITEM.Add(myDiscount);
                            discoutnSequence++;
                        }

                        if (!string.IsNullOrEmpty(ticketOrder.Taxes))
                        {
                            var allMyTaxes  = JsonHelper.Deserialize<List<TaxValue>>(ticketOrder.Taxes);
                            int taxSequence = 1;
                            foreach (var taxValue in allMyTaxes)
                            {
                                var taxItem  = new TAXITEM();
                                taxItem.TAXSEQUENCENUMBER = taxSequence.ToString();
                                taxItem.TAXAMOUNT = string.Format(OutboundConsts.DecimalFormat,
                                    taxValue.GetTax(ticket.TaxIncluded, ticketOrder.Price, taxValue.TaxRate));
                                taxItem.TAXTYPECODE = "YM02";
                                item.TAXITEM.Add(taxItem);
                                taxSequence++;
                            }

                        }
                        my.TRANSACTION.SALESITEM.Add(item);

                        if (IsDepartmentAvailable(ticket, config.CreditSalesDepartments))
                        {
                            item.EXTENSION.SALESORDERITEM = GetSaleOrderItem(ticketOrder);
                            item.RETAILTYPECODE = "Z002";
                        }

                        if (IsDepartmentAvailable(ticket, config.InternalSalesDepartments))
                        {
                            item.EXTENSION.ITEMCCAIO = GetCostCenter(ticket);
                            item.RETAILTYPECODE = "Z003";

                        }
                    }

                   

                    sequenceNumber++;
                }

                var blueCardNumber = GetBlueCardNumber(ticket);
                if (!string.IsNullOrEmpty(blueCardNumber))
                {
                    my.TRANSACTION.LOYALTY.CUSTOMERCARDNUMBER = GetBlueCardNumber(ticket);
                    my.TRANSACTION.LOYALTY.LOYALTYSEQUENCENUMBER = "1";
                }

                int tenderSequence = 1;
                foreach (var myPayment in ticket.Payments)
                {
                    var tenderer = new TENDER
                    {
                        TENDERSEQUENCENUMBER = tenderSequence.ToString(),
                        TENDERTYPECODE = GetTenderTypeCode(uow, myPayment.PaymentTypeId),
                        TENDERAMOUNT = string.Format(OutboundConsts.DecimalFormat, myPayment.TenderedAmount),
                        TENDERCURRENCY = OutboundConsts.Currency,
                        TENDERID = myPayment.PaymentTypeId.ToString()
                    };

                    my.TRANSACTION.TENDER.Add(tenderer);
                }

                if (IsDepartmentAvailable(ticket, config.InternalSalesDepartments))
                {
                    my.TRANSACTION.PARTNERID = GetEmployeeId(ticket);
                    my.TRANSACTION.EXTENSION.EMPLOYEE_ID = my.TRANSACTION.PARTNERID;
                    my.TRANSACTION.EXTENSION.EMPLOYEE_NAME = GetEmployeeName(ticket);
                    my.TRANSACTION.EXTENSION.EMPLOYEE_DEP= GetEmployeeDept(ticket);

                    foreach (var salesitem in my.TRANSACTION.SALESITEM)
                    {
                        salesitem.RETAILTYPECODE = "ZIPS";

                        if (!string.IsNullOrEmpty(ticket.ReferenceNumber))
                        {
                            salesitem.RETAILTYPECODE = "ZIPSR";
                        }
                    }

                }

                if (IsDepartmentAvailable(ticket, config.CreditSalesDepartments))
                {
                    my.TRANSACTION.PARTNERID = GetCustomerCode(ticket);
                    my.TRANSACTION.EXTENSION.ID_CARD = GetIdCardNumber(ticket);
                    foreach (var salesitem in my.TRANSACTION.SALESITEM)
                    {
                        salesitem.EXTENSION.SALESORDER = GetSalesOrder(ticket);
                        salesitem.RETAILTYPECODE = "ZCPS";
                        if (!string.IsNullOrEmpty(ticket.ReferenceNumber))
                        {
                            salesitem.RETAILTYPECODE = "ZCPR";
                        }

                        if (!string.IsNullOrEmpty(salesitem.EXTENSION.SALESORDER))
                        {
                            salesitem.RETAILTYPECODE = "ZCSS";

                            if (!string.IsNullOrEmpty(ticket.ReferenceNumber))
                            {
                                salesitem.RETAILTYPECODE = "ZCSR";
                            }
                        }
                    }
                }

                WriteDataToXml(my, typeof(PosTransactionSend_MT), myPath);

                reason = "";
                returnStatus = true;
                
            }
            catch (Exception exception)
            {
                reason = "Id : " + ticket.Id;
                reason += exception.Message;
                returnStatus = false;
            }

            return returnStatus;
        }
        public static string ConvertToXML<T>(T objectToConvert)
        {
            XmlDocument doc = new XmlDocument();
            XmlNode root = doc.CreateNode(XmlNodeType.Element, objectToConvert.GetType().Name, string.Empty);
            doc.AppendChild(root);
            XmlNode childNode;

            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            foreach (PropertyDescriptor prop in properties)
            {
                if (prop.GetValue(objectToConvert) != null)
                {
                    childNode = doc.CreateNode(XmlNodeType.Element, prop.Name, string.Empty);
                    childNode.InnerText = prop.GetValue(objectToConvert).ToString();
                    root.AppendChild(childNode);
                }
            }            

            return doc.OuterXml;
        }
        private static string GetCostCenter(Ticket ticket)
        {
            var allTagValues = ticket.GetTicketTagValues()
                .LastOrDefault(a => a.TagName.Equals(OutboundConsts.PttEmployeeCostCenter));

            if (allTagValues != null)
                return allTagValues.TagValue;
            return "";
        }

        private static string GetSaleOrderItem(Order ticketOrder)
        {
            var allTagValues = ticketOrder.TransactionOrderTags
                .LastOrDefault(a => a.TagName.Equals(OutboundConsts.S4HanaOrder));

            if (allTagValues != null)
                return allTagValues.TagValue;
            return "";
        }

        private static bool IsDepartmentAvailable(Ticket ticket, string departments)
        {
            if (!string.IsNullOrEmpty(ticket.DepartmentName) && !string.IsNullOrEmpty(departments))
            {
                string[] allDepartments = departments.Split(',');
                if (allDepartments.Any())
                {
                    return allDepartments.Contains(ticket.DepartmentName);
                }
            }
            return false;
        }

        private static string GetSalesOrder(Ticket ticket)
        {
            var allTagValues = ticket.GetTicketTagValues()
                .LastOrDefault(a => a.TagName.Equals(OutboundConsts.S4HANASaleOrderCode));

            if (allTagValues != null)
                return allTagValues.TagValue;
            return "";
        }

        private static string GetIdCardNumber(Ticket ticket)
        {
          
            var allTagValues = ticket.GetTicketTagValues()
                .LastOrDefault(a => a.TagName.Equals(OutboundConsts.CustomerCode));

            if (allTagValues != null)
                return allTagValues.TagValue;
            return "";
        }

        private static string GetTenderTypeCode(IUnitOfWork uow,int myPaymentPaymentTypeId)
        {
            var myType = uow.PaymentTypes.Find(a => a.Id.Equals(myPaymentPaymentTypeId)).LastOrDefault();
            if (myType != null && !string.IsNullOrEmpty(myType.AccountCode))
                return myType.AccountCode.Substring(0,4);
            return "";
        }

        private static string GetCustomerCode(Ticket ticket)
        {

            var allTagValues = ticket.GetTicketTagValues()
                .LastOrDefault(a => a.TagName.Equals(OutboundConsts.CustomerCode));

            if (allTagValues != null)
                return allTagValues.TagValue;
            return "";
        }
        private static string GetEmployeeId(Ticket ticket)
        {

            var allTagValues = ticket.GetTicketTagValues()
                .LastOrDefault(a => a.TagName.Equals(OutboundConsts.EmployeeId));

            if (allTagValues != null)
                return allTagValues.TagValue;
            return "";
        }

        private static string GetEmployeeName(Ticket ticket)
        {

            var allTagValues = ticket.GetTicketTagValues()
                .LastOrDefault(a => a.TagName.Equals(OutboundConsts.EmployeeName));

            if (allTagValues != null)
                return allTagValues.TagValue;
            return "";
        }

        private static string GetEmployeeDept(Ticket ticket)
        {

            var allTagValues = ticket.GetTicketTagValues()
                .LastOrDefault(a => a.TagName.Equals(OutboundConsts.EmployeeDepartment));

            if (allTagValues != null)
                return allTagValues.TagValue;
            return "";
        }
        private static string GetBlueCardNumber(Ticket ticket)
        {

            var allTagValues = ticket.GetTicketTagValues()
                .LastOrDefault(a => a.TagName.Equals(OutboundConsts.BlueCardNo));

            if (allTagValues != null)
                return allTagValues.TagValue;
            return "";
        }

        private static string GetPromotionCategory(IUnitOfWork uow, int promotionSyncId)
        {
            var myType = uow.Promotions.Find(a => a.Id.Equals(promotionSyncId)).LastOrDefault();
            if (myType != null && myType.PromotionCategory!=null)
                return myType.PromotionCategory.Code;

            return "";

        }

        private static string GetComboParentid(Ticket ticket, Order order)
        {
            var allOrder = ticket.Orders.Where(a =>
                !string.IsNullOrEmpty(a.OrderRef) && a.OrderRef == order.OrderRef && a.MenuItemType == 1);
            if(allOrder.Any())
                return allOrder.First().MenuItemId.ToString();


            return "0";
        }

        private static bool IsRefund(Ticket ticket)
        {
            return ticket.TotalAmount == 0 && ticket.TicketStates.Contains(OutboundConsts.Refund);
        }

        private static bool IsReturnTicket(Ticket ticket)
        {
            return ticket.TicketStates.Contains(OutboundConsts.Return);
        }
    }

   
}