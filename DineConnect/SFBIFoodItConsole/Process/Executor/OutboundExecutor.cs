﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using ConnectDatabaseCommon.Data;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using Newtonsoft.Json;
using PttDatabaseCommon.Data;
using SFBIFoodItConsole.Process.Model;
using SFBIFoodItConsole.Process.Utility;

namespace SFBIFoodItConsole.Process.Executor
{
    public class OutboundExecutor
    {
        public static string Run(AppConfig config, IUnitOfWork uow, PttInboundDbContext dbContext)
        {
            try
            {
                var myBuilderReason = new StringBuilder();
                var myStartDay = config.ProcessingDate ?? DateTime.Now.Date;
                for (var i = 0; i < config.RunCycle; i++)
                {
                    var allTickets = uow.Tickets
                        .FindQuery(a => a.TenantId == config.TenantId
                                        && a.LastPaymentTimeTruc == myStartDay.Date
                                        && (config.LocationId == null || a.LocationId == config.LocationId))
                        .OrderByDescending(a => a.Id);

                    if (allTickets.Any())
                        foreach (var myTickets in allTickets.GroupBy(a => a.LocationId))
                        {
                            var myLocation = uow.Locations.Find(a =>
                                a.TenantId == config.TenantId && a.Id == myTickets.Key).LastOrDefault();
                            if (myLocation != null)
                            {
                                var myGenerate = GenerateFile(myTickets.ToList()
                                    , myLocation
                                    , config
                                    , uow
                                    , dbContext
                                    , myStartDay
                                    , out var reason);
                                myBuilderReason.Append(reason);
                            }
                        }
                    myStartDay = myStartDay.AddDays(-1);
                }

                return myBuilderReason.ToString();
            }
            catch (Exception ex)
            {
                var myDate = DateTime.Now;
                uow.ExternalLogs.Add(new ExternalLog
                {
                    AuditType = (int)ExternalLogType.SFBIFoodItConsole,
                    CreatorUserId = config.AppUserId,
                    TenantId = config.TenantId,
                    CreationTime = myDate,
                    LogTime = myDate,
                    ExternalLogTrunc = myDate.Date,
                    LogData = ex.Message,
                    LogDescription = ex.StackTrace
                });
                uow.Complete();
                return ex.Message;
            }

            return "";
        }

        private static bool GenerateFile(List<Ticket> tickets
            , Location location
            , AppConfig appConfig
            , IUnitOfWork uow
            , PttInboundDbContext dbContext
            , DateTime printDate
            , out string reason)
        {
            var returnStatus = false;

            try
            {
                reason = "";

                #region GenerateFile

                reason = null;
                var result = new List<TicketOutputDto>();
                GetRounding(result, tickets);
                GenerateForDiscount(result, tickets, appConfig);
                GenerateForDepartment(result, tickets, appConfig);
                GenerateForVoucher(result, tickets, uow, appConfig);
                GenerateForPaymentTypes(result, tickets, appConfig);

                GetVoid(result, tickets);
                GetMenuGroup(result, tickets, appConfig, uow);

                // 46.売上計Ⅲ（内税抜き） 純売上
                result.Add(new TicketOutputDto
                {
                    Number = 46,
                    Name = "売上計Ⅲ（内税抜き）純売上",
                    AmountOfMoney = result.First(d => d.Number == 44).AmountOfMoney
                });
                result.Add(new TicketOutputDto
                {
                    Number = 47,
                    Name = "売上計Ⅳ（内税込み）",
                    AmountOfMoney = result.First(d => d.Number == 45).AmountOfMoney
                });

                result.Add(new TicketOutputDto
                {
                    Number = 95,
                    Name = "収入計Ⅰ（内税抜き）",
                    AmountOfMoney = result.First(d => d.Number == 44).AmountOfMoney
                });
                result.Add(new TicketOutputDto
                {
                    Number = 96,
                    Name = "収入計Ⅱ（内税込み）",
                    AmountOfMoney = result.First(d => d.Number == 45).AmountOfMoney
                });

                result.Add(new TicketOutputDto
                {
                    Number = 97,
                    Name = "収入計Ⅲ（内税抜き）",
                    AmountOfMoney = result.First(d => d.Number == 46).AmountOfMoney
                });

                result.Add(new TicketOutputDto
                {
                    Number = 98,
                    Name = "収入計Ⅳ（内税込み）",
                    AmountOfMoney = result.First(d => d.Number == 47).AmountOfMoney
                });


                result.Add(new TicketOutputDto
                {
                    Number = 8,
                    Name = "売上計Ⅰ 客単価  (内税抜き)",
                    AmountOfMoney = DivideByZero(result.First(d => d.Number == 44).AmountOfMoney,
                        result.First(d => d.Number == 7).NoOfGuests)
                });
                result.Add(new TicketOutputDto
                {
                    Number = 9,
                    Name = "売上計Ⅱ 客単価  (内税込み)",
                    AmountOfMoney = DivideByZero(result.First(d => d.Number == 45).AmountOfMoney,
                        result.First(d => d.Number == 7).NoOfGuests)
                });

                result.Add(new TicketOutputDto
                {
                    Number = 10,
                    Name = "売上計Ⅲ 客単価  (内税抜き)",
                    AmountOfMoney = DivideByZero(result.First(d => d.Number == 46).AmountOfMoney,
                        result.First(d => d.Number == 7).NoOfGuests)
                });

                result.Add(new TicketOutputDto
                {
                    Number = 11,
                    Name = "売上計Ⅳ 客単価  (内税込み)",
                    AmountOfMoney = DivideByZero(result.First(d => d.Number == 47).AmountOfMoney,
                        result.First(d => d.Number == 7).NoOfGuests)
                });

                result.Add(new TicketOutputDto
                {
                    Number = 12,
                    Name = "(800)収入計 客数",
                    NoOfGuests = result.First(d => d.Number == 7).NoOfGuests,
                    NoOfSets = result.First(d => d.Number == 7).NoOfSets
                });

                result.Add(new TicketOutputDto
                {
                    Number = 13,
                    Name = "収入計Ⅰ 客単価  (内税抜き)",
                    AmountOfMoney = DivideByZero(result.First(d => d.Number == 95).AmountOfMoney,
                        result.First(d => d.Number == 12).NoOfGuests)
                });

                result.Add(new TicketOutputDto
                {
                    Number = 14,
                    Name = "収入計Ⅱ 客単価  (内税込み)",
                    AmountOfMoney = DivideByZero(result.First(d => d.Number == 96).AmountOfMoney,
                        result.First(d => d.Number == 12).NoOfGuests)
                });

                result.Add(new TicketOutputDto
                {
                    Number = 15,
                    Name = "収入計Ⅲ 客単価  (内税抜き)",
                    AmountOfMoney = DivideByZero(result.First(d => d.Number == 97).AmountOfMoney,
                        result.First(d => d.Number == 12).NoOfGuests)
                });

                result.Add(new TicketOutputDto
                {
                    Number = 16,
                    Name = "収入計Ⅳ 客単価  (内税込み)",
                    AmountOfMoney = DivideByZero(result.First(d => d.Number == 98).AmountOfMoney,
                        result.First(d => d.Number == 12).NoOfGuests)
                });


                result.Add(new TicketOutputDto
                {
                    Number = 82,
                    Name = "消費税(内)対象額",
                    AmountOfMoney = result.First(d => d.Number == 47).AmountOfMoney
                });

                result.Add(new TicketOutputDto
                {
                    Number = 149,
                    Name = "現金在高",
                    AmountOfMoney = result.First(d => d.Number == 112).AmountOfMoney
                });

                result.Add(new TicketOutputDto
                {
                    Number = 163,
                    Name = "今回精算日時",
                    AmountOfMoneyStr = DateTime.Now.ToString("yyMMdd"),
                    PointStr = DateTime.Now.ToString("HHmm")
                });
                var lastTime = tickets.Max(t => t.LastPaymentTime);
                result.Add(new TicketOutputDto
                {
                    Number = 185,
                    Name = "最終レジ売上時刻",
                    AmountOfMoneyStr = lastTime.ToString("yyMMdd"),
                    PointStr = lastTime.ToString("HHmm")
                });
                //result.Add(new TicketOutputDto
                //{
                //    Number = 164,
                //    Name = "レシートＮｏ．(FROM)",
                //    AmountOfMoney =
                //        decimal.TryParse(tickets.OrderBy(t => t.Id).First().TicketNumber, out var ticketNumber)
                //            ? ticketNumber
                //            : 0,
                //    AmountOfMoneyStr =
                //        decimal.TryParse(tickets.OrderBy(t => t.Id).First().TicketNumber, out ticketNumber)
                //            ? null
                //            : tickets.OrderByDescending(t => t.Id).First().TicketNumber
                //});
                //result.Add(new TicketOutputDto
                //{
                //    Number = 165,
                //    Name = "レシートＮｏ．(TO)",
                //    AmountOfMoney =
                //        decimal.TryParse(tickets.OrderByDescending(t => t.Id).First().TicketNumber, out ticketNumber)
                //            ? ticketNumber
                //            : 0,
                //    AmountOfMoneyStr =
                //        decimal.TryParse(tickets.OrderByDescending(t => t.Id).First().TicketNumber, out ticketNumber)
                //            ? null
                //            : tickets.OrderByDescending(t => t.Id).First().TicketNumber
                //});


                result.Add(new TicketOutputDto
                {
                    Number = 190,
                    Name = "売上計Ⅴ（外税込み）",
                    AmountOfMoney = result.First(r => r.Number == 47).AmountOfMoney
                                    + result.First(r => r.Number == 49).AmountOfMoney
                });

                #endregion


                var outputExporter = new TicketListExcelExporter(appConfig);
                var outputFile =
                    outputExporter.ExportToFile(result.OrderBy(t => t.Number).ToList(), location, printDate);
                var csvFile = Path.Combine(appConfig.ProcessingFolder, outputFile.FileName);
                var ftpFileName = (location.Code.Length < 5 ? 0.ToString($"D{5 - location.Code.Length}") : "")
                                  + location.Code
                                  + lastTime.ToString("yyyyMMddHHmm")
                                  + "00PFRT8300";
                var zipFile = Path.Combine(appConfig.ProcessingFolder, ftpFileName + ".lzh");
                LzhZipFile(csvFile, zipFile);

                UploadFtpFile(zipFile, appConfig, ftpFileName + ".txt");
                returnStatus = true;

                if (File.Exists(zipFile)) MoveFile(zipFile, appConfig, lastTime.ToString("yyyyMMdd"));

                if (File.Exists(csvFile)) MoveFile(csvFile, appConfig, lastTime.ToString("yyyyMMdd"));
            }
            catch (Exception e)
            {
                reason = e.Message;
                returnStatus = false;
            }


            return returnStatus;
        }

        private static void MoveFile(string csvFile, AppConfig appConfig, string folder)
        {
            try
            {
                if (File.Exists(csvFile))
                {
                    var myPath = Path.Combine(appConfig.BackupFolder, folder);
                    if (!Directory.Exists(myPath)) Directory.CreateDirectory(myPath);
                    var finalPath = Path.Combine(myPath, Path.GetFileName(csvFile));
                    File.Move(csvFile, finalPath);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static void GenerateForDepartment(List<TicketOutputDto> result, List<Ticket> tickets, AppConfig config)
        {
            var myGroups = tickets.GroupBy(t => t.DepartmentName).Select(tg => new DepartmentGroupDto
            {
                DepartmentName = tg.Key,
                NoOfGuests = GetPaxCount(tg.ToList()),
                AmountOfMoneyIncludedTax = tg.ToList().Sum(t => t.TotalAmount),
                PlainSum = tg.ToList().Sum(t => t.GetPlainSum()),
                ActualTax = tg.ToList().Sum(t => t.GetTaxTotal()),
                NoOfSets = tg.Count(),

                NetSales = tg.ToList().SelectMany(a => a.Transactions).Where(a => a.TransactionTypeId == 2)
                    .Sum(a => a.Amount),
                NetTax = tg.ToList().SelectMany(a => a.Transactions).Where(a => a.TransactionTypeId == 8)
                    .Sum(a => a.Amount),
                NetDiscount = tg.ToList().SelectMany(a => a.Transactions).Where(a => a.TransactionTypeId == 4)
                    .Sum(a => a.Amount),
                NetRound = tg.ToList().SelectMany(a => a.Transactions).Where(a => a.TransactionTypeId == 7)
                    .Sum(a => a.Amount)
            });
            var depGroups = new List<DepartmentGroupDto>();
            foreach (var myVaGroup in myGroups)
            {
                myVaGroup.AmountOfMoneyExcludedTax = myVaGroup.AmountOfMoneyIncludedTax - myVaGroup.NetTax;
                depGroups.Add(myVaGroup);
            }

            //17. 店内飲食合計Ⅰ（内税抜き）
            result.Add(new TicketOutputDto
            {
                Number = 17,
                Name = "店内飲食合計Ⅰ（内税抜き）",
                AmountOfMoney = depGroups.Where(d => config.EatInDepartmentNames.Any(dc => dc == d.DepartmentName))
                                    .Sum(t => t.AmountOfMoneyExcludedTax)
                                + result.First(r => r.Number == 66).AmountOfMoney
                                + result.First(r => r.Number == 189).AmountOfMoney
            });
            result.Add(new TicketOutputDto
            {
                Number = 20,
                Name = "店内飲食内税対象（内税抜き）",
                AmountOfMoney = depGroups.Where(d => config.EatInDepartmentNames.Any(dc => dc == d.DepartmentName))
                                    .Sum(t => t.AmountOfMoneyExcludedTax)
                                + result.First(r => r.Number == 66).AmountOfMoney
                                + result.First(r => r.Number == 189).AmountOfMoney
            });
            result.Add(new TicketOutputDto
            {
                Number = 18,
                Name = "店内飲食合計Ⅱ（内税込み）",
                AmountOfMoney = depGroups.Where(d => config.EatInDepartmentNames.Any(dc => dc == d.DepartmentName))
                                    .Sum(t => t.AmountOfMoneyIncludedTax)
                                + result.First(r => r.Number == 66).AmountOfMoney
                                + result.First(r => r.Number == 189).AmountOfMoney
            });
            result.Add(new TicketOutputDto
            {
                Number = 21,
                Name = "店内飲食内税対象（内税込み）",
                AmountOfMoney = depGroups.Where(d => config.EatInDepartmentNames.Any(dc => dc == d.DepartmentName))
                                    .Sum(t => t.AmountOfMoneyIncludedTax)
                                + result.First(r => r.Number == 66).AmountOfMoney
                                + result.First(r => r.Number == 189).AmountOfMoney
            });

            //23. Total Take-Away I(Tax Excluded)
            result.Add(new TicketOutputDto
            {
                Number = 23,
                Name = "ﾃｲｸｱｳﾄ合計Ⅰ（内税抜き）",
                AmountOfMoney = depGroups.Where(d => config.TakeAwayDepartmentNames.Any(dc => dc == d.DepartmentName))
                    .Sum(t => t.AmountOfMoneyExcludedTax)
            });
            result.Add(new TicketOutputDto
            {
                Number = 26,
                Name = "ﾃｲｸｱｳﾄ内税対象（内税抜き）",
                AmountOfMoney = depGroups.Where(d => config.TakeAwayDepartmentNames.Any(dc => dc == d.DepartmentName))
                    .Sum(t => t.AmountOfMoneyExcludedTax)
            });
            result.Add(new TicketOutputDto
            {
                Number = 24,
                Name = "ﾃｲｸｱｳﾄ合計Ⅱ（内税込み）",
                AmountOfMoney = depGroups.Where(d => config.TakeAwayDepartmentNames.Any(dc => dc == d.DepartmentName))
                    .Sum(t => t.AmountOfMoneyIncludedTax)
            });
            result.Add(new TicketOutputDto
            {
                Number = 27,
                Name = "ﾃｲｸｱｳﾄ内税対象（内税込み）",
                AmountOfMoney = depGroups.Where(d => config.TakeAwayDepartmentNames.Any(dc => dc == d.DepartmentName))
                    .Sum(t => t.AmountOfMoneyIncludedTax)
            });

            //350. Delivery売上合計Ⅰ（内税抜き）
            result.Add(new TicketOutputDto
            {
                Number = 350,
                Name = "Delivery売上合計Ⅰ（内税抜き）",
                AmountOfMoney = depGroups.Where(d => config.DeliveryDepartmentNames.Any(dc => dc == d.DepartmentName))
                    .Sum(t => t.AmountOfMoneyExcludedTax)
            });
            result.Add(new TicketOutputDto
            {
                Number = 352,
                Name = "Delivery売上内税対象（内税抜き）",
                AmountOfMoney = depGroups.Where(d => config.DeliveryDepartmentNames.Any(dc => dc == d.DepartmentName))
                    .Sum(t => t.AmountOfMoneyExcludedTax)
            });
            result.Add(new TicketOutputDto
            {
                Number = 351,
                Name = "Delivery売上合計Ⅱ（内税込み）",
                AmountOfMoney = depGroups.Where(d => config.DeliveryDepartmentNames.Any(dc => dc == d.DepartmentName))
                    .Sum(t => t.AmountOfMoneyIncludedTax)
            });
            result.Add(new TicketOutputDto
            {
                Number = 353,
                Name = "Delivery売上内税対象（内税込み）",
                AmountOfMoney = depGroups.Where(d => config.DeliveryDepartmentNames.Any(dc => dc == d.DepartmentName))
                    .Sum(t => t.AmountOfMoneyIncludedTax)
            });
            // 29.Total Restaurant Sales I (Tax Excluded)
            // 30. Total Restaurant Sales II (I Tax Included)
            result.Add(new TicketOutputDto
            {
                Number = 29,
                Name = "ﾚｽﾄﾗﾝ売上合計Ⅰ（内税抜き）",
                AmountOfMoney = result.First(r => r.Number == 17).AmountOfMoney
                                + result.First(r => r.Number == 23).AmountOfMoney
                                + result.First(r => r.Number == 350).AmountOfMoney
            });
            result.Add(new TicketOutputDto
            {
                Number = 30,
                Name = "ﾚｽﾄﾗﾝ売上合計Ⅱ（内税込み）",
                AmountOfMoney = result.First(r => r.Number == 18).AmountOfMoney
                                + result.First(r => r.Number == 24).AmountOfMoney
                                + result.First(r => r.Number == 351).AmountOfMoney
            });
            result.Add(new TicketOutputDto
            {
                Number = 32,
                Name = "ﾚｽﾄﾗﾝ売上内税対象（内税抜き）",
                AmountOfMoney = result.First(r => r.Number == 29).AmountOfMoney
            });
            result.Add(new TicketOutputDto
            {
                Number = 33,
                Name = "ﾚｽﾄﾗﾝ売上内税対象（内税込み）",
                AmountOfMoney = result.First(r => r.Number == 30).AmountOfMoney
            });

            result.Add(new TicketOutputDto
            {
                Number = 44,
                Name = "売上計Ⅰ（内税抜き）",
                AmountOfMoney = tickets.Sum(t => t.TotalAmount)
                                - tickets.SelectMany(a => a.Transactions).Where(a => a.TransactionTypeId == 8)
                                    .Sum(a => a.Amount)
            });
            result.Add(new TicketOutputDto
            {
                Number = 45,
                Name = "売上計Ⅱ（内税込み）",
                AmountOfMoney = tickets.Sum(t => t.TotalAmount)
            });

            // 1. ﾚｽﾄﾗﾝ売上 客数
            result.Add(new TicketOutputDto
            {
                Number = 1,
                Name = "ﾚｽﾄﾗﾝ売上 客数",
                NoOfGuests = depGroups.Where(d => config.DeliveryDepartmentNames.Any(dc => dc == d.DepartmentName)
                                                  || config.TakeAwayDepartmentNames.Any(dc => dc == d.DepartmentName)
                                                  || config.EatInDepartmentNames.Any(dc => dc == d.DepartmentName))
                    .Sum(t => t.NoOfGuests),
                NoOfSets = depGroups.Where(d => config.DeliveryDepartmentNames.Any(dc => dc == d.DepartmentName)
                                                || config.TakeAwayDepartmentNames.Any(dc => dc == d.DepartmentName)
                                                || config.EatInDepartmentNames.Any(dc => dc == d.DepartmentName))
                    .Sum(t => t.NoOfSets)
            });
            result.Add(new TicketOutputDto
            {
                Number = 2,
                Name = "ﾚｽﾄﾗﾝ売上Ⅰ 客単価",
                AmountOfMoney = DivideByZero(result.First(r => r.Number == 29).AmountOfMoney,
                    result.First(r => r.Number == 1).NoOfGuests)
            });
            result.Add(new TicketOutputDto
            {
                Number = 3,
                Name = "ﾚｽﾄﾗﾝ売上Ⅱ 客単価",
                AmountOfMoney = DivideByZero(result.First(r => r.Number == 30).AmountOfMoney,
                    result.First(r => r.Number == 1).NoOfGuests)
            });

            // 4. 店頭売上 客数
            result.Add(new TicketOutputDto
            {
                Number = 4,
                Name = "店頭売上 客数",
                NoOfGuests = depGroups.Where(d => config.TakeAwayDepartmentNames.Any(dc => dc == d.DepartmentName))
                    .Sum(t => t.NoOfGuests),
                NoOfSets = depGroups.Where(d => config.TakeAwayDepartmentNames.Any(dc => dc == d.DepartmentName))
                    .Sum(t => t.NoOfSets)
            });
            result.Add(new TicketOutputDto
            {
                Number = 5,
                Name = "店頭売上Ⅰ 客単価",
                AmountOfMoney = DivideByZero(depGroups
                    .Where(d => config.TakeAwayDepartmentNames.Any(dc => dc == d.DepartmentName))
                    .Sum(t => t.AmountOfMoneyExcludedTax), result.First(r => r.Number == 4).NoOfGuests)
            });
            result.Add(new TicketOutputDto
            {
                Number = 6,
                Name = "店頭売上Ⅱ 客単価",
                AmountOfMoney = DivideByZero(depGroups
                    .Where(d => config.TakeAwayDepartmentNames.Any(dc => dc == d.DepartmentName))
                    .Sum(t => t.AmountOfMoneyIncludedTax), result.First(r => r.Number == 4).NoOfGuests)
            });

            //7. 売上高 客数
            result.Add(new TicketOutputDto
            {
                Number = 7,
                Name = "売上高 客数",
                NoOfGuests = result.First(r => r.Number == 1).NoOfGuests,
                NoOfSets = result.First(r => r.Number == 1).NoOfSets
            });

            //78.消費税合計
            result.Add(new TicketOutputDto
            {
                Number = 78,
                Name = "消費税合計",
                AmountOfMoney = tickets.ToList().SelectMany(a => a.Transactions).Where(a => a.TransactionTypeId == 8)
                    .Sum(a => a.Amount)
            });

            result.Add(new TicketOutputDto
            {
                Number = 80,
                Name = "(200)消費税（内）",
                AmountOfMoney = tickets.ToList().SelectMany(a => a.Transactions).Where(a => a.TransactionTypeId == 8)
                    .Sum(a => a.Amount)
            });

            result.Add(new TicketOutputDto
            {
                Number = 181,
                Name = "店内飲食客数",
                NoOfGuests = depGroups.Where(d => config.EatInDepartmentNames.Any(dc => dc == d.DepartmentName))
                    .Sum(t => t.NoOfGuests),
                NoOfSets = depGroups.Where(d => config.EatInDepartmentNames.Any(dc => dc == d.DepartmentName))
                    .Sum(t => t.NoOfSets)
            });

            result.Add(new TicketOutputDto
            {
                Number = 182,
                Name = "テイクアウト客数",
                NoOfGuests = depGroups.Where(d => config.TakeAwayDepartmentNames.Any(dc => dc == d.DepartmentName))
                    .Sum(t => t.NoOfGuests),
                NoOfSets = depGroups.Where(d => config.TakeAwayDepartmentNames.Any(dc => dc == d.DepartmentName))
                    .Sum(t => t.NoOfSets)
            });

            result.Add(new TicketOutputDto
            {
                Number = 240,
                Name = "店内飲食Ⅰ  客単価",
                AmountOfMoney = DivideByZero(result.First(r => r.Number == 18).AmountOfMoney,
                    result.First(r => r.Number == 181).NoOfGuests)
            });

            result.Add(new TicketOutputDto
            {
                Number = 241,
                Name = "店内飲食Ⅱ  客単価",
                AmountOfMoney = result.First(r => r.Number == 240).AmountOfMoney
            });

            result.Add(new TicketOutputDto
            {
                Number = 801,
                Name = "(296)Deliveroo",
                AmountOfMoney = depGroups.Where(d => "Deliveroo".ToUpper() == d.DepartmentName.ToUpper())
                    .Sum(t => t.AmountOfMoneyIncludedTax),
                NoOfTimes = depGroups.Where(d => "Deliveroo".ToUpper() == d.DepartmentName.ToUpper())
                    .Sum(t => t.NoOfSets)
            });
        }

        private static void GenerateForDiscount(List<TicketOutputDto> result, List<Ticket> tickets, AppConfig config)
        {
            var discounts = GetPromotionDetails(tickets);

            var couponList = new Dictionary<string, string>
            {
                {"(80)10% Discount", "10% Discount"},
                {"(81)15% Discount", "15% Discount"},
                {"(82)20% Discount", "20% Discount"},
                {"(83)25% Discount", "25% Discount"},
                {"(84)50% Discount", "50% Discount"},
                {"(85)50% Staff Discount", "50% Staff Discount"},
                {"(86)$5 Discount", "$5 Discount"},
                {"(87)Open $Discount", "Open $Discount"},
                {"100% Item Comp. Discount", "100% Item Complimentary Discount"},
                {"割引券支払１０", "Coupon Payment 10"},
                {"割引券支払１１", "Coupon Payment 11"},
                {"割引券支払１２", "Coupon Payment 12"},
                {"割引券支払１３", "Coupon Payment 13"},
                {"(90)$10 Discount", "$10 Discount"},
                {"割引券支払１５", "Coupon Payment 15"}
            };
            var index = 49;
            foreach (var item in couponList)
                AddPromotionValueKey(result, index++, item.Key, item.Value, config, discounts);

            //(87)Open $Discount
            result.First(t => t.Number == 56).AmountOfMoney += tickets.SelectMany(t => t.Transactions).Where(t => t.TransactionTypeId == 4).Sum(t => Math.Abs(t.Amount));// Discount type;

            var couponList1 = new Dictionary<string, string>
            {
                {"(95)$ Discount", "$ Discount"},
                {"(96)$10 OFF", "$10 Discount"},
                {"(97)$5 OFF", "$5 OFF"},
                {"(98)% Discount", "% Discount"},
                {"(99)10% off", "10% off"},
                {"(100)20% off", "20% off"},
                {"(101)25% off", "25% off"},
                {"(102)30% off", "30% off"},
                {"(103)$$ Discount", "$$ Discount"},
                {"(104)Item Comp.", "Item Comp."},
                {"(105)15% off", "15% off"}
            };
            var index1 = 301;
            foreach (var item in couponList1)
                AddPromotionValueKey(result, index1++, item.Key, item.Value, config, discounts);

            //48. 割引券支払合計
            var discountsAll = result.Where(r => (r.Number >= 49 && r.Number <= 63) || (r.Number >= 301 && r.Number <= 311)).ToList();
            result.Add(new TicketOutputDto
            {
                Number = 48,
                Name = "割引券支払合計",
                NoOfTimes = discountsAll.Sum(t => t.NoOfTimes),
                AmountOfMoney = discountsAll.Sum(t => t.AmountOfMoney),
                NoOfSheets = discountsAll.Sum(t => t.NoOfSheets)
            });

            result.Add(new TicketOutputDto
            {
                Number = 192,
                Name = "GST in Discount",
                AmountOfMoney = Math.Round(result.First(r => r.Number == 48).AmountOfMoney * 7M / 107, 2)
            });

            result.Add(new TicketOutputDto
            {
                Number = 189,
                Name = "割引券支払合計（内税抜き）",
                AmountOfMoney = result.First(r => r.Number == 48).AmountOfMoney
                                - result.First(r => r.Number == 192).AmountOfMoney
            });
        }

        private static void GenerateForVoucher(List<TicketOutputDto> result, List<Ticket> tickets, IUnitOfWork uow,
            AppConfig config)
        {
            var vouchers = GetPaymentTypes(tickets);


            var voucherList = new Dictionary<string, string>
            {
                {"(240)PL Voucher：$10", "PEPPER LUNCH $10 VOUCHER"},
                {"(250)PL Voucher：$5", "PEPPER LUNCH $5 VOUCHER"},
                {"(260)Mall Voucher", "MALL VOUCHER"},
                {"(270)Open Voucher", "OPEN VOUCHER"},
                {"(280)Promo Voucher", "Promo Voucher"},
                {"(281)SB Voucher", "SB VOUCHER"},
                {"(282)Redemption Voucher", "REDEMPTION VOUCHER"},
                {"金券支払８", "Voucher Payment 8"},
                {"金券支払９", "Voucher Payment 9"},
                {"金券支払１０", "Voucher Payment 10"},
                {"金券差額", "Voucher Excess"}
            };
            var voucherValues = voucherList.Select(t => t.Value).ToList();

            var index = 100;
            foreach (var item in voucherList)
                AddPaymentKey(result, index == 110 ? 111 : index++, item.Key, item.Value, config, vouchers);

            //99. 金券支払合計
            result.Add(new TicketOutputDto
            {
                Number = 99,
                Name = "金券支払合計",
                AmountOfMoney = result.Where(r => r.Number >= 100 && r.Number <= 109).Sum(r => r.AmountOfMoney),
                NoOfTimes = result.Where(r => r.Number >= 100 && r.Number <= 109).Sum(r => r.NoOfTimes),
                NoOfSheets = result.Where(r => r.Number >= 100 && r.Number <= 109).Sum(r => r.NoOfSheets)
            });
        }

        private static void GenerateForPaymentTypes(List<TicketOutputDto> result, List<Ticket> tickets,
            AppConfig config)
        {
            var paymentTypes = GetPaymentTypes(tickets);
            //132. 支払計Ⅰ
            result.Add(new TicketOutputDto
            {
                Number = 132,
                Name = "支払計Ⅰ",
                AmountOfMoney = paymentTypes.Sum(d => d.Amount),
                NoOfTimes = paymentTypes.Count,
                NoOfSheets = paymentTypes.Select(t => t.TicketId).Distinct().Count()
            });
            result.Add(new TicketOutputDto
            {
                Number = 133,
                Name = "支払計Ⅱ",
                AmountOfMoney = paymentTypes.Sum(d => d.Amount),
                NoOfTimes = paymentTypes.Count,
                NoOfSheets = paymentTypes.Select(t => t.TicketId).Distinct().Count()
            });
            //132. 支払計Ⅰ
            result.Add(new TicketOutputDto
            {
                Number = 145,
                Name = "支出計Ⅰ",
                AmountOfMoney = paymentTypes.Sum(d => d.Amount),
                NoOfTimes = paymentTypes.Count,
                NoOfSheets = paymentTypes.Select(t => t.TicketId).Distinct().Count()
            });
            result.Add(new TicketOutputDto
            {
                Number = 146,
                Name = "支出計Ⅱ",
                AmountOfMoney = paymentTypes.Sum(d => d.Amount),
                NoOfTimes = paymentTypes.Count,
                NoOfSheets = paymentTypes.Select(t => t.TicketId).Distinct().Count()
            });
            var paymentList = new Dictionary<string, string>
            {
                {"(440)Cash", "Cash"},
                {"支払メディア２", "Media Payment 2"},
                {"(380)Nets", "Nets"},
                {"(390)Net Flash Pay", "NETS FLASHPAY"},
                {"支払メディア５", "Media Payment 5"},
                {"支払メディア６", "Media Payment 6"},
                {"支払メディア７", "Media Payment 7"},
                {"支払メディア８", "Media Payment 8"},
                {"支払メディア９", "Media Payment 9"},
                {"支払メディア１０", "Media Payment 10"},
                {"(360)Visa", "Visa"},
                {"(370)MasterCard", "MASTER"},
                {"(371)Amex", "Amex"},
                {"Stripe", "STRIPE"},
                {"支払メディア１５", "Media Payment 15"},
                {"支払メディア１６", "Media Payment 16"},
                {"支払メディア１７", "Media Payment 17"},
                {"支払メディア１８", "Media Payment 18"},
                {"支払メディア１９", "Media Payment 19"},
                {"支払メディア２０", "Media Payment 20"}
            };
            var index = 112;
            foreach (var item in paymentList)
                AddPaymentKey(result, index++, item.Key, item.Value, config, paymentTypes);

            AddPaymentKey(result, 802, "(290)FOOD PANDA", "FOODPANDA", config, paymentTypes);
            AddPaymentKey(result, 803, "(298)GRAB FOOD", "GRABFOOD", config, paymentTypes);
            AddPaymentKey(result, 820, "(1610)Fave Pay", "FAVEPAY", config, paymentTypes);
        }

        private static void AddPaymentKey(List<TicketOutputDto> result, int key, string name, string defaultValue,
            AppConfig config, List<PaymentTypeDto> paymentTypes)
        {
            var paymentValues = config.PaymentTypeMaps.ContainsKey(key)
                ? config.PaymentTypeMaps[key]
                : new List<string> { defaultValue };
            var paymentItem = paymentTypes.Where(d => paymentValues.Any(v => v.ToUpper() == d.Name.ToUpper()));
            result.Add(new TicketOutputDto
            {
                Number = key,
                Name = name,
                NoOfTimes = paymentItem.Count(),
                AmountOfMoney = paymentItem.Sum(t => t.Amount),
                NoOfSheets = paymentItem.Select(t => t.TicketId).Distinct().Count()
            });
        }

        private static void AddPromotionValueKey(List<TicketOutputDto> result, int key, string name,
            string defaultValue, AppConfig config, List<PromotionDto> promotionValues)
        {
            var promotionList = config.PaymentTypeMaps.ContainsKey(key)
                ? config.PaymentTypeMaps[key]
                : new List<string> { defaultValue };
            var promotionItems = promotionValues.Where(d => promotionList.Any(v => v.ToUpper() == d.Name.ToUpper()));
            result.Add(new TicketOutputDto
            {
                Number = key,
                Name = name,
                NoOfTimes = promotionItems.Count(),
                AmountOfMoney = promotionItems.Sum(t => t.Amount),
                NoOfSheets = promotionItems.Select(t => t.TicketId).Distinct().Count()
            });
        }

        private static void GetRounding(List<TicketOutputDto> result, List<Ticket> tickets)
        {
            string[] ROUNDTOTALS = { "ROUND+", "ROUND-", "ROUNDING" };
            var roundTotal = tickets.SelectMany(a => a.Transactions)
                .Where(a => ROUNDTOTALS.Contains(a.TransactionType.Name.ToUpper())).Sum(a => a.Amount);

            result.Add(new TicketOutputDto
            {
                Number = 66,
                Name = "端数値引",
                AmountOfMoney = roundTotal
            });
        }

        private static void GetVoid(List<TicketOutputDto> result, List<Ticket> tickets)
        {
            var voidTickets = tickets.Where(a => a.Orders.Any(o => !o.CalculatePrice
                                                                   && !o.DecreaseInventory
                                                                   && (o.OrderStates.Contains("Void")
                                                                       || o.OrderStates.Contains("Refund"))));

            result.Add(new TicketOutputDto
            {
                Number = 158,
                Name = "ＶＯＩＤ計",
                AmountOfMoney = voidTickets.Sum(t => t.TotalAmount),
                NoOfSheets = voidTickets.Count()
            });
            ;
        }

        private static void GetMenuGroup(List<TicketOutputDto> result, List<Ticket> tickets, AppConfig config, IUnitOfWork uow)
        {
            var orders = tickets.SelectMany(t => t.Orders);
            var itemsales = GetItemSales(orders, uow);

            result.Add(new TicketOutputDto
            {
                Number = 951,
                Name = "(651)Beverage  Sales(without GST)",
                AmountOfMoney = itemsales.Where(o => config.BeverageGroups.Contains(o.GroupName))
                    .Sum(o => o.Total)
            });

            result.Add(new TicketOutputDto
            {
                Number = 901,
                Name = "Food Sales(without GST)",
                AmountOfMoney = result.First(r => r.Number == 29).AmountOfMoney
                                - result.First(r => r.Number == 951).AmountOfMoney
                                + result.First(r => r.Number == 66).AmountOfMoney
                                + result.First(r => r.Number == 189).AmountOfMoney
            });

            result.Add(new TicketOutputDto
            {
                Number = 955,
                Name = "(652)Sales Bev Promotion(PLR)",
                AmountOfMoney = 0
            });
        }

        private static List<MenuListDto> GetItemSales(IEnumerable<Order> orders, IUnitOfWork uow)
        {
            var output = (from c in orders
                          group c by new { c.MenuItemPortionId, Location = c.Location_Id }
                        into g
                          select new { g.Key.Location, g.Key.MenuItemPortionId, Items = g });

            var outputOrderTemps = new List<OrderTemp>();
            foreach (var outputOrder in output)
            {
                var list2Dto = new List<OrderList2Dto>();
                foreach (var item in outputOrder.Items)
                {
                    var o = new OrderList2Dto();
                    o.Quantity = item.Quantity;
                    o.OrderPromotionDetails = item.OrderPromotionDetails;
                    o.PromotionAmount = item.PromotionAmount;
                    o.OrderTags = item.OrderTags;
                    o.OrderStates = item.OrderStates;
                    o.Price = item.Price;
                    o.MenuItemId = item.MenuItemId;
                    o.TicketNumber = item.Ticket.TicketNumber;
                    o.TaxIncluded = item.Ticket.TaxIncluded;
                    o.Tags = item.MenuItem?.Tag;
                    o.LocationName = item.Location?.Name;
                    o.LocationCode = item.Location?.Code;
                    o.CostPrice = item.MenuItemPortion?.CostPrice ?? 0;
                    list2Dto.Add(o);
                }
                outputOrderTemps.Add(new OrderTemp
                {
                    Location = outputOrder.Location,
                    MenuItemPortion = outputOrder.MenuItemPortionId,
                    Items = list2Dto
                });
            }

            var menuListOutputDots = new List<MenuListDto>();
            var dicCategory = new Dictionary<int, string>();
            var dicGroups = new Dictionary<int, string>();
            var aliasCodes = new Dictionary<int, string>();
            foreach (var dto in outputOrderTemps)
            {
                var firObj = dto.Items.First(); // First item in list of same MenuItem, Portion
                var cateDto = GetCategoryDto(dicCategory, dicGroups, aliasCodes, dto.Items.First().MenuItemId, uow);
                var sumQuantity = dto.Items.Sum(b => b.Quantity);
                var menL = dto.Items.Aggregate(new MenuListDto
                {
                    LocationId = dto.Location,
                    LocationCode = firObj.LocationCode,
                    LocationName = firObj.LocationName,
                    AliasCode = cateDto.AliasCode,
                    MenuItemPortionId = dto.MenuItemPortion,
                    MenuItemPortionName = firObj.PortionName,
                    MenuItemId = firObj.MenuItemId,
                    MenuItemName = firObj.MenuItemName,
                    CategoryId = cateDto.CatId,
                    CategoryName = cateDto.CatgoryName,
                    CategoryCode = cateDto.CatgoryCode,
                    GroupId = cateDto.GroupId,
                    GroupName = cateDto.GroupName,
                    //Quantity = dto.Items.Sum(a => a.Quantity),
                    CostPrice = firObj.CostPrice,
                    Tags = firObj.Tags,
                    IncludedTax = dto.Items.Any(i => i.TaxIncluded),
                    ActualTax = dto.Items.Sum(o => o.GetOrderTaxTotal())
                }, (item, next) =>
                {
                    item.Price = (item.Quantity + next.Quantity) != 0 ? (item.Price * item.Quantity + next.GetProductPrice() * next.Quantity) / (item.Quantity + next.Quantity) : 0;
                    item.TaxPrice = (item.Quantity + next.Quantity) != 0 ? (item.TaxPrice * item.Quantity + next.GetOrderPriceNoTaxWithTags() * next.Quantity) / (item.Quantity + next.Quantity) : 0;
                    item.Quantity += next.Quantity;
                    return item;
                }, item => item);

                menuListOutputDots.Add(menL);
            }

            return menuListOutputDots;
        }

        private static TCategoryDto GetCategoryDto(Dictionary<int, string> dicCategory, Dictionary<int, string> dicGroups,
            Dictionary<int, string> aliasCodes, int menuItem, IUnitOfWork uow)
        {
            var returnD = new TCategoryDto();

            if (dicCategory.Any() && dicCategory.ContainsKey(menuItem))
            {
                var catSplitname = dicCategory[menuItem];
                var splitName = catSplitname.Split('@');
                if (splitName.Any())
                {
                    returnD.CatId = Convert.ToInt32(splitName[0]);
                    returnD.CatgoryName = splitName[1];
                    returnD.CatgoryCode = splitName[2];
                }
            }
            else
            {
                var lSer = uow.MenuItems.Get(menuItem);
                if (lSer != null)
                {
                    returnD.CatId = lSer.CategoryId ?? 0;
                    returnD.CatgoryName = lSer.Category.Name;
                    returnD.CatgoryCode = lSer.Category.Code;
                    dicCategory.Add(menuItem, returnD.CatId + "@" + returnD.CatgoryName + "@" + returnD.CatgoryCode);
                }
            }

            if (dicGroups.Any() && dicGroups.ContainsKey(menuItem))
            {
                var catSplitname = dicGroups[menuItem];
                var splitName = catSplitname.Split('@');
                if (splitName.Any())
                {
                    returnD.GroupId = Convert.ToInt32(splitName[0]);
                    returnD.GroupName = splitName[1];
                }
            }
            else
            {
                var lSer = uow.MenuItems.Get(menuItem);
                if (lSer != null)
                {
                    returnD.GroupId = lSer.Category?.ProductGroupId ?? 0;
                    returnD.GroupName = lSer.Category?.ProductGroup?.Name;
                    dicGroups.Add(menuItem, returnD.GroupId + "@" + returnD.GroupName);
                }
            }

            if (aliasCodes.Any() && aliasCodes.ContainsKey(menuItem))
            {
                returnD.AliasCode = aliasCodes[menuItem];
            }
            else
            {
                var alisCodeSet = uow.MenuItems.Get(menuItem);
                aliasCodes.Add(menuItem, alisCodeSet.AliasCode);
                returnD.AliasCode = alisCodeSet.AliasCode;
            }

            return returnD;
        }

        internal class TCategoryDto
        {
            public int CatId { get; set; }
            public string CatgoryName { get; set; }

            public string CatgoryCode { get; set; }
            public int GroupId { get; set; }
            public string GroupName { get; set; }
            public string AliasCode { get; set; }
            public string LocationName { get; set; }
            public int PortionId { get; set; }
        }

        internal class OrderTemp
        {
            public int MenuItemPortion { get; set; }

            public int Location { get; set; }

            public List<OrderList2Dto> Items { get; set; }
        }

        private static string FormatMoney(decimal money)
        {
            return money.ToString("C");
        }

        private static decimal DivideByZero(decimal first, decimal second)
        {
            if (second == 0) return 0;
            return first / second;
        }

        private static decimal GetPaxCount(IEnumerable<Ticket> dDto)
        {
            //var totalPaxCount = 0;
            //var toaalll = dDto.Where(a => !string.IsNullOrEmpty(a.TicketTags))
            //    .Select(a => a.TicketTags);
            //foreach (var tag in toaalll)
            //    if (!string.IsNullOrEmpty(tag))
            //    {
            //        var myTag = JsonConvert.DeserializeObject<List<TicketTagValue>>(tag);
            //        var lastTag = myTag?.LastOrDefault(a =>
            //            a.TagName.Equals("COVERS") || a.TagName.Equals("PAX"));
            //        if (lastTag != null)
            //            totalPaxCount += ParseTag(lastTag.TagValue);
            //    }

            //if (totalPaxCount == 0) totalPaxCount = dDto.SelectMany(t => t.Orders).Sum(o => o.NumberOfPax);
            var totalPaxCount = dDto.SelectMany(t => t.Orders).Where(o => o.DecreaseInventory
                        && !o.IsInState("*", "Void")
                        && !o.IsInState("*", "Gift")
                        && !o.IsInState("*", "Comp")
                        && !o.IsInState("*", "Refund")).Sum(o => o.Quantity * (o.MenuItem.Portions?.FirstOrDefault(m => m.Id == o.MenuItemPortionId)?.NumberOfPax ?? 0));
            return totalPaxCount;
        }

        private static int ParseTag(string lastTagTagValue)
        {
            lastTagTagValue = lastTagTagValue.Replace("PAX", "");
            int.TryParse(lastTagTagValue, out var num);
            if (num > 0)
                return num;

            return 0;
        }

        private static List<PromotionDto> GetPromotionDetails(IEnumerable<Ticket> tickets)
        {
            var pdvs = new List<PromotionDto>();
            foreach (var ticket in tickets)
            {
                var pr = new List<PromotionDetailValue>();

                if (!string.IsNullOrEmpty(ticket.TicketPromotionDetails))
                {
                    var promotionDetails =
                        JsonConvert.DeserializeObject<List<PromotionDetailValue>>(ticket.TicketPromotionDetails);
                    pr.AddRange(promotionDetails);
                }

                foreach (var order in ticket.Orders)
                {
                    if (order.IsPromotionOrder
                                && !string.IsNullOrEmpty(order.OrderPromotionDetails)
                                && order.CalculatePrice
                                && order.DecreaseInventory)
                    {
                        var promotionOrders = order.OrderPromotionDetailsList.ToList();
                        foreach (var promotionOrder in promotionOrders)
                        {
                            promotionOrder.PromotionAmount = promotionOrder.PromotionAmount * order.Quantity;
                        }
                        pr.AddRange(promotionOrders);
                    }
                }

                pdvs.AddRange(pr.Select(p => new PromotionDto
                {
                    Name = p.PromotionName,
                    Amount = p.PromotionAmount,
                    TicketId = ticket.TicketId
                }));
            }

            return pdvs;
        }

        private static List<VoucherDto> GetVouchers(IEnumerable<Ticket> tickets, IUnitOfWork uow, AppConfig config)
        {
            var vouchers = uow.GiftVouchers
                .FindQuery(g => g.TenantId == config.TenantId)
                .Include(g => g.GiftVoucherType);

            var results = new List<VoucherDto>();
            foreach (var item in tickets)
            {
                var voucher = vouchers.FirstOrDefault(v => v.TicketNumber == item.TicketNumber);
                if (voucher != null)
                    results.Add(new VoucherDto
                    {
                        Name = voucher.GiftVoucherType.Name,
                        Amount = voucher.ClaimedAmount,
                        TicketId = item.TicketId
                    });
            }

            return results;
        }


        private static List<PaymentTypeDto> GetPaymentTypes(IEnumerable<Ticket> tickets)
        {
            var results = new List<PaymentTypeDto>();
            foreach (var item in tickets)
                foreach (var payment in item.Payments)
                    results.Add(new PaymentTypeDto
                    {
                        Name = payment.PaymentType.Name,
                        Amount = payment.Amount,
                        TicketId = item.TicketId
                    });

            return results;
        }

        private static void LzhZipFile(string filename, string outputFile)
        {
            LzhManager.fnCompressFiles(new List<string> { filename }, outputFile);
        }

        public static void UploadFtpFile(string fileName, AppConfig appConfig, string fptFile)
        {
            FtpWebRequest request;

            var absoluteFileName = fptFile;
            try
            {
                request = WebRequest.Create(new Uri(string.Format(@"ftp://{0}/{1}/", appConfig.FtpUrl,
                    appConfig.FtpDirectory))) as FtpWebRequest;
                if (request != null)
                {
                    request.Method = WebRequestMethods.Ftp.MakeDirectory;
                    request.UseBinary = true;
                    request.UsePassive = true;
                    request.KeepAlive = true;
                    request.Credentials = new NetworkCredential(appConfig.FtpUser, appConfig.FtpPassword);
                    var ftpResponse = (FtpWebResponse)request.GetResponse();
                }
            }
            catch (Exception)
            {
                // ignored
            }

            try
            {
                request = WebRequest.Create(new Uri(string.Format(@"ftp://{0}/{1}/{2}", appConfig.FtpUrl,
                    appConfig.FtpDirectory, absoluteFileName))) as FtpWebRequest;
                if (request != null)
                {
                    request.Method = WebRequestMethods.Ftp.UploadFile;
                    request.UseBinary = true;
                    request.UsePassive = true;
                    request.KeepAlive = true;
                    request.Credentials = new NetworkCredential(appConfig.FtpUser, appConfig.FtpPassword);
                    request.ConnectionGroupName = "group";

                    using (var fs = File.OpenRead(fileName))
                    {
                        var buffer = new byte[fs.Length];
                        fs.Read(buffer, 0, buffer.Length);
                        fs.Close();
                        var requestStream = request.GetRequestStream();
                        requestStream.Write(buffer, 0, buffer.Length);
                        requestStream.Flush();
                        requestStream.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}