﻿using System;
using System.Collections.Generic;

namespace SFBIFoodItConsole
{
    public class AppConfig
    {
        public string ProcessingFolder { get; set; }
        public string BackupFolder { get; set; }
        public string FtpUrl {get;set;}
        public string FtpUser {get;set;}
        public string FtpPassword {get;set;}
        public string FtpDirectory {get;set;}

        public List<string> EatInDepartmentNames { get; set; }
        public List<string> TakeAwayDepartmentNames { get; set; }

        public List<string> DeliveryDepartmentNames { get; set; }

        public List<string> FoodGroups { get; set; }

        public List<string> BeverageGroups { get; set; }


        public string FilePattern { get; set; }

        public int TenantId { get; set; }
        public int CompanyId { get; set; }
        public int AppUserId { get; set; }

        public int RunCycle { get; set; }

        public int? LocationId { get; set; }

        public Dictionary<int, List<string>> PaymentTypeMaps = new Dictionary<int, List<string>>();

        public DateTime? ProcessingDate { get; set; }

    }
}