﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Castle.Core.Logging;
using Castle.Windsor;
using ConnectDatabaseCommon.Data;
using PttDatabaseCommon.Data;
using SFBIFoodItConsole.Process.Executor;

namespace SFBIFoodItConsole
{
    public class CoreService
    {
        private readonly AppConfig _config;
        private readonly IWindsorContainer _container;
        private PttInboundDbContext _dbContext;

        public CoreService(
            IWindsorContainer container,
            AppConfig config, PttInboundDbContext dbContext)
        {
            _container = container;
            _config = config;
            Logger = NullLogger.Instance;
            _dbContext = dbContext;
        }

        public ILogger Logger { get; set; }




       

        public void Start()
        {
            Logger.Info("Job Starts");

            bool isSucceed = true;
            try
            {
                using (var uow = _container.Resolve<IUnitOfWork>())
                {
                    string reason = OutboundExecutor.Run(_config, uow, _dbContext);
                    if (!string.IsNullOrEmpty(reason))
                    {
                        throw new Exception(reason);
                    }
                    uow.Complete();
                }


            }
            catch (Exception ex)
            {
                Logger.Info("Job Failure");
                Logger.Error($"Error=true;{ex.Message}", ex);
                isSucceed = false;
            }
            finally
            {
                if (isSucceed)
                {
                    Logger.Info("Job Completed Successfully");
                }
            }

            
            Logger.Info("Job Ends");
        }

      

     
    }
}