﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using Castle.Core.Logging;
using Castle.Facilities.Logging;
using Castle.MicroKernel.Registration;
using Castle.Services.Logging.Log4netIntegration;
using Castle.Windsor;
using ConnectDatabaseCommon.Data;
using PttDatabaseCommon.Data;
using PttDatabaseCommon.Data.Repositories;

namespace SFBIFoodItConsole

{
    public class Program
    {
        private static void Main(string[] args)
        {

            var container = new WindsorContainer();
            container.AddFacility<LoggingFacility>(f => f.LogUsing<Log4netFactory>());
            var logger = container.Resolve<ILogger>();
            var appConfig = GetAppConfig(logger);
            container.Register(
                Component.For<CoreService>().LifestyleSingleton(),
                Component.For<AppConfig>().Instance(appConfig).LifestyleSingleton(),
                Component.For<IWindsorContainer>().Instance(container).LifestyleSingleton(),
                Component.For<PttInboundDbContext>().LifestyleTransient(),
                Component.For<IUnitOfWork>().ImplementedBy<UnitOfWork>().LifestyleTransient()
            );

            var uow = container.Resolve<CoreService>();
            uow.Start();
        }

        private static AppConfig GetAppConfig(ILogger logger)
        {
            try
            {
                var appConfig = new AppConfig
                {
                    BackupFolder = ConfigurationManager.AppSettings["BackupFolder"],
                    ProcessingFolder = ConfigurationManager.AppSettings["ProcessingFolder"],
                    FilePattern = ConfigurationManager.AppSettings["FilePattern"],
                    EatInDepartmentNames = ConfigurationManager.AppSettings["EatInDepartmentNames"].Split(new char[] { ',' }).Select(t => t.Trim()).ToList(),
                    TakeAwayDepartmentNames = ConfigurationManager.AppSettings["TakeAwayDepartmentNames"].Split(new char[] { ',' }).Select(t => t.Trim()).ToList(),
                    DeliveryDepartmentNames = ConfigurationManager.AppSettings["DeliveryDepartmentNames"].Split(new char[] { ',' }).Select(t => t.Trim()).ToList(),
                    FoodGroups = ConfigurationManager.AppSettings["FoodGroups"].Split(new char[] { ',' }).Select(t => t.Trim()).ToList(),
                    BeverageGroups = ConfigurationManager.AppSettings["BeverageGroups"].Split(new char[] { ',' }).Select(t => t.Trim()).ToList(),
                    FtpUrl = ConfigurationManager.AppSettings["FtpUrl"],
                    FtpUser = ConfigurationManager.AppSettings["FtpUser"],
                    FtpPassword = ConfigurationManager.AppSettings["FtpPassword"],
                    FtpDirectory = ConfigurationManager.AppSettings["FtpDirectory"],
                };

                var tenant = ConfigurationManager.AppSettings["TenantId"];
                appConfig.TenantId = int.TryParse(tenant, out var second) ? second : 1;

                var company = ConfigurationManager.AppSettings["CompanyId"];
                appConfig.CompanyId = int.TryParse(company, out var third) ? third : 1;

                var appUserId = ConfigurationManager.AppSettings["AppUserId"];
                appConfig.AppUserId = int.TryParse(appUserId, out var fourth) ? fourth : 1;

                var runCycle = ConfigurationManager.AppSettings["RunCycle"];
                appConfig.RunCycle = int.TryParse(runCycle, out var fifth) ? fifth : 1;

                var locationId = ConfigurationManager.AppSettings["LocationId"];
                appConfig.LocationId = int.TryParse(locationId, out var location) ? location : (int?)null;

                if (DateTime.TryParseExact(ConfigurationManager.AppSettings["ProcessingDate"], "yyyy/MM/dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out var processingDateTime))
                {
                    appConfig.ProcessingDate = processingDateTime;
                }

                var keys = ConfigurationManager.AppSettings.Keys.Cast<string>().ToList();
                for (int i = 100; i <= 131; i++)
                {
                    if (keys.Contains(i.ToString()) && !appConfig.PaymentTypeMaps.ContainsKey(i))
                    {
                        appConfig.PaymentTypeMaps.Add(i, ConfigurationManager.AppSettings[i.ToString()].Split(new char[] { ',' }).Select(t => t.Trim()).ToList());
                    }
                }



                ValidateFolder(logger, appConfig.ProcessingFolder, "ProcessingFolder - Available");
                ValidateFolder(logger, appConfig.BackupFolder, "ProcessingFolder - Available");

                return appConfig;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                throw;
            }
        }

        private static void ValidateFolder(ILogger logger, string path, string folderName)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            logger.Info($"{folderName}'s information was checked successfully.");
        }
    }
}