﻿using System;
using System.Configuration;
using System.IO;
using Castle.Core.Logging;
using Castle.Facilities.Logging;
using Castle.MicroKernel.Registration;
using Castle.Services.Logging.Log4netIntegration;
using Castle.Windsor;
using PttDatabaseCommon.Data;
using PttDatabaseCommon.Data.Repositories;
using PttOrRevenueConsole.Data;

namespace PttOrRevenueConsole

{
    public class Program
    {
        private static void Main(string[] args)
        {
            var container = new WindsorContainer();

            container.AddFacility<LoggingFacility>(f => f.LogUsing<Log4netFactory>());

            var logger = container.Resolve<ILogger>();

            var appConfig = GetAppConfig(logger);

            container.Register(
                Component.For<CoreService>().LifestyleSingleton(),
                Component.For<AppConfig>().Instance(appConfig).LifestyleSingleton(),
                Component.For<IWindsorContainer>().Instance(container).LifestyleSingleton(),
                Component.For<PttInboundDbContext>().LifestyleTransient(),
                Component.For<IUnitOfWork>().ImplementedBy<UnitOfWork>().LifestyleTransient()
            );

            var uow = container.Resolve<CoreService>();
            uow.Start();
        }

        private static AppConfig GetAppConfig(ILogger logger)
        {
            try
            {
                var appConfig = new AppConfig();
                var tenant = ConfigurationManager.AppSettings["TenantId"];
                appConfig.Tenant = int.TryParse(tenant, out var second) ? second : 1;
                return appConfig;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                throw;
            }
        }

        private static void ValidateFolder(ILogger logger, string path, string folderName)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            logger.Info($"{folderName}'s information was checked successfully.");
        }
    }
}