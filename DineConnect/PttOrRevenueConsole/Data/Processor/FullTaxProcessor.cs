﻿using System;
using System.Linq;
using DinePlan.DineConnect.Connect.Invoice;
using PttDatabaseCommon.Data;

namespace PttOrRevenueConsole.Data.Processor
{
    public class FullTaxProcessor
    {
        public static string Process(FullTaxInvoiceDto dto, AppConfig config, IUnitOfWork uow, PttInboundDbContext dbContext)
        {
            string returnText = "";
            try
            {
                var fMember = uow.FullTaxMembers.Find(t => t.BranchId.Equals(dto.BranchId) && t.TaxId.Equals(dto.TaxId) 
                    && t.TenantId == config.Tenant).LastOrDefault();

                bool update = false;
                if (fMember != null)
                {
                    update = true;
                }
                else
                {
                    fMember = new FullTaxMember();
                }

                fMember.BranchId = dto.BranchId;
                fMember.TaxId = dto.TaxId;
                fMember.Name = dto.Name;
                fMember.BranchName = dto.Name;
                fMember.TenantId = config.Tenant;

                if (update)
                {
                    uow.FullTaxMembers.Update(fMember);
                }
                else
                {
                    uow.FullTaxMembers.Add(fMember);
                }

                uow.Complete();

                var fAddress = uow.FullTaxAddresses.Find(t => t.FullMemberId == fMember.Id && t.Address1.Equals(dto.Address1)).LastOrDefault();

                if (fAddress != null)
                {
                    update = true;
                }
                else
                {
                    fAddress = new FullTaxAddress();
                }
                fAddress.Address1 = dto.Address1;
                fAddress.Address2 = dto.Address2;
                fAddress.City = dto.City;
                fAddress.PostCode = dto.PostCode;
                fAddress.State = dto.State;
                fAddress.FullMemberId = fMember.Id;

                if (update)
                {
                    uow.FullTaxAddresses.Update(fAddress);
                }
                else
                {
                    uow.FullTaxAddresses.Add(fAddress);
                }

                uow.Complete();
            }
            catch (Exception e)
            {
                return e.Message;
            }

            return returnText;

        }
    }
}
