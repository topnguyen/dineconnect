﻿using System;
using System.Linq;
using Castle.Core.Logging;
using Castle.Windsor;
using PttDatabaseCommon.Data;
using PttOrRevenueConsole.Data.Processor;
using PttOrRevenueConsole.RevenueDeptService;

namespace PttOrRevenueConsole
{
    public class CoreService
    {
        private readonly AppConfig _config;
        private readonly IWindsorContainer _container;
        private PttInboundDbContext _dbContext;

        public CoreService(
            IWindsorContainer container,
            AppConfig config, PttInboundDbContext dbContext)
        {
            _container = container;
            _config = config;
            Logger = NullLogger.Instance;
            _dbContext = dbContext;
        }

        public ILogger Logger { get; set; }


        public void Start()
        {
            try
            {
                Logger.Info("Job Starts");
                System.Net.ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => true;

                vatserviceRD3SoapClient client = new vatserviceRD3SoapClient();
                string text = System.IO.File.ReadAllText(@"ProvinceCodes.txt");

                if (!string.IsNullOrEmpty(text))
                {
                    string[] allCodes = text.Split(',');

                    foreach (var allCode in allCodes)
                    {
                        try
                        {
                            var trimCode = allCode.Trim();
                            var myCode = Convert.ToInt32(trimCode);
                            if (myCode > 0)
                            {
                                var output = client.Service("anonymous", "anonymous", "", "", myCode, 0, 0);

                                if (output?.vAmphur != null && output.vAmphur.Any())
                                {
                                    Logger.Info("Processing : " + myCode);
                                    ProcessOutput(output);
                                    Logger.Info("End Processing : " + myCode);
                                }
                        
                            }
                        }
                        catch (Exception myEx)
                        {
                            Logger.Info("Error in Inside Output : " + myEx.Message);
                            continue;
                        }
                      
                    }
                }
                else
                {
                    Logger.Info("No Content in the Text File");

                }

                Logger.Info("Job Ends");
            }
            catch (Exception ex)
            {
                Logger.Info("Error in Program : " + ex.Message);

            }
            

        }

        private void ProcessOutput(vat output)
        {
            int totalBounds = output.vAmphur.Count;

            FullTaxInvoiceDto dto = new FullTaxInvoiceDto();

            for (int i = 0; i < totalBounds; i++)
            {
                dto = new FullTaxInvoiceDto();
                
                dto.TaxId += output.vNID[i];
                dto.BranchId += output.vBranchNumber[i];

                dto.Name += output.vBranchTitleName[i];
                dto.Name += output.vBranchName[i];
                
                dto.Address1 += output.vHouseNumber[i];
                dto.Address1 += output.vFloorNumber[i];
                dto.Address1 += output.vRoomNumber[i];
                dto.Address1 += output.vBuildingName[i];
                dto.Address1 += output.vVillageName[i];

                dto.Address2 += output.vMooNumber[i];
                dto.Address2 += output.vSoiName[i];
                dto.Address2 += output.vStreetName[i];
                dto.Address2 += output.vThambol[i];

                dto.City += output.vAmphur[i];
                dto.State += output.vProvince[i];
                dto.PostCode += output.vPostCode[i];

                using (var uow = _container.Resolve<IUnitOfWork>())
                {
                    string reason = FullTaxProcessor.Process(dto,_config, uow, _dbContext);
                    if (!string.IsNullOrEmpty(reason))
                    {
                        throw new Exception(reason);
                    }
                    uow.Complete();
                }


            }
        }
    }

    public class FullTaxInvoiceDto
    {
        public string TaxId { get; set; }
        public string BranchId { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostCode  { get; set; }

    }
}