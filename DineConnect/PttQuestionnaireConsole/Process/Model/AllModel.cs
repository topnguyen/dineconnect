﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace PttQuestionnaireConsole.Process.Model
{
    [System.Xml.Serialization.XmlRoot("QuestionnaireMasterSend_MT")]
    public class QuestionnaireMaster
    {
        [XmlElement("Question")]
        public List<QuestionnaireChild> QuestionnaireChilds { get; set; }

        public QuestionnaireMaster()
        {
            QuestionnaireChilds = new List<QuestionnaireChild>();
        }
    }
    public class QuestionnaireChild
    {
        [XmlElement("QUESTION_ID")]
        public string QuestionId { get; set; }

        [XmlElement("QUESTION_NAME")]
        public string QuestionName { get; set; }

        [XmlElement("BRAND")]
        public string QuestionnaireBrand { get; set; }
        
        [XmlElement("DEL_FLAG")]
        public string QuestionnaireDeleted { get; set; }

        [XmlElement("CHANGED_DATE")]
        public string QuestionnaireChangeDate { get; set; }

        [XmlElement("LANGUAGE_KEY")]
        public string LanguageKey { get; set; }
   
        [XmlElement("Answer")]
        public List<QuestionnaireSubChild> QuestionnaireSubChilds { get; set; }

        public QuestionnaireChild()
        {
            QuestionnaireSubChilds = new List<QuestionnaireSubChild>();
        }
    }
    public class QuestionnaireSubChild
    {
        [XmlElement("ANSWER_ID")]
        public string Id { get; set; }

        [XmlElement("ANSWER_NAME")]
        public string Name { get; set; }
    }
}