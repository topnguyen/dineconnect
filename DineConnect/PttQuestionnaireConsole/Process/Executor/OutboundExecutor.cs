﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using DinePlan.DineConnect.Connect.MultiLingual;
using DinePlan.DineConnect.Connect.Tag;
using Minio;
using PttDatabaseCommon.Data;
using PttQuestionnaireConsole.Process.Model;

namespace PttQuestionnaireConsole.Process.Executor
{
    public class OutboundExecutor
    {
        public static string DateTimeFileFormat = "yyyyMMddHHmmss";



        public static string TimeFormat = "HHmmss";

        public static string Run(MinioClient client, AppConfig config, IUnitOfWork uow, PttInboundDbContext dbContext)
        {
            var myBuilderReason = new StringBuilder();

            try
            {
                var pm = new QuestionnaireMaster();

                foreach (var mytenantId in config.TenantIds.Split(','))
                {
                    var tenantId = Convert.ToInt32(mytenantId);
                    var tagGroups = uow.TicketTagGroups.Find(a => a.TenantId == tenantId);

                    UpdateValue(config, uow, dbContext, "en", tenantId, tagGroups, pm);
                    UpdateValue(config, uow, dbContext, "th", tenantId, tagGroups, pm);


                }

                if (pm.QuestionnaireChilds.Any())
                {
                    var filePath = Path.Combine(config.InitialFolder,
                        "QuestionnaireMaster_" + DateTime.Now.ToString("yyyyMMddHHmmss",
                                                   CultureInfo.CreateSpecificCulture("en-EN"))
                                               + ".XML");

                    TextWriter writer = new StreamWriter(filePath);
                    var serializer =
                        new XmlSerializer(typeof(QuestionnaireMaster));

                    serializer.Serialize(writer, pm);
                    writer.Close();

                    if (File.Exists(filePath))
                    {
                        MoveToBucket(client, config, new FileInfo(filePath),config.MinioQuestionBucket);
                    }
                }
            }
            catch (Exception e)
            {
                myBuilderReason.Append(e.Message);
            }

            return myBuilderReason.ToString();
        }

        public static void UpdateValue(AppConfig config, IUnitOfWork uow, PttInboundDbContext dbContext,string langaugeCode,
            int tenantId, IEnumerable<TicketTagGroup> tagGroups, QuestionnaireMaster pm)
        {
                    foreach (var tg in tagGroups)
                    {
                        var serverLangauge =
                            uow.LangaugeDescriptions.Find(a =>
                                    a.ReferenceId == tg.Id && a.TenantId == tenantId && 
                                    a.LanguageDescriptionType == LanguageDescriptionType.TicketTagGroup && a.LanguageCode.ToUpper().Equals(langaugeCode))
                                .LastOrDefault();

                        var myChild = new QuestionnaireChild
                        {
                            QuestionId = tg.Id.ToString(),
                            QuestionName = serverLangauge!=null?serverLangauge.Name.ToUpper():tg.Name.ToUpper(),
                            QuestionnaireDeleted = tg.IsDeleted ? "X" : "",
                            QuestionnaireChangeDate = tg.LastModificationTime != null
                                ? tg.LastModificationTime.Value.ToString(DateTimeFileFormat)
                                : tg.CreationTime.ToString(DateTimeFileFormat),
                            LanguageKey = langaugeCode.ToUpper()
                        };

                        if (tg.Oid > 0)
                        {
                            var myCompany = uow.Companies.Find(a => a.Id == tg.Oid).LastOrDefault();

                            if (myCompany != null) myChild.QuestionnaireBrand = myCompany.Code;
                        }

                        myChild.QuestionnaireSubChilds = new List<QuestionnaireSubChild>();
                        foreach (var ticketTag in tg.Tags)
                        {
                            var myTagKey =
                                uow.LangaugeDescriptions.Find(a =>
                                        a.ReferenceId == ticketTag.Id && a.TenantId == tenantId && 
                                        a.LanguageDescriptionType == LanguageDescriptionType.TicketTag && 
                                        a.LanguageCode.ToUpper().Equals(langaugeCode))
                                    .LastOrDefault();

                            myChild.QuestionnaireSubChilds.Add(new QuestionnaireSubChild
                            {
                                Id = ticketTag.Id.ToString(),
                                Name =  myTagKey!=null?myTagKey.Name.ToUpper():ticketTag.Name.ToUpper(),
                            });
                        }

                        pm.QuestionnaireChilds.Add(myChild);
                    }

        }
        public static void MoveToBucket(MinioClient client, AppConfig _config, FileInfo fileInfo, string bucketName)
        {

            var processingFile = GetSafeFileName(_config.InitialFolder, fileInfo,true);

            var nameWithoutExtenstion = Path.GetFileName(processingFile);
            if (client != null)
            {
                client.PutObjectAsync(bucketName, nameWithoutExtenstion, fileInfo.FullName).Wait();

                if (File.Exists(fileInfo.FullName))
                    File.Delete(fileInfo.FullName);
            }

           

        }

        private static string GetSafeFileName(string path, FileInfo fi, bool appendDate = false)
        {
            var extension = fi.Extension;
            var nameWithoutExtenstion = Path.GetFileNameWithoutExtension(fi.FullName);
            nameWithoutExtenstion = nameWithoutExtenstion + extension;
            var safePath = path;
            safePath = !path.EndsWith("\\")
                ? $"{safePath}\\{nameWithoutExtenstion}"
                : $"{safePath}{nameWithoutExtenstion}";
            return safePath;
        }
    }


}