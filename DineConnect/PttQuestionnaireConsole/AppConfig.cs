﻿namespace PttQuestionnaireConsole
{
    public class AppConfig
    {
        public string InitialFolder { get; set; }
        public string FilePattern { get; set; }
        public string MinioServerUrl { get; set; }
        public string MinioApiKey { get; set; }
        public string MinioApiSecret { get; set; }
        public string MinioQuestionBucket { get; set; }

        public string TenantIds { get; set; }

    }
}