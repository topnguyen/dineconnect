﻿using System;
using Castle.Core.Logging;
using Castle.Windsor;
using Minio;
using PttDatabaseCommon.Data;
using PttQuestionnaireConsole.Process.Executor;

namespace PttQuestionnaireConsole
{
    public class CoreService
    {
        private readonly AppConfig _config;
        private readonly IWindsorContainer _container;
        private PttInboundDbContext _dbContext;
        private readonly MinioClient _minioClient;

        public CoreService(
            IWindsorContainer container,
            AppConfig config, PttInboundDbContext dbContext)
        {
            _container = container;
            _config = config;
            Logger = NullLogger.Instance;
            _dbContext = dbContext;

            _minioClient = new MinioClient
                (_config.MinioServerUrl, _config.MinioApiKey, _config.MinioApiSecret);
        }

        public ILogger Logger { get; set; }




       

        public void Start()
        {

            bool isSucceed = true;
            try
            {
                Logger.Info("Job Starts");

                var myInitialBucket = _minioClient.BucketExistsAsync(_config.MinioQuestionBucket).Result;
                if (!myInitialBucket)
                {
                    throw new Exception("Question Bucket is not available : " + _config.MinioQuestionBucket);
                }

                using (var uow = _container.Resolve<IUnitOfWork>())
                {
                    string reason = OutboundExecutor.Run(_minioClient,_config, uow, _dbContext);
                    if (!string.IsNullOrEmpty(reason))
                    {
                        throw new Exception(reason);
                    }
                    uow.Complete();
                }
            }
            catch (Exception ex)
            {
                Logger.Info("Job Failure");
                Logger.Error($"Error=true;{ex.Message}", ex);
                isSucceed = false;
            }
            finally
            {
                if (isSucceed)
                {
                    Logger.Info("Job Completed Successfully");
                }
            }

            
            Logger.Info("Job Ends");
        }

      

     
    }
}