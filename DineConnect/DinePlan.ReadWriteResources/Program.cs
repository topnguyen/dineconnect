﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using OfficeOpenXml;
using OfficeOpenXml.Style;


namespace DinePlan.ReadWriteResources
{
    internal class Program
    {
        private static readonly string _readPath =
            @"C:\_Work\DinePlan\Code\DineConnect\Trunk\DineConnect\DinePlan.DineConnect.Core\Localization\DineConnect\";

        private static readonly string _outputFilePath =
           @"C:\_Work\DinePlan\Code\DineConnect\Trunk\Output\";


        private static readonly string _inputFile =
           @"C:\_Work\DinePlan\Code\DineConnect\Trunk\Output\Input.xlsx";

        static readonly Dictionary<string, string> ReadFiles = new Dictionary<string, string>
        {
            { "DineConnect.xml", "en" },
            { "DineConnect-zh-CN.xml", "zh-CN" }
        };

        private static readonly List<Localizable> _lo = new List<Localizable>();

        private static void Main(string[] args)
        {
            Console.WriteLine("---------STARTING------------");

            ReadResources();
            Console.WriteLine("*********READ COMPLETED********");

            WriteResources();
            Console.WriteLine("********WRITE COMPLETED********");

            Console.Write("Generate from INPUT: Y/N ");
            ConsoleKeyInfo info = Console.ReadKey();

            if (info.Key.Equals(ConsoleKey.Y))
            {
                Console.WriteLine();
                Console.WriteLine("******GENERATE FILE STARTED*************");
                GenerateFiles();
                Console.WriteLine("******GENERATE FILE DONE****************");
            }

        }

        private static void GenerateFiles()
        {
            if (File.Exists(_inputFile))
            {
                var excelData = new ExcelData(_inputFile);
                var allList = excelData.getData(excelData.getWorksheetNames().First());
                foreach (var value in ReadFiles.Values)
                {
                    StringBuilder builder = new StringBuilder();
                    builder.Append("<texts>");
                    foreach (var row in allList)
                    {
                        if (row["Name"] != null && row.Table.Columns.Contains(value))
                        {
                            builder.Append("<text name=\"" + row["Name"].ToString() + "\">");
                            builder.Append(row[value].ToString());
                            builder.Append("</text>");
                            builder.Append("\r\n");
                        }
                    }
                    builder.Append("</texts>");
                    File.WriteAllText(Path.Combine(_outputFilePath, value + ".xml").ToString(), builder.ToString());
                }
            }
        }

        private static void ReadResources()
        {
            foreach (var readFile in ReadFiles.Keys)
            {
                var path = Path.Combine(_readPath, readFile);
                if (File.Exists(path))
                {
                    var languageCode = ReadFiles[readFile];
                    using (var reader = XmlReader.Create(@path))
                    {
                        var resName = "";
                        while (reader.Read())
                        {
                            if (reader.IsStartElement())
                            {
                                switch (reader.Name)
                                {
                                    case "text":
                                        resName = reader["name"];
                                        var valueF = reader["value"];
                                        if (string.IsNullOrEmpty(valueF))
                                        {
                                            if (reader.Read())
                                            {
                                                valueF = reader.Value.Trim();
                                            }
                                        }
                                        Localizable loca = null;
                                        if (_lo.Any())
                                        {
                                            loca = _lo.SingleOrDefault(a => a.Name.Equals(resName));
                                        }
                                        if (loca == null)
                                        {
                                            loca = new Localizable
                                            {
                                                Name = resName
                                            };
                                            _lo.Add(loca);
                                        }

                                        loca.Values[languageCode] = valueF;
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }
        private static void WriteResources()
        {
            var path = Path.Combine(_outputFilePath, DateTime.Now.ToLongDateString() + ".xlsx");
            if (File.Exists(path))
                File.Delete(path);

            var file = new FileInfo(path);

            List<string> columnNames = new List<string> { "Name" };
            columnNames.AddRange(ReadFiles.Values);


            using (var package = new ExcelPackage(file))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("TRANSLATIONS" + DateTime.Now.ToShortDateString());

                var row = 1;
                var col = 1;

                foreach (var cn in columnNames)
                {
                    worksheet.Cells[row, col].Value = cn;
                    worksheet.Column(col).AutoFit();
                    worksheet.Cells[1, col].Style.Font.Bold = true;
                    col++;
                }
                row++;
                foreach (var localizable in _lo)
                {
                    col = 1;

                    worksheet.Cells[row, col++].Value = localizable.Name;

                    foreach (var value in ReadFiles.Values)
                    {
                        if (localizable.Values.ContainsKey(value) && !string.IsNullOrEmpty(value))
                        {
                            worksheet.Cells[row, col++].Value = localizable.Values[value];
                        }
                        else
                        {
                            worksheet.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[row, col].Style.Fill.BackgroundColor.SetColor(Color.Red);
                            worksheet.Cells[row, col].Value = "";
                            col++;

                        }
                    }
                    row++;
                }
                package.Save();
            }

        }

    }

    public class Localizable
    {
        public string Name { get; set; }
        public Dictionary<string, string> Values { get; set; }
        public Localizable()
        {
            Values = new Dictionary<string, string>();
        }

    }
}