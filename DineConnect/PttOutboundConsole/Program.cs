﻿using System;
using System.Configuration;
using System.IO;
using Castle.Core.Logging;
using Castle.Facilities.Logging;
using Castle.MicroKernel.Registration;
using Castle.Services.Logging.Log4netIntegration;
using Castle.Windsor;
using PttDatabaseCommon.Data;
using PttDatabaseCommon.Data.Repositories;

namespace PttOutboundConsole

{
    public class Program
    {
        private static void Main(string[] args)
        {

            var container = new WindsorContainer();
            container.AddFacility<LoggingFacility>(f => f.LogUsing<Log4netFactory>());

            var logger = container.Resolve<ILogger>();

            var appConfig = GetAppConfig(logger);

          
         
            container.Register(
                Component.For<CoreService>().LifestyleSingleton(),
                Component.For<AppConfig>().Instance(appConfig).LifestyleSingleton(),
                Component.For<IWindsorContainer>().Instance(container).LifestyleSingleton(),
                Component.For<PttInboundDbContext>().LifestyleTransient(),
                Component.For<IUnitOfWork>().ImplementedBy<UnitOfWork>().LifestyleTransient()
            );

            var uow = container.Resolve<CoreService>();
            uow.Start();
        }

        private static AppConfig GetAppConfig(ILogger logger)
        {
            try
            {
                var appConfig = new AppConfig
                {
                    ProcessingFolder = ConfigurationManager.AppSettings["ProcessingFolder"],
                    FilePattern = ConfigurationManager.AppSettings["FilePattern"],
                    InternalSalesDepartments = ConfigurationManager.AppSettings["InternalSalesDepartmentNames"],
                    CreditSalesDepartments = ConfigurationManager.AppSettings["CreditSalesDepartmentNames"],
                    MinioServerUrl=ConfigurationManager.AppSettings["MinioServerUrl"],
                    MinioApiKey=ConfigurationManager.AppSettings["MinioApiKey"],
                    MinioApiSecret=ConfigurationManager.AppSettings["MinioApiSecret"],
                    MinioProcessedBucket=ConfigurationManager.AppSettings["MinioProcessedBucket"]
                };

                var tenant = ConfigurationManager.AppSettings["TenantId"];
                appConfig.TenantId = int.TryParse(tenant, out var second) ? second : 1;
                
                var company = ConfigurationManager.AppSettings["CompanyId"];
                appConfig.CompanyId = int.TryParse(company, out var third) ? third : 1;

                var appUserId = ConfigurationManager.AppSettings["AppUserId"];
                appConfig.AppUserId = int.TryParse(appUserId, out var fourth) ? fourth : 1;

                var runCycle = ConfigurationManager.AppSettings["RunCycle"];
                appConfig.RunCycle = int.TryParse(runCycle, out var fifth) ? fifth : 1;


                var maxTicketCount = ConfigurationManager.AppSettings["MaxTicketCount"];
                appConfig.MaxTicketCount = int.TryParse(maxTicketCount, out var sixth) ? sixth : 1;


                ValidateFolder(logger, appConfig.ProcessingFolder, "ProcessingFolder - Available");

                return appConfig;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                throw;
            }
        }

        private static void ValidateFolder(ILogger logger, string path, string folderName)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            logger.Info($"{folderName}'s information was checked successfully.");
        }
    }
}