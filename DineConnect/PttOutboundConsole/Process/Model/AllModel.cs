﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace PttOutboundConsole.Process.Model
{
    [XmlRoot(ElementName = "WORKDAYXML")]
    public class WORKDAYXML
    {
        public WORKDAYXML()
        {
            DISCOUNTS = new List<WORKSHIFTDISCOUNT>();
            TAXS = new List<WORKSHIFTTAX>();
            TENDERS = new List<WORKSHIFTTENDER>();
        }
        [XmlElement(ElementName = "RETAILSTOREID")]
        public string RETAILSTOREID { get; set; }

        [XmlElement(ElementName = "BUSINESSDAYDATE")]
        public string BUSINESSDAYDATE { get; set; }

        [XmlElement(ElementName = "TRANSACTIONTYPECODE")]
        public string TRANSACTIONTYPECODE { get; set; }

        [XmlElement(ElementName = "WORKSTATIONID")]
        public string WORKSTATIONID { get; set; }

        [XmlElement(ElementName = "TRANSACTIONSEQUENCENUMBER")]
        public string TRANSACTIONSEQUENCENUMBER { get; set; }

        [XmlElement(ElementName = "BEGINDATETIMESTAMP")]
        public string BEGINDATETIMESTAMP { get; set; }

        [XmlElement(ElementName = "ENDDATETIMESTAMP")]
        public string ENDDATETIMESTAMP { get; set; }

        [XmlElement(ElementName = "OPERATORID")]
        public string OPERATORID { get; set; }

        [XmlElement(ElementName = "TRANSACTIONCURRENCY")]
        public string TRANSACTIONCURRENCY { get; set; }

        [XmlElement(ElementName = "ORIGTRANSNUMBER")]
        public string ORIGTRANSNUMBER { get; set; }

        [XmlElement(ElementName = "PARTNERID")]
        public string PARTNERID { get; set; }

        [XmlElement(ElementName = "RETAILTYPECODE")]
        public string RETAILTYPECODE { get; set; }

        [XmlElement(ElementName = "SALESAMOUNT")]
        public string SALESAMOUNT { get; set; }

        [XmlElement(ElementName = "LINEITEMCOUNT")]
        public int LINEITEMCOUNT { get; set; }

        public List<WORKSHIFTDISCOUNT> DISCOUNTS { get; set; }
        public List<WORKSHIFTTAX> TAXS { get; set; }
        public List<WORKSHIFTTENDER> TENDERS { get; set; }
    }

    [XmlRoot(ElementName = "WORKSHIFTDISCOUNT")]
    public class WORKSHIFTDISCOUNT
    {
        [XmlElement(ElementName = "DISCOUNTTYPE")]
        public string DISCOUNTTYPE { get; set; }

        [XmlElement(ElementName = "REDUCTIONAMOUNT")]
        public string REDUCTIONAMOUNT { get; set; }

        [XmlElement(ElementName = "DISCOUNTCOUNT")]
        public int DISCOUNTCOUNT { get; set; }
    }
    
    [XmlRoot(ElementName = "WORKSHIFTTAX")]
    public class WORKSHIFTTAX
    {
        [XmlElement(ElementName = "TAXTYPECODE")]
        public string TAXTYPECODE { get; set; }

        [XmlElement(ElementName = "TAXAMOUNT")]
        public string TAXAMOUNT { get; set; }

        [XmlElement(ElementName = "TAXCOUNT")]
        public int TAXCOUNT { get; set; }
    }
    
    [XmlRoot(ElementName = "WORKSHIFTTENDER")]
    public class WORKSHIFTTENDER
    {
        [XmlElement(ElementName = "TENDERTYPECODE")]
        public string TENDERTYPECODE { get; set; }

        [XmlElement(ElementName = "TENDERAMOUNT")]
        public string TENDERAMOUNT { get; set; }

        [XmlElement(ElementName = "TENDERCOUNT")]
        public int TENDERCOUNT { get; set; }
    }

    [XmlRoot(ElementName = "WORKSHIFTXML")]
    public class WORKSHIFTXML
    {
        [XmlElement(ElementName = "RETAILSTOREID")]
        public string RETAILSTOREID { get; set; }

        [XmlElement(ElementName = "BUSINESSDAYDATE")]
        public string BUSINESSDAYDATE { get; set; }

        [XmlElement(ElementName = "TRANSACTIONTYPECODE")]
        public string TRANSACTIONTYPECODE { get; set; }

        [XmlElement(ElementName = "WORKSTATIONID")]
        public string WORKSTATIONID { get; set; }

        [XmlElement(ElementName = "TRANSACTIONSEQUENCENUMBER")]
        public string TRANSACTIONSEQUENCENUMBER { get; set; }

        [XmlElement(ElementName = "BEGINDATETIMESTAMP")]
        public string BEGINDATETIMESTAMP { get; set; }

        [XmlElement(ElementName = "ENDDATETIMESTAMP")]
        public string ENDDATETIMESTAMP { get; set; }

        [XmlElement(ElementName = "OPERATORID")]
        public string OPERATORID { get; set; }

        [XmlElement(ElementName = "TRANSACTIONCURRENCY")]
        public string TRANSACTIONCURRENCY { get; set; }

        [XmlElement(ElementName = "PARTNERID")]
        public string PARTNERID { get; set; }

        [XmlElement(ElementName = "ORIGTRANSNUMBER")]
        public string ORIGTRANSNUMBER { get; set; }

        [XmlElement(ElementName = "FLOAT")]
        public string FLOAT { get; set; }
    }

    [XmlRoot(ElementName = "VOIDED")]
    public class VOIDED
    {
        [XmlElement(ElementName = "VOIDEDRETAILSTOREID")]
        public string VOIDEDRETAILSTOREID { get; set; }

        [XmlElement(ElementName = "VOIDEDBUSINESSDAYDATE")]
        public string VOIDEDBUSINESSDAYDATE { get; set; }

        [XmlElement(ElementName = "VOIDEDWORKSTATIONID")]
        public string VOIDEDWORKSTATIONID { get; set; }

        [XmlElement(ElementName = "VOIDEDTRANSACTIONSEQUENCENUMBE")]
        public string VOIDEDTRANSACTIONSEQUENCENUMBE { get; set; }
    }

    [XmlRoot(ElementName = "VOIDEDADDITIONAL")]
    public class VOIDEDADDITIONAL
    {
        [XmlElement(ElementName = "TRANSREASONCODE")]
        public string TRANSREASONCODE { get; set; }
    }

    [XmlRoot(ElementName = "EXTENSION")]
    public class TRANEXTENSION
    {
        [XmlElement(ElementName = "QUESTIONAIRE")]
        public List<string> QUESTIONAIRE { get; set; }

        [XmlElement(ElementName = "INVOICE_NO")]
        public string INVOICE_NO { get; set; }

        [XmlElement(ElementName = "REFINVOICE_NO")]
        public string REFINVOICE_NO { get; set; }

        [XmlElement(ElementName = "ID_CARD")]
        public string ID_CARD { get; set; }

        [XmlElement(ElementName = "EMPLOYEE_ID")]
        public string EMPLOYEE_ID { get; set; }

        [XmlElement(ElementName = "EMPLOYEE_NAME")]
        public string EMPLOYEE_NAME { get; set; }

        [XmlElement(ElementName = "EMPLOYEE_DEP")]
        public string EMPLOYEE_DEP { get; set; }

        [XmlElement(ElementName = "REF_BONNUMBER")]
        public string REF_BONNUMBER { get; set; }

        [XmlElement(ElementName = "SALESMODE")]
        public string SALESMODE { get; set; }

        [XmlElement(ElementName = "SALESCHANNEL")]
        public string SALESCHANNEL { get; set; }

       
    }

    [XmlRoot(ElementName = "EXTENSION")]
    public class TENDEREXTENSION
    {
     
        [XmlElement(ElementName = "ITEMCCAIO")]
        public string ITEMCCAIO { get; set; }

     
    }


    [XmlRoot(ElementName = "EXTENSION")]
    public class ORDEREXTENSION
    {
     
        public string SALESMODE { get; set; }

        [XmlElement(ElementName = "SALESCHANNEL")]
        public string SALESCHANNEL { get; set; }

        [XmlElement(ElementName = "ITEMOPTION")]
        public string ITEMOPTION { get; set; }

        [XmlElement(ElementName = "COMBOPARENTITEM")]
        public string COMBOPARENTITEM { get; set; }

        [XmlElement(ElementName = "VOUCHERINFO")]
        public string VOUCHERINFO { get; set; }

        [XmlElement(ElementName = "WHTDETAIL_1")]
        public string WHTDETAIL_1 { get; set; }

        [XmlElement(ElementName = "WHTDETAIL_2")]
        public string WHTDETAIL_2 { get; set; }

        [XmlElement(ElementName = "WHTDETAIL_3")]
        public string WHTDETAIL_3 { get; set; }

        [XmlElement(ElementName = "VOUCHERREFID")]
        public string VOUCHERREFID { get; set; }

        [XmlElement(ElementName = "ITEMCCAIO")]
        public string ITEMCCAIO { get; set; }

        [XmlElement(ElementName = "SALESORDER")]
        public string SALESORDER { get; set; }

        [XmlElement(ElementName = "SALESORDERITEM")]
        public string SALESORDERITEM { get; set; }

    }

    [XmlRoot(ElementName = "DISCOUNTITEM")]
    public class DISCOUNTITEM
    {
        [XmlElement(ElementName = "DISCOUNTSEQUENCENUMBER")]
        public string DISCOUNTSEQUENCENUMBER { get; set; }

        [XmlElement(ElementName = "DISCOUNTTYPECODE")]
        public string DISCOUNTTYPECODE { get; set; }

        [XmlElement(ElementName = "REDUCTIONAMOUNT")]
        public string REDUCTIONAMOUNT { get; set; }

        [XmlElement(ElementName = "DISCOUNTID")]
        public string DISCOUNTID { get; set; }
    }

    [XmlRoot(ElementName = "TAXITEM")]
    public class TAXITEM
    {
        [XmlElement(ElementName = "TAXSEQUENCENUMBER")]
        public string TAXSEQUENCENUMBER { get; set; }

        [XmlElement(ElementName = "TAXTYPECODE")]
        public string TAXTYPECODE { get; set; }

        [XmlElement(ElementName = "TAXAMOUNT")]
        public string TAXAMOUNT { get; set; }
    }

    [XmlRoot(ElementName = "SALESITEM")]
    public class SALESITEM

    {
        public SALESITEM()
        {
            EXTENSION = new ORDEREXTENSION();
            DISCOUNTITEM = new List<DISCOUNTITEM>();
            TAXITEM = new List<TAXITEM>();
        }

        [XmlElement(ElementName = "RETAILSEQUENCENUMBER")]
        public string RETAILSEQUENCENUMBER { get; set; }

        [XmlElement(ElementName = "RETAILTYPECODE")]
        public string RETAILTYPECODE { get; set; }

        [XmlElement(ElementName = "RETAILREASONCODE")]
        public string RETAILREASONCODE { get; set; }

        [XmlElement(ElementName = "ITEMIDQUALIFIER")]
        public string ITEMIDQUALIFIER { get; set; }

        [XmlElement(ElementName = "ITEMID")] public string ITEMID { get; set; }

        [XmlElement(ElementName = "RETAILQUANTITY")]
        public string RETAILQUANTITY { get; set; }

        [XmlElement(ElementName = "SALESUNITOFMEASURE")]
        public string SALESUNITOFMEASURE { get; set; }

        [XmlElement(ElementName = "SALESAMOUNT")]
        public string SALESAMOUNT { get; set; }

        [XmlElement(ElementName = "NORMALSALESAMOUNT")]
        public string NORMALSALESAMOUNT { get; set; }

        [XmlElement(ElementName = "ACTUALUNITPRICE")]
        public string ACTUALUNITPRICE { get; set; }

        [XmlElement(ElementName = "STORAGELOCATION")]
        public string STORAGELOCATION { get; set; }

        [XmlElement(ElementName = "EXTENSION")]
        public ORDEREXTENSION EXTENSION { get; set; }

        [XmlElement(ElementName = "DISCOUNTITEM")]
        public List<DISCOUNTITEM> DISCOUNTITEM { get; set; }

        [XmlElement(ElementName = "TAXITEM")] public List<TAXITEM> TAXITEM { get; set; }
    }

    [XmlRoot(ElementName = "LOYALTY")]
    public class LOYALTY
    {
        [XmlElement(ElementName = "LOYALTYSEQUENCENUMBER")]
        public string LOYALTYSEQUENCENUMBER { get; set; }

        [XmlElement(ElementName = "CUSTOMERCARDNUMBER")]
        public string CUSTOMERCARDNUMBER { get; set; }
    }

    [XmlRoot(ElementName = "TENDER")]
    public class TENDER
    {
        public TENDER()
        {
            EXTENSION = new TENDEREXTENSION();
        }

        [XmlElement(ElementName = "TENDERSEQUENCENUMBER")]
        public string TENDERSEQUENCENUMBER { get; set; }

        [XmlElement(ElementName = "TENDERTYPECODE")]
        public string TENDERTYPECODE { get; set; }

        [XmlElement(ElementName = "TENDERAMOUNT")]
        public string TENDERAMOUNT { get; set; }

        [XmlElement(ElementName = "TENDERCURRENCY")]
        public string TENDERCURRENCY { get; set; }

        [XmlElement(ElementName = "TENDERID")] public string TENDERID { get; set; }

        [XmlElement(ElementName = "REFERENCEID")]
        public string REFERENCEID { get; set; }

        [XmlElement(ElementName = "EXTENSION")]
        public TENDEREXTENSION EXTENSION { get; set; }
    }

    [XmlRoot(ElementName = "TRANSACTION")]
    public class TRANSACTION
    {
        public TRANSACTION()
        {
            VOIDEDADDITIONAL = new VOIDEDADDITIONAL();
            VOIDED = new VOIDED();
            EXTENSION = new TRANEXTENSION();
            SALESITEM = new List<SALESITEM>();
            LOYALTY = new LOYALTY();
            TENDER = new List<TENDER>();
        }

        [XmlElement(ElementName = "RETAILSTOREID")]
        public string RETAILSTOREID { get; set; }

        [XmlElement(ElementName = "BUSINESSDAYDATE")]
        public string BUSINESSDAYDATE { get; set; }

        [XmlElement(ElementName = "TRANSACTIONTYPECODE")]
        public string TRANSACTIONTYPECODE { get; set; }

        [XmlElement(ElementName = "WORKSTATIONID")]
        public string WORKSTATIONID { get; set; }

        [XmlElement(ElementName = "TRANSACTIONSEQUENCENUMBER")]
        public string TRANSACTIONSEQUENCENUMBER { get; set; }

        [XmlElement(ElementName = "BEGINDATETIMESTAMP")]
        public string BEGINDATETIMESTAMP { get; set; }

        [XmlElement(ElementName = "ENDDATETIMESTAMP")]
        public string ENDDATETIMESTAMP { get; set; }

        [XmlElement(ElementName = "OPERATORID")]
        public string OPERATORID { get; set; }

        [XmlElement(ElementName = "TRANSACTIONCURRENCY")]
        public string TRANSACTIONCURRENCY { get; set; }

        [XmlElement(ElementName = "ORIGTRANSNUMBER")]
        public string ORIGTRANSNUMBER { get; set; }

        [XmlElement(ElementName = "PARTNERID")]
        public string PARTNERID { get; set; }

        [XmlElement(ElementName = "VOIDED")] public VOIDED VOIDED { get; set; }

        [XmlElement(ElementName = "VOIDEDADDITIONAL")]
        public VOIDEDADDITIONAL VOIDEDADDITIONAL { get; set; }

        [XmlElement(ElementName = "EXTENSION")]
        public TRANEXTENSION EXTENSION { get; set; }

        [XmlElement(ElementName = "SALESITEM")]
        public List<SALESITEM> SALESITEM { get; set; }

        [XmlElement(ElementName = "LOYALTY")] public LOYALTY LOYALTY { get; set; }

        [XmlElement(ElementName = "TENDER")] public List<TENDER> TENDER { get; set; }
    }

    [XmlRoot(ElementName="PosTransactionSend_MT", Namespace="http://pttor.com/i_sap_ca_ca/pos/postransaction_send/")]
	public class PosTransactionSend_MT {

        public PosTransactionSend_MT()
        {
            TRANSACTION = new TRANSACTION();
        }
		[XmlElement(ElementName="TRANSACTION")]
		public TRANSACTION TRANSACTION { get; set; }

		[XmlAttribute(AttributeName="ns0", Namespace="http://www.w3.org/2000/xmlns/")]
		public string Ns0 { get; set; }
	}
}