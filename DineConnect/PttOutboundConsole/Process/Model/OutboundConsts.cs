﻿using System;
using System.Collections.Generic;

namespace PttOutboundConsole.Process.Model
{
    public class WorkShiftDto
    {
        public WorkShiftDto()
        {
            PaymentInfos = new List<WorkTimePaymentInformationDto>();
            PaymentDenos = new List<WorkTimePaymentDenomationDto>();
        }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string TerminalName { get; set; }

        public string StartUser { get; set; }
        public string StopUser { get; set; }
        public int TillCount { get; set; }
        public List<WorkTimePaymentInformationDto> PaymentInfos { get; set; }
        public List<WorkTimePaymentDenomationDto> PaymentDenos { get; set; }
        public decimal Float { get; set; }

        public string UniqueId
        {
            get
            {
                return $"{StartDate.Ticks}:{EndDate.Ticks}:{TerminalName}";
            }
            set { }
        }
        private string _infoId;
        public string infoId
        {
            get
            {
                return _infoId != null && _infoId.ToUpper() == "ALL" ? _infoId : StartDate.ToShortDateString() + " - " + EndDate.ToShortDateString();
            }
            set 
            {
                _infoId = value;
            }
        }
    }

    public class WorkTimePaymentDenomationDto
    {
        public string Value { get; set; }
        public int PaymentType { get; set; }
    }

    public class WorkTimePaymentInformationDto
    {
        public int PaymentType { get; set; }
        public decimal Actual { get; set; }
        public decimal Entered { get; set; }
        public decimal Difference => Entered - Actual;
    }


}
