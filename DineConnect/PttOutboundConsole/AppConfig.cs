﻿namespace PttOutboundConsole
{
    public class AppConfig
    {
        public string ProcessingFolder { get; set; }
        public string MinioServerUrl {get;set;}
        public string MinioApiKey {get;set;}
        public string MinioApiSecret {get;set;}
        public string MinioProcessedBucket {get;set;}

        public string CreditSalesDepartments { get; set; }
        public string InternalSalesDepartments { get; set; }

    
        public string FilePattern { get; set; }

        public int TenantId { get; set; }
        public int CompanyId { get; set; }
        public int AppUserId { get; set; }

        public int RunCycle { get; set; }
        public int MaxTicketCount { get; set; }


    }
}