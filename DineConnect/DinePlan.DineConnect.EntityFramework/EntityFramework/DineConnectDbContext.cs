﻿using Abp.Extensions;
using Abp.Zero.EntityFramework;
using DinePlan.DineConnect.Addon;
using DinePlan.DineConnect.Authorization.Roles;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Book;
using DinePlan.DineConnect.Cater.Quotation;
using DinePlan.DineConnect.CaterModel;
using DinePlan.DineConnect.Connect;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Card;
using DinePlan.DineConnect.Connect.Delivery;
using DinePlan.DineConnect.Connect.Device;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Display;
using DinePlan.DineConnect.Connect.Entity;
using DinePlan.DineConnect.Connect.External;
using DinePlan.DineConnect.Connect.Future;
using DinePlan.DineConnect.Connect.Import;
using DinePlan.DineConnect.Connect.Invoice;
using DinePlan.DineConnect.Connect.Language;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Messages;
using DinePlan.DineConnect.Connect.MultiLingual;
using DinePlan.DineConnect.Connect.OrderTags;
using DinePlan.DineConnect.Connect.Period;
using DinePlan.DineConnect.Connect.Reason;
using DinePlan.DineConnect.Connect.Setting;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.Tag;
using DinePlan.DineConnect.Connect.Taxes;
using DinePlan.DineConnect.Connect.Till;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.Users;
using DinePlan.DineConnect.Cluster;
using DinePlan.DineConnect.Engage;
using DinePlan.DineConnect.Engage.Feed;
using DinePlan.DineConnect.Engage.Gift;
using DinePlan.DineConnect.Engage.Reservation;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.Go;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.House.Document;
using DinePlan.DineConnect.House.Temp;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.Play;
using DinePlan.DineConnect.Session;
using DinePlan.DineConnect.Storage;
using DinePlan.DineConnect.Swipe;
using DinePlan.DineConnect.Template;
using EntityFramework.DynamicFilters;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Dynamic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TrackerEnabledDbContext.Common;
using TrackerEnabledDbContext.Common.Configuration;
using TrackerEnabledDbContext.Common.EventArgs;
using TrackerEnabledDbContext.Common.Interfaces;
using TrackerEnabledDbContext.Common.Models;
using Yield = DinePlan.DineConnect.House.Yield;

//using DinePlan.DineConnect.Engage.Master;

namespace DinePlan.DineConnect.EntityFramework
{
    public class DineConnectDbContext : AbpZeroDbContext<Tenant, Role, User>, ITrackerContext
    {
        /* Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         * But it may cause problems when working Migrate.exe of EF. ABP works either way.         *
         */
        public ConnectSession ConnectSession { get; set; }

        private readonly CoreTracker _coreTracker;

        private Func<string> _usernameFactory;
        private string _defaultUsername;
        private Action<dynamic> _metadataConfiguration;

        private bool _trackingEnabled = true;

        public bool TrackingEnabled
        {
            get => GlobalTrackingConfig.Enabled && _trackingEnabled;
            set => _trackingEnabled = value;
        }

        public void ConfigureUsername(Func<string> usernameFactory)
        {
            _usernameFactory = usernameFactory;
        }

        public void ConfigureUsername(string defaultUsername)
        {
            _defaultUsername = defaultUsername;
        }

        public void ConfigureMetadata(Action<dynamic> metadataConfiguration)
        {
            _metadataConfiguration = metadataConfiguration;
        }

        public DineConnectDbContext()
            : base("Default")
        {
            Database.CommandTimeout = 1800;
            _coreTracker = new CoreTracker(this);
            ConfigureUsername(() => User.AdminUserName);
        }

        /* This constructor is used by ABP to pass connection string defined in DineConnectDataModule.PreInitialize.
         * Notice that, actually you will not directly create an instance of DineConnectDbContext since ABP automatically handles it.
         */

        public DineConnectDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            Database.CommandTimeout = 1800;
            _coreTracker = new CoreTracker(this);
            ConfigureUsername(() => User.AdminUserName);
        }

        public event EventHandler<AuditLogGeneratedEventArgs> OnAuditLogGenerated
        {
            add { _coreTracker.OnAuditLogGenerated += value; }
            remove { _coreTracker.OnAuditLogGenerated -= value; }
        }

        public override int SaveChanges()
        {
            try
            {
                ApplyAbpConcepts();
                ApplyLocalAbpConcepts();

                if (TrackingEnabled)
                    return SaveChanges(_usernameFactory?.Invoke() ?? _defaultUsername);

                var audit = new Z.EntityFramework.Plus.Audit();
                audit.PreSaveChanges(this);
                var rowAffecteds = base.SaveChanges();
                audit.PostSaveChanges();

                if (audit.Configuration.AutoSavePreAction != null)
                {
                    audit.Configuration.AutoSavePreAction(this, audit);
                    base.SaveChanges();
                }

                return rowAffecteds;
            }
            catch (DbEntityValidationException ex)
            {
                LogDbEntityValidationException(ex);
                throw;
            }
        }

        protected virtual void ApplyLocalAbpConcepts()
        {
            foreach (var entry in ChangeTracker.Entries().ToList())
            {
                CheckAndSetConnectFilterProperty(entry.Entity);
            }
        }

        private void CheckAndSetConnectFilterProperty(object entityAsObj)
        {
            if (!(entityAsObj is IOrganization))
            {
                return;
            }

            var entity = entityAsObj.As<IOrganization>();
            if (entity.Oid != 0)
            {
                return;
            }
            entity.Oid = ConnectSession.OrgId;
        }

        protected virtual void LogDbEntityValidationException(DbEntityValidationException exception)
        {
            Logger.Error("There are some validation errors while saving changes in EntityFramework:");
            foreach (var ve in exception.EntityValidationErrors.SelectMany(eve => eve.ValidationErrors))
            {
                Logger.Error(" - " + ve.PropertyName + ": " + ve.ErrorMessage);
            }
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            try
            {
                ApplyAbpConcepts(); ;
                ApplyLocalAbpConcepts();

                if (TrackingEnabled)
                    return await SaveChangesAsync(_usernameFactory?.Invoke() ?? _defaultUsername, cancellationToken);

                var audit = new Z.EntityFramework.Plus.Audit();
                audit.PreSaveChanges(this);
                var rowAffected = await base.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
                audit.PostSaveChanges();

                if (audit.Configuration.AutoSavePreAction != null)
                {
                    audit.Configuration.AutoSavePreAction(this, audit);
                    await base.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
                }

                return rowAffected;
            }
            catch (DbEntityValidationException ex)
            {
                LogDbEntityValidationException(ex);
                throw;
            }
        }

        #region -- TrackerContext --

        public virtual IQueryable<TrackerAuditLog> GetLogs(string entityName)
        {
            return _coreTracker.GetLogs(entityName);
        }

        public virtual IQueryable<TrackerAuditLog> GetLogs(string entityName, object primaryKey)
        {
            return _coreTracker.GetLogs(entityName, primaryKey);
        }

        public virtual IQueryable<TrackerAuditLog> GetLogs<TEntity>()
        {
            return _coreTracker.GetLogs<TEntity>();
        }

        public virtual IQueryable<TrackerAuditLog> GetLogs<TEntity>(object primaryKey)
        {
            return _coreTracker.GetLogs<TEntity>(primaryKey);
        }

        public virtual async Task<int> SaveChangesAsync(object userName, CancellationToken cancellationToken)
        {
            if (!TrackingEnabled) return await base.SaveChangesAsync(cancellationToken);

            if (cancellationToken.IsCancellationRequested)
                cancellationToken.ThrowIfCancellationRequested();

            dynamic metadata = new ExpandoObject();
            _metadataConfiguration?.Invoke(metadata);

            _coreTracker.AuditChanges(userName, metadata);

            IEnumerable<DbEntityEntry> addedEntries = _coreTracker.GetAdditions();

            // Call the original SaveChanges(), which will save both the changes made and the audit records...Note that added entry auditing is still remaining.
            int result = await base.SaveChangesAsync(cancellationToken);

            //By now., we have got the primary keys of added entries of added entiries because of the call to savechanges.
            _coreTracker.AuditAdditions(userName, addedEntries, metadata);

            //save changes to audit of added entries
            await base.SaveChangesAsync(cancellationToken);

            return result;
        }

        public virtual async Task<int> SaveChangesAsync(int userId)
        {
            if (!TrackingEnabled) return await base.SaveChangesAsync(CancellationToken.None);

            return await SaveChangesAsync(userId, CancellationToken.None);
        }

        public virtual async Task<int> SaveChangesAsync(string userName)
        {
            if (!TrackingEnabled) return await base.SaveChangesAsync(CancellationToken.None);

            return await SaveChangesAsync(userName, CancellationToken.None);
        }

        public virtual int SaveChanges(object userName)
        {
            if (!TrackingEnabled) return base.SaveChanges();

            dynamic metaData = new ExpandoObject();
            _metadataConfiguration?.Invoke(metaData);

            _coreTracker.AuditChanges(userName, metaData);

            IEnumerable<DbEntityEntry> addedEntries = _coreTracker.GetAdditions();
            // Call the original SaveChanges(), which will save both the changes made and the audit records...Note that added entry auditing is still remaining.
            int result = base.SaveChanges();
            //By now., we have got the primary keys of added entries of added entiries because of the call to savechanges.

            _coreTracker.AuditAdditions(userName, addedEntries, metaData);

            //save changes to audit of added entries
            base.SaveChanges();
            return result;
        }

        #endregion -- TrackerContext --

        /* This constructor is used in tests to pass a fake/mock connection.
         */

        public DineConnectDbContext(DbConnection dbConnection)
            : base(dbConnection, true)
        {
            _coreTracker = new CoreTracker(this);
        }

        public override void Initialize()
        {
             base.Initialize();
            this.SetFilterScopedParameterValue("ConnectFilter", "oid", ConnectSession.OrgId);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Filter("ConnectFilter", (IOrganization entity, int oid) => entity.Oid == oid, 1);

            modelBuilder.Entity<MenuItemPortion>().Property(a => a.Price).HasPrecision(18, 3);
            modelBuilder.Entity<UnitConversion>().Property(a => a.Conversion).HasPrecision(24, 14);
            modelBuilder.Entity<PriceTagDefinition>().Property(a => a.Price).HasPrecision(18, 3);
            modelBuilder.Entity<Material>().Property(a => a.UserSerialNumber).HasPrecision(18, 3);
            modelBuilder.Entity<MaterialLedger>().Property(a => a.OpenBalance).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialLedger>().Property(a => a.Received).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialLedger>().Property(a => a.Issued).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialLedger>().Property(a => a.Sales).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialLedger>().Property(a => a.TransferIn).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialLedger>().Property(a => a.TransferOut).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialLedger>().Property(a => a.Damaged).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialLedger>().Property(a => a.SupplierReturn).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialLedger>().Property(a => a.ExcessReceived).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialLedger>().Property(a => a.Shortage).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialLedger>().Property(a => a.Return).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialLedger>().Property(a => a.ClBalance).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialLocationWiseStock>().Property(a => a.CurrentInHand).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialLocationWiseStock>().Property(a => a.MinimumStock).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialLocationWiseStock>().Property(a => a.MaximumStock).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialLocationWiseStock>().Property(a => a.ReOrderLevel).HasPrecision(24, 14);
            modelBuilder.Entity<CustomerTagMaterialPrice>().Property(a => a.MaterialPrice).HasPrecision(18, 3);
            modelBuilder.Entity<MaterialRecipeTypes>().Property(a => a.PrdBatchQty).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialRecipeTypes>().Property(a => a.FixedCost).HasPrecision(18, 3);
            modelBuilder.Entity<InterTransferDetail>().Property(a => a.RequestQty).HasPrecision(24, 14);
            modelBuilder.Entity<InterTransferDetail>().Property(a => a.IssueQty).HasPrecision(24, 14);
            modelBuilder.Entity<InterTransferDetail>().Property(a => a.Price).HasPrecision(18, 6);
            modelBuilder.Entity<InterTransferDetail>().Property(a => a.IssueValue).HasPrecision(18, 3);
            modelBuilder.Entity<InterTransferReceivedDetail>().Property(a => a.ReturnQty).HasPrecision(24, 14);
            modelBuilder.Entity<InterTransferReceivedDetail>().Property(a => a.AdjustmentQty).HasPrecision(24, 14);
            modelBuilder.Entity<InterTransferReceivedDetail>().Property(a => a.Price).HasPrecision(18, 6);
            modelBuilder.Entity<InterTransferReceivedDetail>().Property(a => a.ReceivedQty).HasPrecision(24, 14);
            modelBuilder.Entity<InterTransferReceivedDetail>().Property(a => a.ReceivedValue).HasPrecision(18, 3);
            modelBuilder.Entity<InterTransfer>().Property(a => a.TransferValue).HasPrecision(18, 3);
            modelBuilder.Entity<InterTransfer>().Property(a => a.ReceivedValue).HasPrecision(18, 3);
            modelBuilder.Entity<InvoiceDetail>().Property(a => a.TotalQty).HasPrecision(24, 14);
            modelBuilder.Entity<InvoiceDetail>().Property(a => a.Price).HasPrecision(18, 6);
            modelBuilder.Entity<InvoiceDetail>().Property(a => a.TotalAmount).HasPrecision(18, 6);
            modelBuilder.Entity<InvoiceDetail>().Property(a => a.DiscountPercentage).HasPrecision(18, 3);
            modelBuilder.Entity<InvoiceDetail>().Property(a => a.DiscountAmount).HasPrecision(18, 6);
            modelBuilder.Entity<InvoiceDetail>().Property(a => a.TaxAmount).HasPrecision(18, 6);
            modelBuilder.Entity<InvoiceDetail>().Property(a => a.NetAmount).HasPrecision(18, 6);
            modelBuilder.Entity<Invoice>().Property(a => a.TotalDiscountAmount).HasPrecision(18, 6);
            modelBuilder.Entity<Invoice>().Property(a => a.TotalShipmentCharges).HasPrecision(18, 6);
            modelBuilder.Entity<Invoice>().Property(a => a.RoundedAmount).HasPrecision(18, 3);
            modelBuilder.Entity<Invoice>().Property(a => a.InvoiceAmount).HasPrecision(18, 3);
            modelBuilder.Entity<PurchaseOrder>().Property(a => a.PoNetAmount).HasPrecision(18, 3);
            modelBuilder.Entity<SupplierMaterial>().Property(a => a.MaterialPrice).HasPrecision(18, 6);
            modelBuilder.Entity<InvoiceTaxDetail>().Property(a => a.TaxRate).HasPrecision(18, 3);
            modelBuilder.Entity<InvoiceTaxDetail>().Property(a => a.TaxValue).HasPrecision(18, 3);
            modelBuilder.Entity<Tax>().Property(a => a.Percentage).HasPrecision(18, 3);
            modelBuilder.Entity<InwardDirectCreditDetail>().Property(a => a.DcReceivedQty).HasPrecision(24, 14);
            modelBuilder.Entity<InwardDirectCreditDetail>().Property(a => a.DcConvertedQty).HasPrecision(24, 14);
            modelBuilder.Entity<IssueDetail>().Property(a => a.RequestQty).HasPrecision(24, 14);
            modelBuilder.Entity<IssueDetail>().Property(a => a.IssueQty).HasPrecision(24, 14);
            modelBuilder.Entity<Recipe>().Property(a => a.PrdBatchQty).HasPrecision(24, 14);
            modelBuilder.Entity<Recipe>().Property(a => a.FixedCost).HasPrecision(18, 3);
            modelBuilder.Entity<LocationCategoryShare>().Property(a => a.SharingPercentage).HasPrecision(18, 3);
            modelBuilder.Entity<GiftVoucher>().Property(a => a.ClaimedAmount).HasPrecision(18, 3);
            modelBuilder.Entity<GiftVoucherType>().Property(a => a.VoucherValue).HasPrecision(18, 3);
            modelBuilder.Entity<MaterialIngredient>().Property(a => a.MaterialUsedQty).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialIngredient>().Property(a => a.UserSerialNumber).HasPrecision(18, 3);
            modelBuilder.Entity<YieldInput>().Property(a => a.InputQty).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialMenuMapping>().Property(a => a.UserSerialNumber).HasPrecision(18, 3);
            modelBuilder.Entity<MaterialMenuMapping>().Property(a => a.PortionQty).HasPrecision(24, 14);
            modelBuilder.Entity<MaterialMenuMapping>().Property(a => a.WastageExpected).HasPrecision(24, 14);
            modelBuilder.Entity<Yield>().Property(a => a.RecipeProductionQty).HasPrecision(24, 14);
            modelBuilder.Entity<YieldOutput>().Property(a => a.OutputQty).HasPrecision(24, 14);
            modelBuilder.Entity<YieldOutput>().Property(a => a.YieldPrice).HasPrecision(18, 3);
            modelBuilder.Entity<ProductRecipesLink>().Property(a => a.UserSerialNumber).HasPrecision(18, 3);
            modelBuilder.Entity<ProductRecipesLink>().Property(a => a.PortionQty).HasPrecision(24, 14);
            modelBuilder.Entity<ProductRecipesLink>().Property(a => a.WastageExpected).HasPrecision(18, 6);
            modelBuilder.Entity<PurchaseOrderDetail>().Property(a => a.QtyOrdered).HasPrecision(24, 14);
            modelBuilder.Entity<PurchaseOrderDetail>().Property(a => a.QtyReceived).HasPrecision(24, 14);
            modelBuilder.Entity<PurchaseOrderDetail>().Property(a => a.Price).HasPrecision(18, 6);
            modelBuilder.Entity<PurchaseOrderDetail>().Property(a => a.TotalAmount).HasPrecision(18, 6);
            modelBuilder.Entity<PurchaseOrderDetail>().Property(a => a.DiscountValue).HasPrecision(18, 3);
            modelBuilder.Entity<PurchaseOrderDetail>().Property(a => a.TaxAmount).HasPrecision(18, 6);
            modelBuilder.Entity<PurchaseOrderDetail>().Property(a => a.NetAmount).HasPrecision(18, 6);
            modelBuilder.Entity<PurchaseOrderTaxDetail>().Property(a => a.TaxRate).HasPrecision(18, 3);
            modelBuilder.Entity<PurchaseOrderTaxDetail>().Property(a => a.TaxValue).HasPrecision(18, 6);
            modelBuilder.Entity<RecipeIngredient>().Property(a => a.MaterialUsedQty).HasPrecision(24, 14);
            modelBuilder.Entity<RecipeIngredient>().Property(a => a.UserSerialNumber).HasPrecision(18, 3);
            modelBuilder.Entity<MemberPoint>().Property(a => a.TicketTotal).HasPrecision(18, 3);
            modelBuilder.Entity<MemberPoint>().Property(a => a.PointsAccumulation).HasPrecision(18, 3);
            modelBuilder.Entity<MemberPoint>().Property(a => a.PointsAccumulationFactor).HasPrecision(18, 3);
            modelBuilder.Entity<MemberPoint>().Property(a => a.PointsRedemption).HasPrecision(18, 3);
            modelBuilder.Entity<MemberPoint>().Property(a => a.PointsRedemptionFactor).HasPrecision(18, 3);
            modelBuilder.Entity<MemberPoint>().Property(a => a.TotalPoints).HasPrecision(18, 3);
            modelBuilder.Entity<ReturnDetail>().Property(a => a.ReturnQty).HasPrecision(24, 14);
            modelBuilder.Entity<ConnectMember>().Property(a => a.TotalPoints).HasPrecision(18, 3);
            modelBuilder.Entity<ProductionDetail>().Property(a => a.ProductionQty).HasPrecision(24, 14);
            modelBuilder.Entity<ProductionDetail>().Property(a => a.Price).HasPrecision(24, 14);
            modelBuilder.Entity<LocationMenuItemPrice>().Property(a => a.Price).HasPrecision(18, 3);
            modelBuilder.Entity<Location>().Property(a => a.SharingPercentage).HasPrecision(18, 3);
            modelBuilder.Entity<Order>().Property(a => a.Price).HasPrecision(18, 3);
            modelBuilder.Entity<Order>().Property(a => a.CostPrice).HasPrecision(18, 3);
            modelBuilder.Entity<Order>().Property(a => a.Quantity).HasPrecision(18, 3);
            modelBuilder.Entity<Order>().Property(a => a.PromotionAmount).HasPrecision(18, 3);
            modelBuilder.Entity<Ticket>().Property(a => a.RemainingAmount).HasPrecision(18, 3);
            modelBuilder.Entity<Ticket>().Property(a => a.TotalAmount).HasPrecision(18, 3);
            modelBuilder.Entity<Payment>().Property(a => a.TenderedAmount).HasPrecision(18, 3);
            modelBuilder.Entity<Payment>().Property(a => a.Amount).HasPrecision(18, 3);
            modelBuilder.Entity<TicketTransaction>().Property(a => a.Amount).HasPrecision(18, 3);
            modelBuilder.Entity<DinePlanTaxLocation>().Property(a => a.TaxPercentage).HasPrecision(18, 3);
            modelBuilder.Entity<PlanLogger>().Property(a => a.TicketTotal).HasPrecision(18, 3);
            modelBuilder.Entity<RequestDetail>().Property(a => a.RequestQty).HasPrecision(24, 14);
            modelBuilder.Entity<AdjustmentDetail>().Property(a => a.AdjustmentQty).HasPrecision(24, 14);
            modelBuilder.Entity<AdjustmentDetail>().Property(a => a.ClosingStock).HasPrecision(24, 14);
            modelBuilder.Entity<AdjustmentDetail>().Property(a => a.StockEntry).HasPrecision(24, 14);
            modelBuilder.Entity<CustomerMaterial>().Property(a => a.MaterialPrice).HasPrecision(18, 3);
            modelBuilder.Entity<PurchaseReturnDetail>().Property(a => a.Quantity).HasPrecision(24, 14);
            modelBuilder.Entity<TransactionOrderTag>().Property(a => a.Quantity).HasPrecision(18, 3);
            modelBuilder.Entity<TransactionOrderTag>().Property(a => a.Price).HasPrecision(18, 3);

            modelBuilder.Entity<SalesInvoice>().Property(a => a.TotalDiscountAmount).HasPrecision(18, 3);
            modelBuilder.Entity<SalesInvoice>().Property(a => a.TotalShipmentCharges).HasPrecision(18, 3);
            modelBuilder.Entity<SalesInvoice>().Property(a => a.RoundedAmount).HasPrecision(18, 3);
            modelBuilder.Entity<SalesInvoice>().Property(a => a.InvoiceAmount).HasPrecision(18, 3);
            modelBuilder.Entity<SalesInvoiceDetail>().Property(a => a.TotalQty).HasPrecision(24, 14);
            modelBuilder.Entity<SalesInvoiceDetail>().Property(a => a.Price).HasPrecision(18, 6);
            modelBuilder.Entity<SalesInvoiceDetail>().Property(a => a.TotalAmount).HasPrecision(18, 3);
            modelBuilder.Entity<SalesInvoiceDetail>().Property(a => a.DiscountPercentage).HasPrecision(18, 3);
            modelBuilder.Entity<SalesInvoiceDetail>().Property(a => a.DiscountAmount).HasPrecision(18, 3);
            modelBuilder.Entity<SalesInvoiceDetail>().Property(a => a.SalesTaxAmount).HasPrecision(18, 3);
            modelBuilder.Entity<SalesInvoiceDetail>().Property(a => a.NetAmount).HasPrecision(18, 3);
            modelBuilder.Entity<SalesInvoiceTaxDetail>().Property(a => a.SalesTaxValue).HasPrecision(18, 3);
            modelBuilder.Entity<SalesInvoiceTaxDetail>().Property(a => a.SalesTaxRate).HasPrecision(18, 3);

            modelBuilder.Entity<SalesOrder>().Property(a => a.SoNetAmount).HasPrecision(18, 3);
            modelBuilder.Entity<SalesOrderDetail>().Property(a => a.QtyOrdered).HasPrecision(24, 14);
            modelBuilder.Entity<SalesOrderDetail>().Property(a => a.QtyReceived).HasPrecision(24, 14);
            modelBuilder.Entity<SalesOrderDetail>().Property(a => a.Price).HasPrecision(18, 6);
            modelBuilder.Entity<SalesOrderDetail>().Property(a => a.TotalAmount).HasPrecision(18, 3);
            modelBuilder.Entity<SalesOrderDetail>().Property(a => a.DiscountValue).HasPrecision(18, 3);
            modelBuilder.Entity<SalesOrderDetail>().Property(a => a.TaxAmount).HasPrecision(18, 3);
            modelBuilder.Entity<SalesOrderDetail>().Property(a => a.NetAmount).HasPrecision(18, 3);
            modelBuilder.Entity<SalesOrderTaxDetail>().Property(a => a.SalesTaxRate).HasPrecision(18, 3);
            modelBuilder.Entity<SalesOrderTaxDetail>().Property(a => a.SalesTaxValue).HasPrecision(18, 3);

            modelBuilder.Entity<SalesDeliveryOrderDetail>().Property(a => a.BillConvertedQty).HasPrecision(24, 14);
            modelBuilder.Entity<SalesDeliveryOrderDetail>().Property(a => a.DeliveryQty).HasPrecision(24, 14);
            modelBuilder.Entity<ClosingStockDetail>().Property(a => a.OnHand).HasPrecision(24, 14);

            modelBuilder.Entity<SupplierMaterial>().Property(t => t.MinimumOrderQuantity).HasPrecision(24, 14);
            modelBuilder.Entity<SupplierMaterial>().Property(t => t.MaximumOrderQuantity).HasPrecision(24, 14);
            modelBuilder.Entity<MenuItemWastageDetail>().Property(t => t.WastageQty).HasPrecision(24, 14);

            modelBuilder.Entity<IssueRecipeDetail>().Property(t => t.RecipeProductionQty).HasPrecision(24, 14);
            modelBuilder.Entity<IssueRecipeDetail>().Property(t => t.CompletedQty).HasPrecision(24, 14);

            modelBuilder.Entity<MaterialRecipeTypes>().Property(t => t.PrdBatchQty).HasPrecision(24, 14);

            modelBuilder.Entity<CaterQuotationHeader>().HasRequired(s => s.EventAddress).WithMany().WillCascadeOnDelete(false);

            modelBuilder.Entity<AdjustmentDetail>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<ClosingStockDetail>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<ClosingStockUnitWiseDetail>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<InterTransferDetail>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<InterTransferReceivedDetail>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<InvoiceDetail>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<InwardDirectCreditDetail>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<IssueRecipeDetail>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<ProductionDetail>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<PurchaseOrderDetail>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<PurchaseReturnDetail>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<RequestDetail>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<RequestRecipeDetail>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<ReturnDetail>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<SalesDeliveryOrderDetail>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<SalesOrderDetail>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<SalesInvoiceDetail>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<YieldInput>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<YieldOutput>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);
            modelBuilder.Entity<IssueDetail>().Property(t => t.ConversionWithDefaultUnit).HasPrecision(24, 14);

            modelBuilder.Entity<CaterQuoteRequiredMaterial>().HasRequired(s => s.CaterQuotationHeader).WithMany().WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductCombo>()
                .HasMany<ProductComboGroup>(s => s.ComboGroups)
                .WithMany(c => c.ProductCombos)
                .Map(cs =>
                {
                    cs.MapLeftKey("ProductComboId");
                    cs.MapRightKey("ProductComboGroupId");
                    cs.ToTable("ProductComboGroupDetails");
                });

            modelBuilder.Entity<DineGoDepartment>()
              .HasMany<DineGoCharge>(s => s.DineGoCharges)
              .WithMany(t => t.DineGoDepartments)
              .Map(cs =>
              {
                  cs.MapLeftKey("DineGoDepartmentId");
                  cs.MapRightKey("DineGoChargeId");
                  cs.ToTable("DineGoDepartmentChargeDetails");
              });

            modelBuilder.Entity<DineGoBrand>()
              .HasMany<DineGoDepartment>(s => s.DineGoDepartments)
              .WithMany(t => t.DineGoBrands)
              .Map(cs =>
              {
                  cs.MapLeftKey("DineGoBrandId");
                  cs.MapRightKey("DineGoDepartmentId");
                  cs.ToTable("DineGoBrandDepartmentDetails");
              });
            modelBuilder.Entity<DineGoDevice>()
             .HasMany<DineGoBrand>(s => s.DineGoBrands)
             .WithMany(t => t.DineGoDevices)
             .Map(cs =>
             {
                 cs.MapLeftKey("DineGoDeviceId");
                 cs.MapRightKey("DineGoBrandId");
                 cs.ToTable("DineGoDeviceBrandDetails");
             });
            modelBuilder.Entity<DineGoDevice>()
            .HasMany<DineGoPaymentType>(s => s.DineGoPaymentTypes)
            .WithMany(t => t.DineGoDevices)
            .Map(cs =>
            {
                cs.MapLeftKey("DineGoDeviceId");
                cs.MapRightKey("DineGoPaymentTypeId");
                cs.ToTable("DineGoDevicePaymentTypeDetails");
            });

            modelBuilder.Entity<Display>()
                .HasMany<DisplayGroup>(d => d.DisplayGroups)
                .WithMany(g => g.Displays)
                .Map(cs =>
                {
                    cs.MapLeftKey("DisplayId");
                    cs.MapRightKey("DisplayGroupId");
                    cs.ToTable("PlayDisplayGroupDisplays");
                });
        }

        #region Connect

        public virtual IDbSet<CashAudit> CashAudits { get; set; }

        public virtual IDbSet<DineDevice> DineDevices { get; set; }
        public virtual IDbSet<Alert> Alerts { get; set; }
        public virtual IDbSet<DvrDetail> DvrDetails { get; set; }
        public virtual IDbSet<PostData> PostDatas { get; set; }
        public virtual IDbSet<DinePlanLanguageText> DinePlanLanguageTexts { get; set; }
        public virtual IDbSet<ExternalDeliveryTicket> ExternalDeliveryTickets { get; set; }
        public virtual IDbSet<ExternalFile> ExternalFiles { get; set; }

        public virtual IDbSet<PlanReason> PlanReasons { get; set; }
        public virtual IDbSet<Category> Categories { get; set; }
        public virtual IDbSet<PaymentType> PaymentTypes { get; set; }
        public virtual IDbSet<TransactionType> TransactionTypes { get; set; }

        public virtual IDbSet<ForeignCurrency> ForeignCurrencies { get; set; }
        public virtual IDbSet<Location> Locations { get; set; }
        public virtual IDbSet<LocationBranch> LocationBranches { get; set; }
        public virtual IDbSet<LocationTag> LocationTags { get; set; }
        public virtual IDbSet<LocationGroup> LocationGroups { get; set; }
        public virtual IDbSet<LocationSchdule> LocationSchdules { get; set; }

        public virtual IDbSet<ConnectTableGroup> ConnectTableGroups { get; set; }
        public virtual IDbSet<ConnectTable> ConnectTables { get; set; }

        public virtual IDbSet<MenuItem> MenuItems { get; set; }
        public virtual IDbSet<MenuItemPortion> MenuItemPortions { get; set; }
        public virtual IDbSet<LanguageDescription> LanguageDescriptions { get; set; }

        public virtual IDbSet<MenuBarCode> MenuBarCodes { get; set; }
        public virtual IDbSet<UpMenuItem> UpMenuItems { get; set; }
        public virtual IDbSet<UpMenuItemLocationPrice> UpMenuItemLocationPrices { get; set; }
        public virtual IDbSet<MenuItemSchedule> Schedules { get; set; }
        public virtual IDbSet<LocationMenuItemPrice> LocationMenuItemPrices { get; set; }
        public virtual IDbSet<LocationMenuItem> LocationMenuItems { get; set; }
        public virtual IDbSet<MenuItemDescription> MenuItemDescriptions { get; set; }

        public virtual IDbSet<Ticket> Tickets { get; set; }
        public virtual IDbSet<Order> Orders { get; set; }
        public virtual IDbSet<Payment> Payments { get; set; }
        public virtual IDbSet<TicketTransaction> TicketTransaction { get; set; }

        public virtual IDbSet<TicketTagGroup> TicketTagGroups { get; set; }
        public virtual IDbSet<TicketTag> TicketTag { get; set; }
        public virtual IDbSet<Calculation> Calculations { get; set; }
        public virtual IDbSet<TillAccount> TillAccounts { get; set; }
        public virtual IDbSet<TillTransaction> TillTransactions { get; set; }

        public virtual IDbSet<EntityType> EntityTypes { get; set; }
        public virtual IDbSet<EntityCustomField> EntityCustomFields { get; set; }
        public virtual IDbSet<ConnectEntity> Entities { get; set; }
        public virtual IDbSet<DinePlanUser> DinePlanUsers { get; set; }
        public virtual IDbSet<DinePlanUserRole> DinePlanUserRoles { get; set; }
        public virtual IDbSet<DinePlanPermission> DinePlanPermission { get; set; }
        public virtual IDbSet<Syncer> Syncers { get; set; }
        public virtual IDbSet<LocationSyncer> LocationSyncers { get; set; }

        public virtual IDbSet<LastSync> LastSyncs { get; set; }
        public virtual IDbSet<ReportBackground> ReportBackgrounds { get; set; }

        public virtual IDbSet<DinePlanTax> DinePlanTaxes { get; set; }
        public virtual IDbSet<DinePlanTaxLocation> DinePlanTaxLocations { get; set; }
        public virtual IDbSet<DinePlanTaxMapping> DinePlanTaxMappings { get; set; }
        public virtual IDbSet<PlanLogger> PlanLoggers { get; set; }
        public virtual IDbSet<PriceTag> PriceTags { get; set; }
        public virtual IDbSet<PriceTagDefinition> PriceTagDefinitions { get; set; }
        public virtual IDbSet<PriceTagLocationPrice> PriceTagLocationPrices { get; set; }
        public virtual IDbSet<LocationCategoryShare> LocationCategoryShares { get; set; }
        public virtual IDbSet<WorkPeriod> WorkPeriods { get; set; }
        public virtual IDbSet<OrderTagGroup> OrderTagGroups { get; set; }
        public virtual IDbSet<OrderTagLocationPrice> OrderTagLocationPrices { get; set; }
        public virtual IDbSet<OrderTag> OrderTags { get; set; }
        public virtual IDbSet<OrderTagMap> OrderTagMaps { get; set; }
        public virtual IDbSet<Department> Departments { get; set; }
        public virtual IDbSet<DepartmentGroup> DepartmentGroups { get; set; }
        public virtual IDbSet<EmailTemplate> EmailTemplates { get; set; }
        public virtual IDbSet<FreeItemPromotionExecution> FreeItemPromotionExecutions { get; set; }
        public virtual IDbSet<ScreenMenu> ScreenMenus { get; set; }
        public virtual IDbSet<ScreenMenuCategory> ScreenCategories { get; set; }
        public virtual IDbSet<ScreenMenuItem> ScreenMenuItems { get; set; }
        public virtual IDbSet<TransactionOrderTag> TransactionOrderTags { get; set; }
        public virtual IDbSet<ProductCombo> ProductComboes { get; set; }
        public virtual IDbSet<ProductComboGroup> ProductComboGroups { get; set; }
        public virtual IDbSet<ProductComboItem> ProductComboItems { get; set; }
        public virtual IDbSet<TickMessage> TickMessages { get; set; }
        public virtual IDbSet<TickMessageReply> TickMessageReplies { get; set; }
        public virtual IDbSet<Promotion> Promotions { get; set; }
        public virtual IDbSet<PromotionCategory> PromotionCategorys { get; set; }
        public virtual IDbSet<PromotionSchedule> PromotionSchedules { get; set; }
        public virtual IDbSet<PromotionRestrictItem> PromotionRestrictItems { get; set; }

        public virtual IDbSet<TimerPromotion> TimerPromotions { get; set; }
        public virtual IDbSet<FixedPromotion> FixedPromotions { get; set; }
        public virtual IDbSet<FreePromotion> FreePromotions { get; set; }
        public virtual IDbSet<FreePromotionExecution> FreePromotionExecutions { get; set; }
        public virtual IDbSet<FreeValuePromotion> FreeValuePromotions { get; set; }
        public virtual IDbSet<FreeValuePromotionExecution> FreeValuePromotionExecutions { get; set; }
        public virtual IDbSet<DemandDiscountPromotion> DemandPromotions { get; set; }
        public virtual IDbSet<DemandPromotionExecution> DemandPromotionExecutions { get; set; }
        public virtual IDbSet<FreeItemPromotion> FreeItemPromotions { get; set; }
        public virtual IDbSet<TicketDiscountPromotion> TicketDiscountPromotions { get; set; }
        public virtual IDbSet<PromotionQuota> PromotionQuotas { get; set; }

        public virtual IDbSet<ConnectCardType> ConnectCardTypes { get; set; }
        public virtual IDbSet<ConnectCard> ConnectCards { get; set; }
        public virtual IDbSet<ConnectCardTypeCategory> ConnectCardTypeCategories { get; set; }
        public virtual IDbSet<ConnectCardRedemption> ConnectCardRedemptions { get; set; }
        public virtual IDbSet<ImportCardDataDetail> ImportCardDataDetails { get; set; }
        public virtual IDbSet<FutureDateInformation> FutureDateInformations { get; set; }

        public virtual IDbSet<FullTaxMember> FullTaxMembers { get; set; }
        public virtual IDbSet<FullTaxAddress> FullTaxAddresses { get; set; }
        public virtual IDbSet<FullTaxInvoice> FullTaxInvoices { get; set; }
        public virtual IDbSet<FullTaxInvoiceCopyCount> FullTaxInvoiceCopyCounts { get; set; }

        public virtual IDbSet<ProductGroup> ProductGroups { get; set; }
        public virtual IDbSet<Terminal> Terminals { get; set; }
        public virtual IDbSet<ImportSetting> ImportSettings { get; set; }
        public virtual IDbSet<ProgramSettingTemplate> ProgramSettingTemplates { get; set; }
        public virtual IDbSet<ProgramSettingValue> ProgramSettingValues { get; set; }
        public virtual IDbSet<PrintConfiguration> PrinterConfigurations { get; set; }
        public virtual IDbSet<PrintJob> PrintJobs { get; set; }
        public virtual IDbSet<Printer> Printers { get; set; }
        public virtual IDbSet<PrinterMap> PrinterMaps { get; set; }
        public virtual IDbSet<PrintTemplate> PrintTemplates { get; set; }
        public virtual IDbSet<PrintTemplateCondition> PrintTemplateConditions { get; set; }

        public virtual IDbSet<TicketTypeConfiguration> TicketTypeConfigurations { get; set; }
        public virtual IDbSet<TicketType> TicketTypes { get; set; }
        public virtual IDbSet<Numerator> Numerators { get; set; }
        public virtual IDbSet<UserPasswordArchive> UserPasswordArchives { get; set; }
        public virtual IDbSet<ExternalLog> ExternalLogs { get; set; }

        #endregion Connect

        #region House

        public virtual IDbSet<Brand> Brands { get; set; }
        public virtual IDbSet<Supplier> Suppliers { get; set; }
        public virtual IDbSet<SupplierGSTInformation> SupplierGSTInformations { get; set; }
        public virtual IDbSet<Unit> Units { get; set; }
        public virtual IDbSet<UnitConversion> UnitConversions { get; set; }
        public virtual IDbSet<UnitConversionVsSupplier> UnitConversionVsSuppliers { get; set; }
        public virtual IDbSet<RecipeGroup> RecipeGroups { get; set; }
        public virtual IDbSet<Recipe> Recipes { get; set; }
        public virtual IDbSet<RecipeIngredient> RecipeIngredients { get; set; }
        public virtual IDbSet<MaterialGroup> MaterialGroups { get; set; }
        public virtual IDbSet<MaterialGroupCategory> MaterialGroupCategories { get; set; }
        public virtual IDbSet<Material> Materials { get; set; }
        public virtual IDbSet<MaterialStockCycleLink> MaterialStockCycleLinks { get; set; }
        public virtual IDbSet<InventoryCycleTag> InventoryCycleTags { get; set; }
        public virtual IDbSet<InventoryCycleLinkWithLocation> InventoryCycleLinkWithLocations { get; set; }
        public virtual IDbSet<MaterialBarCode> MaterialBarCodes { get; set; }
        public virtual IDbSet<LocationContact> LocationContacts { get; set; }
        public virtual IDbSet<MaterialUnitsLink> MaterialUnitsLinks { get; set; }
        public virtual IDbSet<ManualReason> ManualReasons { get; set; }
        public virtual IDbSet<SupplierContact> SupplierContacts { get; set; }
        public virtual IDbSet<Tax> Taxes { get; set; }
        public virtual IDbSet<MaterialLocationWiseStock> MaterialLocationWiseStocks { get; set; }
        public virtual IDbSet<SupplierMaterial> SupplierMaterials { get; set; }
        public virtual IDbSet<MaterialBrandsLink> MaterialBrandsLinks { get; set; }
        public virtual IDbSet<MaterialLedger> MaterialLedgers { get; set; }
        public virtual IDbSet<LocationWiseMaterialRateView> LocationWiseMaterialRateViews { get; set; }
        public virtual IDbSet<ProductRecipesLink> ProductRecipesLinks { get; set; }
        public virtual IDbSet<InwardDirectCredit> InwardDirectCredits { get; set; }
        public virtual IDbSet<InwardDirectCreditDetail> InwardDirectCreditDetails { get; set; }
        public virtual IDbSet<InterTransfer> InterTransfers { get; set; }
        public virtual IDbSet<InterTransferDetail> InterTransferDetails { get; set; }
        public virtual IDbSet<InterTransferReceivedDetail> InterTransferReceivedDetails { get; set; }
        public virtual IDbSet<Issue> Issues { get; set; }
        public virtual IDbSet<IssueRecipeDetail> IssueRecipeDetails { get; set; }
        public virtual IDbSet<IssueDetail> IssueDetails { get; set; }
        public virtual IDbSet<Return> Returns { get; set; }
        public virtual IDbSet<ReturnDetail> ReturnDetails { get; set; }
        public virtual IDbSet<Adjustment> Adjustments { get; set; }
        public virtual IDbSet<AdjustmentDetail> AdjustmentDetails { get; set; }
        public virtual IDbSet<Invoice> Invoices { get; set; }
        public virtual IDbSet<InvoiceDetail> InvoiceDetails { get; set; }
        public virtual IDbSet<InvoiceTaxDetail> InvoiceTaxDetails { get; set; }
        public virtual IDbSet<InvoiceDirectCreditLink> InvoiceDirectCreditLinks { get; set; }
        public virtual IDbSet<TempInwardDirectCreditDetail> TempInwardDirectCreditDetails { get; set; }
        public virtual IDbSet<UserDefaultOrganization> UserDefaultOrganizations { get; set; }
        public virtual IDbSet<HouseReport> HouseReports { get; set; }
        public virtual IDbSet<PurchaseOrder> PurchaseOrders { get; set; }
        public virtual IDbSet<PurchaseOrderDetail> PurchaseOrderDetails { get; set; }
        public virtual IDbSet<PurchaseOrderTaxDetail> PurchaseOrderTaxDetails { get; set; }
        public virtual IDbSet<DinePlan.DineConnect.House.Template> Templates { get; set; }
        public virtual IDbSet<Customer> Customers { get; set; }
        public virtual IDbSet<CustomerMaterial> CustomerMaterials { get; set; }
        public virtual IDbSet<Company> Companies { get; set; }
        public virtual IDbSet<CustomerTagDefinition> CustomerTagDefinitions { get; set; }
        public virtual IDbSet<CustomerTagMaterialPrice> CustomerTagMaterialPrices { get; set; }
        public virtual IDbSet<MaterialIngredient> MaterialIngredients { get; set; }
        public virtual IDbSet<MaterialMenuMapping> MaterialMenuMappings { get; set; }
        public virtual IDbSet<MenuMappingDepartment> MenuMappingDepartment { get; set; }
        public virtual IDbSet<MaterialRecipeTypes> MaterialRecipeTypes { get; set; }
        public virtual IDbSet<TaxTemplateMapping> TaxTemplateMappings { get; set; }
        public virtual IDbSet<Yield> Yields { get; set; }
        public virtual IDbSet<YieldInput> YieldInputs { get; set; }
        public virtual IDbSet<YieldOutput> YieldOutputs { get; set; }
        public virtual IDbSet<Production> Productins { get; set; }
        public virtual IDbSet<ProductionDetail> ProductionDetails { get; set; }
        public virtual IDbSet<Request> Requests { get; set; }
        public virtual IDbSet<RequestRecipeDetail> RequestRecipeDetails { get; set; }
        public virtual IDbSet<RequestDetail> RequestDetails { get; set; }
        public virtual IDbSet<PurchaseReturn> PurchaseReturns { get; set; }
        public virtual IDbSet<PurchaseReturnDetail> PurchaseReturnDetails { get; set; }
        public virtual IDbSet<PurchaseReturnTaxDetail> PurchaseReturnTaxDetails { get; set; }
        public virtual IDbSet<SupplierDocument> SupplierDocuments { get; set; }
        public virtual IDbSet<ProductionUnit> ProductionUnits { get; set; }
        public virtual IDbSet<UploadDocument> UploadDocuments { get; set; }
        public virtual IDbSet<UploadDocumentComment> UploadDocumentComments { get; set; }
        public virtual IDbSet<PurchaseCategory> PurchaseCatories { get; set; }
        public virtual IDbSet<SalesInvoice> SalesInvoices { get; set; }
        public virtual IDbSet<SalesInvoiceDetail> SalesInvoiceDetails { get; set; }
        public virtual IDbSet<SalesInvoiceTaxDetail> SalesInvoiceTaxDetails { get; set; }
        public virtual IDbSet<SalesInvoiceDeliveryOrderLink> SalesInvoiceDeliveryOrderLinks { get; set; }
        public virtual IDbSet<SalesDeliveryOrder> SalesDeliveryOrders { get; set; }
        public virtual IDbSet<SalesDeliveryOrderDetail> SalesDeliveryOrderDetails { get; set; }
        public virtual IDbSet<SalesTax> SalesTaxes { get; set; }
        public virtual IDbSet<SalesTaxTemplateMapping> SalesTaxTemplateMappings { get; set; }
        public virtual IDbSet<SalesOrder> SalesOrders { get; set; }
        public virtual IDbSet<SalesOrderDetail> SalesOrderDetails { get; set; }
        public virtual IDbSet<SalesOrderTaxDetail> SalesOrderTaxDetails { get; set; }
        public virtual IDbSet<ClosingStock> ClosingStocks { get; set; }
        public virtual IDbSet<ClosingStockDetail> ClosingStockDetails { get; set; }
        public virtual IDbSet<ClosingStockUnitWiseDetail> ClosingStockUnitWiseDetails { get; set; }
        public virtual IDbSet<MenuItemWastage> MenuItemWastages { get; set; }
        public virtual IDbSet<MenuItemWastageDetail> MenuItemWastageDetails { get; set; }
        public virtual IDbSet<DayCloseTemplate> DayCloseTemplates { get; set; }
        public virtual IDbSet<HouseNumber> HouseNumbers { get; set; }

        #endregion House
        #region Cluster
        public virtual IDbSet<DelAggLocationGroup> DelAggLocationGroups { get; set; }
        public virtual IDbSet<DelAggLocation> DelAggLocations { get; set; }
        public virtual IDbSet<DelAggLocMapping> DelAggLocMappings { get; set; }
        public virtual IDbSet<DelAggLocationItem> DelAggLocationItems { get; set; }
        public virtual IDbSet<DelAggItemGroup> DelAggItemGroups { get; set; }
        public virtual IDbSet<DelAggItem> DelAggItems { get; set; }
        public virtual IDbSet<DelAggVariantGroup> DelAggVariantGroups { get; set; }
        public virtual IDbSet<DelAggVariant> DelAggVariants { get; set; }
        public virtual IDbSet<DelAggModifierGroup> DelAggModifierGroups { get; set; }
        public virtual IDbSet<DelAggModifier> DelAggModifiers { get; set; }
        public virtual IDbSet<DelAggModifierGroupItem> DelAggModifierGroupItems { get; set; }
        public virtual IDbSet<DelAggCategory> DelAggCategorys { get; set; }
        public virtual IDbSet<DelTimingGroup> DelTimingGroups { get; set; }
        public virtual IDbSet<DelTimingDetail> DelTimingDetails { get; set; }
        public virtual IDbSet<DelAggTax> DelAggTaxs { get; set; }
        public virtual IDbSet<DelAggTaxMapping> DelAggTaxMappings { get; set; }
        public virtual IDbSet<DelAggCharge> DelAggCharges { get; set; }
        public virtual IDbSet<DelAggChargeMapping> DelAggChargeMappings { get; set; }
        public virtual IDbSet<DelAggLanguage> DelAggLanguages { get; set; }
        public virtual IDbSet<DelAggImage> DelAggImages { get; set; }
        #endregion Cluster

        #region Tick

        public virtual IDbSet<EmployeeRawInfo> EmployeeRawInfos { get; set; }
        public virtual IDbSet<SkillSet> SkillSets { get; set; }
        public virtual IDbSet<PersonalInformation> PersonalInformations { get; set; }

        public virtual IDbSet<EmployeeSkillSet> EmployeeSkillSets { get; set; }
        public virtual IDbSet<DocumentInfo> DocumentInfos { get; set; }
        public virtual IDbSet<EmployeeDocumentInfo> EmployeeDocumentInfos { get; set; }
        public virtual IDbSet<DocumentForSkillSet> DocumentForSkillSets { get; set; }
        public virtual IDbSet<AttendanceMachineLocation> AttendanceMachineLocations { get; set; }
        public virtual IDbSet<AttendanceLog> AttendanceLogs { get; set; }
        public virtual IDbSet<GpsAttendance> GpsAttendances { get; set; }
        public virtual IDbSet<GpsLive> GpsLive { get; set; }
        public virtual IDbSet<EmployeeMailMessage> EmployeeMailMessages { get; set; }
        public virtual IDbSet<CommonMailMessage> CommonMailMessages { get; set; }
        public virtual IDbSet<UserDefaultInformation> UserDefaultInformations { get; set; }
        public virtual IDbSet<LeaveType> LeaveTypes { get; set; }
        public virtual IDbSet<LeaveExhaustedList> LeaveExhaustedLists { get; set; }
        public virtual IDbSet<YearWiseLeaveAllowedForEmployee> YearWiseLeaveAllowedForEmployees { get; set; }
        public virtual IDbSet<LeaveRequest> LeaveRequests { get; set; }
        public virtual IDbSet<SalaryInfo> SalaryInfos { get; set; }
        public virtual IDbSet<WorkDay> WorkDays { get; set; }
        public virtual IDbSet<PublicHoliday> PublicHolidays { get; set; }
        public virtual IDbSet<IncentiveCategory> IncentiveCategories { get; set; }
        public virtual IDbSet<IncentiveTag> IncentiveTags { get; set; }
        public virtual IDbSet<IncentiveExcludedList> IncentiveExcludedLists { get; set; }
        public virtual IDbSet<IncentiveVsEntity> IncentiveVsEntities { get; set; }
        public virtual IDbSet<EmployeeVsIncentive> EmployeeVsIncentives { get; set; }
        public virtual IDbSet<EmployeeVsSalaryTag> EmployeeVsSalaryTags { get; set; }
        public virtual IDbSet<ManualIncentive> ManualIncentives { get; set; }
        public virtual IDbSet<EmployeeCostCentreMaster> EmployeeCostCentreMasters { get; set; }
        public virtual IDbSet<EmployeeResidentStatus> EmployeeResidentStatuses { get; set; }
        public virtual IDbSet<EmployeeDepartmentMaster> EmployeeDepartmentMasters { get; set; }
        public virtual IDbSet<EmployeeSectionMaster> EmployeeSectionMasters { get; set; }
        public virtual IDbSet<EmployeeSalaryClassification> EmployeeSalaryClassifications { get; set; }
        public virtual IDbSet<EmployeeRace> EmployeeRaces { get; set; }
        public virtual IDbSet<EmployeeReligion> EmployeeReligions { get; set; }
        public virtual IDbSet<EMailInformation> EMailInformations { get; set; }
        public virtual IDbSet<EMailLinkedWithActions> EMailLinkedWithActions { get; set; }
        public virtual IDbSet<MonthWiseWorkDay> MonthWiseWorkDays { get; set; }
        public virtual IDbSet<DcHeadMaster> DcHeadMasters { get; set; }
        public virtual IDbSet<DcGroupMaster> DcGroupMasters { get; set; }
        public virtual IDbSet<DcGroupMasterVsSkillSet> DcGroupMasterVsSkillSets { get; set; }
        public virtual IDbSet<DcStationMaster> DcStationMasters { get; set; }
        public virtual IDbSet<DcShiftMaster> DcShiftMasters { get; set; }
        public virtual IDbSet<DutyChart> DutyCharts { get; set; }
        public virtual IDbSet<DutyChartDetail> DutyChartDetails { get; set; }
        public virtual IDbSet<BioMetricExcludedEntry> BioMetricExcludedEntries { get; set; }
        public virtual IDbSet<EmployeeDefaultWorkStation> EmployeeDefaultWorkStations { get; set; }
        public virtual IDbSet<SalaryPaidMaster> SalaryPaidMasters { get; set; }
        public virtual IDbSet<SalaryPaidDetail> SalaryPaidDetails { get; set; }
        public virtual IDbSet<SalaryPaidConsolidatedIncentives> SalaryPaidConsolidatedIncentives { get; set; }
        public virtual IDbSet<TimeSheetMaster> TimeSheetMasters { get; set; }
        public virtual IDbSet<TimeSheetDetail> TimeSheetDetails { get; set; }
        public virtual IDbSet<TimeSheetMaterialUsage> TimeSheetMaterialUsages { get; set; }
        public virtual IDbSet<TimeSheetEmployeeDetail> TimeSheetEmployeeDetails { get; set; }
        public virtual IDbSet<StatusMailDate> StatusMailDates { get; set; }
        public virtual IDbSet<OtBasedOnSlab> OtBasedOnSlabs { get; set; }
        public virtual IDbSet<JobTitleMaster> JobTitleMasters { get; set; }

        #endregion Tick

        #region Wheel

        public virtual IDbSet<DeliveryTicket> DeliveryTickets { get; set; }
        public virtual IDbSet<DeliveryOrder> DeliveryOrders { get; set; }

        #endregion Wheel

        #region Engage

        public virtual IDbSet<FeedBackGroup> FeedBackGroups { get; set; }
        public virtual IDbSet<FeedBackQuestion> FeedBackQuestions { get; set; }
        public virtual IDbSet<FeedBack> FeedBacks { get; set; }
        public virtual IDbSet<FeedBackAnswer> FeedBackAnswers { get; set; }
        public virtual IDbSet<GiftVoucherType> GiftVoucherTypes { get; set; }
        public virtual IDbSet<GiftVoucherCategory> GiftVoucherCategorys { get; set; }
        public virtual IDbSet<GiftVoucher> GiftVouchers { get; set; }
        public virtual IDbSet<ConnectMember> Members { get; set; }
        public virtual IDbSet<MemberContactDetail> MemberContactDetails { get; set; }
        public virtual IDbSet<MemberAddress> Address { get; set; }
        public virtual IDbSet<Reservation> Reservations { get; set; }
        public virtual IDbSet<MemberPoint> MemberPoints { get; set; }
        public virtual IDbSet<LoyaltyProgram> LoyaltyPrograms { get; set; }
        public virtual IDbSet<ProgramStage> ProgramStages { get; set; }
        public virtual IDbSet<PointAccumulation> PointAccumulations { get; set; }
        public virtual IDbSet<MemberAccount> MemberAccount { get; set; }
        public virtual IDbSet<MemberAccountType> MemberAccountType { get; set; }
        public virtual IDbSet<MemberAccountTransaction> MemberAccountTransaction { get; set; }
        public virtual IDbSet<MembershipTier> MembershipTier { get; set; }
        public virtual IDbSet<MemberServiceInfoTemplate> MemberServiceInfo { get; set; }
        public virtual IDbSet<MemberServiceInfoType> MemberServiceInfoType { get; set; }
        public virtual IDbSet<PointCampaign> PointCampaign { get; set; }
        public virtual IDbSet<PointCampaignModifier> PointCampaignModifier { get; set; }
        public virtual IDbSet<PointCampaignModifierType> PointCampaignModifierType { get; set; }

        #endregion Engage

        #region Swipe

        public virtual IDbSet<SwipeCardType> SwipeCardTypes { get; set; }
        public virtual IDbSet<SwipeCard> SwipeCards { get; set; }
        public virtual IDbSet<MemberCard> MemberCards { get; set; }
        public virtual IDbSet<SwipeCardLedger> SwipeCardLedgers { get; set; }
        public virtual IDbSet<SwipePaymentTender> SwipePaymentTenders { get; set; }
        public virtual IDbSet<SwipePaymentType> SwipePaymentTypes { get; set; }
        public virtual IDbSet<SwipeShift> SwipeShifts { get; set; }
        public virtual IDbSet<SwipeShiftTender> SwipeShiftTenders { get; set; }

        #endregion Swipe

        #region Cater

        public virtual IDbSet<CaterQuotationDetail> CaterQuotationDetails { get; set; }
        public virtual IDbSet<CaterCustomer> CaterCustomers { get; set; }
        public virtual IDbSet<CaterCustomerAddressDetail> CaterCustomerAddressDetails { get; set; }
        public virtual IDbSet<CaterQuotationHeader> CaterQuotationHeaders { get; set; }
        public virtual IDbSet<CaterQuoteRequiredMaterial> CaterQuoteRequiredMaterials { get; set; }
        public virtual IDbSet<CaterQuoteRequiredMaterialVsPurchaseOrderLink> CaterQuoteRequiredMaterialVsPurchaseOrderLinks { get; set; }
        public virtual IDbSet<CaterQuoteRequiredMaterialVsMenuLink> CaterQuoteRequiredMaterialVsMenuLinks { get; set; }

        #endregion Cater


        #region Go

        public virtual IDbSet<DineGoDevice> DineGoDevices { get; set; }
        public virtual IDbSet<DineGoBrand> DineGoBrands { get; set; }
        public virtual IDbSet<DineGoDepartment> DineGoDepartments { get; set; }
        public virtual IDbSet<DineGoPaymentType> DineGoPaymentTypes { get; set; }
        public virtual IDbSet<DineGoCharge> DineGoCharges { get; set; }

        #endregion Go

        #region Addons Or Others

        public virtual IDbSet<ConnectAddOn> AddOns { get; set; }
        public virtual IDbSet<BookInvoice> BookInvoices { get; set; }
        public virtual IDbSet<SystemLogging> SystemLoggings { get; set; }
        public virtual IDbSet<BinaryObject> BinaryObjects { get; set; }
        public DbSet<TrackerAuditLog> AuditLog { get; set; }
        public DbSet<TrackerAuditLogDetail> LogDetails { get; set; }
       

        #endregion Addons Or Others

        #region Play

        public virtual IDbSet<Layout> Layouts { get; set; }
        public virtual IDbSet<Resolution> Resolutions { get; set; }
        public virtual IDbSet<LayoutContent> LayoutContents { get; set; }
        public virtual IDbSet<Display> Displays { get; set; }
        public virtual IDbSet<DisplayGroup> DisplayGroups { get; set; }
        public virtual IDbSet<Daypart> Dayparts { get; set; }
        public virtual IDbSet<DaypartException> DaypartException { get; set; }
        public virtual IDbSet<ScheduledEvent> ScheduledEvents { get; set; }

        #endregion Play
    }
}