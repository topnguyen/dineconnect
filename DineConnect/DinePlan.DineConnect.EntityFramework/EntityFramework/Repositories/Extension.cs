﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Builders;
using System.Data.Entity.Migrations.Infrastructure;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.EntityFramework.Repositories
{
    public class AddColumnIfNotExistsOperation : MigrationOperation
    {
        public readonly string Table;
        public readonly string Name;
        public readonly ColumnModel ColumnModel;

        public AddColumnIfNotExistsOperation(string table, string name, Func<ColumnBuilder, ColumnModel> columnAction, object anonymousArguments) : base(anonymousArguments)
        {

            Table = table;
            Name = name;

            ColumnModel = columnAction(new ColumnBuilder());
            ColumnModel.Name = name;

        }

        public override bool IsDestructiveChange => false;

        public override MigrationOperation Inverse => new DropColumnOperation(Table, Name, removedAnnotations: ColumnModel.Annotations.ToDictionary(s => s.Key,s => (object)s.Value) , anonymousArguments: null);
    }

    public class DropColumnIfExistsOperation : MigrationOperation
    {
        public readonly string Table;
        public readonly string Name;

        public DropColumnIfExistsOperation(string table, string name,object anonymousArguments) : base(anonymousArguments)
        {
            Table = table;
            Name = name;
        }

        public override bool IsDestructiveChange => false;

        public override MigrationOperation Inverse => new DropColumnOperation(Table, Name , anonymousArguments: null);
    }

    public static class MigrationExtensions
    {
        public static void AddColumnIfNotExists(this DbMigration migration, string table, string name, Func<ColumnBuilder, ColumnModel> columnAction, object anonymousArguments = null)
        {
            ((IDbMigration)migration)
                .AddOperation(new AddColumnIfNotExistsOperation(table, name, columnAction, anonymousArguments));
        }

        public static void DropColumnIfExists(this DbMigration migration, string table, string name)
        {
            ((IDbMigration)migration)
                .AddOperation(new DropColumnIfExistsOperation(table, name, null));
        }
    }

    public class AddColumnIfNotExistsSqlGenerator : SqlServerMigrationSqlGenerator
    {
        protected override void Generate(MigrationOperation migrationOperation)
        {
            var operation = migrationOperation as AddColumnIfNotExistsOperation;
            if (operation == null) return;

            using (var writer = Writer())
            {
                

                writer.WriteLine("IF NOT EXISTS(SELECT 1 FROM sys.columns");
                writer.WriteLine($"WHERE Name = N'{operation.Name}' AND Object_ID = Object_ID(N'{Name(operation.Table)}'))");
                writer.WriteLine("BEGIN");
                writer.WriteLine("ALTER TABLE ");
                writer.WriteLine(Name(operation.Table));
                writer.Write(" ADD ");

                var column = operation.ColumnModel;
                Generate(column, writer);

                if (column.IsNullable != null
                    && !column.IsNullable.Value
                    && (column.DefaultValue == null)
                    && (string.IsNullOrWhiteSpace(column.DefaultValueSql))
                    && !column.IsIdentity
                    && !column.IsTimestamp)
                {
                    writer.Write(" DEFAULT ");

                    if (column.Type == PrimitiveTypeKind.DateTime)
                    {
                        writer.Write(Generate(DateTime.Parse("1900-01-01 00:00:00", CultureInfo.InvariantCulture)));
                    }
                    else
                    {
                        writer.Write(Generate((dynamic)column.ClrDefaultValue));
                    }
                }

                writer.WriteLine(" END");



                Statement(writer);
            }
        }
    }

    public class DropColumnIfExistsSqlGenerator : SqlServerMigrationSqlGenerator
    {
        protected override void Generate(MigrationOperation migrationOperation)
        {
            var operation = migrationOperation as AddColumnIfNotExistsOperation;
            if (operation == null) return;

            using (var writer = Writer())
            {
                writer.WriteLine("IF EXISTS(SELECT 1 FROM sys.columns");
                writer.WriteLine(
                    $"WHERE Name = N'{operation.Name}' AND Object_ID = Object_ID(N'{Name(operation.Table)}'))");
                writer.WriteLine("BEGIN");
                writer.WriteLine("ALTER TABLE ");
                writer.WriteLine(Name(operation.Table));
                writer.Write(" DROP COLUMN ");
                if (operation.Name.Equals("Key"))
                {
                    writer.Write("\"" + operation.Name + "\"");
                }
                else
                {
                    writer.Write(operation.Name);
                }

                writer.WriteLine(" END");
                Statement(writer);
            }
        }
    }

}
