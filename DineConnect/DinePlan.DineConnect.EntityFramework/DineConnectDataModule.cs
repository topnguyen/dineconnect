﻿using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using Abp.Zero.EntityFramework;
using DinePlan.DineConnect.EntityFramework;
using Z.EntityFramework.Plus;

namespace DinePlan.DineConnect
{
    /// <summary>
    ///     Entity framework module of the application.
    /// </summary>
    [DependsOn(typeof (AbpZeroEntityFrameworkModule), typeof (DineConnectCoreModule))]
    public class DineConnectDataModule : AbpModule
    {
        public override void PreInitialize()
        {
         
            Configuration.DefaultNameOrConnectionString = "Default";
            Configuration.UnitOfWork.RegisterFilter("ConnectFilter", true);
        }

        public override void Initialize()
        {
            Configuration.Auditing.IsEnabled = true;
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DineConnectDbContext, Migrations.Configuration>());
        }
    }
}