namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_MemberContactDetails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MemberContactDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ConnectMemberId = c.Int(nullable: false),
                        ContactDetailType = c.Int(nullable: false),
                        Detail = c.String(),
                        AcceptToCommunicate = c.Boolean(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ConnectMembers", t => t.ConnectMemberId, cascadeDelete: true)
                .Index(t => t.ConnectMemberId);
            
            AddColumn("dbo.MemberAddresses", "Tag", c => c.String());
            AddColumn("dbo.MemberAddresses", "isPrimary", c => c.Boolean(nullable: false));
            AddColumn("dbo.ConnectMembers", "LastName", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MemberContactDetails", "ConnectMemberId", "dbo.ConnectMembers");
            DropIndex("dbo.MemberContactDetails", new[] { "ConnectMemberId" });
            DropColumn("dbo.ConnectMembers", "LastName");
            DropColumn("dbo.MemberAddresses", "isPrimary");
            DropColumn("dbo.MemberAddresses", "Tag");
            DropTable("dbo.MemberContactDetails");
        }
    }
}
