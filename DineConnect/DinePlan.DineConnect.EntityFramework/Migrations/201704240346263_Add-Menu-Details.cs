namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMenuDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItems", "Tag", c => c.String(maxLength: 50));
            AddColumn("dbo.MenuItems", "Location", c => c.String(maxLength: 50));
            AlterColumn("dbo.MenuItems", "Name", c => c.String(maxLength: 100));
            AlterColumn("dbo.MenuItems", "BarCode", c => c.String(maxLength: 50));
            AlterColumn("dbo.MenuItems", "AliasCode", c => c.String(maxLength: 50));
            AlterColumn("dbo.MenuItems", "AliasName", c => c.String(maxLength: 100));
            AlterColumn("dbo.MenuItems", "ItemDescription", c => c.String(maxLength: 1000));
            AlterColumn("dbo.Orders", "DepartmentName", c => c.String(maxLength: 50));
            AlterColumn("dbo.Orders", "MenuItemName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Orders", "PortionName", c => c.String(maxLength: 50));
            AlterColumn("dbo.Orders", "CreatingUserName", c => c.String(maxLength: 50));
            AlterColumn("dbo.Orders", "PriceTag", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "PriceTag", c => c.String());
            AlterColumn("dbo.Orders", "CreatingUserName", c => c.String());
            AlterColumn("dbo.Orders", "PortionName", c => c.String());
            AlterColumn("dbo.Orders", "MenuItemName", c => c.String());
            AlterColumn("dbo.Orders", "DepartmentName", c => c.String());
            AlterColumn("dbo.MenuItems", "ItemDescription", c => c.String());
            AlterColumn("dbo.MenuItems", "AliasName", c => c.String());
            AlterColumn("dbo.MenuItems", "AliasCode", c => c.String());
            AlterColumn("dbo.MenuItems", "BarCode", c => c.String());
            AlterColumn("dbo.MenuItems", "Name", c => c.String());
            DropColumn("dbo.MenuItems", "Location");
            DropColumn("dbo.MenuItems", "Tag");
        }
    }
}
