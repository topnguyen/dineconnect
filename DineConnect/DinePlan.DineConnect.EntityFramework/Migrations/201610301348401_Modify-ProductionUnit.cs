namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyProductionUnit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductionUnits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        Name = c.String(),
                        Code = c.String(),
                        MaterialAllowed = c.String(),
                        IsActive = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProductionUnit_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ProductionUnit_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .Index(t => t.LocationRefId);
            
            AddColumn("dbo.Locations", "IsProductionUnitAllowed", c => c.Boolean(nullable: false));
            AddColumn("dbo.Materials", "IsHighValueItem", c => c.Boolean(nullable: false));
            AddColumn("dbo.SupplierMaterials", "MinimumOrderQuantity", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Issues", "ProductionUnitRefId", c => c.Int());
            AddColumn("dbo.Requests", "ProductionUnitRefId", c => c.Int());
            AddColumn("dbo.Productions", "ProductionUnitRefId", c => c.Int());
            AddColumn("dbo.PurchaseOrderDetails", "QtyReceived", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AddColumn("dbo.Returns", "ProductionUnitRefId", c => c.Int());
            AddColumn("dbo.Yield", "ProductionUnitRefId", c => c.Int());
            CreateIndex("dbo.Issues", "ProductionUnitRefId");
            CreateIndex("dbo.Requests", "ProductionUnitRefId");
            CreateIndex("dbo.Productions", "ProductionUnitRefId");
            CreateIndex("dbo.Returns", "ProductionUnitRefId");
            CreateIndex("dbo.Yield", "ProductionUnitRefId");
            AddForeignKey("dbo.Issues", "ProductionUnitRefId", "dbo.ProductionUnits", "Id");
            AddForeignKey("dbo.Requests", "ProductionUnitRefId", "dbo.ProductionUnits", "Id");
            AddForeignKey("dbo.Productions", "ProductionUnitRefId", "dbo.ProductionUnits", "Id");
            AddForeignKey("dbo.Returns", "ProductionUnitRefId", "dbo.ProductionUnits", "Id");
            AddForeignKey("dbo.Yield", "ProductionUnitRefId", "dbo.ProductionUnits", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Yield", "ProductionUnitRefId", "dbo.ProductionUnits");
            DropForeignKey("dbo.Returns", "ProductionUnitRefId", "dbo.ProductionUnits");
            DropForeignKey("dbo.Productions", "ProductionUnitRefId", "dbo.ProductionUnits");
            DropForeignKey("dbo.Requests", "ProductionUnitRefId", "dbo.ProductionUnits");
            DropForeignKey("dbo.Issues", "ProductionUnitRefId", "dbo.ProductionUnits");
            DropForeignKey("dbo.ProductionUnits", "LocationRefId", "dbo.Locations");
            DropIndex("dbo.Yield", new[] { "ProductionUnitRefId" });
            DropIndex("dbo.Returns", new[] { "ProductionUnitRefId" });
            DropIndex("dbo.Productions", new[] { "ProductionUnitRefId" });
            DropIndex("dbo.Requests", new[] { "ProductionUnitRefId" });
            DropIndex("dbo.Issues", new[] { "ProductionUnitRefId" });
            DropIndex("dbo.ProductionUnits", new[] { "LocationRefId" });
            DropColumn("dbo.Yield", "ProductionUnitRefId");
            DropColumn("dbo.Returns", "ProductionUnitRefId");
            DropColumn("dbo.PurchaseOrderDetails", "QtyReceived");
            DropColumn("dbo.Productions", "ProductionUnitRefId");
            DropColumn("dbo.Requests", "ProductionUnitRefId");
            DropColumn("dbo.Issues", "ProductionUnitRefId");
            DropColumn("dbo.SupplierMaterials", "MinimumOrderQuantity");
            DropColumn("dbo.Materials", "IsHighValueItem");
            DropColumn("dbo.Locations", "IsProductionUnitAllowed");
            DropTable("dbo.ProductionUnits",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProductionUnit_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ProductionUnit_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
