namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_DineConnect_EmployeeisActive : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmployeeInfos", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmployeeInfos", "IsActive");
        }
    }
}
