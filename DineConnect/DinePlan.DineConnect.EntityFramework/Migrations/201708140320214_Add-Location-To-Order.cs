namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLocationToOrder : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Orders", "Location_Id", "dbo.Locations");
            DropIndex("dbo.Orders", new[] { "Location_Id" });
            AlterColumn("dbo.Orders", "Location_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Orders", "Location_Id");
            AddForeignKey("dbo.Orders", "Location_Id", "dbo.Locations", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "Location_Id", "dbo.Locations");
            DropIndex("dbo.Orders", new[] { "Location_Id" });
            AlterColumn("dbo.Orders", "Location_Id", c => c.Int());
            CreateIndex("dbo.Orders", "Location_Id");
            AddForeignKey("dbo.Orders", "Location_Id", "dbo.Locations", "Id");
        }
    }
}
