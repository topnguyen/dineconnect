namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePlan_Logger : DbMigration
    {
        public override void Up()
        {
            Sql("Truncate Table dbo.PlanLoggers");
            AlterColumn("dbo.PlanLoggers", "Terminal", c => c.String());
            AlterColumn("dbo.PlanLoggers", "TicketNo", c => c.String());
            AlterColumn("dbo.PlanLoggers", "UserName", c => c.String());
            AlterColumn("dbo.PlanLoggers", "EventName", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PlanLoggers", "EventName", c => c.String(maxLength: 25));
            AlterColumn("dbo.PlanLoggers", "UserName", c => c.String(maxLength: 25));
            AlterColumn("dbo.PlanLoggers", "TicketNo", c => c.String(maxLength: 25));
            AlterColumn("dbo.PlanLoggers", "Terminal", c => c.String(maxLength: 25));
        }
    }
}
