namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCardTypePromotion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Promotions", "ConnectCardTypeId", c => c.Int());
            CreateIndex("dbo.Promotions", "ConnectCardTypeId");
            AddForeignKey("dbo.Promotions", "ConnectCardTypeId", "dbo.ConnectCardTypes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Promotions", "ConnectCardTypeId", "dbo.ConnectCardTypes");
            DropIndex("dbo.Promotions", new[] { "ConnectCardTypeId" });
            DropColumn("dbo.Promotions", "ConnectCardTypeId");
        }
    }
}
