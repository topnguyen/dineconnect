namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLocationTicketMessage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TicketMessages", "LocationId", c => c.Int());
            CreateIndex("dbo.TicketMessages", "LocationId");
            AddForeignKey("dbo.TicketMessages", "LocationId", "dbo.Locations", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TicketMessages", "LocationId", "dbo.Locations");
            DropIndex("dbo.TicketMessages", new[] { "LocationId" });
            DropColumn("dbo.TicketMessages", "LocationId");
        }
    }
}
