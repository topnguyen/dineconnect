namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLoyaltyChanges : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LoyaltyPrograms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 30),
                        Expiry = c.Boolean(nullable: false),
                        ExpiryDays = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProgramStages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LoyaltyProgramId = c.Int(nullable: false),
                        Name = c.String(maxLength: 30),
                        Condition = c.String(),
                        ConditionDayValue = c.Int(nullable: false),
                        Order = c.Int(nullable: false),
                        HasNext = c.Boolean(nullable: false),
                        PointsExpiryAllowed = c.Boolean(nullable: false),
                        MinRedemptionPoints = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RedemptionValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PointExpiryDays = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LoyaltyPrograms", t => t.LoyaltyProgramId, cascadeDelete: true)
                .Index(t => t.LoyaltyProgramId);
            
            CreateTable(
                "dbo.PointAccumulations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StageId = c.Int(nullable: false),
                        PAccTypeId = c.Int(nullable: false),
                        Point = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RoundOff = c.Boolean(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProgramStages", t => t.StageId, cascadeDelete: true)
                .Index(t => t.StageId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PointAccumulations", "StageId", "dbo.ProgramStages");
            DropForeignKey("dbo.ProgramStages", "LoyaltyProgramId", "dbo.LoyaltyPrograms");
            DropIndex("dbo.PointAccumulations", new[] { "StageId" });
            DropIndex("dbo.ProgramStages", new[] { "LoyaltyProgramId" });
            DropTable("dbo.PointAccumulations");
            DropTable("dbo.ProgramStages");
            DropTable("dbo.LoyaltyPrograms");
        }
    }
}
