namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_SupplierGstInformation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SupplierGSTInformations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SupplierRefId = c.Int(nullable: false),
                        SupplierLegalName = c.String(),
                        GstStatus = c.String(),
                        ConstitutionStatus = c.String(),
                        Gstin = c.String(),
                        GstCertificate = c.Guid(),
                        ReverseChargeApplicable = c.Boolean(nullable: false),
                        PayGstApplicable = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SupplierGSTInformation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SupplierGSTInformation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Suppliers", t => t.SupplierRefId, cascadeDelete: true)
                .Index(t => t.SupplierRefId);
            
            AddColumn("dbo.Materials", "Hsncode", c => c.String());
            AddColumn("dbo.Suppliers", "StateCode", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SupplierGSTInformations", "SupplierRefId", "dbo.Suppliers");
            DropIndex("dbo.SupplierGSTInformations", new[] { "SupplierRefId" });
            DropColumn("dbo.Suppliers", "StateCode");
            DropColumn("dbo.Materials", "Hsncode");
            DropTable("dbo.SupplierGSTInformations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SupplierGSTInformation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SupplierGSTInformation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
