namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SwipeAddMinimum : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SwipeCards", "MinimumTopUpAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SwipeCardTypes", "MinimumTopUpAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SwipeCardTypes", "MinimumTopUpAmount");
            DropColumn("dbo.SwipeCards", "MinimumTopUpAmount");
        }
    }
}
