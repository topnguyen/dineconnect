namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeLocationAndBranchDetails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LocationBranches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        District = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Country = c.String(),
                        PostalCode = c.String(),
                        PhoneNumber = c.String(),
                        Fax = c.String(),
                        Website = c.String(),
                        Email = c.String(),
                        BranchTaxCode = c.String(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Locations", "District", c => c.String());
            AddColumn("dbo.Locations", "PostalCode", c => c.String());
            AddColumn("dbo.Locations", "LocationBranchId", c => c.Int());
            CreateIndex("dbo.Locations", "LocationBranchId");
            AddForeignKey("dbo.Locations", "LocationBranchId", "dbo.LocationBranches", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Locations", "LocationBranchId", "dbo.LocationBranches");
            DropIndex("dbo.Locations", new[] { "LocationBranchId" });
            DropColumn("dbo.Locations", "LocationBranchId");
            DropColumn("dbo.Locations", "PostalCode");
            DropColumn("dbo.Locations", "District");
            DropTable("dbo.LocationBranches");
        }
    }
}
