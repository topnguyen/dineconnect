namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMembershipDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConnectMembers", "BirthDay", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ConnectMembers", "BirthDay");
        }
    }
}
