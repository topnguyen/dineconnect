// <auto-generated />
namespace DinePlan.DineConnect.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Add_NumberOfPax_MenuItem : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Add_NumberOfPax_MenuItem));
        
        string IMigrationMetadata.Id
        {
            get { return "202108190250080_Add_NumberOfPax_MenuItem"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
