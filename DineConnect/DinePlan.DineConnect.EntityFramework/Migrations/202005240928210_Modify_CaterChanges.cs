namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_CaterChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CaterQuoteRequiredMaterials", "StatusRefId", c => c.Int());
            DropColumn("dbo.CaterQuoteRequiredMaterialVsMenuLinks", "DefaultUnitId");
            DropColumn("dbo.CaterQuoteRequiredMaterialVsMenuLinks", "AutoSalesDeduction");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CaterQuoteRequiredMaterialVsMenuLinks", "AutoSalesDeduction", c => c.Boolean(nullable: false));
            AddColumn("dbo.CaterQuoteRequiredMaterialVsMenuLinks", "DefaultUnitId", c => c.Int(nullable: false));
            DropColumn("dbo.CaterQuoteRequiredMaterials", "StatusRefId");
        }
    }
}
