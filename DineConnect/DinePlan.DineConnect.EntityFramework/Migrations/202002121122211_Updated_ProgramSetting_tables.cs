namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updated_ProgramSetting_tables : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProgramSettings", "ActualValue", c => c.String());
            DropColumn("dbo.ProgramSettings", "DefaultValue");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProgramSettings", "DefaultValue", c => c.String());
            DropColumn("dbo.ProgramSettings", "ActualValue");
        }
    }
}
