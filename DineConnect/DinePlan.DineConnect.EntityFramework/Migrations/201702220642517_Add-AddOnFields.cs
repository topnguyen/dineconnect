namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAddOnFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Suppliers", "AddOn", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Suppliers", "AddOn");
        }
    }
}
