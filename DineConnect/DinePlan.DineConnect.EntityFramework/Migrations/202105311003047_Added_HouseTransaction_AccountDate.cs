namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_HouseTransaction_AccountDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Adjustments", "AccountDate", c => c.DateTime());
            AddColumn("dbo.InterTransfers", "AccountDate", c => c.DateTime());
            AddColumn("dbo.InterTransfers", "InterTransferReceivedAccountDate", c => c.DateTime());
            AddColumn("dbo.InwardDirectCredits", "AccountDate", c => c.DateTime());
            AddColumn("dbo.Issues", "AccountDate", c => c.DateTime());
            AddColumn("dbo.Productions", "AccountDate", c => c.DateTime());
            AddColumn("dbo.PurchaseReturns", "AccountDate", c => c.DateTime());
            AddColumn("dbo.Returns", "AccountDate", c => c.DateTime());
            AddColumn("dbo.SalesDeliveryOrders", "AccountDate", c => c.DateTime());
            AddColumn("dbo.Yield", "AccountDate", c => c.DateTime());
            AddColumn("dbo.Yield", "YieldOutputAccountDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Yield", "YieldOutputAccountDate");
            DropColumn("dbo.Yield", "AccountDate");
            DropColumn("dbo.SalesDeliveryOrders", "AccountDate");
            DropColumn("dbo.Returns", "AccountDate");
            DropColumn("dbo.PurchaseReturns", "AccountDate");
            DropColumn("dbo.Productions", "AccountDate");
            DropColumn("dbo.Issues", "AccountDate");
            DropColumn("dbo.InwardDirectCredits", "AccountDate");
            DropColumn("dbo.InterTransfers", "InterTransferReceivedAccountDate");
            DropColumn("dbo.InterTransfers", "AccountDate");
            DropColumn("dbo.Adjustments", "AccountDate");
        }
    }
}
