namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Request : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RequestDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RequestRefId = c.Int(nullable: false),
                        Sno = c.Int(nullable: false),
                        RecipeRefId = c.Int(),
                        MaterialRefId = c.Int(nullable: false),
                        RequestQty = c.Decimal(nullable: false, precision: 18, scale: 3),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RequestDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId, cascadeDelete: true)
                .ForeignKey("dbo.Materials", t => t.RecipeRefId)
                .ForeignKey("dbo.Requests", t => t.RequestRefId, cascadeDelete: true)
                .Index(t => t.RequestRefId)
                .Index(t => t.RecipeRefId)
                .Index(t => t.MaterialRefId);
            
            CreateTable(
                "dbo.Requests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        RequestSlipNumber = c.String(),
                        RequestTime = c.DateTime(nullable: false),
                        RecipeRefId = c.Int(),
                        RecipeProductionQty = c.Decimal(precision: 18, scale: 3),
                        MaterialGroupCategoryRefId = c.Int(nullable: false),
                        MultipleBatchProductionAllowed = c.Boolean(nullable: false),
                        CompletedStatus = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Request_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Request_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId, cascadeDelete: true)
                .ForeignKey("dbo.Materials", t => t.RecipeRefId)
                .Index(t => t.LocationRefId)
                .Index(t => t.RecipeRefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RequestDetails", "RequestRefId", "dbo.Requests");
            DropForeignKey("dbo.Requests", "RecipeRefId", "dbo.Materials");
            DropForeignKey("dbo.Requests", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.RequestDetails", "RecipeRefId", "dbo.Materials");
            DropForeignKey("dbo.RequestDetails", "MaterialRefId", "dbo.Materials");
            DropIndex("dbo.Requests", new[] { "RecipeRefId" });
            DropIndex("dbo.Requests", new[] { "LocationRefId" });
            DropIndex("dbo.RequestDetails", new[] { "MaterialRefId" });
            DropIndex("dbo.RequestDetails", new[] { "RecipeRefId" });
            DropIndex("dbo.RequestDetails", new[] { "RequestRefId" });
            DropTable("dbo.Requests",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Request_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Request_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.RequestDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RequestDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
