namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CalculationHeaderChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Calculations", "ButtonHeader", c => c.String());
            DropColumn("dbo.Calculations", "ButterHeader");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Calculations", "ButterHeader", c => c.String());
            DropColumn("dbo.Calculations", "ButtonHeader");
        }
    }
}
