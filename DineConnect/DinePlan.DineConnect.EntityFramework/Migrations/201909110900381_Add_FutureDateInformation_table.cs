namespace DinePlan.DineConnect.Migrations
{
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;

    public partial class Add_FutureDateInformation_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FutureDateInformations",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(),
                    FutureDate = c.DateTime(nullable: false),
                    FutureDateType = c.Int(nullable: false),
                    Contents = c.String(),
                    Oid = c.Int(nullable: false),
                    Locations = c.String(),
                    Group = c.Boolean(nullable: false),
                    NonLocations = c.String(),
                    TenantId = c.Int(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    DeleterUserId = c.Long(),
                    DeletionTime = c.DateTime(),
                    LastModificationTime = c.DateTime(),
                    LastModifierUserId = c.Long(),
                    CreationTime = c.DateTime(nullable: false),
                    CreatorUserId = c.Long(),
                },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_FutureDateInformation_ConnectFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_FutureDateInformation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_FutureDateInformation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
        }

        public override void Down()
        {
            DropTable("dbo.FutureDateInformations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_FutureDateInformation_ConnectFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_FutureDateInformation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_FutureDateInformation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}