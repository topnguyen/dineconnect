namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_NumberOfPax_Orders : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "NumberOfPax", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "NumberOfPax");
        }
    }
}
