namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDelAggLocationNoGroup : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DelAggLocations", "DelAggLocationGroupId", "dbo.DelAggLocationGroups");
            DropIndex("dbo.DelAggLocations", new[] { "DelAggLocationGroupId" });
            AddColumn("dbo.DelAggLocations", "Name", c => c.String());
            AlterColumn("dbo.DelAggLocations", "DelAggLocationGroupId", c => c.Int());
            CreateIndex("dbo.DelAggLocations", "DelAggLocationGroupId");
            AddForeignKey("dbo.DelAggLocations", "DelAggLocationGroupId", "dbo.DelAggLocationGroups", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DelAggLocations", "DelAggLocationGroupId", "dbo.DelAggLocationGroups");
            DropIndex("dbo.DelAggLocations", new[] { "DelAggLocationGroupId" });
            AlterColumn("dbo.DelAggLocations", "DelAggLocationGroupId", c => c.Int(nullable: false));
            DropColumn("dbo.DelAggLocations", "Name");
            CreateIndex("dbo.DelAggLocations", "DelAggLocationGroupId");
            AddForeignKey("dbo.DelAggLocations", "DelAggLocationGroupId", "dbo.DelAggLocationGroups", "Id", cascadeDelete: true);
        }
    }
}
