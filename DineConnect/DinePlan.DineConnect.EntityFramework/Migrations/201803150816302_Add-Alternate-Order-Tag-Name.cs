namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAlternateOrderTagName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderTags", "AlternateName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderTags", "AlternateName");
        }
    }
}
