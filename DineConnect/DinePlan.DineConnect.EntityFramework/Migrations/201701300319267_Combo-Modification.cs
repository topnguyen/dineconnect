namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ComboModification : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductComboes", "Name", c => c.String());
            DropColumn("dbo.ProductComboes", "SyncId");
            DropColumn("dbo.ProductComboes", "SortOrder");
            Sql("UPDATE MenuItems SET ProductType=1");

        }

        public override void Down()
        {
            AddColumn("dbo.ProductComboes", "SortOrder", c => c.Int(nullable: false));
            AddColumn("dbo.ProductComboes", "SyncId", c => c.Int(nullable: false));
            DropColumn("dbo.ProductComboes", "Name");
        }
    }
}
