namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_ConnectMembers : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.ConnectMembers", "MemberCode");
            AlterColumn("dbo.ConnectMembers", "MemberCode", c => c.String(maxLength: 50));
            CreateIndex("dbo.ConnectMembers", "MemberCode", unique: true, name: "MemberCode");
        }
        
        public override void Down()
        {
            DropIndex("dbo.ConnectMembers", "MemberCode");
            AlterColumn("dbo.ConnectMembers", "MemberCode", c => c.String(maxLength: 30));
            CreateIndex("dbo.ConnectMembers", "MemberCode", unique: true, name: "MemberCode");
        }
    }
}
