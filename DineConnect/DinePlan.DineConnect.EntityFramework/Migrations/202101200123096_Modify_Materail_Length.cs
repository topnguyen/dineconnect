namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Materail_Length : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Materials", "MaterialName", c => c.String(nullable: false, maxLength: 70));
            AlterColumn("dbo.Materials", "MaterialPetName", c => c.String(nullable: false, maxLength: 70));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Materials", "MaterialPetName", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Materials", "MaterialName", c => c.String(nullable: false, maxLength: 50));
        }
    }
}
