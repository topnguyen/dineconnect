namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDemandPromotionAskReference : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DemandPromotions", "AskReference", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DemandPromotions", "AskReference");
        }
    }
}
