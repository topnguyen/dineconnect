namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_MemberCode_Nullable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MemberCards", "MemberRefId", "dbo.SwipeMembers");
            DropForeignKey("dbo.SwipeCardLedgers", "MemberRefId", "dbo.SwipeMembers");
            DropIndex("dbo.MemberCards", new[] { "MemberRefId" });
            DropIndex("dbo.SwipeCardLedgers", new[] { "MemberRefId" });
            AlterColumn("dbo.MemberCards", "MemberRefId", c => c.Int());
            AlterColumn("dbo.SwipeCardLedgers", "MemberRefId", c => c.Int());
            CreateIndex("dbo.MemberCards", "MemberRefId");
            CreateIndex("dbo.SwipeCardLedgers", "MemberRefId");
            AddForeignKey("dbo.MemberCards", "MemberRefId", "dbo.SwipeMembers", "Id");
            AddForeignKey("dbo.SwipeCardLedgers", "MemberRefId", "dbo.SwipeMembers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SwipeCardLedgers", "MemberRefId", "dbo.SwipeMembers");
            DropForeignKey("dbo.MemberCards", "MemberRefId", "dbo.SwipeMembers");
            DropIndex("dbo.SwipeCardLedgers", new[] { "MemberRefId" });
            DropIndex("dbo.MemberCards", new[] { "MemberRefId" });
            AlterColumn("dbo.SwipeCardLedgers", "MemberRefId", c => c.Int(nullable: false));
            AlterColumn("dbo.MemberCards", "MemberRefId", c => c.Int(nullable: false));
            CreateIndex("dbo.SwipeCardLedgers", "MemberRefId");
            CreateIndex("dbo.MemberCards", "MemberRefId");
            AddForeignKey("dbo.SwipeCardLedgers", "MemberRefId", "dbo.SwipeMembers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.MemberCards", "MemberRefId", "dbo.SwipeMembers", "Id", cascadeDelete: true);
        }
    }
}
