namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_Swipe_Member : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.SwipeMembers", "MemberCode");
            DropTable("dbo.SwipeMembers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipeMember_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SwipeMembers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MemberCode = c.String(maxLength: 30),
                        Name = c.String(),
                        EmailId = c.String(),
                        AlternateEmailId = c.String(),
                        PhoneNumber = c.String(),
                        AlternatePhoneNumber = c.String(),
                        AddressLine1 = c.String(),
                        AddressLine2 = c.String(),
                        Locality = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Country = c.String(),
                        PostalCode = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipeMember_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.SwipeMembers", "MemberCode", unique: true, name: "MemberCode");
        }
    }
}
