namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmpinfoAltered3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmployeeInfos", "HireDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.EmployeeInfos", "WageType", c => c.String());
            AddColumn("dbo.EmployeeInfos", "SkillLevel", c => c.String());
            AddColumn("dbo.EmployeeInfos", "MaxWeeklyHours", c => c.Int(nullable: false));
            AddColumn("dbo.EmployeeInfos", "EmployeeId", c => c.Int(nullable: false));
            AddColumn("dbo.EmployeeInfos", "PunchId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmployeeInfos", "PunchId");
            DropColumn("dbo.EmployeeInfos", "EmployeeId");
            DropColumn("dbo.EmployeeInfos", "MaxWeeklyHours");
            DropColumn("dbo.EmployeeInfos", "SkillLevel");
            DropColumn("dbo.EmployeeInfos", "WageType");
            DropColumn("dbo.EmployeeInfos", "HireDate");
        }
    }
}
