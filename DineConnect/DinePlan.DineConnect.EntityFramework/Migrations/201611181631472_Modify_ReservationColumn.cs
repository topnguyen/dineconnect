namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_ReservationColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reservation", "RemainderMediums", c => c.String());
            DropColumn("dbo.Reservation", "RemainderMedieums");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reservation", "RemainderMedieums", c => c.String());
            DropColumn("dbo.Reservation", "RemainderMediums");
        }
    }
}
