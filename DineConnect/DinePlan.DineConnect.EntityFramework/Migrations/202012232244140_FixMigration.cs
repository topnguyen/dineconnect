namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Numerators", "IsApplyLuhanTicketNumber", c => c.Boolean(nullable: false));
            AddColumn("dbo.TicketTypes", "MenuItemId", c => c.Int());
            AddColumn("dbo.TicketTypes", "TicketTypeConfigurationId", c => c.Int());
            AddColumn("dbo.TicketTypes", "IsTaxIncluded", c => c.Boolean(nullable: false));
            AddColumn("dbo.TicketTypes", "IsPreOrder", c => c.Boolean(nullable: false));
            AddColumn("dbo.TicketTypes", "IsAllowZeroPriceProducts", c => c.Boolean(nullable: false));
            CreateIndex("dbo.TicketTypes", "MenuItemId");
            AddForeignKey("dbo.TicketTypes", "MenuItemId", "dbo.MenuItems", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TicketTypes", "MenuItemId", "dbo.MenuItems");
            DropIndex("dbo.TicketTypes", new[] { "MenuItemId" });
            DropColumn("dbo.TicketTypes", "IsAllowZeroPriceProducts");
            DropColumn("dbo.TicketTypes", "IsPreOrder");
            DropColumn("dbo.TicketTypes", "IsTaxIncluded");
            DropColumn("dbo.TicketTypes", "TicketTypeConfigurationId");
            DropColumn("dbo.TicketTypes", "MenuItemId");
            DropColumn("dbo.Numerators", "IsApplyLuhanTicketNumber");
        }
    }
}
