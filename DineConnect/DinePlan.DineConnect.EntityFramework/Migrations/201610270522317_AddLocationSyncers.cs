namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLocationSyncers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LocationSyncers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SyncerGuid = c.Guid(nullable: false),
                        LastSyncTime = c.DateTime(nullable: false),
                        LocationId = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                        Syncer_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .ForeignKey("dbo.Syncers", t => t.Syncer_Id)
                .Index(t => t.LocationId)
                .Index(t => t.Syncer_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LocationSyncers", "Syncer_Id", "dbo.Syncers");
            DropForeignKey("dbo.LocationSyncers", "LocationId", "dbo.Locations");
            DropIndex("dbo.LocationSyncers", new[] { "Syncer_Id" });
            DropIndex("dbo.LocationSyncers", new[] { "LocationId" });
            DropTable("dbo.LocationSyncers");
        }
    }
}
