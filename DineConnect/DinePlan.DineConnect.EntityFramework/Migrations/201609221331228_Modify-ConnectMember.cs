namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyConnectMember : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConnectMembers", "DefaultAddressRefId", c => c.Int());
            DropColumn("dbo.ConnectMembers", "Locality");
            DropColumn("dbo.ConnectMembers", "Address");
            DropColumn("dbo.ConnectMembers", "City");
            DropColumn("dbo.ConnectMembers", "State");
            DropColumn("dbo.ConnectMembers", "Country");
            DropColumn("dbo.ConnectMembers", "PostalCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ConnectMembers", "PostalCode", c => c.String());
            AddColumn("dbo.ConnectMembers", "Country", c => c.String());
            AddColumn("dbo.ConnectMembers", "State", c => c.String());
            AddColumn("dbo.ConnectMembers", "City", c => c.String());
            AddColumn("dbo.ConnectMembers", "Address", c => c.String());
            AddColumn("dbo.ConnectMembers", "Locality", c => c.String());
            DropColumn("dbo.ConnectMembers", "DefaultAddressRefId");
        }
    }
}
