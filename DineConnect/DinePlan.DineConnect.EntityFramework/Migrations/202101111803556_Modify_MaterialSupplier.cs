namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_MaterialSupplier : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SupplierMaterials", "SupplierMaterialAliasName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SupplierMaterials", "SupplierMaterialAliasName");
        }
    }
}
