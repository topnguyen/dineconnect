namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTillTransactionFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TillTransactions", "WorkPeriodId", c => c.Int(nullable: false));
            AddColumn("dbo.TillTransactions", "LocalWorkPeriodId", c => c.Int(nullable: false));
            AddColumn("dbo.TillTransactions", "LocationId", c => c.Int(nullable: false));
            AddColumn("dbo.TillTransactions", "User", c => c.String());
            AddColumn("dbo.TillTransactions", "TransactionTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.TillTransactions", "Status", c => c.Boolean(nullable: false));
            DropColumn("dbo.TillTransactions", "WorkTimeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TillTransactions", "WorkTimeId", c => c.Int(nullable: false));
            DropColumn("dbo.TillTransactions", "Status");
            DropColumn("dbo.TillTransactions", "TransactionTime");
            DropColumn("dbo.TillTransactions", "User");
            DropColumn("dbo.TillTransactions", "LocationId");
            DropColumn("dbo.TillTransactions", "LocalWorkPeriodId");
            DropColumn("dbo.TillTransactions", "WorkPeriodId");
        }
    }
}
