namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeliveryTicketsChange : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DeliveryOrders", "DeliveryTicket_Id", "dbo.DeliveryTickets");
            DropIndex("dbo.DeliveryOrders", new[] { "DeliveryTicket_Id" });
            RenameColumn(table: "dbo.DeliveryOrders", name: "DeliveryTicket_Id", newName: "DeliveryTicketId");
            AddColumn("dbo.DeliveryOrders", "MenuItemPortionId", c => c.Int(nullable: false));
            AddColumn("dbo.DeliveryTickets", "TicketNumber", c => c.String());
            AddColumn("dbo.DeliveryTickets", "LastUpdateTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.DeliveryTickets", "OrderType", c => c.Int(nullable: false));
            AddColumn("dbo.DeliveryTickets", "DeliveryTicketType", c => c.Int(nullable: false));
            AddColumn("dbo.DeliveryTickets", "Deliverer", c => c.String());
            AlterColumn("dbo.DeliveryOrders", "DeliveryTicketId", c => c.Int(nullable: false));
            CreateIndex("dbo.DeliveryOrders", "MenuItemPortionId");
            CreateIndex("dbo.DeliveryOrders", "DeliveryTicketId");
            AddForeignKey("dbo.DeliveryOrders", "MenuItemPortionId", "dbo.MenuItemPortions", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DeliveryOrders", "DeliveryTicketId", "dbo.DeliveryTickets", "Id", cascadeDelete: true);
            DropColumn("dbo.DeliveryOrders", "MenuItemId");
            DropColumn("dbo.DeliveryOrders", "MenuItemName");
            DropColumn("dbo.DeliveryOrders", "PortionName");
            DropColumn("dbo.DeliveryTickets", "LastOrderDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DeliveryTickets", "LastOrderDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.DeliveryOrders", "PortionName", c => c.String());
            AddColumn("dbo.DeliveryOrders", "MenuItemName", c => c.String());
            AddColumn("dbo.DeliveryOrders", "MenuItemId", c => c.Int(nullable: false));
            DropForeignKey("dbo.DeliveryOrders", "DeliveryTicketId", "dbo.DeliveryTickets");
            DropForeignKey("dbo.DeliveryOrders", "MenuItemPortionId", "dbo.MenuItemPortions");
            DropIndex("dbo.DeliveryOrders", new[] { "DeliveryTicketId" });
            DropIndex("dbo.DeliveryOrders", new[] { "MenuItemPortionId" });
            AlterColumn("dbo.DeliveryOrders", "DeliveryTicketId", c => c.Int());
            DropColumn("dbo.DeliveryTickets", "Deliverer");
            DropColumn("dbo.DeliveryTickets", "DeliveryTicketType");
            DropColumn("dbo.DeliveryTickets", "OrderType");
            DropColumn("dbo.DeliveryTickets", "LastUpdateTime");
            DropColumn("dbo.DeliveryTickets", "TicketNumber");
            DropColumn("dbo.DeliveryOrders", "MenuItemPortionId");
            RenameColumn(table: "dbo.DeliveryOrders", name: "DeliveryTicketId", newName: "DeliveryTicket_Id");
            CreateIndex("dbo.DeliveryOrders", "DeliveryTicket_Id");
            AddForeignKey("dbo.DeliveryOrders", "DeliveryTicket_Id", "dbo.DeliveryTickets", "Id");
        }
    }
}
