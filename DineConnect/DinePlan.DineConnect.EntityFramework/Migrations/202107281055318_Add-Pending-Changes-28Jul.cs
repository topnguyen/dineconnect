namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPendingChanges28Jul : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.MenuItemPortions", "PreparationTime", c => c.Int());
            //AddColumn("dbo.Orders", "BindState", c => c.String());
            //AddColumn("dbo.Orders", "BindStateValues", c => c.String());
            //AddColumn("dbo.Orders", "BindCompleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "BindCompleted");
            DropColumn("dbo.Orders", "BindStateValues");
            DropColumn("dbo.Orders", "BindState");
            DropColumn("dbo.MenuItemPortions", "PreparationTime");
        }
    }
}
