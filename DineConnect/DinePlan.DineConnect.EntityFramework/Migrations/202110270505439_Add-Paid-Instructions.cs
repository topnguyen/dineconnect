namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPaidInstructions : DbMigration
    {
        public override void  Up()
        {
            AddColumn("dbo.DeliveryTickets", "Paid", c => c.Boolean(nullable: false));
            AddColumn("dbo.DeliveryTickets", "TableNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DeliveryTickets", "TableNumber");
            DropColumn("dbo.DeliveryTickets", "Paid");
        }
    }
}
