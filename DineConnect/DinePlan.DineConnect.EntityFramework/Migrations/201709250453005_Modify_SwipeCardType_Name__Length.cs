namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_SwipeCardType_Name__Length : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.SwipeCardTypes", "UQ_CARDTYPENAME");
            AlterColumn("dbo.SwipeCardTypes", "Name", c => c.String(maxLength: 30));
            CreateIndex("dbo.SwipeCardTypes", "Name", unique: true, name: "UQ_CARDTYPENAME");
        }
        
        public override void Down()
        {
            DropIndex("dbo.SwipeCardTypes", "UQ_CARDTYPENAME");
            AlterColumn("dbo.SwipeCardTypes", "Name", c => c.String(maxLength: 20));
            CreateIndex("dbo.SwipeCardTypes", "Name", unique: true, name: "UQ_CARDTYPENAME");
        }
    }
}
