namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_NumberOfPax_MenuItem : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItemPortions", "NumberOfPax", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MenuItemPortions", "NumberOfPax");
        }
    }
}
