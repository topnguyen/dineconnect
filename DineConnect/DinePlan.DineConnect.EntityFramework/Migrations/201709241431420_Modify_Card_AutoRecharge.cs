namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Card_AutoRecharge : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SwipeCards", "IsAutoRechargeCard", c => c.Boolean(nullable: false));
            AddColumn("dbo.SwipeCardTypes", "IsAutoRechargeCard", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SwipeCardTypes", "IsAutoRechargeCard");
            DropColumn("dbo.SwipeCards", "IsAutoRechargeCard");
        }
    }
}
