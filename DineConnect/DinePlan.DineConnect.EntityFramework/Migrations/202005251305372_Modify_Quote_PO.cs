namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Quote_PO : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchaseOrders", "PoBasedOnCater", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PurchaseOrders", "PoBasedOnCater");
        }
    }
}
