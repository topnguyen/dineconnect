namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTriedtoPostContents : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PostDatas", "Tried", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PostDatas", "Tried");
        }
    }
}
