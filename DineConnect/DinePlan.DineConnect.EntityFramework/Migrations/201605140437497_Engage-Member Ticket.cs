namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EngageMemberTicket : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MemberPoints", "TicketNumber", c => c.String());
            DropColumn("dbo.MemberPoints", "TicketId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MemberPoints", "TicketId", c => c.Int(nullable: false));
            DropColumn("dbo.MemberPoints", "TicketNumber");
        }
    }
}
