namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeMaterialMappingChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MaterialMenuMappings", "LocationRefId", c => c.Int());
            CreateIndex("dbo.MaterialMenuMappings", "LocationRefId");
            AddForeignKey("dbo.MaterialMenuMappings", "LocationRefId", "dbo.Locations", "Id");
            AddColumn("dbo.MaterialGroupCategories", "AddOn", c => c.String());
            AddColumn("dbo.MaterialLocationWiseStocks", "IsActiveInLocation", c => c.Boolean(nullable: false, defaultValue: true));

            AddColumn("dbo.SupplierMaterials", "LocationRefId", c => c.Int());
            CreateIndex("dbo.SupplierMaterials", "LocationRefId");
            AddForeignKey("dbo.SupplierMaterials", "LocationRefId", "dbo.Locations", "Id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.MaterialMenuMappings", "LocationRefId", "dbo.Locations");
            DropIndex("dbo.MaterialMenuMappings", new[] { "LocationRefId" });
            DropColumn("dbo.MaterialMenuMappings", "LocationRefId");
            DropColumn("dbo.MaterialGroupCategories", "AddOn");
            DropColumn("dbo.MaterialLocationWiseStocks", "IsActiveInLocation");

            DropForeignKey("dbo.SupplierMaterials", "LocationRefId", "dbo.Locations");
            DropIndex("dbo.SupplierMaterials", new[] { "LocationRefId" });
            DropColumn("dbo.SupplierMaterials", "LocationRefId");
            DropColumn("dbo.MaterialLocationWiseStocks", "IsActiveInLocation");

        }
    }
}
