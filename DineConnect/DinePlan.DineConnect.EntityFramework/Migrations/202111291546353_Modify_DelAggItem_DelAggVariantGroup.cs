namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_DelAggItem_DelAggVariantGroup : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DelAggVariantGroups", "DelAggItem_Id", "dbo.DelAggItems");
            DropIndex("dbo.DelAggVariantGroups", new[] { "DelAggItem_Id" });
            DropColumn("dbo.DelAggVariantGroups", "DelAggItem_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DelAggVariantGroups", "DelAggItem_Id", c => c.Int());
            CreateIndex("dbo.DelAggVariantGroups", "DelAggItem_Id");
            AddForeignKey("dbo.DelAggVariantGroups", "DelAggItem_Id", "dbo.DelAggItems", "Id");
        }
    }
}
