namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Price_Uom_Digits : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UnitConversions", "Conversion", c => c.Decimal(nullable: false, precision: 18, scale: 7));
            AlterColumn("dbo.InterTransferDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.InterTransferReceivedDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.InvoiceDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.InvoiceDetails", "TotalAmount", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.InvoiceDetails", "DiscountAmount", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.InvoiceDetails", "TaxAmount", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.InvoiceDetails", "NetAmount", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.Invoices", "TotalDiscountAmount", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.Invoices", "TotalShipmentCharges", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.SupplierMaterials", "MaterialPrice", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.PurchaseOrderDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.PurchaseOrderDetails", "TotalAmount", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.PurchaseOrderDetails", "TaxAmount", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.PurchaseOrderDetails", "NetAmount", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.PurchaseOrderTaxDetails", "TaxValue", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.SalesInvoiceDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.SalesOrderDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 6));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SalesOrderDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.SalesInvoiceDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.PurchaseOrderTaxDetails", "TaxValue", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.PurchaseOrderDetails", "NetAmount", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.PurchaseOrderDetails", "TaxAmount", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.PurchaseOrderDetails", "TotalAmount", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.PurchaseOrderDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.SupplierMaterials", "MaterialPrice", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.Invoices", "TotalShipmentCharges", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.Invoices", "TotalDiscountAmount", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.InvoiceDetails", "NetAmount", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.InvoiceDetails", "TaxAmount", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.InvoiceDetails", "DiscountAmount", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.InvoiceDetails", "TotalAmount", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.InvoiceDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.InterTransferReceivedDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.InterTransferDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.UnitConversions", "Conversion", c => c.Decimal(nullable: false, precision: 18, scale: 6));
        }
    }
}
