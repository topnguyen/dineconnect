namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProductComboGroupToItem : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductComboItems", "GroupTag", c => c.String());
            AddColumn("dbo.ProductComboItems", "ButtonColor", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductComboItems", "ButtonColor");
            DropColumn("dbo.ProductComboItems", "GroupTag");
        }
    }
}
