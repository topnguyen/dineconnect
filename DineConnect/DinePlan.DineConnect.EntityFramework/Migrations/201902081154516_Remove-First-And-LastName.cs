namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveFirstAndLastName : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.EmployeeInfos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeCode = c.String(),
                        EmployeeName = c.String(),
                        Email = c.String(),
                        HomePhone = c.String(),
                        MobilePhone = c.String(),
                        HireDate = c.DateTime(nullable: false),
                        WageType = c.String(),
                        Wage = c.Int(nullable: false),
                        SkillLevel = c.String(),
                        MaxWeeklyHours = c.Int(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        PunchId = c.Int(nullable: false),
                        Notes = c.String(),
                        TenantId = c.Int(nullable: false),
                        UserId = c.Long(),
                        DinePlanUserId = c.Int(),
                        EmployeeDepartmentId = c.Int(),
                        IsActive = c.Boolean(nullable: false),
                        RawId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_EmployeeInfo_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            DropColumn("dbo.EmployeeInfos", "FirstName");
            DropColumn("dbo.EmployeeInfos", "LastName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EmployeeInfos", "LastName", c => c.String());
            AddColumn("dbo.EmployeeInfos", "FirstName", c => c.String());
            AlterTableAnnotations(
                "dbo.EmployeeInfos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeCode = c.String(),
                        EmployeeName = c.String(),
                        Email = c.String(),
                        HomePhone = c.String(),
                        MobilePhone = c.String(),
                        HireDate = c.DateTime(nullable: false),
                        WageType = c.String(),
                        Wage = c.Int(nullable: false),
                        SkillLevel = c.String(),
                        MaxWeeklyHours = c.Int(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        PunchId = c.Int(nullable: false),
                        Notes = c.String(),
                        TenantId = c.Int(nullable: false),
                        UserId = c.Long(),
                        DinePlanUserId = c.Int(),
                        EmployeeDepartmentId = c.Int(),
                        IsActive = c.Boolean(nullable: false),
                        RawId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_EmployeeInfo_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
        }
    }
}
