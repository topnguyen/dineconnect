namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Location_DayCloseRecursive : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "IsDayCloseRecursiveUptoDateAllowed", c => c.Boolean(nullable: false,defaultValue:true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "IsDayCloseRecursiveUptoDateAllowed");
        }
    }
}
