namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTransactionTypeAccountCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TransactionTypes", "AccountCode", c => c.String(maxLength: 50));
            AlterColumn("dbo.TransactionTypes", "Name", c => c.String(maxLength: 50));
            AlterColumn("dbo.PaymentTypes", "Name", c => c.String(maxLength: 50));
            AlterColumn("dbo.PaymentTypes", "AccountCode", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PaymentTypes", "AccountCode", c => c.String());
            AlterColumn("dbo.PaymentTypes", "Name", c => c.String());
            AlterColumn("dbo.TransactionTypes", "Name", c => c.String());
            DropColumn("dbo.TransactionTypes", "AccountCode");
        }
    }
}
