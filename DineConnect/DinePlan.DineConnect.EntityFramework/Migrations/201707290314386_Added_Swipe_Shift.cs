namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Swipe_Shift : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SwipePaymentTenders", "PaymentTypeId", "dbo.PaymentTypes");
            CreateTable(
                "dbo.SwipeShifts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ShiftStartTime = c.DateTime(nullable: false),
                        ShiftEndTime = c.DateTime(),
                        TenderOpenAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenderCredit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenderDebit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenderCloseAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ActualTenderClosing = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UserTenderEntered = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ExcessShortageStatus = c.String(),
                        ExcessShortageAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SwipeShiftTender",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ShiftRefId = c.Int(nullable: false),
                        ShiftInOutStatus = c.Int(nullable: false),
                        PaymentTypeId = c.Int(nullable: false),
                        ActualAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenderedAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SwipePaymentTypes", t => t.PaymentTypeId, cascadeDelete: true)
                .ForeignKey("dbo.SwipeShifts", t => t.ShiftRefId, cascadeDelete: true)
                .Index(t => t.ShiftRefId)
                .Index(t => t.PaymentTypeId);
            
            AddColumn("dbo.MemberCards", "ExpiryDate", c => c.DateTime());
            AddForeignKey("dbo.SwipePaymentTenders", "PaymentTypeId", "dbo.SwipePaymentTypes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SwipeShiftTender", "ShiftRefId", "dbo.SwipeShifts");
            DropForeignKey("dbo.SwipeShiftTender", "PaymentTypeId", "dbo.SwipePaymentTypes");
            DropForeignKey("dbo.SwipePaymentTenders", "PaymentTypeId", "dbo.SwipePaymentTypes");
            DropIndex("dbo.SwipeShiftTender", new[] { "PaymentTypeId" });
            DropIndex("dbo.SwipeShiftTender", new[] { "ShiftRefId" });
            DropColumn("dbo.MemberCards", "ExpiryDate");
            DropTable("dbo.SwipeShiftTender");
            DropTable("dbo.SwipeShifts");
            AddForeignKey("dbo.SwipePaymentTenders", "PaymentTypeId", "dbo.PaymentTypes", "Id", cascadeDelete: true);
        }
    }
}
