using System.Data.Entity.Migrations;
using DinePlan.DineConnect.EntityFramework.Repositories;
using DinePlan.DineConnect.Migrations.Seed;

namespace DinePlan.DineConnect.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DineConnect.EntityFramework.DineConnectDbContext>
    {
        public Configuration( )
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "DineConnect";
            CommandTimeout = 60 * 5;
            SetSqlGenerator("System.Data.SqlClient", new AddColumnIfNotExistsSqlGenerator());
            SetSqlGenerator("System.Data.SqlClient", new DropColumnIfExistsSqlGenerator());

        }

        protected override void Seed(DineConnect.EntityFramework.DineConnectDbContext context)
        {
            new InitialDbBuilder(context).Create();
        }
    }
}
