namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Location_DayCloseStkAdjustment_MaterialGroup_ParentId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "DayCloseSalesStockAdjustmentLocationRefId", c => c.Int());
            AddColumn("dbo.MaterialGroups", "Code", c => c.String(maxLength: 50));
            Sql("Update dbo.MaterialGroups Set Code = MaterialGroupName;");
            AddColumn("dbo.MaterialGroups", "ParentId", c => c.Int());
            CreateIndex("dbo.MaterialGroups", "ParentId");
            AddForeignKey("dbo.MaterialGroups", "ParentId", "dbo.MaterialGroups", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MaterialGroups", "ParentId", "dbo.MaterialGroups");
            DropIndex("dbo.MaterialGroups", new[] { "ParentId" });
            DropColumn("dbo.MaterialGroups", "ParentId");
            DropColumn("dbo.MaterialGroups", "Code");
            DropColumn("dbo.Locations", "DayCloseSalesStockAdjustmentLocationRefId");
        }
    }
}
