namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeFutureDateInformationApplied : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FutureDateInformations", "Applied", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FutureDateInformations", "Applied");
        }
    }
}
