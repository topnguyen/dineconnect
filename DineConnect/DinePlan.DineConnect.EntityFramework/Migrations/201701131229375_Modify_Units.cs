namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Units : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Materials", "IssueUnitId", c => c.Int(nullable: true));
            Sql("Update dbo.Materials Set IssueUnitId = DefaultUnitId");
            AlterColumn("dbo.Materials", "IssueUnitId", c => c.Int(nullable: false));
            CreateIndex("dbo.Materials", "IssueUnitId");
            AddForeignKey("dbo.Materials", "IssueUnitId", "dbo.Units", "Id");


            AddColumn("dbo.AdjustmentDetails", "UnitRefId", c => c.Int(nullable: true));
            Sql("UPDATE AdjustmentDetails SET AdjustmentDetails.UnitRefId = (SELECT Materials.DefaultUnitId from Materials where AdjustmentDetails.MaterialRefId=Materials.ID)");
            AlterColumn("dbo.AdjustmentDetails", "UnitRefId", c => c.Int(nullable: false));
            CreateIndex("dbo.AdjustmentDetails", "UnitRefId");
            AddForeignKey("dbo.AdjustmentDetails", "UnitRefId", "dbo.Units", "Id");


            AddColumn("dbo.IssueDetails", "UnitRefId", c => c.Int(nullable: true));
            Sql("UPDATE IssueDetails SET IssueDetails.UnitRefId = (SELECT Materials.DefaultUnitId from Materials where IssueDetails.MaterialRefId=Materials.ID)");
            AlterColumn("dbo.IssueDetails", "UnitRefId", c => c.Int(nullable: false));
            CreateIndex("dbo.IssueDetails", "UnitRefId");
            AddForeignKey("dbo.IssueDetails", "UnitRefId", "dbo.Units", "Id");

            AddColumn("dbo.PurchaseOrderDetails", "UnitRefId", c => c.Int(nullable: true));
            Sql("UPDATE PurchaseOrderDetails SET PurchaseOrderDetails.UnitRefId = (SELECT Materials.DefaultUnitId from Materials where PurchaseOrderDetails.MaterialRefId=Materials.ID)");
            Sql("UPDATE InwardDirectCreditDetails SET InwardDirectCreditDetails.UnitRefId = (SELECT Materials.DefaultUnitId from Materials where InwardDirectCreditDetails.MaterialRefId=Materials.ID)");
            AlterColumn("dbo.PurchaseOrderDetails", "UnitRefId", c => c.Int(nullable: false));
            CreateIndex("dbo.PurchaseOrderDetails", "UnitRefId");
            AddForeignKey("dbo.PurchaseOrderDetails", "UnitRefId", "dbo.Units", "Id");

            AddColumn("dbo.RequestDetails", "UnitRefId", c => c.Int(nullable: true));
            Sql("UPDATE RequestDetails SET RequestDetails.UnitRefId = (SELECT Materials.DefaultUnitId from Materials where RequestDetails.MaterialRefId=Materials.ID)");
            AlterColumn("dbo.RequestDetails", "UnitRefId", c => c.Int(nullable: false));
            CreateIndex("dbo.RequestDetails", "UnitRefId");
            AddForeignKey("dbo.RequestDetails", "UnitRefId", "dbo.Units", "Id");



            AddColumn("dbo.ReturnDetails", "UnitRefId", c => c.Int(nullable: true));
            Sql("UPDATE ReturnDetails SET ReturnDetails.UnitRefId = (SELECT Materials.DefaultUnitId from Materials where ReturnDetails.MaterialRefId=Materials.ID)");
            AlterColumn("dbo.ReturnDetails", "UnitRefId", c => c.Int(nullable: false));
            CreateIndex("dbo.ReturnDetails", "UnitRefId");
            AddForeignKey("dbo.ReturnDetails", "UnitRefId", "dbo.Units", "Id");

            
            CreateIndex("dbo.InvoiceDetails", "UnitRefId");
            CreateIndex("dbo.Invoices", "SupplierRefId");
            CreateIndex("dbo.InwardDirectCreditDetails", "UnitRefId");
            CreateIndex("dbo.IssueRecipeDetails", "UnitRefId");
            CreateIndex("dbo.MaterialRecipeTypes", "MaterailRefId");
            CreateIndex("dbo.ProductionDetails", "UnitRefId");
            CreateIndex("dbo.PurchaseOrderTaxDetails", "TaxRefId");
            CreateIndex("dbo.PurchaseReturnDetails", "UnitRefId");
            CreateIndex("dbo.RequestRecipeDetails", "UnitRefId");
            
            CreateIndex("dbo.YieldInput", "UnitRefId");
            CreateIndex("dbo.YieldOutput", "UnitRefId");

            AddForeignKey("dbo.Invoices", "SupplierRefId", "dbo.Suppliers", "Id");
            AddForeignKey("dbo.InvoiceDetails", "UnitRefId", "dbo.Units", "Id");
            AddForeignKey("dbo.InwardDirectCreditDetails", "UnitRefId", "dbo.Units", "Id");
            AddForeignKey("dbo.IssueRecipeDetails", "UnitRefId", "dbo.Units", "Id");
            AddForeignKey("dbo.MaterialRecipeTypes", "MaterailRefId", "dbo.Materials", "Id");
            AddForeignKey("dbo.ProductionDetails", "UnitRefId", "dbo.Units", "Id");
            AddForeignKey("dbo.PurchaseOrderTaxDetails", "TaxRefId", "dbo.Taxes", "Id");
            AddForeignKey("dbo.PurchaseReturnDetails", "UnitRefId", "dbo.Units", "Id");
            AddForeignKey("dbo.RequestRecipeDetails", "UnitRefId", "dbo.Units", "Id");
            
            AddForeignKey("dbo.YieldInput", "UnitRefId", "dbo.Units", "Id");
            AddForeignKey("dbo.YieldOutput", "UnitRefId", "dbo.Units", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.YieldOutput", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.YieldInput", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.ReturnDetails", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.RequestRecipeDetails", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.RequestDetails", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.PurchaseReturnDetails", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.PurchaseOrderTaxDetails", "TaxRefId", "dbo.Taxes");
            DropForeignKey("dbo.PurchaseOrderDetails", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.ProductionDetails", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.MaterialRecipeTypes", "MaterailRefId", "dbo.Materials");
            DropForeignKey("dbo.IssueRecipeDetails", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.IssueDetails", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.InwardDirectCreditDetails", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.InvoiceDetails", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.Invoices", "SupplierRefId", "dbo.Suppliers");
            DropForeignKey("dbo.Materials", "IssueUnitId", "dbo.Units");
            DropForeignKey("dbo.AdjustmentDetails", "UnitRefId", "dbo.Units");
            DropIndex("dbo.YieldOutput", new[] { "UnitRefId" });
            DropIndex("dbo.YieldInput", new[] { "UnitRefId" });
            DropIndex("dbo.ReturnDetails", new[] { "UnitRefId" });
            DropIndex("dbo.RequestRecipeDetails", new[] { "UnitRefId" });
            DropIndex("dbo.RequestDetails", new[] { "UnitRefId" });
            DropIndex("dbo.PurchaseReturnDetails", new[] { "UnitRefId" });
            DropIndex("dbo.PurchaseOrderTaxDetails", new[] { "TaxRefId" });
            DropIndex("dbo.PurchaseOrderDetails", new[] { "UnitRefId" });
            DropIndex("dbo.ProductionDetails", new[] { "UnitRefId" });
            DropIndex("dbo.MaterialRecipeTypes", new[] { "MaterailRefId" });
            DropIndex("dbo.IssueRecipeDetails", new[] { "UnitRefId" });
            DropIndex("dbo.IssueDetails", new[] { "UnitRefId" });
            DropIndex("dbo.InwardDirectCreditDetails", new[] { "UnitRefId" });
            DropIndex("dbo.Invoices", new[] { "SupplierRefId" });
            DropIndex("dbo.InvoiceDetails", new[] { "UnitRefId" });
            DropIndex("dbo.Materials", new[] { "IssueUnitId" });
            DropIndex("dbo.AdjustmentDetails", new[] { "UnitRefId" });
            DropColumn("dbo.ReturnDetails", "UnitRefId");
            DropColumn("dbo.RequestDetails", "UnitRefId");
            DropColumn("dbo.PurchaseOrderDetails", "UnitRefId");
            DropColumn("dbo.IssueDetails", "UnitRefId");
            DropColumn("dbo.Materials", "IssueUnitId");
            DropColumn("dbo.AdjustmentDetails", "UnitRefId");
        }
    }
}
