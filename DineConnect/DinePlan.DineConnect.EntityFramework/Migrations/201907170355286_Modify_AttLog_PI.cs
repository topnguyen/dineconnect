namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_AttLog_PI : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AttendanceLogs", "AttendanceMachineLocationRefId", "dbo.AttendanceMachineLocations");
            DropIndex("dbo.AttendanceLogs", new[] { "AttendanceMachineLocationRefId" });
            AddColumn("dbo.AttendanceLogs", "LocationRefId", c => c.Int(nullable: false));
            AddColumn("dbo.AttendanceLogs", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.PersonalInformations", "DinePlanUserId", c => c.Int());
            AlterColumn("dbo.AttendanceLogs", "AttendanceMachineLocationRefId", c => c.Int());
            CreateIndex("dbo.AttendanceLogs", "LocationRefId");
            CreateIndex("dbo.AttendanceLogs", "AttendanceMachineLocationRefId");
            CreateIndex("dbo.PersonalInformations", "DinePlanUserId");
            AddForeignKey("dbo.AttendanceLogs", "LocationRefId", "dbo.Locations", "Id");
            AddForeignKey("dbo.PersonalInformations", "DinePlanUserId", "dbo.DinePlanUsers", "Id");
            AddForeignKey("dbo.AttendanceLogs", "AttendanceMachineLocationRefId", "dbo.AttendanceMachineLocations", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AttendanceLogs", "AttendanceMachineLocationRefId", "dbo.AttendanceMachineLocations");
            DropForeignKey("dbo.PersonalInformations", "DinePlanUserId", "dbo.DinePlanUsers");
            DropForeignKey("dbo.AttendanceLogs", "LocationRefId", "dbo.Locations");
            DropIndex("dbo.PersonalInformations", new[] { "DinePlanUserId" });
            DropIndex("dbo.AttendanceLogs", new[] { "AttendanceMachineLocationRefId" });
            DropIndex("dbo.AttendanceLogs", new[] { "LocationRefId" });
            AlterColumn("dbo.AttendanceLogs", "AttendanceMachineLocationRefId", c => c.Int(nullable: false));
            DropColumn("dbo.PersonalInformations", "DinePlanUserId");
            DropColumn("dbo.AttendanceLogs", "Deleted");
            DropColumn("dbo.AttendanceLogs", "LocationRefId");
            CreateIndex("dbo.AttendanceLogs", "AttendanceMachineLocationRefId");
            AddForeignKey("dbo.AttendanceLogs", "AttendanceMachineLocationRefId", "dbo.AttendanceMachineLocations", "Id", cascadeDelete: true);
        }
    }
}
