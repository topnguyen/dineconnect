namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLocationMenuItem : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItems", "RefLocation", c => c.Int(nullable: false));

        }

        public override void Down()
        {
            DropColumn("dbo.MenuItems", "RefLocation");
        }
    }
}
