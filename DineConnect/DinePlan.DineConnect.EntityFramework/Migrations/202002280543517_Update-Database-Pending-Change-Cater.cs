namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDatabasePendingChangeCater : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.DvrDetails",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            TenantId = c.Int(nullable: false),
            //            LocationId = c.Int(nullable: false),
            //            DvrUrl = c.String(),
            //            DvrPort = c.String(),
            //            DvrUsername = c.String(),
            //            DvrPassword = c.String(),
            //            CameraName = c.String(),
            //            DvrTypeId = c.Int(nullable: false),
            //            CreationTime = c.DateTime(nullable: false),
            //            CreatorUserId = c.Long(),
            //        },
            //    annotations: new Dictionary<string, object>
            //    {
            //        { "DynamicFilter_DvrDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
            //    })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
            //    .Index(t => t.LocationId);
            
            //AddColumn("dbo.Alerts", "AlertDate", c => c.DateTime(nullable: false, storeType: "date"));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DvrDetails", "LocationId", "dbo.Locations");
            DropIndex("dbo.DvrDetails", new[] { "LocationId" });
            DropColumn("dbo.Alerts", "AlertDate");
            DropTable("dbo.DvrDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DvrDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
