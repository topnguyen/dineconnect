namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTransactionNote : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TicketTransactions", "TransactionNote", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TicketTransactions", "TransactionNote");
        }
    }
}
