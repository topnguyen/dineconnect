namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddConnectPaymentTypeTicketDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentTypes", "Group", c => c.String());
            AddColumn("dbo.Tickets", "InvoiceNo", c => c.String());
            AddColumn("dbo.Tickets", "FullTaxInvoiceDetails", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "FullTaxInvoiceDetails");
            DropColumn("dbo.Tickets", "InvoiceNo");
            DropColumn("dbo.PaymentTypes", "Group");
        }
    }
}
