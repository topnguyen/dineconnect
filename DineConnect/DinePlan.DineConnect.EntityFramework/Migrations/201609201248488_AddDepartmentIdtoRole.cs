namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDepartmentIdtoRole : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DinePlanUserRoles", "DepartmentId", c => c.Int());
            CreateIndex("dbo.DinePlanUserRoles", "DepartmentId");
            AddForeignKey("dbo.DinePlanUserRoles", "DepartmentId", "dbo.Departments", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DinePlanUserRoles", "DepartmentId", "dbo.Departments");
            DropIndex("dbo.DinePlanUserRoles", new[] { "DepartmentId" });
            DropColumn("dbo.DinePlanUserRoles", "DepartmentId");
        }
    }
}
