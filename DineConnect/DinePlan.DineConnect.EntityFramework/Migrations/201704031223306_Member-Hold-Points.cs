namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MemberHoldPoints : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MemberPoints", "OnHold", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MemberPoints", "OnHold");
        }
    }
}
