namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class EngageVoucher : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GiftVouchers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VoucherId = c.String(maxLength: 30),
                        GiftVoucherTypeId = c.Int(nullable: false),
                        Claimed = c.Boolean(nullable: false),
                        TicketId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_GiftVoucher_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_GiftVoucher_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GiftVoucherTypes", t => t.GiftVoucherTypeId, cascadeDelete: true)
                .Index(t => t.VoucherId, unique: true, name: "VoucherId")
                .Index(t => t.GiftVoucherTypeId);
            
            CreateTable(
                "dbo.GiftVoucherTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Locations = c.String(),
                        Name = c.String(),
                        Type = c.Int(nullable: false),
                        VoucherFormat = c.String(),
                        VoucherCount = c.Int(nullable: false),
                        StartingNumber = c.Int(nullable: false),
                        VoucherValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ValidTill = c.DateTime(nullable: false),
                        IsPointsApplicable = c.Boolean(nullable: false),
                        IsRepeatApplicable = c.Boolean(nullable: false),
                        RepeatCount = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_GiftVoucherType_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_GiftVoucherType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GiftVouchers", "GiftVoucherTypeId", "dbo.GiftVoucherTypes");
            DropIndex("dbo.GiftVouchers", new[] { "GiftVoucherTypeId" });
            DropIndex("dbo.GiftVouchers", "VoucherId");
            DropTable("dbo.GiftVoucherTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_GiftVoucherType_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_GiftVoucherType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.GiftVouchers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_GiftVoucher_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_GiftVoucher_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
