namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_EmployeeCard_Info : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MemberCards", "EmployeeRefId", c => c.Int());
            AddColumn("dbo.SwipeCards", "IsEmployeeCard", c => c.Boolean(nullable: false));
            AddColumn("dbo.SwipeCardTypes", "IsEmployeeCard", c => c.Boolean(nullable: false));
            CreateIndex("dbo.MemberCards", "EmployeeRefId");
            AddForeignKey("dbo.MemberCards", "EmployeeRefId", "dbo.EmployeeInfos", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MemberCards", "EmployeeRefId", "dbo.EmployeeInfos");
            DropIndex("dbo.MemberCards", new[] { "EmployeeRefId" });
            DropColumn("dbo.SwipeCardTypes", "IsEmployeeCard");
            DropColumn("dbo.SwipeCards", "IsEmployeeCard");
            DropColumn("dbo.MemberCards", "EmployeeRefId");
        }
    }
}
