namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_InterTransfer_Adjustment_ReturnBack : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InterTransfers", "AdjustmentRefId", c => c.Int());
            AddColumn("dbo.InterTransfers", "InterTransferRefId", c => c.Int());
            AddColumn("dbo.InterTransferReceivedDetails", "ReturnQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AddColumn("dbo.InterTransferReceivedDetails", "AdjustmentMode", c => c.String());
            AddColumn("dbo.InterTransferReceivedDetails", "AdjustmentQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            CreateIndex("dbo.InterTransfers", "AdjustmentRefId");
            CreateIndex("dbo.InterTransfers", "InterTransferRefId");
            AddForeignKey("dbo.InterTransfers", "AdjustmentRefId", "dbo.Adjustments", "Id");
            AddForeignKey("dbo.InterTransfers", "InterTransferRefId", "dbo.InterTransfers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InterTransfers", "InterTransferRefId", "dbo.InterTransfers");
            DropForeignKey("dbo.InterTransfers", "AdjustmentRefId", "dbo.Adjustments");
            DropIndex("dbo.InterTransfers", new[] { "InterTransferRefId" });
            DropIndex("dbo.InterTransfers", new[] { "AdjustmentRefId" });
            DropColumn("dbo.InterTransferReceivedDetails", "AdjustmentQty");
            DropColumn("dbo.InterTransferReceivedDetails", "AdjustmentMode");
            DropColumn("dbo.InterTransferReceivedDetails", "ReturnQty");
            DropColumn("dbo.InterTransfers", "InterTransferRefId");
            DropColumn("dbo.InterTransfers", "AdjustmentRefId");
        }
    }
}
