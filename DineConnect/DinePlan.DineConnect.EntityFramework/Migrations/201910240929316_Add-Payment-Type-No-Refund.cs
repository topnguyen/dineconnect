namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPaymentTypeNoRefund : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentTypes", "NoRefund", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaymentTypes", "NoRefund");
        }
    }
}
