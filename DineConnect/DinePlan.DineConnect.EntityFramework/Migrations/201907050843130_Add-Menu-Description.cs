namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMenuDescription : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItemDescriptions", "ProductName", c => c.String());
            DropColumn("dbo.MenuItemDescriptions", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MenuItemDescriptions", "Name", c => c.String());
            DropColumn("dbo.MenuItemDescriptions", "ProductName");
        }
    }
}
