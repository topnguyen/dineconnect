namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Tally_Category_Location_Supplier : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "TallyCompanyName", c => c.String());
            AddColumn("dbo.Suppliers", "TallySupplierName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Suppliers", "TallySupplierName");
            DropColumn("dbo.Locations", "TallyCompanyName");
        }
    }
}
