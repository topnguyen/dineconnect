namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Add_CaterQuotationDetail_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CaterQuotationDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HeaderId = c.Int(nullable: false),
                        GroupName = c.String(),
                        ProductName = c.String(),
                        MenuItemPortionId = c.Int(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Taxes = c.String(),
                        Note = c.String(),
                        Tags = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CaterQuotationDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CaterQuotationDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CaterQuotationHeaders", t => t.HeaderId, cascadeDelete: true)
                .ForeignKey("dbo.MenuItemPortions", t => t.MenuItemPortionId, cascadeDelete: true)
                .Index(t => t.HeaderId)
                .Index(t => t.MenuItemPortionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CaterQuotationDetails", "MenuItemPortionId", "dbo.MenuItemPortions");
            DropForeignKey("dbo.CaterQuotationDetails", "HeaderId", "dbo.CaterQuotationHeaders");
            DropIndex("dbo.CaterQuotationDetails", new[] { "MenuItemPortionId" });
            DropIndex("dbo.CaterQuotationDetails", new[] { "HeaderId" });
            DropTable("dbo.CaterQuotationDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CaterQuotationDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CaterQuotationDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
