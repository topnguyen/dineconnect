namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_CanIssueCardWithOutMember : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SwipeCards", "CanIssueWithOutMember", c => c.Boolean(nullable: false));
            AddColumn("dbo.SwipeCardTypes", "CanIssueWithOutMember", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SwipeCardTypes", "CanIssueWithOutMember");
            DropColumn("dbo.SwipeCards", "CanIssueWithOutMember");
        }
    }
}
