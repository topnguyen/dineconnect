namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAddCurrencyInLocation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "Currency", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "Currency");
        }
    }
}
