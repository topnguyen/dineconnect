namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_SwipeMembers : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.SwipeMembers", "LastVisitDate");
            DropColumn("dbo.SwipeMembers", "DefaultAddressRefId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SwipeMembers", "DefaultAddressRefId", c => c.Int());
            AddColumn("dbo.SwipeMembers", "LastVisitDate", c => c.DateTime());
        }
    }
}
