namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_FullTaxInvoiceCopyCount_tabl : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FullTaxInvoiceCopyCounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FullTaxInvoiceId = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FullTaxInvoices", t => t.FullTaxInvoiceId, cascadeDelete: true)
                .Index(t => t.FullTaxInvoiceId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FullTaxInvoiceCopyCounts", "FullTaxInvoiceId", "dbo.FullTaxInvoices");
            DropIndex("dbo.FullTaxInvoiceCopyCounts", new[] { "FullTaxInvoiceId" });
            DropTable("dbo.FullTaxInvoiceCopyCounts");
        }
    }
}
