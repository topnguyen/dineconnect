namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyReturn : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Returns", "MaterialGroupCategoryRefId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Returns", "MaterialGroupCategoryRefId", c => c.Int(nullable: false));
        }
    }
}
