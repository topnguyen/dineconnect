namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddReturnTicketTypeNumerator : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TicketTypes", "ReturnNumeratorId", c => c.Int());
            CreateIndex("dbo.TicketTypes", "ReturnNumeratorId");
            AddForeignKey("dbo.TicketTypes", "ReturnNumeratorId", "dbo.Numerators", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TicketTypes", "ReturnNumeratorId", "dbo.Numerators");
            DropIndex("dbo.TicketTypes", new[] { "ReturnNumeratorId" });
            DropColumn("dbo.TicketTypes", "ReturnNumeratorId");
        }
    }
}
