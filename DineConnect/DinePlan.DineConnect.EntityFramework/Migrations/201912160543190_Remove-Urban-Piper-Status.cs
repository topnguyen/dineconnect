namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveUrbanPiperStatus : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ExternalDeliveryTickets", "Accepted");
            DropColumn("dbo.ExternalDeliveryTickets", "Completed");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ExternalDeliveryTickets", "Completed", c => c.Boolean(nullable: false));
            AddColumn("dbo.ExternalDeliveryTickets", "Accepted", c => c.Boolean(nullable: false));
        }
    }
}
