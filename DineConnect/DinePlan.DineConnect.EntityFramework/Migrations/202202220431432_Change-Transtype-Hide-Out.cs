namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeTranstypeHideOut : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TransactionTypes", "HideOnReport", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TransactionTypes", "HideOnReport");
        }
    }
}
