namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Code_to_locationTag_LocationGroup_tbls : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LocationGroups", "Code", c => c.String());
            AddColumn("dbo.LocationTags", "Code", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.LocationTags", "Code");
            DropColumn("dbo.LocationGroups", "Code");
        }
    }
}
