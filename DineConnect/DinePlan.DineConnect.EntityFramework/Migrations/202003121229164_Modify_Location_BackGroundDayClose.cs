namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Location_BackGroundDayClose : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "DoesDayCloseRunInBackGround", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "DoesDayCloseRunInBackGround");
        }
    }
}
