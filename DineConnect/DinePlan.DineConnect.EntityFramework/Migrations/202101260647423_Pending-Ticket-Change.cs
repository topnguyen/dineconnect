namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PendingTicketChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "ExternalProcessed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "ExternalProcessed");
        }
    }
}
