// <auto-generated />
namespace DinePlan.DineConnect.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddMembershipTierIdtoConnectMember : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddMembershipTierIdtoConnectMember));
        
        string IMigrationMetadata.Id
        {
            get { return "202109301621446_Add-MembershipTierId-to-ConnectMember"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
