namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLocationIdtoOrderTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "Location_Id", c => c.Int());
            CreateIndex("dbo.Orders", "Location_Id");
            AddForeignKey("dbo.Orders", "Location_Id", "dbo.Locations", "Id");

            Sql("UPDATE ORDERS SET ORDERS.Location_Id=(SELECT t.LocationId from dbo.Tickets t where t.Id=ORDERS.TicketId)");
            Sql("UPDATE ORDERS SET ORDERS.DEPARTMENTNAME=(SELECT t.DEPARTMENTNAME from dbo.Tickets t where t.Id=ORDERS.TicketId)");

        }

        public override void Down()
        {
            DropForeignKey("dbo.Orders", "Location_Id", "dbo.Locations");
            DropIndex("dbo.Orders", new[] { "Location_Id" });
            DropColumn("dbo.Orders", "Location_Id");
        }
    }
}
