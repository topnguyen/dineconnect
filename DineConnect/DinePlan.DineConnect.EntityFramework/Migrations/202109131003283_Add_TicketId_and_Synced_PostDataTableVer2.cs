namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_TicketId_and_Synced_PostDataTableVer2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PostDatas", "Synced", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.PostDatas", "SyncedId", c => c.Int(nullable: false, defaultValue: 0));
        }

        public override void Down()
        {
            DropColumn("dbo.PostDatas", "Synced");
            DropColumn("dbo.PostDatas", "SyncedId");
        }
    }
}
