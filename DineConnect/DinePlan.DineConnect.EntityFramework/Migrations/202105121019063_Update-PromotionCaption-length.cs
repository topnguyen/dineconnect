namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePromotionCaptionlength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DemandPromotions", "ButtonCaption", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DemandPromotions", "ButtonCaption", c => c.String(maxLength: 50));
        }
    }
}
