namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProductJoins : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductComboGroups", "ProductComboId", "dbo.ProductComboes");
            DropIndex("dbo.ProductComboGroups", new[] { "ProductComboId" });
            CreateTable(
                "dbo.ProductComboJoins",
                c => new
                    {
                        ProductComboId = c.Int(nullable: false),
                        ProductComboGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductComboId, t.ProductComboGroupId })
                .ForeignKey("dbo.ProductComboes", t => t.ProductComboId, cascadeDelete: true)
                .ForeignKey("dbo.ProductComboGroups", t => t.ProductComboGroupId, cascadeDelete: true)
                .Index(t => t.ProductComboId)
                .Index(t => t.ProductComboGroupId);

            Sql("insert into ProductComboJoins(ProductComboId, ProductComboGroupId) select ProductComboId,Id from ProductComboGroups;");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductComboJoins", "ProductComboGroupId", "dbo.ProductComboGroups");
            DropForeignKey("dbo.ProductComboJoins", "ProductComboId", "dbo.ProductComboes");
            DropIndex("dbo.ProductComboJoins", new[] { "ProductComboGroupId" });
            DropIndex("dbo.ProductComboJoins", new[] { "ProductComboId" });
            DropTable("dbo.ProductComboJoins");
            CreateIndex("dbo.ProductComboGroups", "ProductComboId");
            AddForeignKey("dbo.ProductComboGroups", "ProductComboId", "dbo.ProductComboes", "Id", cascadeDelete: true);
        }
    }
}
