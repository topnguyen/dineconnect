namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPortiontoComboItem : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductComboItems", "MenuItemPortionId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductComboItems", "MenuItemPortionId");
        }
    }
}
