namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddPendingMemberEmailChange : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EngageMemberAccounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        MemberId = c.Int(nullable: false),
                        Number = c.String(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        AccountTypeId = c.Int(nullable: false),
                        Balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MemberAccount_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MemberAccount_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EngageAccountTypes", t => t.AccountTypeId, cascadeDelete: true)
                .Index(t => t.AccountTypeId);
            
            CreateTable(
                "dbo.EngageAccountTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Description = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MemberAccountType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EngageMemberAccountTransactions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        AccountId = c.Int(nullable: false),
                        PreviousBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionalUnitCount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MemberAccountTransaction_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MemberAccountTransaction_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EngageMemberAccounts", t => t.AccountId, cascadeDelete: true)
                .Index(t => t.AccountId);
            
            CreateTable(
                "dbo.EngageMemberServiceInfoTemplates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        ServiceInfoTypeId = c.Int(nullable: false),
                        Template = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        StoredAsPlainText = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MemberServiceInfoTemplate_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MemberServiceInfoTemplate_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EngageMemberServiceInfoType", t => t.ServiceInfoTypeId, cascadeDelete: true)
                .Index(t => t.ServiceInfoTypeId);
            
            CreateTable(
                "dbo.EngageMemberServiceInfoType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false),
                        Description = c.String(maxLength: 100),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MemberServiceInfoType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EngageMemberships",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        TierId = c.Int(nullable: false),
                        ApplicationDate = c.DateTime(nullable: false),
                        ExpiryDate = c.DateTime(),
                        LocationId = c.Int(),
                        Active = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Membership_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Membership_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId)
                .ForeignKey("dbo.EngageMembershipTiers", t => t.TierId, cascadeDelete: true)
                .Index(t => t.TierId)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.EngageMembershipTiers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        TierLevel = c.Int(nullable: false),
                        Description = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AmountToEarnOnePoint = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RewardingPoints = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RewardingVoucherCount = c.Int(nullable: false),
                        CashPaymentDiscount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreditCardPaymentDiscount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TextOfBenefit = c.String(nullable: false),
                        TextOfConditions = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MembershipTier_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MembershipTier_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EngageMemberships", "TierId", "dbo.EngageMembershipTiers");
            DropForeignKey("dbo.EngageMemberships", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.EngageMemberServiceInfoTemplates", "ServiceInfoTypeId", "dbo.EngageMemberServiceInfoType");
            DropForeignKey("dbo.EngageMemberAccountTransactions", "AccountId", "dbo.EngageMemberAccounts");
            DropForeignKey("dbo.EngageMemberAccounts", "AccountTypeId", "dbo.EngageAccountTypes");
            DropIndex("dbo.EngageMemberships", new[] { "LocationId" });
            DropIndex("dbo.EngageMemberships", new[] { "TierId" });
            DropIndex("dbo.EngageMemberServiceInfoTemplates", new[] { "ServiceInfoTypeId" });
            DropIndex("dbo.EngageMemberAccountTransactions", new[] { "AccountId" });
            DropIndex("dbo.EngageMemberAccounts", new[] { "AccountTypeId" });
            DropTable("dbo.EngageMembershipTiers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MembershipTier_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MembershipTier_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EngageMemberships",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Membership_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Membership_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EngageMemberServiceInfoTemplates",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MemberServiceInfoTemplate_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MemberServiceInfoTemplate_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EngageMemberServiceInfoType",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MemberServiceInfoType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EngageMemberAccountTransactions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MemberAccountTransaction_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MemberAccountTransaction_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EngageAccountTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MemberAccountType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EngageMemberAccounts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MemberAccount_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MemberAccount_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
