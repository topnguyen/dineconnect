namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_ScreenMenuCategorySchedules : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ScreenMenuCategorySchedules", "StartDate", c => c.DateTime());
            AlterColumn("dbo.ScreenMenuCategorySchedules", "EndDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ScreenMenuCategorySchedules", "EndDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.ScreenMenuCategorySchedules", "StartDate", c => c.DateTime(nullable: false));
        }
    }
}
