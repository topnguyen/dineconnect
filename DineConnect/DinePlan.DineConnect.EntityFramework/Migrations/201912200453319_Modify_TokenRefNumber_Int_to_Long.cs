namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_TokenRefNumber_Int_to_Long : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Adjustments", "TokenRefNumber", c => c.Long(nullable: false));
            AlterColumn("dbo.MenuItemWastages", "TokenRefNumber", c => c.Long(nullable: false));
            AlterColumn("dbo.Returns", "TokenRefNumber", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Returns", "TokenRefNumber", c => c.Int(nullable: false));
            AlterColumn("dbo.MenuItemWastages", "TokenRefNumber", c => c.Int(nullable: false));
            AlterColumn("dbo.Adjustments", "TokenRefNumber", c => c.Int(nullable: false));
        }
    }
}
