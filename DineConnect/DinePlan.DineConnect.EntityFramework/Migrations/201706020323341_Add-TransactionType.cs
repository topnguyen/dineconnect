namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTransactionType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItems", "TransactionTypeId", c => c.Int());
            CreateIndex("dbo.MenuItems", "TransactionTypeId");
            AddForeignKey("dbo.MenuItems", "TransactionTypeId", "dbo.TransactionTypes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MenuItems", "TransactionTypeId", "dbo.TransactionTypes");
            DropIndex("dbo.MenuItems", new[] { "TransactionTypeId" });
            DropColumn("dbo.MenuItems", "TransactionTypeId");
        }
    }
}
