namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDepartmentInformationInWorkPeriod : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkPeriods", "DepartmentTicketInformations", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.WorkPeriods", "DepartmentTicketInformations");
        }
    }
}
