namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_ScreenMenuCategorySchedules : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ConnectCardTypeCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 30),
                        Code = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConnectCardTypeCategory_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConnectCardTypeCategory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ScreenMenuCategorySchedules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        ScreenMenuCategoryId = c.Int(nullable: false),
                        StartHour = c.Int(nullable: false),
                        StartMinute = c.Int(nullable: false),
                        EndHour = c.Int(nullable: false),
                        EndMinute = c.Int(nullable: false),
                        MonthDays = c.String(),
                        Days = c.String(maxLength: 100),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ScreenMenuCategories", t => t.ScreenMenuCategoryId, cascadeDelete: true)
                .Index(t => t.ScreenMenuCategoryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScreenMenuCategorySchedules", "ScreenMenuCategoryId", "dbo.ScreenMenuCategories");
            DropIndex("dbo.ScreenMenuCategorySchedules", new[] { "ScreenMenuCategoryId" });
            DropTable("dbo.ScreenMenuCategorySchedules");
            DropTable("dbo.ConnectCardTypeCategories",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConnectCardTypeCategory_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConnectCardTypeCategory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
