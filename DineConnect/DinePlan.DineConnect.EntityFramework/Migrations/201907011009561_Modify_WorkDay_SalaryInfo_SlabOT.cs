namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_WorkDay_SalaryInfo_SlabOT : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OtBasedOnSlabs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeRefId = c.Int(nullable: false),
                        OTRefId = c.Int(nullable: false),
                        FromHours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ToHours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OTAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_OtBasedOnSlab_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_OtBasedOnSlab_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonalInformations", t => t.EmployeeRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.EmployeeRefId)
                .Index(t => t.TenantId);
            
            AddColumn("dbo.IncentiveTags", "IsBulkUpdateAllowed", c => c.Boolean(nullable: false));
            AddColumn("dbo.SalaryInfos", "NormalOTPerHour", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SalaryInfos", "IsOtSlabBasedApplicable", c => c.Boolean(nullable: false));
            AddColumn("dbo.WorkDays", "MinimumOTHoursNeededForAutoManualOTEligible", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OtBasedOnSlabs", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.OtBasedOnSlabs", "EmployeeRefId", "dbo.PersonalInformations");
            DropIndex("dbo.OtBasedOnSlabs", new[] { "TenantId" });
            DropIndex("dbo.OtBasedOnSlabs", new[] { "EmployeeRefId" });
            DropColumn("dbo.WorkDays", "MinimumOTHoursNeededForAutoManualOTEligible");
            DropColumn("dbo.SalaryInfos", "IsOtSlabBasedApplicable");
            DropColumn("dbo.SalaryInfos", "NormalOTPerHour");
            DropColumn("dbo.IncentiveTags", "IsBulkUpdateAllowed");
            DropTable("dbo.OtBasedOnSlabs",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_OtBasedOnSlab_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_OtBasedOnSlab_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
