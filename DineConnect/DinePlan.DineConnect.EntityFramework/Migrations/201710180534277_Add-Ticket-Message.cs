namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddTicketMessage : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmployeeDepartments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeDepartment_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeDepartment_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ForeignCurrencies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CurrencySymbol = c.String(),
                        ExchangeRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Rounding = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PaymentTypeId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ForeignCurrency_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ForeignCurrency_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PaymentTypes", t => t.PaymentTypeId, cascadeDelete: true)
                .Index(t => t.PaymentTypeId);
            
            CreateTable(
                "dbo.InventoryCycleTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InventoryCycleTagCode = c.String(nullable: false, maxLength: 50),
                        InventoryCycleTagDescription = c.String(),
                        InventoryCycleMode = c.Int(nullable: false),
                        InventoryModeSchedule = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InventoryCycleTag_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InventoryCycleTag_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.InventoryCycleTagCode, unique: true, name: "InventoryCycleTagCode");
            
            CreateTable(
                "dbo.MaterialStockCycleLinks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaterialRefId = c.Int(nullable: false),
                        InventoryCycleTagRefId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialStockCycleLink_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MaterialStockCycleLink_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.InventoryCycleTags", t => t.InventoryCycleTagRefId, cascadeDelete: true)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId, cascadeDelete: true)
                .Index(t => new { t.MaterialRefId, t.InventoryCycleTagRefId }, unique: true, name: "MPK_MaterialRefId_InvCycleRefId");
            
            CreateTable(
                "dbo.TicketMessages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        Message = c.String(),
                        DepartmentId = c.Int(nullable: false),
                        MessageStatusId = c.Int(nullable: false),
                        Files = c.String(),
                        LocationId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TicketMessage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EmployeeDepartments", t => t.DepartmentId, cascadeDelete: true)
                .ForeignKey("dbo.Locations", t => t.LocationId)
                .ForeignKey("dbo.TicketMessageStatuses", t => t.MessageStatusId, cascadeDelete: true)
                .ForeignKey("dbo.AbpUsers", t => t.CreatorUserId)
                .Index(t => t.DepartmentId)
                .Index(t => t.MessageStatusId)
                .Index(t => t.LocationId)
                .Index(t => t.CreatorUserId);
            
            CreateTable(
                "dbo.TicketMessageStatuses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessageStatus_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TicketMessagesHistory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TicketMessageReplyId = c.Int(nullable: false),
                        DepartmentId = c.Int(nullable: false),
                        MessageStatusId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessageHistory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TicketMessageReplies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        TicketMessageId = c.Int(nullable: false),
                        TicketMessageStatusId = c.Int(),
                        EmployeeDepartmentId = c.Int(),
                        Files = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessageReply_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EmployeeDepartments", t => t.EmployeeDepartmentId)
                .ForeignKey("dbo.TicketMessages", t => t.TicketMessageId, cascadeDelete: true)
                .ForeignKey("dbo.TicketMessageStatuses", t => t.TicketMessageStatusId)
                .ForeignKey("dbo.AbpUsers", t => t.CreatorUserId)
                .Index(t => t.TicketMessageId)
                .Index(t => t.TicketMessageStatusId)
                .Index(t => t.EmployeeDepartmentId)
                .Index(t => t.CreatorUserId);
            
            CreateTable(
                "dbo.TillAccounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TillAccountTypeId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        Locations = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TillAccount_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TillAccount_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TillTransactions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WorkTimeId = c.Int(nullable: false),
                        Remarks = c.String(),
                        TotalAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TillAccountId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TillTransaction_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TillTransaction_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TillAccounts", t => t.TillAccountId, cascadeDelete: true)
                .Index(t => t.TillAccountId);
            
            AddColumn("dbo.MenuItems", "Files", c => c.String());
            AddColumn("dbo.Departments", "TagId", c => c.Int());
            AddColumn("dbo.EmployeeInfos", "UserId", c => c.Long(nullable: false));
            AddColumn("dbo.EmployeeInfos", "EmployeeDepartmentId", c => c.Int(nullable: false));
            AddColumn("dbo.PaymentTypes", "Locations", c => c.String());
            AlterColumn("dbo.MemberCards", "PrimaryCardRefId", c => c.Int(nullable: false));
            CreateIndex("dbo.Departments", "TagId");
            CreateIndex("dbo.EmployeeInfos", "UserId");
            CreateIndex("dbo.EmployeeInfos", "EmployeeDepartmentId");
            AddForeignKey("dbo.Departments", "TagId", "dbo.PriceTags", "Id");
            AddForeignKey("dbo.EmployeeInfos", "EmployeeDepartmentId", "dbo.EmployeeDepartments", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EmployeeInfos", "UserId", "dbo.AbpUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TillTransactions", "TillAccountId", "dbo.TillAccounts");
            DropForeignKey("dbo.TicketMessageReplies", "CreatorUserId", "dbo.AbpUsers");
            DropForeignKey("dbo.TicketMessageReplies", "TicketMessageStatusId", "dbo.TicketMessageStatuses");
            DropForeignKey("dbo.TicketMessageReplies", "TicketMessageId", "dbo.TicketMessages");
            DropForeignKey("dbo.TicketMessageReplies", "EmployeeDepartmentId", "dbo.EmployeeDepartments");
            DropForeignKey("dbo.TicketMessages", "CreatorUserId", "dbo.AbpUsers");
            DropForeignKey("dbo.TicketMessages", "MessageStatusId", "dbo.TicketMessageStatuses");
            DropForeignKey("dbo.TicketMessages", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.TicketMessages", "DepartmentId", "dbo.EmployeeDepartments");
            DropForeignKey("dbo.MaterialStockCycleLinks", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.MaterialStockCycleLinks", "InventoryCycleTagRefId", "dbo.InventoryCycleTags");
            DropForeignKey("dbo.ForeignCurrencies", "PaymentTypeId", "dbo.PaymentTypes");
            DropForeignKey("dbo.EmployeeInfos", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.EmployeeInfos", "EmployeeDepartmentId", "dbo.EmployeeDepartments");
            DropForeignKey("dbo.Departments", "TagId", "dbo.PriceTags");
            DropIndex("dbo.TillTransactions", new[] { "TillAccountId" });
            DropIndex("dbo.TicketMessageReplies", new[] { "CreatorUserId" });
            DropIndex("dbo.TicketMessageReplies", new[] { "EmployeeDepartmentId" });
            DropIndex("dbo.TicketMessageReplies", new[] { "TicketMessageStatusId" });
            DropIndex("dbo.TicketMessageReplies", new[] { "TicketMessageId" });
            DropIndex("dbo.TicketMessages", new[] { "CreatorUserId" });
            DropIndex("dbo.TicketMessages", new[] { "LocationId" });
            DropIndex("dbo.TicketMessages", new[] { "MessageStatusId" });
            DropIndex("dbo.TicketMessages", new[] { "DepartmentId" });
            DropIndex("dbo.MaterialStockCycleLinks", "MPK_MaterialRefId_InvCycleRefId");
            DropIndex("dbo.InventoryCycleTags", "InventoryCycleTagCode");
            DropIndex("dbo.ForeignCurrencies", new[] { "PaymentTypeId" });
            DropIndex("dbo.EmployeeInfos", new[] { "EmployeeDepartmentId" });
            DropIndex("dbo.EmployeeInfos", new[] { "UserId" });
            DropIndex("dbo.Departments", new[] { "TagId" });
            AlterColumn("dbo.MemberCards", "PrimaryCardRefId", c => c.Int());
            DropColumn("dbo.PaymentTypes", "Locations");
            DropColumn("dbo.EmployeeInfos", "EmployeeDepartmentId");
            DropColumn("dbo.EmployeeInfos", "UserId");
            DropColumn("dbo.Departments", "TagId");
            DropColumn("dbo.MenuItems", "Files");
            DropTable("dbo.TillTransactions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TillTransaction_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TillTransaction_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TillAccounts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TillAccount_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TillAccount_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TicketMessageReplies",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessageReply_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TicketMessagesHistory",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessageHistory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TicketMessageStatuses",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessageStatus_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TicketMessages",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TicketMessage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MaterialStockCycleLinks",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialStockCycleLink_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MaterialStockCycleLink_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.InventoryCycleTags",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InventoryCycleTag_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InventoryCycleTag_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ForeignCurrencies",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ForeignCurrency_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ForeignCurrency_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EmployeeDepartments",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeDepartment_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeDepartment_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
