namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_tick_message_table_Tam : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TickMessages", "Reply", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TickMessages", "Reply", c => c.Boolean());
        }
    }
}
