namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddConfirmationWeightedItem : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ScreenMenuItems", "ConfirmationType", c => c.Int(nullable: false));
            AddColumn("dbo.ScreenMenuItems", "WeightedItem", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ScreenMenuItems", "WeightedItem");
            DropColumn("dbo.ScreenMenuItems", "ConfirmationType");
        }
    }
}
