namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Location_ExtendedBusinessHours : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "ExtendedBusinessHours", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "ExtendedBusinessHours");
        }
    }
}
