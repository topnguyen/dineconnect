namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMultiBarcodes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MenuBarCodes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Barcode = c.String(maxLength: 50),
                        MenuItemId = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MenuItems", t => t.MenuItemId, cascadeDelete: true)
                .Index(t => t.MenuItemId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MenuBarCodes", "MenuItemId", "dbo.MenuItems");
            DropIndex("dbo.MenuBarCodes", new[] { "MenuItemId" });
            DropTable("dbo.MenuBarCodes");
        }
    }
}
