namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTransactionTypeToLocation : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DinePlanTaxes", "TransactionTypeId", "dbo.TransactionTypes");
            DropIndex("dbo.DinePlanTaxes", new[] { "TransactionTypeId" });
            AddColumn("dbo.DinePlanTaxLocations", "TransactionTypeId", c => c.Int());
            CreateIndex("dbo.DinePlanTaxLocations", "TransactionTypeId");
            AddForeignKey("dbo.DinePlanTaxLocations", "TransactionTypeId", "dbo.TransactionTypes", "Id");
            DropColumn("dbo.DinePlanTaxes", "TransactionTypeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DinePlanTaxes", "TransactionTypeId", c => c.Int());
            DropForeignKey("dbo.DinePlanTaxLocations", "TransactionTypeId", "dbo.TransactionTypes");
            DropIndex("dbo.DinePlanTaxLocations", new[] { "TransactionTypeId" });
            DropColumn("dbo.DinePlanTaxLocations", "TransactionTypeId");
            CreateIndex("dbo.DinePlanTaxes", "TransactionTypeId");
            AddForeignKey("dbo.DinePlanTaxes", "TransactionTypeId", "dbo.TransactionTypes", "Id");
        }
    }
}
