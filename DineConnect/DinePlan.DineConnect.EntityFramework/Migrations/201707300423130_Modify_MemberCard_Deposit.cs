namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_MemberCard_Deposit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MemberCards", "DepositAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MemberCards", "DepositAmount");
        }
    }
}
