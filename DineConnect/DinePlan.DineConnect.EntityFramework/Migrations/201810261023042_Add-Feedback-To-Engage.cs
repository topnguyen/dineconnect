namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddFeedbackToEngage : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FeedBackAnswers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FeedbackId = c.Int(nullable: false),
                        FeedbackQuestionId = c.Int(nullable: false),
                        Answer = c.String(),
                        IsNegative = c.Boolean(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FeedBacks", t => t.FeedbackId, cascadeDelete: true)
                .ForeignKey("dbo.FeedBackQuestions", t => t.FeedbackQuestionId, cascadeDelete: true)
                .Index(t => t.FeedbackId)
                .Index(t => t.FeedbackQuestionId);
            
            CreateTable(
                "dbo.FeedBacks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FeedbackGroupId = c.Int(nullable: false),
                        TicketNo = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_FeedBack_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_FeedBack_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FeedBackGroups", t => t.FeedbackGroupId, cascadeDelete: true)
                .Index(t => t.FeedbackGroupId);
            
            CreateTable(
                "dbo.FeedBackGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_FeedBackGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_FeedBackGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FeedBackQuestions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Question = c.String(),
                        QuestionType = c.Int(nullable: false),
                        Answers = c.String(),
                        NegativeAnswer = c.String(),
                        DefaultAnswer = c.String(),
                        Name = c.String(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FeedBackQuestionFeedBackGroups",
                c => new
                    {
                        FeedBackQuestion_Id = c.Int(nullable: false),
                        FeedBackGroup_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.FeedBackQuestion_Id, t.FeedBackGroup_Id })
                .ForeignKey("dbo.FeedBackQuestions", t => t.FeedBackQuestion_Id, cascadeDelete: true)
                .ForeignKey("dbo.FeedBackGroups", t => t.FeedBackGroup_Id, cascadeDelete: true)
                .Index(t => t.FeedBackQuestion_Id)
                .Index(t => t.FeedBackGroup_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FeedBackAnswers", "FeedbackQuestionId", "dbo.FeedBackQuestions");
            DropForeignKey("dbo.FeedBackAnswers", "FeedbackId", "dbo.FeedBacks");
            DropForeignKey("dbo.FeedBacks", "FeedbackGroupId", "dbo.FeedBackGroups");
            DropForeignKey("dbo.FeedBackQuestionFeedBackGroups", "FeedBackGroup_Id", "dbo.FeedBackGroups");
            DropForeignKey("dbo.FeedBackQuestionFeedBackGroups", "FeedBackQuestion_Id", "dbo.FeedBackQuestions");
            DropIndex("dbo.FeedBackQuestionFeedBackGroups", new[] { "FeedBackGroup_Id" });
            DropIndex("dbo.FeedBackQuestionFeedBackGroups", new[] { "FeedBackQuestion_Id" });
            DropIndex("dbo.FeedBacks", new[] { "FeedbackGroupId" });
            DropIndex("dbo.FeedBackAnswers", new[] { "FeedbackQuestionId" });
            DropIndex("dbo.FeedBackAnswers", new[] { "FeedbackId" });
            DropTable("dbo.FeedBackQuestionFeedBackGroups");
            DropTable("dbo.FeedBackQuestions");
            DropTable("dbo.FeedBackGroups",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_FeedBackGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_FeedBackGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.FeedBacks",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_FeedBack_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_FeedBack_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.FeedBackAnswers");
        }
    }
}
