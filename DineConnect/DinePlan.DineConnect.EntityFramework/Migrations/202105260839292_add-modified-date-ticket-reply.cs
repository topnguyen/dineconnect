namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addmodifieddateticketreply : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TickMessageReplies", "LastModificationTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TickMessageReplies", "LastModificationTime");
        }
    }
}
