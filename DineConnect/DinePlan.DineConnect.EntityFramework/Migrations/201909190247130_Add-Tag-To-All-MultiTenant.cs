namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTagToAllMultiTenant : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GiftVoucherTypes", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.PriceTags", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.DinePlanUsers", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.Departments", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.Calculations", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.ConnectCardTypes", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.ConnectTableGroups", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.Promotions", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.PaymentTypes", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.FullTaxMembers", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.FullTaxInvoices", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.FutureDateInformations", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.OrderTagGroups", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.PlanReasons", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.ReceiptContents", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.ScreenMenus", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.SecondDisplays", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.TicketTagGroups", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.TickMessages", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.TillAccounts", "Tag", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TillAccounts", "Tag");
            DropColumn("dbo.TickMessages", "Tag");
            DropColumn("dbo.TicketTagGroups", "Tag");
            DropColumn("dbo.SecondDisplays", "Tag");
            DropColumn("dbo.ScreenMenus", "Tag");
            DropColumn("dbo.ReceiptContents", "Tag");
            DropColumn("dbo.PlanReasons", "Tag");
            DropColumn("dbo.OrderTagGroups", "Tag");
            DropColumn("dbo.FutureDateInformations", "Tag");
            DropColumn("dbo.FullTaxInvoices", "Tag");
            DropColumn("dbo.FullTaxMembers", "Tag");
            DropColumn("dbo.PaymentTypes", "Tag");
            DropColumn("dbo.Promotions", "Tag");
            DropColumn("dbo.ConnectTableGroups", "Tag");
            DropColumn("dbo.ConnectCardTypes", "Tag");
            DropColumn("dbo.Calculations", "Tag");
            DropColumn("dbo.Departments", "Tag");
            DropColumn("dbo.DinePlanUsers", "Tag");
            DropColumn("dbo.PriceTags", "Tag");
            DropColumn("dbo.GiftVoucherTypes", "Tag");
        }
    }
}
