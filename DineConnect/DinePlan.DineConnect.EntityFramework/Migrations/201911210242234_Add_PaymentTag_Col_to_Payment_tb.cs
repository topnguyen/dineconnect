namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_PaymentTag_Col_to_Payment_tb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentTypes", "PaymentTag", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaymentTypes", "PaymentTag");
        }
    }
}
