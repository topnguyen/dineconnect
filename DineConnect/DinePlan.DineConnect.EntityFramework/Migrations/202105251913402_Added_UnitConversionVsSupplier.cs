namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_UnitConversionVsSupplier : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UnitConversionVsSuppliers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UnitConversionRefId = c.Int(nullable: false),
                        SupplierRefId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UnitConversionVsSupplier_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Suppliers", t => t.SupplierRefId, cascadeDelete: true)
                .ForeignKey("dbo.UnitConversions", t => t.UnitConversionRefId, cascadeDelete: true)
                .Index(t => t.UnitConversionRefId)
                .Index(t => t.SupplierRefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UnitConversionVsSuppliers", "UnitConversionRefId", "dbo.UnitConversions");
            DropForeignKey("dbo.UnitConversionVsSuppliers", "SupplierRefId", "dbo.Suppliers");
            DropIndex("dbo.UnitConversionVsSuppliers", new[] { "SupplierRefId" });
            DropIndex("dbo.UnitConversionVsSuppliers", new[] { "UnitConversionRefId" });
            DropTable("dbo.UnitConversionVsSuppliers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UnitConversionVsSupplier_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
