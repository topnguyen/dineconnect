namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTicketTagAlternateName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TicketTags", "AlternateName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TicketTags", "AlternateName");
        }
    }
}
