namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddapplyoncecolumntoTicketDiscountPromotiontable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TicketDiscountPromotions", "PromotionApplyOnce", c => c.Boolean(nullable: false));
            AddColumn("dbo.TicketDiscountPromotions", "PromotionApplyType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TicketDiscountPromotions", "PromotionApplyType");
            DropColumn("dbo.TicketDiscountPromotions", "PromotionApplyOnce");
        }
    }
}
