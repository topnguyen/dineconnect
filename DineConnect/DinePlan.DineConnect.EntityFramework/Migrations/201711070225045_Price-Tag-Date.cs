namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PriceTagDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PriceTags", "StartDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.PriceTags", "EndDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PriceTags", "EndDate");
            DropColumn("dbo.PriceTags", "StartDate");
        }
    }
}
