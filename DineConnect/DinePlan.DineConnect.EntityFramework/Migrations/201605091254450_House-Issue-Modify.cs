namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HouseIssueModify : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Issues", "RecipeRefId", c => c.Int());
            AddColumn("dbo.Issues", "RecipeProductionQty", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Issues", "MultipleBatchProductionAllowed", c => c.Boolean(nullable: false));
            CreateIndex("dbo.IssueDetails", "RecipeRefId");
            CreateIndex("dbo.Issues", "RecipeRefId");
            AddForeignKey("dbo.Issues", "RecipeRefId", "dbo.Materials", "Id");
            AddForeignKey("dbo.IssueDetails", "RecipeRefId", "dbo.Materials", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.IssueDetails", "RecipeRefId", "dbo.Materials");
            DropForeignKey("dbo.Issues", "RecipeRefId", "dbo.Materials");
            DropIndex("dbo.Issues", new[] { "RecipeRefId" });
            DropIndex("dbo.IssueDetails", new[] { "RecipeRefId" });
            DropColumn("dbo.Issues", "MultipleBatchProductionAllowed");
            DropColumn("dbo.Issues", "RecipeProductionQty");
            DropColumn("dbo.Issues", "RecipeRefId");
        }
    }
}
