namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTaxTag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItems", "HsnCode", c => c.String(maxLength: 50));
            AddColumn("dbo.DinePlanTaxLocations", "Tag", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DinePlanTaxLocations", "Tag");
            DropColumn("dbo.MenuItems", "HsnCode");
        }
    }
}
