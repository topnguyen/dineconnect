namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddPreTicket : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PreTickets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationId = c.Int(nullable: false),
                        TicketNumber = c.String(maxLength: 30),
                        Contents = c.String(),
                        Errors = c.String(),
                        Processed = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PreTicket_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .Index(t => new { t.LocationId, t.TicketNumber }, unique: true, name: "IX_FirstAndSecond");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PreTickets", "LocationId", "dbo.Locations");
            DropIndex("dbo.PreTickets", "IX_FirstAndSecond");
            DropTable("dbo.PreTickets",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PreTicket_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
