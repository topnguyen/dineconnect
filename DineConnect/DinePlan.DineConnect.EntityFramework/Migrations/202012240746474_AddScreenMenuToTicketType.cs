namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddScreenMenuToTicketType : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TicketTypes", "MenuItemId", "dbo.MenuItems");
            DropIndex("dbo.TicketTypes", new[] { "MenuItemId" });
            AddColumn("dbo.TicketTypes", "ScreenMenuId", c => c.Int());
            CreateIndex("dbo.TicketTypes", "ScreenMenuId");
            AddForeignKey("dbo.TicketTypes", "ScreenMenuId", "dbo.ScreenMenus", "Id");
            DropColumn("dbo.TicketTypes", "MenuItemId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TicketTypes", "MenuItemId", c => c.Int());
            DropForeignKey("dbo.TicketTypes", "ScreenMenuId", "dbo.ScreenMenus");
            DropIndex("dbo.TicketTypes", new[] { "ScreenMenuId" });
            DropColumn("dbo.TicketTypes", "ScreenMenuId");
            CreateIndex("dbo.TicketTypes", "MenuItemId");
            AddForeignKey("dbo.TicketTypes", "MenuItemId", "dbo.MenuItems", "Id");
        }
    }
}
