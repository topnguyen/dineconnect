namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PendingAug13 : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.PlayDisplays", "RegistrationKey", c => c.String());
            //AddColumn("dbo.PlayScheduledEvents", "ParentId", c => c.Int());
            //AlterColumn("dbo.PlayDisplays", "MacAddress", c => c.String(maxLength: 17));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PlayDisplays", "MacAddress", c => c.String(nullable: false, maxLength: 17));
            DropColumn("dbo.PlayScheduledEvents", "ParentId");
            DropColumn("dbo.PlayDisplays", "RegistrationKey");
        }
    }
}
