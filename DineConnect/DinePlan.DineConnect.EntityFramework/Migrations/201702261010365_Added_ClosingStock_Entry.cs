namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_ClosingStock_Entry : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClosingStockDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClosingStockRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        UnitRefId = c.Int(nullable: false),
                        OnHand = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ClosingStockDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ClosingStockDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClosingStocks", t => t.ClosingStockRefId)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .ForeignKey("dbo.Units", t => t.UnitRefId)
                .Index(t => t.ClosingStockRefId)
                .Index(t => t.MaterialRefId)
                .Index(t => t.UnitRefId);
            
            CreateTable(
                "dbo.ClosingStocks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        StockDate = c.DateTime(nullable: false),
                        ApprovedUserId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ClosingStock_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ClosingStock_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .Index(t => t.LocationRefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ClosingStockDetails", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.ClosingStockDetails", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.ClosingStockDetails", "ClosingStockRefId", "dbo.ClosingStocks");
            DropForeignKey("dbo.ClosingStocks", "LocationRefId", "dbo.Locations");
            DropIndex("dbo.ClosingStocks", new[] { "LocationRefId" });
            DropIndex("dbo.ClosingStockDetails", new[] { "UnitRefId" });
            DropIndex("dbo.ClosingStockDetails", new[] { "MaterialRefId" });
            DropIndex("dbo.ClosingStockDetails", new[] { "ClosingStockRefId" });
            DropTable("dbo.ClosingStocks",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ClosingStock_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ClosingStock_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ClosingStockDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ClosingStockDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ClosingStockDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
