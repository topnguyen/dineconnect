namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSystemLogging : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SystemLoggings",
                c => new
                    {
                        log_id = c.Guid(nullable: false),
                        log_date = c.DateTime(nullable: false),
                        log_level = c.String(),
                        log_logger = c.String(),
                        log_message = c.String(),
                        log_machine_name = c.String(),
                        log_user_name = c.String(),
                        log_call_site = c.String(),
                        log_thread = c.String(),
                        log_exception = c.String(),
                        log_stacktrace = c.String(),
                    })
                .PrimaryKey(t => t.log_id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SystemLoggings");
        }
    }
}
