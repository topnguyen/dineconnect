namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify__InwardMaster_AdjustmentRefId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InwardDirectCredits", "AdjustmentRefId", c => c.Int());
            CreateIndex("dbo.InwardDirectCredits", "AdjustmentRefId");
            AddForeignKey("dbo.InwardDirectCredits", "AdjustmentRefId", "dbo.Adjustments", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InwardDirectCredits", "AdjustmentRefId", "dbo.Adjustments");
            DropIndex("dbo.InwardDirectCredits", new[] { "AdjustmentRefId" });
            DropColumn("dbo.InwardDirectCredits", "AdjustmentRefId");
        }
    }
}
