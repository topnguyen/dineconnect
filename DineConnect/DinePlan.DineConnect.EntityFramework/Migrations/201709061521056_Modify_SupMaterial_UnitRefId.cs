namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_SupMaterial_UnitRefId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SupplierMaterials", "UnitRefId", c => c.Int(nullable: true));
			Sql("Update dbo.SupplierMaterials Set UnitRefId = ( select DefaultUnitId FROM dbo.Materials where dbo.SupplierMaterials.MaterialRefId = dbo.Materials.Id)");
			AlterColumn("dbo.SupplierMaterials", "UnitRefId", c => c.Int(nullable: false));
			CreateIndex("dbo.SupplierMaterials", "UnitRefId");
			AddForeignKey("dbo.SupplierMaterials", "UnitRefId", "dbo.Units", "Id");
		}

		public override void Down()
        {
            DropForeignKey("dbo.SupplierMaterials", "UnitRefId", "dbo.Units");
            DropIndex("dbo.SupplierMaterials", new[] { "UnitRefId" });
            DropColumn("dbo.SupplierMaterials", "UnitRefId");
        }
    }
}
