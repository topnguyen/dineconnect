namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_DocReferenceNumber : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InterTransfers", "DocReferenceNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.InterTransfers", "DocReferenceNumber");
        }
    }
}
