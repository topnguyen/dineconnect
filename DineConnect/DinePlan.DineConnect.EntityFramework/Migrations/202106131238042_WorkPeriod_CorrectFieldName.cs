namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WorkPeriod_CorrectFieldName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkPeriods", "TotalTaxes", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.WorkPeriods", "TotalTaxs");
        }
        
        public override void Down()
        {
            AddColumn("dbo.WorkPeriods", "TotalTaxs", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.WorkPeriods", "TotalTaxes");
        }
    }
}
