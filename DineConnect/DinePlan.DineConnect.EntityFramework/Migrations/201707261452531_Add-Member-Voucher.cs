namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMemberVoucher : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Promotions", "Filter", c => c.String());
            AddColumn("dbo.GiftVouchers", "ConnectMemberId", c => c.Int());
            AddColumn("dbo.GiftVoucherTypes", "MinimumTicketTotal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            CreateIndex("dbo.GiftVouchers", "ConnectMemberId");
            AddForeignKey("dbo.GiftVouchers", "ConnectMemberId", "dbo.ConnectMembers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GiftVouchers", "ConnectMemberId", "dbo.ConnectMembers");
            DropIndex("dbo.GiftVouchers", new[] { "ConnectMemberId" });
            DropColumn("dbo.GiftVoucherTypes", "MinimumTicketTotal");
            DropColumn("dbo.GiftVouchers", "ConnectMemberId");
            DropColumn("dbo.Promotions", "Filter");
        }
    }
}
