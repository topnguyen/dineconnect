namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddTicket : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmployeeDepartments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeDepartment_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeDepartment_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ForeignCurrencies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CurrencySymbol = c.String(),
                        ExchangeRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Rounding = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PaymentTypeId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ForeignCurrency_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ForeignCurrency_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PaymentTypes", t => t.PaymentTypeId, cascadeDelete: true)
                .Index(t => t.PaymentTypeId);
            
            CreateTable(
                "dbo.InventoryCycleTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InventoryCycleTagCode = c.String(nullable: false, maxLength: 50),
                        InventoryCycleTagDescription = c.String(),
                        InventoryCycleMode = c.Int(nullable: false),
                        InventoryModeSchedule = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InventoryCycleTag_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InventoryCycleTag_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.InventoryCycleTagCode, unique: true, name: "InventoryCycleTagCode");
            
            CreateTable(
                "dbo.TicketMessages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        Message = c.String(),
                        DepartmentId = c.Int(nullable: false),
                        MessageStatusId = c.Int(nullable: false),
                        Files = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TicketMessage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EmployeeDepartments", t => t.DepartmentId, cascadeDelete: true)
                .ForeignKey("dbo.TicketMessageStatuses", t => t.MessageStatusId, cascadeDelete: true)
                .Index(t => t.DepartmentId)
                .Index(t => t.MessageStatusId);
            
            CreateTable(
                "dbo.TicketMessageStatuses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessageStatus_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TicketMessageReplies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        TicketMessageId = c.Int(nullable: false),
                        TicketMessageStatusId = c.Int(),
                        EmployeeDepartmentId = c.Int(),
                        Files = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessageReply_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EmployeeDepartments", t => t.EmployeeDepartmentId)
                .ForeignKey("dbo.TicketMessages", t => t.TicketMessageId, cascadeDelete: true)
                .ForeignKey("dbo.TicketMessageStatuses", t => t.TicketMessageStatusId)
                .ForeignKey("dbo.AbpUsers", t => t.CreatorUserId)
                .Index(t => t.TicketMessageId)
                .Index(t => t.TicketMessageStatusId)
                .Index(t => t.EmployeeDepartmentId)
                .Index(t => t.CreatorUserId);
            
            AddColumn("dbo.EmployeeInfos", "UserId", c => c.Long(nullable: false));
            AddColumn("dbo.EmployeeInfos", "EmployeeDepartmentId", c => c.Int(nullable: false));
            AddColumn("dbo.PaymentTypes", "Locations", c => c.String());
            CreateIndex("dbo.EmployeeInfos", "UserId");
            CreateIndex("dbo.EmployeeInfos", "EmployeeDepartmentId");
            AddForeignKey("dbo.EmployeeInfos", "EmployeeDepartmentId", "dbo.EmployeeDepartments", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EmployeeInfos", "UserId", "dbo.AbpUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TicketMessageReplies", "CreatorUserId", "dbo.AbpUsers");
            DropForeignKey("dbo.TicketMessageReplies", "TicketMessageStatusId", "dbo.TicketMessageStatuses");
            DropForeignKey("dbo.TicketMessageReplies", "TicketMessageId", "dbo.TicketMessages");
            DropForeignKey("dbo.TicketMessageReplies", "EmployeeDepartmentId", "dbo.EmployeeDepartments");
            DropForeignKey("dbo.TicketMessages", "MessageStatusId", "dbo.TicketMessageStatuses");
            DropForeignKey("dbo.TicketMessages", "DepartmentId", "dbo.EmployeeDepartments");
            DropForeignKey("dbo.ForeignCurrencies", "PaymentTypeId", "dbo.PaymentTypes");
            DropForeignKey("dbo.EmployeeInfos", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.EmployeeInfos", "EmployeeDepartmentId", "dbo.EmployeeDepartments");
            DropIndex("dbo.TicketMessageReplies", new[] { "CreatorUserId" });
            DropIndex("dbo.TicketMessageReplies", new[] { "EmployeeDepartmentId" });
            DropIndex("dbo.TicketMessageReplies", new[] { "TicketMessageStatusId" });
            DropIndex("dbo.TicketMessageReplies", new[] { "TicketMessageId" });
            DropIndex("dbo.TicketMessages", new[] { "MessageStatusId" });
            DropIndex("dbo.TicketMessages", new[] { "DepartmentId" });
            DropIndex("dbo.InventoryCycleTags", "InventoryCycleTagCode");
            DropIndex("dbo.ForeignCurrencies", new[] { "PaymentTypeId" });
            DropIndex("dbo.EmployeeInfos", new[] { "EmployeeDepartmentId" });
            DropIndex("dbo.EmployeeInfos", new[] { "UserId" });
            DropColumn("dbo.PaymentTypes", "Locations");
            DropColumn("dbo.EmployeeInfos", "EmployeeDepartmentId");
            DropColumn("dbo.EmployeeInfos", "UserId");
            DropTable("dbo.TicketMessageReplies",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessageReply_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TicketMessageStatuses",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessageStatus_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TicketMessages",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TicketMessage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.InventoryCycleTags",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InventoryCycleTag_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InventoryCycleTag_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ForeignCurrencies",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ForeignCurrency_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ForeignCurrency_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EmployeeDepartments",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeDepartment_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeDepartment_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
