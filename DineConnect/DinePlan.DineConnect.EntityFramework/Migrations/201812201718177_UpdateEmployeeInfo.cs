namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateEmployeeInfo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmployeeInfos", "FirstName", c => c.String());
            AddColumn("dbo.EmployeeInfos", "LastName", c => c.String());
            AddColumn("dbo.EmployeeInfos", "Email", c => c.String());
            AddColumn("dbo.EmployeeInfos", "HomePhone", c => c.String());
            AddColumn("dbo.EmployeeInfos", "MobilePhone", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmployeeInfos", "MobilePhone");
            DropColumn("dbo.EmployeeInfos", "HomePhone");
            DropColumn("dbo.EmployeeInfos", "Email");
            DropColumn("dbo.EmployeeInfos", "LastName");
            DropColumn("dbo.EmployeeInfos", "FirstName");
        }
    }
}
