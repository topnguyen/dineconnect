namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_ProductionEntry : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Productions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        ProductionSlipNumber = c.String(),
                        ProductionTime = c.DateTime(nullable: false),
                        Status = c.String(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Production_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Production_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId, cascadeDelete: true)
                .Index(t => t.LocationRefId);
            
            CreateTable(
                "dbo.ProductionDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductionRefId = c.Int(nullable: false),
                        Sno = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        OutputQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitRefId = c.Int(nullable: false),
                        IssueRefId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProductionDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Issues", t => t.IssueRefId)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId, cascadeDelete: true)
                .ForeignKey("dbo.Productions", t => t.ProductionRefId, cascadeDelete: true)
                .Index(t => t.ProductionRefId)
                .Index(t => t.MaterialRefId)
                .Index(t => t.IssueRefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductionDetails", "ProductionRefId", "dbo.Productions");
            DropForeignKey("dbo.ProductionDetails", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.ProductionDetails", "IssueRefId", "dbo.Issues");
            DropForeignKey("dbo.Productions", "LocationRefId", "dbo.Locations");
            DropIndex("dbo.ProductionDetails", new[] { "IssueRefId" });
            DropIndex("dbo.ProductionDetails", new[] { "MaterialRefId" });
            DropIndex("dbo.ProductionDetails", new[] { "ProductionRefId" });
            DropIndex("dbo.Productions", new[] { "LocationRefId" });
            DropTable("dbo.ProductionDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProductionDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Productions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Production_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Production_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
