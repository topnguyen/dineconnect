namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Connect : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Brands",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Brand_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Brand_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Category_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Category_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LocationMenuItemPrices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MenuPortionId = c.Int(nullable: false),
                        LocationId = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MenuItemPortions", t => t.MenuPortionId)
                .ForeignKey("dbo.Locations", t => t.LocationId)
                .Index(t => t.MenuPortionId)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Country = c.String(),
                        PhoneNumber = c.String(),
                        Fax = c.String(),
                        Website = c.String(),
                        Email = c.String(),
                        OrganizationUnitId = c.Long(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Location_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Location_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LocationMenuItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MenuItemId = c.Int(nullable: false),
                        LocationId = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId)
                .ForeignKey("dbo.MenuItems", t => t.MenuItemId)
                .Index(t => t.MenuItemId)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.MenuItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        BarCode = c.String(),
                        AliasCode = c.String(),
                        AliasName = c.String(),
                        ItemDescription = c.String(),
                        ForceQuantity = c.Boolean(nullable: false),
                        ForceChangePrice = c.Boolean(nullable: false),
                        CategoryId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MenuItem_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MenuItem_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.MenuItemPortions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        MenuItemId = c.Int(nullable: false),
                        MultiPlier = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MenuItems", t => t.MenuItemId)
                .Index(t => t.MenuItemId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderId = c.Int(nullable: false),
                        TicketId = c.Int(nullable: false),
                        DepartmentName = c.String(),
                        MenuItemId = c.Int(nullable: false),
                        MenuItemName = c.String(),
                        PortionName = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PortionCount = c.Int(nullable: false),
                        Note = c.String(),
                        Locked = c.Boolean(nullable: false),
                        CalculatePrice = c.Boolean(nullable: false),
                        IncreaseInventory = c.Boolean(nullable: false),
                        DecreaseInventory = c.Boolean(nullable: false),
                        OrderNumber = c.String(),
                        CreatingUserName = c.String(),
                        OrderCreatedTime = c.DateTime(nullable: false),
                        PriceTag = c.String(),
                        Taxes = c.String(),
                        OrderTags = c.String(),
                        OrderStates = c.String(),
                        IsPromotionOrder = c.Boolean(nullable: false),
                        PromotionAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MenuItemPortionId = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MenuItemPortions", t => t.MenuItemPortionId)
                .ForeignKey("dbo.Tickets", t => t.TicketId)
                .Index(t => t.TicketId)
                .Index(t => t.MenuItemPortionId);
            
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationId = c.Int(nullable: false),
                        TicketId = c.Int(nullable: false),
                        TicketNumber = c.String(),
                        TicketCreatedTime = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(nullable: false),
                        LastOrderTime = c.DateTime(nullable: false),
                        LastPaymentTime = c.DateTime(nullable: false),
                        IsClosed = c.Boolean(nullable: false),
                        IsLocked = c.Boolean(nullable: false),
                        RemainingAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DepartmentName = c.String(),
                        TicketTypeName = c.String(),
                        Note = c.String(),
                        LastModifiedUserName = c.String(),
                        TicketTags = c.String(),
                        TicketStates = c.String(),
                        TicketLogs = c.String(),
                        TaxIncluded = c.Boolean(nullable: false),
                        TerminalName = c.String(),
                        PreOrder = c.Boolean(nullable: false),
                        TicketEntities = c.String(),
                        WorkPeriodId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Ticket_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Ticket_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PaymentTypeId = c.Int(nullable: false),
                        TicketId = c.Int(nullable: false),
                        PaymentCreatedTime = c.DateTime(nullable: false),
                        TenderedAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TerminalName = c.String(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PaymentUserName = c.String(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PaymentTypes", t => t.PaymentTypeId)
                .ForeignKey("dbo.Tickets", t => t.TicketId)
                .Index(t => t.PaymentTypeId)
                .Index(t => t.TicketId);
            
            CreateTable(
                "dbo.PaymentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PaymentType_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PaymentType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TicketTransactions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TransactionTypeId = c.Int(nullable: false),
                        TicketId = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tickets", t => t.TicketId)
                .ForeignKey("dbo.TransactionTypes", t => t.TransactionTypeId)
                .Index(t => t.TransactionTypeId)
                .Index(t => t.TicketId);
            
            CreateTable(
                "dbo.TransactionTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionType_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TransactionType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TicketTransactions", "TransactionTypeId", "dbo.TransactionTypes");
            DropForeignKey("dbo.TicketTransactions", "TicketId", "dbo.Tickets");
            DropForeignKey("dbo.Payments", "TicketId", "dbo.Tickets");
            DropForeignKey("dbo.Payments", "PaymentTypeId", "dbo.PaymentTypes");
            DropForeignKey("dbo.Orders", "TicketId", "dbo.Tickets");
            DropForeignKey("dbo.Tickets", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.Orders", "MenuItemPortionId", "dbo.MenuItemPortions");
            DropForeignKey("dbo.LocationMenuItemPrices", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.LocationMenuItems", "MenuItemId", "dbo.MenuItems");
            DropForeignKey("dbo.MenuItemPortions", "MenuItemId", "dbo.MenuItems");
            DropForeignKey("dbo.LocationMenuItemPrices", "MenuPortionId", "dbo.MenuItemPortions");
            DropForeignKey("dbo.MenuItems", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.LocationMenuItems", "LocationId", "dbo.Locations");
            DropIndex("dbo.TicketTransactions", new[] { "TicketId" });
            DropIndex("dbo.TicketTransactions", new[] { "TransactionTypeId" });
            DropIndex("dbo.Payments", new[] { "TicketId" });
            DropIndex("dbo.Payments", new[] { "PaymentTypeId" });
            DropIndex("dbo.Tickets", new[] { "LocationId" });
            DropIndex("dbo.Orders", new[] { "MenuItemPortionId" });
            DropIndex("dbo.Orders", new[] { "TicketId" });
            DropIndex("dbo.MenuItemPortions", new[] { "MenuItemId" });
            DropIndex("dbo.MenuItems", new[] { "CategoryId" });
            DropIndex("dbo.LocationMenuItems", new[] { "LocationId" });
            DropIndex("dbo.LocationMenuItems", new[] { "MenuItemId" });
            DropIndex("dbo.LocationMenuItemPrices", new[] { "LocationId" });
            DropIndex("dbo.LocationMenuItemPrices", new[] { "MenuPortionId" });
            DropTable("dbo.TransactionTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TransactionType_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TransactionType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TicketTransactions");
            DropTable("dbo.PaymentTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PaymentType_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PaymentType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Payments");
            DropTable("dbo.Tickets",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Ticket_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Ticket_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Orders");
            DropTable("dbo.MenuItemPortions");
            DropTable("dbo.MenuItems",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MenuItem_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MenuItem_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.LocationMenuItems");
            DropTable("dbo.Locations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Location_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Location_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.LocationMenuItemPrices");
            DropTable("dbo.Categories",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Category_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Category_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Brands",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Brand_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Brand_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
