namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_JobTitle : DbMigration
    {
        public override void Up()
        {
            Sql(" DELETE  FROM BioMetricExcludedEntries ");
            Sql(" DELETE  FROM EmployeeVsSalaryTags ");
            Sql(" DELETE  FROM DutyChartDetails ");
            Sql(" DELETE  FROM DutyCharts ");
            Sql(" DELETE  FROM EmployeeDefaultWorkStations ");
            Sql(" DELETE  FROM DcShiftMasters ");
            Sql(" DELETE  FROM DcStationMasters ");
            Sql(" DELETE  FROM DcGroupMasterVsSkillSets ");
            Sql(" DELETE  FROM DcGroupMasters ");
            Sql(" DELETE  FROM DcHeadMasters ");
            Sql(" DELETE  FROM LeaveRequests ");
            Sql(" DELETE  FROM SalaryInfos ");
            Sql(" DELETE  FROM LeaveExhaustedLists ");
            Sql(" DELETE  FROM YearWiseLeaveAllowedForEmployees ");
            Sql(" DELETE  FROM LeaveTypes ");
            Sql(" DELETE  FROM EmployeeMailMessages ");
            Sql(" DELETE  FROM CommonMailMessages ");
            Sql(" DELETE  FROM UserDefaultInformations ");
            Sql(" DELETE  FROM AttendanceLogs ");
            Sql(" DELETE  FROM AttendanceMachineLocations ");
            Sql(" DELETE  FROM EmployeeDocumentInfos ");
            Sql(" DELETE  FROM DocumentForSkillSets ");
            Sql(" DELETE  FROM DocumentInfos ");
            Sql(" DELETE  FROM EmployeeSkillSets ");
            Sql(" DELETE  FROM PersonalInformations ");
            Sql(" DELETE  FROM SkillSets ");
            Sql(" DELETE  FROM EMailLinkedWithAction ");

            CreateTable(
                "dbo.JobTitleMasters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        JobTitle = c.String(maxLength: 50),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_JobTitleMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_JobTitleMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.JobTitle, unique: true, name: "JobTitle");
            
            AddColumn("dbo.PersonalInformations", "JobTitleRefId", c => c.Int());
            CreateIndex("dbo.PersonalInformations", "JobTitleRefId");
            AddForeignKey("dbo.PersonalInformations", "JobTitleRefId", "dbo.JobTitleMasters", "Id");
            DropColumn("dbo.PersonalInformations", "JobTitle");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PersonalInformations", "JobTitle", c => c.String(maxLength: 50));
            DropForeignKey("dbo.PersonalInformations", "JobTitleRefId", "dbo.JobTitleMasters");
            DropIndex("dbo.JobTitleMasters", "JobTitle");
            DropIndex("dbo.PersonalInformations", new[] { "JobTitleRefId" });
            DropColumn("dbo.PersonalInformations", "JobTitleRefId");
            DropTable("dbo.JobTitleMasters",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_JobTitleMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_JobTitleMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
