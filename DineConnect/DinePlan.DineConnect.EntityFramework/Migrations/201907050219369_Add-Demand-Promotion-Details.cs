namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddDemandPromotionDetails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MenuItemDescriptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LanguageCode = c.String(),
                        Name = c.String(),
                        Description = c.String(),
                        MenuItemId = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MenuItems", t => t.MenuItemId, cascadeDelete: true)
                .Index(t => t.MenuItemId);
            
            CreateTable(
                "dbo.DemandPromotionExecutions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DemandDiscountPromotionId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        MenuItemId = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DemandPromotions", t => t.DemandDiscountPromotionId, cascadeDelete: true)
                .Index(t => t.DemandDiscountPromotionId);
            
            CreateTable(
                "dbo.PromotionRestrictItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PromotionId = c.Int(nullable: false),
                        MenuItemId = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Promotions", t => t.PromotionId, cascadeDelete: true)
                .Index(t => t.PromotionId);
            
            DropTable("dbo.RestrictDates",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RestrictDate_ConnectFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RestrictDate_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RestrictDate_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.RestrictDates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Day = c.DateTime(nullable: false),
                        Oid = c.Int(nullable: false),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        NonLocations = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RestrictDate_ConnectFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RestrictDate_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RestrictDate_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.PromotionRestrictItems", "PromotionId", "dbo.Promotions");
            DropForeignKey("dbo.DemandPromotionExecutions", "DemandDiscountPromotionId", "dbo.DemandPromotions");
            DropForeignKey("dbo.MenuItemDescriptions", "MenuItemId", "dbo.MenuItems");
            DropIndex("dbo.PromotionRestrictItems", new[] { "PromotionId" });
            DropIndex("dbo.DemandPromotionExecutions", new[] { "DemandDiscountPromotionId" });
            DropIndex("dbo.MenuItemDescriptions", new[] { "MenuItemId" });
            DropTable("dbo.PromotionRestrictItems");
            DropTable("dbo.DemandPromotionExecutions");
            DropTable("dbo.MenuItemDescriptions");
        }
    }
}
