namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeSystemLoggingEntityType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SystemLoggings", "Id", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SystemLoggings", "Id");
        }
    }
}
