namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTransactionTypetoTax : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DinePlanTaxes", "TransactionTypeId", c => c.Int());
            CreateIndex("dbo.DinePlanTaxes", "TransactionTypeId");
            AddForeignKey("dbo.DinePlanTaxes", "TransactionTypeId", "dbo.TransactionTypes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DinePlanTaxes", "TransactionTypeId", "dbo.TransactionTypes");
            DropIndex("dbo.DinePlanTaxes", new[] { "TransactionTypeId" });
            DropColumn("dbo.DinePlanTaxes", "TransactionTypeId");
        }
    }
}
