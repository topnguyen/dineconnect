namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Hr_Phase2_New_Include : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StatusMailDates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DutyChartDate = c.DateTime(nullable: false),
                        EmployeeListJson = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_StatusMailDate_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_StatusMailDate_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.TenantId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StatusMailDates", "TenantId", "dbo.AbpTenants");
            DropIndex("dbo.StatusMailDates", new[] { "TenantId" });
            DropTable("dbo.StatusMailDates",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_StatusMailDate_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_StatusMailDate_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
