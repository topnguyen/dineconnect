namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ScreenCategoryHourChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ScreenMenuCategories", "FromHour", c => c.Int(nullable: false));
            AddColumn("dbo.ScreenMenuCategories", "FromMinute", c => c.Int(nullable: false));
            AddColumn("dbo.ScreenMenuCategories", "ToHour", c => c.String());
            AddColumn("dbo.ScreenMenuCategories", "ToMinute", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ScreenMenuCategories", "ToMinute");
            DropColumn("dbo.ScreenMenuCategories", "ToHour");
            DropColumn("dbo.ScreenMenuCategories", "FromMinute");
            DropColumn("dbo.ScreenMenuCategories", "FromHour");
        }
    }
}
