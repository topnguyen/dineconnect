namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyYieldRecipe : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Yield", "RecipeRefId", c => c.Int());
            AddColumn("dbo.Yield", "RecipeProductionQty", c => c.Decimal(precision: 18, scale: 2));
            CreateIndex("dbo.Yield", "RecipeRefId");
            AddForeignKey("dbo.Yield", "RecipeRefId", "dbo.Materials", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Yield", "RecipeRefId", "dbo.Materials");
            DropIndex("dbo.Yield", new[] { "RecipeRefId" });
            DropColumn("dbo.Yield", "RecipeProductionQty");
            DropColumn("dbo.Yield", "RecipeRefId");
        }
    }
}
