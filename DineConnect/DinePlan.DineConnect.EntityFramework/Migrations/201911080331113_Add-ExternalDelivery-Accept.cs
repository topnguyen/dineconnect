namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddExternalDeliveryAccept : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExternalDeliveryTickets", "Accepted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ExternalDeliveryTickets", "Accepted");
        }
    }
}
