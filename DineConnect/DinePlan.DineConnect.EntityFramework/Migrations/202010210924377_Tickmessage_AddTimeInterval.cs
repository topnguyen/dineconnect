namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Tickmessage_AddTimeInterval : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TickMessages", "TimeIntervals", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TickMessages", "TimeIntervals");
        }
    }
}
