namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveTenantIdsSwipe : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.SwipeCardLedgers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LedgerTime = c.DateTime(nullable: false),
                        MemberRefId = c.Int(nullable: false),
                        CardRefId = c.Int(nullable: false),
                        PrimaryCardRefId = c.Int(nullable: false),
                        TransactionType = c.Int(nullable: false),
                        Description = c.String(),
                        OpeningBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Credit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Debit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ClosingBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LocationId = c.Int(),
                        TicketNumber = c.String(maxLength: 15),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_SwipeCardLedger_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            DropColumn("dbo.MemberCards", "TenantId");
            DropColumn("dbo.SwipeMembers", "TenantId");
            DropColumn("dbo.SwipeCardLedgers", "TenantId");
            DropColumn("dbo.SwipeMemberSpecialDates", "TenantId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SwipeMemberSpecialDates", "TenantId", c => c.Int(nullable: false));
            AddColumn("dbo.SwipeCardLedgers", "TenantId", c => c.Int(nullable: false));
            AddColumn("dbo.SwipeMembers", "TenantId", c => c.Int(nullable: false));
            AddColumn("dbo.MemberCards", "TenantId", c => c.Int(nullable: false));
            AlterTableAnnotations(
                "dbo.SwipeCardLedgers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LedgerTime = c.DateTime(nullable: false),
                        MemberRefId = c.Int(nullable: false),
                        CardRefId = c.Int(nullable: false),
                        PrimaryCardRefId = c.Int(nullable: false),
                        TransactionType = c.Int(nullable: false),
                        Description = c.String(),
                        OpeningBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Credit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Debit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ClosingBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LocationId = c.Int(),
                        TicketNumber = c.String(maxLength: 15),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_SwipeCardLedger_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
        }
    }
}
