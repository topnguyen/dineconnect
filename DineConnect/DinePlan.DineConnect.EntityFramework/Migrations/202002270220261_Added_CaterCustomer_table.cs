namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_CaterCustomer_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CaterCustomers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerName = c.String(nullable: false, maxLength: 60),
                        LegalEntityName = c.String(maxLength: 60),
                        TaxRegistrationNumber = c.String(maxLength: 60),
                        Address1 = c.String(maxLength: 50),
                        Address2 = c.String(maxLength: 50),
                        Address3 = c.String(maxLength: 50),
                        City = c.String(maxLength: 50),
                        State = c.String(maxLength: 50),
                        Country = c.String(maxLength: 50),
                        ZipCode = c.String(maxLength: 50),
                        PhoneNumber = c.String(maxLength: 50),
                        CreditTerm = c.String(),
                        OrderChannel = c.Int(nullable: false),
                        Email = c.String(maxLength: 50),
                        FaxNumber = c.String(maxLength: 50),
                        Website = c.String(maxLength: 50),
                        AddOn = c.String(maxLength: 100),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CaterCustomer_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CaterCustomer_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CaterCustomers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CaterCustomer_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CaterCustomer_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
