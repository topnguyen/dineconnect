namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Purchase_Return : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PurchaseReturnTaxDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PurchaseReturnRefId = c.Int(nullable: false),
                        Sno = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        TaxRefId = c.Int(nullable: false),
                        TaxRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TaxValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseReturnTaxDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PurchaseReturnTaxDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .ForeignKey("dbo.PurchaseReturns", t => t.PurchaseReturnRefId)
                .ForeignKey("dbo.Taxes", t => t.TaxRefId)
                .Index(t => t.PurchaseReturnRefId)
                .Index(t => t.MaterialRefId)
                .Index(t => t.TaxRefId);
            
            AddColumn("dbo.PurchaseReturnDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.PurchaseReturnDetails", "TotalAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.PurchaseReturnDetails", "TaxAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.PurchaseReturnDetails", "NetAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.PurchaseReturnDetails", "Remarks", c => c.String());
            CreateIndex("dbo.PurchaseReturnDetails", "PurchaseReturnRefId");
            AddForeignKey("dbo.PurchaseReturnDetails", "PurchaseReturnRefId", "dbo.PurchaseReturns", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PurchaseReturnTaxDetails", "TaxRefId", "dbo.Taxes");
            DropForeignKey("dbo.PurchaseReturnTaxDetails", "PurchaseReturnRefId", "dbo.PurchaseReturns");
            DropForeignKey("dbo.PurchaseReturnTaxDetails", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.PurchaseReturnDetails", "PurchaseReturnRefId", "dbo.PurchaseReturns");
            DropIndex("dbo.PurchaseReturnTaxDetails", new[] { "TaxRefId" });
            DropIndex("dbo.PurchaseReturnTaxDetails", new[] { "MaterialRefId" });
            DropIndex("dbo.PurchaseReturnTaxDetails", new[] { "PurchaseReturnRefId" });
            DropIndex("dbo.PurchaseReturnDetails", new[] { "PurchaseReturnRefId" });
            DropColumn("dbo.PurchaseReturnDetails", "Remarks");
            DropColumn("dbo.PurchaseReturnDetails", "NetAmount");
            DropColumn("dbo.PurchaseReturnDetails", "TaxAmount");
            DropColumn("dbo.PurchaseReturnDetails", "TotalAmount");
            DropColumn("dbo.PurchaseReturnDetails", "Price");
            DropTable("dbo.PurchaseReturnTaxDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseReturnTaxDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PurchaseReturnTaxDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
