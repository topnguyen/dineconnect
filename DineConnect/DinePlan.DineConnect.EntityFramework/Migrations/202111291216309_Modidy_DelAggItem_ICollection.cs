namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modidy_DelAggItem_ICollection : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DelAggVariants", "DelAggItem_Id", c => c.Int());
            AddColumn("dbo.DelAggVariantGroups", "DelAggItem_Id", c => c.Int());
            CreateIndex("dbo.DelAggVariantGroups", "DelAggItem_Id");
            CreateIndex("dbo.DelAggVariants", "DelAggItem_Id");
            AddForeignKey("dbo.DelAggVariantGroups", "DelAggItem_Id", "dbo.DelAggItems", "Id");
            AddForeignKey("dbo.DelAggVariants", "DelAggItem_Id", "dbo.DelAggItems", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DelAggVariants", "DelAggItem_Id", "dbo.DelAggItems");
            DropForeignKey("dbo.DelAggVariantGroups", "DelAggItem_Id", "dbo.DelAggItems");
            DropIndex("dbo.DelAggVariants", new[] { "DelAggItem_Id" });
            DropIndex("dbo.DelAggVariantGroups", new[] { "DelAggItem_Id" });
            DropColumn("dbo.DelAggVariantGroups", "DelAggItem_Id");
            DropColumn("dbo.DelAggVariants", "DelAggItem_Id");
        }
    }
}
