namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updated_PaymentType_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentTypes", "Departments", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaymentTypes", "Departments");
        }
    }
}
