namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_PurchaseYieldPercentage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Materials", "YieldPercentage", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.InvoiceDetails", "YieldPercentage", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.InvoiceDetails", "YieldMode", c => c.String());
            AddColumn("dbo.InvoiceDetails", "YieldExcessShortageQty", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Invoices", "AdjustmentRefId", c => c.Int());
            AddColumn("dbo.SupplierMaterials", "YieldPercentage", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.InwardDirectCreditDetails", "YieldPercentage", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.InwardDirectCreditDetails", "YieldMode", c => c.String());
            AddColumn("dbo.InwardDirectCreditDetails", "YieldExcessShortageQty", c => c.Decimal(precision: 18, scale: 2));
            CreateIndex("dbo.Invoices", "AdjustmentRefId");
            AddForeignKey("dbo.Invoices", "AdjustmentRefId", "dbo.Adjustments", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Invoices", "AdjustmentRefId", "dbo.Adjustments");
            DropIndex("dbo.Invoices", new[] { "AdjustmentRefId" });
            DropColumn("dbo.InwardDirectCreditDetails", "YieldExcessShortageQty");
            DropColumn("dbo.InwardDirectCreditDetails", "YieldMode");
            DropColumn("dbo.InwardDirectCreditDetails", "YieldPercentage");
            DropColumn("dbo.SupplierMaterials", "YieldPercentage");
            DropColumn("dbo.Invoices", "AdjustmentRefId");
            DropColumn("dbo.InvoiceDetails", "YieldExcessShortageQty");
            DropColumn("dbo.InvoiceDetails", "YieldMode");
            DropColumn("dbo.InvoiceDetails", "YieldPercentage");
            DropColumn("dbo.Materials", "YieldPercentage");
        }
    }
}
