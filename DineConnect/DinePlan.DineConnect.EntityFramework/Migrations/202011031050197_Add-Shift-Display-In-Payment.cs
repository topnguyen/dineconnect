namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddShiftDisplayInPayment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentTypes", "DisplayInShift", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaymentTypes", "DisplayInShift");
        }
    }
}
