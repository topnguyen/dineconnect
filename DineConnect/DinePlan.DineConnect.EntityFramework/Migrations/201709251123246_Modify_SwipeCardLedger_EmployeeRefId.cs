namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_SwipeCardLedger_EmployeeRefId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SwipeCardLedgers", "EmployeeRefId", c => c.Int());
            CreateIndex("dbo.SwipeCardLedgers", "EmployeeRefId");
            AddForeignKey("dbo.SwipeCardLedgers", "EmployeeRefId", "dbo.EmployeeInfos", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SwipeCardLedgers", "EmployeeRefId", "dbo.EmployeeInfos");
            DropIndex("dbo.SwipeCardLedgers", new[] { "EmployeeRefId" });
            DropColumn("dbo.SwipeCardLedgers", "EmployeeRefId");
        }
    }
}
