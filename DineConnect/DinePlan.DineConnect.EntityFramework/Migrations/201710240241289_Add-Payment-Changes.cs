namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPaymentChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PriceTags", "Locations", c => c.String());
            AddColumn("dbo.PaymentTypes", "Processors", c => c.String());
            DropColumn("dbo.MenuItems", "RefLocation");
            DropColumn("dbo.PaymentTypes", "Locations");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PaymentTypes", "Locations", c => c.String());
            AddColumn("dbo.MenuItems", "RefLocation", c => c.Int(nullable: false));
            DropColumn("dbo.PaymentTypes", "Processors");
            DropColumn("dbo.PriceTags", "Locations");
        }
    }
}
