namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveCombinePromotion : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Promotions", "CombinePro");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Promotions", "CombinePro", c => c.Boolean(nullable: false));
        }
    }
}
