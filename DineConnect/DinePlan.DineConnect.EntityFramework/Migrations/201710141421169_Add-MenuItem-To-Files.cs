namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMenuItemToFiles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItems", "Files", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MenuItems", "Files");
        }
    }
}
