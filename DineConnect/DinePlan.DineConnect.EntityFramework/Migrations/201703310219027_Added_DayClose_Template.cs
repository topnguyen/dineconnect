namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_DayClose_Template : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DayCloseTemplates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        ClosedDate = c.DateTime(nullable: false),
                        TableName = c.String(nullable: false, maxLength: 100),
                        TemplateType = c.Int(nullable: false),
                        TemplateData = c.String(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DayCloseTemplate_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DayCloseTemplate_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId, cascadeDelete: true)
                .Index(t => t.LocationRefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DayCloseTemplates", "LocationRefId", "dbo.Locations");
            DropIndex("dbo.DayCloseTemplates", new[] { "LocationRefId" });
            DropTable("dbo.DayCloseTemplates",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DayCloseTemplate_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DayCloseTemplate_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }

    }
}
