namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_MaterialAvgRate_Settings : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LocationWiseMaterialRateViews",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(),
                        MaterialRefId = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        AveragePriceTagRefId = c.Int(nullable: false),
                        BilledQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DefaultUnitId = c.Int(nullable: false),
                        NetAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AvgRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RefreshRequired = c.Boolean(nullable: false),
                        LastUpdatedTime = c.DateTime(nullable: false),
                        InvoiceDateRange = c.String(),
                        IsOmitFOCMaterialQuantities = c.Boolean(nullable: false),
                        IsCalculateAverageRateWithoutTax = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LocationWiseMaterialRateView_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_LocationWiseMaterialRateView_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => new { t.LocationRefId, t.MaterialRefId, t.AveragePriceTagRefId }, unique: true, name: "MPK_LocationRefId_MaterialRefId_AveragePriceTagRefId")
                .Index(t => t.TenantId);
            
            AddColumn("dbo.Materials", "AveragePriceTagRefId", c => c.Int(nullable: false));
            AddColumn("dbo.Materials", "NoOfMonthAvgTaken", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LocationWiseMaterialRateViews", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.LocationWiseMaterialRateViews", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.LocationWiseMaterialRateViews", "LocationRefId", "dbo.Locations");
            DropIndex("dbo.LocationWiseMaterialRateViews", new[] { "TenantId" });
            DropIndex("dbo.LocationWiseMaterialRateViews", "MPK_LocationRefId_MaterialRefId_AveragePriceTagRefId");
            DropColumn("dbo.Materials", "NoOfMonthAvgTaken");
            DropColumn("dbo.Materials", "AveragePriceTagRefId");
            DropTable("dbo.LocationWiseMaterialRateViews",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LocationWiseMaterialRateView_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_LocationWiseMaterialRateView_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
