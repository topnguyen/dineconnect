namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SortOrderForMenuItems : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ScreenMenuCategories", "SortOrder", c => c.Int(nullable: false));
            AddColumn("dbo.ScreenMenuItems", "SortOrder", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ScreenMenuItems", "SortOrder");
            DropColumn("dbo.ScreenMenuCategories", "SortOrder");
        }
    }
}
