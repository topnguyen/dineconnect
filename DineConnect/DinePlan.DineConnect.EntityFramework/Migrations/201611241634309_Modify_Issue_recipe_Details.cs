namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Issue_recipe_Details : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Issues", "RecipeRefId", "dbo.Materials");
            DropIndex("dbo.Issues", new[] { "RecipeRefId" });
            AddColumn("dbo.IssueRecipeDetails", "UnitRefId", c => c.Int(nullable: false));
            DropColumn("dbo.Issues", "RecipeRefId");
            DropColumn("dbo.Issues", "RecipeProductionQty");
            DropColumn("dbo.Issues", "MultipleBatchProductionAllowed");
            DropColumn("dbo.IssueRecipeDetails", "ProductionUnitRefId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.IssueRecipeDetails", "ProductionUnitRefId", c => c.Int(nullable: false));
            AddColumn("dbo.Issues", "MultipleBatchProductionAllowed", c => c.Boolean(nullable: false));
            AddColumn("dbo.Issues", "RecipeProductionQty", c => c.Decimal(precision: 18, scale: 6));
            AddColumn("dbo.Issues", "RecipeRefId", c => c.Int());
            DropColumn("dbo.IssueRecipeDetails", "UnitRefId");
            CreateIndex("dbo.Issues", "RecipeRefId");
            AddForeignKey("dbo.Issues", "RecipeRefId", "dbo.Materials", "Id");
        }
    }
}
