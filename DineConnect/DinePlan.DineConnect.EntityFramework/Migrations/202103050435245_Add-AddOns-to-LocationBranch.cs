namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAddOnstoLocationBranch : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LocationBranches", "AddOns", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.LocationBranches", "AddOns");
        }
    }
}
