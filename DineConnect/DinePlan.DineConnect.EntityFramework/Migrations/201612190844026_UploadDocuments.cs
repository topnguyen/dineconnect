namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class UploadDocuments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UploadDocumentComments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Comments = c.String(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                        UploadDocument_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UploadDocuments", t => t.UploadDocument_Id)
                .Index(t => t.UploadDocument_Id);
            
            CreateTable(
                "dbo.UploadDocuments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SupplierId = c.Int(),
                        LocationId = c.Int(nullable: false),
                        InvoiceId = c.Int(),
                        DocumentStatus = c.Int(nullable: false),
                        Url = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UploadDocument_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_UploadDocument_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Invoices", t => t.InvoiceId)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .ForeignKey("dbo.Suppliers", t => t.SupplierId)
                .Index(t => t.SupplierId)
                .Index(t => t.LocationId)
                .Index(t => t.InvoiceId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UploadDocuments", "SupplierId", "dbo.Suppliers");
            DropForeignKey("dbo.UploadDocuments", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.UploadDocuments", "InvoiceId", "dbo.Invoices");
            DropForeignKey("dbo.UploadDocumentComments", "UploadDocument_Id", "dbo.UploadDocuments");
            DropIndex("dbo.UploadDocuments", new[] { "InvoiceId" });
            DropIndex("dbo.UploadDocuments", new[] { "LocationId" });
            DropIndex("dbo.UploadDocuments", new[] { "SupplierId" });
            DropIndex("dbo.UploadDocumentComments", new[] { "UploadDocument_Id" });
            DropTable("dbo.UploadDocuments",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UploadDocument_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_UploadDocument_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.UploadDocumentComments");
        }
    }
}
