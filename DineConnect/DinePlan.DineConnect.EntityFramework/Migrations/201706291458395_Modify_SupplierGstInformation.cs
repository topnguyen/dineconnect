namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_SupplierGstInformation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SupplierGSTInformations", "GstStatus", c => c.Int());
            AlterColumn("dbo.SupplierGSTInformations", "ConstitutionStatus", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SupplierGSTInformations", "ConstitutionStatus", c => c.Int(nullable: false));
            AlterColumn("dbo.SupplierGSTInformations", "GstStatus", c => c.Int(nullable: false));
        }
    }
}
