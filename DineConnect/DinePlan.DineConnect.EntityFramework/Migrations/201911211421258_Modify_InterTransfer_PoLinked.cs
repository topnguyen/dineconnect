namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_InterTransfer_PoLinked : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InterTransfers", "ParentRequestRefId", c => c.Int());
            AddColumn("dbo.PurchaseOrders", "PoBasedOnInterTransferRequest", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PurchaseOrders", "PoBasedOnInterTransferRequest");
            DropColumn("dbo.InterTransfers", "ParentRequestRefId");
        }
    }
}
