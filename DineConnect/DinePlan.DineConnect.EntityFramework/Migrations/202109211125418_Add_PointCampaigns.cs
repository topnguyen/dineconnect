namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Add_PointCampaigns : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EngagePointCampaign",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        OfferedPoints = c.Int(nullable: false),
                        RequiredAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PointCampaign_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PointCampaign_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EngagePoinCampaignModifier",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CampaignId = c.Int(nullable: false),
                        ModifierTypeId = c.Int(nullable: false),
                        ModifyingFactor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StartDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        MembershipTierId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EngagePointCampaign", t => t.CampaignId, cascadeDelete: true)
                .ForeignKey("dbo.EngageMembershipTiers", t => t.MembershipTierId)
                .ForeignKey("dbo.EngagePoinCampaignModifierType", t => t.ModifierTypeId, cascadeDelete: true)
                .Index(t => t.CampaignId)
                .Index(t => t.ModifierTypeId)
                .Index(t => t.MembershipTierId);
            
            CreateTable(
                "dbo.EngagePoinCampaignModifierType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EngagePoinCampaignModifier", "ModifierTypeId", "dbo.EngagePoinCampaignModifierType");
            DropForeignKey("dbo.EngagePoinCampaignModifier", "MembershipTierId", "dbo.EngageMembershipTiers");
            DropForeignKey("dbo.EngagePoinCampaignModifier", "CampaignId", "dbo.EngagePointCampaign");
            DropIndex("dbo.EngagePoinCampaignModifier", new[] { "MembershipTierId" });
            DropIndex("dbo.EngagePoinCampaignModifier", new[] { "ModifierTypeId" });
            DropIndex("dbo.EngagePoinCampaignModifier", new[] { "CampaignId" });
            DropTable("dbo.EngagePoinCampaignModifierType");
            DropTable("dbo.EngagePoinCampaignModifier");
            DropTable("dbo.EngagePointCampaign",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PointCampaign_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PointCampaign_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
