namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Location_AvgPrice_Location : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "AvgPriceLocationRefId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "AvgPriceLocationRefId");
        }
    }
}
