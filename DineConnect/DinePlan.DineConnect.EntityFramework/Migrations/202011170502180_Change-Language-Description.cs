namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeLanguageDescription : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LanguageDescriptions", "Name", c => c.String());
            DropColumn("dbo.LanguageDescriptions", "ProductName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LanguageDescriptions", "ProductName", c => c.String());
            DropColumn("dbo.LanguageDescriptions", "Name");
        }
    }
}
