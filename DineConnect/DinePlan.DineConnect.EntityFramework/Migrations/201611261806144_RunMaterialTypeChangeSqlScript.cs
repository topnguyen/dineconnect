namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RunMaterialTypeChangeSqlScript : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE Materials SET MaterialTypeId = 1 WHERE MaterialTypeId = 2 ");

        }
        
        public override void Down()
        {

        }
    }
}
