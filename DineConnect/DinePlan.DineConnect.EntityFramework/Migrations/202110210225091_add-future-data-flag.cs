namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addfuturedataflag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PriceTags", "FutureData", c => c.Boolean(nullable: false));
            AddColumn("dbo.ScreenMenus", "FutureData", c => c.Boolean(nullable: false));
            AddColumn("dbo.DinePlanTaxes", "FutureData", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DinePlanTaxes", "FutureData");
            DropColumn("dbo.ScreenMenus", "FutureData");
            DropColumn("dbo.PriceTags", "FutureData");
        }
    }
}
