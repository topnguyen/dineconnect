namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAddontoMenuItem : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItems", "AddOns", c => c.String());
            DropColumn("dbo.MenuItems", "StarItem");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MenuItems", "StarItem", c => c.Boolean(nullable: false));
            DropColumn("dbo.MenuItems", "AddOns");
        }
    }
}
