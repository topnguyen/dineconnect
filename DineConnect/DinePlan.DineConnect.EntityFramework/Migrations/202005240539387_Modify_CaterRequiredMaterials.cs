namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_CaterRequiredMaterials : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CaterQuoteRequiredMaterials",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CaterQuoteHeaderId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        TotalQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitRefId = c.Int(nullable: false),
                        Remarks = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CaterQuoteRequiredMaterial_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CaterQuoteRequiredMaterial_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CaterQuotationHeaders", t => t.CaterQuoteHeaderId)
                .Index(t => t.CaterQuoteHeaderId);
            
            CreateTable(
                "dbo.CaterQuoteRequiredMaterialVsMenuLinks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CaterQuoteHeaderId = c.Int(nullable: false),
                        CaterQuoteRequiredMaterialDataRefId = c.Int(nullable: false),
                        PosMenuPortionRefId = c.Int(nullable: false),
                        MenuQuantitySold = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        PortionPerUnitOfSales = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PortionQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PortionUnitId = c.Int(nullable: false),
                        DefaultUnitId = c.Int(nullable: false),
                        WastageExpected = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AutoSalesDeduction = c.Boolean(nullable: false),
                        NoOfPax = c.Int(nullable: false),
                        TotalSaleQuantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CaterQuoteRequiredMaterialVsMenuLink_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CaterQuoteRequiredMaterialVsMenuLink_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CaterQuotationHeaders", t => t.CaterQuoteHeaderId)
                .ForeignKey("dbo.CaterQuoteRequiredMaterials", t => t.CaterQuoteRequiredMaterialDataRefId)
                .Index(t => t.CaterQuoteHeaderId)
                .Index(t => t.CaterQuoteRequiredMaterialDataRefId);
            
            CreateTable(
                "dbo.CaterQuoteRequiredMaterialVsPurchaseOrderLinks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CaterQuoteRequiredMaterialDataRefId = c.Int(nullable: false),
                        PurchaseOrderRefId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CaterQuoteRequiredMaterialVsPurchaseOrderLink_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CaterQuoteRequiredMaterialVsPurchaseOrderLink_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CaterQuoteRequiredMaterials", t => t.CaterQuoteRequiredMaterialDataRefId)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderRefId)
                .Index(t => t.CaterQuoteRequiredMaterialDataRefId)
                .Index(t => t.PurchaseOrderRefId);
            
            AddColumn("dbo.CaterQuotationHeaders", "DoesPOCreatedForThisCateringOrder", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CaterQuoteRequiredMaterialVsPurchaseOrderLinks", "PurchaseOrderRefId", "dbo.PurchaseOrders");
            DropForeignKey("dbo.CaterQuoteRequiredMaterialVsPurchaseOrderLinks", "CaterQuoteRequiredMaterialDataRefId", "dbo.CaterQuoteRequiredMaterials");
            DropForeignKey("dbo.CaterQuoteRequiredMaterialVsMenuLinks", "CaterQuoteRequiredMaterialDataRefId", "dbo.CaterQuoteRequiredMaterials");
            DropForeignKey("dbo.CaterQuoteRequiredMaterialVsMenuLinks", "CaterQuoteHeaderId", "dbo.CaterQuotationHeaders");
            DropForeignKey("dbo.CaterQuoteRequiredMaterials", "CaterQuoteHeaderId", "dbo.CaterQuotationHeaders");
            DropIndex("dbo.CaterQuoteRequiredMaterialVsPurchaseOrderLinks", new[] { "PurchaseOrderRefId" });
            DropIndex("dbo.CaterQuoteRequiredMaterialVsPurchaseOrderLinks", new[] { "CaterQuoteRequiredMaterialDataRefId" });
            DropIndex("dbo.CaterQuoteRequiredMaterialVsMenuLinks", new[] { "CaterQuoteRequiredMaterialDataRefId" });
            DropIndex("dbo.CaterQuoteRequiredMaterialVsMenuLinks", new[] { "CaterQuoteHeaderId" });
            DropIndex("dbo.CaterQuoteRequiredMaterials", new[] { "CaterQuoteHeaderId" });
            DropColumn("dbo.CaterQuotationHeaders", "DoesPOCreatedForThisCateringOrder");
            DropTable("dbo.CaterQuoteRequiredMaterialVsPurchaseOrderLinks",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CaterQuoteRequiredMaterialVsPurchaseOrderLink_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CaterQuoteRequiredMaterialVsPurchaseOrderLink_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.CaterQuoteRequiredMaterialVsMenuLinks",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CaterQuoteRequiredMaterialVsMenuLink_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CaterQuoteRequiredMaterialVsMenuLink_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.CaterQuoteRequiredMaterials",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CaterQuoteRequiredMaterial_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CaterQuoteRequiredMaterial_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
