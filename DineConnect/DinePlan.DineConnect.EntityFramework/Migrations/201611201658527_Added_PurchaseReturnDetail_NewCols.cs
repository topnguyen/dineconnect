namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_PurchaseReturnDetail_NewCols : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchaseReturnDetails", "Sno", c => c.Int(nullable: false));
            AddColumn("dbo.PurchaseReturns", "LocationRefId", c => c.Int(nullable: false));
            AddColumn("dbo.PurchaseReturns", "PurchaseReturnAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            CreateIndex("dbo.PurchaseReturns", "LocationRefId");
            AddForeignKey("dbo.PurchaseReturns", "LocationRefId", "dbo.Locations", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PurchaseReturns", "LocationRefId", "dbo.Locations");
            DropIndex("dbo.PurchaseReturns", new[] { "LocationRefId" });
            DropColumn("dbo.PurchaseReturns", "PurchaseReturnAmount");
            DropColumn("dbo.PurchaseReturns", "LocationRefId");
            DropColumn("dbo.PurchaseReturnDetails", "Sno");
        }
    }
}
