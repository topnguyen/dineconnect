namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updated_type_Of_Active_DeliveryAggPlatform : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DeliveryAggPlatforms", "Active", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DeliveryAggPlatforms", "Active", c => c.Int(nullable: false));
        }
    }
}
