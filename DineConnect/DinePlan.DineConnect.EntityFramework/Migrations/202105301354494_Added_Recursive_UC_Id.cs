namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Recursive_UC_Id : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UnitConversions", "ReferenceRecursiveId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UnitConversions", "ReferenceRecursiveId");
        }
    }
}
