namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConnectCardActive : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConnectCards", "Active", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ConnectCards", "Active");
        }
    }
}
