namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveEmployeeTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EmployeeAttendances", "AuthorisedBy", "dbo.PersonalInformations");
            DropForeignKey("dbo.EmployeeAttendances", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.EmployeeAttendances", "EmployeeRefId", "dbo.PersonalInformations");
            DropIndex("dbo.EmployeeAttendances", new[] { "LocationRefId" });
            DropIndex("dbo.EmployeeAttendances", new[] { "EmployeeRefId" });
            DropIndex("dbo.EmployeeAttendances", new[] { "AuthorisedBy" });
            DropTable("dbo.EmployeeAttendances",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeAttendance_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeAttendance_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.EmployeeAttendances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        EmployeeRefId = c.Int(nullable: false),
                        CheckTime = c.DateTime(nullable: false),
                        CheckType = c.Int(nullable: false),
                        VerifyMode = c.Int(nullable: false),
                        AuthorisedBy = c.Int(),
                        TenantId = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeAttendance_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeAttendance_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.EmployeeAttendances", "AuthorisedBy");
            CreateIndex("dbo.EmployeeAttendances", "EmployeeRefId");
            CreateIndex("dbo.EmployeeAttendances", "LocationRefId");
            AddForeignKey("dbo.EmployeeAttendances", "EmployeeRefId", "dbo.PersonalInformations", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EmployeeAttendances", "LocationRefId", "dbo.Locations", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EmployeeAttendances", "AuthorisedBy", "dbo.PersonalInformations", "Id");
        }
    }
}
