namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAddOnToCustomer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "AddOn", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "AddOn");
        }
    }
}
