namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrderDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "MenuItemType", c => c.Int(nullable: false));
            AddColumn("dbo.Orders", "OrderRef", c => c.String());
            AddColumn("dbo.Orders", "IsParent", c => c.Boolean(nullable: false));
            AddColumn("dbo.Orders", "IsUpSelling", c => c.Boolean(nullable: false));
            AddColumn("dbo.Orders", "UpSellingOrderRef", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "UpSellingOrderRef");
            DropColumn("dbo.Orders", "IsUpSelling");
            DropColumn("dbo.Orders", "IsParent");
            DropColumn("dbo.Orders", "OrderRef");
            DropColumn("dbo.Orders", "MenuItemType");
        }
    }
}
