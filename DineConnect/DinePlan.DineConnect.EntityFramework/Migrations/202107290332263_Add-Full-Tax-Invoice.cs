namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFullTaxInvoice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FullTaxInvoiceCopyCounts", "CopyPrintTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.FullTaxInvoiceCopyCounts", "CopyPrintUser", c => c.String());
            AddColumn("dbo.FullTaxInvoiceCopyCounts", "Reason", c => c.String());
            AddColumn("dbo.FullTaxInvoiceCopyCounts", "IsCopy", c => c.Boolean(nullable: false));
            AddColumn("dbo.FullTaxInvoiceCopyCounts", "Name", c => c.String());
            AddColumn("dbo.FullTaxInvoices", "Reason", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FullTaxInvoices", "Reason");
            DropColumn("dbo.FullTaxInvoiceCopyCounts", "Name");
            DropColumn("dbo.FullTaxInvoiceCopyCounts", "IsCopy");
            DropColumn("dbo.FullTaxInvoiceCopyCounts", "Reason");
            DropColumn("dbo.FullTaxInvoiceCopyCounts", "CopyPrintUser");
            DropColumn("dbo.FullTaxInvoiceCopyCounts", "CopyPrintTime");
        }
    }
}
