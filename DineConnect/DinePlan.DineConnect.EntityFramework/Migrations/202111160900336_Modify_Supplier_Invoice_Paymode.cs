namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Supplier_Invoice_Paymode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Suppliers", "DefaultPayModeEnumRefId", c => c.Int(nullable: false));
            AddColumn("dbo.Invoices", "InvoicePayMode", c => c.Int(nullable: false));
            Sql("Update dbo.Suppliers Set DefaultPayModeEnumRefId = 1");
            Sql("Update dbo.Invoices Set InvoicePayMode = 1 Where Upper(InvoiceMode)='CASH'");
            Sql("Update dbo.Invoices Set InvoicePayMode = 2 Where Upper(InvoiceMode)='CREDIT'");
            DropColumn("dbo.Invoices", "InvoiceMode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Invoices", "InvoiceMode", c => c.String(nullable: false, maxLength: 150));
            DropColumn("dbo.Invoices", "InvoicePayMode");
            DropColumn("dbo.Suppliers", "DefaultPayModeEnumRefId");
        }
    }
}
