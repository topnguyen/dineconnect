namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncreaseTicketNumberSizeSwipeLedger : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SwipeCardLedgers", "TicketNumber", c => c.String(maxLength: 30));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SwipeCardLedgers", "TicketNumber", c => c.String(maxLength: 15));
        }
    }
}
