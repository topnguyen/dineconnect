namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyYieldSno : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.YieldInput", "Sno", c => c.Int(nullable: false));
            AddColumn("dbo.YieldOutput", "Sno", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.YieldOutput", "Sno");
            DropColumn("dbo.YieldInput", "Sno");
        }
    }
}
