namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddVoucherSales : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ScreenMenuItems", "VoucherItem", c => c.Boolean(nullable: false));
            AddColumn("dbo.ScreenMenuItems", "ConnectCardTypeId", c => c.Int());
            CreateIndex("dbo.ScreenMenuItems", "ConnectCardTypeId");
            AddForeignKey("dbo.ScreenMenuItems", "ConnectCardTypeId", "dbo.ConnectCardTypes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScreenMenuItems", "ConnectCardTypeId", "dbo.ConnectCardTypes");
            DropIndex("dbo.ScreenMenuItems", new[] { "ConnectCardTypeId" });
            DropColumn("dbo.ScreenMenuItems", "ConnectCardTypeId");
            DropColumn("dbo.ScreenMenuItems", "VoucherItem");
        }
    }
}
