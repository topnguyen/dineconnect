namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveMenuItemTax : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MenuItemTaxDetails", "DinePlanTaxId", "dbo.DinePlanTaxes");
            DropForeignKey("dbo.MenuItemTaxDetails", "MenuItemId", "dbo.MenuItems");
            DropIndex("dbo.MenuItemTaxDetails", new[] { "DinePlanTaxId" });
            DropIndex("dbo.MenuItemTaxDetails", new[] { "MenuItemId" });
            DropTable("dbo.MenuItemTaxDetails");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.MenuItemTaxDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DinePlanTaxId = c.Int(nullable: false),
                        MenuItemId = c.Int(nullable: false),
                        ChangeValue = c.Boolean(nullable: false),
                        TaxValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.MenuItemTaxDetails", "MenuItemId");
            CreateIndex("dbo.MenuItemTaxDetails", "DinePlanTaxId");
            AddForeignKey("dbo.MenuItemTaxDetails", "MenuItemId", "dbo.MenuItems", "Id", cascadeDelete: true);
            AddForeignKey("dbo.MenuItemTaxDetails", "DinePlanTaxId", "dbo.DinePlanTaxes", "Id", cascadeDelete: true);
        }
    }
}
