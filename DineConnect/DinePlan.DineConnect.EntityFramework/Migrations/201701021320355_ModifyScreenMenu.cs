namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyScreenMenu : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ScreenMenuItems", "ScreenMenuCategory_Id", "dbo.ScreenMenuCategories");
            DropForeignKey("dbo.ScreenMenuCategories", "ScreenMenu_Id", "dbo.ScreenMenus");
            DropIndex("dbo.ScreenMenuCategories", new[] { "ScreenMenu_Id" });
            DropIndex("dbo.ScreenMenuItems", new[] { "ScreenMenuCategory_Id" });
            RenameColumn(table: "dbo.ScreenMenuItems", name: "ScreenMenuCategory_Id", newName: "ScreenCategoryId");
            RenameColumn(table: "dbo.ScreenMenuCategories", name: "ScreenMenu_Id", newName: "ScreenMenuId");
            AlterColumn("dbo.ScreenMenuCategories", "ScreenMenuId", c => c.Int(nullable: false));
            AlterColumn("dbo.ScreenMenuItems", "ScreenCategoryId", c => c.Int(nullable: false));
            CreateIndex("dbo.ScreenMenuCategories", "ScreenMenuId");
            CreateIndex("dbo.ScreenMenuItems", "ScreenCategoryId");
            AddForeignKey("dbo.ScreenMenuItems", "ScreenCategoryId", "dbo.ScreenMenuCategories", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ScreenMenuCategories", "ScreenMenuId", "dbo.ScreenMenus", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScreenMenuCategories", "ScreenMenuId", "dbo.ScreenMenus");
            DropForeignKey("dbo.ScreenMenuItems", "ScreenCategoryId", "dbo.ScreenMenuCategories");
            DropIndex("dbo.ScreenMenuItems", new[] { "ScreenCategoryId" });
            DropIndex("dbo.ScreenMenuCategories", new[] { "ScreenMenuId" });
            AlterColumn("dbo.ScreenMenuItems", "ScreenCategoryId", c => c.Int());
            AlterColumn("dbo.ScreenMenuCategories", "ScreenMenuId", c => c.Int());
            RenameColumn(table: "dbo.ScreenMenuCategories", name: "ScreenMenuId", newName: "ScreenMenu_Id");
            RenameColumn(table: "dbo.ScreenMenuItems", name: "ScreenCategoryId", newName: "ScreenMenuCategory_Id");
            CreateIndex("dbo.ScreenMenuItems", "ScreenMenuCategory_Id");
            CreateIndex("dbo.ScreenMenuCategories", "ScreenMenu_Id");
            AddForeignKey("dbo.ScreenMenuCategories", "ScreenMenu_Id", "dbo.ScreenMenus", "Id");
            AddForeignKey("dbo.ScreenMenuItems", "ScreenMenuCategory_Id", "dbo.ScreenMenuCategories", "Id");
        }
    }
}
