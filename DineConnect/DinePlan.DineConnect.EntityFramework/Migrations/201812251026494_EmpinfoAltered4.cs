namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmpinfoAltered4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmployeeInfos", "Wage", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmployeeInfos", "Wage");
        }
    }
}
