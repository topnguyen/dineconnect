using System.Configuration;
using System.Data.Entity;
using System.Linq;

namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveCustomSetting : DbMigration
    {
        public override void Up()
        {
            if (Exists("dbo.CustomSettings"))
            {
                DropTable("dbo.CustomSettings",
                    removedAnnotations: new Dictionary<string, object>
                    {
                        { "DynamicFilter_CustomSetting_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                        { "DynamicFilter_CustomSetting_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    });
            }
        }

     
       
        
        public override void Down()
        {
            CreateTable(
                "dbo.CustomSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        MaxRePrint = c.Int(nullable: false),
                        AutoGenerate = c.Boolean(nullable: false),
                        GenerateDays = c.Int(nullable: false),
                        MaxVoid = c.Int(nullable: false),
                        PrintSummary = c.Boolean(nullable: false),
                        Form = c.String(),
                        UpdateDay = c.Int(nullable: false),
                        CentralFolder = c.String(),
                        LocalFolder = c.String(),
                        WebUrl = c.String(),
                        PrintType = c.String(),
                        Locations = c.String(),
                        NonLocations = c.String(),
                        Group = c.Boolean(nullable: false),
                        LocationTag = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CustomSetting_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CustomSetting_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
        }

        private static bool Exists(string tableName)
        {
            using (var context = new DbContext(ConfigurationManager.ConnectionStrings["Default"].ConnectionString))
            {
                var count = context.Database.SqlQuery<int>("SELECT COUNT(OBJECT_ID(@p0, 'U'))", tableName);

                return count.Any() && count.First() > 0;
            }
        }

    }
}
