namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_files_col_to_PaymentType_tb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentTypes", "Files", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaymentTypes", "Files");
        }
    }
}
