namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPrefixVoucherType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GiftVoucherTypes", "Prefix", c => c.String());
            AddColumn("dbo.GiftVoucherTypes", "Suffix", c => c.String());
            AddColumn("dbo.GiftVoucherTypes", "VoucherDigit", c => c.Int(nullable: false));
            DropColumn("dbo.GiftVoucherTypes", "VoucherFormat");
            DropColumn("dbo.GiftVoucherTypes", "StartingNumber");
        }
        
        public override void Down()
        {
            AddColumn("dbo.GiftVoucherTypes", "StartingNumber", c => c.Int(nullable: false));
            AddColumn("dbo.GiftVoucherTypes", "VoucherFormat", c => c.String());
            DropColumn("dbo.GiftVoucherTypes", "VoucherDigit");
            DropColumn("dbo.GiftVoucherTypes", "Suffix");
            DropColumn("dbo.GiftVoucherTypes", "Prefix");
        }
    }
}
