namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddTicketHistory : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TicketMessagesHistory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TicketMessageReplyId = c.Int(nullable: false),
                        DepartmentId = c.Int(nullable: false),
                        MessageStatusId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessageHistory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TillAccounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TillAccountTypeId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        Locations = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TillAccount_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TillAccount_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TillTransactions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WorkTimeId = c.Int(nullable: false),
                        Remarks = c.String(),
                        TotalAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TillAccountId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TillTransaction_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TillTransaction_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TillAccounts", t => t.TillAccountId, cascadeDelete: true)
                .Index(t => t.TillAccountId);
            
            AddColumn("dbo.Departments", "TagId", c => c.Int());
            CreateIndex("dbo.Departments", "TagId");
            AddForeignKey("dbo.Departments", "TagId", "dbo.PriceTags", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TillTransactions", "TillAccountId", "dbo.TillAccounts");
            DropForeignKey("dbo.Departments", "TagId", "dbo.PriceTags");
            DropIndex("dbo.TillTransactions", new[] { "TillAccountId" });
            DropIndex("dbo.Departments", new[] { "TagId" });
            DropColumn("dbo.Departments", "TagId");
            DropTable("dbo.TillTransactions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TillTransaction_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TillTransaction_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TillAccounts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TillAccount_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TillAccount_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TicketMessagesHistory",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessageHistory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
