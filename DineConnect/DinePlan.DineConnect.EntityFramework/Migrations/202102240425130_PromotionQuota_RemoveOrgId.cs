namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class PromotionQuota_RemoveOrgId : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.PromotionQuotas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PromotionId = c.Int(nullable: false),
                        QuotaAmount = c.Int(nullable: false),
                        QuotaUsed = c.Int(nullable: false),
                        ResetType = c.Int(nullable: false),
                        ResetWeekValue = c.Int(),
                        ResetMonthValue = c.DateTime(),
                        LastResetDate = c.DateTime(),
                        PlantResetDate = c.DateTime(),
                        Plants = c.String(),
                        LocationQuota = c.String(),
                        Locations = c.String(),
                        NonLocations = c.String(),
                        Group = c.Boolean(nullable: false),
                        LocationTag = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_PromotionQuota_ConnectFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            DropColumn("dbo.PromotionQuotas", "Oid");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PromotionQuotas", "Oid", c => c.Int(nullable: false));
            AlterTableAnnotations(
                "dbo.PromotionQuotas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PromotionId = c.Int(nullable: false),
                        QuotaAmount = c.Int(nullable: false),
                        QuotaUsed = c.Int(nullable: false),
                        ResetType = c.Int(nullable: false),
                        ResetWeekValue = c.Int(),
                        ResetMonthValue = c.DateTime(),
                        LastResetDate = c.DateTime(),
                        PlantResetDate = c.DateTime(),
                        Plants = c.String(),
                        LocationQuota = c.String(),
                        Locations = c.String(),
                        NonLocations = c.String(),
                        Group = c.Boolean(nullable: false),
                        LocationTag = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_PromotionQuota_ConnectFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
        }
    }
}
