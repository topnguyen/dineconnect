namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Supplier_Add_Mpk_MaterialLocationWiseStock : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.MaterialLocationWiseStocks", new[] { "LocationRefId" });
            DropIndex("dbo.MaterialLocationWiseStocks", new[] { "MaterialRefId" });
            AddColumn("dbo.Suppliers", "TaxApplicable", c => c.Boolean(nullable: false));
            CreateIndex("dbo.MaterialLocationWiseStocks", new[] { "LocationRefId", "MaterialRefId" }, unique: true, name: "MPK_LocationRefId_MaterialRefId");
            Sql("Update dbo.Suppliers Set TaxApplicable = 1 Where LEN(TaxRegistrationNumber)>0");
        }
        
        public override void Down()
        {
            DropIndex("dbo.MaterialLocationWiseStocks", "MPK_LocationRefId_MaterialRefId");
            DropColumn("dbo.Suppliers", "TaxApplicable");
            CreateIndex("dbo.MaterialLocationWiseStocks", "MaterialRefId");
            CreateIndex("dbo.MaterialLocationWiseStocks", "LocationRefId");
        }
    }
}
