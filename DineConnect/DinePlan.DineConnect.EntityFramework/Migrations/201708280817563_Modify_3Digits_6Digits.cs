namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_3Digits_6Digits : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AdjustmentDetails", "AdjustmentQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.AdjustmentDetails", "StockEntry", c => c.Decimal(precision: 18, scale: 6));
            AlterColumn("dbo.AdjustmentDetails", "ClosingStock", c => c.Decimal(precision: 18, scale: 6));
            AlterColumn("dbo.UnitConversions", "Conversion", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.ClosingStockDetails", "OnHand", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialLedgers", "OpenBalance", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialLedgers", "Received", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialLedgers", "Issued", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialLedgers", "Sales", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialLedgers", "TransferIn", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialLedgers", "TransferOut", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialLedgers", "Damaged", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialLedgers", "SupplierReturn", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialLedgers", "ExcessReceived", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialLedgers", "Shortage", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialLedgers", "Return", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialLedgers", "ClBalance", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialLocationWiseStocks", "CurrentInHand", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialLocationWiseStocks", "MinimumStock", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialLocationWiseStocks", "MaximumStock", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialLocationWiseStocks", "ReOrderLevel", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.InterTransferDetails", "RequestQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.InterTransferDetails", "IssueQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.InterTransferReceivedDetails", "ReceivedQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.InterTransferReceivedDetails", "ReturnQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.InterTransferReceivedDetails", "AdjustmentQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.InvoiceDetails", "TotalQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.InwardDirectCreditDetails", "DcReceivedQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.InwardDirectCreditDetails", "DcConvertedQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.IssueDetails", "RequestQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.IssueDetails", "IssueQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialIngredients", "MaterialUsedQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialMenuMappings", "PortionQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialMenuMappings", "WastageExpected", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.ProductionDetails", "ProductionQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.ProductRecipeLinks", "PortionQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.ProductRecipeLinks", "WastageExpected", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.Recipes", "PrdBatchQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.PurchaseOrderDetails", "QtyOrdered", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.PurchaseOrderDetails", "QtyReceived", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.PurchaseReturnDetails", "Quantity", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.RecipeIngredients", "MaterialUsedQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.RequestDetails", "RequestQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.ReturnDetails", "ReturnQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.SalesDeliveryOrderDetails", "DeliveryQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.SalesDeliveryOrderDetails", "BillConvertedQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.SalesInvoiceDetails", "TotalQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.SalesOrderDetails", "QtyOrdered", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.SalesOrderDetails", "QtyReceived", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.YieldInput", "InputQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.Yield", "RecipeProductionQty", c => c.Decimal(precision: 18, scale: 6));
            AlterColumn("dbo.YieldOutput", "OutputQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.YieldOutput", "OutputQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.Yield", "RecipeProductionQty", c => c.Decimal(precision: 18, scale: 3));
            AlterColumn("dbo.YieldInput", "InputQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.SalesOrderDetails", "QtyReceived", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.SalesOrderDetails", "QtyOrdered", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.SalesInvoiceDetails", "TotalQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.SalesDeliveryOrderDetails", "BillConvertedQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.SalesDeliveryOrderDetails", "DeliveryQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.ReturnDetails", "ReturnQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.RequestDetails", "RequestQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.RecipeIngredients", "MaterialUsedQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.PurchaseReturnDetails", "Quantity", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.PurchaseOrderDetails", "QtyReceived", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.PurchaseOrderDetails", "QtyOrdered", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.Recipes", "PrdBatchQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.ProductRecipeLinks", "WastageExpected", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.ProductRecipeLinks", "PortionQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.ProductionDetails", "ProductionQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialMenuMappings", "WastageExpected", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialMenuMappings", "PortionQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialIngredients", "MaterialUsedQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.IssueDetails", "IssueQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.IssueDetails", "RequestQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.InwardDirectCreditDetails", "DcConvertedQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.InwardDirectCreditDetails", "DcReceivedQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.InvoiceDetails", "TotalQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.InterTransferReceivedDetails", "AdjustmentQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.InterTransferReceivedDetails", "ReturnQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.InterTransferReceivedDetails", "ReceivedQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.InterTransferDetails", "IssueQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.InterTransferDetails", "RequestQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialLocationWiseStocks", "ReOrderLevel", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialLocationWiseStocks", "MaximumStock", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialLocationWiseStocks", "MinimumStock", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialLocationWiseStocks", "CurrentInHand", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialLedgers", "ClBalance", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialLedgers", "Return", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialLedgers", "Shortage", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialLedgers", "ExcessReceived", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialLedgers", "SupplierReturn", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialLedgers", "Damaged", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialLedgers", "TransferOut", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialLedgers", "TransferIn", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialLedgers", "Sales", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialLedgers", "Issued", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialLedgers", "Received", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.MaterialLedgers", "OpenBalance", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.ClosingStockDetails", "OnHand", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.UnitConversions", "Conversion", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.AdjustmentDetails", "ClosingStock", c => c.Decimal(precision: 18, scale: 3));
            AlterColumn("dbo.AdjustmentDetails", "StockEntry", c => c.Decimal(precision: 18, scale: 3));
            AlterColumn("dbo.AdjustmentDetails", "AdjustmentQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
        }
    }
}
