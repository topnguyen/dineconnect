// <auto-generated />
namespace DinePlan.DineConnect.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class add_table_HouseNumbers : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(add_table_HouseNumbers));
        
        string IMigrationMetadata.Id
        {
            get { return "202107300831293_add_table_HouseNumbers"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
