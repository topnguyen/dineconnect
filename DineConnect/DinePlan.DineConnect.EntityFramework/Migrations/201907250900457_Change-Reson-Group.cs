namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeResonGroup : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlanReasons", "ReasonGroup", c => c.String());
            DropColumn("dbo.PlanReasons", "ReasonType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PlanReasons", "ReasonType", c => c.Int(nullable: false));
            DropColumn("dbo.PlanReasons", "ReasonGroup");
        }
    }
}
