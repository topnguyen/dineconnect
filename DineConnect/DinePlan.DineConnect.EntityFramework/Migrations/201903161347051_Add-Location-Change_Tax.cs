namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLocationChange_Tax : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "ChangeTax", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "ChangeTax");
        }
    }
}
