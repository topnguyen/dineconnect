namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeReceiptContent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReceiptContents", "Contents", c => c.String());
            DropColumn("dbo.ReceiptContents", "Header");
            DropColumn("dbo.ReceiptContents", "Footer");
            DropTable("dbo.SecondDisplays",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SecondDisplay_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SecondDisplay_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SecondDisplays",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Images = c.String(),
                        Videos = c.String(),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        LocationTag = c.Boolean(nullable: false),
                        NonLocations = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SecondDisplay_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SecondDisplay_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ReceiptContents", "Footer", c => c.String());
            AddColumn("dbo.ReceiptContents", "Header", c => c.String());
            DropColumn("dbo.ReceiptContents", "Contents");
        }
    }
}
