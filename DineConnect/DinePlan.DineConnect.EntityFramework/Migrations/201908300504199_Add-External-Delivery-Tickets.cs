namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddExternalDeliveryTickets : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExternalDeliveryTickets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationId = c.Int(nullable: false),
                        TicketNumber = c.String(maxLength: 30),
                        Status = c.String(maxLength: 2),
                        Claimed = c.Boolean(nullable: false),
                        DeliveryContents = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ExternalDeliveryTicket_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ExternalDeliveryTicket_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.LocationId, t.TicketNumber }, unique: true, name: "IX_ExternalDeliveryTicket");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.ExternalDeliveryTickets", "IX_ExternalDeliveryTicket");
            DropTable("dbo.ExternalDeliveryTickets",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ExternalDeliveryTicket_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ExternalDeliveryTicket_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
