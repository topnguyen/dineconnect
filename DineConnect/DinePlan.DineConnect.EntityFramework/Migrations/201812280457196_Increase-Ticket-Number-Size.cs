namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncreaseTicketNumberSize : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Tickets", "IX_FirstAndSecond");
            AlterColumn("dbo.Tickets", "TicketNumber", c => c.String(maxLength: 30));
            CreateIndex("dbo.Tickets", new[] { "LocationId", "TicketNumber" }, unique: true, name: "IX_FirstAndSecond");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tickets", "IX_FirstAndSecond");
            AlterColumn("dbo.Tickets", "TicketNumber", c => c.String(maxLength: 15));
            CreateIndex("dbo.Tickets", new[] { "LocationId", "TicketNumber" }, unique: true, name: "IX_FirstAndSecond");
        }
    }
}
