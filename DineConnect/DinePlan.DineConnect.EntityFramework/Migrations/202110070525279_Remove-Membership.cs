namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveMembership : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EngageMemberships", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.EngageMemberships", "TierId", "dbo.EngageMembershipTiers");
            DropIndex("dbo.EngageMemberships", new[] { "TierId" });
            DropIndex("dbo.EngageMemberships", new[] { "LocationId" });
            DropTable("dbo.EngageMemberships",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Membership_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Membership_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.EngageMemberships",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        TierId = c.Int(nullable: false),
                        ApplicationDate = c.DateTime(nullable: false),
                        ExpiryDate = c.DateTime(),
                        LocationId = c.Int(),
                        Active = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Membership_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Membership_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.EngageMemberships", "LocationId");
            CreateIndex("dbo.EngageMemberships", "TierId");
            AddForeignKey("dbo.EngageMemberships", "TierId", "dbo.EngageMembershipTiers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EngageMemberships", "LocationId", "dbo.Locations", "Id");
        }
    }
}
