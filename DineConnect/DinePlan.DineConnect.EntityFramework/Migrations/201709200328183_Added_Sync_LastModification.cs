namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Sync_LastModification : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Units", "SyncLastModification", c => c.DateTime());
            AddColumn("dbo.UnitConversions", "SyncLastModification", c => c.DateTime());
            AddColumn("dbo.Materials", "SyncLastModification", c => c.DateTime());
            AddColumn("dbo.MaterialGroupCategories", "SyncLastModification", c => c.DateTime());
            AddColumn("dbo.MaterialGroups", "SyncLastModification", c => c.DateTime());
            AddColumn("dbo.Suppliers", "SyncLastModification", c => c.DateTime());
            AddColumn("dbo.SupplierMaterials", "SyncLastModification", c => c.DateTime());
            AddColumn("dbo.Taxes", "SyncLastModification", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Taxes", "SyncLastModification");
            DropColumn("dbo.SupplierMaterials", "SyncLastModification");
            DropColumn("dbo.Suppliers", "SyncLastModification");
            DropColumn("dbo.MaterialGroups", "SyncLastModification");
            DropColumn("dbo.MaterialGroupCategories", "SyncLastModification");
            DropColumn("dbo.Materials", "SyncLastModification");
            DropColumn("dbo.UnitConversions", "SyncLastModification");
            DropColumn("dbo.Units", "SyncLastModification");
        }
    }
}
