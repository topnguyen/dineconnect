namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Returns_IssueRefId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Returns", "IssueRefId", c => c.Int());
            CreateIndex("dbo.Returns", "IssueRefId");
            AddForeignKey("dbo.Returns", "IssueRefId", "dbo.Issues", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Returns", "IssueRefId", "dbo.Issues");
            DropIndex("dbo.Returns", new[] { "IssueRefId" });
            DropColumn("dbo.Returns", "IssueRefId");
        }
    }
}
