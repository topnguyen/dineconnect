namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class UpsellingMenuItems : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UpMenuItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MenuItemId = c.Int(nullable: false),
                        RefMenuItemId = c.Int(nullable: false),
                        AddBaseProductPrice = c.Boolean(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MenuItems", t => t.MenuItemId, cascadeDelete: true)
                .Index(t => t.MenuItemId)
                .Index(t => t.RefMenuItemId, unique: true);
            
            CreateTable(
                "dbo.ConnectTableGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Locations = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConnectTableGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConnectTableGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ConnectTables",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Pax = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConnectTable_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConnectTable_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ConnectTableConnectTableGroups",
                c => new
                    {
                        ConnectTable_Id = c.Int(nullable: false),
                        ConnectTableGroup_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ConnectTable_Id, t.ConnectTableGroup_Id })
                .ForeignKey("dbo.ConnectTables", t => t.ConnectTable_Id, cascadeDelete: true)
                .ForeignKey("dbo.ConnectTableGroups", t => t.ConnectTableGroup_Id, cascadeDelete: true)
                .Index(t => t.ConnectTable_Id)
                .Index(t => t.ConnectTableGroup_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ConnectTableConnectTableGroups", "ConnectTableGroup_Id", "dbo.ConnectTableGroups");
            DropForeignKey("dbo.ConnectTableConnectTableGroups", "ConnectTable_Id", "dbo.ConnectTables");
            DropForeignKey("dbo.UpMenuItems", "MenuItemId", "dbo.MenuItems");
            DropIndex("dbo.ConnectTableConnectTableGroups", new[] { "ConnectTableGroup_Id" });
            DropIndex("dbo.ConnectTableConnectTableGroups", new[] { "ConnectTable_Id" });
            DropIndex("dbo.UpMenuItems", new[] { "RefMenuItemId" });
            DropIndex("dbo.UpMenuItems", new[] { "MenuItemId" });
            DropTable("dbo.ConnectTableConnectTableGroups");
            DropTable("dbo.ConnectTables",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConnectTable_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConnectTable_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ConnectTableGroups",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConnectTableGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConnectTableGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.UpMenuItems");
        }
    }
}
