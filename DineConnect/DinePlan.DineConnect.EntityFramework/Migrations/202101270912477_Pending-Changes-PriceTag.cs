namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PendingChangesPriceTag : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ExternalPriceDefinitions", "MenuPortionId", "dbo.MenuItemPortions");
            DropIndex("dbo.ExternalPriceDefinitions", new[] { "MenuPortionId" });
            DropTable("dbo.ExternalPriceDefinitions");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ExternalPriceDefinitions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExternalPriceTagId = c.Int(nullable: false),
                        MenuPortionId = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 3),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.ExternalPriceDefinitions", "MenuPortionId");
            AddForeignKey("dbo.ExternalPriceDefinitions", "MenuPortionId", "dbo.MenuItemPortions", "Id", cascadeDelete: true);
        }
    }
}
