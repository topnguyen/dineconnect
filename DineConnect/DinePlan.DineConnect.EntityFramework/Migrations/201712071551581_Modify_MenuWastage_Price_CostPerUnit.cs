namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_MenuWastage_Price_CostPerUnit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItemWastageDetails", "Cost", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.MenuItemWastageDetails", "Price");
			Sql("update A set A.Price = B.Price, A.ReceivedValue = A.ReceivedQty * B.Price FROM [dbo].[InterTransferReceivedDetails] A INNER JOIN  [dbo].[InterTransferDetails]  B ON A.InterTransferRefId = B.InterTransferRefId WHERE A.Price = 0 and a.MaterialRefId = b.MaterialRefId ");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MenuItemWastageDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.MenuItemWastageDetails", "Cost");
        }
    }
}
