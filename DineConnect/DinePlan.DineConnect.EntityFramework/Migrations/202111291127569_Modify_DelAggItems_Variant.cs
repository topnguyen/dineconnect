namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_DelAggItems_Variant : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DelAggVariants", "SalesPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.DelAggVariants", "MarkupPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.DelAggItems", "SalesPrice");
            DropColumn("dbo.DelAggItems", "MarkupPrice");
            DropColumn("dbo.DelAggVariants", "Price");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DelAggVariants", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.DelAggItems", "MarkupPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.DelAggItems", "SalesPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.DelAggVariants", "MarkupPrice");
            DropColumn("dbo.DelAggVariants", "SalesPrice");
        }
    }
}
