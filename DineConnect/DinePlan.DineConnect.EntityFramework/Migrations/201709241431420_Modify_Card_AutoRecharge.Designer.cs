// <auto-generated />
namespace DinePlan.DineConnect.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Modify_Card_AutoRecharge : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Modify_Card_AutoRecharge));
        
        string IMigrationMetadata.Id
        {
            get { return "201709241431420_Modify_Card_AutoRecharge"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
