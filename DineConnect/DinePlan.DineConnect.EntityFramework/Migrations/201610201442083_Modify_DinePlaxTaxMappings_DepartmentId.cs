namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_DinePlaxTaxMappings_DepartmentId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DinePlanTaxMappings", "DepartmentId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DinePlanTaxMappings", "DepartmentId");
        }
    }
}
