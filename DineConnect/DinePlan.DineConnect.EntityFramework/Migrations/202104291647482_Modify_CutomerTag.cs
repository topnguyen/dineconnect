namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_CutomerTag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomerTagWiseMaterialPrices", "AvgRateBasedPrice", c => c.Boolean(nullable: false));
            AddColumn("dbo.CustomerTagWiseMaterialPrices", "MarginPercentage", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.CustomerTagWiseMaterialPrices", "MinimumPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CustomerTagWiseMaterialPrices", "MinimumPrice");
            DropColumn("dbo.CustomerTagWiseMaterialPrices", "MarginPercentage");
            DropColumn("dbo.CustomerTagWiseMaterialPrices", "AvgRateBasedPrice");
        }
    }
}
