namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPromotionDays : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PromotionSchedules", "Days", c => c.String(maxLength: 10));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PromotionSchedules", "Days");
        }
    }
}
