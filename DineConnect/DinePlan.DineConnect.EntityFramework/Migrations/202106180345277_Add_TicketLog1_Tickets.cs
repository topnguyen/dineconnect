namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_TicketLog1_Tickets : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "TicketLog1", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "TicketLog1");
        }
    }
}
