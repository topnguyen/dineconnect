namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCardTagToCardType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConnectCardTypes", "Tag", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ConnectCardTypes", "Tag");
        }
    }
}
