namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPrintTemplateName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrintTemplates", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PrintTemplates", "Name");
        }
    }
}
