namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Hr_Phase2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SalaryPaidConsolidatedIncentives",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SalaryPaidMasterRefId = c.Int(nullable: false),
                        IncentiveTagRefId = c.Int(nullable: false),
                        AmountPerUnit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OverAllManualOtHours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Count = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalIncentiveAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remarks = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalaryPaidConsolidatedIncentives_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalaryPaidConsolidatedIncentives_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IncentiveTags", t => t.IncentiveTagRefId)
                .ForeignKey("dbo.SalaryPaidMasters", t => t.SalaryPaidMasterRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.SalaryPaidMasterRefId)
                .Index(t => t.IncentiveTagRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.SalaryPaidMasters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeRefId = c.Int(nullable: false),
                        JobTitle = c.String(),
                        IsBillable = c.Boolean(nullable: false),
                        CompanyRefId = c.Int(nullable: false),
                        SalaryAsOnDate = c.DateTime(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        SalaryMode = c.Int(nullable: false),
                        WithEffectFrom = c.DateTime(),
                        BasicSalary = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Ot1PerHour = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Ot2PerHour = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OtAllowedIfWorkTimeOver = c.Boolean(nullable: false),
                        ReportOtPerHour = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IncentiveAndOTCalculationPeriodRefId = c.Int(nullable: false),
                        MaximumOTAllowedHoursOnPublicHoliday = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NoOfStandardWorkingDays = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NoOfWorkedDays = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NoOfPaidLeaveDays = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NoOfAbsentDays = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NoOfIdleDays = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NoOfExcessLevyDays = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BasicPay = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BasicPayRemarks = c.String(),
                        GrossSalaryAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DeductionAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NetSalaryAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IdleSalaryAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IdleSalaryRemarks = c.String(),
                        IdleSalarypercentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AbsentLevyAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AbsentLevyRemarks = c.String(),
                        ExcessLevyAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ExcessLevyRemarks = c.String(),
                        NoOfWorkHours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NoOfIdleHours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NoOfOT1Hours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NoOfOT2Hours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StatutoryAllowance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StatutoryDeduction = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalaryPaidMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalaryPaidMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyRefId)
                .ForeignKey("dbo.PersonalInformations", t => t.EmployeeRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.EmployeeRefId)
                .Index(t => t.CompanyRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.SalaryPaidDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SalaryPaidMasterRefId = c.Int(nullable: false),
                        IncentivePayModeId = c.Int(nullable: false),
                        AllowanceOrDeductionRefId = c.Int(nullable: false),
                        SalaryRefId = c.Int(nullable: false),
                        SalaryTagAliasName = c.String(),
                        EffectiveFrom = c.DateTime(nullable: false),
                        RollBackFrom = c.DateTime(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remarks = c.String(),
                        IsDynamicAmount = c.Boolean(nullable: false),
                        IsFixedAmount = c.Boolean(nullable: false),
                        CalculationPayModeRefId = c.Int(nullable: false),
                        IsSytemGeneratedIncentive = c.Boolean(nullable: false),
                        FormulaRefId = c.Int(),
                        IsItDeductableFromGrossSalary = c.Boolean(nullable: false),
                        IsThisStatutoryAddedWithGrossSalary = c.Boolean(nullable: false),
                        AmountCalculatedForSalaryProcess = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IdleAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IdleRemarks = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalaryPaidDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalaryPaidDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SalaryPaidMasters", t => t.SalaryPaidMasterRefId)
                .ForeignKey("dbo.IncentiveTags", t => t.SalaryRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => new { t.SalaryPaidMasterRefId, t.SalaryRefId }, unique: true, name: "MPK_SalaryPaidMasterRefId_SalaryRefId")
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.TimeSheetDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TsMasterRefId = c.Int(nullable: false),
                        TsDate = c.DateTime(nullable: false),
                        WorkDescription = c.String(),
                        FromHour = c.Int(nullable: false),
                        ToHour = c.Int(nullable: false),
                        BreakOutHour = c.Int(nullable: false),
                        BreakInHour = c.Int(nullable: false),
                        TotalHours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OverTimeFromHour = c.Int(nullable: false),
                        OverTimeToHour = c.Int(nullable: false),
                        TotalOverTimeHours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remarks = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TimeSheetDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TimeSheetDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .ForeignKey("dbo.TimeSheetMasters", t => t.TsMasterRefId)
                .Index(t => t.TsMasterRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.TimeSheetMasters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyRefId = c.Int(nullable: false),
                        AcYear = c.Int(nullable: false),
                        TsMode = c.String(nullable: false),
                        ManualTsNumber = c.Long(nullable: false),
                        JobOtherLocationRefId = c.Int(),
                        TsDateFrom = c.DateTime(nullable: false),
                        TsDateTo = c.DateTime(nullable: false),
                        TotalManHours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalOverTimeHours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TsPreparedEmployeeRefId = c.Int(nullable: false),
                        PreparedDate = c.DateTime(),
                        ApprovedDate = c.DateTime(),
                        TsStatus = c.String(),
                        SubmissionDate = c.DateTime(),
                        SubmissionDateToInvoiceDepartment = c.DateTime(),
                        InvoiceDate = c.DateTime(),
                        SaleInvoiceReferenceNumber = c.String(),
                        FileName = c.String(),
                        FileExtenstionType = c.String(),
                        RtSourceType = c.String(),
                        MainCode = c.String(),
                        ProjectNumber = c.String(),
                        Module = c.String(),
                        Location = c.String(),
                        WoNumber = c.String(),
                        PoNumber = c.String(),
                        JVRNumber = c.String(),
                        DoesThisAttachmentSupply = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TimeSheetMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TimeSheetMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyRefId)
                .ForeignKey("dbo.Locations", t => t.JobOtherLocationRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.CompanyRefId)
                .Index(t => t.JobOtherLocationRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.TimeSheetEmployeeDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TsMasterRefId = c.Int(nullable: false),
                        TsDetailRefId = c.Int(nullable: false),
                        EmployeeRefId = c.Int(nullable: false),
                        WorkPositionRefId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TimeSheetEmployeeDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TimeSheetEmployeeDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonalInformations", t => t.EmployeeRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .ForeignKey("dbo.TimeSheetDetails", t => t.TsDetailRefId)
                .ForeignKey("dbo.TimeSheetMasters", t => t.TsMasterRefId)
                .Index(t => t.TsMasterRefId)
                .Index(t => t.TsDetailRefId)
                .Index(t => t.EmployeeRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.TimeSheetMaterialUsages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TsMasterRefId = c.Int(nullable: false),
                        TsDetailRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        QuantityUsed = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TimeSheetMaterialUsage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TimeSheetMaterialUsage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .ForeignKey("dbo.TimeSheetDetails", t => t.TsDetailRefId)
                .ForeignKey("dbo.TimeSheetMasters", t => t.TsMasterRefId)
                .Index(t => t.TsMasterRefId)
                .Index(t => t.TsDetailRefId)
                .Index(t => t.MaterialRefId)
                .Index(t => t.TenantId);
            
            AddColumn("dbo.PersonalInformations", "IsBillableAttendanceRequired", c => c.Boolean(nullable: false));
            AddColumn("dbo.PersonalInformations", "WPClassNumber", c => c.String());
            AddColumn("dbo.PersonalInformations", "WorkPassStartDate", c => c.DateTime());
            AddColumn("dbo.PersonalInformations", "WorkPassEndDate", c => c.DateTime());
            AddColumn("dbo.PersonalInformations", "PRNumber", c => c.String());
            AddColumn("dbo.PersonalInformations", "PRStartDate", c => c.DateTime());
            AddColumn("dbo.PersonalInformations", "PREndDate", c => c.DateTime());
            AddColumn("dbo.IncentiveTags", "IsFixedAmount", c => c.Boolean(nullable: false));
            AddColumn("dbo.LeaveRequests", "DoesSalaryPayableForThisLeaveRequest", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TimeSheetMaterialUsages", "TsMasterRefId", "dbo.TimeSheetMasters");
            DropForeignKey("dbo.TimeSheetMaterialUsages", "TsDetailRefId", "dbo.TimeSheetDetails");
            DropForeignKey("dbo.TimeSheetMaterialUsages", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.TimeSheetMaterialUsages", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.TimeSheetEmployeeDetails", "TsMasterRefId", "dbo.TimeSheetMasters");
            DropForeignKey("dbo.TimeSheetEmployeeDetails", "TsDetailRefId", "dbo.TimeSheetDetails");
            DropForeignKey("dbo.TimeSheetEmployeeDetails", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.TimeSheetEmployeeDetails", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.TimeSheetDetails", "TsMasterRefId", "dbo.TimeSheetMasters");
            DropForeignKey("dbo.TimeSheetMasters", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.TimeSheetMasters", "JobOtherLocationRefId", "dbo.Locations");
            DropForeignKey("dbo.TimeSheetMasters", "CompanyRefId", "dbo.Companies");
            DropForeignKey("dbo.TimeSheetDetails", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.SalaryPaidDetails", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.SalaryPaidDetails", "SalaryRefId", "dbo.IncentiveTags");
            DropForeignKey("dbo.SalaryPaidDetails", "SalaryPaidMasterRefId", "dbo.SalaryPaidMasters");
            DropForeignKey("dbo.SalaryPaidConsolidatedIncentives", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.SalaryPaidConsolidatedIncentives", "SalaryPaidMasterRefId", "dbo.SalaryPaidMasters");
            DropForeignKey("dbo.SalaryPaidMasters", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.SalaryPaidMasters", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.SalaryPaidMasters", "CompanyRefId", "dbo.Companies");
            DropForeignKey("dbo.SalaryPaidConsolidatedIncentives", "IncentiveTagRefId", "dbo.IncentiveTags");
            DropIndex("dbo.TimeSheetMaterialUsages", new[] { "TenantId" });
            DropIndex("dbo.TimeSheetMaterialUsages", new[] { "MaterialRefId" });
            DropIndex("dbo.TimeSheetMaterialUsages", new[] { "TsDetailRefId" });
            DropIndex("dbo.TimeSheetMaterialUsages", new[] { "TsMasterRefId" });
            DropIndex("dbo.TimeSheetEmployeeDetails", new[] { "TenantId" });
            DropIndex("dbo.TimeSheetEmployeeDetails", new[] { "EmployeeRefId" });
            DropIndex("dbo.TimeSheetEmployeeDetails", new[] { "TsDetailRefId" });
            DropIndex("dbo.TimeSheetEmployeeDetails", new[] { "TsMasterRefId" });
            DropIndex("dbo.TimeSheetMasters", new[] { "TenantId" });
            DropIndex("dbo.TimeSheetMasters", new[] { "JobOtherLocationRefId" });
            DropIndex("dbo.TimeSheetMasters", new[] { "CompanyRefId" });
            DropIndex("dbo.TimeSheetDetails", new[] { "TenantId" });
            DropIndex("dbo.TimeSheetDetails", new[] { "TsMasterRefId" });
            DropIndex("dbo.SalaryPaidDetails", new[] { "TenantId" });
            DropIndex("dbo.SalaryPaidDetails", "MPK_SalaryPaidMasterRefId_SalaryRefId");
            DropIndex("dbo.SalaryPaidMasters", new[] { "TenantId" });
            DropIndex("dbo.SalaryPaidMasters", new[] { "CompanyRefId" });
            DropIndex("dbo.SalaryPaidMasters", new[] { "EmployeeRefId" });
            DropIndex("dbo.SalaryPaidConsolidatedIncentives", new[] { "TenantId" });
            DropIndex("dbo.SalaryPaidConsolidatedIncentives", new[] { "IncentiveTagRefId" });
            DropIndex("dbo.SalaryPaidConsolidatedIncentives", new[] { "SalaryPaidMasterRefId" });
            DropColumn("dbo.LeaveRequests", "DoesSalaryPayableForThisLeaveRequest");
            DropColumn("dbo.IncentiveTags", "IsFixedAmount");
            DropColumn("dbo.PersonalInformations", "PREndDate");
            DropColumn("dbo.PersonalInformations", "PRStartDate");
            DropColumn("dbo.PersonalInformations", "PRNumber");
            DropColumn("dbo.PersonalInformations", "WorkPassEndDate");
            DropColumn("dbo.PersonalInformations", "WorkPassStartDate");
            DropColumn("dbo.PersonalInformations", "WPClassNumber");
            DropColumn("dbo.PersonalInformations", "IsBillableAttendanceRequired");
            DropTable("dbo.TimeSheetMaterialUsages",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TimeSheetMaterialUsage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TimeSheetMaterialUsage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TimeSheetEmployeeDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TimeSheetEmployeeDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TimeSheetEmployeeDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TimeSheetMasters",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TimeSheetMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TimeSheetMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TimeSheetDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TimeSheetDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TimeSheetDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SalaryPaidDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalaryPaidDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalaryPaidDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SalaryPaidMasters",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalaryPaidMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalaryPaidMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SalaryPaidConsolidatedIncentives",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalaryPaidConsolidatedIncentives_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalaryPaidConsolidatedIncentives_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
