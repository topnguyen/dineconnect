namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_MessageUntil_col_to_From_and_To_Date : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TickMessages", "FromDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.TickMessages", "ToDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.TickMessages", "MessageUntil");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TickMessages", "MessageUntil", c => c.DateTime(nullable: false));
            DropColumn("dbo.TickMessages", "ToDate");
            DropColumn("dbo.TickMessages", "FromDate");
        }
    }
}
