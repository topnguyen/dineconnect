namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_QuotationHeader_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CaterQuotationHeaders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CaterCustomerId = c.Int(nullable: false),
                        EventAddressId = c.Int(nullable: false),
                        EventDateTime = c.DateTime(nullable: false),
                        NoOfPax = c.Int(nullable: false),
                        OrderChannel = c.Int(nullable: false),
                        TotalAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Note = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CaterQuotationHeader_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CaterQuotationHeader_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CaterCustomers", t => t.CaterCustomerId, cascadeDelete: true)
                .ForeignKey("dbo.CaterCustomerAddressDetails", t => t.EventAddressId)
                .Index(t => t.CaterCustomerId)
                .Index(t => t.EventAddressId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CaterQuotationHeaders", "EventAddressId", "dbo.CaterCustomerAddressDetails");
            DropForeignKey("dbo.CaterQuotationHeaders", "CaterCustomerId", "dbo.CaterCustomers");
            DropIndex("dbo.CaterQuotationHeaders", new[] { "EventAddressId" });
            DropIndex("dbo.CaterQuotationHeaders", new[] { "CaterCustomerId" });
            DropTable("dbo.CaterQuotationHeaders",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CaterQuotationHeader_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CaterQuotationHeader_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
