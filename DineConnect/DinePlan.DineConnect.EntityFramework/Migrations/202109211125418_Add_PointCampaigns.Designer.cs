// <auto-generated />
namespace DinePlan.DineConnect.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Add_PointCampaigns : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Add_PointCampaigns));
        
        string IMigrationMetadata.Id
        {
            get { return "202109211125418_Add_PointCampaigns"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
