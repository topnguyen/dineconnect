namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddLanguageDescription : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LanguageDescriptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LanguageCode = c.String(),
                        ProductName = c.String(),
                        Description = c.String(),
                        LanguageDescriptionType = c.Int(nullable: false),
                        ReferenceId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LanguageDescription_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_LanguageDescription_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.PaymentTypes", "ProcessorName", c => c.String());
            AddColumn("dbo.ProductComboes", "Sorting", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductComboes", "Sorting");
            DropColumn("dbo.PaymentTypes", "ProcessorName");
            DropTable("dbo.LanguageDescriptions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LanguageDescription_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_LanguageDescription_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
