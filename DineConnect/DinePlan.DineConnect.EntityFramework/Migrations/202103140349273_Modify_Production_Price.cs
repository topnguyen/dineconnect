namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Production_Price : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductionDetails", "Price", c => c.Decimal(nullable: false, precision: 24, scale: 14));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductionDetails", "Price");
        }
    }
}
