namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_DemandDiscountPromotions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DemandPromotions", "PromotionApplyOnce", c => c.Boolean(nullable: false));
            AddColumn("dbo.DemandPromotions", "PromotionApplyType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DemandPromotions", "PromotionApplyType");
            DropColumn("dbo.DemandPromotions", "PromotionApplyOnce");
        }
    }
}
