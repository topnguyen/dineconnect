namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PlanLoggerChange : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PlanLoggers", "Location_Id", "dbo.Locations");
            DropIndex("dbo.PlanLoggers", new[] { "Location_Id" });
            RenameColumn(table: "dbo.PlanLoggers", name: "Location_Id", newName: "LocationId");
            AddColumn("dbo.PlanLoggers", "Terminal", c => c.String(maxLength: 25));
            AddColumn("dbo.PlanLoggers", "TicketId", c => c.Int(nullable: false));
            AddColumn("dbo.PlanLoggers", "TicketNo", c => c.String(maxLength: 25));
            AddColumn("dbo.PlanLoggers", "TicketTotal", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AddColumn("dbo.PlanLoggers", "EventLog", c => c.String());
            AddColumn("dbo.PlanLoggers", "EventName", c => c.String(maxLength: 25));
            AlterColumn("dbo.PlanLoggers", "UserName", c => c.String(maxLength: 25));
            AlterColumn("dbo.PlanLoggers", "LocationId", c => c.Int(nullable: false));
            CreateIndex("dbo.PlanLoggers", "LocationId");
            AddForeignKey("dbo.PlanLoggers", "LocationId", "dbo.Locations", "Id", cascadeDelete: true);
            DropColumn("dbo.PlanLoggers", "TicketNumber");
            DropColumn("dbo.PlanLoggers", "TicketAmount");
            DropColumn("dbo.PlanLoggers", "FinalAmount");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PlanLoggers", "FinalAmount", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AddColumn("dbo.PlanLoggers", "TicketAmount", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AddColumn("dbo.PlanLoggers", "TicketNumber", c => c.String());
            DropForeignKey("dbo.PlanLoggers", "LocationId", "dbo.Locations");
            DropIndex("dbo.PlanLoggers", new[] { "LocationId" });
            AlterColumn("dbo.PlanLoggers", "LocationId", c => c.Int());
            AlterColumn("dbo.PlanLoggers", "UserName", c => c.String());
            DropColumn("dbo.PlanLoggers", "EventName");
            DropColumn("dbo.PlanLoggers", "EventLog");
            DropColumn("dbo.PlanLoggers", "TicketTotal");
            DropColumn("dbo.PlanLoggers", "TicketNo");
            DropColumn("dbo.PlanLoggers", "TicketId");
            DropColumn("dbo.PlanLoggers", "Terminal");
            RenameColumn(table: "dbo.PlanLoggers", name: "LocationId", newName: "Location_Id");
            CreateIndex("dbo.PlanLoggers", "Location_Id");
            AddForeignKey("dbo.PlanLoggers", "Location_Id", "dbo.Locations", "Id");
        }
    }
}
