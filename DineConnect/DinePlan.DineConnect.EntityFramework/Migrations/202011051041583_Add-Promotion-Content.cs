namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPromotionContent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Promotions", "PromotionContents", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Promotions", "PromotionContents");
        }
    }
}
