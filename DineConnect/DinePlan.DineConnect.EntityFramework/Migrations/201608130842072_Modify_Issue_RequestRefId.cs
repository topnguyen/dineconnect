namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Issue_RequestRefId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Issues", "RequestRefId", c => c.Int());
            CreateIndex("dbo.Issues", "RequestRefId");
            AddForeignKey("dbo.Issues", "RequestRefId", "dbo.Requests", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Issues", "RequestRefId", "dbo.Requests");
            DropIndex("dbo.Issues", new[] { "RequestRefId" });
            DropColumn("dbo.Issues", "RequestRefId");
        }
    }
}
