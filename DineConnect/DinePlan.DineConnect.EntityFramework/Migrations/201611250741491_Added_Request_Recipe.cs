namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Request_Recipe : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RequestRecipeDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RequestRefId = c.Int(nullable: false),
                        Sno = c.Int(nullable: false),
                        RecipeRefId = c.Int(nullable: false),
                        RecipeProductionQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitRefId = c.Int(nullable: false),
                        MultipleBatchProductionAllowed = c.Boolean(nullable: false),
                        Remarks = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RequestRecipeDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.RecipeRefId, cascadeDelete: true)
                .ForeignKey("dbo.Requests", t => t.RequestRefId, cascadeDelete: true)
                .Index(t => t.RequestRefId)
                .Index(t => t.RecipeRefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RequestRecipeDetails", "RequestRefId", "dbo.Requests");
            DropForeignKey("dbo.RequestRecipeDetails", "RecipeRefId", "dbo.Materials");
            DropIndex("dbo.RequestRecipeDetails", new[] { "RecipeRefId" });
            DropIndex("dbo.RequestRecipeDetails", new[] { "RequestRefId" });
            DropTable("dbo.RequestRecipeDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RequestRecipeDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
