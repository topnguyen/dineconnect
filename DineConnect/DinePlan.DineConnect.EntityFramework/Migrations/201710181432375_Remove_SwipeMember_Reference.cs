namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_SwipeMember_Reference : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MemberCards", "MemberRefId", "dbo.SwipeMembers");
            DropForeignKey("dbo.SwipeCardLedgers", "MemberRefId", "dbo.SwipeMembers");
            DropForeignKey("dbo.SwipeMemberSpecialDates", "SwipeMemberRefId", "dbo.SwipeMembers");
            DropIndex("dbo.SwipeMemberSpecialDates", new[] { "SwipeMemberRefId" });
            AddColumn("dbo.ConnectMembers", "IsActive", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MemberAddresses", "LocationId", c => c.Int());
            AddForeignKey("dbo.MemberCards", "MemberRefId", "dbo.ConnectMembers", "Id");
            AddForeignKey("dbo.SwipeCardLedgers", "MemberRefId", "dbo.ConnectMembers", "Id");
            DropTable("dbo.SwipeMemberSpecialDates",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipeMemberSpecialDate_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SwipeMemberSpecialDates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SwipeMemberRefId = c.Int(nullable: false),
                        SpecialDatesRefId = c.Int(nullable: false),
                        SpecialDate = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipeMemberSpecialDate_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.SwipeCardLedgers", "MemberRefId", "dbo.ConnectMembers");
            DropForeignKey("dbo.MemberCards", "MemberRefId", "dbo.ConnectMembers");
            AlterColumn("dbo.MemberAddresses", "LocationId", c => c.Int(nullable: false));
            DropColumn("dbo.ConnectMembers", "IsActive");
            CreateIndex("dbo.SwipeMemberSpecialDates", "SwipeMemberRefId");
            AddForeignKey("dbo.SwipeMemberSpecialDates", "SwipeMemberRefId", "dbo.SwipeMembers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.SwipeCardLedgers", "MemberRefId", "dbo.SwipeMembers", "Id");
            AddForeignKey("dbo.MemberCards", "MemberRefId", "dbo.SwipeMembers", "Id");
        }
    }
}
