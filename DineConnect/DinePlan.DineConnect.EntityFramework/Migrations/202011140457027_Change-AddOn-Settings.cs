namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeAddOnSettings : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConnectAddOns", "AddOnSettings", c => c.String());
            AlterColumn("dbo.ConnectAddOns", "Settings", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ConnectAddOns", "Settings", c => c.String(maxLength: 3000));
            DropColumn("dbo.ConnectAddOns", "AddOnSettings");
        }
    }
}
