namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Clstock_AdjustmentRefId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ClosingStocks", "AdjustmentRefId", c => c.Int());
            AlterColumn("dbo.ClosingStocks", "ApprovedUserId", c => c.Long());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ClosingStocks", "ApprovedUserId", c => c.Int());
            DropColumn("dbo.ClosingStocks", "AdjustmentRefId");
        }
    }
}
