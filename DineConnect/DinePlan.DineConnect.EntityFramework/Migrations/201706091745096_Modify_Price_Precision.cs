namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Price_Precision : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.InterTransferDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.InterTransferReceivedDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 3));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.InterTransferReceivedDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.InterTransferDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
