namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveTicketMessage : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TicketMessages", "DepartmentId", "dbo.EmployeeDepartments");
            DropForeignKey("dbo.TicketMessages", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.TicketMessages", "MessageStatusId", "dbo.TicketMessageStatuses");
            DropForeignKey("dbo.TicketMessages", "CreatorUserId", "dbo.AbpUsers");
            DropForeignKey("dbo.TicketMessageReplies", "EmployeeDepartmentId", "dbo.EmployeeDepartments");
            DropForeignKey("dbo.TicketMessageReplies", "TicketMessageId", "dbo.TicketMessages");
            DropForeignKey("dbo.TicketMessageReplies", "TicketMessageStatusId", "dbo.TicketMessageStatuses");
            DropForeignKey("dbo.TicketMessageReplies", "CreatorUserId", "dbo.AbpUsers");
            DropIndex("dbo.TicketMessages", new[] { "DepartmentId" });
            DropIndex("dbo.TicketMessages", new[] { "MessageStatusId" });
            DropIndex("dbo.TicketMessages", new[] { "LocationId" });
            DropIndex("dbo.TicketMessages", new[] { "CreatorUserId" });
            DropIndex("dbo.TicketMessageReplies", new[] { "TicketMessageId" });
            DropIndex("dbo.TicketMessageReplies", new[] { "TicketMessageStatusId" });
            DropIndex("dbo.TicketMessageReplies", new[] { "EmployeeDepartmentId" });
            DropIndex("dbo.TicketMessageReplies", new[] { "CreatorUserId" });
            DropTable("dbo.EmployeeDepartments",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeDepartment_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeDepartment_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TicketMessages",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TicketMessage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TicketMessageStatuses",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessageStatus_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TicketMessagesHistory",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessageHistory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TicketMessageReplies",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessageReply_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TicketMessageReplies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        TicketMessageId = c.Int(nullable: false),
                        TicketMessageStatusId = c.Int(),
                        EmployeeDepartmentId = c.Int(),
                        Files = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessageReply_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TicketMessagesHistory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TicketMessageReplyId = c.Int(nullable: false),
                        DepartmentId = c.Int(nullable: false),
                        MessageStatusId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessageHistory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TicketMessageStatuses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessageStatus_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TicketMessages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        Message = c.String(),
                        DepartmentId = c.Int(nullable: false),
                        MessageStatusId = c.Int(nullable: false),
                        Files = c.String(),
                        LocationId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketMessage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TicketMessage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EmployeeDepartments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeDepartment_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeDepartment_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.TicketMessageReplies", "CreatorUserId");
            CreateIndex("dbo.TicketMessageReplies", "EmployeeDepartmentId");
            CreateIndex("dbo.TicketMessageReplies", "TicketMessageStatusId");
            CreateIndex("dbo.TicketMessageReplies", "TicketMessageId");
            CreateIndex("dbo.TicketMessages", "CreatorUserId");
            CreateIndex("dbo.TicketMessages", "LocationId");
            CreateIndex("dbo.TicketMessages", "MessageStatusId");
            CreateIndex("dbo.TicketMessages", "DepartmentId");
            AddForeignKey("dbo.TicketMessageReplies", "CreatorUserId", "dbo.AbpUsers", "Id");
            AddForeignKey("dbo.TicketMessageReplies", "TicketMessageStatusId", "dbo.TicketMessageStatuses", "Id");
            AddForeignKey("dbo.TicketMessageReplies", "TicketMessageId", "dbo.TicketMessages", "Id", cascadeDelete: true);
            AddForeignKey("dbo.TicketMessageReplies", "EmployeeDepartmentId", "dbo.EmployeeDepartments", "Id");
            AddForeignKey("dbo.TicketMessages", "CreatorUserId", "dbo.AbpUsers", "Id");
            AddForeignKey("dbo.TicketMessages", "MessageStatusId", "dbo.TicketMessageStatuses", "Id", cascadeDelete: true);
            AddForeignKey("dbo.TicketMessages", "LocationId", "dbo.Locations", "Id");
            AddForeignKey("dbo.TicketMessages", "DepartmentId", "dbo.EmployeeDepartments", "Id", cascadeDelete: true);
        }
    }
}
