namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Swipe_Structure : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SwipeTransactionDetails", "CardRefId", "dbo.SwipeCards");
            DropForeignKey("dbo.SwipeTransactionDetails", "MemberRefId", "dbo.SwipeMembers");
            DropIndex("dbo.SwipeTransactionDetails", new[] { "MemberRefId" });
            DropIndex("dbo.SwipeTransactionDetails", new[] { "CardRefId" });
            AlterColumn("dbo.SwipeCards", "CardNumber", c => c.String(maxLength: 20));
            CreateIndex("dbo.SwipeCards", "CardNumber", unique: true, name: "UQ_CARDNUMBER");
            DropTable("dbo.SwipeTransactionDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipeTransactionDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SwipeTransactionDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SwipeTransactionDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TransactionTime = c.DateTime(nullable: false),
                        MemberRefId = c.Int(nullable: false),
                        CardRefId = c.Int(nullable: false),
                        PrimaryCardRefId = c.Int(nullable: false),
                        TransactionType = c.Int(nullable: false),
                        Description = c.String(),
                        Credit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Debit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipeTransactionDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SwipeTransactionDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            DropIndex("dbo.SwipeCards", "UQ_CARDNUMBER");
            AlterColumn("dbo.SwipeCards", "CardNumber", c => c.String());
            CreateIndex("dbo.SwipeTransactionDetails", "CardRefId");
            CreateIndex("dbo.SwipeTransactionDetails", "MemberRefId");
            AddForeignKey("dbo.SwipeTransactionDetails", "MemberRefId", "dbo.SwipeMembers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.SwipeTransactionDetails", "CardRefId", "dbo.SwipeCards", "Id", cascadeDelete: true);
        }
    }
}
