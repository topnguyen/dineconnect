namespace DinePlan.DineConnect.Migrations
{
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;

    public partial class Added_Printer_tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PrinterMaps",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    MenuItemGroupCode = c.String(),
                    MenuItemId = c.Int(nullable: false),
                    PrinterId = c.Int(nullable: false),
                    PrintTemplateId = c.Int(nullable: false),
                    PrintJobId = c.Int(),
                    IsDeleted = c.Boolean(nullable: false),
                    DeleterUserId = c.Long(),
                    DeletionTime = c.DateTime(),
                    LastModificationTime = c.DateTime(),
                    LastModifierUserId = c.Long(),
                    CreationTime = c.DateTime(nullable: false),
                    CreatorUserId = c.Long(),
                },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PrinterMap_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Printers", t => t.PrinterId, cascadeDelete: true)
                .ForeignKey("dbo.PrintJobs", t => t.PrintJobId)
                .ForeignKey("dbo.PrintTemplates", t => t.PrintTemplateId, cascadeDelete: true)
                .Index(t => t.PrinterId)
                .Index(t => t.PrintTemplateId)
                .Index(t => t.PrintJobId);

            CreateTable(
                "dbo.Printers",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(),
                    PrinterType = c.Int(nullable: false),
                    CodePage = c.Int(nullable: false),
                    CharsPerLine = c.Int(nullable: false),
                    NumberOfLines = c.Int(nullable: false),
                    FallBackPrinter = c.Int(nullable: false),
                    CharReplacement = c.String(),
                    CustomPrinterName = c.String(),
                    CustomPrinterData = c.String(),
                    Locations = c.String(maxLength: 1000),
                    NonLocations = c.String(maxLength: 1000),
                    Group = c.Boolean(nullable: false),
                    LocationTag = c.Boolean(nullable: false),
                    TenantId = c.Int(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    DeleterUserId = c.Long(),
                    DeletionTime = c.DateTime(),
                    LastModificationTime = c.DateTime(),
                    LastModifierUserId = c.Long(),
                    CreationTime = c.DateTime(nullable: false),
                    CreatorUserId = c.Long(),
                },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Printer_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Printer_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.PrintJobs",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(),
                    WhatToPrint = c.Int(nullable: false),
                    Negative = c.Boolean(nullable: false),
                    JobGroup = c.String(),
                    Locations = c.String(maxLength: 1000),
                    NonLocations = c.String(maxLength: 1000),
                    Group = c.Boolean(nullable: false),
                    LocationTag = c.Boolean(nullable: false),
                    TenantId = c.Int(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    DeleterUserId = c.Long(),
                    DeletionTime = c.DateTime(),
                    LastModificationTime = c.DateTime(),
                    LastModifierUserId = c.Long(),
                    CreationTime = c.DateTime(nullable: false),
                    CreatorUserId = c.Long(),
                },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PrintJob_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PrintJob_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
        }

        public override void Down()
        {
            DropForeignKey("dbo.PrinterMaps", "PrintTemplateId", "dbo.PrintTemplates");
            DropForeignKey("dbo.PrinterMaps", "PrintJobId", "dbo.PrintJobs");
            DropForeignKey("dbo.PrinterMaps", "PrinterId", "dbo.Printers");
            DropIndex("dbo.PrinterMaps", new[] { "PrintJobId" });
            DropIndex("dbo.PrinterMaps", new[] { "PrintTemplateId" });
            DropIndex("dbo.PrinterMaps", new[] { "PrinterId" });
            DropTable("dbo.PrintJobs",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PrintJob_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PrintJob_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Printers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Printer_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Printer_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PrinterMaps",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PrinterMap_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}