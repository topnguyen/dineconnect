// <auto-generated />
namespace DinePlan.DineConnect.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Ticket_IncreaseDataSize : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Ticket_IncreaseDataSize));
        
        string IMigrationMetadata.Id
        {
            get { return "202103160831093_Ticket_IncreaseDataSize"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
