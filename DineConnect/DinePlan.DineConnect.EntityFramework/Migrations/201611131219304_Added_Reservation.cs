namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Reservation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Reservation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        ReservationDate = c.DateTime(nullable: false),
                        ConnectMemberId = c.Int(nullable: false),
                        Pax = c.Int(nullable: false),
                        RemainderRequired = c.Boolean(nullable: false),
                        RemainderTimes = c.String(),
                        RemainderMedieums = c.String(),
                        Remarks = c.String(),
                        Status = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Reservation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Reservation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ConnectMembers", t => t.ConnectMemberId, cascadeDelete: true)
                .ForeignKey("dbo.Locations", t => t.LocationRefId, cascadeDelete: true)
                .Index(t => t.LocationRefId)
                .Index(t => t.ConnectMemberId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reservation", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.Reservation", "ConnectMemberId", "dbo.ConnectMembers");
            DropIndex("dbo.Reservation", new[] { "ConnectMemberId" });
            DropIndex("dbo.Reservation", new[] { "LocationRefId" });
            DropTable("dbo.Reservation",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Reservation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Reservation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
