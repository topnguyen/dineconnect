namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class EngageMember : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.GiftVouchers", "VoucherId");
            CreateTable(
                "dbo.MemberPoints",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ConnectMemberId = c.Int(nullable: false),
                        LocationId = c.Int(nullable: false),
                        TicketId = c.Int(nullable: false),
                        TicketDate = c.DateTime(),
                        TicketTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsPointAccumulation = c.Boolean(nullable: false),
                        IsPointRedemption = c.Boolean(nullable: false),
                        PointsAccumulation = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PointsAccumulationFactor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PointsRedemption = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PointsRedemptionFactor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalPoints = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ConnectMembers", t => t.ConnectMemberId, cascadeDelete: true)
                .Index(t => t.ConnectMemberId);
            
            CreateTable(
                "dbo.ConnectMembers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MemberCode = c.String(maxLength: 30),
                        Name = c.String(),
                        Address = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Country = c.String(),
                        PostalCode = c.String(),
                        EmailId = c.String(),
                        PhoneNumber = c.String(),
                        CustomData = c.String(),
                        TotalPoints = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LastVisitDate = c.DateTime(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConnectMember_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConnectMember_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.MemberCode, unique: true, name: "MemberCode");
            
            AddColumn("dbo.GiftVouchers", "Voucher", c => c.String(maxLength: 30));
            AddColumn("dbo.GiftVouchers", "ClaimedDate", c => c.DateTime());
            AddColumn("dbo.GiftVouchers", "Remarks", c => c.String());
            AddColumn("dbo.GiftVouchers", "ClaimedAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.GiftVouchers", "TicketNumber", c => c.String());
            AddColumn("dbo.GiftVouchers", "LocationId", c => c.Int(nullable: false));
            AddColumn("dbo.Payments", "PaymentTags", c => c.String());
            CreateIndex("dbo.GiftVouchers", "Voucher", unique: true, name: "VoucherId");
            DropColumn("dbo.GiftVouchers", "VoucherId");
            DropColumn("dbo.GiftVouchers", "TicketId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.GiftVouchers", "TicketId", c => c.Int(nullable: false));
            AddColumn("dbo.GiftVouchers", "VoucherId", c => c.String(maxLength: 30));
            DropForeignKey("dbo.MemberPoints", "ConnectMemberId", "dbo.ConnectMembers");
            DropIndex("dbo.ConnectMembers", "MemberCode");
            DropIndex("dbo.MemberPoints", new[] { "ConnectMemberId" });
            DropIndex("dbo.GiftVouchers", "VoucherId");
            DropColumn("dbo.Payments", "PaymentTags");
            DropColumn("dbo.GiftVouchers", "LocationId");
            DropColumn("dbo.GiftVouchers", "TicketNumber");
            DropColumn("dbo.GiftVouchers", "ClaimedAmount");
            DropColumn("dbo.GiftVouchers", "Remarks");
            DropColumn("dbo.GiftVouchers", "ClaimedDate");
            DropColumn("dbo.GiftVouchers", "Voucher");
            DropTable("dbo.ConnectMembers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConnectMember_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConnectMember_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MemberPoints");
            CreateIndex("dbo.GiftVouchers", "VoucherId", unique: true, name: "VoucherId");
        }
    }
}
