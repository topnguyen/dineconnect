namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addticketstatusforticket : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "TicketStatus", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "TicketStatus");
        }
    }
}
