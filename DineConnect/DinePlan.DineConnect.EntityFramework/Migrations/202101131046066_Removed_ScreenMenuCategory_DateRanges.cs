namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Removed_ScreenMenuCategory_DateRanges : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ScreenMenuCategories", "StartDate");
            DropColumn("dbo.ScreenMenuCategories", "EndDate");
            }
        
        public override void Down()
        {
            AddColumn("dbo.ScreenMenuCategories", "EndDate", c => c.DateTime());
            AddColumn("dbo.ScreenMenuCategories", "StartDate", c => c.DateTime());
        }
    }
}
