namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_SupMaterial_MaximumOrderQty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SupplierMaterials", "MaximumOrderQuantity", c => c.Decimal(nullable: false, precision: 24, scale: 14));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SupplierMaterials", "MaximumOrderQuantity");
        }
    }
}
