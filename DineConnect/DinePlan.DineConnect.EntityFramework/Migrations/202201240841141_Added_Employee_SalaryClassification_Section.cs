namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Employee_SalaryClassification_Section : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.JobTitleMasters", "JobTitle");
            CreateTable(
                "dbo.EmployeeSalaryClassifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SalaryClassification = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeSalaryClassification_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeSalaryClassification_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId, cascadeDelete: true)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.EmployeeSectionMasters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SectionName = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeSectionMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeSectionMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId, cascadeDelete: true)
                .Index(t => t.TenantId);
            
            CreateIndex("dbo.JobTitleMasters", new[] { "JobTitle", "TenantId" }, unique: true, name: "JobTitle_TenantId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EmployeeSectionMasters", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.EmployeeSalaryClassifications", "TenantId", "dbo.AbpTenants");
            DropIndex("dbo.EmployeeSectionMasters", new[] { "TenantId" });
            DropIndex("dbo.EmployeeSalaryClassifications", new[] { "TenantId" });
            DropIndex("dbo.JobTitleMasters", "JobTitle_TenantId");
            DropTable("dbo.EmployeeSectionMasters",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeSectionMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeSectionMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EmployeeSalaryClassifications",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeSalaryClassification_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeSalaryClassification_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            CreateIndex("dbo.JobTitleMasters", "JobTitle", unique: true, name: "JobTitle");
        }
    }
}
