namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_ClosingStock_OnHand : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ClosingStockDetails", "OnHand", c => c.Decimal(nullable: false, precision: 18, scale: 3));

        }
        
        public override void Down()
        {
            AlterColumn("dbo.ClosingStockDetails", "OnHand", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
