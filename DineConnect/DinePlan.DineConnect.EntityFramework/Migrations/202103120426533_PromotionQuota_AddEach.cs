namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PromotionQuota_AddEach : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PromotionQuotas", "Each", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PromotionQuotas", "Each");
        }
    }
}
