namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDepartmentGroupToTicket : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "DepartmentGroup", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "DepartmentGroup");
        }
    }
}
