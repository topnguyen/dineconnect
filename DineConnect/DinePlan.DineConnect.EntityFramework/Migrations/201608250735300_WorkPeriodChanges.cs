namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class WorkPeriodChanges : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WorkPeriodInformations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WorkPeriodId = c.Int(nullable: false),
                        PaymentTypeId = c.Int(nullable: false),
                        Actual = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Entered = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PaymentTypes", t => t.PaymentTypeId, cascadeDelete: true)
                .ForeignKey("dbo.WorkPeriods", t => t.WorkPeriodId, cascadeDelete: true)
                .Index(t => t.WorkPeriodId)
                .Index(t => t.PaymentTypeId);
            
            CreateTable(
                "dbo.WorkPeriods",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Float = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        LocationId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_WorkPeriod_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_WorkPeriod_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .Index(t => t.LocationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkPeriodInformations", "WorkPeriodId", "dbo.WorkPeriods");
            DropForeignKey("dbo.WorkPeriods", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.WorkPeriodInformations", "PaymentTypeId", "dbo.PaymentTypes");
            DropIndex("dbo.WorkPeriods", new[] { "LocationId" });
            DropIndex("dbo.WorkPeriodInformations", new[] { "PaymentTypeId" });
            DropIndex("dbo.WorkPeriodInformations", new[] { "WorkPeriodId" });
            DropTable("dbo.WorkPeriods",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_WorkPeriod_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_WorkPeriod_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.WorkPeriodInformations");
        }
    }
}
