namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTillChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SwipeShifts", "TillShiftCashIn", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SwipeShifts", "TillShiftCashOut", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SwipeShifts", "TillShiftCashOut");
            DropColumn("dbo.SwipeShifts", "TillShiftCashIn");
        }
    }
}
