namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Unit_UC_SyncId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Units", "SyncId", c => c.Int());
            AddColumn("dbo.UnitConversions", "SyncId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UnitConversions", "SyncId");
            DropColumn("dbo.Units", "SyncId");
        }
    }
}
