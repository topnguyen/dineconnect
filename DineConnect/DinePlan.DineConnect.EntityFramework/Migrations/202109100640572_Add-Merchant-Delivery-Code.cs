namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMerchantDeliveryCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DeliveryAggPlatforms", "PartnerMerchantCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DeliveryAggPlatforms", "PartnerMerchantCode");
        }
    }
}
