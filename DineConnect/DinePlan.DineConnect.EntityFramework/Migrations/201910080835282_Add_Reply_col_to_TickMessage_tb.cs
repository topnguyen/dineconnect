namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Reply_col_to_TickMessage_tb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TickMessages", "Reply", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TickMessages", "Reply");
        }
    }
}
