namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddImageOrderTagGroup : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DineGoDevices", "PushToConnect", c => c.Boolean(nullable: false));
            AddColumn("dbo.OrderTagGroups", "Files", c => c.String());
            AddColumn("dbo.OrderTagGroups", "DownloadImage", c => c.Guid(nullable: false));
            AddColumn("dbo.OrderTags", "Files", c => c.String());
            AddColumn("dbo.OrderTags", "DownloadImage", c => c.Guid(nullable: false));
            AddColumn("dbo.ProductComboItems", "Files", c => c.String());
            AddColumn("dbo.ProductComboItems", "DownloadImage", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductComboItems", "DownloadImage");
            DropColumn("dbo.ProductComboItems", "Files");
            DropColumn("dbo.OrderTags", "DownloadImage");
            DropColumn("dbo.OrderTags", "Files");
            DropColumn("dbo.OrderTagGroups", "DownloadImage");
            DropColumn("dbo.OrderTagGroups", "Files");
            DropColumn("dbo.DineGoDevices", "PushToConnect");
        }
    }
}
