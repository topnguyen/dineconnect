namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CardTypeExpiry : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConnectCardTypes", "ValidTill", c => c.DateTime(nullable: false));
            Sql("Update ConnectCardTypes set ValidTill='2022-10-10'");
            AddColumn("dbo.Promotions", "IgnorePromotionLimitation", c => c.Boolean(nullable: false));
            AddColumn("dbo.Promotions", "ApplyOffline", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Promotions", "ApplyOffline");
            DropColumn("dbo.Promotions", "IgnorePromotionLimitation");
            DropColumn("dbo.ConnectCardTypes", "ValidTill");
        }
    }
}
