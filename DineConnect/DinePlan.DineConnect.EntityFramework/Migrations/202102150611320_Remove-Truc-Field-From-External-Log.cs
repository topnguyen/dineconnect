namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveTrucFieldFromExternalLog : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ExternalLogs", "LogTimeTrunc");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ExternalLogs", "LogTimeTrunc", c => c.DateTime(nullable: false, storeType: "date"));
        }
    }
}
