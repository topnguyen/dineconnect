namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Quota_RemoveEach : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.PromotionQuotas", "Each");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PromotionQuotas", "Each", c => c.Boolean(nullable: false));
        }
    }
}
