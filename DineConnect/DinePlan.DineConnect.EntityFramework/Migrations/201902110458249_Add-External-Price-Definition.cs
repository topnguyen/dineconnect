namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddExternalPriceDefinition : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExternalPriceDefinitions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExternalPriceTagId = c.Int(nullable: false),
                        MenuPortionId = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 3),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MenuItemPortions", t => t.MenuPortionId, cascadeDelete: true)
                .Index(t => t.MenuPortionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ExternalPriceDefinitions", "MenuPortionId", "dbo.MenuItemPortions");
            DropIndex("dbo.ExternalPriceDefinitions", new[] { "MenuPortionId" });
            DropTable("dbo.ExternalPriceDefinitions");
        }
    }
}
