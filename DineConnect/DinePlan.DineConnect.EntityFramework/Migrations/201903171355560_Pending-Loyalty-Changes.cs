namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PendingLoyaltyChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProgramStages", "NextStageId", c => c.Int());
            DropColumn("dbo.ProgramStages", "Order");
            DropColumn("dbo.ProgramStages", "HasNext");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProgramStages", "HasNext", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProgramStages", "Order", c => c.Int(nullable: false));
            DropColumn("dbo.ProgramStages", "NextStageId");
        }
    }
}
