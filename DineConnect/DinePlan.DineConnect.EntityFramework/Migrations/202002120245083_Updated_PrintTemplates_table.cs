namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updated_PrintTemplates_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrintTemplates", "MergeLines", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PrintTemplates", "MergeLines");
        }
    }
}
