namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderTagChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderTags", "MaxQuantity", c => c.Int(nullable: false));
            AlterColumn("dbo.OrderTagMaps", "CategoryId", c => c.Int());
            AlterColumn("dbo.OrderTagMaps", "MenuItemId", c => c.Int());
            AlterColumn("dbo.OrderTags", "MenuItemId", c => c.Int());
            DropColumn("dbo.OrderTagMaps", "PortionId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderTagMaps", "PortionId", c => c.Int(nullable: false));
            AlterColumn("dbo.OrderTags", "MenuItemId", c => c.Int(nullable: false));
            AlterColumn("dbo.OrderTagMaps", "MenuItemId", c => c.Int(nullable: false));
            AlterColumn("dbo.OrderTagMaps", "CategoryId", c => c.Int(nullable: false));
            DropColumn("dbo.OrderTags", "MaxQuantity");
        }
    }
}
