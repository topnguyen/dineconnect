namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_ConnectCardTypes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConnectCardTypes", "ConnectCardTypeCategoryId", c => c.Int());
            CreateIndex("dbo.ConnectCardTypes", "ConnectCardTypeCategoryId");
            AddForeignKey("dbo.ConnectCardTypes", "ConnectCardTypeCategoryId", "dbo.ConnectCardTypeCategories", "Id");

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ConnectCardTypes", "ConnectCardTypeCategoryId", "dbo.ConnectCardTypeCategories");
            DropIndex("dbo.ConnectCardTypes", new[] { "ConnectCardTypeCategoryId" });
            DropColumn("dbo.ConnectCardTypes", "ConnectCardTypeCategoryId");

        }
    }
}
