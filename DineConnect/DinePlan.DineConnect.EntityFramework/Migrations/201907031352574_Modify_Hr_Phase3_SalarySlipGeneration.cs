namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Hr_Phase3_SalarySlipGeneration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PersonalInformations", "EmployeeReligionRefId", "dbo.EmployeeReligions");
            DropIndex("dbo.PersonalInformations", new[] { "EmployeeReligionRefId" });
            RenameColumn(table: "dbo.PersonalInformations", name: "EmployeeReligionRefId", newName: "ReligionRefId");
            AddColumn("dbo.PersonalInformations", "isBillable", c => c.Boolean(nullable: false));
            AddColumn("dbo.IncentiveTags", "DoesIssueOnlyForBillableEmployee", c => c.Boolean(nullable: false));
            AddColumn("dbo.IncentiveTags", "IsPfPayable", c => c.Boolean(nullable: false));
            AddColumn("dbo.IncentiveTags", "IsGovernmentInsurancePayable", c => c.Boolean(nullable: false));
            AddColumn("dbo.OtBasedOnSlabs", "FixedOTAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SalaryInfos", "IdleStandByCost", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SalaryInfos", "HourlySalaryModeRefId", c => c.Int());
            AlterColumn("dbo.PersonalInformations", "ReligionRefId", c => c.Int());
            CreateIndex("dbo.PersonalInformations", "ReligionRefId");
            AddForeignKey("dbo.PersonalInformations", "ReligionRefId", "dbo.EmployeeReligions", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PersonalInformations", "ReligionRefId", "dbo.EmployeeReligions");
            DropIndex("dbo.PersonalInformations", new[] { "ReligionRefId" });
            AlterColumn("dbo.PersonalInformations", "ReligionRefId", c => c.Int(nullable: false));
            DropColumn("dbo.SalaryInfos", "HourlySalaryModeRefId");
            DropColumn("dbo.SalaryInfos", "IdleStandByCost");
            DropColumn("dbo.OtBasedOnSlabs", "FixedOTAmount");
            DropColumn("dbo.IncentiveTags", "IsGovernmentInsurancePayable");
            DropColumn("dbo.IncentiveTags", "IsPfPayable");
            DropColumn("dbo.IncentiveTags", "DoesIssueOnlyForBillableEmployee");
            DropColumn("dbo.PersonalInformations", "isBillable");
            RenameColumn(table: "dbo.PersonalInformations", name: "ReligionRefId", newName: "EmployeeReligionRefId");
            CreateIndex("dbo.PersonalInformations", "EmployeeReligionRefId");
            AddForeignKey("dbo.PersonalInformations", "EmployeeReligionRefId", "dbo.EmployeeReligions", "Id", cascadeDelete: true);
        }
    }
}
