namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddImageHandlingMenuItem : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItems", "DownloadImage", c => c.Guid(nullable: false, defaultValue: Guid.NewGuid()));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MenuItems", "DownloadImage");
        }
    }
}
