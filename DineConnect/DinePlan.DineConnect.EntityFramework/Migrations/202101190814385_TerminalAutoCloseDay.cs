namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TerminalAutoCloseDay : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Terminals", "AutoDayClose", c => c.Boolean(nullable: false));
            AddColumn("dbo.Terminals", "AutoDayCloseSettings", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Terminals", "AutoDayCloseSettings");
            DropColumn("dbo.Terminals", "AutoDayClose");
        }
    }
}
