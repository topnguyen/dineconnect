namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserToDinePlanUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DinePlanUsers", "UserId", c => c.Long());
            CreateIndex("dbo.DinePlanUsers", "UserId");
            AddForeignKey("dbo.DinePlanUsers", "UserId", "dbo.AbpUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DinePlanUsers", "UserId", "dbo.AbpUsers");
            DropIndex("dbo.DinePlanUsers", new[] { "UserId" });
            DropColumn("dbo.DinePlanUsers", "UserId");
        }
    }
}
