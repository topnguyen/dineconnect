namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddCashAudit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CashAudits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocalId = c.Int(nullable: false),
                        LocationId = c.Int(nullable: false),
                        AuditTime = c.DateTime(nullable: false),
                        User = c.String(),
                        LoginBy = c.String(),
                        ApprovedBy = c.String(),
                        AuditValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ActualAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CashSales = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FloatAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UniqueGuid = c.Guid(nullable: false),
                        CashIn = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CashOut = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Denominations = c.String(),
                        ReasonId = c.String(),
                        Remark = c.String(),
                        WorkPeriodId = c.Int(nullable: false),
                        TerminalName = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CashAudit_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CashAudit_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CashAudits",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CashAudit_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CashAudit_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
