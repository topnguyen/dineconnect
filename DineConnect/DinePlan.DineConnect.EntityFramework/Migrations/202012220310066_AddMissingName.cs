namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMissingName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Numerators", "Name", c => c.String());
            AddColumn("dbo.TicketTypes", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TicketTypes", "Name");
            DropColumn("dbo.Numerators", "Name");
        }
    }
}
