namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_CostPrice_MenuItemPortion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItemPortions", "CostPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MenuItemPortions", "CostPrice");
        }
    }
}
