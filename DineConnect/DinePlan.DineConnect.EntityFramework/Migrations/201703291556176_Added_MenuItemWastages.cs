namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_MenuItemWastages : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MenuItemWastageDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MenuItemWastageRefId = c.Int(nullable: false),
                        Sno = c.Int(nullable: false),
                        PosMenuPortionRefId = c.Int(nullable: false),
                        WastageQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WastageRemarks = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MenuItemPortions", t => t.PosMenuPortionRefId)
                .ForeignKey("dbo.MenuItemWastages", t => t.MenuItemWastageRefId)
                .Index(t => t.MenuItemWastageRefId)
                .Index(t => t.PosMenuPortionRefId);
            
            CreateTable(
                "dbo.MenuItemWastages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        SalesDate = c.DateTime(nullable: false),
                        AccountDate = c.DateTime(),
                        AdjustmentRefId = c.Int(),
                        TokenRefNumber = c.Int(nullable: false),
                        Remarks = c.String(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MenuItemWastage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MenuItemWastage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Adjustments", t => t.AdjustmentRefId)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .Index(t => t.LocationRefId)
                .Index(t => t.AdjustmentRefId);

            Sql("DELETE FROM MaterialMenuMappings WHERE PosMenuPortionRefId NOT IN (SELECT ID FROM MenuItemPortions)");
            
            CreateIndex("dbo.MaterialMenuMappings", "PosMenuPortionRefId");
            AddForeignKey("dbo.MaterialMenuMappings", "PosMenuPortionRefId", "dbo.MenuItemPortions", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MenuItemWastageDetails", "MenuItemWastageRefId", "dbo.MenuItemWastages");
            DropForeignKey("dbo.MenuItemWastages", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.MenuItemWastages", "AdjustmentRefId", "dbo.Adjustments");
            DropForeignKey("dbo.MenuItemWastageDetails", "PosMenuPortionRefId", "dbo.MenuItemPortions");
            DropForeignKey("dbo.MaterialMenuMappings", "PosMenuPortionRefId", "dbo.MenuItemPortions");
            DropIndex("dbo.MenuItemWastages", new[] { "AdjustmentRefId" });
            DropIndex("dbo.MenuItemWastages", new[] { "LocationRefId" });
            DropIndex("dbo.MenuItemWastageDetails", new[] { "PosMenuPortionRefId" });
            DropIndex("dbo.MenuItemWastageDetails", new[] { "MenuItemWastageRefId" });
            DropIndex("dbo.MaterialMenuMappings", new[] { "PosMenuPortionRefId" });
            DropTable("dbo.MenuItemWastages",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MenuItemWastage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MenuItemWastage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MenuItemWastageDetails");
        }
    }
}
