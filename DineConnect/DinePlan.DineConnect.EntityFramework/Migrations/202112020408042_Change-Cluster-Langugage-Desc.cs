namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeClusterLangugageDesc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DelAggLanguages", "LanguageDescriptionType", c => c.Int(nullable: false));
            DropColumn("dbo.DelAggLanguages", "LanguageDescription");
            DropColumn("dbo.DelAggLanguages", "Key");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DelAggLanguages", "Key", c => c.String());
            AddColumn("dbo.DelAggLanguages", "LanguageDescription", c => c.String());
            DropColumn("dbo.DelAggLanguages", "LanguageDescriptionType");
        }
    }
}
