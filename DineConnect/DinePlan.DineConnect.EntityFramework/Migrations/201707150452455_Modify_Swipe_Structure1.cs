namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Swipe_Structure1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SwipeCardLedgers", "TicketId", c => c.Int());
            CreateIndex("dbo.SwipeCardLedgers", "TicketId");
            AddForeignKey("dbo.SwipeCardLedgers", "TicketId", "dbo.Tickets", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SwipeCardLedgers", "TicketId", "dbo.Tickets");
            DropIndex("dbo.SwipeCardLedgers", new[] { "TicketId" });
            DropColumn("dbo.SwipeCardLedgers", "TicketId");
        }
    }
}
