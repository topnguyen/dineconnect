namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Personal_Information_RawId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PersonalInformations", "RawId", c => c.Int());
            CreateIndex("dbo.PersonalInformations", "RawId");
            AddForeignKey("dbo.PersonalInformations", "RawId", "dbo.EmployeeRawInfoes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PersonalInformations", "RawId", "dbo.EmployeeRawInfoes");
            DropIndex("dbo.PersonalInformations", new[] { "RawId" });
            DropColumn("dbo.PersonalInformations", "RawId");
        }
    }
}
