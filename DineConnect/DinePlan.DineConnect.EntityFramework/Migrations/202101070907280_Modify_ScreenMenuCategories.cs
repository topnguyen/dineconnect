namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_ScreenMenuCategories : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ScreenMenuCategories", "StartDate", c => c.DateTime());
            AddColumn("dbo.ScreenMenuCategories", "EndDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ScreenMenuCategories", "EndDate");
            DropColumn("dbo.ScreenMenuCategories", "StartDate");
        }
    }
}
