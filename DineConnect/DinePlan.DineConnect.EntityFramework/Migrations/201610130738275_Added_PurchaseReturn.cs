namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_PurchaseReturn : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PurchaseReturnDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PurchaseReturnRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 6),
                        UnitRefId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseReturnDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId, cascadeDelete: true)
                .Index(t => t.MaterialRefId);
            
            CreateTable(
                "dbo.PurchaseReturns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SupplierRefId = c.Int(nullable: false),
                        ReturnDate = c.DateTime(nullable: false),
                        ReferenceNumber = c.String(),
                        DcRefId = c.Int(),
                        InvoiceRefId = c.Int(),
                        Remarks = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseReturn_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PurchaseReturn_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Suppliers", t => t.SupplierRefId, cascadeDelete: true)
                .Index(t => t.SupplierRefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PurchaseReturns", "SupplierRefId", "dbo.Suppliers");
            DropForeignKey("dbo.PurchaseReturnDetails", "MaterialRefId", "dbo.Materials");
            DropIndex("dbo.PurchaseReturns", new[] { "SupplierRefId" });
            DropIndex("dbo.PurchaseReturnDetails", new[] { "MaterialRefId" });
            DropTable("dbo.PurchaseReturns",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseReturn_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PurchaseReturn_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PurchaseReturnDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseReturnDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
