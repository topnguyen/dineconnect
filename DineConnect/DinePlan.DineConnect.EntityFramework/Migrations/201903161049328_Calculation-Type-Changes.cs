namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class CalculationTypeChanges : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Calculations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ButterHeader = c.String(),
                        SortOrder = c.Int(nullable: false),
                        FontSize = c.Int(nullable: false),
                        ConfirmationType = c.Int(nullable: false),
                        CalculationTypeId = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IncludeTax = c.Boolean(nullable: false),
                        DecreaseAmount = c.Boolean(nullable: false),
                        TransactionTypeId = c.Int(nullable: false),
                        Departments = c.String(),
                        TenantId = c.Int(nullable: false),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        Oid = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Calculation_ConnectFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Calculation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Calculation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TransactionTypes", t => t.TransactionTypeId, cascadeDelete: true)
                .Index(t => t.TransactionTypeId);
            
            AddColumn("dbo.Locations", "Settings", c => c.String());
            AddColumn("dbo.MenuItems", "NoTax", c => c.Boolean(nullable: false));
            AddColumn("dbo.PaymentTypes", "ButtonHeader", c => c.String());
            AddColumn("dbo.PaymentTypes", "ButtonColor", c => c.String());
            AddColumn("dbo.PaymentTypes", "Departments", c => c.String());
            AddColumn("dbo.FreePromotionExecutions", "CategoryId", c => c.Int(nullable: false));
            AddColumn("dbo.FreeValuePromotionExecution", "CategoryId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Calculations", "TransactionTypeId", "dbo.TransactionTypes");
            DropIndex("dbo.Calculations", new[] { "TransactionTypeId" });
            DropColumn("dbo.FreeValuePromotionExecution", "CategoryId");
            DropColumn("dbo.FreePromotionExecutions", "CategoryId");
            DropColumn("dbo.PaymentTypes", "Departments");
            DropColumn("dbo.PaymentTypes", "ButtonColor");
            DropColumn("dbo.PaymentTypes", "ButtonHeader");
            DropColumn("dbo.MenuItems", "NoTax");
            DropColumn("dbo.Locations", "Settings");
            DropTable("dbo.Calculations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Calculation_ConnectFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Calculation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Calculation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
