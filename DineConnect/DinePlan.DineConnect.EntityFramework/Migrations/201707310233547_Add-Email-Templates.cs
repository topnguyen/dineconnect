namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmailTemplates : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmailTemplates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        Name = c.String(),
                        Template = c.String(),
                        Variables = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmailTemplate_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmailTemplate_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FreeItemPromotionExecutions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FreeItemPromotionId = c.Int(nullable: false),
                        MenuItemId = c.Int(nullable: false),
                        PortionName = c.String(),
                        MenuItemPortionId = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FreeItemPromotions", t => t.FreeItemPromotionId, cascadeDelete: true)
                .Index(t => t.FreeItemPromotionId);
            
            CreateTable(
                "dbo.FreeItemPromotions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PromotionId = c.Int(nullable: false),
                        ItemCount = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Promotions", t => t.PromotionId, cascadeDelete: true)
                .Index(t => t.PromotionId);
            
            AddColumn("dbo.PaymentTypes", "PaymentProcessor", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FreeItemPromotions", "PromotionId", "dbo.Promotions");
            DropForeignKey("dbo.FreeItemPromotionExecutions", "FreeItemPromotionId", "dbo.FreeItemPromotions");
            DropIndex("dbo.FreeItemPromotions", new[] { "PromotionId" });
            DropIndex("dbo.FreeItemPromotionExecutions", new[] { "FreeItemPromotionId" });
            DropColumn("dbo.PaymentTypes", "PaymentProcessor");
            DropTable("dbo.FreeItemPromotions");
            DropTable("dbo.FreeItemPromotionExecutions");
            DropTable("dbo.EmailTemplates",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmailTemplate_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmailTemplate_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
