namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_BackgroundClose : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "BackGroundStartTime", c => c.DateTime());
            AddColumn("dbo.Locations", "BackGrandEndTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "BackGrandEndTime");
            DropColumn("dbo.Locations", "BackGroundStartTime");
        }
    }
}
