namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyLocation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "IsPurchaseAllowed", c => c.Boolean(nullable: false));
            AddColumn("dbo.Locations", "IsProductionAllowed", c => c.Boolean(nullable: false));
            AddColumn("dbo.Locations", "DefaultRequestLocationRefId", c => c.Int(nullable: false));
            AddColumn("dbo.Locations", "RequestedLocations", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "RequestedLocations");
            DropColumn("dbo.Locations", "DefaultRequestLocationRefId");
            DropColumn("dbo.Locations", "IsProductionAllowed");
            DropColumn("dbo.Locations", "IsPurchaseAllowed");
        }
    }
}
