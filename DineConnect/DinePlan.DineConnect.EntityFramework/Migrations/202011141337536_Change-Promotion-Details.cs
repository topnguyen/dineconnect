namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePromotionDetails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PromotionCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        Locations = c.String(),
                        NonLocations = c.String(),
                        Group = c.Boolean(nullable: false),
                        LocationTag = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PromotionCategory_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PromotionCategory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Promotions", "PromotionCategoryId", c => c.Int());
            AddColumn("dbo.PromotionSchedules", "StartDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.PromotionSchedules", "EndDate", c => c.DateTime(nullable: false));
            CreateIndex("dbo.Promotions", "PromotionCategoryId");
            AddForeignKey("dbo.Promotions", "PromotionCategoryId", "dbo.PromotionCategories", "Id");
            DropColumn("dbo.Promotions", "RefCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Promotions", "RefCode", c => c.String());
            DropForeignKey("dbo.Promotions", "PromotionCategoryId", "dbo.PromotionCategories");
            DropIndex("dbo.Promotions", new[] { "PromotionCategoryId" });
            DropColumn("dbo.PromotionSchedules", "EndDate");
            DropColumn("dbo.PromotionSchedules", "StartDate");
            DropColumn("dbo.Promotions", "PromotionCategoryId");
            DropTable("dbo.PromotionCategories",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PromotionCategory_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PromotionCategory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
