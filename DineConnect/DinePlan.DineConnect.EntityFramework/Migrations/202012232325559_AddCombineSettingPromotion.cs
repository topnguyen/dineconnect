namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCombineSettingPromotion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Promotions", "CombineAllPro", c => c.Boolean(nullable: false));
            AddColumn("dbo.Promotions", "CombinePro", c => c.Boolean(nullable: false));
            AddColumn("dbo.Promotions", "CombineProValues", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Promotions", "CombineProValues");
            DropColumn("dbo.Promotions", "CombinePro");
            DropColumn("dbo.Promotions", "CombineAllPro");
        }
    }
}
