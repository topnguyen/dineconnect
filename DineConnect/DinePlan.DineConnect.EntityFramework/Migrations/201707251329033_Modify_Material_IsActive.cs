namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Material_IsActive : DbMigration
    {
        public override void Up()
        {
			AddColumn("dbo.Materials", "IsActive", c => c.Boolean(nullable: false, defaultValue: true));
		}
        
        public override void Down()
        {
            DropColumn("dbo.Materials", "IsActive");
        }
    }
}
