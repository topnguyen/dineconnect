namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_OrderChannel_To_Customer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CaterCustomers", "OrderChannel", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CaterCustomers", "OrderChannel");
        }
    }
}
