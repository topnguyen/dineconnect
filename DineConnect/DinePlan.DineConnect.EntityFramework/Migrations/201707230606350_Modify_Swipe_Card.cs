namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Swipe_Card : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SwipeCards", "DepositRequired", c => c.Boolean(nullable: false));
            AddColumn("dbo.SwipeCards", "DepositAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SwipeCards", "ExpiryDaysFromIssue", c => c.Int(nullable: false));
            AddColumn("dbo.SwipeCards", "RefundAllowed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SwipeCards", "RefundAllowed");
            DropColumn("dbo.SwipeCards", "ExpiryDaysFromIssue");
            DropColumn("dbo.SwipeCards", "DepositAmount");
            DropColumn("dbo.SwipeCards", "DepositRequired");
        }
    }
}
