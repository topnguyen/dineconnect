namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDelAggTimingDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DelTimingDetails", "AddOns", c => c.String());
            DropColumn("dbo.DelTimingDetails", "StartTime");
            DropColumn("dbo.DelTimingDetails", "EndTime");
            AddColumn("dbo.DelTimingDetails", "StartHour", c => c.Int());
            AddColumn("dbo.DelTimingDetails", "StartMinute", c => c.Int());
            AddColumn("dbo.DelTimingDetails", "EndHour", c => c.Int());
            AddColumn("dbo.DelTimingDetails", "EndMinute", c => c.Int());

        }
        
        public override void Down()
        {
            DropColumn("dbo.DelTimingDetails", "AddOns");
            DropColumn("dbo.DelTimingDetails", "StartHour");
            DropColumn("dbo.DelTimingDetails", "StartMinute");
            DropColumn("dbo.DelTimingDetails", "EndHour");
            DropColumn("dbo.DelTimingDetails", "EndMinute");
        }
    }
}
