namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_SwipeShift : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.SwipeShifts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ShiftStartTime = c.DateTime(nullable: false),
                        ShiftEndTime = c.DateTime(),
                        TenderOpenAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenderCredit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenderDebit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenderCloseAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ActualTenderClosing = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UserTenderEntered = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ExcessShortageStatus = c.String(),
                        ExcessShortageAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_SwipeShift_SoftDelete",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.SwipeShiftTender",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ShiftRefId = c.Int(nullable: false),
                        ShiftInOutStatus = c.Int(nullable: false),
                        PaymentTypeId = c.Int(nullable: false),
                        ActualAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenderedAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_SwipeShiftTender_SoftDelete",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AddColumn("dbo.SwipeShifts", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.SwipeShifts", "DeleterUserId", c => c.Long());
            AddColumn("dbo.SwipeShifts", "DeletionTime", c => c.DateTime());
            AddColumn("dbo.SwipeShifts", "LastModificationTime", c => c.DateTime());
            AddColumn("dbo.SwipeShifts", "LastModifierUserId", c => c.Long());
            AddColumn("dbo.SwipeShiftTender", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.SwipeShiftTender", "DeleterUserId", c => c.Long());
            AddColumn("dbo.SwipeShiftTender", "DeletionTime", c => c.DateTime());
            AddColumn("dbo.SwipeShiftTender", "LastModificationTime", c => c.DateTime());
            AddColumn("dbo.SwipeShiftTender", "LastModifierUserId", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SwipeShiftTender", "LastModifierUserId");
            DropColumn("dbo.SwipeShiftTender", "LastModificationTime");
            DropColumn("dbo.SwipeShiftTender", "DeletionTime");
            DropColumn("dbo.SwipeShiftTender", "DeleterUserId");
            DropColumn("dbo.SwipeShiftTender", "IsDeleted");
            DropColumn("dbo.SwipeShifts", "LastModifierUserId");
            DropColumn("dbo.SwipeShifts", "LastModificationTime");
            DropColumn("dbo.SwipeShifts", "DeletionTime");
            DropColumn("dbo.SwipeShifts", "DeleterUserId");
            DropColumn("dbo.SwipeShifts", "IsDeleted");
            AlterTableAnnotations(
                "dbo.SwipeShiftTender",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ShiftRefId = c.Int(nullable: false),
                        ShiftInOutStatus = c.Int(nullable: false),
                        PaymentTypeId = c.Int(nullable: false),
                        ActualAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenderedAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_SwipeShiftTender_SoftDelete",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.SwipeShifts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ShiftStartTime = c.DateTime(nullable: false),
                        ShiftEndTime = c.DateTime(),
                        TenderOpenAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenderCredit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenderDebit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenderCloseAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ActualTenderClosing = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UserTenderEntered = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ExcessShortageStatus = c.String(),
                        ExcessShortageAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_SwipeShift_SoftDelete",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
        }
    }
}
