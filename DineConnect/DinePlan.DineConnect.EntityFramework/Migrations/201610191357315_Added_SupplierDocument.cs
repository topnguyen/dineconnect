namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_SupplierDocument : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SupplierDocuments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentRefId = c.Int(nullable: false),
                        SupplierRefId = c.Int(nullable: false),
                        FileName = c.String(nullable: false),
                        IsEntryFinished = c.Boolean(nullable: false),
                        CompletedTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SupplierDocument_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SupplierDocuments",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SupplierDocument_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
