namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPortionBarcode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItemPortions", "Barcode", c => c.String(maxLength: 50));
            AlterColumn("dbo.MenuItemPortions", "Name", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MenuItemPortions", "Name", c => c.String());
            DropColumn("dbo.MenuItemPortions", "Barcode");
        }
    }
}
