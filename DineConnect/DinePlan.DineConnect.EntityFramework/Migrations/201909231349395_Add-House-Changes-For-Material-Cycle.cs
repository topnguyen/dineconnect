namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddHouseChangesForMaterialCycle : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MaterialStockCycleLinks", "LocationGroupRefId", "dbo.LocationGroups");
            DropForeignKey("dbo.MaterialStockCycleLinks", "LocationRefId", "dbo.Locations");
            DropIndex("dbo.MaterialStockCycleLinks", "MPK_LocationGroupId_LocationRefId_MaterialRefId_InvCycleRefId");
            CreateTable(
                "dbo.ClosingStockUnitWiseDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClosingStockDetailRefId = c.Int(nullable: false),
                        OnHand = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitRefId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ClosingStockUnitWiseDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClosingStockDetails", t => t.ClosingStockDetailRefId, cascadeDelete: true)
                .ForeignKey("dbo.Units", t => t.UnitRefId, cascadeDelete: true)
                .Index(t => t.ClosingStockDetailRefId)
                .Index(t => t.UnitRefId);
            
            CreateTable(
                "dbo.InventoryCycleLinkWithLocations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InventoryCycleTagRefId = c.Int(nullable: false),
                        LocationGroupRefId = c.Int(),
                        LocationTagRefId = c.Int(),
                        LocationRefId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InventoryCycleLinkWithLocation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InventoryCycleLinkWithLocation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.InventoryCycleTags", t => t.InventoryCycleTagRefId, cascadeDelete: true)
                .ForeignKey("dbo.LocationGroups", t => t.LocationGroupRefId)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .ForeignKey("dbo.LocationTags", t => t.LocationTagRefId)
                .Index(t => new { t.InventoryCycleTagRefId, t.LocationGroupRefId, t.LocationTagRefId, t.LocationRefId }, unique: true, name: "MPK_LocationGroupId_LocationRefId_LocationTagRefId_InvCycleRefId");
            
            CreateIndex("dbo.InventoryCycleTags", "TenantId");
            CreateIndex("dbo.MaterialStockCycleLinks", "MaterialRefId");
            CreateIndex("dbo.MaterialStockCycleLinks", "InventoryCycleTagRefId");
            AddForeignKey("dbo.InventoryCycleTags", "TenantId", "dbo.AbpTenants", "Id", cascadeDelete: true);
            DropColumn("dbo.MaterialStockCycleLinks", "LocationGroupRefId");
            DropColumn("dbo.MaterialStockCycleLinks", "LocationRefId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MaterialStockCycleLinks", "LocationRefId", c => c.Int());
            AddColumn("dbo.MaterialStockCycleLinks", "LocationGroupRefId", c => c.Int());
            DropForeignKey("dbo.InventoryCycleLinkWithLocations", "LocationTagRefId", "dbo.LocationTags");
            DropForeignKey("dbo.InventoryCycleLinkWithLocations", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.InventoryCycleLinkWithLocations", "LocationGroupRefId", "dbo.LocationGroups");
            DropForeignKey("dbo.InventoryCycleLinkWithLocations", "InventoryCycleTagRefId", "dbo.InventoryCycleTags");
            DropForeignKey("dbo.InventoryCycleTags", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.ClosingStockUnitWiseDetails", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.ClosingStockUnitWiseDetails", "ClosingStockDetailRefId", "dbo.ClosingStockDetails");
            DropIndex("dbo.MaterialStockCycleLinks", new[] { "InventoryCycleTagRefId" });
            DropIndex("dbo.MaterialStockCycleLinks", new[] { "MaterialRefId" });
            DropIndex("dbo.InventoryCycleTags", new[] { "TenantId" });
            DropIndex("dbo.InventoryCycleLinkWithLocations", "MPK_LocationGroupId_LocationRefId_LocationTagRefId_InvCycleRefId");
            DropIndex("dbo.ClosingStockUnitWiseDetails", new[] { "UnitRefId" });
            DropIndex("dbo.ClosingStockUnitWiseDetails", new[] { "ClosingStockDetailRefId" });
            DropTable("dbo.InventoryCycleLinkWithLocations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InventoryCycleLinkWithLocation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InventoryCycleLinkWithLocation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ClosingStockUnitWiseDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ClosingStockUnitWiseDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            CreateIndex("dbo.MaterialStockCycleLinks", new[] { "MaterialRefId", "InventoryCycleTagRefId", "LocationRefId", "LocationGroupRefId" }, unique: true, name: "MPK_LocationGroupId_LocationRefId_MaterialRefId_InvCycleRefId");
            AddForeignKey("dbo.MaterialStockCycleLinks", "LocationRefId", "dbo.Locations", "Id");
            AddForeignKey("dbo.MaterialStockCycleLinks", "LocationGroupRefId", "dbo.LocationGroups", "Id");
        }
    }
}
