namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Purchase_Category : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PurchaseCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PurchaseCategoryName = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseCategory_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PurchaseCategory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Invoices", "PurchaseCategoryRefId", c => c.Int());
            CreateIndex("dbo.Invoices", "PurchaseCategoryRefId");
            AddForeignKey("dbo.Invoices", "PurchaseCategoryRefId", "dbo.PurchaseCategories", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Invoices", "PurchaseCategoryRefId", "dbo.PurchaseCategories");
            DropIndex("dbo.Invoices", new[] { "PurchaseCategoryRefId" });
            DropColumn("dbo.Invoices", "PurchaseCategoryRefId");
            DropTable("dbo.PurchaseCategories",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseCategory_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PurchaseCategory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
