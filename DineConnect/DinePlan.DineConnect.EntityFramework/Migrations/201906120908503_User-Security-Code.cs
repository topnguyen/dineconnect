namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserSecurityCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DinePlanUsers", "SecurityCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DinePlanUsers", "SecurityCode");
        }
    }
}
