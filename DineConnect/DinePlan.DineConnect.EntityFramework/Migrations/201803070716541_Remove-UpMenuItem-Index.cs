namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveUpMenuItemIndex : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.UpMenuItems", new[] { "RefMenuItemId" });
            AddColumn("dbo.UpMenuItems", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UpMenuItems", "Price");
            CreateIndex("dbo.UpMenuItems", "RefMenuItemId", unique: true);
        }
    }
}
