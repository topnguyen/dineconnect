namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CompanyApprovalid : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Companies", "GovtApprovalId", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Companies", "GovtApprovalId", c => c.Int(nullable: false));
        }
    }
}
