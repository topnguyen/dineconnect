namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUpMenuItemAutoAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UpMenuItems", "MinimumQuantity", c => c.Int(nullable: false));
            AddColumn("dbo.UpMenuItems", "AddAuto", c => c.Boolean(nullable: false));
            AddColumn("dbo.UpMenuItems", "AddQuantity", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UpMenuItems", "AddQuantity");
            DropColumn("dbo.UpMenuItems", "AddAuto");
            DropColumn("dbo.UpMenuItems", "MinimumQuantity");
        }
    }
}
