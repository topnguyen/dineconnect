namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UnitConversion_DecimalPlaceRounding : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UnitConversions", "DecimalPlaceRounding", c => c.Byte(nullable: false));
            DropColumn("dbo.UnitConversions", "FlatConversion");
			Sql("UPDATE dbo.UnitConversions SET DecimalPlaceRounding = 6");
			Sql("UPDATE [dbo].[UnitConversions] SET [DecimalPlaceRounding] = 0 WHERE RefUnitId IN (SELECT ID FROM DBO.Units WHERE UPPER(NAME) IN('PC', 'PIECE', 'PIECES', 'NUMBER', 'NO'))");
		}
        
        public override void Down()
        {
            AddColumn("dbo.UnitConversions", "FlatConversion", c => c.Single(nullable: false));
            DropColumn("dbo.UnitConversions", "DecimalPlaceRounding");
        }
    }
}
