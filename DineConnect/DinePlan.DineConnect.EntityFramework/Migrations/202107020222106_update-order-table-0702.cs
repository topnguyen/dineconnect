namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateordertable0702 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "BindState", c => c.String());
            AddColumn("dbo.Orders", "BindStateValues", c => c.String());
            AddColumn("dbo.Orders", "BindCompleted", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MenuItemPortions", "PreparationTime", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MenuItemPortions", "PreparationTime", c => c.Int(nullable: false));
            DropColumn("dbo.Orders", "BindCompleted");
            DropColumn("dbo.Orders", "BindStateValues");
            DropColumn("dbo.Orders", "BindState");
        }
    }
}
