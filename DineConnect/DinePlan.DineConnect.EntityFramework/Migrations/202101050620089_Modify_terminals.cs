namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_terminals : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Terminals", "Tid", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Terminals", "Tid");
        }
    }
}
