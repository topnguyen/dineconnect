namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_HR_Structure : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AttendanceLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AttendanceMachineLocationRefId = c.Int(nullable: false),
                        EmployeeRefId = c.Int(nullable: false),
                        CheckTime = c.DateTime(),
                        CheckTypeCode = c.String(),
                        VerifyMode = c.Int(nullable: false),
                        AuthorisedBy = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_AttendanceLog_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_AttendanceLog_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AttendanceMachineLocations", t => t.AttendanceMachineLocationRefId)
                .ForeignKey("dbo.PersonalInformations", t => t.EmployeeRefId)
                .ForeignKey("dbo.PersonalInformations", t => t.AuthorisedBy)
                .Index(t => t.AttendanceMachineLocationRefId)
                .Index(t => t.EmployeeRefId)
                .Index(t => t.AuthorisedBy);
            
            CreateTable(
                "dbo.AttendanceMachineLocations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        MachineLocation = c.String(),
                        MachinePrefix = c.String(maxLength: 4),
                        IsActive = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_AttendanceMachineLocation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_AttendanceMachineLocation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .Index(t => t.LocationRefId)
                .Index(t => t.MachinePrefix, unique: true, name: "MachinePrefix");
            
            CreateTable(
                "dbo.PersonalInformations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        BioMetricCode = c.String(nullable: false, maxLength: 20),
                        UserSerialNumber = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EmployeeGivenName = c.String(nullable: false),
                        EmployeeSurName = c.String(nullable: false),
                        EmployeeName = c.String(nullable: false),
                        EmployeeCode = c.String(nullable: false, maxLength: 20),
                        PerAddress1 = c.String(),
                        PerAddress2 = c.String(),
                        PerAddress3 = c.String(),
                        PerAddress4 = c.String(),
                        PostalCode = c.String(),
                        ComAddress1 = c.String(),
                        ComAddress2 = c.String(),
                        ComAddress3 = c.String(),
                        ComAddress4 = c.String(),
                        ZipCode = c.String(),
                        StaffType = c.String(nullable: false),
                        NationalIdentificationNumber = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        Gender = c.String(),
                        MaritalStatus = c.String(),
                        MaritalDate = c.DateTime(),
                        PassPortNumber = c.String(),
                        DateHired = c.DateTime(nullable: false),
                        JobTitle = c.String(),
                        LastWorkingDate = c.DateTime(),
                        ActiveStatus = c.Boolean(nullable: false),
                        ProfilePictureId = c.Guid(),
                        DefaultSkillRefId = c.Int(nullable: false),
                        DefaultWeekOff = c.Int(nullable: false),
                        IsAttendanceRequired = c.Boolean(nullable: false),
                        HandPhone1 = c.String(),
                        HandPhone2 = c.String(),
                        Email = c.String(),
                        PersonalEmail = c.String(),
                        IncentiveAllowedFlag = c.Boolean(nullable: false),
                        SupervisorEmployeeRefId = c.Int(),
                        InchargeEmployeeRefId = c.Int(),
                        IsFixedCostCentreInDutyChart = c.Boolean(nullable: false),
                        DutyFixedUptoDate = c.DateTime(),
                        IsSmartAttendanceFingerAllowed = c.Boolean(nullable: false),
                        IsDayCloseStatusMailRequired = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PersonalInformation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PersonalInformation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.LocationRefId)
                .Index(t => t.BioMetricCode, unique: true, name: "BioMetricCode")
                .Index(t => t.EmployeeCode, unique: true, name: "EmployeeCode")
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.CommonMailMessages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MailTime = c.DateTime(nullable: false),
                        MessageType = c.Int(nullable: false),
                        ReceipientsMailIdsList = c.String(),
                        MessageText = c.String(),
                        MessageApprovedBy = c.Long(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CommonMailMessage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CommonMailMessage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.DocumentForSkillSets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentRefId = c.Int(nullable: false),
                        SkillSetRefId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DocumentForSkillSet_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DocumentForSkillSet_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DocumentInfos", t => t.DocumentRefId)
                .ForeignKey("dbo.SkillSets", t => t.SkillSetRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => new { t.DocumentRefId, t.SkillSetRefId }, unique: true, name: "MPK_DocumentRefId_SkillRefId")
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.DocumentInfos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentName = c.String(nullable: false),
                        DocumentType = c.Int(nullable: false),
                        LifeTimeFlag = c.Boolean(nullable: false),
                        DefaultAlertDays = c.Int(nullable: false),
                        DefaultExpiryDays = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DocumentInfo_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DocumentInfo_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.SkillSets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SkillCode = c.String(nullable: false, maxLength: 20),
                        Description = c.String(nullable: false, maxLength: 40),
                        IsActive = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SkillSet_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SkillSet_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.SkillCode, unique: true, name: "SkillCode")
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.EMailInformations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmailId = c.String(),
                        Password = c.String(),
                        SmtpPort = c.String(),
                        SmtpHost = c.String(),
                        SmtpDeliveryMethod = c.Int(nullable: false),
                        SmtpTimeOutSeconds = c.Int(nullable: false),
                        SSLStatus = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EMailInformation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EMailInformation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.EMailLinkedWithAction",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EMailInformationRefId = c.Int(nullable: false),
                        MailActionRefId = c.Int(nullable: false),
                        ToEmailList = c.String(),
                        CCEmailList = c.String(),
                        BCCEmailList = c.String(),
                        SupervisorEmployeeRefId = c.Int(),
                        InchargeEmployeeRefId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EMailLinkedWithActions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EMailInformations", t => t.EMailInformationRefId)
                .ForeignKey("dbo.PersonalInformations", t => t.InchargeEmployeeRefId)
                .ForeignKey("dbo.PersonalInformations", t => t.SupervisorEmployeeRefId)
                .Index(t => t.EMailInformationRefId)
                .Index(t => t.SupervisorEmployeeRefId)
                .Index(t => t.InchargeEmployeeRefId);
            
            CreateTable(
                "dbo.EmployeeDocumentInfos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeRefId = c.Int(nullable: false),
                        DocumentInfoRefId = c.Int(nullable: false),
                        DocReferenceNumber = c.String(),
                        DefaultAlertDays = c.Int(nullable: false),
                        LifeTimeFlag = c.Boolean(nullable: false),
                        StartDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        FileExtenstionType = c.String(nullable: false),
                        FileName = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeDocumentInfo_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeDocumentInfo_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DocumentInfos", t => t.DocumentInfoRefId)
                .ForeignKey("dbo.PersonalInformations", t => t.EmployeeRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.EmployeeRefId)
                .Index(t => t.DocumentInfoRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.EmployeeMailMessages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeRefId = c.Int(nullable: false),
                        MailTime = c.DateTime(nullable: false),
                        DutyChartDate = c.DateTime(),
                        DutyStatusMailed = c.String(),
                        MessageType = c.Int(nullable: false),
                        ReceipientsMailIdsList = c.String(),
                        MessageText = c.String(),
                        MessageApprovedBy = c.Long(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeMailMessage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeMailMessage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonalInformations", t => t.EmployeeRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.EmployeeRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.EmployeeSkillSets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeRefId = c.Int(nullable: false),
                        SkillSetRefId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeSkillSet_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeSkillSet_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonalInformations", t => t.EmployeeRefId)
                .ForeignKey("dbo.SkillSets", t => t.SkillSetRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => new { t.EmployeeRefId, t.SkillSetRefId }, unique: true, name: "MPK_EmpRefId_SkillRefId")
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.GpsAttendances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeRefId = c.Int(nullable: false),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        Altitude = c.Double(nullable: false),
                        Address = c.String(),
                        Photo = c.Binary(),
                        AttendanceStatus = c.Int(nullable: false),
                        LoginEmployeeRefId = c.Int(nullable: false),
                        IsSmartAttendanceFingerReceived = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_GpsAttendance_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonalInformations", t => t.EmployeeRefId)
                .Index(t => t.EmployeeRefId);
            
            CreateTable(
                "dbo.GpsLive",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeRefId = c.Int(nullable: false),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        Altitude = c.Double(nullable: false),
                        Address = c.String(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LeaveExhaustedLists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LeaveTypeRefId = c.Int(nullable: false),
                        LeaveExhaustedRefId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LeaveExhaustedList_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_LeaveExhaustedList_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LeaveTypes", t => t.LeaveTypeRefId)
                .ForeignKey("dbo.LeaveTypes", t => t.LeaveExhaustedRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.LeaveTypeRefId)
                .Index(t => t.LeaveExhaustedRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.LeaveTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LeaveTypeName = c.String(nullable: false),
                        LeaveTypeShortName = c.String(nullable: false),
                        NumberOfLeaveAllowed = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DefaultNumberOfLeaveAllowed = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CalculationPeriodStarts = c.DateTime(nullable: false),
                        CalculationPeriodEnds = c.DateTime(nullable: false),
                        MaleGenderAllowed = c.Boolean(nullable: false),
                        FemaleGenderAllowed = c.Boolean(nullable: false),
                        IsSupportingDocumentsRequired = c.Boolean(nullable: false),
                        EligibleOnlyAnnualLeaveExhausted = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LeaveType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LeaveRequests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeRefId = c.Int(nullable: false),
                        LeaveTypeRefCode = c.Int(nullable: false),
                        DateOfApply = c.DateTime(nullable: false),
                        LeaveFrom = c.DateTime(nullable: false),
                        LeaveTo = c.DateTime(nullable: false),
                        TotalNumberOfDays = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LeaveReason = c.String(nullable: false),
                        LeaveStatus = c.String(),
                        HandOverWorkTo = c.String(),
                        ContactOfWorkIncharge = c.String(),
                        HalfDayFlag = c.Boolean(nullable: false),
                        HalfDayTimeSpecification = c.String(),
                        ApprovedPersonRefId = c.Int(),
                        ApprovedTime = c.DateTime(),
                        ApprovedRemarks = c.String(),
                        FileName = c.String(),
                        FileExtenstionType = c.String(),
                        IsSupportingDocumentsOverRide = c.Boolean(nullable: false),
                        SupportingDocumentsOverRideUserId = c.Int(),
                        CommonMailMessageRefId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LeaveRequest_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CommonMailMessages", t => t.CommonMailMessageRefId)
                .ForeignKey("dbo.LeaveTypes", t => t.LeaveTypeRefCode)
                .ForeignKey("dbo.PersonalInformations", t => t.EmployeeRefId)
                .Index(t => t.EmployeeRefId)
                .Index(t => t.LeaveTypeRefCode)
                .Index(t => t.CommonMailMessageRefId);
            
            CreateTable(
                "dbo.PublicHolidays",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HolidayDate = c.DateTime(nullable: false),
                        HolidayReason = c.String(),
                        OTProportion = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PublicHoliday_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PublicHoliday_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.SalaryInfos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeRefId = c.Int(nullable: false),
                        SalaryMode = c.Int(nullable: false),
                        WithEffectFrom = c.DateTime(),
                        BasicSalary = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Allowance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Deduction = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Accomodation = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Levy = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Ot1PerHour = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Ot2PerHour = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OtAllowedIfWorkTimeOver = c.Boolean(nullable: false),
                        ReportOtPerHour = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalaryInfo_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalaryInfo_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonalInformations", t => t.EmployeeRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.EmployeeRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.UserDefaultInformations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Long(nullable: false),
                        EmployeeRefId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserDefaultInformation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_UserDefaultInformation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonalInformations", t => t.EmployeeRefId)
                .ForeignKey("dbo.AbpUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.EmployeeRefId);
            
            CreateTable(
                "dbo.WorkDays",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DayOfWeekRefId = c.Int(nullable: false),
                        DayOfWeekRefName = c.String(nullable: false, maxLength: 15),
                        DayOfWeekShortName = c.String(nullable: false, maxLength: 10),
                        NoOfHourWorks = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RestHours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OtRatio = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_WorkDay_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_WorkDay_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.TenantId, t.DayOfWeekRefId }, unique: true, name: "TenantId_DayOfWeekRefId")
                .Index(t => new { t.TenantId, t.DayOfWeekRefName }, unique: true, name: "TenantId_DayOfWeekRefName")
                .Index(t => t.DayOfWeekShortName, unique: true, name: "DayOfWeekShortName");
            
            CreateTable(
                "dbo.YearWiseLeaveAllowedForEmployees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AcYear = c.Int(nullable: false),
                        LeaveAcYearFrom = c.DateTime(nullable: false),
                        LeaveAcYearTo = c.DateTime(nullable: false),
                        EmployeeRefId = c.Int(nullable: false),
                        LeaveTypeRefCode = c.Int(nullable: false),
                        NumberOfLeaveAllowed = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_YearWiseLeaveAllowedForEmployee_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_YearWiseLeaveAllowedForEmployee_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LeaveTypes", t => t.LeaveTypeRefCode)
                .ForeignKey("dbo.PersonalInformations", t => t.EmployeeRefId)
                .Index(t => t.EmployeeRefId)
                .Index(t => t.LeaveTypeRefCode);
            
            AddColumn("dbo.Companies", "NeedToTreatAnyWorkDayAsHalfDay", c => c.Boolean(nullable: false));
            AddColumn("dbo.Companies", "WeekDayTreatAsHalfDay", c => c.Int());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.YearWiseLeaveAllowedForEmployees", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.YearWiseLeaveAllowedForEmployees", "LeaveTypeRefCode", "dbo.LeaveTypes");
            DropForeignKey("dbo.UserDefaultInformations", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.UserDefaultInformations", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.SalaryInfos", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.SalaryInfos", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.PublicHolidays", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.LeaveRequests", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.LeaveRequests", "LeaveTypeRefCode", "dbo.LeaveTypes");
            DropForeignKey("dbo.LeaveRequests", "CommonMailMessageRefId", "dbo.CommonMailMessages");
            DropForeignKey("dbo.LeaveExhaustedLists", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.LeaveExhaustedLists", "LeaveExhaustedRefId", "dbo.LeaveTypes");
            DropForeignKey("dbo.LeaveExhaustedLists", "LeaveTypeRefId", "dbo.LeaveTypes");
            DropForeignKey("dbo.GpsAttendances", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.EmployeeSkillSets", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.EmployeeSkillSets", "SkillSetRefId", "dbo.SkillSets");
            DropForeignKey("dbo.EmployeeSkillSets", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.EmployeeMailMessages", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.EmployeeMailMessages", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.EmployeeDocumentInfos", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.EmployeeDocumentInfos", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.EmployeeDocumentInfos", "DocumentInfoRefId", "dbo.DocumentInfos");
            DropForeignKey("dbo.EMailLinkedWithAction", "SupervisorEmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.EMailLinkedWithAction", "InchargeEmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.EMailLinkedWithAction", "EMailInformationRefId", "dbo.EMailInformations");
            DropForeignKey("dbo.EMailInformations", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.DocumentForSkillSets", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.DocumentForSkillSets", "SkillSetRefId", "dbo.SkillSets");
            DropForeignKey("dbo.SkillSets", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.DocumentForSkillSets", "DocumentRefId", "dbo.DocumentInfos");
            DropForeignKey("dbo.DocumentInfos", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.CommonMailMessages", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.AttendanceLogs", "AuthorisedBy", "dbo.PersonalInformations");
            DropForeignKey("dbo.AttendanceLogs", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.PersonalInformations", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.PersonalInformations", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.AttendanceLogs", "AttendanceMachineLocationRefId", "dbo.AttendanceMachineLocations");
            DropForeignKey("dbo.AttendanceMachineLocations", "LocationRefId", "dbo.Locations");
            DropIndex("dbo.YearWiseLeaveAllowedForEmployees", new[] { "LeaveTypeRefCode" });
            DropIndex("dbo.YearWiseLeaveAllowedForEmployees", new[] { "EmployeeRefId" });
            DropIndex("dbo.WorkDays", "DayOfWeekShortName");
            DropIndex("dbo.WorkDays", "TenantId_DayOfWeekRefName");
            DropIndex("dbo.WorkDays", "TenantId_DayOfWeekRefId");
            DropIndex("dbo.UserDefaultInformations", new[] { "EmployeeRefId" });
            DropIndex("dbo.UserDefaultInformations", new[] { "UserId" });
            DropIndex("dbo.SalaryInfos", new[] { "TenantId" });
            DropIndex("dbo.SalaryInfos", new[] { "EmployeeRefId" });
            DropIndex("dbo.PublicHolidays", new[] { "TenantId" });
            DropIndex("dbo.LeaveRequests", new[] { "CommonMailMessageRefId" });
            DropIndex("dbo.LeaveRequests", new[] { "LeaveTypeRefCode" });
            DropIndex("dbo.LeaveRequests", new[] { "EmployeeRefId" });
            DropIndex("dbo.LeaveExhaustedLists", new[] { "TenantId" });
            DropIndex("dbo.LeaveExhaustedLists", new[] { "LeaveExhaustedRefId" });
            DropIndex("dbo.LeaveExhaustedLists", new[] { "LeaveTypeRefId" });
            DropIndex("dbo.GpsAttendances", new[] { "EmployeeRefId" });
            DropIndex("dbo.EmployeeSkillSets", new[] { "TenantId" });
            DropIndex("dbo.EmployeeSkillSets", "MPK_EmpRefId_SkillRefId");
            DropIndex("dbo.EmployeeMailMessages", new[] { "TenantId" });
            DropIndex("dbo.EmployeeMailMessages", new[] { "EmployeeRefId" });
            DropIndex("dbo.EmployeeDocumentInfos", new[] { "TenantId" });
            DropIndex("dbo.EmployeeDocumentInfos", new[] { "DocumentInfoRefId" });
            DropIndex("dbo.EmployeeDocumentInfos", new[] { "EmployeeRefId" });
            DropIndex("dbo.EMailLinkedWithAction", new[] { "InchargeEmployeeRefId" });
            DropIndex("dbo.EMailLinkedWithAction", new[] { "SupervisorEmployeeRefId" });
            DropIndex("dbo.EMailLinkedWithAction", new[] { "EMailInformationRefId" });
            DropIndex("dbo.EMailInformations", new[] { "TenantId" });
            DropIndex("dbo.SkillSets", new[] { "TenantId" });
            DropIndex("dbo.SkillSets", "SkillCode");
            DropIndex("dbo.DocumentInfos", new[] { "TenantId" });
            DropIndex("dbo.DocumentForSkillSets", new[] { "TenantId" });
            DropIndex("dbo.DocumentForSkillSets", "MPK_DocumentRefId_SkillRefId");
            DropIndex("dbo.CommonMailMessages", new[] { "TenantId" });
            DropIndex("dbo.PersonalInformations", new[] { "TenantId" });
            DropIndex("dbo.PersonalInformations", "EmployeeCode");
            DropIndex("dbo.PersonalInformations", "BioMetricCode");
            DropIndex("dbo.PersonalInformations", new[] { "LocationRefId" });
            DropIndex("dbo.AttendanceMachineLocations", "MachinePrefix");
            DropIndex("dbo.AttendanceMachineLocations", new[] { "LocationRefId" });
            DropIndex("dbo.AttendanceLogs", new[] { "AuthorisedBy" });
            DropIndex("dbo.AttendanceLogs", new[] { "EmployeeRefId" });
            DropIndex("dbo.AttendanceLogs", new[] { "AttendanceMachineLocationRefId" });
            DropColumn("dbo.Companies", "WeekDayTreatAsHalfDay");
            DropColumn("dbo.Companies", "NeedToTreatAnyWorkDayAsHalfDay");
            DropTable("dbo.YearWiseLeaveAllowedForEmployees",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_YearWiseLeaveAllowedForEmployee_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_YearWiseLeaveAllowedForEmployee_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.WorkDays",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_WorkDay_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_WorkDay_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.UserDefaultInformations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserDefaultInformation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_UserDefaultInformation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SalaryInfos",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalaryInfo_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalaryInfo_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PublicHolidays",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PublicHoliday_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PublicHoliday_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.LeaveRequests",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LeaveRequest_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.LeaveTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LeaveType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.LeaveExhaustedLists",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LeaveExhaustedList_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_LeaveExhaustedList_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.GpsLive");
            DropTable("dbo.GpsAttendances",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_GpsAttendance_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EmployeeSkillSets",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeSkillSet_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeSkillSet_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EmployeeMailMessages",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeMailMessage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeMailMessage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EmployeeDocumentInfos",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeDocumentInfo_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeDocumentInfo_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EMailLinkedWithAction",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EMailLinkedWithActions_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EMailInformations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EMailInformation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EMailInformation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SkillSets",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SkillSet_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SkillSet_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DocumentInfos",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DocumentInfo_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DocumentInfo_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DocumentForSkillSets",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DocumentForSkillSet_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DocumentForSkillSet_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.CommonMailMessages",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CommonMailMessage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CommonMailMessage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PersonalInformations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PersonalInformation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PersonalInformation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.AttendanceMachineLocations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_AttendanceMachineLocation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_AttendanceMachineLocation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.AttendanceLogs",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_AttendanceLog_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_AttendanceLog_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
