namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDepartmentToCaps : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PriceTags", "Departments", c => c.String());
            AddColumn("dbo.ScreenMenus", "Departments", c => c.String());
            DropColumn("dbo.PriceTags", "Deparment");
            DropColumn("dbo.ScreenMenus", "Deparment");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ScreenMenus", "Deparment", c => c.String());
            AddColumn("dbo.PriceTags", "Deparment", c => c.String());
            DropColumn("dbo.ScreenMenus", "Departments");
            DropColumn("dbo.PriceTags", "Departments");
        }
    }
}
