namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Supplier_PO : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchaseOrders", "ExportDateToAccountSoftware", c => c.DateTime());
            AlterColumn("dbo.Materials", "Hsncode", c => c.String(maxLength: 100));
            AlterColumn("dbo.Materials", "AddOn", c => c.String(maxLength: 200));
            AlterColumn("dbo.PurchaseOrders", "PoReferenceCode", c => c.String(nullable: false, maxLength: 70));
            AlterColumn("dbo.PurchaseOrders", "QuoteReference", c => c.String(maxLength: 70));
            AlterColumn("dbo.Suppliers", "SupplierName", c => c.String(nullable: false, maxLength: 80));
            AlterColumn("dbo.Suppliers", "Address1", c => c.String());
            AlterColumn("dbo.Suppliers", "Address2", c => c.String());
            AlterColumn("dbo.Suppliers", "Address3", c => c.String());
            AlterColumn("dbo.Suppliers", "City", c => c.String());
            AlterColumn("dbo.Suppliers", "State", c => c.String());
            AlterColumn("dbo.Suppliers", "StateCode", c => c.String());
            AlterColumn("dbo.Suppliers", "Country", c => c.String());
            AlterColumn("dbo.Suppliers", "ZipCode", c => c.String());
            AlterColumn("dbo.Suppliers", "PhoneNumber1", c => c.String());
            AlterColumn("dbo.Suppliers", "OrderPlacedThrough", c => c.String());
            AlterColumn("dbo.Suppliers", "Email", c => c.String());
            AlterColumn("dbo.Suppliers", "FaxNumber", c => c.String());
            AlterColumn("dbo.Suppliers", "Website", c => c.String());
            AlterColumn("dbo.Suppliers", "SupplierCode", c => c.String());
            AlterColumn("dbo.Suppliers", "TaxRegistrationNumber", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Suppliers", "TaxRegistrationNumber", c => c.String(maxLength: 50));
            AlterColumn("dbo.Suppliers", "SupplierCode", c => c.String(maxLength: 50));
            AlterColumn("dbo.Suppliers", "Website", c => c.String(maxLength: 50));
            AlterColumn("dbo.Suppliers", "FaxNumber", c => c.String(maxLength: 50));
            AlterColumn("dbo.Suppliers", "Email", c => c.String(maxLength: 50));
            AlterColumn("dbo.Suppliers", "OrderPlacedThrough", c => c.String(maxLength: 50));
            AlterColumn("dbo.Suppliers", "PhoneNumber1", c => c.String(maxLength: 50));
            AlterColumn("dbo.Suppliers", "ZipCode", c => c.String(maxLength: 50));
            AlterColumn("dbo.Suppliers", "Country", c => c.String(maxLength: 50));
            AlterColumn("dbo.Suppliers", "StateCode", c => c.String(maxLength: 50));
            AlterColumn("dbo.Suppliers", "State", c => c.String(maxLength: 50));
            AlterColumn("dbo.Suppliers", "City", c => c.String(maxLength: 50));
            AlterColumn("dbo.Suppliers", "Address3", c => c.String(maxLength: 50));
            AlterColumn("dbo.Suppliers", "Address2", c => c.String(maxLength: 50));
            AlterColumn("dbo.Suppliers", "Address1", c => c.String(maxLength: 50));
            AlterColumn("dbo.Suppliers", "SupplierName", c => c.String(nullable: false, maxLength: 60));
            AlterColumn("dbo.PurchaseOrders", "QuoteReference", c => c.String(maxLength: 50));
            AlterColumn("dbo.PurchaseOrders", "PoReferenceCode", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Materials", "AddOn", c => c.String(maxLength: 50));
            AlterColumn("dbo.Materials", "Hsncode", c => c.String(maxLength: 50));
            DropColumn("dbo.PurchaseOrders", "ExportDateToAccountSoftware");
        }
    }
}
