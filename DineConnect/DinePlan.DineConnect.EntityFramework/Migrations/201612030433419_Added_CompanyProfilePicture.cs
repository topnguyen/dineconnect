namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_CompanyProfilePicture : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "CompanyProfilePictureId", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "CompanyProfilePictureId");
        }
    }
}
