namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmployeeTimeIn : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EmployeeInfos", "EmployeeDepartmentId", "dbo.EmployeeDepartments");
            DropForeignKey("dbo.EmployeeInfos", "UserId", "dbo.AbpUsers");
            DropIndex("dbo.EmployeeInfos", new[] { "UserId" });
            DropIndex("dbo.EmployeeInfos", new[] { "EmployeeDepartmentId" });
            CreateTable(
                "dbo.EmployeeAttendances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        EmployeeRefId = c.Int(nullable: false),
                        CheckTime = c.DateTime(nullable: false),
                        CheckType = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeAttendance_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeAttendance_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EmployeeInfos", t => t.EmployeeRefId, cascadeDelete: true)
                .ForeignKey("dbo.Locations", t => t.LocationRefId, cascadeDelete: true)
                .Index(t => t.LocationRefId)
                .Index(t => t.EmployeeRefId);
            
            CreateTable(
                "dbo.EmployeeRawInfoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Identity = c.String(),
                        IdenitityData = c.Binary(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FreeValuePromotionExecution",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FreeValuePromotionId = c.Int(nullable: false),
                        MenuItemId = c.Int(nullable: false),
                        PortionName = c.String(),
                        MenuItemPortionId = c.Int(nullable: false),
                        From = c.Boolean(nullable: false),
                        ValueType = c.Int(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FreeValuePromotions", t => t.FreeValuePromotionId, cascadeDelete: true)
                .Index(t => t.FreeValuePromotionId);
            
            CreateTable(
                "dbo.FreeValuePromotions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PromotionId = c.Int(nullable: false),
                        AllFrom = c.Boolean(nullable: false),
                        FromCount = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Promotions", t => t.PromotionId, cascadeDelete: true)
                .Index(t => t.PromotionId);
            
            AddColumn("dbo.ConnectMembers", "UserId", c => c.Long());
            AddColumn("dbo.PromotionSchedules", "MonthDays", c => c.String());
            AddColumn("dbo.EmployeeInfos", "DinePlanUserId", c => c.Int());
            AddColumn("dbo.EmployeeInfos", "RawId", c => c.Int());
            AddColumn("dbo.ProductComboGroups", "Maximum", c => c.Int(nullable: false));
            AddColumn("dbo.TillTransactions", "LocalId", c => c.Int(nullable: false));
            AlterColumn("dbo.EmployeeInfos", "UserId", c => c.Long());
            AlterColumn("dbo.EmployeeInfos", "EmployeeDepartmentId", c => c.Int());
            CreateIndex("dbo.ConnectMembers", "UserId");
            CreateIndex("dbo.EmployeeInfos", "UserId");
            CreateIndex("dbo.EmployeeInfos", "DinePlanUserId");
            CreateIndex("dbo.EmployeeInfos", "EmployeeDepartmentId");
            CreateIndex("dbo.EmployeeInfos", "RawId");
            CreateIndex("dbo.TillTransactions", "LocationId");
            AddForeignKey("dbo.ConnectMembers", "UserId", "dbo.AbpUsers", "Id");
            AddForeignKey("dbo.EmployeeInfos", "DinePlanUserId", "dbo.DinePlanUsers", "Id");
            AddForeignKey("dbo.EmployeeInfos", "RawId", "dbo.EmployeeRawInfoes", "Id");
            AddForeignKey("dbo.TillTransactions", "LocationId", "dbo.Locations", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EmployeeInfos", "EmployeeDepartmentId", "dbo.EmployeeDepartments", "Id");
            AddForeignKey("dbo.EmployeeInfos", "UserId", "dbo.AbpUsers", "Id");
            DropColumn("dbo.EmployeeInfos", "BioMetricCode");
            DropColumn("dbo.EmployeeInfos", "PIN");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EmployeeInfos", "PIN", c => c.String());
            AddColumn("dbo.EmployeeInfos", "BioMetricCode", c => c.String());
            DropForeignKey("dbo.EmployeeInfos", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.EmployeeInfos", "EmployeeDepartmentId", "dbo.EmployeeDepartments");
            DropForeignKey("dbo.TillTransactions", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.FreeValuePromotions", "PromotionId", "dbo.Promotions");
            DropForeignKey("dbo.FreeValuePromotionExecution", "FreeValuePromotionId", "dbo.FreeValuePromotions");
            DropForeignKey("dbo.EmployeeAttendances", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.EmployeeAttendances", "EmployeeRefId", "dbo.EmployeeInfos");
            DropForeignKey("dbo.EmployeeInfos", "RawId", "dbo.EmployeeRawInfoes");
            DropForeignKey("dbo.EmployeeInfos", "DinePlanUserId", "dbo.DinePlanUsers");
            DropForeignKey("dbo.ConnectMembers", "UserId", "dbo.AbpUsers");
            DropIndex("dbo.TillTransactions", new[] { "LocationId" });
            DropIndex("dbo.FreeValuePromotions", new[] { "PromotionId" });
            DropIndex("dbo.FreeValuePromotionExecution", new[] { "FreeValuePromotionId" });
            DropIndex("dbo.EmployeeInfos", new[] { "RawId" });
            DropIndex("dbo.EmployeeInfos", new[] { "EmployeeDepartmentId" });
            DropIndex("dbo.EmployeeInfos", new[] { "DinePlanUserId" });
            DropIndex("dbo.EmployeeInfos", new[] { "UserId" });
            DropIndex("dbo.EmployeeAttendances", new[] { "EmployeeRefId" });
            DropIndex("dbo.EmployeeAttendances", new[] { "LocationRefId" });
            DropIndex("dbo.ConnectMembers", new[] { "UserId" });
            AlterColumn("dbo.EmployeeInfos", "EmployeeDepartmentId", c => c.Int(nullable: false));
            AlterColumn("dbo.EmployeeInfos", "UserId", c => c.Long(nullable: false));
            DropColumn("dbo.TillTransactions", "LocalId");
            DropColumn("dbo.ProductComboGroups", "Maximum");
            DropColumn("dbo.EmployeeInfos", "RawId");
            DropColumn("dbo.EmployeeInfos", "DinePlanUserId");
            DropColumn("dbo.PromotionSchedules", "MonthDays");
            DropColumn("dbo.ConnectMembers", "UserId");
            DropTable("dbo.FreeValuePromotions");
            DropTable("dbo.FreeValuePromotionExecution");
            DropTable("dbo.EmployeeRawInfoes");
            DropTable("dbo.EmployeeAttendances",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeAttendance_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeAttendance_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            CreateIndex("dbo.EmployeeInfos", "EmployeeDepartmentId");
            CreateIndex("dbo.EmployeeInfos", "UserId");
            AddForeignKey("dbo.EmployeeInfos", "UserId", "dbo.AbpUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EmployeeInfos", "EmployeeDepartmentId", "dbo.EmployeeDepartments", "Id", cascadeDelete: true);
        }
    }
}
