namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeSystemLoggingEntityTypeModifyFields : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SystemLoggings", "log_date", c => c.DateTime(nullable: false));
            DropColumn("dbo.SystemLoggings", "entered_date");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SystemLoggings", "entered_date", c => c.DateTime());
            AlterColumn("dbo.SystemLoggings", "log_date", c => c.String());
        }
    }
}
