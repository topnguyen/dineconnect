namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TaxMember : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FullTaxAddresses", "PostCode", c => c.String());
            AddColumn("dbo.FullTaxMembers", "BranchName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FullTaxMembers", "BranchName");
            DropColumn("dbo.FullTaxAddresses", "PostCode");
        }
    }
}
