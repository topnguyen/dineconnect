namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_FoodType_StarItem_To_MenuItem_tbl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItems", "FoodType", c => c.Int(nullable: false));
            AddColumn("dbo.MenuItems", "StarItem", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MenuItems", "StarItem");
            DropColumn("dbo.MenuItems", "FoodType");
        }
    }
}
