namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddPostData : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.PreTickets", newName: "PostDatas");
            DropIndex("dbo.PostDatas", "IX_FirstAndSecond");
            AlterTableAnnotations(
                "dbo.PostDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationId = c.Int(nullable: false),
                        Contents = c.String(),
                        Errors = c.String(),
                        Processed = c.Boolean(nullable: false),
                        ContentType = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_PostData_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                    { 
                        "DynamicFilter_PostData_SoftDelete",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                    { 
                        "DynamicFilter_PreTicket_SoftDelete",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AddColumn("dbo.PostDatas", "ContentType", c => c.Int(nullable: false));
            AddColumn("dbo.PostDatas", "TenantId", c => c.Int(nullable: false));
            CreateIndex("dbo.PostDatas", "LocationId");
            DropColumn("dbo.PostDatas", "TicketNumber");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PostDatas", "TicketNumber", c => c.String(maxLength: 30));
            DropIndex("dbo.PostDatas", new[] { "LocationId" });
            DropColumn("dbo.PostDatas", "TenantId");
            DropColumn("dbo.PostDatas", "ContentType");
            AlterTableAnnotations(
                "dbo.PostDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationId = c.Int(nullable: false),
                        Contents = c.String(),
                        Errors = c.String(),
                        Processed = c.Boolean(nullable: false),
                        ContentType = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_PostData_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                    { 
                        "DynamicFilter_PostData_SoftDelete",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                    { 
                        "DynamicFilter_PreTicket_SoftDelete",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            CreateIndex("dbo.PostDatas", new[] { "LocationId", "TicketNumber" }, unique: true, name: "IX_FirstAndSecond");
            RenameTable(name: "dbo.PostDatas", newName: "PreTickets");
        }
    }
}
