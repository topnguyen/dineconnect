namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeCardTypeOrganization : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.ConnectCardTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 30),
                        UseOneTime = c.Boolean(nullable: false),
                        Oid = c.Int(nullable: false),
                        Length = c.Int(nullable: false),
                        ConnectCardTypeCategoryId = c.Int(),
                        Tag = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ConnectCardType_ConnectFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            DropColumn("dbo.ConnectCardTypes", "Locations");
            DropColumn("dbo.ConnectCardTypes", "NonLocations");
            DropColumn("dbo.ConnectCardTypes", "Group");
            DropColumn("dbo.ConnectCardTypes", "LocationTag");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ConnectCardTypes", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.ConnectCardTypes", "Group", c => c.Boolean(nullable: false));
            AddColumn("dbo.ConnectCardTypes", "NonLocations", c => c.String());
            AddColumn("dbo.ConnectCardTypes", "Locations", c => c.String());
            AlterTableAnnotations(
                "dbo.ConnectCardTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 30),
                        UseOneTime = c.Boolean(nullable: false),
                        Oid = c.Int(nullable: false),
                        Length = c.Int(nullable: false),
                        ConnectCardTypeCategoryId = c.Int(),
                        Tag = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ConnectCardType_ConnectFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
        }
    }
}
