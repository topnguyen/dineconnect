namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Unitconversion_MaterialWise : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UnitConversions", "MaterialRefId", c => c.Int());
            CreateIndex("dbo.UnitConversions", "MaterialRefId");
            AddForeignKey("dbo.UnitConversions", "MaterialRefId", "dbo.Materials", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UnitConversions", "MaterialRefId", "dbo.Materials");
            DropIndex("dbo.UnitConversions", new[] { "MaterialRefId" });
            DropColumn("dbo.UnitConversions", "MaterialRefId");
        }
    }
}
