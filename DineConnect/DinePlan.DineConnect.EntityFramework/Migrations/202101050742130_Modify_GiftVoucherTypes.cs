namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_GiftVoucherTypes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GiftVoucherTypes", "CategoryId", c => c.Int());
            CreateIndex("dbo.GiftVoucherTypes", "CategoryId");
            AddForeignKey("dbo.GiftVoucherTypes", "CategoryId", "dbo.Categories", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GiftVoucherTypes", "CategoryId", "dbo.Categories");
            DropIndex("dbo.GiftVoucherTypes", new[] { "CategoryId" });
            DropColumn("dbo.GiftVoucherTypes", "CategoryId");
        }
    }
}
