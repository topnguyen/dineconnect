namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_TicketDiscountPromotions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TicketDiscountPromotions", "ApplyAsPayment", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TicketDiscountPromotions", "ApplyAsPayment");
        }
    }
}
