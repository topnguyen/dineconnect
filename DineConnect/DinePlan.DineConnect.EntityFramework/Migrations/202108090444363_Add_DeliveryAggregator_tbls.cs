namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Add_DeliveryAggregator_tbls : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DeliveryAggPlatforms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DeliveryAggregatorId = c.Int(nullable: false),
                        DeliveryAggType = c.Int(nullable: false),
                        Active = c.Int(nullable: false),
                        TotalActiveItems = c.Int(nullable: false),
                        TotalInActiveItems = c.Int(nullable: false),
                        MenuContents = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DeliveryAggPlatform_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DeliveryAggregators", t => t.DeliveryAggregatorId, cascadeDelete: true)
                .Index(t => t.DeliveryAggregatorId);
            
            CreateTable(
                "dbo.DeliveryAggregators",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DeliveryAggregator_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DeliveryAggregator_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .Index(t => t.LocationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DeliveryAggPlatforms", "DeliveryAggregatorId", "dbo.DeliveryAggregators");
            DropForeignKey("dbo.DeliveryAggregators", "LocationId", "dbo.Locations");
            DropIndex("dbo.DeliveryAggregators", new[] { "LocationId" });
            DropIndex("dbo.DeliveryAggPlatforms", new[] { "DeliveryAggregatorId" });
            DropTable("dbo.DeliveryAggregators",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DeliveryAggregator_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DeliveryAggregator_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DeliveryAggPlatforms",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DeliveryAggPlatform_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
