namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_ImportCardDataDetails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ImportCardDataDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        JobName = c.String(),
                        FileName = c.String(),
                        FileLocation = c.String(),
                        StartTime = c.DateTime(),
                        EndTime = c.DateTime(),
                        OutputJsonDto = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ImportCardDataDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ImportCardDataDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ImportCardDataDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
