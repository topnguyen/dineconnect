namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_IssueRecipeDetail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.IssueRecipeDetails", "ProductionUnitRefId", c => c.Int(nullable: false));
            DropColumn("dbo.IssueRecipeDetails", "ProductionUomRefId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.IssueRecipeDetails", "ProductionUomRefId", c => c.Int(nullable: false));
            DropColumn("dbo.IssueRecipeDetails", "ProductionUnitRefId");
        }
    }
}
