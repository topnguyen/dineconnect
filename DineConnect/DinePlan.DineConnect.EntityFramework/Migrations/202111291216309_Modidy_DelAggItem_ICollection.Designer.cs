// <auto-generated />
namespace DinePlan.DineConnect.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Modidy_DelAggItem_ICollection : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Modidy_DelAggItem_ICollection));
        
        string IMigrationMetadata.Id
        {
            get { return "202111291216309_Modidy_DelAggItem_ICollection"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
