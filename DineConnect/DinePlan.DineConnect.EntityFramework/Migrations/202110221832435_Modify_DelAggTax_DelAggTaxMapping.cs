namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_DelAggTax_DelAggTaxMapping : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DelAggTaxes", "TaxTypeId", c => c.Int(nullable: false));
            AddColumn("dbo.DelAggTaxMappings", "DelAggTaxId", c => c.Int(nullable: false));
            CreateIndex("dbo.DelAggTaxMappings", "DelAggTaxId");
            AddForeignKey("dbo.DelAggTaxMappings", "DelAggTaxId", "dbo.DelAggTaxes", "Id");
            DropColumn("dbo.DelAggTaxes", "TaxType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DelAggTaxes", "TaxType", c => c.String());
            DropForeignKey("dbo.DelAggTaxMappings", "DelAggTaxId", "dbo.DelAggTaxes");
            DropIndex("dbo.DelAggTaxMappings", new[] { "DelAggTaxId" });
            DropColumn("dbo.DelAggTaxMappings", "DelAggTaxId");
            DropColumn("dbo.DelAggTaxes", "TaxTypeId");
        }
    }
}
