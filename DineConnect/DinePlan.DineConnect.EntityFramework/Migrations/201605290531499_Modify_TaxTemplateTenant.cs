namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_TaxTemplateTenant : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.TaxTemplateMappings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaxRefId = c.Int(nullable: false),
                        CompanyRefId = c.Int(),
                        LocationRefId = c.Int(),
                        MaterialGroupRefId = c.Int(),
                        MaterialGroupCategoryRefId = c.Int(),
                        MaterialRefId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_TaxTemplateMapping_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AddColumn("dbo.TaxTemplateMappings", "TenantId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TaxTemplateMappings", "TenantId");
            AlterTableAnnotations(
                "dbo.TaxTemplateMappings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaxRefId = c.Int(nullable: false),
                        CompanyRefId = c.Int(),
                        LocationRefId = c.Int(),
                        MaterialGroupRefId = c.Int(),
                        MaterialGroupCategoryRefId = c.Int(),
                        MaterialRefId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_TaxTemplateMapping_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
        }
    }
}
