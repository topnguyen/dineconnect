namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSubCategoryButtons : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ScreenMenuCategories", "SubButtonHeight", c => c.Int(nullable: false));
            AddColumn("dbo.ScreenMenuCategories", "SubButtonRows", c => c.Int(nullable: false));
            AddColumn("dbo.ScreenMenuCategories", "SubButtonColorDef", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ScreenMenuCategories", "SubButtonColorDef");
            DropColumn("dbo.ScreenMenuCategories", "SubButtonRows");
            DropColumn("dbo.ScreenMenuCategories", "SubButtonHeight");
        }
    }
}
