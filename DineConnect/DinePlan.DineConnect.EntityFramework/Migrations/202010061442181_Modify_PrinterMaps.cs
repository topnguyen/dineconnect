namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_PrinterMaps : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PrinterMaps", "PrinterId", "dbo.Printers");
            DropForeignKey("dbo.PrinterMaps", "PrintTemplateId", "dbo.PrintTemplates");
            DropIndex("dbo.PrinterMaps", new[] { "PrinterId" });
            DropIndex("dbo.PrinterMaps", new[] { "PrintTemplateId" });
            AddColumn("dbo.PrinterMaps", "CategoryId", c => c.Int(nullable: false));
            AddColumn("dbo.PrinterMaps", "CategoryName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PrinterMaps", "CategoryName");
            DropColumn("dbo.PrinterMaps", "CategoryId");
            CreateIndex("dbo.PrinterMaps", "PrintTemplateId");
            CreateIndex("dbo.PrinterMaps", "PrinterId");
            AddForeignKey("dbo.PrinterMaps", "PrintTemplateId", "dbo.PrintTemplates", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PrinterMaps", "PrinterId", "dbo.Printers", "Id", cascadeDelete: true);
        }
    }
}
