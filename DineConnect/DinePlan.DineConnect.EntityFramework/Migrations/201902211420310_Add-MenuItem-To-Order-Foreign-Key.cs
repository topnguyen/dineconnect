namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMenuItemToOrderForeignKey : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Orders", "MenuItemId");
            AddForeignKey("dbo.Orders", "MenuItemId", "dbo.MenuItems", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "MenuItemId", "dbo.MenuItems");
            DropIndex("dbo.Orders", new[] { "MenuItemId" });
        }
    }
}
