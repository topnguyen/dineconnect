namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_DineGoDevices : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DineGoDevices", "ChangeGuid", c => c.Guid(nullable: false));
            DropColumn("dbo.DineGoDevices", "BannerImage");
            DropColumn("dbo.DineGoDevices", "AdsImage");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DineGoDevices", "AdsImage", c => c.String());
            AddColumn("dbo.DineGoDevices", "BannerImage", c => c.String());
            DropColumn("dbo.DineGoDevices", "ChangeGuid");
        }
    }
}
