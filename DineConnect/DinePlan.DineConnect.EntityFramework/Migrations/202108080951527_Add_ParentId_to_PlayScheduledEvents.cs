namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ParentId_to_PlayScheduledEvents : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlayScheduledEvents", "ParentId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlayScheduledEvents", "ParentId");
        }
    }
}
