namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePaymentTypeGroupName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentTypes", "PaymentGroup", c => c.String());
            Sql("Update PaymentTypes set PaymentGroup = [Group];");
            Sql("Update PaymentTypes set [Group] = '0';");
            AlterColumn("dbo.PaymentTypes", "Group", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PaymentTypes", "Group", c => c.String());
            DropColumn("dbo.PaymentTypes", "PaymentGroup");
        }
    }
}
