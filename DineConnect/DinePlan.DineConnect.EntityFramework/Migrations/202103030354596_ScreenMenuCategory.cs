namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ScreenMenuCategory : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ScreenMenuCategories", "FromHour");
            DropColumn("dbo.ScreenMenuCategories", "FromMinute");
            DropColumn("dbo.ScreenMenuCategories", "ToHour");
            DropColumn("dbo.ScreenMenuCategories", "ToMinute");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ScreenMenuCategories", "ToMinute", c => c.Int(nullable: false));
            AddColumn("dbo.ScreenMenuCategories", "ToHour", c => c.Int(nullable: false));
            AddColumn("dbo.ScreenMenuCategories", "FromMinute", c => c.Int(nullable: false));
            AddColumn("dbo.ScreenMenuCategories", "FromHour", c => c.Int(nullable: false));
        }
    }
}
