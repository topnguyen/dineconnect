namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyIssueCompleteStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Issues", "CompletedStatus", c => c.Boolean(nullable: false,defaultValue:false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Issues", "CompletedStatus");
        }
    }
}
