namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class empinfonotes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmployeeInfos", "Notes", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmployeeInfos", "Notes");
        }
    }
}
