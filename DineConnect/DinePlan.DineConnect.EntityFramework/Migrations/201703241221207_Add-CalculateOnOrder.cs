namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCalculateOnOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MemberPoints", "CalculationOnOrder", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MemberPoints", "CalculationOnOrder");
        }
    }
}
