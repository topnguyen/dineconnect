namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTicketReference : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "ReferenceTicket", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "ReferenceTicket");
        }
    }
}
