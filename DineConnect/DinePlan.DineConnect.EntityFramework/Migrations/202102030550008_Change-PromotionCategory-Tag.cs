namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePromotionCategoryTag : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.PromotionCategories", "Locations");
            DropColumn("dbo.PromotionCategories", "NonLocations");
            DropColumn("dbo.PromotionCategories", "Group");
            DropColumn("dbo.PromotionCategories", "LocationTag");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PromotionCategories", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.PromotionCategories", "Group", c => c.Boolean(nullable: false));
            AddColumn("dbo.PromotionCategories", "NonLocations", c => c.String());
            AddColumn("dbo.PromotionCategories", "Locations", c => c.String());
        }
    }
}
