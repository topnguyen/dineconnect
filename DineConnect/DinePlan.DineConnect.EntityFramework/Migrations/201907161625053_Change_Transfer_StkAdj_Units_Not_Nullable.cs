namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change_Transfer_StkAdj_Units_Not_Nullable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Materials", "ReceivingUnitId", "dbo.Units");
            DropForeignKey("dbo.Materials", "StockAdjustmentUnitId", "dbo.Units");
            DropForeignKey("dbo.Materials", "TransferUnitId", "dbo.Units");
            DropIndex("dbo.Materials", new[] { "ReceivingUnitId" });
            DropIndex("dbo.Materials", new[] { "TransferUnitId" });
            DropIndex("dbo.Materials", new[] { "StockAdjustmentUnitId" });
            AlterColumn("dbo.Materials", "ReceivingUnitId", c => c.Int(nullable: false));
            AlterColumn("dbo.Materials", "TransferUnitId", c => c.Int(nullable: false));
            AlterColumn("dbo.Materials", "StockAdjustmentUnitId", c => c.Int(nullable: false));
            CreateIndex("dbo.Materials", "ReceivingUnitId");
            CreateIndex("dbo.Materials", "TransferUnitId");
            CreateIndex("dbo.Materials", "StockAdjustmentUnitId");
            AddForeignKey("dbo.Materials", "ReceivingUnitId", "dbo.Units", "Id");
            AddForeignKey("dbo.Materials", "StockAdjustmentUnitId", "dbo.Units", "Id");
            AddForeignKey("dbo.Materials", "TransferUnitId", "dbo.Units", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Materials", "TransferUnitId", "dbo.Units");
            DropForeignKey("dbo.Materials", "StockAdjustmentUnitId", "dbo.Units");
            DropForeignKey("dbo.Materials", "ReceivingUnitId", "dbo.Units");
            DropIndex("dbo.Materials", new[] { "StockAdjustmentUnitId" });
            DropIndex("dbo.Materials", new[] { "TransferUnitId" });
            DropIndex("dbo.Materials", new[] { "ReceivingUnitId" });
            AlterColumn("dbo.Materials", "StockAdjustmentUnitId", c => c.Int());
            AlterColumn("dbo.Materials", "TransferUnitId", c => c.Int());
            AlterColumn("dbo.Materials", "ReceivingUnitId", c => c.Int());
            CreateIndex("dbo.Materials", "StockAdjustmentUnitId");
            CreateIndex("dbo.Materials", "TransferUnitId");
            CreateIndex("dbo.Materials", "ReceivingUnitId");
            AddForeignKey("dbo.Materials", "TransferUnitId", "dbo.Units", "Id");
            AddForeignKey("dbo.Materials", "StockAdjustmentUnitId", "dbo.Units", "Id");
            AddForeignKey("dbo.Materials", "ReceivingUnitId", "dbo.Units", "Id");
        }
    }
}
