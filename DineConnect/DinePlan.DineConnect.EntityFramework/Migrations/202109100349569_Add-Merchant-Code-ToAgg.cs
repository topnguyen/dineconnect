namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMerchantCodeToAgg : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DeliveryAggPlatforms", "MerchantCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DeliveryAggPlatforms", "MerchantCode");
        }
    }
}
